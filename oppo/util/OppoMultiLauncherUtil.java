package oppo.util;

import android.os.SystemProperties;
import com.oplus.multiapp.OplusMultiAppManager;
import java.util.Arrays;
import java.util.List;

@Deprecated
public class OppoMultiLauncherUtil {
  public static final String ACTION_MULTI_APP_RENAME = "oppo.intent.action.MULTI_APP_RENAME";
  
  public static final String ACTION_PACKAGE_ADDED = "oppo.intent.action.MULTI_APP_PACKAGE_ADDED";
  
  public static final String ACTION_PACKAGE_REMOVED = "oppo.intent.action.MULTI_APP_PACKAGE_REMOVED";
  
  public static boolean DEBUG_MULTI_APP = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private static final List<String> MULTIAPP_SETTINGS = Arrays.asList(new String[] { "show_password", "screen_off_timeout", "time_12_24" });
  
  public static final String MULTI_APP_ADDED = "multi_app_add";
  
  public static final String MULTI_APP_EXTRA = "user_id";
  
  public static final String MULTI_APP_REMOVED = "multi_app_remove";
  
  public static final String MULTI_CHANGE_USERID = "multi.change.userid";
  
  public static final String MULTI_TAG = "com.multiple.launcher";
  
  public static final String OPLUS_ALLOWED_APP_FILE = "/data/oppo/coloros/multiapp/oppo_allowed_app.xml";
  
  public static final String OPLUS_MULTI_APP_ALIAS_FILE = "/data/oppo/coloros/multiapp/oppo_multi_app_alias.xml";
  
  public static final String OPLUS_MULTI_APP_CREATED_FILE = "/data/oppo/coloros/multiapp/oppo_multi_app.xml";
  
  private static final String TAG = "OppoMultiLauncherUtil";
  
  public static final int USER_ID = 999;
  
  public static final int USER_ORIGINAL = 0;
  
  private static volatile OppoMultiLauncherUtil sMultiUtil = null;
  
  public static OppoMultiLauncherUtil getInstance() {
    if (sMultiUtil == null)
      sMultiUtil = new OppoMultiLauncherUtil(); 
    return sMultiUtil;
  }
  
  public boolean isCrossUserSetting(String paramString) {
    return MULTIAPP_SETTINGS.contains(paramString);
  }
  
  public List<String> getAllowedMultiApp() {
    return OplusMultiAppManager.getInstance().getMultiAppList(1);
  }
  
  public List<String> getCreatedMultiApp() {
    return OplusMultiAppManager.getInstance().getMultiAppList(0);
  }
  
  public String getAliasByPackage(String paramString) {
    return OplusMultiAppManager.getInstance().getMultiAppAlias(paramString);
  }
  
  public boolean isMultiApp(String paramString) {
    return getCreatedMultiApp().contains(paramString);
  }
}
