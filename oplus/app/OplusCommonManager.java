package oplus.app;

import android.os.IBinder;
import android.os.ServiceManager;

public abstract class OplusCommonManager {
  protected final IBinder mRemote;
  
  public OplusCommonManager(String paramString) {
    this(ServiceManager.getService(paramString));
  }
  
  public OplusCommonManager(IBinder paramIBinder) {
    this.mRemote = paramIBinder;
  }
}
