package oplus.content.res;

import android.os.Parcel;

public class OplusExtraConfiguration implements Comparable {
  public int mUserId = -1;
  
  public int mFontUserId = -1;
  
  public long mMaterialColor = -1L;
  
  public int mFontVariationSettings = -1;
  
  public String mIconPackName = "";
  
  public float mDarkModeDialogBgMaxL = -1.0F;
  
  public float mDarkModeBackgroundMaxL = -1.0F;
  
  public float mDarkModeForegroundMinL = -1.0F;
  
  public static final int ACESSIBLE_OPLUS_MODE_CHANGED = 67108864;
  
  public static final int CONFIG_FLIPFONT = 33554432;
  
  public static final int FONT_VARIATION_SETTINGS_CHANGED = 16777216;
  
  public static final int OPLUS_CONFIG_CHANGED = 268435456;
  
  public static final int OPLUS_DARKMODE_RANK_CHANGED = 1;
  
  public static final int THEME_NEW_SKIN_CHANGED = 150994944;
  
  public static final int THEME_OLD_SKIN_CHANGED = 134217728;
  
  public static final int UX_ICON_CONFIG_CHANGED = -2147483648;
  
  public int mAccessibleChanged;
  
  public int mFlipFont;
  
  public long mOplusConfigType;
  
  public int mThemeChanged;
  
  public long mThemeChangedFlags;
  
  public long mUxIconConfig;
  
  public int compareTo(Object paramObject) {
    return compareTo((OplusExtraConfiguration)paramObject);
  }
  
  public int compareTo(OplusExtraConfiguration paramOplusExtraConfiguration) {
    int i = this.mThemeChanged - paramOplusExtraConfiguration.mThemeChanged;
    if (i != 0)
      return i; 
    i = this.mFlipFont - paramOplusExtraConfiguration.mFlipFont;
    if (i != 0)
      return i; 
    boolean bool = this.mIconPackName.equals(paramOplusExtraConfiguration.mIconPackName);
    if (!bool)
      return -1; 
    i = this.mUserId - paramOplusExtraConfiguration.mUserId;
    if (i != 0)
      return i; 
    i = this.mFontUserId - paramOplusExtraConfiguration.mFontUserId;
    if (i != 0)
      return i; 
    i = this.mAccessibleChanged - paramOplusExtraConfiguration.mAccessibleChanged;
    if (i != 0)
      return i; 
    i = Long.compare(this.mMaterialColor, paramOplusExtraConfiguration.mMaterialColor);
    if (i != 0)
      return i; 
    i = Long.compare(this.mUxIconConfig, paramOplusExtraConfiguration.mUxIconConfig);
    if (i != 0)
      return i; 
    i = Float.compare(this.mDarkModeBackgroundMaxL, paramOplusExtraConfiguration.mDarkModeBackgroundMaxL);
    if (i != 0)
      return i; 
    i = Float.compare(this.mDarkModeDialogBgMaxL, paramOplusExtraConfiguration.mDarkModeDialogBgMaxL);
    if (i != 0)
      return i; 
    i = Float.compare(this.mDarkModeForegroundMinL, paramOplusExtraConfiguration.mDarkModeForegroundMinL);
    if (i != 0)
      return i; 
    i = this.mFontVariationSettings;
    int j = paramOplusExtraConfiguration.mFontVariationSettings;
    return i - j;
  }
  
  public void setTo(OplusExtraConfiguration paramOplusExtraConfiguration) {
    this.mThemeChanged = paramOplusExtraConfiguration.mThemeChanged;
    this.mThemeChangedFlags = paramOplusExtraConfiguration.mThemeChangedFlags;
    this.mFlipFont = paramOplusExtraConfiguration.mFlipFont;
    this.mUserId = paramOplusExtraConfiguration.mUserId;
    this.mAccessibleChanged = paramOplusExtraConfiguration.mAccessibleChanged;
    this.mUxIconConfig = paramOplusExtraConfiguration.mUxIconConfig;
    this.mFontUserId = paramOplusExtraConfiguration.mFontUserId;
    this.mMaterialColor = paramOplusExtraConfiguration.mMaterialColor;
    this.mFontVariationSettings = paramOplusExtraConfiguration.mFontVariationSettings;
    this.mIconPackName = paramOplusExtraConfiguration.mIconPackName;
    this.mOplusConfigType = paramOplusExtraConfiguration.mOplusConfigType;
    this.mDarkModeDialogBgMaxL = paramOplusExtraConfiguration.mDarkModeDialogBgMaxL;
    this.mDarkModeForegroundMinL = paramOplusExtraConfiguration.mDarkModeForegroundMinL;
    this.mDarkModeBackgroundMaxL = paramOplusExtraConfiguration.mDarkModeBackgroundMaxL;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("mThemeChanged= ");
    stringBuilder.append(this.mThemeChanged);
    stringBuilder.append(", mThemeChangedFlags= ");
    stringBuilder.append(this.mThemeChangedFlags);
    stringBuilder.append(", mFlipFont= ");
    stringBuilder.append(this.mFlipFont);
    stringBuilder.append(", mAccessibleChanged= ");
    stringBuilder.append(this.mAccessibleChanged);
    stringBuilder.append(", mUxIconConfig= ");
    stringBuilder.append(this.mUxIconConfig);
    stringBuilder.append(", mMaterialColor= ");
    stringBuilder.append(this.mMaterialColor);
    stringBuilder.append(", mUserId= ");
    stringBuilder.append(this.mUserId);
    stringBuilder.append(", mFontUserId= ");
    stringBuilder.append(this.mFontUserId);
    stringBuilder.append(", mFontVariationSettings= ");
    stringBuilder.append(Integer.toHexString(this.mFontVariationSettings));
    stringBuilder.append(", mIconPackName= ");
    stringBuilder.append(this.mIconPackName);
    stringBuilder.append(", mDarkModeBackgroundMaxL= ");
    stringBuilder.append(this.mDarkModeBackgroundMaxL);
    stringBuilder.append(", mDarkModeDialogBgMaxL= ");
    stringBuilder.append(this.mDarkModeDialogBgMaxL);
    stringBuilder.append(", mDarkModeForegroundMinL= ");
    stringBuilder.append(this.mDarkModeForegroundMinL);
    stringBuilder.append(", mOplusConfigType= ");
    stringBuilder.append(this.mOplusConfigType);
    return stringBuilder.toString();
  }
  
  public void setToDefaults() {
    this.mThemeChanged = 0;
    this.mThemeChangedFlags = 0L;
    this.mFlipFont = 0;
    this.mUserId = -1;
    this.mAccessibleChanged = 0;
    this.mUxIconConfig = 0L;
    this.mFontUserId = -1;
    this.mMaterialColor = -1L;
    this.mFontVariationSettings = 0;
    this.mIconPackName = "";
    this.mOplusConfigType = 0L;
    this.mDarkModeBackgroundMaxL = -1.0F;
    this.mDarkModeForegroundMinL = -1.0F;
    this.mDarkModeDialogBgMaxL = -1.0F;
  }
  
  public int updateFrom(OplusExtraConfiguration paramOplusExtraConfiguration) {
    int i = 0;
    int j = paramOplusExtraConfiguration.mThemeChanged, k = i;
    if (j > 0) {
      k = i;
      if (this.mThemeChanged != j) {
        k = 0x0 | 0x8000000;
        this.mThemeChanged = j;
        this.mThemeChangedFlags = paramOplusExtraConfiguration.mThemeChangedFlags;
        this.mUserId = paramOplusExtraConfiguration.mUserId;
      } 
    } 
    j = paramOplusExtraConfiguration.mAccessibleChanged;
    i = k;
    if (j != 0) {
      i = k;
      if (this.mAccessibleChanged != j) {
        i = k | 0x4000000;
        this.mAccessibleChanged = j;
        this.mUserId = paramOplusExtraConfiguration.mUserId;
      } 
    } 
    k = paramOplusExtraConfiguration.mFlipFont;
    j = i;
    if (k > 0) {
      j = i;
      if (this.mFlipFont != k) {
        j = i | 0x2000000;
        this.mFlipFont = k;
      } 
    } 
    i = paramOplusExtraConfiguration.mUserId;
    k = j;
    if (i >= 0) {
      k = j;
      if (this.mUserId != i) {
        k = j | 0x10000000;
        this.mUserId = i;
      } 
    } 
    j = paramOplusExtraConfiguration.mFontUserId;
    i = k;
    if (j >= 0) {
      i = k;
      if (this.mFontUserId != j) {
        i = k | 0x10000000;
        this.mFontUserId = j;
      } 
    } 
    long l = paramOplusExtraConfiguration.mUxIconConfig;
    k = i;
    if (l > 0L) {
      k = i;
      if (l != this.mUxIconConfig) {
        k = i | Integer.MIN_VALUE;
        this.mUxIconConfig = l;
        this.mUserId = paramOplusExtraConfiguration.mUserId;
      } 
    } 
    l = paramOplusExtraConfiguration.mMaterialColor;
    i = k;
    if (l >= 0L) {
      i = k;
      if (this.mMaterialColor != l) {
        i = k | 0x4000000;
        this.mMaterialColor = l;
        this.mUserId = paramOplusExtraConfiguration.mUserId;
      } 
    } 
    j = paramOplusExtraConfiguration.mFontVariationSettings;
    k = i;
    if (j > 0) {
      k = i;
      if (j != this.mFontVariationSettings) {
        k = i | 0x1000000;
        this.mFontVariationSettings = j;
      } 
    } 
    String str = paramOplusExtraConfiguration.mIconPackName;
    i = k;
    if (str != null) {
      i = k;
      if (!str.equals("")) {
        String str1 = paramOplusExtraConfiguration.mIconPackName;
        str = this.mIconPackName;
        i = k;
        if (!str1.equals(str)) {
          i = k | Integer.MIN_VALUE;
          this.mIconPackName = paramOplusExtraConfiguration.mIconPackName;
          this.mUserId = paramOplusExtraConfiguration.mUserId;
        } 
      } 
    } 
    j = i;
    if (isDarkModeBgChanged(paramOplusExtraConfiguration)) {
      j = i | 0x10000000;
      this.mDarkModeBackgroundMaxL = paramOplusExtraConfiguration.mDarkModeBackgroundMaxL;
      this.mOplusConfigType = 1L;
    } 
    k = j;
    if (isDarkModeDialogBgChanged(paramOplusExtraConfiguration)) {
      k = j | 0x10000000;
      this.mDarkModeDialogBgMaxL = paramOplusExtraConfiguration.mDarkModeDialogBgMaxL;
      this.mOplusConfigType = 1L;
    } 
    i = k;
    if (isDarkModeFgChanged(paramOplusExtraConfiguration)) {
      i = k | 0x10000000;
      this.mDarkModeForegroundMinL = paramOplusExtraConfiguration.mDarkModeForegroundMinL;
      this.mOplusConfigType = 1L;
    } 
    return i;
  }
  
  public int diff(OplusExtraConfiguration paramOplusExtraConfiguration) {
    int i = 0;
    int j = paramOplusExtraConfiguration.mThemeChanged, k = i;
    if (j > 0) {
      k = i;
      if (this.mThemeChanged != j)
        k = 0x0 | 0x8000000; 
    } 
    i = paramOplusExtraConfiguration.mAccessibleChanged;
    j = k;
    if (i != 0) {
      j = k;
      if (this.mAccessibleChanged != i)
        j = k | 0x4000000; 
    } 
    k = paramOplusExtraConfiguration.mFlipFont;
    i = j;
    if (k > 0) {
      i = j;
      if (this.mFlipFont != k)
        i = j | 0x2000000; 
    } 
    j = paramOplusExtraConfiguration.mUserId;
    k = i;
    if (j >= 0) {
      k = i;
      if (this.mUserId != j)
        k = i | 0x10000000; 
    } 
    j = paramOplusExtraConfiguration.mFontUserId;
    i = k;
    if (j >= 0) {
      i = k;
      if (this.mFontUserId != j)
        i = k | 0x10000000; 
    } 
    long l = paramOplusExtraConfiguration.mUxIconConfig;
    k = i;
    if (l > 0L) {
      k = i;
      if (this.mUxIconConfig != l)
        k = i | Integer.MIN_VALUE; 
    } 
    l = paramOplusExtraConfiguration.mMaterialColor;
    i = k;
    if (l >= 0L) {
      i = k;
      if (this.mMaterialColor != l)
        i = k | 0x4000000; 
    } 
    j = paramOplusExtraConfiguration.mFontVariationSettings;
    k = i;
    if (j >= 0) {
      k = i;
      if (this.mFontVariationSettings != j) {
        k = i | 0x1000000;
        this.mFontVariationSettings = j;
      } 
    } 
    String str = paramOplusExtraConfiguration.mIconPackName;
    i = k;
    if (str != null) {
      i = k;
      if (!str.equals("")) {
        str = paramOplusExtraConfiguration.mIconPackName;
        String str1 = this.mIconPackName;
        i = k;
        if (!str.equals(str1)) {
          i = k | Integer.MIN_VALUE;
          this.mIconPackName = paramOplusExtraConfiguration.mIconPackName;
        } 
      } 
    } 
    j = i;
    if (isDarkModeBgChanged(paramOplusExtraConfiguration)) {
      j = i | 0x10000000;
      this.mOplusConfigType = 1L;
    } 
    k = j;
    if (isDarkModeDialogBgChanged(paramOplusExtraConfiguration)) {
      k = j | 0x10000000;
      this.mOplusConfigType = 1L;
    } 
    i = k;
    if (isDarkModeFgChanged(paramOplusExtraConfiguration)) {
      i = k | 0x10000000;
      this.mOplusConfigType = 1L;
    } 
    return i;
  }
  
  public static boolean needNewResources(int paramInt) {
    boolean bool;
    if ((0x8000000 & paramInt) != 0) {
      bool = true;
    } else if ((0x2000000 & paramInt) != 0) {
      bool = true;
    } else if ((paramInt & 0x200) != 0) {
      bool = true;
    } else if ((Integer.MIN_VALUE & paramInt) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean needAccessNewResources(int paramInt) {
    boolean bool;
    if ((0x4000000 & paramInt) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mThemeChanged);
    paramParcel.writeLong(this.mThemeChangedFlags);
    paramParcel.writeInt(this.mFlipFont);
    paramParcel.writeInt(this.mUserId);
    paramParcel.writeInt(this.mAccessibleChanged);
    paramParcel.writeLong(this.mUxIconConfig);
    paramParcel.writeInt(this.mFontUserId);
    paramParcel.writeLong(this.mMaterialColor);
    paramParcel.writeInt(this.mFontVariationSettings);
    paramParcel.writeString(this.mIconPackName);
    paramParcel.writeLong(this.mOplusConfigType);
    paramParcel.writeFloat(this.mDarkModeForegroundMinL);
    paramParcel.writeFloat(this.mDarkModeBackgroundMaxL);
    paramParcel.writeFloat(this.mDarkModeDialogBgMaxL);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mThemeChanged = paramParcel.readInt();
    this.mThemeChangedFlags = paramParcel.readLong();
    this.mFlipFont = paramParcel.readInt();
    this.mUserId = paramParcel.readInt();
    this.mAccessibleChanged = paramParcel.readInt();
    this.mUxIconConfig = paramParcel.readLong();
    this.mFontUserId = paramParcel.readInt();
    this.mMaterialColor = paramParcel.readLong();
    this.mFontVariationSettings = paramParcel.readInt();
    this.mIconPackName = paramParcel.readString();
    this.mOplusConfigType = paramParcel.readLong();
    this.mDarkModeForegroundMinL = paramParcel.readFloat();
    this.mDarkModeBackgroundMaxL = paramParcel.readFloat();
    this.mDarkModeDialogBgMaxL = paramParcel.readFloat();
  }
  
  public int hashCode() {
    int i = this.mThemeChanged, j = (int)this.mThemeChangedFlags;
    int k = this.mFlipFont, m = this.mAccessibleChanged, n = this.mUserId, i1 = (int)this.mUxIconConfig;
    int i2 = this.mIconPackName.hashCode();
    return (i + j) * 31 + k + m * 16 + n * 8 + i1 + i2;
  }
  
  private boolean isDarkModeBgChanged(OplusExtraConfiguration paramOplusExtraConfiguration) {
    boolean bool;
    float f = paramOplusExtraConfiguration.mDarkModeBackgroundMaxL;
    if (f >= 0.0F && f != this.mDarkModeBackgroundMaxL) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isDarkModeFgChanged(OplusExtraConfiguration paramOplusExtraConfiguration) {
    boolean bool;
    float f = paramOplusExtraConfiguration.mDarkModeForegroundMinL;
    if (f >= 0.0F && f != this.mDarkModeForegroundMinL) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isDarkModeDialogBgChanged(OplusExtraConfiguration paramOplusExtraConfiguration) {
    boolean bool;
    float f = paramOplusExtraConfiguration.mDarkModeDialogBgMaxL;
    if (f >= 0.0F && f != this.mDarkModeDialogBgMaxL) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean shouldReportExtra(int paramInt1, int paramInt2) {
    boolean bool = false;
    if (((paramInt2 ^ 0xFFFFFFFF) & paramInt1 & 0xEFFFFFFF) == 0)
      bool = true; 
    return bool;
  }
}
