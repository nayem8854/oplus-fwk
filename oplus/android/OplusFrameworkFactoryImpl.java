package oplus.android;

import android.app.IOplusCommonInjector;
import android.common.ColorFrameworkFactory;
import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.common.OplusFeatureManager;
import android.content.Context;
import android.content.res.IOplusThemeManager;
import android.content.res.OplusThemeManager;
import android.content.res.Resources;
import android.drawable.IOplusGradientHooks;
import android.drawable.OplusGradientHooksImpl;
import android.inputmethodservice.IOplusInputMethodServiceUtils;
import android.inputmethodservice.OplusInputMethodServiceUtils;
import android.net.wifi.IOplusWifiGlobalMethod;
import android.net.wifi.IOplusWifiNetworkConfig;
import android.net.wifi.IWifiRomUpdateHelper;
import android.net.wifi.OplusWifiGlobalMethod;
import android.net.wifi.OplusWifiNetworkConfig;
import android.net.wifi.OplusWifiRomUpdateHelper;
import android.text.ITextJustificationHooks;
import android.text.TextJustificationHooksImpl;
import android.util.Log;
import android.view.IOplusAccidentallyTouchHelper;
import android.view.IOplusBurmeseZgHooks;
import android.view.IOplusDirectViewHelper;
import android.view.IOplusViewHooks;
import android.view.IOplusViewRootUtil;
import android.view.OplusAccidentallyTouchHelper;
import android.view.OplusBurmeseZgFlagHooksImpl;
import android.view.OplusDirectViewHelper;
import android.view.OplusViewHooksImp;
import android.view.OplusViewRootUtil;
import android.view.View;
import android.view.ViewRootImpl;
import android.widget.IOplusDragTextShadowHelper;
import android.widget.IOplusFloatingToolbarUtil;
import android.widget.IOplusFtHooks;
import android.widget.IOplusListHooks;
import android.widget.IOplusNotificationUiManager;
import android.widget.IOplusOverScrollerHelper;
import android.widget.IOplusTextViewRTLUtilForUG;
import android.widget.OplusDragTextShadowHelper;
import android.widget.OplusFloatingToolbarUtil;
import android.widget.OplusListHooksImp;
import android.widget.OplusNotificationUiManager;
import android.widget.OplusOverScrollerHelper;
import android.widget.OverScroller;
import com.android.internal.app.IOplusAlertControllerEuclidManager;
import com.android.internal.app.IOplusResolverManager;
import com.android.internal.app.IOplusResolverStyle;
import com.android.internal.app.OplusAlertControllerEuclidManger;
import com.android.internal.app.OplusResolverManager;
import com.android.internal.app.OplusResolverStyle;
import com.android.internal.widget.OplusFtHooksImpl;
import com.oplus.app.IOplusAppDynamicFeatureManager;
import com.oplus.app.OplusAppDynamicFeatureManager;
import com.oplus.darkmode.IOplusDarkModeManager;
import com.oplus.darkmode.OplusDarkModeManager;
import com.oplus.deepthinker.IOplusDeepThinkerManager;
import com.oplus.deepthinker.OplusDeepThinkerManager;
import com.oplus.drmDecoder.IOplusDrmDecoderFeature;
import com.oplus.drmDecoder.OplusDrmDecoderFeature;
import com.oplus.favorite.IOplusFavoriteManager;
import com.oplus.favorite.OplusFavoriteManager;
import com.oplus.font.IOplusFontManager;
import com.oplus.font.OplusFontManager;
import com.oplus.media.IOplusZenModeFeature;
import com.oplus.media.OplusZenModeFeature;
import com.oplus.multiapp.IOplusMultiApp;
import com.oplus.multiapp.OplusMultiAppImpl;
import com.oplus.rp.bridge.IOplusRedPacketManager;
import com.oplus.rp.bridge.OplusRedPacketManager;
import com.oplus.screenmode.IOplusScreenModeFeature;
import com.oplus.screenmode.OplusScreenModeFeature;
import com.oplus.screenshot.IOplusScreenShotEuclidManager;
import com.oplus.screenshot.OplusScreenShotEuclidManager;
import com.oplus.theme.IOplusThemeStyle;
import com.oplus.theme.OplusThemeStyle;
import com.oplus.uifirst.IOplusUIFirstManager;
import com.oplus.uifirst.OplusUIFirstManager;
import com.oppo.nwpower.IOplusNwPowerManager;
import com.oppo.nwpower.OplusNwPowerManager;
import java.lang.ref.WeakReference;

public class OplusFrameworkFactoryImpl extends ColorFrameworkFactory {
  private static final boolean DEBUG = false;
  
  private static final String TAG = "oplus.android.OplusFrameworkFactoryImpl";
  
  public <T extends IOplusCommonFeature> T getFeature(T paramT, Object... paramVarArgs) {
    StringBuilder stringBuilder1, stringBuilder2;
    verityParams((IOplusCommonFeature)paramT);
    if (!OplusFeatureManager.isSupport((IOplusCommonFeature)paramT))
      return paramT; 
    switch (paramT.index()) {
      default:
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Unknow feature:");
        stringBuilder1.append(paramT.index().name());
        Log.i("oplus.android.OplusFrameworkFactoryImpl", stringBuilder1.toString());
        return paramT;
      case IOplusNwPowerManager:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getOplusNwPowerManager((Object[])stringBuilder1));
      case IOplusUIFirstManager:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getOplusUIFirstManager((Object[])stringBuilder1));
      case IOplusRedPacketManager:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getOplusRedPacketManager((Object[])stringBuilder1));
      case IOplusDrmDecoderFeature:
        stringBuilder2 = new StringBuilder();
        stringBuilder2.append("get feature:");
        stringBuilder2.append(paramT.index().name());
        Log.i("oplus.android.OplusFrameworkFactoryImpl", stringBuilder2.toString());
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getOplusDrmDecoderFeature((Object[])stringBuilder1));
      case IOplusZenModeFeature:
        stringBuilder2 = new StringBuilder();
        stringBuilder2.append("get feature:");
        stringBuilder2.append(paramT.index().name());
        Log.i("oplus.android.OplusFrameworkFactoryImpl", stringBuilder2.toString());
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getOplusZenModeFeature((Object[])stringBuilder1));
      case IOplusScreenModeFeature:
        stringBuilder2 = new StringBuilder();
        stringBuilder2.append("get feature:");
        stringBuilder2.append(paramT.index().name());
        Log.i("oplus.android.OplusFrameworkFactoryImpl", stringBuilder2.toString());
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getOplusScreenModeFeature((Object[])stringBuilder1));
      case IOplusWifiNetworkConfig:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getWifiNetworkConfig((Object[])stringBuilder1));
      case IOplusWifiGlobalMethod:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getWifiGlobalMethod());
      case IWifiRomUpdateHelper:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getWifiRomUpdateHelper((Object[])stringBuilder1));
      case IOplusResolverStyle:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getOplusResolverStyle());
      case IOplusDragTextShadowHelper:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getDragTextShadowHelper());
      case IOplusAppDynamicFeatureManager:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getOplusAppDynamicFeatureManager());
      case IOplusNotificationUiManager:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getOplusNotificationUiManager((Object[])stringBuilder1));
      case IOplusThemeStyle:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getOplusThemeStyle((Object[])stringBuilder1));
      case IOplusBurmeseZgHooks:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getOplusBurmeseZgFlagHooks());
      case IOplusDeepThinkerManager:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getColorDeepThinkerManager((Object[])stringBuilder1));
      case IOplusAlertControllerEuclidManager:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getOplusAlertControllerEuclidManger());
      case IOplusMagnifierHooks:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getColorMagnifierHooks());
      case IOplusFtHooks:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getOplusFtHooks());
      case IOplusFloatingToolbarUtil:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getOplusFloatingToolbarUtil());
      case IOplusGradientHooks:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getOplusGradientHooks());
      case ITextJustificationHooks:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getTextJustificationHooks());
      case IOplusResolverManager:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getOplusResolverManager());
      case IOplusInputMethodServiceUtils:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getOplusInputMethodServiceUtils());
      case IOplusDarkModeManager:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getOplusDarkModeManager());
      case IOplusScreenShotEuclidManager:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getOplusScreenShotEuclidManager());
      case IOplusFavoriteManager:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getOplusFavoriteManager());
      case IOplusOverScrollerHelper:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getColorOverScrollerHelper((Object[])stringBuilder1));
      case IOplusTextViewRTLUtilForUG:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getOplusTextViewRTLUtilForUG());
      case IOplusListHooks:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getOplusListHooks());
      case IOplusFontManager:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getColorFontManager((Object[])stringBuilder1));
      case IOplusViewRootUtil:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getColorViewRootUtil((Object[])stringBuilder1));
      case IOplusCommonInjector:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getOplusCommonInjector());
      case IOplusViewHooks:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getColorViewHooks((Object[])stringBuilder1));
      case IOplusDirectViewHelper:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getColorDirectViewHelper((Object[])stringBuilder1));
      case IOplusAccidentallyTouchHelper:
        return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getOplusAccidentallyTouchHelper());
      case IOplusThemeManager:
        break;
    } 
    return (T)OplusFeatureManager.getTraceMonitor((IOplusCommonFeature)getOplusThemeManager());
  }
  
  private IOplusViewRootUtil getColorViewRootUtil(Object... paramVarArgs) {
    return OplusViewRootUtil.getInstance();
  }
  
  private IOplusFontManager getColorFontManager(Object... paramVarArgs) {
    return OplusFontManager.getInstance();
  }
  
  private IOplusDirectViewHelper getColorDirectViewHelper(Object... paramVarArgs) {
    WeakReference<ViewRootImpl> weakReference = (WeakReference)paramVarArgs[0];
    return (IOplusDirectViewHelper)new OplusDirectViewHelper(weakReference);
  }
  
  private IOplusViewHooks getColorViewHooks(Object... paramVarArgs) {
    verityParamsType("getOplusViewHooks", paramVarArgs, 2, new Class[] { View.class, Resources.class });
    View view = (View)paramVarArgs[0];
    Resources resources = (Resources)paramVarArgs[1];
    return new OplusViewHooksImp(view, resources);
  }
  
  private IOplusOverScrollerHelper getColorOverScrollerHelper(Object... paramVarArgs) {
    verityParamsType("getOplusOverScrollerHelper", paramVarArgs, 1, new Class[] { OverScroller.class });
    OverScroller overScroller = (OverScroller)paramVarArgs[0];
    return new OplusOverScrollerHelper(overScroller);
  }
  
  private IOplusDeepThinkerManager getColorDeepThinkerManager(Object... paramVarArgs) {
    Context context = (Context)paramVarArgs[0];
    return OplusDeepThinkerManager.getInstance(context);
  }
  
  public IOplusViewRootUtil getOplusViewRootUtil() {
    return OplusViewRootUtil.getInstance();
  }
  
  public IOplusFontManager getOplusFontManager() {
    return OplusFontManager.getInstance();
  }
  
  public IOplusFavoriteManager getOplusFavoriteManager() {
    return OplusFavoriteManager.getInstance();
  }
  
  public IOplusDarkModeManager getOplusDarkModeManager() {
    return OplusDarkModeManager.getInstance();
  }
  
  public IOplusDarkModeManager newOplusDarkModeManager() {
    return new OplusDarkModeManager();
  }
  
  public IOplusDirectViewHelper getOplusDirectViewHelper(WeakReference<ViewRootImpl> paramWeakReference) {
    return (IOplusDirectViewHelper)new OplusDirectViewHelper(paramWeakReference);
  }
  
  public IOplusCommonInjector getOplusCommonInjector() {
    return OplusCommonInjector.getInstance();
  }
  
  public IOplusTextViewRTLUtilForUG getOplusTextViewRTLUtilForUG() {
    return IOplusTextViewRTLUtilForUG.DEFAULT;
  }
  
  public IOplusViewHooks getOplusViewHooks(View paramView, Resources paramResources) {
    return new OplusViewHooksImp(paramView, paramResources);
  }
  
  public IOplusScreenShotEuclidManager getOplusScreenShotEuclidManager() {
    return OplusScreenShotEuclidManager.getInstance();
  }
  
  public IOplusInputMethodServiceUtils getOplusInputMethodServiceUtils() {
    return new OplusInputMethodServiceUtils();
  }
  
  public IOplusResolverManager getOplusResolverManager() {
    return new OplusResolverManager();
  }
  
  public IOplusResolverStyle getOplusResolverStyle() {
    return new OplusResolverStyle();
  }
  
  public IOplusThemeManager getOplusThemeManager() {
    return OplusThemeManager.getInstance();
  }
  
  public IOplusAccidentallyTouchHelper getOplusAccidentallyTouchHelper() {
    return OplusAccidentallyTouchHelper.getInstance();
  }
  
  public IOplusOverScrollerHelper getOplusOverScrollerHelper(OverScroller paramOverScroller) {
    return new OplusOverScrollerHelper(paramOverScroller);
  }
  
  public IOplusListHooks getOplusListHooks() {
    return new OplusListHooksImp();
  }
  
  public ITextJustificationHooks getTextJustificationHooks() {
    return new TextJustificationHooksImpl();
  }
  
  private IOplusGradientHooks getOplusGradientHooks() {
    return new OplusGradientHooksImpl();
  }
  
  public IOplusFtHooks getOplusFtHooks() {
    return new OplusFtHooksImpl();
  }
  
  public IOplusAlertControllerEuclidManager getOplusAlertControllerEuclidManger() {
    return OplusAlertControllerEuclidManger.getInstance();
  }
  
  public IOplusDeepThinkerManager getOplusDeepThinkerManager(Context paramContext) {
    return OplusDeepThinkerManager.getInstance(paramContext);
  }
  
  public IOplusBurmeseZgHooks getOplusBurmeseZgFlagHooks() {
    return new OplusBurmeseZgFlagHooksImpl();
  }
  
  public IOplusMultiApp getOplusMultiApp() {
    return (IOplusMultiApp)new OplusMultiAppImpl();
  }
  
  public IOplusThemeStyle getOplusThemeStyle(Object... paramVarArgs) {
    return new OplusThemeStyle();
  }
  
  public IOplusNotificationUiManager getOplusNotificationUiManager(Object... paramVarArgs) {
    return new OplusNotificationUiManager();
  }
  
  public IOplusAppDynamicFeatureManager getOplusAppDynamicFeatureManager() {
    return OplusAppDynamicFeatureManager.getInstance();
  }
  
  public IOplusFloatingToolbarUtil getOplusFloatingToolbarUtil() {
    return new OplusFloatingToolbarUtil();
  }
  
  public IOplusDragTextShadowHelper getDragTextShadowHelper() {
    return OplusDragTextShadowHelper.getInstance();
  }
  
  private IWifiRomUpdateHelper getWifiRomUpdateHelper(Object... paramVarArgs) {
    Context context = (Context)paramVarArgs[0];
    return OplusWifiRomUpdateHelper.getInstance(context);
  }
  
  private IOplusWifiGlobalMethod getWifiGlobalMethod() {
    return OplusWifiGlobalMethod.getInstance();
  }
  
  private IOplusWifiNetworkConfig getWifiNetworkConfig(Object... paramVarArgs) {
    Context context = (Context)paramVarArgs[0];
    return OplusWifiNetworkConfig.getInstance(context);
  }
  
  private IOplusScreenModeFeature getOplusScreenModeFeature(Object... paramVarArgs) {
    Log.i("oplus.android.OplusFrameworkFactoryImpl", "getOplusScreenModeFeature");
    return OplusScreenModeFeature.getInstance();
  }
  
  private IOplusZenModeFeature getOplusZenModeFeature(Object... paramVarArgs) {
    return new OplusZenModeFeature();
  }
  
  private IOplusDrmDecoderFeature getOplusDrmDecoderFeature(Object... paramVarArgs) {
    return OplusDrmDecoderFeature.getInstance();
  }
  
  private IOplusRedPacketManager getOplusRedPacketManager(Object... paramVarArgs) {
    Log.i("oplus.android.OplusFrameworkFactoryImpl", "getOplusRedPacketManager");
    return OplusRedPacketManager.getInstance();
  }
  
  private IOplusUIFirstManager getOplusUIFirstManager(Object... paramVarArgs) {
    return OplusUIFirstManager.getInstance();
  }
  
  private IOplusNwPowerManager getOplusNwPowerManager(Object... paramVarArgs) {
    return OplusNwPowerManager.getInstance();
  }
}
