package oplus.android;

import android.app.Activity;
import android.app.Application;
import android.app.IOplusCommonInjector;
import android.app.OplusThemeHelper;
import android.common.IOplusCommonFeature;
import android.common.OplusFeatureCache;
import android.content.Context;
import android.content.pm.PackageParser;
import android.content.res.Configuration;
import android.content.res.IOplusThemeManager;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import com.oplus.font.IOplusFontManager;
import com.oplus.internal.R;

public class OplusCommonInjector implements IOplusCommonInjector {
  private static volatile OplusCommonInjector sInstance = null;
  
  public static OplusCommonInjector getInstance() {
    // Byte code:
    //   0: getstatic oplus/android/OplusCommonInjector.sInstance : Loplus/android/OplusCommonInjector;
    //   3: ifnonnull -> 39
    //   6: ldc oplus/android/OplusCommonInjector
    //   8: monitorenter
    //   9: getstatic oplus/android/OplusCommonInjector.sInstance : Loplus/android/OplusCommonInjector;
    //   12: ifnonnull -> 27
    //   15: new oplus/android/OplusCommonInjector
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic oplus/android/OplusCommonInjector.sInstance : Loplus/android/OplusCommonInjector;
    //   27: ldc oplus/android/OplusCommonInjector
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc oplus/android/OplusCommonInjector
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic oplus/android/OplusCommonInjector.sInstance : Loplus/android/OplusCommonInjector;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #47	-> 0
    //   #48	-> 6
    //   #49	-> 9
    //   #50	-> 15
    //   #52	-> 27
    //   #54	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  public void onCreateForActivity(Activity paramActivity, Bundle paramBundle) {}
  
  public void onConfigurationChangedForApplication(Application paramApplication, Configuration paramConfiguration) {
    ((IOplusFontManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusFontManager.DEFAULT, new Object[0])).updateLanguageLocale(paramConfiguration);
  }
  
  public void onCreateForApplication(Application paramApplication) {
    if (paramApplication != null) {
      ((IOplusFontManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusFontManager.DEFAULT, new Object[0])).setCurrentAppName(paramApplication.getPackageName());
      ((IOplusFontManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusFontManager.DEFAULT, new Object[0])).initVariationFontVariable((Context)paramApplication);
    } 
  }
  
  public void applyConfigurationToResourcesForResourcesManager(Configuration paramConfiguration, int paramInt) {
    OplusThemeHelper.handleExtraConfigurationChanges(paramInt);
    ((IOplusFontManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusFontManager.DEFAULT, new Object[0])).updateTypefaceInCurrProcess(paramConfiguration);
    ((IOplusThemeManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusThemeManager.DEFAULT, new Object[0])).updateExtraConfigForUxIcon(paramInt);
    ((IOplusFontManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusFontManager.DEFAULT, new Object[0])).updateFontVariationConfiguration(paramConfiguration, paramInt);
  }
  
  public void hookPreloadResources(Resources paramResources, String paramString) {
    long l = SystemClock.uptimeMillis();
    TypedArray typedArray2 = paramResources.obtainTypedArray(201785380);
    int i = preloadDrawables(paramResources, typedArray2, paramString);
    typedArray2.recycle();
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("...preloaded ");
    stringBuilder2.append(i);
    stringBuilder2.append(" oppo drawable resources in ");
    stringBuilder2.append(SystemClock.uptimeMillis() - l);
    stringBuilder2.append("ms.");
    String str2 = stringBuilder2.toString();
    Log.i(paramString, str2);
    l = SystemClock.uptimeMillis();
    TypedArray typedArray1 = paramResources.obtainTypedArray(201785379);
    i = preloadOplusStateLists(paramResources, typedArray1, paramString);
    typedArray1.recycle();
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("...preloaded ");
    stringBuilder1.append(i);
    stringBuilder1.append(" oppo color resources in ");
    stringBuilder1.append(SystemClock.uptimeMillis() - l);
    stringBuilder1.append("ms.");
    String str1 = stringBuilder1.toString();
    Log.i(paramString, str1);
  }
  
  public void hookActivityAliasTheme(PackageParser.Activity paramActivity1, Resources paramResources, XmlResourceParser paramXmlResourceParser, PackageParser.Activity paramActivity2) {
    TypedArray typedArray = paramResources.obtainAttributes((AttributeSet)paramXmlResourceParser, R.styleable.AndroidManifestActivityAlias);
    paramActivity1.info.theme = typedArray.getResourceId(0, paramActivity2.info.theme);
    typedArray.recycle();
  }
  
  private int preloadDrawables(Resources paramResources, TypedArray paramTypedArray, String paramString) {
    int i = paramTypedArray.length();
    for (byte b = 0; b < i; ) {
      int j = paramTypedArray.getResourceId(b, 0);
      if (j == 0 || 
        paramResources.getDrawable(j, null) != null) {
        b++;
        continue;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to find preloaded drawable resource #0x");
      stringBuilder.append(Integer.toHexString(j));
      stringBuilder.append(" (");
      stringBuilder.append(paramTypedArray.getString(b));
      stringBuilder.append(")");
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    return i;
  }
  
  private int preloadOplusStateLists(Resources paramResources, TypedArray paramTypedArray, String paramString) {
    int i = paramTypedArray.length();
    for (byte b = 0; b < i; ) {
      int j = paramTypedArray.getResourceId(b, 0);
      if (j == 0 || 
        paramResources.getColorStateList(j, null) != null) {
        b++;
        continue;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to find preloaded color resource #0x");
      stringBuilder.append(Integer.toHexString(j));
      stringBuilder.append(" (");
      stringBuilder.append(paramTypedArray.getString(b));
      stringBuilder.append(")");
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    return i;
  }
}
