package oplus;

public final class R {
  public static final class anim {}
  
  public static final class array {
    public static final int android_common_nicknames = 201785348;
    
    public static final int android_config_mobile_hotspot_provision_app = 201785346;
    
    public static final int android_config_virtualKeyVibePattern = 201785347;
    
    public static final int brand_select_ringtone_files = 201785350;
    
    public static final int brand_select_ringtone_names = 201785351;
    
    public static final int color_uxicon_common_style_path_exp = 201785349;
    
    public static final int oppo_select_ringtone_new = 201785344;
    
    public static final int oppo_select_ringtone_old = 201785345;
  }
  
  public static final class attr {
    public static final int colorLockText = 201392128;
    
    public static final int colorManuallyNormalDrawable = 201392129;
    
    public static final int colorOShareButtonBackground = 201392131;
    
    public static final int colorOShareButtonTextColor = 201392130;
  }
  
  public static final class bool {
    public static final int android_config_alwaysUseCdmaRssi = 202113030;
    
    public static final int android_config_automatic_brightness_available = 202113026;
    
    public static final int android_config_dreamsActivatedOnDockByDefault = 202113032;
    
    public static final int android_config_dreamsActivatedOnSleepByDefault = 202113033;
    
    public static final int android_config_dreamsEnabledByDefault = 202113031;
    
    public static final int android_config_enableDreams = 202113027;
    
    public static final int android_config_intrusiveNotificationLed = 202113029;
    
    public static final int android_config_use_strict_phone_number_comparation = 202113024;
    
    public static final int android_config_voice_capable = 202113025;
    
    public static final int android_config_wimaxEnabled = 202113028;
  }
  
  public static final class color {
    public static final int android_accent_device_default_dark = 201719826;
    
    public static final int android_accent_device_default_light = 201719827;
    
    public static final int android_error_color_material_dark = 201719817;
    
    public static final int android_error_color_material_light = 201719818;
    
    public static final int android_notification_default_color = 201719821;
    
    public static final int android_notification_material_background_color = 201719822;
    
    public static final int android_notification_primary_text_color_dark = 201719820;
    
    public static final int android_notification_primary_text_color_light = 201719819;
    
    public static final int android_primary_text_material_dark = 201719813;
    
    public static final int android_primary_text_material_light = 201719814;
    
    public static final int android_quaternary_device_default_settings = 201719823;
    
    public static final int android_secondary_text_material_dark = 201719815;
    
    public static final int android_secondary_text_material_light = 201719816;
    
    public static final int android_tertiary_device_default_settings = 201719824;
    
    public static final int color_input_method_navigation_guard = 201719829;
    
    public static final int color_input_method_navigation_guard_exp = 201719830;
    
    public static final int color_manually_lock_button_pressed = 201719837;
    
    public static final int color_manually_lock_text_color = 201719836;
    
    public static final int color_notification_content_color = 201719812;
    
    public static final int color_notification_summary_color = 201719810;
    
    public static final int color_notification_time_color = 201719811;
    
    public static final int color_notification_title_color = 201719809;
    
    public static final int color_permission_controller_green = 201719828;
    
    public static final int color_save_dialog_background_color = 201719834;
    
    public static final int color_save_dialog_divider_color = 201719833;
    
    public static final int color_save_dialog_ripple_color = 201719835;
    
    public static final int color_save_dialog_text_color = 201719831;
    
    public static final int color_save_dialog_title_color = 201719832;
  }
  
  public static final class dimen {
    public static final int android_navigation_bar_height = 201654273;
    
    public static final int android_notification_content_margin_end = 201654278;
    
    public static final int android_notification_content_margin_start = 201654277;
    
    public static final int android_notification_content_margin_top = 201654276;
    
    public static final int android_notification_header_icon_size_ambient = 201654280;
    
    public static final int android_notification_header_padding_top = 201654279;
    
    public static final int android_notification_text_size = 201654275;
    
    public static final int android_status_bar_height = 201654272;
    
    public static final int color_dimen_gesture_area_height = 201654286;
    
    public static final int color_manually_lock_drawable_margin = 201654283;
    
    public static final int color_manually_lock_text_size = 201654285;
    
    public static final int color_manually_lock_width = 201654284;
    
    public static final int color_systemui_gesture_bar_height = 201654274;
    
    public static final int notification_progress_bar_radius_material = 201654282;
  }
  
  public static final class drawable {
    public static final int android_btn_keyboard_key_fulltrans = 201850888;
    
    public static final int android_code_lock_bottom = 201850891;
    
    public static final int android_code_lock_left = 201850889;
    
    public static final int android_code_lock_top = 201850890;
    
    public static final int android_ic_audio_vol = 201850921;
    
    public static final int android_ic_audio_vol_mute = 201850920;
    
    public static final int android_ic_contact_picture = 201850922;
    
    public static final int android_ic_emergency = 201850917;
    
    public static final int android_ic_media_route_disabled_holo_dark = 201850919;
    
    public static final int android_ic_media_route_on_holo_dark = 201850918;
    
    public static final int android_quickcontact_badge_overlay_normal_light = 201850887;
    
    public static final int android_quickcontact_badge_overlay_pressed_light = 201850886;
    
    public static final int android_scrubber_control_disabled_holo = 201850897;
    
    public static final int android_scrubber_control_selector_holo = 201850898;
    
    public static final int android_scrubber_progress_horizontal_holo_dark = 201850899;
    
    public static final int android_stat_sys_gps_on = 201850892;
    
    public static final int android_sym_app_on_sd_unavailable_icon = 201850893;
    
    public static final int android_usb_android = 201850900;
    
    public static final int android_usb_android_connected = 201850901;
    
    public static final int color_autofill_save_dialog_background = 201850907;
    
    public static final int color_autofill_save_text_ripple = 201850908;
    
    public static final int color_ic_account_circle = 201850910;
    
    public static final int color_ic_corp_badge_case_multiapp = 201850911;
    
    public static final int color_ic_corp_badge_no_background_multiapp = 201850912;
    
    public static final int color_ic_corp_icon_badge_multiapp = 201850903;
    
    public static final int color_notification_bg = 201850896;
    
    public static final int color_permission_controller_center_alert_dialog_bg = 201850904;
    
    public static final int color_permission_controller_dialog_button_middle_ripple = 201850905;
    
    public static final int color_permission_controller_list_divider = 201850906;
    
    public static final int color_stat_notify_disk_full = 201850931;
    
    public static final int color_upgrade_logo = 201850885;
    
    public static final int oplus_notification_progress_horizontal_material = 201850909;
    
    public static final int oppo_default_wallpaper = 201850902;
    
    public static final int oppo_ic_audio_alarm = 201850895;
    
    public static final int oppo_ic_audio_alarm_mute = 201850894;
    
    public static final int oppo_root_large_icon = 201850930;
    
    public static final int oppo_root_small_icon = 201850929;
    
    public static final int oppo_stat_notify_wifi_in_range = 201850916;
    
    public static final int oppo_stat_notify_wifi_in_range_large = 201850924;
    
    public static final int oppo_stat_sys_tether_bluetooth = 201850923;
    
    public static final int oppo_stat_sys_tether_bluetooth_large = 201850926;
    
    public static final int oppo_stat_sys_tether_general = 201850913;
    
    public static final int oppo_stat_sys_tether_general_large = 201850928;
    
    public static final int oppo_stat_sys_tether_usb = 201850915;
    
    public static final int oppo_stat_sys_tether_usb_large = 201850927;
    
    public static final int oppo_stat_sys_tether_wifi = 201850914;
    
    public static final int oppo_stat_sys_tether_wifi_large = 201850925;
  }
  
  public static final class fraction {}
  
  public static final class id {
    public static final int android_status = 201457665;
  }
  
  public static final class integer {
    public static final int color_config_bottom_gesture_area_height = 202178571;
    
    public static final int color_config_gesture_area_height = 202178561;
    
    public static final int color_config_gesture_area_height_screenassistant = 202178562;
    
    public static final int color_config_side_gesture_area_width = 202178573;
    
    public static final int color_config_top_gesture_area_height = 202178572;
  }
  
  public static final class interpolator {}
  
  public static final class layout {}
  
  public static final class plurals {}
  
  public static final class raw {
    public static final int android_fallbackring = 202244096;
  }
  
  public static final class string {
    public static final int android_action_bar_up_description = 201588830;
    
    public static final int android_chooseUsbActivity = 201588754;
    
    public static final int android_common_last_name_prefixes = 201588751;
    
    public static final int android_common_name_conjunctions = 201588753;
    
    public static final int android_common_name_prefixes = 201588750;
    
    public static final int android_common_name_suffixes = 201588752;
    
    public static final int android_config_bodyFontFamily = 201588835;
    
    public static final int android_config_bodyFontFamilyMedium = 201588836;
    
    public static final int android_config_defaultDreamComponent = 201588755;
    
    public static final int android_config_dreamsDefaultComponent = 201588777;
    
    public static final int android_config_headlineFontFamily = 201588834;
    
    public static final int android_config_headlineFontFamilyMedium = 201588825;
    
    public static final int android_config_headlineFontFeatureSettings = 201588831;
    
    public static final int android_ext_media_badremoval_notification_message = 201588756;
    
    public static final int android_ext_media_badremoval_notification_title = 201588757;
    
    public static final int android_ext_media_checking_notification_message = 201588758;
    
    public static final int android_ext_media_checking_notification_title = 201588759;
    
    public static final int android_ext_media_nofs_notification_message = 201588760;
    
    public static final int android_ext_media_nofs_notification_title = 201588761;
    
    public static final int android_ext_media_nomedia_notification_message = 201588762;
    
    public static final int android_ext_media_nomedia_notification_title = 201588763;
    
    public static final int android_ext_media_safe_unmount_notification_message = 201588764;
    
    public static final int android_ext_media_safe_unmount_notification_title = 201588765;
    
    public static final int android_ext_media_unmountable_notification_message = 201588766;
    
    public static final int android_ext_media_unmountable_notification_title = 201588767;
    
    public static final int android_fast_scroll_alphabet = 201588747;
    
    public static final int android_global_action_logout = 201588826;
    
    public static final int android_ime_action_send = 201588829;
    
    public static final int android_media_route_controller_disconnect = 201588828;
    
    public static final int android_notification_header_divider_symbol = 201588827;
    
    public static final int android_power_off = 201588744;
    
    public static final int android_report = 201588745;
    
    public static final int android_ringtone_silent = 201588743;
    
    public static final int android_ringtone_unknown = 201588742;
    
    public static final int android_share = 201588768;
    
    public static final int android_ssl_certificate = 201588746;
    
    public static final int android_start_title_string = 201588837;
    
    public static final int android_system_ui_date_pattern = 201588824;
    
    public static final int android_usb_storage_message = 201588769;
    
    public static final int android_usb_storage_notification_message = 201588770;
    
    public static final int android_usb_storage_notification_title = 201588771;
    
    public static final int android_usb_storage_stop_message = 201588772;
    
    public static final int android_usb_storage_stop_notification_message = 201588773;
    
    public static final int android_usb_storage_stop_notification_title = 201588774;
    
    public static final int android_usb_storage_stop_title = 201588775;
    
    public static final int android_usb_storage_title = 201588776;
    
    public static final int android_volume_unknown = 201588832;
    
    public static final int color_oppo_set_default_sms_mms_toast = 201588778;
    
    public static final int color_oppo_set_default_sms_mms_toast_ok = 201588779;
    
    public static final int color_oppo_set_default_sms_mms_toast_title = 201588780;
    
    public static final int oppo_wifi_tether_configure_ssid_default = 201588739;
  }
  
  public static final class style {
    public static final int PermissionControllerCategoryTitle = 201523218;
    
    public static final int PermissionControllerSettings = 201523217;
    
    public static final int TextAppearance_Android_DeviceDefault_Body2 = 201523204;
    
    public static final int TextAppearance_Android_DeviceDefault_Notification_Info = 201523221;
    
    public static final int TextAppearance_Android_DeviceDefault_Notification_Title = 201523220;
    
    public static final int TextAppearance_Android_DeviceDefault_Subhead = 201523205;
    
    public static final int TextAppearance_Android_StatusBar = 201523203;
    
    public static final int Theme = 201523202;
    
    public static final int ThemeOverlay_Android_DeviceDefault_Accent = 201523211;
    
    public static final int ThemeOverlay_Android_DeviceDefault_Accent_Light = 201523212;
    
    public static final int Theme_Android_DeviceDefault_QuickSettings = 201523200;
    
    public static final int Theme_Android_DeviceDefault_QuickSettings_Dialog = 201523201;
    
    public static final int Theme_Android_DeviceDefault_Settings_Dialog = 201523219;
    
    public static final int Theme_Dialog = 201523206;
    
    public static final int Theme_Dialog_Alert = 201523207;
    
    public static final int Theme_OPPO = 201523222;
    
    public static final int Theme_OPPO_Dialog = 201523223;
    
    public static final int Theme_OPPO_Dialog_Alert = 201523224;
    
    public static final int Theme_Realme = 201523213;
    
    public static final int Theme_Realme_Dialog = 201523214;
    
    public static final int Theme_Realme_Dialog_Alert = 201523215;
    
    public static final int Theme_Realme_Dialog_Alert_Share = 201523216;
    
    public static final int Widget_Android_DeviceDefault_Notification_Text = 201523210;
    
    public static final int Widget_Android_DeviceDefault_Toolbar = 201523209;
    
    public static final int Widget_Android_Material_Notification_Text = 201523208;
  }
  
  public static final class styleable {
    public static final int[] AndroidManifestActivityAlias = new int[] { 16842752 };
    
    public static final int AndroidManifestActivityAlias_android_theme = 0;
    
    public static final int[] ApduPatternFilter = new int[] { 201982005, 201982041, 201982085 };
    
    public static final int[] ApduPatternGroup = new int[] { 201982041 };
    
    public static final int[] ApkLock = new int[] { 201982061, 201982062, 201982063, 201982064, 201982065 };
    
    public static final int[] ColorDialog = new int[] { 201982013 };
    
    public static final int[] ColorGlobalDragTextShadowContainer = new int[] { 201982035, 201982036, 201982037, 201982038, 201982039, 201982040, 201982051 };
    
    public static final int[] ColorLockPatternView = new int[] { 201982017, 201982019, 201982020, 201982022, 201982025 };
    
    public static final int[] ColorMenuItem = new int[] { 201982033, 201982034 };
    
    public static final int[] ColorToast = new int[] { 16842927, 16842994, 201982031 };
    
    public static final int[] ColorToastTheme = new int[] { 201982032 };
    
    public static final int ColorToast_android_gravity = 0;
    
    public static final int ColorToast_android_layout = 1;
    
    public static final int[] EdgeTriggerView = new int[] { 201982050, 201982060 };
    
    public static final int[] Mapping = new int[] { 201982042, 201982056, 201982067, 201982086, 201982091 };
    
    public static final int[] OplusAlertLinearLayout = new int[] { 201982002, 201982003, 201982004 };
    
    public static final int[] OplusCircleProgressBar = new int[] { 201982066, 201982079, 201982080, 201982081, 201982082, 201982083, 201982084 };
    
    public static final int[] OplusLoadingView = new int[] { 201981988, 201981989, 201981991, 201981992 };
    
    public static final int[] OplusMaxLinearLayout = new int[] { 201982058, 201982059, 201982077, 201982078 };
    
    public static final int[] OplusPageIndicator = new int[] { 201982043, 201982044, 201982045, 201982046, 201982047, 201982048, 201982049, 201982099 };
    
    public static final int[] OplusResolverDrawerLayout = new int[] { 201982059, 201982078 };
    
    public static final int[] OplusTheme = new int[] { 201982057, 201982096 };
    
    public static final int[] Oppo3DMultiCallView = new int[] { 201982092, 201982093, 201982094, 201982095 };
    
    public static final int[] Oppo3DRollCoverView = new int[] { 201982100 };
    
    public static final int[] Oppo3DScenePreView = new int[] { 201982006, 201982052, 201982053, 201982088, 201982089, 201982103 };
    
    public static final int[] OppoWindow = new int[] { 201982070, 201982075 };
    
    public static final int[] RoundFrameLayout = new int[] { 201982087 };
    
    public static final int[] ScenePreView = new int[] { 201982068, 201982090 };
    
    public static final int[] ShutdownView = new int[] { 201392128, 201392129, 201982010, 201982011, 201982012, 201982014, 201982015, 201982016, 201982021, 201982023 };
    
    public static final int ShutdownView_colorLockText = 0;
    
    public static final int ShutdownView_colorManuallyNormalDrawable = 1;
    
    public static final int[] SlidePiece = new int[] { 
        201981980, 201981981, 201981982, 201981983, 201981984, 201981993, 201981994, 201981995, 201981996, 201981997, 
        201981998, 201981999, 201982000, 201982001 };
    
    public static final int[] TextAppearance = new int[] { 16842901, 16842902, 16842903, 16842904 };
    
    public static final int TextAppearance_android_textColor = 3;
    
    public static final int TextAppearance_android_textSize = 0;
    
    public static final int TextAppearance_android_textStyle = 2;
    
    public static final int TextAppearance_android_typeface = 1;
    
    public static final int[] VerticalRollAnimation = new int[] { 201982054, 201982055, 201982097, 201982098 };
    
    public static final int[] WindowAnimation = new int[] { 201982101, 201982102 };
  }
  
  public static final class xml {
    public static final int android_apns = 202047488;
  }
}
