package oplus.net.wifi;

import android.net.wifi.WifiEnterpriseConfig;
import com.oplus.reflect.MethodParams;
import com.oplus.reflect.RefClass;
import com.oplus.reflect.RefMethod;

public class OplusMirrorWifiEnterpriseConfig {
  public static Class<?> TYPE = RefClass.load(OplusMirrorWifiEnterpriseConfig.class, WifiEnterpriseConfig.class);
  
  public static RefMethod<String> getSimNum;
  
  @MethodParams({int.class})
  public static RefMethod<Void> setSimNum;
}
