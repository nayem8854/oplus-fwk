package oplus.net.wifi;

import android.os.Parcel;
import android.os.Parcelable;

public class HotspotClient implements Parcelable {
  public HotspotClient(String paramString) {
    this.deviceAddress = paramString;
  }
  
  public HotspotClient(String paramString1, String paramString2, String paramString3) {
    this.deviceAddress = paramString1;
    this.name = paramString2;
    this.conTime = paramString3;
  }
  
  public HotspotClient(String paramString1, String paramString2) {
    this.deviceAddress = paramString1;
    this.name = paramString2;
  }
  
  public HotspotClient(HotspotClient paramHotspotClient) {
    if (paramHotspotClient != null) {
      this.deviceAddress = paramHotspotClient.deviceAddress;
      this.name = paramHotspotClient.name;
      this.conTime = paramHotspotClient.conTime;
    } 
  }
  
  public String toString() {
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(" deviceAddress: ");
    stringBuffer.append(this.deviceAddress);
    stringBuffer.append('\n');
    stringBuffer.append(" name: ");
    stringBuffer.append(this.name);
    stringBuffer.append("\n");
    stringBuffer.append(" conTime: ");
    stringBuffer.append(this.conTime);
    stringBuffer.append("\n");
    return stringBuffer.toString();
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof HotspotClient))
      return false; 
    paramObject = paramObject;
    paramObject = ((HotspotClient)paramObject).deviceAddress;
    if (paramObject == null)
      return false; 
    return paramObject.equals(this.deviceAddress);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.deviceAddress);
    paramParcel.writeString(this.name);
    paramParcel.writeString(this.conTime);
  }
  
  public static final Parcelable.Creator<HotspotClient> CREATOR = new Parcelable.Creator<HotspotClient>() {
      public HotspotClient createFromParcel(Parcel param1Parcel) {
        String str1 = param1Parcel.readString();
        String str2 = param1Parcel.readString();
        return new HotspotClient(str1, str2, param1Parcel.readString());
      }
      
      public HotspotClient[] newArray(int param1Int) {
        return new HotspotClient[param1Int];
      }
    };
  
  public String conTime;
  
  public String deviceAddress;
  
  public String name;
}
