package oplus.util;

public class OplusSafeCenterFeature {
  private static boolean mAssociateStartFeature = true;
  
  public static boolean isAssociationStartEnabled() {
    return mAssociateStartFeature;
  }
  
  public static void setAssociationStartFeature(boolean paramBoolean) {
    mAssociateStartFeature = paramBoolean;
  }
  
  public static boolean isLaunchRecordEnabled() {
    return true;
  }
}
