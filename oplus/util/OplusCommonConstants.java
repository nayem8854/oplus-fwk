package oplus.util;

import android.util.Log;

public final class OplusCommonConstants {
  private static final int COLOR_CALL_TRANSACTION_INDEX = 10000;
  
  private static final int COLOR_MESSAGE_INDEX = 1000;
  
  public static final int OPLUS_FIRST_CALL_TRANSACTION = 10001;
  
  public static final int OPLUS_FIRST_MESSAGE = 1001;
  
  public static final int OPLUS_GROUP = 1;
  
  public static final int OPLUS_LAST_CALL_TRANSACTION = 20000;
  
  public static final int OPLUS_LAST_MESSAGE = 2000;
  
  public static final int OPLUS_LAYOUT_IN_DISPLAY_CUTOUT_MODE_FORCE = 5;
  
  private static final int OPPO_CALL_TRANSACTION_INDEX = 10000;
  
  private static final int OPPO_FIRST_CALL_TRANSACTION = 1;
  
  private static final int OPPO_FIRST_MESSAGE = 1;
  
  private static final int OPPO_MESSAGE_INDEX = 1000;
  
  private static final int PSW_CALL_TRANSACTION_INDEX = 20000;
  
  public static final int PSW_FIRST_CALL_TRANSACTION = 20001;
  
  public static final int PSW_FIRST_MESSAGE = 2001;
  
  public static final int PSW_GROUP = 2;
  
  public static final int PSW_LAST_CALL_TRANSACTION = 30000;
  
  public static final int PSW_LAST_MESSAGE = 3000;
  
  private static final int PSW_MESSAGE_INDEX = 2000;
  
  private static final int SCREEN_CAST_CALL_TRANSACTION_INDEX = 30000;
  
  public static final int SCREEN_CAST_FIRST_CALL_TRANSACTION = 30001;
  
  public static final int SCREEN_CAST_LAST_CALL_TRANSACTION = 40000;
  
  private static final String TAG = "OplusCommonConstants";
  
  public static final int TYPE_BINDER = 1;
  
  public static final int TYPE_MESSAGE = 2;
  
  public static boolean checkCodeValid(int paramInt1, int paramInt2, int paramInt3) {
    if (paramInt2 != 1) {
      if (paramInt2 != 2) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("UNKNOW type = ");
        stringBuilder.append(paramInt2);
        Log.i("OplusCommonConstants", stringBuilder.toString());
        return false;
      } 
      return checkMessageCodeValie(paramInt1, paramInt3);
    } 
    return checkBinderCodeValid(paramInt1, paramInt3);
  }
  
  private static boolean checkBinderCodeValid(int paramInt1, int paramInt2) {
    if (paramInt2 != 1) {
      if (paramInt2 != 2) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("UNKNOW group = ");
        stringBuilder.append(paramInt2);
        Log.i("OplusCommonConstants", stringBuilder.toString());
        return false;
      } 
      return inside(paramInt1, 20001, 30000);
    } 
    return inside(paramInt1, 10001, 20000);
  }
  
  private static boolean checkMessageCodeValie(int paramInt1, int paramInt2) {
    if (paramInt2 != 1) {
      if (paramInt2 != 2) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Uknow group = ");
        stringBuilder.append(paramInt2);
        Log.i("OplusCommonConstants", stringBuilder.toString());
        return false;
      } 
      return inside(paramInt1, 2001, 3000);
    } 
    return inside(paramInt1, 1001, 2000);
  }
  
  private static boolean inside(int paramInt1, int paramInt2, int paramInt3) {
    boolean bool;
    if (paramInt1 >= paramInt2 && paramInt1 <= paramInt3) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
}
