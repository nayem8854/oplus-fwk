package oplus.util;

public class OplusDisplayUtils {
  private static final int[] DENSITIES = new int[] { 480, 320, 1, 0 };
  
  public static final int DENSITY_NONE = 1;
  
  public static int[] getBestDensityOrder(int paramInt) {
    int j, k;
    if (paramInt <= 1)
      return DENSITIES; 
    byte b = 0;
    byte b1 = -1;
    int i = 0;
    while (true) {
      int[] arrayOfInt = DENSITIES;
      j = b;
      k = b1;
      if (i < arrayOfInt.length) {
        if (paramInt > arrayOfInt[i]) {
          j = 0 + 1;
          k = i;
          break;
        } 
        if (paramInt == arrayOfInt[i]) {
          j = b;
          k = i;
          break;
        } 
        i++;
        continue;
      } 
      break;
    } 
    int[] arrayOfInt2 = DENSITIES, arrayOfInt1 = new int[arrayOfInt2.length + j];
    if (k != 0) {
      if (k == 1 || k == 2) {
        i = arrayOfInt1.length;
        arrayOfInt1[0] = paramInt;
        arrayOfInt1[i - 1] = 0;
        arrayOfInt1[i - 2] = 1;
        if (j == 0) {
          arrayOfInt1[j + 1] = DENSITIES[j];
        } else {
          arrayOfInt2 = DENSITIES;
          arrayOfInt1[j] = arrayOfInt2[j];
          arrayOfInt1[j + 1] = arrayOfInt2[j - 1];
        } 
      } 
    } else {
      if (j == 0)
        return arrayOfInt2; 
      arrayOfInt1[k] = paramInt;
      while (j < arrayOfInt1.length) {
        arrayOfInt1[j] = DENSITIES[j - 1];
        j++;
      } 
    } 
    return arrayOfInt1;
  }
  
  public static String getDensityName(int paramInt) {
    String str;
    if (paramInt != 1) {
      if (paramInt != 120) {
        if (paramInt != 160) {
          if (paramInt != 240) {
            if (paramInt != 320) {
              if (paramInt != 480) {
                if (paramInt != 640) {
                  str = "";
                } else {
                  str = "xxxhdpi";
                } 
              } else {
                str = "xxhdpi";
              } 
            } else {
              str = "xhdpi";
            } 
          } else {
            str = "hdpi";
          } 
        } else {
          str = "mdpi";
        } 
      } else {
        str = "ldpi";
      } 
    } else {
      str = "nodpi";
    } 
    return str;
  }
  
  public static String getDensitySuffix(int paramInt) {
    String str1 = getDensityName(paramInt);
    String str2 = str1;
    if (!str1.equals("")) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("-");
      stringBuilder.append(str1);
      str2 = stringBuilder.toString();
    } 
    return str2;
  }
  
  public static String getDrawbleDensityFolder(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("res/");
    stringBuilder.append(getDrawbleDensityName(paramInt));
    return stringBuilder.toString();
  }
  
  public static String getDrawbleDensityName(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("drawable");
    stringBuilder.append(getDensitySuffix(paramInt));
    return stringBuilder.toString();
  }
}
