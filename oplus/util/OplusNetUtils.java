package oplus.util;

import android.net.MacAddress;
import android.util.Log;

public class OplusNetUtils {
  private static final String TAG = "OplusNetUtils";
  
  public static String ipStrMask(String paramString) {
    if (paramString == null || "".equals(paramString))
      return ""; 
    try {
      paramString = paramString.replaceAll("((\\d{1,3}\\.){3})\\d{1,3}", "$1***");
      return paramString;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("invalid ip address exception e");
      stringBuilder.append(exception);
      Log.e("OplusNetUtils", stringBuilder.toString());
      return "invalid ip address";
    } 
  }
  
  public static String macStrMask(String paramString) {
    if (paramString == null)
      return "null"; 
    if ("".equals(paramString) || "any".equals(paramString))
      return paramString; 
    try {
      paramString = paramString.replaceAll("([A-Fa-f0-9]{2})((:[A-Fa-f0-9]{2}){3})((:[A-Fa-f0-9]{2}){2})", "$1:**:**:**$4");
      return paramString;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("invalid mac address exception e");
      stringBuilder.append(exception);
      Log.e("OplusNetUtils", stringBuilder.toString());
      return "invalid mac address";
    } 
  }
  
  public static String macStrMask(MacAddress paramMacAddress) {
    if (paramMacAddress == null)
      return "null"; 
    return macStrMask(paramMacAddress.toString());
  }
  
  public static String normalStrMask(String paramString) {
    if (paramString == null || paramString.isEmpty())
      return "EMPTY"; 
    char[] arrayOfChar = paramString.toCharArray();
    if (arrayOfChar.length > 4) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("");
      stringBuilder1.append(arrayOfChar[0]);
      stringBuilder1.append(arrayOfChar[1]);
      stringBuilder1.append("***");
      stringBuilder1.append(arrayOfChar[arrayOfChar.length - 2]);
      stringBuilder1.append(arrayOfChar[arrayOfChar.length - 1]);
      return stringBuilder1.toString();
    } 
    if (arrayOfChar.length < 3) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("");
      stringBuilder1.append(arrayOfChar[0]);
      stringBuilder1.append("*");
      return stringBuilder1.toString();
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("");
    stringBuilder.append(arrayOfChar[0]);
    stringBuilder.append("**");
    stringBuilder.append(arrayOfChar[arrayOfChar.length - 1]);
    return stringBuilder.toString();
  }
}
