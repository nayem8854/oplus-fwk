package oplus.util;

import android.content.Context;
import android.text.TextUtils;
import android.util.Slog;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class OplusStatistics {
  private static final int APP_ID_FRAMEWORK = 20120;
  
  public static final int FLAG_SEND_TO_ATOM = 2;
  
  public static final int FLAG_SEND_TO_DCS = 1;
  
  private static final String IMPL_OPLUS = "com.oplus.util.OplusStatisticsImpl";
  
  private static final String TAG = "OplusStatistics--";
  
  private static volatile IOplusStatistics sInstance = null;
  
  public static void onCommon(Context paramContext, String paramString1, String paramString2, Map<String, String> paramMap, boolean paramBoolean) {
    onCommon(paramContext, 20120, paramString1, paramString2, paramMap, paramBoolean);
  }
  
  public static void onCommon(Context paramContext, String paramString1, String paramString2, String paramString3, Map<String, String> paramMap, boolean paramBoolean) {
    if (TextUtils.isEmpty(paramString1)) {
      Slog.w("OplusStatistics--", "onCommon: appId is null.");
      return;
    } 
    int i = -1;
    try {
      int j = Integer.valueOf(paramString1).intValue();
    } catch (NumberFormatException numberFormatException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onCommon: illegal appId=");
      stringBuilder.append(paramString1);
      Slog.w("OplusStatistics--", stringBuilder.toString());
    } 
    if (i == -1)
      return; 
    onCommon(paramContext, i, paramString2, paramString3, paramMap, 1);
  }
  
  public static void onCommon(Context paramContext, int paramInt, String paramString1, String paramString2, Map<String, String> paramMap, boolean paramBoolean) {
    onCommon(paramContext, paramInt, paramString1, paramString2, paramMap, 1);
  }
  
  public static void onCommon(Context paramContext, String paramString1, String paramString2, List<Map<String, String>> paramList, boolean paramBoolean) {
    EventData eventData;
    if (paramList == null) {
      paramList = new ArrayList<>();
    } else {
      ArrayList<Map<String, String>> arrayList = new ArrayList(paramList.size());
      Iterator<Map<String, String>> iterator = paramList.iterator();
      while (true) {
        paramList = arrayList;
        if (iterator.hasNext()) {
          Map<? extends String, ? extends String> map = iterator.next();
          eventData = new EventData();
          eventData.appId = 20120;
          eventData.logTag = paramString1;
          eventData.eventId = paramString2;
          if (map == null) {
            eventData.logMap = new HashMap<>();
          } else {
            eventData.logMap = new HashMap<>(map);
          } 
          arrayList.add(eventData);
          continue;
        } 
        break;
      } 
    } 
    getInstance().onCommon(paramContext, (List<EventData>)eventData, 1);
  }
  
  public static void onCommon(Context paramContext, String paramString1, String paramString2, Map<String, String> paramMap, boolean paramBoolean, int paramInt) {
    onCommon(paramContext, 20120, paramString1, paramString2, paramMap, paramInt);
  }
  
  public static void onCommonSync(Context paramContext, String paramString1, String paramString2, Map<String, String> paramMap, boolean paramBoolean) {
    EventData eventData = new EventData();
    eventData.appId = 20120;
    eventData.logTag = paramString1;
    eventData.eventId = paramString2;
    if (paramMap == null) {
      eventData.logMap = new HashMap<>();
    } else {
      eventData.logMap = new HashMap<>(paramMap);
    } 
    getInstance().onCommonSync(paramContext, eventData, 1);
  }
  
  private static void onCommon(Context paramContext, int paramInt1, String paramString1, String paramString2, Map<String, String> paramMap, int paramInt2) {
    EventData eventData = new EventData();
    eventData.appId = paramInt1;
    eventData.logTag = paramString1;
    eventData.eventId = paramString2;
    if (paramMap == null) {
      eventData.logMap = new HashMap<>();
    } else {
      eventData.logMap = new HashMap<>(paramMap);
    } 
    getInstance().onCommon(paramContext, eventData, paramInt2);
  }
  
  private static IOplusStatistics getInstance() {
    // Byte code:
    //   0: getstatic oplus/util/OplusStatistics.sInstance : Loplus/util/OplusStatistics$IOplusStatistics;
    //   3: ifnonnull -> 56
    //   6: ldc oplus/util/OplusStatistics$IOplusStatistics
    //   8: monitorenter
    //   9: getstatic oplus/util/OplusStatistics.sInstance : Loplus/util/OplusStatistics$IOplusStatistics;
    //   12: ifnonnull -> 44
    //   15: ldc 'com.oplus.util.OplusStatisticsImpl'
    //   17: invokestatic newInstance : (Ljava/lang/String;)Ljava/lang/Object;
    //   20: checkcast oplus/util/OplusStatistics$IOplusStatistics
    //   23: putstatic oplus/util/OplusStatistics.sInstance : Loplus/util/OplusStatistics$IOplusStatistics;
    //   26: getstatic oplus/util/OplusStatistics.sInstance : Loplus/util/OplusStatistics$IOplusStatistics;
    //   29: ifnonnull -> 44
    //   32: new oplus/util/OplusStatistics$Dummy
    //   35: astore_0
    //   36: aload_0
    //   37: invokespecial <init> : ()V
    //   40: aload_0
    //   41: putstatic oplus/util/OplusStatistics.sInstance : Loplus/util/OplusStatistics$IOplusStatistics;
    //   44: ldc oplus/util/OplusStatistics$IOplusStatistics
    //   46: monitorexit
    //   47: goto -> 56
    //   50: astore_0
    //   51: ldc oplus/util/OplusStatistics$IOplusStatistics
    //   53: monitorexit
    //   54: aload_0
    //   55: athrow
    //   56: getstatic oplus/util/OplusStatistics.sInstance : Loplus/util/OplusStatistics$IOplusStatistics;
    //   59: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #193	-> 0
    //   #194	-> 6
    //   #195	-> 9
    //   #196	-> 15
    //   #198	-> 26
    //   #199	-> 32
    //   #202	-> 44
    //   #204	-> 56
    // Exception table:
    //   from	to	target	type
    //   9	15	50	finally
    //   15	26	50	finally
    //   26	32	50	finally
    //   32	44	50	finally
    //   44	47	50	finally
    //   51	54	50	finally
  }
  
  static Object newInstance(String paramString) {
    Constructor<?> constructor;
    Constructor constructor1 = null;
    try {
      Class<?> clazz = Class.forName(paramString);
      Constructor<?> constructor2 = clazz.getConstructor(new Class[0]);
      constructor = constructor2 = (Constructor<?>)constructor2.newInstance(new Object[0]);
    } catch (ClassNotFoundException|NoSuchMethodException|IllegalAccessException|InstantiationException|java.lang.reflect.InvocationTargetException classNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Reflect ");
      stringBuilder.append((String)constructor);
      stringBuilder.append(" exception");
      stringBuilder.append(classNotFoundException.toString());
      Slog.e("OplusStatistics--", stringBuilder.toString());
      constructor = constructor1;
    } 
    return constructor;
  }
  
  class Dummy implements IOplusStatistics {
    public void onCommon(Context param1Context, OplusStatistics.EventData param1EventData, int param1Int) {
      Slog.w("OplusStatistics--", "Dummy onCommon");
    }
    
    public void onCommon(Context param1Context, List<OplusStatistics.EventData> param1List, int param1Int) {
      Slog.w("OplusStatistics--", "Dummy onCommon list");
    }
    
    public void onCommonSync(Context param1Context, OplusStatistics.EventData param1EventData, int param1Int) {
      Slog.w("OplusStatistics--", "Dummy onCommonSync");
    }
  }
  
  public static class EventData {
    public int appId;
    
    public String eventId;
    
    public Map<String, String> logMap;
    
    public String logTag;
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("EventData{appId=");
      stringBuilder.append(this.appId);
      stringBuilder.append(", logTag=");
      stringBuilder.append(this.logTag);
      stringBuilder.append(", eventId=");
      stringBuilder.append(this.eventId);
      stringBuilder.append(" }");
      return stringBuilder.toString();
    }
  }
  
  public static interface IOplusStatistics {
    public static final int FLAG_SEND_TO_ATOM = 2;
    
    public static final int FLAG_SEND_TO_DATA_CENTER = 1;
    
    void onCommon(Context param1Context, List<OplusStatistics.EventData> param1List, int param1Int);
    
    void onCommon(Context param1Context, OplusStatistics.EventData param1EventData, int param1Int);
    
    void onCommonSync(Context param1Context, OplusStatistics.EventData param1EventData, int param1Int);
  }
}
