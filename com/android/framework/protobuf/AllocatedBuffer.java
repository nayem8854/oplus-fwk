package com.android.framework.protobuf;

import java.nio.ByteBuffer;

abstract class AllocatedBuffer {
  public static AllocatedBuffer wrap(byte[] paramArrayOfbyte) {
    return wrapNoCheck(paramArrayOfbyte, 0, paramArrayOfbyte.length);
  }
  
  public static AllocatedBuffer wrap(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    if (paramInt1 >= 0 && paramInt2 >= 0 && paramInt1 + paramInt2 <= paramArrayOfbyte.length)
      return wrapNoCheck(paramArrayOfbyte, paramInt1, paramInt2); 
    int i = paramArrayOfbyte.length;
    throw new IndexOutOfBoundsException(String.format("bytes.length=%d, offset=%d, length=%d", new Object[] { Integer.valueOf(i), Integer.valueOf(paramInt1), Integer.valueOf(paramInt2) }));
  }
  
  public static AllocatedBuffer wrap(ByteBuffer paramByteBuffer) {
    Internal.checkNotNull(paramByteBuffer, "buffer");
    return (AllocatedBuffer)new Object(paramByteBuffer);
  }
  
  private static AllocatedBuffer wrapNoCheck(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    return (AllocatedBuffer)new Object(paramArrayOfbyte, paramInt1, paramInt2);
  }
  
  public abstract byte[] array();
  
  public abstract int arrayOffset();
  
  public abstract boolean hasArray();
  
  public abstract boolean hasNioBuffer();
  
  public abstract int limit();
  
  public abstract ByteBuffer nioBuffer();
  
  public abstract int position();
  
  public abstract AllocatedBuffer position(int paramInt);
  
  public abstract int remaining();
}
