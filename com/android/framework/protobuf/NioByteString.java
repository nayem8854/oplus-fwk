package com.android.framework.protobuf;

import java.io.IOException;
import java.io.InputStream;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.List;

final class NioByteString extends ByteString.LeafByteString {
  private final ByteBuffer buffer;
  
  NioByteString(ByteBuffer paramByteBuffer) {
    Internal.checkNotNull(paramByteBuffer, "buffer");
    this.buffer = paramByteBuffer.slice().order(ByteOrder.nativeOrder());
  }
  
  private Object writeReplace() {
    return ByteString.copyFrom(this.buffer.slice());
  }
  
  private void readObject(ObjectInputStream paramObjectInputStream) throws IOException {
    throw new InvalidObjectException("NioByteString instances are not to be serialized directly");
  }
  
  public byte byteAt(int paramInt) {
    try {
      return this.buffer.get(paramInt);
    } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) {
      throw arrayIndexOutOfBoundsException;
    } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
      throw new ArrayIndexOutOfBoundsException(indexOutOfBoundsException.getMessage());
    } 
  }
  
  public byte internalByteAt(int paramInt) {
    return byteAt(paramInt);
  }
  
  public int size() {
    return this.buffer.remaining();
  }
  
  public ByteString substring(int paramInt1, int paramInt2) {
    try {
      ByteBuffer byteBuffer = slice(paramInt1, paramInt2);
      return new NioByteString(byteBuffer);
    } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) {
      throw arrayIndexOutOfBoundsException;
    } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
      throw new ArrayIndexOutOfBoundsException(indexOutOfBoundsException.getMessage());
    } 
  }
  
  protected void copyToInternal(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3) {
    ByteBuffer byteBuffer = this.buffer.slice();
    byteBuffer.position(paramInt1);
    byteBuffer.get(paramArrayOfbyte, paramInt2, paramInt3);
  }
  
  public void copyTo(ByteBuffer paramByteBuffer) {
    paramByteBuffer.put(this.buffer.slice());
  }
  
  public void writeTo(OutputStream paramOutputStream) throws IOException {
    paramOutputStream.write(toByteArray());
  }
  
  boolean equalsRange(ByteString paramByteString, int paramInt1, int paramInt2) {
    return substring(0, paramInt2).equals(paramByteString.substring(paramInt1, paramInt1 + paramInt2));
  }
  
  void writeToInternal(OutputStream paramOutputStream, int paramInt1, int paramInt2) throws IOException {
    if (this.buffer.hasArray()) {
      int i = this.buffer.arrayOffset(), j = this.buffer.position();
      paramOutputStream.write(this.buffer.array(), i + j + paramInt1, paramInt2);
      return;
    } 
    ByteBufferWriter.write(slice(paramInt1, paramInt1 + paramInt2), paramOutputStream);
  }
  
  void writeTo(ByteOutput paramByteOutput) throws IOException {
    paramByteOutput.writeLazy(this.buffer.slice());
  }
  
  public ByteBuffer asReadOnlyByteBuffer() {
    return this.buffer.asReadOnlyBuffer();
  }
  
  public List<ByteBuffer> asReadOnlyByteBufferList() {
    return Collections.singletonList(asReadOnlyByteBuffer());
  }
  
  protected String toStringInternal(Charset paramCharset) {
    byte[] arrayOfByte;
    boolean bool;
    int i;
    if (this.buffer.hasArray()) {
      arrayOfByte = this.buffer.array();
      bool = this.buffer.arrayOffset() + this.buffer.position();
      i = this.buffer.remaining();
    } else {
      arrayOfByte = toByteArray();
      bool = false;
      i = arrayOfByte.length;
    } 
    return new String(arrayOfByte, bool, i, paramCharset);
  }
  
  public boolean isValidUtf8() {
    return Utf8.isValidUtf8(this.buffer);
  }
  
  protected int partialIsValidUtf8(int paramInt1, int paramInt2, int paramInt3) {
    return Utf8.partialIsValidUtf8(paramInt1, this.buffer, paramInt2, paramInt2 + paramInt3);
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject == this)
      return true; 
    if (!(paramObject instanceof ByteString))
      return false; 
    ByteString byteString = (ByteString)paramObject;
    if (size() != byteString.size())
      return false; 
    if (size() == 0)
      return true; 
    if (paramObject instanceof NioByteString)
      return this.buffer.equals(((NioByteString)paramObject).buffer); 
    if (paramObject instanceof RopeByteString)
      return paramObject.equals(this); 
    return this.buffer.equals(byteString.asReadOnlyByteBuffer());
  }
  
  protected int partialHash(int paramInt1, int paramInt2, int paramInt3) {
    int j;
    for (int i = paramInt2; paramInt1 < paramInt2 + paramInt3; paramInt1++)
      j = j * 31 + this.buffer.get(paramInt1); 
    return j;
  }
  
  public InputStream newInput() {
    return (InputStream)new Object(this);
  }
  
  public CodedInputStream newCodedInput() {
    return CodedInputStream.newInstance(this.buffer, true);
  }
  
  private ByteBuffer slice(int paramInt1, int paramInt2) {
    if (paramInt1 >= this.buffer.position() && paramInt2 <= this.buffer.limit() && paramInt1 <= paramInt2) {
      ByteBuffer byteBuffer = this.buffer.slice();
      byteBuffer.position(paramInt1 - this.buffer.position());
      byteBuffer.limit(paramInt2 - this.buffer.position());
      return byteBuffer;
    } 
    throw new IllegalArgumentException(String.format("Invalid indices [%d, %d]", new Object[] { Integer.valueOf(paramInt1), Integer.valueOf(paramInt2) }));
  }
}
