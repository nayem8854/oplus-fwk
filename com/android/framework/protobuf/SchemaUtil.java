package com.android.framework.protobuf;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.List;

final class SchemaUtil {
  private static final int DEFAULT_LOOK_UP_START_NUMBER = 40;
  
  private static final Class<?> GENERATED_MESSAGE_CLASS = getGeneratedMessageClass();
  
  private static final UnknownFieldSchema<?, ?> PROTO2_UNKNOWN_FIELD_SET_SCHEMA = getUnknownFieldSetSchema(false);
  
  private static final UnknownFieldSchema<?, ?> PROTO3_UNKNOWN_FIELD_SET_SCHEMA = getUnknownFieldSetSchema(true);
  
  private static final UnknownFieldSchema<?, ?> UNKNOWN_FIELD_SET_LITE_SCHEMA = new UnknownFieldSetLiteSchema();
  
  public static void requireGeneratedMessage(Class<?> paramClass) {
    if (!GeneratedMessageLite.class.isAssignableFrom(paramClass)) {
      Class<?> clazz = GENERATED_MESSAGE_CLASS;
      if (clazz != null)
        if (!clazz.isAssignableFrom(paramClass))
          throw new IllegalArgumentException("Message classes must extend GeneratedMessage or GeneratedMessageLite");  
    } 
  }
  
  public static void writeDouble(int paramInt, double paramDouble, Writer paramWriter) throws IOException {
    if (Double.compare(paramDouble, 0.0D) != 0)
      paramWriter.writeDouble(paramInt, paramDouble); 
  }
  
  public static void writeFloat(int paramInt, float paramFloat, Writer paramWriter) throws IOException {
    if (Float.compare(paramFloat, 0.0F) != 0)
      paramWriter.writeFloat(paramInt, paramFloat); 
  }
  
  public static void writeInt64(int paramInt, long paramLong, Writer paramWriter) throws IOException {
    if (paramLong != 0L)
      paramWriter.writeInt64(paramInt, paramLong); 
  }
  
  public static void writeUInt64(int paramInt, long paramLong, Writer paramWriter) throws IOException {
    if (paramLong != 0L)
      paramWriter.writeUInt64(paramInt, paramLong); 
  }
  
  public static void writeSInt64(int paramInt, long paramLong, Writer paramWriter) throws IOException {
    if (paramLong != 0L)
      paramWriter.writeSInt64(paramInt, paramLong); 
  }
  
  public static void writeFixed64(int paramInt, long paramLong, Writer paramWriter) throws IOException {
    if (paramLong != 0L)
      paramWriter.writeFixed64(paramInt, paramLong); 
  }
  
  public static void writeSFixed64(int paramInt, long paramLong, Writer paramWriter) throws IOException {
    if (paramLong != 0L)
      paramWriter.writeSFixed64(paramInt, paramLong); 
  }
  
  public static void writeInt32(int paramInt1, int paramInt2, Writer paramWriter) throws IOException {
    if (paramInt2 != 0)
      paramWriter.writeInt32(paramInt1, paramInt2); 
  }
  
  public static void writeUInt32(int paramInt1, int paramInt2, Writer paramWriter) throws IOException {
    if (paramInt2 != 0)
      paramWriter.writeUInt32(paramInt1, paramInt2); 
  }
  
  public static void writeSInt32(int paramInt1, int paramInt2, Writer paramWriter) throws IOException {
    if (paramInt2 != 0)
      paramWriter.writeSInt32(paramInt1, paramInt2); 
  }
  
  public static void writeFixed32(int paramInt1, int paramInt2, Writer paramWriter) throws IOException {
    if (paramInt2 != 0)
      paramWriter.writeFixed32(paramInt1, paramInt2); 
  }
  
  public static void writeSFixed32(int paramInt1, int paramInt2, Writer paramWriter) throws IOException {
    if (paramInt2 != 0)
      paramWriter.writeSFixed32(paramInt1, paramInt2); 
  }
  
  public static void writeEnum(int paramInt1, int paramInt2, Writer paramWriter) throws IOException {
    if (paramInt2 != 0)
      paramWriter.writeEnum(paramInt1, paramInt2); 
  }
  
  public static void writeBool(int paramInt, boolean paramBoolean, Writer paramWriter) throws IOException {
    if (paramBoolean)
      paramWriter.writeBool(paramInt, true); 
  }
  
  public static void writeString(int paramInt, Object paramObject, Writer paramWriter) throws IOException {
    if (paramObject instanceof String) {
      writeStringInternal(paramInt, (String)paramObject, paramWriter);
    } else {
      writeBytes(paramInt, (ByteString)paramObject, paramWriter);
    } 
  }
  
  private static void writeStringInternal(int paramInt, String paramString, Writer paramWriter) throws IOException {
    if (paramString != null && !paramString.isEmpty())
      paramWriter.writeString(paramInt, paramString); 
  }
  
  public static void writeBytes(int paramInt, ByteString paramByteString, Writer paramWriter) throws IOException {
    if (paramByteString != null && !paramByteString.isEmpty())
      paramWriter.writeBytes(paramInt, paramByteString); 
  }
  
  public static void writeMessage(int paramInt, Object paramObject, Writer paramWriter) throws IOException {
    if (paramObject != null)
      paramWriter.writeMessage(paramInt, paramObject); 
  }
  
  public static void writeDoubleList(int paramInt, List<Double> paramList, Writer paramWriter, boolean paramBoolean) throws IOException {
    if (paramList != null && !paramList.isEmpty())
      paramWriter.writeDoubleList(paramInt, paramList, paramBoolean); 
  }
  
  public static void writeFloatList(int paramInt, List<Float> paramList, Writer paramWriter, boolean paramBoolean) throws IOException {
    if (paramList != null && !paramList.isEmpty())
      paramWriter.writeFloatList(paramInt, paramList, paramBoolean); 
  }
  
  public static void writeInt64List(int paramInt, List<Long> paramList, Writer paramWriter, boolean paramBoolean) throws IOException {
    if (paramList != null && !paramList.isEmpty())
      paramWriter.writeInt64List(paramInt, paramList, paramBoolean); 
  }
  
  public static void writeUInt64List(int paramInt, List<Long> paramList, Writer paramWriter, boolean paramBoolean) throws IOException {
    if (paramList != null && !paramList.isEmpty())
      paramWriter.writeUInt64List(paramInt, paramList, paramBoolean); 
  }
  
  public static void writeSInt64List(int paramInt, List<Long> paramList, Writer paramWriter, boolean paramBoolean) throws IOException {
    if (paramList != null && !paramList.isEmpty())
      paramWriter.writeSInt64List(paramInt, paramList, paramBoolean); 
  }
  
  public static void writeFixed64List(int paramInt, List<Long> paramList, Writer paramWriter, boolean paramBoolean) throws IOException {
    if (paramList != null && !paramList.isEmpty())
      paramWriter.writeFixed64List(paramInt, paramList, paramBoolean); 
  }
  
  public static void writeSFixed64List(int paramInt, List<Long> paramList, Writer paramWriter, boolean paramBoolean) throws IOException {
    if (paramList != null && !paramList.isEmpty())
      paramWriter.writeSFixed64List(paramInt, paramList, paramBoolean); 
  }
  
  public static void writeInt32List(int paramInt, List<Integer> paramList, Writer paramWriter, boolean paramBoolean) throws IOException {
    if (paramList != null && !paramList.isEmpty())
      paramWriter.writeInt32List(paramInt, paramList, paramBoolean); 
  }
  
  public static void writeUInt32List(int paramInt, List<Integer> paramList, Writer paramWriter, boolean paramBoolean) throws IOException {
    if (paramList != null && !paramList.isEmpty())
      paramWriter.writeUInt32List(paramInt, paramList, paramBoolean); 
  }
  
  public static void writeSInt32List(int paramInt, List<Integer> paramList, Writer paramWriter, boolean paramBoolean) throws IOException {
    if (paramList != null && !paramList.isEmpty())
      paramWriter.writeSInt32List(paramInt, paramList, paramBoolean); 
  }
  
  public static void writeFixed32List(int paramInt, List<Integer> paramList, Writer paramWriter, boolean paramBoolean) throws IOException {
    if (paramList != null && !paramList.isEmpty())
      paramWriter.writeFixed32List(paramInt, paramList, paramBoolean); 
  }
  
  public static void writeSFixed32List(int paramInt, List<Integer> paramList, Writer paramWriter, boolean paramBoolean) throws IOException {
    if (paramList != null && !paramList.isEmpty())
      paramWriter.writeSFixed32List(paramInt, paramList, paramBoolean); 
  }
  
  public static void writeEnumList(int paramInt, List<Integer> paramList, Writer paramWriter, boolean paramBoolean) throws IOException {
    if (paramList != null && !paramList.isEmpty())
      paramWriter.writeEnumList(paramInt, paramList, paramBoolean); 
  }
  
  public static void writeBoolList(int paramInt, List<Boolean> paramList, Writer paramWriter, boolean paramBoolean) throws IOException {
    if (paramList != null && !paramList.isEmpty())
      paramWriter.writeBoolList(paramInt, paramList, paramBoolean); 
  }
  
  public static void writeStringList(int paramInt, List<String> paramList, Writer paramWriter) throws IOException {
    if (paramList != null && !paramList.isEmpty())
      paramWriter.writeStringList(paramInt, paramList); 
  }
  
  public static void writeBytesList(int paramInt, List<ByteString> paramList, Writer paramWriter) throws IOException {
    if (paramList != null && !paramList.isEmpty())
      paramWriter.writeBytesList(paramInt, paramList); 
  }
  
  public static void writeMessageList(int paramInt, List<?> paramList, Writer paramWriter) throws IOException {
    if (paramList != null && !paramList.isEmpty())
      paramWriter.writeMessageList(paramInt, paramList); 
  }
  
  public static void writeMessageList(int paramInt, List<?> paramList, Writer paramWriter, Schema paramSchema) throws IOException {
    if (paramList != null && !paramList.isEmpty())
      paramWriter.writeMessageList(paramInt, paramList, paramSchema); 
  }
  
  public static void writeLazyFieldList(int paramInt, List<?> paramList, Writer paramWriter) throws IOException {
    if (paramList != null && !paramList.isEmpty())
      for (Object object : paramList)
        ((LazyFieldLite)object).writeTo(paramWriter, paramInt);  
  }
  
  public static void writeGroupList(int paramInt, List<?> paramList, Writer paramWriter) throws IOException {
    if (paramList != null && !paramList.isEmpty())
      paramWriter.writeGroupList(paramInt, paramList); 
  }
  
  public static void writeGroupList(int paramInt, List<?> paramList, Writer paramWriter, Schema paramSchema) throws IOException {
    if (paramList != null && !paramList.isEmpty())
      paramWriter.writeGroupList(paramInt, paramList, paramSchema); 
  }
  
  static int computeSizeInt64ListNoTag(List<Long> paramList) {
    int i = paramList.size();
    if (i == 0)
      return 0; 
    int j = 0, k = 0;
    if (paramList instanceof LongArrayList) {
      paramList = paramList;
      for (byte b = 0; b < i; b++)
        j += CodedOutputStream.computeInt64SizeNoTag(paramList.getLong(b)); 
      k = j;
    } else {
      byte b = 0;
      while (true) {
        k = j;
        if (b < i) {
          j += CodedOutputStream.computeInt64SizeNoTag(((Long)paramList.get(b)).longValue());
          b++;
          continue;
        } 
        break;
      } 
    } 
    return k;
  }
  
  static int computeSizeInt64List(int paramInt, List<Long> paramList, boolean paramBoolean) {
    int i = paramList.size();
    if (i == 0)
      return 0; 
    i = computeSizeInt64ListNoTag(paramList);
    if (paramBoolean) {
      paramInt = CodedOutputStream.computeTagSize(paramInt);
      i = CodedOutputStream.computeLengthDelimitedFieldSize(i);
      return paramInt + i;
    } 
    return paramList.size() * CodedOutputStream.computeTagSize(paramInt) + i;
  }
  
  static int computeSizeUInt64ListNoTag(List<Long> paramList) {
    int i = paramList.size();
    if (i == 0)
      return 0; 
    int j = 0, k = 0;
    if (paramList instanceof LongArrayList) {
      paramList = paramList;
      for (byte b = 0; b < i; b++)
        k += CodedOutputStream.computeUInt64SizeNoTag(paramList.getLong(b)); 
      j = k;
    } else {
      byte b = 0;
      k = j;
      while (true) {
        j = k;
        if (b < i) {
          k += CodedOutputStream.computeUInt64SizeNoTag(((Long)paramList.get(b)).longValue());
          b++;
          continue;
        } 
        break;
      } 
    } 
    return j;
  }
  
  static int computeSizeUInt64List(int paramInt, List<Long> paramList, boolean paramBoolean) {
    int i = paramList.size();
    if (i == 0)
      return 0; 
    int j = computeSizeUInt64ListNoTag(paramList);
    if (paramBoolean) {
      paramInt = CodedOutputStream.computeTagSize(paramInt);
      j = CodedOutputStream.computeLengthDelimitedFieldSize(j);
      return paramInt + j;
    } 
    return CodedOutputStream.computeTagSize(paramInt) * i + j;
  }
  
  static int computeSizeSInt64ListNoTag(List<Long> paramList) {
    int i = paramList.size();
    if (i == 0)
      return 0; 
    int j = 0, k = 0;
    if (paramList instanceof LongArrayList) {
      paramList = paramList;
      for (byte b = 0; b < i; b++)
        j += CodedOutputStream.computeSInt64SizeNoTag(paramList.getLong(b)); 
      k = j;
    } else {
      byte b = 0;
      while (true) {
        k = j;
        if (b < i) {
          j += CodedOutputStream.computeSInt64SizeNoTag(((Long)paramList.get(b)).longValue());
          b++;
          continue;
        } 
        break;
      } 
    } 
    return k;
  }
  
  static int computeSizeSInt64List(int paramInt, List<Long> paramList, boolean paramBoolean) {
    int i = paramList.size();
    if (i == 0)
      return 0; 
    int j = computeSizeSInt64ListNoTag(paramList);
    if (paramBoolean) {
      paramInt = CodedOutputStream.computeTagSize(paramInt);
      j = CodedOutputStream.computeLengthDelimitedFieldSize(j);
      return paramInt + j;
    } 
    return CodedOutputStream.computeTagSize(paramInt) * i + j;
  }
  
  static int computeSizeEnumListNoTag(List<Integer> paramList) {
    int i = paramList.size();
    if (i == 0)
      return 0; 
    int j = 0, k = 0;
    if (paramList instanceof IntArrayList) {
      paramList = paramList;
      for (byte b = 0; b < i; b++)
        j += CodedOutputStream.computeEnumSizeNoTag(paramList.getInt(b)); 
      k = j;
    } else {
      byte b = 0;
      while (true) {
        k = j;
        if (b < i) {
          j += CodedOutputStream.computeEnumSizeNoTag(((Integer)paramList.get(b)).intValue());
          b++;
          continue;
        } 
        break;
      } 
    } 
    return k;
  }
  
  static int computeSizeEnumList(int paramInt, List<Integer> paramList, boolean paramBoolean) {
    int i = paramList.size();
    if (i == 0)
      return 0; 
    int j = computeSizeEnumListNoTag(paramList);
    if (paramBoolean) {
      paramInt = CodedOutputStream.computeTagSize(paramInt);
      j = CodedOutputStream.computeLengthDelimitedFieldSize(j);
      return paramInt + j;
    } 
    return CodedOutputStream.computeTagSize(paramInt) * i + j;
  }
  
  static int computeSizeInt32ListNoTag(List<Integer> paramList) {
    int i = paramList.size();
    if (i == 0)
      return 0; 
    int j = 0, k = 0;
    if (paramList instanceof IntArrayList) {
      paramList = paramList;
      for (byte b = 0; b < i; b++)
        j += CodedOutputStream.computeInt32SizeNoTag(paramList.getInt(b)); 
      k = j;
    } else {
      byte b = 0;
      while (true) {
        k = j;
        if (b < i) {
          j += CodedOutputStream.computeInt32SizeNoTag(((Integer)paramList.get(b)).intValue());
          b++;
          continue;
        } 
        break;
      } 
    } 
    return k;
  }
  
  static int computeSizeInt32List(int paramInt, List<Integer> paramList, boolean paramBoolean) {
    int i = paramList.size();
    if (i == 0)
      return 0; 
    int j = computeSizeInt32ListNoTag(paramList);
    if (paramBoolean) {
      paramInt = CodedOutputStream.computeTagSize(paramInt);
      j = CodedOutputStream.computeLengthDelimitedFieldSize(j);
      return paramInt + j;
    } 
    return CodedOutputStream.computeTagSize(paramInt) * i + j;
  }
  
  static int computeSizeUInt32ListNoTag(List<Integer> paramList) {
    int i = paramList.size();
    if (i == 0)
      return 0; 
    int j = 0, k = 0;
    if (paramList instanceof IntArrayList) {
      paramList = paramList;
      for (byte b = 0; b < i; b++)
        k += CodedOutputStream.computeUInt32SizeNoTag(paramList.getInt(b)); 
      j = k;
    } else {
      byte b = 0;
      k = j;
      while (true) {
        j = k;
        if (b < i) {
          k += CodedOutputStream.computeUInt32SizeNoTag(((Integer)paramList.get(b)).intValue());
          b++;
          continue;
        } 
        break;
      } 
    } 
    return j;
  }
  
  static int computeSizeUInt32List(int paramInt, List<Integer> paramList, boolean paramBoolean) {
    int i = paramList.size();
    if (i == 0)
      return 0; 
    int j = computeSizeUInt32ListNoTag(paramList);
    if (paramBoolean) {
      paramInt = CodedOutputStream.computeTagSize(paramInt);
      j = CodedOutputStream.computeLengthDelimitedFieldSize(j);
      return paramInt + j;
    } 
    return CodedOutputStream.computeTagSize(paramInt) * i + j;
  }
  
  static int computeSizeSInt32ListNoTag(List<Integer> paramList) {
    int i = paramList.size();
    if (i == 0)
      return 0; 
    int j = 0, k = 0;
    if (paramList instanceof IntArrayList) {
      paramList = paramList;
      for (byte b = 0; b < i; b++)
        j += CodedOutputStream.computeSInt32SizeNoTag(paramList.getInt(b)); 
      k = j;
    } else {
      byte b = 0;
      while (true) {
        k = j;
        if (b < i) {
          j += CodedOutputStream.computeSInt32SizeNoTag(((Integer)paramList.get(b)).intValue());
          b++;
          continue;
        } 
        break;
      } 
    } 
    return k;
  }
  
  static int computeSizeSInt32List(int paramInt, List<Integer> paramList, boolean paramBoolean) {
    int i = paramList.size();
    if (i == 0)
      return 0; 
    int j = computeSizeSInt32ListNoTag(paramList);
    if (paramBoolean) {
      paramInt = CodedOutputStream.computeTagSize(paramInt);
      j = CodedOutputStream.computeLengthDelimitedFieldSize(j);
      return paramInt + j;
    } 
    return CodedOutputStream.computeTagSize(paramInt) * i + j;
  }
  
  static int computeSizeFixed32ListNoTag(List<?> paramList) {
    return paramList.size() * 4;
  }
  
  static int computeSizeFixed32List(int paramInt, List<?> paramList, boolean paramBoolean) {
    int i = paramList.size();
    if (i == 0)
      return 0; 
    if (paramBoolean) {
      paramInt = CodedOutputStream.computeTagSize(paramInt);
      i = CodedOutputStream.computeLengthDelimitedFieldSize(i * 4);
      return paramInt + i;
    } 
    return CodedOutputStream.computeFixed32Size(paramInt, 0) * i;
  }
  
  static int computeSizeFixed64ListNoTag(List<?> paramList) {
    return paramList.size() * 8;
  }
  
  static int computeSizeFixed64List(int paramInt, List<?> paramList, boolean paramBoolean) {
    int i = paramList.size();
    if (i == 0)
      return 0; 
    if (paramBoolean) {
      paramInt = CodedOutputStream.computeTagSize(paramInt);
      i = CodedOutputStream.computeLengthDelimitedFieldSize(i * 8);
      return paramInt + i;
    } 
    return CodedOutputStream.computeFixed64Size(paramInt, 0L) * i;
  }
  
  static int computeSizeBoolListNoTag(List<?> paramList) {
    return paramList.size();
  }
  
  static int computeSizeBoolList(int paramInt, List<?> paramList, boolean paramBoolean) {
    int i = paramList.size();
    if (i == 0)
      return 0; 
    if (paramBoolean) {
      paramInt = CodedOutputStream.computeTagSize(paramInt);
      i = CodedOutputStream.computeLengthDelimitedFieldSize(i);
      return paramInt + i;
    } 
    return CodedOutputStream.computeBoolSize(paramInt, true) * i;
  }
  
  static int computeSizeStringList(int paramInt, List<?> paramList) {
    int j, i = paramList.size();
    if (i == 0)
      return 0; 
    paramInt = CodedOutputStream.computeTagSize(paramInt) * i;
    if (paramList instanceof LazyStringList) {
      paramList = paramList;
      for (byte b = 0; b < i; b++) {
        Object object = paramList.getRaw(b);
        if (object instanceof ByteString) {
          paramInt += CodedOutputStream.computeBytesSizeNoTag((ByteString)object);
        } else {
          paramInt += CodedOutputStream.computeStringSizeNoTag((String)object);
        } 
      } 
      j = paramInt;
    } else {
      byte b = 0;
      while (true) {
        j = paramInt;
        if (b < i) {
          Object object = paramList.get(b);
          if (object instanceof ByteString) {
            paramInt += CodedOutputStream.computeBytesSizeNoTag((ByteString)object);
          } else {
            paramInt += CodedOutputStream.computeStringSizeNoTag((String)object);
          } 
          b++;
          continue;
        } 
        break;
      } 
    } 
    return j;
  }
  
  static int computeSizeMessage(int paramInt, Object paramObject, Schema paramSchema) {
    if (paramObject instanceof LazyFieldLite)
      return CodedOutputStream.computeLazyFieldSize(paramInt, (LazyFieldLite)paramObject); 
    return CodedOutputStream.computeMessageSize(paramInt, (MessageLite)paramObject, paramSchema);
  }
  
  static int computeSizeMessageList(int paramInt, List<?> paramList) {
    int i = paramList.size();
    if (i == 0)
      return 0; 
    paramInt = CodedOutputStream.computeTagSize(paramInt) * i;
    for (byte b = 0; b < i; b++) {
      Object object = paramList.get(b);
      if (object instanceof LazyFieldLite) {
        paramInt += CodedOutputStream.computeLazyFieldSizeNoTag((LazyFieldLite)object);
      } else {
        paramInt += CodedOutputStream.computeMessageSizeNoTag((MessageLite)object);
      } 
    } 
    return paramInt;
  }
  
  static int computeSizeMessageList(int paramInt, List<?> paramList, Schema paramSchema) {
    int i = paramList.size();
    if (i == 0)
      return 0; 
    paramInt = CodedOutputStream.computeTagSize(paramInt) * i;
    for (byte b = 0; b < i; b++) {
      Object object = paramList.get(b);
      if (object instanceof LazyFieldLite) {
        paramInt += CodedOutputStream.computeLazyFieldSizeNoTag((LazyFieldLite)object);
      } else {
        paramInt += CodedOutputStream.computeMessageSizeNoTag((MessageLite)object, paramSchema);
      } 
    } 
    return paramInt;
  }
  
  static int computeSizeByteStringList(int paramInt, List<ByteString> paramList) {
    int i = paramList.size();
    if (i == 0)
      return 0; 
    i = CodedOutputStream.computeTagSize(paramInt) * i;
    for (paramInt = 0; paramInt < paramList.size(); paramInt++)
      i += CodedOutputStream.computeBytesSizeNoTag(paramList.get(paramInt)); 
    return i;
  }
  
  static int computeSizeGroupList(int paramInt, List<MessageLite> paramList) {
    int i = paramList.size();
    if (i == 0)
      return 0; 
    int j = 0;
    for (byte b = 0; b < i; b++)
      j += CodedOutputStream.computeGroupSize(paramInt, paramList.get(b)); 
    return j;
  }
  
  static int computeSizeGroupList(int paramInt, List<MessageLite> paramList, Schema paramSchema) {
    int i = paramList.size();
    if (i == 0)
      return 0; 
    int j = 0;
    for (byte b = 0; b < i; b++)
      j += CodedOutputStream.computeGroupSize(paramInt, paramList.get(b), paramSchema); 
    return j;
  }
  
  public static boolean shouldUseTableSwitch(FieldInfo[] paramArrayOfFieldInfo) {
    if (paramArrayOfFieldInfo.length == 0)
      return false; 
    int i = paramArrayOfFieldInfo[0].getFieldNumber();
    int j = paramArrayOfFieldInfo[paramArrayOfFieldInfo.length - 1].getFieldNumber();
    return shouldUseTableSwitch(i, j, paramArrayOfFieldInfo.length);
  }
  
  public static boolean shouldUseTableSwitch(int paramInt1, int paramInt2, int paramInt3) {
    boolean bool = true;
    if (paramInt2 < 40)
      return true; 
    long l1 = paramInt2, l2 = paramInt1;
    long l3 = paramInt3;
    long l4 = paramInt3;
    if (3L * 3L + l1 - l2 + 1L > 3L * (l4 + 3L) + l3 * 2L + 3L)
      bool = false; 
    return bool;
  }
  
  public static UnknownFieldSchema<?, ?> proto2UnknownFieldSetSchema() {
    return PROTO2_UNKNOWN_FIELD_SET_SCHEMA;
  }
  
  public static UnknownFieldSchema<?, ?> proto3UnknownFieldSetSchema() {
    return PROTO3_UNKNOWN_FIELD_SET_SCHEMA;
  }
  
  public static UnknownFieldSchema<?, ?> unknownFieldSetLiteSchema() {
    return UNKNOWN_FIELD_SET_LITE_SCHEMA;
  }
  
  private static UnknownFieldSchema<?, ?> getUnknownFieldSetSchema(boolean paramBoolean) {
    try {
      Class<?> clazz = getUnknownFieldSetSchemaClass();
      return clazz.getConstructor(new Class[] { boolean.class }).newInstance(new Object[] { Boolean.valueOf(paramBoolean) });
    } finally {
      Exception exception = null;
    } 
  }
  
  private static Class<?> getGeneratedMessageClass() {
    try {
      return Class.forName("com.android.framework.protobuf.GeneratedMessageV3");
    } finally {
      Exception exception = null;
    } 
  }
  
  private static Class<?> getUnknownFieldSetSchemaClass() {
    try {
      return Class.forName("com.android.framework.protobuf.UnknownFieldSetSchema");
    } finally {
      Exception exception = null;
    } 
  }
  
  static Object getMapDefaultEntry(Class<?> paramClass, String paramString) {
    try {
      StringBuilder stringBuilder1 = new StringBuilder();
      this();
      stringBuilder1.append(paramClass.getName());
      stringBuilder1.append("$");
      stringBuilder1.append(toCamelCase(paramString, true));
      stringBuilder1.append("DefaultEntryHolder");
      Class<?> clazz = Class.forName(stringBuilder1.toString());
      Field[] arrayOfField = clazz.getDeclaredFields();
      if (arrayOfField.length == 1)
        return UnsafeUtil.getStaticObject(arrayOfField[0]); 
      IllegalStateException illegalStateException = new IllegalStateException();
      StringBuilder stringBuilder2 = new StringBuilder();
      this();
      stringBuilder2.append("Unable to look up map field default entry holder class for ");
      stringBuilder2.append(paramString);
      stringBuilder2.append(" in ");
      stringBuilder2.append(paramClass.getName());
      this(stringBuilder2.toString());
      throw illegalStateException;
    } finally {
      paramClass = null;
    } 
  }
  
  static String toCamelCase(String paramString, boolean paramBoolean) {
    StringBuilder stringBuilder = new StringBuilder();
    for (byte b = 0; b < paramString.length(); b++) {
      char c = paramString.charAt(b);
      if ('a' <= c && c <= 'z') {
        if (paramBoolean) {
          stringBuilder.append((char)(c - 32));
        } else {
          stringBuilder.append(c);
        } 
        paramBoolean = false;
      } else if ('A' <= c && c <= 'Z') {
        if (b == 0 && !paramBoolean) {
          stringBuilder.append((char)(c + 32));
        } else {
          stringBuilder.append(c);
        } 
        paramBoolean = false;
      } else if ('0' <= c && c <= '9') {
        stringBuilder.append(c);
        paramBoolean = true;
      } else {
        paramBoolean = true;
      } 
    } 
    return stringBuilder.toString();
  }
  
  static boolean safeEquals(Object paramObject1, Object paramObject2) {
    return (paramObject1 == paramObject2 || (paramObject1 != null && paramObject1.equals(paramObject2)));
  }
  
  static <T> void mergeMap(MapFieldSchema paramMapFieldSchema, T paramT1, T paramT2, long paramLong) {
    Object object2 = UnsafeUtil.getObject(paramT1, paramLong);
    paramT2 = (T)UnsafeUtil.getObject(paramT2, paramLong);
    Object object1 = paramMapFieldSchema.mergeFrom(object2, paramT2);
    UnsafeUtil.putObject(paramT1, paramLong, object1);
  }
  
  static <T, FT extends FieldSet.FieldDescriptorLite<FT>> void mergeExtensions(ExtensionSchema<FT> paramExtensionSchema, T paramT1, T paramT2) {
    FieldSet<FT> fieldSet = paramExtensionSchema.getExtensions(paramT2);
    if (!fieldSet.isEmpty()) {
      FieldSet<FT> fieldSet1 = paramExtensionSchema.getMutableExtensions(paramT1);
      fieldSet1.mergeFrom(fieldSet);
    } 
  }
  
  static <T, UT, UB> void mergeUnknownFields(UnknownFieldSchema<UT, UB> paramUnknownFieldSchema, T paramT1, T paramT2) {
    UT uT = paramUnknownFieldSchema.getFromMessage(paramT1);
    paramT2 = (T)paramUnknownFieldSchema.getFromMessage(paramT2);
    paramT2 = (T)paramUnknownFieldSchema.merge(uT, (UT)paramT2);
    paramUnknownFieldSchema.setToMessage(paramT1, (UT)paramT2);
  }
  
  static <UT, UB> UB filterUnknownEnumList(int paramInt, List<Integer> paramList, Internal.EnumLiteMap<?> paramEnumLiteMap, UB paramUB, UnknownFieldSchema<UT, UB> paramUnknownFieldSchema) {
    UB uB;
    if (paramEnumLiteMap == null)
      return paramUB; 
    if (paramList instanceof java.util.RandomAccess) {
      byte b1 = 0;
      int i = paramList.size();
      for (byte b2 = 0; b2 < i; b2++) {
        int j = ((Integer)paramList.get(b2)).intValue();
        if (paramEnumLiteMap.findValueByNumber(j) != null) {
          if (b2 != b1)
            paramList.set(b1, Integer.valueOf(j)); 
          b1++;
        } else {
          paramUB = storeUnknownEnum(paramInt, j, paramUB, paramUnknownFieldSchema);
        } 
      } 
      if (b1 != i)
        paramList.subList(b1, i).clear(); 
      uB = paramUB;
    } else {
      Iterator<Integer> iterator = uB.iterator();
      while (true) {
        uB = paramUB;
        if (iterator.hasNext()) {
          int i = ((Integer)iterator.next()).intValue();
          uB = paramUB;
          if (paramEnumLiteMap.findValueByNumber(i) == null) {
            uB = storeUnknownEnum(paramInt, i, paramUB, paramUnknownFieldSchema);
            iterator.remove();
          } 
          paramUB = uB;
          continue;
        } 
        break;
      } 
    } 
    return uB;
  }
  
  static <UT, UB> UB filterUnknownEnumList(int paramInt, List<Integer> paramList, Internal.EnumVerifier paramEnumVerifier, UB paramUB, UnknownFieldSchema<UT, UB> paramUnknownFieldSchema) {
    UB uB;
    if (paramEnumVerifier == null)
      return paramUB; 
    if (paramList instanceof java.util.RandomAccess) {
      byte b1 = 0;
      int i = paramList.size();
      for (byte b2 = 0; b2 < i; b2++) {
        int j = ((Integer)paramList.get(b2)).intValue();
        if (paramEnumVerifier.isInRange(j)) {
          if (b2 != b1)
            paramList.set(b1, Integer.valueOf(j)); 
          b1++;
        } else {
          paramUB = storeUnknownEnum(paramInt, j, paramUB, paramUnknownFieldSchema);
        } 
      } 
      if (b1 != i)
        paramList.subList(b1, i).clear(); 
      uB = paramUB;
    } else {
      Iterator<Integer> iterator = uB.iterator();
      while (true) {
        uB = paramUB;
        if (iterator.hasNext()) {
          int i = ((Integer)iterator.next()).intValue();
          uB = paramUB;
          if (!paramEnumVerifier.isInRange(i)) {
            uB = storeUnknownEnum(paramInt, i, paramUB, paramUnknownFieldSchema);
            iterator.remove();
          } 
          paramUB = uB;
          continue;
        } 
        break;
      } 
    } 
    return uB;
  }
  
  static <UT, UB> UB storeUnknownEnum(int paramInt1, int paramInt2, UB paramUB, UnknownFieldSchema<UT, UB> paramUnknownFieldSchema) {
    UB uB = paramUB;
    if (paramUB == null)
      uB = paramUnknownFieldSchema.newBuilder(); 
    paramUnknownFieldSchema.addVarint(uB, paramInt1, paramInt2);
    return uB;
  }
}
