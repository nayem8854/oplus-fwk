package com.android.framework.protobuf;

import java.util.Collections;
import java.util.List;

public class UninitializedMessageException extends RuntimeException {
  private static final long serialVersionUID = -7466929953374883507L;
  
  private final List<String> missingFields;
  
  public UninitializedMessageException(MessageLite paramMessageLite) {
    super("Message was missing required fields.  (Lite runtime could not determine which fields were missing).");
    this.missingFields = null;
  }
  
  public UninitializedMessageException(List<String> paramList) {
    super(buildDescription(paramList));
    this.missingFields = paramList;
  }
  
  public List<String> getMissingFields() {
    return Collections.unmodifiableList(this.missingFields);
  }
  
  public InvalidProtocolBufferException asInvalidProtocolBufferException() {
    return new InvalidProtocolBufferException(getMessage());
  }
  
  private static String buildDescription(List<String> paramList) {
    StringBuilder stringBuilder = new StringBuilder("Message missing required fields: ");
    boolean bool = true;
    for (String str : paramList) {
      if (bool) {
        bool = false;
      } else {
        stringBuilder.append(", ");
      } 
      stringBuilder.append(str);
    } 
    return stringBuilder.toString();
  }
}
