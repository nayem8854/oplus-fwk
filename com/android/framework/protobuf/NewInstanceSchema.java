package com.android.framework.protobuf;

interface NewInstanceSchema {
  Object newInstance(Object paramObject);
}
