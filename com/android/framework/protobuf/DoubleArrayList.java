package com.android.framework.protobuf;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

final class DoubleArrayList extends AbstractProtobufList<Double> implements Internal.DoubleList, RandomAccess, PrimitiveNonBoxingCollection {
  private static final DoubleArrayList EMPTY_LIST;
  
  private double[] array;
  
  private int size;
  
  static {
    DoubleArrayList doubleArrayList = new DoubleArrayList(new double[0], 0);
    doubleArrayList.makeImmutable();
  }
  
  public static DoubleArrayList emptyList() {
    return EMPTY_LIST;
  }
  
  DoubleArrayList() {
    this(new double[10], 0);
  }
  
  private DoubleArrayList(double[] paramArrayOfdouble, int paramInt) {
    this.array = paramArrayOfdouble;
    this.size = paramInt;
  }
  
  protected void removeRange(int paramInt1, int paramInt2) {
    ensureIsMutable();
    if (paramInt2 >= paramInt1) {
      double[] arrayOfDouble = this.array;
      System.arraycopy(arrayOfDouble, paramInt2, arrayOfDouble, paramInt1, this.size - paramInt2);
      this.size -= paramInt2 - paramInt1;
      this.modCount++;
      return;
    } 
    throw new IndexOutOfBoundsException("toIndex < fromIndex");
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof DoubleArrayList))
      return super.equals(paramObject); 
    paramObject = paramObject;
    if (this.size != ((DoubleArrayList)paramObject).size)
      return false; 
    paramObject = ((DoubleArrayList)paramObject).array;
    for (byte b = 0; b < this.size; b++) {
      if (Double.doubleToLongBits(this.array[b]) != Double.doubleToLongBits(paramObject[b]))
        return false; 
    } 
    return true;
  }
  
  public int hashCode() {
    int i = 1;
    for (byte b = 0; b < this.size; b++) {
      long l = Double.doubleToLongBits(this.array[b]);
      i = i * 31 + Internal.hashLong(l);
    } 
    return i;
  }
  
  public Internal.DoubleList mutableCopyWithCapacity(int paramInt) {
    if (paramInt >= this.size)
      return new DoubleArrayList(Arrays.copyOf(this.array, paramInt), this.size); 
    throw new IllegalArgumentException();
  }
  
  public Double get(int paramInt) {
    return Double.valueOf(getDouble(paramInt));
  }
  
  public double getDouble(int paramInt) {
    ensureIndexInRange(paramInt);
    return this.array[paramInt];
  }
  
  public int size() {
    return this.size;
  }
  
  public Double set(int paramInt, Double paramDouble) {
    return Double.valueOf(setDouble(paramInt, paramDouble.doubleValue()));
  }
  
  public double setDouble(int paramInt, double paramDouble) {
    ensureIsMutable();
    ensureIndexInRange(paramInt);
    double arrayOfDouble[] = this.array, d = arrayOfDouble[paramInt];
    arrayOfDouble[paramInt] = paramDouble;
    return d;
  }
  
  public void add(int paramInt, Double paramDouble) {
    addDouble(paramInt, paramDouble.doubleValue());
  }
  
  public void addDouble(double paramDouble) {
    addDouble(this.size, paramDouble);
  }
  
  private void addDouble(int paramInt, double paramDouble) {
    ensureIsMutable();
    if (paramInt >= 0) {
      int i = this.size;
      if (paramInt <= i) {
        double[] arrayOfDouble = this.array;
        if (i < arrayOfDouble.length) {
          System.arraycopy(arrayOfDouble, paramInt, arrayOfDouble, paramInt + 1, i - paramInt);
        } else {
          i = i * 3 / 2;
          double[] arrayOfDouble1 = new double[i + 1];
          System.arraycopy(arrayOfDouble, 0, arrayOfDouble1, 0, paramInt);
          System.arraycopy(this.array, paramInt, arrayOfDouble1, paramInt + 1, this.size - paramInt);
          this.array = arrayOfDouble1;
        } 
        this.array[paramInt] = paramDouble;
        this.size++;
        this.modCount++;
        return;
      } 
    } 
    throw new IndexOutOfBoundsException(makeOutOfBoundsExceptionMessage(paramInt));
  }
  
  public boolean addAll(Collection<? extends Double> paramCollection) {
    ensureIsMutable();
    Internal.checkNotNull(paramCollection);
    if (!(paramCollection instanceof DoubleArrayList))
      return super.addAll(paramCollection); 
    DoubleArrayList doubleArrayList = (DoubleArrayList)paramCollection;
    int i = doubleArrayList.size;
    if (i == 0)
      return false; 
    int j = this.size;
    if (Integer.MAX_VALUE - j >= i) {
      j += i;
      double[] arrayOfDouble = this.array;
      if (j > arrayOfDouble.length)
        this.array = Arrays.copyOf(arrayOfDouble, j); 
      System.arraycopy(doubleArrayList.array, 0, this.array, this.size, doubleArrayList.size);
      this.size = j;
      this.modCount++;
      return true;
    } 
    throw new OutOfMemoryError();
  }
  
  public boolean remove(Object paramObject) {
    ensureIsMutable();
    for (byte b = 0; b < this.size; b++) {
      if (paramObject.equals(Double.valueOf(this.array[b]))) {
        paramObject = this.array;
        System.arraycopy(paramObject, b + 1, paramObject, b, this.size - b - 1);
        this.size--;
        this.modCount++;
        return true;
      } 
    } 
    return false;
  }
  
  public Double remove(int paramInt) {
    ensureIsMutable();
    ensureIndexInRange(paramInt);
    double arrayOfDouble[] = this.array, d = arrayOfDouble[paramInt];
    int i = this.size;
    if (paramInt < i - 1)
      System.arraycopy(arrayOfDouble, paramInt + 1, arrayOfDouble, paramInt, i - paramInt - 1); 
    this.size--;
    this.modCount++;
    return Double.valueOf(d);
  }
  
  private void ensureIndexInRange(int paramInt) {
    if (paramInt >= 0 && paramInt < this.size)
      return; 
    throw new IndexOutOfBoundsException(makeOutOfBoundsExceptionMessage(paramInt));
  }
  
  private String makeOutOfBoundsExceptionMessage(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Index:");
    stringBuilder.append(paramInt);
    stringBuilder.append(", Size:");
    stringBuilder.append(this.size);
    return stringBuilder.toString();
  }
}
