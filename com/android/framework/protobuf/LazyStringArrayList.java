package com.android.framework.protobuf;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;

public class LazyStringArrayList extends AbstractProtobufList<String> implements LazyStringList, RandomAccess {
  static {
    LazyStringArrayList lazyStringArrayList = new LazyStringArrayList();
    lazyStringArrayList.makeImmutable();
  }
  
  static LazyStringArrayList emptyList() {
    return EMPTY_LIST;
  }
  
  public static final LazyStringList EMPTY = EMPTY_LIST;
  
  private static final LazyStringArrayList EMPTY_LIST;
  
  private final List<Object> list;
  
  public LazyStringArrayList() {
    this(10);
  }
  
  public LazyStringArrayList(int paramInt) {
    this(new ArrayList(paramInt));
  }
  
  public LazyStringArrayList(LazyStringList paramLazyStringList) {
    this.list = new ArrayList(paramLazyStringList.size());
    addAll(paramLazyStringList);
  }
  
  public LazyStringArrayList(List<String> paramList) {
    this(new ArrayList(paramList));
  }
  
  private LazyStringArrayList(ArrayList<Object> paramArrayList) {
    this.list = paramArrayList;
  }
  
  public LazyStringArrayList mutableCopyWithCapacity(int paramInt) {
    if (paramInt >= size()) {
      ArrayList<Object> arrayList = new ArrayList(paramInt);
      arrayList.addAll(this.list);
      return new LazyStringArrayList(arrayList);
    } 
    throw new IllegalArgumentException();
  }
  
  public String get(int paramInt) {
    Object object = this.list.get(paramInt);
    if (object instanceof String)
      return (String)object; 
    if (object instanceof ByteString) {
      ByteString byteString = (ByteString)object;
      object = byteString.toStringUtf8();
      if (byteString.isValidUtf8())
        this.list.set(paramInt, object); 
      return (String)object;
    } 
    object = object;
    String str = Internal.toStringUtf8((byte[])object);
    if (Internal.isValidUtf8((byte[])object))
      this.list.set(paramInt, str); 
    return str;
  }
  
  public int size() {
    return this.list.size();
  }
  
  public String set(int paramInt, String paramString) {
    ensureIsMutable();
    paramString = (String)this.list.set(paramInt, paramString);
    return asString(paramString);
  }
  
  public void add(int paramInt, String paramString) {
    ensureIsMutable();
    this.list.add(paramInt, paramString);
    this.modCount++;
  }
  
  private void add(int paramInt, ByteString paramByteString) {
    ensureIsMutable();
    this.list.add(paramInt, paramByteString);
    this.modCount++;
  }
  
  private void add(int paramInt, byte[] paramArrayOfbyte) {
    ensureIsMutable();
    this.list.add(paramInt, paramArrayOfbyte);
    this.modCount++;
  }
  
  public boolean addAll(Collection<? extends String> paramCollection) {
    return addAll(size(), paramCollection);
  }
  
  public boolean addAll(int paramInt, Collection<? extends String> paramCollection) {
    ensureIsMutable();
    if (paramCollection instanceof LazyStringList)
      paramCollection = (Collection)((LazyStringList)paramCollection).getUnderlyingElements(); 
    boolean bool = this.list.addAll(paramInt, paramCollection);
    this.modCount++;
    return bool;
  }
  
  public boolean addAllByteString(Collection<? extends ByteString> paramCollection) {
    ensureIsMutable();
    boolean bool = this.list.addAll(paramCollection);
    this.modCount++;
    return bool;
  }
  
  public boolean addAllByteArray(Collection<byte[]> paramCollection) {
    ensureIsMutable();
    boolean bool = this.list.addAll(paramCollection);
    this.modCount++;
    return bool;
  }
  
  public String remove(int paramInt) {
    ensureIsMutable();
    Object object = this.list.remove(paramInt);
    this.modCount++;
    return asString(object);
  }
  
  public void clear() {
    ensureIsMutable();
    this.list.clear();
    this.modCount++;
  }
  
  public void add(ByteString paramByteString) {
    ensureIsMutable();
    this.list.add(paramByteString);
    this.modCount++;
  }
  
  public void add(byte[] paramArrayOfbyte) {
    ensureIsMutable();
    this.list.add(paramArrayOfbyte);
    this.modCount++;
  }
  
  public Object getRaw(int paramInt) {
    return this.list.get(paramInt);
  }
  
  public ByteString getByteString(int paramInt) {
    Object object = this.list.get(paramInt);
    ByteString byteString = asByteString(object);
    if (byteString != object)
      this.list.set(paramInt, byteString); 
    return byteString;
  }
  
  public byte[] getByteArray(int paramInt) {
    Object object = this.list.get(paramInt);
    byte[] arrayOfByte = asByteArray(object);
    if (arrayOfByte != object)
      this.list.set(paramInt, arrayOfByte); 
    return arrayOfByte;
  }
  
  public void set(int paramInt, ByteString paramByteString) {
    setAndReturn(paramInt, paramByteString);
  }
  
  private Object setAndReturn(int paramInt, ByteString paramByteString) {
    ensureIsMutable();
    return this.list.set(paramInt, paramByteString);
  }
  
  public void set(int paramInt, byte[] paramArrayOfbyte) {
    setAndReturn(paramInt, paramArrayOfbyte);
  }
  
  private Object setAndReturn(int paramInt, byte[] paramArrayOfbyte) {
    ensureIsMutable();
    return this.list.set(paramInt, paramArrayOfbyte);
  }
  
  private static String asString(Object paramObject) {
    if (paramObject instanceof String)
      return (String)paramObject; 
    if (paramObject instanceof ByteString)
      return ((ByteString)paramObject).toStringUtf8(); 
    return Internal.toStringUtf8((byte[])paramObject);
  }
  
  private static ByteString asByteString(Object paramObject) {
    if (paramObject instanceof ByteString)
      return (ByteString)paramObject; 
    if (paramObject instanceof String)
      return ByteString.copyFromUtf8((String)paramObject); 
    return ByteString.copyFrom((byte[])paramObject);
  }
  
  private static byte[] asByteArray(Object paramObject) {
    if (paramObject instanceof byte[])
      return (byte[])paramObject; 
    if (paramObject instanceof String)
      return Internal.toByteArray((String)paramObject); 
    return ((ByteString)paramObject).toByteArray();
  }
  
  public List<?> getUnderlyingElements() {
    return Collections.unmodifiableList(this.list);
  }
  
  public void mergeFrom(LazyStringList paramLazyStringList) {
    ensureIsMutable();
    for (Object object : paramLazyStringList.getUnderlyingElements()) {
      if (object instanceof byte[]) {
        object = object;
        this.list.add(Arrays.copyOf((byte[])object, object.length));
        continue;
      } 
      this.list.add(object);
    } 
  }
  
  class ByteArrayListView extends AbstractList<byte[]> implements RandomAccess {
    private final LazyStringArrayList list;
    
    ByteArrayListView(LazyStringArrayList this$0) {
      this.list = this$0;
    }
    
    public byte[] get(int param1Int) {
      return this.list.getByteArray(param1Int);
    }
    
    public int size() {
      return this.list.size();
    }
    
    public byte[] set(int param1Int, byte[] param1ArrayOfbyte) {
      Object object = this.list.setAndReturn(param1Int, param1ArrayOfbyte);
      this.modCount++;
      return LazyStringArrayList.asByteArray(object);
    }
    
    public void add(int param1Int, byte[] param1ArrayOfbyte) {
      this.list.add(param1Int, param1ArrayOfbyte);
      this.modCount++;
    }
    
    public byte[] remove(int param1Int) {
      String str = this.list.remove(param1Int);
      this.modCount++;
      return LazyStringArrayList.asByteArray(str);
    }
  }
  
  public List<byte[]> asByteArrayList() {
    return new ByteArrayListView(this);
  }
  
  class ByteStringListView extends AbstractList<ByteString> implements RandomAccess {
    private final LazyStringArrayList list;
    
    ByteStringListView(LazyStringArrayList this$0) {
      this.list = this$0;
    }
    
    public ByteString get(int param1Int) {
      return this.list.getByteString(param1Int);
    }
    
    public int size() {
      return this.list.size();
    }
    
    public ByteString set(int param1Int, ByteString param1ByteString) {
      Object object = this.list.setAndReturn(param1Int, param1ByteString);
      this.modCount++;
      return LazyStringArrayList.asByteString(object);
    }
    
    public void add(int param1Int, ByteString param1ByteString) {
      this.list.add(param1Int, param1ByteString);
      this.modCount++;
    }
    
    public ByteString remove(int param1Int) {
      String str = this.list.remove(param1Int);
      this.modCount++;
      return LazyStringArrayList.asByteString(str);
    }
  }
  
  public List<ByteString> asByteStringList() {
    return new ByteStringListView(this);
  }
  
  public LazyStringList getUnmodifiableView() {
    if (isModifiable())
      return new UnmodifiableLazyStringList(this); 
    return this;
  }
}
