package com.android.framework.protobuf;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

final class CodedOutputStreamWriter implements Writer {
  private final CodedOutputStream output;
  
  public static CodedOutputStreamWriter forCodedOutput(CodedOutputStream paramCodedOutputStream) {
    if (paramCodedOutputStream.wrapper != null)
      return paramCodedOutputStream.wrapper; 
    return new CodedOutputStreamWriter(paramCodedOutputStream);
  }
  
  private CodedOutputStreamWriter(CodedOutputStream paramCodedOutputStream) {
    this.output = paramCodedOutputStream = Internal.<CodedOutputStream>checkNotNull(paramCodedOutputStream, "output");
    paramCodedOutputStream.wrapper = this;
  }
  
  public Writer.FieldOrder fieldOrder() {
    return Writer.FieldOrder.ASCENDING;
  }
  
  public int getTotalBytesWritten() {
    return this.output.getTotalBytesWritten();
  }
  
  public void writeSFixed32(int paramInt1, int paramInt2) throws IOException {
    this.output.writeSFixed32(paramInt1, paramInt2);
  }
  
  public void writeInt64(int paramInt, long paramLong) throws IOException {
    this.output.writeInt64(paramInt, paramLong);
  }
  
  public void writeSFixed64(int paramInt, long paramLong) throws IOException {
    this.output.writeSFixed64(paramInt, paramLong);
  }
  
  public void writeFloat(int paramInt, float paramFloat) throws IOException {
    this.output.writeFloat(paramInt, paramFloat);
  }
  
  public void writeDouble(int paramInt, double paramDouble) throws IOException {
    this.output.writeDouble(paramInt, paramDouble);
  }
  
  public void writeEnum(int paramInt1, int paramInt2) throws IOException {
    this.output.writeEnum(paramInt1, paramInt2);
  }
  
  public void writeUInt64(int paramInt, long paramLong) throws IOException {
    this.output.writeUInt64(paramInt, paramLong);
  }
  
  public void writeInt32(int paramInt1, int paramInt2) throws IOException {
    this.output.writeInt32(paramInt1, paramInt2);
  }
  
  public void writeFixed64(int paramInt, long paramLong) throws IOException {
    this.output.writeFixed64(paramInt, paramLong);
  }
  
  public void writeFixed32(int paramInt1, int paramInt2) throws IOException {
    this.output.writeFixed32(paramInt1, paramInt2);
  }
  
  public void writeBool(int paramInt, boolean paramBoolean) throws IOException {
    this.output.writeBool(paramInt, paramBoolean);
  }
  
  public void writeString(int paramInt, String paramString) throws IOException {
    this.output.writeString(paramInt, paramString);
  }
  
  public void writeBytes(int paramInt, ByteString paramByteString) throws IOException {
    this.output.writeBytes(paramInt, paramByteString);
  }
  
  public void writeUInt32(int paramInt1, int paramInt2) throws IOException {
    this.output.writeUInt32(paramInt1, paramInt2);
  }
  
  public void writeSInt32(int paramInt1, int paramInt2) throws IOException {
    this.output.writeSInt32(paramInt1, paramInt2);
  }
  
  public void writeSInt64(int paramInt, long paramLong) throws IOException {
    this.output.writeSInt64(paramInt, paramLong);
  }
  
  public void writeMessage(int paramInt, Object paramObject) throws IOException {
    this.output.writeMessage(paramInt, (MessageLite)paramObject);
  }
  
  public void writeMessage(int paramInt, Object paramObject, Schema paramSchema) throws IOException {
    this.output.writeMessage(paramInt, (MessageLite)paramObject, paramSchema);
  }
  
  public void writeGroup(int paramInt, Object paramObject) throws IOException {
    this.output.writeGroup(paramInt, (MessageLite)paramObject);
  }
  
  public void writeGroup(int paramInt, Object paramObject, Schema paramSchema) throws IOException {
    this.output.writeGroup(paramInt, (MessageLite)paramObject, paramSchema);
  }
  
  public void writeStartGroup(int paramInt) throws IOException {
    this.output.writeTag(paramInt, 3);
  }
  
  public void writeEndGroup(int paramInt) throws IOException {
    this.output.writeTag(paramInt, 4);
  }
  
  public final void writeMessageSetItem(int paramInt, Object paramObject) throws IOException {
    if (paramObject instanceof ByteString) {
      this.output.writeRawMessageSetExtension(paramInt, (ByteString)paramObject);
    } else {
      this.output.writeMessageSetExtension(paramInt, (MessageLite)paramObject);
    } 
  }
  
  public void writeInt32List(int paramInt, List<Integer> paramList, boolean paramBoolean) throws IOException {
    if (paramBoolean) {
      this.output.writeTag(paramInt, 2);
      int i = 0;
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        i += CodedOutputStream.computeInt32SizeNoTag(((Integer)paramList.get(paramInt)).intValue()); 
      this.output.writeUInt32NoTag(i);
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        this.output.writeInt32NoTag(((Integer)paramList.get(paramInt)).intValue()); 
    } else {
      for (byte b = 0; b < paramList.size(); b++)
        this.output.writeInt32(paramInt, ((Integer)paramList.get(b)).intValue()); 
    } 
  }
  
  public void writeFixed32List(int paramInt, List<Integer> paramList, boolean paramBoolean) throws IOException {
    if (paramBoolean) {
      this.output.writeTag(paramInt, 2);
      int i = 0;
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        i += CodedOutputStream.computeFixed32SizeNoTag(((Integer)paramList.get(paramInt)).intValue()); 
      this.output.writeUInt32NoTag(i);
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        this.output.writeFixed32NoTag(((Integer)paramList.get(paramInt)).intValue()); 
    } else {
      for (byte b = 0; b < paramList.size(); b++)
        this.output.writeFixed32(paramInt, ((Integer)paramList.get(b)).intValue()); 
    } 
  }
  
  public void writeInt64List(int paramInt, List<Long> paramList, boolean paramBoolean) throws IOException {
    if (paramBoolean) {
      this.output.writeTag(paramInt, 2);
      int i = 0;
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        i += CodedOutputStream.computeInt64SizeNoTag(((Long)paramList.get(paramInt)).longValue()); 
      this.output.writeUInt32NoTag(i);
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        this.output.writeInt64NoTag(((Long)paramList.get(paramInt)).longValue()); 
    } else {
      for (byte b = 0; b < paramList.size(); b++)
        this.output.writeInt64(paramInt, ((Long)paramList.get(b)).longValue()); 
    } 
  }
  
  public void writeUInt64List(int paramInt, List<Long> paramList, boolean paramBoolean) throws IOException {
    if (paramBoolean) {
      this.output.writeTag(paramInt, 2);
      int i = 0;
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        i += CodedOutputStream.computeUInt64SizeNoTag(((Long)paramList.get(paramInt)).longValue()); 
      this.output.writeUInt32NoTag(i);
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        this.output.writeUInt64NoTag(((Long)paramList.get(paramInt)).longValue()); 
    } else {
      for (byte b = 0; b < paramList.size(); b++)
        this.output.writeUInt64(paramInt, ((Long)paramList.get(b)).longValue()); 
    } 
  }
  
  public void writeFixed64List(int paramInt, List<Long> paramList, boolean paramBoolean) throws IOException {
    if (paramBoolean) {
      this.output.writeTag(paramInt, 2);
      int i = 0;
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        i += CodedOutputStream.computeFixed64SizeNoTag(((Long)paramList.get(paramInt)).longValue()); 
      this.output.writeUInt32NoTag(i);
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        this.output.writeFixed64NoTag(((Long)paramList.get(paramInt)).longValue()); 
    } else {
      for (byte b = 0; b < paramList.size(); b++)
        this.output.writeFixed64(paramInt, ((Long)paramList.get(b)).longValue()); 
    } 
  }
  
  public void writeFloatList(int paramInt, List<Float> paramList, boolean paramBoolean) throws IOException {
    if (paramBoolean) {
      this.output.writeTag(paramInt, 2);
      int i = 0;
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        i += CodedOutputStream.computeFloatSizeNoTag(((Float)paramList.get(paramInt)).floatValue()); 
      this.output.writeUInt32NoTag(i);
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        this.output.writeFloatNoTag(((Float)paramList.get(paramInt)).floatValue()); 
    } else {
      for (byte b = 0; b < paramList.size(); b++)
        this.output.writeFloat(paramInt, ((Float)paramList.get(b)).floatValue()); 
    } 
  }
  
  public void writeDoubleList(int paramInt, List<Double> paramList, boolean paramBoolean) throws IOException {
    if (paramBoolean) {
      this.output.writeTag(paramInt, 2);
      int i = 0;
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        i += CodedOutputStream.computeDoubleSizeNoTag(((Double)paramList.get(paramInt)).doubleValue()); 
      this.output.writeUInt32NoTag(i);
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        this.output.writeDoubleNoTag(((Double)paramList.get(paramInt)).doubleValue()); 
    } else {
      for (byte b = 0; b < paramList.size(); b++)
        this.output.writeDouble(paramInt, ((Double)paramList.get(b)).doubleValue()); 
    } 
  }
  
  public void writeEnumList(int paramInt, List<Integer> paramList, boolean paramBoolean) throws IOException {
    if (paramBoolean) {
      this.output.writeTag(paramInt, 2);
      int i = 0;
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        i += CodedOutputStream.computeEnumSizeNoTag(((Integer)paramList.get(paramInt)).intValue()); 
      this.output.writeUInt32NoTag(i);
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        this.output.writeEnumNoTag(((Integer)paramList.get(paramInt)).intValue()); 
    } else {
      for (byte b = 0; b < paramList.size(); b++)
        this.output.writeEnum(paramInt, ((Integer)paramList.get(b)).intValue()); 
    } 
  }
  
  public void writeBoolList(int paramInt, List<Boolean> paramList, boolean paramBoolean) throws IOException {
    if (paramBoolean) {
      this.output.writeTag(paramInt, 2);
      int i = 0;
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        i += CodedOutputStream.computeBoolSizeNoTag(((Boolean)paramList.get(paramInt)).booleanValue()); 
      this.output.writeUInt32NoTag(i);
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        this.output.writeBoolNoTag(((Boolean)paramList.get(paramInt)).booleanValue()); 
    } else {
      for (byte b = 0; b < paramList.size(); b++)
        this.output.writeBool(paramInt, ((Boolean)paramList.get(b)).booleanValue()); 
    } 
  }
  
  public void writeStringList(int paramInt, List<String> paramList) throws IOException {
    if (paramList instanceof LazyStringList) {
      LazyStringList lazyStringList = (LazyStringList)paramList;
      for (byte b = 0; b < paramList.size(); b++)
        writeLazyString(paramInt, lazyStringList.getRaw(b)); 
    } else {
      for (byte b = 0; b < paramList.size(); b++)
        this.output.writeString(paramInt, paramList.get(b)); 
    } 
  }
  
  private void writeLazyString(int paramInt, Object paramObject) throws IOException {
    if (paramObject instanceof String) {
      this.output.writeString(paramInt, (String)paramObject);
    } else {
      this.output.writeBytes(paramInt, (ByteString)paramObject);
    } 
  }
  
  public void writeBytesList(int paramInt, List<ByteString> paramList) throws IOException {
    for (byte b = 0; b < paramList.size(); b++)
      this.output.writeBytes(paramInt, paramList.get(b)); 
  }
  
  public void writeUInt32List(int paramInt, List<Integer> paramList, boolean paramBoolean) throws IOException {
    if (paramBoolean) {
      this.output.writeTag(paramInt, 2);
      int i = 0;
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        i += CodedOutputStream.computeUInt32SizeNoTag(((Integer)paramList.get(paramInt)).intValue()); 
      this.output.writeUInt32NoTag(i);
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        this.output.writeUInt32NoTag(((Integer)paramList.get(paramInt)).intValue()); 
    } else {
      for (byte b = 0; b < paramList.size(); b++)
        this.output.writeUInt32(paramInt, ((Integer)paramList.get(b)).intValue()); 
    } 
  }
  
  public void writeSFixed32List(int paramInt, List<Integer> paramList, boolean paramBoolean) throws IOException {
    if (paramBoolean) {
      this.output.writeTag(paramInt, 2);
      int i = 0;
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        i += CodedOutputStream.computeSFixed32SizeNoTag(((Integer)paramList.get(paramInt)).intValue()); 
      this.output.writeUInt32NoTag(i);
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        this.output.writeSFixed32NoTag(((Integer)paramList.get(paramInt)).intValue()); 
    } else {
      for (byte b = 0; b < paramList.size(); b++)
        this.output.writeSFixed32(paramInt, ((Integer)paramList.get(b)).intValue()); 
    } 
  }
  
  public void writeSFixed64List(int paramInt, List<Long> paramList, boolean paramBoolean) throws IOException {
    if (paramBoolean) {
      this.output.writeTag(paramInt, 2);
      int i = 0;
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        i += CodedOutputStream.computeSFixed64SizeNoTag(((Long)paramList.get(paramInt)).longValue()); 
      this.output.writeUInt32NoTag(i);
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        this.output.writeSFixed64NoTag(((Long)paramList.get(paramInt)).longValue()); 
    } else {
      for (byte b = 0; b < paramList.size(); b++)
        this.output.writeSFixed64(paramInt, ((Long)paramList.get(b)).longValue()); 
    } 
  }
  
  public void writeSInt32List(int paramInt, List<Integer> paramList, boolean paramBoolean) throws IOException {
    if (paramBoolean) {
      this.output.writeTag(paramInt, 2);
      int i = 0;
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        i += CodedOutputStream.computeSInt32SizeNoTag(((Integer)paramList.get(paramInt)).intValue()); 
      this.output.writeUInt32NoTag(i);
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        this.output.writeSInt32NoTag(((Integer)paramList.get(paramInt)).intValue()); 
    } else {
      for (byte b = 0; b < paramList.size(); b++)
        this.output.writeSInt32(paramInt, ((Integer)paramList.get(b)).intValue()); 
    } 
  }
  
  public void writeSInt64List(int paramInt, List<Long> paramList, boolean paramBoolean) throws IOException {
    if (paramBoolean) {
      this.output.writeTag(paramInt, 2);
      int i = 0;
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        i += CodedOutputStream.computeSInt64SizeNoTag(((Long)paramList.get(paramInt)).longValue()); 
      this.output.writeUInt32NoTag(i);
      for (paramInt = 0; paramInt < paramList.size(); paramInt++)
        this.output.writeSInt64NoTag(((Long)paramList.get(paramInt)).longValue()); 
    } else {
      for (byte b = 0; b < paramList.size(); b++)
        this.output.writeSInt64(paramInt, ((Long)paramList.get(b)).longValue()); 
    } 
  }
  
  public void writeMessageList(int paramInt, List<?> paramList) throws IOException {
    for (byte b = 0; b < paramList.size(); b++)
      writeMessage(paramInt, paramList.get(b)); 
  }
  
  public void writeMessageList(int paramInt, List<?> paramList, Schema paramSchema) throws IOException {
    for (byte b = 0; b < paramList.size(); b++)
      writeMessage(paramInt, paramList.get(b), paramSchema); 
  }
  
  public void writeGroupList(int paramInt, List<?> paramList) throws IOException {
    for (byte b = 0; b < paramList.size(); b++)
      writeGroup(paramInt, paramList.get(b)); 
  }
  
  public void writeGroupList(int paramInt, List<?> paramList, Schema paramSchema) throws IOException {
    for (byte b = 0; b < paramList.size(); b++)
      writeGroup(paramInt, paramList.get(b), paramSchema); 
  }
  
  public <K, V> void writeMap(int paramInt, MapEntryLite.Metadata<K, V> paramMetadata, Map<K, V> paramMap) throws IOException {
    if (this.output.isSerializationDeterministic()) {
      writeDeterministicMap(paramInt, paramMetadata, paramMap);
      return;
    } 
    for (Map.Entry<K, V> entry : paramMap.entrySet()) {
      this.output.writeTag(paramInt, 2);
      CodedOutputStream codedOutputStream = this.output;
      int i = MapEntryLite.computeSerializedSize(paramMetadata, (K)entry.getKey(), (V)entry.getValue());
      codedOutputStream.writeUInt32NoTag(i);
      MapEntryLite.writeTo(this.output, paramMetadata, (K)entry.getKey(), (V)entry.getValue());
    } 
  }
  
  private <K, V> void writeDeterministicMap(int paramInt, MapEntryLite.Metadata<K, V> paramMetadata, Map<K, V> paramMap) throws IOException {
    switch (null.$SwitchMap$com$google$protobuf$WireFormat$FieldType[paramMetadata.keyType.ordinal()]) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("does not support key type: ");
        stringBuilder.append(paramMetadata.keyType);
        throw new IllegalArgumentException(stringBuilder.toString());
      case 12:
        writeDeterministicStringMap(paramInt, (MapEntryLite.Metadata)paramMetadata, (Map<String, V>)stringBuilder);
        return;
      case 7:
      case 8:
      case 9:
      case 10:
      case 11:
        writeDeterministicLongMap(paramInt, (MapEntryLite.Metadata)paramMetadata, (Map<Long, V>)stringBuilder);
        return;
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
        writeDeterministicIntegerMap(paramInt, (MapEntryLite.Metadata)paramMetadata, (Map<Integer, V>)stringBuilder);
        return;
      case 1:
        break;
    } 
    Object object = stringBuilder.get(Boolean.FALSE);
    if (object != null)
      writeDeterministicBooleanMapEntry(paramInt, false, object, (MapEntryLite.Metadata)paramMetadata); 
    StringBuilder stringBuilder = (StringBuilder)stringBuilder.get(Boolean.TRUE);
    if (stringBuilder != null)
      writeDeterministicBooleanMapEntry(paramInt, true, stringBuilder, (MapEntryLite.Metadata)paramMetadata); 
  }
  
  private <V> void writeDeterministicBooleanMapEntry(int paramInt, boolean paramBoolean, V paramV, MapEntryLite.Metadata<Boolean, V> paramMetadata) throws IOException {
    this.output.writeTag(paramInt, 2);
    this.output.writeUInt32NoTag(MapEntryLite.computeSerializedSize(paramMetadata, Boolean.valueOf(paramBoolean), paramV));
    MapEntryLite.writeTo(this.output, paramMetadata, Boolean.valueOf(paramBoolean), paramV);
  }
  
  private <V> void writeDeterministicIntegerMap(int paramInt, MapEntryLite.Metadata<Integer, V> paramMetadata, Map<Integer, V> paramMap) throws IOException {
    int[] arrayOfInt = new int[paramMap.size()];
    byte b = 0;
    Iterator<Integer> iterator;
    for (iterator = paramMap.keySet().iterator(); iterator.hasNext(); ) {
      int j = ((Integer)iterator.next()).intValue();
      arrayOfInt[b] = j;
      b++;
    } 
    Arrays.sort(arrayOfInt);
    for (int i = arrayOfInt.length; b < i; ) {
      int j = arrayOfInt[b];
      iterator = (Iterator<Integer>)paramMap.get(Integer.valueOf(j));
      this.output.writeTag(paramInt, 2);
      this.output.writeUInt32NoTag(MapEntryLite.computeSerializedSize(paramMetadata, Integer.valueOf(j), (V)iterator));
      MapEntryLite.writeTo(this.output, paramMetadata, Integer.valueOf(j), (V)iterator);
      b++;
    } 
  }
  
  private <V> void writeDeterministicLongMap(int paramInt, MapEntryLite.Metadata<Long, V> paramMetadata, Map<Long, V> paramMap) throws IOException {
    long[] arrayOfLong = new long[paramMap.size()];
    byte b = 0;
    Iterator<Long> iterator;
    for (iterator = paramMap.keySet().iterator(); iterator.hasNext(); ) {
      long l = ((Long)iterator.next()).longValue();
      arrayOfLong[b] = l;
      b++;
    } 
    Arrays.sort(arrayOfLong);
    for (int i = arrayOfLong.length; b < i; ) {
      long l = arrayOfLong[b];
      iterator = (Iterator<Long>)paramMap.get(Long.valueOf(l));
      this.output.writeTag(paramInt, 2);
      this.output.writeUInt32NoTag(MapEntryLite.computeSerializedSize(paramMetadata, Long.valueOf(l), (V)iterator));
      MapEntryLite.writeTo(this.output, paramMetadata, Long.valueOf(l), (V)iterator);
      b++;
    } 
  }
  
  private <V> void writeDeterministicStringMap(int paramInt, MapEntryLite.Metadata<String, V> paramMetadata, Map<String, V> paramMap) throws IOException {
    String[] arrayOfString = new String[paramMap.size()];
    byte b = 0;
    for (String str : paramMap.keySet()) {
      arrayOfString[b] = str;
      b++;
    } 
    Arrays.sort((Object[])arrayOfString);
    for (int i = arrayOfString.length; b < i; ) {
      String str = arrayOfString[b];
      V v = paramMap.get(str);
      this.output.writeTag(paramInt, 2);
      this.output.writeUInt32NoTag(MapEntryLite.computeSerializedSize(paramMetadata, str, v));
      MapEntryLite.writeTo(this.output, paramMetadata, str, v);
      b++;
    } 
  }
}
