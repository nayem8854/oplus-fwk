package com.android.framework.protobuf;

final class NewInstanceSchemaLite implements NewInstanceSchema {
  public Object newInstance(Object paramObject) {
    GeneratedMessageLite generatedMessageLite = (GeneratedMessageLite)paramObject;
    paramObject = GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE;
    return 
      generatedMessageLite.dynamicMethod((GeneratedMessageLite.MethodToInvoke)paramObject);
  }
}
