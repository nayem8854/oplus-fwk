package com.android.framework.protobuf;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

final class LongArrayList extends AbstractProtobufList<Long> implements Internal.LongList, RandomAccess, PrimitiveNonBoxingCollection {
  private static final LongArrayList EMPTY_LIST;
  
  private long[] array;
  
  private int size;
  
  static {
    LongArrayList longArrayList = new LongArrayList(new long[0], 0);
    longArrayList.makeImmutable();
  }
  
  public static LongArrayList emptyList() {
    return EMPTY_LIST;
  }
  
  LongArrayList() {
    this(new long[10], 0);
  }
  
  private LongArrayList(long[] paramArrayOflong, int paramInt) {
    this.array = paramArrayOflong;
    this.size = paramInt;
  }
  
  protected void removeRange(int paramInt1, int paramInt2) {
    ensureIsMutable();
    if (paramInt2 >= paramInt1) {
      long[] arrayOfLong = this.array;
      System.arraycopy(arrayOfLong, paramInt2, arrayOfLong, paramInt1, this.size - paramInt2);
      this.size -= paramInt2 - paramInt1;
      this.modCount++;
      return;
    } 
    throw new IndexOutOfBoundsException("toIndex < fromIndex");
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof LongArrayList))
      return super.equals(paramObject); 
    paramObject = paramObject;
    if (this.size != ((LongArrayList)paramObject).size)
      return false; 
    paramObject = ((LongArrayList)paramObject).array;
    for (byte b = 0; b < this.size; b++) {
      if (this.array[b] != paramObject[b])
        return false; 
    } 
    return true;
  }
  
  public int hashCode() {
    int i = 1;
    for (byte b = 0; b < this.size; b++)
      i = i * 31 + Internal.hashLong(this.array[b]); 
    return i;
  }
  
  public Internal.LongList mutableCopyWithCapacity(int paramInt) {
    if (paramInt >= this.size)
      return new LongArrayList(Arrays.copyOf(this.array, paramInt), this.size); 
    throw new IllegalArgumentException();
  }
  
  public Long get(int paramInt) {
    return Long.valueOf(getLong(paramInt));
  }
  
  public long getLong(int paramInt) {
    ensureIndexInRange(paramInt);
    return this.array[paramInt];
  }
  
  public int size() {
    return this.size;
  }
  
  public Long set(int paramInt, Long paramLong) {
    return Long.valueOf(setLong(paramInt, paramLong.longValue()));
  }
  
  public long setLong(int paramInt, long paramLong) {
    ensureIsMutable();
    ensureIndexInRange(paramInt);
    long arrayOfLong[] = this.array, l = arrayOfLong[paramInt];
    arrayOfLong[paramInt] = paramLong;
    return l;
  }
  
  public void add(int paramInt, Long paramLong) {
    addLong(paramInt, paramLong.longValue());
  }
  
  public void addLong(long paramLong) {
    addLong(this.size, paramLong);
  }
  
  private void addLong(int paramInt, long paramLong) {
    ensureIsMutable();
    if (paramInt >= 0) {
      int i = this.size;
      if (paramInt <= i) {
        long[] arrayOfLong = this.array;
        if (i < arrayOfLong.length) {
          System.arraycopy(arrayOfLong, paramInt, arrayOfLong, paramInt + 1, i - paramInt);
        } else {
          i = i * 3 / 2;
          long[] arrayOfLong1 = new long[i + 1];
          System.arraycopy(arrayOfLong, 0, arrayOfLong1, 0, paramInt);
          System.arraycopy(this.array, paramInt, arrayOfLong1, paramInt + 1, this.size - paramInt);
          this.array = arrayOfLong1;
        } 
        this.array[paramInt] = paramLong;
        this.size++;
        this.modCount++;
        return;
      } 
    } 
    throw new IndexOutOfBoundsException(makeOutOfBoundsExceptionMessage(paramInt));
  }
  
  public boolean addAll(Collection<? extends Long> paramCollection) {
    ensureIsMutable();
    Internal.checkNotNull(paramCollection);
    if (!(paramCollection instanceof LongArrayList))
      return super.addAll(paramCollection); 
    paramCollection = paramCollection;
    int i = ((LongArrayList)paramCollection).size;
    if (i == 0)
      return false; 
    int j = this.size;
    if (Integer.MAX_VALUE - j >= i) {
      j += i;
      long[] arrayOfLong = this.array;
      if (j > arrayOfLong.length)
        this.array = Arrays.copyOf(arrayOfLong, j); 
      System.arraycopy(((LongArrayList)paramCollection).array, 0, this.array, this.size, ((LongArrayList)paramCollection).size);
      this.size = j;
      this.modCount++;
      return true;
    } 
    throw new OutOfMemoryError();
  }
  
  public boolean remove(Object paramObject) {
    ensureIsMutable();
    for (byte b = 0; b < this.size; b++) {
      if (paramObject.equals(Long.valueOf(this.array[b]))) {
        paramObject = this.array;
        System.arraycopy(paramObject, b + 1, paramObject, b, this.size - b - 1);
        this.size--;
        this.modCount++;
        return true;
      } 
    } 
    return false;
  }
  
  public Long remove(int paramInt) {
    ensureIsMutable();
    ensureIndexInRange(paramInt);
    long arrayOfLong[] = this.array, l = arrayOfLong[paramInt];
    int i = this.size;
    if (paramInt < i - 1)
      System.arraycopy(arrayOfLong, paramInt + 1, arrayOfLong, paramInt, i - paramInt - 1); 
    this.size--;
    this.modCount++;
    return Long.valueOf(l);
  }
  
  private void ensureIndexInRange(int paramInt) {
    if (paramInt >= 0 && paramInt < this.size)
      return; 
    throw new IndexOutOfBoundsException(makeOutOfBoundsExceptionMessage(paramInt));
  }
  
  private String makeOutOfBoundsExceptionMessage(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Index:");
    stringBuilder.append(paramInt);
    stringBuilder.append(", Size:");
    stringBuilder.append(this.size);
    return stringBuilder.toString();
  }
}
