package com.android.framework.protobuf;

interface SchemaFactory {
  <T> Schema<T> createSchema(Class<T> paramClass);
}
