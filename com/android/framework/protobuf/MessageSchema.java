package com.android.framework.protobuf;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import sun.misc.Unsafe;

final class MessageSchema<T> implements Schema<T> {
  private static final int[] EMPTY_INT_ARRAY = new int[0];
  
  private static final int ENFORCE_UTF8_MASK = 536870912;
  
  private static final int FIELD_TYPE_MASK = 267386880;
  
  private static final int INTS_PER_FIELD = 3;
  
  private static final int OFFSET_BITS = 20;
  
  private static final int OFFSET_MASK = 1048575;
  
  static final int ONEOF_TYPE_OFFSET = 51;
  
  private static final int REQUIRED_MASK = 268435456;
  
  private static final Unsafe UNSAFE = UnsafeUtil.getUnsafe();
  
  private final int[] buffer;
  
  private final int checkInitializedCount;
  
  private final MessageLite defaultInstance;
  
  private final ExtensionSchema<?> extensionSchema;
  
  private final boolean hasExtensions;
  
  private final int[] intArray;
  
  private final ListFieldSchema listFieldSchema;
  
  private final boolean lite;
  
  private final MapFieldSchema mapFieldSchema;
  
  private final int maxFieldNumber;
  
  private final int minFieldNumber;
  
  private final NewInstanceSchema newInstanceSchema;
  
  private final Object[] objects;
  
  private final boolean proto3;
  
  private final int repeatedFieldOffsetStart;
  
  private final UnknownFieldSchema<?, ?> unknownFieldSchema;
  
  private final boolean useCachedSizeField;
  
  private MessageSchema(int[] paramArrayOfint1, Object[] paramArrayOfObject, int paramInt1, int paramInt2, MessageLite paramMessageLite, boolean paramBoolean1, boolean paramBoolean2, int[] paramArrayOfint2, int paramInt3, int paramInt4, NewInstanceSchema paramNewInstanceSchema, ListFieldSchema paramListFieldSchema, UnknownFieldSchema<?, ?> paramUnknownFieldSchema, ExtensionSchema<?> paramExtensionSchema, MapFieldSchema paramMapFieldSchema) {
    this.buffer = paramArrayOfint1;
    this.objects = paramArrayOfObject;
    this.minFieldNumber = paramInt1;
    this.maxFieldNumber = paramInt2;
    this.lite = paramMessageLite instanceof GeneratedMessageLite;
    this.proto3 = paramBoolean1;
    if (paramExtensionSchema != null && paramExtensionSchema.hasExtensions(paramMessageLite)) {
      paramBoolean1 = true;
    } else {
      paramBoolean1 = false;
    } 
    this.hasExtensions = paramBoolean1;
    this.useCachedSizeField = paramBoolean2;
    this.intArray = paramArrayOfint2;
    this.checkInitializedCount = paramInt3;
    this.repeatedFieldOffsetStart = paramInt4;
    this.newInstanceSchema = paramNewInstanceSchema;
    this.listFieldSchema = paramListFieldSchema;
    this.unknownFieldSchema = paramUnknownFieldSchema;
    this.extensionSchema = paramExtensionSchema;
    this.defaultInstance = paramMessageLite;
    this.mapFieldSchema = paramMapFieldSchema;
  }
  
  static <T> MessageSchema<T> newSchema(Class<T> paramClass, MessageInfo paramMessageInfo, NewInstanceSchema paramNewInstanceSchema, ListFieldSchema paramListFieldSchema, UnknownFieldSchema<?, ?> paramUnknownFieldSchema, ExtensionSchema<?> paramExtensionSchema, MapFieldSchema paramMapFieldSchema) {
    if (paramMessageInfo instanceof RawMessageInfo)
      return newSchemaForRawMessageInfo((RawMessageInfo)paramMessageInfo, paramNewInstanceSchema, paramListFieldSchema, paramUnknownFieldSchema, paramExtensionSchema, paramMapFieldSchema); 
    return newSchemaForMessageInfo((StructuralMessageInfo)paramMessageInfo, paramNewInstanceSchema, paramListFieldSchema, paramUnknownFieldSchema, paramExtensionSchema, paramMapFieldSchema);
  }
  
  static <T> MessageSchema<T> newSchemaForRawMessageInfo(RawMessageInfo paramRawMessageInfo, NewInstanceSchema paramNewInstanceSchema, ListFieldSchema paramListFieldSchema, UnknownFieldSchema<?, ?> paramUnknownFieldSchema, ExtensionSchema<?> paramExtensionSchema, MapFieldSchema paramMapFieldSchema) {
    boolean bool;
    int arrayOfInt1[], i2, i3, i5, i6;
    if (paramRawMessageInfo.getSyntax() == ProtoSyntax.PROTO3) {
      bool = true;
    } else {
      bool = false;
    } 
    String str = paramRawMessageInfo.getStringInfo();
    int i = str.length();
    int j = 0 + 1, k = str.charAt(0);
    int m = k, n = j;
    if (k >= 55296) {
      int i11 = k & 0x1FFF;
      n = 13;
      int i12 = j;
      while (true) {
        j = i12 + 1;
        i12 = str.charAt(i12);
        if (i12 >= 55296) {
          i11 |= (i12 & 0x1FFF) << n;
          n += 13;
          i12 = j;
          continue;
        } 
        break;
      } 
      m = i11 | i12 << n;
      n = j;
    } 
    j = n + 1;
    char c = str.charAt(n);
    k = c;
    n = j;
    if (c >= '?') {
      k = c & 0x1FFF;
      n = 13;
      int i11 = j;
      while (true) {
        j = i11 + 1;
        i11 = str.charAt(i11);
        if (i11 >= 55296) {
          k |= (i11 & 0x1FFF) << n;
          n += 13;
          i11 = j;
          continue;
        } 
        break;
      } 
      k |= i11 << n;
      n = j;
    } 
    if (k == 0) {
      arrayOfInt1 = EMPTY_INT_ARRAY;
      j = 0;
      i2 = 0;
      i3 = 0;
      i4 = 0;
      i5 = 0;
      i6 = 0;
      boolean bool1 = false;
      int i11 = k;
      k = bool1;
    } else {
      j = n + 1;
      c = str.charAt(n);
      n = c;
      k = j;
      if (c >= '?') {
        k = c & 0x1FFF;
        n = 13;
        int i15 = j;
        while (true) {
          j = i15 + 1;
          i15 = str.charAt(i15);
          if (i15 >= 55296) {
            k |= (i15 & 0x1FFF) << n;
            n += 13;
            i15 = j;
            continue;
          } 
          break;
        } 
        n = k | i15 << n;
        k = j;
      } 
      j = k + 1;
      c = str.charAt(k);
      int i13 = c;
      k = j;
      if (c >= '?') {
        int i15 = c & 0x1FFF;
        k = 13;
        int i16 = j;
        while (true) {
          j = i16 + 1;
          i16 = str.charAt(i16);
          if (i16 >= 55296) {
            i15 |= (i16 & 0x1FFF) << k;
            k += 13;
            i16 = j;
            continue;
          } 
          break;
        } 
        i13 = i15 | i16 << k;
        k = j;
      } 
      j = k + 1;
      char c4 = str.charAt(k);
      k = c4;
      int i11 = j;
      if (c4 >= '?') {
        i11 = c4 & 0x1FFF;
        k = 13;
        int i15 = j;
        while (true) {
          j = i15 + 1;
          i15 = str.charAt(i15);
          if (i15 >= 55296) {
            i11 |= (i15 & 0x1FFF) << k;
            k += 13;
            i15 = j;
            continue;
          } 
          break;
        } 
        k = i11 | i15 << k;
        i11 = j;
      } 
      j = i11 + 1;
      char c3 = str.charAt(i11);
      i11 = c3;
      i6 = j;
      if (c3 >= '?') {
        i6 = c3 & 0x1FFF;
        i11 = 13;
        int i15 = j;
        while (true) {
          j = i15 + 1;
          i15 = str.charAt(i15);
          if (i15 >= 55296) {
            i6 |= (i15 & 0x1FFF) << i11;
            i11 += 13;
            i15 = j;
            continue;
          } 
          break;
        } 
        i11 = i6 | i15 << i11;
        i6 = j;
      } 
      j = i6 + 1;
      char c5 = str.charAt(i6);
      i6 = c5;
      i4 = j;
      if (c5 >= '?') {
        i4 = c5 & 0x1FFF;
        i6 = 13;
        int i15 = j;
        while (true) {
          j = i15 + 1;
          i15 = str.charAt(i15);
          if (i15 >= 55296) {
            i4 |= (i15 & 0x1FFF) << i6;
            i6 += 13;
            i15 = j;
            continue;
          } 
          break;
        } 
        i6 = i4 | i15 << i6;
        i4 = j;
      } 
      j = i4 + 1;
      i5 = str.charAt(i4);
      i4 = i5;
      int i12 = j;
      if (i5 >= 55296) {
        i12 = i5 & 0x1FFF;
        i4 = 13;
        int i15 = j;
        while (true) {
          j = i15 + 1;
          i15 = str.charAt(i15);
          if (i15 >= 55296) {
            i12 |= (i15 & 0x1FFF) << i4;
            i4 += 13;
            i15 = j;
            continue;
          } 
          break;
        } 
        i4 = i12 | i15 << i4;
        i12 = j;
      } 
      j = i12 + 1;
      char c1 = str.charAt(i12);
      i5 = c1;
      i12 = j;
      if (c1 >= '?') {
        i5 = c1 & 0x1FFF;
        i12 = 13;
        int i15 = j;
        while (true) {
          j = i15 + 1;
          i15 = str.charAt(i15);
          if (i15 >= 55296) {
            i5 |= (i15 & 0x1FFF) << i12;
            i12 += 13;
            i15 = j;
            continue;
          } 
          break;
        } 
        i5 |= i15 << i12;
        i12 = j;
      } 
      i2 = i12 + 1;
      char c2 = str.charAt(i12);
      j = c2;
      i12 = i2;
      if (c2 >= '?') {
        j = c2 & 0x1FFF;
        i12 = 13;
        int i15 = i2;
        i2 = j;
        while (true) {
          j = i15 + 1;
          i15 = str.charAt(i15);
          if (i15 >= 55296) {
            i2 |= (i15 & 0x1FFF) << i12;
            i12 += 13;
            i15 = j;
            continue;
          } 
          break;
        } 
        i2 |= i15 << i12;
        i12 = j;
        j = i2;
      } 
      arrayOfInt1 = new int[j + i4 + i5];
      int i14 = n * 2 + i13;
      i13 = n;
      i2 = i6;
      i6 = j;
      n = i12;
      i5 = i4;
      i4 = i2;
      i3 = i11;
      i2 = k;
      k = i14;
      i11 = j;
      j = i13;
    } 
    Unsafe unsafe = UNSAFE;
    Object[] arrayOfObject1 = paramRawMessageInfo.getObjects();
    Class<?> clazz = paramRawMessageInfo.getDefaultInstance().getClass();
    int[] arrayOfInt2 = new int[i4 * 3];
    Object[] arrayOfObject2 = new Object[i4 * 2];
    int i7 = 0, i1 = i6, i9 = i6 + i5, i10 = 0, i4 = n;
    n = i1;
    i1 = i4;
    int i8 = j;
    while (i1 < i) {
      j = i1 + 1;
      i4 = str.charAt(i1);
      i1 = j;
      int i12 = i4;
      if (i4 >= 55296) {
        i4 &= 0x1FFF;
        i1 = 13;
        int i16 = j;
        while (true) {
          j = i16 + 1;
          i16 = str.charAt(i16);
          if (i16 >= 55296) {
            i4 |= (i16 & 0x1FFF) << i1;
            i1 += 13;
            i16 = j;
            continue;
          } 
          break;
        } 
        i12 = i4 | i16 << i1;
        i1 = j;
      } 
      j = i1 + 1;
      int i11 = str.charAt(i1);
      i4 = i11;
      i1 = j;
      if (i11 >= 55296) {
        i4 = i11 & 0x1FFF;
        i1 = 13;
        int i16 = j;
        while (true) {
          j = i16 + 1;
          i16 = str.charAt(i16);
          if (i16 >= 55296) {
            i4 |= (i16 & 0x1FFF) << i1;
            i1 += 13;
            i16 = j;
            continue;
          } 
          break;
        } 
        i4 |= i16 << i1;
        i1 = j;
      } 
      int i13 = i4 & 0xFF;
      int i14 = i7;
      if ((i4 & 0x400) != 0) {
        arrayOfInt1[i7] = i10;
        i14 = i7 + 1;
      } 
      if (i13 >= 51) {
        j = i1 + 1;
        i11 = str.charAt(i1);
        i7 = i11;
        i1 = j;
        if (i11 >= 55296) {
          i7 = i11 & 0x1FFF;
          i1 = 13;
          int i19 = j;
          while (true) {
            j = i19 + 1;
            i19 = str.charAt(i19);
            if (i19 >= 55296) {
              i7 |= (i19 & 0x1FFF) << i1;
              i1 += 13;
              i19 = j;
              continue;
            } 
            break;
          } 
          i7 |= i19 << i1;
          i1 = j;
        } 
        j = i13 - 51;
        if (j == 9 || j == 17) {
          arrayOfObject2[i10 / 3 * 2 + 1] = arrayOfObject1[k];
          j = k + 1;
        } else if (j == 12) {
          j = k;
          if ((m & 0x1) == 1) {
            arrayOfObject2[i10 / 3 * 2 + 1] = arrayOfObject1[k];
            j = k + 1;
          } 
        } else {
          j = k;
        } 
        k = i7 * 2;
        Object object = arrayOfObject1[k];
        if (object instanceof Field) {
          object = object;
        } else {
          object = reflectField(clazz, (String)object);
          arrayOfObject1[k] = object;
        } 
        int i16 = (int)unsafe.objectFieldOffset((Field)object);
        k++;
        object = arrayOfObject1[k];
        if (object instanceof Field) {
          object = object;
        } else {
          object = reflectField(clazz, (String)object);
          arrayOfObject1[k] = object;
        } 
        int i17 = (int)unsafe.objectFieldOffset((Field)object);
        boolean bool1 = false;
        int i18 = i7;
        k = i1;
        i7 = bool1;
        i1 = i17;
      } else {
        i7 = k + 1;
        Field field = reflectField(clazz, (String)arrayOfObject1[k]);
        if (i13 == 9 || i13 == 17) {
          arrayOfObject2[i10 / 3 * 2 + 1] = field.getType();
          j = i7;
        } else if (i13 == 27 || i13 == 49) {
          arrayOfObject2[i10 / 3 * 2 + 1] = arrayOfObject1[i7];
          j = i7 + 1;
        } else if (i13 == 12 || i13 == 30 || i13 == 44) {
          if ((m & 0x1) == 1) {
            arrayOfObject2[i10 / 3 * 2 + 1] = arrayOfObject1[i7];
            j = i7 + 1;
          } else {
            j = i7;
          } 
        } else if (i13 == 50) {
          j = n + 1;
          arrayOfInt1[n] = i10;
          n = i10 / 3;
          k = i7 + 1;
          arrayOfObject2[n * 2] = arrayOfObject1[i7];
          if ((i4 & 0x800) != 0) {
            arrayOfObject2[i10 / 3 * 2 + 1] = arrayOfObject1[k];
            n = j;
            j = k + 1;
          } else {
            n = j;
            j = k;
          } 
        } else {
          j = i7;
        } 
        int i16 = (int)unsafe.objectFieldOffset(field);
        if ((m & 0x1) == 1 && i13 <= 17) {
          i11 = i1 + 1;
          i1 = str.charAt(i1);
          if (i1 >= 55296) {
            i7 = i1 & 0x1FFF;
            k = 13;
            while (true) {
              i1 = i11 + 1;
              i11 = str.charAt(i11);
              if (i11 >= 55296) {
                i7 |= (i11 & 0x1FFF) << k;
                k += 13;
                i11 = i1;
                continue;
              } 
              break;
            } 
            i11 = i7 | i11 << k;
            k = i1;
          } else {
            k = i11;
            i11 = i1;
          } 
          i1 = i8 * 2 + i11 / 32;
          Object object = arrayOfObject1[i1];
          if (object instanceof Field) {
            object = object;
          } else {
            object = reflectField(clazz, (String)object);
            arrayOfObject1[i1] = object;
          } 
          i1 = (int)unsafe.objectFieldOffset((Field)object);
          i7 = i11 % 32;
        } else {
          boolean bool1 = false;
          i7 = 0;
          k = i1;
          i11 = i4;
          i1 = bool1;
        } 
        int i17 = i11;
        if (i13 >= 18 && i13 <= 49) {
          arrayOfInt1[i9] = i16;
          i9++;
          i11 = i16;
        } else {
          i11 = i16;
        } 
      } 
      int i15 = i10 + 1;
      arrayOfInt2[i10] = i12;
      i12 = i15 + 1;
      if ((i4 & 0x200) != 0) {
        i10 = 536870912;
      } else {
        i10 = 0;
      } 
      if ((i4 & 0x100) != 0) {
        i4 = 268435456;
      } else {
        i4 = 0;
      } 
      arrayOfInt2[i15] = i10 | i4 | i13 << 20 | i11;
      i10 = i12 + 1;
      arrayOfInt2[i12] = i7 << 20 | i1;
      i1 = k;
      i7 = i14;
      k = j;
    } 
    return 



      
      new MessageSchema(arrayOfInt2, arrayOfObject2, i2, i3, paramRawMessageInfo.getDefaultInstance(), bool, false, arrayOfInt1, i6, i6 + i5, paramNewInstanceSchema, paramListFieldSchema, paramUnknownFieldSchema, paramExtensionSchema, paramMapFieldSchema);
  }
  
  private static Field reflectField(Class<?> paramClass, String paramString) {
    try {
      return paramClass.getDeclaredField(paramString);
    } catch (NoSuchFieldException noSuchFieldException) {
      for (Field field : paramClass.getDeclaredFields()) {
        if (paramString.equals(field.getName()))
          return field; 
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Field ");
      stringBuilder.append(paramString);
      stringBuilder.append(" for ");
      stringBuilder.append(paramClass.getName());
      stringBuilder.append(" not found. Known fields are ");
      stringBuilder.append(Arrays.toString((Object[])noSuchFieldException));
      throw new RuntimeException(stringBuilder.toString());
    } 
  }
  
  static <T> MessageSchema<T> newSchemaForMessageInfo(StructuralMessageInfo paramStructuralMessageInfo, NewInstanceSchema paramNewInstanceSchema, ListFieldSchema paramListFieldSchema, UnknownFieldSchema<?, ?> paramUnknownFieldSchema, ExtensionSchema<?> paramExtensionSchema, MapFieldSchema paramMapFieldSchema) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual getSyntax : ()Lcom/android/framework/protobuf/ProtoSyntax;
    //   4: getstatic com/android/framework/protobuf/ProtoSyntax.PROTO3 : Lcom/android/framework/protobuf/ProtoSyntax;
    //   7: if_acmpne -> 16
    //   10: iconst_1
    //   11: istore #6
    //   13: goto -> 19
    //   16: iconst_0
    //   17: istore #6
    //   19: aload_0
    //   20: invokevirtual getFields : ()[Lcom/android/framework/protobuf/FieldInfo;
    //   23: astore #7
    //   25: aload #7
    //   27: arraylength
    //   28: ifne -> 40
    //   31: iconst_0
    //   32: istore #8
    //   34: iconst_0
    //   35: istore #9
    //   37: goto -> 62
    //   40: aload #7
    //   42: iconst_0
    //   43: aaload
    //   44: invokevirtual getFieldNumber : ()I
    //   47: istore #8
    //   49: aload #7
    //   51: aload #7
    //   53: arraylength
    //   54: iconst_1
    //   55: isub
    //   56: aaload
    //   57: invokevirtual getFieldNumber : ()I
    //   60: istore #9
    //   62: aload #7
    //   64: arraylength
    //   65: istore #10
    //   67: iload #10
    //   69: iconst_3
    //   70: imul
    //   71: newarray int
    //   73: astore #11
    //   75: iload #10
    //   77: iconst_2
    //   78: imul
    //   79: anewarray java/lang/Object
    //   82: astore #12
    //   84: iconst_0
    //   85: istore #13
    //   87: iconst_0
    //   88: istore #14
    //   90: aload #7
    //   92: arraylength
    //   93: istore #15
    //   95: iconst_0
    //   96: istore #10
    //   98: iload #10
    //   100: iload #15
    //   102: if_icmpge -> 202
    //   105: aload #7
    //   107: iload #10
    //   109: aaload
    //   110: astore #16
    //   112: aload #16
    //   114: invokevirtual getType : ()Lcom/android/framework/protobuf/FieldType;
    //   117: getstatic com/android/framework/protobuf/FieldType.MAP : Lcom/android/framework/protobuf/FieldType;
    //   120: if_acmpne -> 136
    //   123: iload #13
    //   125: iconst_1
    //   126: iadd
    //   127: istore #17
    //   129: iload #14
    //   131: istore #18
    //   133: goto -> 188
    //   136: iload #13
    //   138: istore #17
    //   140: iload #14
    //   142: istore #18
    //   144: aload #16
    //   146: invokevirtual getType : ()Lcom/android/framework/protobuf/FieldType;
    //   149: invokevirtual id : ()I
    //   152: bipush #18
    //   154: if_icmplt -> 188
    //   157: iload #13
    //   159: istore #17
    //   161: iload #14
    //   163: istore #18
    //   165: aload #16
    //   167: invokevirtual getType : ()Lcom/android/framework/protobuf/FieldType;
    //   170: invokevirtual id : ()I
    //   173: bipush #49
    //   175: if_icmpgt -> 188
    //   178: iload #14
    //   180: iconst_1
    //   181: iadd
    //   182: istore #18
    //   184: iload #13
    //   186: istore #17
    //   188: iinc #10, 1
    //   191: iload #17
    //   193: istore #13
    //   195: iload #18
    //   197: istore #14
    //   199: goto -> 98
    //   202: aconst_null
    //   203: astore #16
    //   205: iload #13
    //   207: ifle -> 219
    //   210: iload #13
    //   212: newarray int
    //   214: astore #19
    //   216: goto -> 222
    //   219: aconst_null
    //   220: astore #19
    //   222: iload #14
    //   224: ifle -> 233
    //   227: iload #14
    //   229: newarray int
    //   231: astore #16
    //   233: aload_0
    //   234: invokevirtual getCheckInitialized : ()[I
    //   237: astore #20
    //   239: aload #20
    //   241: ifnonnull -> 252
    //   244: getstatic com/android/framework/protobuf/MessageSchema.EMPTY_INT_ARRAY : [I
    //   247: astore #20
    //   249: goto -> 252
    //   252: iconst_0
    //   253: istore #14
    //   255: iconst_0
    //   256: istore #10
    //   258: iconst_0
    //   259: istore #13
    //   261: iconst_0
    //   262: istore #15
    //   264: iconst_0
    //   265: istore #17
    //   267: iload #17
    //   269: aload #7
    //   271: arraylength
    //   272: if_icmpge -> 424
    //   275: aload #7
    //   277: iload #17
    //   279: aaload
    //   280: astore #21
    //   282: aload #21
    //   284: invokevirtual getFieldNumber : ()I
    //   287: istore #22
    //   289: aload #21
    //   291: aload #11
    //   293: iload #13
    //   295: iload #6
    //   297: aload #12
    //   299: invokestatic storeFieldData : (Lcom/android/framework/protobuf/FieldInfo;[IIZ[Ljava/lang/Object;)V
    //   302: iload #15
    //   304: istore #18
    //   306: iload #15
    //   308: aload #20
    //   310: arraylength
    //   311: if_icmpge -> 341
    //   314: iload #15
    //   316: istore #18
    //   318: aload #20
    //   320: iload #15
    //   322: iaload
    //   323: iload #22
    //   325: if_icmpne -> 341
    //   328: aload #20
    //   330: iload #15
    //   332: iload #13
    //   334: iastore
    //   335: iload #15
    //   337: iconst_1
    //   338: iadd
    //   339: istore #18
    //   341: aload #21
    //   343: invokevirtual getType : ()Lcom/android/framework/protobuf/FieldType;
    //   346: getstatic com/android/framework/protobuf/FieldType.MAP : Lcom/android/framework/protobuf/FieldType;
    //   349: if_acmpne -> 365
    //   352: aload #19
    //   354: iload #14
    //   356: iload #13
    //   358: iastore
    //   359: iinc #14, 1
    //   362: goto -> 411
    //   365: aload #21
    //   367: invokevirtual getType : ()Lcom/android/framework/protobuf/FieldType;
    //   370: invokevirtual id : ()I
    //   373: bipush #18
    //   375: if_icmplt -> 411
    //   378: aload #21
    //   380: invokevirtual getType : ()Lcom/android/framework/protobuf/FieldType;
    //   383: invokevirtual id : ()I
    //   386: bipush #49
    //   388: if_icmpgt -> 411
    //   391: aload #16
    //   393: iload #10
    //   395: aload #21
    //   397: invokevirtual getField : ()Ljava/lang/reflect/Field;
    //   400: invokestatic objectFieldOffset : (Ljava/lang/reflect/Field;)J
    //   403: l2i
    //   404: iastore
    //   405: iinc #10, 1
    //   408: goto -> 411
    //   411: iinc #17, 1
    //   414: iinc #13, 3
    //   417: iload #18
    //   419: istore #15
    //   421: goto -> 267
    //   424: aload #19
    //   426: ifnonnull -> 437
    //   429: getstatic com/android/framework/protobuf/MessageSchema.EMPTY_INT_ARRAY : [I
    //   432: astore #19
    //   434: goto -> 437
    //   437: aload #16
    //   439: ifnonnull -> 450
    //   442: getstatic com/android/framework/protobuf/MessageSchema.EMPTY_INT_ARRAY : [I
    //   445: astore #16
    //   447: goto -> 450
    //   450: aload #20
    //   452: arraylength
    //   453: aload #19
    //   455: arraylength
    //   456: iadd
    //   457: aload #16
    //   459: arraylength
    //   460: iadd
    //   461: newarray int
    //   463: astore #7
    //   465: aload #20
    //   467: iconst_0
    //   468: aload #7
    //   470: iconst_0
    //   471: aload #20
    //   473: arraylength
    //   474: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   477: aload #19
    //   479: iconst_0
    //   480: aload #7
    //   482: aload #20
    //   484: arraylength
    //   485: aload #19
    //   487: arraylength
    //   488: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   491: aload #16
    //   493: iconst_0
    //   494: aload #7
    //   496: aload #20
    //   498: arraylength
    //   499: aload #19
    //   501: arraylength
    //   502: iadd
    //   503: aload #16
    //   505: arraylength
    //   506: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   509: new com/android/framework/protobuf/MessageSchema
    //   512: dup
    //   513: aload #11
    //   515: aload #12
    //   517: iload #8
    //   519: iload #9
    //   521: aload_0
    //   522: invokevirtual getDefaultInstance : ()Lcom/android/framework/protobuf/MessageLite;
    //   525: iload #6
    //   527: iconst_1
    //   528: aload #7
    //   530: aload #20
    //   532: arraylength
    //   533: aload #20
    //   535: arraylength
    //   536: aload #19
    //   538: arraylength
    //   539: iadd
    //   540: aload_1
    //   541: aload_2
    //   542: aload_3
    //   543: aload #4
    //   545: aload #5
    //   547: invokespecial <init> : ([I[Ljava/lang/Object;IILcom/android/framework/protobuf/MessageLite;ZZ[IIILcom/android/framework/protobuf/NewInstanceSchema;Lcom/android/framework/protobuf/ListFieldSchema;Lcom/android/framework/protobuf/UnknownFieldSchema;Lcom/android/framework/protobuf/ExtensionSchema;Lcom/android/framework/protobuf/MapFieldSchema;)V
    //   550: astore_0
    //   551: aload_0
    //   552: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #619	-> 0
    //   #620	-> 19
    //   #623	-> 25
    //   #624	-> 31
    //   #625	-> 34
    //   #627	-> 40
    //   #628	-> 49
    //   #631	-> 62
    //   #633	-> 67
    //   #634	-> 75
    //   #636	-> 84
    //   #637	-> 87
    //   #638	-> 90
    //   #639	-> 112
    //   #640	-> 123
    //   #641	-> 136
    //   #644	-> 178
    //   #638	-> 188
    //   #647	-> 202
    //   #648	-> 222
    //   #649	-> 233
    //   #650	-> 233
    //   #652	-> 233
    //   #653	-> 239
    //   #654	-> 244
    //   #653	-> 252
    //   #656	-> 252
    //   #658	-> 252
    //   #659	-> 252
    //   #660	-> 275
    //   #661	-> 282
    //   #665	-> 289
    //   #668	-> 302
    //   #670	-> 328
    //   #673	-> 341
    //   #674	-> 352
    //   #675	-> 365
    //   #678	-> 391
    //   #679	-> 391
    //   #675	-> 411
    //   #682	-> 411
    //   #659	-> 414
    //   #685	-> 424
    //   #686	-> 429
    //   #685	-> 437
    //   #688	-> 437
    //   #689	-> 442
    //   #688	-> 450
    //   #691	-> 450
    //   #693	-> 465
    //   #694	-> 477
    //   #696	-> 491
    //   #703	-> 509
    //   #708	-> 509
    //   #703	-> 551
  }
  
  private static void storeFieldData(FieldInfo paramFieldInfo, int[] paramArrayOfint, int paramInt, boolean paramBoolean, Object[] paramArrayOfObject) {
    int i, j, k;
    byte b;
    int n;
    OneofInfo oneofInfo = paramFieldInfo.getOneof();
    if (oneofInfo != null) {
      i = paramFieldInfo.getType().id() + 51;
      j = (int)UnsafeUtil.objectFieldOffset(oneofInfo.getValueField());
      k = (int)UnsafeUtil.objectFieldOffset(oneofInfo.getCaseField());
      b = 0;
    } else {
      FieldType fieldType = paramFieldInfo.getType();
      j = (int)UnsafeUtil.objectFieldOffset(paramFieldInfo.getField());
      i = fieldType.id();
      if (!paramBoolean && !fieldType.isList() && !fieldType.isMap()) {
        k = (int)UnsafeUtil.objectFieldOffset(paramFieldInfo.getPresenceField());
        b = Integer.numberOfTrailingZeros(paramFieldInfo.getPresenceMask());
      } else if (paramFieldInfo.getCachedSizeField() == null) {
        k = 0;
        b = 0;
      } else {
        k = (int)UnsafeUtil.objectFieldOffset(paramFieldInfo.getCachedSizeField());
        b = 0;
      } 
    } 
    paramArrayOfint[paramInt] = paramFieldInfo.getFieldNumber();
    paramBoolean = paramFieldInfo.isEnforceUtf8();
    int m = 0;
    if (paramBoolean) {
      n = 536870912;
    } else {
      n = 0;
    } 
    if (paramFieldInfo.isRequired())
      m = 268435456; 
    paramArrayOfint[paramInt + 1] = n | m | i << 20 | j;
    paramArrayOfint[paramInt + 2] = b << 20 | k;
    Class<?> clazz = paramFieldInfo.getMessageFieldClass();
    if (paramFieldInfo.getMapDefaultEntry() != null) {
      paramArrayOfObject[paramInt / 3 * 2] = paramFieldInfo.getMapDefaultEntry();
      if (clazz != null) {
        paramArrayOfObject[paramInt / 3 * 2 + 1] = clazz;
      } else if (paramFieldInfo.getEnumVerifier() != null) {
        paramArrayOfObject[paramInt / 3 * 2 + 1] = paramFieldInfo.getEnumVerifier();
      } 
    } else if (clazz != null) {
      paramArrayOfObject[paramInt / 3 * 2 + 1] = clazz;
    } else if (paramFieldInfo.getEnumVerifier() != null) {
      paramArrayOfObject[paramInt / 3 * 2 + 1] = paramFieldInfo.getEnumVerifier();
    } 
  }
  
  public T newInstance() {
    return (T)this.newInstanceSchema.newInstance(this.defaultInstance);
  }
  
  public boolean equals(T paramT1, T paramT2) {
    int i = this.buffer.length;
    for (byte b = 0; b < i; b += 3) {
      if (!equals(paramT1, paramT2, b))
        return false; 
    } 
    Object object1 = this.unknownFieldSchema.getFromMessage(paramT1);
    Object object2 = this.unknownFieldSchema.getFromMessage(paramT2);
    if (!object1.equals(object2))
      return false; 
    if (this.hasExtensions) {
      FieldSet<?> fieldSet1 = this.extensionSchema.getExtensions(paramT1);
      FieldSet<?> fieldSet2 = this.extensionSchema.getExtensions(paramT2);
      return fieldSet1.equals(fieldSet2);
    } 
    return true;
  }
  
  private boolean equals(T paramT1, T paramT2, int paramInt) {
    int i = typeAndOffsetAt(paramInt);
    long l = offset(i);
    i = type(i);
    boolean bool1 = false, bool2 = false, bool3 = false, bool4 = false, bool5 = false, bool6 = false, bool7 = false, bool8 = false, bool9 = false, bool10 = false, bool11 = false, bool12 = false, bool13 = false;
    null = false;
    boolean bool14 = false, bool15 = false, bool16 = false, bool17 = false, bool18 = false;
    switch (i) {
      default:
        return true;
      case 51:
      case 52:
      case 53:
      case 54:
      case 55:
      case 56:
      case 57:
      case 58:
      case 59:
      case 60:
      case 61:
      case 62:
      case 63:
      case 64:
      case 65:
      case 66:
      case 67:
      case 68:
        if (isOneofCaseEqual(paramT1, paramT2, paramInt)) {
          paramT1 = (T)UnsafeUtil.getObject(paramT1, l);
          paramT2 = (T)UnsafeUtil.getObject(paramT2, l);
          if (SchemaUtil.safeEquals(paramT1, paramT2))
            return true; 
        } 
        return bool18;
      case 50:
        paramT1 = (T)UnsafeUtil.getObject(paramT1, l);
        paramT2 = (T)UnsafeUtil.getObject(paramT2, l);
        return SchemaUtil.safeEquals(paramT1, paramT2);
      case 18:
      case 19:
      case 20:
      case 21:
      case 22:
      case 23:
      case 24:
      case 25:
      case 26:
      case 27:
      case 28:
      case 29:
      case 30:
      case 31:
      case 32:
      case 33:
      case 34:
      case 35:
      case 36:
      case 37:
      case 38:
      case 39:
      case 40:
      case 41:
      case 42:
      case 43:
      case 44:
      case 45:
      case 46:
      case 47:
      case 48:
      case 49:
        paramT1 = (T)UnsafeUtil.getObject(paramT1, l);
        paramT2 = (T)UnsafeUtil.getObject(paramT2, l);
        return SchemaUtil.safeEquals(paramT1, paramT2);
      case 17:
        if (arePresentForEquals(paramT1, paramT2, paramInt)) {
          paramT1 = (T)UnsafeUtil.getObject(paramT1, l);
          paramT2 = (T)UnsafeUtil.getObject(paramT2, l);
          if (SchemaUtil.safeEquals(paramT1, paramT2))
            return true; 
        } 
        return bool1;
      case 16:
        if (arePresentForEquals(paramT1, paramT2, paramInt) && UnsafeUtil.getLong(paramT1, l) == UnsafeUtil.getLong(paramT2, l)) {
          null = true;
        } else {
          null = bool2;
        } 
        return null;
      case 15:
        if (arePresentForEquals(paramT1, paramT2, paramInt) && UnsafeUtil.getInt(paramT1, l) == UnsafeUtil.getInt(paramT2, l)) {
          null = true;
        } else {
          null = bool3;
        } 
        return null;
      case 14:
        if (arePresentForEquals(paramT1, paramT2, paramInt) && UnsafeUtil.getLong(paramT1, l) == UnsafeUtil.getLong(paramT2, l)) {
          null = true;
        } else {
          null = bool4;
        } 
        return null;
      case 13:
        if (arePresentForEquals(paramT1, paramT2, paramInt) && UnsafeUtil.getInt(paramT1, l) == UnsafeUtil.getInt(paramT2, l)) {
          null = true;
        } else {
          null = bool5;
        } 
        return null;
      case 12:
        if (arePresentForEquals(paramT1, paramT2, paramInt) && UnsafeUtil.getInt(paramT1, l) == UnsafeUtil.getInt(paramT2, l)) {
          null = true;
        } else {
          null = bool6;
        } 
        return null;
      case 11:
        if (arePresentForEquals(paramT1, paramT2, paramInt) && UnsafeUtil.getInt(paramT1, l) == UnsafeUtil.getInt(paramT2, l)) {
          null = true;
        } else {
          null = bool7;
        } 
        return null;
      case 10:
        if (arePresentForEquals(paramT1, paramT2, paramInt)) {
          paramT1 = (T)UnsafeUtil.getObject(paramT1, l);
          paramT2 = (T)UnsafeUtil.getObject(paramT2, l);
          if (SchemaUtil.safeEquals(paramT1, paramT2))
            return true; 
        } 
        return bool8;
      case 9:
        if (arePresentForEquals(paramT1, paramT2, paramInt)) {
          paramT1 = (T)UnsafeUtil.getObject(paramT1, l);
          paramT2 = (T)UnsafeUtil.getObject(paramT2, l);
          if (SchemaUtil.safeEquals(paramT1, paramT2))
            return true; 
        } 
        return bool9;
      case 8:
        if (arePresentForEquals(paramT1, paramT2, paramInt)) {
          paramT1 = (T)UnsafeUtil.getObject(paramT1, l);
          paramT2 = (T)UnsafeUtil.getObject(paramT2, l);
          if (SchemaUtil.safeEquals(paramT1, paramT2))
            return true; 
        } 
        return bool10;
      case 7:
        if (arePresentForEquals(paramT1, paramT2, paramInt) && UnsafeUtil.getBoolean(paramT1, l) == UnsafeUtil.getBoolean(paramT2, l)) {
          null = true;
        } else {
          null = bool11;
        } 
        return null;
      case 6:
        if (arePresentForEquals(paramT1, paramT2, paramInt) && UnsafeUtil.getInt(paramT1, l) == UnsafeUtil.getInt(paramT2, l)) {
          null = true;
        } else {
          null = bool12;
        } 
        return null;
      case 5:
        if (arePresentForEquals(paramT1, paramT2, paramInt) && UnsafeUtil.getLong(paramT1, l) == UnsafeUtil.getLong(paramT2, l)) {
          null = true;
        } else {
          null = bool13;
        } 
        return null;
      case 4:
        if (arePresentForEquals(paramT1, paramT2, paramInt) && UnsafeUtil.getInt(paramT1, l) == UnsafeUtil.getInt(paramT2, l))
          null = true; 
        return null;
      case 3:
        if (arePresentForEquals(paramT1, paramT2, paramInt) && UnsafeUtil.getLong(paramT1, l) == UnsafeUtil.getLong(paramT2, l)) {
          null = true;
        } else {
          null = bool14;
        } 
        return null;
      case 2:
        if (arePresentForEquals(paramT1, paramT2, paramInt) && UnsafeUtil.getLong(paramT1, l) == UnsafeUtil.getLong(paramT2, l)) {
          null = true;
        } else {
          null = bool15;
        } 
        return null;
      case 1:
        if (arePresentForEquals(paramT1, paramT2, paramInt)) {
          paramInt = Float.floatToIntBits(UnsafeUtil.getFloat(paramT1, l));
          if (paramInt == Float.floatToIntBits(UnsafeUtil.getFloat(paramT2, l)))
            return true; 
        } 
        return bool16;
      case 0:
        break;
    } 
    if (arePresentForEquals(paramT1, paramT2, paramInt)) {
      long l1 = Double.doubleToLongBits(UnsafeUtil.getDouble(paramT1, l));
      if (l1 == Double.doubleToLongBits(UnsafeUtil.getDouble(paramT2, l)))
        return true; 
    } 
    return bool17;
  }
  
  public int hashCode(T paramT) {
    int i = 0;
    int j = this.buffer.length;
    int k;
    for (k = 0; k < j; k += 3, i = n) {
      Object object;
      int n = typeAndOffsetAt(k);
      int i1 = numberAt(k);
      long l = offset(n);
      switch (type(n)) {
        default:
          n = i;
          break;
        case 68:
          n = i;
          if (isOneofPresent(paramT, i1, k)) {
            Object object1 = UnsafeUtil.getObject(paramT, l);
            n = object1.hashCode();
            n = i * 53 + n;
          } 
          break;
        case 67:
          n = i;
          if (isOneofPresent(paramT, i1, k))
            n = i * 53 + Internal.hashLong(oneofLongAt(paramT, l)); 
          break;
        case 66:
          n = i;
          if (isOneofPresent(paramT, i1, k))
            n = i * 53 + oneofIntAt(paramT, l); 
          break;
        case 65:
          n = i;
          if (isOneofPresent(paramT, i1, k))
            n = i * 53 + Internal.hashLong(oneofLongAt(paramT, l)); 
          break;
        case 64:
          n = i;
          if (isOneofPresent(paramT, i1, k))
            n = i * 53 + oneofIntAt(paramT, l); 
          break;
        case 63:
          n = i;
          if (isOneofPresent(paramT, i1, k))
            n = i * 53 + oneofIntAt(paramT, l); 
          break;
        case 62:
          n = i;
          if (isOneofPresent(paramT, i1, k))
            n = i * 53 + oneofIntAt(paramT, l); 
          break;
        case 61:
          n = i;
          if (isOneofPresent(paramT, i1, k))
            n = i * 53 + UnsafeUtil.getObject(paramT, l).hashCode(); 
          break;
        case 60:
          n = i;
          if (isOneofPresent(paramT, i1, k)) {
            Object object1 = UnsafeUtil.getObject(paramT, l);
            n = object1.hashCode();
            n = i * 53 + n;
          } 
          break;
        case 59:
          n = i;
          if (isOneofPresent(paramT, i1, k))
            n = i * 53 + ((String)UnsafeUtil.getObject(paramT, l)).hashCode(); 
          break;
        case 58:
          n = i;
          if (isOneofPresent(paramT, i1, k))
            n = i * 53 + Internal.hashBoolean(oneofBooleanAt(paramT, l)); 
          break;
        case 57:
          n = i;
          if (isOneofPresent(paramT, i1, k))
            n = i * 53 + oneofIntAt(paramT, l); 
          break;
        case 56:
          n = i;
          if (isOneofPresent(paramT, i1, k))
            n = i * 53 + Internal.hashLong(oneofLongAt(paramT, l)); 
          break;
        case 55:
          n = i;
          if (isOneofPresent(paramT, i1, k))
            n = i * 53 + oneofIntAt(paramT, l); 
          break;
        case 54:
          n = i;
          if (isOneofPresent(paramT, i1, k))
            n = i * 53 + Internal.hashLong(oneofLongAt(paramT, l)); 
          break;
        case 53:
          n = i;
          if (isOneofPresent(paramT, i1, k))
            n = i * 53 + Internal.hashLong(oneofLongAt(paramT, l)); 
          break;
        case 52:
          n = i;
          if (isOneofPresent(paramT, i1, k))
            n = i * 53 + Float.floatToIntBits(oneofFloatAt(paramT, l)); 
          break;
        case 51:
          n = i;
          if (isOneofPresent(paramT, i1, k))
            n = i * 53 + Internal.hashLong(Double.doubleToLongBits(oneofDoubleAt(paramT, l))); 
          break;
        case 50:
          n = UnsafeUtil.getObject(paramT, l).hashCode();
          n = i * 53 + n;
          break;
        case 18:
        case 19:
        case 20:
        case 21:
        case 22:
        case 23:
        case 24:
        case 25:
        case 26:
        case 27:
        case 28:
        case 29:
        case 30:
        case 31:
        case 32:
        case 33:
        case 34:
        case 35:
        case 36:
        case 37:
        case 38:
        case 39:
        case 40:
        case 41:
        case 42:
        case 43:
        case 44:
        case 45:
        case 46:
        case 47:
        case 48:
        case 49:
          n = UnsafeUtil.getObject(paramT, l).hashCode();
          n = i * 53 + n;
          break;
        case 17:
          n = 37;
          object = UnsafeUtil.getObject(paramT, l);
          if (object != null)
            n = object.hashCode(); 
          n = i * 53 + n;
          break;
        case 16:
          n = Internal.hashLong(UnsafeUtil.getLong(paramT, l));
          n = i * 53 + n;
          break;
        case 15:
          n = UnsafeUtil.getInt(paramT, l);
          n = i * 53 + n;
          break;
        case 14:
          n = Internal.hashLong(UnsafeUtil.getLong(paramT, l));
          n = i * 53 + n;
          break;
        case 13:
          n = UnsafeUtil.getInt(paramT, l);
          n = i * 53 + n;
          break;
        case 12:
          n = UnsafeUtil.getInt(paramT, l);
          n = i * 53 + n;
          break;
        case 11:
          n = UnsafeUtil.getInt(paramT, l);
          n = i * 53 + n;
          break;
        case 10:
          n = UnsafeUtil.getObject(paramT, l).hashCode();
          n = i * 53 + n;
          break;
        case 9:
          n = 37;
          object = UnsafeUtil.getObject(paramT, l);
          if (object != null)
            n = object.hashCode(); 
          n = i * 53 + n;
          break;
        case 8:
          n = ((String)UnsafeUtil.getObject(paramT, l)).hashCode();
          n = i * 53 + n;
          break;
        case 7:
          n = Internal.hashBoolean(UnsafeUtil.getBoolean(paramT, l));
          n = i * 53 + n;
          break;
        case 6:
          n = UnsafeUtil.getInt(paramT, l);
          n = i * 53 + n;
          break;
        case 5:
          n = Internal.hashLong(UnsafeUtil.getLong(paramT, l));
          n = i * 53 + n;
          break;
        case 4:
          n = UnsafeUtil.getInt(paramT, l);
          n = i * 53 + n;
          break;
        case 3:
          n = Internal.hashLong(UnsafeUtil.getLong(paramT, l));
          n = i * 53 + n;
          break;
        case 2:
          n = Internal.hashLong(UnsafeUtil.getLong(paramT, l));
          n = i * 53 + n;
          break;
        case 1:
          n = Float.floatToIntBits(UnsafeUtil.getFloat(paramT, l));
          n = i * 53 + n;
          break;
        case 0:
          l = Double.doubleToLongBits(UnsafeUtil.getDouble(paramT, l));
          n = Internal.hashLong(l);
          n = i * 53 + n;
          break;
      } 
    } 
    k = i * 53 + this.unknownFieldSchema.getFromMessage(paramT).hashCode();
    int m = k;
    if (this.hasExtensions)
      m = k * 53 + this.extensionSchema.getExtensions(paramT).hashCode(); 
    return m;
  }
  
  public void mergeFrom(T paramT1, T paramT2) {
    if (paramT2 != null) {
      for (byte b = 0; b < this.buffer.length; b += 3)
        mergeSingleField(paramT1, paramT2, b); 
      if (!this.proto3) {
        SchemaUtil.mergeUnknownFields(this.unknownFieldSchema, paramT1, paramT2);
        if (this.hasExtensions)
          SchemaUtil.mergeExtensions(this.extensionSchema, paramT1, paramT2); 
      } 
      return;
    } 
    throw null;
  }
  
  private void mergeSingleField(T paramT1, T paramT2, int paramInt) {
    int i = typeAndOffsetAt(paramInt);
    long l = offset(i);
    int j = numberAt(paramInt);
    switch (type(i)) {
      default:
        return;
      case 68:
        mergeOneofMessage(paramT1, paramT2, paramInt);
      case 61:
      case 62:
      case 63:
      case 64:
      case 65:
      case 66:
      case 67:
        if (isOneofPresent(paramT2, j, paramInt)) {
          UnsafeUtil.putObject(paramT1, l, UnsafeUtil.getObject(paramT2, l));
          setOneofPresent(paramT1, j, paramInt);
        } 
      case 60:
        mergeOneofMessage(paramT1, paramT2, paramInt);
      case 51:
      case 52:
      case 53:
      case 54:
      case 55:
      case 56:
      case 57:
      case 58:
      case 59:
        if (isOneofPresent(paramT2, j, paramInt)) {
          UnsafeUtil.putObject(paramT1, l, UnsafeUtil.getObject(paramT2, l));
          setOneofPresent(paramT1, j, paramInt);
        } 
      case 50:
        SchemaUtil.mergeMap(this.mapFieldSchema, paramT1, paramT2, l);
      case 18:
      case 19:
      case 20:
      case 21:
      case 22:
      case 23:
      case 24:
      case 25:
      case 26:
      case 27:
      case 28:
      case 29:
      case 30:
      case 31:
      case 32:
      case 33:
      case 34:
      case 35:
      case 36:
      case 37:
      case 38:
      case 39:
      case 40:
      case 41:
      case 42:
      case 43:
      case 44:
      case 45:
      case 46:
      case 47:
      case 48:
      case 49:
        this.listFieldSchema.mergeListsAt(paramT1, paramT2, l);
      case 17:
        mergeMessage(paramT1, paramT2, paramInt);
      case 16:
        if (isFieldPresent(paramT2, paramInt)) {
          UnsafeUtil.putLong(paramT1, l, UnsafeUtil.getLong(paramT2, l));
          setFieldPresent(paramT1, paramInt);
        } 
      case 15:
        if (isFieldPresent(paramT2, paramInt)) {
          UnsafeUtil.putInt(paramT1, l, UnsafeUtil.getInt(paramT2, l));
          setFieldPresent(paramT1, paramInt);
        } 
      case 14:
        if (isFieldPresent(paramT2, paramInt)) {
          UnsafeUtil.putLong(paramT1, l, UnsafeUtil.getLong(paramT2, l));
          setFieldPresent(paramT1, paramInt);
        } 
      case 13:
        if (isFieldPresent(paramT2, paramInt)) {
          UnsafeUtil.putInt(paramT1, l, UnsafeUtil.getInt(paramT2, l));
          setFieldPresent(paramT1, paramInt);
        } 
      case 12:
        if (isFieldPresent(paramT2, paramInt)) {
          UnsafeUtil.putInt(paramT1, l, UnsafeUtil.getInt(paramT2, l));
          setFieldPresent(paramT1, paramInt);
        } 
      case 11:
        if (isFieldPresent(paramT2, paramInt)) {
          UnsafeUtil.putInt(paramT1, l, UnsafeUtil.getInt(paramT2, l));
          setFieldPresent(paramT1, paramInt);
        } 
      case 10:
        if (isFieldPresent(paramT2, paramInt)) {
          UnsafeUtil.putObject(paramT1, l, UnsafeUtil.getObject(paramT2, l));
          setFieldPresent(paramT1, paramInt);
        } 
      case 9:
        mergeMessage(paramT1, paramT2, paramInt);
      case 8:
        if (isFieldPresent(paramT2, paramInt)) {
          UnsafeUtil.putObject(paramT1, l, UnsafeUtil.getObject(paramT2, l));
          setFieldPresent(paramT1, paramInt);
        } 
      case 7:
        if (isFieldPresent(paramT2, paramInt)) {
          UnsafeUtil.putBoolean(paramT1, l, UnsafeUtil.getBoolean(paramT2, l));
          setFieldPresent(paramT1, paramInt);
        } 
      case 6:
        if (isFieldPresent(paramT2, paramInt)) {
          UnsafeUtil.putInt(paramT1, l, UnsafeUtil.getInt(paramT2, l));
          setFieldPresent(paramT1, paramInt);
        } 
      case 5:
        if (isFieldPresent(paramT2, paramInt)) {
          UnsafeUtil.putLong(paramT1, l, UnsafeUtil.getLong(paramT2, l));
          setFieldPresent(paramT1, paramInt);
        } 
      case 4:
        if (isFieldPresent(paramT2, paramInt)) {
          UnsafeUtil.putInt(paramT1, l, UnsafeUtil.getInt(paramT2, l));
          setFieldPresent(paramT1, paramInt);
        } 
      case 3:
        if (isFieldPresent(paramT2, paramInt)) {
          UnsafeUtil.putLong(paramT1, l, UnsafeUtil.getLong(paramT2, l));
          setFieldPresent(paramT1, paramInt);
        } 
      case 2:
        if (isFieldPresent(paramT2, paramInt)) {
          UnsafeUtil.putLong(paramT1, l, UnsafeUtil.getLong(paramT2, l));
          setFieldPresent(paramT1, paramInt);
        } 
      case 1:
        if (isFieldPresent(paramT2, paramInt)) {
          UnsafeUtil.putFloat(paramT1, l, UnsafeUtil.getFloat(paramT2, l));
          setFieldPresent(paramT1, paramInt);
        } 
      case 0:
        break;
    } 
    if (isFieldPresent(paramT2, paramInt)) {
      UnsafeUtil.putDouble(paramT1, l, UnsafeUtil.getDouble(paramT2, l));
      setFieldPresent(paramT1, paramInt);
    } 
  }
  
  private void mergeMessage(T paramT1, T paramT2, int paramInt) {
    int i = typeAndOffsetAt(paramInt);
    long l = offset(i);
    if (!isFieldPresent(paramT2, paramInt))
      return; 
    Object object = UnsafeUtil.getObject(paramT1, l);
    paramT2 = (T)UnsafeUtil.getObject(paramT2, l);
    if (object != null && paramT2 != null) {
      paramT2 = (T)Internal.mergeMessage(object, paramT2);
      UnsafeUtil.putObject(paramT1, l, paramT2);
      setFieldPresent(paramT1, paramInt);
    } else if (paramT2 != null) {
      UnsafeUtil.putObject(paramT1, l, paramT2);
      setFieldPresent(paramT1, paramInt);
    } 
  }
  
  private void mergeOneofMessage(T paramT1, T paramT2, int paramInt) {
    int i = typeAndOffsetAt(paramInt);
    int j = numberAt(paramInt);
    long l = offset(i);
    if (!isOneofPresent(paramT2, j, paramInt))
      return; 
    Object object = UnsafeUtil.getObject(paramT1, l);
    paramT2 = (T)UnsafeUtil.getObject(paramT2, l);
    if (object != null && paramT2 != null) {
      paramT2 = (T)Internal.mergeMessage(object, paramT2);
      UnsafeUtil.putObject(paramT1, l, paramT2);
      setOneofPresent(paramT1, j, paramInt);
    } else if (paramT2 != null) {
      UnsafeUtil.putObject(paramT1, l, paramT2);
      setOneofPresent(paramT1, j, paramInt);
    } 
  }
  
  public int getSerializedSize(T paramT) {
    int i;
    if (this.proto3) {
      i = getSerializedSizeProto3(paramT);
    } else {
      i = getSerializedSizeProto2(paramT);
    } 
    return i;
  }
  
  private int getSerializedSizeProto2(T paramT) {
    int i = 0;
    Unsafe unsafe = UNSAFE;
    int j = -1;
    int k = 0;
    for (byte b = 0; b < this.buffer.length; b += 3, i = j, j = i7, k = i8) {
      int i7, i8;
      Object object1, object2;
      MapFieldSchema mapFieldSchema;
      int n = typeAndOffsetAt(b);
      int i1 = numberAt(b);
      int i2 = type(n);
      int i3 = 0, i4 = 0;
      int i5 = 0, i6 = 0;
      if (i2 <= 17) {
        i5 = this.buffer[b + 2];
        int i9 = i5 & 0xFFFFF;
        i3 = 1 << i5 >>> 20;
        i7 = j;
        i8 = k;
        i4 = i5;
        i6 = i3;
        if (i9 != j) {
          i7 = i9;
          i8 = unsafe.getInt(paramT, i9);
          i6 = i3;
          i4 = i5;
        } 
      } else {
        i7 = j;
        i8 = k;
        if (this.useCachedSizeField) {
          FieldType fieldType = FieldType.DOUBLE_LIST_PACKED;
          i7 = j;
          i8 = k;
          i4 = i3;
          i6 = i5;
          if (i2 >= fieldType.id()) {
            fieldType = FieldType.SINT64_LIST_PACKED;
            i7 = j;
            i8 = k;
            i4 = i3;
            i6 = i5;
            if (i2 <= fieldType.id()) {
              i4 = this.buffer[b + 2] & 0xFFFFF;
              i6 = i5;
              i8 = k;
              i7 = j;
            } 
          } 
        } 
      } 
      long l = offset(n);
      switch (i2) {
        default:
          j = i;
          break;
        case 68:
          if (isOneofPresent(paramT, i1, b)) {
            MessageLite messageLite = (MessageLite)unsafe.getObject(paramT, l);
            Schema schema = getMessageFieldSchema(b);
            j = i + CodedOutputStream.computeGroupSize(i1, messageLite, schema);
            break;
          } 
          j = i;
          break;
        case 67:
          if (isOneofPresent(paramT, i1, b)) {
            j = i + CodedOutputStream.computeSInt64Size(i1, oneofLongAt(paramT, l));
            break;
          } 
          j = i;
          break;
        case 66:
          if (isOneofPresent(paramT, i1, b)) {
            j = i + CodedOutputStream.computeSInt32Size(i1, oneofIntAt(paramT, l));
            break;
          } 
          j = i;
          break;
        case 65:
          if (isOneofPresent(paramT, i1, b)) {
            j = i + CodedOutputStream.computeSFixed64Size(i1, 0L);
            break;
          } 
          j = i;
          break;
        case 64:
          if (isOneofPresent(paramT, i1, b)) {
            j = i + CodedOutputStream.computeSFixed32Size(i1, 0);
            break;
          } 
          j = i;
          break;
        case 63:
          if (isOneofPresent(paramT, i1, b)) {
            j = i + CodedOutputStream.computeEnumSize(i1, oneofIntAt(paramT, l));
            break;
          } 
          j = i;
          break;
        case 62:
          if (isOneofPresent(paramT, i1, b)) {
            j = i + CodedOutputStream.computeUInt32Size(i1, oneofIntAt(paramT, l));
            break;
          } 
          j = i;
          break;
        case 61:
          if (isOneofPresent(paramT, i1, b)) {
            ByteString byteString = (ByteString)unsafe.getObject(paramT, l);
            j = i + CodedOutputStream.computeBytesSize(i1, byteString);
            break;
          } 
          j = i;
          break;
        case 60:
          if (isOneofPresent(paramT, i1, b)) {
            Object object = unsafe.getObject(paramT, l);
            j = i + SchemaUtil.computeSizeMessage(i1, object, getMessageFieldSchema(b));
            break;
          } 
          j = i;
          break;
        case 59:
          if (isOneofPresent(paramT, i1, b)) {
            Object object = unsafe.getObject(paramT, l);
            if (object instanceof ByteString) {
              j = i + CodedOutputStream.computeBytesSize(i1, (ByteString)object);
              break;
            } 
            j = i + CodedOutputStream.computeStringSize(i1, (String)object);
            break;
          } 
          j = i;
          break;
        case 58:
          if (isOneofPresent(paramT, i1, b)) {
            j = i + CodedOutputStream.computeBoolSize(i1, true);
            break;
          } 
          j = i;
          break;
        case 57:
          if (isOneofPresent(paramT, i1, b)) {
            j = i + CodedOutputStream.computeFixed32Size(i1, 0);
            break;
          } 
          j = i;
          break;
        case 56:
          if (isOneofPresent(paramT, i1, b)) {
            j = i + CodedOutputStream.computeFixed64Size(i1, 0L);
            break;
          } 
          j = i;
          break;
        case 55:
          if (isOneofPresent(paramT, i1, b)) {
            j = i + CodedOutputStream.computeInt32Size(i1, oneofIntAt(paramT, l));
            break;
          } 
          j = i;
          break;
        case 54:
          if (isOneofPresent(paramT, i1, b)) {
            j = i + CodedOutputStream.computeUInt64Size(i1, oneofLongAt(paramT, l));
            break;
          } 
          j = i;
          break;
        case 53:
          if (isOneofPresent(paramT, i1, b)) {
            j = i + CodedOutputStream.computeInt64Size(i1, oneofLongAt(paramT, l));
            break;
          } 
          j = i;
          break;
        case 52:
          if (isOneofPresent(paramT, i1, b)) {
            j = i + CodedOutputStream.computeFloatSize(i1, 0.0F);
            break;
          } 
          j = i;
          break;
        case 51:
          if (isOneofPresent(paramT, i1, b)) {
            j = i + CodedOutputStream.computeDoubleSize(i1, 0.0D);
            break;
          } 
          j = i;
          break;
        case 50:
          mapFieldSchema = this.mapFieldSchema;
          object2 = unsafe.getObject(paramT, l);
          object1 = getMapFieldDefaultEntry(b);
          j = i + mapFieldSchema.getSerializedSize(i1, object2, object1);
          break;
        case 49:
          object1 = unsafe.getObject(paramT, l);
          object2 = getMessageFieldSchema(b);
          j = i + SchemaUtil.computeSizeGroupList(i1, (List<MessageLite>)object1, (Schema)object2);
          break;
        case 48:
          object1 = unsafe.getObject(paramT, l);
          k = SchemaUtil.computeSizeSInt64ListNoTag((List<Long>)object1);
          j = i;
          if (k > 0) {
            if (this.useCachedSizeField)
              unsafe.putInt(paramT, i4, k); 
            j = CodedOutputStream.computeTagSize(i1);
            j = i + j + CodedOutputStream.computeUInt32SizeNoTag(k) + k;
          } 
          break;
        case 47:
          object1 = unsafe.getObject(paramT, l);
          k = SchemaUtil.computeSizeSInt32ListNoTag((List<Integer>)object1);
          j = i;
          if (k > 0) {
            if (this.useCachedSizeField)
              unsafe.putInt(paramT, i4, k); 
            j = CodedOutputStream.computeTagSize(i1);
            j = i + j + CodedOutputStream.computeUInt32SizeNoTag(k) + k;
          } 
          break;
        case 46:
          object1 = unsafe.getObject(paramT, l);
          k = SchemaUtil.computeSizeFixed64ListNoTag((List<?>)object1);
          j = i;
          if (k > 0) {
            if (this.useCachedSizeField)
              unsafe.putInt(paramT, i4, k); 
            j = CodedOutputStream.computeTagSize(i1);
            j = i + j + CodedOutputStream.computeUInt32SizeNoTag(k) + k;
          } 
          break;
        case 45:
          object1 = unsafe.getObject(paramT, l);
          k = SchemaUtil.computeSizeFixed32ListNoTag((List<?>)object1);
          j = i;
          if (k > 0) {
            if (this.useCachedSizeField)
              unsafe.putInt(paramT, i4, k); 
            j = CodedOutputStream.computeTagSize(i1);
            j = i + j + CodedOutputStream.computeUInt32SizeNoTag(k) + k;
          } 
          break;
        case 44:
          object1 = unsafe.getObject(paramT, l);
          k = SchemaUtil.computeSizeEnumListNoTag((List<Integer>)object1);
          j = i;
          if (k > 0) {
            if (this.useCachedSizeField)
              unsafe.putInt(paramT, i4, k); 
            j = CodedOutputStream.computeTagSize(i1);
            j = i + j + CodedOutputStream.computeUInt32SizeNoTag(k) + k;
          } 
          break;
        case 43:
          object1 = unsafe.getObject(paramT, l);
          k = SchemaUtil.computeSizeUInt32ListNoTag((List<Integer>)object1);
          j = i;
          if (k > 0) {
            if (this.useCachedSizeField)
              unsafe.putInt(paramT, i4, k); 
            j = CodedOutputStream.computeTagSize(i1);
            j = i + j + CodedOutputStream.computeUInt32SizeNoTag(k) + k;
          } 
          break;
        case 42:
          object1 = unsafe.getObject(paramT, l);
          k = SchemaUtil.computeSizeBoolListNoTag((List<?>)object1);
          j = i;
          if (k > 0) {
            if (this.useCachedSizeField)
              unsafe.putInt(paramT, i4, k); 
            j = CodedOutputStream.computeTagSize(i1);
            j = i + j + CodedOutputStream.computeUInt32SizeNoTag(k) + k;
          } 
          break;
        case 41:
          object1 = unsafe.getObject(paramT, l);
          k = SchemaUtil.computeSizeFixed32ListNoTag((List<?>)object1);
          j = i;
          if (k > 0) {
            if (this.useCachedSizeField)
              unsafe.putInt(paramT, i4, k); 
            j = CodedOutputStream.computeTagSize(i1);
            j = i + j + CodedOutputStream.computeUInt32SizeNoTag(k) + k;
          } 
          break;
        case 40:
          object1 = unsafe.getObject(paramT, l);
          k = SchemaUtil.computeSizeFixed64ListNoTag((List<?>)object1);
          j = i;
          if (k > 0) {
            if (this.useCachedSizeField)
              unsafe.putInt(paramT, i4, k); 
            j = CodedOutputStream.computeTagSize(i1);
            j = i + j + CodedOutputStream.computeUInt32SizeNoTag(k) + k;
          } 
          break;
        case 39:
          object1 = unsafe.getObject(paramT, l);
          k = SchemaUtil.computeSizeInt32ListNoTag((List<Integer>)object1);
          j = i;
          if (k > 0) {
            if (this.useCachedSizeField)
              unsafe.putInt(paramT, i4, k); 
            j = CodedOutputStream.computeTagSize(i1);
            j = i + j + CodedOutputStream.computeUInt32SizeNoTag(k) + k;
          } 
          break;
        case 38:
          object1 = unsafe.getObject(paramT, l);
          k = SchemaUtil.computeSizeUInt64ListNoTag((List<Long>)object1);
          j = i;
          if (k > 0) {
            if (this.useCachedSizeField)
              unsafe.putInt(paramT, i4, k); 
            j = CodedOutputStream.computeTagSize(i1);
            j = i + j + CodedOutputStream.computeUInt32SizeNoTag(k) + k;
          } 
          break;
        case 37:
          object1 = unsafe.getObject(paramT, l);
          k = SchemaUtil.computeSizeInt64ListNoTag((List<Long>)object1);
          j = i;
          if (k > 0) {
            if (this.useCachedSizeField)
              unsafe.putInt(paramT, i4, k); 
            j = CodedOutputStream.computeTagSize(i1);
            j = i + j + CodedOutputStream.computeUInt32SizeNoTag(k) + k;
          } 
          break;
        case 36:
          object1 = unsafe.getObject(paramT, l);
          k = SchemaUtil.computeSizeFixed32ListNoTag((List<?>)object1);
          j = i;
          if (k > 0) {
            if (this.useCachedSizeField)
              unsafe.putInt(paramT, i4, k); 
            j = CodedOutputStream.computeTagSize(i1);
            j = i + j + CodedOutputStream.computeUInt32SizeNoTag(k) + k;
          } 
          break;
        case 35:
          object1 = unsafe.getObject(paramT, l);
          k = SchemaUtil.computeSizeFixed64ListNoTag((List<?>)object1);
          j = i;
          if (k > 0) {
            if (this.useCachedSizeField)
              unsafe.putInt(paramT, i4, k); 
            j = CodedOutputStream.computeTagSize(i1);
            j = i + j + CodedOutputStream.computeUInt32SizeNoTag(k) + k;
          } 
          break;
        case 34:
          object1 = unsafe.getObject(paramT, l);
          j = i + SchemaUtil.computeSizeSInt64List(i1, (List<Long>)object1, false);
          break;
        case 33:
          object1 = unsafe.getObject(paramT, l);
          j = i + SchemaUtil.computeSizeSInt32List(i1, (List<Integer>)object1, false);
          break;
        case 32:
          object1 = unsafe.getObject(paramT, l);
          j = i + SchemaUtil.computeSizeFixed64List(i1, (List<?>)object1, false);
          break;
        case 31:
          object1 = unsafe.getObject(paramT, l);
          j = i + SchemaUtil.computeSizeFixed32List(i1, (List<?>)object1, false);
          break;
        case 30:
          object1 = unsafe.getObject(paramT, l);
          j = i + SchemaUtil.computeSizeEnumList(i1, (List<Integer>)object1, false);
          break;
        case 29:
          object1 = unsafe.getObject(paramT, l);
          j = i + SchemaUtil.computeSizeUInt32List(i1, (List<Integer>)object1, false);
          break;
        case 28:
          object1 = unsafe.getObject(paramT, l);
          j = i + SchemaUtil.computeSizeByteStringList(i1, (List<ByteString>)object1);
          break;
        case 27:
          object1 = unsafe.getObject(paramT, l);
          object2 = getMessageFieldSchema(b);
          j = i + SchemaUtil.computeSizeMessageList(i1, (List<?>)object1, (Schema)object2);
          break;
        case 26:
          j = i + SchemaUtil.computeSizeStringList(i1, (List)unsafe.getObject(paramT, l));
          break;
        case 25:
          object1 = unsafe.getObject(paramT, l);
          j = i + SchemaUtil.computeSizeBoolList(i1, (List<?>)object1, false);
          break;
        case 24:
          object1 = unsafe.getObject(paramT, l);
          j = i + SchemaUtil.computeSizeFixed32List(i1, (List<?>)object1, false);
          break;
        case 23:
          object1 = unsafe.getObject(paramT, l);
          j = i + SchemaUtil.computeSizeFixed64List(i1, (List<?>)object1, false);
          break;
        case 22:
          object1 = unsafe.getObject(paramT, l);
          j = i + SchemaUtil.computeSizeInt32List(i1, (List<Integer>)object1, false);
          break;
        case 21:
          object1 = unsafe.getObject(paramT, l);
          j = i + SchemaUtil.computeSizeUInt64List(i1, (List<Long>)object1, false);
          break;
        case 20:
          object1 = unsafe.getObject(paramT, l);
          j = i + SchemaUtil.computeSizeInt64List(i1, (List<Long>)object1, false);
          break;
        case 19:
          object1 = unsafe.getObject(paramT, l);
          j = i + SchemaUtil.computeSizeFixed32List(i1, (List<?>)object1, false);
          break;
        case 18:
          object1 = unsafe.getObject(paramT, l);
          j = i + SchemaUtil.computeSizeFixed64List(i1, (List<?>)object1, false);
          break;
        case 17:
          j = i;
          if ((i8 & i6) != 0) {
            object2 = unsafe.getObject(paramT, l);
            object1 = getMessageFieldSchema(b);
            j = i + CodedOutputStream.computeGroupSize(i1, (MessageLite)object2, (Schema)object1);
          } 
          break;
        case 16:
          j = i;
          if ((i8 & i6) != 0)
            j = i + CodedOutputStream.computeSInt64Size(i1, unsafe.getLong(paramT, l)); 
          break;
        case 15:
          j = i;
          if ((i8 & i6) != 0)
            j = i + CodedOutputStream.computeSInt32Size(i1, unsafe.getInt(paramT, l)); 
          break;
        case 14:
          j = i;
          if ((i8 & i6) != 0)
            j = i + CodedOutputStream.computeSFixed64Size(i1, 0L); 
          break;
        case 13:
          j = i;
          if ((i8 & i6) != 0)
            j = i + CodedOutputStream.computeSFixed32Size(i1, 0); 
          break;
        case 12:
          j = i;
          if ((i8 & i6) != 0)
            j = i + CodedOutputStream.computeEnumSize(i1, unsafe.getInt(paramT, l)); 
          break;
        case 11:
          j = i;
          if ((i8 & i6) != 0)
            j = i + CodedOutputStream.computeUInt32Size(i1, unsafe.getInt(paramT, l)); 
          break;
        case 10:
          j = i;
          if ((i8 & i6) != 0) {
            object1 = unsafe.getObject(paramT, l);
            j = i + CodedOutputStream.computeBytesSize(i1, (ByteString)object1);
          } 
          break;
        case 9:
          j = i;
          if ((i8 & i6) != 0) {
            object1 = unsafe.getObject(paramT, l);
            j = i + SchemaUtil.computeSizeMessage(i1, object1, getMessageFieldSchema(b));
          } 
          break;
        case 8:
          j = i;
          if ((i8 & i6) != 0) {
            object1 = unsafe.getObject(paramT, l);
            if (object1 instanceof ByteString) {
              j = i + CodedOutputStream.computeBytesSize(i1, (ByteString)object1);
              break;
            } 
            j = i + CodedOutputStream.computeStringSize(i1, (String)object1);
          } 
          break;
        case 7:
          j = i;
          if ((i8 & i6) != 0)
            j = i + CodedOutputStream.computeBoolSize(i1, true); 
          break;
        case 6:
          j = i;
          if ((i8 & i6) != 0)
            j = i + CodedOutputStream.computeFixed32Size(i1, 0); 
          break;
        case 5:
          j = i;
          if ((i8 & i6) != 0)
            j = i + CodedOutputStream.computeFixed64Size(i1, 0L); 
          break;
        case 4:
          j = i;
          if ((i8 & i6) != 0)
            j = i + CodedOutputStream.computeInt32Size(i1, unsafe.getInt(paramT, l)); 
          break;
        case 3:
          j = i;
          if ((i8 & i6) != 0)
            j = i + CodedOutputStream.computeUInt64Size(i1, unsafe.getLong(paramT, l)); 
          break;
        case 2:
          j = i;
          if ((i8 & i6) != 0)
            j = i + CodedOutputStream.computeInt64Size(i1, unsafe.getLong(paramT, l)); 
          break;
        case 1:
          j = i;
          if ((i8 & i6) != 0)
            j = i + CodedOutputStream.computeFloatSize(i1, 0.0F); 
          break;
        case 0:
          j = i;
          if ((i8 & i6) != 0)
            j = i + CodedOutputStream.computeDoubleSize(i1, 0.0D); 
          break;
      } 
    } 
    int m = i + getUnknownFieldsSerializedSize(this.unknownFieldSchema, paramT);
    j = m;
    if (this.hasExtensions)
      j = m + this.extensionSchema.getExtensions(paramT).getSerializedSize(); 
    return j;
  }
  
  private int getSerializedSizeProto3(T paramT) {
    // Byte code:
    //   0: getstatic com/android/framework/protobuf/MessageSchema.UNSAFE : Lsun/misc/Unsafe;
    //   3: astore_2
    //   4: iconst_0
    //   5: istore_3
    //   6: iconst_0
    //   7: istore #4
    //   9: iload #4
    //   11: aload_0
    //   12: getfield buffer : [I
    //   15: arraylength
    //   16: if_icmpge -> 3012
    //   19: aload_0
    //   20: iload #4
    //   22: invokespecial typeAndOffsetAt : (I)I
    //   25: istore #5
    //   27: iload #5
    //   29: invokestatic type : (I)I
    //   32: istore #6
    //   34: aload_0
    //   35: iload #4
    //   37: invokespecial numberAt : (I)I
    //   40: istore #7
    //   42: iload #5
    //   44: invokestatic offset : (I)J
    //   47: lstore #8
    //   49: getstatic com/android/framework/protobuf/FieldType.DOUBLE_LIST_PACKED : Lcom/android/framework/protobuf/FieldType;
    //   52: astore #10
    //   54: iload #6
    //   56: aload #10
    //   58: invokevirtual id : ()I
    //   61: if_icmplt -> 96
    //   64: getstatic com/android/framework/protobuf/FieldType.SINT64_LIST_PACKED : Lcom/android/framework/protobuf/FieldType;
    //   67: astore #10
    //   69: iload #6
    //   71: aload #10
    //   73: invokevirtual id : ()I
    //   76: if_icmpgt -> 96
    //   79: aload_0
    //   80: getfield buffer : [I
    //   83: iload #4
    //   85: iconst_2
    //   86: iadd
    //   87: iaload
    //   88: ldc 1048575
    //   90: iand
    //   91: istore #5
    //   93: goto -> 99
    //   96: iconst_0
    //   97: istore #5
    //   99: iload #6
    //   101: tableswitch default -> 392, 0 -> 2980, 1 -> 2954, 2 -> 2923, 3 -> 2892, 4 -> 2861, 5 -> 2835, 6 -> 2809, 7 -> 2783, 8 -> 2720, 9 -> 2679, 10 -> 2641, 11 -> 2610, 12 -> 2579, 13 -> 2553, 14 -> 2527, 15 -> 2496, 16 -> 2465, 17 -> 2417, 18 -> 2398, 19 -> 2379, 20 -> 2360, 21 -> 2341, 22 -> 2318, 23 -> 2299, 24 -> 2280, 25 -> 2261, 26 -> 2243, 27 -> 2211, 28 -> 2189, 29 -> 2166, 30 -> 2143, 31 -> 2124, 32 -> 2105, 33 -> 2082, 34 -> 2063, 35 -> 1994, 36 -> 1925, 37 -> 1856, 38 -> 1787, 39 -> 1718, 40 -> 1649, 41 -> 1580, 42 -> 1511, 43 -> 1442, 44 -> 1373, 45 -> 1304, 46 -> 1235, 47 -> 1166, 48 -> 1097, 49 -> 1065, 50 -> 1023, 51 -> 995, 52 -> 967, 53 -> 934, 54 -> 901, 55 -> 868, 56 -> 840, 57 -> 812, 58 -> 784, 59 -> 719, 60 -> 676, 61 -> 636, 62 -> 603, 63 -> 570, 64 -> 542, 65 -> 514, 66 -> 481, 67 -> 448, 68 -> 398
    //   392: iload_3
    //   393: istore #6
    //   395: goto -> 3003
    //   398: iload_3
    //   399: istore #6
    //   401: aload_0
    //   402: aload_1
    //   403: iload #7
    //   405: iload #4
    //   407: invokespecial isOneofPresent : (Ljava/lang/Object;II)Z
    //   410: ifeq -> 3003
    //   413: aload_1
    //   414: lload #8
    //   416: invokestatic getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   419: checkcast com/android/framework/protobuf/MessageLite
    //   422: astore #11
    //   424: aload_0
    //   425: iload #4
    //   427: invokespecial getMessageFieldSchema : (I)Lcom/android/framework/protobuf/Schema;
    //   430: astore #10
    //   432: iload_3
    //   433: iload #7
    //   435: aload #11
    //   437: aload #10
    //   439: invokestatic computeGroupSize : (ILcom/android/framework/protobuf/MessageLite;Lcom/android/framework/protobuf/Schema;)I
    //   442: iadd
    //   443: istore #6
    //   445: goto -> 3003
    //   448: iload_3
    //   449: istore #6
    //   451: aload_0
    //   452: aload_1
    //   453: iload #7
    //   455: iload #4
    //   457: invokespecial isOneofPresent : (Ljava/lang/Object;II)Z
    //   460: ifeq -> 3003
    //   463: iload_3
    //   464: iload #7
    //   466: aload_1
    //   467: lload #8
    //   469: invokestatic oneofLongAt : (Ljava/lang/Object;J)J
    //   472: invokestatic computeSInt64Size : (IJ)I
    //   475: iadd
    //   476: istore #6
    //   478: goto -> 3003
    //   481: iload_3
    //   482: istore #6
    //   484: aload_0
    //   485: aload_1
    //   486: iload #7
    //   488: iload #4
    //   490: invokespecial isOneofPresent : (Ljava/lang/Object;II)Z
    //   493: ifeq -> 3003
    //   496: iload_3
    //   497: iload #7
    //   499: aload_1
    //   500: lload #8
    //   502: invokestatic oneofIntAt : (Ljava/lang/Object;J)I
    //   505: invokestatic computeSInt32Size : (II)I
    //   508: iadd
    //   509: istore #6
    //   511: goto -> 3003
    //   514: iload_3
    //   515: istore #6
    //   517: aload_0
    //   518: aload_1
    //   519: iload #7
    //   521: iload #4
    //   523: invokespecial isOneofPresent : (Ljava/lang/Object;II)Z
    //   526: ifeq -> 3003
    //   529: iload_3
    //   530: iload #7
    //   532: lconst_0
    //   533: invokestatic computeSFixed64Size : (IJ)I
    //   536: iadd
    //   537: istore #6
    //   539: goto -> 3003
    //   542: iload_3
    //   543: istore #6
    //   545: aload_0
    //   546: aload_1
    //   547: iload #7
    //   549: iload #4
    //   551: invokespecial isOneofPresent : (Ljava/lang/Object;II)Z
    //   554: ifeq -> 3003
    //   557: iload_3
    //   558: iload #7
    //   560: iconst_0
    //   561: invokestatic computeSFixed32Size : (II)I
    //   564: iadd
    //   565: istore #6
    //   567: goto -> 3003
    //   570: iload_3
    //   571: istore #6
    //   573: aload_0
    //   574: aload_1
    //   575: iload #7
    //   577: iload #4
    //   579: invokespecial isOneofPresent : (Ljava/lang/Object;II)Z
    //   582: ifeq -> 3003
    //   585: iload_3
    //   586: iload #7
    //   588: aload_1
    //   589: lload #8
    //   591: invokestatic oneofIntAt : (Ljava/lang/Object;J)I
    //   594: invokestatic computeEnumSize : (II)I
    //   597: iadd
    //   598: istore #6
    //   600: goto -> 3003
    //   603: iload_3
    //   604: istore #6
    //   606: aload_0
    //   607: aload_1
    //   608: iload #7
    //   610: iload #4
    //   612: invokespecial isOneofPresent : (Ljava/lang/Object;II)Z
    //   615: ifeq -> 3003
    //   618: iload_3
    //   619: iload #7
    //   621: aload_1
    //   622: lload #8
    //   624: invokestatic oneofIntAt : (Ljava/lang/Object;J)I
    //   627: invokestatic computeUInt32Size : (II)I
    //   630: iadd
    //   631: istore #6
    //   633: goto -> 3003
    //   636: iload_3
    //   637: istore #6
    //   639: aload_0
    //   640: aload_1
    //   641: iload #7
    //   643: iload #4
    //   645: invokespecial isOneofPresent : (Ljava/lang/Object;II)Z
    //   648: ifeq -> 3003
    //   651: aload_1
    //   652: lload #8
    //   654: invokestatic getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   657: checkcast com/android/framework/protobuf/ByteString
    //   660: astore #10
    //   662: iload_3
    //   663: iload #7
    //   665: aload #10
    //   667: invokestatic computeBytesSize : (ILcom/android/framework/protobuf/ByteString;)I
    //   670: iadd
    //   671: istore #6
    //   673: goto -> 3003
    //   676: iload_3
    //   677: istore #6
    //   679: aload_0
    //   680: aload_1
    //   681: iload #7
    //   683: iload #4
    //   685: invokespecial isOneofPresent : (Ljava/lang/Object;II)Z
    //   688: ifeq -> 3003
    //   691: aload_1
    //   692: lload #8
    //   694: invokestatic getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   697: astore #10
    //   699: iload_3
    //   700: iload #7
    //   702: aload #10
    //   704: aload_0
    //   705: iload #4
    //   707: invokespecial getMessageFieldSchema : (I)Lcom/android/framework/protobuf/Schema;
    //   710: invokestatic computeSizeMessage : (ILjava/lang/Object;Lcom/android/framework/protobuf/Schema;)I
    //   713: iadd
    //   714: istore #6
    //   716: goto -> 3003
    //   719: iload_3
    //   720: istore #6
    //   722: aload_0
    //   723: aload_1
    //   724: iload #7
    //   726: iload #4
    //   728: invokespecial isOneofPresent : (Ljava/lang/Object;II)Z
    //   731: ifeq -> 3003
    //   734: aload_1
    //   735: lload #8
    //   737: invokestatic getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   740: astore #10
    //   742: aload #10
    //   744: instanceof com/android/framework/protobuf/ByteString
    //   747: ifeq -> 767
    //   750: iload_3
    //   751: iload #7
    //   753: aload #10
    //   755: checkcast com/android/framework/protobuf/ByteString
    //   758: invokestatic computeBytesSize : (ILcom/android/framework/protobuf/ByteString;)I
    //   761: iadd
    //   762: istore #6
    //   764: goto -> 781
    //   767: iload_3
    //   768: iload #7
    //   770: aload #10
    //   772: checkcast java/lang/String
    //   775: invokestatic computeStringSize : (ILjava/lang/String;)I
    //   778: iadd
    //   779: istore #6
    //   781: goto -> 3003
    //   784: iload_3
    //   785: istore #6
    //   787: aload_0
    //   788: aload_1
    //   789: iload #7
    //   791: iload #4
    //   793: invokespecial isOneofPresent : (Ljava/lang/Object;II)Z
    //   796: ifeq -> 3003
    //   799: iload_3
    //   800: iload #7
    //   802: iconst_1
    //   803: invokestatic computeBoolSize : (IZ)I
    //   806: iadd
    //   807: istore #6
    //   809: goto -> 3003
    //   812: iload_3
    //   813: istore #6
    //   815: aload_0
    //   816: aload_1
    //   817: iload #7
    //   819: iload #4
    //   821: invokespecial isOneofPresent : (Ljava/lang/Object;II)Z
    //   824: ifeq -> 3003
    //   827: iload_3
    //   828: iload #7
    //   830: iconst_0
    //   831: invokestatic computeFixed32Size : (II)I
    //   834: iadd
    //   835: istore #6
    //   837: goto -> 3003
    //   840: iload_3
    //   841: istore #6
    //   843: aload_0
    //   844: aload_1
    //   845: iload #7
    //   847: iload #4
    //   849: invokespecial isOneofPresent : (Ljava/lang/Object;II)Z
    //   852: ifeq -> 3003
    //   855: iload_3
    //   856: iload #7
    //   858: lconst_0
    //   859: invokestatic computeFixed64Size : (IJ)I
    //   862: iadd
    //   863: istore #6
    //   865: goto -> 3003
    //   868: iload_3
    //   869: istore #6
    //   871: aload_0
    //   872: aload_1
    //   873: iload #7
    //   875: iload #4
    //   877: invokespecial isOneofPresent : (Ljava/lang/Object;II)Z
    //   880: ifeq -> 3003
    //   883: iload_3
    //   884: iload #7
    //   886: aload_1
    //   887: lload #8
    //   889: invokestatic oneofIntAt : (Ljava/lang/Object;J)I
    //   892: invokestatic computeInt32Size : (II)I
    //   895: iadd
    //   896: istore #6
    //   898: goto -> 3003
    //   901: iload_3
    //   902: istore #6
    //   904: aload_0
    //   905: aload_1
    //   906: iload #7
    //   908: iload #4
    //   910: invokespecial isOneofPresent : (Ljava/lang/Object;II)Z
    //   913: ifeq -> 3003
    //   916: iload_3
    //   917: iload #7
    //   919: aload_1
    //   920: lload #8
    //   922: invokestatic oneofLongAt : (Ljava/lang/Object;J)J
    //   925: invokestatic computeUInt64Size : (IJ)I
    //   928: iadd
    //   929: istore #6
    //   931: goto -> 3003
    //   934: iload_3
    //   935: istore #6
    //   937: aload_0
    //   938: aload_1
    //   939: iload #7
    //   941: iload #4
    //   943: invokespecial isOneofPresent : (Ljava/lang/Object;II)Z
    //   946: ifeq -> 3003
    //   949: iload_3
    //   950: iload #7
    //   952: aload_1
    //   953: lload #8
    //   955: invokestatic oneofLongAt : (Ljava/lang/Object;J)J
    //   958: invokestatic computeInt64Size : (IJ)I
    //   961: iadd
    //   962: istore #6
    //   964: goto -> 3003
    //   967: iload_3
    //   968: istore #6
    //   970: aload_0
    //   971: aload_1
    //   972: iload #7
    //   974: iload #4
    //   976: invokespecial isOneofPresent : (Ljava/lang/Object;II)Z
    //   979: ifeq -> 3003
    //   982: iload_3
    //   983: iload #7
    //   985: fconst_0
    //   986: invokestatic computeFloatSize : (IF)I
    //   989: iadd
    //   990: istore #6
    //   992: goto -> 3003
    //   995: iload_3
    //   996: istore #6
    //   998: aload_0
    //   999: aload_1
    //   1000: iload #7
    //   1002: iload #4
    //   1004: invokespecial isOneofPresent : (Ljava/lang/Object;II)Z
    //   1007: ifeq -> 3003
    //   1010: iload_3
    //   1011: iload #7
    //   1013: dconst_0
    //   1014: invokestatic computeDoubleSize : (ID)I
    //   1017: iadd
    //   1018: istore #6
    //   1020: goto -> 3003
    //   1023: aload_0
    //   1024: getfield mapFieldSchema : Lcom/android/framework/protobuf/MapFieldSchema;
    //   1027: astore #10
    //   1029: aload_1
    //   1030: lload #8
    //   1032: invokestatic getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   1035: astore #11
    //   1037: aload_0
    //   1038: iload #4
    //   1040: invokespecial getMapFieldDefaultEntry : (I)Ljava/lang/Object;
    //   1043: astore #12
    //   1045: iload_3
    //   1046: aload #10
    //   1048: iload #7
    //   1050: aload #11
    //   1052: aload #12
    //   1054: invokeinterface getSerializedSize : (ILjava/lang/Object;Ljava/lang/Object;)I
    //   1059: iadd
    //   1060: istore #6
    //   1062: goto -> 3003
    //   1065: aload_1
    //   1066: lload #8
    //   1068: invokestatic listAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   1071: astore #11
    //   1073: aload_0
    //   1074: iload #4
    //   1076: invokespecial getMessageFieldSchema : (I)Lcom/android/framework/protobuf/Schema;
    //   1079: astore #10
    //   1081: iload_3
    //   1082: iload #7
    //   1084: aload #11
    //   1086: aload #10
    //   1088: invokestatic computeSizeGroupList : (ILjava/util/List;Lcom/android/framework/protobuf/Schema;)I
    //   1091: iadd
    //   1092: istore #6
    //   1094: goto -> 3003
    //   1097: aload_2
    //   1098: aload_1
    //   1099: lload #8
    //   1101: invokevirtual getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   1104: checkcast java/util/List
    //   1107: astore #10
    //   1109: aload #10
    //   1111: invokestatic computeSizeSInt64ListNoTag : (Ljava/util/List;)I
    //   1114: istore #13
    //   1116: iload_3
    //   1117: istore #6
    //   1119: iload #13
    //   1121: ifle -> 3003
    //   1124: aload_0
    //   1125: getfield useCachedSizeField : Z
    //   1128: ifeq -> 1141
    //   1131: aload_2
    //   1132: aload_1
    //   1133: iload #5
    //   1135: i2l
    //   1136: iload #13
    //   1138: invokevirtual putInt : (Ljava/lang/Object;JI)V
    //   1141: iload #7
    //   1143: invokestatic computeTagSize : (I)I
    //   1146: istore #6
    //   1148: iload_3
    //   1149: iload #6
    //   1151: iload #13
    //   1153: invokestatic computeUInt32SizeNoTag : (I)I
    //   1156: iadd
    //   1157: iload #13
    //   1159: iadd
    //   1160: iadd
    //   1161: istore #6
    //   1163: goto -> 3003
    //   1166: aload_2
    //   1167: aload_1
    //   1168: lload #8
    //   1170: invokevirtual getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   1173: checkcast java/util/List
    //   1176: astore #10
    //   1178: aload #10
    //   1180: invokestatic computeSizeSInt32ListNoTag : (Ljava/util/List;)I
    //   1183: istore #13
    //   1185: iload_3
    //   1186: istore #6
    //   1188: iload #13
    //   1190: ifle -> 3003
    //   1193: aload_0
    //   1194: getfield useCachedSizeField : Z
    //   1197: ifeq -> 1210
    //   1200: aload_2
    //   1201: aload_1
    //   1202: iload #5
    //   1204: i2l
    //   1205: iload #13
    //   1207: invokevirtual putInt : (Ljava/lang/Object;JI)V
    //   1210: iload #7
    //   1212: invokestatic computeTagSize : (I)I
    //   1215: istore #6
    //   1217: iload_3
    //   1218: iload #6
    //   1220: iload #13
    //   1222: invokestatic computeUInt32SizeNoTag : (I)I
    //   1225: iadd
    //   1226: iload #13
    //   1228: iadd
    //   1229: iadd
    //   1230: istore #6
    //   1232: goto -> 3003
    //   1235: aload_2
    //   1236: aload_1
    //   1237: lload #8
    //   1239: invokevirtual getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   1242: checkcast java/util/List
    //   1245: astore #10
    //   1247: aload #10
    //   1249: invokestatic computeSizeFixed64ListNoTag : (Ljava/util/List;)I
    //   1252: istore #13
    //   1254: iload_3
    //   1255: istore #6
    //   1257: iload #13
    //   1259: ifle -> 3003
    //   1262: aload_0
    //   1263: getfield useCachedSizeField : Z
    //   1266: ifeq -> 1279
    //   1269: aload_2
    //   1270: aload_1
    //   1271: iload #5
    //   1273: i2l
    //   1274: iload #13
    //   1276: invokevirtual putInt : (Ljava/lang/Object;JI)V
    //   1279: iload #7
    //   1281: invokestatic computeTagSize : (I)I
    //   1284: istore #6
    //   1286: iload_3
    //   1287: iload #6
    //   1289: iload #13
    //   1291: invokestatic computeUInt32SizeNoTag : (I)I
    //   1294: iadd
    //   1295: iload #13
    //   1297: iadd
    //   1298: iadd
    //   1299: istore #6
    //   1301: goto -> 3003
    //   1304: aload_2
    //   1305: aload_1
    //   1306: lload #8
    //   1308: invokevirtual getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   1311: checkcast java/util/List
    //   1314: astore #10
    //   1316: aload #10
    //   1318: invokestatic computeSizeFixed32ListNoTag : (Ljava/util/List;)I
    //   1321: istore #13
    //   1323: iload_3
    //   1324: istore #6
    //   1326: iload #13
    //   1328: ifle -> 3003
    //   1331: aload_0
    //   1332: getfield useCachedSizeField : Z
    //   1335: ifeq -> 1348
    //   1338: aload_2
    //   1339: aload_1
    //   1340: iload #5
    //   1342: i2l
    //   1343: iload #13
    //   1345: invokevirtual putInt : (Ljava/lang/Object;JI)V
    //   1348: iload #7
    //   1350: invokestatic computeTagSize : (I)I
    //   1353: istore #6
    //   1355: iload_3
    //   1356: iload #6
    //   1358: iload #13
    //   1360: invokestatic computeUInt32SizeNoTag : (I)I
    //   1363: iadd
    //   1364: iload #13
    //   1366: iadd
    //   1367: iadd
    //   1368: istore #6
    //   1370: goto -> 3003
    //   1373: aload_2
    //   1374: aload_1
    //   1375: lload #8
    //   1377: invokevirtual getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   1380: checkcast java/util/List
    //   1383: astore #10
    //   1385: aload #10
    //   1387: invokestatic computeSizeEnumListNoTag : (Ljava/util/List;)I
    //   1390: istore #13
    //   1392: iload_3
    //   1393: istore #6
    //   1395: iload #13
    //   1397: ifle -> 3003
    //   1400: aload_0
    //   1401: getfield useCachedSizeField : Z
    //   1404: ifeq -> 1417
    //   1407: aload_2
    //   1408: aload_1
    //   1409: iload #5
    //   1411: i2l
    //   1412: iload #13
    //   1414: invokevirtual putInt : (Ljava/lang/Object;JI)V
    //   1417: iload #7
    //   1419: invokestatic computeTagSize : (I)I
    //   1422: istore #6
    //   1424: iload_3
    //   1425: iload #6
    //   1427: iload #13
    //   1429: invokestatic computeUInt32SizeNoTag : (I)I
    //   1432: iadd
    //   1433: iload #13
    //   1435: iadd
    //   1436: iadd
    //   1437: istore #6
    //   1439: goto -> 3003
    //   1442: aload_2
    //   1443: aload_1
    //   1444: lload #8
    //   1446: invokevirtual getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   1449: checkcast java/util/List
    //   1452: astore #10
    //   1454: aload #10
    //   1456: invokestatic computeSizeUInt32ListNoTag : (Ljava/util/List;)I
    //   1459: istore #13
    //   1461: iload_3
    //   1462: istore #6
    //   1464: iload #13
    //   1466: ifle -> 3003
    //   1469: aload_0
    //   1470: getfield useCachedSizeField : Z
    //   1473: ifeq -> 1486
    //   1476: aload_2
    //   1477: aload_1
    //   1478: iload #5
    //   1480: i2l
    //   1481: iload #13
    //   1483: invokevirtual putInt : (Ljava/lang/Object;JI)V
    //   1486: iload #7
    //   1488: invokestatic computeTagSize : (I)I
    //   1491: istore #6
    //   1493: iload_3
    //   1494: iload #6
    //   1496: iload #13
    //   1498: invokestatic computeUInt32SizeNoTag : (I)I
    //   1501: iadd
    //   1502: iload #13
    //   1504: iadd
    //   1505: iadd
    //   1506: istore #6
    //   1508: goto -> 3003
    //   1511: aload_2
    //   1512: aload_1
    //   1513: lload #8
    //   1515: invokevirtual getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   1518: checkcast java/util/List
    //   1521: astore #10
    //   1523: aload #10
    //   1525: invokestatic computeSizeBoolListNoTag : (Ljava/util/List;)I
    //   1528: istore #13
    //   1530: iload_3
    //   1531: istore #6
    //   1533: iload #13
    //   1535: ifle -> 3003
    //   1538: aload_0
    //   1539: getfield useCachedSizeField : Z
    //   1542: ifeq -> 1555
    //   1545: aload_2
    //   1546: aload_1
    //   1547: iload #5
    //   1549: i2l
    //   1550: iload #13
    //   1552: invokevirtual putInt : (Ljava/lang/Object;JI)V
    //   1555: iload #7
    //   1557: invokestatic computeTagSize : (I)I
    //   1560: istore #6
    //   1562: iload_3
    //   1563: iload #6
    //   1565: iload #13
    //   1567: invokestatic computeUInt32SizeNoTag : (I)I
    //   1570: iadd
    //   1571: iload #13
    //   1573: iadd
    //   1574: iadd
    //   1575: istore #6
    //   1577: goto -> 3003
    //   1580: aload_2
    //   1581: aload_1
    //   1582: lload #8
    //   1584: invokevirtual getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   1587: checkcast java/util/List
    //   1590: astore #10
    //   1592: aload #10
    //   1594: invokestatic computeSizeFixed32ListNoTag : (Ljava/util/List;)I
    //   1597: istore #13
    //   1599: iload_3
    //   1600: istore #6
    //   1602: iload #13
    //   1604: ifle -> 3003
    //   1607: aload_0
    //   1608: getfield useCachedSizeField : Z
    //   1611: ifeq -> 1624
    //   1614: aload_2
    //   1615: aload_1
    //   1616: iload #5
    //   1618: i2l
    //   1619: iload #13
    //   1621: invokevirtual putInt : (Ljava/lang/Object;JI)V
    //   1624: iload #7
    //   1626: invokestatic computeTagSize : (I)I
    //   1629: istore #6
    //   1631: iload_3
    //   1632: iload #6
    //   1634: iload #13
    //   1636: invokestatic computeUInt32SizeNoTag : (I)I
    //   1639: iadd
    //   1640: iload #13
    //   1642: iadd
    //   1643: iadd
    //   1644: istore #6
    //   1646: goto -> 3003
    //   1649: aload_2
    //   1650: aload_1
    //   1651: lload #8
    //   1653: invokevirtual getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   1656: checkcast java/util/List
    //   1659: astore #10
    //   1661: aload #10
    //   1663: invokestatic computeSizeFixed64ListNoTag : (Ljava/util/List;)I
    //   1666: istore #13
    //   1668: iload_3
    //   1669: istore #6
    //   1671: iload #13
    //   1673: ifle -> 3003
    //   1676: aload_0
    //   1677: getfield useCachedSizeField : Z
    //   1680: ifeq -> 1693
    //   1683: aload_2
    //   1684: aload_1
    //   1685: iload #5
    //   1687: i2l
    //   1688: iload #13
    //   1690: invokevirtual putInt : (Ljava/lang/Object;JI)V
    //   1693: iload #7
    //   1695: invokestatic computeTagSize : (I)I
    //   1698: istore #6
    //   1700: iload_3
    //   1701: iload #6
    //   1703: iload #13
    //   1705: invokestatic computeUInt32SizeNoTag : (I)I
    //   1708: iadd
    //   1709: iload #13
    //   1711: iadd
    //   1712: iadd
    //   1713: istore #6
    //   1715: goto -> 3003
    //   1718: aload_2
    //   1719: aload_1
    //   1720: lload #8
    //   1722: invokevirtual getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   1725: checkcast java/util/List
    //   1728: astore #10
    //   1730: aload #10
    //   1732: invokestatic computeSizeInt32ListNoTag : (Ljava/util/List;)I
    //   1735: istore #13
    //   1737: iload_3
    //   1738: istore #6
    //   1740: iload #13
    //   1742: ifle -> 3003
    //   1745: aload_0
    //   1746: getfield useCachedSizeField : Z
    //   1749: ifeq -> 1762
    //   1752: aload_2
    //   1753: aload_1
    //   1754: iload #5
    //   1756: i2l
    //   1757: iload #13
    //   1759: invokevirtual putInt : (Ljava/lang/Object;JI)V
    //   1762: iload #7
    //   1764: invokestatic computeTagSize : (I)I
    //   1767: istore #6
    //   1769: iload_3
    //   1770: iload #6
    //   1772: iload #13
    //   1774: invokestatic computeUInt32SizeNoTag : (I)I
    //   1777: iadd
    //   1778: iload #13
    //   1780: iadd
    //   1781: iadd
    //   1782: istore #6
    //   1784: goto -> 3003
    //   1787: aload_2
    //   1788: aload_1
    //   1789: lload #8
    //   1791: invokevirtual getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   1794: checkcast java/util/List
    //   1797: astore #10
    //   1799: aload #10
    //   1801: invokestatic computeSizeUInt64ListNoTag : (Ljava/util/List;)I
    //   1804: istore #13
    //   1806: iload_3
    //   1807: istore #6
    //   1809: iload #13
    //   1811: ifle -> 3003
    //   1814: aload_0
    //   1815: getfield useCachedSizeField : Z
    //   1818: ifeq -> 1831
    //   1821: aload_2
    //   1822: aload_1
    //   1823: iload #5
    //   1825: i2l
    //   1826: iload #13
    //   1828: invokevirtual putInt : (Ljava/lang/Object;JI)V
    //   1831: iload #7
    //   1833: invokestatic computeTagSize : (I)I
    //   1836: istore #6
    //   1838: iload_3
    //   1839: iload #6
    //   1841: iload #13
    //   1843: invokestatic computeUInt32SizeNoTag : (I)I
    //   1846: iadd
    //   1847: iload #13
    //   1849: iadd
    //   1850: iadd
    //   1851: istore #6
    //   1853: goto -> 3003
    //   1856: aload_2
    //   1857: aload_1
    //   1858: lload #8
    //   1860: invokevirtual getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   1863: checkcast java/util/List
    //   1866: astore #10
    //   1868: aload #10
    //   1870: invokestatic computeSizeInt64ListNoTag : (Ljava/util/List;)I
    //   1873: istore #13
    //   1875: iload_3
    //   1876: istore #6
    //   1878: iload #13
    //   1880: ifle -> 3003
    //   1883: aload_0
    //   1884: getfield useCachedSizeField : Z
    //   1887: ifeq -> 1900
    //   1890: aload_2
    //   1891: aload_1
    //   1892: iload #5
    //   1894: i2l
    //   1895: iload #13
    //   1897: invokevirtual putInt : (Ljava/lang/Object;JI)V
    //   1900: iload #7
    //   1902: invokestatic computeTagSize : (I)I
    //   1905: istore #6
    //   1907: iload_3
    //   1908: iload #6
    //   1910: iload #13
    //   1912: invokestatic computeUInt32SizeNoTag : (I)I
    //   1915: iadd
    //   1916: iload #13
    //   1918: iadd
    //   1919: iadd
    //   1920: istore #6
    //   1922: goto -> 3003
    //   1925: aload_2
    //   1926: aload_1
    //   1927: lload #8
    //   1929: invokevirtual getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   1932: checkcast java/util/List
    //   1935: astore #10
    //   1937: aload #10
    //   1939: invokestatic computeSizeFixed32ListNoTag : (Ljava/util/List;)I
    //   1942: istore #13
    //   1944: iload_3
    //   1945: istore #6
    //   1947: iload #13
    //   1949: ifle -> 3003
    //   1952: aload_0
    //   1953: getfield useCachedSizeField : Z
    //   1956: ifeq -> 1969
    //   1959: aload_2
    //   1960: aload_1
    //   1961: iload #5
    //   1963: i2l
    //   1964: iload #13
    //   1966: invokevirtual putInt : (Ljava/lang/Object;JI)V
    //   1969: iload #7
    //   1971: invokestatic computeTagSize : (I)I
    //   1974: istore #6
    //   1976: iload_3
    //   1977: iload #6
    //   1979: iload #13
    //   1981: invokestatic computeUInt32SizeNoTag : (I)I
    //   1984: iadd
    //   1985: iload #13
    //   1987: iadd
    //   1988: iadd
    //   1989: istore #6
    //   1991: goto -> 3003
    //   1994: aload_2
    //   1995: aload_1
    //   1996: lload #8
    //   1998: invokevirtual getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   2001: checkcast java/util/List
    //   2004: astore #10
    //   2006: aload #10
    //   2008: invokestatic computeSizeFixed64ListNoTag : (Ljava/util/List;)I
    //   2011: istore #13
    //   2013: iload_3
    //   2014: istore #6
    //   2016: iload #13
    //   2018: ifle -> 3003
    //   2021: aload_0
    //   2022: getfield useCachedSizeField : Z
    //   2025: ifeq -> 2038
    //   2028: aload_2
    //   2029: aload_1
    //   2030: iload #5
    //   2032: i2l
    //   2033: iload #13
    //   2035: invokevirtual putInt : (Ljava/lang/Object;JI)V
    //   2038: iload #7
    //   2040: invokestatic computeTagSize : (I)I
    //   2043: istore #6
    //   2045: iload_3
    //   2046: iload #6
    //   2048: iload #13
    //   2050: invokestatic computeUInt32SizeNoTag : (I)I
    //   2053: iadd
    //   2054: iload #13
    //   2056: iadd
    //   2057: iadd
    //   2058: istore #6
    //   2060: goto -> 3003
    //   2063: iload_3
    //   2064: iload #7
    //   2066: aload_1
    //   2067: lload #8
    //   2069: invokestatic listAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2072: iconst_0
    //   2073: invokestatic computeSizeSInt64List : (ILjava/util/List;Z)I
    //   2076: iadd
    //   2077: istore #6
    //   2079: goto -> 3003
    //   2082: aload_1
    //   2083: lload #8
    //   2085: invokestatic listAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2088: astore #10
    //   2090: iload_3
    //   2091: iload #7
    //   2093: aload #10
    //   2095: iconst_0
    //   2096: invokestatic computeSizeSInt32List : (ILjava/util/List;Z)I
    //   2099: iadd
    //   2100: istore #6
    //   2102: goto -> 3003
    //   2105: iload_3
    //   2106: iload #7
    //   2108: aload_1
    //   2109: lload #8
    //   2111: invokestatic listAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2114: iconst_0
    //   2115: invokestatic computeSizeFixed64List : (ILjava/util/List;Z)I
    //   2118: iadd
    //   2119: istore #6
    //   2121: goto -> 3003
    //   2124: iload_3
    //   2125: iload #7
    //   2127: aload_1
    //   2128: lload #8
    //   2130: invokestatic listAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2133: iconst_0
    //   2134: invokestatic computeSizeFixed32List : (ILjava/util/List;Z)I
    //   2137: iadd
    //   2138: istore #6
    //   2140: goto -> 3003
    //   2143: aload_1
    //   2144: lload #8
    //   2146: invokestatic listAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2149: astore #10
    //   2151: iload_3
    //   2152: iload #7
    //   2154: aload #10
    //   2156: iconst_0
    //   2157: invokestatic computeSizeEnumList : (ILjava/util/List;Z)I
    //   2160: iadd
    //   2161: istore #6
    //   2163: goto -> 3003
    //   2166: aload_1
    //   2167: lload #8
    //   2169: invokestatic listAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2172: astore #10
    //   2174: iload_3
    //   2175: iload #7
    //   2177: aload #10
    //   2179: iconst_0
    //   2180: invokestatic computeSizeUInt32List : (ILjava/util/List;Z)I
    //   2183: iadd
    //   2184: istore #6
    //   2186: goto -> 3003
    //   2189: aload_1
    //   2190: lload #8
    //   2192: invokestatic listAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2195: astore #10
    //   2197: iload_3
    //   2198: iload #7
    //   2200: aload #10
    //   2202: invokestatic computeSizeByteStringList : (ILjava/util/List;)I
    //   2205: iadd
    //   2206: istore #6
    //   2208: goto -> 3003
    //   2211: aload_1
    //   2212: lload #8
    //   2214: invokestatic listAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2217: astore #11
    //   2219: aload_0
    //   2220: iload #4
    //   2222: invokespecial getMessageFieldSchema : (I)Lcom/android/framework/protobuf/Schema;
    //   2225: astore #10
    //   2227: iload_3
    //   2228: iload #7
    //   2230: aload #11
    //   2232: aload #10
    //   2234: invokestatic computeSizeMessageList : (ILjava/util/List;Lcom/android/framework/protobuf/Schema;)I
    //   2237: iadd
    //   2238: istore #6
    //   2240: goto -> 3003
    //   2243: iload_3
    //   2244: iload #7
    //   2246: aload_1
    //   2247: lload #8
    //   2249: invokestatic listAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2252: invokestatic computeSizeStringList : (ILjava/util/List;)I
    //   2255: iadd
    //   2256: istore #6
    //   2258: goto -> 3003
    //   2261: iload_3
    //   2262: iload #7
    //   2264: aload_1
    //   2265: lload #8
    //   2267: invokestatic listAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2270: iconst_0
    //   2271: invokestatic computeSizeBoolList : (ILjava/util/List;Z)I
    //   2274: iadd
    //   2275: istore #6
    //   2277: goto -> 3003
    //   2280: iload_3
    //   2281: iload #7
    //   2283: aload_1
    //   2284: lload #8
    //   2286: invokestatic listAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2289: iconst_0
    //   2290: invokestatic computeSizeFixed32List : (ILjava/util/List;Z)I
    //   2293: iadd
    //   2294: istore #6
    //   2296: goto -> 3003
    //   2299: iload_3
    //   2300: iload #7
    //   2302: aload_1
    //   2303: lload #8
    //   2305: invokestatic listAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2308: iconst_0
    //   2309: invokestatic computeSizeFixed64List : (ILjava/util/List;Z)I
    //   2312: iadd
    //   2313: istore #6
    //   2315: goto -> 3003
    //   2318: aload_1
    //   2319: lload #8
    //   2321: invokestatic listAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2324: astore #10
    //   2326: iload_3
    //   2327: iload #7
    //   2329: aload #10
    //   2331: iconst_0
    //   2332: invokestatic computeSizeInt32List : (ILjava/util/List;Z)I
    //   2335: iadd
    //   2336: istore #6
    //   2338: goto -> 3003
    //   2341: iload_3
    //   2342: iload #7
    //   2344: aload_1
    //   2345: lload #8
    //   2347: invokestatic listAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2350: iconst_0
    //   2351: invokestatic computeSizeUInt64List : (ILjava/util/List;Z)I
    //   2354: iadd
    //   2355: istore #6
    //   2357: goto -> 3003
    //   2360: iload_3
    //   2361: iload #7
    //   2363: aload_1
    //   2364: lload #8
    //   2366: invokestatic listAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2369: iconst_0
    //   2370: invokestatic computeSizeInt64List : (ILjava/util/List;Z)I
    //   2373: iadd
    //   2374: istore #6
    //   2376: goto -> 3003
    //   2379: iload_3
    //   2380: iload #7
    //   2382: aload_1
    //   2383: lload #8
    //   2385: invokestatic listAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2388: iconst_0
    //   2389: invokestatic computeSizeFixed32List : (ILjava/util/List;Z)I
    //   2392: iadd
    //   2393: istore #6
    //   2395: goto -> 3003
    //   2398: iload_3
    //   2399: iload #7
    //   2401: aload_1
    //   2402: lload #8
    //   2404: invokestatic listAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2407: iconst_0
    //   2408: invokestatic computeSizeFixed64List : (ILjava/util/List;Z)I
    //   2411: iadd
    //   2412: istore #6
    //   2414: goto -> 3003
    //   2417: iload_3
    //   2418: istore #6
    //   2420: aload_0
    //   2421: aload_1
    //   2422: iload #4
    //   2424: invokespecial isFieldPresent : (Ljava/lang/Object;I)Z
    //   2427: ifeq -> 3003
    //   2430: aload_1
    //   2431: lload #8
    //   2433: invokestatic getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   2436: checkcast com/android/framework/protobuf/MessageLite
    //   2439: astore #10
    //   2441: aload_0
    //   2442: iload #4
    //   2444: invokespecial getMessageFieldSchema : (I)Lcom/android/framework/protobuf/Schema;
    //   2447: astore #11
    //   2449: iload_3
    //   2450: iload #7
    //   2452: aload #10
    //   2454: aload #11
    //   2456: invokestatic computeGroupSize : (ILcom/android/framework/protobuf/MessageLite;Lcom/android/framework/protobuf/Schema;)I
    //   2459: iadd
    //   2460: istore #6
    //   2462: goto -> 3003
    //   2465: iload_3
    //   2466: istore #6
    //   2468: aload_0
    //   2469: aload_1
    //   2470: iload #4
    //   2472: invokespecial isFieldPresent : (Ljava/lang/Object;I)Z
    //   2475: ifeq -> 3003
    //   2478: iload_3
    //   2479: iload #7
    //   2481: aload_1
    //   2482: lload #8
    //   2484: invokestatic getLong : (Ljava/lang/Object;J)J
    //   2487: invokestatic computeSInt64Size : (IJ)I
    //   2490: iadd
    //   2491: istore #6
    //   2493: goto -> 3003
    //   2496: iload_3
    //   2497: istore #6
    //   2499: aload_0
    //   2500: aload_1
    //   2501: iload #4
    //   2503: invokespecial isFieldPresent : (Ljava/lang/Object;I)Z
    //   2506: ifeq -> 3003
    //   2509: iload_3
    //   2510: iload #7
    //   2512: aload_1
    //   2513: lload #8
    //   2515: invokestatic getInt : (Ljava/lang/Object;J)I
    //   2518: invokestatic computeSInt32Size : (II)I
    //   2521: iadd
    //   2522: istore #6
    //   2524: goto -> 3003
    //   2527: iload_3
    //   2528: istore #6
    //   2530: aload_0
    //   2531: aload_1
    //   2532: iload #4
    //   2534: invokespecial isFieldPresent : (Ljava/lang/Object;I)Z
    //   2537: ifeq -> 3003
    //   2540: iload_3
    //   2541: iload #7
    //   2543: lconst_0
    //   2544: invokestatic computeSFixed64Size : (IJ)I
    //   2547: iadd
    //   2548: istore #6
    //   2550: goto -> 3003
    //   2553: iload_3
    //   2554: istore #6
    //   2556: aload_0
    //   2557: aload_1
    //   2558: iload #4
    //   2560: invokespecial isFieldPresent : (Ljava/lang/Object;I)Z
    //   2563: ifeq -> 3003
    //   2566: iload_3
    //   2567: iload #7
    //   2569: iconst_0
    //   2570: invokestatic computeSFixed32Size : (II)I
    //   2573: iadd
    //   2574: istore #6
    //   2576: goto -> 3003
    //   2579: iload_3
    //   2580: istore #6
    //   2582: aload_0
    //   2583: aload_1
    //   2584: iload #4
    //   2586: invokespecial isFieldPresent : (Ljava/lang/Object;I)Z
    //   2589: ifeq -> 3003
    //   2592: iload_3
    //   2593: iload #7
    //   2595: aload_1
    //   2596: lload #8
    //   2598: invokestatic getInt : (Ljava/lang/Object;J)I
    //   2601: invokestatic computeEnumSize : (II)I
    //   2604: iadd
    //   2605: istore #6
    //   2607: goto -> 3003
    //   2610: iload_3
    //   2611: istore #6
    //   2613: aload_0
    //   2614: aload_1
    //   2615: iload #4
    //   2617: invokespecial isFieldPresent : (Ljava/lang/Object;I)Z
    //   2620: ifeq -> 3003
    //   2623: iload_3
    //   2624: iload #7
    //   2626: aload_1
    //   2627: lload #8
    //   2629: invokestatic getInt : (Ljava/lang/Object;J)I
    //   2632: invokestatic computeUInt32Size : (II)I
    //   2635: iadd
    //   2636: istore #6
    //   2638: goto -> 3003
    //   2641: iload_3
    //   2642: istore #6
    //   2644: aload_0
    //   2645: aload_1
    //   2646: iload #4
    //   2648: invokespecial isFieldPresent : (Ljava/lang/Object;I)Z
    //   2651: ifeq -> 3003
    //   2654: aload_1
    //   2655: lload #8
    //   2657: invokestatic getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   2660: checkcast com/android/framework/protobuf/ByteString
    //   2663: astore #10
    //   2665: iload_3
    //   2666: iload #7
    //   2668: aload #10
    //   2670: invokestatic computeBytesSize : (ILcom/android/framework/protobuf/ByteString;)I
    //   2673: iadd
    //   2674: istore #6
    //   2676: goto -> 3003
    //   2679: iload_3
    //   2680: istore #6
    //   2682: aload_0
    //   2683: aload_1
    //   2684: iload #4
    //   2686: invokespecial isFieldPresent : (Ljava/lang/Object;I)Z
    //   2689: ifeq -> 3003
    //   2692: aload_1
    //   2693: lload #8
    //   2695: invokestatic getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   2698: astore #10
    //   2700: iload_3
    //   2701: iload #7
    //   2703: aload #10
    //   2705: aload_0
    //   2706: iload #4
    //   2708: invokespecial getMessageFieldSchema : (I)Lcom/android/framework/protobuf/Schema;
    //   2711: invokestatic computeSizeMessage : (ILjava/lang/Object;Lcom/android/framework/protobuf/Schema;)I
    //   2714: iadd
    //   2715: istore #6
    //   2717: goto -> 3003
    //   2720: iload_3
    //   2721: istore #6
    //   2723: aload_0
    //   2724: aload_1
    //   2725: iload #4
    //   2727: invokespecial isFieldPresent : (Ljava/lang/Object;I)Z
    //   2730: ifeq -> 3003
    //   2733: aload_1
    //   2734: lload #8
    //   2736: invokestatic getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   2739: astore #10
    //   2741: aload #10
    //   2743: instanceof com/android/framework/protobuf/ByteString
    //   2746: ifeq -> 2766
    //   2749: iload_3
    //   2750: iload #7
    //   2752: aload #10
    //   2754: checkcast com/android/framework/protobuf/ByteString
    //   2757: invokestatic computeBytesSize : (ILcom/android/framework/protobuf/ByteString;)I
    //   2760: iadd
    //   2761: istore #6
    //   2763: goto -> 2780
    //   2766: iload_3
    //   2767: iload #7
    //   2769: aload #10
    //   2771: checkcast java/lang/String
    //   2774: invokestatic computeStringSize : (ILjava/lang/String;)I
    //   2777: iadd
    //   2778: istore #6
    //   2780: goto -> 3003
    //   2783: iload_3
    //   2784: istore #6
    //   2786: aload_0
    //   2787: aload_1
    //   2788: iload #4
    //   2790: invokespecial isFieldPresent : (Ljava/lang/Object;I)Z
    //   2793: ifeq -> 3003
    //   2796: iload_3
    //   2797: iload #7
    //   2799: iconst_1
    //   2800: invokestatic computeBoolSize : (IZ)I
    //   2803: iadd
    //   2804: istore #6
    //   2806: goto -> 3003
    //   2809: iload_3
    //   2810: istore #6
    //   2812: aload_0
    //   2813: aload_1
    //   2814: iload #4
    //   2816: invokespecial isFieldPresent : (Ljava/lang/Object;I)Z
    //   2819: ifeq -> 3003
    //   2822: iload_3
    //   2823: iload #7
    //   2825: iconst_0
    //   2826: invokestatic computeFixed32Size : (II)I
    //   2829: iadd
    //   2830: istore #6
    //   2832: goto -> 3003
    //   2835: iload_3
    //   2836: istore #6
    //   2838: aload_0
    //   2839: aload_1
    //   2840: iload #4
    //   2842: invokespecial isFieldPresent : (Ljava/lang/Object;I)Z
    //   2845: ifeq -> 3003
    //   2848: iload_3
    //   2849: iload #7
    //   2851: lconst_0
    //   2852: invokestatic computeFixed64Size : (IJ)I
    //   2855: iadd
    //   2856: istore #6
    //   2858: goto -> 3003
    //   2861: iload_3
    //   2862: istore #6
    //   2864: aload_0
    //   2865: aload_1
    //   2866: iload #4
    //   2868: invokespecial isFieldPresent : (Ljava/lang/Object;I)Z
    //   2871: ifeq -> 3003
    //   2874: iload_3
    //   2875: iload #7
    //   2877: aload_1
    //   2878: lload #8
    //   2880: invokestatic getInt : (Ljava/lang/Object;J)I
    //   2883: invokestatic computeInt32Size : (II)I
    //   2886: iadd
    //   2887: istore #6
    //   2889: goto -> 3003
    //   2892: iload_3
    //   2893: istore #6
    //   2895: aload_0
    //   2896: aload_1
    //   2897: iload #4
    //   2899: invokespecial isFieldPresent : (Ljava/lang/Object;I)Z
    //   2902: ifeq -> 3003
    //   2905: iload_3
    //   2906: iload #7
    //   2908: aload_1
    //   2909: lload #8
    //   2911: invokestatic getLong : (Ljava/lang/Object;J)J
    //   2914: invokestatic computeUInt64Size : (IJ)I
    //   2917: iadd
    //   2918: istore #6
    //   2920: goto -> 3003
    //   2923: iload_3
    //   2924: istore #6
    //   2926: aload_0
    //   2927: aload_1
    //   2928: iload #4
    //   2930: invokespecial isFieldPresent : (Ljava/lang/Object;I)Z
    //   2933: ifeq -> 3003
    //   2936: iload_3
    //   2937: iload #7
    //   2939: aload_1
    //   2940: lload #8
    //   2942: invokestatic getLong : (Ljava/lang/Object;J)J
    //   2945: invokestatic computeInt64Size : (IJ)I
    //   2948: iadd
    //   2949: istore #6
    //   2951: goto -> 3003
    //   2954: iload_3
    //   2955: istore #6
    //   2957: aload_0
    //   2958: aload_1
    //   2959: iload #4
    //   2961: invokespecial isFieldPresent : (Ljava/lang/Object;I)Z
    //   2964: ifeq -> 3003
    //   2967: iload_3
    //   2968: iload #7
    //   2970: fconst_0
    //   2971: invokestatic computeFloatSize : (IF)I
    //   2974: iadd
    //   2975: istore #6
    //   2977: goto -> 3003
    //   2980: iload_3
    //   2981: istore #6
    //   2983: aload_0
    //   2984: aload_1
    //   2985: iload #4
    //   2987: invokespecial isFieldPresent : (Ljava/lang/Object;I)Z
    //   2990: ifeq -> 3003
    //   2993: iload_3
    //   2994: iload #7
    //   2996: dconst_0
    //   2997: invokestatic computeDoubleSize : (ID)I
    //   3000: iadd
    //   3001: istore #6
    //   3003: iinc #4, 3
    //   3006: iload #6
    //   3008: istore_3
    //   3009: goto -> 9
    //   3012: aload_0
    //   3013: aload_0
    //   3014: getfield unknownFieldSchema : Lcom/android/framework/protobuf/UnknownFieldSchema;
    //   3017: aload_1
    //   3018: invokespecial getUnknownFieldsSerializedSize : (Lcom/android/framework/protobuf/UnknownFieldSchema;Ljava/lang/Object;)I
    //   3021: istore #6
    //   3023: iload_3
    //   3024: iload #6
    //   3026: iadd
    //   3027: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1984	-> 0
    //   #1985	-> 4
    //   #1986	-> 6
    //   #1987	-> 19
    //   #1988	-> 27
    //   #1989	-> 34
    //   #1991	-> 42
    //   #1994	-> 49
    //   #1993	-> 54
    //   #1994	-> 69
    //   #1995	-> 79
    //   #1996	-> 96
    //   #1998	-> 99
    //   #2497	-> 398
    //   #2498	-> 413
    //   #2501	-> 413
    //   #2502	-> 424
    //   #2499	-> 432
    //   #2492	-> 448
    //   #2493	-> 463
    //   #2487	-> 481
    //   #2488	-> 496
    //   #2482	-> 514
    //   #2483	-> 529
    //   #2477	-> 542
    //   #2478	-> 557
    //   #2472	-> 570
    //   #2473	-> 585
    //   #2467	-> 603
    //   #2468	-> 618
    //   #2460	-> 636
    //   #2461	-> 651
    //   #2463	-> 651
    //   #2462	-> 662
    //   #2454	-> 676
    //   #2455	-> 691
    //   #2456	-> 699
    //   #2457	-> 716
    //   #2444	-> 719
    //   #2445	-> 734
    //   #2446	-> 742
    //   #2447	-> 750
    //   #2449	-> 767
    //   #2451	-> 781
    //   #2439	-> 784
    //   #2440	-> 799
    //   #2434	-> 812
    //   #2435	-> 827
    //   #2429	-> 840
    //   #2430	-> 855
    //   #2424	-> 868
    //   #2425	-> 883
    //   #2419	-> 901
    //   #2420	-> 916
    //   #2414	-> 934
    //   #2415	-> 949
    //   #2409	-> 967
    //   #2410	-> 982
    //   #2404	-> 995
    //   #2405	-> 1010
    //   #2399	-> 1023
    //   #2401	-> 1029
    //   #2400	-> 1045
    //   #2402	-> 1062
    //   #2393	-> 1065
    //   #2395	-> 1065
    //   #2394	-> 1081
    //   #2396	-> 1094
    //   #2378	-> 1097
    //   #2380	-> 1097
    //   #2379	-> 1109
    //   #2381	-> 1116
    //   #2382	-> 1124
    //   #2383	-> 1131
    //   #2385	-> 1141
    //   #2386	-> 1141
    //   #2387	-> 1148
    //   #2362	-> 1166
    //   #2364	-> 1166
    //   #2363	-> 1178
    //   #2365	-> 1185
    //   #2366	-> 1193
    //   #2367	-> 1200
    //   #2369	-> 1210
    //   #2370	-> 1210
    //   #2371	-> 1217
    //   #2346	-> 1235
    //   #2348	-> 1235
    //   #2347	-> 1247
    //   #2349	-> 1254
    //   #2350	-> 1262
    //   #2351	-> 1269
    //   #2353	-> 1279
    //   #2354	-> 1279
    //   #2355	-> 1286
    //   #2330	-> 1304
    //   #2332	-> 1304
    //   #2331	-> 1316
    //   #2333	-> 1323
    //   #2334	-> 1331
    //   #2335	-> 1338
    //   #2337	-> 1348
    //   #2338	-> 1348
    //   #2339	-> 1355
    //   #2314	-> 1373
    //   #2316	-> 1373
    //   #2315	-> 1385
    //   #2317	-> 1392
    //   #2318	-> 1400
    //   #2319	-> 1407
    //   #2321	-> 1417
    //   #2322	-> 1417
    //   #2323	-> 1424
    //   #2298	-> 1442
    //   #2300	-> 1442
    //   #2299	-> 1454
    //   #2301	-> 1461
    //   #2302	-> 1469
    //   #2303	-> 1476
    //   #2305	-> 1486
    //   #2306	-> 1486
    //   #2307	-> 1493
    //   #2282	-> 1511
    //   #2284	-> 1511
    //   #2283	-> 1523
    //   #2285	-> 1530
    //   #2286	-> 1538
    //   #2287	-> 1545
    //   #2289	-> 1555
    //   #2290	-> 1555
    //   #2291	-> 1562
    //   #2266	-> 1580
    //   #2268	-> 1580
    //   #2267	-> 1592
    //   #2269	-> 1599
    //   #2270	-> 1607
    //   #2271	-> 1614
    //   #2273	-> 1624
    //   #2274	-> 1624
    //   #2275	-> 1631
    //   #2250	-> 1649
    //   #2252	-> 1649
    //   #2251	-> 1661
    //   #2253	-> 1668
    //   #2254	-> 1676
    //   #2255	-> 1683
    //   #2257	-> 1693
    //   #2258	-> 1693
    //   #2259	-> 1700
    //   #2234	-> 1718
    //   #2236	-> 1718
    //   #2235	-> 1730
    //   #2237	-> 1737
    //   #2238	-> 1745
    //   #2239	-> 1752
    //   #2241	-> 1762
    //   #2242	-> 1762
    //   #2243	-> 1769
    //   #2218	-> 1787
    //   #2220	-> 1787
    //   #2219	-> 1799
    //   #2221	-> 1806
    //   #2222	-> 1814
    //   #2223	-> 1821
    //   #2225	-> 1831
    //   #2226	-> 1831
    //   #2227	-> 1838
    //   #2202	-> 1856
    //   #2204	-> 1856
    //   #2203	-> 1868
    //   #2205	-> 1875
    //   #2206	-> 1883
    //   #2207	-> 1890
    //   #2209	-> 1900
    //   #2210	-> 1900
    //   #2211	-> 1907
    //   #2186	-> 1925
    //   #2188	-> 1925
    //   #2187	-> 1937
    //   #2189	-> 1944
    //   #2190	-> 1952
    //   #2191	-> 1959
    //   #2193	-> 1969
    //   #2194	-> 1969
    //   #2195	-> 1976
    //   #2170	-> 1994
    //   #2172	-> 1994
    //   #2171	-> 2006
    //   #2173	-> 2013
    //   #2174	-> 2021
    //   #2175	-> 2028
    //   #2177	-> 2038
    //   #2178	-> 2038
    //   #2179	-> 2045
    //   #2165	-> 2063
    //   #2166	-> 2063
    //   #2167	-> 2079
    //   #2160	-> 2082
    //   #2162	-> 2082
    //   #2161	-> 2090
    //   #2163	-> 2102
    //   #2157	-> 2105
    //   #2158	-> 2121
    //   #2154	-> 2124
    //   #2155	-> 2140
    //   #2149	-> 2143
    //   #2151	-> 2143
    //   #2150	-> 2151
    //   #2152	-> 2163
    //   #2144	-> 2166
    //   #2146	-> 2166
    //   #2145	-> 2174
    //   #2147	-> 2186
    //   #2139	-> 2189
    //   #2141	-> 2189
    //   #2140	-> 2197
    //   #2142	-> 2208
    //   #2134	-> 2211
    //   #2136	-> 2211
    //   #2135	-> 2227
    //   #2137	-> 2240
    //   #2131	-> 2243
    //   #2132	-> 2258
    //   #2128	-> 2261
    //   #2129	-> 2277
    //   #2125	-> 2280
    //   #2126	-> 2296
    //   #2122	-> 2299
    //   #2123	-> 2315
    //   #2117	-> 2318
    //   #2119	-> 2318
    //   #2118	-> 2326
    //   #2120	-> 2338
    //   #2113	-> 2341
    //   #2114	-> 2341
    //   #2115	-> 2357
    //   #2109	-> 2360
    //   #2110	-> 2360
    //   #2111	-> 2376
    //   #2106	-> 2379
    //   #2107	-> 2395
    //   #2103	-> 2398
    //   #2104	-> 2414
    //   #2094	-> 2417
    //   #2095	-> 2430
    //   #2098	-> 2430
    //   #2099	-> 2441
    //   #2096	-> 2449
    //   #2088	-> 2465
    //   #2089	-> 2478
    //   #2090	-> 2478
    //   #2083	-> 2496
    //   #2084	-> 2509
    //   #2078	-> 2527
    //   #2079	-> 2540
    //   #2073	-> 2553
    //   #2074	-> 2566
    //   #2068	-> 2579
    //   #2069	-> 2592
    //   #2063	-> 2610
    //   #2064	-> 2623
    //   #2057	-> 2641
    //   #2058	-> 2654
    //   #2059	-> 2665
    //   #2060	-> 2676
    //   #2051	-> 2679
    //   #2052	-> 2692
    //   #2053	-> 2700
    //   #2054	-> 2717
    //   #2041	-> 2720
    //   #2042	-> 2733
    //   #2043	-> 2741
    //   #2044	-> 2749
    //   #2046	-> 2766
    //   #2048	-> 2780
    //   #2036	-> 2783
    //   #2037	-> 2796
    //   #2031	-> 2809
    //   #2032	-> 2822
    //   #2026	-> 2835
    //   #2027	-> 2848
    //   #2021	-> 2861
    //   #2022	-> 2874
    //   #2015	-> 2892
    //   #2016	-> 2905
    //   #2017	-> 2905
    //   #2010	-> 2923
    //   #2011	-> 2936
    //   #2005	-> 2954
    //   #2006	-> 2967
    //   #2000	-> 2980
    //   #2001	-> 2993
    //   #1986	-> 3003
    //   #2510	-> 3012
    //   #2512	-> 3023
  }
  
  private <UT, UB> int getUnknownFieldsSerializedSize(UnknownFieldSchema<UT, UB> paramUnknownFieldSchema, T paramT) {
    paramT = (T)paramUnknownFieldSchema.getFromMessage(paramT);
    return paramUnknownFieldSchema.getSerializedSize((UT)paramT);
  }
  
  private static List<?> listAt(Object paramObject, long paramLong) {
    return (List)UnsafeUtil.getObject(paramObject, paramLong);
  }
  
  public void writeTo(T paramT, Writer paramWriter) throws IOException {
    if (paramWriter.fieldOrder() == Writer.FieldOrder.DESCENDING) {
      writeFieldsInDescendingOrder(paramT, paramWriter);
    } else if (this.proto3) {
      writeFieldsInAscendingOrderProto3(paramT, paramWriter);
    } else {
      writeFieldsInAscendingOrderProto2(paramT, paramWriter);
    } 
  }
  
  private void writeFieldsInAscendingOrderProto2(T paramT, Writer paramWriter) throws IOException {
    Iterator<Map.Entry<?, Object>> iterator1 = null;
    Map.Entry<?, ?> entry1 = null;
    Iterator<Map.Entry<?, Object>> iterator2 = iterator1;
    Map.Entry<?, ?> entry2 = entry1;
    if (this.hasExtensions) {
      FieldSet<?> fieldSet = this.extensionSchema.getExtensions(paramT);
      iterator2 = iterator1;
      entry2 = entry1;
      if (!fieldSet.isEmpty()) {
        iterator2 = fieldSet.iterator();
        entry2 = iterator2.next();
      } 
    } 
    int i = -1;
    int j = 0;
    int k = this.buffer.length;
    Unsafe unsafe = UNSAFE;
    for (byte b = 0; b < k; b += 3) {
      Schema schema2;
      List<Long> list1;
      Schema schema1;
      List<String> list;
      List<?> list2;
      int m = typeAndOffsetAt(b);
      int n = numberAt(b);
      int i1 = type(m);
      int i2 = 0;
      if (!this.proto3 && i1 <= 17) {
        int i3 = this.buffer[b + 2];
        int i4 = 0xFFFFF & i3;
        i2 = i;
        if (i4 != i) {
          j = unsafe.getInt(paramT, i4);
          i2 = i4;
        } 
        i4 = 1 << i3 >>> 20;
        i = i2;
        i2 = i4;
      } 
      while (entry2 != null && this.extensionSchema.extensionNumber(entry2) <= n) {
        this.extensionSchema.serializeExtension(paramWriter, entry2);
        if (iterator2.hasNext()) {
          entry2 = iterator2.next();
          continue;
        } 
        entry2 = null;
      } 
      long l = offset(m);
      switch (i1) {
        case 68:
          if (isOneofPresent(paramT, n, b)) {
            Object object = unsafe.getObject(paramT, l);
            Schema schema = getMessageFieldSchema(b);
            paramWriter.writeGroup(n, object, schema);
          } 
          break;
        case 67:
          if (isOneofPresent(paramT, n, b))
            paramWriter.writeSInt64(n, oneofLongAt(paramT, l)); 
          break;
        case 66:
          if (isOneofPresent(paramT, n, b))
            paramWriter.writeSInt32(n, oneofIntAt(paramT, l)); 
          break;
        case 65:
          if (isOneofPresent(paramT, n, b))
            paramWriter.writeSFixed64(n, oneofLongAt(paramT, l)); 
          break;
        case 64:
          if (isOneofPresent(paramT, n, b))
            paramWriter.writeSFixed32(n, oneofIntAt(paramT, l)); 
          break;
        case 63:
          if (isOneofPresent(paramT, n, b))
            paramWriter.writeEnum(n, oneofIntAt(paramT, l)); 
          break;
        case 62:
          if (isOneofPresent(paramT, n, b))
            paramWriter.writeUInt32(n, oneofIntAt(paramT, l)); 
          break;
        case 61:
          if (isOneofPresent(paramT, n, b))
            paramWriter.writeBytes(n, (ByteString)unsafe.getObject(paramT, l)); 
          break;
        case 60:
          if (isOneofPresent(paramT, n, b)) {
            Object object = unsafe.getObject(paramT, l);
            paramWriter.writeMessage(n, object, getMessageFieldSchema(b));
          } 
          break;
        case 59:
          if (isOneofPresent(paramT, n, b))
            writeString(n, unsafe.getObject(paramT, l), paramWriter); 
          break;
        case 58:
          if (isOneofPresent(paramT, n, b))
            paramWriter.writeBool(n, oneofBooleanAt(paramT, l)); 
          break;
        case 57:
          if (isOneofPresent(paramT, n, b))
            paramWriter.writeFixed32(n, oneofIntAt(paramT, l)); 
          break;
        case 56:
          if (isOneofPresent(paramT, n, b))
            paramWriter.writeFixed64(n, oneofLongAt(paramT, l)); 
          break;
        case 55:
          if (isOneofPresent(paramT, n, b))
            paramWriter.writeInt32(n, oneofIntAt(paramT, l)); 
          break;
        case 54:
          if (isOneofPresent(paramT, n, b))
            paramWriter.writeUInt64(n, oneofLongAt(paramT, l)); 
          break;
        case 53:
          if (isOneofPresent(paramT, n, b))
            paramWriter.writeInt64(n, oneofLongAt(paramT, l)); 
          break;
        case 52:
          if (isOneofPresent(paramT, n, b))
            paramWriter.writeFloat(n, oneofFloatAt(paramT, l)); 
          break;
        case 51:
          if (isOneofPresent(paramT, n, b))
            paramWriter.writeDouble(n, oneofDoubleAt(paramT, l)); 
          break;
        case 50:
          writeMapHelper(paramWriter, n, unsafe.getObject(paramT, l), b);
          break;
        case 49:
          i2 = numberAt(b);
          list2 = (List)unsafe.getObject(paramT, l);
          schema2 = getMessageFieldSchema(b);
          SchemaUtil.writeGroupList(i2, list2, paramWriter, schema2);
          break;
        case 48:
          i2 = numberAt(b);
          list1 = (List)unsafe.getObject(paramT, l);
          SchemaUtil.writeSInt64List(i2, list1, paramWriter, true);
          break;
        case 47:
          i2 = numberAt(b);
          list1 = (List<Long>)unsafe.getObject(paramT, l);
          SchemaUtil.writeSInt32List(i2, (List)list1, paramWriter, true);
          break;
        case 46:
          i2 = numberAt(b);
          list1 = (List<Long>)unsafe.getObject(paramT, l);
          SchemaUtil.writeSFixed64List(i2, list1, paramWriter, true);
          break;
        case 45:
          i2 = numberAt(b);
          list1 = (List<Long>)unsafe.getObject(paramT, l);
          SchemaUtil.writeSFixed32List(i2, (List)list1, paramWriter, true);
          break;
        case 44:
          i2 = numberAt(b);
          list1 = (List<Long>)unsafe.getObject(paramT, l);
          SchemaUtil.writeEnumList(i2, (List)list1, paramWriter, true);
          break;
        case 43:
          i2 = numberAt(b);
          list1 = (List<Long>)unsafe.getObject(paramT, l);
          SchemaUtil.writeUInt32List(i2, (List)list1, paramWriter, true);
          break;
        case 42:
          i2 = numberAt(b);
          list1 = (List<Long>)unsafe.getObject(paramT, l);
          SchemaUtil.writeBoolList(i2, (List)list1, paramWriter, true);
          break;
        case 41:
          i2 = numberAt(b);
          list1 = (List<Long>)unsafe.getObject(paramT, l);
          SchemaUtil.writeFixed32List(i2, (List)list1, paramWriter, true);
          break;
        case 40:
          i2 = numberAt(b);
          list1 = (List<Long>)unsafe.getObject(paramT, l);
          SchemaUtil.writeFixed64List(i2, list1, paramWriter, true);
          break;
        case 39:
          i2 = numberAt(b);
          list1 = (List<Long>)unsafe.getObject(paramT, l);
          SchemaUtil.writeInt32List(i2, (List)list1, paramWriter, true);
          break;
        case 38:
          i2 = numberAt(b);
          list1 = (List<Long>)unsafe.getObject(paramT, l);
          SchemaUtil.writeUInt64List(i2, list1, paramWriter, true);
          break;
        case 37:
          i2 = numberAt(b);
          list1 = (List<Long>)unsafe.getObject(paramT, l);
          SchemaUtil.writeInt64List(i2, list1, paramWriter, true);
          break;
        case 36:
          i2 = numberAt(b);
          list1 = (List<Long>)unsafe.getObject(paramT, l);
          SchemaUtil.writeFloatList(i2, (List)list1, paramWriter, true);
          break;
        case 35:
          i2 = numberAt(b);
          list1 = (List<Long>)unsafe.getObject(paramT, l);
          SchemaUtil.writeDoubleList(i2, (List)list1, paramWriter, true);
          break;
        case 34:
          i2 = numberAt(b);
          list1 = (List<Long>)unsafe.getObject(paramT, l);
          SchemaUtil.writeSInt64List(i2, list1, paramWriter, false);
          break;
        case 33:
          i2 = numberAt(b);
          list1 = (List<Long>)unsafe.getObject(paramT, l);
          SchemaUtil.writeSInt32List(i2, (List)list1, paramWriter, false);
          break;
        case 32:
          i2 = numberAt(b);
          list1 = (List<Long>)unsafe.getObject(paramT, l);
          SchemaUtil.writeSFixed64List(i2, list1, paramWriter, false);
          break;
        case 31:
          i2 = numberAt(b);
          list1 = (List<Long>)unsafe.getObject(paramT, l);
          SchemaUtil.writeSFixed32List(i2, (List)list1, paramWriter, false);
          break;
        case 30:
          i2 = numberAt(b);
          list1 = (List<Long>)unsafe.getObject(paramT, l);
          SchemaUtil.writeEnumList(i2, (List)list1, paramWriter, false);
          break;
        case 29:
          i2 = numberAt(b);
          list1 = (List<Long>)unsafe.getObject(paramT, l);
          SchemaUtil.writeUInt32List(i2, (List)list1, paramWriter, false);
          break;
        case 28:
          i2 = numberAt(b);
          list1 = (List<Long>)unsafe.getObject(paramT, l);
          SchemaUtil.writeBytesList(i2, (List)list1, paramWriter);
          break;
        case 27:
          i2 = numberAt(b);
          list2 = (List)unsafe.getObject(paramT, l);
          schema1 = getMessageFieldSchema(b);
          SchemaUtil.writeMessageList(i2, list2, paramWriter, schema1);
          break;
        case 26:
          i2 = numberAt(b);
          list = (List)unsafe.getObject(paramT, l);
          SchemaUtil.writeStringList(i2, list, paramWriter);
          break;
        case 25:
          i2 = numberAt(b);
          list = (List<String>)unsafe.getObject(paramT, l);
          SchemaUtil.writeBoolList(i2, (List)list, paramWriter, false);
          break;
        case 24:
          i2 = numberAt(b);
          list = (List<String>)unsafe.getObject(paramT, l);
          SchemaUtil.writeFixed32List(i2, (List)list, paramWriter, false);
          break;
        case 23:
          i2 = numberAt(b);
          list = (List<String>)unsafe.getObject(paramT, l);
          SchemaUtil.writeFixed64List(i2, (List)list, paramWriter, false);
          break;
        case 22:
          i2 = numberAt(b);
          list = (List<String>)unsafe.getObject(paramT, l);
          SchemaUtil.writeInt32List(i2, (List)list, paramWriter, false);
          break;
        case 21:
          i2 = numberAt(b);
          list = (List<String>)unsafe.getObject(paramT, l);
          SchemaUtil.writeUInt64List(i2, (List)list, paramWriter, false);
          break;
        case 20:
          i2 = numberAt(b);
          list = (List<String>)unsafe.getObject(paramT, l);
          SchemaUtil.writeInt64List(i2, (List)list, paramWriter, false);
          break;
        case 19:
          i2 = numberAt(b);
          list = (List<String>)unsafe.getObject(paramT, l);
          SchemaUtil.writeFloatList(i2, (List)list, paramWriter, false);
          break;
        case 18:
          i2 = numberAt(b);
          list = (List<String>)unsafe.getObject(paramT, l);
          SchemaUtil.writeDoubleList(i2, (List)list, paramWriter, false);
          break;
        case 17:
          if ((j & i2) != 0) {
            Object object = unsafe.getObject(paramT, l);
            Schema schema = getMessageFieldSchema(b);
            paramWriter.writeGroup(n, object, schema);
          } 
          break;
        case 16:
          if ((j & i2) != 0)
            paramWriter.writeSInt64(n, unsafe.getLong(paramT, l)); 
          break;
        case 15:
          if ((j & i2) != 0)
            paramWriter.writeSInt32(n, unsafe.getInt(paramT, l)); 
          break;
        case 14:
          if ((j & i2) != 0)
            paramWriter.writeSFixed64(n, unsafe.getLong(paramT, l)); 
          break;
        case 13:
          if ((j & i2) != 0)
            paramWriter.writeSFixed32(n, unsafe.getInt(paramT, l)); 
          break;
        case 12:
          if ((j & i2) != 0)
            paramWriter.writeEnum(n, unsafe.getInt(paramT, l)); 
          break;
        case 11:
          if ((j & i2) != 0)
            paramWriter.writeUInt32(n, unsafe.getInt(paramT, l)); 
          break;
        case 10:
          if ((j & i2) != 0)
            paramWriter.writeBytes(n, (ByteString)unsafe.getObject(paramT, l)); 
          break;
        case 9:
          if ((j & i2) != 0) {
            Object object = unsafe.getObject(paramT, l);
            paramWriter.writeMessage(n, object, getMessageFieldSchema(b));
          } 
          break;
        case 8:
          if ((j & i2) != 0)
            writeString(n, unsafe.getObject(paramT, l), paramWriter); 
          break;
        case 7:
          if ((j & i2) != 0)
            paramWriter.writeBool(n, booleanAt(paramT, l)); 
          break;
        case 6:
          if ((j & i2) != 0)
            paramWriter.writeFixed32(n, unsafe.getInt(paramT, l)); 
          break;
        case 5:
          if ((j & i2) != 0)
            paramWriter.writeFixed64(n, unsafe.getLong(paramT, l)); 
          break;
        case 4:
          if ((j & i2) != 0)
            paramWriter.writeInt32(n, unsafe.getInt(paramT, l)); 
          break;
        case 3:
          if ((j & i2) != 0)
            paramWriter.writeUInt64(n, unsafe.getLong(paramT, l)); 
          break;
        case 2:
          if ((j & i2) != 0)
            paramWriter.writeInt64(n, unsafe.getLong(paramT, l)); 
          break;
        case 1:
          if ((j & i2) != 0)
            paramWriter.writeFloat(n, floatAt(paramT, l)); 
          break;
        case 0:
          if ((j & i2) != 0)
            paramWriter.writeDouble(n, doubleAt(paramT, l)); 
          break;
      } 
    } 
    while (entry2 != null) {
      this.extensionSchema.serializeExtension(paramWriter, entry2);
      if (iterator2.hasNext()) {
        entry2 = iterator2.next();
        continue;
      } 
      entry2 = null;
    } 
    writeUnknownInMessageTo(this.unknownFieldSchema, paramT, paramWriter);
  }
  
  private void writeFieldsInAscendingOrderProto3(T paramT, Writer paramWriter) throws IOException {
    Object<?, ?> object = null;
    Map.Entry<?, ?> entry1 = null;
    Iterator<Map.Entry<?, Object>> iterator = (Iterator)object;
    Map.Entry<?, ?> entry2 = entry1;
    if (this.hasExtensions) {
      FieldSet<?> fieldSet = this.extensionSchema.getExtensions(paramT);
      iterator = (Iterator)object;
      entry2 = entry1;
      if (!fieldSet.isEmpty()) {
        iterator = fieldSet.iterator();
        entry2 = iterator.next();
      } 
    } 
    int i = this.buffer.length;
    byte b = 0;
    while (true) {
      Map.Entry<?, ?> entry = entry2;
      if (b < i) {
        Schema schema1;
        List<Long> list;
        List<?> list1;
        Schema schema2;
        int j = typeAndOffsetAt(b);
        int k = numberAt(b);
        while (entry2 != null && this.extensionSchema.extensionNumber(entry2) <= k) {
          this.extensionSchema.serializeExtension(paramWriter, entry2);
          if (iterator.hasNext()) {
            entry2 = iterator.next();
            continue;
          } 
          entry2 = null;
        } 
        switch (type(j)) {
          case 68:
            if (isOneofPresent(paramT, k, b)) {
              Object object1 = UnsafeUtil.getObject(paramT, offset(j));
              Schema schema = getMessageFieldSchema(b);
              paramWriter.writeGroup(k, object1, schema);
            } 
            break;
          case 67:
            if (isOneofPresent(paramT, k, b))
              paramWriter.writeSInt64(k, oneofLongAt(paramT, offset(j))); 
            break;
          case 66:
            if (isOneofPresent(paramT, k, b))
              paramWriter.writeSInt32(k, oneofIntAt(paramT, offset(j))); 
            break;
          case 65:
            if (isOneofPresent(paramT, k, b))
              paramWriter.writeSFixed64(k, oneofLongAt(paramT, offset(j))); 
            break;
          case 64:
            if (isOneofPresent(paramT, k, b))
              paramWriter.writeSFixed32(k, oneofIntAt(paramT, offset(j))); 
            break;
          case 63:
            if (isOneofPresent(paramT, k, b))
              paramWriter.writeEnum(k, oneofIntAt(paramT, offset(j))); 
            break;
          case 62:
            if (isOneofPresent(paramT, k, b))
              paramWriter.writeUInt32(k, oneofIntAt(paramT, offset(j))); 
            break;
          case 61:
            if (isOneofPresent(paramT, k, b)) {
              ByteString byteString = (ByteString)UnsafeUtil.getObject(paramT, offset(j));
              paramWriter.writeBytes(k, byteString);
            } 
            break;
          case 60:
            if (isOneofPresent(paramT, k, b)) {
              Object object1 = UnsafeUtil.getObject(paramT, offset(j));
              paramWriter.writeMessage(k, object1, getMessageFieldSchema(b));
            } 
            break;
          case 59:
            if (isOneofPresent(paramT, k, b))
              writeString(k, UnsafeUtil.getObject(paramT, offset(j)), paramWriter); 
            break;
          case 58:
            if (isOneofPresent(paramT, k, b))
              paramWriter.writeBool(k, oneofBooleanAt(paramT, offset(j))); 
            break;
          case 57:
            if (isOneofPresent(paramT, k, b))
              paramWriter.writeFixed32(k, oneofIntAt(paramT, offset(j))); 
            break;
          case 56:
            if (isOneofPresent(paramT, k, b))
              paramWriter.writeFixed64(k, oneofLongAt(paramT, offset(j))); 
            break;
          case 55:
            if (isOneofPresent(paramT, k, b))
              paramWriter.writeInt32(k, oneofIntAt(paramT, offset(j))); 
            break;
          case 54:
            if (isOneofPresent(paramT, k, b))
              paramWriter.writeUInt64(k, oneofLongAt(paramT, offset(j))); 
            break;
          case 53:
            if (isOneofPresent(paramT, k, b))
              paramWriter.writeInt64(k, oneofLongAt(paramT, offset(j))); 
            break;
          case 52:
            if (isOneofPresent(paramT, k, b))
              paramWriter.writeFloat(k, oneofFloatAt(paramT, offset(j))); 
            break;
          case 51:
            if (isOneofPresent(paramT, k, b))
              paramWriter.writeDouble(k, oneofDoubleAt(paramT, offset(j))); 
            break;
          case 50:
            writeMapHelper(paramWriter, k, UnsafeUtil.getObject(paramT, offset(j)), b);
            break;
          case 49:
            k = numberAt(b);
            list1 = (List)UnsafeUtil.getObject(paramT, offset(j));
            schema1 = getMessageFieldSchema(b);
            SchemaUtil.writeGroupList(k, list1, paramWriter, schema1);
            break;
          case 48:
            k = numberAt(b);
            list = (List)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeSInt64List(k, list, paramWriter, true);
            break;
          case 47:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeSInt32List(k, (List)list, paramWriter, true);
            break;
          case 46:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeSFixed64List(k, list, paramWriter, true);
            break;
          case 45:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeSFixed32List(k, (List)list, paramWriter, true);
            break;
          case 44:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeEnumList(k, (List)list, paramWriter, true);
            break;
          case 43:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeUInt32List(k, (List)list, paramWriter, true);
            break;
          case 42:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeBoolList(k, (List)list, paramWriter, true);
            break;
          case 41:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeFixed32List(k, (List)list, paramWriter, true);
            break;
          case 40:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeFixed64List(k, list, paramWriter, true);
            break;
          case 39:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeInt32List(k, (List)list, paramWriter, true);
            break;
          case 38:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeUInt64List(k, list, paramWriter, true);
            break;
          case 37:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeInt64List(k, list, paramWriter, true);
            break;
          case 36:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeFloatList(k, (List)list, paramWriter, true);
            break;
          case 35:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeDoubleList(k, (List)list, paramWriter, true);
            break;
          case 34:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeSInt64List(k, list, paramWriter, false);
            break;
          case 33:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeSInt32List(k, (List)list, paramWriter, false);
            break;
          case 32:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeSFixed64List(k, list, paramWriter, false);
            break;
          case 31:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeSFixed32List(k, (List)list, paramWriter, false);
            break;
          case 30:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeEnumList(k, (List)list, paramWriter, false);
            break;
          case 29:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeUInt32List(k, (List)list, paramWriter, false);
            break;
          case 28:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeBytesList(k, (List)list, paramWriter);
            break;
          case 27:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            schema2 = getMessageFieldSchema(b);
            SchemaUtil.writeMessageList(k, list, paramWriter, schema2);
            break;
          case 26:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeStringList(k, (List)list, paramWriter);
            break;
          case 25:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeBoolList(k, (List)list, paramWriter, false);
            break;
          case 24:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeFixed32List(k, (List)list, paramWriter, false);
            break;
          case 23:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeFixed64List(k, list, paramWriter, false);
            break;
          case 22:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeInt32List(k, (List)list, paramWriter, false);
            break;
          case 21:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeUInt64List(k, list, paramWriter, false);
            break;
          case 20:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeInt64List(k, list, paramWriter, false);
            break;
          case 19:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeFloatList(k, (List)list, paramWriter, false);
            break;
          case 18:
            k = numberAt(b);
            list = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeDoubleList(k, (List)list, paramWriter, false);
            break;
          case 17:
            if (isFieldPresent(paramT, b)) {
              Object object1 = UnsafeUtil.getObject(paramT, offset(j));
              schema2 = getMessageFieldSchema(b);
              paramWriter.writeGroup(k, object1, schema2);
            } 
            break;
          case 16:
            if (isFieldPresent(paramT, b))
              paramWriter.writeSInt64(k, longAt(paramT, offset(j))); 
            break;
          case 15:
            if (isFieldPresent(paramT, b))
              paramWriter.writeSInt32(k, intAt(paramT, offset(j))); 
            break;
          case 14:
            if (isFieldPresent(paramT, b))
              paramWriter.writeSFixed64(k, longAt(paramT, offset(j))); 
            break;
          case 13:
            if (isFieldPresent(paramT, b))
              paramWriter.writeSFixed32(k, intAt(paramT, offset(j))); 
            break;
          case 12:
            if (isFieldPresent(paramT, b))
              paramWriter.writeEnum(k, intAt(paramT, offset(j))); 
            break;
          case 11:
            if (isFieldPresent(paramT, b))
              paramWriter.writeUInt32(k, intAt(paramT, offset(j))); 
            break;
          case 10:
            if (isFieldPresent(paramT, b)) {
              ByteString byteString = (ByteString)UnsafeUtil.getObject(paramT, offset(j));
              paramWriter.writeBytes(k, byteString);
            } 
            break;
          case 9:
            if (isFieldPresent(paramT, b)) {
              object = (Object<?, ?>)UnsafeUtil.getObject(paramT, offset(j));
              paramWriter.writeMessage(k, object, getMessageFieldSchema(b));
            } 
            break;
          case 8:
            if (isFieldPresent(paramT, b))
              writeString(k, UnsafeUtil.getObject(paramT, offset(j)), paramWriter); 
            break;
          case 7:
            if (isFieldPresent(paramT, b))
              paramWriter.writeBool(k, booleanAt(paramT, offset(j))); 
            break;
          case 6:
            if (isFieldPresent(paramT, b))
              paramWriter.writeFixed32(k, intAt(paramT, offset(j))); 
            break;
          case 5:
            if (isFieldPresent(paramT, b))
              paramWriter.writeFixed64(k, longAt(paramT, offset(j))); 
            break;
          case 4:
            if (isFieldPresent(paramT, b))
              paramWriter.writeInt32(k, intAt(paramT, offset(j))); 
            break;
          case 3:
            if (isFieldPresent(paramT, b))
              paramWriter.writeUInt64(k, longAt(paramT, offset(j))); 
            break;
          case 2:
            if (isFieldPresent(paramT, b))
              paramWriter.writeInt64(k, longAt(paramT, offset(j))); 
            break;
          case 1:
            if (isFieldPresent(paramT, b))
              paramWriter.writeFloat(k, floatAt(paramT, offset(j))); 
            break;
          case 0:
            if (isFieldPresent(paramT, b))
              paramWriter.writeDouble(k, doubleAt(paramT, offset(j))); 
            break;
        } 
        b += 3;
        continue;
      } 
      break;
    } 
    while (object != null) {
      this.extensionSchema.serializeExtension(paramWriter, (Map.Entry<?, ?>)object);
      if (iterator.hasNext()) {
        entry2 = iterator.next();
      } else {
        entry2 = null;
      } 
      object = (Object<?, ?>)entry2;
    } 
    writeUnknownInMessageTo(this.unknownFieldSchema, paramT, paramWriter);
  }
  
  private void writeFieldsInDescendingOrder(T paramT, Writer paramWriter) throws IOException {
    writeUnknownInMessageTo(this.unknownFieldSchema, paramT, paramWriter);
    Iterator<Map.Entry<?, Object>> iterator1 = null;
    Object object = null;
    Iterator<Map.Entry<?, Object>> iterator2 = iterator1;
    Map.Entry<?, ?> entry = (Map.Entry<?, ?>)object;
    if (this.hasExtensions) {
      FieldSet<?> fieldSet = this.extensionSchema.getExtensions(paramT);
      iterator2 = iterator1;
      entry = (Map.Entry<?, ?>)object;
      if (!fieldSet.isEmpty()) {
        iterator2 = fieldSet.descendingIterator();
        entry = iterator2.next();
      } 
    } 
    int i = this.buffer.length - 3;
    while (true) {
      object = entry;
      if (i >= 0) {
        List<?> list;
        Schema schema2;
        List<Long> list2;
        Schema schema1;
        List<String> list1;
        int j = typeAndOffsetAt(i);
        int k = numberAt(i);
        while (entry != null && this.extensionSchema.extensionNumber(entry) > k) {
          this.extensionSchema.serializeExtension(paramWriter, entry);
          if (iterator2.hasNext()) {
            entry = iterator2.next();
            continue;
          } 
          entry = null;
        } 
        switch (type(j)) {
          case 68:
            if (isOneofPresent(paramT, k, i)) {
              Object object1 = UnsafeUtil.getObject(paramT, offset(j));
              Schema schema = getMessageFieldSchema(i);
              paramWriter.writeGroup(k, object1, schema);
            } 
            break;
          case 67:
            if (isOneofPresent(paramT, k, i))
              paramWriter.writeSInt64(k, oneofLongAt(paramT, offset(j))); 
            break;
          case 66:
            if (isOneofPresent(paramT, k, i))
              paramWriter.writeSInt32(k, oneofIntAt(paramT, offset(j))); 
            break;
          case 65:
            if (isOneofPresent(paramT, k, i))
              paramWriter.writeSFixed64(k, oneofLongAt(paramT, offset(j))); 
            break;
          case 64:
            if (isOneofPresent(paramT, k, i))
              paramWriter.writeSFixed32(k, oneofIntAt(paramT, offset(j))); 
            break;
          case 63:
            if (isOneofPresent(paramT, k, i))
              paramWriter.writeEnum(k, oneofIntAt(paramT, offset(j))); 
            break;
          case 62:
            if (isOneofPresent(paramT, k, i))
              paramWriter.writeUInt32(k, oneofIntAt(paramT, offset(j))); 
            break;
          case 61:
            if (isOneofPresent(paramT, k, i)) {
              ByteString byteString = (ByteString)UnsafeUtil.getObject(paramT, offset(j));
              paramWriter.writeBytes(k, byteString);
            } 
            break;
          case 60:
            if (isOneofPresent(paramT, k, i)) {
              Object object1 = UnsafeUtil.getObject(paramT, offset(j));
              paramWriter.writeMessage(k, object1, getMessageFieldSchema(i));
            } 
            break;
          case 59:
            if (isOneofPresent(paramT, k, i))
              writeString(k, UnsafeUtil.getObject(paramT, offset(j)), paramWriter); 
            break;
          case 58:
            if (isOneofPresent(paramT, k, i))
              paramWriter.writeBool(k, oneofBooleanAt(paramT, offset(j))); 
            break;
          case 57:
            if (isOneofPresent(paramT, k, i))
              paramWriter.writeFixed32(k, oneofIntAt(paramT, offset(j))); 
            break;
          case 56:
            if (isOneofPresent(paramT, k, i))
              paramWriter.writeFixed64(k, oneofLongAt(paramT, offset(j))); 
            break;
          case 55:
            if (isOneofPresent(paramT, k, i))
              paramWriter.writeInt32(k, oneofIntAt(paramT, offset(j))); 
            break;
          case 54:
            if (isOneofPresent(paramT, k, i))
              paramWriter.writeUInt64(k, oneofLongAt(paramT, offset(j))); 
            break;
          case 53:
            if (isOneofPresent(paramT, k, i))
              paramWriter.writeInt64(k, oneofLongAt(paramT, offset(j))); 
            break;
          case 52:
            if (isOneofPresent(paramT, k, i))
              paramWriter.writeFloat(k, oneofFloatAt(paramT, offset(j))); 
            break;
          case 51:
            if (isOneofPresent(paramT, k, i))
              paramWriter.writeDouble(k, oneofDoubleAt(paramT, offset(j))); 
            break;
          case 50:
            writeMapHelper(paramWriter, k, UnsafeUtil.getObject(paramT, offset(j)), i);
            break;
          case 49:
            k = numberAt(i);
            list = (List)UnsafeUtil.getObject(paramT, offset(j));
            schema2 = getMessageFieldSchema(i);
            SchemaUtil.writeGroupList(k, list, paramWriter, schema2);
            break;
          case 48:
            k = numberAt(i);
            list2 = (List)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeSInt64List(k, list2, paramWriter, true);
            break;
          case 47:
            k = numberAt(i);
            list2 = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeSInt32List(k, (List)list2, paramWriter, true);
            break;
          case 46:
            k = numberAt(i);
            list2 = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeSFixed64List(k, list2, paramWriter, true);
            break;
          case 45:
            k = numberAt(i);
            list2 = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeSFixed32List(k, (List)list2, paramWriter, true);
            break;
          case 44:
            k = numberAt(i);
            list2 = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeEnumList(k, (List)list2, paramWriter, true);
            break;
          case 43:
            k = numberAt(i);
            list2 = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeUInt32List(k, (List)list2, paramWriter, true);
            break;
          case 42:
            k = numberAt(i);
            list2 = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeBoolList(k, (List)list2, paramWriter, true);
            break;
          case 41:
            k = numberAt(i);
            list2 = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeFixed32List(k, (List)list2, paramWriter, true);
            break;
          case 40:
            k = numberAt(i);
            list2 = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeFixed64List(k, list2, paramWriter, true);
            break;
          case 39:
            k = numberAt(i);
            list2 = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeInt32List(k, (List)list2, paramWriter, true);
            break;
          case 38:
            k = numberAt(i);
            list2 = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeUInt64List(k, list2, paramWriter, true);
            break;
          case 37:
            k = numberAt(i);
            list2 = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeInt64List(k, list2, paramWriter, true);
            break;
          case 36:
            k = numberAt(i);
            list2 = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeFloatList(k, (List)list2, paramWriter, true);
            break;
          case 35:
            k = numberAt(i);
            list2 = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeDoubleList(k, (List)list2, paramWriter, true);
            break;
          case 34:
            k = numberAt(i);
            list2 = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeSInt64List(k, list2, paramWriter, false);
            break;
          case 33:
            k = numberAt(i);
            list2 = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeSInt32List(k, (List)list2, paramWriter, false);
            break;
          case 32:
            k = numberAt(i);
            list2 = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeSFixed64List(k, list2, paramWriter, false);
            break;
          case 31:
            k = numberAt(i);
            list2 = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeSFixed32List(k, (List)list2, paramWriter, false);
            break;
          case 30:
            k = numberAt(i);
            list2 = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeEnumList(k, (List)list2, paramWriter, false);
            break;
          case 29:
            k = numberAt(i);
            list2 = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeUInt32List(k, (List)list2, paramWriter, false);
            break;
          case 28:
            k = numberAt(i);
            list2 = (List<Long>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeBytesList(k, (List)list2, paramWriter);
            break;
          case 27:
            k = numberAt(i);
            list = (List)UnsafeUtil.getObject(paramT, offset(j));
            schema1 = getMessageFieldSchema(i);
            SchemaUtil.writeMessageList(k, list, paramWriter, schema1);
            break;
          case 26:
            k = numberAt(i);
            list1 = (List)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeStringList(k, list1, paramWriter);
            break;
          case 25:
            k = numberAt(i);
            list1 = (List<String>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeBoolList(k, (List)list1, paramWriter, false);
            break;
          case 24:
            k = numberAt(i);
            list1 = (List<String>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeFixed32List(k, (List)list1, paramWriter, false);
            break;
          case 23:
            k = numberAt(i);
            list1 = (List<String>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeFixed64List(k, (List)list1, paramWriter, false);
            break;
          case 22:
            k = numberAt(i);
            list1 = (List<String>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeInt32List(k, (List)list1, paramWriter, false);
            break;
          case 21:
            k = numberAt(i);
            list1 = (List<String>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeUInt64List(k, (List)list1, paramWriter, false);
            break;
          case 20:
            k = numberAt(i);
            list1 = (List<String>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeInt64List(k, (List)list1, paramWriter, false);
            break;
          case 19:
            k = numberAt(i);
            list1 = (List<String>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeFloatList(k, (List)list1, paramWriter, false);
            break;
          case 18:
            k = numberAt(i);
            list1 = (List<String>)UnsafeUtil.getObject(paramT, offset(j));
            SchemaUtil.writeDoubleList(k, (List)list1, paramWriter, false);
            break;
          case 17:
            if (isFieldPresent(paramT, i)) {
              Object object1 = UnsafeUtil.getObject(paramT, offset(j));
              Schema schema = getMessageFieldSchema(i);
              paramWriter.writeGroup(k, object1, schema);
            } 
            break;
          case 16:
            if (isFieldPresent(paramT, i))
              paramWriter.writeSInt64(k, longAt(paramT, offset(j))); 
            break;
          case 15:
            if (isFieldPresent(paramT, i))
              paramWriter.writeSInt32(k, intAt(paramT, offset(j))); 
            break;
          case 14:
            if (isFieldPresent(paramT, i))
              paramWriter.writeSFixed64(k, longAt(paramT, offset(j))); 
            break;
          case 13:
            if (isFieldPresent(paramT, i))
              paramWriter.writeSFixed32(k, intAt(paramT, offset(j))); 
            break;
          case 12:
            if (isFieldPresent(paramT, i))
              paramWriter.writeEnum(k, intAt(paramT, offset(j))); 
            break;
          case 11:
            if (isFieldPresent(paramT, i))
              paramWriter.writeUInt32(k, intAt(paramT, offset(j))); 
            break;
          case 10:
            if (isFieldPresent(paramT, i)) {
              ByteString byteString = (ByteString)UnsafeUtil.getObject(paramT, offset(j));
              paramWriter.writeBytes(k, byteString);
            } 
            break;
          case 9:
            if (isFieldPresent(paramT, i)) {
              object = UnsafeUtil.getObject(paramT, offset(j));
              paramWriter.writeMessage(k, object, getMessageFieldSchema(i));
            } 
            break;
          case 8:
            if (isFieldPresent(paramT, i))
              writeString(k, UnsafeUtil.getObject(paramT, offset(j)), paramWriter); 
            break;
          case 7:
            if (isFieldPresent(paramT, i))
              paramWriter.writeBool(k, booleanAt(paramT, offset(j))); 
            break;
          case 6:
            if (isFieldPresent(paramT, i))
              paramWriter.writeFixed32(k, intAt(paramT, offset(j))); 
            break;
          case 5:
            if (isFieldPresent(paramT, i))
              paramWriter.writeFixed64(k, longAt(paramT, offset(j))); 
            break;
          case 4:
            if (isFieldPresent(paramT, i))
              paramWriter.writeInt32(k, intAt(paramT, offset(j))); 
            break;
          case 3:
            if (isFieldPresent(paramT, i))
              paramWriter.writeUInt64(k, longAt(paramT, offset(j))); 
            break;
          case 2:
            if (isFieldPresent(paramT, i))
              paramWriter.writeInt64(k, longAt(paramT, offset(j))); 
            break;
          case 1:
            if (isFieldPresent(paramT, i))
              paramWriter.writeFloat(k, floatAt(paramT, offset(j))); 
            break;
          case 0:
            if (isFieldPresent(paramT, i))
              paramWriter.writeDouble(k, doubleAt(paramT, offset(j))); 
            break;
        } 
        i -= 3;
        continue;
      } 
      break;
    } 
    while (object != null) {
      this.extensionSchema.serializeExtension(paramWriter, (Map.Entry<?, ?>)object);
      if (iterator2.hasNext()) {
        Map.Entry entry1 = iterator2.next();
      } else {
        paramT = null;
      } 
      object = paramT;
    } 
  }
  
  private <K, V> void writeMapHelper(Writer paramWriter, int paramInt1, Object<?, ?> paramObject, int paramInt2) throws IOException {
    if (paramObject != null) {
      MapFieldSchema mapFieldSchema1 = this.mapFieldSchema;
      MapEntryLite.Metadata<?, ?> metadata = mapFieldSchema1.forMapMetadata(getMapFieldDefaultEntry(paramInt2));
      MapFieldSchema mapFieldSchema2 = this.mapFieldSchema;
      paramObject = (Object<?, ?>)mapFieldSchema2.forMapData(paramObject);
      paramWriter.writeMap(paramInt1, metadata, (Map<?, ?>)paramObject);
    } 
  }
  
  private <UT, UB> void writeUnknownInMessageTo(UnknownFieldSchema<UT, UB> paramUnknownFieldSchema, T paramT, Writer paramWriter) throws IOException {
    paramUnknownFieldSchema.writeTo(paramUnknownFieldSchema.getFromMessage(paramT), paramWriter);
  }
  
  public void mergeFrom(T paramT, Reader paramReader, ExtensionRegistryLite paramExtensionRegistryLite) throws IOException {
    if (paramExtensionRegistryLite != null) {
      mergeFromHelper(this.unknownFieldSchema, this.extensionSchema, paramT, paramReader, paramExtensionRegistryLite);
      return;
    } 
    throw null;
  }
  
  private <UT, UB, ET extends FieldSet.FieldDescriptorLite<ET>> void mergeFromHelper(UnknownFieldSchema<UT, UB> paramUnknownFieldSchema, ExtensionSchema<ET> paramExtensionSchema, T paramT, Reader paramReader, ExtensionRegistryLite paramExtensionRegistryLite) throws IOException {
    // Byte code:
    //   0: aconst_null
    //   1: astore #6
    //   3: aconst_null
    //   4: astore #7
    //   6: aload #4
    //   8: invokeinterface getFieldNumber : ()I
    //   13: istore #8
    //   15: aload_0
    //   16: iload #8
    //   18: invokespecial positionForFieldNumber : (I)I
    //   21: istore #9
    //   23: iload #9
    //   25: ifge -> 349
    //   28: iload #8
    //   30: ldc_w 2147483647
    //   33: if_icmpne -> 91
    //   36: aload_0
    //   37: getfield checkInitializedCount : I
    //   40: istore #9
    //   42: iload #9
    //   44: aload_0
    //   45: getfield repeatedFieldOffsetStart : I
    //   48: if_icmpge -> 78
    //   51: aload_0
    //   52: getfield intArray : [I
    //   55: iload #9
    //   57: iaload
    //   58: istore #8
    //   60: aload_0
    //   61: aload_3
    //   62: iload #8
    //   64: aload #6
    //   66: aload_1
    //   67: invokespecial filterMapUnknownEnumValues : (Ljava/lang/Object;ILjava/lang/Object;Lcom/android/framework/protobuf/UnknownFieldSchema;)Ljava/lang/Object;
    //   70: astore #6
    //   72: iinc #9, 1
    //   75: goto -> 42
    //   78: aload #6
    //   80: ifnull -> 90
    //   83: aload_1
    //   84: aload_3
    //   85: aload #6
    //   87: invokevirtual setBuilderToMessage : (Ljava/lang/Object;Ljava/lang/Object;)V
    //   90: return
    //   91: aload_0
    //   92: getfield hasExtensions : Z
    //   95: ifne -> 104
    //   98: aconst_null
    //   99: astore #10
    //   101: goto -> 118
    //   104: aload_2
    //   105: aload #5
    //   107: aload_0
    //   108: getfield defaultInstance : Lcom/android/framework/protobuf/MessageLite;
    //   111: iload #8
    //   113: invokevirtual findExtensionByNumber : (Lcom/android/framework/protobuf/ExtensionRegistryLite;Lcom/android/framework/protobuf/MessageLite;I)Ljava/lang/Object;
    //   116: astore #10
    //   118: aload #10
    //   120: ifnull -> 182
    //   123: aload #7
    //   125: astore #11
    //   127: aload #7
    //   129: ifnonnull -> 146
    //   132: aload_2
    //   133: aload_3
    //   134: invokevirtual getMutableExtensions : (Ljava/lang/Object;)Lcom/android/framework/protobuf/FieldSet;
    //   137: astore #11
    //   139: goto -> 146
    //   142: astore_2
    //   143: goto -> 5773
    //   146: aload #11
    //   148: astore #12
    //   150: aload #6
    //   152: astore #7
    //   154: aload #12
    //   156: astore #13
    //   158: aload_2
    //   159: aload #4
    //   161: aload #10
    //   163: aload #5
    //   165: aload #11
    //   167: aload #6
    //   169: aload_1
    //   170: invokevirtual parseExtension : (Lcom/android/framework/protobuf/Reader;Ljava/lang/Object;Lcom/android/framework/protobuf/ExtensionRegistryLite;Lcom/android/framework/protobuf/FieldSet;Ljava/lang/Object;Lcom/android/framework/protobuf/UnknownFieldSchema;)Ljava/lang/Object;
    //   173: astore #6
    //   175: aload #12
    //   177: astore #7
    //   179: goto -> 6
    //   182: aload #7
    //   184: astore #12
    //   186: aload #6
    //   188: astore #7
    //   190: aload #12
    //   192: astore #13
    //   194: aload_1
    //   195: aload #4
    //   197: invokevirtual shouldDiscardUnknownFields : (Lcom/android/framework/protobuf/Reader;)Z
    //   200: ifeq -> 232
    //   203: aload #6
    //   205: astore #10
    //   207: aload #6
    //   209: astore #7
    //   211: aload #12
    //   213: astore #13
    //   215: aload #4
    //   217: invokeinterface skipField : ()Z
    //   222: ifeq -> 294
    //   225: aload #12
    //   227: astore #7
    //   229: goto -> 6
    //   232: aload #6
    //   234: astore #11
    //   236: aload #6
    //   238: ifnonnull -> 256
    //   241: aload #6
    //   243: astore #7
    //   245: aload #12
    //   247: astore #13
    //   249: aload_1
    //   250: aload_3
    //   251: invokevirtual getBuilderFromMessage : (Ljava/lang/Object;)Ljava/lang/Object;
    //   254: astore #11
    //   256: aload #11
    //   258: astore #7
    //   260: aload #12
    //   262: astore #13
    //   264: aload_1
    //   265: aload #11
    //   267: aload #4
    //   269: invokevirtual mergeOneFieldFrom : (Ljava/lang/Object;Lcom/android/framework/protobuf/Reader;)Z
    //   272: istore #14
    //   274: aload #11
    //   276: astore #10
    //   278: iload #14
    //   280: ifeq -> 294
    //   283: aload #12
    //   285: astore #7
    //   287: aload #11
    //   289: astore #6
    //   291: goto -> 6
    //   294: aload_0
    //   295: getfield checkInitializedCount : I
    //   298: istore #9
    //   300: iload #9
    //   302: aload_0
    //   303: getfield repeatedFieldOffsetStart : I
    //   306: if_icmpge -> 336
    //   309: aload_0
    //   310: getfield intArray : [I
    //   313: iload #9
    //   315: iaload
    //   316: istore #8
    //   318: aload_0
    //   319: aload_3
    //   320: iload #8
    //   322: aload #10
    //   324: aload_1
    //   325: invokespecial filterMapUnknownEnumValues : (Ljava/lang/Object;ILjava/lang/Object;Lcom/android/framework/protobuf/UnknownFieldSchema;)Ljava/lang/Object;
    //   328: astore #10
    //   330: iinc #9, 1
    //   333: goto -> 300
    //   336: aload #10
    //   338: ifnull -> 348
    //   341: aload_1
    //   342: aload_3
    //   343: aload #10
    //   345: invokevirtual setBuilderToMessage : (Ljava/lang/Object;Ljava/lang/Object;)V
    //   348: return
    //   349: aload #7
    //   351: astore #11
    //   353: aload #6
    //   355: astore #7
    //   357: aload #11
    //   359: astore #13
    //   361: aload_0
    //   362: iload #9
    //   364: invokespecial typeAndOffsetAt : (I)I
    //   367: istore #15
    //   369: aload #6
    //   371: astore #7
    //   373: aload #11
    //   375: astore #13
    //   377: iload #15
    //   379: invokestatic type : (I)I
    //   382: istore #16
    //   384: iload #16
    //   386: tableswitch default -> 676, 0 -> 5395, 1 -> 5345, 2 -> 5295, 3 -> 5245, 4 -> 5195, 5 -> 5145, 6 -> 5095, 7 -> 5045, 8 -> 5002, 9 -> 4759, 10 -> 4709, 11 -> 4659, 12 -> 4514, 13 -> 4464, 14 -> 4414, 15 -> 4364, 16 -> 4314, 17 -> 4071, 18 -> 4004, 19 -> 3937, 20 -> 3870, 21 -> 3803, 22 -> 3736, 23 -> 3669, 24 -> 3602, 25 -> 3535, 26 -> 3511, 27 -> 3466, 28 -> 3411, 29 -> 3356, 30 -> 3259, 31 -> 3204, 32 -> 3149, 33 -> 3094, 34 -> 3039, 35 -> 2984, 36 -> 2929, 37 -> 2874, 38 -> 2819, 39 -> 2764, 40 -> 2709, 41 -> 2654, 42 -> 2599, 43 -> 2544, 44 -> 2447, 45 -> 2392, 46 -> 2337, 47 -> 2282, 48 -> 2227, 49 -> 2167, 50 -> 2134, 51 -> 2063, 52 -> 1992, 53 -> 1921, 54 -> 1850, 55 -> 1779, 56 -> 1708, 57 -> 1637, 58 -> 1566, 59 -> 1529, 60 -> 1311, 61 -> 1267, 62 -> 1196, 63 -> 1066, 64 -> 995, 65 -> 924, 66 -> 853, 67 -> 782, 68 -> 706
    //   676: aload #6
    //   678: astore #12
    //   680: aload #6
    //   682: ifnonnull -> 5454
    //   685: aload #6
    //   687: astore #10
    //   689: aload #6
    //   691: astore #7
    //   693: aload #11
    //   695: astore #13
    //   697: aload_1
    //   698: invokevirtual newBuilder : ()Ljava/lang/Object;
    //   701: astore #12
    //   703: goto -> 5454
    //   706: aload #6
    //   708: astore #7
    //   710: aload #11
    //   712: astore #13
    //   714: iload #15
    //   716: invokestatic offset : (I)J
    //   719: lstore #17
    //   721: aload #6
    //   723: astore #7
    //   725: aload #11
    //   727: astore #13
    //   729: aload #4
    //   731: aload_0
    //   732: iload #9
    //   734: invokespecial getMessageFieldSchema : (I)Lcom/android/framework/protobuf/Schema;
    //   737: aload #5
    //   739: invokeinterface readGroupBySchemaWithCheck : (Lcom/android/framework/protobuf/Schema;Lcom/android/framework/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;
    //   744: astore #12
    //   746: aload #6
    //   748: astore #7
    //   750: aload #11
    //   752: astore #13
    //   754: aload_3
    //   755: lload #17
    //   757: aload #12
    //   759: invokestatic putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   762: aload #6
    //   764: astore #7
    //   766: aload #11
    //   768: astore #13
    //   770: aload_0
    //   771: aload_3
    //   772: iload #8
    //   774: iload #9
    //   776: invokespecial setOneofPresent : (Ljava/lang/Object;II)V
    //   779: goto -> 5540
    //   782: aload #6
    //   784: astore #7
    //   786: aload #11
    //   788: astore #13
    //   790: iload #15
    //   792: invokestatic offset : (I)J
    //   795: lstore #19
    //   797: aload #6
    //   799: astore #7
    //   801: aload #11
    //   803: astore #13
    //   805: aload #4
    //   807: invokeinterface readSInt64 : ()J
    //   812: lstore #17
    //   814: aload #6
    //   816: astore #7
    //   818: aload #11
    //   820: astore #13
    //   822: aload_3
    //   823: lload #19
    //   825: lload #17
    //   827: invokestatic valueOf : (J)Ljava/lang/Long;
    //   830: invokestatic putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   833: aload #6
    //   835: astore #7
    //   837: aload #11
    //   839: astore #13
    //   841: aload_0
    //   842: aload_3
    //   843: iload #8
    //   845: iload #9
    //   847: invokespecial setOneofPresent : (Ljava/lang/Object;II)V
    //   850: goto -> 5540
    //   853: aload #6
    //   855: astore #7
    //   857: aload #11
    //   859: astore #13
    //   861: iload #15
    //   863: invokestatic offset : (I)J
    //   866: lstore #17
    //   868: aload #6
    //   870: astore #7
    //   872: aload #11
    //   874: astore #13
    //   876: aload #4
    //   878: invokeinterface readSInt32 : ()I
    //   883: istore #15
    //   885: aload #6
    //   887: astore #7
    //   889: aload #11
    //   891: astore #13
    //   893: aload_3
    //   894: lload #17
    //   896: iload #15
    //   898: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   901: invokestatic putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   904: aload #6
    //   906: astore #7
    //   908: aload #11
    //   910: astore #13
    //   912: aload_0
    //   913: aload_3
    //   914: iload #8
    //   916: iload #9
    //   918: invokespecial setOneofPresent : (Ljava/lang/Object;II)V
    //   921: goto -> 5540
    //   924: aload #6
    //   926: astore #7
    //   928: aload #11
    //   930: astore #13
    //   932: iload #15
    //   934: invokestatic offset : (I)J
    //   937: lstore #17
    //   939: aload #6
    //   941: astore #7
    //   943: aload #11
    //   945: astore #13
    //   947: aload #4
    //   949: invokeinterface readSFixed64 : ()J
    //   954: lstore #19
    //   956: aload #6
    //   958: astore #7
    //   960: aload #11
    //   962: astore #13
    //   964: aload_3
    //   965: lload #17
    //   967: lload #19
    //   969: invokestatic valueOf : (J)Ljava/lang/Long;
    //   972: invokestatic putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   975: aload #6
    //   977: astore #7
    //   979: aload #11
    //   981: astore #13
    //   983: aload_0
    //   984: aload_3
    //   985: iload #8
    //   987: iload #9
    //   989: invokespecial setOneofPresent : (Ljava/lang/Object;II)V
    //   992: goto -> 5540
    //   995: aload #6
    //   997: astore #7
    //   999: aload #11
    //   1001: astore #13
    //   1003: iload #15
    //   1005: invokestatic offset : (I)J
    //   1008: lstore #17
    //   1010: aload #6
    //   1012: astore #7
    //   1014: aload #11
    //   1016: astore #13
    //   1018: aload #4
    //   1020: invokeinterface readSFixed32 : ()I
    //   1025: istore #15
    //   1027: aload #6
    //   1029: astore #7
    //   1031: aload #11
    //   1033: astore #13
    //   1035: aload_3
    //   1036: lload #17
    //   1038: iload #15
    //   1040: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1043: invokestatic putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   1046: aload #6
    //   1048: astore #7
    //   1050: aload #11
    //   1052: astore #13
    //   1054: aload_0
    //   1055: aload_3
    //   1056: iload #8
    //   1058: iload #9
    //   1060: invokespecial setOneofPresent : (Ljava/lang/Object;II)V
    //   1063: goto -> 5540
    //   1066: aload #6
    //   1068: astore #7
    //   1070: aload #11
    //   1072: astore #13
    //   1074: aload #4
    //   1076: invokeinterface readEnum : ()I
    //   1081: istore #16
    //   1083: aload #6
    //   1085: astore #7
    //   1087: aload #11
    //   1089: astore #13
    //   1091: aload_0
    //   1092: iload #9
    //   1094: invokespecial getEnumFieldVerifier : (I)Lcom/android/framework/protobuf/Internal$EnumVerifier;
    //   1097: astore #12
    //   1099: aload #12
    //   1101: ifnull -> 1154
    //   1104: aload #6
    //   1106: astore #7
    //   1108: aload #11
    //   1110: astore #13
    //   1112: aload #12
    //   1114: iload #16
    //   1116: invokeinterface isInRange : (I)Z
    //   1121: ifeq -> 1127
    //   1124: goto -> 1154
    //   1127: aload #6
    //   1129: astore #7
    //   1131: aload #11
    //   1133: astore #13
    //   1135: iload #8
    //   1137: iload #16
    //   1139: aload #6
    //   1141: aload_1
    //   1142: invokestatic storeUnknownEnum : (IILjava/lang/Object;Lcom/android/framework/protobuf/UnknownFieldSchema;)Ljava/lang/Object;
    //   1145: astore #12
    //   1147: aload #12
    //   1149: astore #6
    //   1151: goto -> 5540
    //   1154: aload #6
    //   1156: astore #7
    //   1158: aload #11
    //   1160: astore #13
    //   1162: aload_3
    //   1163: iload #15
    //   1165: invokestatic offset : (I)J
    //   1168: iload #16
    //   1170: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1173: invokestatic putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   1176: aload #6
    //   1178: astore #7
    //   1180: aload #11
    //   1182: astore #13
    //   1184: aload_0
    //   1185: aload_3
    //   1186: iload #8
    //   1188: iload #9
    //   1190: invokespecial setOneofPresent : (Ljava/lang/Object;II)V
    //   1193: goto -> 5540
    //   1196: aload #6
    //   1198: astore #7
    //   1200: aload #11
    //   1202: astore #13
    //   1204: iload #15
    //   1206: invokestatic offset : (I)J
    //   1209: lstore #17
    //   1211: aload #6
    //   1213: astore #7
    //   1215: aload #11
    //   1217: astore #13
    //   1219: aload #4
    //   1221: invokeinterface readUInt32 : ()I
    //   1226: istore #15
    //   1228: aload #6
    //   1230: astore #7
    //   1232: aload #11
    //   1234: astore #13
    //   1236: aload_3
    //   1237: lload #17
    //   1239: iload #15
    //   1241: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1244: invokestatic putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   1247: aload #6
    //   1249: astore #7
    //   1251: aload #11
    //   1253: astore #13
    //   1255: aload_0
    //   1256: aload_3
    //   1257: iload #8
    //   1259: iload #9
    //   1261: invokespecial setOneofPresent : (Ljava/lang/Object;II)V
    //   1264: goto -> 5540
    //   1267: aload #6
    //   1269: astore #7
    //   1271: aload #11
    //   1273: astore #13
    //   1275: aload_3
    //   1276: iload #15
    //   1278: invokestatic offset : (I)J
    //   1281: aload #4
    //   1283: invokeinterface readBytes : ()Lcom/android/framework/protobuf/ByteString;
    //   1288: invokestatic putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   1291: aload #6
    //   1293: astore #7
    //   1295: aload #11
    //   1297: astore #13
    //   1299: aload_0
    //   1300: aload_3
    //   1301: iload #8
    //   1303: iload #9
    //   1305: invokespecial setOneofPresent : (Ljava/lang/Object;II)V
    //   1308: goto -> 5540
    //   1311: aload #6
    //   1313: astore #7
    //   1315: aload #11
    //   1317: astore #13
    //   1319: aload_0
    //   1320: aload_3
    //   1321: iload #8
    //   1323: iload #9
    //   1325: invokespecial isOneofPresent : (Ljava/lang/Object;II)Z
    //   1328: ifeq -> 1426
    //   1331: aload #6
    //   1333: astore #7
    //   1335: aload #11
    //   1337: astore #13
    //   1339: aload_3
    //   1340: iload #15
    //   1342: invokestatic offset : (I)J
    //   1345: invokestatic getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   1348: astore #12
    //   1350: aload #6
    //   1352: astore #7
    //   1354: aload #11
    //   1356: astore #13
    //   1358: aload_0
    //   1359: iload #9
    //   1361: invokespecial getMessageFieldSchema : (I)Lcom/android/framework/protobuf/Schema;
    //   1364: astore #10
    //   1366: aload #6
    //   1368: astore #7
    //   1370: aload #11
    //   1372: astore #13
    //   1374: aload #4
    //   1376: aload #10
    //   1378: aload #5
    //   1380: invokeinterface readMessageBySchemaWithCheck : (Lcom/android/framework/protobuf/Schema;Lcom/android/framework/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;
    //   1385: astore #10
    //   1387: aload #6
    //   1389: astore #7
    //   1391: aload #11
    //   1393: astore #13
    //   1395: aload #12
    //   1397: aload #10
    //   1399: invokestatic mergeMessage : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   1402: astore #12
    //   1404: aload #6
    //   1406: astore #7
    //   1408: aload #11
    //   1410: astore #13
    //   1412: aload_3
    //   1413: iload #15
    //   1415: invokestatic offset : (I)J
    //   1418: aload #12
    //   1420: invokestatic putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   1423: goto -> 1509
    //   1426: aload #6
    //   1428: astore #7
    //   1430: aload #11
    //   1432: astore #13
    //   1434: iload #15
    //   1436: invokestatic offset : (I)J
    //   1439: lstore #17
    //   1441: aload #6
    //   1443: astore #7
    //   1445: aload #11
    //   1447: astore #13
    //   1449: aload_0
    //   1450: iload #9
    //   1452: invokespecial getMessageFieldSchema : (I)Lcom/android/framework/protobuf/Schema;
    //   1455: astore #12
    //   1457: aload #6
    //   1459: astore #7
    //   1461: aload #11
    //   1463: astore #13
    //   1465: aload #4
    //   1467: aload #12
    //   1469: aload #5
    //   1471: invokeinterface readMessageBySchemaWithCheck : (Lcom/android/framework/protobuf/Schema;Lcom/android/framework/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;
    //   1476: astore #12
    //   1478: aload #6
    //   1480: astore #7
    //   1482: aload #11
    //   1484: astore #13
    //   1486: aload_3
    //   1487: lload #17
    //   1489: aload #12
    //   1491: invokestatic putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   1494: aload #6
    //   1496: astore #7
    //   1498: aload #11
    //   1500: astore #13
    //   1502: aload_0
    //   1503: aload_3
    //   1504: iload #9
    //   1506: invokespecial setFieldPresent : (Ljava/lang/Object;I)V
    //   1509: aload #6
    //   1511: astore #7
    //   1513: aload #11
    //   1515: astore #13
    //   1517: aload_0
    //   1518: aload_3
    //   1519: iload #8
    //   1521: iload #9
    //   1523: invokespecial setOneofPresent : (Ljava/lang/Object;II)V
    //   1526: goto -> 5540
    //   1529: aload #6
    //   1531: astore #7
    //   1533: aload #11
    //   1535: astore #13
    //   1537: aload_0
    //   1538: aload_3
    //   1539: iload #15
    //   1541: aload #4
    //   1543: invokespecial readString : (Ljava/lang/Object;ILcom/android/framework/protobuf/Reader;)V
    //   1546: aload #6
    //   1548: astore #7
    //   1550: aload #11
    //   1552: astore #13
    //   1554: aload_0
    //   1555: aload_3
    //   1556: iload #8
    //   1558: iload #9
    //   1560: invokespecial setOneofPresent : (Ljava/lang/Object;II)V
    //   1563: goto -> 5540
    //   1566: aload #6
    //   1568: astore #7
    //   1570: aload #11
    //   1572: astore #13
    //   1574: iload #15
    //   1576: invokestatic offset : (I)J
    //   1579: lstore #17
    //   1581: aload #6
    //   1583: astore #7
    //   1585: aload #11
    //   1587: astore #13
    //   1589: aload #4
    //   1591: invokeinterface readBool : ()Z
    //   1596: istore #14
    //   1598: aload #6
    //   1600: astore #7
    //   1602: aload #11
    //   1604: astore #13
    //   1606: aload_3
    //   1607: lload #17
    //   1609: iload #14
    //   1611: invokestatic valueOf : (Z)Ljava/lang/Boolean;
    //   1614: invokestatic putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   1617: aload #6
    //   1619: astore #7
    //   1621: aload #11
    //   1623: astore #13
    //   1625: aload_0
    //   1626: aload_3
    //   1627: iload #8
    //   1629: iload #9
    //   1631: invokespecial setOneofPresent : (Ljava/lang/Object;II)V
    //   1634: goto -> 5540
    //   1637: aload #6
    //   1639: astore #7
    //   1641: aload #11
    //   1643: astore #13
    //   1645: iload #15
    //   1647: invokestatic offset : (I)J
    //   1650: lstore #17
    //   1652: aload #6
    //   1654: astore #7
    //   1656: aload #11
    //   1658: astore #13
    //   1660: aload #4
    //   1662: invokeinterface readFixed32 : ()I
    //   1667: istore #15
    //   1669: aload #6
    //   1671: astore #7
    //   1673: aload #11
    //   1675: astore #13
    //   1677: aload_3
    //   1678: lload #17
    //   1680: iload #15
    //   1682: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1685: invokestatic putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   1688: aload #6
    //   1690: astore #7
    //   1692: aload #11
    //   1694: astore #13
    //   1696: aload_0
    //   1697: aload_3
    //   1698: iload #8
    //   1700: iload #9
    //   1702: invokespecial setOneofPresent : (Ljava/lang/Object;II)V
    //   1705: goto -> 5540
    //   1708: aload #6
    //   1710: astore #7
    //   1712: aload #11
    //   1714: astore #13
    //   1716: iload #15
    //   1718: invokestatic offset : (I)J
    //   1721: lstore #19
    //   1723: aload #6
    //   1725: astore #7
    //   1727: aload #11
    //   1729: astore #13
    //   1731: aload #4
    //   1733: invokeinterface readFixed64 : ()J
    //   1738: lstore #17
    //   1740: aload #6
    //   1742: astore #7
    //   1744: aload #11
    //   1746: astore #13
    //   1748: aload_3
    //   1749: lload #19
    //   1751: lload #17
    //   1753: invokestatic valueOf : (J)Ljava/lang/Long;
    //   1756: invokestatic putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   1759: aload #6
    //   1761: astore #7
    //   1763: aload #11
    //   1765: astore #13
    //   1767: aload_0
    //   1768: aload_3
    //   1769: iload #8
    //   1771: iload #9
    //   1773: invokespecial setOneofPresent : (Ljava/lang/Object;II)V
    //   1776: goto -> 5540
    //   1779: aload #6
    //   1781: astore #7
    //   1783: aload #11
    //   1785: astore #13
    //   1787: iload #15
    //   1789: invokestatic offset : (I)J
    //   1792: lstore #17
    //   1794: aload #6
    //   1796: astore #7
    //   1798: aload #11
    //   1800: astore #13
    //   1802: aload #4
    //   1804: invokeinterface readInt32 : ()I
    //   1809: istore #15
    //   1811: aload #6
    //   1813: astore #7
    //   1815: aload #11
    //   1817: astore #13
    //   1819: aload_3
    //   1820: lload #17
    //   1822: iload #15
    //   1824: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1827: invokestatic putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   1830: aload #6
    //   1832: astore #7
    //   1834: aload #11
    //   1836: astore #13
    //   1838: aload_0
    //   1839: aload_3
    //   1840: iload #8
    //   1842: iload #9
    //   1844: invokespecial setOneofPresent : (Ljava/lang/Object;II)V
    //   1847: goto -> 5540
    //   1850: aload #6
    //   1852: astore #7
    //   1854: aload #11
    //   1856: astore #13
    //   1858: iload #15
    //   1860: invokestatic offset : (I)J
    //   1863: lstore #17
    //   1865: aload #6
    //   1867: astore #7
    //   1869: aload #11
    //   1871: astore #13
    //   1873: aload #4
    //   1875: invokeinterface readUInt64 : ()J
    //   1880: lstore #19
    //   1882: aload #6
    //   1884: astore #7
    //   1886: aload #11
    //   1888: astore #13
    //   1890: aload_3
    //   1891: lload #17
    //   1893: lload #19
    //   1895: invokestatic valueOf : (J)Ljava/lang/Long;
    //   1898: invokestatic putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   1901: aload #6
    //   1903: astore #7
    //   1905: aload #11
    //   1907: astore #13
    //   1909: aload_0
    //   1910: aload_3
    //   1911: iload #8
    //   1913: iload #9
    //   1915: invokespecial setOneofPresent : (Ljava/lang/Object;II)V
    //   1918: goto -> 5540
    //   1921: aload #6
    //   1923: astore #7
    //   1925: aload #11
    //   1927: astore #13
    //   1929: iload #15
    //   1931: invokestatic offset : (I)J
    //   1934: lstore #17
    //   1936: aload #6
    //   1938: astore #7
    //   1940: aload #11
    //   1942: astore #13
    //   1944: aload #4
    //   1946: invokeinterface readInt64 : ()J
    //   1951: lstore #19
    //   1953: aload #6
    //   1955: astore #7
    //   1957: aload #11
    //   1959: astore #13
    //   1961: aload_3
    //   1962: lload #17
    //   1964: lload #19
    //   1966: invokestatic valueOf : (J)Ljava/lang/Long;
    //   1969: invokestatic putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   1972: aload #6
    //   1974: astore #7
    //   1976: aload #11
    //   1978: astore #13
    //   1980: aload_0
    //   1981: aload_3
    //   1982: iload #8
    //   1984: iload #9
    //   1986: invokespecial setOneofPresent : (Ljava/lang/Object;II)V
    //   1989: goto -> 5540
    //   1992: aload #6
    //   1994: astore #7
    //   1996: aload #11
    //   1998: astore #13
    //   2000: iload #15
    //   2002: invokestatic offset : (I)J
    //   2005: lstore #17
    //   2007: aload #6
    //   2009: astore #7
    //   2011: aload #11
    //   2013: astore #13
    //   2015: aload #4
    //   2017: invokeinterface readFloat : ()F
    //   2022: fstore #21
    //   2024: aload #6
    //   2026: astore #7
    //   2028: aload #11
    //   2030: astore #13
    //   2032: aload_3
    //   2033: lload #17
    //   2035: fload #21
    //   2037: invokestatic valueOf : (F)Ljava/lang/Float;
    //   2040: invokestatic putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   2043: aload #6
    //   2045: astore #7
    //   2047: aload #11
    //   2049: astore #13
    //   2051: aload_0
    //   2052: aload_3
    //   2053: iload #8
    //   2055: iload #9
    //   2057: invokespecial setOneofPresent : (Ljava/lang/Object;II)V
    //   2060: goto -> 5540
    //   2063: aload #6
    //   2065: astore #7
    //   2067: aload #11
    //   2069: astore #13
    //   2071: iload #15
    //   2073: invokestatic offset : (I)J
    //   2076: lstore #17
    //   2078: aload #6
    //   2080: astore #7
    //   2082: aload #11
    //   2084: astore #13
    //   2086: aload #4
    //   2088: invokeinterface readDouble : ()D
    //   2093: dstore #22
    //   2095: aload #6
    //   2097: astore #7
    //   2099: aload #11
    //   2101: astore #13
    //   2103: aload_3
    //   2104: lload #17
    //   2106: dload #22
    //   2108: invokestatic valueOf : (D)Ljava/lang/Double;
    //   2111: invokestatic putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   2114: aload #6
    //   2116: astore #7
    //   2118: aload #11
    //   2120: astore #13
    //   2122: aload_0
    //   2123: aload_3
    //   2124: iload #8
    //   2126: iload #9
    //   2128: invokespecial setOneofPresent : (Ljava/lang/Object;II)V
    //   2131: goto -> 5540
    //   2134: aload #6
    //   2136: astore #7
    //   2138: aload #11
    //   2140: astore #13
    //   2142: aload_0
    //   2143: aload_3
    //   2144: iload #9
    //   2146: aload_0
    //   2147: iload #9
    //   2149: invokespecial getMapFieldDefaultEntry : (I)Ljava/lang/Object;
    //   2152: aload #5
    //   2154: aload #4
    //   2156: invokespecial mergeMap : (Ljava/lang/Object;ILjava/lang/Object;Lcom/android/framework/protobuf/ExtensionRegistryLite;Lcom/android/framework/protobuf/Reader;)V
    //   2159: goto -> 5540
    //   2162: astore #7
    //   2164: goto -> 5549
    //   2167: aload #6
    //   2169: astore #7
    //   2171: aload #11
    //   2173: astore #13
    //   2175: iload #15
    //   2177: invokestatic offset : (I)J
    //   2180: lstore #17
    //   2182: aload #6
    //   2184: astore #7
    //   2186: aload #11
    //   2188: astore #13
    //   2190: aload_0
    //   2191: iload #9
    //   2193: invokespecial getMessageFieldSchema : (I)Lcom/android/framework/protobuf/Schema;
    //   2196: astore #12
    //   2198: aload #6
    //   2200: astore #7
    //   2202: aload #11
    //   2204: astore #13
    //   2206: aload_0
    //   2207: aload_3
    //   2208: lload #17
    //   2210: aload #4
    //   2212: aload #12
    //   2214: aload #5
    //   2216: invokespecial readGroupList : (Ljava/lang/Object;JLcom/android/framework/protobuf/Reader;Lcom/android/framework/protobuf/Schema;Lcom/android/framework/protobuf/ExtensionRegistryLite;)V
    //   2219: goto -> 5540
    //   2222: astore #7
    //   2224: goto -> 5549
    //   2227: aload #6
    //   2229: astore #7
    //   2231: aload #11
    //   2233: astore #13
    //   2235: aload_0
    //   2236: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   2239: astore #12
    //   2241: aload #6
    //   2243: astore #7
    //   2245: aload #11
    //   2247: astore #13
    //   2249: aload #12
    //   2251: aload_3
    //   2252: iload #15
    //   2254: invokestatic offset : (I)J
    //   2257: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2260: astore #12
    //   2262: aload #6
    //   2264: astore #7
    //   2266: aload #11
    //   2268: astore #13
    //   2270: aload #4
    //   2272: aload #12
    //   2274: invokeinterface readSInt64List : (Ljava/util/List;)V
    //   2279: goto -> 5540
    //   2282: aload #6
    //   2284: astore #7
    //   2286: aload #11
    //   2288: astore #13
    //   2290: aload_0
    //   2291: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   2294: astore #12
    //   2296: aload #6
    //   2298: astore #7
    //   2300: aload #11
    //   2302: astore #13
    //   2304: aload #12
    //   2306: aload_3
    //   2307: iload #15
    //   2309: invokestatic offset : (I)J
    //   2312: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2315: astore #12
    //   2317: aload #6
    //   2319: astore #7
    //   2321: aload #11
    //   2323: astore #13
    //   2325: aload #4
    //   2327: aload #12
    //   2329: invokeinterface readSInt32List : (Ljava/util/List;)V
    //   2334: goto -> 5540
    //   2337: aload #6
    //   2339: astore #7
    //   2341: aload #11
    //   2343: astore #13
    //   2345: aload_0
    //   2346: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   2349: astore #12
    //   2351: aload #6
    //   2353: astore #7
    //   2355: aload #11
    //   2357: astore #13
    //   2359: aload #12
    //   2361: aload_3
    //   2362: iload #15
    //   2364: invokestatic offset : (I)J
    //   2367: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2370: astore #12
    //   2372: aload #6
    //   2374: astore #7
    //   2376: aload #11
    //   2378: astore #13
    //   2380: aload #4
    //   2382: aload #12
    //   2384: invokeinterface readSFixed64List : (Ljava/util/List;)V
    //   2389: goto -> 5540
    //   2392: aload #6
    //   2394: astore #7
    //   2396: aload #11
    //   2398: astore #13
    //   2400: aload_0
    //   2401: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   2404: astore #12
    //   2406: aload #6
    //   2408: astore #7
    //   2410: aload #11
    //   2412: astore #13
    //   2414: aload #12
    //   2416: aload_3
    //   2417: iload #15
    //   2419: invokestatic offset : (I)J
    //   2422: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2425: astore #12
    //   2427: aload #6
    //   2429: astore #7
    //   2431: aload #11
    //   2433: astore #13
    //   2435: aload #4
    //   2437: aload #12
    //   2439: invokeinterface readSFixed32List : (Ljava/util/List;)V
    //   2444: goto -> 5540
    //   2447: aload #6
    //   2449: astore #7
    //   2451: aload #11
    //   2453: astore #13
    //   2455: aload_0
    //   2456: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   2459: astore #12
    //   2461: aload #6
    //   2463: astore #7
    //   2465: aload #11
    //   2467: astore #13
    //   2469: aload #12
    //   2471: aload_3
    //   2472: iload #15
    //   2474: invokestatic offset : (I)J
    //   2477: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2480: astore #10
    //   2482: aload #6
    //   2484: astore #7
    //   2486: aload #11
    //   2488: astore #13
    //   2490: aload #4
    //   2492: aload #10
    //   2494: invokeinterface readEnumList : (Ljava/util/List;)V
    //   2499: aload #6
    //   2501: astore #7
    //   2503: aload #11
    //   2505: astore #13
    //   2507: aload_0
    //   2508: iload #9
    //   2510: invokespecial getEnumFieldVerifier : (I)Lcom/android/framework/protobuf/Internal$EnumVerifier;
    //   2513: astore #12
    //   2515: aload #6
    //   2517: astore #7
    //   2519: aload #11
    //   2521: astore #13
    //   2523: iload #8
    //   2525: aload #10
    //   2527: aload #12
    //   2529: aload #6
    //   2531: aload_1
    //   2532: invokestatic filterUnknownEnumList : (ILjava/util/List;Lcom/android/framework/protobuf/Internal$EnumVerifier;Ljava/lang/Object;Lcom/android/framework/protobuf/UnknownFieldSchema;)Ljava/lang/Object;
    //   2535: astore #12
    //   2537: aload #12
    //   2539: astore #6
    //   2541: goto -> 5540
    //   2544: aload #6
    //   2546: astore #7
    //   2548: aload #11
    //   2550: astore #13
    //   2552: aload_0
    //   2553: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   2556: astore #12
    //   2558: aload #6
    //   2560: astore #7
    //   2562: aload #11
    //   2564: astore #13
    //   2566: aload #12
    //   2568: aload_3
    //   2569: iload #15
    //   2571: invokestatic offset : (I)J
    //   2574: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2577: astore #12
    //   2579: aload #6
    //   2581: astore #7
    //   2583: aload #11
    //   2585: astore #13
    //   2587: aload #4
    //   2589: aload #12
    //   2591: invokeinterface readUInt32List : (Ljava/util/List;)V
    //   2596: goto -> 5540
    //   2599: aload #6
    //   2601: astore #7
    //   2603: aload #11
    //   2605: astore #13
    //   2607: aload_0
    //   2608: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   2611: astore #12
    //   2613: aload #6
    //   2615: astore #7
    //   2617: aload #11
    //   2619: astore #13
    //   2621: aload #12
    //   2623: aload_3
    //   2624: iload #15
    //   2626: invokestatic offset : (I)J
    //   2629: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2632: astore #12
    //   2634: aload #6
    //   2636: astore #7
    //   2638: aload #11
    //   2640: astore #13
    //   2642: aload #4
    //   2644: aload #12
    //   2646: invokeinterface readBoolList : (Ljava/util/List;)V
    //   2651: goto -> 5540
    //   2654: aload #6
    //   2656: astore #7
    //   2658: aload #11
    //   2660: astore #13
    //   2662: aload_0
    //   2663: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   2666: astore #12
    //   2668: aload #6
    //   2670: astore #7
    //   2672: aload #11
    //   2674: astore #13
    //   2676: aload #12
    //   2678: aload_3
    //   2679: iload #15
    //   2681: invokestatic offset : (I)J
    //   2684: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2687: astore #12
    //   2689: aload #6
    //   2691: astore #7
    //   2693: aload #11
    //   2695: astore #13
    //   2697: aload #4
    //   2699: aload #12
    //   2701: invokeinterface readFixed32List : (Ljava/util/List;)V
    //   2706: goto -> 5540
    //   2709: aload #6
    //   2711: astore #7
    //   2713: aload #11
    //   2715: astore #13
    //   2717: aload_0
    //   2718: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   2721: astore #12
    //   2723: aload #6
    //   2725: astore #7
    //   2727: aload #11
    //   2729: astore #13
    //   2731: aload #12
    //   2733: aload_3
    //   2734: iload #15
    //   2736: invokestatic offset : (I)J
    //   2739: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2742: astore #12
    //   2744: aload #6
    //   2746: astore #7
    //   2748: aload #11
    //   2750: astore #13
    //   2752: aload #4
    //   2754: aload #12
    //   2756: invokeinterface readFixed64List : (Ljava/util/List;)V
    //   2761: goto -> 5540
    //   2764: aload #6
    //   2766: astore #7
    //   2768: aload #11
    //   2770: astore #13
    //   2772: aload_0
    //   2773: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   2776: astore #12
    //   2778: aload #6
    //   2780: astore #7
    //   2782: aload #11
    //   2784: astore #13
    //   2786: aload #12
    //   2788: aload_3
    //   2789: iload #15
    //   2791: invokestatic offset : (I)J
    //   2794: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2797: astore #12
    //   2799: aload #6
    //   2801: astore #7
    //   2803: aload #11
    //   2805: astore #13
    //   2807: aload #4
    //   2809: aload #12
    //   2811: invokeinterface readInt32List : (Ljava/util/List;)V
    //   2816: goto -> 5540
    //   2819: aload #6
    //   2821: astore #7
    //   2823: aload #11
    //   2825: astore #13
    //   2827: aload_0
    //   2828: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   2831: astore #12
    //   2833: aload #6
    //   2835: astore #7
    //   2837: aload #11
    //   2839: astore #13
    //   2841: aload #12
    //   2843: aload_3
    //   2844: iload #15
    //   2846: invokestatic offset : (I)J
    //   2849: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2852: astore #12
    //   2854: aload #6
    //   2856: astore #7
    //   2858: aload #11
    //   2860: astore #13
    //   2862: aload #4
    //   2864: aload #12
    //   2866: invokeinterface readUInt64List : (Ljava/util/List;)V
    //   2871: goto -> 5540
    //   2874: aload #6
    //   2876: astore #7
    //   2878: aload #11
    //   2880: astore #13
    //   2882: aload_0
    //   2883: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   2886: astore #12
    //   2888: aload #6
    //   2890: astore #7
    //   2892: aload #11
    //   2894: astore #13
    //   2896: aload #12
    //   2898: aload_3
    //   2899: iload #15
    //   2901: invokestatic offset : (I)J
    //   2904: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2907: astore #12
    //   2909: aload #6
    //   2911: astore #7
    //   2913: aload #11
    //   2915: astore #13
    //   2917: aload #4
    //   2919: aload #12
    //   2921: invokeinterface readInt64List : (Ljava/util/List;)V
    //   2926: goto -> 5540
    //   2929: aload #6
    //   2931: astore #7
    //   2933: aload #11
    //   2935: astore #13
    //   2937: aload_0
    //   2938: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   2941: astore #12
    //   2943: aload #6
    //   2945: astore #7
    //   2947: aload #11
    //   2949: astore #13
    //   2951: aload #12
    //   2953: aload_3
    //   2954: iload #15
    //   2956: invokestatic offset : (I)J
    //   2959: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   2962: astore #12
    //   2964: aload #6
    //   2966: astore #7
    //   2968: aload #11
    //   2970: astore #13
    //   2972: aload #4
    //   2974: aload #12
    //   2976: invokeinterface readFloatList : (Ljava/util/List;)V
    //   2981: goto -> 5540
    //   2984: aload #6
    //   2986: astore #7
    //   2988: aload #11
    //   2990: astore #13
    //   2992: aload_0
    //   2993: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   2996: astore #12
    //   2998: aload #6
    //   3000: astore #7
    //   3002: aload #11
    //   3004: astore #13
    //   3006: aload #12
    //   3008: aload_3
    //   3009: iload #15
    //   3011: invokestatic offset : (I)J
    //   3014: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   3017: astore #12
    //   3019: aload #6
    //   3021: astore #7
    //   3023: aload #11
    //   3025: astore #13
    //   3027: aload #4
    //   3029: aload #12
    //   3031: invokeinterface readDoubleList : (Ljava/util/List;)V
    //   3036: goto -> 5540
    //   3039: aload #6
    //   3041: astore #7
    //   3043: aload #11
    //   3045: astore #13
    //   3047: aload_0
    //   3048: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   3051: astore #12
    //   3053: aload #6
    //   3055: astore #7
    //   3057: aload #11
    //   3059: astore #13
    //   3061: aload #12
    //   3063: aload_3
    //   3064: iload #15
    //   3066: invokestatic offset : (I)J
    //   3069: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   3072: astore #12
    //   3074: aload #6
    //   3076: astore #7
    //   3078: aload #11
    //   3080: astore #13
    //   3082: aload #4
    //   3084: aload #12
    //   3086: invokeinterface readSInt64List : (Ljava/util/List;)V
    //   3091: goto -> 5540
    //   3094: aload #6
    //   3096: astore #7
    //   3098: aload #11
    //   3100: astore #13
    //   3102: aload_0
    //   3103: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   3106: astore #12
    //   3108: aload #6
    //   3110: astore #7
    //   3112: aload #11
    //   3114: astore #13
    //   3116: aload #12
    //   3118: aload_3
    //   3119: iload #15
    //   3121: invokestatic offset : (I)J
    //   3124: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   3127: astore #12
    //   3129: aload #6
    //   3131: astore #7
    //   3133: aload #11
    //   3135: astore #13
    //   3137: aload #4
    //   3139: aload #12
    //   3141: invokeinterface readSInt32List : (Ljava/util/List;)V
    //   3146: goto -> 5540
    //   3149: aload #6
    //   3151: astore #7
    //   3153: aload #11
    //   3155: astore #13
    //   3157: aload_0
    //   3158: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   3161: astore #12
    //   3163: aload #6
    //   3165: astore #7
    //   3167: aload #11
    //   3169: astore #13
    //   3171: aload #12
    //   3173: aload_3
    //   3174: iload #15
    //   3176: invokestatic offset : (I)J
    //   3179: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   3182: astore #12
    //   3184: aload #6
    //   3186: astore #7
    //   3188: aload #11
    //   3190: astore #13
    //   3192: aload #4
    //   3194: aload #12
    //   3196: invokeinterface readSFixed64List : (Ljava/util/List;)V
    //   3201: goto -> 5540
    //   3204: aload #6
    //   3206: astore #7
    //   3208: aload #11
    //   3210: astore #13
    //   3212: aload_0
    //   3213: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   3216: astore #12
    //   3218: aload #6
    //   3220: astore #7
    //   3222: aload #11
    //   3224: astore #13
    //   3226: aload #12
    //   3228: aload_3
    //   3229: iload #15
    //   3231: invokestatic offset : (I)J
    //   3234: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   3237: astore #12
    //   3239: aload #6
    //   3241: astore #7
    //   3243: aload #11
    //   3245: astore #13
    //   3247: aload #4
    //   3249: aload #12
    //   3251: invokeinterface readSFixed32List : (Ljava/util/List;)V
    //   3256: goto -> 5540
    //   3259: aload #6
    //   3261: astore #7
    //   3263: aload #11
    //   3265: astore #13
    //   3267: aload_0
    //   3268: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   3271: astore #12
    //   3273: aload #6
    //   3275: astore #7
    //   3277: aload #11
    //   3279: astore #13
    //   3281: aload #12
    //   3283: aload_3
    //   3284: iload #15
    //   3286: invokestatic offset : (I)J
    //   3289: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   3292: astore #12
    //   3294: aload #6
    //   3296: astore #7
    //   3298: aload #11
    //   3300: astore #13
    //   3302: aload #4
    //   3304: aload #12
    //   3306: invokeinterface readEnumList : (Ljava/util/List;)V
    //   3311: aload #6
    //   3313: astore #7
    //   3315: aload #11
    //   3317: astore #13
    //   3319: aload_0
    //   3320: iload #9
    //   3322: invokespecial getEnumFieldVerifier : (I)Lcom/android/framework/protobuf/Internal$EnumVerifier;
    //   3325: astore #10
    //   3327: aload #6
    //   3329: astore #7
    //   3331: aload #11
    //   3333: astore #13
    //   3335: iload #8
    //   3337: aload #12
    //   3339: aload #10
    //   3341: aload #6
    //   3343: aload_1
    //   3344: invokestatic filterUnknownEnumList : (ILjava/util/List;Lcom/android/framework/protobuf/Internal$EnumVerifier;Ljava/lang/Object;Lcom/android/framework/protobuf/UnknownFieldSchema;)Ljava/lang/Object;
    //   3347: astore #12
    //   3349: aload #12
    //   3351: astore #6
    //   3353: goto -> 5540
    //   3356: aload #6
    //   3358: astore #7
    //   3360: aload #11
    //   3362: astore #13
    //   3364: aload_0
    //   3365: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   3368: astore #12
    //   3370: aload #6
    //   3372: astore #7
    //   3374: aload #11
    //   3376: astore #13
    //   3378: aload #12
    //   3380: aload_3
    //   3381: iload #15
    //   3383: invokestatic offset : (I)J
    //   3386: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   3389: astore #12
    //   3391: aload #6
    //   3393: astore #7
    //   3395: aload #11
    //   3397: astore #13
    //   3399: aload #4
    //   3401: aload #12
    //   3403: invokeinterface readUInt32List : (Ljava/util/List;)V
    //   3408: goto -> 5540
    //   3411: aload #6
    //   3413: astore #7
    //   3415: aload #11
    //   3417: astore #13
    //   3419: aload_0
    //   3420: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   3423: astore #12
    //   3425: aload #6
    //   3427: astore #7
    //   3429: aload #11
    //   3431: astore #13
    //   3433: aload #12
    //   3435: aload_3
    //   3436: iload #15
    //   3438: invokestatic offset : (I)J
    //   3441: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   3444: astore #12
    //   3446: aload #6
    //   3448: astore #7
    //   3450: aload #11
    //   3452: astore #13
    //   3454: aload #4
    //   3456: aload #12
    //   3458: invokeinterface readBytesList : (Ljava/util/List;)V
    //   3463: goto -> 5540
    //   3466: aload #6
    //   3468: astore #7
    //   3470: aload #11
    //   3472: astore #13
    //   3474: aload_0
    //   3475: iload #9
    //   3477: invokespecial getMessageFieldSchema : (I)Lcom/android/framework/protobuf/Schema;
    //   3480: astore #12
    //   3482: aload #6
    //   3484: astore #7
    //   3486: aload #11
    //   3488: astore #13
    //   3490: aload_0
    //   3491: aload_3
    //   3492: iload #15
    //   3494: aload #4
    //   3496: aload #12
    //   3498: aload #5
    //   3500: invokespecial readMessageList : (Ljava/lang/Object;ILcom/android/framework/protobuf/Reader;Lcom/android/framework/protobuf/Schema;Lcom/android/framework/protobuf/ExtensionRegistryLite;)V
    //   3503: goto -> 5540
    //   3506: astore #7
    //   3508: goto -> 5549
    //   3511: aload #6
    //   3513: astore #10
    //   3515: aload #6
    //   3517: astore #7
    //   3519: aload #11
    //   3521: astore #13
    //   3523: aload_0
    //   3524: aload_3
    //   3525: iload #15
    //   3527: aload #4
    //   3529: invokespecial readStringList : (Ljava/lang/Object;ILcom/android/framework/protobuf/Reader;)V
    //   3532: goto -> 5540
    //   3535: aload #6
    //   3537: astore #10
    //   3539: aload #6
    //   3541: astore #7
    //   3543: aload #11
    //   3545: astore #13
    //   3547: aload_0
    //   3548: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   3551: astore #12
    //   3553: aload #6
    //   3555: astore #10
    //   3557: aload #6
    //   3559: astore #7
    //   3561: aload #11
    //   3563: astore #13
    //   3565: aload #12
    //   3567: aload_3
    //   3568: iload #15
    //   3570: invokestatic offset : (I)J
    //   3573: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   3576: astore #12
    //   3578: aload #6
    //   3580: astore #10
    //   3582: aload #6
    //   3584: astore #7
    //   3586: aload #11
    //   3588: astore #13
    //   3590: aload #4
    //   3592: aload #12
    //   3594: invokeinterface readBoolList : (Ljava/util/List;)V
    //   3599: goto -> 5540
    //   3602: aload #6
    //   3604: astore #10
    //   3606: aload #6
    //   3608: astore #7
    //   3610: aload #11
    //   3612: astore #13
    //   3614: aload_0
    //   3615: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   3618: astore #12
    //   3620: aload #6
    //   3622: astore #10
    //   3624: aload #6
    //   3626: astore #7
    //   3628: aload #11
    //   3630: astore #13
    //   3632: aload #12
    //   3634: aload_3
    //   3635: iload #15
    //   3637: invokestatic offset : (I)J
    //   3640: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   3643: astore #12
    //   3645: aload #6
    //   3647: astore #10
    //   3649: aload #6
    //   3651: astore #7
    //   3653: aload #11
    //   3655: astore #13
    //   3657: aload #4
    //   3659: aload #12
    //   3661: invokeinterface readFixed32List : (Ljava/util/List;)V
    //   3666: goto -> 5540
    //   3669: aload #6
    //   3671: astore #10
    //   3673: aload #6
    //   3675: astore #7
    //   3677: aload #11
    //   3679: astore #13
    //   3681: aload_0
    //   3682: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   3685: astore #12
    //   3687: aload #6
    //   3689: astore #10
    //   3691: aload #6
    //   3693: astore #7
    //   3695: aload #11
    //   3697: astore #13
    //   3699: aload #12
    //   3701: aload_3
    //   3702: iload #15
    //   3704: invokestatic offset : (I)J
    //   3707: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   3710: astore #12
    //   3712: aload #6
    //   3714: astore #10
    //   3716: aload #6
    //   3718: astore #7
    //   3720: aload #11
    //   3722: astore #13
    //   3724: aload #4
    //   3726: aload #12
    //   3728: invokeinterface readFixed64List : (Ljava/util/List;)V
    //   3733: goto -> 5540
    //   3736: aload #6
    //   3738: astore #10
    //   3740: aload #6
    //   3742: astore #7
    //   3744: aload #11
    //   3746: astore #13
    //   3748: aload_0
    //   3749: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   3752: astore #12
    //   3754: aload #6
    //   3756: astore #10
    //   3758: aload #6
    //   3760: astore #7
    //   3762: aload #11
    //   3764: astore #13
    //   3766: aload #12
    //   3768: aload_3
    //   3769: iload #15
    //   3771: invokestatic offset : (I)J
    //   3774: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   3777: astore #12
    //   3779: aload #6
    //   3781: astore #10
    //   3783: aload #6
    //   3785: astore #7
    //   3787: aload #11
    //   3789: astore #13
    //   3791: aload #4
    //   3793: aload #12
    //   3795: invokeinterface readInt32List : (Ljava/util/List;)V
    //   3800: goto -> 5540
    //   3803: aload #6
    //   3805: astore #10
    //   3807: aload #6
    //   3809: astore #7
    //   3811: aload #11
    //   3813: astore #13
    //   3815: aload_0
    //   3816: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   3819: astore #12
    //   3821: aload #6
    //   3823: astore #10
    //   3825: aload #6
    //   3827: astore #7
    //   3829: aload #11
    //   3831: astore #13
    //   3833: aload #12
    //   3835: aload_3
    //   3836: iload #15
    //   3838: invokestatic offset : (I)J
    //   3841: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   3844: astore #12
    //   3846: aload #6
    //   3848: astore #10
    //   3850: aload #6
    //   3852: astore #7
    //   3854: aload #11
    //   3856: astore #13
    //   3858: aload #4
    //   3860: aload #12
    //   3862: invokeinterface readUInt64List : (Ljava/util/List;)V
    //   3867: goto -> 5540
    //   3870: aload #6
    //   3872: astore #10
    //   3874: aload #6
    //   3876: astore #7
    //   3878: aload #11
    //   3880: astore #13
    //   3882: aload_0
    //   3883: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   3886: astore #12
    //   3888: aload #6
    //   3890: astore #10
    //   3892: aload #6
    //   3894: astore #7
    //   3896: aload #11
    //   3898: astore #13
    //   3900: aload #12
    //   3902: aload_3
    //   3903: iload #15
    //   3905: invokestatic offset : (I)J
    //   3908: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   3911: astore #12
    //   3913: aload #6
    //   3915: astore #10
    //   3917: aload #6
    //   3919: astore #7
    //   3921: aload #11
    //   3923: astore #13
    //   3925: aload #4
    //   3927: aload #12
    //   3929: invokeinterface readInt64List : (Ljava/util/List;)V
    //   3934: goto -> 5540
    //   3937: aload #6
    //   3939: astore #10
    //   3941: aload #6
    //   3943: astore #7
    //   3945: aload #11
    //   3947: astore #13
    //   3949: aload_0
    //   3950: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   3953: astore #12
    //   3955: aload #6
    //   3957: astore #10
    //   3959: aload #6
    //   3961: astore #7
    //   3963: aload #11
    //   3965: astore #13
    //   3967: aload #12
    //   3969: aload_3
    //   3970: iload #15
    //   3972: invokestatic offset : (I)J
    //   3975: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   3978: astore #12
    //   3980: aload #6
    //   3982: astore #10
    //   3984: aload #6
    //   3986: astore #7
    //   3988: aload #11
    //   3990: astore #13
    //   3992: aload #4
    //   3994: aload #12
    //   3996: invokeinterface readFloatList : (Ljava/util/List;)V
    //   4001: goto -> 5540
    //   4004: aload #6
    //   4006: astore #10
    //   4008: aload #6
    //   4010: astore #7
    //   4012: aload #11
    //   4014: astore #13
    //   4016: aload_0
    //   4017: getfield listFieldSchema : Lcom/android/framework/protobuf/ListFieldSchema;
    //   4020: astore #12
    //   4022: aload #6
    //   4024: astore #10
    //   4026: aload #6
    //   4028: astore #7
    //   4030: aload #11
    //   4032: astore #13
    //   4034: aload #12
    //   4036: aload_3
    //   4037: iload #15
    //   4039: invokestatic offset : (I)J
    //   4042: invokevirtual mutableListAt : (Ljava/lang/Object;J)Ljava/util/List;
    //   4045: astore #12
    //   4047: aload #6
    //   4049: astore #10
    //   4051: aload #6
    //   4053: astore #7
    //   4055: aload #11
    //   4057: astore #13
    //   4059: aload #4
    //   4061: aload #12
    //   4063: invokeinterface readDoubleList : (Ljava/util/List;)V
    //   4068: goto -> 5540
    //   4071: aload #6
    //   4073: astore #10
    //   4075: aload #6
    //   4077: astore #7
    //   4079: aload #11
    //   4081: astore #13
    //   4083: aload_0
    //   4084: aload_3
    //   4085: iload #9
    //   4087: invokespecial isFieldPresent : (Ljava/lang/Object;I)Z
    //   4090: ifeq -> 4208
    //   4093: aload #6
    //   4095: astore #10
    //   4097: aload #6
    //   4099: astore #7
    //   4101: aload #11
    //   4103: astore #13
    //   4105: aload_3
    //   4106: iload #15
    //   4108: invokestatic offset : (I)J
    //   4111: invokestatic getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   4114: astore #12
    //   4116: aload #6
    //   4118: astore #10
    //   4120: aload #6
    //   4122: astore #7
    //   4124: aload #11
    //   4126: astore #13
    //   4128: aload_0
    //   4129: iload #9
    //   4131: invokespecial getMessageFieldSchema : (I)Lcom/android/framework/protobuf/Schema;
    //   4134: astore #24
    //   4136: aload #6
    //   4138: astore #10
    //   4140: aload #6
    //   4142: astore #7
    //   4144: aload #11
    //   4146: astore #13
    //   4148: aload #4
    //   4150: aload #24
    //   4152: aload #5
    //   4154: invokeinterface readGroupBySchemaWithCheck : (Lcom/android/framework/protobuf/Schema;Lcom/android/framework/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;
    //   4159: astore #24
    //   4161: aload #6
    //   4163: astore #10
    //   4165: aload #6
    //   4167: astore #7
    //   4169: aload #11
    //   4171: astore #13
    //   4173: aload #12
    //   4175: aload #24
    //   4177: invokestatic mergeMessage : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   4180: astore #12
    //   4182: aload #6
    //   4184: astore #10
    //   4186: aload #6
    //   4188: astore #7
    //   4190: aload #11
    //   4192: astore #13
    //   4194: aload_3
    //   4195: iload #15
    //   4197: invokestatic offset : (I)J
    //   4200: aload #12
    //   4202: invokestatic putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   4205: goto -> 5540
    //   4208: aload #6
    //   4210: astore #10
    //   4212: aload #6
    //   4214: astore #7
    //   4216: aload #11
    //   4218: astore #13
    //   4220: iload #15
    //   4222: invokestatic offset : (I)J
    //   4225: lstore #17
    //   4227: aload #6
    //   4229: astore #10
    //   4231: aload #6
    //   4233: astore #7
    //   4235: aload #11
    //   4237: astore #13
    //   4239: aload_0
    //   4240: iload #9
    //   4242: invokespecial getMessageFieldSchema : (I)Lcom/android/framework/protobuf/Schema;
    //   4245: astore #12
    //   4247: aload #6
    //   4249: astore #10
    //   4251: aload #6
    //   4253: astore #7
    //   4255: aload #11
    //   4257: astore #13
    //   4259: aload #4
    //   4261: aload #12
    //   4263: aload #5
    //   4265: invokeinterface readGroupBySchemaWithCheck : (Lcom/android/framework/protobuf/Schema;Lcom/android/framework/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;
    //   4270: astore #12
    //   4272: aload #6
    //   4274: astore #10
    //   4276: aload #6
    //   4278: astore #7
    //   4280: aload #11
    //   4282: astore #13
    //   4284: aload_3
    //   4285: lload #17
    //   4287: aload #12
    //   4289: invokestatic putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   4292: aload #6
    //   4294: astore #10
    //   4296: aload #6
    //   4298: astore #7
    //   4300: aload #11
    //   4302: astore #13
    //   4304: aload_0
    //   4305: aload_3
    //   4306: iload #9
    //   4308: invokespecial setFieldPresent : (Ljava/lang/Object;I)V
    //   4311: goto -> 5540
    //   4314: aload #6
    //   4316: astore #10
    //   4318: aload #6
    //   4320: astore #7
    //   4322: aload #11
    //   4324: astore #13
    //   4326: aload_3
    //   4327: iload #15
    //   4329: invokestatic offset : (I)J
    //   4332: aload #4
    //   4334: invokeinterface readSInt64 : ()J
    //   4339: invokestatic putLong : (Ljava/lang/Object;JJ)V
    //   4342: aload #6
    //   4344: astore #10
    //   4346: aload #6
    //   4348: astore #7
    //   4350: aload #11
    //   4352: astore #13
    //   4354: aload_0
    //   4355: aload_3
    //   4356: iload #9
    //   4358: invokespecial setFieldPresent : (Ljava/lang/Object;I)V
    //   4361: goto -> 5540
    //   4364: aload #6
    //   4366: astore #10
    //   4368: aload #6
    //   4370: astore #7
    //   4372: aload #11
    //   4374: astore #13
    //   4376: aload_3
    //   4377: iload #15
    //   4379: invokestatic offset : (I)J
    //   4382: aload #4
    //   4384: invokeinterface readSInt32 : ()I
    //   4389: invokestatic putInt : (Ljava/lang/Object;JI)V
    //   4392: aload #6
    //   4394: astore #10
    //   4396: aload #6
    //   4398: astore #7
    //   4400: aload #11
    //   4402: astore #13
    //   4404: aload_0
    //   4405: aload_3
    //   4406: iload #9
    //   4408: invokespecial setFieldPresent : (Ljava/lang/Object;I)V
    //   4411: goto -> 5540
    //   4414: aload #6
    //   4416: astore #10
    //   4418: aload #6
    //   4420: astore #7
    //   4422: aload #11
    //   4424: astore #13
    //   4426: aload_3
    //   4427: iload #15
    //   4429: invokestatic offset : (I)J
    //   4432: aload #4
    //   4434: invokeinterface readSFixed64 : ()J
    //   4439: invokestatic putLong : (Ljava/lang/Object;JJ)V
    //   4442: aload #6
    //   4444: astore #10
    //   4446: aload #6
    //   4448: astore #7
    //   4450: aload #11
    //   4452: astore #13
    //   4454: aload_0
    //   4455: aload_3
    //   4456: iload #9
    //   4458: invokespecial setFieldPresent : (Ljava/lang/Object;I)V
    //   4461: goto -> 5540
    //   4464: aload #6
    //   4466: astore #10
    //   4468: aload #6
    //   4470: astore #7
    //   4472: aload #11
    //   4474: astore #13
    //   4476: aload_3
    //   4477: iload #15
    //   4479: invokestatic offset : (I)J
    //   4482: aload #4
    //   4484: invokeinterface readSFixed32 : ()I
    //   4489: invokestatic putInt : (Ljava/lang/Object;JI)V
    //   4492: aload #6
    //   4494: astore #10
    //   4496: aload #6
    //   4498: astore #7
    //   4500: aload #11
    //   4502: astore #13
    //   4504: aload_0
    //   4505: aload_3
    //   4506: iload #9
    //   4508: invokespecial setFieldPresent : (Ljava/lang/Object;I)V
    //   4511: goto -> 5540
    //   4514: aload #6
    //   4516: astore #10
    //   4518: aload #6
    //   4520: astore #7
    //   4522: aload #11
    //   4524: astore #13
    //   4526: aload #4
    //   4528: invokeinterface readEnum : ()I
    //   4533: istore #16
    //   4535: aload #6
    //   4537: astore #10
    //   4539: aload #6
    //   4541: astore #7
    //   4543: aload #11
    //   4545: astore #13
    //   4547: aload_0
    //   4548: iload #9
    //   4550: invokespecial getEnumFieldVerifier : (I)Lcom/android/framework/protobuf/Internal$EnumVerifier;
    //   4553: astore #12
    //   4555: aload #12
    //   4557: ifnull -> 4614
    //   4560: aload #6
    //   4562: astore #10
    //   4564: aload #6
    //   4566: astore #7
    //   4568: aload #11
    //   4570: astore #13
    //   4572: aload #12
    //   4574: iload #16
    //   4576: invokeinterface isInRange : (I)Z
    //   4581: ifeq -> 4587
    //   4584: goto -> 4614
    //   4587: aload #6
    //   4589: astore #10
    //   4591: aload #6
    //   4593: astore #7
    //   4595: aload #11
    //   4597: astore #13
    //   4599: iload #8
    //   4601: iload #16
    //   4603: aload #6
    //   4605: aload_1
    //   4606: invokestatic storeUnknownEnum : (IILjava/lang/Object;Lcom/android/framework/protobuf/UnknownFieldSchema;)Ljava/lang/Object;
    //   4609: astore #6
    //   4611: goto -> 5540
    //   4614: aload #6
    //   4616: astore #10
    //   4618: aload #6
    //   4620: astore #7
    //   4622: aload #11
    //   4624: astore #13
    //   4626: aload_3
    //   4627: iload #15
    //   4629: invokestatic offset : (I)J
    //   4632: iload #16
    //   4634: invokestatic putInt : (Ljava/lang/Object;JI)V
    //   4637: aload #6
    //   4639: astore #10
    //   4641: aload #6
    //   4643: astore #7
    //   4645: aload #11
    //   4647: astore #13
    //   4649: aload_0
    //   4650: aload_3
    //   4651: iload #9
    //   4653: invokespecial setFieldPresent : (Ljava/lang/Object;I)V
    //   4656: goto -> 5540
    //   4659: aload #6
    //   4661: astore #10
    //   4663: aload #6
    //   4665: astore #7
    //   4667: aload #11
    //   4669: astore #13
    //   4671: aload_3
    //   4672: iload #15
    //   4674: invokestatic offset : (I)J
    //   4677: aload #4
    //   4679: invokeinterface readUInt32 : ()I
    //   4684: invokestatic putInt : (Ljava/lang/Object;JI)V
    //   4687: aload #6
    //   4689: astore #10
    //   4691: aload #6
    //   4693: astore #7
    //   4695: aload #11
    //   4697: astore #13
    //   4699: aload_0
    //   4700: aload_3
    //   4701: iload #9
    //   4703: invokespecial setFieldPresent : (Ljava/lang/Object;I)V
    //   4706: goto -> 5540
    //   4709: aload #6
    //   4711: astore #10
    //   4713: aload #6
    //   4715: astore #7
    //   4717: aload #11
    //   4719: astore #13
    //   4721: aload_3
    //   4722: iload #15
    //   4724: invokestatic offset : (I)J
    //   4727: aload #4
    //   4729: invokeinterface readBytes : ()Lcom/android/framework/protobuf/ByteString;
    //   4734: invokestatic putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   4737: aload #6
    //   4739: astore #10
    //   4741: aload #6
    //   4743: astore #7
    //   4745: aload #11
    //   4747: astore #13
    //   4749: aload_0
    //   4750: aload_3
    //   4751: iload #9
    //   4753: invokespecial setFieldPresent : (Ljava/lang/Object;I)V
    //   4756: goto -> 5540
    //   4759: aload #6
    //   4761: astore #10
    //   4763: aload #6
    //   4765: astore #7
    //   4767: aload #11
    //   4769: astore #13
    //   4771: aload_0
    //   4772: aload_3
    //   4773: iload #9
    //   4775: invokespecial isFieldPresent : (Ljava/lang/Object;I)Z
    //   4778: ifeq -> 4896
    //   4781: aload #6
    //   4783: astore #10
    //   4785: aload #6
    //   4787: astore #7
    //   4789: aload #11
    //   4791: astore #13
    //   4793: aload_3
    //   4794: iload #15
    //   4796: invokestatic offset : (I)J
    //   4799: invokestatic getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   4802: astore #12
    //   4804: aload #6
    //   4806: astore #10
    //   4808: aload #6
    //   4810: astore #7
    //   4812: aload #11
    //   4814: astore #13
    //   4816: aload_0
    //   4817: iload #9
    //   4819: invokespecial getMessageFieldSchema : (I)Lcom/android/framework/protobuf/Schema;
    //   4822: astore #24
    //   4824: aload #6
    //   4826: astore #10
    //   4828: aload #6
    //   4830: astore #7
    //   4832: aload #11
    //   4834: astore #13
    //   4836: aload #4
    //   4838: aload #24
    //   4840: aload #5
    //   4842: invokeinterface readMessageBySchemaWithCheck : (Lcom/android/framework/protobuf/Schema;Lcom/android/framework/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;
    //   4847: astore #24
    //   4849: aload #6
    //   4851: astore #10
    //   4853: aload #6
    //   4855: astore #7
    //   4857: aload #11
    //   4859: astore #13
    //   4861: aload #12
    //   4863: aload #24
    //   4865: invokestatic mergeMessage : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   4868: astore #12
    //   4870: aload #6
    //   4872: astore #10
    //   4874: aload #6
    //   4876: astore #7
    //   4878: aload #11
    //   4880: astore #13
    //   4882: aload_3
    //   4883: iload #15
    //   4885: invokestatic offset : (I)J
    //   4888: aload #12
    //   4890: invokestatic putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   4893: goto -> 5540
    //   4896: aload #6
    //   4898: astore #10
    //   4900: aload #6
    //   4902: astore #7
    //   4904: aload #11
    //   4906: astore #13
    //   4908: iload #15
    //   4910: invokestatic offset : (I)J
    //   4913: lstore #17
    //   4915: aload #6
    //   4917: astore #10
    //   4919: aload #6
    //   4921: astore #7
    //   4923: aload #11
    //   4925: astore #13
    //   4927: aload_0
    //   4928: iload #9
    //   4930: invokespecial getMessageFieldSchema : (I)Lcom/android/framework/protobuf/Schema;
    //   4933: astore #12
    //   4935: aload #6
    //   4937: astore #10
    //   4939: aload #6
    //   4941: astore #7
    //   4943: aload #11
    //   4945: astore #13
    //   4947: aload #4
    //   4949: aload #12
    //   4951: aload #5
    //   4953: invokeinterface readMessageBySchemaWithCheck : (Lcom/android/framework/protobuf/Schema;Lcom/android/framework/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;
    //   4958: astore #12
    //   4960: aload #6
    //   4962: astore #10
    //   4964: aload #6
    //   4966: astore #7
    //   4968: aload #11
    //   4970: astore #13
    //   4972: aload_3
    //   4973: lload #17
    //   4975: aload #12
    //   4977: invokestatic putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   4980: aload #6
    //   4982: astore #10
    //   4984: aload #6
    //   4986: astore #7
    //   4988: aload #11
    //   4990: astore #13
    //   4992: aload_0
    //   4993: aload_3
    //   4994: iload #9
    //   4996: invokespecial setFieldPresent : (Ljava/lang/Object;I)V
    //   4999: goto -> 5540
    //   5002: aload #6
    //   5004: astore #10
    //   5006: aload #6
    //   5008: astore #7
    //   5010: aload #11
    //   5012: astore #13
    //   5014: aload_0
    //   5015: aload_3
    //   5016: iload #15
    //   5018: aload #4
    //   5020: invokespecial readString : (Ljava/lang/Object;ILcom/android/framework/protobuf/Reader;)V
    //   5023: aload #6
    //   5025: astore #10
    //   5027: aload #6
    //   5029: astore #7
    //   5031: aload #11
    //   5033: astore #13
    //   5035: aload_0
    //   5036: aload_3
    //   5037: iload #9
    //   5039: invokespecial setFieldPresent : (Ljava/lang/Object;I)V
    //   5042: goto -> 5540
    //   5045: aload #6
    //   5047: astore #10
    //   5049: aload #6
    //   5051: astore #7
    //   5053: aload #11
    //   5055: astore #13
    //   5057: aload_3
    //   5058: iload #15
    //   5060: invokestatic offset : (I)J
    //   5063: aload #4
    //   5065: invokeinterface readBool : ()Z
    //   5070: invokestatic putBoolean : (Ljava/lang/Object;JZ)V
    //   5073: aload #6
    //   5075: astore #10
    //   5077: aload #6
    //   5079: astore #7
    //   5081: aload #11
    //   5083: astore #13
    //   5085: aload_0
    //   5086: aload_3
    //   5087: iload #9
    //   5089: invokespecial setFieldPresent : (Ljava/lang/Object;I)V
    //   5092: goto -> 5540
    //   5095: aload #6
    //   5097: astore #10
    //   5099: aload #6
    //   5101: astore #7
    //   5103: aload #11
    //   5105: astore #13
    //   5107: aload_3
    //   5108: iload #15
    //   5110: invokestatic offset : (I)J
    //   5113: aload #4
    //   5115: invokeinterface readFixed32 : ()I
    //   5120: invokestatic putInt : (Ljava/lang/Object;JI)V
    //   5123: aload #6
    //   5125: astore #10
    //   5127: aload #6
    //   5129: astore #7
    //   5131: aload #11
    //   5133: astore #13
    //   5135: aload_0
    //   5136: aload_3
    //   5137: iload #9
    //   5139: invokespecial setFieldPresent : (Ljava/lang/Object;I)V
    //   5142: goto -> 5540
    //   5145: aload #6
    //   5147: astore #10
    //   5149: aload #6
    //   5151: astore #7
    //   5153: aload #11
    //   5155: astore #13
    //   5157: aload_3
    //   5158: iload #15
    //   5160: invokestatic offset : (I)J
    //   5163: aload #4
    //   5165: invokeinterface readFixed64 : ()J
    //   5170: invokestatic putLong : (Ljava/lang/Object;JJ)V
    //   5173: aload #6
    //   5175: astore #10
    //   5177: aload #6
    //   5179: astore #7
    //   5181: aload #11
    //   5183: astore #13
    //   5185: aload_0
    //   5186: aload_3
    //   5187: iload #9
    //   5189: invokespecial setFieldPresent : (Ljava/lang/Object;I)V
    //   5192: goto -> 5540
    //   5195: aload #6
    //   5197: astore #10
    //   5199: aload #6
    //   5201: astore #7
    //   5203: aload #11
    //   5205: astore #13
    //   5207: aload_3
    //   5208: iload #15
    //   5210: invokestatic offset : (I)J
    //   5213: aload #4
    //   5215: invokeinterface readInt32 : ()I
    //   5220: invokestatic putInt : (Ljava/lang/Object;JI)V
    //   5223: aload #6
    //   5225: astore #10
    //   5227: aload #6
    //   5229: astore #7
    //   5231: aload #11
    //   5233: astore #13
    //   5235: aload_0
    //   5236: aload_3
    //   5237: iload #9
    //   5239: invokespecial setFieldPresent : (Ljava/lang/Object;I)V
    //   5242: goto -> 5540
    //   5245: aload #6
    //   5247: astore #10
    //   5249: aload #6
    //   5251: astore #7
    //   5253: aload #11
    //   5255: astore #13
    //   5257: aload_3
    //   5258: iload #15
    //   5260: invokestatic offset : (I)J
    //   5263: aload #4
    //   5265: invokeinterface readUInt64 : ()J
    //   5270: invokestatic putLong : (Ljava/lang/Object;JJ)V
    //   5273: aload #6
    //   5275: astore #10
    //   5277: aload #6
    //   5279: astore #7
    //   5281: aload #11
    //   5283: astore #13
    //   5285: aload_0
    //   5286: aload_3
    //   5287: iload #9
    //   5289: invokespecial setFieldPresent : (Ljava/lang/Object;I)V
    //   5292: goto -> 5540
    //   5295: aload #6
    //   5297: astore #10
    //   5299: aload #6
    //   5301: astore #7
    //   5303: aload #11
    //   5305: astore #13
    //   5307: aload_3
    //   5308: iload #15
    //   5310: invokestatic offset : (I)J
    //   5313: aload #4
    //   5315: invokeinterface readInt64 : ()J
    //   5320: invokestatic putLong : (Ljava/lang/Object;JJ)V
    //   5323: aload #6
    //   5325: astore #10
    //   5327: aload #6
    //   5329: astore #7
    //   5331: aload #11
    //   5333: astore #13
    //   5335: aload_0
    //   5336: aload_3
    //   5337: iload #9
    //   5339: invokespecial setFieldPresent : (Ljava/lang/Object;I)V
    //   5342: goto -> 5540
    //   5345: aload #6
    //   5347: astore #10
    //   5349: aload #6
    //   5351: astore #7
    //   5353: aload #11
    //   5355: astore #13
    //   5357: aload_3
    //   5358: iload #15
    //   5360: invokestatic offset : (I)J
    //   5363: aload #4
    //   5365: invokeinterface readFloat : ()F
    //   5370: invokestatic putFloat : (Ljava/lang/Object;JF)V
    //   5373: aload #6
    //   5375: astore #10
    //   5377: aload #6
    //   5379: astore #7
    //   5381: aload #11
    //   5383: astore #13
    //   5385: aload_0
    //   5386: aload_3
    //   5387: iload #9
    //   5389: invokespecial setFieldPresent : (Ljava/lang/Object;I)V
    //   5392: goto -> 5540
    //   5395: aload #6
    //   5397: astore #10
    //   5399: aload #6
    //   5401: astore #7
    //   5403: aload #11
    //   5405: astore #13
    //   5407: aload_3
    //   5408: iload #15
    //   5410: invokestatic offset : (I)J
    //   5413: aload #4
    //   5415: invokeinterface readDouble : ()D
    //   5420: invokestatic putDouble : (Ljava/lang/Object;JD)V
    //   5423: aload #6
    //   5425: astore #10
    //   5427: aload #6
    //   5429: astore #7
    //   5431: aload #11
    //   5433: astore #13
    //   5435: aload_0
    //   5436: aload_3
    //   5437: iload #9
    //   5439: invokespecial setFieldPresent : (Ljava/lang/Object;I)V
    //   5442: goto -> 5540
    //   5445: astore #6
    //   5447: aload #10
    //   5449: astore #6
    //   5451: goto -> 5549
    //   5454: aload #12
    //   5456: astore #10
    //   5458: aload #12
    //   5460: astore #7
    //   5462: aload #11
    //   5464: astore #13
    //   5466: aload_1
    //   5467: aload #12
    //   5469: aload #4
    //   5471: invokevirtual mergeOneFieldFrom : (Ljava/lang/Object;Lcom/android/framework/protobuf/Reader;)Z
    //   5474: istore #14
    //   5476: aload #12
    //   5478: astore #6
    //   5480: iload #14
    //   5482: ifne -> 5540
    //   5485: aload_0
    //   5486: getfield checkInitializedCount : I
    //   5489: istore #9
    //   5491: iload #9
    //   5493: aload_0
    //   5494: getfield repeatedFieldOffsetStart : I
    //   5497: if_icmpge -> 5527
    //   5500: aload_0
    //   5501: getfield intArray : [I
    //   5504: iload #9
    //   5506: iaload
    //   5507: istore #8
    //   5509: aload_0
    //   5510: aload_3
    //   5511: iload #8
    //   5513: aload #12
    //   5515: aload_1
    //   5516: invokespecial filterMapUnknownEnumValues : (Ljava/lang/Object;ILjava/lang/Object;Lcom/android/framework/protobuf/UnknownFieldSchema;)Ljava/lang/Object;
    //   5519: astore #12
    //   5521: iinc #9, 1
    //   5524: goto -> 5491
    //   5527: aload #12
    //   5529: ifnull -> 5539
    //   5532: aload_1
    //   5533: aload_3
    //   5534: aload #12
    //   5536: invokevirtual setBuilderToMessage : (Ljava/lang/Object;Ljava/lang/Object;)V
    //   5539: return
    //   5540: aload #6
    //   5542: astore #7
    //   5544: goto -> 5753
    //   5547: astore #7
    //   5549: aload #6
    //   5551: astore #7
    //   5553: aload #11
    //   5555: astore #13
    //   5557: aload_1
    //   5558: aload #4
    //   5560: invokevirtual shouldDiscardUnknownFields : (Lcom/android/framework/protobuf/Reader;)Z
    //   5563: ifeq -> 5647
    //   5566: aload #6
    //   5568: astore #7
    //   5570: aload #11
    //   5572: astore #13
    //   5574: aload #4
    //   5576: invokeinterface skipField : ()Z
    //   5581: istore #14
    //   5583: aload #6
    //   5585: astore #7
    //   5587: iload #14
    //   5589: ifne -> 5753
    //   5592: aload_0
    //   5593: getfield checkInitializedCount : I
    //   5596: istore #9
    //   5598: iload #9
    //   5600: aload_0
    //   5601: getfield repeatedFieldOffsetStart : I
    //   5604: if_icmpge -> 5634
    //   5607: aload_0
    //   5608: getfield intArray : [I
    //   5611: iload #9
    //   5613: iaload
    //   5614: istore #8
    //   5616: aload_0
    //   5617: aload_3
    //   5618: iload #8
    //   5620: aload #6
    //   5622: aload_1
    //   5623: invokespecial filterMapUnknownEnumValues : (Ljava/lang/Object;ILjava/lang/Object;Lcom/android/framework/protobuf/UnknownFieldSchema;)Ljava/lang/Object;
    //   5626: astore #6
    //   5628: iinc #9, 1
    //   5631: goto -> 5598
    //   5634: aload #6
    //   5636: ifnull -> 5646
    //   5639: aload_1
    //   5640: aload_3
    //   5641: aload #6
    //   5643: invokevirtual setBuilderToMessage : (Ljava/lang/Object;Ljava/lang/Object;)V
    //   5646: return
    //   5647: aload #6
    //   5649: astore #12
    //   5651: aload #6
    //   5653: ifnonnull -> 5671
    //   5656: aload #6
    //   5658: astore #7
    //   5660: aload #11
    //   5662: astore #13
    //   5664: aload_1
    //   5665: aload_3
    //   5666: invokevirtual getBuilderFromMessage : (Ljava/lang/Object;)Ljava/lang/Object;
    //   5669: astore #12
    //   5671: aload #12
    //   5673: astore #7
    //   5675: aload #11
    //   5677: astore #13
    //   5679: aload_1
    //   5680: aload #12
    //   5682: aload #4
    //   5684: invokevirtual mergeOneFieldFrom : (Ljava/lang/Object;Lcom/android/framework/protobuf/Reader;)Z
    //   5687: istore #14
    //   5689: aload #12
    //   5691: astore #7
    //   5693: iload #14
    //   5695: ifne -> 5753
    //   5698: aload_0
    //   5699: getfield checkInitializedCount : I
    //   5702: istore #9
    //   5704: iload #9
    //   5706: aload_0
    //   5707: getfield repeatedFieldOffsetStart : I
    //   5710: if_icmpge -> 5740
    //   5713: aload_0
    //   5714: getfield intArray : [I
    //   5717: iload #9
    //   5719: iaload
    //   5720: istore #8
    //   5722: aload_0
    //   5723: aload_3
    //   5724: iload #8
    //   5726: aload #12
    //   5728: aload_1
    //   5729: invokespecial filterMapUnknownEnumValues : (Ljava/lang/Object;ILjava/lang/Object;Lcom/android/framework/protobuf/UnknownFieldSchema;)Ljava/lang/Object;
    //   5732: astore #12
    //   5734: iinc #9, 1
    //   5737: goto -> 5704
    //   5740: aload #12
    //   5742: ifnull -> 5752
    //   5745: aload_1
    //   5746: aload_3
    //   5747: aload #12
    //   5749: invokevirtual setBuilderToMessage : (Ljava/lang/Object;Ljava/lang/Object;)V
    //   5752: return
    //   5753: aload #7
    //   5755: astore #6
    //   5757: aload #11
    //   5759: astore #7
    //   5761: goto -> 6
    //   5764: astore_2
    //   5765: aload #7
    //   5767: astore #6
    //   5769: goto -> 5773
    //   5772: astore_2
    //   5773: aload_0
    //   5774: getfield checkInitializedCount : I
    //   5777: istore #9
    //   5779: iload #9
    //   5781: aload_0
    //   5782: getfield repeatedFieldOffsetStart : I
    //   5785: if_icmpge -> 5815
    //   5788: aload_0
    //   5789: getfield intArray : [I
    //   5792: iload #9
    //   5794: iaload
    //   5795: istore #8
    //   5797: aload_0
    //   5798: aload_3
    //   5799: iload #8
    //   5801: aload #6
    //   5803: aload_1
    //   5804: invokespecial filterMapUnknownEnumValues : (Ljava/lang/Object;ILjava/lang/Object;Lcom/android/framework/protobuf/UnknownFieldSchema;)Ljava/lang/Object;
    //   5807: astore #6
    //   5809: iinc #9, 1
    //   5812: goto -> 5779
    //   5815: aload #6
    //   5817: ifnull -> 5827
    //   5820: aload_1
    //   5821: aload_3
    //   5822: aload #6
    //   5824: invokevirtual setBuilderToMessage : (Ljava/lang/Object;Ljava/lang/Object;)V
    //   5827: aload_2
    //   5828: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #3860	-> 0
    //   #3861	-> 0
    //   #3864	-> 6
    //   #3865	-> 15
    //   #3866	-> 23
    //   #3867	-> 28
    //   #4324	-> 36
    //   #4325	-> 51
    //   #4326	-> 60
    //   #4324	-> 72
    //   #4328	-> 78
    //   #4329	-> 83
    //   #3868	-> 90
    //   #3872	-> 91
    //   #3873	-> 98
    //   #3874	-> 104
    //   #3876	-> 118
    //   #3877	-> 123
    //   #3878	-> 132
    //   #4324	-> 142
    //   #3880	-> 146
    //   #3881	-> 146
    //   #3888	-> 175
    //   #3890	-> 182
    //   #3891	-> 203
    //   #3892	-> 225
    //   #3895	-> 232
    //   #3896	-> 241
    //   #3899	-> 256
    //   #3900	-> 283
    //   #4324	-> 294
    //   #4325	-> 309
    //   #4326	-> 318
    //   #4324	-> 330
    //   #4328	-> 336
    //   #4329	-> 341
    //   #3904	-> 348
    //   #3906	-> 349
    //   #3909	-> 369
    //   #4298	-> 676
    //   #4299	-> 685
    //   #4290	-> 706
    //   #4292	-> 706
    //   #4293	-> 721
    //   #4290	-> 746
    //   #4294	-> 762
    //   #4295	-> 779
    //   #4285	-> 782
    //   #4286	-> 782
    //   #4285	-> 814
    //   #4287	-> 833
    //   #4288	-> 850
    //   #4280	-> 853
    //   #4281	-> 853
    //   #4280	-> 885
    //   #4282	-> 904
    //   #4283	-> 921
    //   #4275	-> 924
    //   #4276	-> 924
    //   #4275	-> 956
    //   #4277	-> 975
    //   #4278	-> 992
    //   #4270	-> 995
    //   #4271	-> 995
    //   #4270	-> 1027
    //   #4272	-> 1046
    //   #4273	-> 1063
    //   #4257	-> 1066
    //   #4258	-> 1083
    //   #4259	-> 1099
    //   #4263	-> 1127
    //   #4264	-> 1127
    //   #4267	-> 1147
    //   #4260	-> 1154
    //   #4261	-> 1176
    //   #4251	-> 1196
    //   #4252	-> 1196
    //   #4251	-> 1228
    //   #4253	-> 1247
    //   #4254	-> 1264
    //   #4247	-> 1267
    //   #4248	-> 1291
    //   #4249	-> 1308
    //   #4229	-> 1311
    //   #4230	-> 1331
    //   #4232	-> 1331
    //   #4234	-> 1350
    //   #4233	-> 1366
    //   #4231	-> 1387
    //   #4235	-> 1404
    //   #4236	-> 1423
    //   #4237	-> 1426
    //   #4239	-> 1426
    //   #4241	-> 1441
    //   #4240	-> 1457
    //   #4237	-> 1478
    //   #4242	-> 1494
    //   #4244	-> 1509
    //   #4245	-> 1526
    //   #4225	-> 1529
    //   #4226	-> 1546
    //   #4227	-> 1563
    //   #4220	-> 1566
    //   #4221	-> 1566
    //   #4220	-> 1598
    //   #4222	-> 1617
    //   #4223	-> 1634
    //   #4215	-> 1637
    //   #4216	-> 1637
    //   #4215	-> 1669
    //   #4217	-> 1688
    //   #4218	-> 1705
    //   #4210	-> 1708
    //   #4211	-> 1708
    //   #4210	-> 1740
    //   #4212	-> 1759
    //   #4213	-> 1776
    //   #4205	-> 1779
    //   #4206	-> 1779
    //   #4205	-> 1811
    //   #4207	-> 1830
    //   #4208	-> 1847
    //   #4200	-> 1850
    //   #4201	-> 1850
    //   #4200	-> 1882
    //   #4202	-> 1901
    //   #4203	-> 1918
    //   #4195	-> 1921
    //   #4196	-> 1921
    //   #4195	-> 1953
    //   #4197	-> 1972
    //   #4198	-> 1989
    //   #4190	-> 1992
    //   #4191	-> 1992
    //   #4190	-> 2024
    //   #4192	-> 2043
    //   #4193	-> 2060
    //   #4185	-> 2063
    //   #4186	-> 2063
    //   #4185	-> 2095
    //   #4187	-> 2114
    //   #4188	-> 2131
    //   #4182	-> 2134
    //   #4183	-> 2159
    //   #4306	-> 2162
    //   #4173	-> 2167
    //   #4175	-> 2167
    //   #4177	-> 2182
    //   #4173	-> 2198
    //   #4179	-> 2219
    //   #4306	-> 2222
    //   #4168	-> 2227
    //   #4169	-> 2241
    //   #4168	-> 2262
    //   #4170	-> 2279
    //   #4164	-> 2282
    //   #4165	-> 2296
    //   #4164	-> 2317
    //   #4166	-> 2334
    //   #4160	-> 2337
    //   #4161	-> 2351
    //   #4160	-> 2372
    //   #4162	-> 2389
    //   #4156	-> 2392
    //   #4157	-> 2406
    //   #4156	-> 2427
    //   #4158	-> 2444
    //   #4143	-> 2447
    //   #4144	-> 2461
    //   #4145	-> 2482
    //   #4146	-> 2499
    //   #4150	-> 2499
    //   #4147	-> 2515
    //   #4153	-> 2537
    //   #4138	-> 2544
    //   #4139	-> 2558
    //   #4138	-> 2579
    //   #4140	-> 2596
    //   #4134	-> 2599
    //   #4135	-> 2613
    //   #4134	-> 2634
    //   #4136	-> 2651
    //   #4130	-> 2654
    //   #4131	-> 2668
    //   #4130	-> 2689
    //   #4132	-> 2706
    //   #4126	-> 2709
    //   #4127	-> 2723
    //   #4126	-> 2744
    //   #4128	-> 2761
    //   #4122	-> 2764
    //   #4123	-> 2778
    //   #4122	-> 2799
    //   #4124	-> 2816
    //   #4118	-> 2819
    //   #4119	-> 2833
    //   #4118	-> 2854
    //   #4120	-> 2871
    //   #4114	-> 2874
    //   #4115	-> 2888
    //   #4114	-> 2909
    //   #4116	-> 2926
    //   #4110	-> 2929
    //   #4111	-> 2943
    //   #4110	-> 2964
    //   #4112	-> 2981
    //   #4106	-> 2984
    //   #4107	-> 2998
    //   #4106	-> 3019
    //   #4108	-> 3036
    //   #4102	-> 3039
    //   #4103	-> 3053
    //   #4102	-> 3074
    //   #4104	-> 3091
    //   #4098	-> 3094
    //   #4099	-> 3108
    //   #4098	-> 3129
    //   #4100	-> 3146
    //   #4094	-> 3149
    //   #4095	-> 3163
    //   #4094	-> 3184
    //   #4096	-> 3201
    //   #4090	-> 3204
    //   #4091	-> 3218
    //   #4090	-> 3239
    //   #4092	-> 3256
    //   #4077	-> 3259
    //   #4078	-> 3273
    //   #4079	-> 3294
    //   #4080	-> 3311
    //   #4084	-> 3311
    //   #4081	-> 3327
    //   #4087	-> 3349
    //   #4072	-> 3356
    //   #4073	-> 3370
    //   #4072	-> 3391
    //   #4074	-> 3408
    //   #4068	-> 3411
    //   #4069	-> 3425
    //   #4068	-> 3446
    //   #4070	-> 3463
    //   #4059	-> 3466
    //   #4063	-> 3466
    //   #4059	-> 3482
    //   #4065	-> 3503
    //   #4306	-> 3506
    //   #4055	-> 3511
    //   #4056	-> 3532
    //   #4051	-> 3535
    //   #4052	-> 3553
    //   #4051	-> 3578
    //   #4053	-> 3599
    //   #4047	-> 3602
    //   #4048	-> 3620
    //   #4047	-> 3645
    //   #4049	-> 3666
    //   #4043	-> 3669
    //   #4044	-> 3687
    //   #4043	-> 3712
    //   #4045	-> 3733
    //   #4039	-> 3736
    //   #4040	-> 3754
    //   #4039	-> 3779
    //   #4041	-> 3800
    //   #4035	-> 3803
    //   #4036	-> 3821
    //   #4035	-> 3846
    //   #4037	-> 3867
    //   #4031	-> 3870
    //   #4032	-> 3888
    //   #4031	-> 3913
    //   #4033	-> 3934
    //   #4027	-> 3937
    //   #4028	-> 3955
    //   #4027	-> 3980
    //   #4029	-> 4001
    //   #4023	-> 4004
    //   #4024	-> 4022
    //   #4023	-> 4047
    //   #4025	-> 4068
    //   #4005	-> 4071
    //   #4006	-> 4093
    //   #4008	-> 4093
    //   #4010	-> 4116
    //   #4009	-> 4136
    //   #4007	-> 4161
    //   #4011	-> 4182
    //   #4012	-> 4205
    //   #4013	-> 4208
    //   #4015	-> 4208
    //   #4017	-> 4227
    //   #4016	-> 4247
    //   #4013	-> 4272
    //   #4018	-> 4292
    //   #4020	-> 4311
    //   #4000	-> 4314
    //   #4001	-> 4342
    //   #4002	-> 4361
    //   #3996	-> 4364
    //   #3997	-> 4392
    //   #3998	-> 4411
    //   #3992	-> 4414
    //   #3993	-> 4442
    //   #3994	-> 4461
    //   #3988	-> 4464
    //   #3989	-> 4492
    //   #3990	-> 4511
    //   #3975	-> 4514
    //   #3976	-> 4535
    //   #3977	-> 4555
    //   #3981	-> 4587
    //   #3982	-> 4587
    //   #3985	-> 4611
    //   #3978	-> 4614
    //   #3979	-> 4637
    //   #3970	-> 4659
    //   #3971	-> 4687
    //   #3972	-> 4706
    //   #3966	-> 4709
    //   #3967	-> 4737
    //   #3968	-> 4756
    //   #3948	-> 4759
    //   #3949	-> 4781
    //   #3951	-> 4781
    //   #3953	-> 4804
    //   #3952	-> 4824
    //   #3950	-> 4849
    //   #3954	-> 4870
    //   #3955	-> 4893
    //   #3956	-> 4896
    //   #3958	-> 4896
    //   #3960	-> 4915
    //   #3959	-> 4935
    //   #3956	-> 4960
    //   #3961	-> 4980
    //   #3963	-> 4999
    //   #3943	-> 5002
    //   #3944	-> 5023
    //   #3945	-> 5042
    //   #3939	-> 5045
    //   #3940	-> 5073
    //   #3941	-> 5092
    //   #3935	-> 5095
    //   #3936	-> 5123
    //   #3937	-> 5142
    //   #3931	-> 5145
    //   #3932	-> 5173
    //   #3933	-> 5192
    //   #3927	-> 5195
    //   #3928	-> 5223
    //   #3929	-> 5242
    //   #3923	-> 5245
    //   #3924	-> 5273
    //   #3925	-> 5292
    //   #3919	-> 5295
    //   #3920	-> 5323
    //   #3921	-> 5342
    //   #3915	-> 5345
    //   #3916	-> 5373
    //   #3917	-> 5392
    //   #3911	-> 5395
    //   #3912	-> 5423
    //   #3913	-> 5442
    //   #4306	-> 5445
    //   #4299	-> 5454
    //   #4301	-> 5454
    //   #4324	-> 5485
    //   #4325	-> 5500
    //   #4326	-> 5509
    //   #4324	-> 5521
    //   #4328	-> 5527
    //   #4329	-> 5532
    //   #4302	-> 5539
    //   #4321	-> 5540
    //   #4306	-> 5547
    //   #4309	-> 5549
    //   #4310	-> 5566
    //   #4324	-> 5592
    //   #4325	-> 5607
    //   #4326	-> 5616
    //   #4324	-> 5628
    //   #4328	-> 5634
    //   #4329	-> 5639
    //   #4311	-> 5646
    //   #4314	-> 5647
    //   #4315	-> 5656
    //   #4317	-> 5671
    //   #4324	-> 5698
    //   #4325	-> 5713
    //   #4326	-> 5722
    //   #4324	-> 5734
    //   #4328	-> 5740
    //   #4329	-> 5745
    //   #4318	-> 5752
    //   #4322	-> 5753
    //   #4324	-> 5764
    //   #4325	-> 5788
    //   #4326	-> 5797
    //   #4324	-> 5809
    //   #4328	-> 5815
    //   #4329	-> 5820
    //   #4331	-> 5827
    // Exception table:
    //   from	to	target	type
    //   6	15	5772	finally
    //   15	23	5772	finally
    //   91	98	5772	finally
    //   104	118	5772	finally
    //   132	139	142	finally
    //   158	175	5764	finally
    //   194	203	5764	finally
    //   215	225	5764	finally
    //   249	256	5764	finally
    //   264	274	5764	finally
    //   361	369	5764	finally
    //   377	384	5547	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   377	384	5764	finally
    //   697	703	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   697	703	5764	finally
    //   714	721	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   714	721	5764	finally
    //   729	746	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   729	746	5764	finally
    //   754	762	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   754	762	5764	finally
    //   770	779	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   770	779	5764	finally
    //   790	797	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   790	797	5764	finally
    //   805	814	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   805	814	5764	finally
    //   822	833	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   822	833	5764	finally
    //   841	850	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   841	850	5764	finally
    //   861	868	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   861	868	5764	finally
    //   876	885	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   876	885	5764	finally
    //   893	904	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   893	904	5764	finally
    //   912	921	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   912	921	5764	finally
    //   932	939	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   932	939	5764	finally
    //   947	956	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   947	956	5764	finally
    //   964	975	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   964	975	5764	finally
    //   983	992	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   983	992	5764	finally
    //   1003	1010	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1003	1010	5764	finally
    //   1018	1027	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1018	1027	5764	finally
    //   1035	1046	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1035	1046	5764	finally
    //   1054	1063	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1054	1063	5764	finally
    //   1074	1083	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1074	1083	5764	finally
    //   1091	1099	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1091	1099	5764	finally
    //   1112	1124	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1112	1124	5764	finally
    //   1135	1147	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1135	1147	5764	finally
    //   1162	1176	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1162	1176	5764	finally
    //   1184	1193	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1184	1193	5764	finally
    //   1204	1211	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1204	1211	5764	finally
    //   1219	1228	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1219	1228	5764	finally
    //   1236	1247	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1236	1247	5764	finally
    //   1255	1264	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1255	1264	5764	finally
    //   1275	1291	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1275	1291	5764	finally
    //   1299	1308	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1299	1308	5764	finally
    //   1319	1331	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1319	1331	5764	finally
    //   1339	1350	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1339	1350	5764	finally
    //   1358	1366	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1358	1366	5764	finally
    //   1374	1387	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1374	1387	5764	finally
    //   1395	1404	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1395	1404	5764	finally
    //   1412	1423	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1412	1423	5764	finally
    //   1434	1441	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1434	1441	5764	finally
    //   1449	1457	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1449	1457	5764	finally
    //   1465	1478	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1465	1478	5764	finally
    //   1486	1494	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1486	1494	5764	finally
    //   1502	1509	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1502	1509	5764	finally
    //   1517	1526	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1517	1526	5764	finally
    //   1537	1546	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1537	1546	5764	finally
    //   1554	1563	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1554	1563	5764	finally
    //   1574	1581	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1574	1581	5764	finally
    //   1589	1598	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1589	1598	5764	finally
    //   1606	1617	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1606	1617	5764	finally
    //   1625	1634	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1625	1634	5764	finally
    //   1645	1652	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1645	1652	5764	finally
    //   1660	1669	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1660	1669	5764	finally
    //   1677	1688	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1677	1688	5764	finally
    //   1696	1705	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1696	1705	5764	finally
    //   1716	1723	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1716	1723	5764	finally
    //   1731	1740	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1731	1740	5764	finally
    //   1748	1759	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1748	1759	5764	finally
    //   1767	1776	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1767	1776	5764	finally
    //   1787	1794	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1787	1794	5764	finally
    //   1802	1811	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1802	1811	5764	finally
    //   1819	1830	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1819	1830	5764	finally
    //   1838	1847	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1838	1847	5764	finally
    //   1858	1865	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1858	1865	5764	finally
    //   1873	1882	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1873	1882	5764	finally
    //   1890	1901	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1890	1901	5764	finally
    //   1909	1918	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1909	1918	5764	finally
    //   1929	1936	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1929	1936	5764	finally
    //   1944	1953	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1944	1953	5764	finally
    //   1961	1972	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1961	1972	5764	finally
    //   1980	1989	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   1980	1989	5764	finally
    //   2000	2007	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2000	2007	5764	finally
    //   2015	2024	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2015	2024	5764	finally
    //   2032	2043	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2032	2043	5764	finally
    //   2051	2060	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2051	2060	5764	finally
    //   2071	2078	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2071	2078	5764	finally
    //   2086	2095	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2086	2095	5764	finally
    //   2103	2114	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2103	2114	5764	finally
    //   2122	2131	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2122	2131	5764	finally
    //   2142	2159	2162	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2142	2159	5764	finally
    //   2175	2182	2222	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2175	2182	5764	finally
    //   2190	2198	2222	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2190	2198	5764	finally
    //   2206	2219	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2206	2219	5764	finally
    //   2235	2241	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2235	2241	5764	finally
    //   2249	2262	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2249	2262	5764	finally
    //   2270	2279	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2270	2279	5764	finally
    //   2290	2296	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2290	2296	5764	finally
    //   2304	2317	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2304	2317	5764	finally
    //   2325	2334	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2325	2334	5764	finally
    //   2345	2351	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2345	2351	5764	finally
    //   2359	2372	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2359	2372	5764	finally
    //   2380	2389	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2380	2389	5764	finally
    //   2400	2406	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2400	2406	5764	finally
    //   2414	2427	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2414	2427	5764	finally
    //   2435	2444	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2435	2444	5764	finally
    //   2455	2461	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2455	2461	5764	finally
    //   2469	2482	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2469	2482	5764	finally
    //   2490	2499	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2490	2499	5764	finally
    //   2507	2515	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2507	2515	5764	finally
    //   2523	2537	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2523	2537	5764	finally
    //   2552	2558	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2552	2558	5764	finally
    //   2566	2579	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2566	2579	5764	finally
    //   2587	2596	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2587	2596	5764	finally
    //   2607	2613	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2607	2613	5764	finally
    //   2621	2634	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2621	2634	5764	finally
    //   2642	2651	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2642	2651	5764	finally
    //   2662	2668	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2662	2668	5764	finally
    //   2676	2689	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2676	2689	5764	finally
    //   2697	2706	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2697	2706	5764	finally
    //   2717	2723	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2717	2723	5764	finally
    //   2731	2744	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2731	2744	5764	finally
    //   2752	2761	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2752	2761	5764	finally
    //   2772	2778	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2772	2778	5764	finally
    //   2786	2799	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2786	2799	5764	finally
    //   2807	2816	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2807	2816	5764	finally
    //   2827	2833	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2827	2833	5764	finally
    //   2841	2854	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2841	2854	5764	finally
    //   2862	2871	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2862	2871	5764	finally
    //   2882	2888	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2882	2888	5764	finally
    //   2896	2909	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2896	2909	5764	finally
    //   2917	2926	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2917	2926	5764	finally
    //   2937	2943	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2937	2943	5764	finally
    //   2951	2964	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2951	2964	5764	finally
    //   2972	2981	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2972	2981	5764	finally
    //   2992	2998	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   2992	2998	5764	finally
    //   3006	3019	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3006	3019	5764	finally
    //   3027	3036	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3027	3036	5764	finally
    //   3047	3053	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3047	3053	5764	finally
    //   3061	3074	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3061	3074	5764	finally
    //   3082	3091	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3082	3091	5764	finally
    //   3102	3108	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3102	3108	5764	finally
    //   3116	3129	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3116	3129	5764	finally
    //   3137	3146	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3137	3146	5764	finally
    //   3157	3163	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3157	3163	5764	finally
    //   3171	3184	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3171	3184	5764	finally
    //   3192	3201	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3192	3201	5764	finally
    //   3212	3218	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3212	3218	5764	finally
    //   3226	3239	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3226	3239	5764	finally
    //   3247	3256	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3247	3256	5764	finally
    //   3267	3273	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3267	3273	5764	finally
    //   3281	3294	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3281	3294	5764	finally
    //   3302	3311	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3302	3311	5764	finally
    //   3319	3327	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3319	3327	5764	finally
    //   3335	3349	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3335	3349	5764	finally
    //   3364	3370	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3364	3370	5764	finally
    //   3378	3391	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3378	3391	5764	finally
    //   3399	3408	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3399	3408	5764	finally
    //   3419	3425	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3419	3425	5764	finally
    //   3433	3446	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3433	3446	5764	finally
    //   3454	3463	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3454	3463	5764	finally
    //   3474	3482	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3474	3482	5764	finally
    //   3490	3503	3506	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3490	3503	5764	finally
    //   3523	3532	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3523	3532	5764	finally
    //   3547	3553	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3547	3553	5764	finally
    //   3565	3578	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3565	3578	5764	finally
    //   3590	3599	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3590	3599	5764	finally
    //   3614	3620	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3614	3620	5764	finally
    //   3632	3645	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3632	3645	5764	finally
    //   3657	3666	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3657	3666	5764	finally
    //   3681	3687	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3681	3687	5764	finally
    //   3699	3712	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3699	3712	5764	finally
    //   3724	3733	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3724	3733	5764	finally
    //   3748	3754	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3748	3754	5764	finally
    //   3766	3779	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3766	3779	5764	finally
    //   3791	3800	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3791	3800	5764	finally
    //   3815	3821	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3815	3821	5764	finally
    //   3833	3846	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3833	3846	5764	finally
    //   3858	3867	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3858	3867	5764	finally
    //   3882	3888	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3882	3888	5764	finally
    //   3900	3913	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3900	3913	5764	finally
    //   3925	3934	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3925	3934	5764	finally
    //   3949	3955	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3949	3955	5764	finally
    //   3967	3980	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3967	3980	5764	finally
    //   3992	4001	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   3992	4001	5764	finally
    //   4016	4022	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4016	4022	5764	finally
    //   4034	4047	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4034	4047	5764	finally
    //   4059	4068	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4059	4068	5764	finally
    //   4083	4093	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4083	4093	5764	finally
    //   4105	4116	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4105	4116	5764	finally
    //   4128	4136	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4128	4136	5764	finally
    //   4148	4161	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4148	4161	5764	finally
    //   4173	4182	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4173	4182	5764	finally
    //   4194	4205	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4194	4205	5764	finally
    //   4220	4227	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4220	4227	5764	finally
    //   4239	4247	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4239	4247	5764	finally
    //   4259	4272	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4259	4272	5764	finally
    //   4284	4292	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4284	4292	5764	finally
    //   4304	4311	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4304	4311	5764	finally
    //   4326	4342	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4326	4342	5764	finally
    //   4354	4361	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4354	4361	5764	finally
    //   4376	4392	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4376	4392	5764	finally
    //   4404	4411	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4404	4411	5764	finally
    //   4426	4442	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4426	4442	5764	finally
    //   4454	4461	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4454	4461	5764	finally
    //   4476	4492	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4476	4492	5764	finally
    //   4504	4511	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4504	4511	5764	finally
    //   4526	4535	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4526	4535	5764	finally
    //   4547	4555	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4547	4555	5764	finally
    //   4572	4584	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4572	4584	5764	finally
    //   4599	4611	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4599	4611	5764	finally
    //   4626	4637	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4626	4637	5764	finally
    //   4649	4656	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4649	4656	5764	finally
    //   4671	4687	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4671	4687	5764	finally
    //   4699	4706	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4699	4706	5764	finally
    //   4721	4737	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4721	4737	5764	finally
    //   4749	4756	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4749	4756	5764	finally
    //   4771	4781	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4771	4781	5764	finally
    //   4793	4804	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4793	4804	5764	finally
    //   4816	4824	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4816	4824	5764	finally
    //   4836	4849	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4836	4849	5764	finally
    //   4861	4870	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4861	4870	5764	finally
    //   4882	4893	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4882	4893	5764	finally
    //   4908	4915	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4908	4915	5764	finally
    //   4927	4935	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4927	4935	5764	finally
    //   4947	4960	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4947	4960	5764	finally
    //   4972	4980	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4972	4980	5764	finally
    //   4992	4999	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   4992	4999	5764	finally
    //   5014	5023	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   5014	5023	5764	finally
    //   5035	5042	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   5035	5042	5764	finally
    //   5057	5073	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   5057	5073	5764	finally
    //   5085	5092	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   5085	5092	5764	finally
    //   5107	5123	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   5107	5123	5764	finally
    //   5135	5142	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   5135	5142	5764	finally
    //   5157	5173	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   5157	5173	5764	finally
    //   5185	5192	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   5185	5192	5764	finally
    //   5207	5223	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   5207	5223	5764	finally
    //   5235	5242	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   5235	5242	5764	finally
    //   5257	5273	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   5257	5273	5764	finally
    //   5285	5292	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   5285	5292	5764	finally
    //   5307	5323	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   5307	5323	5764	finally
    //   5335	5342	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   5335	5342	5764	finally
    //   5357	5373	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   5357	5373	5764	finally
    //   5385	5392	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   5385	5392	5764	finally
    //   5407	5423	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   5407	5423	5764	finally
    //   5435	5442	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   5435	5442	5764	finally
    //   5466	5476	5445	com/android/framework/protobuf/InvalidProtocolBufferException$InvalidWireTypeException
    //   5466	5476	5764	finally
    //   5557	5566	5764	finally
    //   5574	5583	5764	finally
    //   5664	5671	5764	finally
    //   5679	5689	5764	finally
  }
  
  static UnknownFieldSetLite getMutableUnknownFields(Object paramObject) {
    UnknownFieldSetLite unknownFieldSetLite1 = ((GeneratedMessageLite)paramObject).unknownFields;
    UnknownFieldSetLite unknownFieldSetLite2 = unknownFieldSetLite1;
    if (unknownFieldSetLite1 == UnknownFieldSetLite.getDefaultInstance()) {
      unknownFieldSetLite2 = UnknownFieldSetLite.newInstance();
      ((GeneratedMessageLite)paramObject).unknownFields = unknownFieldSetLite2;
    } 
    return unknownFieldSetLite2;
  }
  
  private int decodeMapEntryValue(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, WireFormat.FieldType paramFieldType, Class<?> paramClass, ArrayDecoders.Registers paramRegisters) throws IOException {
    Schema<?> schema;
    boolean bool;
    switch (null.$SwitchMap$com$google$protobuf$WireFormat$FieldType[paramFieldType.ordinal()]) {
      default:
        throw new RuntimeException("unsupported field type.");
      case 17:
        paramInt1 = ArrayDecoders.decodeStringRequireUtf8(paramArrayOfbyte, paramInt1, paramRegisters);
        return paramInt1;
      case 16:
        paramInt1 = ArrayDecoders.decodeVarint64(paramArrayOfbyte, paramInt1, paramRegisters);
        paramRegisters.object1 = Long.valueOf(CodedInputStream.decodeZigZag64(paramRegisters.long1));
        return paramInt1;
      case 15:
        paramInt1 = ArrayDecoders.decodeVarint32(paramArrayOfbyte, paramInt1, paramRegisters);
        paramRegisters.object1 = Integer.valueOf(CodedInputStream.decodeZigZag32(paramRegisters.int1));
        return paramInt1;
      case 14:
        schema = Protobuf.getInstance().schemaFor(paramClass);
        paramInt1 = ArrayDecoders.decodeMessageField(schema, paramArrayOfbyte, paramInt1, paramInt2, paramRegisters);
        return paramInt1;
      case 12:
      case 13:
        paramInt1 = ArrayDecoders.decodeVarint64(paramArrayOfbyte, paramInt1, paramRegisters);
        paramRegisters.object1 = Long.valueOf(paramRegisters.long1);
        return paramInt1;
      case 9:
      case 10:
      case 11:
        paramInt1 = ArrayDecoders.decodeVarint32(paramArrayOfbyte, paramInt1, paramRegisters);
        paramRegisters.object1 = Integer.valueOf(paramRegisters.int1);
        return paramInt1;
      case 8:
        paramRegisters.object1 = Float.valueOf(ArrayDecoders.decodeFloat(paramArrayOfbyte, paramInt1));
        paramInt1 += 4;
        return paramInt1;
      case 6:
      case 7:
        paramRegisters.object1 = Long.valueOf(ArrayDecoders.decodeFixed64(paramArrayOfbyte, paramInt1));
        paramInt1 += 8;
        return paramInt1;
      case 4:
      case 5:
        paramRegisters.object1 = Integer.valueOf(ArrayDecoders.decodeFixed32(paramArrayOfbyte, paramInt1));
        paramInt1 += 4;
        return paramInt1;
      case 3:
        paramRegisters.object1 = Double.valueOf(ArrayDecoders.decodeDouble(paramArrayOfbyte, paramInt1));
        paramInt1 += 8;
        return paramInt1;
      case 2:
        paramInt1 = ArrayDecoders.decodeBytes(paramArrayOfbyte, paramInt1, paramRegisters);
        return paramInt1;
      case 1:
        break;
    } 
    paramInt1 = ArrayDecoders.decodeVarint64(paramArrayOfbyte, paramInt1, paramRegisters);
    if (paramRegisters.long1 != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    paramRegisters.object1 = Boolean.valueOf(bool);
    return paramInt1;
  }
  
  private <K, V> int decodeMapEntry(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, MapEntryLite.Metadata<K, V> paramMetadata, Map<K, V> paramMap, ArrayDecoders.Registers paramRegisters) throws IOException {
    int i = ArrayDecoders.decodeVarint32(paramArrayOfbyte, paramInt1, paramRegisters);
    paramInt1 = paramRegisters.int1;
    if (paramInt1 >= 0 && paramInt1 <= paramInt2 - i) {
      int j = i + paramInt1;
      Object object1 = paramMetadata.defaultKey;
      Object object2 = paramMetadata.defaultValue;
      while (i < j) {
        int k = i + 1;
        i = paramArrayOfbyte[i];
        if (i < 0) {
          k = ArrayDecoders.decodeVarint32(i, paramArrayOfbyte, k, paramRegisters);
          i = paramRegisters.int1;
        } 
        int m = i >>> 3;
        int n = i & 0x7;
        if (m != 1) {
          if (m == 2)
            if (n == paramMetadata.valueType.getWireType()) {
              WireFormat.FieldType fieldType = paramMetadata.valueType;
              V v = paramMetadata.defaultValue;
              Class<?> clazz = v.getClass();
              i = decodeMapEntryValue(paramArrayOfbyte, k, paramInt2, fieldType, clazz, paramRegisters);
              object2 = paramRegisters.object1;
              continue;
            }  
        } else if (n == paramMetadata.keyType.getWireType()) {
          WireFormat.FieldType fieldType = paramMetadata.keyType;
          i = decodeMapEntryValue(paramArrayOfbyte, k, paramInt2, fieldType, null, paramRegisters);
          object1 = paramRegisters.object1;
          continue;
        } 
        i = ArrayDecoders.skipField(i, paramArrayOfbyte, k, paramInt2, paramRegisters);
      } 
      if (i == j) {
        paramMap.put((K)object1, (V)object2);
        return j;
      } 
      throw InvalidProtocolBufferException.parseFailure();
    } 
    throw InvalidProtocolBufferException.truncatedMessage();
  }
  
  private int parseRepeatedField(T paramT, byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, long paramLong1, int paramInt7, long paramLong2, ArrayDecoders.Registers paramRegisters) throws IOException {
    Schema<?> schema;
    UnknownFieldSetLite unknownFieldSetLite1, unknownFieldSetLite2;
    Internal.EnumVerifier enumVerifier;
    UnknownFieldSchema<?, ?> unknownFieldSchema;
    Internal.ProtobufList<?> protobufList = (Internal.ProtobufList)UNSAFE.getObject(paramT, paramLong2);
    if (!protobufList.isModifiable()) {
      int i = protobufList.size();
      if (i == 0) {
        i = 10;
      } else {
        i *= 2;
      } 
      protobufList = protobufList.mutableCopyWithCapacity(i);
      UNSAFE.putObject(paramT, paramLong2, protobufList);
    } 
    switch (paramInt7) {
      default:
        return paramInt1;
      case 49:
        if (paramInt5 == 3) {
          schema = getMessageFieldSchema(paramInt6);
          paramInt1 = ArrayDecoders.decodeGroupList(schema, paramInt3, paramArrayOfbyte, paramInt1, paramInt2, protobufList, paramRegisters);
        } 
      case 34:
      case 48:
        if (paramInt5 == 2) {
          paramInt1 = ArrayDecoders.decodePackedSInt64List(paramArrayOfbyte, paramInt1, protobufList, paramRegisters);
        } else if (paramInt5 == 0) {
          paramInt1 = ArrayDecoders.decodeSInt64List(paramInt3, paramArrayOfbyte, paramInt1, paramInt2, protobufList, paramRegisters);
        } 
      case 33:
      case 47:
        if (paramInt5 == 2) {
          paramInt1 = ArrayDecoders.decodePackedSInt32List(paramArrayOfbyte, paramInt1, protobufList, paramRegisters);
        } else if (paramInt5 == 0) {
          paramInt1 = ArrayDecoders.decodeSInt32List(paramInt3, paramArrayOfbyte, paramInt1, paramInt2, protobufList, paramRegisters);
        } 
      case 30:
      case 44:
        if (paramInt5 == 2) {
          paramInt2 = ArrayDecoders.decodePackedVarint32List(paramArrayOfbyte, paramInt1, protobufList, paramRegisters);
        } else if (paramInt5 == 0) {
          paramInt2 = ArrayDecoders.decodeVarint32List(paramInt3, paramArrayOfbyte, paramInt1, paramInt2, protobufList, paramRegisters);
        } else {
        
        } 
        unknownFieldSetLite2 = ((GeneratedMessageLite)schema).unknownFields;
        unknownFieldSetLite1 = unknownFieldSetLite2;
        if (unknownFieldSetLite2 == UnknownFieldSetLite.getDefaultInstance())
          unknownFieldSetLite1 = null; 
        enumVerifier = getEnumFieldVerifier(paramInt6);
        unknownFieldSchema = this.unknownFieldSchema;
        unknownFieldSetLite1 = (UnknownFieldSetLite)SchemaUtil.filterUnknownEnumList(paramInt4, (List)protobufList, enumVerifier, unknownFieldSetLite1, unknownFieldSchema);
        paramInt1 = paramInt2;
        if (unknownFieldSetLite1 != null) {
          ((GeneratedMessageLite)schema).unknownFields = unknownFieldSetLite1;
          paramInt1 = paramInt2;
        } 
      case 28:
        if (paramInt5 == 2)
          paramInt1 = ArrayDecoders.decodeBytesList(paramInt3, (byte[])unknownFieldSetLite1, paramInt1, paramInt2, protobufList, (ArrayDecoders.Registers)enumVerifier); 
      case 27:
        if (paramInt5 == 2) {
          schema = getMessageFieldSchema(paramInt6);
          paramInt1 = ArrayDecoders.decodeMessageList(schema, paramInt3, (byte[])unknownFieldSetLite1, paramInt1, paramInt2, protobufList, (ArrayDecoders.Registers)enumVerifier);
        } 
      case 26:
        if (paramInt5 == 2)
          if ((paramLong1 & 0x20000000L) == 0L) {
            paramInt1 = ArrayDecoders.decodeStringList(paramInt3, (byte[])unknownFieldSetLite1, paramInt1, paramInt2, protobufList, (ArrayDecoders.Registers)enumVerifier);
          } else {
            paramInt1 = ArrayDecoders.decodeStringListRequireUtf8(paramInt3, (byte[])unknownFieldSetLite1, paramInt1, paramInt2, protobufList, (ArrayDecoders.Registers)enumVerifier);
          }  
      case 25:
      case 42:
        if (paramInt5 == 2) {
          paramInt1 = ArrayDecoders.decodePackedBoolList((byte[])unknownFieldSetLite1, paramInt1, protobufList, (ArrayDecoders.Registers)enumVerifier);
        } else if (paramInt5 == 0) {
          paramInt1 = ArrayDecoders.decodeBoolList(paramInt3, (byte[])unknownFieldSetLite1, paramInt1, paramInt2, protobufList, (ArrayDecoders.Registers)enumVerifier);
        } 
      case 24:
      case 31:
      case 41:
      case 45:
        if (paramInt5 == 2) {
          paramInt1 = ArrayDecoders.decodePackedFixed32List((byte[])unknownFieldSetLite1, paramInt1, protobufList, (ArrayDecoders.Registers)enumVerifier);
        } else if (paramInt5 == 5) {
          paramInt1 = ArrayDecoders.decodeFixed32List(paramInt3, (byte[])unknownFieldSetLite1, paramInt1, paramInt2, protobufList, (ArrayDecoders.Registers)enumVerifier);
        } 
      case 23:
      case 32:
      case 40:
      case 46:
        if (paramInt5 == 2) {
          paramInt1 = ArrayDecoders.decodePackedFixed64List((byte[])unknownFieldSetLite1, paramInt1, protobufList, (ArrayDecoders.Registers)enumVerifier);
        } else if (paramInt5 == 1) {
          paramInt1 = ArrayDecoders.decodeFixed64List(paramInt3, (byte[])unknownFieldSetLite1, paramInt1, paramInt2, protobufList, (ArrayDecoders.Registers)enumVerifier);
        } 
      case 22:
      case 29:
      case 39:
      case 43:
        if (paramInt5 == 2) {
          paramInt1 = ArrayDecoders.decodePackedVarint32List((byte[])unknownFieldSetLite1, paramInt1, protobufList, (ArrayDecoders.Registers)enumVerifier);
        } else if (paramInt5 == 0) {
          paramInt1 = ArrayDecoders.decodeVarint32List(paramInt3, (byte[])unknownFieldSetLite1, paramInt1, paramInt2, protobufList, (ArrayDecoders.Registers)enumVerifier);
        } 
      case 20:
      case 21:
      case 37:
      case 38:
        if (paramInt5 == 2) {
          paramInt1 = ArrayDecoders.decodePackedVarint64List((byte[])unknownFieldSetLite1, paramInt1, protobufList, (ArrayDecoders.Registers)enumVerifier);
        } else if (paramInt5 == 0) {
          paramInt1 = ArrayDecoders.decodeVarint64List(paramInt3, (byte[])unknownFieldSetLite1, paramInt1, paramInt2, protobufList, (ArrayDecoders.Registers)enumVerifier);
        } 
      case 19:
      case 36:
        if (paramInt5 == 2) {
          paramInt1 = ArrayDecoders.decodePackedFloatList((byte[])unknownFieldSetLite1, paramInt1, protobufList, (ArrayDecoders.Registers)enumVerifier);
        } else if (paramInt5 == 5) {
          paramInt1 = ArrayDecoders.decodeFloatList(paramInt3, (byte[])unknownFieldSetLite1, paramInt1, paramInt2, protobufList, (ArrayDecoders.Registers)enumVerifier);
        } 
      case 18:
      case 35:
        break;
    } 
    if (paramInt5 == 2)
      paramInt1 = ArrayDecoders.decodePackedDoubleList((byte[])unknownFieldSetLite1, paramInt1, protobufList, (ArrayDecoders.Registers)enumVerifier); 
    if (paramInt5 == 1)
      paramInt1 = ArrayDecoders.decodeDoubleList(paramInt3, (byte[])unknownFieldSetLite1, paramInt1, paramInt2, protobufList, (ArrayDecoders.Registers)enumVerifier); 
  }
  
  private <K, V> int parseMapField(T paramT, byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3, long paramLong, ArrayDecoders.Registers paramRegisters) throws IOException {
    Unsafe unsafe = UNSAFE;
    Object object = getMapFieldDefaultEntry(paramInt3);
    Object<?, ?> object1 = (Object<?, ?>)unsafe.getObject(paramT, paramLong);
    if (this.mapFieldSchema.isImmutable(object1)) {
      Object object2 = this.mapFieldSchema.newMapField(object);
      this.mapFieldSchema.mergeFrom(object2, object1);
      unsafe.putObject(paramT, paramLong, object2);
      paramT = (T)object2;
    } else {
      paramT = (T)object1;
    } 
    object1 = (Object<?, ?>)this.mapFieldSchema;
    object1 = (Object<?, ?>)object1.forMapMetadata(object);
    MapFieldSchema mapFieldSchema = this.mapFieldSchema;
    Map<?, ?> map = mapFieldSchema.forMutableMapData(paramT);
    return decodeMapEntry(paramArrayOfbyte, paramInt1, paramInt2, (MapEntryLite.Metadata<?, ?>)object1, map, paramRegisters);
  }
  
  private int parseOneofField(T paramT, byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, long paramLong, int paramInt8, ArrayDecoders.Registers paramRegisters) throws IOException {
    Object object1, object2;
    Unsafe unsafe = UNSAFE;
    long l = (this.buffer[paramInt8 + 2] & 0xFFFFF);
    Schema schema = null;
    byte[] arrayOfByte = null;
    switch (paramInt7) {
      default:
        return paramInt1;
      case 68:
        if (paramInt5 == 3) {
          schema = getMessageFieldSchema(paramInt8);
          paramInt1 = ArrayDecoders.decodeGroupField(schema, paramArrayOfbyte, paramInt1, paramInt2, paramInt3 & 0xFFFFFFF8 | 0x4, paramRegisters);
          if (unsafe.getInt(paramT, l) == paramInt4) {
            object1 = unsafe.getObject(paramT, paramLong);
          } else {
            paramArrayOfbyte = arrayOfByte;
          } 
          if (paramArrayOfbyte == null) {
            unsafe.putObject(paramT, paramLong, paramRegisters.object1);
          } else {
            object2 = paramRegisters.object1;
            object1 = Internal.mergeMessage(paramArrayOfbyte, object2);
            unsafe.putObject(paramT, paramLong, object1);
          } 
          unsafe.putInt(paramT, l, paramInt4);
        } 
      case 67:
        if (paramInt5 == 0) {
          paramInt1 = ArrayDecoders.decodeVarint64((byte[])object1, paramInt1, (ArrayDecoders.Registers)object2);
          unsafe.putObject(paramT, paramLong, Long.valueOf(CodedInputStream.decodeZigZag64(((ArrayDecoders.Registers)object2).long1)));
          unsafe.putInt(paramT, l, paramInt4);
        } 
      case 66:
        if (paramInt5 == 0) {
          paramInt1 = ArrayDecoders.decodeVarint32((byte[])object1, paramInt1, (ArrayDecoders.Registers)object2);
          unsafe.putObject(paramT, paramLong, Integer.valueOf(CodedInputStream.decodeZigZag32(((ArrayDecoders.Registers)object2).int1)));
          unsafe.putInt(paramT, l, paramInt4);
        } 
      case 63:
        if (paramInt5 == 0) {
          paramInt1 = ArrayDecoders.decodeVarint32((byte[])object1, paramInt1, (ArrayDecoders.Registers)object2);
          paramInt2 = ((ArrayDecoders.Registers)object2).int1;
          object1 = getEnumFieldVerifier(paramInt8);
          if (object1 == null || object1.isInRange(paramInt2)) {
            unsafe.putObject(paramT, paramLong, Integer.valueOf(paramInt2));
            unsafe.putInt(paramT, l, paramInt4);
          } 
          getMutableUnknownFields(paramT).storeField(paramInt3, Long.valueOf(paramInt2));
        } 
      case 61:
        if (paramInt5 == 2) {
          paramInt1 = ArrayDecoders.decodeBytes((byte[])object1, paramInt1, (ArrayDecoders.Registers)object2);
          unsafe.putObject(paramT, paramLong, ((ArrayDecoders.Registers)object2).object1);
          unsafe.putInt(paramT, l, paramInt4);
        } 
      case 60:
        if (paramInt5 == 2) {
          Schema schema1 = getMessageFieldSchema(paramInt8);
          paramInt1 = ArrayDecoders.decodeMessageField(schema1, (byte[])object1, paramInt1, paramInt2, (ArrayDecoders.Registers)object2);
          if (unsafe.getInt(paramT, l) == paramInt4) {
            object1 = unsafe.getObject(paramT, paramLong);
          } else {
            object1 = schema;
          } 
          if (object1 == null) {
            unsafe.putObject(paramT, paramLong, ((ArrayDecoders.Registers)object2).object1);
          } else {
            object2 = ((ArrayDecoders.Registers)object2).object1;
            object1 = Internal.mergeMessage(object1, object2);
            unsafe.putObject(paramT, paramLong, object1);
          } 
          unsafe.putInt(paramT, l, paramInt4);
        } 
      case 59:
        if (paramInt5 == 2) {
          paramInt1 = ArrayDecoders.decodeVarint32((byte[])object1, paramInt1, (ArrayDecoders.Registers)object2);
          paramInt2 = ((ArrayDecoders.Registers)object2).int1;
          if (paramInt2 == 0) {
            unsafe.putObject(paramT, paramLong, "");
          } else if ((paramInt6 & 0x20000000) == 0 || Utf8.isValidUtf8((byte[])object1, paramInt1, paramInt1 + paramInt2)) {
            object1 = new String((byte[])object1, paramInt1, paramInt2, Internal.UTF_8);
            unsafe.putObject(paramT, paramLong, object1);
            paramInt1 += paramInt2;
          } else {
            throw InvalidProtocolBufferException.invalidUtf8();
          } 
          unsafe.putInt(paramT, l, paramInt4);
        } 
      case 58:
        if (paramInt5 == 0) {
          boolean bool;
          paramInt1 = ArrayDecoders.decodeVarint64((byte[])object1, paramInt1, (ArrayDecoders.Registers)object2);
          if (((ArrayDecoders.Registers)object2).long1 != 0L) {
            bool = true;
          } else {
            bool = false;
          } 
          unsafe.putObject(paramT, paramLong, Boolean.valueOf(bool));
          unsafe.putInt(paramT, l, paramInt4);
        } 
      case 57:
      case 64:
        if (paramInt5 == 5) {
          unsafe.putObject(paramT, paramLong, Integer.valueOf(ArrayDecoders.decodeFixed32((byte[])object1, paramInt1)));
          paramInt1 += 4;
          unsafe.putInt(paramT, l, paramInt4);
        } 
      case 56:
      case 65:
        if (paramInt5 == 1) {
          unsafe.putObject(paramT, paramLong, Long.valueOf(ArrayDecoders.decodeFixed64((byte[])object1, paramInt1)));
          paramInt1 += 8;
          unsafe.putInt(paramT, l, paramInt4);
        } 
      case 55:
      case 62:
        if (paramInt5 == 0) {
          paramInt1 = ArrayDecoders.decodeVarint32((byte[])object1, paramInt1, (ArrayDecoders.Registers)object2);
          unsafe.putObject(paramT, paramLong, Integer.valueOf(((ArrayDecoders.Registers)object2).int1));
          unsafe.putInt(paramT, l, paramInt4);
        } 
      case 53:
      case 54:
        if (paramInt5 == 0) {
          paramInt1 = ArrayDecoders.decodeVarint64((byte[])object1, paramInt1, (ArrayDecoders.Registers)object2);
          unsafe.putObject(paramT, paramLong, Long.valueOf(((ArrayDecoders.Registers)object2).long1));
          unsafe.putInt(paramT, l, paramInt4);
        } 
      case 52:
        if (paramInt5 == 5) {
          unsafe.putObject(paramT, paramLong, Float.valueOf(ArrayDecoders.decodeFloat((byte[])object1, paramInt1)));
          paramInt1 += 4;
          unsafe.putInt(paramT, l, paramInt4);
        } 
      case 51:
        break;
    } 
    if (paramInt5 == 1) {
      unsafe.putObject(paramT, paramLong, Double.valueOf(ArrayDecoders.decodeDouble((byte[])object1, paramInt1)));
      paramInt1 += 8;
      unsafe.putInt(paramT, l, paramInt4);
    } 
  }
  
  private Schema getMessageFieldSchema(int paramInt) {
    paramInt = paramInt / 3 * 2;
    Schema<?> schema = (Schema)this.objects[paramInt];
    if (schema != null)
      return schema; 
    schema = Protobuf.getInstance().schemaFor((Class)this.objects[paramInt + 1]);
    this.objects[paramInt] = schema;
    return schema;
  }
  
  private Object getMapFieldDefaultEntry(int paramInt) {
    return this.objects[paramInt / 3 * 2];
  }
  
  private Internal.EnumVerifier getEnumFieldVerifier(int paramInt) {
    return (Internal.EnumVerifier)this.objects[paramInt / 3 * 2 + 1];
  }
  
  int parseProto2Message(T paramT, byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3, ArrayDecoders.Registers paramRegisters) throws IOException {
    // Byte code:
    //   0: aload #6
    //   2: astore #7
    //   4: getstatic com/android/framework/protobuf/MessageSchema.UNSAFE : Lsun/misc/Unsafe;
    //   7: astore #8
    //   9: iconst_0
    //   10: istore #9
    //   12: iconst_m1
    //   13: istore #10
    //   15: iconst_0
    //   16: istore #11
    //   18: iconst_m1
    //   19: istore #12
    //   21: iconst_0
    //   22: istore #13
    //   24: iload_3
    //   25: istore #14
    //   27: iload #12
    //   29: istore_3
    //   30: iload #14
    //   32: iload #4
    //   34: if_icmpge -> 1846
    //   37: iload #14
    //   39: iconst_1
    //   40: iadd
    //   41: istore #15
    //   43: aload_2
    //   44: iload #14
    //   46: baload
    //   47: istore #9
    //   49: iload #9
    //   51: ifge -> 76
    //   54: iload #9
    //   56: aload_2
    //   57: iload #15
    //   59: aload #7
    //   61: invokestatic decodeVarint32 : (I[BILcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   64: istore #15
    //   66: aload #7
    //   68: getfield int1 : I
    //   71: istore #9
    //   73: goto -> 76
    //   76: iload #9
    //   78: iconst_3
    //   79: iushr
    //   80: istore #12
    //   82: iload #9
    //   84: bipush #7
    //   86: iand
    //   87: istore #16
    //   89: iload #12
    //   91: iload #10
    //   93: if_icmple -> 111
    //   96: aload_0
    //   97: iload #12
    //   99: iload #11
    //   101: iconst_3
    //   102: idiv
    //   103: invokespecial positionForFieldNumber : (II)I
    //   106: istore #14
    //   108: goto -> 119
    //   111: aload_0
    //   112: iload #12
    //   114: invokespecial positionForFieldNumber : (I)I
    //   117: istore #14
    //   119: iload #14
    //   121: iconst_m1
    //   122: if_icmpne -> 143
    //   125: iload #9
    //   127: istore #14
    //   129: iconst_0
    //   130: istore #10
    //   132: iload #15
    //   134: istore #14
    //   136: aload #8
    //   138: astore #7
    //   140: goto -> 1732
    //   143: aload_0
    //   144: getfield buffer : [I
    //   147: iload #14
    //   149: iconst_1
    //   150: iadd
    //   151: iaload
    //   152: istore #17
    //   154: iload #17
    //   156: invokestatic type : (I)I
    //   159: istore #18
    //   161: iload #17
    //   163: invokestatic offset : (I)J
    //   166: lstore #19
    //   168: iload #18
    //   170: bipush #17
    //   172: if_icmpgt -> 1335
    //   175: aload_0
    //   176: getfield buffer : [I
    //   179: iload #14
    //   181: iconst_2
    //   182: iadd
    //   183: iaload
    //   184: istore #10
    //   186: iconst_1
    //   187: iload #10
    //   189: bipush #20
    //   191: iushr
    //   192: ishl
    //   193: istore #11
    //   195: iload #10
    //   197: ldc 1048575
    //   199: iand
    //   200: istore #10
    //   202: iload #10
    //   204: iload_3
    //   205: if_icmpeq -> 243
    //   208: iload_3
    //   209: iconst_m1
    //   210: if_icmpeq -> 226
    //   213: aload #8
    //   215: aload_1
    //   216: iload_3
    //   217: i2l
    //   218: iload #13
    //   220: invokevirtual putInt : (Ljava/lang/Object;JI)V
    //   223: goto -> 226
    //   226: iload #10
    //   228: istore_3
    //   229: aload #8
    //   231: aload_1
    //   232: iload #10
    //   234: i2l
    //   235: invokevirtual getInt : (Ljava/lang/Object;J)I
    //   238: istore #13
    //   240: goto -> 243
    //   243: iload #18
    //   245: tableswitch default -> 332, 0 -> 1273, 1 -> 1230, 2 -> 1172, 3 -> 1172, 4 -> 1122, 5 -> 1074, 6 -> 1026, 7 -> 951, 8 -> 876, 9 -> 763, 10 -> 705, 11 -> 1122, 12 -> 582, 13 -> 1026, 14 -> 1074, 15 -> 518, 16 -> 454, 17 -> 335
    //   332: goto -> 1316
    //   335: iload #16
    //   337: iconst_3
    //   338: if_icmpne -> 451
    //   341: aload_0
    //   342: iload #14
    //   344: invokespecial getMessageFieldSchema : (I)Lcom/android/framework/protobuf/Schema;
    //   347: astore #21
    //   349: aload #21
    //   351: aload_2
    //   352: iload #15
    //   354: iload #4
    //   356: iload #12
    //   358: iconst_3
    //   359: ishl
    //   360: iconst_4
    //   361: ior
    //   362: aload #6
    //   364: invokestatic decodeGroupField : (Lcom/android/framework/protobuf/Schema;[BIIILcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   367: istore #15
    //   369: iload #13
    //   371: iload #11
    //   373: iand
    //   374: ifne -> 393
    //   377: aload #8
    //   379: aload_1
    //   380: lload #19
    //   382: aload #7
    //   384: getfield object1 : Ljava/lang/Object;
    //   387: invokevirtual putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   390: goto -> 429
    //   393: aload #8
    //   395: aload_1
    //   396: lload #19
    //   398: invokevirtual getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   401: astore #21
    //   403: aload #7
    //   405: getfield object1 : Ljava/lang/Object;
    //   408: astore #22
    //   410: aload #21
    //   412: aload #22
    //   414: invokestatic mergeMessage : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   417: astore #21
    //   419: aload #8
    //   421: aload_1
    //   422: lload #19
    //   424: aload #21
    //   426: invokevirtual putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   429: iload #13
    //   431: iload #11
    //   433: ior
    //   434: istore #13
    //   436: iload #14
    //   438: istore #11
    //   440: iload #12
    //   442: istore #10
    //   444: iload #15
    //   446: istore #14
    //   448: goto -> 30
    //   451: goto -> 1316
    //   454: iload #16
    //   456: ifne -> 515
    //   459: aload_2
    //   460: iload #15
    //   462: aload #7
    //   464: invokestatic decodeVarint64 : ([BILcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   467: istore #10
    //   469: aload #7
    //   471: getfield long1 : J
    //   474: lstore #23
    //   476: lload #23
    //   478: invokestatic decodeZigZag64 : (J)J
    //   481: lstore #23
    //   483: aload #8
    //   485: aload_1
    //   486: lload #19
    //   488: lload #23
    //   490: invokevirtual putLong : (Ljava/lang/Object;JJ)V
    //   493: iload #13
    //   495: iload #11
    //   497: ior
    //   498: istore #13
    //   500: iload #14
    //   502: istore #11
    //   504: iload #10
    //   506: istore #14
    //   508: iload #12
    //   510: istore #10
    //   512: goto -> 30
    //   515: goto -> 1316
    //   518: iload #16
    //   520: ifne -> 579
    //   523: aload_2
    //   524: iload #15
    //   526: aload #7
    //   528: invokestatic decodeVarint32 : ([BILcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   531: istore #15
    //   533: aload #7
    //   535: getfield int1 : I
    //   538: istore #10
    //   540: iload #10
    //   542: invokestatic decodeZigZag32 : (I)I
    //   545: istore #10
    //   547: aload #8
    //   549: aload_1
    //   550: lload #19
    //   552: iload #10
    //   554: invokevirtual putInt : (Ljava/lang/Object;JI)V
    //   557: iload #13
    //   559: iload #11
    //   561: ior
    //   562: istore #13
    //   564: iload #14
    //   566: istore #11
    //   568: iload #12
    //   570: istore #10
    //   572: iload #15
    //   574: istore #14
    //   576: goto -> 30
    //   579: goto -> 1316
    //   582: iload #16
    //   584: ifne -> 702
    //   587: aload_2
    //   588: iload #15
    //   590: aload #7
    //   592: invokestatic decodeVarint32 : ([BILcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   595: istore #15
    //   597: aload #7
    //   599: getfield int1 : I
    //   602: istore #10
    //   604: aload_0
    //   605: iload #14
    //   607: invokespecial getEnumFieldVerifier : (I)Lcom/android/framework/protobuf/Internal$EnumVerifier;
    //   610: astore #7
    //   612: aload #7
    //   614: ifnull -> 666
    //   617: aload #7
    //   619: iload #10
    //   621: invokeinterface isInRange : (I)Z
    //   626: ifeq -> 632
    //   629: goto -> 666
    //   632: aload_1
    //   633: invokestatic getMutableUnknownFields : (Ljava/lang/Object;)Lcom/android/framework/protobuf/UnknownFieldSetLite;
    //   636: iload #9
    //   638: iload #10
    //   640: i2l
    //   641: invokestatic valueOf : (J)Ljava/lang/Long;
    //   644: invokevirtual storeField : (ILjava/lang/Object;)V
    //   647: iload #14
    //   649: istore #11
    //   651: iload #12
    //   653: istore #10
    //   655: aload #6
    //   657: astore #7
    //   659: iload #15
    //   661: istore #14
    //   663: goto -> 30
    //   666: aload #8
    //   668: aload_1
    //   669: lload #19
    //   671: iload #10
    //   673: invokevirtual putInt : (Ljava/lang/Object;JI)V
    //   676: iload #13
    //   678: iload #11
    //   680: ior
    //   681: istore #13
    //   683: iload #14
    //   685: istore #11
    //   687: iload #12
    //   689: istore #10
    //   691: aload #6
    //   693: astore #7
    //   695: iload #15
    //   697: istore #14
    //   699: goto -> 30
    //   702: goto -> 1316
    //   705: iload #16
    //   707: iconst_2
    //   708: if_icmpne -> 760
    //   711: aload_2
    //   712: iload #15
    //   714: aload #6
    //   716: invokestatic decodeBytes : ([BILcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   719: istore #15
    //   721: aload #8
    //   723: aload_1
    //   724: lload #19
    //   726: aload #6
    //   728: getfield object1 : Ljava/lang/Object;
    //   731: invokevirtual putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   734: iload #13
    //   736: iload #11
    //   738: ior
    //   739: istore #13
    //   741: iload #14
    //   743: istore #11
    //   745: iload #12
    //   747: istore #10
    //   749: aload #6
    //   751: astore #7
    //   753: iload #15
    //   755: istore #14
    //   757: goto -> 30
    //   760: goto -> 1316
    //   763: iload #16
    //   765: iconst_2
    //   766: if_icmpne -> 873
    //   769: aload_0
    //   770: iload #14
    //   772: invokespecial getMessageFieldSchema : (I)Lcom/android/framework/protobuf/Schema;
    //   775: astore #21
    //   777: aload #21
    //   779: aload_2
    //   780: iload #15
    //   782: iload #4
    //   784: aload #7
    //   786: invokestatic decodeMessageField : (Lcom/android/framework/protobuf/Schema;[BIILcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   789: istore #15
    //   791: iload #13
    //   793: iload #11
    //   795: iand
    //   796: ifne -> 815
    //   799: aload #8
    //   801: aload_1
    //   802: lload #19
    //   804: aload #7
    //   806: getfield object1 : Ljava/lang/Object;
    //   809: invokevirtual putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   812: goto -> 851
    //   815: aload #8
    //   817: aload_1
    //   818: lload #19
    //   820: invokevirtual getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   823: astore #22
    //   825: aload #7
    //   827: getfield object1 : Ljava/lang/Object;
    //   830: astore #21
    //   832: aload #22
    //   834: aload #21
    //   836: invokestatic mergeMessage : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   839: astore #21
    //   841: aload #8
    //   843: aload_1
    //   844: lload #19
    //   846: aload #21
    //   848: invokevirtual putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   851: iload #13
    //   853: iload #11
    //   855: ior
    //   856: istore #13
    //   858: iload #14
    //   860: istore #11
    //   862: iload #12
    //   864: istore #10
    //   866: iload #15
    //   868: istore #14
    //   870: goto -> 30
    //   873: goto -> 1316
    //   876: iload #16
    //   878: iconst_2
    //   879: if_icmpne -> 948
    //   882: ldc 536870912
    //   884: iload #17
    //   886: iand
    //   887: ifne -> 903
    //   890: aload_2
    //   891: iload #15
    //   893: aload #7
    //   895: invokestatic decodeString : ([BILcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   898: istore #10
    //   900: goto -> 913
    //   903: aload_2
    //   904: iload #15
    //   906: aload #7
    //   908: invokestatic decodeStringRequireUtf8 : ([BILcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   911: istore #10
    //   913: aload #8
    //   915: aload_1
    //   916: lload #19
    //   918: aload #7
    //   920: getfield object1 : Ljava/lang/Object;
    //   923: invokevirtual putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   926: iload #13
    //   928: iload #11
    //   930: ior
    //   931: istore #13
    //   933: iload #14
    //   935: istore #11
    //   937: iload #10
    //   939: istore #14
    //   941: iload #12
    //   943: istore #10
    //   945: goto -> 30
    //   948: goto -> 1316
    //   951: iload #16
    //   953: ifne -> 1023
    //   956: aload_2
    //   957: iload #15
    //   959: aload #7
    //   961: invokestatic decodeVarint64 : ([BILcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   964: istore #10
    //   966: aload #7
    //   968: getfield long1 : J
    //   971: lconst_0
    //   972: lcmp
    //   973: ifeq -> 982
    //   976: iconst_1
    //   977: istore #25
    //   979: goto -> 985
    //   982: iconst_0
    //   983: istore #25
    //   985: aload_1
    //   986: lload #19
    //   988: iload #25
    //   990: invokestatic putBoolean : (Ljava/lang/Object;JZ)V
    //   993: iload #13
    //   995: iload #11
    //   997: ior
    //   998: istore #15
    //   1000: iload #10
    //   1002: istore #13
    //   1004: iload #14
    //   1006: istore #11
    //   1008: iload #12
    //   1010: istore #10
    //   1012: iload #13
    //   1014: istore #14
    //   1016: iload #15
    //   1018: istore #13
    //   1020: goto -> 30
    //   1023: goto -> 1316
    //   1026: iload #16
    //   1028: iconst_5
    //   1029: if_icmpne -> 1071
    //   1032: aload #8
    //   1034: aload_1
    //   1035: lload #19
    //   1037: aload_2
    //   1038: iload #15
    //   1040: invokestatic decodeFixed32 : ([BI)I
    //   1043: invokevirtual putInt : (Ljava/lang/Object;JI)V
    //   1046: iinc #15, 4
    //   1049: iload #13
    //   1051: iload #11
    //   1053: ior
    //   1054: istore #13
    //   1056: iload #14
    //   1058: istore #11
    //   1060: iload #12
    //   1062: istore #10
    //   1064: iload #15
    //   1066: istore #14
    //   1068: goto -> 30
    //   1071: goto -> 1316
    //   1074: iload #16
    //   1076: iconst_1
    //   1077: if_icmpne -> 1119
    //   1080: aload #8
    //   1082: aload_1
    //   1083: lload #19
    //   1085: aload_2
    //   1086: iload #15
    //   1088: invokestatic decodeFixed64 : ([BI)J
    //   1091: invokevirtual putLong : (Ljava/lang/Object;JJ)V
    //   1094: iinc #15, 8
    //   1097: iload #13
    //   1099: iload #11
    //   1101: ior
    //   1102: istore #13
    //   1104: iload #12
    //   1106: istore #10
    //   1108: iload #14
    //   1110: istore #11
    //   1112: iload #15
    //   1114: istore #14
    //   1116: goto -> 30
    //   1119: goto -> 1316
    //   1122: iload #16
    //   1124: ifne -> 1316
    //   1127: aload_2
    //   1128: iload #15
    //   1130: aload #7
    //   1132: invokestatic decodeVarint32 : ([BILcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   1135: istore #15
    //   1137: aload #8
    //   1139: aload_1
    //   1140: lload #19
    //   1142: aload #7
    //   1144: getfield int1 : I
    //   1147: invokevirtual putInt : (Ljava/lang/Object;JI)V
    //   1150: iload #13
    //   1152: iload #11
    //   1154: ior
    //   1155: istore #13
    //   1157: iload #12
    //   1159: istore #10
    //   1161: iload #14
    //   1163: istore #11
    //   1165: iload #15
    //   1167: istore #14
    //   1169: goto -> 30
    //   1172: iload #16
    //   1174: ifne -> 1316
    //   1177: aload_2
    //   1178: iload #15
    //   1180: aload #7
    //   1182: invokestatic decodeVarint64 : ([BILcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   1185: istore #10
    //   1187: aload #8
    //   1189: aload_1
    //   1190: lload #19
    //   1192: aload #7
    //   1194: getfield long1 : J
    //   1197: invokevirtual putLong : (Ljava/lang/Object;JJ)V
    //   1200: iload #13
    //   1202: iload #11
    //   1204: ior
    //   1205: istore #15
    //   1207: iload #10
    //   1209: istore #13
    //   1211: iload #12
    //   1213: istore #10
    //   1215: iload #14
    //   1217: istore #11
    //   1219: iload #13
    //   1221: istore #14
    //   1223: iload #15
    //   1225: istore #13
    //   1227: goto -> 30
    //   1230: iload #16
    //   1232: iconst_5
    //   1233: if_icmpne -> 1316
    //   1236: aload_1
    //   1237: lload #19
    //   1239: aload_2
    //   1240: iload #15
    //   1242: invokestatic decodeFloat : ([BI)F
    //   1245: invokestatic putFloat : (Ljava/lang/Object;JF)V
    //   1248: iinc #15, 4
    //   1251: iload #13
    //   1253: iload #11
    //   1255: ior
    //   1256: istore #13
    //   1258: iload #12
    //   1260: istore #10
    //   1262: iload #14
    //   1264: istore #11
    //   1266: iload #15
    //   1268: istore #14
    //   1270: goto -> 30
    //   1273: iload #16
    //   1275: iconst_1
    //   1276: if_icmpne -> 1316
    //   1279: aload_1
    //   1280: lload #19
    //   1282: aload_2
    //   1283: iload #15
    //   1285: invokestatic decodeDouble : ([BI)D
    //   1288: invokestatic putDouble : (Ljava/lang/Object;JD)V
    //   1291: iinc #15, 8
    //   1294: iload #13
    //   1296: iload #11
    //   1298: ior
    //   1299: istore #13
    //   1301: iload #12
    //   1303: istore #10
    //   1305: iload #14
    //   1307: istore #11
    //   1309: iload #15
    //   1311: istore #14
    //   1313: goto -> 30
    //   1316: aload #8
    //   1318: astore #7
    //   1320: iload #9
    //   1322: istore #10
    //   1324: iload #14
    //   1326: istore #10
    //   1328: iload #15
    //   1330: istore #14
    //   1332: goto -> 1732
    //   1335: iload #18
    //   1337: bipush #27
    //   1339: if_icmpne -> 1466
    //   1342: iload #16
    //   1344: iconst_2
    //   1345: if_icmpne -> 1463
    //   1348: aload #8
    //   1350: aload_1
    //   1351: lload #19
    //   1353: invokevirtual getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   1356: checkcast com/android/framework/protobuf/Internal$ProtobufList
    //   1359: astore #21
    //   1361: aload #21
    //   1363: invokeinterface isModifiable : ()Z
    //   1368: ifne -> 1422
    //   1371: aload #21
    //   1373: invokeinterface size : ()I
    //   1378: istore #10
    //   1380: iload #10
    //   1382: ifne -> 1392
    //   1385: bipush #10
    //   1387: istore #10
    //   1389: goto -> 1398
    //   1392: iload #10
    //   1394: iconst_2
    //   1395: imul
    //   1396: istore #10
    //   1398: aload #21
    //   1400: iload #10
    //   1402: invokeinterface mutableCopyWithCapacity : (I)Lcom/android/framework/protobuf/Internal$ProtobufList;
    //   1407: astore #21
    //   1409: aload #8
    //   1411: aload_1
    //   1412: lload #19
    //   1414: aload #21
    //   1416: invokevirtual putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   1419: goto -> 1422
    //   1422: aload_0
    //   1423: iload #14
    //   1425: invokespecial getMessageFieldSchema : (I)Lcom/android/framework/protobuf/Schema;
    //   1428: astore #22
    //   1430: aload #22
    //   1432: iload #9
    //   1434: aload_2
    //   1435: iload #15
    //   1437: iload #4
    //   1439: aload #21
    //   1441: aload #6
    //   1443: invokestatic decodeMessageList : (Lcom/android/framework/protobuf/Schema;I[BIILcom/android/framework/protobuf/Internal$ProtobufList;Lcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   1446: istore #15
    //   1448: iload #12
    //   1450: istore #10
    //   1452: iload #14
    //   1454: istore #11
    //   1456: iload #15
    //   1458: istore #14
    //   1460: goto -> 30
    //   1463: goto -> 1643
    //   1466: iload #14
    //   1468: istore #10
    //   1470: iload #18
    //   1472: bipush #49
    //   1474: if_icmpgt -> 1555
    //   1477: iload #17
    //   1479: i2l
    //   1480: lstore #23
    //   1482: iload #9
    //   1484: istore #11
    //   1486: aload #8
    //   1488: astore #21
    //   1490: aload_0
    //   1491: aload_1
    //   1492: aload_2
    //   1493: iload #15
    //   1495: iload #4
    //   1497: iload #9
    //   1499: iload #12
    //   1501: iload #16
    //   1503: iload #10
    //   1505: lload #23
    //   1507: iload #18
    //   1509: lload #19
    //   1511: aload #6
    //   1513: invokespecial parseRepeatedField : (Ljava/lang/Object;[BIIIIIIJIJLcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   1516: istore #14
    //   1518: iload #14
    //   1520: iload #15
    //   1522: if_icmpeq -> 1552
    //   1525: aload #6
    //   1527: astore #7
    //   1529: iload #10
    //   1531: istore #15
    //   1533: iload #11
    //   1535: istore #9
    //   1537: aload #21
    //   1539: astore #8
    //   1541: iload #12
    //   1543: istore #10
    //   1545: iload #15
    //   1547: istore #11
    //   1549: goto -> 30
    //   1552: goto -> 1732
    //   1555: iload #13
    //   1557: istore #11
    //   1559: aload #8
    //   1561: astore #7
    //   1563: iload #9
    //   1565: istore #26
    //   1567: iload #18
    //   1569: bipush #50
    //   1571: if_icmpne -> 1662
    //   1574: iload #16
    //   1576: iconst_2
    //   1577: if_icmpne -> 1643
    //   1580: aload_0
    //   1581: aload_1
    //   1582: aload_2
    //   1583: iload #15
    //   1585: iload #4
    //   1587: iload #10
    //   1589: lload #19
    //   1591: aload #6
    //   1593: invokespecial parseMapField : (Ljava/lang/Object;[BIIIJLcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   1596: istore #14
    //   1598: iload #14
    //   1600: iload #15
    //   1602: if_icmpeq -> 1636
    //   1605: aload #6
    //   1607: astore #21
    //   1609: iload #11
    //   1611: istore #13
    //   1613: iload #10
    //   1615: istore #11
    //   1617: iload #26
    //   1619: istore #9
    //   1621: aload #7
    //   1623: astore #8
    //   1625: iload #12
    //   1627: istore #10
    //   1629: aload #21
    //   1631: astore #7
    //   1633: goto -> 30
    //   1636: iload #11
    //   1638: istore #13
    //   1640: goto -> 1732
    //   1643: aload #8
    //   1645: astore #7
    //   1647: iload #9
    //   1649: istore #10
    //   1651: iload #14
    //   1653: istore #10
    //   1655: iload #15
    //   1657: istore #14
    //   1659: goto -> 1732
    //   1662: aload_0
    //   1663: aload_1
    //   1664: aload_2
    //   1665: iload #15
    //   1667: iload #4
    //   1669: iload #26
    //   1671: iload #12
    //   1673: iload #16
    //   1675: iload #17
    //   1677: iload #18
    //   1679: lload #19
    //   1681: iload #10
    //   1683: aload #6
    //   1685: invokespecial parseOneofField : (Ljava/lang/Object;[BIIIIIIIJILcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   1688: istore #14
    //   1690: iload #14
    //   1692: iload #15
    //   1694: if_icmpeq -> 1728
    //   1697: aload #6
    //   1699: astore #21
    //   1701: iload #11
    //   1703: istore #13
    //   1705: iload #10
    //   1707: istore #11
    //   1709: iload #26
    //   1711: istore #9
    //   1713: aload #7
    //   1715: astore #8
    //   1717: iload #12
    //   1719: istore #10
    //   1721: aload #21
    //   1723: astore #7
    //   1725: goto -> 30
    //   1728: iload #11
    //   1730: istore #13
    //   1732: aload #8
    //   1734: astore #7
    //   1736: iload #5
    //   1738: istore #11
    //   1740: iload #9
    //   1742: iload #11
    //   1744: if_icmpne -> 1757
    //   1747: iload #11
    //   1749: ifeq -> 1757
    //   1752: aload_0
    //   1753: astore_2
    //   1754: goto -> 1855
    //   1757: aload_0
    //   1758: getfield hasExtensions : Z
    //   1761: ifeq -> 1805
    //   1764: aload #6
    //   1766: getfield extensionRegistry : Lcom/android/framework/protobuf/ExtensionRegistryLite;
    //   1769: astore #8
    //   1771: aload #8
    //   1773: invokestatic getEmptyRegistry : ()Lcom/android/framework/protobuf/ExtensionRegistryLite;
    //   1776: if_acmpeq -> 1805
    //   1779: iload #9
    //   1781: aload_2
    //   1782: iload #14
    //   1784: iload #4
    //   1786: aload_1
    //   1787: aload_0
    //   1788: getfield defaultInstance : Lcom/android/framework/protobuf/MessageLite;
    //   1791: aload_0
    //   1792: getfield unknownFieldSchema : Lcom/android/framework/protobuf/UnknownFieldSchema;
    //   1795: aload #6
    //   1797: invokestatic decodeExtensionOrUnknownField : (I[BIILjava/lang/Object;Lcom/android/framework/protobuf/MessageLite;Lcom/android/framework/protobuf/UnknownFieldSchema;Lcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   1800: istore #14
    //   1802: goto -> 1827
    //   1805: aload_1
    //   1806: invokestatic getMutableUnknownFields : (Ljava/lang/Object;)Lcom/android/framework/protobuf/UnknownFieldSetLite;
    //   1809: astore #8
    //   1811: iload #9
    //   1813: aload_2
    //   1814: iload #14
    //   1816: iload #4
    //   1818: aload #8
    //   1820: aload #6
    //   1822: invokestatic decodeUnknownField : (I[BIILcom/android/framework/protobuf/UnknownFieldSetLite;Lcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   1825: istore #14
    //   1827: iload #10
    //   1829: istore #11
    //   1831: aload #7
    //   1833: astore #8
    //   1835: aload #6
    //   1837: astore #7
    //   1839: iload #12
    //   1841: istore #10
    //   1843: goto -> 30
    //   1846: aload #8
    //   1848: astore_2
    //   1849: iload #5
    //   1851: istore #10
    //   1853: aload_0
    //   1854: astore_2
    //   1855: aload_0
    //   1856: astore #6
    //   1858: iload_3
    //   1859: iconst_m1
    //   1860: if_icmpeq -> 1876
    //   1863: aload #8
    //   1865: aload_1
    //   1866: iload_3
    //   1867: i2l
    //   1868: iload #13
    //   1870: invokevirtual putInt : (Ljava/lang/Object;JI)V
    //   1873: goto -> 1876
    //   1876: aconst_null
    //   1877: astore_2
    //   1878: aload #6
    //   1880: getfield checkInitializedCount : I
    //   1883: istore_3
    //   1884: iload_3
    //   1885: aload #6
    //   1887: getfield repeatedFieldOffsetStart : I
    //   1890: if_icmpge -> 1930
    //   1893: aload #6
    //   1895: getfield intArray : [I
    //   1898: iload_3
    //   1899: iaload
    //   1900: istore #13
    //   1902: aload #6
    //   1904: getfield unknownFieldSchema : Lcom/android/framework/protobuf/UnknownFieldSchema;
    //   1907: astore #8
    //   1909: aload #6
    //   1911: aload_1
    //   1912: iload #13
    //   1914: aload_2
    //   1915: aload #8
    //   1917: invokespecial filterMapUnknownEnumValues : (Ljava/lang/Object;ILjava/lang/Object;Lcom/android/framework/protobuf/UnknownFieldSchema;)Ljava/lang/Object;
    //   1920: checkcast com/android/framework/protobuf/UnknownFieldSetLite
    //   1923: astore_2
    //   1924: iinc #3, 1
    //   1927: goto -> 1884
    //   1930: aload_2
    //   1931: ifnull -> 1948
    //   1934: aload #6
    //   1936: getfield unknownFieldSchema : Lcom/android/framework/protobuf/UnknownFieldSchema;
    //   1939: astore #6
    //   1941: aload #6
    //   1943: aload_1
    //   1944: aload_2
    //   1945: invokevirtual setBuilderToMessage : (Ljava/lang/Object;Ljava/lang/Object;)V
    //   1948: iload #5
    //   1950: ifne -> 1967
    //   1953: iload #14
    //   1955: iload #4
    //   1957: if_icmpne -> 1963
    //   1960: goto -> 1981
    //   1963: invokestatic parseFailure : ()Lcom/android/framework/protobuf/InvalidProtocolBufferException;
    //   1966: athrow
    //   1967: iload #14
    //   1969: iload #4
    //   1971: if_icmpgt -> 1984
    //   1974: iload #9
    //   1976: iload #5
    //   1978: if_icmpne -> 1984
    //   1981: iload #14
    //   1983: ireturn
    //   1984: invokestatic parseFailure : ()Lcom/android/framework/protobuf/InvalidProtocolBufferException;
    //   1987: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #4871	-> 0
    //   #4872	-> 9
    //   #4873	-> 9
    //   #4874	-> 9
    //   #4875	-> 12
    //   #4876	-> 15
    //   #4877	-> 30
    //   #4878	-> 37
    //   #4879	-> 49
    //   #4880	-> 54
    //   #4881	-> 66
    //   #4879	-> 76
    //   #4883	-> 76
    //   #4884	-> 82
    //   #4885	-> 89
    //   #4886	-> 96
    //   #4888	-> 111
    //   #4890	-> 119
    //   #4891	-> 119
    //   #4893	-> 125
    //   #4895	-> 143
    //   #4896	-> 154
    //   #4897	-> 161
    //   #4898	-> 168
    //   #4900	-> 175
    //   #4901	-> 186
    //   #4902	-> 195
    //   #4905	-> 202
    //   #4906	-> 208
    //   #4907	-> 213
    //   #4906	-> 226
    //   #4909	-> 226
    //   #4910	-> 226
    //   #4905	-> 243
    //   #4912	-> 243
    //   #5046	-> 335
    //   #5047	-> 341
    //   #5048	-> 341
    //   #5050	-> 341
    //   #5049	-> 349
    //   #5051	-> 369
    //   #5052	-> 377
    //   #5054	-> 393
    //   #5058	-> 393
    //   #5057	-> 410
    //   #5054	-> 419
    //   #5061	-> 429
    //   #5062	-> 436
    //   #5046	-> 451
    //   #5036	-> 454
    //   #5037	-> 459
    //   #5038	-> 469
    //   #5039	-> 476
    //   #5038	-> 483
    //   #5041	-> 493
    //   #5042	-> 500
    //   #5036	-> 515
    //   #5027	-> 518
    //   #5028	-> 523
    //   #5029	-> 533
    //   #5030	-> 540
    //   #5029	-> 547
    //   #5031	-> 557
    //   #5032	-> 564
    //   #5027	-> 579
    //   #5012	-> 582
    //   #5013	-> 587
    //   #5014	-> 597
    //   #5015	-> 604
    //   #5016	-> 612
    //   #5021	-> 632
    //   #5023	-> 647
    //   #5016	-> 666
    //   #5017	-> 666
    //   #5018	-> 676
    //   #5012	-> 702
    //   #5004	-> 705
    //   #5005	-> 711
    //   #5006	-> 721
    //   #5007	-> 734
    //   #5008	-> 741
    //   #5004	-> 760
    //   #4986	-> 763
    //   #4987	-> 769
    //   #4989	-> 769
    //   #4988	-> 777
    //   #4990	-> 791
    //   #4991	-> 799
    //   #4993	-> 815
    //   #4997	-> 815
    //   #4996	-> 832
    //   #4993	-> 841
    //   #4999	-> 851
    //   #5000	-> 858
    //   #4986	-> 873
    //   #4974	-> 876
    //   #4975	-> 882
    //   #4976	-> 890
    //   #4978	-> 903
    //   #4980	-> 913
    //   #4981	-> 926
    //   #4982	-> 933
    //   #4974	-> 948
    //   #4966	-> 951
    //   #4967	-> 956
    //   #4968	-> 966
    //   #4969	-> 993
    //   #4970	-> 1000
    //   #4966	-> 1023
    //   #4958	-> 1026
    //   #4959	-> 1032
    //   #4960	-> 1046
    //   #4961	-> 1049
    //   #4962	-> 1056
    //   #4958	-> 1071
    //   #4949	-> 1074
    //   #4950	-> 1080
    //   #4951	-> 1094
    //   #4952	-> 1097
    //   #4953	-> 1104
    //   #4949	-> 1119
    //   #4940	-> 1122
    //   #4941	-> 1127
    //   #4942	-> 1137
    //   #4943	-> 1150
    //   #4944	-> 1157
    //   #4931	-> 1172
    //   #4932	-> 1177
    //   #4933	-> 1187
    //   #4934	-> 1200
    //   #4935	-> 1207
    //   #4922	-> 1230
    //   #4923	-> 1236
    //   #4924	-> 1248
    //   #4925	-> 1251
    //   #4926	-> 1258
    //   #4914	-> 1273
    //   #4915	-> 1279
    //   #4916	-> 1291
    //   #4917	-> 1294
    //   #4918	-> 1301
    //   #5068	-> 1316
    //   #5070	-> 1342
    //   #5071	-> 1348
    //   #5072	-> 1361
    //   #5073	-> 1371
    //   #5074	-> 1380
    //   #5076	-> 1380
    //   #5075	-> 1398
    //   #5077	-> 1409
    //   #5072	-> 1422
    //   #5079	-> 1422
    //   #5081	-> 1422
    //   #5080	-> 1430
    //   #5082	-> 1448
    //   #5070	-> 1463
    //   #5084	-> 1466
    //   #5086	-> 1477
    //   #5087	-> 1477
    //   #5088	-> 1482
    //   #5101	-> 1518
    //   #5102	-> 1525
    //   #5104	-> 1552
    //   #5105	-> 1574
    //   #5106	-> 1580
    //   #5107	-> 1580
    //   #5108	-> 1598
    //   #5109	-> 1605
    //   #5111	-> 1636
    //   #5133	-> 1643
    //   #5113	-> 1662
    //   #5114	-> 1662
    //   #5115	-> 1662
    //   #5128	-> 1690
    //   #5129	-> 1697
    //   #5128	-> 1728
    //   #5133	-> 1732
    //   #5134	-> 1752
    //   #5137	-> 1757
    //   #5138	-> 1771
    //   #5139	-> 1779
    //   #5137	-> 1805
    //   #5144	-> 1805
    //   #5145	-> 1805
    //   #5144	-> 1811
    //   #5147	-> 1827
    //   #4877	-> 1846
    //   #5148	-> 1855
    //   #5149	-> 1863
    //   #5148	-> 1876
    //   #5151	-> 1876
    //   #5152	-> 1878
    //   #5153	-> 1893
    //   #5154	-> 1909
    //   #5152	-> 1924
    //   #5160	-> 1930
    //   #5161	-> 1934
    //   #5162	-> 1941
    //   #5164	-> 1948
    //   #5165	-> 1953
    //   #5166	-> 1963
    //   #5169	-> 1967
    //   #5173	-> 1981
    //   #5170	-> 1984
  }
  
  private int parseProto3Message(T paramT, byte[] paramArrayOfbyte, int paramInt1, int paramInt2, ArrayDecoders.Registers paramRegisters) throws IOException {
    // Byte code:
    //   0: getstatic com/android/framework/protobuf/MessageSchema.UNSAFE : Lsun/misc/Unsafe;
    //   3: astore #6
    //   5: iconst_0
    //   6: istore #7
    //   8: iconst_m1
    //   9: istore #8
    //   11: iload_3
    //   12: iload #4
    //   14: if_icmpge -> 1246
    //   17: iload_3
    //   18: iconst_1
    //   19: iadd
    //   20: istore #9
    //   22: aload_2
    //   23: iload_3
    //   24: baload
    //   25: istore #10
    //   27: iload #10
    //   29: ifge -> 54
    //   32: iload #10
    //   34: aload_2
    //   35: iload #9
    //   37: aload #5
    //   39: invokestatic decodeVarint32 : (I[BILcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   42: istore #9
    //   44: aload #5
    //   46: getfield int1 : I
    //   49: istore #10
    //   51: goto -> 54
    //   54: iload #10
    //   56: iconst_3
    //   57: iushr
    //   58: istore_3
    //   59: iload #10
    //   61: bipush #7
    //   63: iand
    //   64: istore #11
    //   66: iload_3
    //   67: iload #8
    //   69: if_icmple -> 86
    //   72: aload_0
    //   73: iload_3
    //   74: iload #7
    //   76: iconst_3
    //   77: idiv
    //   78: invokespecial positionForFieldNumber : (II)I
    //   81: istore #8
    //   83: goto -> 93
    //   86: aload_0
    //   87: iload_3
    //   88: invokespecial positionForFieldNumber : (I)I
    //   91: istore #8
    //   93: iload #8
    //   95: iconst_m1
    //   96: if_icmpne -> 109
    //   99: iconst_0
    //   100: istore #7
    //   102: iload #9
    //   104: istore #8
    //   106: goto -> 1215
    //   109: aload_0
    //   110: getfield buffer : [I
    //   113: iload #8
    //   115: iconst_1
    //   116: iadd
    //   117: iaload
    //   118: istore #12
    //   120: iload #12
    //   122: invokestatic type : (I)I
    //   125: istore #13
    //   127: iload #12
    //   129: invokestatic offset : (I)J
    //   132: lstore #14
    //   134: iload #13
    //   136: bipush #17
    //   138: if_icmpgt -> 904
    //   141: iconst_1
    //   142: istore #16
    //   144: iload #13
    //   146: tableswitch default -> 228, 0 -> 867, 1 -> 830, 2 -> 778, 3 -> 778, 4 -> 734, 5 -> 695, 6 -> 656, 7 -> 601, 8 -> 531, 9 -> 430, 10 -> 385, 11 -> 734, 12 -> 341, 13 -> 656, 14 -> 695, 15 -> 286, 16 -> 231
    //   228: goto -> 1157
    //   231: iload #11
    //   233: ifne -> 283
    //   236: aload_2
    //   237: iload #9
    //   239: aload #5
    //   241: invokestatic decodeVarint64 : ([BILcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   244: istore #9
    //   246: aload #5
    //   248: getfield long1 : J
    //   251: lstore #17
    //   253: lload #17
    //   255: invokestatic decodeZigZag64 : (J)J
    //   258: lstore #17
    //   260: aload #6
    //   262: aload_1
    //   263: lload #14
    //   265: lload #17
    //   267: invokevirtual putLong : (Ljava/lang/Object;JJ)V
    //   270: iload #8
    //   272: istore #7
    //   274: iload_3
    //   275: istore #8
    //   277: iload #9
    //   279: istore_3
    //   280: goto -> 11
    //   283: goto -> 1157
    //   286: iload #11
    //   288: ifne -> 338
    //   291: aload_2
    //   292: iload #9
    //   294: aload #5
    //   296: invokestatic decodeVarint32 : ([BILcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   299: istore #9
    //   301: aload #5
    //   303: getfield int1 : I
    //   306: istore #7
    //   308: iload #7
    //   310: invokestatic decodeZigZag32 : (I)I
    //   313: istore #7
    //   315: aload #6
    //   317: aload_1
    //   318: lload #14
    //   320: iload #7
    //   322: invokevirtual putInt : (Ljava/lang/Object;JI)V
    //   325: iload #8
    //   327: istore #7
    //   329: iload_3
    //   330: istore #8
    //   332: iload #9
    //   334: istore_3
    //   335: goto -> 11
    //   338: goto -> 1157
    //   341: iload #11
    //   343: ifne -> 382
    //   346: aload_2
    //   347: iload #9
    //   349: aload #5
    //   351: invokestatic decodeVarint32 : ([BILcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   354: istore #9
    //   356: aload #6
    //   358: aload_1
    //   359: lload #14
    //   361: aload #5
    //   363: getfield int1 : I
    //   366: invokevirtual putInt : (Ljava/lang/Object;JI)V
    //   369: iload #8
    //   371: istore #7
    //   373: iload_3
    //   374: istore #8
    //   376: iload #9
    //   378: istore_3
    //   379: goto -> 11
    //   382: goto -> 1157
    //   385: iload #11
    //   387: iconst_2
    //   388: if_icmpne -> 427
    //   391: aload_2
    //   392: iload #9
    //   394: aload #5
    //   396: invokestatic decodeBytes : ([BILcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   399: istore #9
    //   401: aload #6
    //   403: aload_1
    //   404: lload #14
    //   406: aload #5
    //   408: getfield object1 : Ljava/lang/Object;
    //   411: invokevirtual putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   414: iload #8
    //   416: istore #7
    //   418: iload_3
    //   419: istore #8
    //   421: iload #9
    //   423: istore_3
    //   424: goto -> 11
    //   427: goto -> 1157
    //   430: iload #11
    //   432: iconst_2
    //   433: if_icmpne -> 528
    //   436: aload_0
    //   437: iload #8
    //   439: invokespecial getMessageFieldSchema : (I)Lcom/android/framework/protobuf/Schema;
    //   442: astore #19
    //   444: aload #19
    //   446: aload_2
    //   447: iload #9
    //   449: iload #4
    //   451: aload #5
    //   453: invokestatic decodeMessageField : (Lcom/android/framework/protobuf/Schema;[BIILcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   456: istore #9
    //   458: aload #6
    //   460: aload_1
    //   461: lload #14
    //   463: invokevirtual getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   466: astore #19
    //   468: aload #19
    //   470: ifnonnull -> 489
    //   473: aload #6
    //   475: aload_1
    //   476: lload #14
    //   478: aload #5
    //   480: getfield object1 : Ljava/lang/Object;
    //   483: invokevirtual putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   486: goto -> 515
    //   489: aload #5
    //   491: getfield object1 : Ljava/lang/Object;
    //   494: astore #20
    //   496: aload #19
    //   498: aload #20
    //   500: invokestatic mergeMessage : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   503: astore #19
    //   505: aload #6
    //   507: aload_1
    //   508: lload #14
    //   510: aload #19
    //   512: invokevirtual putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   515: iload #8
    //   517: istore #7
    //   519: iload_3
    //   520: istore #8
    //   522: iload #9
    //   524: istore_3
    //   525: goto -> 11
    //   528: goto -> 1157
    //   531: iload #11
    //   533: iconst_2
    //   534: if_icmpne -> 598
    //   537: ldc 536870912
    //   539: iload #12
    //   541: iand
    //   542: ifne -> 558
    //   545: aload_2
    //   546: iload #9
    //   548: aload #5
    //   550: invokestatic decodeString : ([BILcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   553: istore #7
    //   555: goto -> 568
    //   558: aload_2
    //   559: iload #9
    //   561: aload #5
    //   563: invokestatic decodeStringRequireUtf8 : ([BILcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   566: istore #7
    //   568: aload #6
    //   570: aload_1
    //   571: lload #14
    //   573: aload #5
    //   575: getfield object1 : Ljava/lang/Object;
    //   578: invokevirtual putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   581: iload #8
    //   583: istore #9
    //   585: iload_3
    //   586: istore #8
    //   588: iload #7
    //   590: istore_3
    //   591: iload #9
    //   593: istore #7
    //   595: goto -> 11
    //   598: goto -> 1157
    //   601: iload #11
    //   603: ifne -> 653
    //   606: aload_2
    //   607: iload #9
    //   609: aload #5
    //   611: invokestatic decodeVarint64 : ([BILcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   614: istore #9
    //   616: aload #5
    //   618: getfield long1 : J
    //   621: lconst_0
    //   622: lcmp
    //   623: ifeq -> 629
    //   626: goto -> 632
    //   629: iconst_0
    //   630: istore #16
    //   632: aload_1
    //   633: lload #14
    //   635: iload #16
    //   637: invokestatic putBoolean : (Ljava/lang/Object;JZ)V
    //   640: iload #8
    //   642: istore #7
    //   644: iload_3
    //   645: istore #8
    //   647: iload #9
    //   649: istore_3
    //   650: goto -> 11
    //   653: goto -> 1157
    //   656: iload #11
    //   658: iconst_5
    //   659: if_icmpne -> 692
    //   662: aload #6
    //   664: aload_1
    //   665: lload #14
    //   667: aload_2
    //   668: iload #9
    //   670: invokestatic decodeFixed32 : ([BI)I
    //   673: invokevirtual putInt : (Ljava/lang/Object;JI)V
    //   676: iinc #9, 4
    //   679: iload #8
    //   681: istore #7
    //   683: iload_3
    //   684: istore #8
    //   686: iload #9
    //   688: istore_3
    //   689: goto -> 11
    //   692: goto -> 1157
    //   695: iload #11
    //   697: iconst_1
    //   698: if_icmpne -> 731
    //   701: aload #6
    //   703: aload_1
    //   704: lload #14
    //   706: aload_2
    //   707: iload #9
    //   709: invokestatic decodeFixed64 : ([BI)J
    //   712: invokevirtual putLong : (Ljava/lang/Object;JJ)V
    //   715: iinc #9, 8
    //   718: iload #8
    //   720: istore #7
    //   722: iload_3
    //   723: istore #8
    //   725: iload #9
    //   727: istore_3
    //   728: goto -> 11
    //   731: goto -> 1157
    //   734: iload #11
    //   736: ifne -> 775
    //   739: aload_2
    //   740: iload #9
    //   742: aload #5
    //   744: invokestatic decodeVarint32 : ([BILcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   747: istore #9
    //   749: aload #6
    //   751: aload_1
    //   752: lload #14
    //   754: aload #5
    //   756: getfield int1 : I
    //   759: invokevirtual putInt : (Ljava/lang/Object;JI)V
    //   762: iload #8
    //   764: istore #7
    //   766: iload_3
    //   767: istore #8
    //   769: iload #9
    //   771: istore_3
    //   772: goto -> 11
    //   775: goto -> 1157
    //   778: iload #11
    //   780: ifne -> 827
    //   783: aload_2
    //   784: iload #9
    //   786: aload #5
    //   788: invokestatic decodeVarint64 : ([BILcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   791: istore #9
    //   793: aload #6
    //   795: aload_1
    //   796: lload #14
    //   798: aload #5
    //   800: getfield long1 : J
    //   803: invokevirtual putLong : (Ljava/lang/Object;JJ)V
    //   806: iload #8
    //   808: istore #7
    //   810: iload #9
    //   812: istore #8
    //   814: iload_3
    //   815: istore #9
    //   817: iload #8
    //   819: istore_3
    //   820: iload #9
    //   822: istore #8
    //   824: goto -> 11
    //   827: goto -> 1157
    //   830: iload #11
    //   832: iconst_5
    //   833: if_icmpne -> 864
    //   836: aload_1
    //   837: lload #14
    //   839: aload_2
    //   840: iload #9
    //   842: invokestatic decodeFloat : ([BI)F
    //   845: invokestatic putFloat : (Ljava/lang/Object;JF)V
    //   848: iinc #9, 4
    //   851: iload #8
    //   853: istore #7
    //   855: iload_3
    //   856: istore #8
    //   858: iload #9
    //   860: istore_3
    //   861: goto -> 11
    //   864: goto -> 1157
    //   867: iload #11
    //   869: iconst_1
    //   870: if_icmpne -> 901
    //   873: aload_1
    //   874: lload #14
    //   876: aload_2
    //   877: iload #9
    //   879: invokestatic decodeDouble : ([BI)D
    //   882: invokestatic putDouble : (Ljava/lang/Object;JD)V
    //   885: iinc #9, 8
    //   888: iload #8
    //   890: istore #7
    //   892: iload_3
    //   893: istore #8
    //   895: iload #9
    //   897: istore_3
    //   898: goto -> 11
    //   901: goto -> 1157
    //   904: iload #13
    //   906: bipush #27
    //   908: if_icmpne -> 1037
    //   911: iload #11
    //   913: iconst_2
    //   914: if_icmpne -> 1034
    //   917: aload #6
    //   919: aload_1
    //   920: lload #14
    //   922: invokevirtual getObject : (Ljava/lang/Object;J)Ljava/lang/Object;
    //   925: checkcast com/android/framework/protobuf/Internal$ProtobufList
    //   928: astore #19
    //   930: aload #19
    //   932: invokeinterface isModifiable : ()Z
    //   937: ifne -> 991
    //   940: aload #19
    //   942: invokeinterface size : ()I
    //   947: istore #7
    //   949: iload #7
    //   951: ifne -> 961
    //   954: bipush #10
    //   956: istore #7
    //   958: goto -> 967
    //   961: iload #7
    //   963: iconst_2
    //   964: imul
    //   965: istore #7
    //   967: aload #19
    //   969: iload #7
    //   971: invokeinterface mutableCopyWithCapacity : (I)Lcom/android/framework/protobuf/Internal$ProtobufList;
    //   976: astore #19
    //   978: aload #6
    //   980: aload_1
    //   981: lload #14
    //   983: aload #19
    //   985: invokevirtual putObject : (Ljava/lang/Object;JLjava/lang/Object;)V
    //   988: goto -> 991
    //   991: aload_0
    //   992: iload #8
    //   994: invokespecial getMessageFieldSchema : (I)Lcom/android/framework/protobuf/Schema;
    //   997: astore #20
    //   999: aload #20
    //   1001: iload #10
    //   1003: aload_2
    //   1004: iload #9
    //   1006: iload #4
    //   1008: aload #19
    //   1010: aload #5
    //   1012: invokestatic decodeMessageList : (Lcom/android/framework/protobuf/Schema;I[BIILcom/android/framework/protobuf/Internal$ProtobufList;Lcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   1015: istore #10
    //   1017: iload_3
    //   1018: istore #9
    //   1020: iload #8
    //   1022: istore #7
    //   1024: iload #10
    //   1026: istore_3
    //   1027: iload #9
    //   1029: istore #8
    //   1031: goto -> 11
    //   1034: goto -> 1157
    //   1037: iload #8
    //   1039: istore #7
    //   1041: iload #13
    //   1043: bipush #49
    //   1045: if_icmpgt -> 1103
    //   1048: iload #12
    //   1050: i2l
    //   1051: lstore #17
    //   1053: aload_0
    //   1054: aload_1
    //   1055: aload_2
    //   1056: iload #9
    //   1058: iload #4
    //   1060: iload #10
    //   1062: iload_3
    //   1063: iload #11
    //   1065: iload #7
    //   1067: lload #17
    //   1069: iload #13
    //   1071: lload #14
    //   1073: aload #5
    //   1075: invokespecial parseRepeatedField : (Ljava/lang/Object;[BIIIIIIJIJLcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   1078: istore #8
    //   1080: iload #8
    //   1082: iload #9
    //   1084: if_icmpeq -> 1100
    //   1087: iload_3
    //   1088: istore #9
    //   1090: iload #8
    //   1092: istore_3
    //   1093: iload #9
    //   1095: istore #8
    //   1097: goto -> 11
    //   1100: goto -> 1215
    //   1103: iload #13
    //   1105: bipush #50
    //   1107: if_icmpne -> 1168
    //   1110: iload #11
    //   1112: iconst_2
    //   1113: if_icmpne -> 1157
    //   1116: aload_0
    //   1117: aload_1
    //   1118: aload_2
    //   1119: iload #9
    //   1121: iload #4
    //   1123: iload #7
    //   1125: lload #14
    //   1127: aload #5
    //   1129: invokespecial parseMapField : (Ljava/lang/Object;[BIIIJLcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   1132: istore #8
    //   1134: iload #8
    //   1136: iload #9
    //   1138: if_icmpeq -> 1154
    //   1141: iload_3
    //   1142: istore #9
    //   1144: iload #8
    //   1146: istore_3
    //   1147: iload #9
    //   1149: istore #8
    //   1151: goto -> 11
    //   1154: goto -> 1215
    //   1157: iload #8
    //   1159: istore #7
    //   1161: iload #9
    //   1163: istore #8
    //   1165: goto -> 1215
    //   1168: aload_0
    //   1169: aload_1
    //   1170: aload_2
    //   1171: iload #9
    //   1173: iload #4
    //   1175: iload #10
    //   1177: iload_3
    //   1178: iload #11
    //   1180: iload #12
    //   1182: iload #13
    //   1184: lload #14
    //   1186: iload #7
    //   1188: aload #5
    //   1190: invokespecial parseOneofField : (Ljava/lang/Object;[BIIIIIIIJILcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   1193: istore #8
    //   1195: iload #8
    //   1197: iload #9
    //   1199: if_icmpeq -> 1215
    //   1202: iload_3
    //   1203: istore #9
    //   1205: iload #8
    //   1207: istore_3
    //   1208: iload #9
    //   1210: istore #8
    //   1212: goto -> 11
    //   1215: aload_1
    //   1216: invokestatic getMutableUnknownFields : (Ljava/lang/Object;)Lcom/android/framework/protobuf/UnknownFieldSetLite;
    //   1219: astore #19
    //   1221: iload #10
    //   1223: aload_2
    //   1224: iload #8
    //   1226: iload #4
    //   1228: aload #19
    //   1230: aload #5
    //   1232: invokestatic decodeUnknownField : (I[BIILcom/android/framework/protobuf/UnknownFieldSetLite;Lcom/android/framework/protobuf/ArrayDecoders$Registers;)I
    //   1235: istore #9
    //   1237: iload_3
    //   1238: istore #8
    //   1240: iload #9
    //   1242: istore_3
    //   1243: goto -> 11
    //   1246: iload_3
    //   1247: iload #4
    //   1249: if_icmpne -> 1254
    //   1252: iload_3
    //   1253: ireturn
    //   1254: invokestatic parseFailure : ()Lcom/android/framework/protobuf/InvalidProtocolBufferException;
    //   1257: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #5179	-> 0
    //   #5180	-> 5
    //   #5181	-> 5
    //   #5182	-> 5
    //   #5183	-> 11
    //   #5184	-> 17
    //   #5185	-> 27
    //   #5186	-> 32
    //   #5187	-> 44
    //   #5185	-> 54
    //   #5189	-> 54
    //   #5190	-> 59
    //   #5191	-> 66
    //   #5192	-> 72
    //   #5194	-> 86
    //   #5196	-> 93
    //   #5197	-> 93
    //   #5199	-> 99
    //   #5201	-> 109
    //   #5202	-> 120
    //   #5203	-> 127
    //   #5204	-> 134
    //   #5205	-> 141
    //   #5316	-> 228
    //   #5308	-> 231
    //   #5309	-> 236
    //   #5310	-> 246
    //   #5311	-> 253
    //   #5310	-> 260
    //   #5312	-> 270
    //   #5308	-> 283
    //   #5300	-> 286
    //   #5301	-> 291
    //   #5302	-> 301
    //   #5303	-> 308
    //   #5302	-> 315
    //   #5304	-> 325
    //   #5300	-> 338
    //   #5293	-> 341
    //   #5294	-> 346
    //   #5295	-> 356
    //   #5296	-> 369
    //   #5293	-> 382
    //   #5286	-> 385
    //   #5287	-> 391
    //   #5288	-> 401
    //   #5289	-> 414
    //   #5286	-> 427
    //   #5271	-> 430
    //   #5272	-> 436
    //   #5274	-> 436
    //   #5273	-> 444
    //   #5275	-> 458
    //   #5276	-> 468
    //   #5277	-> 473
    //   #5279	-> 489
    //   #5280	-> 496
    //   #5279	-> 505
    //   #5282	-> 515
    //   #5183	-> 515
    //   #5271	-> 528
    //   #5260	-> 531
    //   #5261	-> 537
    //   #5262	-> 545
    //   #5264	-> 558
    //   #5266	-> 568
    //   #5267	-> 581
    //   #5260	-> 598
    //   #5253	-> 601
    //   #5254	-> 606
    //   #5255	-> 616
    //   #5256	-> 640
    //   #5253	-> 653
    //   #5246	-> 656
    //   #5247	-> 662
    //   #5248	-> 676
    //   #5249	-> 679
    //   #5246	-> 692
    //   #5238	-> 695
    //   #5239	-> 701
    //   #5240	-> 715
    //   #5241	-> 718
    //   #5238	-> 731
    //   #5230	-> 734
    //   #5231	-> 739
    //   #5232	-> 749
    //   #5233	-> 762
    //   #5230	-> 775
    //   #5222	-> 778
    //   #5223	-> 783
    //   #5224	-> 793
    //   #5225	-> 806
    //   #5222	-> 827
    //   #5214	-> 830
    //   #5215	-> 836
    //   #5216	-> 848
    //   #5217	-> 851
    //   #5214	-> 864
    //   #5207	-> 867
    //   #5208	-> 873
    //   #5209	-> 885
    //   #5210	-> 888
    //   #5207	-> 901
    //   #5318	-> 904
    //   #5320	-> 911
    //   #5321	-> 917
    //   #5322	-> 930
    //   #5323	-> 940
    //   #5324	-> 949
    //   #5326	-> 949
    //   #5325	-> 967
    //   #5327	-> 978
    //   #5322	-> 991
    //   #5329	-> 991
    //   #5331	-> 991
    //   #5330	-> 999
    //   #5332	-> 1017
    //   #5320	-> 1034
    //   #5334	-> 1037
    //   #5336	-> 1048
    //   #5337	-> 1048
    //   #5338	-> 1053
    //   #5351	-> 1080
    //   #5352	-> 1087
    //   #5354	-> 1100
    //   #5355	-> 1110
    //   #5356	-> 1116
    //   #5357	-> 1116
    //   #5358	-> 1134
    //   #5359	-> 1141
    //   #5361	-> 1154
    //   #5383	-> 1157
    //   #5363	-> 1168
    //   #5364	-> 1168
    //   #5365	-> 1168
    //   #5378	-> 1195
    //   #5379	-> 1202
    //   #5378	-> 1215
    //   #5383	-> 1215
    //   #5384	-> 1215
    //   #5383	-> 1221
    //   #5385	-> 1237
    //   #5386	-> 1246
    //   #5389	-> 1252
    //   #5387	-> 1254
  }
  
  public void mergeFrom(T paramT, byte[] paramArrayOfbyte, int paramInt1, int paramInt2, ArrayDecoders.Registers paramRegisters) throws IOException {
    if (this.proto3) {
      parseProto3Message(paramT, paramArrayOfbyte, paramInt1, paramInt2, paramRegisters);
    } else {
      parseProto2Message(paramT, paramArrayOfbyte, paramInt1, paramInt2, 0, paramRegisters);
    } 
  }
  
  public void makeImmutable(T paramT) {
    int i;
    for (i = this.checkInitializedCount; i < this.repeatedFieldOffsetStart; i++) {
      long l = offset(typeAndOffsetAt(this.intArray[i]));
      Object object = UnsafeUtil.getObject(paramT, l);
      if (object != null)
        UnsafeUtil.putObject(paramT, l, this.mapFieldSchema.toImmutable(object)); 
    } 
    int j = this.intArray.length;
    for (i = this.repeatedFieldOffsetStart; i < j; i++)
      this.listFieldSchema.makeImmutableListAt(paramT, this.intArray[i]); 
    this.unknownFieldSchema.makeImmutable(paramT);
    if (this.hasExtensions)
      this.extensionSchema.makeImmutable(paramT); 
  }
  
  private final <K, V> void mergeMap(Object<?, ?> paramObject1, int paramInt, Object<?, ?> paramObject2, ExtensionRegistryLite paramExtensionRegistryLite, Reader paramReader) throws IOException {
    long l = offset(typeAndOffsetAt(paramInt));
    Object object1 = UnsafeUtil.getObject(paramObject1, l);
    if (object1 == null) {
      object2 = this.mapFieldSchema.newMapField(paramObject2);
      UnsafeUtil.putObject(paramObject1, l, object2);
    } else {
      object2 = object1;
      if (this.mapFieldSchema.isImmutable(object1)) {
        object2 = this.mapFieldSchema.newMapField(paramObject2);
        this.mapFieldSchema.mergeFrom(object2, object1);
        UnsafeUtil.putObject(paramObject1, l, object2);
      } 
    } 
    paramObject1 = (Object<?, ?>)this.mapFieldSchema;
    paramObject1 = (Object<?, ?>)paramObject1.forMutableMapData(object2);
    Object object2 = this.mapFieldSchema;
    paramObject2 = (Object<?, ?>)object2.forMapMetadata(paramObject2);
    paramReader.readMap((Map<?, ?>)paramObject1, (MapEntryLite.Metadata<?, ?>)paramObject2, paramExtensionRegistryLite);
  }
  
  private final <UT, UB> UB filterMapUnknownEnumValues(Object paramObject, int paramInt, UB paramUB, UnknownFieldSchema<UT, UB> paramUnknownFieldSchema) {
    int i = numberAt(paramInt);
    long l = offset(typeAndOffsetAt(paramInt));
    Object<?, ?> object = (Object<?, ?>)UnsafeUtil.getObject(paramObject, l);
    if (object == null)
      return paramUB; 
    paramObject = getEnumFieldVerifier(paramInt);
    if (paramObject == null)
      return paramUB; 
    object = (Object<?, ?>)this.mapFieldSchema.forMutableMapData(object);
    paramObject = filterUnknownEnumMap(paramInt, i, (Map<?, ?>)object, (Internal.EnumVerifier)paramObject, paramUB, paramUnknownFieldSchema);
    return (UB)paramObject;
  }
  
  private final <K, V, UT, UB> UB filterUnknownEnumMap(int paramInt1, int paramInt2, Map<K, V> paramMap, Internal.EnumVerifier paramEnumVerifier, UB paramUB, UnknownFieldSchema<UT, UB> paramUnknownFieldSchema) {
    IOException iOException;
    MapFieldSchema mapFieldSchema = this.mapFieldSchema;
    MapEntryLite.Metadata<?, ?> metadata = mapFieldSchema.forMapMetadata(getMapFieldDefaultEntry(paramInt1));
    for (Iterator<Map.Entry> iterator = paramMap.entrySet().iterator(); iterator.hasNext(); ) {
      Map.Entry entry = iterator.next();
      UB uB = paramUB;
      if (!paramEnumVerifier.isInRange(((Integer)entry.getValue()).intValue())) {
        uB = paramUB;
        if (paramUB == null)
          uB = paramUnknownFieldSchema.newBuilder(); 
        paramInt1 = MapEntryLite.computeSerializedSize(metadata, entry.getKey(), entry.getValue());
        ByteString.CodedBuilder codedBuilder = ByteString.newCodedBuilder(paramInt1);
        CodedOutputStream codedOutputStream = codedBuilder.getCodedOutput();
        try {
          MapEntryLite.writeTo(codedOutputStream, metadata, entry.getKey(), entry.getValue());
          paramUnknownFieldSchema.addLengthDelimited(uB, paramInt2, codedBuilder.build());
          iterator.remove();
        } catch (IOException iOException1) {
          throw new RuntimeException(iOException1);
        } 
      } 
      iOException = iOException1;
    } 
    return (UB)iOException;
  }
  
  public final boolean isInitialized(T paramT) {
    // Byte code:
    //   0: iconst_m1
    //   1: istore_2
    //   2: iconst_0
    //   3: istore_3
    //   4: iconst_0
    //   5: istore #4
    //   7: iload #4
    //   9: aload_0
    //   10: getfield checkInitializedCount : I
    //   13: if_icmpge -> 295
    //   16: aload_0
    //   17: getfield intArray : [I
    //   20: iload #4
    //   22: iaload
    //   23: istore #5
    //   25: aload_0
    //   26: iload #5
    //   28: invokespecial numberAt : (I)I
    //   31: istore #6
    //   33: aload_0
    //   34: iload #5
    //   36: invokespecial typeAndOffsetAt : (I)I
    //   39: istore #7
    //   41: iconst_0
    //   42: istore #8
    //   44: iload_2
    //   45: istore #9
    //   47: iload_3
    //   48: istore #10
    //   50: aload_0
    //   51: getfield proto3 : Z
    //   54: ifne -> 120
    //   57: aload_0
    //   58: getfield buffer : [I
    //   61: iload #5
    //   63: iconst_2
    //   64: iadd
    //   65: iaload
    //   66: istore #10
    //   68: ldc 1048575
    //   70: iload #10
    //   72: iand
    //   73: istore #11
    //   75: iconst_1
    //   76: iload #10
    //   78: bipush #20
    //   80: iushr
    //   81: ishl
    //   82: istore #12
    //   84: iload_2
    //   85: istore #9
    //   87: iload_3
    //   88: istore #10
    //   90: iload #12
    //   92: istore #8
    //   94: iload #11
    //   96: iload_2
    //   97: if_icmpeq -> 120
    //   100: iload #11
    //   102: istore #9
    //   104: getstatic com/android/framework/protobuf/MessageSchema.UNSAFE : Lsun/misc/Unsafe;
    //   107: aload_1
    //   108: iload #11
    //   110: i2l
    //   111: invokevirtual getInt : (Ljava/lang/Object;J)I
    //   114: istore #10
    //   116: iload #12
    //   118: istore #8
    //   120: iload #7
    //   122: invokestatic isRequired : (I)Z
    //   125: ifeq -> 144
    //   128: aload_0
    //   129: aload_1
    //   130: iload #5
    //   132: iload #10
    //   134: iload #8
    //   136: invokespecial isFieldPresent : (Ljava/lang/Object;III)Z
    //   139: ifne -> 144
    //   142: iconst_0
    //   143: ireturn
    //   144: iload #7
    //   146: invokestatic type : (I)I
    //   149: istore_3
    //   150: iload_3
    //   151: bipush #9
    //   153: if_icmpeq -> 252
    //   156: iload_3
    //   157: bipush #17
    //   159: if_icmpeq -> 252
    //   162: iload_3
    //   163: bipush #27
    //   165: if_icmpeq -> 238
    //   168: iload_3
    //   169: bipush #60
    //   171: if_icmpeq -> 209
    //   174: iload_3
    //   175: bipush #68
    //   177: if_icmpeq -> 209
    //   180: iload_3
    //   181: bipush #49
    //   183: if_icmpeq -> 238
    //   186: iload_3
    //   187: bipush #50
    //   189: if_icmpeq -> 195
    //   192: goto -> 283
    //   195: aload_0
    //   196: aload_1
    //   197: iload #7
    //   199: iload #5
    //   201: invokespecial isMapInitialized : (Ljava/lang/Object;II)Z
    //   204: ifne -> 283
    //   207: iconst_0
    //   208: ireturn
    //   209: aload_0
    //   210: aload_1
    //   211: iload #6
    //   213: iload #5
    //   215: invokespecial isOneofPresent : (Ljava/lang/Object;II)Z
    //   218: ifeq -> 283
    //   221: aload_1
    //   222: iload #7
    //   224: aload_0
    //   225: iload #5
    //   227: invokespecial getMessageFieldSchema : (I)Lcom/android/framework/protobuf/Schema;
    //   230: invokestatic isInitialized : (Ljava/lang/Object;ILcom/android/framework/protobuf/Schema;)Z
    //   233: ifne -> 283
    //   236: iconst_0
    //   237: ireturn
    //   238: aload_0
    //   239: aload_1
    //   240: iload #7
    //   242: iload #5
    //   244: invokespecial isListInitialized : (Ljava/lang/Object;II)Z
    //   247: ifne -> 283
    //   250: iconst_0
    //   251: ireturn
    //   252: aload_0
    //   253: aload_1
    //   254: iload #5
    //   256: iload #10
    //   258: iload #8
    //   260: invokespecial isFieldPresent : (Ljava/lang/Object;III)Z
    //   263: ifeq -> 283
    //   266: aload_1
    //   267: iload #7
    //   269: aload_0
    //   270: iload #5
    //   272: invokespecial getMessageFieldSchema : (I)Lcom/android/framework/protobuf/Schema;
    //   275: invokestatic isInitialized : (Ljava/lang/Object;ILcom/android/framework/protobuf/Schema;)Z
    //   278: ifne -> 283
    //   281: iconst_0
    //   282: ireturn
    //   283: iinc #4, 1
    //   286: iload #9
    //   288: istore_2
    //   289: iload #10
    //   291: istore_3
    //   292: goto -> 7
    //   295: aload_0
    //   296: getfield hasExtensions : Z
    //   299: ifeq -> 318
    //   302: aload_0
    //   303: getfield extensionSchema : Lcom/android/framework/protobuf/ExtensionSchema;
    //   306: aload_1
    //   307: invokevirtual getExtensions : (Ljava/lang/Object;)Lcom/android/framework/protobuf/FieldSet;
    //   310: invokevirtual isInitialized : ()Z
    //   313: ifne -> 318
    //   316: iconst_0
    //   317: ireturn
    //   318: iconst_1
    //   319: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #5507	-> 0
    //   #5508	-> 2
    //   #5509	-> 4
    //   #5510	-> 16
    //   #5511	-> 25
    //   #5513	-> 33
    //   #5515	-> 41
    //   #5516	-> 41
    //   #5517	-> 44
    //   #5518	-> 57
    //   #5519	-> 68
    //   #5520	-> 75
    //   #5521	-> 84
    //   #5522	-> 100
    //   #5523	-> 104
    //   #5527	-> 120
    //   #5528	-> 128
    //   #5529	-> 142
    //   #5536	-> 144
    //   #5558	-> 195
    //   #5559	-> 207
    //   #5552	-> 209
    //   #5553	-> 221
    //   #5554	-> 236
    //   #5546	-> 238
    //   #5547	-> 250
    //   #5539	-> 252
    //   #5540	-> 266
    //   #5541	-> 281
    //   #5509	-> 283
    //   #5567	-> 295
    //   #5568	-> 302
    //   #5569	-> 316
    //   #5573	-> 318
  }
  
  private static boolean isInitialized(Object paramObject, int paramInt, Schema<Object> paramSchema) {
    paramObject = UnsafeUtil.getObject(paramObject, offset(paramInt));
    return paramSchema.isInitialized(paramObject);
  }
  
  private <N> boolean isListInitialized(Object paramObject, int paramInt1, int paramInt2) {
    paramObject = UnsafeUtil.getObject(paramObject, offset(paramInt1));
    if (paramObject.isEmpty())
      return true; 
    Schema schema = getMessageFieldSchema(paramInt2);
    for (paramInt1 = 0; paramInt1 < paramObject.size(); paramInt1++) {
      Object object = paramObject.get(paramInt1);
      if (!schema.isInitialized(object))
        return false; 
    } 
    return true;
  }
  
  private boolean isMapInitialized(T paramT, int paramInt1, int paramInt2) {
    Map<?, ?> map = this.mapFieldSchema.forMapData(UnsafeUtil.getObject(paramT, offset(paramInt1)));
    if (map.isEmpty())
      return true; 
    Object<?, ?> object = (Object<?, ?>)getMapFieldDefaultEntry(paramInt2);
    object = (Object<?, ?>)this.mapFieldSchema.forMapMetadata(object);
    if (((MapEntryLite.Metadata)object).valueType.getJavaType() != WireFormat.JavaType.MESSAGE)
      return true; 
    object = null;
    for (Object object2 : map.values()) {
      Object<?, ?> object1 = object;
      if (object == null)
        object1 = Protobuf.getInstance().schemaFor(object2.getClass()); 
      if (!object1.isInitialized(object2))
        return false; 
      object = object1;
    } 
    return true;
  }
  
  private void writeString(int paramInt, Object paramObject, Writer paramWriter) throws IOException {
    if (paramObject instanceof String) {
      paramWriter.writeString(paramInt, (String)paramObject);
    } else {
      paramWriter.writeBytes(paramInt, (ByteString)paramObject);
    } 
  }
  
  private void readString(Object paramObject, int paramInt, Reader paramReader) throws IOException {
    if (isEnforceUtf8(paramInt)) {
      UnsafeUtil.putObject(paramObject, offset(paramInt), paramReader.readStringRequireUtf8());
    } else if (this.lite) {
      UnsafeUtil.putObject(paramObject, offset(paramInt), paramReader.readString());
    } else {
      UnsafeUtil.putObject(paramObject, offset(paramInt), paramReader.readBytes());
    } 
  }
  
  private void readStringList(Object<?> paramObject, int paramInt, Reader paramReader) throws IOException {
    if (isEnforceUtf8(paramInt)) {
      ListFieldSchema listFieldSchema = this.listFieldSchema;
      paramObject = listFieldSchema.mutableListAt(paramObject, offset(paramInt));
      paramReader.readStringListRequireUtf8((List)paramObject);
    } else {
      paramReader.readStringList(this.listFieldSchema.mutableListAt(paramObject, offset(paramInt)));
    } 
  }
  
  private <E> void readMessageList(Object<?> paramObject, int paramInt, Reader paramReader, Schema<E> paramSchema, ExtensionRegistryLite paramExtensionRegistryLite) throws IOException {
    long l = offset(paramInt);
    ListFieldSchema listFieldSchema = this.listFieldSchema;
    paramObject = listFieldSchema.mutableListAt(paramObject, l);
    paramReader.readMessageList((List<?>)paramObject, paramSchema, paramExtensionRegistryLite);
  }
  
  private <E> void readGroupList(Object<?> paramObject, long paramLong, Reader paramReader, Schema<E> paramSchema, ExtensionRegistryLite paramExtensionRegistryLite) throws IOException {
    ListFieldSchema listFieldSchema = this.listFieldSchema;
    paramObject = listFieldSchema.mutableListAt(paramObject, paramLong);
    paramReader.readGroupList((List<?>)paramObject, paramSchema, paramExtensionRegistryLite);
  }
  
  private int numberAt(int paramInt) {
    return this.buffer[paramInt];
  }
  
  private int typeAndOffsetAt(int paramInt) {
    return this.buffer[paramInt + 1];
  }
  
  private int presenceMaskAndOffsetAt(int paramInt) {
    return this.buffer[paramInt + 2];
  }
  
  private static int type(int paramInt) {
    return (0xFF00000 & paramInt) >>> 20;
  }
  
  private static boolean isRequired(int paramInt) {
    boolean bool;
    if ((0x10000000 & paramInt) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static boolean isEnforceUtf8(int paramInt) {
    boolean bool;
    if ((0x20000000 & paramInt) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static long offset(int paramInt) {
    return (0xFFFFF & paramInt);
  }
  
  private static <T> double doubleAt(T paramT, long paramLong) {
    return UnsafeUtil.getDouble(paramT, paramLong);
  }
  
  private static <T> float floatAt(T paramT, long paramLong) {
    return UnsafeUtil.getFloat(paramT, paramLong);
  }
  
  private static <T> int intAt(T paramT, long paramLong) {
    return UnsafeUtil.getInt(paramT, paramLong);
  }
  
  private static <T> long longAt(T paramT, long paramLong) {
    return UnsafeUtil.getLong(paramT, paramLong);
  }
  
  private static <T> boolean booleanAt(T paramT, long paramLong) {
    return UnsafeUtil.getBoolean(paramT, paramLong);
  }
  
  private static <T> double oneofDoubleAt(T paramT, long paramLong) {
    return ((Double)UnsafeUtil.getObject(paramT, paramLong)).doubleValue();
  }
  
  private static <T> float oneofFloatAt(T paramT, long paramLong) {
    return ((Float)UnsafeUtil.getObject(paramT, paramLong)).floatValue();
  }
  
  private static <T> int oneofIntAt(T paramT, long paramLong) {
    return ((Integer)UnsafeUtil.getObject(paramT, paramLong)).intValue();
  }
  
  private static <T> long oneofLongAt(T paramT, long paramLong) {
    return ((Long)UnsafeUtil.getObject(paramT, paramLong)).longValue();
  }
  
  private static <T> boolean oneofBooleanAt(T paramT, long paramLong) {
    return ((Boolean)UnsafeUtil.getObject(paramT, paramLong)).booleanValue();
  }
  
  private boolean arePresentForEquals(T paramT1, T paramT2, int paramInt) {
    boolean bool;
    if (isFieldPresent(paramT1, paramInt) == isFieldPresent(paramT2, paramInt)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isFieldPresent(T paramT, int paramInt1, int paramInt2, int paramInt3) {
    boolean bool;
    if (this.proto3)
      return isFieldPresent(paramT, paramInt1); 
    if ((paramInt2 & paramInt3) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isFieldPresent(T paramT, int paramInt) {
    boolean bool = this.proto3;
    boolean bool1 = false, bool2 = false, bool3 = false, bool4 = false, bool5 = false, bool6 = false, bool7 = false, bool8 = false, bool9 = false, bool10 = false, bool11 = false, bool12 = false, bool13 = false, bool14 = false, bool15 = false, bool16 = false;
    if (bool) {
      paramInt = typeAndOffsetAt(paramInt);
      long l = offset(paramInt);
      switch (type(paramInt)) {
        default:
          throw new IllegalArgumentException();
        case 17:
          bool14 = bool16;
          if (UnsafeUtil.getObject(paramT, l) != null)
            bool14 = true; 
          return bool14;
        case 16:
          bool14 = bool1;
          if (UnsafeUtil.getLong(paramT, l) != 0L)
            bool14 = true; 
          return bool14;
        case 15:
          bool14 = bool2;
          if (UnsafeUtil.getInt(paramT, l) != 0)
            bool14 = true; 
          return bool14;
        case 14:
          bool14 = bool3;
          if (UnsafeUtil.getLong(paramT, l) != 0L)
            bool14 = true; 
          return bool14;
        case 13:
          bool14 = bool4;
          if (UnsafeUtil.getInt(paramT, l) != 0)
            bool14 = true; 
          return bool14;
        case 12:
          bool14 = bool5;
          if (UnsafeUtil.getInt(paramT, l) != 0)
            bool14 = true; 
          return bool14;
        case 11:
          bool14 = bool6;
          if (UnsafeUtil.getInt(paramT, l) != 0)
            bool14 = true; 
          return bool14;
        case 10:
          return ByteString.EMPTY.equals(UnsafeUtil.getObject(paramT, l)) ^ true;
        case 9:
          bool14 = bool7;
          if (UnsafeUtil.getObject(paramT, l) != null)
            bool14 = true; 
          return bool14;
        case 8:
          paramT = (T)UnsafeUtil.getObject(paramT, l);
          if (paramT instanceof String)
            return true ^ ((String)paramT).isEmpty(); 
          if (paramT instanceof ByteString)
            return true ^ ByteString.EMPTY.equals(paramT); 
          throw new IllegalArgumentException();
        case 7:
          return UnsafeUtil.getBoolean(paramT, l);
        case 6:
          bool14 = bool8;
          if (UnsafeUtil.getInt(paramT, l) != 0)
            bool14 = true; 
          return bool14;
        case 5:
          bool14 = bool9;
          if (UnsafeUtil.getLong(paramT, l) != 0L)
            bool14 = true; 
          return bool14;
        case 4:
          bool14 = bool10;
          if (UnsafeUtil.getInt(paramT, l) != 0)
            bool14 = true; 
          return bool14;
        case 3:
          bool14 = bool11;
          if (UnsafeUtil.getLong(paramT, l) != 0L)
            bool14 = true; 
          return bool14;
        case 2:
          bool14 = bool12;
          if (UnsafeUtil.getLong(paramT, l) != 0L)
            bool14 = true; 
          return bool14;
        case 1:
          bool14 = bool13;
          if (UnsafeUtil.getFloat(paramT, l) != 0.0F)
            bool14 = true; 
          return bool14;
        case 0:
          break;
      } 
      if (UnsafeUtil.getDouble(paramT, l) != 0.0D)
        bool14 = true; 
      return bool14;
    } 
    paramInt = presenceMaskAndOffsetAt(paramInt);
    bool14 = bool15;
    if ((UnsafeUtil.getInt(paramT, (0xFFFFF & paramInt)) & 1 << paramInt >>> 20) != 0)
      bool14 = true; 
    return bool14;
  }
  
  private void setFieldPresent(T paramT, int paramInt) {
    if (this.proto3)
      return; 
    paramInt = presenceMaskAndOffsetAt(paramInt);
    long l = (0xFFFFF & paramInt);
    int i = UnsafeUtil.getInt(paramT, l);
    UnsafeUtil.putInt(paramT, l, i | 1 << paramInt >>> 20);
  }
  
  private boolean isOneofPresent(T paramT, int paramInt1, int paramInt2) {
    boolean bool;
    paramInt2 = presenceMaskAndOffsetAt(paramInt2);
    if (UnsafeUtil.getInt(paramT, (0xFFFFF & paramInt2)) == paramInt1) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isOneofCaseEqual(T paramT1, T paramT2, int paramInt) {
    boolean bool;
    paramInt = presenceMaskAndOffsetAt(paramInt);
    int i = UnsafeUtil.getInt(paramT1, (paramInt & 0xFFFFF));
    long l = (0xFFFFF & paramInt);
    if (i == UnsafeUtil.getInt(paramT2, l)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void setOneofPresent(T paramT, int paramInt1, int paramInt2) {
    paramInt2 = presenceMaskAndOffsetAt(paramInt2);
    UnsafeUtil.putInt(paramT, (0xFFFFF & paramInt2), paramInt1);
  }
  
  private int positionForFieldNumber(int paramInt) {
    if (paramInt >= this.minFieldNumber && paramInt <= this.maxFieldNumber)
      return slowPositionForFieldNumber(paramInt, 0); 
    return -1;
  }
  
  private int positionForFieldNumber(int paramInt1, int paramInt2) {
    if (paramInt1 >= this.minFieldNumber && paramInt1 <= this.maxFieldNumber)
      return slowPositionForFieldNumber(paramInt1, paramInt2); 
    return -1;
  }
  
  private int slowPositionForFieldNumber(int paramInt1, int paramInt2) {
    int i = this.buffer.length / 3 - 1;
    while (paramInt2 <= i) {
      int j = i + paramInt2 >>> 1;
      int k = j * 3;
      int m = numberAt(k);
      if (paramInt1 == m)
        return k; 
      if (paramInt1 < m) {
        i = j - 1;
        continue;
      } 
      paramInt2 = j + 1;
    } 
    return -1;
  }
  
  int getSchemaSize() {
    return this.buffer.length * 3;
  }
}
