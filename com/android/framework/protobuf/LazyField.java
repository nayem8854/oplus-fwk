package com.android.framework.protobuf;

import java.util.Iterator;
import java.util.Map;

public class LazyField extends LazyFieldLite {
  private final MessageLite defaultInstance;
  
  public LazyField(MessageLite paramMessageLite, ExtensionRegistryLite paramExtensionRegistryLite, ByteString paramByteString) {
    super(paramExtensionRegistryLite, paramByteString);
    this.defaultInstance = paramMessageLite;
  }
  
  public boolean containsDefaultInstance() {
    return (super.containsDefaultInstance() || this.value == this.defaultInstance);
  }
  
  public MessageLite getValue() {
    return getValue(this.defaultInstance);
  }
  
  public int hashCode() {
    return getValue().hashCode();
  }
  
  public boolean equals(Object paramObject) {
    return getValue().equals(paramObject);
  }
  
  public String toString() {
    return getValue().toString();
  }
  
  class LazyEntry<K> implements Map.Entry<K, Object> {
    private Map.Entry<K, LazyField> entry;
    
    private LazyEntry(LazyField this$0) {
      this.entry = (Map.Entry<K, LazyField>)this$0;
    }
    
    public K getKey() {
      return this.entry.getKey();
    }
    
    public Object getValue() {
      LazyField lazyField = this.entry.getValue();
      if (lazyField == null)
        return null; 
      return lazyField.getValue();
    }
    
    public LazyField getField() {
      return this.entry.getValue();
    }
    
    public Object setValue(Object param1Object) {
      if (param1Object instanceof MessageLite)
        return ((LazyField)this.entry.getValue()).setValue((MessageLite)param1Object); 
      throw new IllegalArgumentException("LazyField now only used for MessageSet, and the value of MessageSet must be an instance of MessageLite");
    }
  }
  
  class LazyIterator<K> implements Iterator<Map.Entry<K, Object>> {
    private Iterator<Map.Entry<K, Object>> iterator;
    
    public LazyIterator(LazyField this$0) {
      this.iterator = (Iterator<Map.Entry<K, Object>>)this$0;
    }
    
    public boolean hasNext() {
      return this.iterator.hasNext();
    }
    
    public Map.Entry<K, Object> next() {
      Map.Entry<K, Object> entry = this.iterator.next();
      if (entry.getValue() instanceof LazyField)
        return new LazyField.LazyEntry<>(); 
      return entry;
    }
    
    public void remove() {
      this.iterator.remove();
    }
  }
}
