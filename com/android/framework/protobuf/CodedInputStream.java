package com.android.framework.protobuf;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public abstract class CodedInputStream {
  private static final int DEFAULT_BUFFER_SIZE = 4096;
  
  private static final int DEFAULT_RECURSION_LIMIT = 100;
  
  private static final int DEFAULT_SIZE_LIMIT = 2147483647;
  
  int recursionDepth;
  
  int recursionLimit = 100;
  
  private boolean shouldDiscardUnknownFields;
  
  int sizeLimit = Integer.MAX_VALUE;
  
  CodedInputStreamReader wrapper;
  
  public static CodedInputStream newInstance(InputStream paramInputStream) {
    return newInstance(paramInputStream, 4096);
  }
  
  public static CodedInputStream newInstance(InputStream paramInputStream, int paramInt) {
    if (paramInt > 0) {
      if (paramInputStream == null)
        return newInstance(Internal.EMPTY_BYTE_ARRAY); 
      return new StreamDecoder(paramInt);
    } 
    throw new IllegalArgumentException("bufferSize must be > 0");
  }
  
  public static CodedInputStream newInstance(Iterable<ByteBuffer> paramIterable) {
    if (!UnsafeDirectNioDecoder.isSupported())
      return newInstance(new IterableByteBufferInputStream(paramIterable)); 
    return newInstance(paramIterable, false);
  }
  
  static CodedInputStream newInstance(Iterable<ByteBuffer> paramIterable, boolean paramBoolean) {
    int i = 0;
    int j = 0;
    for (ByteBuffer byteBuffer : paramIterable) {
      j += byteBuffer.remaining();
      if (byteBuffer.hasArray()) {
        i |= 0x1;
        continue;
      } 
      if (byteBuffer.isDirect()) {
        i |= 0x2;
        continue;
      } 
      i |= 0x4;
    } 
    if (i == 2)
      return new IterableDirectByteBufferDecoder(j, paramBoolean); 
    return newInstance(new IterableByteBufferInputStream(paramIterable));
  }
  
  public static CodedInputStream newInstance(byte[] paramArrayOfbyte) {
    return newInstance(paramArrayOfbyte, 0, paramArrayOfbyte.length);
  }
  
  public static CodedInputStream newInstance(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    return newInstance(paramArrayOfbyte, paramInt1, paramInt2, false);
  }
  
  static CodedInputStream newInstance(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, boolean paramBoolean) {
    ArrayDecoder arrayDecoder = new ArrayDecoder(paramInt1, paramInt2, paramBoolean);
    try {
      arrayDecoder.pushLimit(paramInt2);
      return arrayDecoder;
    } catch (InvalidProtocolBufferException invalidProtocolBufferException) {
      throw new IllegalArgumentException(invalidProtocolBufferException);
    } 
  }
  
  public static CodedInputStream newInstance(ByteBuffer paramByteBuffer) {
    return newInstance(paramByteBuffer, false);
  }
  
  static CodedInputStream newInstance(ByteBuffer paramByteBuffer, boolean paramBoolean) {
    if (paramByteBuffer.hasArray()) {
      byte[] arrayOfByte1 = paramByteBuffer.array();
      int i = paramByteBuffer.arrayOffset(), j = paramByteBuffer.position(), k = paramByteBuffer.remaining();
      return newInstance(arrayOfByte1, i + j, k, paramBoolean);
    } 
    if (paramByteBuffer.isDirect() && UnsafeDirectNioDecoder.isSupported())
      return new UnsafeDirectNioDecoder(paramBoolean); 
    byte[] arrayOfByte = new byte[paramByteBuffer.remaining()];
    paramByteBuffer.duplicate().get(arrayOfByte);
    return newInstance(arrayOfByte, 0, arrayOfByte.length, true);
  }
  
  public final int setRecursionLimit(int paramInt) {
    if (paramInt >= 0) {
      int i = this.recursionLimit;
      this.recursionLimit = paramInt;
      return i;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Recursion limit cannot be negative: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public final int setSizeLimit(int paramInt) {
    if (paramInt >= 0) {
      int i = this.sizeLimit;
      this.sizeLimit = paramInt;
      return i;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Size limit cannot be negative: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  private CodedInputStream() {
    this.shouldDiscardUnknownFields = false;
  }
  
  final void discardUnknownFields() {
    this.shouldDiscardUnknownFields = true;
  }
  
  final void unsetDiscardUnknownFields() {
    this.shouldDiscardUnknownFields = false;
  }
  
  final boolean shouldDiscardUnknownFields() {
    return this.shouldDiscardUnknownFields;
  }
  
  public static int decodeZigZag32(int paramInt) {
    return paramInt >>> 1 ^ -(paramInt & 0x1);
  }
  
  public static long decodeZigZag64(long paramLong) {
    return paramLong >>> 1L ^ -(0x1L & paramLong);
  }
  
  public static int readRawVarint32(int paramInt, InputStream paramInputStream) throws IOException {
    int j;
    if ((paramInt & 0x80) == 0)
      return paramInt; 
    int i = paramInt & 0x7F;
    paramInt = 7;
    while (true) {
      j = paramInt;
      if (paramInt < 32) {
        j = paramInputStream.read();
        if (j != -1) {
          i |= (j & 0x7F) << paramInt;
          if ((j & 0x80) == 0)
            return i; 
          paramInt += 7;
        } 
        throw InvalidProtocolBufferException.truncatedMessage();
      } 
      break;
    } 
    while (j < 64) {
      paramInt = paramInputStream.read();
      if (paramInt != -1) {
        if ((paramInt & 0x80) == 0)
          return i; 
        j += 7;
      } 
      throw InvalidProtocolBufferException.truncatedMessage();
    } 
    throw InvalidProtocolBufferException.malformedVarint();
  }
  
  static int readRawVarint32(InputStream paramInputStream) throws IOException {
    int i = paramInputStream.read();
    if (i != -1)
      return readRawVarint32(i, paramInputStream); 
    throw InvalidProtocolBufferException.truncatedMessage();
  }
  
  public abstract void checkLastTagWas(int paramInt) throws InvalidProtocolBufferException;
  
  public abstract void enableAliasing(boolean paramBoolean);
  
  public abstract int getBytesUntilLimit();
  
  public abstract int getLastTag();
  
  public abstract int getTotalBytesRead();
  
  public abstract boolean isAtEnd() throws IOException;
  
  public abstract void popLimit(int paramInt);
  
  public abstract int pushLimit(int paramInt) throws InvalidProtocolBufferException;
  
  public abstract boolean readBool() throws IOException;
  
  public abstract byte[] readByteArray() throws IOException;
  
  public abstract ByteBuffer readByteBuffer() throws IOException;
  
  public abstract ByteString readBytes() throws IOException;
  
  public abstract double readDouble() throws IOException;
  
  public abstract int readEnum() throws IOException;
  
  public abstract int readFixed32() throws IOException;
  
  public abstract long readFixed64() throws IOException;
  
  public abstract float readFloat() throws IOException;
  
  public abstract <T extends MessageLite> T readGroup(int paramInt, Parser<T> paramParser, ExtensionRegistryLite paramExtensionRegistryLite) throws IOException;
  
  public abstract void readGroup(int paramInt, MessageLite.Builder paramBuilder, ExtensionRegistryLite paramExtensionRegistryLite) throws IOException;
  
  public abstract int readInt32() throws IOException;
  
  public abstract long readInt64() throws IOException;
  
  public abstract <T extends MessageLite> T readMessage(Parser<T> paramParser, ExtensionRegistryLite paramExtensionRegistryLite) throws IOException;
  
  public abstract void readMessage(MessageLite.Builder paramBuilder, ExtensionRegistryLite paramExtensionRegistryLite) throws IOException;
  
  public abstract byte readRawByte() throws IOException;
  
  public abstract byte[] readRawBytes(int paramInt) throws IOException;
  
  public abstract int readRawLittleEndian32() throws IOException;
  
  public abstract long readRawLittleEndian64() throws IOException;
  
  public abstract int readRawVarint32() throws IOException;
  
  public abstract long readRawVarint64() throws IOException;
  
  abstract long readRawVarint64SlowPath() throws IOException;
  
  public abstract int readSFixed32() throws IOException;
  
  public abstract long readSFixed64() throws IOException;
  
  public abstract int readSInt32() throws IOException;
  
  public abstract long readSInt64() throws IOException;
  
  public abstract String readString() throws IOException;
  
  public abstract String readStringRequireUtf8() throws IOException;
  
  public abstract int readTag() throws IOException;
  
  public abstract int readUInt32() throws IOException;
  
  public abstract long readUInt64() throws IOException;
  
  @Deprecated
  public abstract void readUnknownGroup(int paramInt, MessageLite.Builder paramBuilder) throws IOException;
  
  public abstract void resetSizeCounter();
  
  public abstract boolean skipField(int paramInt) throws IOException;
  
  @Deprecated
  public abstract boolean skipField(int paramInt, CodedOutputStream paramCodedOutputStream) throws IOException;
  
  public abstract void skipMessage() throws IOException;
  
  public abstract void skipMessage(CodedOutputStream paramCodedOutputStream) throws IOException;
  
  public abstract void skipRawBytes(int paramInt) throws IOException;
  
  class ArrayDecoder extends CodedInputStream {
    private final byte[] buffer;
    
    private int bufferSizeAfterLimit;
    
    private int currentLimit = Integer.MAX_VALUE;
    
    private boolean enableAliasing;
    
    private final boolean immutable;
    
    private int lastTag;
    
    private int limit;
    
    private int pos;
    
    private int startPos;
    
    private ArrayDecoder(CodedInputStream this$0, int param1Int1, int param1Int2, boolean param1Boolean) {
      this.buffer = (byte[])this$0;
      this.limit = param1Int1 + param1Int2;
      this.pos = param1Int1;
      this.startPos = param1Int1;
      this.immutable = param1Boolean;
    }
    
    public int readTag() throws IOException {
      if (isAtEnd()) {
        this.lastTag = 0;
        return 0;
      } 
      int i = readRawVarint32();
      if (WireFormat.getTagFieldNumber(i) != 0)
        return this.lastTag; 
      throw InvalidProtocolBufferException.invalidTag();
    }
    
    public void checkLastTagWas(int param1Int) throws InvalidProtocolBufferException {
      if (this.lastTag == param1Int)
        return; 
      throw InvalidProtocolBufferException.invalidEndTag();
    }
    
    public int getLastTag() {
      return this.lastTag;
    }
    
    public boolean skipField(int param1Int) throws IOException {
      int i = WireFormat.getTagWireType(param1Int);
      if (i != 0) {
        if (i != 1) {
          if (i != 2) {
            if (i != 3) {
              if (i != 4) {
                if (i == 5) {
                  skipRawBytes(4);
                  return true;
                } 
                throw InvalidProtocolBufferException.invalidWireType();
              } 
              return false;
            } 
            skipMessage();
            param1Int = WireFormat.makeTag(WireFormat.getTagFieldNumber(param1Int), 4);
            checkLastTagWas(param1Int);
            return true;
          } 
          skipRawBytes(readRawVarint32());
          return true;
        } 
        skipRawBytes(8);
        return true;
      } 
      skipRawVarint();
      return true;
    }
    
    public boolean skipField(int param1Int, CodedOutputStream param1CodedOutputStream) throws IOException {
      int i = WireFormat.getTagWireType(param1Int);
      if (i != 0) {
        if (i != 1) {
          if (i != 2) {
            if (i != 3) {
              if (i != 4) {
                if (i == 5) {
                  i = readRawLittleEndian32();
                  param1CodedOutputStream.writeRawVarint32(param1Int);
                  param1CodedOutputStream.writeFixed32NoTag(i);
                  return true;
                } 
                throw InvalidProtocolBufferException.invalidWireType();
              } 
              return false;
            } 
            param1CodedOutputStream.writeRawVarint32(param1Int);
            skipMessage(param1CodedOutputStream);
            param1Int = WireFormat.getTagFieldNumber(param1Int);
            param1Int = WireFormat.makeTag(param1Int, 4);
            checkLastTagWas(param1Int);
            param1CodedOutputStream.writeRawVarint32(param1Int);
            return true;
          } 
          ByteString byteString = readBytes();
          param1CodedOutputStream.writeRawVarint32(param1Int);
          param1CodedOutputStream.writeBytesNoTag(byteString);
          return true;
        } 
        long l1 = readRawLittleEndian64();
        param1CodedOutputStream.writeRawVarint32(param1Int);
        param1CodedOutputStream.writeFixed64NoTag(l1);
        return true;
      } 
      long l = readInt64();
      param1CodedOutputStream.writeRawVarint32(param1Int);
      param1CodedOutputStream.writeUInt64NoTag(l);
      return true;
    }
    
    public void skipMessage() throws IOException {
      int i;
      do {
        i = readTag();
      } while (i != 0 && skipField(i));
    }
    
    public void skipMessage(CodedOutputStream param1CodedOutputStream) throws IOException {
      int i;
      do {
        i = readTag();
      } while (i != 0 && skipField(i, param1CodedOutputStream));
    }
    
    public double readDouble() throws IOException {
      return Double.longBitsToDouble(readRawLittleEndian64());
    }
    
    public float readFloat() throws IOException {
      return Float.intBitsToFloat(readRawLittleEndian32());
    }
    
    public long readUInt64() throws IOException {
      return readRawVarint64();
    }
    
    public long readInt64() throws IOException {
      return readRawVarint64();
    }
    
    public int readInt32() throws IOException {
      return readRawVarint32();
    }
    
    public long readFixed64() throws IOException {
      return readRawLittleEndian64();
    }
    
    public int readFixed32() throws IOException {
      return readRawLittleEndian32();
    }
    
    public boolean readBool() throws IOException {
      boolean bool;
      if (readRawVarint64() != 0L) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public String readString() throws IOException {
      int i = readRawVarint32();
      if (i > 0 && i <= this.limit - this.pos) {
        String str = new String(this.buffer, this.pos, i, Internal.UTF_8);
        this.pos += i;
        return str;
      } 
      if (i == 0)
        return ""; 
      if (i < 0)
        throw InvalidProtocolBufferException.negativeSize(); 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public String readStringRequireUtf8() throws IOException {
      int i = readRawVarint32();
      if (i > 0) {
        int j = this.limit, k = this.pos;
        if (i <= j - k) {
          String str = Utf8.decodeUtf8(this.buffer, k, i);
          this.pos += i;
          return str;
        } 
      } 
      if (i == 0)
        return ""; 
      if (i <= 0)
        throw InvalidProtocolBufferException.negativeSize(); 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public void readGroup(int param1Int, MessageLite.Builder param1Builder, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      if (this.recursionDepth < this.recursionLimit) {
        this.recursionDepth++;
        param1Builder.mergeFrom(this, param1ExtensionRegistryLite);
        checkLastTagWas(WireFormat.makeTag(param1Int, 4));
        this.recursionDepth--;
        return;
      } 
      throw InvalidProtocolBufferException.recursionLimitExceeded();
    }
    
    public <T extends MessageLite> T readGroup(int param1Int, Parser<T> param1Parser, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      if (this.recursionDepth < this.recursionLimit) {
        this.recursionDepth++;
        MessageLite messageLite = (MessageLite)param1Parser.parsePartialFrom(this, param1ExtensionRegistryLite);
        checkLastTagWas(WireFormat.makeTag(param1Int, 4));
        this.recursionDepth--;
        return (T)messageLite;
      } 
      throw InvalidProtocolBufferException.recursionLimitExceeded();
    }
    
    @Deprecated
    public void readUnknownGroup(int param1Int, MessageLite.Builder param1Builder) throws IOException {
      readGroup(param1Int, param1Builder, ExtensionRegistryLite.getEmptyRegistry());
    }
    
    public void readMessage(MessageLite.Builder param1Builder, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      int i = readRawVarint32();
      if (this.recursionDepth < this.recursionLimit) {
        i = pushLimit(i);
        this.recursionDepth++;
        param1Builder.mergeFrom(this, param1ExtensionRegistryLite);
        checkLastTagWas(0);
        this.recursionDepth--;
        popLimit(i);
        return;
      } 
      throw InvalidProtocolBufferException.recursionLimitExceeded();
    }
    
    public <T extends MessageLite> T readMessage(Parser<T> param1Parser, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      int i = readRawVarint32();
      if (this.recursionDepth < this.recursionLimit) {
        i = pushLimit(i);
        this.recursionDepth++;
        MessageLite messageLite = (MessageLite)param1Parser.parsePartialFrom(this, param1ExtensionRegistryLite);
        checkLastTagWas(0);
        this.recursionDepth--;
        popLimit(i);
        return (T)messageLite;
      } 
      throw InvalidProtocolBufferException.recursionLimitExceeded();
    }
    
    public ByteString readBytes() throws IOException {
      int i = readRawVarint32();
      if (i > 0) {
        int j = this.limit, k = this.pos;
        if (i <= j - k) {
          ByteString byteString;
          if (this.immutable && this.enableAliasing) {
            byteString = ByteString.wrap(this.buffer, k, i);
          } else {
            byteString = ByteString.copyFrom(this.buffer, this.pos, i);
          } 
          this.pos += i;
          return byteString;
        } 
      } 
      if (i == 0)
        return ByteString.EMPTY; 
      return ByteString.wrap(readRawBytes(i));
    }
    
    public byte[] readByteArray() throws IOException {
      int i = readRawVarint32();
      return readRawBytes(i);
    }
    
    public ByteBuffer readByteBuffer() throws IOException {
      int i = readRawVarint32();
      if (i > 0) {
        int j = this.limit, k = this.pos;
        if (i <= j - k) {
          ByteBuffer byteBuffer;
          if (!this.immutable && this.enableAliasing) {
            byteBuffer = ByteBuffer.wrap(this.buffer, k, i).slice();
          } else {
            byte[] arrayOfByte = this.buffer;
            j = this.pos;
            byteBuffer = ByteBuffer.wrap(Arrays.copyOfRange(arrayOfByte, j, j + i));
          } 
          this.pos += i;
          return byteBuffer;
        } 
      } 
      if (i == 0)
        return Internal.EMPTY_BYTE_BUFFER; 
      if (i < 0)
        throw InvalidProtocolBufferException.negativeSize(); 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public int readUInt32() throws IOException {
      return readRawVarint32();
    }
    
    public int readEnum() throws IOException {
      return readRawVarint32();
    }
    
    public int readSFixed32() throws IOException {
      return readRawLittleEndian32();
    }
    
    public long readSFixed64() throws IOException {
      return readRawLittleEndian64();
    }
    
    public int readSInt32() throws IOException {
      return decodeZigZag32(readRawVarint32());
    }
    
    public long readSInt64() throws IOException {
      return decodeZigZag64(readRawVarint64());
    }
    
    public int readRawVarint32() throws IOException {
      int i = this.pos;
      int j = this.limit;
      if (j != i) {
        byte[] arrayOfByte = this.buffer;
        int k = i + 1;
        i = arrayOfByte[i];
        if (i >= 0) {
          this.pos = k;
          return i;
        } 
        if (j - k >= 9) {
          j = k + 1;
          i = arrayOfByte[k] << 7 ^ i;
          if (i < 0) {
            k = i ^ 0xFFFFFF80;
          } else {
            k = j + 1;
            i = arrayOfByte[j] << 14 ^ i;
            if (i >= 0) {
              i ^= 0x3F80;
              j = k;
              k = i;
            } else {
              j = k + 1;
              k = arrayOfByte[k] << 21 ^ i;
              if (k < 0) {
                k = 0xFFE03F80 ^ k;
              } else {
                i = j + 1;
                byte b = arrayOfByte[j];
                k = k ^ b << 28 ^ 0xFE03F80;
                j = i;
                if (b < 0) {
                  int m = i + 1;
                  if (arrayOfByte[i] < 0) {
                    j = i = m + 1;
                    if (arrayOfByte[m] < 0) {
                      j = m = i + 1;
                      if (arrayOfByte[i] < 0) {
                        j = i = m + 1;
                        if (arrayOfByte[m] < 0) {
                          j = i + 1;
                          if (arrayOfByte[i] < 0)
                            return (int)readRawVarint64SlowPath(); 
                        } 
                      } 
                    } 
                  } 
                } 
              } 
            } 
          } 
          this.pos = j;
          return k;
        } 
      } 
      return (int)readRawVarint64SlowPath();
    }
    
    private void skipRawVarint() throws IOException {
      if (this.limit - this.pos >= 10) {
        skipRawVarintFastPath();
      } else {
        skipRawVarintSlowPath();
      } 
    }
    
    private void skipRawVarintFastPath() throws IOException {
      for (byte b = 0; b < 10; b++) {
        byte[] arrayOfByte = this.buffer;
        int i = this.pos;
        this.pos = i + 1;
        if (arrayOfByte[i] >= 0)
          return; 
      } 
      throw InvalidProtocolBufferException.malformedVarint();
    }
    
    private void skipRawVarintSlowPath() throws IOException {
      for (byte b = 0; b < 10; b++) {
        if (readRawByte() >= 0)
          return; 
      } 
      throw InvalidProtocolBufferException.malformedVarint();
    }
    
    public long readRawVarint64() throws IOException {
      int i = this.pos;
      int j = this.limit;
      if (j != i) {
        byte[] arrayOfByte = this.buffer;
        int k = i + 1;
        i = arrayOfByte[i];
        if (i >= 0) {
          this.pos = k;
          return i;
        } 
        if (j - k >= 9) {
          long l;
          j = k + 1;
          i = arrayOfByte[k] << 7 ^ i;
          if (i < 0) {
            l = (i ^ 0xFFFFFF80);
          } else {
            k = j + 1;
            i = arrayOfByte[j] << 14 ^ i;
            if (i >= 0) {
              l = (i ^ 0x3F80);
              j = k;
            } else {
              j = k + 1;
              k = arrayOfByte[k] << 21 ^ i;
              if (k < 0) {
                l = (0xFFE03F80 ^ k);
              } else {
                l = k;
                k = j + 1;
                l ^= arrayOfByte[j] << 28L;
                if (l >= 0L) {
                  l = 0xFE03F80L ^ l;
                  j = k;
                } else {
                  j = k + 1;
                  l = arrayOfByte[k] << 35L ^ l;
                  if (l < 0L) {
                    l = 0xFFFFFFF80FE03F80L ^ l;
                  } else {
                    k = j + 1;
                    l = arrayOfByte[j] << 42L ^ l;
                    if (l >= 0L) {
                      l = 0x3F80FE03F80L ^ l;
                      j = k;
                    } else {
                      j = k + 1;
                      l = arrayOfByte[k] << 49L ^ l;
                      if (l < 0L) {
                        l = 0xFFFE03F80FE03F80L ^ l;
                      } else {
                        k = j + 1;
                        long l1 = arrayOfByte[j];
                        l = l1 << 56L ^ l ^ 0xFE03F80FE03F80L;
                        if (l < 0L) {
                          j = k + 1;
                          if (arrayOfByte[k] < 0L)
                            return readRawVarint64SlowPath(); 
                        } else {
                          j = k;
                        } 
                      } 
                    } 
                  } 
                } 
              } 
            } 
          } 
          this.pos = j;
          return l;
        } 
      } 
      return readRawVarint64SlowPath();
    }
    
    long readRawVarint64SlowPath() throws IOException {
      long l = 0L;
      for (byte b = 0; b < 64; b += 7) {
        byte b1 = readRawByte();
        l |= (b1 & Byte.MAX_VALUE) << b;
        if ((b1 & 0x80) == 0)
          return l; 
      } 
      throw InvalidProtocolBufferException.malformedVarint();
    }
    
    public int readRawLittleEndian32() throws IOException {
      int i = this.pos;
      if (this.limit - i >= 4) {
        byte[] arrayOfByte = this.buffer;
        this.pos = i + 4;
        return arrayOfByte[i] & 0xFF | (arrayOfByte[i + 1] & 0xFF) << 8 | (arrayOfByte[i + 2] & 0xFF) << 16 | (arrayOfByte[i + 3] & 0xFF) << 24;
      } 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public long readRawLittleEndian64() throws IOException {
      int i = this.pos;
      if (this.limit - i >= 8) {
        byte[] arrayOfByte = this.buffer;
        this.pos = i + 8;
        return arrayOfByte[i] & 0xFFL | (arrayOfByte[i + 1] & 0xFFL) << 8L | (arrayOfByte[i + 2] & 0xFFL) << 16L | (arrayOfByte[i + 3] & 0xFFL) << 24L | (arrayOfByte[i + 4] & 0xFFL) << 32L | (arrayOfByte[i + 5] & 0xFFL) << 40L | (arrayOfByte[i + 6] & 0xFFL) << 48L | (arrayOfByte[i + 7] & 0xFFL) << 56L;
      } 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public void enableAliasing(boolean param1Boolean) {
      this.enableAliasing = param1Boolean;
    }
    
    public void resetSizeCounter() {
      this.startPos = this.pos;
    }
    
    public int pushLimit(int param1Int) throws InvalidProtocolBufferException {
      if (param1Int >= 0) {
        int i = param1Int + getTotalBytesRead();
        param1Int = this.currentLimit;
        if (i <= param1Int) {
          this.currentLimit = i;
          recomputeBufferSizeAfterLimit();
          return param1Int;
        } 
        throw InvalidProtocolBufferException.truncatedMessage();
      } 
      throw InvalidProtocolBufferException.negativeSize();
    }
    
    private void recomputeBufferSizeAfterLimit() {
      int i = this.limit + this.bufferSizeAfterLimit;
      int j = i - this.startPos;
      int k = this.currentLimit;
      if (j > k) {
        this.bufferSizeAfterLimit = j -= k;
        this.limit = i - j;
      } else {
        this.bufferSizeAfterLimit = 0;
      } 
    }
    
    public void popLimit(int param1Int) {
      this.currentLimit = param1Int;
      recomputeBufferSizeAfterLimit();
    }
    
    public int getBytesUntilLimit() {
      int i = this.currentLimit;
      if (i == Integer.MAX_VALUE)
        return -1; 
      return i - getTotalBytesRead();
    }
    
    public boolean isAtEnd() throws IOException {
      boolean bool;
      if (this.pos == this.limit) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public int getTotalBytesRead() {
      return this.pos - this.startPos;
    }
    
    public byte readRawByte() throws IOException {
      int i = this.pos;
      if (i != this.limit) {
        byte[] arrayOfByte = this.buffer;
        this.pos = i + 1;
        return arrayOfByte[i];
      } 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public byte[] readRawBytes(int param1Int) throws IOException {
      if (param1Int > 0) {
        int i = this.limit, j = this.pos;
        if (param1Int <= i - j) {
          i = this.pos;
          this.pos = param1Int = j + param1Int;
          return Arrays.copyOfRange(this.buffer, i, param1Int);
        } 
      } 
      if (param1Int <= 0) {
        if (param1Int == 0)
          return Internal.EMPTY_BYTE_ARRAY; 
        throw InvalidProtocolBufferException.negativeSize();
      } 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public void skipRawBytes(int param1Int) throws IOException {
      if (param1Int >= 0) {
        int i = this.limit, j = this.pos;
        if (param1Int <= i - j) {
          this.pos = j + param1Int;
          return;
        } 
      } 
      if (param1Int < 0)
        throw InvalidProtocolBufferException.negativeSize(); 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
  }
  
  class UnsafeDirectNioDecoder extends CodedInputStream {
    private final long address;
    
    private final ByteBuffer buffer;
    
    private int bufferSizeAfterLimit;
    
    private int currentLimit = Integer.MAX_VALUE;
    
    private boolean enableAliasing;
    
    private final boolean immutable;
    
    private int lastTag;
    
    private long limit;
    
    private long pos;
    
    private long startPos;
    
    static boolean isSupported() {
      return UnsafeUtil.hasUnsafeByteBufferOperations();
    }
    
    private UnsafeDirectNioDecoder(CodedInputStream this$0, boolean param1Boolean) {
      this.buffer = (ByteBuffer)this$0;
      long l = UnsafeUtil.addressOffset((ByteBuffer)this$0);
      this.limit = l + this$0.limit();
      this.pos = l = this.address + this$0.position();
      this.startPos = l;
      this.immutable = param1Boolean;
    }
    
    public int readTag() throws IOException {
      if (isAtEnd()) {
        this.lastTag = 0;
        return 0;
      } 
      int i = readRawVarint32();
      if (WireFormat.getTagFieldNumber(i) != 0)
        return this.lastTag; 
      throw InvalidProtocolBufferException.invalidTag();
    }
    
    public void checkLastTagWas(int param1Int) throws InvalidProtocolBufferException {
      if (this.lastTag == param1Int)
        return; 
      throw InvalidProtocolBufferException.invalidEndTag();
    }
    
    public int getLastTag() {
      return this.lastTag;
    }
    
    public boolean skipField(int param1Int) throws IOException {
      int i = WireFormat.getTagWireType(param1Int);
      if (i != 0) {
        if (i != 1) {
          if (i != 2) {
            if (i != 3) {
              if (i != 4) {
                if (i == 5) {
                  skipRawBytes(4);
                  return true;
                } 
                throw InvalidProtocolBufferException.invalidWireType();
              } 
              return false;
            } 
            skipMessage();
            param1Int = WireFormat.makeTag(WireFormat.getTagFieldNumber(param1Int), 4);
            checkLastTagWas(param1Int);
            return true;
          } 
          skipRawBytes(readRawVarint32());
          return true;
        } 
        skipRawBytes(8);
        return true;
      } 
      skipRawVarint();
      return true;
    }
    
    public boolean skipField(int param1Int, CodedOutputStream param1CodedOutputStream) throws IOException {
      int i = WireFormat.getTagWireType(param1Int);
      if (i != 0) {
        if (i != 1) {
          if (i != 2) {
            if (i != 3) {
              if (i != 4) {
                if (i == 5) {
                  i = readRawLittleEndian32();
                  param1CodedOutputStream.writeRawVarint32(param1Int);
                  param1CodedOutputStream.writeFixed32NoTag(i);
                  return true;
                } 
                throw InvalidProtocolBufferException.invalidWireType();
              } 
              return false;
            } 
            param1CodedOutputStream.writeRawVarint32(param1Int);
            skipMessage(param1CodedOutputStream);
            param1Int = WireFormat.getTagFieldNumber(param1Int);
            param1Int = WireFormat.makeTag(param1Int, 4);
            checkLastTagWas(param1Int);
            param1CodedOutputStream.writeRawVarint32(param1Int);
            return true;
          } 
          ByteString byteString = readBytes();
          param1CodedOutputStream.writeRawVarint32(param1Int);
          param1CodedOutputStream.writeBytesNoTag(byteString);
          return true;
        } 
        long l1 = readRawLittleEndian64();
        param1CodedOutputStream.writeRawVarint32(param1Int);
        param1CodedOutputStream.writeFixed64NoTag(l1);
        return true;
      } 
      long l = readInt64();
      param1CodedOutputStream.writeRawVarint32(param1Int);
      param1CodedOutputStream.writeUInt64NoTag(l);
      return true;
    }
    
    public void skipMessage() throws IOException {
      int i;
      do {
        i = readTag();
      } while (i != 0 && skipField(i));
    }
    
    public void skipMessage(CodedOutputStream param1CodedOutputStream) throws IOException {
      int i;
      do {
        i = readTag();
      } while (i != 0 && skipField(i, param1CodedOutputStream));
    }
    
    public double readDouble() throws IOException {
      return Double.longBitsToDouble(readRawLittleEndian64());
    }
    
    public float readFloat() throws IOException {
      return Float.intBitsToFloat(readRawLittleEndian32());
    }
    
    public long readUInt64() throws IOException {
      return readRawVarint64();
    }
    
    public long readInt64() throws IOException {
      return readRawVarint64();
    }
    
    public int readInt32() throws IOException {
      return readRawVarint32();
    }
    
    public long readFixed64() throws IOException {
      return readRawLittleEndian64();
    }
    
    public int readFixed32() throws IOException {
      return readRawLittleEndian32();
    }
    
    public boolean readBool() throws IOException {
      boolean bool;
      if (readRawVarint64() != 0L) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public String readString() throws IOException {
      int i = readRawVarint32();
      if (i > 0 && i <= remaining()) {
        byte[] arrayOfByte = new byte[i];
        UnsafeUtil.copyMemory(this.pos, arrayOfByte, 0L, i);
        String str = new String(arrayOfByte, Internal.UTF_8);
        this.pos += i;
        return str;
      } 
      if (i == 0)
        return ""; 
      if (i < 0)
        throw InvalidProtocolBufferException.negativeSize(); 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public String readStringRequireUtf8() throws IOException {
      int i = readRawVarint32();
      if (i > 0 && i <= remaining()) {
        int j = bufferPos(this.pos);
        String str = Utf8.decodeUtf8(this.buffer, j, i);
        this.pos += i;
        return str;
      } 
      if (i == 0)
        return ""; 
      if (i <= 0)
        throw InvalidProtocolBufferException.negativeSize(); 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public void readGroup(int param1Int, MessageLite.Builder param1Builder, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      if (this.recursionDepth < this.recursionLimit) {
        this.recursionDepth++;
        param1Builder.mergeFrom(this, param1ExtensionRegistryLite);
        checkLastTagWas(WireFormat.makeTag(param1Int, 4));
        this.recursionDepth--;
        return;
      } 
      throw InvalidProtocolBufferException.recursionLimitExceeded();
    }
    
    public <T extends MessageLite> T readGroup(int param1Int, Parser<T> param1Parser, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      if (this.recursionDepth < this.recursionLimit) {
        this.recursionDepth++;
        MessageLite messageLite = (MessageLite)param1Parser.parsePartialFrom(this, param1ExtensionRegistryLite);
        checkLastTagWas(WireFormat.makeTag(param1Int, 4));
        this.recursionDepth--;
        return (T)messageLite;
      } 
      throw InvalidProtocolBufferException.recursionLimitExceeded();
    }
    
    @Deprecated
    public void readUnknownGroup(int param1Int, MessageLite.Builder param1Builder) throws IOException {
      readGroup(param1Int, param1Builder, ExtensionRegistryLite.getEmptyRegistry());
    }
    
    public void readMessage(MessageLite.Builder param1Builder, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      int i = readRawVarint32();
      if (this.recursionDepth < this.recursionLimit) {
        i = pushLimit(i);
        this.recursionDepth++;
        param1Builder.mergeFrom(this, param1ExtensionRegistryLite);
        checkLastTagWas(0);
        this.recursionDepth--;
        popLimit(i);
        return;
      } 
      throw InvalidProtocolBufferException.recursionLimitExceeded();
    }
    
    public <T extends MessageLite> T readMessage(Parser<T> param1Parser, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      int i = readRawVarint32();
      if (this.recursionDepth < this.recursionLimit) {
        i = pushLimit(i);
        this.recursionDepth++;
        MessageLite messageLite = (MessageLite)param1Parser.parsePartialFrom(this, param1ExtensionRegistryLite);
        checkLastTagWas(0);
        this.recursionDepth--;
        popLimit(i);
        return (T)messageLite;
      } 
      throw InvalidProtocolBufferException.recursionLimitExceeded();
    }
    
    public ByteString readBytes() throws IOException {
      int i = readRawVarint32();
      if (i > 0 && i <= remaining()) {
        if (this.immutable && this.enableAliasing) {
          long l = this.pos;
          ByteBuffer byteBuffer = slice(l, i + l);
          this.pos += i;
          return ByteString.wrap(byteBuffer);
        } 
        byte[] arrayOfByte = new byte[i];
        UnsafeUtil.copyMemory(this.pos, arrayOfByte, 0L, i);
        this.pos += i;
        return ByteString.wrap(arrayOfByte);
      } 
      if (i == 0)
        return ByteString.EMPTY; 
      if (i < 0)
        throw InvalidProtocolBufferException.negativeSize(); 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public byte[] readByteArray() throws IOException {
      return readRawBytes(readRawVarint32());
    }
    
    public ByteBuffer readByteBuffer() throws IOException {
      int i = readRawVarint32();
      if (i > 0 && i <= remaining()) {
        if (!this.immutable && this.enableAliasing) {
          long l = this.pos;
          ByteBuffer byteBuffer = slice(l, i + l);
          this.pos += i;
          return byteBuffer;
        } 
        byte[] arrayOfByte = new byte[i];
        UnsafeUtil.copyMemory(this.pos, arrayOfByte, 0L, i);
        this.pos += i;
        return ByteBuffer.wrap(arrayOfByte);
      } 
      if (i == 0)
        return Internal.EMPTY_BYTE_BUFFER; 
      if (i < 0)
        throw InvalidProtocolBufferException.negativeSize(); 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public int readUInt32() throws IOException {
      return readRawVarint32();
    }
    
    public int readEnum() throws IOException {
      return readRawVarint32();
    }
    
    public int readSFixed32() throws IOException {
      return readRawLittleEndian32();
    }
    
    public long readSFixed64() throws IOException {
      return readRawLittleEndian64();
    }
    
    public int readSInt32() throws IOException {
      return decodeZigZag32(readRawVarint32());
    }
    
    public long readSInt64() throws IOException {
      return decodeZigZag64(readRawVarint64());
    }
    
    public int readRawVarint32() throws IOException {
      long l = this.pos;
      if (this.limit != l) {
        long l1 = l + 1L;
        byte b = UnsafeUtil.getByte(l);
        if (b >= 0) {
          this.pos = l1;
          return b;
        } 
        if (this.limit - l1 >= 9L) {
          l = l1 + 1L;
          int i = UnsafeUtil.getByte(l1) << 7 ^ b;
          if (i < 0) {
            i ^= 0xFFFFFF80;
            l1 = l;
          } else {
            l1 = l + 1L;
            i = UnsafeUtil.getByte(l) << 14 ^ i;
            if (i >= 0) {
              i ^= 0x3F80;
            } else {
              l = l1 + 1L;
              i = UnsafeUtil.getByte(l1) << 21 ^ i;
              if (i < 0) {
                i = 0xFFE03F80 ^ i;
                l1 = l;
              } else {
                l1 = l + 1L;
                byte b1 = UnsafeUtil.getByte(l);
                i = i ^ b1 << 28 ^ 0xFE03F80;
                if (b1 < 0) {
                  l = l1 + 1L;
                  if (UnsafeUtil.getByte(l1) < 0) {
                    l1 = l + 1L;
                    if (UnsafeUtil.getByte(l) < 0) {
                      l = l1 + 1L;
                      if (UnsafeUtil.getByte(l1) < 0) {
                        l1 = l + 1L;
                        if (UnsafeUtil.getByte(l) < 0) {
                          l = l1 + 1L;
                          if (UnsafeUtil.getByte(l1) < 0)
                            return (int)readRawVarint64SlowPath(); 
                          l1 = l;
                        } 
                      } else {
                        l1 = l;
                      } 
                    } 
                  } else {
                    l1 = l;
                  } 
                } 
              } 
            } 
          } 
          this.pos = l1;
          return i;
        } 
      } 
      return (int)readRawVarint64SlowPath();
    }
    
    private void skipRawVarint() throws IOException {
      if (remaining() >= 10) {
        skipRawVarintFastPath();
      } else {
        skipRawVarintSlowPath();
      } 
    }
    
    private void skipRawVarintFastPath() throws IOException {
      for (byte b = 0; b < 10; b++) {
        long l = this.pos;
        this.pos = 1L + l;
        if (UnsafeUtil.getByte(l) >= 0)
          return; 
      } 
      throw InvalidProtocolBufferException.malformedVarint();
    }
    
    private void skipRawVarintSlowPath() throws IOException {
      for (byte b = 0; b < 10; b++) {
        if (readRawByte() >= 0)
          return; 
      } 
      throw InvalidProtocolBufferException.malformedVarint();
    }
    
    public long readRawVarint64() throws IOException {
      long l = this.pos;
      if (this.limit != l) {
        long l1 = l + 1L;
        byte b = UnsafeUtil.getByte(l);
        if (b >= 0) {
          this.pos = l1;
          return b;
        } 
        if (this.limit - l1 >= 9L) {
          l = l1 + 1L;
          int i = UnsafeUtil.getByte(l1) << 7 ^ b;
          if (i < 0) {
            l1 = (i ^ 0xFFFFFF80);
          } else {
            l1 = l + 1L;
            i = UnsafeUtil.getByte(l) << 14 ^ i;
            if (i >= 0) {
              long l2 = (i ^ 0x3F80);
              l = l1;
              l1 = l2;
            } else {
              long l2 = l1 + 1L;
              i = UnsafeUtil.getByte(l1) << 21 ^ i;
              if (i < 0) {
                l1 = (0xFFE03F80 ^ i);
                l = l2;
              } else {
                l1 = i;
                l = l2 + 1L;
                l2 = l1 ^ UnsafeUtil.getByte(l2) << 28L;
                if (l2 >= 0L) {
                  l1 = 0xFE03F80L ^ l2;
                } else {
                  l1 = l + 1L;
                  l2 = UnsafeUtil.getByte(l) << 35L ^ l2;
                  if (l2 < 0L) {
                    l2 = 0xFFFFFFF80FE03F80L ^ l2;
                    l = l1;
                    l1 = l2;
                  } else {
                    l = l1 + 1L;
                    l2 = UnsafeUtil.getByte(l1) << 42L ^ l2;
                    if (l2 >= 0L) {
                      l1 = 0x3F80FE03F80L ^ l2;
                    } else {
                      l1 = l + 1L;
                      l2 = UnsafeUtil.getByte(l) << 49L ^ l2;
                      if (l2 < 0L) {
                        l2 = 0xFFFE03F80FE03F80L ^ l2;
                        l = l1;
                        l1 = l2;
                      } else {
                        l = l1 + 1L;
                        l1 = UnsafeUtil.getByte(l1);
                        l1 = l1 << 56L ^ l2 ^ 0xFE03F80FE03F80L;
                        if (l1 < 0L) {
                          l2 = l + 1L;
                          if (UnsafeUtil.getByte(l) < 0L)
                            return readRawVarint64SlowPath(); 
                          l = l2;
                        } 
                      } 
                    } 
                  } 
                } 
              } 
            } 
          } 
          this.pos = l;
          return l1;
        } 
      } 
      return readRawVarint64SlowPath();
    }
    
    long readRawVarint64SlowPath() throws IOException {
      long l = 0L;
      for (byte b = 0; b < 64; b += 7) {
        byte b1 = readRawByte();
        l |= (b1 & Byte.MAX_VALUE) << b;
        if ((b1 & 0x80) == 0)
          return l; 
      } 
      throw InvalidProtocolBufferException.malformedVarint();
    }
    
    public int readRawLittleEndian32() throws IOException {
      long l = this.pos;
      if (this.limit - l >= 4L) {
        this.pos = 4L + l;
        byte b1 = UnsafeUtil.getByte(l);
        byte b2 = UnsafeUtil.getByte(1L + l);
        byte b3 = UnsafeUtil.getByte(2L + l);
        byte b4 = UnsafeUtil.getByte(3L + l);
        return b1 & 0xFF | (b2 & 0xFF) << 8 | (b3 & 0xFF) << 16 | (b4 & 0xFF) << 24;
      } 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public long readRawLittleEndian64() throws IOException {
      long l = this.pos;
      if (this.limit - l >= 8L) {
        this.pos = 8L + l;
        long l1 = UnsafeUtil.getByte(l);
        long l2 = UnsafeUtil.getByte(1L + l);
        long l3 = UnsafeUtil.getByte(2L + l);
        long l4 = UnsafeUtil.getByte(3L + l);
        long l5 = UnsafeUtil.getByte(4L + l);
        long l6 = UnsafeUtil.getByte(5L + l);
        long l7 = UnsafeUtil.getByte(6L + l);
        l = UnsafeUtil.getByte(7L + l);
        return l1 & 0xFFL | (l2 & 0xFFL) << 8L | (l3 & 0xFFL) << 16L | (l4 & 0xFFL) << 24L | (l5 & 0xFFL) << 32L | (l6 & 0xFFL) << 40L | (l7 & 0xFFL) << 48L | (0xFFL & l) << 56L;
      } 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public void enableAliasing(boolean param1Boolean) {
      this.enableAliasing = param1Boolean;
    }
    
    public void resetSizeCounter() {
      this.startPos = this.pos;
    }
    
    public int pushLimit(int param1Int) throws InvalidProtocolBufferException {
      if (param1Int >= 0) {
        param1Int += getTotalBytesRead();
        int i = this.currentLimit;
        if (param1Int <= i) {
          this.currentLimit = param1Int;
          recomputeBufferSizeAfterLimit();
          return i;
        } 
        throw InvalidProtocolBufferException.truncatedMessage();
      } 
      throw InvalidProtocolBufferException.negativeSize();
    }
    
    public void popLimit(int param1Int) {
      this.currentLimit = param1Int;
      recomputeBufferSizeAfterLimit();
    }
    
    public int getBytesUntilLimit() {
      int i = this.currentLimit;
      if (i == Integer.MAX_VALUE)
        return -1; 
      return i - getTotalBytesRead();
    }
    
    public boolean isAtEnd() throws IOException {
      boolean bool;
      if (this.pos == this.limit) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public int getTotalBytesRead() {
      return (int)(this.pos - this.startPos);
    }
    
    public byte readRawByte() throws IOException {
      long l = this.pos;
      if (l != this.limit) {
        this.pos = 1L + l;
        return UnsafeUtil.getByte(l);
      } 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public byte[] readRawBytes(int param1Int) throws IOException {
      if (param1Int >= 0 && param1Int <= remaining()) {
        byte[] arrayOfByte = new byte[param1Int];
        long l = this.pos;
        slice(l, param1Int + l).get(arrayOfByte);
        this.pos += param1Int;
        return arrayOfByte;
      } 
      if (param1Int <= 0) {
        if (param1Int == 0)
          return Internal.EMPTY_BYTE_ARRAY; 
        throw InvalidProtocolBufferException.negativeSize();
      } 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public void skipRawBytes(int param1Int) throws IOException {
      if (param1Int >= 0 && param1Int <= remaining()) {
        this.pos += param1Int;
        return;
      } 
      if (param1Int < 0)
        throw InvalidProtocolBufferException.negativeSize(); 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    private void recomputeBufferSizeAfterLimit() {
      long l = this.limit + this.bufferSizeAfterLimit;
      int i = (int)(l - this.startPos);
      int j = this.currentLimit;
      if (i > j) {
        this.bufferSizeAfterLimit = i -= j;
        this.limit = l - i;
      } else {
        this.bufferSizeAfterLimit = 0;
      } 
    }
    
    private int remaining() {
      return (int)(this.limit - this.pos);
    }
    
    private int bufferPos(long param1Long) {
      return (int)(param1Long - this.address);
    }
    
    private ByteBuffer slice(long param1Long1, long param1Long2) throws IOException {
      Exception exception;
      int i = this.buffer.position();
      int j = this.buffer.limit();
      try {
        this.buffer.position(bufferPos(param1Long1));
        this.buffer.limit(bufferPos(param1Long2));
        ByteBuffer byteBuffer = this.buffer.slice();
        this.buffer.position(i);
        this.buffer.limit(j);
        return byteBuffer;
      } catch (IllegalArgumentException null) {
        throw InvalidProtocolBufferException.truncatedMessage();
      } finally {}
      this.buffer.position(i);
      this.buffer.limit(j);
      throw exception;
    }
  }
  
  class StreamDecoder extends CodedInputStream {
    private final byte[] buffer;
    
    private int bufferSize;
    
    private int bufferSizeAfterLimit;
    
    private int currentLimit = Integer.MAX_VALUE;
    
    private final InputStream input;
    
    private int lastTag;
    
    private int pos;
    
    private RefillCallback refillCallback;
    
    private int totalBytesRetired;
    
    public int readTag() throws IOException {
      if (isAtEnd()) {
        this.lastTag = 0;
        return 0;
      } 
      int i = readRawVarint32();
      if (WireFormat.getTagFieldNumber(i) != 0)
        return this.lastTag; 
      throw InvalidProtocolBufferException.invalidTag();
    }
    
    public void checkLastTagWas(int param1Int) throws InvalidProtocolBufferException {
      if (this.lastTag == param1Int)
        return; 
      throw InvalidProtocolBufferException.invalidEndTag();
    }
    
    public int getLastTag() {
      return this.lastTag;
    }
    
    public boolean skipField(int param1Int) throws IOException {
      int i = WireFormat.getTagWireType(param1Int);
      if (i != 0) {
        if (i != 1) {
          if (i != 2) {
            if (i != 3) {
              if (i != 4) {
                if (i == 5) {
                  skipRawBytes(4);
                  return true;
                } 
                throw InvalidProtocolBufferException.invalidWireType();
              } 
              return false;
            } 
            skipMessage();
            param1Int = WireFormat.makeTag(WireFormat.getTagFieldNumber(param1Int), 4);
            checkLastTagWas(param1Int);
            return true;
          } 
          skipRawBytes(readRawVarint32());
          return true;
        } 
        skipRawBytes(8);
        return true;
      } 
      skipRawVarint();
      return true;
    }
    
    public boolean skipField(int param1Int, CodedOutputStream param1CodedOutputStream) throws IOException {
      int i = WireFormat.getTagWireType(param1Int);
      if (i != 0) {
        if (i != 1) {
          if (i != 2) {
            if (i != 3) {
              if (i != 4) {
                if (i == 5) {
                  i = readRawLittleEndian32();
                  param1CodedOutputStream.writeRawVarint32(param1Int);
                  param1CodedOutputStream.writeFixed32NoTag(i);
                  return true;
                } 
                throw InvalidProtocolBufferException.invalidWireType();
              } 
              return false;
            } 
            param1CodedOutputStream.writeRawVarint32(param1Int);
            skipMessage(param1CodedOutputStream);
            param1Int = WireFormat.getTagFieldNumber(param1Int);
            param1Int = WireFormat.makeTag(param1Int, 4);
            checkLastTagWas(param1Int);
            param1CodedOutputStream.writeRawVarint32(param1Int);
            return true;
          } 
          ByteString byteString = readBytes();
          param1CodedOutputStream.writeRawVarint32(param1Int);
          param1CodedOutputStream.writeBytesNoTag(byteString);
          return true;
        } 
        long l1 = readRawLittleEndian64();
        param1CodedOutputStream.writeRawVarint32(param1Int);
        param1CodedOutputStream.writeFixed64NoTag(l1);
        return true;
      } 
      long l = readInt64();
      param1CodedOutputStream.writeRawVarint32(param1Int);
      param1CodedOutputStream.writeUInt64NoTag(l);
      return true;
    }
    
    public void skipMessage() throws IOException {
      int i;
      do {
        i = readTag();
      } while (i != 0 && skipField(i));
    }
    
    public void skipMessage(CodedOutputStream param1CodedOutputStream) throws IOException {
      int i;
      do {
        i = readTag();
      } while (i != 0 && skipField(i, param1CodedOutputStream));
    }
    
    class RefillCallback {
      public abstract void onRefill();
    }
    
    private class SkippedDataSink implements RefillCallback {
      private ByteArrayOutputStream byteArrayStream;
      
      private int lastPos = CodedInputStream.StreamDecoder.this.pos;
      
      final CodedInputStream.StreamDecoder this$0;
      
      public void onRefill() {
        if (this.byteArrayStream == null)
          this.byteArrayStream = new ByteArrayOutputStream(); 
        this.byteArrayStream.write(CodedInputStream.StreamDecoder.this.buffer, this.lastPos, CodedInputStream.StreamDecoder.this.pos - this.lastPos);
        this.lastPos = 0;
      }
      
      ByteBuffer getSkippedData() {
        ByteArrayOutputStream byteArrayOutputStream = this.byteArrayStream;
        if (byteArrayOutputStream == null)
          return ByteBuffer.wrap(CodedInputStream.StreamDecoder.this.buffer, this.lastPos, CodedInputStream.StreamDecoder.this.pos - this.lastPos); 
        byteArrayOutputStream.write(CodedInputStream.StreamDecoder.this.buffer, this.lastPos, CodedInputStream.StreamDecoder.this.pos);
        return ByteBuffer.wrap(this.byteArrayStream.toByteArray());
      }
    }
    
    public double readDouble() throws IOException {
      return Double.longBitsToDouble(readRawLittleEndian64());
    }
    
    public float readFloat() throws IOException {
      return Float.intBitsToFloat(readRawLittleEndian32());
    }
    
    public long readUInt64() throws IOException {
      return readRawVarint64();
    }
    
    public long readInt64() throws IOException {
      return readRawVarint64();
    }
    
    public int readInt32() throws IOException {
      return readRawVarint32();
    }
    
    public long readFixed64() throws IOException {
      return readRawLittleEndian64();
    }
    
    public int readFixed32() throws IOException {
      return readRawLittleEndian32();
    }
    
    public boolean readBool() throws IOException {
      boolean bool;
      if (readRawVarint64() != 0L) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public String readString() throws IOException {
      int i = readRawVarint32();
      if (i > 0 && i <= this.bufferSize - this.pos) {
        String str = new String(this.buffer, this.pos, i, Internal.UTF_8);
        this.pos += i;
        return str;
      } 
      if (i == 0)
        return ""; 
      if (i <= this.bufferSize) {
        refillBuffer(i);
        String str = new String(this.buffer, this.pos, i, Internal.UTF_8);
        this.pos += i;
        return str;
      } 
      return new String(readRawBytesSlowPath(i, false), Internal.UTF_8);
    }
    
    public String readStringRequireUtf8() throws IOException {
      byte[] arrayOfByte;
      int i = readRawVarint32();
      int j = this.pos;
      if (i <= this.bufferSize - j && i > 0) {
        arrayOfByte = this.buffer;
        this.pos = j + i;
      } else {
        if (i == 0)
          return ""; 
        if (i <= this.bufferSize) {
          refillBuffer(i);
          arrayOfByte = this.buffer;
          j = 0;
          this.pos = 0 + i;
        } else {
          arrayOfByte = readRawBytesSlowPath(i, false);
          j = 0;
        } 
      } 
      return Utf8.decodeUtf8(arrayOfByte, j, i);
    }
    
    public void readGroup(int param1Int, MessageLite.Builder param1Builder, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      if (this.recursionDepth < this.recursionLimit) {
        this.recursionDepth++;
        param1Builder.mergeFrom(this, param1ExtensionRegistryLite);
        checkLastTagWas(WireFormat.makeTag(param1Int, 4));
        this.recursionDepth--;
        return;
      } 
      throw InvalidProtocolBufferException.recursionLimitExceeded();
    }
    
    public <T extends MessageLite> T readGroup(int param1Int, Parser<T> param1Parser, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      if (this.recursionDepth < this.recursionLimit) {
        this.recursionDepth++;
        MessageLite messageLite = (MessageLite)param1Parser.parsePartialFrom(this, param1ExtensionRegistryLite);
        checkLastTagWas(WireFormat.makeTag(param1Int, 4));
        this.recursionDepth--;
        return (T)messageLite;
      } 
      throw InvalidProtocolBufferException.recursionLimitExceeded();
    }
    
    @Deprecated
    public void readUnknownGroup(int param1Int, MessageLite.Builder param1Builder) throws IOException {
      readGroup(param1Int, param1Builder, ExtensionRegistryLite.getEmptyRegistry());
    }
    
    public void readMessage(MessageLite.Builder param1Builder, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      int i = readRawVarint32();
      if (this.recursionDepth < this.recursionLimit) {
        i = pushLimit(i);
        this.recursionDepth++;
        param1Builder.mergeFrom(this, param1ExtensionRegistryLite);
        checkLastTagWas(0);
        this.recursionDepth--;
        popLimit(i);
        return;
      } 
      throw InvalidProtocolBufferException.recursionLimitExceeded();
    }
    
    public <T extends MessageLite> T readMessage(Parser<T> param1Parser, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      int i = readRawVarint32();
      if (this.recursionDepth < this.recursionLimit) {
        i = pushLimit(i);
        this.recursionDepth++;
        MessageLite messageLite = (MessageLite)param1Parser.parsePartialFrom(this, param1ExtensionRegistryLite);
        checkLastTagWas(0);
        this.recursionDepth--;
        popLimit(i);
        return (T)messageLite;
      } 
      throw InvalidProtocolBufferException.recursionLimitExceeded();
    }
    
    public ByteString readBytes() throws IOException {
      int i = readRawVarint32();
      int j = this.bufferSize, k = this.pos;
      if (i <= j - k && i > 0) {
        ByteString byteString = ByteString.copyFrom(this.buffer, k, i);
        this.pos += i;
        return byteString;
      } 
      if (i == 0)
        return ByteString.EMPTY; 
      return readBytesSlowPath(i);
    }
    
    public byte[] readByteArray() throws IOException {
      int i = readRawVarint32();
      int j = this.bufferSize, k = this.pos;
      if (i <= j - k && i > 0) {
        byte[] arrayOfByte = Arrays.copyOfRange(this.buffer, k, k + i);
        this.pos += i;
        return arrayOfByte;
      } 
      return readRawBytesSlowPath(i, false);
    }
    
    public ByteBuffer readByteBuffer() throws IOException {
      int i = readRawVarint32();
      int j = this.bufferSize, k = this.pos;
      if (i <= j - k && i > 0) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(Arrays.copyOfRange(this.buffer, k, k + i));
        this.pos += i;
        return byteBuffer;
      } 
      if (i == 0)
        return Internal.EMPTY_BYTE_BUFFER; 
      return ByteBuffer.wrap(readRawBytesSlowPath(i, true));
    }
    
    public int readUInt32() throws IOException {
      return readRawVarint32();
    }
    
    public int readEnum() throws IOException {
      return readRawVarint32();
    }
    
    public int readSFixed32() throws IOException {
      return readRawLittleEndian32();
    }
    
    public long readSFixed64() throws IOException {
      return readRawLittleEndian64();
    }
    
    public int readSInt32() throws IOException {
      return decodeZigZag32(readRawVarint32());
    }
    
    public long readSInt64() throws IOException {
      return decodeZigZag64(readRawVarint64());
    }
    
    public int readRawVarint32() throws IOException {
      int i = this.pos;
      int j = this.bufferSize;
      if (j != i) {
        byte[] arrayOfByte = this.buffer;
        int k = i + 1;
        i = arrayOfByte[i];
        if (i >= 0) {
          this.pos = k;
          return i;
        } 
        if (j - k >= 9) {
          j = k + 1;
          i = arrayOfByte[k] << 7 ^ i;
          if (i < 0) {
            k = i ^ 0xFFFFFF80;
          } else {
            k = j + 1;
            i = arrayOfByte[j] << 14 ^ i;
            if (i >= 0) {
              i ^= 0x3F80;
              j = k;
              k = i;
            } else {
              j = k + 1;
              k = arrayOfByte[k] << 21 ^ i;
              if (k < 0) {
                k = 0xFFE03F80 ^ k;
              } else {
                i = j + 1;
                byte b = arrayOfByte[j];
                k = k ^ b << 28 ^ 0xFE03F80;
                j = i;
                if (b < 0) {
                  int m = i + 1;
                  if (arrayOfByte[i] < 0) {
                    j = i = m + 1;
                    if (arrayOfByte[m] < 0) {
                      j = m = i + 1;
                      if (arrayOfByte[i] < 0) {
                        j = i = m + 1;
                        if (arrayOfByte[m] < 0) {
                          j = i + 1;
                          if (arrayOfByte[i] < 0)
                            return (int)readRawVarint64SlowPath(); 
                        } 
                      } 
                    } 
                  } 
                } 
              } 
            } 
          } 
          this.pos = j;
          return k;
        } 
      } 
      return (int)readRawVarint64SlowPath();
    }
    
    private void skipRawVarint() throws IOException {
      if (this.bufferSize - this.pos >= 10) {
        skipRawVarintFastPath();
      } else {
        skipRawVarintSlowPath();
      } 
    }
    
    private void skipRawVarintFastPath() throws IOException {
      for (byte b = 0; b < 10; b++) {
        byte[] arrayOfByte = this.buffer;
        int i = this.pos;
        this.pos = i + 1;
        if (arrayOfByte[i] >= 0)
          return; 
      } 
      throw InvalidProtocolBufferException.malformedVarint();
    }
    
    private void skipRawVarintSlowPath() throws IOException {
      for (byte b = 0; b < 10; b++) {
        if (readRawByte() >= 0)
          return; 
      } 
      throw InvalidProtocolBufferException.malformedVarint();
    }
    
    public long readRawVarint64() throws IOException {
      int i = this.pos;
      int j = this.bufferSize;
      if (j != i) {
        byte[] arrayOfByte = this.buffer;
        int k = i + 1;
        i = arrayOfByte[i];
        if (i >= 0) {
          this.pos = k;
          return i;
        } 
        if (j - k >= 9) {
          long l;
          j = k + 1;
          i = arrayOfByte[k] << 7 ^ i;
          if (i < 0) {
            l = (i ^ 0xFFFFFF80);
          } else {
            k = j + 1;
            i = arrayOfByte[j] << 14 ^ i;
            if (i >= 0) {
              l = (i ^ 0x3F80);
              j = k;
            } else {
              j = k + 1;
              k = arrayOfByte[k] << 21 ^ i;
              if (k < 0) {
                l = (0xFFE03F80 ^ k);
              } else {
                l = k;
                k = j + 1;
                l ^= arrayOfByte[j] << 28L;
                if (l >= 0L) {
                  l = 0xFE03F80L ^ l;
                  j = k;
                } else {
                  j = k + 1;
                  l = arrayOfByte[k] << 35L ^ l;
                  if (l < 0L) {
                    l = 0xFFFFFFF80FE03F80L ^ l;
                  } else {
                    k = j + 1;
                    l = arrayOfByte[j] << 42L ^ l;
                    if (l >= 0L) {
                      l = 0x3F80FE03F80L ^ l;
                      j = k;
                    } else {
                      j = k + 1;
                      l = arrayOfByte[k] << 49L ^ l;
                      if (l < 0L) {
                        l = 0xFFFE03F80FE03F80L ^ l;
                      } else {
                        k = j + 1;
                        long l1 = arrayOfByte[j];
                        l = l1 << 56L ^ l ^ 0xFE03F80FE03F80L;
                        if (l < 0L) {
                          j = k + 1;
                          if (arrayOfByte[k] < 0L)
                            return readRawVarint64SlowPath(); 
                        } else {
                          j = k;
                        } 
                      } 
                    } 
                  } 
                } 
              } 
            } 
          } 
          this.pos = j;
          return l;
        } 
      } 
      return readRawVarint64SlowPath();
    }
    
    long readRawVarint64SlowPath() throws IOException {
      long l = 0L;
      for (byte b = 0; b < 64; b += 7) {
        byte b1 = readRawByte();
        l |= (b1 & Byte.MAX_VALUE) << b;
        if ((b1 & 0x80) == 0)
          return l; 
      } 
      throw InvalidProtocolBufferException.malformedVarint();
    }
    
    public int readRawLittleEndian32() throws IOException {
      int i = this.pos;
      int j = i;
      if (this.bufferSize - i < 4) {
        refillBuffer(4);
        j = this.pos;
      } 
      byte[] arrayOfByte = this.buffer;
      this.pos = j + 4;
      return arrayOfByte[j] & 0xFF | (arrayOfByte[j + 1] & 0xFF) << 8 | (arrayOfByte[j + 2] & 0xFF) << 16 | (arrayOfByte[j + 3] & 0xFF) << 24;
    }
    
    public long readRawLittleEndian64() throws IOException {
      int i = this.pos;
      int j = i;
      if (this.bufferSize - i < 8) {
        refillBuffer(8);
        j = this.pos;
      } 
      byte[] arrayOfByte = this.buffer;
      this.pos = j + 8;
      return arrayOfByte[j] & 0xFFL | (arrayOfByte[j + 1] & 0xFFL) << 8L | (arrayOfByte[j + 2] & 0xFFL) << 16L | (arrayOfByte[j + 3] & 0xFFL) << 24L | (arrayOfByte[j + 4] & 0xFFL) << 32L | (arrayOfByte[j + 5] & 0xFFL) << 40L | (arrayOfByte[j + 6] & 0xFFL) << 48L | (arrayOfByte[j + 7] & 0xFFL) << 56L;
    }
    
    public void enableAliasing(boolean param1Boolean) {}
    
    public void resetSizeCounter() {
      this.totalBytesRetired = -this.pos;
    }
    
    public int pushLimit(int param1Int) throws InvalidProtocolBufferException {
      if (param1Int >= 0) {
        int i = param1Int + this.totalBytesRetired + this.pos;
        param1Int = this.currentLimit;
        if (i <= param1Int) {
          this.currentLimit = i;
          recomputeBufferSizeAfterLimit();
          return param1Int;
        } 
        throw InvalidProtocolBufferException.truncatedMessage();
      } 
      throw InvalidProtocolBufferException.negativeSize();
    }
    
    private void recomputeBufferSizeAfterLimit() {
      int i = this.bufferSize + this.bufferSizeAfterLimit;
      int j = this.totalBytesRetired + i;
      int k = this.currentLimit;
      if (j > k) {
        this.bufferSizeAfterLimit = k = j - k;
        this.bufferSize = i - k;
      } else {
        this.bufferSizeAfterLimit = 0;
      } 
    }
    
    public void popLimit(int param1Int) {
      this.currentLimit = param1Int;
      recomputeBufferSizeAfterLimit();
    }
    
    public int getBytesUntilLimit() {
      int i = this.currentLimit;
      if (i == Integer.MAX_VALUE)
        return -1; 
      int j = this.totalBytesRetired, k = this.pos;
      return i - j + k;
    }
    
    public boolean isAtEnd() throws IOException {
      int i = this.pos, j = this.bufferSize;
      boolean bool = true;
      if (i != j || tryRefillBuffer(1))
        bool = false; 
      return bool;
    }
    
    public int getTotalBytesRead() {
      return this.totalBytesRetired + this.pos;
    }
    
    private StreamDecoder(CodedInputStream this$0, int param1Int) {
      this.refillCallback = null;
      Internal.checkNotNull(this$0, "input");
      this.input = (InputStream)this$0;
      this.buffer = new byte[param1Int];
      this.bufferSize = 0;
      this.pos = 0;
      this.totalBytesRetired = 0;
    }
    
    private void refillBuffer(int param1Int) throws IOException {
      if (!tryRefillBuffer(param1Int)) {
        if (param1Int > this.sizeLimit - this.totalBytesRetired - this.pos)
          throw InvalidProtocolBufferException.sizeLimitExceeded(); 
        throw InvalidProtocolBufferException.truncatedMessage();
      } 
    }
    
    private boolean tryRefillBuffer(int param1Int) throws IOException {
      if (this.pos + param1Int > this.bufferSize) {
        int i = this.sizeLimit, j = this.totalBytesRetired, k = this.pos;
        if (param1Int > i - j - k)
          return false; 
        if (j + k + param1Int > this.currentLimit)
          return false; 
        RefillCallback refillCallback = this.refillCallback;
        if (refillCallback != null)
          refillCallback.onRefill(); 
        j = this.pos;
        if (j > 0) {
          i = this.bufferSize;
          if (i > j) {
            byte[] arrayOfByte1 = this.buffer;
            System.arraycopy(arrayOfByte1, j, arrayOfByte1, 0, i - j);
          } 
          this.totalBytesRetired += j;
          this.bufferSize -= j;
          this.pos = 0;
        } 
        InputStream inputStream = this.input;
        byte[] arrayOfByte = this.buffer;
        j = this.bufferSize;
        int m = arrayOfByte.length;
        k = this.sizeLimit;
        int n = this.totalBytesRetired;
        i = this.bufferSize;
        i = Math.min(m - j, k - n - i);
        j = inputStream.read(arrayOfByte, j, i);
        if (j != 0 && j >= -1 && j <= this.buffer.length) {
          if (j > 0) {
            boolean bool;
            this.bufferSize += j;
            recomputeBufferSizeAfterLimit();
            if (this.bufferSize >= param1Int) {
              bool = true;
            } else {
              bool = tryRefillBuffer(param1Int);
            } 
            return bool;
          } 
          return false;
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        inputStream = this.input;
        stringBuilder1.append(inputStream.getClass());
        stringBuilder1.append("#read(byte[]) returned invalid result: ");
        stringBuilder1.append(j);
        stringBuilder1.append("\nThe InputStream implementation is buggy.");
        throw new IllegalStateException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("refillBuffer() called when ");
      stringBuilder.append(param1Int);
      stringBuilder.append(" bytes were already available in buffer");
      throw new IllegalStateException(stringBuilder.toString());
    }
    
    public byte readRawByte() throws IOException {
      if (this.pos == this.bufferSize)
        refillBuffer(1); 
      byte[] arrayOfByte = this.buffer;
      int i = this.pos;
      this.pos = i + 1;
      return arrayOfByte[i];
    }
    
    public byte[] readRawBytes(int param1Int) throws IOException {
      int i = this.pos;
      if (param1Int <= this.bufferSize - i && param1Int > 0) {
        this.pos = i + param1Int;
        return Arrays.copyOfRange(this.buffer, i, i + param1Int);
      } 
      return readRawBytesSlowPath(param1Int, false);
    }
    
    private byte[] readRawBytesSlowPath(int param1Int, boolean param1Boolean) throws IOException {
      byte[] arrayOfByte = readRawBytesSlowPathOneChunk(param1Int);
      if (arrayOfByte != null) {
        if (param1Boolean)
          arrayOfByte = (byte[])arrayOfByte.clone(); 
        return arrayOfByte;
      } 
      int i = this.pos;
      int j = this.bufferSize, k = j - this.pos;
      this.totalBytesRetired += j;
      this.pos = 0;
      this.bufferSize = 0;
      List<byte[]> list = readRawBytesSlowPathRemainingChunks(param1Int - k);
      arrayOfByte = new byte[param1Int];
      System.arraycopy(this.buffer, i, arrayOfByte, 0, k);
      param1Int = k;
      for (byte[] arrayOfByte1 : list) {
        System.arraycopy(arrayOfByte1, 0, arrayOfByte, param1Int, arrayOfByte1.length);
        param1Int += arrayOfByte1.length;
      } 
      return arrayOfByte;
    }
    
    private byte[] readRawBytesSlowPathOneChunk(int param1Int) throws IOException {
      if (param1Int == 0)
        return Internal.EMPTY_BYTE_ARRAY; 
      if (param1Int >= 0) {
        int i = this.totalBytesRetired + this.pos + param1Int;
        if (i - this.sizeLimit <= 0) {
          int j = this.currentLimit;
          if (i <= j) {
            i = this.bufferSize - this.pos;
            j = param1Int - i;
            if (j < 4096 || j <= this.input.available()) {
              byte[] arrayOfByte = new byte[param1Int];
              System.arraycopy(this.buffer, this.pos, arrayOfByte, 0, i);
              this.totalBytesRetired += this.bufferSize;
              this.pos = 0;
              this.bufferSize = 0;
              while (i < arrayOfByte.length) {
                j = this.input.read(arrayOfByte, i, param1Int - i);
                if (j != -1) {
                  this.totalBytesRetired += j;
                  i += j;
                  continue;
                } 
                throw InvalidProtocolBufferException.truncatedMessage();
              } 
              return arrayOfByte;
            } 
            return null;
          } 
          skipRawBytes(j - this.totalBytesRetired - this.pos);
          throw InvalidProtocolBufferException.truncatedMessage();
        } 
        throw InvalidProtocolBufferException.sizeLimitExceeded();
      } 
      throw InvalidProtocolBufferException.negativeSize();
    }
    
    private List<byte[]> readRawBytesSlowPathRemainingChunks(int param1Int) throws IOException {
      ArrayList<byte[]> arrayList = new ArrayList();
      while (param1Int > 0) {
        byte[] arrayOfByte = new byte[Math.min(param1Int, 4096)];
        int i = 0;
        while (i < arrayOfByte.length) {
          int j = this.input.read(arrayOfByte, i, arrayOfByte.length - i);
          if (j != -1) {
            this.totalBytesRetired += j;
            i += j;
            continue;
          } 
          throw InvalidProtocolBufferException.truncatedMessage();
        } 
        param1Int -= arrayOfByte.length;
        arrayList.add(arrayOfByte);
      } 
      return (List<byte[]>)arrayList;
    }
    
    private ByteString readBytesSlowPath(int param1Int) throws IOException {
      byte[] arrayOfByte = readRawBytesSlowPathOneChunk(param1Int);
      if (arrayOfByte != null)
        return ByteString.copyFrom(arrayOfByte); 
      int i = this.pos;
      int j = this.bufferSize, k = j - this.pos;
      this.totalBytesRetired += j;
      this.pos = 0;
      this.bufferSize = 0;
      List<byte[]> list = readRawBytesSlowPathRemainingChunks(param1Int - k);
      arrayOfByte = new byte[param1Int];
      System.arraycopy(this.buffer, i, arrayOfByte, 0, k);
      param1Int = k;
      for (byte[] arrayOfByte1 : list) {
        System.arraycopy(arrayOfByte1, 0, arrayOfByte, param1Int, arrayOfByte1.length);
        param1Int += arrayOfByte1.length;
      } 
      return ByteString.wrap(arrayOfByte);
    }
    
    public void skipRawBytes(int param1Int) throws IOException {
      int i = this.bufferSize, j = this.pos;
      if (param1Int <= i - j && param1Int >= 0) {
        this.pos = j + param1Int;
      } else {
        skipRawBytesSlowPath(param1Int);
      } 
    }
    
    private void skipRawBytesSlowPath(int param1Int) throws IOException {
      if (param1Int >= 0) {
        int i = this.totalBytesRetired, j = this.pos, k = this.currentLimit;
        if (i + j + param1Int <= k) {
          k = 0;
          if (this.refillCallback == null) {
            this.totalBytesRetired = i + j;
            k = this.bufferSize;
            this.bufferSize = 0;
            this.pos = 0;
            k -= j;
            while (k < param1Int) {
              j = param1Int - k;
              try {
                long l = this.input.skip(j);
                if (l >= 0L && l <= j) {
                  if (l == 0L)
                    break; 
                  k += (int)l;
                  continue;
                } 
                IllegalStateException illegalStateException = new IllegalStateException();
                StringBuilder stringBuilder = new StringBuilder();
                this();
                InputStream inputStream = this.input;
                stringBuilder.append(inputStream.getClass());
                stringBuilder.append("#skip returned invalid result: ");
                stringBuilder.append(l);
                stringBuilder.append("\nThe InputStream implementation is buggy.");
                this(stringBuilder.toString());
                throw illegalStateException;
              } finally {
                this.totalBytesRetired += k;
                recomputeBufferSizeAfterLimit();
              } 
            } 
            this.totalBytesRetired += k;
            recomputeBufferSizeAfterLimit();
          } 
          if (k < param1Int) {
            j = this.bufferSize;
            k = j - this.pos;
            this.pos = j;
            refillBuffer(1);
            while (true) {
              j = this.bufferSize;
              if (param1Int - k > j) {
                k += j;
                this.pos = j;
                refillBuffer(1);
                continue;
              } 
              break;
            } 
            this.pos = param1Int - k;
          } 
          return;
        } 
        skipRawBytes(k - i - j);
        throw InvalidProtocolBufferException.truncatedMessage();
      } 
      throw InvalidProtocolBufferException.negativeSize();
    }
  }
  
  class IterableDirectByteBufferDecoder extends CodedInputStream {
    private int bufferSizeAfterCurrentLimit;
    
    private long currentAddress;
    
    private ByteBuffer currentByteBuffer;
    
    private long currentByteBufferLimit;
    
    private long currentByteBufferPos;
    
    private long currentByteBufferStartPos;
    
    private int currentLimit = Integer.MAX_VALUE;
    
    private boolean enableAliasing;
    
    private boolean immutable;
    
    private Iterable<ByteBuffer> input;
    
    private Iterator<ByteBuffer> iterator;
    
    private int lastTag;
    
    private int startOffset;
    
    private int totalBufferSize;
    
    private int totalBytesRead;
    
    private IterableDirectByteBufferDecoder(CodedInputStream this$0, int param1Int, boolean param1Boolean) {
      this.totalBufferSize = param1Int;
      this.input = (Iterable<ByteBuffer>)this$0;
      this.iterator = this$0.iterator();
      this.immutable = param1Boolean;
      this.totalBytesRead = 0;
      this.startOffset = 0;
      if (param1Int == 0) {
        this.currentByteBuffer = Internal.EMPTY_BYTE_BUFFER;
        this.currentByteBufferPos = 0L;
        this.currentByteBufferStartPos = 0L;
        this.currentByteBufferLimit = 0L;
        this.currentAddress = 0L;
      } else {
        tryGetNextByteBuffer();
      } 
    }
    
    private void getNextByteBuffer() throws InvalidProtocolBufferException {
      if (this.iterator.hasNext()) {
        tryGetNextByteBuffer();
        return;
      } 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    private void tryGetNextByteBuffer() {
      ByteBuffer byteBuffer = this.iterator.next();
      this.totalBytesRead += (int)(this.currentByteBufferPos - this.currentByteBufferStartPos);
      long l = byteBuffer.position();
      this.currentByteBufferStartPos = l;
      this.currentByteBufferLimit = this.currentByteBuffer.limit();
      this.currentAddress = l = UnsafeUtil.addressOffset(this.currentByteBuffer);
      this.currentByteBufferPos += l;
      this.currentByteBufferStartPos += l;
      this.currentByteBufferLimit += l;
    }
    
    public int readTag() throws IOException {
      if (isAtEnd()) {
        this.lastTag = 0;
        return 0;
      } 
      int i = readRawVarint32();
      if (WireFormat.getTagFieldNumber(i) != 0)
        return this.lastTag; 
      throw InvalidProtocolBufferException.invalidTag();
    }
    
    public void checkLastTagWas(int param1Int) throws InvalidProtocolBufferException {
      if (this.lastTag == param1Int)
        return; 
      throw InvalidProtocolBufferException.invalidEndTag();
    }
    
    public int getLastTag() {
      return this.lastTag;
    }
    
    public boolean skipField(int param1Int) throws IOException {
      int i = WireFormat.getTagWireType(param1Int);
      if (i != 0) {
        if (i != 1) {
          if (i != 2) {
            if (i != 3) {
              if (i != 4) {
                if (i == 5) {
                  skipRawBytes(4);
                  return true;
                } 
                throw InvalidProtocolBufferException.invalidWireType();
              } 
              return false;
            } 
            skipMessage();
            param1Int = WireFormat.makeTag(WireFormat.getTagFieldNumber(param1Int), 4);
            checkLastTagWas(param1Int);
            return true;
          } 
          skipRawBytes(readRawVarint32());
          return true;
        } 
        skipRawBytes(8);
        return true;
      } 
      skipRawVarint();
      return true;
    }
    
    public boolean skipField(int param1Int, CodedOutputStream param1CodedOutputStream) throws IOException {
      int i = WireFormat.getTagWireType(param1Int);
      if (i != 0) {
        if (i != 1) {
          if (i != 2) {
            if (i != 3) {
              if (i != 4) {
                if (i == 5) {
                  i = readRawLittleEndian32();
                  param1CodedOutputStream.writeRawVarint32(param1Int);
                  param1CodedOutputStream.writeFixed32NoTag(i);
                  return true;
                } 
                throw InvalidProtocolBufferException.invalidWireType();
              } 
              return false;
            } 
            param1CodedOutputStream.writeRawVarint32(param1Int);
            skipMessage(param1CodedOutputStream);
            param1Int = WireFormat.getTagFieldNumber(param1Int);
            param1Int = WireFormat.makeTag(param1Int, 4);
            checkLastTagWas(param1Int);
            param1CodedOutputStream.writeRawVarint32(param1Int);
            return true;
          } 
          ByteString byteString = readBytes();
          param1CodedOutputStream.writeRawVarint32(param1Int);
          param1CodedOutputStream.writeBytesNoTag(byteString);
          return true;
        } 
        long l1 = readRawLittleEndian64();
        param1CodedOutputStream.writeRawVarint32(param1Int);
        param1CodedOutputStream.writeFixed64NoTag(l1);
        return true;
      } 
      long l = readInt64();
      param1CodedOutputStream.writeRawVarint32(param1Int);
      param1CodedOutputStream.writeUInt64NoTag(l);
      return true;
    }
    
    public void skipMessage() throws IOException {
      int i;
      do {
        i = readTag();
      } while (i != 0 && skipField(i));
    }
    
    public void skipMessage(CodedOutputStream param1CodedOutputStream) throws IOException {
      int i;
      do {
        i = readTag();
      } while (i != 0 && skipField(i, param1CodedOutputStream));
    }
    
    public double readDouble() throws IOException {
      return Double.longBitsToDouble(readRawLittleEndian64());
    }
    
    public float readFloat() throws IOException {
      return Float.intBitsToFloat(readRawLittleEndian32());
    }
    
    public long readUInt64() throws IOException {
      return readRawVarint64();
    }
    
    public long readInt64() throws IOException {
      return readRawVarint64();
    }
    
    public int readInt32() throws IOException {
      return readRawVarint32();
    }
    
    public long readFixed64() throws IOException {
      return readRawLittleEndian64();
    }
    
    public int readFixed32() throws IOException {
      return readRawLittleEndian32();
    }
    
    public boolean readBool() throws IOException {
      boolean bool;
      if (readRawVarint64() != 0L) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public String readString() throws IOException {
      int i = readRawVarint32();
      if (i > 0) {
        long l1 = i, l2 = this.currentByteBufferLimit, l3 = this.currentByteBufferPos;
        if (l1 <= l2 - l3) {
          byte[] arrayOfByte = new byte[i];
          UnsafeUtil.copyMemory(l3, arrayOfByte, 0L, i);
          String str = new String(arrayOfByte, Internal.UTF_8);
          this.currentByteBufferPos += i;
          return str;
        } 
      } 
      if (i > 0 && i <= remaining()) {
        byte[] arrayOfByte = new byte[i];
        readRawBytesTo(arrayOfByte, 0, i);
        return new String(arrayOfByte, Internal.UTF_8);
      } 
      if (i == 0)
        return ""; 
      if (i < 0)
        throw InvalidProtocolBufferException.negativeSize(); 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public String readStringRequireUtf8() throws IOException {
      int i = readRawVarint32();
      if (i > 0) {
        long l1 = i, l2 = this.currentByteBufferLimit, l3 = this.currentByteBufferPos;
        if (l1 <= l2 - l3) {
          int j = (int)(l3 - this.currentByteBufferStartPos);
          String str = Utf8.decodeUtf8(this.currentByteBuffer, j, i);
          this.currentByteBufferPos += i;
          return str;
        } 
      } 
      if (i >= 0 && i <= remaining()) {
        byte[] arrayOfByte = new byte[i];
        readRawBytesTo(arrayOfByte, 0, i);
        return Utf8.decodeUtf8(arrayOfByte, 0, i);
      } 
      if (i == 0)
        return ""; 
      if (i <= 0)
        throw InvalidProtocolBufferException.negativeSize(); 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public void readGroup(int param1Int, MessageLite.Builder param1Builder, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      if (this.recursionDepth < this.recursionLimit) {
        this.recursionDepth++;
        param1Builder.mergeFrom(this, param1ExtensionRegistryLite);
        checkLastTagWas(WireFormat.makeTag(param1Int, 4));
        this.recursionDepth--;
        return;
      } 
      throw InvalidProtocolBufferException.recursionLimitExceeded();
    }
    
    public <T extends MessageLite> T readGroup(int param1Int, Parser<T> param1Parser, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      if (this.recursionDepth < this.recursionLimit) {
        this.recursionDepth++;
        MessageLite messageLite = (MessageLite)param1Parser.parsePartialFrom(this, param1ExtensionRegistryLite);
        checkLastTagWas(WireFormat.makeTag(param1Int, 4));
        this.recursionDepth--;
        return (T)messageLite;
      } 
      throw InvalidProtocolBufferException.recursionLimitExceeded();
    }
    
    @Deprecated
    public void readUnknownGroup(int param1Int, MessageLite.Builder param1Builder) throws IOException {
      readGroup(param1Int, param1Builder, ExtensionRegistryLite.getEmptyRegistry());
    }
    
    public void readMessage(MessageLite.Builder param1Builder, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      int i = readRawVarint32();
      if (this.recursionDepth < this.recursionLimit) {
        i = pushLimit(i);
        this.recursionDepth++;
        param1Builder.mergeFrom(this, param1ExtensionRegistryLite);
        checkLastTagWas(0);
        this.recursionDepth--;
        popLimit(i);
        return;
      } 
      throw InvalidProtocolBufferException.recursionLimitExceeded();
    }
    
    public <T extends MessageLite> T readMessage(Parser<T> param1Parser, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      int i = readRawVarint32();
      if (this.recursionDepth < this.recursionLimit) {
        i = pushLimit(i);
        this.recursionDepth++;
        MessageLite messageLite = (MessageLite)param1Parser.parsePartialFrom(this, param1ExtensionRegistryLite);
        checkLastTagWas(0);
        this.recursionDepth--;
        popLimit(i);
        return (T)messageLite;
      } 
      throw InvalidProtocolBufferException.recursionLimitExceeded();
    }
    
    public ByteString readBytes() throws IOException {
      int i = readRawVarint32();
      if (i > 0) {
        long l1 = i, l2 = this.currentByteBufferLimit, l3 = this.currentByteBufferPos;
        if (l1 <= l2 - l3) {
          if (this.immutable && this.enableAliasing) {
            int j = (int)(l3 - this.currentAddress);
            ByteString byteString = ByteString.wrap(slice(j, j + i));
            this.currentByteBufferPos += i;
            return byteString;
          } 
          byte[] arrayOfByte = new byte[i];
          UnsafeUtil.copyMemory(this.currentByteBufferPos, arrayOfByte, 0L, i);
          this.currentByteBufferPos += i;
          return ByteString.wrap(arrayOfByte);
        } 
      } 
      if (i > 0 && i <= remaining()) {
        byte[] arrayOfByte = new byte[i];
        readRawBytesTo(arrayOfByte, 0, i);
        return ByteString.wrap(arrayOfByte);
      } 
      if (i == 0)
        return ByteString.EMPTY; 
      if (i < 0)
        throw InvalidProtocolBufferException.negativeSize(); 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public byte[] readByteArray() throws IOException {
      return readRawBytes(readRawVarint32());
    }
    
    public ByteBuffer readByteBuffer() throws IOException {
      int i = readRawVarint32();
      if (i > 0 && i <= currentRemaining()) {
        if (!this.immutable && this.enableAliasing) {
          long l1 = this.currentByteBufferPos + i;
          long l2 = this.currentAddress;
          return slice((int)(l1 - l2 - i), (int)(l1 - l2));
        } 
        byte[] arrayOfByte = new byte[i];
        UnsafeUtil.copyMemory(this.currentByteBufferPos, arrayOfByte, 0L, i);
        this.currentByteBufferPos += i;
        return ByteBuffer.wrap(arrayOfByte);
      } 
      if (i > 0 && i <= remaining()) {
        byte[] arrayOfByte = new byte[i];
        readRawBytesTo(arrayOfByte, 0, i);
        return ByteBuffer.wrap(arrayOfByte);
      } 
      if (i == 0)
        return Internal.EMPTY_BYTE_BUFFER; 
      if (i < 0)
        throw InvalidProtocolBufferException.negativeSize(); 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public int readUInt32() throws IOException {
      return readRawVarint32();
    }
    
    public int readEnum() throws IOException {
      return readRawVarint32();
    }
    
    public int readSFixed32() throws IOException {
      return readRawLittleEndian32();
    }
    
    public long readSFixed64() throws IOException {
      return readRawLittleEndian64();
    }
    
    public int readSInt32() throws IOException {
      return decodeZigZag32(readRawVarint32());
    }
    
    public long readSInt64() throws IOException {
      return decodeZigZag64(readRawVarint64());
    }
    
    public int readRawVarint32() throws IOException {
      long l = this.currentByteBufferPos;
      if (this.currentByteBufferLimit != this.currentByteBufferPos) {
        long l1 = l + 1L;
        byte b = UnsafeUtil.getByte(l);
        if (b >= 0) {
          this.currentByteBufferPos++;
          return b;
        } 
        if (this.currentByteBufferLimit - this.currentByteBufferPos >= 10L) {
          l = l1 + 1L;
          int i = UnsafeUtil.getByte(l1) << 7 ^ b;
          if (i < 0) {
            i ^= 0xFFFFFF80;
          } else {
            l1 = l + 1L;
            i = UnsafeUtil.getByte(l) << 14 ^ i;
            if (i >= 0) {
              i ^= 0x3F80;
              l = l1;
            } else {
              l = l1 + 1L;
              i = UnsafeUtil.getByte(l1) << 21 ^ i;
              if (i < 0) {
                i = 0xFFE03F80 ^ i;
              } else {
                l1 = l + 1L;
                byte b1 = UnsafeUtil.getByte(l);
                i = i ^ b1 << 28 ^ 0xFE03F80;
                if (b1 < 0) {
                  long l2 = l1 + 1L;
                  if (UnsafeUtil.getByte(l1) < 0) {
                    l = l2 + 1L;
                    if (UnsafeUtil.getByte(l2) < 0) {
                      l1 = l + 1L;
                      if (UnsafeUtil.getByte(l) < 0) {
                        l = l1 + 1L;
                        if (UnsafeUtil.getByte(l1) < 0) {
                          l1 = l + 1L;
                          if (UnsafeUtil.getByte(l) < 0)
                            return (int)readRawVarint64SlowPath(); 
                          l = l1;
                        } 
                      } else {
                        l = l1;
                      } 
                    } 
                  } else {
                    l = l2;
                  } 
                } else {
                  l = l1;
                } 
              } 
            } 
          } 
          this.currentByteBufferPos = l;
          return i;
        } 
      } 
      return (int)readRawVarint64SlowPath();
    }
    
    public long readRawVarint64() throws IOException {
      long l = this.currentByteBufferPos;
      if (this.currentByteBufferLimit != this.currentByteBufferPos) {
        long l1 = l + 1L;
        byte b = UnsafeUtil.getByte(l);
        if (b >= 0) {
          this.currentByteBufferPos++;
          return b;
        } 
        if (this.currentByteBufferLimit - this.currentByteBufferPos >= 10L) {
          l = l1 + 1L;
          int i = UnsafeUtil.getByte(l1) << 7 ^ b;
          if (i < 0) {
            l1 = (i ^ 0xFFFFFF80);
          } else {
            l1 = l + 1L;
            i = UnsafeUtil.getByte(l) << 14 ^ i;
            if (i >= 0) {
              long l2 = (i ^ 0x3F80);
              l = l1;
              l1 = l2;
            } else {
              long l2 = l1 + 1L;
              i = UnsafeUtil.getByte(l1) << 21 ^ i;
              if (i < 0) {
                l1 = (0xFFE03F80 ^ i);
                l = l2;
              } else {
                l1 = i;
                l = l2 + 1L;
                l2 = l1 ^ UnsafeUtil.getByte(l2) << 28L;
                if (l2 >= 0L) {
                  l1 = 0xFE03F80L ^ l2;
                } else {
                  l1 = l + 1L;
                  l2 = UnsafeUtil.getByte(l) << 35L ^ l2;
                  if (l2 < 0L) {
                    l2 = 0xFFFFFFF80FE03F80L ^ l2;
                    l = l1;
                    l1 = l2;
                  } else {
                    l = l1 + 1L;
                    l2 = UnsafeUtil.getByte(l1) << 42L ^ l2;
                    if (l2 >= 0L) {
                      l1 = 0x3F80FE03F80L ^ l2;
                    } else {
                      l1 = l + 1L;
                      l2 = UnsafeUtil.getByte(l) << 49L ^ l2;
                      if (l2 < 0L) {
                        l2 = 0xFFFE03F80FE03F80L ^ l2;
                        l = l1;
                        l1 = l2;
                      } else {
                        l = l1 + 1L;
                        l1 = UnsafeUtil.getByte(l1);
                        l1 = l1 << 56L ^ l2 ^ 0xFE03F80FE03F80L;
                        if (l1 < 0L) {
                          l2 = l + 1L;
                          if (UnsafeUtil.getByte(l) < 0L)
                            return readRawVarint64SlowPath(); 
                          l = l2;
                        } 
                      } 
                    } 
                  } 
                } 
              } 
            } 
          } 
          this.currentByteBufferPos = l;
          return l1;
        } 
      } 
      return readRawVarint64SlowPath();
    }
    
    long readRawVarint64SlowPath() throws IOException {
      long l = 0L;
      for (byte b = 0; b < 64; b += 7) {
        byte b1 = readRawByte();
        l |= (b1 & Byte.MAX_VALUE) << b;
        if ((b1 & 0x80) == 0)
          return l; 
      } 
      throw InvalidProtocolBufferException.malformedVarint();
    }
    
    public int readRawLittleEndian32() throws IOException {
      if (currentRemaining() >= 4L) {
        long l = this.currentByteBufferPos;
        this.currentByteBufferPos += 4L;
        byte b5 = UnsafeUtil.getByte(l);
        byte b6 = UnsafeUtil.getByte(1L + l);
        byte b7 = UnsafeUtil.getByte(2L + l);
        byte b8 = UnsafeUtil.getByte(3L + l);
        return b5 & 0xFF | (b6 & 0xFF) << 8 | (b7 & 0xFF) << 16 | (b8 & 0xFF) << 24;
      } 
      byte b2 = readRawByte();
      byte b4 = readRawByte();
      byte b3 = readRawByte();
      byte b1 = readRawByte();
      return b2 & 0xFF | (b4 & 0xFF) << 8 | (b3 & 0xFF) << 16 | (b1 & 0xFF) << 24;
    }
    
    public long readRawLittleEndian64() throws IOException {
      if (currentRemaining() >= 8L) {
        long l9 = this.currentByteBufferPos;
        this.currentByteBufferPos += 8L;
        long l10 = UnsafeUtil.getByte(l9);
        long l11 = UnsafeUtil.getByte(1L + l9);
        long l12 = UnsafeUtil.getByte(2L + l9);
        long l13 = UnsafeUtil.getByte(3L + l9);
        long l14 = UnsafeUtil.getByte(4L + l9);
        long l15 = UnsafeUtil.getByte(5L + l9);
        long l16 = UnsafeUtil.getByte(6L + l9);
        l9 = UnsafeUtil.getByte(7L + l9);
        return (l9 & 0xFFL) << 56L | l10 & 0xFFL | (l11 & 0xFFL) << 8L | (l12 & 0xFFL) << 16L | (l13 & 0xFFL) << 24L | (l14 & 0xFFL) << 32L | (l15 & 0xFFL) << 40L | (l16 & 0xFFL) << 48L;
      } 
      long l1 = readRawByte();
      long l7 = readRawByte();
      long l2 = readRawByte();
      long l5 = readRawByte();
      long l8 = readRawByte();
      long l6 = readRawByte();
      long l4 = readRawByte();
      long l3 = readRawByte();
      return (l3 & 0xFFL) << 56L | l1 & 0xFFL | (l7 & 0xFFL) << 8L | (l2 & 0xFFL) << 16L | (l5 & 0xFFL) << 24L | (l8 & 0xFFL) << 32L | (l6 & 0xFFL) << 40L | (l4 & 0xFFL) << 48L;
    }
    
    public void enableAliasing(boolean param1Boolean) {
      this.enableAliasing = param1Boolean;
    }
    
    public void resetSizeCounter() {
      this.startOffset = (int)(this.totalBytesRead + this.currentByteBufferPos - this.currentByteBufferStartPos);
    }
    
    public int pushLimit(int param1Int) throws InvalidProtocolBufferException {
      if (param1Int >= 0) {
        param1Int += getTotalBytesRead();
        int i = this.currentLimit;
        if (param1Int <= i) {
          this.currentLimit = param1Int;
          recomputeBufferSizeAfterLimit();
          return i;
        } 
        throw InvalidProtocolBufferException.truncatedMessage();
      } 
      throw InvalidProtocolBufferException.negativeSize();
    }
    
    private void recomputeBufferSizeAfterLimit() {
      int i = this.totalBufferSize + this.bufferSizeAfterCurrentLimit;
      int j = i - this.startOffset;
      int k = this.currentLimit;
      if (j > k) {
        this.bufferSizeAfterCurrentLimit = j -= k;
        this.totalBufferSize = i - j;
      } else {
        this.bufferSizeAfterCurrentLimit = 0;
      } 
    }
    
    public void popLimit(int param1Int) {
      this.currentLimit = param1Int;
      recomputeBufferSizeAfterLimit();
    }
    
    public int getBytesUntilLimit() {
      int i = this.currentLimit;
      if (i == Integer.MAX_VALUE)
        return -1; 
      return i - getTotalBytesRead();
    }
    
    public boolean isAtEnd() throws IOException {
      boolean bool;
      if (this.totalBytesRead + this.currentByteBufferPos - this.currentByteBufferStartPos == this.totalBufferSize) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public int getTotalBytesRead() {
      return (int)((this.totalBytesRead - this.startOffset) + this.currentByteBufferPos - this.currentByteBufferStartPos);
    }
    
    public byte readRawByte() throws IOException {
      if (currentRemaining() == 0L)
        getNextByteBuffer(); 
      long l = this.currentByteBufferPos;
      this.currentByteBufferPos = 1L + l;
      return UnsafeUtil.getByte(l);
    }
    
    public byte[] readRawBytes(int param1Int) throws IOException {
      if (param1Int >= 0 && param1Int <= currentRemaining()) {
        byte[] arrayOfByte = new byte[param1Int];
        UnsafeUtil.copyMemory(this.currentByteBufferPos, arrayOfByte, 0L, param1Int);
        this.currentByteBufferPos += param1Int;
        return arrayOfByte;
      } 
      if (param1Int >= 0 && param1Int <= remaining()) {
        byte[] arrayOfByte = new byte[param1Int];
        readRawBytesTo(arrayOfByte, 0, param1Int);
        return arrayOfByte;
      } 
      if (param1Int <= 0) {
        if (param1Int == 0)
          return Internal.EMPTY_BYTE_ARRAY; 
        throw InvalidProtocolBufferException.negativeSize();
      } 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    private void readRawBytesTo(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws IOException {
      if (param1Int2 >= 0 && param1Int2 <= remaining()) {
        int i = param1Int2;
        while (i > 0) {
          if (currentRemaining() == 0L)
            getNextByteBuffer(); 
          int j = Math.min(i, (int)currentRemaining());
          UnsafeUtil.copyMemory(this.currentByteBufferPos, param1ArrayOfbyte, (param1Int2 - i + param1Int1), j);
          i -= j;
          this.currentByteBufferPos += j;
        } 
        return;
      } 
      if (param1Int2 <= 0) {
        if (param1Int2 == 0)
          return; 
        throw InvalidProtocolBufferException.negativeSize();
      } 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public void skipRawBytes(int param1Int) throws IOException {
      if (param1Int >= 0 && param1Int <= (this.totalBufferSize - this.totalBytesRead) - this.currentByteBufferPos + this.currentByteBufferStartPos) {
        while (param1Int > 0) {
          if (currentRemaining() == 0L)
            getNextByteBuffer(); 
          int i = Math.min(param1Int, (int)currentRemaining());
          param1Int -= i;
          this.currentByteBufferPos += i;
        } 
        return;
      } 
      if (param1Int < 0)
        throw InvalidProtocolBufferException.negativeSize(); 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    private void skipRawVarint() throws IOException {
      for (byte b = 0; b < 10; b++) {
        if (readRawByte() >= 0)
          return; 
      } 
      throw InvalidProtocolBufferException.malformedVarint();
    }
    
    private int remaining() {
      return (int)((this.totalBufferSize - this.totalBytesRead) - this.currentByteBufferPos + this.currentByteBufferStartPos);
    }
    
    private long currentRemaining() {
      return this.currentByteBufferLimit - this.currentByteBufferPos;
    }
    
    private ByteBuffer slice(int param1Int1, int param1Int2) throws IOException {
      Exception exception;
      int i = this.currentByteBuffer.position();
      int j = this.currentByteBuffer.limit();
      try {
        this.currentByteBuffer.position(param1Int1);
        this.currentByteBuffer.limit(param1Int2);
        ByteBuffer byteBuffer = this.currentByteBuffer.slice();
        this.currentByteBuffer.position(i);
        this.currentByteBuffer.limit(j);
        return byteBuffer;
      } catch (IllegalArgumentException null) {
        throw InvalidProtocolBufferException.truncatedMessage();
      } finally {}
      this.currentByteBuffer.position(i);
      this.currentByteBuffer.limit(j);
      throw exception;
    }
  }
}
