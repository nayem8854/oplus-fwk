package com.android.framework.protobuf;

public enum ProtoSyntax {
  PROTO2, PROTO3;
  
  private static final ProtoSyntax[] $VALUES;
  
  static {
    ProtoSyntax protoSyntax = new ProtoSyntax("PROTO3", 1);
    $VALUES = new ProtoSyntax[] { PROTO2, protoSyntax };
  }
}
