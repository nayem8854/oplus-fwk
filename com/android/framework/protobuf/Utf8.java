package com.android.framework.protobuf;

import java.nio.ByteBuffer;

final class Utf8 {
  private static final long ASCII_MASK_LONG = -9187201950435737472L;
  
  public static final int COMPLETE = 0;
  
  public static final int MALFORMED = -1;
  
  static final int MAX_BYTES_PER_CHAR = 3;
  
  private static final int UNSAFE_COUNT_ASCII_THRESHOLD = 16;
  
  private static final Processor processor;
  
  static {
    SafeProcessor safeProcessor;
    if (UnsafeProcessor.isAvailable() && !Android.isOnAndroidDevice()) {
      UnsafeProcessor unsafeProcessor = new UnsafeProcessor();
    } else {
      safeProcessor = new SafeProcessor();
    } 
    processor = safeProcessor;
  }
  
  public static boolean isValidUtf8(byte[] paramArrayOfbyte) {
    return processor.isValidUtf8(paramArrayOfbyte, 0, paramArrayOfbyte.length);
  }
  
  public static boolean isValidUtf8(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    return processor.isValidUtf8(paramArrayOfbyte, paramInt1, paramInt2);
  }
  
  public static int partialIsValidUtf8(int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3) {
    return processor.partialIsValidUtf8(paramInt1, paramArrayOfbyte, paramInt2, paramInt3);
  }
  
  private static int incompleteStateFor(int paramInt) {
    if (paramInt > -12)
      paramInt = -1; 
    return paramInt;
  }
  
  private static int incompleteStateFor(int paramInt1, int paramInt2) {
    return (paramInt1 > -12 || paramInt2 > -65) ? -1 : (paramInt2 << 8 ^ paramInt1);
  }
  
  private static int incompleteStateFor(int paramInt1, int paramInt2, int paramInt3) {
    return (paramInt1 > -12 || paramInt2 > -65 || paramInt3 > -65) ? 
      -1 : (
      paramInt2 << 8 ^ paramInt1 ^ paramInt3 << 16);
  }
  
  private static int incompleteStateFor(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    byte b = paramArrayOfbyte[paramInt1 - 1];
    paramInt2 -= paramInt1;
    if (paramInt2 != 0) {
      if (paramInt2 != 1) {
        if (paramInt2 == 2)
          return incompleteStateFor(b, paramArrayOfbyte[paramInt1], paramArrayOfbyte[paramInt1 + 1]); 
        throw new AssertionError();
      } 
      return incompleteStateFor(b, paramArrayOfbyte[paramInt1]);
    } 
    return incompleteStateFor(b);
  }
  
  private static int incompleteStateFor(ByteBuffer paramByteBuffer, int paramInt1, int paramInt2, int paramInt3) {
    if (paramInt3 != 0) {
      if (paramInt3 != 1) {
        if (paramInt3 == 2)
          return incompleteStateFor(paramInt1, paramByteBuffer.get(paramInt2), paramByteBuffer.get(paramInt2 + 1)); 
        throw new AssertionError();
      } 
      return incompleteStateFor(paramInt1, paramByteBuffer.get(paramInt2));
    } 
    return incompleteStateFor(paramInt1);
  }
  
  class UnpairedSurrogateException extends IllegalArgumentException {
    UnpairedSurrogateException(Utf8 this$0, int param1Int1) {
      super(stringBuilder.toString());
    }
  }
  
  static int encodedLength(CharSequence paramCharSequence) {
    int m, n, i = paramCharSequence.length();
    int j = i;
    int k = 0;
    while (true) {
      m = j;
      n = k;
      if (k < i) {
        m = j;
        n = k;
        if (paramCharSequence.charAt(k) < '') {
          k++;
          continue;
        } 
      } 
      break;
    } 
    while (true) {
      k = m;
      if (n < i) {
        k = paramCharSequence.charAt(n);
        if (k < 2048) {
          m += 127 - k >>> 31;
          n++;
        } 
        k = m + encodedLengthGeneral(paramCharSequence, n);
      } 
      break;
    } 
    if (k >= i)
      return k; 
    paramCharSequence = new StringBuilder();
    paramCharSequence.append("UTF-8 length does not fit in int: ");
    paramCharSequence.append(k + 4294967296L);
    throw new IllegalArgumentException(paramCharSequence.toString());
  }
  
  private static int encodedLengthGeneral(CharSequence paramCharSequence, int paramInt) {
    int i = paramCharSequence.length();
    int j = 0;
    for (; paramInt < i; paramInt = k + 1) {
      int k;
      char c = paramCharSequence.charAt(paramInt);
      if (c < 'ࠀ') {
        j += 127 - c >>> 31;
        k = paramInt;
      } else {
        int m = j + 2;
        j = m;
        k = paramInt;
        if ('?' <= c) {
          j = m;
          k = paramInt;
          if (c <= '?') {
            j = Character.codePointAt(paramCharSequence, paramInt);
            if (j >= 65536) {
              k = paramInt + 1;
              j = m;
            } else {
              throw new UnpairedSurrogateException(paramInt, i);
            } 
          } 
        } 
      } 
    } 
    return j;
  }
  
  static int encode(CharSequence paramCharSequence, byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    return processor.encodeUtf8(paramCharSequence, paramArrayOfbyte, paramInt1, paramInt2);
  }
  
  static boolean isValidUtf8(ByteBuffer paramByteBuffer) {
    return processor.isValidUtf8(paramByteBuffer, paramByteBuffer.position(), paramByteBuffer.remaining());
  }
  
  static int partialIsValidUtf8(int paramInt1, ByteBuffer paramByteBuffer, int paramInt2, int paramInt3) {
    return processor.partialIsValidUtf8(paramInt1, paramByteBuffer, paramInt2, paramInt3);
  }
  
  static String decodeUtf8(ByteBuffer paramByteBuffer, int paramInt1, int paramInt2) throws InvalidProtocolBufferException {
    return processor.decodeUtf8(paramByteBuffer, paramInt1, paramInt2);
  }
  
  static String decodeUtf8(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws InvalidProtocolBufferException {
    return processor.decodeUtf8(paramArrayOfbyte, paramInt1, paramInt2);
  }
  
  static void encodeUtf8(CharSequence paramCharSequence, ByteBuffer paramByteBuffer) {
    processor.encodeUtf8(paramCharSequence, paramByteBuffer);
  }
  
  private static int estimateConsecutiveAscii(ByteBuffer paramByteBuffer, int paramInt1, int paramInt2) {
    int i = paramInt1;
    for (; i < paramInt2 - 7 && (paramByteBuffer.getLong(i) & 0x8080808080808080L) == 0L; i += 8);
    return i - paramInt1;
  }
  
  class Processor {
    final boolean isValidUtf8(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) {
      boolean bool = false;
      if (partialIsValidUtf8(0, param1ArrayOfbyte, param1Int1, param1Int2) == 0)
        bool = true; 
      return bool;
    }
    
    final boolean isValidUtf8(ByteBuffer param1ByteBuffer, int param1Int1, int param1Int2) {
      boolean bool = false;
      if (partialIsValidUtf8(0, param1ByteBuffer, param1Int1, param1Int2) == 0)
        bool = true; 
      return bool;
    }
    
    final int partialIsValidUtf8(int param1Int1, ByteBuffer param1ByteBuffer, int param1Int2, int param1Int3) {
      if (param1ByteBuffer.hasArray()) {
        int i = param1ByteBuffer.arrayOffset();
        return partialIsValidUtf8(param1Int1, param1ByteBuffer.array(), i + param1Int2, i + param1Int3);
      } 
      if (param1ByteBuffer.isDirect())
        return partialIsValidUtf8Direct(param1Int1, param1ByteBuffer, param1Int2, param1Int3); 
      return partialIsValidUtf8Default(param1Int1, param1ByteBuffer, param1Int2, param1Int3);
    }
    
    final int partialIsValidUtf8Default(int param1Int1, ByteBuffer param1ByteBuffer, int param1Int2, int param1Int3) {
      int i = param1Int2;
      if (param1Int1 != 0) {
        if (param1Int2 >= param1Int3)
          return param1Int1; 
        byte b = (byte)param1Int1;
        if (b < -32) {
          if (b >= -62) {
            if (param1ByteBuffer.get(param1Int2) > -65)
              return -1; 
            i = param1Int2 + 1;
          } else {
            return -1;
          } 
        } else if (b < -16) {
          byte b1 = (byte)(param1Int1 >> 8 ^ 0xFFFFFFFF);
          i = b1;
          param1Int1 = param1Int2;
          if (b1 == 0) {
            param1Int1 = param1Int2 + 1;
            i = param1ByteBuffer.get(param1Int2);
            if (param1Int1 >= param1Int3)
              return Utf8.incompleteStateFor(b, i); 
          } 
          if (i <= -65 && (b != -32 || i >= -96) && (b != -19 || i < -96)) {
            if (param1ByteBuffer.get(param1Int1) > -65)
              return -1; 
            i = param1Int1 + 1;
          } else {
            return -1;
          } 
        } else {
          i = (byte)(param1Int1 >> 8 ^ 0xFFFFFFFF);
          int j = 0;
          if (i == 0) {
            param1Int1 = param1Int2 + 1;
            i = param1ByteBuffer.get(param1Int2);
            if (param1Int1 >= param1Int3)
              return Utf8.incompleteStateFor(b, i); 
            param1Int2 = param1Int1;
            param1Int1 = j;
          } else {
            param1Int1 = (byte)(param1Int1 >> 16);
          } 
          int k = param1Int1;
          j = param1Int2;
          if (param1Int1 == 0) {
            j = param1Int2 + 1;
            k = param1ByteBuffer.get(param1Int2);
            if (j >= param1Int3)
              return Utf8.incompleteStateFor(b, i, k); 
          } 
          if (i <= -65 && (b << 28) + i + 112 >> 30 == 0 && k <= -65) {
            if (param1ByteBuffer.get(j) > -65)
              return -1; 
            i = j + 1;
          } else {
            return -1;
          } 
        } 
      } 
      return partialIsValidUtf8(param1ByteBuffer, i, param1Int3);
    }
    
    private static int partialIsValidUtf8(ByteBuffer param1ByteBuffer, int param1Int1, int param1Int2) {
      param1Int1 += Utf8.estimateConsecutiveAscii(param1ByteBuffer, param1Int1, param1Int2);
      while (true) {
        if (param1Int1 >= param1Int2)
          return 0; 
        int i = param1Int1 + 1;
        param1Int1 = param1ByteBuffer.get(param1Int1);
        if (param1Int1 < 0) {
          if (param1Int1 < -32) {
            if (i >= param1Int2)
              return param1Int1; 
            if (param1Int1 < -62 || param1ByteBuffer.get(i) > -65)
              return -1; 
            param1Int1 = i + 1;
            continue;
          } 
          if (param1Int1 < -16) {
            if (i >= param1Int2 - 1)
              return Utf8.incompleteStateFor(param1ByteBuffer, param1Int1, i, param1Int2 - i); 
            int k = i + 1;
            i = param1ByteBuffer.get(i);
            if (i <= -65 && (param1Int1 != -32 || i >= -96) && (param1Int1 != -19 || i < -96))
              if (param1ByteBuffer.get(k) <= -65) {
                param1Int1 = k + 1;
                continue;
              }  
            return -1;
          } 
          if (i >= param1Int2 - 2)
            return Utf8.incompleteStateFor(param1ByteBuffer, param1Int1, i, param1Int2 - i); 
          int j = i + 1;
          i = param1ByteBuffer.get(i);
          if (i <= -65 && (param1Int1 << 28) + i + 112 >> 30 == 0) {
            i = j + 1;
            if (param1ByteBuffer.get(j) <= -65) {
              param1Int1 = i + 1;
              if (param1ByteBuffer.get(i) > -65)
                return -1; 
              continue;
            } 
          } 
          return -1;
        } 
        param1Int1 = i;
      } 
    }
    
    final String decodeUtf8(ByteBuffer param1ByteBuffer, int param1Int1, int param1Int2) throws InvalidProtocolBufferException {
      if (param1ByteBuffer.hasArray()) {
        int i = param1ByteBuffer.arrayOffset();
        return decodeUtf8(param1ByteBuffer.array(), i + param1Int1, param1Int2);
      } 
      if (param1ByteBuffer.isDirect())
        return decodeUtf8Direct(param1ByteBuffer, param1Int1, param1Int2); 
      return decodeUtf8Default(param1ByteBuffer, param1Int1, param1Int2);
    }
    
    final String decodeUtf8Default(ByteBuffer param1ByteBuffer, int param1Int1, int param1Int2) throws InvalidProtocolBufferException {
      if ((param1Int1 | param1Int2 | param1ByteBuffer.limit() - param1Int1 - param1Int2) >= 0) {
        int i = param1Int1 + param1Int2;
        char[] arrayOfChar = new char[param1Int2];
        param1Int2 = 0;
        while (param1Int1 < i) {
          byte b = param1ByteBuffer.get(param1Int1);
          if (!Utf8.DecodeUtil.isOneByte(b))
            break; 
          param1Int1++;
          Utf8.DecodeUtil.handleOneByte(b, arrayOfChar, param1Int2);
          param1Int2++;
        } 
        while (param1Int1 < i) {
          int j = param1Int1 + 1;
          byte b = param1ByteBuffer.get(param1Int1);
          if (Utf8.DecodeUtil.isOneByte(b)) {
            param1Int1 = param1Int2 + 1;
            Utf8.DecodeUtil.handleOneByte(b, arrayOfChar, param1Int2);
            param1Int2 = j;
            while (param1Int2 < i) {
              b = param1ByteBuffer.get(param1Int2);
              if (!Utf8.DecodeUtil.isOneByte(b))
                break; 
              param1Int2++;
              Utf8.DecodeUtil.handleOneByte(b, arrayOfChar, param1Int1);
              param1Int1++;
            } 
            j = param1Int2;
            param1Int2 = param1Int1;
            param1Int1 = j;
            continue;
          } 
          if (Utf8.DecodeUtil.isTwoBytes(b)) {
            if (j < i) {
              byte b1 = param1ByteBuffer.get(j);
              Utf8.DecodeUtil.handleTwoBytes(b, b1, arrayOfChar, param1Int2);
              param1Int1 = j + 1;
              param1Int2++;
              continue;
            } 
            throw InvalidProtocolBufferException.invalidUtf8();
          } 
          if (Utf8.DecodeUtil.isThreeBytes(b)) {
            if (j < i - 1) {
              param1Int1 = j + 1;
              byte b1 = param1ByteBuffer.get(j);
              byte b2 = param1ByteBuffer.get(param1Int1);
              Utf8.DecodeUtil.handleThreeBytes(b, b1, b2, arrayOfChar, param1Int2);
              param1Int1++;
              param1Int2++;
              continue;
            } 
            throw InvalidProtocolBufferException.invalidUtf8();
          } 
          if (j < i - 2) {
            param1Int1 = j + 1;
            byte b3 = param1ByteBuffer.get(j);
            j = param1Int1 + 1;
            byte b1 = param1ByteBuffer.get(param1Int1);
            byte b2 = param1ByteBuffer.get(j);
            Utf8.DecodeUtil.handleFourBytes(b, b3, b1, b2, arrayOfChar, param1Int2);
            param1Int1 = j + 1;
            param1Int2 = param1Int2 + 1 + 1;
            continue;
          } 
          throw InvalidProtocolBufferException.invalidUtf8();
        } 
        return new String(arrayOfChar, 0, param1Int2);
      } 
      throw new ArrayIndexOutOfBoundsException(String.format("buffer limit=%d, index=%d, limit=%d", new Object[] { Integer.valueOf(param1ByteBuffer.limit()), Integer.valueOf(param1Int1), Integer.valueOf(param1Int2) }));
    }
    
    final void encodeUtf8(CharSequence param1CharSequence, ByteBuffer param1ByteBuffer) {
      if (param1ByteBuffer.hasArray()) {
        int i = param1ByteBuffer.arrayOffset();
        int j = Utf8.encode(param1CharSequence, param1ByteBuffer.array(), param1ByteBuffer.position() + i, param1ByteBuffer.remaining());
        param1ByteBuffer.position(j - i);
      } else if (param1ByteBuffer.isDirect()) {
        encodeUtf8Direct(param1CharSequence, param1ByteBuffer);
      } else {
        encodeUtf8Default(param1CharSequence, param1ByteBuffer);
      } 
    }
    
    final void encodeUtf8Default(CharSequence param1CharSequence, ByteBuffer param1ByteBuffer) {
      int i = param1CharSequence.length();
      int j = param1ByteBuffer.position();
      byte b1 = 0;
      while (b1 < i) {
        int m = j;
        byte b = b1;
        try {
          char c = param1CharSequence.charAt(b1);
          if (c < '') {
            m = j;
            b = b1;
            param1ByteBuffer.put(j + b1, (byte)c);
            b1++;
          } 
        } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
          b1 = b;
          // Byte code: goto -> 633
        } 
      } 
      if (b1 == i) {
        int m = j;
        byte b = b1;
        param1ByteBuffer.position(j + b1);
        return;
      } 
      j += b1;
      for (; b1 < i; b1++, j++) {
        int m = j, n = b1;
        char c = param1CharSequence.charAt(b1);
        if (c < '') {
          m = j;
          n = b1;
          param1ByteBuffer.put(j, (byte)c);
          continue;
        } 
        if (c < 'ࠀ') {
          m = j + 1;
          byte b = (byte)(c >>> 6 | 0xC0);
          n = m;
          try {
            param1ByteBuffer.put(j, b);
            n = m;
            param1ByteBuffer.put(m, (byte)(c & 0x3F | 0x80));
            j = m;
          } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
            m = n;
          } 
          continue;
        } 
        if (c < '?' || '?' < c) {
          int i2 = j + 1;
          byte b = (byte)(c >>> 12 | 0xE0);
          n = i2;
          param1ByteBuffer.put(j, b);
          j = i2 + 1;
          b = (byte)(c >>> 6 & 0x3F | 0x80);
          m = j;
          n = b1;
          param1ByteBuffer.put(i2, b);
          m = j;
          n = b1;
          param1ByteBuffer.put(j, (byte)(c & 0x3F | 0x80));
          continue;
        } 
        int i1 = b1;
        if (b1 + 1 != i) {
          b1++;
          m = j;
          n = b1;
          char c1 = param1CharSequence.charAt(b1);
          m = j;
          n = b1;
          i1 = b1;
          if (Character.isSurrogatePair(c, c1)) {
            m = j;
            n = b1;
            int i2 = Character.toCodePoint(c, c1);
            i1 = j + 1;
            byte b = (byte)(i2 >>> 18 | 0xF0);
            n = i1;
            try {
              param1ByteBuffer.put(j, b);
              j = i1 + 1;
              b = (byte)(i2 >>> 12 & 0x3F | 0x80);
              m = j;
              n = b1;
              param1ByteBuffer.put(i1, b);
              m = j + 1;
              b = (byte)(i2 >>> 6 & 0x3F | 0x80);
              n = m;
              param1ByteBuffer.put(j, b);
              n = m;
              param1ByteBuffer.put(m, (byte)(i2 & 0x3F | 0x80));
              j = m;
            } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
              m = n;
              // Byte code: goto -> 633
            } 
            continue;
          } 
        } 
        m = j;
        n = i1;
        Utf8.UnpairedSurrogateException unpairedSurrogateException = new Utf8.UnpairedSurrogateException();
        m = j;
        n = i1;
        this(i1, i);
        m = j;
        n = i1;
        throw unpairedSurrogateException;
      } 
      int k = j;
      byte b2 = b1;
      param1ByteBuffer.position(j);
    }
    
    abstract String decodeUtf8(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws InvalidProtocolBufferException;
    
    abstract String decodeUtf8Direct(ByteBuffer param1ByteBuffer, int param1Int1, int param1Int2) throws InvalidProtocolBufferException;
    
    abstract int encodeUtf8(CharSequence param1CharSequence, byte[] param1ArrayOfbyte, int param1Int1, int param1Int2);
    
    abstract void encodeUtf8Direct(CharSequence param1CharSequence, ByteBuffer param1ByteBuffer);
    
    abstract int partialIsValidUtf8(int param1Int1, byte[] param1ArrayOfbyte, int param1Int2, int param1Int3);
    
    abstract int partialIsValidUtf8Direct(int param1Int1, ByteBuffer param1ByteBuffer, int param1Int2, int param1Int3);
  }
  
  class SafeProcessor extends Processor {
    int partialIsValidUtf8(int param1Int1, byte[] param1ArrayOfbyte, int param1Int2, int param1Int3) {
      int i = param1Int2;
      if (param1Int1 != 0) {
        if (param1Int2 >= param1Int3)
          return param1Int1; 
        byte b = (byte)param1Int1;
        if (b < -32) {
          if (b < -62 || param1ArrayOfbyte[param1Int2] > -65)
            return -1; 
          i = param1Int2 + 1;
        } else if (b < -16) {
          byte b1 = (byte)(param1Int1 >> 8 ^ 0xFFFFFFFF);
          i = b1;
          param1Int1 = param1Int2;
          if (b1 == 0) {
            param1Int1 = param1Int2 + 1;
            i = param1ArrayOfbyte[param1Int2];
            if (param1Int1 >= param1Int3)
              return Utf8.incompleteStateFor(b, i); 
          } 
          if (i > -65 || (b == -32 && i < -96) || (b == -19 && i >= -96) || param1ArrayOfbyte[param1Int1] > -65)
            return -1; 
          i = param1Int1 + 1;
        } else {
          i = (byte)(param1Int1 >> 8 ^ 0xFFFFFFFF);
          int j = 0;
          if (i == 0) {
            param1Int1 = param1Int2 + 1;
            i = param1ArrayOfbyte[param1Int2];
            if (param1Int1 >= param1Int3)
              return Utf8.incompleteStateFor(b, i); 
            param1Int2 = param1Int1;
            param1Int1 = j;
          } else {
            param1Int1 = (byte)(param1Int1 >> 16);
          } 
          int k = param1Int1;
          j = param1Int2;
          if (param1Int1 == 0) {
            j = param1Int2 + 1;
            k = param1ArrayOfbyte[param1Int2];
            if (j >= param1Int3)
              return Utf8.incompleteStateFor(b, i, k); 
          } 
          if (i > -65 || (b << 28) + i + 112 >> 30 != 0 || k > -65 || param1ArrayOfbyte[j] > -65)
            return -1; 
          i = j + 1;
        } 
      } 
      return partialIsValidUtf8(param1ArrayOfbyte, i, param1Int3);
    }
    
    int partialIsValidUtf8Direct(int param1Int1, ByteBuffer param1ByteBuffer, int param1Int2, int param1Int3) {
      return partialIsValidUtf8Default(param1Int1, param1ByteBuffer, param1Int2, param1Int3);
    }
    
    String decodeUtf8(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws InvalidProtocolBufferException {
      if ((param1Int1 | param1Int2 | param1ArrayOfbyte.length - param1Int1 - param1Int2) >= 0) {
        int j = param1Int1 + param1Int2;
        char[] arrayOfChar = new char[param1Int2];
        param1Int2 = 0;
        while (param1Int1 < j) {
          byte b = param1ArrayOfbyte[param1Int1];
          if (!Utf8.DecodeUtil.isOneByte(b))
            break; 
          param1Int1++;
          Utf8.DecodeUtil.handleOneByte(b, arrayOfChar, param1Int2);
          param1Int2++;
        } 
        while (param1Int1 < j) {
          int k = param1Int1 + 1;
          byte b = param1ArrayOfbyte[param1Int1];
          if (Utf8.DecodeUtil.isOneByte(b)) {
            param1Int1 = param1Int2 + 1;
            Utf8.DecodeUtil.handleOneByte(b, arrayOfChar, param1Int2);
            param1Int2 = k;
            while (param1Int2 < j) {
              b = param1ArrayOfbyte[param1Int2];
              if (!Utf8.DecodeUtil.isOneByte(b))
                break; 
              param1Int2++;
              Utf8.DecodeUtil.handleOneByte(b, arrayOfChar, param1Int1);
              param1Int1++;
            } 
            k = param1Int1;
            param1Int1 = param1Int2;
            param1Int2 = k;
            continue;
          } 
          if (Utf8.DecodeUtil.isTwoBytes(b)) {
            if (k < j) {
              Utf8.DecodeUtil.handleTwoBytes(b, param1ArrayOfbyte[k], arrayOfChar, param1Int2);
              param1Int1 = k + 1;
              param1Int2++;
              continue;
            } 
            throw InvalidProtocolBufferException.invalidUtf8();
          } 
          if (Utf8.DecodeUtil.isThreeBytes(b)) {
            if (k < j - 1) {
              param1Int1 = k + 1;
              Utf8.DecodeUtil.handleThreeBytes(b, param1ArrayOfbyte[k], param1ArrayOfbyte[param1Int1], arrayOfChar, param1Int2);
              param1Int1++;
              param1Int2++;
              continue;
            } 
            throw InvalidProtocolBufferException.invalidUtf8();
          } 
          if (k < j - 2) {
            param1Int1 = k + 1;
            byte b1 = param1ArrayOfbyte[k];
            k = param1Int1 + 1;
            Utf8.DecodeUtil.handleFourBytes(b, b1, param1ArrayOfbyte[param1Int1], param1ArrayOfbyte[k], arrayOfChar, param1Int2);
            param1Int1 = k + 1;
            param1Int2 = param1Int2 + 1 + 1;
            continue;
          } 
          throw InvalidProtocolBufferException.invalidUtf8();
        } 
        return new String(arrayOfChar, 0, param1Int2);
      } 
      int i = param1ArrayOfbyte.length;
      throw new ArrayIndexOutOfBoundsException(String.format("buffer length=%d, index=%d, size=%d", new Object[] { Integer.valueOf(i), Integer.valueOf(param1Int1), Integer.valueOf(param1Int2) }));
    }
    
    String decodeUtf8Direct(ByteBuffer param1ByteBuffer, int param1Int1, int param1Int2) throws InvalidProtocolBufferException {
      return decodeUtf8Default(param1ByteBuffer, param1Int1, param1Int2);
    }
    
    int encodeUtf8(CharSequence param1CharSequence, byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) {
      int i = param1CharSequence.length();
      int j = 0;
      int k = param1Int1 + param1Int2;
      param1Int2 = j;
      while (param1Int2 < i && param1Int2 + param1Int1 < k) {
        j = param1CharSequence.charAt(param1Int2);
        if (j < 128) {
          param1ArrayOfbyte[param1Int1 + param1Int2] = (byte)j;
          param1Int2++;
        } 
      } 
      if (param1Int2 == i)
        return param1Int1 + i; 
      param1Int1 += param1Int2;
      for (; param1Int2 < i; param1Int2++) {
        char c = param1CharSequence.charAt(param1Int2);
        if (c < '' && param1Int1 < k) {
          param1ArrayOfbyte[param1Int1] = (byte)c;
          param1Int1++;
        } else if (c < 'ࠀ' && param1Int1 <= k - 2) {
          j = param1Int1 + 1;
          param1ArrayOfbyte[param1Int1] = (byte)(c >>> 6 | 0x3C0);
          param1Int1 = j + 1;
          param1ArrayOfbyte[j] = (byte)(c & 0x3F | 0x80);
        } else if ((c < '?' || '?' < c) && param1Int1 <= k - 3) {
          j = param1Int1 + 1;
          param1ArrayOfbyte[param1Int1] = (byte)(c >>> 12 | 0x1E0);
          param1Int1 = j + 1;
          param1ArrayOfbyte[j] = (byte)(c >>> 6 & 0x3F | 0x80);
          param1ArrayOfbyte[param1Int1] = (byte)(c & 0x3F | 0x80);
          param1Int1++;
        } else if (param1Int1 <= k - 4) {
          j = param1Int2;
          if (param1Int2 + 1 != param1CharSequence.length()) {
            char c1 = param1CharSequence.charAt(++param1Int2);
            j = param1Int2;
            if (Character.isSurrogatePair(c, c1)) {
              j = Character.toCodePoint(c, c1);
              int m = param1Int1 + 1;
              param1ArrayOfbyte[param1Int1] = (byte)(j >>> 18 | 0xF0);
              param1Int1 = m + 1;
              param1ArrayOfbyte[m] = (byte)(j >>> 12 & 0x3F | 0x80);
              m = param1Int1 + 1;
              param1ArrayOfbyte[param1Int1] = (byte)(j >>> 6 & 0x3F | 0x80);
              param1Int1 = m + 1;
              param1ArrayOfbyte[m] = (byte)(j & 0x3F | 0x80);
            } else {
              throw new Utf8.UnpairedSurrogateException(j - 1, i);
            } 
          } else {
            throw new Utf8.UnpairedSurrogateException(j - 1, i);
          } 
        } else {
          if ('?' <= c && c <= '?' && (
            param1Int2 + 1 == param1CharSequence.length() || !Character.isSurrogatePair(c, param1CharSequence.charAt(param1Int2 + 1))))
            throw new Utf8.UnpairedSurrogateException(param1Int2, i); 
          param1CharSequence = new StringBuilder();
          param1CharSequence.append("Failed writing ");
          param1CharSequence.append(c);
          param1CharSequence.append(" at index ");
          param1CharSequence.append(param1Int1);
          throw new ArrayIndexOutOfBoundsException(param1CharSequence.toString());
        } 
      } 
      return param1Int1;
    }
    
    void encodeUtf8Direct(CharSequence param1CharSequence, ByteBuffer param1ByteBuffer) {
      encodeUtf8Default(param1CharSequence, param1ByteBuffer);
    }
    
    private static int partialIsValidUtf8(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) {
      while (param1Int1 < param1Int2 && param1ArrayOfbyte[param1Int1] >= 0)
        param1Int1++; 
      if (param1Int1 >= param1Int2) {
        param1Int1 = 0;
      } else {
        param1Int1 = partialIsValidUtf8NonAscii(param1ArrayOfbyte, param1Int1, param1Int2);
      } 
      return param1Int1;
    }
    
    private static int partialIsValidUtf8NonAscii(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) {
      while (true) {
        if (param1Int1 >= param1Int2)
          return 0; 
        int i = param1Int1 + 1;
        param1Int1 = param1ArrayOfbyte[param1Int1];
        if (param1Int1 < 0) {
          if (param1Int1 < -32) {
            if (i >= param1Int2)
              return param1Int1; 
            if (param1Int1 >= -62) {
              param1Int1 = i + 1;
              if (param1ArrayOfbyte[i] > -65)
                return -1; 
              continue;
            } 
            return -1;
          } 
          if (param1Int1 < -16) {
            if (i >= param1Int2 - 1)
              return Utf8.incompleteStateFor(param1ArrayOfbyte, i, param1Int2); 
            int k = i + 1;
            i = param1ArrayOfbyte[i];
            if (i <= -65 && (param1Int1 != -32 || i >= -96) && (param1Int1 != -19 || i < -96)) {
              param1Int1 = k + 1;
              if (param1ArrayOfbyte[k] > -65)
                return -1; 
              continue;
            } 
            return -1;
          } 
          if (i >= param1Int2 - 2)
            return Utf8.incompleteStateFor(param1ArrayOfbyte, i, param1Int2); 
          int j = i + 1;
          i = param1ArrayOfbyte[i];
          if (i <= -65 && (param1Int1 << 28) + i + 112 >> 30 == 0) {
            i = j + 1;
            if (param1ArrayOfbyte[j] <= -65) {
              param1Int1 = i + 1;
              if (param1ArrayOfbyte[i] > -65)
                return -1; 
              continue;
            } 
          } 
          return -1;
        } 
        param1Int1 = i;
      } 
    }
  }
  
  class UnsafeProcessor extends Processor {
    static boolean isAvailable() {
      boolean bool;
      if (UnsafeUtil.hasUnsafeArrayOperations() && UnsafeUtil.hasUnsafeByteBufferOperations()) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    int partialIsValidUtf8(int param1Int1, byte[] param1ArrayOfbyte, int param1Int2, int param1Int3) {
      if ((param1Int2 | param1Int3 | param1ArrayOfbyte.length - param1Int3) >= 0) {
        long l1 = param1Int2;
        long l2 = param1Int3;
        long l3 = l1;
        if (param1Int1 != 0) {
          if (l1 >= l2)
            return param1Int1; 
          byte b = (byte)param1Int1;
          if (b < -32) {
            if (b >= -62) {
              if (UnsafeUtil.getByte(param1ArrayOfbyte, l1) > -65)
                return -1; 
              l3 = 1L + l1;
            } else {
              return -1;
            } 
          } else if (b < -16) {
            param1Int2 = (byte)(param1Int1 >> 8 ^ 0xFFFFFFFF);
            l3 = l1;
            param1Int1 = param1Int2;
            if (param1Int2 == 0) {
              l3 = l1 + 1L;
              param1Int1 = UnsafeUtil.getByte(param1ArrayOfbyte, l1);
              if (l3 >= l2)
                return Utf8.incompleteStateFor(b, param1Int1); 
            } 
            if (param1Int1 <= -65 && (b != -32 || param1Int1 >= -96) && (b != -19 || param1Int1 < -96)) {
              if (UnsafeUtil.getByte(param1ArrayOfbyte, l3) > -65)
                return -1; 
              l3 = 1L + l3;
            } else {
              return -1;
            } 
          } else {
            param1Int2 = (byte)(param1Int1 >> 8 ^ 0xFFFFFFFF);
            param1Int3 = 0;
            if (param1Int2 == 0) {
              l3 = l1 + 1L;
              param1Int2 = UnsafeUtil.getByte(param1ArrayOfbyte, l1);
              if (l3 >= l2)
                return Utf8.incompleteStateFor(b, param1Int2); 
              l1 = l3;
              param1Int1 = param1Int3;
            } else {
              param1Int1 = (byte)(param1Int1 >> 16);
            } 
            l3 = l1;
            param1Int3 = param1Int1;
            if (param1Int1 == 0) {
              l3 = l1 + 1L;
              param1Int3 = UnsafeUtil.getByte(param1ArrayOfbyte, l1);
              if (l3 >= l2)
                return Utf8.incompleteStateFor(b, param1Int2, param1Int3); 
            } 
            if (param1Int2 <= -65 && (b << 28) + param1Int2 + 112 >> 30 == 0 && param1Int3 <= -65) {
              if (UnsafeUtil.getByte(param1ArrayOfbyte, l3) > -65)
                return -1; 
              l3 = 1L + l3;
            } else {
              return -1;
            } 
          } 
        } 
        return partialIsValidUtf8(param1ArrayOfbyte, l3, (int)(l2 - l3));
      } 
      param1Int1 = param1ArrayOfbyte.length;
      throw new ArrayIndexOutOfBoundsException(String.format("Array length=%d, index=%d, limit=%d", new Object[] { Integer.valueOf(param1Int1), Integer.valueOf(param1Int2), Integer.valueOf(param1Int3) }));
    }
    
    int partialIsValidUtf8Direct(int param1Int1, ByteBuffer param1ByteBuffer, int param1Int2, int param1Int3) {
      if ((param1Int2 | param1Int3 | param1ByteBuffer.limit() - param1Int3) >= 0) {
        long l1 = UnsafeUtil.addressOffset(param1ByteBuffer) + param1Int2;
        long l2 = (param1Int3 - param1Int2) + l1;
        long l3 = l1;
        if (param1Int1 != 0) {
          if (l1 >= l2)
            return param1Int1; 
          byte b = (byte)param1Int1;
          if (b < -32) {
            if (b >= -62) {
              if (UnsafeUtil.getByte(l1) > -65)
                return -1; 
              l3 = 1L + l1;
            } else {
              return -1;
            } 
          } else if (b < -16) {
            param1Int2 = (byte)(param1Int1 >> 8 ^ 0xFFFFFFFF);
            l3 = l1;
            param1Int1 = param1Int2;
            if (param1Int2 == 0) {
              l3 = l1 + 1L;
              param1Int1 = UnsafeUtil.getByte(l1);
              if (l3 >= l2)
                return Utf8.incompleteStateFor(b, param1Int1); 
            } 
            if (param1Int1 <= -65 && (b != -32 || param1Int1 >= -96) && (b != -19 || param1Int1 < -96)) {
              if (UnsafeUtil.getByte(l3) > -65)
                return -1; 
              l3 = 1L + l3;
            } else {
              return -1;
            } 
          } else {
            param1Int2 = (byte)(param1Int1 >> 8 ^ 0xFFFFFFFF);
            param1Int3 = 0;
            if (param1Int2 == 0) {
              l3 = l1 + 1L;
              param1Int2 = UnsafeUtil.getByte(l1);
              if (l3 >= l2)
                return Utf8.incompleteStateFor(b, param1Int2); 
              l1 = l3;
              param1Int1 = param1Int3;
            } else {
              param1Int1 = (byte)(param1Int1 >> 16);
            } 
            l3 = l1;
            param1Int3 = param1Int1;
            if (param1Int1 == 0) {
              l3 = l1 + 1L;
              param1Int3 = UnsafeUtil.getByte(l1);
              if (l3 >= l2)
                return Utf8.incompleteStateFor(b, param1Int2, param1Int3); 
            } 
            if (param1Int2 <= -65 && (b << 28) + param1Int2 + 112 >> 30 == 0 && param1Int3 <= -65) {
              if (UnsafeUtil.getByte(l3) > -65)
                return -1; 
              l3 = 1L + l3;
            } else {
              return -1;
            } 
          } 
        } 
        return partialIsValidUtf8(l3, (int)(l2 - l3));
      } 
      throw new ArrayIndexOutOfBoundsException(String.format("buffer limit=%d, index=%d, limit=%d", new Object[] { Integer.valueOf(param1ByteBuffer.limit()), Integer.valueOf(param1Int2), Integer.valueOf(param1Int3) }));
    }
    
    String decodeUtf8(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws InvalidProtocolBufferException {
      if ((param1Int1 | param1Int2 | param1ArrayOfbyte.length - param1Int1 - param1Int2) >= 0) {
        int j = param1Int1 + param1Int2;
        char[] arrayOfChar = new char[param1Int2];
        param1Int2 = 0;
        while (param1Int1 < j) {
          byte b = UnsafeUtil.getByte(param1ArrayOfbyte, param1Int1);
          if (!Utf8.DecodeUtil.isOneByte(b))
            break; 
          param1Int1++;
          Utf8.DecodeUtil.handleOneByte(b, arrayOfChar, param1Int2);
          param1Int2++;
        } 
        while (param1Int1 < j) {
          int k = param1Int1 + 1;
          byte b = UnsafeUtil.getByte(param1ArrayOfbyte, param1Int1);
          if (Utf8.DecodeUtil.isOneByte(b)) {
            param1Int1 = param1Int2 + 1;
            Utf8.DecodeUtil.handleOneByte(b, arrayOfChar, param1Int2);
            param1Int2 = k;
            while (param1Int2 < j) {
              b = UnsafeUtil.getByte(param1ArrayOfbyte, param1Int2);
              if (!Utf8.DecodeUtil.isOneByte(b))
                break; 
              param1Int2++;
              Utf8.DecodeUtil.handleOneByte(b, arrayOfChar, param1Int1);
              param1Int1++;
            } 
            k = param1Int1;
            param1Int1 = param1Int2;
            param1Int2 = k;
            continue;
          } 
          if (Utf8.DecodeUtil.isTwoBytes(b)) {
            if (k < j) {
              long l = k;
              byte b1 = UnsafeUtil.getByte(param1ArrayOfbyte, l);
              Utf8.DecodeUtil.handleTwoBytes(b, b1, arrayOfChar, param1Int2);
              param1Int1 = k + 1;
              param1Int2++;
              continue;
            } 
            throw InvalidProtocolBufferException.invalidUtf8();
          } 
          if (Utf8.DecodeUtil.isThreeBytes(b)) {
            if (k < j - 1) {
              param1Int1 = k + 1;
              long l = k;
              byte b2 = UnsafeUtil.getByte(param1ArrayOfbyte, l);
              l = param1Int1;
              byte b1 = UnsafeUtil.getByte(param1ArrayOfbyte, l);
              Utf8.DecodeUtil.handleThreeBytes(b, b2, b1, arrayOfChar, param1Int2);
              param1Int1++;
              param1Int2++;
              continue;
            } 
            throw InvalidProtocolBufferException.invalidUtf8();
          } 
          if (k < j - 2) {
            param1Int1 = k + 1;
            long l = k;
            byte b2 = UnsafeUtil.getByte(param1ArrayOfbyte, l);
            k = param1Int1 + 1;
            l = param1Int1;
            byte b3 = UnsafeUtil.getByte(param1ArrayOfbyte, l);
            l = k;
            byte b1 = UnsafeUtil.getByte(param1ArrayOfbyte, l);
            Utf8.DecodeUtil.handleFourBytes(b, b2, b3, b1, arrayOfChar, param1Int2);
            param1Int1 = k + 1;
            param1Int2 = param1Int2 + 1 + 1;
            continue;
          } 
          throw InvalidProtocolBufferException.invalidUtf8();
        } 
        return new String(arrayOfChar, 0, param1Int2);
      } 
      int i = param1ArrayOfbyte.length;
      throw new ArrayIndexOutOfBoundsException(String.format("buffer length=%d, index=%d, size=%d", new Object[] { Integer.valueOf(i), Integer.valueOf(param1Int1), Integer.valueOf(param1Int2) }));
    }
    
    String decodeUtf8Direct(ByteBuffer param1ByteBuffer, int param1Int1, int param1Int2) throws InvalidProtocolBufferException {
      char[] arrayOfChar;
      if ((param1Int1 | param1Int2 | param1ByteBuffer.limit() - param1Int1 - param1Int2) >= 0) {
        long l1 = UnsafeUtil.addressOffset(param1ByteBuffer) + param1Int1;
        long l2 = param1Int2 + l1;
        arrayOfChar = new char[param1Int2];
        param1Int1 = 0;
        while (l1 < l2) {
          byte b = UnsafeUtil.getByte(l1);
          if (!Utf8.DecodeUtil.isOneByte(b))
            break; 
          l1++;
          Utf8.DecodeUtil.handleOneByte(b, arrayOfChar, param1Int1);
          param1Int1++;
        } 
        while (l1 < l2) {
          long l = l1 + 1L;
          byte b = UnsafeUtil.getByte(l1);
          if (Utf8.DecodeUtil.isOneByte(b)) {
            param1Int2 = param1Int1 + 1;
            Utf8.DecodeUtil.handleOneByte(b, arrayOfChar, param1Int1);
            l1 = l;
            param1Int1 = param1Int2;
            while (l1 < l2) {
              b = UnsafeUtil.getByte(l1);
              if (!Utf8.DecodeUtil.isOneByte(b))
                break; 
              l1++;
              Utf8.DecodeUtil.handleOneByte(b, arrayOfChar, param1Int1);
              param1Int1++;
            } 
            continue;
          } 
          if (Utf8.DecodeUtil.isTwoBytes(b)) {
            if (l < l2) {
              byte b1 = UnsafeUtil.getByte(l);
              Utf8.DecodeUtil.handleTwoBytes(b, b1, arrayOfChar, param1Int1);
              param1Int1++;
              l1 = l + 1L;
              continue;
            } 
            throw InvalidProtocolBufferException.invalidUtf8();
          } 
          if (Utf8.DecodeUtil.isThreeBytes(b)) {
            if (l < l2 - 1L) {
              l1 = l + 1L;
              byte b2 = UnsafeUtil.getByte(l);
              byte b1 = UnsafeUtil.getByte(l1);
              Utf8.DecodeUtil.handleThreeBytes(b, b2, b1, arrayOfChar, param1Int1);
              l1++;
              param1Int1++;
              continue;
            } 
            throw InvalidProtocolBufferException.invalidUtf8();
          } 
          if (l < l2 - 2L) {
            l1 = l + 1L;
            byte b2 = UnsafeUtil.getByte(l);
            l = l1 + 1L;
            byte b3 = UnsafeUtil.getByte(l1);
            byte b1 = UnsafeUtil.getByte(l);
            Utf8.DecodeUtil.handleFourBytes(b, b2, b3, b1, arrayOfChar, param1Int1);
            param1Int1 = param1Int1 + 1 + 1;
            l1 = l + 1L;
            continue;
          } 
          throw InvalidProtocolBufferException.invalidUtf8();
        } 
        return new String(arrayOfChar, 0, param1Int1);
      } 
      throw new ArrayIndexOutOfBoundsException(String.format("buffer limit=%d, index=%d, limit=%d", new Object[] { Integer.valueOf(arrayOfChar.limit()), Integer.valueOf(param1Int1), Integer.valueOf(param1Int2) }));
    }
    
    int encodeUtf8(CharSequence param1CharSequence, byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) {
      long l1 = param1Int1;
      long l2 = param1Int2 + l1;
      int i = param1CharSequence.length();
      if (i <= param1Int2 && param1ArrayOfbyte.length - param1Int2 >= param1Int1) {
        param1Int2 = 0;
        while (param1Int2 < i) {
          param1Int1 = param1CharSequence.charAt(param1Int2);
          if (param1Int1 < 128) {
            UnsafeUtil.putByte(param1ArrayOfbyte, l1, (byte)param1Int1);
            param1Int2++;
            l1++;
          } 
        } 
        long l = l1;
        param1Int1 = param1Int2;
        if (param1Int2 == i)
          return (int)l1; 
        for (; param1Int1 < i; param1Int1++, l = l1) {
          char c = param1CharSequence.charAt(param1Int1);
          if (c < '' && l < l2) {
            UnsafeUtil.putByte(param1ArrayOfbyte, l, (byte)c);
            l1 = l + 1L;
          } else if (c < 'ࠀ' && l <= l2 - 2L) {
            long l3 = l + 1L;
            UnsafeUtil.putByte(param1ArrayOfbyte, l, (byte)(c >>> 6 | 0x3C0));
            l1 = l3 + 1L;
            UnsafeUtil.putByte(param1ArrayOfbyte, l3, (byte)(c & 0x3F | 0x80));
          } else if ((c < '?' || '?' < c) && l <= l2 - 3L) {
            l1 = l + 1L;
            UnsafeUtil.putByte(param1ArrayOfbyte, l, (byte)(c >>> 12 | 0x1E0));
            l = l1 + 1L;
            UnsafeUtil.putByte(param1ArrayOfbyte, l1, (byte)(c >>> 6 & 0x3F | 0x80));
            UnsafeUtil.putByte(param1ArrayOfbyte, l, (byte)(c & 0x3F | 0x80));
            l1 = l + 1L;
          } else if (l <= l2 - 4L) {
            if (param1Int1 + 1 != i) {
              char c1 = param1CharSequence.charAt(++param1Int1);
              if (Character.isSurrogatePair(c, c1)) {
                param1Int2 = Character.toCodePoint(c, c1);
                long l3 = l + 1L;
                UnsafeUtil.putByte(param1ArrayOfbyte, l, (byte)(param1Int2 >>> 18 | 0xF0));
                l1 = l3 + 1L;
                UnsafeUtil.putByte(param1ArrayOfbyte, l3, (byte)(param1Int2 >>> 12 & 0x3F | 0x80));
                l = l1 + 1L;
                UnsafeUtil.putByte(param1ArrayOfbyte, l1, (byte)(param1Int2 >>> 6 & 0x3F | 0x80));
                l1 = l + 1L;
                UnsafeUtil.putByte(param1ArrayOfbyte, l, (byte)(param1Int2 & 0x3F | 0x80));
              } else {
                throw new Utf8.UnpairedSurrogateException(param1Int1 - 1, i);
              } 
            } else {
              throw new Utf8.UnpairedSurrogateException(param1Int1 - 1, i);
            } 
          } else {
            if ('?' <= c && c <= '?' && (param1Int1 + 1 == i || 
              !Character.isSurrogatePair(c, param1CharSequence.charAt(param1Int1 + 1))))
              throw new Utf8.UnpairedSurrogateException(param1Int1, i); 
            param1CharSequence = new StringBuilder();
            param1CharSequence.append("Failed writing ");
            param1CharSequence.append(c);
            param1CharSequence.append(" at index ");
            param1CharSequence.append(l);
            throw new ArrayIndexOutOfBoundsException(param1CharSequence.toString());
          } 
        } 
        return (int)l;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed writing ");
      stringBuilder.append(param1CharSequence.charAt(i - 1));
      stringBuilder.append(" at index ");
      stringBuilder.append(param1Int1 + param1Int2);
      throw new ArrayIndexOutOfBoundsException(stringBuilder.toString());
    }
    
    void encodeUtf8Direct(CharSequence param1CharSequence, ByteBuffer param1ByteBuffer) {
      long l1 = UnsafeUtil.addressOffset(param1ByteBuffer);
      long l2 = param1ByteBuffer.position() + l1;
      long l3 = param1ByteBuffer.limit() + l1;
      int i = param1CharSequence.length();
      if (i <= l3 - l2) {
        int j = 0;
        while (true) {
          l4 = 1L;
          if (j < i) {
            char c = param1CharSequence.charAt(j);
            if (c < '') {
              UnsafeUtil.putByte(l2, (byte)c);
              j++;
              l2++;
              continue;
            } 
          } 
          break;
        } 
        long l5 = l1, l6 = l2;
        int k = j;
        if (j == i) {
          param1ByteBuffer.position((int)(l2 - l1));
          return;
        } 
        long l4;
        for (; k < i; k++, l6 = l4, l4 = l2) {
          char c = param1CharSequence.charAt(k);
          if (c < '' && l6 < l3) {
            UnsafeUtil.putByte(l6, (byte)c);
            l2 = l4;
            l4 = l6 + l4;
          } else if (c < 'ࠀ' && l6 <= l3 - 2L) {
            l2 = l6 + l4;
            UnsafeUtil.putByte(l6, (byte)(c >>> 6 | 0x3C0));
            UnsafeUtil.putByte(l2, (byte)(c & 0x3F | 0x80));
            l6 = l2 + l4;
            l2 = l4;
            l4 = l6;
          } else if ((c < '?' || '?' < c) && l6 <= l3 - 3L) {
            l2 = l6 + l4;
            UnsafeUtil.putByte(l6, (byte)(c >>> 12 | 0x1E0));
            l6 = l2 + l4;
            UnsafeUtil.putByte(l2, (byte)(c >>> 6 & 0x3F | 0x80));
            UnsafeUtil.putByte(l6, (byte)(c & 0x3F | 0x80));
            l6 += l4;
            l2 = l4;
            l4 = l6;
          } else if (l6 <= l3 - 4L) {
            if (k + 1 != i) {
              char c1 = param1CharSequence.charAt(++k);
              if (Character.isSurrogatePair(c, c1)) {
                j = Character.toCodePoint(c, c1);
                l2 = l6 + l4;
                UnsafeUtil.putByte(l6, (byte)(j >>> 18 | 0xF0));
                l4 = l2 + 1L;
                UnsafeUtil.putByte(l2, (byte)(j >>> 12 & 0x3F | 0x80));
                l6 = l4 + 1L;
                UnsafeUtil.putByte(l4, (byte)(j >>> 6 & 0x3F | 0x80));
                l2 = 1L;
                l4 = l6 + 1L;
                UnsafeUtil.putByte(l6, (byte)(j & 0x3F | 0x80));
              } else {
                throw new Utf8.UnpairedSurrogateException(k - 1, i);
              } 
            } else {
              throw new Utf8.UnpairedSurrogateException(k - 1, i);
            } 
          } else {
            if ('?' <= c && c <= '?' && (k + 1 == i || 
              !Character.isSurrogatePair(c, param1CharSequence.charAt(k + 1))))
              throw new Utf8.UnpairedSurrogateException(k, i); 
            param1CharSequence = new StringBuilder();
            param1CharSequence.append("Failed writing ");
            param1CharSequence.append(c);
            param1CharSequence.append(" at index ");
            param1CharSequence.append(l6);
            throw new ArrayIndexOutOfBoundsException(param1CharSequence.toString());
          } 
        } 
        param1ByteBuffer.position((int)(l6 - l5));
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed writing ");
      stringBuilder.append(param1CharSequence.charAt(i - 1));
      stringBuilder.append(" at index ");
      stringBuilder.append(param1ByteBuffer.limit());
      throw new ArrayIndexOutOfBoundsException(stringBuilder.toString());
    }
    
    private static int unsafeEstimateConsecutiveAscii(byte[] param1ArrayOfbyte, long param1Long, int param1Int) {
      if (param1Int < 16)
        return 0; 
      for (byte b = 0; b < param1Int; b++, param1Long = 1L + param1Long) {
        if (UnsafeUtil.getByte(param1ArrayOfbyte, param1Long) < 0)
          return b; 
      } 
      return param1Int;
    }
    
    private static int unsafeEstimateConsecutiveAscii(long param1Long, int param1Int) {
      if (param1Int < 16)
        return 0; 
      int i = 8 - ((int)param1Long & 0x7);
      int j;
      for (j = i; j > 0; j--, param1Long = 1L + param1Long) {
        if (UnsafeUtil.getByte(param1Long) < 0)
          return i - j; 
      } 
      j = param1Int - i;
      while (j >= 8 && (UnsafeUtil.getLong(param1Long) & 0x8080808080808080L) == 0L) {
        param1Long += 8L;
        j -= 8;
      } 
      return param1Int - j;
    }
    
    private static int partialIsValidUtf8(byte[] param1ArrayOfbyte, long param1Long, int param1Int) {
      int i = unsafeEstimateConsecutiveAscii(param1ArrayOfbyte, param1Long, param1Int);
      param1Int -= i;
      param1Long += i;
      while (true) {
        long l;
        byte b = 0;
        i = param1Int;
        param1Int = b;
        while (true) {
          l = param1Long;
          if (i > 0) {
            l = param1Long + 1L;
            param1Int = b = UnsafeUtil.getByte(param1ArrayOfbyte, param1Long);
            if (b >= 0) {
              i--;
              param1Long = l;
              continue;
            } 
          } 
          break;
        } 
        if (i == 0)
          return 0; 
        i--;
        if (param1Int < -32) {
          if (i == 0)
            return param1Int; 
          i--;
          if (param1Int < -62 || UnsafeUtil.getByte(param1ArrayOfbyte, l) > -65)
            return -1; 
          param1Long = 1L + l;
          param1Int = i;
          continue;
        } 
        if (param1Int < -16) {
          if (i < 2)
            return unsafeIncompleteStateFor(param1ArrayOfbyte, param1Int, l, i); 
          i -= 2;
          param1Long = l + 1L;
          b = UnsafeUtil.getByte(param1ArrayOfbyte, l);
          if (b <= -65 && (param1Int != -32 || b >= -96) && (param1Int != -19 || b < -96))
            if (UnsafeUtil.getByte(param1ArrayOfbyte, param1Long) <= -65) {
              param1Long = 1L + param1Long;
              param1Int = i;
              continue;
            }  
          return -1;
        } 
        if (i < 3)
          return unsafeIncompleteStateFor(param1ArrayOfbyte, param1Int, l, i); 
        i -= 3;
        param1Long = l + 1L;
        b = UnsafeUtil.getByte(param1ArrayOfbyte, l);
        if (b <= -65 && (param1Int << 28) + b + 112 >> 30 == 0) {
          l = param1Long + 1L;
          if (UnsafeUtil.getByte(param1ArrayOfbyte, param1Long) <= -65) {
            if (UnsafeUtil.getByte(param1ArrayOfbyte, l) > -65)
              break; 
            param1Long = l + 1L;
            param1Int = i;
            continue;
          } 
        } 
        break;
      } 
      return -1;
    }
    
    private static int partialIsValidUtf8(long param1Long, int param1Int) {
      int i = unsafeEstimateConsecutiveAscii(param1Long, param1Int);
      param1Long += i;
      param1Int -= i;
      while (true) {
        long l;
        byte b = 0;
        i = param1Int;
        param1Int = b;
        while (true) {
          l = param1Long;
          if (i > 0) {
            l = param1Long + 1L;
            param1Int = b = UnsafeUtil.getByte(param1Long);
            if (b >= 0) {
              i--;
              param1Long = l;
              continue;
            } 
          } 
          break;
        } 
        if (i == 0)
          return 0; 
        i--;
        if (param1Int < -32) {
          if (i == 0)
            return param1Int; 
          i--;
          if (param1Int < -62 || UnsafeUtil.getByte(l) > -65)
            return -1; 
          param1Long = 1L + l;
          param1Int = i;
          continue;
        } 
        if (param1Int < -16) {
          if (i < 2)
            return unsafeIncompleteStateFor(l, param1Int, i); 
          i -= 2;
          param1Long = l + 1L;
          b = UnsafeUtil.getByte(l);
          if (b <= -65 && (param1Int != -32 || b >= -96) && (param1Int != -19 || b < -96))
            if (UnsafeUtil.getByte(param1Long) <= -65) {
              param1Long = 1L + param1Long;
              param1Int = i;
              continue;
            }  
          return -1;
        } 
        if (i < 3)
          return unsafeIncompleteStateFor(l, param1Int, i); 
        i -= 3;
        param1Long = l + 1L;
        b = UnsafeUtil.getByte(l);
        if (b <= -65 && (param1Int << 28) + b + 112 >> 30 == 0) {
          l = param1Long + 1L;
          if (UnsafeUtil.getByte(param1Long) <= -65) {
            if (UnsafeUtil.getByte(l) > -65)
              break; 
            param1Long = l + 1L;
            param1Int = i;
            continue;
          } 
        } 
        break;
      } 
      return -1;
    }
    
    private static int unsafeIncompleteStateFor(byte[] param1ArrayOfbyte, int param1Int1, long param1Long, int param1Int2) {
      if (param1Int2 != 0) {
        if (param1Int2 != 1) {
          if (param1Int2 == 2) {
            param1Int2 = UnsafeUtil.getByte(param1ArrayOfbyte, param1Long);
            byte b = UnsafeUtil.getByte(param1ArrayOfbyte, 1L + param1Long);
            return Utf8.incompleteStateFor(param1Int1, param1Int2, b);
          } 
          throw new AssertionError();
        } 
        return Utf8.incompleteStateFor(param1Int1, UnsafeUtil.getByte(param1ArrayOfbyte, param1Long));
      } 
      return Utf8.incompleteStateFor(param1Int1);
    }
    
    private static int unsafeIncompleteStateFor(long param1Long, int param1Int1, int param1Int2) {
      if (param1Int2 != 0) {
        if (param1Int2 != 1) {
          if (param1Int2 == 2) {
            param1Int2 = UnsafeUtil.getByte(param1Long);
            byte b = UnsafeUtil.getByte(1L + param1Long);
            return Utf8.incompleteStateFor(param1Int1, param1Int2, b);
          } 
          throw new AssertionError();
        } 
        return Utf8.incompleteStateFor(param1Int1, UnsafeUtil.getByte(param1Long));
      } 
      return Utf8.incompleteStateFor(param1Int1);
    }
  }
  
  class DecodeUtil {
    private static boolean isOneByte(byte param1Byte) {
      boolean bool;
      if (param1Byte >= 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    private static boolean isTwoBytes(byte param1Byte) {
      boolean bool;
      if (param1Byte < -32) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    private static boolean isThreeBytes(byte param1Byte) {
      boolean bool;
      if (param1Byte < -16) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    private static void handleOneByte(byte param1Byte, char[] param1ArrayOfchar, int param1Int) {
      param1ArrayOfchar[param1Int] = (char)param1Byte;
    }
    
    private static void handleTwoBytes(byte param1Byte1, byte param1Byte2, char[] param1ArrayOfchar, int param1Int) throws InvalidProtocolBufferException {
      if (param1Byte1 >= -62 && !isNotTrailingByte(param1Byte2)) {
        param1ArrayOfchar[param1Int] = (char)((param1Byte1 & 0x1F) << 6 | trailingByteValue(param1Byte2));
        return;
      } 
      throw InvalidProtocolBufferException.invalidUtf8();
    }
    
    private static void handleThreeBytes(byte param1Byte1, byte param1Byte2, byte param1Byte3, char[] param1ArrayOfchar, int param1Int) throws InvalidProtocolBufferException {
      if (!isNotTrailingByte(param1Byte2) && (param1Byte1 != -32 || param1Byte2 >= -96) && (param1Byte1 != -19 || param1Byte2 < -96))
        if (!isNotTrailingByte(param1Byte3)) {
          param1ArrayOfchar[param1Int] = (char)((param1Byte1 & 0xF) << 12 | trailingByteValue(param1Byte2) << 6 | trailingByteValue(param1Byte3));
          return;
        }  
      throw InvalidProtocolBufferException.invalidUtf8();
    }
    
    private static void handleFourBytes(byte param1Byte1, byte param1Byte2, byte param1Byte3, byte param1Byte4, char[] param1ArrayOfchar, int param1Int) throws InvalidProtocolBufferException {
      if (!isNotTrailingByte(param1Byte2) && (param1Byte1 << 28) + param1Byte2 + 112 >> 30 == 0)
        if (!isNotTrailingByte(param1Byte3) && 
          !isNotTrailingByte(param1Byte4)) {
          int j = trailingByteValue(param1Byte2);
          int k = trailingByteValue(param1Byte3);
          int i = (param1Byte1 & 0x7) << 18 | j << 12 | k << 6 | trailingByteValue(param1Byte4);
          param1ArrayOfchar[param1Int] = highSurrogate(i);
          param1ArrayOfchar[param1Int + 1] = lowSurrogate(i);
          return;
        }  
      throw InvalidProtocolBufferException.invalidUtf8();
    }
    
    private static boolean isNotTrailingByte(byte param1Byte) {
      boolean bool;
      if (param1Byte > -65) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    private static int trailingByteValue(byte param1Byte) {
      return param1Byte & 0x3F;
    }
    
    private static char highSurrogate(int param1Int) {
      return (char)((param1Int >>> 10) + 55232);
    }
    
    private static char lowSurrogate(int param1Int) {
      return (char)((param1Int & 0x3FF) + 56320);
    }
  }
}
