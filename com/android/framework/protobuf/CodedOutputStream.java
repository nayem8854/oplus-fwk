package com.android.framework.protobuf;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class CodedOutputStream extends ByteOutput {
  public static final int DEFAULT_BUFFER_SIZE = 4096;
  
  private static final boolean HAS_UNSAFE_ARRAY_OPERATIONS;
  
  @Deprecated
  public static final int LITTLE_ENDIAN_32_SIZE = 4;
  
  private static final Logger logger = Logger.getLogger(CodedOutputStream.class.getName());
  
  private boolean serializationDeterministic;
  
  CodedOutputStreamWriter wrapper;
  
  static {
    HAS_UNSAFE_ARRAY_OPERATIONS = UnsafeUtil.hasUnsafeArrayOperations();
  }
  
  static int computePreferredBufferSize(int paramInt) {
    if (paramInt > 4096)
      return 4096; 
    return paramInt;
  }
  
  public static CodedOutputStream newInstance(OutputStream paramOutputStream) {
    return newInstance(paramOutputStream, 4096);
  }
  
  public static CodedOutputStream newInstance(OutputStream paramOutputStream, int paramInt) {
    return new OutputStreamEncoder(paramOutputStream, paramInt);
  }
  
  public static CodedOutputStream newInstance(byte[] paramArrayOfbyte) {
    return newInstance(paramArrayOfbyte, 0, paramArrayOfbyte.length);
  }
  
  public static CodedOutputStream newInstance(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    return new ArrayEncoder(paramArrayOfbyte, paramInt1, paramInt2);
  }
  
  public static CodedOutputStream newInstance(ByteBuffer paramByteBuffer) {
    if (paramByteBuffer.hasArray())
      return new HeapNioEncoder(paramByteBuffer); 
    if (paramByteBuffer.isDirect() && !paramByteBuffer.isReadOnly()) {
      CodedOutputStream codedOutputStream;
      if (UnsafeDirectNioEncoder.isSupported()) {
        codedOutputStream = newUnsafeInstance(paramByteBuffer);
      } else {
        codedOutputStream = newSafeInstance((ByteBuffer)codedOutputStream);
      } 
      return codedOutputStream;
    } 
    throw new IllegalArgumentException("ByteBuffer is read-only");
  }
  
  static CodedOutputStream newUnsafeInstance(ByteBuffer paramByteBuffer) {
    return new UnsafeDirectNioEncoder(paramByteBuffer);
  }
  
  static CodedOutputStream newSafeInstance(ByteBuffer paramByteBuffer) {
    return new SafeDirectNioEncoder(paramByteBuffer);
  }
  
  public void useDeterministicSerialization() {
    this.serializationDeterministic = true;
  }
  
  boolean isSerializationDeterministic() {
    return this.serializationDeterministic;
  }
  
  @Deprecated
  public static CodedOutputStream newInstance(ByteBuffer paramByteBuffer, int paramInt) {
    return newInstance(paramByteBuffer);
  }
  
  static CodedOutputStream newInstance(ByteOutput paramByteOutput, int paramInt) {
    if (paramInt >= 0)
      return new ByteOutputEncoder(paramByteOutput, paramInt); 
    throw new IllegalArgumentException("bufferSize must be positive");
  }
  
  private CodedOutputStream() {}
  
  public final void writeSInt32(int paramInt1, int paramInt2) throws IOException {
    writeUInt32(paramInt1, encodeZigZag32(paramInt2));
  }
  
  public final void writeSFixed32(int paramInt1, int paramInt2) throws IOException {
    writeFixed32(paramInt1, paramInt2);
  }
  
  public final void writeInt64(int paramInt, long paramLong) throws IOException {
    writeUInt64(paramInt, paramLong);
  }
  
  public final void writeSInt64(int paramInt, long paramLong) throws IOException {
    writeUInt64(paramInt, encodeZigZag64(paramLong));
  }
  
  public final void writeSFixed64(int paramInt, long paramLong) throws IOException {
    writeFixed64(paramInt, paramLong);
  }
  
  public final void writeFloat(int paramInt, float paramFloat) throws IOException {
    writeFixed32(paramInt, Float.floatToRawIntBits(paramFloat));
  }
  
  public final void writeDouble(int paramInt, double paramDouble) throws IOException {
    writeFixed64(paramInt, Double.doubleToRawLongBits(paramDouble));
  }
  
  public final void writeEnum(int paramInt1, int paramInt2) throws IOException {
    writeInt32(paramInt1, paramInt2);
  }
  
  public final void writeRawByte(byte paramByte) throws IOException {
    write(paramByte);
  }
  
  public final void writeRawByte(int paramInt) throws IOException {
    write((byte)paramInt);
  }
  
  public final void writeRawBytes(byte[] paramArrayOfbyte) throws IOException {
    write(paramArrayOfbyte, 0, paramArrayOfbyte.length);
  }
  
  public final void writeRawBytes(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws IOException {
    write(paramArrayOfbyte, paramInt1, paramInt2);
  }
  
  public final void writeRawBytes(ByteString paramByteString) throws IOException {
    paramByteString.writeTo(this);
  }
  
  public final void writeSInt32NoTag(int paramInt) throws IOException {
    writeUInt32NoTag(encodeZigZag32(paramInt));
  }
  
  public final void writeSFixed32NoTag(int paramInt) throws IOException {
    writeFixed32NoTag(paramInt);
  }
  
  public final void writeInt64NoTag(long paramLong) throws IOException {
    writeUInt64NoTag(paramLong);
  }
  
  public final void writeSInt64NoTag(long paramLong) throws IOException {
    writeUInt64NoTag(encodeZigZag64(paramLong));
  }
  
  public final void writeSFixed64NoTag(long paramLong) throws IOException {
    writeFixed64NoTag(paramLong);
  }
  
  public final void writeFloatNoTag(float paramFloat) throws IOException {
    writeFixed32NoTag(Float.floatToRawIntBits(paramFloat));
  }
  
  public final void writeDoubleNoTag(double paramDouble) throws IOException {
    writeFixed64NoTag(Double.doubleToRawLongBits(paramDouble));
  }
  
  public final void writeBoolNoTag(boolean paramBoolean) throws IOException {
    write((byte)paramBoolean);
  }
  
  public final void writeEnumNoTag(int paramInt) throws IOException {
    writeInt32NoTag(paramInt);
  }
  
  public final void writeByteArrayNoTag(byte[] paramArrayOfbyte) throws IOException {
    writeByteArrayNoTag(paramArrayOfbyte, 0, paramArrayOfbyte.length);
  }
  
  public static int computeInt32Size(int paramInt1, int paramInt2) {
    return computeTagSize(paramInt1) + computeInt32SizeNoTag(paramInt2);
  }
  
  public static int computeUInt32Size(int paramInt1, int paramInt2) {
    return computeTagSize(paramInt1) + computeUInt32SizeNoTag(paramInt2);
  }
  
  public static int computeSInt32Size(int paramInt1, int paramInt2) {
    return computeTagSize(paramInt1) + computeSInt32SizeNoTag(paramInt2);
  }
  
  public static int computeFixed32Size(int paramInt1, int paramInt2) {
    return computeTagSize(paramInt1) + computeFixed32SizeNoTag(paramInt2);
  }
  
  public static int computeSFixed32Size(int paramInt1, int paramInt2) {
    return computeTagSize(paramInt1) + computeSFixed32SizeNoTag(paramInt2);
  }
  
  public static int computeInt64Size(int paramInt, long paramLong) {
    return computeTagSize(paramInt) + computeInt64SizeNoTag(paramLong);
  }
  
  public static int computeUInt64Size(int paramInt, long paramLong) {
    return computeTagSize(paramInt) + computeUInt64SizeNoTag(paramLong);
  }
  
  public static int computeSInt64Size(int paramInt, long paramLong) {
    return computeTagSize(paramInt) + computeSInt64SizeNoTag(paramLong);
  }
  
  public static int computeFixed64Size(int paramInt, long paramLong) {
    return computeTagSize(paramInt) + computeFixed64SizeNoTag(paramLong);
  }
  
  public static int computeSFixed64Size(int paramInt, long paramLong) {
    return computeTagSize(paramInt) + computeSFixed64SizeNoTag(paramLong);
  }
  
  public static int computeFloatSize(int paramInt, float paramFloat) {
    return computeTagSize(paramInt) + computeFloatSizeNoTag(paramFloat);
  }
  
  public static int computeDoubleSize(int paramInt, double paramDouble) {
    return computeTagSize(paramInt) + computeDoubleSizeNoTag(paramDouble);
  }
  
  public static int computeBoolSize(int paramInt, boolean paramBoolean) {
    return computeTagSize(paramInt) + computeBoolSizeNoTag(paramBoolean);
  }
  
  public static int computeEnumSize(int paramInt1, int paramInt2) {
    return computeTagSize(paramInt1) + computeEnumSizeNoTag(paramInt2);
  }
  
  public static int computeStringSize(int paramInt, String paramString) {
    return computeTagSize(paramInt) + computeStringSizeNoTag(paramString);
  }
  
  public static int computeBytesSize(int paramInt, ByteString paramByteString) {
    return computeTagSize(paramInt) + computeBytesSizeNoTag(paramByteString);
  }
  
  public static int computeByteArraySize(int paramInt, byte[] paramArrayOfbyte) {
    return computeTagSize(paramInt) + computeByteArraySizeNoTag(paramArrayOfbyte);
  }
  
  public static int computeByteBufferSize(int paramInt, ByteBuffer paramByteBuffer) {
    return computeTagSize(paramInt) + computeByteBufferSizeNoTag(paramByteBuffer);
  }
  
  public static int computeLazyFieldSize(int paramInt, LazyFieldLite paramLazyFieldLite) {
    return computeTagSize(paramInt) + computeLazyFieldSizeNoTag(paramLazyFieldLite);
  }
  
  public static int computeMessageSize(int paramInt, MessageLite paramMessageLite) {
    return computeTagSize(paramInt) + computeMessageSizeNoTag(paramMessageLite);
  }
  
  static int computeMessageSize(int paramInt, MessageLite paramMessageLite, Schema paramSchema) {
    return computeTagSize(paramInt) + computeMessageSizeNoTag(paramMessageLite, paramSchema);
  }
  
  public static int computeMessageSetExtensionSize(int paramInt, MessageLite paramMessageLite) {
    int i = computeTagSize(1);
    int j = computeUInt32Size(2, paramInt);
    paramInt = computeMessageSize(3, paramMessageLite);
    return i * 2 + j + paramInt;
  }
  
  public static int computeRawMessageSetExtensionSize(int paramInt, ByteString paramByteString) {
    int i = computeTagSize(1);
    int j = computeUInt32Size(2, paramInt);
    paramInt = computeBytesSize(3, paramByteString);
    return i * 2 + j + paramInt;
  }
  
  public static int computeLazyFieldMessageSetExtensionSize(int paramInt, LazyFieldLite paramLazyFieldLite) {
    int i = computeTagSize(1);
    paramInt = computeUInt32Size(2, paramInt);
    int j = computeLazyFieldSize(3, paramLazyFieldLite);
    return i * 2 + paramInt + j;
  }
  
  public static int computeTagSize(int paramInt) {
    return computeUInt32SizeNoTag(WireFormat.makeTag(paramInt, 0));
  }
  
  public static int computeInt32SizeNoTag(int paramInt) {
    if (paramInt >= 0)
      return computeUInt32SizeNoTag(paramInt); 
    return 10;
  }
  
  public static int computeUInt32SizeNoTag(int paramInt) {
    if ((paramInt & 0xFFFFFF80) == 0)
      return 1; 
    if ((paramInt & 0xFFFFC000) == 0)
      return 2; 
    if ((0xFFE00000 & paramInt) == 0)
      return 3; 
    if ((0xF0000000 & paramInt) == 0)
      return 4; 
    return 5;
  }
  
  public static int computeSInt32SizeNoTag(int paramInt) {
    return computeUInt32SizeNoTag(encodeZigZag32(paramInt));
  }
  
  public static int computeFixed32SizeNoTag(int paramInt) {
    return 4;
  }
  
  public static int computeSFixed32SizeNoTag(int paramInt) {
    return 4;
  }
  
  public static int computeInt64SizeNoTag(long paramLong) {
    return computeUInt64SizeNoTag(paramLong);
  }
  
  public static int computeUInt64SizeNoTag(long paramLong) {
    if ((0xFFFFFFFFFFFFFF80L & paramLong) == 0L)
      return 1; 
    if (paramLong < 0L)
      return 10; 
    int i = 2;
    long l = paramLong;
    if ((0xFFFFFFF800000000L & paramLong) != 0L) {
      i = 2 + 4;
      l = paramLong >>> 28L;
    } 
    int j = i;
    paramLong = l;
    if ((0xFFFFFFFFFFE00000L & l) != 0L) {
      j = i + 2;
      paramLong = l >>> 14L;
    } 
    i = j;
    if ((0xFFFFFFFFFFFFC000L & paramLong) != 0L)
      i = j + 1; 
    return i;
  }
  
  public static int computeSInt64SizeNoTag(long paramLong) {
    return computeUInt64SizeNoTag(encodeZigZag64(paramLong));
  }
  
  public static int computeFixed64SizeNoTag(long paramLong) {
    return 8;
  }
  
  public static int computeSFixed64SizeNoTag(long paramLong) {
    return 8;
  }
  
  public static int computeFloatSizeNoTag(float paramFloat) {
    return 4;
  }
  
  public static int computeDoubleSizeNoTag(double paramDouble) {
    return 8;
  }
  
  public static int computeBoolSizeNoTag(boolean paramBoolean) {
    return 1;
  }
  
  public static int computeEnumSizeNoTag(int paramInt) {
    return computeInt32SizeNoTag(paramInt);
  }
  
  public static int computeStringSizeNoTag(String paramString) {
    int i;
    try {
      i = Utf8.encodedLength(paramString);
    } catch (UnpairedSurrogateException unpairedSurrogateException) {
      byte[] arrayOfByte = paramString.getBytes(Internal.UTF_8);
      i = arrayOfByte.length;
    } 
    return computeLengthDelimitedFieldSize(i);
  }
  
  public static int computeLazyFieldSizeNoTag(LazyFieldLite paramLazyFieldLite) {
    return computeLengthDelimitedFieldSize(paramLazyFieldLite.getSerializedSize());
  }
  
  public static int computeBytesSizeNoTag(ByteString paramByteString) {
    return computeLengthDelimitedFieldSize(paramByteString.size());
  }
  
  public static int computeByteArraySizeNoTag(byte[] paramArrayOfbyte) {
    return computeLengthDelimitedFieldSize(paramArrayOfbyte.length);
  }
  
  public static int computeByteBufferSizeNoTag(ByteBuffer paramByteBuffer) {
    return computeLengthDelimitedFieldSize(paramByteBuffer.capacity());
  }
  
  public static int computeMessageSizeNoTag(MessageLite paramMessageLite) {
    return computeLengthDelimitedFieldSize(paramMessageLite.getSerializedSize());
  }
  
  static int computeMessageSizeNoTag(MessageLite paramMessageLite, Schema paramSchema) {
    return computeLengthDelimitedFieldSize(((AbstractMessageLite)paramMessageLite).getSerializedSize(paramSchema));
  }
  
  static int computeLengthDelimitedFieldSize(int paramInt) {
    return computeUInt32SizeNoTag(paramInt) + paramInt;
  }
  
  public static int encodeZigZag32(int paramInt) {
    return paramInt << 1 ^ paramInt >> 31;
  }
  
  public static long encodeZigZag64(long paramLong) {
    return paramLong << 1L ^ paramLong >> 63L;
  }
  
  public final void checkNoSpaceLeft() {
    if (spaceLeft() == 0)
      return; 
    throw new IllegalStateException("Did not write as much data as expected.");
  }
  
  class OutOfSpaceException extends IOException {
    private static final String MESSAGE = "CodedOutputStream was writing to a flat byte array and ran out of space.";
    
    private static final long serialVersionUID = -6947486886997889499L;
    
    OutOfSpaceException() {
      super("CodedOutputStream was writing to a flat byte array and ran out of space.");
    }
    
    OutOfSpaceException(CodedOutputStream this$0) {
      super(stringBuilder.toString());
    }
    
    OutOfSpaceException(CodedOutputStream this$0) {
      super("CodedOutputStream was writing to a flat byte array and ran out of space.", (Throwable)this$0);
    }
    
    OutOfSpaceException(CodedOutputStream this$0, Throwable param1Throwable) {
      super(stringBuilder.toString(), param1Throwable);
    }
  }
  
  final void inefficientWriteStringNoTag(String paramString, Utf8.UnpairedSurrogateException paramUnpairedSurrogateException) throws IOException {
    logger.log(Level.WARNING, "Converting ill-formed UTF-16. Your Protocol Buffer will not round trip correctly!", paramUnpairedSurrogateException);
    byte[] arrayOfByte = paramString.getBytes(Internal.UTF_8);
    try {
      writeUInt32NoTag(arrayOfByte.length);
      writeLazy(arrayOfByte, 0, arrayOfByte.length);
      return;
    } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
      throw new OutOfSpaceException(indexOutOfBoundsException);
    } catch (OutOfSpaceException outOfSpaceException) {
      throw outOfSpaceException;
    } 
  }
  
  @Deprecated
  public final void writeGroup(int paramInt, MessageLite paramMessageLite) throws IOException {
    writeTag(paramInt, 3);
    writeGroupNoTag(paramMessageLite);
    writeTag(paramInt, 4);
  }
  
  @Deprecated
  final void writeGroup(int paramInt, MessageLite paramMessageLite, Schema paramSchema) throws IOException {
    writeTag(paramInt, 3);
    writeGroupNoTag(paramMessageLite, paramSchema);
    writeTag(paramInt, 4);
  }
  
  @Deprecated
  public final void writeGroupNoTag(MessageLite paramMessageLite) throws IOException {
    paramMessageLite.writeTo(this);
  }
  
  @Deprecated
  final void writeGroupNoTag(MessageLite paramMessageLite, Schema<MessageLite> paramSchema) throws IOException {
    paramSchema.writeTo(paramMessageLite, this.wrapper);
  }
  
  @Deprecated
  public static int computeGroupSize(int paramInt, MessageLite paramMessageLite) {
    return computeTagSize(paramInt) * 2 + computeGroupSizeNoTag(paramMessageLite);
  }
  
  @Deprecated
  static int computeGroupSize(int paramInt, MessageLite paramMessageLite, Schema paramSchema) {
    return computeTagSize(paramInt) * 2 + computeGroupSizeNoTag(paramMessageLite, paramSchema);
  }
  
  @Deprecated
  public static int computeGroupSizeNoTag(MessageLite paramMessageLite) {
    return paramMessageLite.getSerializedSize();
  }
  
  @Deprecated
  static int computeGroupSizeNoTag(MessageLite paramMessageLite, Schema paramSchema) {
    return ((AbstractMessageLite)paramMessageLite).getSerializedSize(paramSchema);
  }
  
  @Deprecated
  public final void writeRawVarint32(int paramInt) throws IOException {
    writeUInt32NoTag(paramInt);
  }
  
  @Deprecated
  public final void writeRawVarint64(long paramLong) throws IOException {
    writeUInt64NoTag(paramLong);
  }
  
  @Deprecated
  public static int computeRawVarint32Size(int paramInt) {
    return computeUInt32SizeNoTag(paramInt);
  }
  
  @Deprecated
  public static int computeRawVarint64Size(long paramLong) {
    return computeUInt64SizeNoTag(paramLong);
  }
  
  @Deprecated
  public final void writeRawLittleEndian32(int paramInt) throws IOException {
    writeFixed32NoTag(paramInt);
  }
  
  @Deprecated
  public final void writeRawLittleEndian64(long paramLong) throws IOException {
    writeFixed64NoTag(paramLong);
  }
  
  public abstract void flush() throws IOException;
  
  public abstract int getTotalBytesWritten();
  
  public abstract int spaceLeft();
  
  public abstract void write(byte paramByte) throws IOException;
  
  public abstract void write(ByteBuffer paramByteBuffer) throws IOException;
  
  public abstract void write(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws IOException;
  
  public abstract void writeBool(int paramInt, boolean paramBoolean) throws IOException;
  
  public abstract void writeByteArray(int paramInt, byte[] paramArrayOfbyte) throws IOException;
  
  public abstract void writeByteArray(int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3) throws IOException;
  
  abstract void writeByteArrayNoTag(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws IOException;
  
  public abstract void writeByteBuffer(int paramInt, ByteBuffer paramByteBuffer) throws IOException;
  
  public abstract void writeBytes(int paramInt, ByteString paramByteString) throws IOException;
  
  public abstract void writeBytesNoTag(ByteString paramByteString) throws IOException;
  
  public abstract void writeFixed32(int paramInt1, int paramInt2) throws IOException;
  
  public abstract void writeFixed32NoTag(int paramInt) throws IOException;
  
  public abstract void writeFixed64(int paramInt, long paramLong) throws IOException;
  
  public abstract void writeFixed64NoTag(long paramLong) throws IOException;
  
  public abstract void writeInt32(int paramInt1, int paramInt2) throws IOException;
  
  public abstract void writeInt32NoTag(int paramInt) throws IOException;
  
  public abstract void writeLazy(ByteBuffer paramByteBuffer) throws IOException;
  
  public abstract void writeLazy(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws IOException;
  
  public abstract void writeMessage(int paramInt, MessageLite paramMessageLite) throws IOException;
  
  abstract void writeMessage(int paramInt, MessageLite paramMessageLite, Schema paramSchema) throws IOException;
  
  public abstract void writeMessageNoTag(MessageLite paramMessageLite) throws IOException;
  
  abstract void writeMessageNoTag(MessageLite paramMessageLite, Schema paramSchema) throws IOException;
  
  public abstract void writeMessageSetExtension(int paramInt, MessageLite paramMessageLite) throws IOException;
  
  public abstract void writeRawBytes(ByteBuffer paramByteBuffer) throws IOException;
  
  public abstract void writeRawMessageSetExtension(int paramInt, ByteString paramByteString) throws IOException;
  
  public abstract void writeString(int paramInt, String paramString) throws IOException;
  
  public abstract void writeStringNoTag(String paramString) throws IOException;
  
  public abstract void writeTag(int paramInt1, int paramInt2) throws IOException;
  
  public abstract void writeUInt32(int paramInt1, int paramInt2) throws IOException;
  
  public abstract void writeUInt32NoTag(int paramInt) throws IOException;
  
  public abstract void writeUInt64(int paramInt, long paramLong) throws IOException;
  
  public abstract void writeUInt64NoTag(long paramLong) throws IOException;
  
  class ArrayEncoder extends CodedOutputStream {
    private final byte[] buffer;
    
    private final int limit;
    
    private final int offset;
    
    private int position;
    
    ArrayEncoder(CodedOutputStream this$0, int param1Int1, int param1Int2) {
      if (this$0 != null) {
        if ((param1Int1 | param1Int2 | this$0.length - param1Int1 + param1Int2) >= 0) {
          this.buffer = (byte[])this$0;
          this.offset = param1Int1;
          this.position = param1Int1;
          this.limit = param1Int1 + param1Int2;
          return;
        } 
        int i = this$0.length;
        throw new IllegalArgumentException(String.format("Array range is invalid. Buffer.length=%d, offset=%d, length=%d", new Object[] { Integer.valueOf(i), Integer.valueOf(param1Int1), Integer.valueOf(param1Int2) }));
      } 
      throw new NullPointerException("buffer");
    }
    
    public final void writeTag(int param1Int1, int param1Int2) throws IOException {
      writeUInt32NoTag(WireFormat.makeTag(param1Int1, param1Int2));
    }
    
    public final void writeInt32(int param1Int1, int param1Int2) throws IOException {
      writeTag(param1Int1, 0);
      writeInt32NoTag(param1Int2);
    }
    
    public final void writeUInt32(int param1Int1, int param1Int2) throws IOException {
      writeTag(param1Int1, 0);
      writeUInt32NoTag(param1Int2);
    }
    
    public final void writeFixed32(int param1Int1, int param1Int2) throws IOException {
      writeTag(param1Int1, 5);
      writeFixed32NoTag(param1Int2);
    }
    
    public final void writeUInt64(int param1Int, long param1Long) throws IOException {
      writeTag(param1Int, 0);
      writeUInt64NoTag(param1Long);
    }
    
    public final void writeFixed64(int param1Int, long param1Long) throws IOException {
      writeTag(param1Int, 1);
      writeFixed64NoTag(param1Long);
    }
    
    public final void writeBool(int param1Int, boolean param1Boolean) throws IOException {
      writeTag(param1Int, 0);
      write((byte)param1Boolean);
    }
    
    public final void writeString(int param1Int, String param1String) throws IOException {
      writeTag(param1Int, 2);
      writeStringNoTag(param1String);
    }
    
    public final void writeBytes(int param1Int, ByteString param1ByteString) throws IOException {
      writeTag(param1Int, 2);
      writeBytesNoTag(param1ByteString);
    }
    
    public final void writeByteArray(int param1Int, byte[] param1ArrayOfbyte) throws IOException {
      writeByteArray(param1Int, param1ArrayOfbyte, 0, param1ArrayOfbyte.length);
    }
    
    public final void writeByteArray(int param1Int1, byte[] param1ArrayOfbyte, int param1Int2, int param1Int3) throws IOException {
      writeTag(param1Int1, 2);
      writeByteArrayNoTag(param1ArrayOfbyte, param1Int2, param1Int3);
    }
    
    public final void writeByteBuffer(int param1Int, ByteBuffer param1ByteBuffer) throws IOException {
      writeTag(param1Int, 2);
      writeUInt32NoTag(param1ByteBuffer.capacity());
      writeRawBytes(param1ByteBuffer);
    }
    
    public final void writeBytesNoTag(ByteString param1ByteString) throws IOException {
      writeUInt32NoTag(param1ByteString.size());
      param1ByteString.writeTo(this);
    }
    
    public final void writeByteArrayNoTag(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws IOException {
      writeUInt32NoTag(param1Int2);
      write(param1ArrayOfbyte, param1Int1, param1Int2);
    }
    
    public final void writeRawBytes(ByteBuffer param1ByteBuffer) throws IOException {
      if (param1ByteBuffer.hasArray()) {
        write(param1ByteBuffer.array(), param1ByteBuffer.arrayOffset(), param1ByteBuffer.capacity());
      } else {
        param1ByteBuffer = param1ByteBuffer.duplicate();
        param1ByteBuffer.clear();
        write(param1ByteBuffer);
      } 
    }
    
    public final void writeMessage(int param1Int, MessageLite param1MessageLite) throws IOException {
      writeTag(param1Int, 2);
      writeMessageNoTag(param1MessageLite);
    }
    
    final void writeMessage(int param1Int, MessageLite param1MessageLite, Schema<MessageLite> param1Schema) throws IOException {
      writeTag(param1Int, 2);
      writeUInt32NoTag(((AbstractMessageLite)param1MessageLite).getSerializedSize(param1Schema));
      param1Schema.writeTo(param1MessageLite, this.wrapper);
    }
    
    public final void writeMessageSetExtension(int param1Int, MessageLite param1MessageLite) throws IOException {
      writeTag(1, 3);
      writeUInt32(2, param1Int);
      writeMessage(3, param1MessageLite);
      writeTag(1, 4);
    }
    
    public final void writeRawMessageSetExtension(int param1Int, ByteString param1ByteString) throws IOException {
      writeTag(1, 3);
      writeUInt32(2, param1Int);
      writeBytes(3, param1ByteString);
      writeTag(1, 4);
    }
    
    public final void writeMessageNoTag(MessageLite param1MessageLite) throws IOException {
      writeUInt32NoTag(param1MessageLite.getSerializedSize());
      param1MessageLite.writeTo(this);
    }
    
    final void writeMessageNoTag(MessageLite param1MessageLite, Schema<MessageLite> param1Schema) throws IOException {
      writeUInt32NoTag(((AbstractMessageLite)param1MessageLite).getSerializedSize(param1Schema));
      param1Schema.writeTo(param1MessageLite, this.wrapper);
    }
    
    public final void write(byte param1Byte) throws IOException {
      try {
        byte[] arrayOfByte = this.buffer;
        int i = this.position;
        this.position = i + 1;
        arrayOfByte[i] = param1Byte;
        return;
      } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
        int i = this.position;
        throw new CodedOutputStream.OutOfSpaceException(String.format("Pos: %d, limit: %d, len: %d", new Object[] { Integer.valueOf(i), Integer.valueOf(this.limit), Integer.valueOf(1) }), indexOutOfBoundsException);
      } 
    }
    
    public final void writeInt32NoTag(int param1Int) throws IOException {
      if (param1Int >= 0) {
        writeUInt32NoTag(param1Int);
      } else {
        writeUInt64NoTag(param1Int);
      } 
    }
    
    public final void writeUInt32NoTag(int param1Int) throws IOException {
      // Byte code:
      //   0: iload_1
      //   1: istore_2
      //   2: invokestatic access$100 : ()Z
      //   5: ifeq -> 318
      //   8: iload_1
      //   9: istore_2
      //   10: invokestatic isOnAndroidDevice : ()Z
      //   13: ifne -> 318
      //   16: iload_1
      //   17: istore_2
      //   18: aload_0
      //   19: invokevirtual spaceLeft : ()I
      //   22: iconst_5
      //   23: if_icmplt -> 318
      //   26: iload_1
      //   27: bipush #-128
      //   29: iand
      //   30: ifne -> 59
      //   33: aload_0
      //   34: getfield buffer : [B
      //   37: astore_3
      //   38: aload_0
      //   39: getfield position : I
      //   42: istore_2
      //   43: aload_0
      //   44: iload_2
      //   45: iconst_1
      //   46: iadd
      //   47: putfield position : I
      //   50: aload_3
      //   51: iload_2
      //   52: i2l
      //   53: iload_1
      //   54: i2b
      //   55: invokestatic putByte : ([BJB)V
      //   58: return
      //   59: aload_0
      //   60: getfield buffer : [B
      //   63: astore_3
      //   64: aload_0
      //   65: getfield position : I
      //   68: istore_2
      //   69: aload_0
      //   70: iload_2
      //   71: iconst_1
      //   72: iadd
      //   73: putfield position : I
      //   76: aload_3
      //   77: iload_2
      //   78: i2l
      //   79: iload_1
      //   80: sipush #128
      //   83: ior
      //   84: i2b
      //   85: invokestatic putByte : ([BJB)V
      //   88: iload_1
      //   89: bipush #7
      //   91: iushr
      //   92: istore_1
      //   93: iload_1
      //   94: bipush #-128
      //   96: iand
      //   97: ifne -> 126
      //   100: aload_0
      //   101: getfield buffer : [B
      //   104: astore_3
      //   105: aload_0
      //   106: getfield position : I
      //   109: istore_2
      //   110: aload_0
      //   111: iload_2
      //   112: iconst_1
      //   113: iadd
      //   114: putfield position : I
      //   117: aload_3
      //   118: iload_2
      //   119: i2l
      //   120: iload_1
      //   121: i2b
      //   122: invokestatic putByte : ([BJB)V
      //   125: return
      //   126: aload_0
      //   127: getfield buffer : [B
      //   130: astore_3
      //   131: aload_0
      //   132: getfield position : I
      //   135: istore_2
      //   136: aload_0
      //   137: iload_2
      //   138: iconst_1
      //   139: iadd
      //   140: putfield position : I
      //   143: aload_3
      //   144: iload_2
      //   145: i2l
      //   146: iload_1
      //   147: sipush #128
      //   150: ior
      //   151: i2b
      //   152: invokestatic putByte : ([BJB)V
      //   155: iload_1
      //   156: bipush #7
      //   158: iushr
      //   159: istore_1
      //   160: iload_1
      //   161: bipush #-128
      //   163: iand
      //   164: ifne -> 193
      //   167: aload_0
      //   168: getfield buffer : [B
      //   171: astore_3
      //   172: aload_0
      //   173: getfield position : I
      //   176: istore_2
      //   177: aload_0
      //   178: iload_2
      //   179: iconst_1
      //   180: iadd
      //   181: putfield position : I
      //   184: aload_3
      //   185: iload_2
      //   186: i2l
      //   187: iload_1
      //   188: i2b
      //   189: invokestatic putByte : ([BJB)V
      //   192: return
      //   193: aload_0
      //   194: getfield buffer : [B
      //   197: astore_3
      //   198: aload_0
      //   199: getfield position : I
      //   202: istore_2
      //   203: aload_0
      //   204: iload_2
      //   205: iconst_1
      //   206: iadd
      //   207: putfield position : I
      //   210: aload_3
      //   211: iload_2
      //   212: i2l
      //   213: iload_1
      //   214: sipush #128
      //   217: ior
      //   218: i2b
      //   219: invokestatic putByte : ([BJB)V
      //   222: iload_1
      //   223: bipush #7
      //   225: iushr
      //   226: istore_1
      //   227: iload_1
      //   228: bipush #-128
      //   230: iand
      //   231: ifne -> 260
      //   234: aload_0
      //   235: getfield buffer : [B
      //   238: astore_3
      //   239: aload_0
      //   240: getfield position : I
      //   243: istore_2
      //   244: aload_0
      //   245: iload_2
      //   246: iconst_1
      //   247: iadd
      //   248: putfield position : I
      //   251: aload_3
      //   252: iload_2
      //   253: i2l
      //   254: iload_1
      //   255: i2b
      //   256: invokestatic putByte : ([BJB)V
      //   259: return
      //   260: aload_0
      //   261: getfield buffer : [B
      //   264: astore_3
      //   265: aload_0
      //   266: getfield position : I
      //   269: istore_2
      //   270: aload_0
      //   271: iload_2
      //   272: iconst_1
      //   273: iadd
      //   274: putfield position : I
      //   277: aload_3
      //   278: iload_2
      //   279: i2l
      //   280: iload_1
      //   281: sipush #128
      //   284: ior
      //   285: i2b
      //   286: invokestatic putByte : ([BJB)V
      //   289: aload_0
      //   290: getfield buffer : [B
      //   293: astore_3
      //   294: aload_0
      //   295: getfield position : I
      //   298: istore_2
      //   299: aload_0
      //   300: iload_2
      //   301: iconst_1
      //   302: iadd
      //   303: putfield position : I
      //   306: aload_3
      //   307: iload_2
      //   308: i2l
      //   309: iload_1
      //   310: bipush #7
      //   312: iushr
      //   313: i2b
      //   314: invokestatic putByte : ([BJB)V
      //   317: return
      //   318: iload_2
      //   319: bipush #-128
      //   321: iand
      //   322: ifne -> 348
      //   325: aload_0
      //   326: getfield buffer : [B
      //   329: astore_3
      //   330: aload_0
      //   331: getfield position : I
      //   334: istore_1
      //   335: aload_0
      //   336: iload_1
      //   337: iconst_1
      //   338: iadd
      //   339: putfield position : I
      //   342: aload_3
      //   343: iload_1
      //   344: iload_2
      //   345: i2b
      //   346: bastore
      //   347: return
      //   348: aload_0
      //   349: getfield buffer : [B
      //   352: astore_3
      //   353: aload_0
      //   354: getfield position : I
      //   357: istore_1
      //   358: aload_0
      //   359: iload_1
      //   360: iconst_1
      //   361: iadd
      //   362: putfield position : I
      //   365: aload_3
      //   366: iload_1
      //   367: iload_2
      //   368: bipush #127
      //   370: iand
      //   371: sipush #128
      //   374: ior
      //   375: i2b
      //   376: bastore
      //   377: iload_2
      //   378: bipush #7
      //   380: iushr
      //   381: istore_2
      //   382: goto -> 318
      //   385: astore_3
      //   386: aload_0
      //   387: getfield position : I
      //   390: istore_1
      //   391: new com/android/framework/protobuf/CodedOutputStream$OutOfSpaceException
      //   394: dup
      //   395: ldc 'Pos: %d, limit: %d, len: %d'
      //   397: iconst_3
      //   398: anewarray java/lang/Object
      //   401: dup
      //   402: iconst_0
      //   403: iload_1
      //   404: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   407: aastore
      //   408: dup
      //   409: iconst_1
      //   410: aload_0
      //   411: getfield limit : I
      //   414: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   417: aastore
      //   418: dup
      //   419: iconst_2
      //   420: iconst_1
      //   421: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   424: aastore
      //   425: invokestatic format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
      //   428: aload_3
      //   429: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
      //   432: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1337	-> 0
      //   #1338	-> 8
      //   #1339	-> 16
      //   #1340	-> 26
      //   #1341	-> 33
      //   #1342	-> 58
      //   #1344	-> 59
      //   #1345	-> 88
      //   #1346	-> 93
      //   #1347	-> 100
      //   #1348	-> 125
      //   #1350	-> 126
      //   #1351	-> 155
      //   #1352	-> 160
      //   #1353	-> 167
      //   #1354	-> 192
      //   #1356	-> 193
      //   #1357	-> 222
      //   #1358	-> 227
      //   #1359	-> 234
      //   #1360	-> 259
      //   #1362	-> 260
      //   #1363	-> 289
      //   #1364	-> 289
      //   #1381	-> 317
      //   #1368	-> 318
      //   #1369	-> 325
      //   #1370	-> 347
      //   #1372	-> 348
      //   #1373	-> 377
      //   #1376	-> 385
      //   #1377	-> 386
      //   #1378	-> 391
      // Exception table:
      //   from	to	target	type
      //   325	342	385	java/lang/IndexOutOfBoundsException
      //   348	365	385	java/lang/IndexOutOfBoundsException
    }
    
    public final void writeFixed32NoTag(int param1Int) throws IOException {
      try {
        byte[] arrayOfByte = this.buffer;
        int i = this.position, j = i + 1;
        arrayOfByte[i] = (byte)(param1Int & 0xFF);
        arrayOfByte = this.buffer;
        this.position = i = j + 1;
        arrayOfByte[j] = (byte)(param1Int >> 8 & 0xFF);
        arrayOfByte = this.buffer;
        this.position = j = i + 1;
        arrayOfByte[i] = (byte)(param1Int >> 16 & 0xFF);
        arrayOfByte = this.buffer;
        this.position = j + 1;
        arrayOfByte[j] = (byte)(param1Int >> 24 & 0xFF);
        return;
      } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
        param1Int = this.position;
        throw new CodedOutputStream.OutOfSpaceException(String.format("Pos: %d, limit: %d, len: %d", new Object[] { Integer.valueOf(param1Int), Integer.valueOf(this.limit), Integer.valueOf(1) }), indexOutOfBoundsException);
      } 
    }
    
    public final void writeUInt64NoTag(long param1Long) throws IOException {
      // Byte code:
      //   0: lload_1
      //   1: lstore_3
      //   2: invokestatic access$100 : ()Z
      //   5: ifeq -> 107
      //   8: lload_1
      //   9: lstore_3
      //   10: aload_0
      //   11: invokevirtual spaceLeft : ()I
      //   14: bipush #10
      //   16: if_icmplt -> 107
      //   19: lload_1
      //   20: ldc2_w -128
      //   23: land
      //   24: lconst_0
      //   25: lcmp
      //   26: ifne -> 61
      //   29: aload_0
      //   30: getfield buffer : [B
      //   33: astore #5
      //   35: aload_0
      //   36: getfield position : I
      //   39: istore #6
      //   41: aload_0
      //   42: iload #6
      //   44: iconst_1
      //   45: iadd
      //   46: putfield position : I
      //   49: aload #5
      //   51: iload #6
      //   53: i2l
      //   54: lload_1
      //   55: l2i
      //   56: i2b
      //   57: invokestatic putByte : ([BJB)V
      //   60: return
      //   61: aload_0
      //   62: getfield buffer : [B
      //   65: astore #5
      //   67: aload_0
      //   68: getfield position : I
      //   71: istore #6
      //   73: aload_0
      //   74: iload #6
      //   76: iconst_1
      //   77: iadd
      //   78: putfield position : I
      //   81: aload #5
      //   83: iload #6
      //   85: i2l
      //   86: lload_1
      //   87: l2i
      //   88: bipush #127
      //   90: iand
      //   91: sipush #128
      //   94: ior
      //   95: i2b
      //   96: invokestatic putByte : ([BJB)V
      //   99: lload_1
      //   100: bipush #7
      //   102: lushr
      //   103: lstore_1
      //   104: goto -> 19
      //   107: lload_3
      //   108: ldc2_w -128
      //   111: land
      //   112: lconst_0
      //   113: lcmp
      //   114: ifne -> 146
      //   117: aload_0
      //   118: getfield buffer : [B
      //   121: astore #5
      //   123: aload_0
      //   124: getfield position : I
      //   127: istore #6
      //   129: aload_0
      //   130: iload #6
      //   132: iconst_1
      //   133: iadd
      //   134: putfield position : I
      //   137: aload #5
      //   139: iload #6
      //   141: lload_3
      //   142: l2i
      //   143: i2b
      //   144: bastore
      //   145: return
      //   146: aload_0
      //   147: getfield buffer : [B
      //   150: astore #5
      //   152: aload_0
      //   153: getfield position : I
      //   156: istore #6
      //   158: aload_0
      //   159: iload #6
      //   161: iconst_1
      //   162: iadd
      //   163: putfield position : I
      //   166: aload #5
      //   168: iload #6
      //   170: lload_3
      //   171: l2i
      //   172: bipush #127
      //   174: iand
      //   175: sipush #128
      //   178: ior
      //   179: i2b
      //   180: bastore
      //   181: lload_3
      //   182: bipush #7
      //   184: lushr
      //   185: lstore_3
      //   186: goto -> 107
      //   189: astore #5
      //   191: aload_0
      //   192: getfield position : I
      //   195: istore #6
      //   197: new com/android/framework/protobuf/CodedOutputStream$OutOfSpaceException
      //   200: dup
      //   201: ldc 'Pos: %d, limit: %d, len: %d'
      //   203: iconst_3
      //   204: anewarray java/lang/Object
      //   207: dup
      //   208: iconst_0
      //   209: iload #6
      //   211: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   214: aastore
      //   215: dup
      //   216: iconst_1
      //   217: aload_0
      //   218: getfield limit : I
      //   221: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   224: aastore
      //   225: dup
      //   226: iconst_2
      //   227: iconst_1
      //   228: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   231: aastore
      //   232: invokestatic format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
      //   235: aload #5
      //   237: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
      //   240: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1398	-> 0
      //   #1400	-> 19
      //   #1401	-> 29
      //   #1402	-> 60
      //   #1404	-> 61
      //   #1405	-> 99
      //   #1411	-> 107
      //   #1412	-> 117
      //   #1413	-> 145
      //   #1415	-> 146
      //   #1416	-> 181
      //   #1419	-> 189
      //   #1420	-> 191
      //   #1421	-> 197
      // Exception table:
      //   from	to	target	type
      //   117	137	189	java/lang/IndexOutOfBoundsException
      //   146	166	189	java/lang/IndexOutOfBoundsException
    }
    
    public final void writeFixed64NoTag(long param1Long) throws IOException {
      try {
        byte[] arrayOfByte = this.buffer;
        int i = this.position, j = i + 1;
        arrayOfByte[i] = (byte)((int)param1Long & 0xFF);
        arrayOfByte = this.buffer;
        this.position = i = j + 1;
        arrayOfByte[j] = (byte)((int)(param1Long >> 8L) & 0xFF);
        arrayOfByte = this.buffer;
        this.position = j = i + 1;
        arrayOfByte[i] = (byte)((int)(param1Long >> 16L) & 0xFF);
        arrayOfByte = this.buffer;
        this.position = i = j + 1;
        arrayOfByte[j] = (byte)((int)(param1Long >> 24L) & 0xFF);
        arrayOfByte = this.buffer;
        int k = i + 1;
        arrayOfByte[i] = (byte)((int)(param1Long >> 32L) & 0xFF);
        arrayOfByte = this.buffer;
        this.position = j = k + 1;
        arrayOfByte[k] = (byte)((int)(param1Long >> 40L) & 0xFF);
        arrayOfByte = this.buffer;
        this.position = i = j + 1;
        arrayOfByte[j] = (byte)((int)(param1Long >> 48L) & 0xFF);
        arrayOfByte = this.buffer;
        this.position = i + 1;
        arrayOfByte[i] = (byte)((int)(param1Long >> 56L) & 0xFF);
        return;
      } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
        int i = this.position;
        throw new CodedOutputStream.OutOfSpaceException(String.format("Pos: %d, limit: %d, len: %d", new Object[] { Integer.valueOf(i), Integer.valueOf(this.limit), Integer.valueOf(1) }), indexOutOfBoundsException);
      } 
    }
    
    public final void write(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws IOException {
      try {
        System.arraycopy(param1ArrayOfbyte, param1Int1, this.buffer, this.position, param1Int2);
        this.position += param1Int2;
        return;
      } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
        param1Int1 = this.position;
        throw new CodedOutputStream.OutOfSpaceException(String.format("Pos: %d, limit: %d, len: %d", new Object[] { Integer.valueOf(param1Int1), Integer.valueOf(this.limit), Integer.valueOf(param1Int2) }), indexOutOfBoundsException);
      } 
    }
    
    public final void writeLazy(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws IOException {
      write(param1ArrayOfbyte, param1Int1, param1Int2);
    }
    
    public final void write(ByteBuffer param1ByteBuffer) throws IOException {
      int i = param1ByteBuffer.remaining();
      try {
        param1ByteBuffer.get(this.buffer, this.position, i);
        this.position += i;
        return;
      } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
        int j = this.position;
        throw new CodedOutputStream.OutOfSpaceException(String.format("Pos: %d, limit: %d, len: %d", new Object[] { Integer.valueOf(j), Integer.valueOf(this.limit), Integer.valueOf(i) }), indexOutOfBoundsException);
      } 
    }
    
    public final void writeLazy(ByteBuffer param1ByteBuffer) throws IOException {
      write(param1ByteBuffer);
    }
    
    public final void writeStringNoTag(String param1String) throws IOException {
      int i = this.position;
      try {
        int j = param1String.length();
        int k = computeUInt32SizeNoTag(j * 3);
        j = computeUInt32SizeNoTag(param1String.length());
        if (j == k) {
          this.position = k = i + j;
          k = Utf8.encode(param1String, this.buffer, k, spaceLeft());
          this.position = i;
          writeUInt32NoTag(k - i - j);
          this.position = k;
        } else {
          j = Utf8.encodedLength(param1String);
          writeUInt32NoTag(j);
          this.position = Utf8.encode(param1String, this.buffer, this.position, spaceLeft());
        } 
      } catch (UnpairedSurrogateException unpairedSurrogateException) {
        this.position = i;
        inefficientWriteStringNoTag(param1String, unpairedSurrogateException);
      } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
        throw new CodedOutputStream.OutOfSpaceException(indexOutOfBoundsException);
      } 
    }
    
    public void flush() {}
    
    public final int spaceLeft() {
      return this.limit - this.position;
    }
    
    public final int getTotalBytesWritten() {
      return this.position - this.offset;
    }
  }
  
  class HeapNioEncoder extends ArrayEncoder {
    private final ByteBuffer byteBuffer;
    
    private int initialPosition;
    
    HeapNioEncoder(CodedOutputStream this$0) {
      super(arrayOfByte, i + j, k);
      this.byteBuffer = (ByteBuffer)this$0;
      this.initialPosition = this$0.position();
    }
    
    public void flush() {
      this.byteBuffer.position(this.initialPosition + getTotalBytesWritten());
    }
  }
  
  class SafeDirectNioEncoder extends CodedOutputStream {
    private final ByteBuffer buffer;
    
    private final int initialPosition;
    
    private final ByteBuffer originalBuffer;
    
    SafeDirectNioEncoder(CodedOutputStream this$0) {
      this.originalBuffer = (ByteBuffer)this$0;
      this.buffer = this$0.duplicate().order(ByteOrder.LITTLE_ENDIAN);
      this.initialPosition = this$0.position();
    }
    
    public void writeTag(int param1Int1, int param1Int2) throws IOException {
      writeUInt32NoTag(WireFormat.makeTag(param1Int1, param1Int2));
    }
    
    public void writeInt32(int param1Int1, int param1Int2) throws IOException {
      writeTag(param1Int1, 0);
      writeInt32NoTag(param1Int2);
    }
    
    public void writeUInt32(int param1Int1, int param1Int2) throws IOException {
      writeTag(param1Int1, 0);
      writeUInt32NoTag(param1Int2);
    }
    
    public void writeFixed32(int param1Int1, int param1Int2) throws IOException {
      writeTag(param1Int1, 5);
      writeFixed32NoTag(param1Int2);
    }
    
    public void writeUInt64(int param1Int, long param1Long) throws IOException {
      writeTag(param1Int, 0);
      writeUInt64NoTag(param1Long);
    }
    
    public void writeFixed64(int param1Int, long param1Long) throws IOException {
      writeTag(param1Int, 1);
      writeFixed64NoTag(param1Long);
    }
    
    public void writeBool(int param1Int, boolean param1Boolean) throws IOException {
      writeTag(param1Int, 0);
      write((byte)param1Boolean);
    }
    
    public void writeString(int param1Int, String param1String) throws IOException {
      writeTag(param1Int, 2);
      writeStringNoTag(param1String);
    }
    
    public void writeBytes(int param1Int, ByteString param1ByteString) throws IOException {
      writeTag(param1Int, 2);
      writeBytesNoTag(param1ByteString);
    }
    
    public void writeByteArray(int param1Int, byte[] param1ArrayOfbyte) throws IOException {
      writeByteArray(param1Int, param1ArrayOfbyte, 0, param1ArrayOfbyte.length);
    }
    
    public void writeByteArray(int param1Int1, byte[] param1ArrayOfbyte, int param1Int2, int param1Int3) throws IOException {
      writeTag(param1Int1, 2);
      writeByteArrayNoTag(param1ArrayOfbyte, param1Int2, param1Int3);
    }
    
    public void writeByteBuffer(int param1Int, ByteBuffer param1ByteBuffer) throws IOException {
      writeTag(param1Int, 2);
      writeUInt32NoTag(param1ByteBuffer.capacity());
      writeRawBytes(param1ByteBuffer);
    }
    
    public void writeMessage(int param1Int, MessageLite param1MessageLite) throws IOException {
      writeTag(param1Int, 2);
      writeMessageNoTag(param1MessageLite);
    }
    
    void writeMessage(int param1Int, MessageLite param1MessageLite, Schema param1Schema) throws IOException {
      writeTag(param1Int, 2);
      writeMessageNoTag(param1MessageLite, param1Schema);
    }
    
    public void writeMessageSetExtension(int param1Int, MessageLite param1MessageLite) throws IOException {
      writeTag(1, 3);
      writeUInt32(2, param1Int);
      writeMessage(3, param1MessageLite);
      writeTag(1, 4);
    }
    
    public void writeRawMessageSetExtension(int param1Int, ByteString param1ByteString) throws IOException {
      writeTag(1, 3);
      writeUInt32(2, param1Int);
      writeBytes(3, param1ByteString);
      writeTag(1, 4);
    }
    
    public void writeMessageNoTag(MessageLite param1MessageLite) throws IOException {
      writeUInt32NoTag(param1MessageLite.getSerializedSize());
      param1MessageLite.writeTo(this);
    }
    
    void writeMessageNoTag(MessageLite param1MessageLite, Schema<MessageLite> param1Schema) throws IOException {
      writeUInt32NoTag(((AbstractMessageLite)param1MessageLite).getSerializedSize(param1Schema));
      param1Schema.writeTo(param1MessageLite, this.wrapper);
    }
    
    public void write(byte param1Byte) throws IOException {
      try {
        this.buffer.put(param1Byte);
        return;
      } catch (BufferOverflowException bufferOverflowException) {
        throw new CodedOutputStream.OutOfSpaceException(bufferOverflowException);
      } 
    }
    
    public void writeBytesNoTag(ByteString param1ByteString) throws IOException {
      writeUInt32NoTag(param1ByteString.size());
      param1ByteString.writeTo(this);
    }
    
    public void writeByteArrayNoTag(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws IOException {
      writeUInt32NoTag(param1Int2);
      write(param1ArrayOfbyte, param1Int1, param1Int2);
    }
    
    public void writeRawBytes(ByteBuffer param1ByteBuffer) throws IOException {
      if (param1ByteBuffer.hasArray()) {
        write(param1ByteBuffer.array(), param1ByteBuffer.arrayOffset(), param1ByteBuffer.capacity());
      } else {
        param1ByteBuffer = param1ByteBuffer.duplicate();
        param1ByteBuffer.clear();
        write(param1ByteBuffer);
      } 
    }
    
    public void writeInt32NoTag(int param1Int) throws IOException {
      if (param1Int >= 0) {
        writeUInt32NoTag(param1Int);
      } else {
        writeUInt64NoTag(param1Int);
      } 
    }
    
    public void writeUInt32NoTag(int param1Int) throws IOException {
      // Byte code:
      //   0: iload_1
      //   1: bipush #-128
      //   3: iand
      //   4: ifne -> 18
      //   7: aload_0
      //   8: getfield buffer : Ljava/nio/ByteBuffer;
      //   11: iload_1
      //   12: i2b
      //   13: invokevirtual put : (B)Ljava/nio/ByteBuffer;
      //   16: pop
      //   17: return
      //   18: aload_0
      //   19: getfield buffer : Ljava/nio/ByteBuffer;
      //   22: iload_1
      //   23: bipush #127
      //   25: iand
      //   26: sipush #128
      //   29: ior
      //   30: i2b
      //   31: invokevirtual put : (B)Ljava/nio/ByteBuffer;
      //   34: pop
      //   35: iload_1
      //   36: bipush #7
      //   38: iushr
      //   39: istore_1
      //   40: goto -> 0
      //   43: astore_2
      //   44: new com/android/framework/protobuf/CodedOutputStream$OutOfSpaceException
      //   47: dup
      //   48: aload_2
      //   49: invokespecial <init> : (Ljava/lang/Throwable;)V
      //   52: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1727	-> 0
      //   #1728	-> 7
      //   #1729	-> 17
      //   #1731	-> 18
      //   #1732	-> 35
      //   #1735	-> 43
      //   #1736	-> 44
      // Exception table:
      //   from	to	target	type
      //   7	17	43	java/nio/BufferOverflowException
      //   18	35	43	java/nio/BufferOverflowException
    }
    
    public void writeFixed32NoTag(int param1Int) throws IOException {
      try {
        this.buffer.putInt(param1Int);
        return;
      } catch (BufferOverflowException bufferOverflowException) {
        throw new CodedOutputStream.OutOfSpaceException(bufferOverflowException);
      } 
    }
    
    public void writeUInt64NoTag(long param1Long) throws IOException {
      // Byte code:
      //   0: ldc2_w -128
      //   3: lload_1
      //   4: land
      //   5: lconst_0
      //   6: lcmp
      //   7: ifne -> 22
      //   10: aload_0
      //   11: getfield buffer : Ljava/nio/ByteBuffer;
      //   14: lload_1
      //   15: l2i
      //   16: i2b
      //   17: invokevirtual put : (B)Ljava/nio/ByteBuffer;
      //   20: pop
      //   21: return
      //   22: aload_0
      //   23: getfield buffer : Ljava/nio/ByteBuffer;
      //   26: lload_1
      //   27: l2i
      //   28: bipush #127
      //   30: iand
      //   31: sipush #128
      //   34: ior
      //   35: i2b
      //   36: invokevirtual put : (B)Ljava/nio/ByteBuffer;
      //   39: pop
      //   40: lload_1
      //   41: bipush #7
      //   43: lushr
      //   44: lstore_1
      //   45: goto -> 0
      //   48: astore_3
      //   49: new com/android/framework/protobuf/CodedOutputStream$OutOfSpaceException
      //   52: dup
      //   53: aload_3
      //   54: invokespecial <init> : (Ljava/lang/Throwable;)V
      //   57: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1753	-> 0
      //   #1754	-> 10
      //   #1755	-> 21
      //   #1757	-> 22
      //   #1758	-> 40
      //   #1761	-> 48
      //   #1762	-> 49
      // Exception table:
      //   from	to	target	type
      //   10	21	48	java/nio/BufferOverflowException
      //   22	40	48	java/nio/BufferOverflowException
    }
    
    public void writeFixed64NoTag(long param1Long) throws IOException {
      try {
        this.buffer.putLong(param1Long);
        return;
      } catch (BufferOverflowException bufferOverflowException) {
        throw new CodedOutputStream.OutOfSpaceException(bufferOverflowException);
      } 
    }
    
    public void write(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws IOException {
      try {
        this.buffer.put(param1ArrayOfbyte, param1Int1, param1Int2);
        return;
      } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
        throw new CodedOutputStream.OutOfSpaceException(indexOutOfBoundsException);
      } catch (BufferOverflowException bufferOverflowException) {
        throw new CodedOutputStream.OutOfSpaceException(bufferOverflowException);
      } 
    }
    
    public void writeLazy(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws IOException {
      write(param1ArrayOfbyte, param1Int1, param1Int2);
    }
    
    public void write(ByteBuffer param1ByteBuffer) throws IOException {
      try {
        this.buffer.put(param1ByteBuffer);
        return;
      } catch (BufferOverflowException bufferOverflowException) {
        throw new CodedOutputStream.OutOfSpaceException(bufferOverflowException);
      } 
    }
    
    public void writeLazy(ByteBuffer param1ByteBuffer) throws IOException {
      write(param1ByteBuffer);
    }
    
    public void writeStringNoTag(String param1String) throws IOException {
      int i = this.buffer.position();
      try {
        int j = param1String.length();
        j = computeUInt32SizeNoTag(j * 3);
        int k = computeUInt32SizeNoTag(param1String.length());
        if (k == j) {
          j = this.buffer.position() + k;
          this.buffer.position(j);
          encode(param1String);
          k = this.buffer.position();
          this.buffer.position(i);
          writeUInt32NoTag(k - j);
          this.buffer.position(k);
        } else {
          j = Utf8.encodedLength(param1String);
          writeUInt32NoTag(j);
          encode(param1String);
        } 
      } catch (UnpairedSurrogateException unpairedSurrogateException) {
        this.buffer.position(i);
        inefficientWriteStringNoTag(param1String, unpairedSurrogateException);
      } catch (IllegalArgumentException illegalArgumentException) {
        throw new CodedOutputStream.OutOfSpaceException(illegalArgumentException);
      } 
    }
    
    public void flush() {
      this.originalBuffer.position(this.buffer.position());
    }
    
    public int spaceLeft() {
      return this.buffer.remaining();
    }
    
    public int getTotalBytesWritten() {
      return this.buffer.position() - this.initialPosition;
    }
    
    private void encode(String param1String) throws IOException {
      try {
        Utf8.encodeUtf8(param1String, this.buffer);
        return;
      } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
        throw new CodedOutputStream.OutOfSpaceException(indexOutOfBoundsException);
      } 
    }
  }
  
  class UnsafeDirectNioEncoder extends CodedOutputStream {
    private final long address;
    
    private final ByteBuffer buffer;
    
    private final long initialPosition;
    
    private final long limit;
    
    private final long oneVarintLimit;
    
    private final ByteBuffer originalBuffer;
    
    private long position;
    
    UnsafeDirectNioEncoder(CodedOutputStream this$0) {
      this.originalBuffer = (ByteBuffer)this$0;
      this.buffer = this$0.duplicate().order(ByteOrder.LITTLE_ENDIAN);
      long l = UnsafeUtil.addressOffset((ByteBuffer)this$0);
      this.initialPosition = l + this$0.position();
      this.limit = l = this.address + this$0.limit();
      this.oneVarintLimit = l - 10L;
      this.position = this.initialPosition;
    }
    
    static boolean isSupported() {
      return UnsafeUtil.hasUnsafeByteBufferOperations();
    }
    
    public void writeTag(int param1Int1, int param1Int2) throws IOException {
      writeUInt32NoTag(WireFormat.makeTag(param1Int1, param1Int2));
    }
    
    public void writeInt32(int param1Int1, int param1Int2) throws IOException {
      writeTag(param1Int1, 0);
      writeInt32NoTag(param1Int2);
    }
    
    public void writeUInt32(int param1Int1, int param1Int2) throws IOException {
      writeTag(param1Int1, 0);
      writeUInt32NoTag(param1Int2);
    }
    
    public void writeFixed32(int param1Int1, int param1Int2) throws IOException {
      writeTag(param1Int1, 5);
      writeFixed32NoTag(param1Int2);
    }
    
    public void writeUInt64(int param1Int, long param1Long) throws IOException {
      writeTag(param1Int, 0);
      writeUInt64NoTag(param1Long);
    }
    
    public void writeFixed64(int param1Int, long param1Long) throws IOException {
      writeTag(param1Int, 1);
      writeFixed64NoTag(param1Long);
    }
    
    public void writeBool(int param1Int, boolean param1Boolean) throws IOException {
      writeTag(param1Int, 0);
      write((byte)param1Boolean);
    }
    
    public void writeString(int param1Int, String param1String) throws IOException {
      writeTag(param1Int, 2);
      writeStringNoTag(param1String);
    }
    
    public void writeBytes(int param1Int, ByteString param1ByteString) throws IOException {
      writeTag(param1Int, 2);
      writeBytesNoTag(param1ByteString);
    }
    
    public void writeByteArray(int param1Int, byte[] param1ArrayOfbyte) throws IOException {
      writeByteArray(param1Int, param1ArrayOfbyte, 0, param1ArrayOfbyte.length);
    }
    
    public void writeByteArray(int param1Int1, byte[] param1ArrayOfbyte, int param1Int2, int param1Int3) throws IOException {
      writeTag(param1Int1, 2);
      writeByteArrayNoTag(param1ArrayOfbyte, param1Int2, param1Int3);
    }
    
    public void writeByteBuffer(int param1Int, ByteBuffer param1ByteBuffer) throws IOException {
      writeTag(param1Int, 2);
      writeUInt32NoTag(param1ByteBuffer.capacity());
      writeRawBytes(param1ByteBuffer);
    }
    
    public void writeMessage(int param1Int, MessageLite param1MessageLite) throws IOException {
      writeTag(param1Int, 2);
      writeMessageNoTag(param1MessageLite);
    }
    
    void writeMessage(int param1Int, MessageLite param1MessageLite, Schema param1Schema) throws IOException {
      writeTag(param1Int, 2);
      writeMessageNoTag(param1MessageLite, param1Schema);
    }
    
    public void writeMessageSetExtension(int param1Int, MessageLite param1MessageLite) throws IOException {
      writeTag(1, 3);
      writeUInt32(2, param1Int);
      writeMessage(3, param1MessageLite);
      writeTag(1, 4);
    }
    
    public void writeRawMessageSetExtension(int param1Int, ByteString param1ByteString) throws IOException {
      writeTag(1, 3);
      writeUInt32(2, param1Int);
      writeBytes(3, param1ByteString);
      writeTag(1, 4);
    }
    
    public void writeMessageNoTag(MessageLite param1MessageLite) throws IOException {
      writeUInt32NoTag(param1MessageLite.getSerializedSize());
      param1MessageLite.writeTo(this);
    }
    
    void writeMessageNoTag(MessageLite param1MessageLite, Schema<MessageLite> param1Schema) throws IOException {
      writeUInt32NoTag(((AbstractMessageLite)param1MessageLite).getSerializedSize(param1Schema));
      param1Schema.writeTo(param1MessageLite, this.wrapper);
    }
    
    public void write(byte param1Byte) throws IOException {
      long l = this.position;
      if (l < this.limit) {
        this.position = 1L + l;
        UnsafeUtil.putByte(l, param1Byte);
        return;
      } 
      l = this.position;
      throw new CodedOutputStream.OutOfSpaceException(String.format("Pos: %d, limit: %d, len: %d", new Object[] { Long.valueOf(l), Long.valueOf(this.limit), Integer.valueOf(1) }));
    }
    
    public void writeBytesNoTag(ByteString param1ByteString) throws IOException {
      writeUInt32NoTag(param1ByteString.size());
      param1ByteString.writeTo(this);
    }
    
    public void writeByteArrayNoTag(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws IOException {
      writeUInt32NoTag(param1Int2);
      write(param1ArrayOfbyte, param1Int1, param1Int2);
    }
    
    public void writeRawBytes(ByteBuffer param1ByteBuffer) throws IOException {
      if (param1ByteBuffer.hasArray()) {
        write(param1ByteBuffer.array(), param1ByteBuffer.arrayOffset(), param1ByteBuffer.capacity());
      } else {
        param1ByteBuffer = param1ByteBuffer.duplicate();
        param1ByteBuffer.clear();
        write(param1ByteBuffer);
      } 
    }
    
    public void writeInt32NoTag(int param1Int) throws IOException {
      if (param1Int >= 0) {
        writeUInt32NoTag(param1Int);
      } else {
        writeUInt64NoTag(param1Int);
      } 
    }
    
    public void writeUInt32NoTag(int param1Int) throws IOException {
      int i = param1Int;
      if (this.position <= this.oneVarintLimit)
        while (true) {
          if ((param1Int & 0xFFFFFF80) == 0) {
            long l2 = this.position;
            this.position = 1L + l2;
            UnsafeUtil.putByte(l2, (byte)param1Int);
            return;
          } 
          long l1 = this.position;
          this.position = l1 + 1L;
          UnsafeUtil.putByte(l1, (byte)(param1Int & 0x7F | 0x80));
          param1Int >>>= 7;
        }  
      while (true) {
        long l1 = this.position;
        if (l1 < this.limit) {
          if ((i & 0xFFFFFF80) == 0) {
            this.position = 1L + l1;
            UnsafeUtil.putByte(l1, (byte)i);
            return;
          } 
          this.position = l1 + 1L;
          UnsafeUtil.putByte(l1, (byte)(i & 0x7F | 0x80));
          i >>>= 7;
          continue;
        } 
        break;
      } 
      long l = this.position;
      throw new CodedOutputStream.OutOfSpaceException(String.format("Pos: %d, limit: %d, len: %d", new Object[] { Long.valueOf(l), Long.valueOf(this.limit), Integer.valueOf(1) }));
    }
    
    public void writeFixed32NoTag(int param1Int) throws IOException {
      this.buffer.putInt(bufferPos(this.position), param1Int);
      this.position += 4L;
    }
    
    public void writeUInt64NoTag(long param1Long) throws IOException {
      long l = param1Long;
      if (this.position <= this.oneVarintLimit)
        while (true) {
          if ((param1Long & 0xFFFFFFFFFFFFFF80L) == 0L) {
            l = this.position;
            this.position = 1L + l;
            UnsafeUtil.putByte(l, (byte)(int)param1Long);
            return;
          } 
          l = this.position;
          this.position = l + 1L;
          UnsafeUtil.putByte(l, (byte)((int)param1Long & 0x7F | 0x80));
          param1Long >>>= 7L;
        }  
      while (true) {
        param1Long = this.position;
        if (param1Long < this.limit) {
          if ((l & 0xFFFFFFFFFFFFFF80L) == 0L) {
            this.position = 1L + param1Long;
            UnsafeUtil.putByte(param1Long, (byte)(int)l);
            return;
          } 
          this.position = param1Long + 1L;
          UnsafeUtil.putByte(param1Long, (byte)((int)l & 0x7F | 0x80));
          l >>>= 7L;
          continue;
        } 
        break;
      } 
      param1Long = this.position;
      throw new CodedOutputStream.OutOfSpaceException(String.format("Pos: %d, limit: %d, len: %d", new Object[] { Long.valueOf(param1Long), Long.valueOf(this.limit), Integer.valueOf(1) }));
    }
    
    public void writeFixed64NoTag(long param1Long) throws IOException {
      this.buffer.putLong(bufferPos(this.position), param1Long);
      this.position += 8L;
    }
    
    public void write(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws IOException {
      if (param1ArrayOfbyte != null && param1Int1 >= 0 && param1Int2 >= 0 && param1ArrayOfbyte.length - param1Int2 >= param1Int1) {
        long l1 = this.limit, l2 = param1Int2, l3 = this.position;
        if (l1 - l2 >= l3) {
          UnsafeUtil.copyMemory(param1ArrayOfbyte, param1Int1, l3, param1Int2);
          this.position += param1Int2;
          return;
        } 
      } 
      if (param1ArrayOfbyte == null)
        throw new NullPointerException("value"); 
      long l = this.position;
      throw new CodedOutputStream.OutOfSpaceException(String.format("Pos: %d, limit: %d, len: %d", new Object[] { Long.valueOf(l), Long.valueOf(this.limit), Integer.valueOf(param1Int2) }));
    }
    
    public void writeLazy(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws IOException {
      write(param1ArrayOfbyte, param1Int1, param1Int2);
    }
    
    public void write(ByteBuffer param1ByteBuffer) throws IOException {
      try {
        int i = param1ByteBuffer.remaining();
        repositionBuffer(this.position);
        this.buffer.put(param1ByteBuffer);
        this.position += i;
        return;
      } catch (BufferOverflowException bufferOverflowException) {
        throw new CodedOutputStream.OutOfSpaceException(bufferOverflowException);
      } 
    }
    
    public void writeLazy(ByteBuffer param1ByteBuffer) throws IOException {
      write(param1ByteBuffer);
    }
    
    public void writeStringNoTag(String param1String) throws IOException {
      long l = this.position;
      try {
        int i = param1String.length();
        int j = computeUInt32SizeNoTag(i * 3);
        i = computeUInt32SizeNoTag(param1String.length());
        if (i == j) {
          i = bufferPos(this.position) + i;
          this.buffer.position(i);
          Utf8.encodeUtf8(param1String, this.buffer);
          i = this.buffer.position() - i;
          writeUInt32NoTag(i);
          this.position += i;
        } else {
          i = Utf8.encodedLength(param1String);
          writeUInt32NoTag(i);
          repositionBuffer(this.position);
          Utf8.encodeUtf8(param1String, this.buffer);
          this.position += i;
        } 
      } catch (UnpairedSurrogateException unpairedSurrogateException) {
        this.position = l;
        repositionBuffer(l);
        inefficientWriteStringNoTag(param1String, unpairedSurrogateException);
      } catch (IllegalArgumentException illegalArgumentException) {
        throw new CodedOutputStream.OutOfSpaceException(illegalArgumentException);
      } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
        throw new CodedOutputStream.OutOfSpaceException(indexOutOfBoundsException);
      } 
    }
    
    public void flush() {
      this.originalBuffer.position(bufferPos(this.position));
    }
    
    public int spaceLeft() {
      return (int)(this.limit - this.position);
    }
    
    public int getTotalBytesWritten() {
      return (int)(this.position - this.initialPosition);
    }
    
    private void repositionBuffer(long param1Long) {
      this.buffer.position(bufferPos(param1Long));
    }
    
    private int bufferPos(long param1Long) {
      return (int)(param1Long - this.address);
    }
  }
  
  class AbstractBufferedEncoder extends CodedOutputStream {
    final byte[] buffer;
    
    final int limit;
    
    int position;
    
    int totalBytesWritten;
    
    AbstractBufferedEncoder(CodedOutputStream this$0) {
      if (this$0 >= null) {
        byte[] arrayOfByte = new byte[Math.max(this$0, 20)];
        this.limit = arrayOfByte.length;
        return;
      } 
      throw new IllegalArgumentException("bufferSize must be >= 0");
    }
    
    public final int spaceLeft() {
      throw new UnsupportedOperationException("spaceLeft() can only be called on CodedOutputStreams that are writing to a flat array or ByteBuffer.");
    }
    
    public final int getTotalBytesWritten() {
      return this.totalBytesWritten;
    }
    
    final void buffer(byte param1Byte) {
      byte[] arrayOfByte = this.buffer;
      int i = this.position;
      this.position = i + 1;
      arrayOfByte[i] = param1Byte;
      this.totalBytesWritten++;
    }
    
    final void bufferTag(int param1Int1, int param1Int2) {
      bufferUInt32NoTag(WireFormat.makeTag(param1Int1, param1Int2));
    }
    
    final void bufferInt32NoTag(int param1Int) {
      if (param1Int >= 0) {
        bufferUInt32NoTag(param1Int);
      } else {
        bufferUInt64NoTag(param1Int);
      } 
    }
    
    final void bufferUInt32NoTag(int param1Int) {
      int i = param1Int;
      if (CodedOutputStream.HAS_UNSAFE_ARRAY_OPERATIONS) {
        long l = this.position;
        while (true) {
          if ((param1Int & 0xFFFFFF80) == 0) {
            byte[] arrayOfByte1 = this.buffer;
            i = this.position;
            this.position = i + 1;
            UnsafeUtil.putByte(arrayOfByte1, i, (byte)param1Int);
            param1Int = (int)(this.position - l);
            this.totalBytesWritten += param1Int;
            return;
          } 
          byte[] arrayOfByte = this.buffer;
          i = this.position;
          this.position = i + 1;
          UnsafeUtil.putByte(arrayOfByte, i, (byte)(param1Int & 0x7F | 0x80));
          param1Int >>>= 7;
        } 
      } 
      while (true) {
        if ((i & 0xFFFFFF80) == 0) {
          byte[] arrayOfByte1 = this.buffer;
          param1Int = this.position;
          this.position = param1Int + 1;
          arrayOfByte1[param1Int] = (byte)i;
          this.totalBytesWritten++;
          return;
        } 
        byte[] arrayOfByte = this.buffer;
        param1Int = this.position;
        this.position = param1Int + 1;
        arrayOfByte[param1Int] = (byte)(i & 0x7F | 0x80);
        this.totalBytesWritten++;
        i >>>= 7;
      } 
    }
    
    final void bufferUInt64NoTag(long param1Long) {
      long l = param1Long;
      if (CodedOutputStream.HAS_UNSAFE_ARRAY_OPERATIONS) {
        l = this.position;
        while (true) {
          if ((param1Long & 0xFFFFFFFFFFFFFF80L) == 0L) {
            byte[] arrayOfByte1 = this.buffer;
            int j = this.position;
            this.position = j + 1;
            UnsafeUtil.putByte(arrayOfByte1, j, (byte)(int)param1Long);
            j = (int)(this.position - l);
            this.totalBytesWritten += j;
            return;
          } 
          byte[] arrayOfByte = this.buffer;
          int i = this.position;
          this.position = i + 1;
          UnsafeUtil.putByte(arrayOfByte, i, (byte)((int)param1Long & 0x7F | 0x80));
          param1Long >>>= 7L;
        } 
      } 
      while (true) {
        if ((l & 0xFFFFFFFFFFFFFF80L) == 0L) {
          byte[] arrayOfByte1 = this.buffer;
          int j = this.position;
          this.position = j + 1;
          arrayOfByte1[j] = (byte)(int)l;
          this.totalBytesWritten++;
          return;
        } 
        byte[] arrayOfByte = this.buffer;
        int i = this.position;
        this.position = i + 1;
        arrayOfByte[i] = (byte)((int)l & 0x7F | 0x80);
        this.totalBytesWritten++;
        l >>>= 7L;
      } 
    }
    
    final void bufferFixed32NoTag(int param1Int) {
      byte[] arrayOfByte = this.buffer;
      int i = this.position, j = i + 1;
      arrayOfByte[i] = (byte)(param1Int & 0xFF);
      this.position = i = j + 1;
      arrayOfByte[j] = (byte)(param1Int >> 8 & 0xFF);
      this.position = j = i + 1;
      arrayOfByte[i] = (byte)(param1Int >> 16 & 0xFF);
      this.position = j + 1;
      arrayOfByte[j] = (byte)(param1Int >> 24 & 0xFF);
      this.totalBytesWritten += 4;
    }
    
    final void bufferFixed64NoTag(long param1Long) {
      byte[] arrayOfByte = this.buffer;
      int i = this.position, j = i + 1;
      arrayOfByte[i] = (byte)(int)(param1Long & 0xFFL);
      this.position = i = j + 1;
      arrayOfByte[j] = (byte)(int)(param1Long >> 8L & 0xFFL);
      this.position = j = i + 1;
      arrayOfByte[i] = (byte)(int)(param1Long >> 16L & 0xFFL);
      this.position = i = j + 1;
      arrayOfByte[j] = (byte)(int)(0xFFL & param1Long >> 24L);
      this.position = j = i + 1;
      arrayOfByte[i] = (byte)((int)(param1Long >> 32L) & 0xFF);
      this.position = i = j + 1;
      arrayOfByte[j] = (byte)((int)(param1Long >> 40L) & 0xFF);
      this.position = j = i + 1;
      arrayOfByte[i] = (byte)((int)(param1Long >> 48L) & 0xFF);
      this.position = j + 1;
      arrayOfByte[j] = (byte)((int)(param1Long >> 56L) & 0xFF);
      this.totalBytesWritten += 8;
    }
  }
  
  class ByteOutputEncoder extends AbstractBufferedEncoder {
    private final ByteOutput out;
    
    ByteOutputEncoder(CodedOutputStream this$0, int param1Int) {
      super(param1Int);
      if (this$0 != null) {
        this.out = this$0;
        return;
      } 
      throw new NullPointerException("out");
    }
    
    public void writeTag(int param1Int1, int param1Int2) throws IOException {
      writeUInt32NoTag(WireFormat.makeTag(param1Int1, param1Int2));
    }
    
    public void writeInt32(int param1Int1, int param1Int2) throws IOException {
      flushIfNotAvailable(20);
      bufferTag(param1Int1, 0);
      bufferInt32NoTag(param1Int2);
    }
    
    public void writeUInt32(int param1Int1, int param1Int2) throws IOException {
      flushIfNotAvailable(20);
      bufferTag(param1Int1, 0);
      bufferUInt32NoTag(param1Int2);
    }
    
    public void writeFixed32(int param1Int1, int param1Int2) throws IOException {
      flushIfNotAvailable(14);
      bufferTag(param1Int1, 5);
      bufferFixed32NoTag(param1Int2);
    }
    
    public void writeUInt64(int param1Int, long param1Long) throws IOException {
      flushIfNotAvailable(20);
      bufferTag(param1Int, 0);
      bufferUInt64NoTag(param1Long);
    }
    
    public void writeFixed64(int param1Int, long param1Long) throws IOException {
      flushIfNotAvailable(18);
      bufferTag(param1Int, 1);
      bufferFixed64NoTag(param1Long);
    }
    
    public void writeBool(int param1Int, boolean param1Boolean) throws IOException {
      flushIfNotAvailable(11);
      bufferTag(param1Int, 0);
      buffer((byte)param1Boolean);
    }
    
    public void writeString(int param1Int, String param1String) throws IOException {
      writeTag(param1Int, 2);
      writeStringNoTag(param1String);
    }
    
    public void writeBytes(int param1Int, ByteString param1ByteString) throws IOException {
      writeTag(param1Int, 2);
      writeBytesNoTag(param1ByteString);
    }
    
    public void writeByteArray(int param1Int, byte[] param1ArrayOfbyte) throws IOException {
      writeByteArray(param1Int, param1ArrayOfbyte, 0, param1ArrayOfbyte.length);
    }
    
    public void writeByteArray(int param1Int1, byte[] param1ArrayOfbyte, int param1Int2, int param1Int3) throws IOException {
      writeTag(param1Int1, 2);
      writeByteArrayNoTag(param1ArrayOfbyte, param1Int2, param1Int3);
    }
    
    public void writeByteBuffer(int param1Int, ByteBuffer param1ByteBuffer) throws IOException {
      writeTag(param1Int, 2);
      writeUInt32NoTag(param1ByteBuffer.capacity());
      writeRawBytes(param1ByteBuffer);
    }
    
    public void writeBytesNoTag(ByteString param1ByteString) throws IOException {
      writeUInt32NoTag(param1ByteString.size());
      param1ByteString.writeTo(this);
    }
    
    public void writeByteArrayNoTag(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws IOException {
      writeUInt32NoTag(param1Int2);
      write(param1ArrayOfbyte, param1Int1, param1Int2);
    }
    
    public void writeRawBytes(ByteBuffer param1ByteBuffer) throws IOException {
      if (param1ByteBuffer.hasArray()) {
        write(param1ByteBuffer.array(), param1ByteBuffer.arrayOffset(), param1ByteBuffer.capacity());
      } else {
        param1ByteBuffer = param1ByteBuffer.duplicate();
        param1ByteBuffer.clear();
        write(param1ByteBuffer);
      } 
    }
    
    public void writeMessage(int param1Int, MessageLite param1MessageLite) throws IOException {
      writeTag(param1Int, 2);
      writeMessageNoTag(param1MessageLite);
    }
    
    void writeMessage(int param1Int, MessageLite param1MessageLite, Schema param1Schema) throws IOException {
      writeTag(param1Int, 2);
      writeMessageNoTag(param1MessageLite, param1Schema);
    }
    
    public void writeMessageSetExtension(int param1Int, MessageLite param1MessageLite) throws IOException {
      writeTag(1, 3);
      writeUInt32(2, param1Int);
      writeMessage(3, param1MessageLite);
      writeTag(1, 4);
    }
    
    public void writeRawMessageSetExtension(int param1Int, ByteString param1ByteString) throws IOException {
      writeTag(1, 3);
      writeUInt32(2, param1Int);
      writeBytes(3, param1ByteString);
      writeTag(1, 4);
    }
    
    public void writeMessageNoTag(MessageLite param1MessageLite) throws IOException {
      writeUInt32NoTag(param1MessageLite.getSerializedSize());
      param1MessageLite.writeTo(this);
    }
    
    void writeMessageNoTag(MessageLite param1MessageLite, Schema<MessageLite> param1Schema) throws IOException {
      writeUInt32NoTag(((AbstractMessageLite)param1MessageLite).getSerializedSize(param1Schema));
      param1Schema.writeTo(param1MessageLite, this.wrapper);
    }
    
    public void write(byte param1Byte) throws IOException {
      if (this.position == this.limit)
        doFlush(); 
      buffer(param1Byte);
    }
    
    public void writeInt32NoTag(int param1Int) throws IOException {
      if (param1Int >= 0) {
        writeUInt32NoTag(param1Int);
      } else {
        writeUInt64NoTag(param1Int);
      } 
    }
    
    public void writeUInt32NoTag(int param1Int) throws IOException {
      flushIfNotAvailable(5);
      bufferUInt32NoTag(param1Int);
    }
    
    public void writeFixed32NoTag(int param1Int) throws IOException {
      flushIfNotAvailable(4);
      bufferFixed32NoTag(param1Int);
    }
    
    public void writeUInt64NoTag(long param1Long) throws IOException {
      flushIfNotAvailable(10);
      bufferUInt64NoTag(param1Long);
    }
    
    public void writeFixed64NoTag(long param1Long) throws IOException {
      flushIfNotAvailable(8);
      bufferFixed64NoTag(param1Long);
    }
    
    public void writeStringNoTag(String param1String) throws IOException {
      int i = param1String.length() * 3;
      int j = computeUInt32SizeNoTag(i);
      if (j + i > this.limit) {
        byte[] arrayOfByte = new byte[i];
        i = Utf8.encode(param1String, arrayOfByte, 0, i);
        writeUInt32NoTag(i);
        writeLazy(arrayOfByte, 0, i);
        return;
      } 
      if (j + i > this.limit - this.position)
        doFlush(); 
      i = this.position;
      try {
        int k = computeUInt32SizeNoTag(param1String.length());
        if (k == j) {
          this.position = i + k;
          j = Utf8.encode(param1String, this.buffer, this.position, this.limit - this.position);
          this.position = i;
          k = j - i - k;
          bufferUInt32NoTag(k);
          this.position = j;
          this.totalBytesWritten += k;
        } else {
          k = Utf8.encodedLength(param1String);
          bufferUInt32NoTag(k);
          this.position = Utf8.encode(param1String, this.buffer, this.position, k);
          this.totalBytesWritten += k;
        } 
      } catch (UnpairedSurrogateException unpairedSurrogateException) {
        this.totalBytesWritten -= this.position - i;
        this.position = i;
        inefficientWriteStringNoTag(param1String, unpairedSurrogateException);
      } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
        throw new CodedOutputStream.OutOfSpaceException(indexOutOfBoundsException);
      } 
    }
    
    public void flush() throws IOException {
      if (this.position > 0)
        doFlush(); 
    }
    
    public void write(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws IOException {
      flush();
      this.out.write(param1ArrayOfbyte, param1Int1, param1Int2);
      this.totalBytesWritten += param1Int2;
    }
    
    public void writeLazy(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws IOException {
      flush();
      this.out.writeLazy(param1ArrayOfbyte, param1Int1, param1Int2);
      this.totalBytesWritten += param1Int2;
    }
    
    public void write(ByteBuffer param1ByteBuffer) throws IOException {
      flush();
      int i = param1ByteBuffer.remaining();
      this.out.write(param1ByteBuffer);
      this.totalBytesWritten += i;
    }
    
    public void writeLazy(ByteBuffer param1ByteBuffer) throws IOException {
      flush();
      int i = param1ByteBuffer.remaining();
      this.out.writeLazy(param1ByteBuffer);
      this.totalBytesWritten += i;
    }
    
    private void flushIfNotAvailable(int param1Int) throws IOException {
      if (this.limit - this.position < param1Int)
        doFlush(); 
    }
    
    private void doFlush() throws IOException {
      this.out.write(this.buffer, 0, this.position);
      this.position = 0;
    }
  }
  
  class OutputStreamEncoder extends AbstractBufferedEncoder {
    private final OutputStream out;
    
    OutputStreamEncoder(CodedOutputStream this$0, int param1Int) {
      super(param1Int);
      if (this$0 != null) {
        this.out = (OutputStream)this$0;
        return;
      } 
      throw new NullPointerException("out");
    }
    
    public void writeTag(int param1Int1, int param1Int2) throws IOException {
      writeUInt32NoTag(WireFormat.makeTag(param1Int1, param1Int2));
    }
    
    public void writeInt32(int param1Int1, int param1Int2) throws IOException {
      flushIfNotAvailable(20);
      bufferTag(param1Int1, 0);
      bufferInt32NoTag(param1Int2);
    }
    
    public void writeUInt32(int param1Int1, int param1Int2) throws IOException {
      flushIfNotAvailable(20);
      bufferTag(param1Int1, 0);
      bufferUInt32NoTag(param1Int2);
    }
    
    public void writeFixed32(int param1Int1, int param1Int2) throws IOException {
      flushIfNotAvailable(14);
      bufferTag(param1Int1, 5);
      bufferFixed32NoTag(param1Int2);
    }
    
    public void writeUInt64(int param1Int, long param1Long) throws IOException {
      flushIfNotAvailable(20);
      bufferTag(param1Int, 0);
      bufferUInt64NoTag(param1Long);
    }
    
    public void writeFixed64(int param1Int, long param1Long) throws IOException {
      flushIfNotAvailable(18);
      bufferTag(param1Int, 1);
      bufferFixed64NoTag(param1Long);
    }
    
    public void writeBool(int param1Int, boolean param1Boolean) throws IOException {
      flushIfNotAvailable(11);
      bufferTag(param1Int, 0);
      buffer((byte)param1Boolean);
    }
    
    public void writeString(int param1Int, String param1String) throws IOException {
      writeTag(param1Int, 2);
      writeStringNoTag(param1String);
    }
    
    public void writeBytes(int param1Int, ByteString param1ByteString) throws IOException {
      writeTag(param1Int, 2);
      writeBytesNoTag(param1ByteString);
    }
    
    public void writeByteArray(int param1Int, byte[] param1ArrayOfbyte) throws IOException {
      writeByteArray(param1Int, param1ArrayOfbyte, 0, param1ArrayOfbyte.length);
    }
    
    public void writeByteArray(int param1Int1, byte[] param1ArrayOfbyte, int param1Int2, int param1Int3) throws IOException {
      writeTag(param1Int1, 2);
      writeByteArrayNoTag(param1ArrayOfbyte, param1Int2, param1Int3);
    }
    
    public void writeByteBuffer(int param1Int, ByteBuffer param1ByteBuffer) throws IOException {
      writeTag(param1Int, 2);
      writeUInt32NoTag(param1ByteBuffer.capacity());
      writeRawBytes(param1ByteBuffer);
    }
    
    public void writeBytesNoTag(ByteString param1ByteString) throws IOException {
      writeUInt32NoTag(param1ByteString.size());
      param1ByteString.writeTo(this);
    }
    
    public void writeByteArrayNoTag(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws IOException {
      writeUInt32NoTag(param1Int2);
      write(param1ArrayOfbyte, param1Int1, param1Int2);
    }
    
    public void writeRawBytes(ByteBuffer param1ByteBuffer) throws IOException {
      if (param1ByteBuffer.hasArray()) {
        write(param1ByteBuffer.array(), param1ByteBuffer.arrayOffset(), param1ByteBuffer.capacity());
      } else {
        param1ByteBuffer = param1ByteBuffer.duplicate();
        param1ByteBuffer.clear();
        write(param1ByteBuffer);
      } 
    }
    
    public void writeMessage(int param1Int, MessageLite param1MessageLite) throws IOException {
      writeTag(param1Int, 2);
      writeMessageNoTag(param1MessageLite);
    }
    
    void writeMessage(int param1Int, MessageLite param1MessageLite, Schema param1Schema) throws IOException {
      writeTag(param1Int, 2);
      writeMessageNoTag(param1MessageLite, param1Schema);
    }
    
    public void writeMessageSetExtension(int param1Int, MessageLite param1MessageLite) throws IOException {
      writeTag(1, 3);
      writeUInt32(2, param1Int);
      writeMessage(3, param1MessageLite);
      writeTag(1, 4);
    }
    
    public void writeRawMessageSetExtension(int param1Int, ByteString param1ByteString) throws IOException {
      writeTag(1, 3);
      writeUInt32(2, param1Int);
      writeBytes(3, param1ByteString);
      writeTag(1, 4);
    }
    
    public void writeMessageNoTag(MessageLite param1MessageLite) throws IOException {
      writeUInt32NoTag(param1MessageLite.getSerializedSize());
      param1MessageLite.writeTo(this);
    }
    
    void writeMessageNoTag(MessageLite param1MessageLite, Schema<MessageLite> param1Schema) throws IOException {
      writeUInt32NoTag(((AbstractMessageLite)param1MessageLite).getSerializedSize(param1Schema));
      param1Schema.writeTo(param1MessageLite, this.wrapper);
    }
    
    public void write(byte param1Byte) throws IOException {
      if (this.position == this.limit)
        doFlush(); 
      buffer(param1Byte);
    }
    
    public void writeInt32NoTag(int param1Int) throws IOException {
      if (param1Int >= 0) {
        writeUInt32NoTag(param1Int);
      } else {
        writeUInt64NoTag(param1Int);
      } 
    }
    
    public void writeUInt32NoTag(int param1Int) throws IOException {
      flushIfNotAvailable(5);
      bufferUInt32NoTag(param1Int);
    }
    
    public void writeFixed32NoTag(int param1Int) throws IOException {
      flushIfNotAvailable(4);
      bufferFixed32NoTag(param1Int);
    }
    
    public void writeUInt64NoTag(long param1Long) throws IOException {
      flushIfNotAvailable(10);
      bufferUInt64NoTag(param1Long);
    }
    
    public void writeFixed64NoTag(long param1Long) throws IOException {
      flushIfNotAvailable(8);
      bufferFixed64NoTag(param1Long);
    }
    
    public void writeStringNoTag(String param1String) throws IOException {
      try {
        int i = param1String.length() * 3;
        int j = computeUInt32SizeNoTag(i);
        if (j + i > this.limit) {
          byte[] arrayOfByte = new byte[i];
          i = Utf8.encode(param1String, arrayOfByte, 0, i);
          writeUInt32NoTag(i);
          writeLazy(arrayOfByte, 0, i);
          return;
        } 
        if (j + i > this.limit - this.position)
          doFlush(); 
        i = computeUInt32SizeNoTag(param1String.length());
        int k = this.position;
        if (i == j) {
          try {
            this.position = k + i;
            j = Utf8.encode(param1String, this.buffer, this.position, this.limit - this.position);
            this.position = k;
            i = j - k - i;
            bufferUInt32NoTag(i);
            this.position = j;
            this.totalBytesWritten += i;
          } catch (UnpairedSurrogateException unpairedSurrogateException) {
            this.totalBytesWritten -= this.position - k;
            this.position = k;
            throw unpairedSurrogateException;
          } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) {
            CodedOutputStream.OutOfSpaceException outOfSpaceException = new CodedOutputStream.OutOfSpaceException();
            this(arrayIndexOutOfBoundsException);
            throw outOfSpaceException;
          } 
        } else {
          i = Utf8.encodedLength(param1String);
          bufferUInt32NoTag(i);
          this.position = Utf8.encode(param1String, this.buffer, this.position, i);
          this.totalBytesWritten += i;
        } 
      } catch (UnpairedSurrogateException unpairedSurrogateException) {
        inefficientWriteStringNoTag(param1String, unpairedSurrogateException);
      } 
    }
    
    public void flush() throws IOException {
      if (this.position > 0)
        doFlush(); 
    }
    
    public void write(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws IOException {
      if (this.limit - this.position >= param1Int2) {
        System.arraycopy(param1ArrayOfbyte, param1Int1, this.buffer, this.position, param1Int2);
        this.position += param1Int2;
        this.totalBytesWritten += param1Int2;
      } else {
        int i = this.limit - this.position;
        System.arraycopy(param1ArrayOfbyte, param1Int1, this.buffer, this.position, i);
        param1Int1 += i;
        param1Int2 -= i;
        this.position = this.limit;
        this.totalBytesWritten += i;
        doFlush();
        if (param1Int2 <= this.limit) {
          System.arraycopy(param1ArrayOfbyte, param1Int1, this.buffer, 0, param1Int2);
          this.position = param1Int2;
        } else {
          this.out.write(param1ArrayOfbyte, param1Int1, param1Int2);
        } 
        this.totalBytesWritten += param1Int2;
      } 
    }
    
    public void writeLazy(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws IOException {
      write(param1ArrayOfbyte, param1Int1, param1Int2);
    }
    
    public void write(ByteBuffer param1ByteBuffer) throws IOException {
      int i = param1ByteBuffer.remaining();
      if (this.limit - this.position >= i) {
        param1ByteBuffer.get(this.buffer, this.position, i);
        this.position += i;
        this.totalBytesWritten += i;
      } else {
        int j = this.limit - this.position;
        param1ByteBuffer.get(this.buffer, this.position, j);
        i -= j;
        this.position = this.limit;
        this.totalBytesWritten += j;
        doFlush();
        while (i > this.limit) {
          param1ByteBuffer.get(this.buffer, 0, this.limit);
          this.out.write(this.buffer, 0, this.limit);
          i -= this.limit;
          this.totalBytesWritten += this.limit;
        } 
        param1ByteBuffer.get(this.buffer, 0, i);
        this.position = i;
        this.totalBytesWritten += i;
      } 
    }
    
    public void writeLazy(ByteBuffer param1ByteBuffer) throws IOException {
      write(param1ByteBuffer);
    }
    
    private void flushIfNotAvailable(int param1Int) throws IOException {
      if (this.limit - this.position < param1Int)
        doFlush(); 
    }
    
    private void doFlush() throws IOException {
      this.out.write(this.buffer, 0, this.position);
      this.position = 0;
    }
  }
}
