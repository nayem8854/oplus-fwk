package com.android.framework.protobuf;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.Map;

public class MapEntryLite<K, V> {
  private static final int KEY_FIELD_NUMBER = 1;
  
  private static final int VALUE_FIELD_NUMBER = 2;
  
  private final K key;
  
  private final Metadata<K, V> metadata;
  
  private final V value;
  
  static class Metadata<K, V> {
    public final K defaultKey;
    
    public final V defaultValue;
    
    public final WireFormat.FieldType keyType;
    
    public final WireFormat.FieldType valueType;
    
    public Metadata(WireFormat.FieldType param1FieldType1, K param1K, WireFormat.FieldType param1FieldType2, V param1V) {
      this.keyType = param1FieldType1;
      this.defaultKey = param1K;
      this.valueType = param1FieldType2;
      this.defaultValue = param1V;
    }
  }
  
  private MapEntryLite(WireFormat.FieldType paramFieldType1, K paramK, WireFormat.FieldType paramFieldType2, V paramV) {
    this.metadata = new Metadata<>(paramFieldType1, paramK, paramFieldType2, paramV);
    this.key = paramK;
    this.value = paramV;
  }
  
  private MapEntryLite(Metadata<K, V> paramMetadata, K paramK, V paramV) {
    this.metadata = paramMetadata;
    this.key = paramK;
    this.value = paramV;
  }
  
  public K getKey() {
    return this.key;
  }
  
  public V getValue() {
    return this.value;
  }
  
  public static <K, V> MapEntryLite<K, V> newDefaultInstance(WireFormat.FieldType paramFieldType1, K paramK, WireFormat.FieldType paramFieldType2, V paramV) {
    return new MapEntryLite<>(paramFieldType1, paramK, paramFieldType2, paramV);
  }
  
  static <K, V> void writeTo(CodedOutputStream paramCodedOutputStream, Metadata<K, V> paramMetadata, K paramK, V paramV) throws IOException {
    FieldSet.writeElement(paramCodedOutputStream, paramMetadata.keyType, 1, paramK);
    FieldSet.writeElement(paramCodedOutputStream, paramMetadata.valueType, 2, paramV);
  }
  
  static <K, V> int computeSerializedSize(Metadata<K, V> paramMetadata, K paramK, V paramV) {
    int i = FieldSet.computeElementSize(paramMetadata.keyType, 1, paramK);
    WireFormat.FieldType fieldType = paramMetadata.valueType;
    int j = FieldSet.computeElementSize(fieldType, 2, paramV);
    return i + j;
  }
  
  static <T> T parseField(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite, WireFormat.FieldType paramFieldType, T paramT) throws IOException {
    int i = null.$SwitchMap$com$google$protobuf$WireFormat$FieldType[paramFieldType.ordinal()];
    if (i != 1) {
      if (i != 2) {
        if (i != 3)
          return (T)FieldSet.readPrimitiveField(paramCodedInputStream, paramFieldType, true); 
        throw new RuntimeException("Groups are not allowed in maps.");
      } 
      return (T)Integer.valueOf(paramCodedInputStream.readEnum());
    } 
    MessageLite.Builder builder = ((MessageLite)paramT).toBuilder();
    paramCodedInputStream.readMessage(builder, paramExtensionRegistryLite);
    return (T)builder.buildPartial();
  }
  
  public void serializeTo(CodedOutputStream paramCodedOutputStream, int paramInt, K paramK, V paramV) throws IOException {
    paramCodedOutputStream.writeTag(paramInt, 2);
    paramCodedOutputStream.writeUInt32NoTag(computeSerializedSize(this.metadata, paramK, paramV));
    writeTo(paramCodedOutputStream, this.metadata, paramK, paramV);
  }
  
  public int computeMessageSize(int paramInt, K paramK, V paramV) {
    paramInt = CodedOutputStream.computeTagSize(paramInt);
    Metadata<K, V> metadata = this.metadata;
    int i = computeSerializedSize(metadata, paramK, paramV);
    i = CodedOutputStream.computeLengthDelimitedFieldSize(i);
    return paramInt + i;
  }
  
  public Map.Entry<K, V> parseEntry(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite) throws IOException {
    return parseEntry(paramByteString.newCodedInput(), this.metadata, paramExtensionRegistryLite);
  }
  
  static <K, V> Map.Entry<K, V> parseEntry(CodedInputStream paramCodedInputStream, Metadata<K, V> paramMetadata, ExtensionRegistryLite paramExtensionRegistryLite) throws IOException {
    K k = paramMetadata.defaultKey;
    V v = paramMetadata.defaultValue;
    while (true) {
      K k1;
      V v1;
      int i = paramCodedInputStream.readTag();
      if (i == 0)
        return new AbstractMap.SimpleImmutableEntry<>(k, v); 
      if (i == WireFormat.makeTag(1, paramMetadata.keyType.getWireType())) {
        Object object = parseField(paramCodedInputStream, paramExtensionRegistryLite, paramMetadata.keyType, (Object)k);
        v1 = v;
      } else if (i == WireFormat.makeTag(2, paramMetadata.valueType.getWireType())) {
        Object object = parseField(paramCodedInputStream, paramExtensionRegistryLite, paramMetadata.valueType, (Object)v);
        k1 = k;
      } else {
        k1 = k;
        v1 = v;
        if (!paramCodedInputStream.skipField(i))
          return new AbstractMap.SimpleImmutableEntry<>(k, v); 
      } 
      k = k1;
      v = v1;
    } 
  }
  
  public void parseInto(MapFieldLite<K, V> paramMapFieldLite, CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite) throws IOException {
    int i = paramCodedInputStream.readRawVarint32();
    int j = paramCodedInputStream.pushLimit(i);
    K k = this.metadata.defaultKey;
    V v = this.metadata.defaultValue;
    while (true) {
      i = paramCodedInputStream.readTag();
      if (i != 0) {
        K k1;
        V v1;
        if (i == WireFormat.makeTag(1, this.metadata.keyType.getWireType())) {
          Object object = parseField(paramCodedInputStream, paramExtensionRegistryLite, this.metadata.keyType, (Object)k);
          v1 = v;
        } else if (i == WireFormat.makeTag(2, this.metadata.valueType.getWireType())) {
          Object object = parseField(paramCodedInputStream, paramExtensionRegistryLite, this.metadata.valueType, (Object)v);
          k1 = k;
        } else {
          k1 = k;
          v1 = v;
          if (!paramCodedInputStream.skipField(i)) {
            paramCodedInputStream.checkLastTagWas(0);
            paramCodedInputStream.popLimit(j);
            paramMapFieldLite.put(k, v);
            return;
          } 
        } 
        k = k1;
        v = v1;
        continue;
      } 
      paramCodedInputStream.checkLastTagWas(0);
      paramCodedInputStream.popLimit(j);
      paramMapFieldLite.put(k, v);
      return;
    } 
  }
  
  Metadata<K, V> getMetadata() {
    return this.metadata;
  }
}
