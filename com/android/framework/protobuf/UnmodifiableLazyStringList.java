package com.android.framework.protobuf;

import java.util.AbstractList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

public class UnmodifiableLazyStringList extends AbstractList<String> implements LazyStringList, RandomAccess {
  private final LazyStringList list;
  
  public UnmodifiableLazyStringList(LazyStringList paramLazyStringList) {
    this.list = paramLazyStringList;
  }
  
  public String get(int paramInt) {
    return this.list.get(paramInt);
  }
  
  public Object getRaw(int paramInt) {
    return this.list.getRaw(paramInt);
  }
  
  public int size() {
    return this.list.size();
  }
  
  public ByteString getByteString(int paramInt) {
    return this.list.getByteString(paramInt);
  }
  
  public void add(ByteString paramByteString) {
    throw new UnsupportedOperationException();
  }
  
  public void set(int paramInt, ByteString paramByteString) {
    throw new UnsupportedOperationException();
  }
  
  public boolean addAllByteString(Collection<? extends ByteString> paramCollection) {
    throw new UnsupportedOperationException();
  }
  
  public byte[] getByteArray(int paramInt) {
    return this.list.getByteArray(paramInt);
  }
  
  public void add(byte[] paramArrayOfbyte) {
    throw new UnsupportedOperationException();
  }
  
  public void set(int paramInt, byte[] paramArrayOfbyte) {
    throw new UnsupportedOperationException();
  }
  
  public boolean addAllByteArray(Collection<byte[]> paramCollection) {
    throw new UnsupportedOperationException();
  }
  
  public ListIterator<String> listIterator(int paramInt) {
    return (ListIterator<String>)new Object(this, paramInt);
  }
  
  public Iterator<String> iterator() {
    return (Iterator<String>)new Object(this);
  }
  
  public List<?> getUnderlyingElements() {
    return this.list.getUnderlyingElements();
  }
  
  public void mergeFrom(LazyStringList paramLazyStringList) {
    throw new UnsupportedOperationException();
  }
  
  public List<byte[]> asByteArrayList() {
    return (List)Collections.unmodifiableList((List)this.list.asByteArrayList());
  }
  
  public List<ByteString> asByteStringList() {
    return Collections.unmodifiableList(this.list.asByteStringList());
  }
  
  public LazyStringList getUnmodifiableView() {
    return this;
  }
}
