package com.android.framework.protobuf;

import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.AbstractList;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.RandomAccess;
import java.util.Set;

public final class Internal {
  private static final int DEFAULT_BUFFER_SIZE = 4096;
  
  public static final byte[] EMPTY_BYTE_ARRAY;
  
  public static final ByteBuffer EMPTY_BYTE_BUFFER;
  
  public static final CodedInputStream EMPTY_CODED_INPUT_STREAM;
  
  static final Charset ISO_8859_1;
  
  static final Charset UTF_8 = Charset.forName("UTF-8");
  
  static {
    ISO_8859_1 = Charset.forName("ISO-8859-1");
    byte[] arrayOfByte = new byte[0];
    EMPTY_BYTE_BUFFER = ByteBuffer.wrap(arrayOfByte);
    arrayOfByte = EMPTY_BYTE_ARRAY;
    EMPTY_CODED_INPUT_STREAM = CodedInputStream.newInstance(arrayOfByte);
  }
  
  static <T> T checkNotNull(T paramT) {
    if (paramT != null)
      return paramT; 
    throw null;
  }
  
  static <T> T checkNotNull(T paramT, String paramString) {
    if (paramT != null)
      return paramT; 
    throw new NullPointerException(paramString);
  }
  
  public static String stringDefaultValue(String paramString) {
    return new String(paramString.getBytes(ISO_8859_1), UTF_8);
  }
  
  public static ByteString bytesDefaultValue(String paramString) {
    return ByteString.copyFrom(paramString.getBytes(ISO_8859_1));
  }
  
  public static byte[] byteArrayDefaultValue(String paramString) {
    return paramString.getBytes(ISO_8859_1);
  }
  
  public static ByteBuffer byteBufferDefaultValue(String paramString) {
    return ByteBuffer.wrap(byteArrayDefaultValue(paramString));
  }
  
  public static ByteBuffer copyByteBuffer(ByteBuffer paramByteBuffer) {
    paramByteBuffer = paramByteBuffer.duplicate();
    paramByteBuffer.clear();
    ByteBuffer byteBuffer = ByteBuffer.allocate(paramByteBuffer.capacity());
    byteBuffer.put(paramByteBuffer);
    byteBuffer.clear();
    return byteBuffer;
  }
  
  public static boolean isValidUtf8(ByteString paramByteString) {
    return paramByteString.isValidUtf8();
  }
  
  public static boolean isValidUtf8(byte[] paramArrayOfbyte) {
    return Utf8.isValidUtf8(paramArrayOfbyte);
  }
  
  public static byte[] toByteArray(String paramString) {
    return paramString.getBytes(UTF_8);
  }
  
  public static String toStringUtf8(byte[] paramArrayOfbyte) {
    return new String(paramArrayOfbyte, UTF_8);
  }
  
  public static int hashLong(long paramLong) {
    return (int)(paramLong >>> 32L ^ paramLong);
  }
  
  public static int hashBoolean(boolean paramBoolean) {
    char c;
    if (paramBoolean) {
      c = 'ӏ';
    } else {
      c = 'ӕ';
    } 
    return c;
  }
  
  public static int hashEnum(EnumLite paramEnumLite) {
    return paramEnumLite.getNumber();
  }
  
  public static int hashEnumList(List<? extends EnumLite> paramList) {
    int i = 1;
    for (EnumLite enumLite : paramList)
      i = i * 31 + hashEnum(enumLite); 
    return i;
  }
  
  public static boolean equals(List<byte[]> paramList1, List<byte[]> paramList2) {
    if (paramList1.size() != paramList2.size())
      return false; 
    for (byte b = 0; b < paramList1.size(); b++) {
      if (!Arrays.equals(paramList1.get(b), paramList2.get(b)))
        return false; 
    } 
    return true;
  }
  
  public static int hashCode(List<byte[]> paramList) {
    int i = 1;
    for (byte[] arrayOfByte : paramList)
      i = i * 31 + hashCode(arrayOfByte); 
    return i;
  }
  
  public static int hashCode(byte[] paramArrayOfbyte) {
    return hashCode(paramArrayOfbyte, 0, paramArrayOfbyte.length);
  }
  
  static int hashCode(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    paramInt1 = partialHash(paramInt2, paramArrayOfbyte, paramInt1, paramInt2);
    if (paramInt1 == 0)
      paramInt1 = 1; 
    return paramInt1;
  }
  
  static int partialHash(int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3) {
    for (int i = paramInt2; i < paramInt2 + paramInt3; i++)
      paramInt1 = paramInt1 * 31 + paramArrayOfbyte[i]; 
    return paramInt1;
  }
  
  public static boolean equalsByteBuffer(ByteBuffer paramByteBuffer1, ByteBuffer paramByteBuffer2) {
    if (paramByteBuffer1.capacity() != paramByteBuffer2.capacity())
      return false; 
    return paramByteBuffer1.duplicate().clear().equals(paramByteBuffer2.duplicate().clear());
  }
  
  public static boolean equalsByteBuffer(List<ByteBuffer> paramList1, List<ByteBuffer> paramList2) {
    if (paramList1.size() != paramList2.size())
      return false; 
    for (byte b = 0; b < paramList1.size(); b++) {
      if (!equalsByteBuffer(paramList1.get(b), paramList2.get(b)))
        return false; 
    } 
    return true;
  }
  
  public static int hashCodeByteBuffer(List<ByteBuffer> paramList) {
    int i = 1;
    for (ByteBuffer byteBuffer : paramList)
      i = i * 31 + hashCodeByteBuffer(byteBuffer); 
    return i;
  }
  
  public static int hashCodeByteBuffer(ByteBuffer paramByteBuffer) {
    boolean bool = paramByteBuffer.hasArray();
    boolean bool1 = true;
    int i = 1;
    if (bool) {
      int k = partialHash(paramByteBuffer.capacity(), paramByteBuffer.array(), paramByteBuffer.arrayOffset(), paramByteBuffer.capacity());
      if (k != 0)
        i = k; 
      return i;
    } 
    int j = paramByteBuffer.capacity();
    i = 4096;
    if (j <= 4096)
      i = paramByteBuffer.capacity(); 
    byte[] arrayOfByte = new byte[i];
    ByteBuffer byteBuffer = paramByteBuffer.duplicate();
    byteBuffer.clear();
    j = paramByteBuffer.capacity();
    while (byteBuffer.remaining() > 0) {
      int k;
      if (byteBuffer.remaining() <= i) {
        k = byteBuffer.remaining();
      } else {
        k = i;
      } 
      byteBuffer.get(arrayOfByte, 0, k);
      j = partialHash(j, arrayOfByte, 0, k);
    } 
    if (j == 0) {
      i = bool1;
    } else {
      i = j;
    } 
    return i;
  }
  
  public static <T extends MessageLite> T getDefaultInstance(Class<T> paramClass) {
    try {
      Method method = paramClass.getMethod("getDefaultInstance", new Class[0]);
      return (T)method.invoke(method, new Object[0]);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to get default instance for ");
      stringBuilder.append(paramClass);
      throw new RuntimeException(stringBuilder.toString(), exception);
    } 
  }
  
  static Object mergeMessage(Object paramObject1, Object paramObject2) {
    return ((MessageLite)paramObject1).toBuilder().mergeFrom((MessageLite)paramObject2).buildPartial();
  }
  
  public static class ListAdapter<F, T> extends AbstractList<T> {
    private final Converter<F, T> converter;
    
    private final List<F> fromList;
    
    public ListAdapter(List<F> param1List, Converter<F, T> param1Converter) {
      this.fromList = param1List;
      this.converter = param1Converter;
    }
    
    public T get(int param1Int) {
      return this.converter.convert(this.fromList.get(param1Int));
    }
    
    public int size() {
      return this.fromList.size();
    }
    
    public static interface Converter<F, T> {
      T convert(F param2F);
    }
  }
  
  public static class MapAdapter<K, V, RealValue> extends AbstractMap<K, V> {
    private final Map<K, RealValue> realMap;
    
    private final Converter<RealValue, V> valueConverter;
    
    public static <T extends Internal.EnumLite> Converter<Integer, T> newEnumConverter(Internal.EnumLiteMap<T> param1EnumLiteMap, T param1T) {
      return (Converter<Integer, T>)new Object(param1EnumLiteMap, (Internal.EnumLite)param1T);
    }
    
    public MapAdapter(Map<K, RealValue> param1Map, Converter<RealValue, V> param1Converter) {
      this.realMap = param1Map;
      this.valueConverter = param1Converter;
    }
    
    public V get(Object param1Object) {
      param1Object = this.realMap.get(param1Object);
      if (param1Object == null)
        return null; 
      return this.valueConverter.doForward((RealValue)param1Object);
    }
    
    public V put(K param1K, V param1V) {
      param1K = (K)this.realMap.put(param1K, this.valueConverter.doBackward(param1V));
      if (param1K == null)
        return null; 
      return this.valueConverter.doForward((RealValue)param1K);
    }
    
    public Set<Map.Entry<K, V>> entrySet() {
      return new SetAdapter(this.realMap.entrySet());
    }
    
    private class SetAdapter extends AbstractSet<Map.Entry<K, V>> {
      private final Set<Map.Entry<K, RealValue>> realSet;
      
      final Internal.MapAdapter this$0;
      
      public SetAdapter(Set<Map.Entry<K, RealValue>> param2Set) {
        this.realSet = param2Set;
      }
      
      public Iterator<Map.Entry<K, V>> iterator() {
        return new Internal.MapAdapter.IteratorAdapter(this.realSet.iterator());
      }
      
      public int size() {
        return this.realSet.size();
      }
    }
    
    private class IteratorAdapter implements Iterator<Map.Entry<K, V>> {
      private final Iterator<Map.Entry<K, RealValue>> realIterator;
      
      final Internal.MapAdapter this$0;
      
      public IteratorAdapter(Iterator<Map.Entry<K, RealValue>> param2Iterator) {
        this.realIterator = param2Iterator;
      }
      
      public boolean hasNext() {
        return this.realIterator.hasNext();
      }
      
      public Map.Entry<K, V> next() {
        return new Internal.MapAdapter.EntryAdapter(this.realIterator.next());
      }
      
      public void remove() {
        this.realIterator.remove();
      }
    }
    
    private class EntryAdapter implements Map.Entry<K, V> {
      private final Map.Entry<K, RealValue> realEntry;
      
      final Internal.MapAdapter this$0;
      
      public EntryAdapter(Map.Entry<K, RealValue> param2Entry) {
        this.realEntry = param2Entry;
      }
      
      public K getKey() {
        return this.realEntry.getKey();
      }
      
      public V getValue() {
        return (V)Internal.MapAdapter.this.valueConverter.doForward(this.realEntry.getValue());
      }
      
      public V setValue(V param2V) {
        param2V = (V)this.realEntry.setValue((RealValue)Internal.MapAdapter.this.valueConverter.doBackward(param2V));
        if (param2V == null)
          return null; 
        return (V)Internal.MapAdapter.this.valueConverter.doForward(param2V);
      }
      
      public boolean equals(Object param2Object) {
        boolean bool = true;
        if (param2Object == this)
          return true; 
        if (!(param2Object instanceof Map.Entry))
          return false; 
        param2Object = param2Object;
        if (!getKey().equals(param2Object.getKey()) || !getValue().equals(getValue()))
          bool = false; 
        return bool;
      }
      
      public int hashCode() {
        return this.realEntry.hashCode();
      }
    }
    
    public static interface Converter<A, B> {
      A doBackward(B param2B);
      
      B doForward(A param2A);
    }
  }
  
  private class SetAdapter extends AbstractSet<Map.Entry<K, V>> {
    private final Set<Map.Entry<K, RealValue>> realSet;
    
    final Internal.MapAdapter this$0;
    
    public SetAdapter(Set<Map.Entry<K, RealValue>> param1Set) {
      this.realSet = param1Set;
    }
    
    public Iterator<Map.Entry<K, V>> iterator() {
      return new Internal.MapAdapter.IteratorAdapter(this.realSet.iterator());
    }
    
    public int size() {
      return this.realSet.size();
    }
  }
  
  private class IteratorAdapter implements Iterator<Map.Entry<K, V>> {
    private final Iterator<Map.Entry<K, RealValue>> realIterator;
    
    final Internal.MapAdapter this$0;
    
    public IteratorAdapter(Iterator<Map.Entry<K, RealValue>> param1Iterator) {
      this.realIterator = param1Iterator;
    }
    
    public boolean hasNext() {
      return this.realIterator.hasNext();
    }
    
    public Map.Entry<K, V> next() {
      return new Internal.MapAdapter.EntryAdapter(this.realIterator.next());
    }
    
    public void remove() {
      this.realIterator.remove();
    }
  }
  
  private class EntryAdapter implements Map.Entry<K, V> {
    private final Map.Entry<K, RealValue> realEntry;
    
    final Internal.MapAdapter this$0;
    
    public EntryAdapter(Map.Entry<K, RealValue> param1Entry) {
      this.realEntry = param1Entry;
    }
    
    public K getKey() {
      return this.realEntry.getKey();
    }
    
    public V getValue() {
      return (V)this.this$0.valueConverter.doForward(this.realEntry.getValue());
    }
    
    public V setValue(V param1V) {
      param1V = (V)this.realEntry.setValue((RealValue)this.this$0.valueConverter.doBackward(param1V));
      if (param1V == null)
        return null; 
      return (V)this.this$0.valueConverter.doForward(param1V);
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = true;
      if (param1Object == this)
        return true; 
      if (!(param1Object instanceof Map.Entry))
        return false; 
      param1Object = param1Object;
      if (!getKey().equals(param1Object.getKey()) || !getValue().equals(getValue()))
        bool = false; 
      return bool;
    }
    
    public int hashCode() {
      return this.realEntry.hashCode();
    }
  }
  
  class BooleanList implements ProtobufList<Boolean> {
    public abstract void addBoolean(boolean param1Boolean);
    
    public abstract boolean getBoolean(int param1Int);
    
    public abstract BooleanList mutableCopyWithCapacity(int param1Int);
    
    public abstract boolean setBoolean(int param1Int, boolean param1Boolean);
  }
  
  class DoubleList implements ProtobufList<Double> {
    public abstract void addDouble(double param1Double);
    
    public abstract double getDouble(int param1Int);
    
    public abstract DoubleList mutableCopyWithCapacity(int param1Int);
    
    public abstract double setDouble(int param1Int, double param1Double);
  }
  
  public static interface EnumLite {
    int getNumber();
  }
  
  public static interface EnumLiteMap<T extends EnumLite> {
    T findValueByNumber(int param1Int);
  }
  
  public static interface EnumVerifier {
    boolean isInRange(int param1Int);
  }
  
  class FloatList implements ProtobufList<Float> {
    public abstract void addFloat(float param1Float);
    
    public abstract float getFloat(int param1Int);
    
    public abstract FloatList mutableCopyWithCapacity(int param1Int);
    
    public abstract float setFloat(int param1Int, float param1Float);
  }
  
  class IntList implements ProtobufList<Integer> {
    public abstract void addInt(int param1Int);
    
    public abstract int getInt(int param1Int);
    
    public abstract IntList mutableCopyWithCapacity(int param1Int);
    
    public abstract int setInt(int param1Int1, int param1Int2);
  }
  
  public static interface Converter<F, T> {
    T convert(F param1F);
  }
  
  class LongList implements ProtobufList<Long> {
    public abstract void addLong(long param1Long);
    
    public abstract long getLong(int param1Int);
    
    public abstract LongList mutableCopyWithCapacity(int param1Int);
    
    public abstract long setLong(int param1Int, long param1Long);
  }
  
  public static interface Converter<A, B> {
    A doBackward(B param1B);
    
    B doForward(A param1A);
  }
  
  public static interface ProtobufList<E> extends List<E>, RandomAccess {
    boolean isModifiable();
    
    void makeImmutable();
    
    ProtobufList<E> mutableCopyWithCapacity(int param1Int);
  }
}
