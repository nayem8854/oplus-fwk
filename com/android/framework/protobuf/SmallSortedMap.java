package com.android.framework.protobuf;

import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

class SmallSortedMap<K extends Comparable<K>, V> extends AbstractMap<K, V> {
  private List<Entry> entryList;
  
  private boolean isImmutable;
  
  private volatile DescendingEntrySet lazyDescendingEntrySet;
  
  private volatile EntrySet lazyEntrySet;
  
  private final int maxArraySize;
  
  private Map<K, V> overflowEntries;
  
  private Map<K, V> overflowEntriesDescending;
  
  static <FieldDescriptorType extends FieldSet.FieldDescriptorLite<FieldDescriptorType>> SmallSortedMap<FieldDescriptorType, Object> newFieldMap(int paramInt) {
    return (SmallSortedMap<FieldDescriptorType, Object>)new Object(paramInt);
  }
  
  static <K extends Comparable<K>, V> SmallSortedMap<K, V> newInstanceForTest(int paramInt) {
    return new SmallSortedMap<>(paramInt);
  }
  
  private SmallSortedMap(int paramInt) {
    this.maxArraySize = paramInt;
    this.entryList = Collections.emptyList();
    this.overflowEntries = Collections.emptyMap();
    this.overflowEntriesDescending = Collections.emptyMap();
  }
  
  public void makeImmutable() {
    if (!this.isImmutable) {
      Map<?, ?> map;
      if (this.overflowEntries.isEmpty()) {
        map = Collections.emptyMap();
      } else {
        map = Collections.unmodifiableMap(this.overflowEntries);
      } 
      this.overflowEntries = (Map)map;
      if (this.overflowEntriesDescending.isEmpty()) {
        map = Collections.emptyMap();
      } else {
        map = Collections.unmodifiableMap(this.overflowEntriesDescending);
      } 
      this.overflowEntriesDescending = (Map)map;
      this.isImmutable = true;
    } 
  }
  
  public boolean isImmutable() {
    return this.isImmutable;
  }
  
  public int getNumArrayEntries() {
    return this.entryList.size();
  }
  
  public Map.Entry<K, V> getArrayEntryAt(int paramInt) {
    return this.entryList.get(paramInt);
  }
  
  public int getNumOverflowEntries() {
    return this.overflowEntries.size();
  }
  
  public Iterable<Map.Entry<K, V>> getOverflowEntries() {
    Iterable<?> iterable;
    if (this.overflowEntries.isEmpty()) {
      iterable = EmptySet.iterable();
    } else {
      iterable = this.overflowEntries.entrySet();
    } 
    return (Iterable)iterable;
  }
  
  Iterable<Map.Entry<K, V>> getOverflowEntriesDescending() {
    Iterable<?> iterable;
    if (this.overflowEntriesDescending.isEmpty()) {
      iterable = EmptySet.iterable();
    } else {
      iterable = this.overflowEntriesDescending.entrySet();
    } 
    return (Iterable)iterable;
  }
  
  public int size() {
    return this.entryList.size() + this.overflowEntries.size();
  }
  
  public boolean containsKey(Object paramObject) {
    paramObject = paramObject;
    return (binarySearchInArray((K)paramObject) >= 0 || this.overflowEntries.containsKey(paramObject));
  }
  
  public V get(Object paramObject) {
    paramObject = paramObject;
    int i = binarySearchInArray((K)paramObject);
    if (i >= 0)
      return ((Entry)this.entryList.get(i)).getValue(); 
    return this.overflowEntries.get(paramObject);
  }
  
  public V put(K paramK, V paramV) {
    checkMutable();
    int i = binarySearchInArray(paramK);
    if (i >= 0)
      return ((Entry)this.entryList.get(i)).setValue(paramV); 
    ensureEntryArrayMutable();
    int j = -(i + 1);
    if (j >= this.maxArraySize)
      return getOverflowEntriesMutable().put(paramK, paramV); 
    i = this.entryList.size();
    int k = this.maxArraySize;
    if (i == k) {
      Entry entry = this.entryList.remove(k - 1);
      getOverflowEntriesMutable().put(entry.getKey(), entry.getValue());
    } 
    this.entryList.add(j, new Entry(paramK, paramV));
    return null;
  }
  
  public void clear() {
    checkMutable();
    if (!this.entryList.isEmpty())
      this.entryList.clear(); 
    if (!this.overflowEntries.isEmpty())
      this.overflowEntries.clear(); 
  }
  
  public V remove(Object paramObject) {
    checkMutable();
    paramObject = paramObject;
    int i = binarySearchInArray((K)paramObject);
    if (i >= 0)
      return removeArrayEntryAt(i); 
    if (this.overflowEntries.isEmpty())
      return null; 
    return this.overflowEntries.remove(paramObject);
  }
  
  private V removeArrayEntryAt(int paramInt) {
    checkMutable();
    V v = ((Entry)this.entryList.remove(paramInt)).getValue();
    if (!this.overflowEntries.isEmpty()) {
      Iterator<Map.Entry<K, V>> iterator = getOverflowEntriesMutable().entrySet().iterator();
      this.entryList.add(new Entry(iterator.next()));
      iterator.remove();
    } 
    return v;
  }
  
  private int binarySearchInArray(K paramK) {
    int i = 0;
    int j = this.entryList.size() - 1;
    int k = i, m = j;
    if (j >= 0) {
      int n = paramK.compareTo(((Entry)this.entryList.get(j)).getKey());
      if (n > 0)
        return -(j + 2); 
      k = i;
      m = j;
      if (n == 0)
        return j; 
    } 
    while (k <= m) {
      j = (k + m) / 2;
      i = paramK.compareTo(((Entry)this.entryList.get(j)).getKey());
      if (i < 0) {
        m = j - 1;
        continue;
      } 
      if (i > 0) {
        k = j + 1;
        continue;
      } 
      return j;
    } 
    return -(k + 1);
  }
  
  public Set<Map.Entry<K, V>> entrySet() {
    if (this.lazyEntrySet == null)
      this.lazyEntrySet = new EntrySet(); 
    return this.lazyEntrySet;
  }
  
  Set<Map.Entry<K, V>> descendingEntrySet() {
    if (this.lazyDescendingEntrySet == null)
      this.lazyDescendingEntrySet = new DescendingEntrySet(); 
    return this.lazyDescendingEntrySet;
  }
  
  private void checkMutable() {
    if (!this.isImmutable)
      return; 
    throw new UnsupportedOperationException();
  }
  
  private SortedMap<K, V> getOverflowEntriesMutable() {
    checkMutable();
    if (this.overflowEntries.isEmpty() && !(this.overflowEntries instanceof TreeMap)) {
      TreeMap<Object, Object> treeMap = new TreeMap<>();
      this.overflowEntriesDescending = (Map)treeMap.descendingMap();
    } 
    return (SortedMap<K, V>)this.overflowEntries;
  }
  
  private void ensureEntryArrayMutable() {
    checkMutable();
    if (this.entryList.isEmpty() && !(this.entryList instanceof ArrayList))
      this.entryList = new ArrayList<>(this.maxArraySize); 
  }
  
  private class Entry implements Map.Entry<K, V>, Comparable<Entry> {
    private final K key;
    
    final SmallSortedMap this$0;
    
    private V value;
    
    Entry(Map.Entry<K, V> param1Entry) {
      this(param1Entry.getKey(), param1Entry.getValue());
    }
    
    Entry(K param1K, V param1V) {
      this.key = param1K;
      this.value = param1V;
    }
    
    public K getKey() {
      return this.key;
    }
    
    public V getValue() {
      return this.value;
    }
    
    public int compareTo(Entry param1Entry) {
      return getKey().compareTo(param1Entry.getKey());
    }
    
    public V setValue(V param1V) {
      SmallSortedMap.this.checkMutable();
      V v = this.value;
      this.value = param1V;
      return v;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = true;
      if (param1Object == this)
        return true; 
      if (!(param1Object instanceof Map.Entry))
        return false; 
      param1Object = param1Object;
      if (!equals(this.key, param1Object.getKey()) || !equals(this.value, param1Object.getValue()))
        bool = false; 
      return bool;
    }
    
    public int hashCode() {
      int j;
      K k = this.key;
      int i = 0;
      if (k == null) {
        j = 0;
      } else {
        j = k.hashCode();
      } 
      V v = this.value;
      if (v != null)
        i = v.hashCode(); 
      return j ^ i;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(this.key);
      stringBuilder.append("=");
      stringBuilder.append(this.value);
      return stringBuilder.toString();
    }
    
    private boolean equals(Object param1Object1, Object param1Object2) {
      boolean bool;
      if (param1Object1 == null) {
        if (param1Object2 == null) {
          bool = true;
        } else {
          bool = false;
        } 
      } else {
        bool = param1Object1.equals(param1Object2);
      } 
      return bool;
    }
  }
  
  private class EntrySet extends AbstractSet<Map.Entry<K, V>> {
    final SmallSortedMap this$0;
    
    private EntrySet() {}
    
    public Iterator<Map.Entry<K, V>> iterator() {
      return new SmallSortedMap.EntryIterator();
    }
    
    public int size() {
      return SmallSortedMap.this.size();
    }
    
    public boolean contains(Object param1Object) {
      Map.Entry entry = (Map.Entry)param1Object;
      param1Object = SmallSortedMap.this.get(entry.getKey());
      entry = (Map.Entry)entry.getValue();
      return (param1Object == entry || (param1Object != null && param1Object.equals(entry)));
    }
    
    public boolean add(Map.Entry<K, V> param1Entry) {
      if (!contains(param1Entry)) {
        SmallSortedMap.this.put((Comparable)param1Entry.getKey(), param1Entry.getValue());
        return true;
      } 
      return false;
    }
    
    public boolean remove(Object param1Object) {
      param1Object = param1Object;
      if (contains(param1Object)) {
        SmallSortedMap.this.remove(param1Object.getKey());
        return true;
      } 
      return false;
    }
    
    public void clear() {
      SmallSortedMap.this.clear();
    }
  }
  
  class DescendingEntrySet extends EntrySet {
    final SmallSortedMap this$0;
    
    private DescendingEntrySet() {}
    
    public Iterator<Map.Entry<K, V>> iterator() {
      return new SmallSortedMap.DescendingEntryIterator();
    }
  }
  
  private class EntryIterator implements Iterator<Map.Entry<K, V>> {
    private Iterator<Map.Entry<K, V>> lazyOverflowIterator;
    
    private boolean nextCalledBeforeRemove;
    
    private int pos;
    
    final SmallSortedMap this$0;
    
    private EntryIterator() {
      this.pos = -1;
    }
    
    public boolean hasNext() {
      int i = this.pos;
      boolean bool = true;
      if (i + 1 >= SmallSortedMap.access$600(SmallSortedMap.this).size()) {
        SmallSortedMap smallSortedMap = SmallSortedMap.this;
        if (smallSortedMap.overflowEntries.isEmpty() || !getOverflowIterator().hasNext())
          bool = false; 
      } 
      return bool;
    }
    
    public Map.Entry<K, V> next() {
      this.nextCalledBeforeRemove = true;
      int i = this.pos + 1;
      if (i < SmallSortedMap.access$600(SmallSortedMap.this).size())
        return SmallSortedMap.access$600(SmallSortedMap.this).get(this.pos); 
      return getOverflowIterator().next();
    }
    
    public void remove() {
      if (this.nextCalledBeforeRemove) {
        this.nextCalledBeforeRemove = false;
        SmallSortedMap.this.checkMutable();
        if (this.pos < SmallSortedMap.access$600(SmallSortedMap.this).size()) {
          SmallSortedMap smallSortedMap = SmallSortedMap.this;
          int i = this.pos;
          this.pos = i - 1;
          smallSortedMap.removeArrayEntryAt(i);
        } else {
          getOverflowIterator().remove();
        } 
        return;
      } 
      throw new IllegalStateException("remove() was called before next()");
    }
    
    private Iterator<Map.Entry<K, V>> getOverflowIterator() {
      if (this.lazyOverflowIterator == null)
        this.lazyOverflowIterator = SmallSortedMap.this.overflowEntries.entrySet().iterator(); 
      return this.lazyOverflowIterator;
    }
  }
  
  private class DescendingEntryIterator implements Iterator<Map.Entry<K, V>> {
    private Iterator<Map.Entry<K, V>> lazyOverflowIterator;
    
    private int pos = SmallSortedMap.access$600(SmallSortedMap.this).size();
    
    final SmallSortedMap this$0;
    
    public boolean hasNext() {
      boolean bool;
      int i = this.pos;
      if ((i > 0 && i <= SmallSortedMap.access$600(SmallSortedMap.this).size()) || getOverflowIterator().hasNext()) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public Map.Entry<K, V> next() {
      if (getOverflowIterator().hasNext())
        return getOverflowIterator().next(); 
      List<Map.Entry<K, V>> list = SmallSortedMap.access$600(SmallSortedMap.this);
      int i = this.pos - 1;
      return list.get(i);
    }
    
    public void remove() {
      throw new UnsupportedOperationException();
    }
    
    private Iterator<Map.Entry<K, V>> getOverflowIterator() {
      if (this.lazyOverflowIterator == null)
        this.lazyOverflowIterator = SmallSortedMap.this.overflowEntriesDescending.entrySet().iterator(); 
      return this.lazyOverflowIterator;
    }
    
    private DescendingEntryIterator() {}
  }
  
  private static class EmptySet {
    private static final Iterable<Object> ITERABLE = new Iterable() {
        public Iterator<Object> iterator() {
          return SmallSortedMap.EmptySet.ITERATOR;
        }
      };
    
    private static final Iterator<Object> ITERATOR = new Iterator() {
        public boolean hasNext() {
          return false;
        }
        
        public Object next() {
          throw new NoSuchElementException();
        }
        
        public void remove() {
          throw new UnsupportedOperationException();
        }
      };
    
    static {
    
    }
    
    static <T> Iterable<T> iterable() {
      return (Iterable)ITERABLE;
    }
  }
  
  class null implements Iterator<Object> {
    public boolean hasNext() {
      return false;
    }
    
    public Object next() {
      throw new NoSuchElementException();
    }
    
    public void remove() {
      throw new UnsupportedOperationException();
    }
  }
  
  class null implements Iterable<Object> {
    public Iterator<Object> iterator() {
      return SmallSortedMap.EmptySet.ITERATOR;
    }
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof SmallSortedMap))
      return super.equals(paramObject); 
    paramObject = paramObject;
    int i = size();
    if (i != paramObject.size())
      return false; 
    int j = getNumArrayEntries();
    if (j != paramObject.getNumArrayEntries())
      return entrySet().equals(paramObject.entrySet()); 
    for (byte b = 0; b < j; b++) {
      if (!getArrayEntryAt(b).equals(paramObject.getArrayEntryAt(b)))
        return false; 
    } 
    if (j != i)
      return this.overflowEntries.equals(((SmallSortedMap)paramObject).overflowEntries); 
    return true;
  }
  
  public int hashCode() {
    int i = 0;
    int j = getNumArrayEntries();
    int k;
    for (k = 0; k < j; k++)
      i += ((Entry)this.entryList.get(k)).hashCode(); 
    k = i;
    if (getNumOverflowEntries() > 0)
      k = i + this.overflowEntries.hashCode(); 
    return k;
  }
}
