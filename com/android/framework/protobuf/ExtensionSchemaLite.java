package com.android.framework.protobuf;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

final class ExtensionSchemaLite extends ExtensionSchema<GeneratedMessageLite.ExtensionDescriptor> {
  boolean hasExtensions(MessageLite paramMessageLite) {
    return paramMessageLite instanceof GeneratedMessageLite.ExtendableMessage;
  }
  
  FieldSet<GeneratedMessageLite.ExtensionDescriptor> getExtensions(Object paramObject) {
    return ((GeneratedMessageLite.ExtendableMessage)paramObject).extensions;
  }
  
  void setExtensions(Object paramObject, FieldSet<GeneratedMessageLite.ExtensionDescriptor> paramFieldSet) {
    ((GeneratedMessageLite.ExtendableMessage)paramObject).extensions = paramFieldSet;
  }
  
  FieldSet<GeneratedMessageLite.ExtensionDescriptor> getMutableExtensions(Object paramObject) {
    return ((GeneratedMessageLite.ExtendableMessage)paramObject).ensureExtensionsAreMutable();
  }
  
  void makeImmutable(Object paramObject) {
    getExtensions(paramObject).makeImmutable();
  }
  
  <UT, UB> UB parseExtension(Reader paramReader, Object<?> paramObject, ExtensionRegistryLite paramExtensionRegistryLite, FieldSet<GeneratedMessageLite.ExtensionDescriptor> paramFieldSet, UB paramUB, UnknownFieldSchema<UT, UB> paramUnknownFieldSchema) throws IOException {
    Object<?> object;
    GeneratedMessageLite.GeneratedExtension generatedExtension = (GeneratedMessageLite.GeneratedExtension)paramObject;
    int i = generatedExtension.getNumber();
    if (generatedExtension.descriptor.isRepeated() && generatedExtension.descriptor.isPacked()) {
      StringBuilder stringBuilder;
      GeneratedMessageLite.ExtensionDescriptor extensionDescriptor;
      Internal.EnumLiteMap<?> enumLiteMap;
      switch (null.$SwitchMap$com$google$protobuf$WireFormat$FieldType[generatedExtension.getLiteType().ordinal()]) {
        default:
          stringBuilder = new StringBuilder();
          stringBuilder.append("Type cannot be packed: ");
          paramObject = (Object<?>)generatedExtension.descriptor;
          stringBuilder.append(paramObject.getLiteType());
          throw new IllegalStateException(stringBuilder.toString());
        case 14:
          paramObject = (Object<?>)new ArrayList();
          stringBuilder.readEnumList((List)paramObject);
          extensionDescriptor = generatedExtension.descriptor;
          enumLiteMap = extensionDescriptor.getEnumType();
          paramUB = SchemaUtil.filterUnknownEnumList(i, (List)paramObject, enumLiteMap, paramUB, paramUnknownFieldSchema);
          object = paramObject;
          break;
        case 13:
          paramObject = (Object<?>)new ArrayList();
          object.readSInt64List((List)paramObject);
          object = paramObject;
          break;
        case 12:
          paramObject = (Object<?>)new ArrayList();
          object.readSInt32List((List)paramObject);
          object = paramObject;
          break;
        case 11:
          paramObject = (Object<?>)new ArrayList();
          object.readSFixed64List((List)paramObject);
          object = paramObject;
          break;
        case 10:
          paramObject = (Object<?>)new ArrayList();
          object.readSFixed32List((List)paramObject);
          object = paramObject;
          break;
        case 9:
          paramObject = (Object<?>)new ArrayList();
          object.readUInt32List((List)paramObject);
          object = paramObject;
          break;
        case 8:
          paramObject = (Object<?>)new ArrayList();
          object.readBoolList((List)paramObject);
          object = paramObject;
          break;
        case 7:
          paramObject = (Object<?>)new ArrayList();
          object.readFixed32List((List)paramObject);
          object = paramObject;
          break;
        case 6:
          paramObject = (Object<?>)new ArrayList();
          object.readFixed64List((List)paramObject);
          object = paramObject;
          break;
        case 5:
          paramObject = (Object<?>)new ArrayList();
          object.readInt32List((List)paramObject);
          object = paramObject;
          break;
        case 4:
          paramObject = (Object<?>)new ArrayList();
          object.readUInt64List((List)paramObject);
          object = paramObject;
          break;
        case 3:
          paramObject = (Object<?>)new ArrayList();
          object.readInt64List((List)paramObject);
          object = paramObject;
          break;
        case 2:
          paramObject = (Object<?>)new ArrayList();
          object.readFloatList((List)paramObject);
          object = paramObject;
          break;
        case 1:
          paramObject = (Object<?>)new ArrayList();
          object.readDoubleList((List)paramObject);
          object = paramObject;
          break;
      } 
      paramFieldSet.setField(generatedExtension.descriptor, object);
    } else {
      paramObject = null;
      if (generatedExtension.getLiteType() == WireFormat.FieldType.ENUM) {
        int j = object.readInt32();
        object = (Object<?>)generatedExtension.descriptor.getEnumType().findValueByNumber(j);
        if (object == null)
          return SchemaUtil.storeUnknownEnum(i, j, paramUB, paramUnknownFieldSchema); 
        object = (Object<?>)Integer.valueOf(j);
      } else {
        switch (null.$SwitchMap$com$google$protobuf$WireFormat$FieldType[generatedExtension.getLiteType().ordinal()]) {
          default:
            object = paramObject;
            break;
          case 18:
            paramObject = (Object<?>)generatedExtension.getMessageDefaultInstance().getClass();
            object = object.readMessage((Class<?>)paramObject, paramExtensionRegistryLite);
            break;
          case 17:
            paramObject = (Object<?>)generatedExtension.getMessageDefaultInstance().getClass();
            object = object.readGroup((Class<?>)paramObject, paramExtensionRegistryLite);
            break;
          case 16:
            object = (Object<?>)object.readString();
            break;
          case 15:
            object = (Object<?>)object.readBytes();
            break;
          case 14:
            throw new IllegalStateException("Shouldn't reach here.");
          case 13:
            object = (Object<?>)Long.valueOf(object.readSInt64());
            break;
          case 12:
            object = (Object<?>)Integer.valueOf(object.readSInt32());
            break;
          case 11:
            object = (Object<?>)Long.valueOf(object.readSFixed64());
            break;
          case 10:
            object = (Object<?>)Integer.valueOf(object.readSFixed32());
            break;
          case 9:
            object = (Object<?>)Integer.valueOf(object.readUInt32());
            break;
          case 8:
            object = (Object<?>)Boolean.valueOf(object.readBool());
            break;
          case 7:
            object = (Object<?>)Integer.valueOf(object.readFixed32());
            break;
          case 6:
            object = (Object<?>)Long.valueOf(object.readFixed64());
            break;
          case 5:
            object = (Object<?>)Integer.valueOf(object.readInt32());
            break;
          case 4:
            object = (Object<?>)Long.valueOf(object.readUInt64());
            break;
          case 3:
            object = (Object<?>)Long.valueOf(object.readInt64());
            break;
          case 2:
            object = (Object<?>)Float.valueOf(object.readFloat());
            break;
          case 1:
            object = (Object<?>)Double.valueOf(object.readDouble());
            break;
        } 
      } 
      if (generatedExtension.isRepeated()) {
        paramFieldSet.addRepeatedField(generatedExtension.descriptor, object);
      } else {
        int j = null.$SwitchMap$com$google$protobuf$WireFormat$FieldType[generatedExtension.getLiteType().ordinal()];
        if (j != 17 && j != 18) {
          paramObject = object;
        } else {
          Object object1 = paramFieldSet.getField(generatedExtension.descriptor);
          paramObject = object;
          if (object1 != null)
            paramObject = (Object<?>)Internal.mergeMessage(object1, object); 
        } 
        paramFieldSet.setField(generatedExtension.descriptor, paramObject);
      } 
    } 
    return paramUB;
  }
  
  int extensionNumber(Map.Entry<?, ?> paramEntry) {
    GeneratedMessageLite.ExtensionDescriptor extensionDescriptor = (GeneratedMessageLite.ExtensionDescriptor)paramEntry.getKey();
    return extensionDescriptor.getNumber();
  }
  
  void serializeExtension(Writer paramWriter, Map.Entry<?, ?> paramEntry) throws IOException {
    List<?> list;
    Schema<?> schema1, schema2;
    int i;
    GeneratedMessageLite.ExtensionDescriptor extensionDescriptor = (GeneratedMessageLite.ExtensionDescriptor)paramEntry.getKey();
    if (extensionDescriptor.isRepeated()) {
      List<E> list1;
      switch (null.$SwitchMap$com$google$protobuf$WireFormat$FieldType[extensionDescriptor.getLiteType().ordinal()]) {
        default:
          return;
        case 18:
          list1 = (List)paramEntry.getValue();
          if (list1 != null && !list1.isEmpty()) {
            int k = extensionDescriptor.getNumber();
            list = (List)paramEntry.getValue();
            schema2 = Protobuf.getInstance().schemaFor(list1.get(0).getClass());
            SchemaUtil.writeMessageList(k, list, paramWriter, schema2);
          } 
        case 17:
          list1 = list.getValue();
          if (list1 != null && !list1.isEmpty()) {
            int k = schema2.getNumber();
            list = list.getValue();
            schema2 = Protobuf.getInstance().schemaFor(list1.get(0).getClass());
            SchemaUtil.writeGroupList(k, list, paramWriter, schema2);
          } 
        case 16:
          j = schema2.getNumber();
          list = list.getValue();
          SchemaUtil.writeStringList(j, (List)list, paramWriter);
        case 15:
          j = schema2.getNumber();
          list = list.getValue();
          SchemaUtil.writeBytesList(j, (List)list, paramWriter);
        case 14:
          j = schema2.getNumber();
          list = list.getValue();
          bool = schema2.isPacked();
          SchemaUtil.writeInt32List(j, (List)list, paramWriter, bool);
        case 13:
          j = schema2.getNumber();
          list = list.getValue();
          bool = schema2.isPacked();
          SchemaUtil.writeSInt64List(j, (List)list, paramWriter, bool);
        case 12:
          j = schema2.getNumber();
          list = list.getValue();
          bool = schema2.isPacked();
          SchemaUtil.writeSInt32List(j, (List)list, paramWriter, bool);
        case 11:
          j = schema2.getNumber();
          list = list.getValue();
          bool = schema2.isPacked();
          SchemaUtil.writeSFixed64List(j, (List)list, paramWriter, bool);
        case 10:
          j = schema2.getNumber();
          list = list.getValue();
          bool = schema2.isPacked();
          SchemaUtil.writeSFixed32List(j, (List)list, paramWriter, bool);
        case 9:
          j = schema2.getNumber();
          list = list.getValue();
          bool = schema2.isPacked();
          SchemaUtil.writeUInt32List(j, (List)list, paramWriter, bool);
        case 8:
          j = schema2.getNumber();
          list = list.getValue();
          bool = schema2.isPacked();
          SchemaUtil.writeBoolList(j, (List)list, paramWriter, bool);
        case 7:
          j = schema2.getNumber();
          list = list.getValue();
          bool = schema2.isPacked();
          SchemaUtil.writeFixed32List(j, (List)list, paramWriter, bool);
        case 6:
          j = schema2.getNumber();
          list = list.getValue();
          bool = schema2.isPacked();
          SchemaUtil.writeFixed64List(j, (List)list, paramWriter, bool);
        case 5:
          j = schema2.getNumber();
          list = list.getValue();
          bool = schema2.isPacked();
          SchemaUtil.writeInt32List(j, (List)list, paramWriter, bool);
        case 4:
          j = schema2.getNumber();
          list = list.getValue();
          bool = schema2.isPacked();
          SchemaUtil.writeUInt64List(j, (List)list, paramWriter, bool);
        case 3:
          j = schema2.getNumber();
          list = list.getValue();
          bool = schema2.isPacked();
          SchemaUtil.writeInt64List(j, (List)list, paramWriter, bool);
        case 2:
          j = schema2.getNumber();
          list = list.getValue();
          bool = schema2.isPacked();
          SchemaUtil.writeFloatList(j, (List)list, paramWriter, bool);
        case 1:
          break;
      } 
      int j = schema2.getNumber();
      list = list.getValue();
      boolean bool = schema2.isPacked();
      SchemaUtil.writeDoubleList(j, (List)list, paramWriter, bool);
    } 
    switch (null.$SwitchMap$com$google$protobuf$WireFormat$FieldType[schema2.getLiteType().ordinal()]) {
      default:
      
      case 18:
        i = schema2.getNumber();
        schema2 = list.getValue();
        schema1 = Protobuf.getInstance().schemaFor(list.getValue().getClass());
        paramWriter.writeMessage(i, schema2, schema1);
      case 17:
        i = schema2.getNumber();
        schema2 = schema1.getValue();
        schema1 = Protobuf.getInstance().schemaFor(schema1.getValue().getClass());
        paramWriter.writeGroup(i, schema2, schema1);
      case 16:
        paramWriter.writeString(schema2.getNumber(), schema1.getValue());
      case 15:
        paramWriter.writeBytes(schema2.getNumber(), schema1.getValue());
      case 14:
        paramWriter.writeInt32(schema2.getNumber(), ((Integer)schema1.getValue()).intValue());
      case 13:
        paramWriter.writeSInt64(schema2.getNumber(), ((Long)schema1.getValue()).longValue());
      case 12:
        paramWriter.writeSInt32(schema2.getNumber(), ((Integer)schema1.getValue()).intValue());
      case 11:
        paramWriter.writeSFixed64(schema2.getNumber(), ((Long)schema1.getValue()).longValue());
      case 10:
        paramWriter.writeSFixed32(schema2.getNumber(), ((Integer)schema1.getValue()).intValue());
      case 9:
        paramWriter.writeUInt32(schema2.getNumber(), ((Integer)schema1.getValue()).intValue());
      case 8:
        paramWriter.writeBool(schema2.getNumber(), ((Boolean)schema1.getValue()).booleanValue());
      case 7:
        paramWriter.writeFixed32(schema2.getNumber(), ((Integer)schema1.getValue()).intValue());
      case 6:
        paramWriter.writeFixed64(schema2.getNumber(), ((Long)schema1.getValue()).longValue());
      case 5:
        paramWriter.writeInt32(schema2.getNumber(), ((Integer)schema1.getValue()).intValue());
      case 4:
        paramWriter.writeUInt64(schema2.getNumber(), ((Long)schema1.getValue()).longValue());
      case 3:
        paramWriter.writeInt64(schema2.getNumber(), ((Long)schema1.getValue()).longValue());
      case 2:
        paramWriter.writeFloat(schema2.getNumber(), ((Float)schema1.getValue()).floatValue());
      case 1:
        break;
    } 
    paramWriter.writeDouble(schema2.getNumber(), ((Double)schema1.getValue()).doubleValue());
  }
  
  Object findExtensionByNumber(ExtensionRegistryLite paramExtensionRegistryLite, MessageLite paramMessageLite, int paramInt) {
    return paramExtensionRegistryLite.findLiteExtensionByNumber(paramMessageLite, paramInt);
  }
  
  void parseLengthPrefixedMessageSetItem(Reader paramReader, Object paramObject, ExtensionRegistryLite paramExtensionRegistryLite, FieldSet<GeneratedMessageLite.ExtensionDescriptor> paramFieldSet) throws IOException {
    paramObject = paramObject;
    paramReader = paramReader.readMessage(paramObject.getMessageDefaultInstance().getClass(), paramExtensionRegistryLite);
    paramFieldSet.setField(((GeneratedMessageLite.GeneratedExtension)paramObject).descriptor, paramReader);
  }
  
  void parseMessageSetItem(ByteString paramByteString, Object paramObject, ExtensionRegistryLite paramExtensionRegistryLite, FieldSet<GeneratedMessageLite.ExtensionDescriptor> paramFieldSet) throws IOException {
    GeneratedMessageLite.GeneratedExtension generatedExtension = (GeneratedMessageLite.GeneratedExtension)paramObject;
    paramObject = generatedExtension.getMessageDefaultInstance().newBuilderForType().buildPartial();
    BinaryReader binaryReader = BinaryReader.newInstance(ByteBuffer.wrap(paramByteString.toByteArray()), true);
    Protobuf.getInstance().mergeFrom(paramObject, binaryReader, paramExtensionRegistryLite);
    paramFieldSet.setField(generatedExtension.descriptor, paramObject);
    if (binaryReader.getFieldNumber() == Integer.MAX_VALUE)
      return; 
    throw InvalidProtocolBufferException.invalidEndTag();
  }
}
