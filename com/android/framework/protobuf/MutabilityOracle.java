package com.android.framework.protobuf;

interface MutabilityOracle {
  public static final MutabilityOracle IMMUTABLE = (MutabilityOracle)new Object();
  
  void ensureMutable();
}
