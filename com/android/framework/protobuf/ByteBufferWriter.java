package com.android.framework.protobuf;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.ref.SoftReference;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

final class ByteBufferWriter {
  private static final ThreadLocal<SoftReference<byte[]>> BUFFER = new ThreadLocal<>();
  
  private static final float BUFFER_REALLOCATION_THRESHOLD = 0.5F;
  
  private static final long CHANNEL_FIELD_OFFSET;
  
  private static final Class<?> FILE_OUTPUT_STREAM_CLASS;
  
  private static final int MAX_CACHED_BUFFER_SIZE = 16384;
  
  private static final int MIN_CACHED_BUFFER_SIZE = 1024;
  
  static {
    Class<?> clazz = safeGetClass("java.io.FileOutputStream");
    CHANNEL_FIELD_OFFSET = getChannelFieldOffset(clazz);
  }
  
  static void clearCachedBuffer() {
    BUFFER.set(null);
  }
  
  static void write(ByteBuffer paramByteBuffer, OutputStream paramOutputStream) throws IOException {
    int i = paramByteBuffer.position();
    try {
      if (paramByteBuffer.hasArray()) {
        paramOutputStream.write(paramByteBuffer.array(), paramByteBuffer.arrayOffset() + paramByteBuffer.position(), paramByteBuffer.remaining());
      } else if (!writeToChannel(paramByteBuffer, paramOutputStream)) {
        byte[] arrayOfByte = getOrCreateBuffer(paramByteBuffer.remaining());
        while (paramByteBuffer.hasRemaining()) {
          int j = Math.min(paramByteBuffer.remaining(), arrayOfByte.length);
          paramByteBuffer.get(arrayOfByte, 0, j);
          paramOutputStream.write(arrayOfByte, 0, j);
        } 
      } 
      return;
    } finally {
      paramByteBuffer.position(i);
    } 
  }
  
  private static byte[] getOrCreateBuffer(int paramInt) {
    // Byte code:
    //   0: iload_0
    //   1: sipush #1024
    //   4: invokestatic max : (II)I
    //   7: istore_0
    //   8: invokestatic getBuffer : ()[B
    //   11: astore_1
    //   12: aload_1
    //   13: ifnull -> 27
    //   16: aload_1
    //   17: astore_2
    //   18: iload_0
    //   19: aload_1
    //   20: arraylength
    //   21: invokestatic needToReallocate : (II)Z
    //   24: ifeq -> 46
    //   27: iload_0
    //   28: newarray byte
    //   30: astore_1
    //   31: aload_1
    //   32: astore_2
    //   33: iload_0
    //   34: sipush #16384
    //   37: if_icmpgt -> 46
    //   40: aload_1
    //   41: invokestatic setBuffer : ([B)V
    //   44: aload_1
    //   45: astore_2
    //   46: aload_2
    //   47: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #115	-> 0
    //   #117	-> 8
    //   #119	-> 12
    //   #120	-> 27
    //   #123	-> 31
    //   #124	-> 40
    //   #127	-> 46
  }
  
  private static boolean needToReallocate(int paramInt1, int paramInt2) {
    boolean bool;
    if (paramInt2 < paramInt1 && paramInt2 < paramInt1 * 0.5F) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static byte[] getBuffer() {
    byte[] arrayOfByte;
    SoftReference<byte[]> softReference = BUFFER.get();
    if (softReference == null) {
      softReference = null;
    } else {
      arrayOfByte = softReference.get();
    } 
    return arrayOfByte;
  }
  
  private static void setBuffer(byte[] paramArrayOfbyte) {
    BUFFER.set((SoftReference)new SoftReference<>(paramArrayOfbyte));
  }
  
  private static boolean writeToChannel(ByteBuffer paramByteBuffer, OutputStream paramOutputStream) throws IOException {
    if (CHANNEL_FIELD_OFFSET >= 0L && FILE_OUTPUT_STREAM_CLASS.isInstance(paramOutputStream)) {
      ClassCastException classCastException2 = null;
      try {
        WritableByteChannel writableByteChannel = (WritableByteChannel)UnsafeUtil.getObject(paramOutputStream, CHANNEL_FIELD_OFFSET);
      } catch (ClassCastException classCastException1) {
        classCastException1 = classCastException2;
      } 
      if (classCastException1 != null) {
        classCastException1.write(paramByteBuffer);
        return true;
      } 
    } 
    return false;
  }
  
  private static Class<?> safeGetClass(String paramString) {
    try {
      return Class.forName(paramString);
    } catch (ClassNotFoundException classNotFoundException) {
      return null;
    } 
  }
  
  private static long getChannelFieldOffset(Class<?> paramClass) {
    if (paramClass != null)
      try {
        if (UnsafeUtil.hasUnsafeArrayOperations()) {
          Field field = paramClass.getDeclaredField("channel");
          return UnsafeUtil.objectFieldOffset(field);
        } 
      } finally {} 
    return -1L;
  }
}
