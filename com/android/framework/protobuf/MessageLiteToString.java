package com.android.framework.protobuf;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

final class MessageLiteToString {
  private static final String BUILDER_LIST_SUFFIX = "OrBuilderList";
  
  private static final String BYTES_SUFFIX = "Bytes";
  
  private static final String LIST_SUFFIX = "List";
  
  private static final String MAP_SUFFIX = "Map";
  
  static String toString(MessageLite paramMessageLite, String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("# ");
    stringBuilder.append(paramString);
    reflectivePrintWithIndent(paramMessageLite, stringBuilder, 0);
    return stringBuilder.toString();
  }
  
  private static void reflectivePrintWithIndent(MessageLite paramMessageLite, StringBuilder paramStringBuilder, int paramInt) {
    HashMap<Object, Object> hashMap1 = new HashMap<>();
    HashMap<Object, Object> hashMap2 = new HashMap<>();
    TreeSet<String> treeSet = new TreeSet();
    for (Method method : paramMessageLite.getClass().getDeclaredMethods()) {
      hashMap2.put(method.getName(), method);
      if ((method.getParameterTypes()).length == 0) {
        hashMap1.put(method.getName(), method);
        if (method.getName().startsWith("get"))
          treeSet.add(method.getName()); 
      } 
    } 
    for (String str1 : treeSet) {
      String str2 = str1.replaceFirst("get", "");
      if (str2.endsWith("List") && 
        !str2.endsWith("OrBuilderList"))
        if (!str2.equals("List")) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(str2.substring(0, 1).toLowerCase());
          stringBuilder.append(str2.substring(1, str2.length() - "List".length()));
          String str = stringBuilder.toString();
          Method method = (Method)hashMap1.get(str1);
          if (method != null && method.getReturnType().equals(List.class)) {
            str2 = camelCaseToSnakeCase(str);
            str1 = (String)GeneratedMessageLite.invokeOrDie(method, paramMessageLite, new Object[0]);
            printField(paramStringBuilder, paramInt, str2, str1);
            continue;
          } 
        }  
      if (str2.endsWith("Map"))
        if (!str2.equals("Map")) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(str2.substring(0, 1).toLowerCase());
          stringBuilder.append(str2.substring(1, str2.length() - "Map".length()));
          String str = stringBuilder.toString();
          str1 = (String)hashMap1.get(str1);
          if (str1 != null && 
            str1.getReturnType().equals(Map.class))
            if (!str1.isAnnotationPresent((Class)Deprecated.class))
              if (Modifier.isPublic(str1.getModifiers())) {
                str2 = camelCaseToSnakeCase(str);
                str1 = (String)GeneratedMessageLite.invokeOrDie((Method)str1, paramMessageLite, new Object[0]);
                printField(paramStringBuilder, paramInt, str2, str1);
                continue;
              }   
        }  
      str1 = (String)new StringBuilder();
      str1.append("set");
      str1.append(str2);
      str1 = (String)hashMap2.get(str1.toString());
      if (str1 == null)
        continue; 
      if (str2.endsWith("Bytes")) {
        str1 = (String)new StringBuilder();
        str1.append("get");
        str1.append(str2.substring(0, str2.length() - "Bytes".length()));
        str1 = str1.toString();
        if (hashMap1.containsKey(str1))
          continue; 
      } 
      str1 = (String)new StringBuilder();
      str1.append(str2.substring(0, 1).toLowerCase());
      str1.append(str2.substring(1));
      str1 = str1.toString();
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("get");
      stringBuilder1.append(str2);
      Method method2 = (Method)hashMap1.get(stringBuilder1.toString());
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("has");
      stringBuilder2.append(str2);
      Method method1 = (Method)hashMap1.get(stringBuilder2.toString());
      if (method2 != null) {
        boolean bool;
        Object object = GeneratedMessageLite.invokeOrDie(method2, paramMessageLite, new Object[0]);
        if (method1 == null) {
          if (!isDefaultValue(object)) {
            bool = true;
          } else {
            bool = false;
          } 
        } else {
          bool = ((Boolean)GeneratedMessageLite.invokeOrDie(method1, paramMessageLite, new Object[0])).booleanValue();
        } 
        if (bool)
          printField(paramStringBuilder, paramInt, camelCaseToSnakeCase(str1), object); 
      } 
    } 
    if (paramMessageLite instanceof GeneratedMessageLite.ExtendableMessage) {
      FieldSet<GeneratedMessageLite.ExtensionDescriptor> fieldSet = ((GeneratedMessageLite.ExtendableMessage)paramMessageLite).extensions;
      Iterator<Map.Entry<GeneratedMessageLite.ExtensionDescriptor, Object>> iterator = fieldSet.iterator();
      while (iterator.hasNext()) {
        Map.Entry entry = iterator.next();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        stringBuilder.append(((GeneratedMessageLite.ExtensionDescriptor)entry.getKey()).getNumber());
        stringBuilder.append("]");
        printField(paramStringBuilder, paramInt, stringBuilder.toString(), entry.getValue());
      } 
    } 
    if (((GeneratedMessageLite)paramMessageLite).unknownFields != null)
      ((GeneratedMessageLite)paramMessageLite).unknownFields.printWithIndent(paramStringBuilder, paramInt); 
  }
  
  private static boolean isDefaultValue(Object paramObject) {
    boolean bool = paramObject instanceof Boolean;
    boolean bool1 = true, bool2 = true, bool3 = true, bool4 = true, bool5 = true;
    if (bool)
      return ((Boolean)paramObject).booleanValue() ^ true; 
    if (paramObject instanceof Integer) {
      if (((Integer)paramObject).intValue() == 0) {
        bool3 = bool5;
      } else {
        bool3 = false;
      } 
      return bool3;
    } 
    if (paramObject instanceof Float) {
      if (((Float)paramObject).floatValue() == 0.0F) {
        bool3 = bool1;
      } else {
        bool3 = false;
      } 
      return bool3;
    } 
    if (paramObject instanceof Double) {
      if (((Double)paramObject).doubleValue() == 0.0D) {
        bool3 = bool2;
      } else {
        bool3 = false;
      } 
      return bool3;
    } 
    if (paramObject instanceof String)
      return paramObject.equals(""); 
    if (paramObject instanceof ByteString)
      return paramObject.equals(ByteString.EMPTY); 
    if (paramObject instanceof MessageLite) {
      if (paramObject != ((MessageLite)paramObject).getDefaultInstanceForType())
        bool3 = false; 
      return bool3;
    } 
    if (paramObject instanceof Enum) {
      if (((Enum)paramObject).ordinal() == 0) {
        bool3 = bool4;
      } else {
        bool3 = false;
      } 
      return bool3;
    } 
    return false;
  }
  
  static final void printField(StringBuilder paramStringBuilder, int paramInt, String paramString, Object paramObject) {
    if (paramObject instanceof List) {
      paramObject = paramObject;
      for (Object paramObject : paramObject)
        printField(paramStringBuilder, paramInt, paramString, paramObject); 
      return;
    } 
    if (paramObject instanceof Map) {
      paramObject = paramObject;
      for (Object paramObject : paramObject.entrySet())
        printField(paramStringBuilder, paramInt, paramString, paramObject); 
      return;
    } 
    paramStringBuilder.append('\n');
    byte b;
    for (b = 0; b < paramInt; b++)
      paramStringBuilder.append(' '); 
    paramStringBuilder.append(paramString);
    if (paramObject instanceof String) {
      paramStringBuilder.append(": \"");
      paramStringBuilder.append(TextFormatEscaper.escapeText((String)paramObject));
      paramStringBuilder.append('"');
    } else if (paramObject instanceof ByteString) {
      paramStringBuilder.append(": \"");
      paramStringBuilder.append(TextFormatEscaper.escapeBytes((ByteString)paramObject));
      paramStringBuilder.append('"');
    } else if (paramObject instanceof GeneratedMessageLite) {
      paramStringBuilder.append(" {");
      reflectivePrintWithIndent((GeneratedMessageLite)paramObject, paramStringBuilder, paramInt + 2);
      paramStringBuilder.append("\n");
      for (b = 0; b < paramInt; b++)
        paramStringBuilder.append(' '); 
      paramStringBuilder.append("}");
    } else if (paramObject instanceof Map.Entry) {
      paramStringBuilder.append(" {");
      Map.Entry entry = (Map.Entry)paramObject;
      printField(paramStringBuilder, paramInt + 2, "key", entry.getKey());
      printField(paramStringBuilder, paramInt + 2, "value", entry.getValue());
      paramStringBuilder.append("\n");
      for (b = 0; b < paramInt; b++)
        paramStringBuilder.append(' '); 
      paramStringBuilder.append("}");
    } else {
      paramStringBuilder.append(": ");
      paramStringBuilder.append(paramObject.toString());
    } 
  }
  
  private static final String camelCaseToSnakeCase(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    for (byte b = 0; b < paramString.length(); b++) {
      char c = paramString.charAt(b);
      if (Character.isUpperCase(c))
        stringBuilder.append("_"); 
      stringBuilder.append(Character.toLowerCase(c));
    } 
    return stringBuilder.toString();
  }
}
