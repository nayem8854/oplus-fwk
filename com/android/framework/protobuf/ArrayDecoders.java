package com.android.framework.protobuf;

import java.io.IOException;

final class ArrayDecoders {
  static final class Registers {
    public final ExtensionRegistryLite extensionRegistry;
    
    public int int1;
    
    public long long1;
    
    public Object object1;
    
    Registers() {
      this.extensionRegistry = ExtensionRegistryLite.getEmptyRegistry();
    }
    
    Registers(ExtensionRegistryLite param1ExtensionRegistryLite) {
      if (param1ExtensionRegistryLite != null) {
        this.extensionRegistry = param1ExtensionRegistryLite;
        return;
      } 
      throw null;
    }
  }
  
  static int decodeVarint32(byte[] paramArrayOfbyte, int paramInt, Registers paramRegisters) {
    int i = paramInt + 1;
    paramInt = paramArrayOfbyte[paramInt];
    if (paramInt >= 0) {
      paramRegisters.int1 = paramInt;
      return i;
    } 
    return decodeVarint32(paramInt, paramArrayOfbyte, i, paramRegisters);
  }
  
  static int decodeVarint32(int paramInt1, byte[] paramArrayOfbyte, int paramInt2, Registers paramRegisters) {
    int i = paramInt1 & 0x7F;
    paramInt1 = paramInt2 + 1;
    paramInt2 = paramArrayOfbyte[paramInt2];
    if (paramInt2 >= 0) {
      paramRegisters.int1 = paramInt2 << 7 | i;
      return paramInt1;
    } 
    paramInt2 = i | (paramInt2 & 0x7F) << 7;
    i = paramInt1 + 1;
    paramInt1 = paramArrayOfbyte[paramInt1];
    if (paramInt1 >= 0) {
      paramRegisters.int1 = paramInt1 << 14 | paramInt2;
      return i;
    } 
    paramInt1 = paramInt2 | (paramInt1 & 0x7F) << 14;
    paramInt2 = i + 1;
    i = paramArrayOfbyte[i];
    if (i >= 0) {
      paramRegisters.int1 = i << 21 | paramInt1;
      return paramInt2;
    } 
    i = paramInt1 | (i & 0x7F) << 21;
    paramInt1 = paramInt2 + 1;
    byte b = paramArrayOfbyte[paramInt2];
    if (b >= 0) {
      paramRegisters.int1 = b << 28 | i;
      return paramInt1;
    } 
    while (true) {
      paramInt2 = paramInt1 + 1;
      if (paramArrayOfbyte[paramInt1] < 0) {
        paramInt1 = paramInt2;
        continue;
      } 
      break;
    } 
    paramRegisters.int1 = i | (b & Byte.MAX_VALUE) << 28;
    return paramInt2;
  }
  
  static int decodeVarint64(byte[] paramArrayOfbyte, int paramInt, Registers paramRegisters) {
    int i = paramInt + 1;
    long l = paramArrayOfbyte[paramInt];
    if (l >= 0L) {
      paramRegisters.long1 = l;
      return i;
    } 
    return decodeVarint64(l, paramArrayOfbyte, i, paramRegisters);
  }
  
  static int decodeVarint64(long paramLong, byte[] paramArrayOfbyte, int paramInt, Registers paramRegisters) {
    int i = paramInt + 1;
    byte b = paramArrayOfbyte[paramInt];
    byte b1 = 7;
    paramLong = 0x7FL & paramLong | (b & Byte.MAX_VALUE) << 7L;
    paramInt = i;
    while (b < 0) {
      b = paramArrayOfbyte[paramInt];
      b1 += 7;
      paramLong |= (b & Byte.MAX_VALUE) << b1;
      paramInt++;
    } 
    paramRegisters.long1 = paramLong;
    return paramInt;
  }
  
  static int decodeFixed32(byte[] paramArrayOfbyte, int paramInt) {
    return paramArrayOfbyte[paramInt] & 0xFF | (paramArrayOfbyte[paramInt + 1] & 0xFF) << 8 | (paramArrayOfbyte[paramInt + 2] & 0xFF) << 16 | (paramArrayOfbyte[paramInt + 3] & 0xFF) << 24;
  }
  
  static long decodeFixed64(byte[] paramArrayOfbyte, int paramInt) {
    return paramArrayOfbyte[paramInt] & 0xFFL | (paramArrayOfbyte[paramInt + 1] & 0xFFL) << 8L | (paramArrayOfbyte[paramInt + 2] & 0xFFL) << 16L | (paramArrayOfbyte[paramInt + 3] & 0xFFL) << 24L | (paramArrayOfbyte[paramInt + 4] & 0xFFL) << 32L | (paramArrayOfbyte[paramInt + 5] & 0xFFL) << 40L | (paramArrayOfbyte[paramInt + 6] & 0xFFL) << 48L | (0xFFL & paramArrayOfbyte[paramInt + 7]) << 56L;
  }
  
  static double decodeDouble(byte[] paramArrayOfbyte, int paramInt) {
    return Double.longBitsToDouble(decodeFixed64(paramArrayOfbyte, paramInt));
  }
  
  static float decodeFloat(byte[] paramArrayOfbyte, int paramInt) {
    return Float.intBitsToFloat(decodeFixed32(paramArrayOfbyte, paramInt));
  }
  
  static int decodeString(byte[] paramArrayOfbyte, int paramInt, Registers paramRegisters) throws InvalidProtocolBufferException {
    int i = decodeVarint32(paramArrayOfbyte, paramInt, paramRegisters);
    paramInt = paramRegisters.int1;
    if (paramInt >= 0) {
      if (paramInt == 0) {
        paramRegisters.object1 = "";
        return i;
      } 
      paramRegisters.object1 = new String(paramArrayOfbyte, i, paramInt, Internal.UTF_8);
      return i + paramInt;
    } 
    throw InvalidProtocolBufferException.negativeSize();
  }
  
  static int decodeStringRequireUtf8(byte[] paramArrayOfbyte, int paramInt, Registers paramRegisters) throws InvalidProtocolBufferException {
    int i = decodeVarint32(paramArrayOfbyte, paramInt, paramRegisters);
    paramInt = paramRegisters.int1;
    if (paramInt >= 0) {
      if (paramInt == 0) {
        paramRegisters.object1 = "";
        return i;
      } 
      paramRegisters.object1 = Utf8.decodeUtf8(paramArrayOfbyte, i, paramInt);
      return i + paramInt;
    } 
    throw InvalidProtocolBufferException.negativeSize();
  }
  
  static int decodeBytes(byte[] paramArrayOfbyte, int paramInt, Registers paramRegisters) throws InvalidProtocolBufferException {
    int i = decodeVarint32(paramArrayOfbyte, paramInt, paramRegisters);
    paramInt = paramRegisters.int1;
    if (paramInt >= 0) {
      if (paramInt <= paramArrayOfbyte.length - i) {
        if (paramInt == 0) {
          paramRegisters.object1 = ByteString.EMPTY;
          return i;
        } 
        paramRegisters.object1 = ByteString.copyFrom(paramArrayOfbyte, i, paramInt);
        return i + paramInt;
      } 
      throw InvalidProtocolBufferException.truncatedMessage();
    } 
    throw InvalidProtocolBufferException.negativeSize();
  }
  
  static int decodeMessageField(Schema<Object> paramSchema, byte[] paramArrayOfbyte, int paramInt1, int paramInt2, Registers paramRegisters) throws IOException {
    int i = paramInt1 + 1;
    paramInt1 = paramArrayOfbyte[paramInt1];
    if (paramInt1 < 0) {
      i = decodeVarint32(paramInt1, paramArrayOfbyte, i, paramRegisters);
      paramInt1 = paramRegisters.int1;
    } 
    if (paramInt1 >= 0 && paramInt1 <= paramInt2 - i) {
      Object object = paramSchema.newInstance();
      paramSchema.mergeFrom(object, paramArrayOfbyte, i, i + paramInt1, paramRegisters);
      paramSchema.makeImmutable(object);
      paramRegisters.object1 = object;
      return i + paramInt1;
    } 
    throw InvalidProtocolBufferException.truncatedMessage();
  }
  
  static int decodeGroupField(Schema<Object> paramSchema, byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3, Registers paramRegisters) throws IOException {
    paramSchema = paramSchema;
    Object object = paramSchema.newInstance();
    paramInt1 = paramSchema.parseProto2Message(object, paramArrayOfbyte, paramInt1, paramInt2, paramInt3, paramRegisters);
    paramSchema.makeImmutable(object);
    paramRegisters.object1 = object;
    return paramInt1;
  }
  
  static int decodeVarint32List(int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3, Internal.ProtobufList<?> paramProtobufList, Registers paramRegisters) {
    paramProtobufList = paramProtobufList;
    paramInt2 = decodeVarint32(paramArrayOfbyte, paramInt2, paramRegisters);
    paramProtobufList.addInt(paramRegisters.int1);
    while (paramInt2 < paramInt3) {
      int i = decodeVarint32(paramArrayOfbyte, paramInt2, paramRegisters);
      if (paramInt1 != paramRegisters.int1)
        break; 
      paramInt2 = decodeVarint32(paramArrayOfbyte, i, paramRegisters);
      paramProtobufList.addInt(paramRegisters.int1);
    } 
    return paramInt2;
  }
  
  static int decodeVarint64List(int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3, Internal.ProtobufList<?> paramProtobufList, Registers paramRegisters) {
    paramProtobufList = paramProtobufList;
    paramInt2 = decodeVarint64(paramArrayOfbyte, paramInt2, paramRegisters);
    paramProtobufList.addLong(paramRegisters.long1);
    while (paramInt2 < paramInt3) {
      int i = decodeVarint32(paramArrayOfbyte, paramInt2, paramRegisters);
      if (paramInt1 != paramRegisters.int1)
        break; 
      paramInt2 = decodeVarint64(paramArrayOfbyte, i, paramRegisters);
      paramProtobufList.addLong(paramRegisters.long1);
    } 
    return paramInt2;
  }
  
  static int decodeFixed32List(int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3, Internal.ProtobufList<?> paramProtobufList, Registers paramRegisters) {
    paramProtobufList = paramProtobufList;
    paramProtobufList.addInt(decodeFixed32(paramArrayOfbyte, paramInt2));
    paramInt2 += 4;
    while (paramInt2 < paramInt3) {
      int i = decodeVarint32(paramArrayOfbyte, paramInt2, paramRegisters);
      if (paramInt1 != paramRegisters.int1)
        break; 
      paramProtobufList.addInt(decodeFixed32(paramArrayOfbyte, i));
      paramInt2 = i + 4;
    } 
    return paramInt2;
  }
  
  static int decodeFixed64List(int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3, Internal.ProtobufList<?> paramProtobufList, Registers paramRegisters) {
    paramProtobufList = paramProtobufList;
    paramProtobufList.addLong(decodeFixed64(paramArrayOfbyte, paramInt2));
    paramInt2 += 8;
    while (paramInt2 < paramInt3) {
      int i = decodeVarint32(paramArrayOfbyte, paramInt2, paramRegisters);
      if (paramInt1 != paramRegisters.int1)
        break; 
      paramProtobufList.addLong(decodeFixed64(paramArrayOfbyte, i));
      paramInt2 = i + 8;
    } 
    return paramInt2;
  }
  
  static int decodeFloatList(int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3, Internal.ProtobufList<?> paramProtobufList, Registers paramRegisters) {
    paramProtobufList = paramProtobufList;
    paramProtobufList.addFloat(decodeFloat(paramArrayOfbyte, paramInt2));
    paramInt2 += 4;
    while (paramInt2 < paramInt3) {
      int i = decodeVarint32(paramArrayOfbyte, paramInt2, paramRegisters);
      if (paramInt1 != paramRegisters.int1)
        break; 
      paramProtobufList.addFloat(decodeFloat(paramArrayOfbyte, i));
      paramInt2 = i + 4;
    } 
    return paramInt2;
  }
  
  static int decodeDoubleList(int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3, Internal.ProtobufList<?> paramProtobufList, Registers paramRegisters) {
    paramProtobufList = paramProtobufList;
    paramProtobufList.addDouble(decodeDouble(paramArrayOfbyte, paramInt2));
    paramInt2 += 8;
    while (paramInt2 < paramInt3) {
      int i = decodeVarint32(paramArrayOfbyte, paramInt2, paramRegisters);
      if (paramInt1 != paramRegisters.int1)
        break; 
      paramProtobufList.addDouble(decodeDouble(paramArrayOfbyte, i));
      paramInt2 = i + 8;
    } 
    return paramInt2;
  }
  
  static int decodeBoolList(int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3, Internal.ProtobufList<?> paramProtobufList, Registers paramRegisters) {
    boolean bool;
    paramProtobufList = paramProtobufList;
    paramInt2 = decodeVarint64(paramArrayOfbyte, paramInt2, paramRegisters);
    if (paramRegisters.long1 != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    paramProtobufList.addBoolean(bool);
    while (paramInt2 < paramInt3) {
      int i = decodeVarint32(paramArrayOfbyte, paramInt2, paramRegisters);
      if (paramInt1 != paramRegisters.int1)
        break; 
      paramInt2 = decodeVarint64(paramArrayOfbyte, i, paramRegisters);
      if (paramRegisters.long1 != 0L) {
        bool = true;
      } else {
        bool = false;
      } 
      paramProtobufList.addBoolean(bool);
    } 
    return paramInt2;
  }
  
  static int decodeSInt32List(int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3, Internal.ProtobufList<?> paramProtobufList, Registers paramRegisters) {
    paramProtobufList = paramProtobufList;
    paramInt2 = decodeVarint32(paramArrayOfbyte, paramInt2, paramRegisters);
    paramProtobufList.addInt(CodedInputStream.decodeZigZag32(paramRegisters.int1));
    while (paramInt2 < paramInt3) {
      int i = decodeVarint32(paramArrayOfbyte, paramInt2, paramRegisters);
      if (paramInt1 != paramRegisters.int1)
        break; 
      paramInt2 = decodeVarint32(paramArrayOfbyte, i, paramRegisters);
      paramProtobufList.addInt(CodedInputStream.decodeZigZag32(paramRegisters.int1));
    } 
    return paramInt2;
  }
  
  static int decodeSInt64List(int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3, Internal.ProtobufList<?> paramProtobufList, Registers paramRegisters) {
    paramProtobufList = paramProtobufList;
    paramInt2 = decodeVarint64(paramArrayOfbyte, paramInt2, paramRegisters);
    paramProtobufList.addLong(CodedInputStream.decodeZigZag64(paramRegisters.long1));
    while (paramInt2 < paramInt3) {
      int i = decodeVarint32(paramArrayOfbyte, paramInt2, paramRegisters);
      if (paramInt1 != paramRegisters.int1)
        break; 
      paramInt2 = decodeVarint64(paramArrayOfbyte, i, paramRegisters);
      paramProtobufList.addLong(CodedInputStream.decodeZigZag64(paramRegisters.long1));
    } 
    return paramInt2;
  }
  
  static int decodePackedVarint32List(byte[] paramArrayOfbyte, int paramInt, Internal.ProtobufList<?> paramProtobufList, Registers paramRegisters) throws IOException {
    paramProtobufList = paramProtobufList;
    paramInt = decodeVarint32(paramArrayOfbyte, paramInt, paramRegisters);
    int i = paramRegisters.int1 + paramInt;
    while (paramInt < i) {
      paramInt = decodeVarint32(paramArrayOfbyte, paramInt, paramRegisters);
      paramProtobufList.addInt(paramRegisters.int1);
    } 
    if (paramInt == i)
      return paramInt; 
    throw InvalidProtocolBufferException.truncatedMessage();
  }
  
  static int decodePackedVarint64List(byte[] paramArrayOfbyte, int paramInt, Internal.ProtobufList<?> paramProtobufList, Registers paramRegisters) throws IOException {
    paramProtobufList = paramProtobufList;
    paramInt = decodeVarint32(paramArrayOfbyte, paramInt, paramRegisters);
    int i = paramRegisters.int1 + paramInt;
    while (paramInt < i) {
      paramInt = decodeVarint64(paramArrayOfbyte, paramInt, paramRegisters);
      paramProtobufList.addLong(paramRegisters.long1);
    } 
    if (paramInt == i)
      return paramInt; 
    throw InvalidProtocolBufferException.truncatedMessage();
  }
  
  static int decodePackedFixed32List(byte[] paramArrayOfbyte, int paramInt, Internal.ProtobufList<?> paramProtobufList, Registers paramRegisters) throws IOException {
    paramProtobufList = paramProtobufList;
    paramInt = decodeVarint32(paramArrayOfbyte, paramInt, paramRegisters);
    int i = paramRegisters.int1 + paramInt;
    while (paramInt < i) {
      paramProtobufList.addInt(decodeFixed32(paramArrayOfbyte, paramInt));
      paramInt += 4;
    } 
    if (paramInt == i)
      return paramInt; 
    throw InvalidProtocolBufferException.truncatedMessage();
  }
  
  static int decodePackedFixed64List(byte[] paramArrayOfbyte, int paramInt, Internal.ProtobufList<?> paramProtobufList, Registers paramRegisters) throws IOException {
    paramProtobufList = paramProtobufList;
    paramInt = decodeVarint32(paramArrayOfbyte, paramInt, paramRegisters);
    int i = paramRegisters.int1 + paramInt;
    while (paramInt < i) {
      paramProtobufList.addLong(decodeFixed64(paramArrayOfbyte, paramInt));
      paramInt += 8;
    } 
    if (paramInt == i)
      return paramInt; 
    throw InvalidProtocolBufferException.truncatedMessage();
  }
  
  static int decodePackedFloatList(byte[] paramArrayOfbyte, int paramInt, Internal.ProtobufList<?> paramProtobufList, Registers paramRegisters) throws IOException {
    paramProtobufList = paramProtobufList;
    paramInt = decodeVarint32(paramArrayOfbyte, paramInt, paramRegisters);
    int i = paramRegisters.int1 + paramInt;
    while (paramInt < i) {
      paramProtobufList.addFloat(decodeFloat(paramArrayOfbyte, paramInt));
      paramInt += 4;
    } 
    if (paramInt == i)
      return paramInt; 
    throw InvalidProtocolBufferException.truncatedMessage();
  }
  
  static int decodePackedDoubleList(byte[] paramArrayOfbyte, int paramInt, Internal.ProtobufList<?> paramProtobufList, Registers paramRegisters) throws IOException {
    paramProtobufList = paramProtobufList;
    paramInt = decodeVarint32(paramArrayOfbyte, paramInt, paramRegisters);
    int i = paramRegisters.int1 + paramInt;
    while (paramInt < i) {
      paramProtobufList.addDouble(decodeDouble(paramArrayOfbyte, paramInt));
      paramInt += 8;
    } 
    if (paramInt == i)
      return paramInt; 
    throw InvalidProtocolBufferException.truncatedMessage();
  }
  
  static int decodePackedBoolList(byte[] paramArrayOfbyte, int paramInt, Internal.ProtobufList<?> paramProtobufList, Registers paramRegisters) throws IOException {
    paramProtobufList = paramProtobufList;
    paramInt = decodeVarint32(paramArrayOfbyte, paramInt, paramRegisters);
    int i = paramRegisters.int1 + paramInt;
    while (paramInt < i) {
      boolean bool;
      paramInt = decodeVarint64(paramArrayOfbyte, paramInt, paramRegisters);
      if (paramRegisters.long1 != 0L) {
        bool = true;
      } else {
        bool = false;
      } 
      paramProtobufList.addBoolean(bool);
    } 
    if (paramInt == i)
      return paramInt; 
    throw InvalidProtocolBufferException.truncatedMessage();
  }
  
  static int decodePackedSInt32List(byte[] paramArrayOfbyte, int paramInt, Internal.ProtobufList<?> paramProtobufList, Registers paramRegisters) throws IOException {
    paramProtobufList = paramProtobufList;
    paramInt = decodeVarint32(paramArrayOfbyte, paramInt, paramRegisters);
    int i = paramRegisters.int1 + paramInt;
    while (paramInt < i) {
      paramInt = decodeVarint32(paramArrayOfbyte, paramInt, paramRegisters);
      paramProtobufList.addInt(CodedInputStream.decodeZigZag32(paramRegisters.int1));
    } 
    if (paramInt == i)
      return paramInt; 
    throw InvalidProtocolBufferException.truncatedMessage();
  }
  
  static int decodePackedSInt64List(byte[] paramArrayOfbyte, int paramInt, Internal.ProtobufList<?> paramProtobufList, Registers paramRegisters) throws IOException {
    paramProtobufList = paramProtobufList;
    paramInt = decodeVarint32(paramArrayOfbyte, paramInt, paramRegisters);
    int i = paramRegisters.int1 + paramInt;
    while (paramInt < i) {
      paramInt = decodeVarint64(paramArrayOfbyte, paramInt, paramRegisters);
      paramProtobufList.addLong(CodedInputStream.decodeZigZag64(paramRegisters.long1));
    } 
    if (paramInt == i)
      return paramInt; 
    throw InvalidProtocolBufferException.truncatedMessage();
  }
  
  static int decodeStringList(int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3, Internal.ProtobufList<?> paramProtobufList, Registers paramRegisters) throws InvalidProtocolBufferException {
    paramInt2 = decodeVarint32(paramArrayOfbyte, paramInt2, paramRegisters);
    int i = paramRegisters.int1;
    if (i >= 0) {
      if (i == 0) {
        paramProtobufList.add("");
      } else {
        String str = new String(paramArrayOfbyte, paramInt2, i, Internal.UTF_8);
        paramProtobufList.add(str);
        paramInt2 += i;
      } 
      while (paramInt2 < paramInt3) {
        i = decodeVarint32(paramArrayOfbyte, paramInt2, paramRegisters);
        if (paramInt1 != paramRegisters.int1)
          break; 
        paramInt2 = decodeVarint32(paramArrayOfbyte, i, paramRegisters);
        i = paramRegisters.int1;
        if (i >= 0) {
          if (i == 0) {
            paramProtobufList.add("");
            continue;
          } 
          String str = new String(paramArrayOfbyte, paramInt2, i, Internal.UTF_8);
          paramProtobufList.add(str);
          paramInt2 += i;
          continue;
        } 
        throw InvalidProtocolBufferException.negativeSize();
      } 
      return paramInt2;
    } 
    throw InvalidProtocolBufferException.negativeSize();
  }
  
  static int decodeStringListRequireUtf8(int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3, Internal.ProtobufList<?> paramProtobufList, Registers paramRegisters) throws InvalidProtocolBufferException {
    paramInt2 = decodeVarint32(paramArrayOfbyte, paramInt2, paramRegisters);
    int i = paramRegisters.int1;
    if (i >= 0) {
      if (i == 0) {
        paramProtobufList.add("");
      } else if (Utf8.isValidUtf8(paramArrayOfbyte, paramInt2, paramInt2 + i)) {
        String str = new String(paramArrayOfbyte, paramInt2, i, Internal.UTF_8);
        paramProtobufList.add(str);
        paramInt2 += i;
      } else {
        throw InvalidProtocolBufferException.invalidUtf8();
      } 
      while (paramInt2 < paramInt3) {
        i = decodeVarint32(paramArrayOfbyte, paramInt2, paramRegisters);
        if (paramInt1 != paramRegisters.int1)
          break; 
        paramInt2 = decodeVarint32(paramArrayOfbyte, i, paramRegisters);
        i = paramRegisters.int1;
        if (i >= 0) {
          if (i == 0) {
            paramProtobufList.add("");
            continue;
          } 
          if (Utf8.isValidUtf8(paramArrayOfbyte, paramInt2, paramInt2 + i)) {
            String str = new String(paramArrayOfbyte, paramInt2, i, Internal.UTF_8);
            paramProtobufList.add(str);
            paramInt2 += i;
            continue;
          } 
          throw InvalidProtocolBufferException.invalidUtf8();
        } 
        throw InvalidProtocolBufferException.negativeSize();
      } 
      return paramInt2;
    } 
    throw InvalidProtocolBufferException.negativeSize();
  }
  
  static int decodeBytesList(int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3, Internal.ProtobufList<?> paramProtobufList, Registers paramRegisters) throws InvalidProtocolBufferException {
    paramInt2 = decodeVarint32(paramArrayOfbyte, paramInt2, paramRegisters);
    int i = paramRegisters.int1;
    if (i >= 0) {
      if (i <= paramArrayOfbyte.length - paramInt2) {
        if (i == 0) {
          paramProtobufList.add(ByteString.EMPTY);
        } else {
          paramProtobufList.add(ByteString.copyFrom(paramArrayOfbyte, paramInt2, i));
          paramInt2 += i;
        } 
        while (paramInt2 < paramInt3) {
          i = decodeVarint32(paramArrayOfbyte, paramInt2, paramRegisters);
          if (paramInt1 != paramRegisters.int1)
            break; 
          paramInt2 = decodeVarint32(paramArrayOfbyte, i, paramRegisters);
          i = paramRegisters.int1;
          if (i >= 0) {
            if (i <= paramArrayOfbyte.length - paramInt2) {
              if (i == 0) {
                paramProtobufList.add(ByteString.EMPTY);
                continue;
              } 
              paramProtobufList.add(ByteString.copyFrom(paramArrayOfbyte, paramInt2, i));
              paramInt2 += i;
              continue;
            } 
            throw InvalidProtocolBufferException.truncatedMessage();
          } 
          throw InvalidProtocolBufferException.negativeSize();
        } 
        return paramInt2;
      } 
      throw InvalidProtocolBufferException.truncatedMessage();
    } 
    throw InvalidProtocolBufferException.negativeSize();
  }
  
  static int decodeMessageList(Schema<?> paramSchema, int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3, Internal.ProtobufList<?> paramProtobufList, Registers paramRegisters) throws IOException {
    paramInt2 = decodeMessageField(paramSchema, paramArrayOfbyte, paramInt2, paramInt3, paramRegisters);
    paramProtobufList.add(paramRegisters.object1);
    while (paramInt2 < paramInt3) {
      int i = decodeVarint32(paramArrayOfbyte, paramInt2, paramRegisters);
      if (paramInt1 != paramRegisters.int1)
        break; 
      paramInt2 = decodeMessageField(paramSchema, paramArrayOfbyte, i, paramInt3, paramRegisters);
      paramProtobufList.add(paramRegisters.object1);
    } 
    return paramInt2;
  }
  
  static int decodeGroupList(Schema paramSchema, int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3, Internal.ProtobufList<?> paramProtobufList, Registers paramRegisters) throws IOException {
    int i = paramInt1 & 0xFFFFFFF8 | 0x4;
    paramInt2 = decodeGroupField(paramSchema, paramArrayOfbyte, paramInt2, paramInt3, i, paramRegisters);
    paramProtobufList.add(paramRegisters.object1);
    while (paramInt2 < paramInt3) {
      int j = decodeVarint32(paramArrayOfbyte, paramInt2, paramRegisters);
      if (paramInt1 != paramRegisters.int1)
        break; 
      paramInt2 = decodeGroupField(paramSchema, paramArrayOfbyte, j, paramInt3, i, paramRegisters);
      paramProtobufList.add(paramRegisters.object1);
    } 
    return paramInt2;
  }
  
  static int decodeExtensionOrUnknownField(int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3, Object paramObject, MessageLite paramMessageLite, UnknownFieldSchema<UnknownFieldSetLite, UnknownFieldSetLite> paramUnknownFieldSchema, Registers paramRegisters) throws IOException {
    ExtensionRegistryLite extensionRegistryLite = paramRegisters.extensionRegistry;
    GeneratedMessageLite.GeneratedExtension<MessageLite, ?> generatedExtension = extensionRegistryLite.findLiteExtensionByNumber(paramMessageLite, paramInt1 >>> 3);
    if (generatedExtension == null) {
      paramObject = MessageSchema.getMutableUnknownFields(paramObject);
      return decodeUnknownField(paramInt1, paramArrayOfbyte, paramInt2, paramInt3, (UnknownFieldSetLite)paramObject, paramRegisters);
    } 
    ((GeneratedMessageLite.ExtendableMessage)paramObject).ensureExtensionsAreMutable();
    return decodeExtension(paramInt1, paramArrayOfbyte, paramInt2, paramInt3, (GeneratedMessageLite.ExtendableMessage<?, ?>)paramObject, generatedExtension, paramUnknownFieldSchema, paramRegisters);
  }
  
  static int decodeExtension(int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3, GeneratedMessageLite.ExtendableMessage<?, ?> paramExtendableMessage, GeneratedMessageLite.GeneratedExtension<?, ?> paramGeneratedExtension, UnknownFieldSchema<UnknownFieldSetLite, UnknownFieldSetLite> paramUnknownFieldSchema, Registers paramRegisters) throws IOException {
    UnknownFieldSetLite unknownFieldSetLite;
    DoubleArrayList doubleArrayList;
    Internal.EnumLiteMap<?> enumLiteMap;
    FieldSet<GeneratedMessageLite.ExtensionDescriptor> fieldSet = paramExtendableMessage.extensions;
    int i = paramInt1 >>> 3;
    if (paramGeneratedExtension.descriptor.isRepeated() && paramGeneratedExtension.descriptor.isPacked()) {
      StringBuilder stringBuilder;
      GeneratedMessageLite.ExtensionDescriptor extensionDescriptor1;
      LongArrayList longArrayList3;
      IntArrayList intArrayList3;
      BooleanArrayList booleanArrayList;
      IntArrayList intArrayList2;
      LongArrayList longArrayList2;
      IntArrayList intArrayList1;
      LongArrayList longArrayList1;
      FloatArrayList floatArrayList;
      UnknownFieldSetLite unknownFieldSetLite1;
      GeneratedMessageLite.ExtensionDescriptor extensionDescriptor2;
      IntArrayList intArrayList4;
      switch (paramGeneratedExtension.getLiteType()) {
        default:
          stringBuilder = new StringBuilder();
          stringBuilder.append("Type cannot be packed: ");
          extensionDescriptor1 = paramGeneratedExtension.descriptor;
          stringBuilder.append(extensionDescriptor1.getLiteType());
          throw new IllegalStateException(stringBuilder.toString());
        case ENUM:
          intArrayList4 = new IntArrayList();
          paramInt1 = decodePackedVarint32List((byte[])stringBuilder, paramInt2, intArrayList4, paramRegisters);
          unknownFieldSetLite1 = ((GeneratedMessageLite.ExtendableMessage)extensionDescriptor1).unknownFields;
          unknownFieldSetLite = unknownFieldSetLite1;
          if (unknownFieldSetLite1 == UnknownFieldSetLite.getDefaultInstance())
            unknownFieldSetLite = null; 
          extensionDescriptor2 = paramGeneratedExtension.descriptor;
          enumLiteMap = extensionDescriptor2.getEnumType();
          unknownFieldSetLite = SchemaUtil.<UnknownFieldSetLite, UnknownFieldSetLite>filterUnknownEnumList(i, intArrayList4, enumLiteMap, unknownFieldSetLite, paramUnknownFieldSchema);
          if (unknownFieldSetLite != null)
            ((GeneratedMessageLite.ExtendableMessage)extensionDescriptor1).unknownFields = unknownFieldSetLite; 
          fieldSet.setField(paramGeneratedExtension.descriptor, intArrayList4);
          return paramInt1;
        case SINT64:
          longArrayList3 = new LongArrayList();
          paramInt1 = decodePackedSInt64List((byte[])unknownFieldSetLite, paramInt2, longArrayList3, (Registers)enumLiteMap);
          fieldSet.setField(paramGeneratedExtension.descriptor, longArrayList3);
          return paramInt1;
        case SINT32:
          intArrayList3 = new IntArrayList();
          paramInt1 = decodePackedSInt32List((byte[])unknownFieldSetLite, paramInt2, intArrayList3, (Registers)enumLiteMap);
          fieldSet.setField(paramGeneratedExtension.descriptor, intArrayList3);
          return paramInt1;
        case BOOL:
          booleanArrayList = new BooleanArrayList();
          paramInt1 = decodePackedBoolList((byte[])unknownFieldSetLite, paramInt2, booleanArrayList, (Registers)enumLiteMap);
          fieldSet.setField(paramGeneratedExtension.descriptor, booleanArrayList);
          return paramInt1;
        case FIXED32:
        case SFIXED32:
          intArrayList2 = new IntArrayList();
          paramInt1 = decodePackedFixed32List((byte[])unknownFieldSetLite, paramInt2, intArrayList2, (Registers)enumLiteMap);
          fieldSet.setField(paramGeneratedExtension.descriptor, intArrayList2);
          return paramInt1;
        case FIXED64:
        case SFIXED64:
          longArrayList2 = new LongArrayList();
          paramInt1 = decodePackedFixed64List((byte[])unknownFieldSetLite, paramInt2, longArrayList2, (Registers)enumLiteMap);
          fieldSet.setField(paramGeneratedExtension.descriptor, longArrayList2);
          return paramInt1;
        case INT32:
        case UINT32:
          intArrayList1 = new IntArrayList();
          paramInt1 = decodePackedVarint32List((byte[])unknownFieldSetLite, paramInt2, intArrayList1, (Registers)enumLiteMap);
          fieldSet.setField(paramGeneratedExtension.descriptor, intArrayList1);
          return paramInt1;
        case INT64:
        case UINT64:
          longArrayList1 = new LongArrayList();
          paramInt1 = decodePackedVarint64List((byte[])unknownFieldSetLite, paramInt2, longArrayList1, (Registers)enumLiteMap);
          fieldSet.setField(paramGeneratedExtension.descriptor, longArrayList1);
          return paramInt1;
        case FLOAT:
          floatArrayList = new FloatArrayList();
          paramInt1 = decodePackedFloatList((byte[])unknownFieldSetLite, paramInt2, floatArrayList, (Registers)enumLiteMap);
          fieldSet.setField(paramGeneratedExtension.descriptor, floatArrayList);
          return paramInt1;
        case DOUBLE:
          break;
      } 
      doubleArrayList = new DoubleArrayList();
      paramInt1 = decodePackedDoubleList((byte[])unknownFieldSetLite, paramInt2, doubleArrayList, (Registers)enumLiteMap);
      fieldSet.setField(paramGeneratedExtension.descriptor, doubleArrayList);
    } else {
      Object object;
      UnknownFieldSetLite unknownFieldSetLite1, unknownFieldSetLite2 = null;
      if (paramGeneratedExtension.getLiteType() == WireFormat.FieldType.ENUM) {
        paramInt1 = decodeVarint32((byte[])unknownFieldSetLite, paramInt2, (Registers)enumLiteMap);
        unknownFieldSetLite = (UnknownFieldSetLite)paramGeneratedExtension.descriptor.getEnumType().findValueByNumber(((Registers)enumLiteMap).int1);
        if (unknownFieldSetLite == null) {
          unknownFieldSetLite1 = ((GeneratedMessageLite)doubleArrayList).unknownFields;
          unknownFieldSetLite = unknownFieldSetLite1;
          if (unknownFieldSetLite1 == UnknownFieldSetLite.getDefaultInstance()) {
            unknownFieldSetLite = UnknownFieldSetLite.newInstance();
            ((GeneratedMessageLite)doubleArrayList).unknownFields = unknownFieldSetLite;
          } 
          SchemaUtil.storeUnknownEnum(i, ((Registers)enumLiteMap).int1, unknownFieldSetLite, paramUnknownFieldSchema);
          return paramInt1;
        } 
        object = Integer.valueOf(((Registers)enumLiteMap).int1);
      } else {
        Schema<?> schema;
        boolean bool;
        switch (unknownFieldSetLite1.getLiteType()) {
          default:
            paramInt1 = paramInt2;
            unknownFieldSetLite = unknownFieldSetLite2;
            break;
          case MESSAGE:
            schema = Protobuf.getInstance().schemaFor(unknownFieldSetLite1.getMessageDefaultInstance().getClass());
            paramInt1 = decodeMessageField(schema, (byte[])unknownFieldSetLite, paramInt2, paramInt3, (Registers)enumLiteMap);
            object = ((Registers)enumLiteMap).object1;
            break;
          case GROUP:
            schema = Protobuf.getInstance().schemaFor(unknownFieldSetLite1.getMessageDefaultInstance().getClass());
            paramInt1 = decodeGroupField(schema, (byte[])object, paramInt2, paramInt3, i << 3 | 0x4, (Registers)enumLiteMap);
            object = ((Registers)enumLiteMap).object1;
            break;
          case STRING:
            paramInt1 = decodeString((byte[])object, paramInt2, (Registers)enumLiteMap);
            object = ((Registers)enumLiteMap).object1;
            break;
          case BYTES:
            paramInt1 = decodeBytes((byte[])object, paramInt2, (Registers)enumLiteMap);
            object = ((Registers)enumLiteMap).object1;
            break;
          case ENUM:
            throw new IllegalStateException("Shouldn't reach here.");
          case SINT64:
            paramInt1 = decodeVarint64((byte[])object, paramInt2, (Registers)enumLiteMap);
            object = Long.valueOf(CodedInputStream.decodeZigZag64(((Registers)enumLiteMap).long1));
            break;
          case SINT32:
            paramInt1 = decodeVarint32((byte[])object, paramInt2, (Registers)enumLiteMap);
            object = Integer.valueOf(CodedInputStream.decodeZigZag32(((Registers)enumLiteMap).int1));
            break;
          case BOOL:
            paramInt1 = decodeVarint64((byte[])object, paramInt2, (Registers)enumLiteMap);
            if (((Registers)enumLiteMap).long1 != 0L) {
              bool = true;
            } else {
              bool = false;
            } 
            object = Boolean.valueOf(bool);
            break;
          case FIXED32:
          case SFIXED32:
            object = Integer.valueOf(decodeFixed32((byte[])object, paramInt2));
            paramInt1 = paramInt2 + 4;
            break;
          case FIXED64:
          case SFIXED64:
            object = Long.valueOf(decodeFixed64((byte[])object, paramInt2));
            paramInt1 = paramInt2 + 8;
            break;
          case INT32:
          case UINT32:
            paramInt1 = decodeVarint32((byte[])object, paramInt2, (Registers)enumLiteMap);
            object = Integer.valueOf(((Registers)enumLiteMap).int1);
            break;
          case INT64:
          case UINT64:
            paramInt1 = decodeVarint64((byte[])object, paramInt2, (Registers)enumLiteMap);
            object = Long.valueOf(((Registers)enumLiteMap).long1);
            break;
          case FLOAT:
            object = Float.valueOf(decodeFloat((byte[])object, paramInt2));
            paramInt1 = paramInt2 + 4;
            break;
          case DOUBLE:
            object = Double.valueOf(decodeDouble((byte[])object, paramInt2));
            paramInt1 = paramInt2 + 8;
            break;
        } 
      } 
      if (unknownFieldSetLite1.isRepeated()) {
        fieldSet.addRepeatedField(((GeneratedMessageLite.GeneratedExtension)unknownFieldSetLite1).descriptor, object);
      } else {
        Object object1;
        paramInt2 = null.$SwitchMap$com$google$protobuf$WireFormat$FieldType[unknownFieldSetLite1.getLiteType().ordinal()];
        if (paramInt2 != 17 && paramInt2 != 18) {
          object1 = object;
        } else {
          Object object2 = fieldSet.getField(((GeneratedMessageLite.GeneratedExtension)unknownFieldSetLite1).descriptor);
          object1 = object;
          if (object2 != null)
            object1 = Internal.mergeMessage(object2, object); 
        } 
        fieldSet.setField(((GeneratedMessageLite.GeneratedExtension)unknownFieldSetLite1).descriptor, object1);
      } 
    } 
    return paramInt1;
  }
  
  static int decodeUnknownField(int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3, UnknownFieldSetLite paramUnknownFieldSetLite, Registers paramRegisters) throws InvalidProtocolBufferException {
    if (WireFormat.getTagFieldNumber(paramInt1) != 0) {
      int i = WireFormat.getTagWireType(paramInt1);
      if (i != 0) {
        if (i != 1) {
          if (i != 2) {
            int k;
            if (i != 3) {
              if (i == 5) {
                paramUnknownFieldSetLite.storeField(paramInt1, Integer.valueOf(decodeFixed32(paramArrayOfbyte, paramInt2)));
                return paramInt2 + 4;
              } 
              throw InvalidProtocolBufferException.invalidTag();
            } 
            UnknownFieldSetLite unknownFieldSetLite = UnknownFieldSetLite.newInstance();
            int j = paramInt1 & 0xFFFFFFF8 | 0x4;
            i = 0;
            while (true) {
              k = paramInt2;
              if (paramInt2 < paramInt3) {
                k = decodeVarint32(paramArrayOfbyte, paramInt2, paramRegisters);
                i = paramRegisters.int1;
                if (i == j)
                  break; 
                paramInt2 = decodeUnknownField(i, paramArrayOfbyte, k, paramInt3, unknownFieldSetLite, paramRegisters);
                continue;
              } 
              break;
            } 
            if (k <= paramInt3 && i == j) {
              paramUnknownFieldSetLite.storeField(paramInt1, unknownFieldSetLite);
              return k;
            } 
            throw InvalidProtocolBufferException.parseFailure();
          } 
          paramInt3 = decodeVarint32(paramArrayOfbyte, paramInt2, paramRegisters);
          paramInt2 = paramRegisters.int1;
          if (paramInt2 >= 0) {
            if (paramInt2 <= paramArrayOfbyte.length - paramInt3) {
              if (paramInt2 == 0) {
                paramUnknownFieldSetLite.storeField(paramInt1, ByteString.EMPTY);
              } else {
                paramUnknownFieldSetLite.storeField(paramInt1, ByteString.copyFrom(paramArrayOfbyte, paramInt3, paramInt2));
              } 
              return paramInt3 + paramInt2;
            } 
            throw InvalidProtocolBufferException.truncatedMessage();
          } 
          throw InvalidProtocolBufferException.negativeSize();
        } 
        paramUnknownFieldSetLite.storeField(paramInt1, Long.valueOf(decodeFixed64(paramArrayOfbyte, paramInt2)));
        return paramInt2 + 8;
      } 
      paramInt2 = decodeVarint64(paramArrayOfbyte, paramInt2, paramRegisters);
      paramUnknownFieldSetLite.storeField(paramInt1, Long.valueOf(paramRegisters.long1));
      return paramInt2;
    } 
    throw InvalidProtocolBufferException.invalidTag();
  }
  
  static int skipField(int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3, Registers paramRegisters) throws InvalidProtocolBufferException {
    if (WireFormat.getTagFieldNumber(paramInt1) != 0) {
      int i = WireFormat.getTagWireType(paramInt1);
      if (i != 0) {
        if (i != 1) {
          if (i != 2) {
            if (i != 3) {
              if (i == 5)
                return paramInt2 + 4; 
              throw InvalidProtocolBufferException.invalidTag();
            } 
            int j = paramInt1 & 0xFFFFFFF8 | 0x4;
            paramInt1 = 0;
            while (true) {
              i = paramInt2;
              if (paramInt2 < paramInt3) {
                i = decodeVarint32(paramArrayOfbyte, paramInt2, paramRegisters);
                paramInt1 = paramRegisters.int1;
                if (paramInt1 == j)
                  break; 
                paramInt2 = skipField(paramInt1, paramArrayOfbyte, i, paramInt3, paramRegisters);
                continue;
              } 
              break;
            } 
            if (i <= paramInt3 && paramInt1 == j)
              return i; 
            throw InvalidProtocolBufferException.parseFailure();
          } 
          paramInt1 = decodeVarint32(paramArrayOfbyte, paramInt2, paramRegisters);
          return paramRegisters.int1 + paramInt1;
        } 
        return paramInt2 + 8;
      } 
      paramInt1 = decodeVarint64(paramArrayOfbyte, paramInt2, paramRegisters);
      return paramInt1;
    } 
    throw InvalidProtocolBufferException.invalidTag();
  }
}
