package com.android.framework.protobuf;

final class RawMessageInfo implements MessageInfo {
  private final MessageLite defaultInstance;
  
  private final int flags;
  
  private final String info;
  
  private final Object[] objects;
  
  RawMessageInfo(MessageLite paramMessageLite, String paramString, Object[] paramArrayOfObject) {
    this.defaultInstance = paramMessageLite;
    this.info = paramString;
    this.objects = paramArrayOfObject;
    int i = 0 + 1;
    char c = paramString.charAt(0);
    if (c < '?') {
      this.flags = c;
    } else {
      char c1;
      int j = c & 0x1FFF;
      c = '\r';
      while (true) {
        c1 = paramString.charAt(i);
        if (c1 >= '?') {
          j |= (c1 & 0x1FFF) << c;
          c += '\r';
          i++;
          continue;
        } 
        break;
      } 
      this.flags = c1 << c | j;
    } 
  }
  
  String getStringInfo() {
    return this.info;
  }
  
  Object[] getObjects() {
    return this.objects;
  }
  
  public MessageLite getDefaultInstance() {
    return this.defaultInstance;
  }
  
  public ProtoSyntax getSyntax() {
    ProtoSyntax protoSyntax;
    if ((this.flags & 0x1) == 1) {
      protoSyntax = ProtoSyntax.PROTO2;
    } else {
      protoSyntax = ProtoSyntax.PROTO3;
    } 
    return protoSyntax;
  }
  
  public boolean isMessageSetWireFormat() {
    boolean bool;
    if ((this.flags & 0x2) == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
}
