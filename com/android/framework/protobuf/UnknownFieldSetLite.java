package com.android.framework.protobuf;

import java.io.IOException;
import java.util.Arrays;

public final class UnknownFieldSetLite {
  private static final UnknownFieldSetLite DEFAULT_INSTANCE = new UnknownFieldSetLite(0, new int[0], new Object[0], false);
  
  private static final int MIN_CAPACITY = 8;
  
  private int count;
  
  private boolean isMutable;
  
  public static UnknownFieldSetLite getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }
  
  static UnknownFieldSetLite newInstance() {
    return new UnknownFieldSetLite();
  }
  
  static UnknownFieldSetLite mutableCopyOf(UnknownFieldSetLite paramUnknownFieldSetLite1, UnknownFieldSetLite paramUnknownFieldSetLite2) {
    int i = paramUnknownFieldSetLite1.count + paramUnknownFieldSetLite2.count;
    int[] arrayOfInt = Arrays.copyOf(paramUnknownFieldSetLite1.tags, i);
    System.arraycopy(paramUnknownFieldSetLite2.tags, 0, arrayOfInt, paramUnknownFieldSetLite1.count, paramUnknownFieldSetLite2.count);
    Object[] arrayOfObject = Arrays.copyOf(paramUnknownFieldSetLite1.objects, i);
    System.arraycopy(paramUnknownFieldSetLite2.objects, 0, arrayOfObject, paramUnknownFieldSetLite1.count, paramUnknownFieldSetLite2.count);
    return new UnknownFieldSetLite(i, arrayOfInt, arrayOfObject, true);
  }
  
  private int memoizedSerializedSize = -1;
  
  private Object[] objects;
  
  private int[] tags;
  
  private UnknownFieldSetLite() {
    this(0, new int[8], new Object[8], true);
  }
  
  private UnknownFieldSetLite(int paramInt, int[] paramArrayOfint, Object[] paramArrayOfObject, boolean paramBoolean) {
    this.count = paramInt;
    this.tags = paramArrayOfint;
    this.objects = paramArrayOfObject;
    this.isMutable = paramBoolean;
  }
  
  public void makeImmutable() {
    this.isMutable = false;
  }
  
  void checkMutable() {
    if (this.isMutable)
      return; 
    throw new UnsupportedOperationException();
  }
  
  public void writeTo(CodedOutputStream paramCodedOutputStream) throws IOException {
    for (byte b = 0; b < this.count; b++) {
      int i = this.tags[b];
      int j = WireFormat.getTagFieldNumber(i);
      i = WireFormat.getTagWireType(i);
      if (i != 0) {
        if (i != 1) {
          if (i != 2) {
            if (i != 3) {
              if (i == 5) {
                paramCodedOutputStream.writeFixed32(j, ((Integer)this.objects[b]).intValue());
              } else {
                throw InvalidProtocolBufferException.invalidWireType();
              } 
            } else {
              paramCodedOutputStream.writeTag(j, 3);
              ((UnknownFieldSetLite)this.objects[b]).writeTo(paramCodedOutputStream);
              paramCodedOutputStream.writeTag(j, 4);
            } 
          } else {
            paramCodedOutputStream.writeBytes(j, (ByteString)this.objects[b]);
          } 
        } else {
          paramCodedOutputStream.writeFixed64(j, ((Long)this.objects[b]).longValue());
        } 
      } else {
        paramCodedOutputStream.writeUInt64(j, ((Long)this.objects[b]).longValue());
      } 
    } 
  }
  
  public void writeAsMessageSetTo(CodedOutputStream paramCodedOutputStream) throws IOException {
    for (byte b = 0; b < this.count; b++) {
      int i = WireFormat.getTagFieldNumber(this.tags[b]);
      paramCodedOutputStream.writeRawMessageSetExtension(i, (ByteString)this.objects[b]);
    } 
  }
  
  void writeAsMessageSetTo(Writer paramWriter) throws IOException {
    if (paramWriter.fieldOrder() == Writer.FieldOrder.DESCENDING) {
      for (int i = this.count - 1; i >= 0; i--) {
        int j = WireFormat.getTagFieldNumber(this.tags[i]);
        paramWriter.writeMessageSetItem(j, this.objects[i]);
      } 
    } else {
      for (byte b = 0; b < this.count; b++) {
        int i = WireFormat.getTagFieldNumber(this.tags[b]);
        paramWriter.writeMessageSetItem(i, this.objects[b]);
      } 
    } 
  }
  
  public void writeTo(Writer paramWriter) throws IOException {
    if (this.count == 0)
      return; 
    if (paramWriter.fieldOrder() == Writer.FieldOrder.ASCENDING) {
      for (byte b = 0; b < this.count; b++)
        writeField(this.tags[b], this.objects[b], paramWriter); 
    } else {
      for (int i = this.count - 1; i >= 0; i--)
        writeField(this.tags[i], this.objects[i], paramWriter); 
    } 
  }
  
  private static void writeField(int paramInt, Object paramObject, Writer paramWriter) throws IOException {
    int i = WireFormat.getTagFieldNumber(paramInt);
    paramInt = WireFormat.getTagWireType(paramInt);
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt == 5) {
              paramWriter.writeFixed32(i, ((Integer)paramObject).intValue());
            } else {
              throw new RuntimeException(InvalidProtocolBufferException.invalidWireType());
            } 
          } else if (paramWriter.fieldOrder() == Writer.FieldOrder.ASCENDING) {
            paramWriter.writeStartGroup(i);
            ((UnknownFieldSetLite)paramObject).writeTo(paramWriter);
            paramWriter.writeEndGroup(i);
          } else {
            paramWriter.writeEndGroup(i);
            ((UnknownFieldSetLite)paramObject).writeTo(paramWriter);
            paramWriter.writeStartGroup(i);
          } 
        } else {
          paramWriter.writeBytes(i, (ByteString)paramObject);
        } 
      } else {
        paramWriter.writeFixed64(i, ((Long)paramObject).longValue());
      } 
    } else {
      paramWriter.writeInt64(i, ((Long)paramObject).longValue());
    } 
  }
  
  public int getSerializedSizeAsMessageSet() {
    int i = this.memoizedSerializedSize;
    if (i != -1)
      return i; 
    int j = 0;
    for (i = 0; i < this.count; i++) {
      int k = this.tags[i];
      k = WireFormat.getTagFieldNumber(k);
      ByteString byteString = (ByteString)this.objects[i];
      j += CodedOutputStream.computeRawMessageSetExtensionSize(k, byteString);
    } 
    this.memoizedSerializedSize = j;
    return j;
  }
  
  public int getSerializedSize() {
    int i = this.memoizedSerializedSize;
    if (i != -1)
      return i; 
    i = 0;
    for (byte b = 0; b < this.count; b++) {
      int j = this.tags[b];
      int k = WireFormat.getTagFieldNumber(j);
      j = WireFormat.getTagWireType(j);
      if (j != 0) {
        if (j != 1) {
          if (j != 2) {
            if (j != 3) {
              if (j == 5) {
                i += CodedOutputStream.computeFixed32Size(k, ((Integer)this.objects[b]).intValue());
              } else {
                throw new IllegalStateException(InvalidProtocolBufferException.invalidWireType());
              } 
            } else {
              k = CodedOutputStream.computeTagSize(k);
              UnknownFieldSetLite unknownFieldSetLite = (UnknownFieldSetLite)this.objects[b];
              i += k * 2 + unknownFieldSetLite.getSerializedSize();
            } 
          } else {
            i += CodedOutputStream.computeBytesSize(k, (ByteString)this.objects[b]);
          } 
        } else {
          i += CodedOutputStream.computeFixed64Size(k, ((Long)this.objects[b]).longValue());
        } 
      } else {
        i += CodedOutputStream.computeUInt64Size(k, ((Long)this.objects[b]).longValue());
      } 
    } 
    this.memoizedSerializedSize = i;
    return i;
  }
  
  private static boolean equals(int[] paramArrayOfint1, int[] paramArrayOfint2, int paramInt) {
    for (byte b = 0; b < paramInt; b++) {
      if (paramArrayOfint1[b] != paramArrayOfint2[b])
        return false; 
    } 
    return true;
  }
  
  private static boolean equals(Object[] paramArrayOfObject1, Object[] paramArrayOfObject2, int paramInt) {
    for (byte b = 0; b < paramInt; b++) {
      if (!paramArrayOfObject1[b].equals(paramArrayOfObject2[b]))
        return false; 
    } 
    return true;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (!(paramObject instanceof UnknownFieldSetLite))
      return false; 
    paramObject = paramObject;
    int i = this.count;
    if (i == ((UnknownFieldSetLite)paramObject).count) {
      int[] arrayOfInt1 = this.tags, arrayOfInt2 = ((UnknownFieldSetLite)paramObject).tags;
      if (equals(arrayOfInt1, arrayOfInt2, i)) {
        Object[] arrayOfObject = this.objects;
        paramObject = ((UnknownFieldSetLite)paramObject).objects;
        i = this.count;
        if (equals(arrayOfObject, (Object[])paramObject, i))
          return true; 
      } 
    } 
    return false;
  }
  
  private static int hashCode(int[] paramArrayOfint, int paramInt) {
    int i = 17;
    for (byte b = 0; b < paramInt; b++)
      i = i * 31 + paramArrayOfint[b]; 
    return i;
  }
  
  private static int hashCode(Object[] paramArrayOfObject, int paramInt) {
    int i = 17;
    for (byte b = 0; b < paramInt; b++)
      i = i * 31 + paramArrayOfObject[b].hashCode(); 
    return i;
  }
  
  public int hashCode() {
    int i = this.count;
    int j = hashCode(this.tags, i);
    int k = hashCode(this.objects, this.count);
    return ((17 * 31 + i) * 31 + j) * 31 + k;
  }
  
  final void printWithIndent(StringBuilder paramStringBuilder, int paramInt) {
    for (byte b = 0; b < this.count; b++) {
      int i = WireFormat.getTagFieldNumber(this.tags[b]);
      MessageLiteToString.printField(paramStringBuilder, paramInt, String.valueOf(i), this.objects[b]);
    } 
  }
  
  void storeField(int paramInt, Object paramObject) {
    checkMutable();
    ensureCapacity();
    int arrayOfInt[] = this.tags, i = this.count;
    arrayOfInt[i] = paramInt;
    this.objects[i] = paramObject;
    this.count = i + 1;
  }
  
  private void ensureCapacity() {
    int i = this.count;
    if (i == this.tags.length) {
      if (i < 4) {
        i = 8;
      } else {
        i >>= 1;
      } 
      i = this.count + i;
      this.tags = Arrays.copyOf(this.tags, i);
      this.objects = Arrays.copyOf(this.objects, i);
    } 
  }
  
  boolean mergeFieldFrom(int paramInt, CodedInputStream paramCodedInputStream) throws IOException {
    checkMutable();
    int i = WireFormat.getTagFieldNumber(paramInt);
    int j = WireFormat.getTagWireType(paramInt);
    if (j != 0) {
      if (j != 1) {
        if (j != 2) {
          if (j != 3) {
            if (j != 4) {
              if (j == 5) {
                storeField(paramInt, Integer.valueOf(paramCodedInputStream.readFixed32()));
                return true;
              } 
              throw InvalidProtocolBufferException.invalidWireType();
            } 
            return false;
          } 
          UnknownFieldSetLite unknownFieldSetLite = new UnknownFieldSetLite();
          unknownFieldSetLite.mergeFrom(paramCodedInputStream);
          paramCodedInputStream.checkLastTagWas(WireFormat.makeTag(i, 4));
          storeField(paramInt, unknownFieldSetLite);
          return true;
        } 
        storeField(paramInt, paramCodedInputStream.readBytes());
        return true;
      } 
      storeField(paramInt, Long.valueOf(paramCodedInputStream.readFixed64()));
      return true;
    } 
    storeField(paramInt, Long.valueOf(paramCodedInputStream.readInt64()));
    return true;
  }
  
  UnknownFieldSetLite mergeVarintField(int paramInt1, int paramInt2) {
    checkMutable();
    if (paramInt1 != 0) {
      storeField(WireFormat.makeTag(paramInt1, 0), Long.valueOf(paramInt2));
      return this;
    } 
    throw new IllegalArgumentException("Zero is not a valid field number.");
  }
  
  UnknownFieldSetLite mergeLengthDelimitedField(int paramInt, ByteString paramByteString) {
    checkMutable();
    if (paramInt != 0) {
      storeField(WireFormat.makeTag(paramInt, 2), paramByteString);
      return this;
    } 
    throw new IllegalArgumentException("Zero is not a valid field number.");
  }
  
  private UnknownFieldSetLite mergeFrom(CodedInputStream paramCodedInputStream) throws IOException {
    int i;
    do {
      i = paramCodedInputStream.readTag();
    } while (i != 0 && mergeFieldFrom(i, paramCodedInputStream));
    return this;
  }
}
