package com.android.framework.protobuf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

abstract class ListFieldSchema {
  private ListFieldSchema() {}
  
  private static final ListFieldSchema FULL_INSTANCE = new ListFieldSchemaFull();
  
  private static final ListFieldSchema LITE_INSTANCE = new ListFieldSchemaLite();
  
  static ListFieldSchema full() {
    return FULL_INSTANCE;
  }
  
  static ListFieldSchema lite() {
    return LITE_INSTANCE;
  }
  
  abstract void makeImmutableListAt(Object paramObject, long paramLong);
  
  abstract <L> void mergeListsAt(Object paramObject1, Object paramObject2, long paramLong);
  
  abstract <L> List<L> mutableListAt(Object paramObject, long paramLong);
  
  class ListFieldSchemaFull extends ListFieldSchema {
    private ListFieldSchemaFull() {}
    
    private static final Class<?> UNMODIFIABLE_LIST_CLASS = Collections.unmodifiableList(Collections.emptyList()).getClass();
    
    <L> List<L> mutableListAt(Object param1Object, long param1Long) {
      return mutableListAt(param1Object, param1Long, 10);
    }
    
    void makeImmutableListAt(Object param1Object, long param1Long) {
      List<?> list = (List)UnsafeUtil.getObject(param1Object, param1Long);
      if (list instanceof LazyStringList) {
        list = ((LazyStringList)list).getUnmodifiableView();
      } else {
        if (UNMODIFIABLE_LIST_CLASS.isAssignableFrom(list.getClass()))
          return; 
        if (list instanceof PrimitiveNonBoxingCollection && list instanceof Internal.ProtobufList) {
          if (((Internal.ProtobufList)list).isModifiable())
            ((Internal.ProtobufList)list).makeImmutable(); 
          return;
        } 
        list = Collections.unmodifiableList(list);
      } 
      UnsafeUtil.putObject(param1Object, param1Long, list);
    }
    
    private static <L> List<L> mutableListAt(Object param1Object, long param1Long, int param1Int) {
      List<?> list2, list1 = getList(param1Object, param1Long);
      if (list1.isEmpty()) {
        if (list1 instanceof LazyStringList) {
          LazyStringArrayList lazyStringArrayList = new LazyStringArrayList(param1Int);
        } else if (list1 instanceof PrimitiveNonBoxingCollection && list1 instanceof Internal.ProtobufList) {
          Internal.ProtobufList protobufList = ((Internal.ProtobufList)list1).mutableCopyWithCapacity(param1Int);
        } else {
          list2 = new ArrayList(param1Int);
        } 
        UnsafeUtil.putObject(param1Object, param1Long, list2);
      } else if (UNMODIFIABLE_LIST_CLASS.isAssignableFrom(list1.getClass())) {
        list2 = new ArrayList(list1.size() + param1Int);
        list2.addAll(list1);
        UnsafeUtil.putObject(param1Object, param1Long, list2);
      } else if (list1 instanceof UnmodifiableLazyStringList) {
        list2 = new LazyStringArrayList(list1.size() + param1Int);
        list2.addAll((UnmodifiableLazyStringList)list1);
        UnsafeUtil.putObject(param1Object, param1Long, list2);
      } else {
        list2 = list1;
        if (list1 instanceof PrimitiveNonBoxingCollection) {
          list2 = list1;
          if (list1 instanceof Internal.ProtobufList) {
            Internal.ProtobufList protobufList = (Internal.ProtobufList)list1;
            list2 = list1;
            if (!protobufList.isModifiable()) {
              list2 = ((Internal.ProtobufList)list1).mutableCopyWithCapacity(list1.size() + param1Int);
              UnsafeUtil.putObject(param1Object, param1Long, list2);
            } 
          } 
        } 
      } 
      return (List)list2;
    }
    
    <E> void mergeListsAt(Object param1Object1, Object<?> param1Object2, long param1Long) {
      param1Object2 = getList(param1Object2, param1Long);
      List<?> list = mutableListAt(param1Object1, param1Long, param1Object2.size());
      int i = list.size();
      int j = param1Object2.size();
      if (i > 0 && j > 0)
        list.addAll((Collection<?>)param1Object2); 
      if (i > 0)
        param1Object2 = (Object<?>)list; 
      UnsafeUtil.putObject(param1Object1, param1Long, param1Object2);
    }
    
    static <E> List<E> getList(Object param1Object, long param1Long) {
      return (List<E>)UnsafeUtil.getObject(param1Object, param1Long);
    }
  }
  
  class ListFieldSchemaLite extends ListFieldSchema {
    private ListFieldSchemaLite() {}
    
    <L> List<L> mutableListAt(Object param1Object, long param1Long) {
      Internal.ProtobufList<?> protobufList1 = getProtobufList(param1Object, param1Long);
      Internal.ProtobufList<?> protobufList2 = protobufList1;
      if (!protobufList1.isModifiable()) {
        int i = protobufList1.size();
        if (i == 0) {
          i = 10;
        } else {
          i *= 2;
        } 
        protobufList2 = protobufList1.mutableCopyWithCapacity(i);
        UnsafeUtil.putObject(param1Object, param1Long, protobufList2);
      } 
      return (List)protobufList2;
    }
    
    void makeImmutableListAt(Object<?> param1Object, long param1Long) {
      param1Object = getProtobufList(param1Object, param1Long);
      param1Object.makeImmutable();
    }
    
    <E> void mergeListsAt(Object param1Object1, Object<?> param1Object2, long param1Long) {
      Internal.ProtobufList<?> protobufList1 = getProtobufList(param1Object1, param1Long);
      Internal.ProtobufList<?> protobufList2 = getProtobufList(param1Object2, param1Long);
      int i = protobufList1.size();
      int j = protobufList2.size();
      param1Object2 = (Object<?>)protobufList1;
      if (i > 0) {
        param1Object2 = (Object<?>)protobufList1;
        if (j > 0) {
          param1Object2 = (Object<?>)protobufList1;
          if (!protobufList1.isModifiable())
            param1Object2 = (Object<?>)protobufList1.mutableCopyWithCapacity(i + j); 
          param1Object2.addAll(protobufList2);
        } 
      } 
      if (i <= 0)
        param1Object2 = (Object<?>)protobufList2; 
      UnsafeUtil.putObject(param1Object1, param1Long, param1Object2);
    }
    
    static <E> Internal.ProtobufList<E> getProtobufList(Object param1Object, long param1Long) {
      return (Internal.ProtobufList<E>)UnsafeUtil.getObject(param1Object, param1Long);
    }
  }
}
