package com.android.framework.protobuf;

import java.io.IOException;

abstract class UnknownFieldSchema<T, B> {
  abstract void writeTo(T paramT, Writer paramWriter) throws IOException;
  
  abstract void writeAsMessageSetTo(T paramT, Writer paramWriter) throws IOException;
  
  abstract T toImmutable(B paramB);
  
  abstract boolean shouldDiscardUnknownFields(Reader paramReader);
  
  abstract void setToMessage(Object paramObject, T paramT);
  
  abstract void setBuilderToMessage(Object paramObject, B paramB);
  
  abstract B newBuilder();
  
  final boolean mergeOneFieldFrom(B paramB, Reader paramReader) throws IOException {
    int i = paramReader.getTag();
    int j = WireFormat.getTagFieldNumber(i);
    i = WireFormat.getTagWireType(i);
    if (i != 0) {
      if (i != 1) {
        if (i != 2) {
          if (i != 3) {
            if (i != 4) {
              if (i == 5) {
                addFixed32(paramB, j, paramReader.readFixed32());
                return true;
              } 
              throw InvalidProtocolBufferException.invalidWireType();
            } 
            return false;
          } 
          B b = newBuilder();
          i = WireFormat.makeTag(j, 4);
          mergeFrom(b, paramReader);
          if (i == paramReader.getTag()) {
            addGroup(paramB, j, toImmutable(b));
            return true;
          } 
          throw InvalidProtocolBufferException.invalidEndTag();
        } 
        addLengthDelimited(paramB, j, paramReader.readBytes());
        return true;
      } 
      addFixed64(paramB, j, paramReader.readFixed64());
      return true;
    } 
    addVarint(paramB, j, paramReader.readInt64());
    return true;
  }
  
  final void mergeFrom(B paramB, Reader paramReader) throws IOException {
    do {
    
    } while (paramReader.getFieldNumber() != Integer.MAX_VALUE && 
      mergeOneFieldFrom(paramB, paramReader));
  }
  
  abstract T merge(T paramT1, T paramT2);
  
  abstract void makeImmutable(Object paramObject);
  
  abstract int getSerializedSizeAsMessageSet(T paramT);
  
  abstract int getSerializedSize(T paramT);
  
  abstract T getFromMessage(Object paramObject);
  
  abstract B getBuilderFromMessage(Object paramObject);
  
  abstract void addVarint(B paramB, int paramInt, long paramLong);
  
  abstract void addLengthDelimited(B paramB, int paramInt, ByteString paramByteString);
  
  abstract void addGroup(B paramB, int paramInt, T paramT);
  
  abstract void addFixed64(B paramB, int paramInt, long paramLong);
  
  abstract void addFixed32(B paramB, int paramInt1, int paramInt2);
}
