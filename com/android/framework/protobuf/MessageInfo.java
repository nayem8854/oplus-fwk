package com.android.framework.protobuf;

interface MessageInfo {
  MessageLite getDefaultInstance();
  
  ProtoSyntax getSyntax();
  
  boolean isMessageSetWireFormat();
}
