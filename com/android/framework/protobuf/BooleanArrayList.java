package com.android.framework.protobuf;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

final class BooleanArrayList extends AbstractProtobufList<Boolean> implements Internal.BooleanList, RandomAccess, PrimitiveNonBoxingCollection {
  private static final BooleanArrayList EMPTY_LIST;
  
  private boolean[] array;
  
  private int size;
  
  static {
    BooleanArrayList booleanArrayList = new BooleanArrayList(new boolean[0], 0);
    booleanArrayList.makeImmutable();
  }
  
  public static BooleanArrayList emptyList() {
    return EMPTY_LIST;
  }
  
  BooleanArrayList() {
    this(new boolean[10], 0);
  }
  
  private BooleanArrayList(boolean[] paramArrayOfboolean, int paramInt) {
    this.array = paramArrayOfboolean;
    this.size = paramInt;
  }
  
  protected void removeRange(int paramInt1, int paramInt2) {
    ensureIsMutable();
    if (paramInt2 >= paramInt1) {
      boolean[] arrayOfBoolean = this.array;
      System.arraycopy(arrayOfBoolean, paramInt2, arrayOfBoolean, paramInt1, this.size - paramInt2);
      this.size -= paramInt2 - paramInt1;
      this.modCount++;
      return;
    } 
    throw new IndexOutOfBoundsException("toIndex < fromIndex");
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof BooleanArrayList))
      return super.equals(paramObject); 
    paramObject = paramObject;
    if (this.size != ((BooleanArrayList)paramObject).size)
      return false; 
    paramObject = ((BooleanArrayList)paramObject).array;
    for (byte b = 0; b < this.size; b++) {
      if (this.array[b] != paramObject[b])
        return false; 
    } 
    return true;
  }
  
  public int hashCode() {
    int i = 1;
    for (byte b = 0; b < this.size; b++)
      i = i * 31 + Internal.hashBoolean(this.array[b]); 
    return i;
  }
  
  public Internal.BooleanList mutableCopyWithCapacity(int paramInt) {
    if (paramInt >= this.size)
      return new BooleanArrayList(Arrays.copyOf(this.array, paramInt), this.size); 
    throw new IllegalArgumentException();
  }
  
  public Boolean get(int paramInt) {
    return Boolean.valueOf(getBoolean(paramInt));
  }
  
  public boolean getBoolean(int paramInt) {
    ensureIndexInRange(paramInt);
    return this.array[paramInt];
  }
  
  public int size() {
    return this.size;
  }
  
  public Boolean set(int paramInt, Boolean paramBoolean) {
    return Boolean.valueOf(setBoolean(paramInt, paramBoolean.booleanValue()));
  }
  
  public boolean setBoolean(int paramInt, boolean paramBoolean) {
    ensureIsMutable();
    ensureIndexInRange(paramInt);
    boolean arrayOfBoolean[] = this.array, bool = arrayOfBoolean[paramInt];
    arrayOfBoolean[paramInt] = paramBoolean;
    return bool;
  }
  
  public void add(int paramInt, Boolean paramBoolean) {
    addBoolean(paramInt, paramBoolean.booleanValue());
  }
  
  public void addBoolean(boolean paramBoolean) {
    addBoolean(this.size, paramBoolean);
  }
  
  private void addBoolean(int paramInt, boolean paramBoolean) {
    ensureIsMutable();
    if (paramInt >= 0) {
      int i = this.size;
      if (paramInt <= i) {
        boolean[] arrayOfBoolean = this.array;
        if (i < arrayOfBoolean.length) {
          System.arraycopy(arrayOfBoolean, paramInt, arrayOfBoolean, paramInt + 1, i - paramInt);
        } else {
          i = i * 3 / 2;
          boolean[] arrayOfBoolean1 = new boolean[i + 1];
          System.arraycopy(arrayOfBoolean, 0, arrayOfBoolean1, 0, paramInt);
          System.arraycopy(this.array, paramInt, arrayOfBoolean1, paramInt + 1, this.size - paramInt);
          this.array = arrayOfBoolean1;
        } 
        this.array[paramInt] = paramBoolean;
        this.size++;
        this.modCount++;
        return;
      } 
    } 
    throw new IndexOutOfBoundsException(makeOutOfBoundsExceptionMessage(paramInt));
  }
  
  public boolean addAll(Collection<? extends Boolean> paramCollection) {
    ensureIsMutable();
    Internal.checkNotNull(paramCollection);
    if (!(paramCollection instanceof BooleanArrayList))
      return super.addAll(paramCollection); 
    BooleanArrayList booleanArrayList = (BooleanArrayList)paramCollection;
    int i = booleanArrayList.size;
    if (i == 0)
      return false; 
    int j = this.size;
    if (Integer.MAX_VALUE - j >= i) {
      i = j + i;
      boolean[] arrayOfBoolean = this.array;
      if (i > arrayOfBoolean.length)
        this.array = Arrays.copyOf(arrayOfBoolean, i); 
      System.arraycopy(booleanArrayList.array, 0, this.array, this.size, booleanArrayList.size);
      this.size = i;
      this.modCount++;
      return true;
    } 
    throw new OutOfMemoryError();
  }
  
  public boolean remove(Object paramObject) {
    ensureIsMutable();
    for (byte b = 0; b < this.size; b++) {
      if (paramObject.equals(Boolean.valueOf(this.array[b]))) {
        paramObject = this.array;
        System.arraycopy(paramObject, b + 1, paramObject, b, this.size - b - 1);
        this.size--;
        this.modCount++;
        return true;
      } 
    } 
    return false;
  }
  
  public Boolean remove(int paramInt) {
    ensureIsMutable();
    ensureIndexInRange(paramInt);
    boolean arrayOfBoolean[] = this.array, bool = arrayOfBoolean[paramInt];
    int i = this.size;
    if (paramInt < i - 1)
      System.arraycopy(arrayOfBoolean, paramInt + 1, arrayOfBoolean, paramInt, i - paramInt - 1); 
    this.size--;
    this.modCount++;
    return Boolean.valueOf(bool);
  }
  
  private void ensureIndexInRange(int paramInt) {
    if (paramInt >= 0 && paramInt < this.size)
      return; 
    throw new IndexOutOfBoundsException(makeOutOfBoundsExceptionMessage(paramInt));
  }
  
  private String makeOutOfBoundsExceptionMessage(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Index:");
    stringBuilder.append(paramInt);
    stringBuilder.append(", Size:");
    stringBuilder.append(this.size);
    return stringBuilder.toString();
  }
}
