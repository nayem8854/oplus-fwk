package com.android.framework.protobuf;

import java.io.IOException;

public class LazyFieldLite {
  private static final ExtensionRegistryLite EMPTY_REGISTRY = ExtensionRegistryLite.getEmptyRegistry();
  
  private ByteString delayedBytes;
  
  private ExtensionRegistryLite extensionRegistry;
  
  private volatile ByteString memoizedBytes;
  
  protected volatile MessageLite value;
  
  public LazyFieldLite(ExtensionRegistryLite paramExtensionRegistryLite, ByteString paramByteString) {
    checkArguments(paramExtensionRegistryLite, paramByteString);
    this.extensionRegistry = paramExtensionRegistryLite;
    this.delayedBytes = paramByteString;
  }
  
  public LazyFieldLite() {}
  
  public static LazyFieldLite fromValue(MessageLite paramMessageLite) {
    LazyFieldLite lazyFieldLite = new LazyFieldLite();
    lazyFieldLite.setValue(paramMessageLite);
    return lazyFieldLite;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof LazyFieldLite))
      return false; 
    paramObject = paramObject;
    MessageLite messageLite1 = this.value;
    MessageLite messageLite2 = ((LazyFieldLite)paramObject).value;
    if (messageLite1 == null && messageLite2 == null)
      return toByteString().equals(paramObject.toByteString()); 
    if (messageLite1 != null && messageLite2 != null)
      return messageLite1.equals(messageLite2); 
    if (messageLite1 != null)
      return messageLite1.equals(paramObject.getValue(messageLite1.getDefaultInstanceForType())); 
    return getValue(messageLite2.getDefaultInstanceForType()).equals(messageLite2);
  }
  
  public int hashCode() {
    return 1;
  }
  
  public boolean containsDefaultInstance() {
    if (this.memoizedBytes != ByteString.EMPTY) {
      if (this.value == null) {
        ByteString byteString = this.delayedBytes;
        if (byteString == null || byteString == ByteString.EMPTY)
          return true; 
      } 
      return false;
    } 
    return true;
  }
  
  public void clear() {
    this.delayedBytes = null;
    this.value = null;
    this.memoizedBytes = null;
  }
  
  public void set(LazyFieldLite paramLazyFieldLite) {
    this.delayedBytes = paramLazyFieldLite.delayedBytes;
    this.value = paramLazyFieldLite.value;
    this.memoizedBytes = paramLazyFieldLite.memoizedBytes;
    ExtensionRegistryLite extensionRegistryLite = paramLazyFieldLite.extensionRegistry;
    if (extensionRegistryLite != null)
      this.extensionRegistry = extensionRegistryLite; 
  }
  
  public MessageLite getValue(MessageLite paramMessageLite) {
    ensureInitialized(paramMessageLite);
    return this.value;
  }
  
  public MessageLite setValue(MessageLite paramMessageLite) {
    MessageLite messageLite = this.value;
    this.delayedBytes = null;
    this.memoizedBytes = null;
    this.value = paramMessageLite;
    return messageLite;
  }
  
  public void merge(LazyFieldLite paramLazyFieldLite) {
    if (paramLazyFieldLite.containsDefaultInstance())
      return; 
    if (containsDefaultInstance()) {
      set(paramLazyFieldLite);
      return;
    } 
    if (this.extensionRegistry == null)
      this.extensionRegistry = paramLazyFieldLite.extensionRegistry; 
    ByteString byteString = this.delayedBytes;
    if (byteString != null) {
      ByteString byteString1 = paramLazyFieldLite.delayedBytes;
      if (byteString1 != null) {
        this.delayedBytes = byteString.concat(byteString1);
        return;
      } 
    } 
    if (this.value == null && paramLazyFieldLite.value != null) {
      setValue(mergeValueAndBytes(paramLazyFieldLite.value, this.delayedBytes, this.extensionRegistry));
      return;
    } 
    if (this.value != null && paramLazyFieldLite.value == null) {
      setValue(mergeValueAndBytes(this.value, paramLazyFieldLite.delayedBytes, paramLazyFieldLite.extensionRegistry));
      return;
    } 
    setValue(this.value.toBuilder().mergeFrom(paramLazyFieldLite.value).build());
  }
  
  public void mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite) throws IOException {
    if (containsDefaultInstance()) {
      setByteString(paramCodedInputStream.readBytes(), paramExtensionRegistryLite);
      return;
    } 
    if (this.extensionRegistry == null)
      this.extensionRegistry = paramExtensionRegistryLite; 
    ByteString byteString = this.delayedBytes;
    if (byteString != null) {
      setByteString(byteString.concat(paramCodedInputStream.readBytes()), this.extensionRegistry);
      return;
    } 
    try {
      setValue(this.value.toBuilder().mergeFrom(paramCodedInputStream, paramExtensionRegistryLite).build());
    } catch (InvalidProtocolBufferException invalidProtocolBufferException) {}
  }
  
  private static MessageLite mergeValueAndBytes(MessageLite paramMessageLite, ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite) {
    try {
      return paramMessageLite.toBuilder().mergeFrom(paramByteString, paramExtensionRegistryLite).build();
    } catch (InvalidProtocolBufferException invalidProtocolBufferException) {
      return paramMessageLite;
    } 
  }
  
  public void setByteString(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite) {
    checkArguments(paramExtensionRegistryLite, paramByteString);
    this.delayedBytes = paramByteString;
    this.extensionRegistry = paramExtensionRegistryLite;
    this.value = null;
    this.memoizedBytes = null;
  }
  
  public int getSerializedSize() {
    if (this.memoizedBytes != null)
      return this.memoizedBytes.size(); 
    ByteString byteString = this.delayedBytes;
    if (byteString != null)
      return byteString.size(); 
    if (this.value != null)
      return this.value.getSerializedSize(); 
    return 0;
  }
  
  public ByteString toByteString() {
    // Byte code:
    //   0: aload_0
    //   1: getfield memoizedBytes : Lcom/android/framework/protobuf/ByteString;
    //   4: ifnull -> 12
    //   7: aload_0
    //   8: getfield memoizedBytes : Lcom/android/framework/protobuf/ByteString;
    //   11: areturn
    //   12: aload_0
    //   13: getfield delayedBytes : Lcom/android/framework/protobuf/ByteString;
    //   16: astore_1
    //   17: aload_1
    //   18: ifnull -> 23
    //   21: aload_1
    //   22: areturn
    //   23: aload_0
    //   24: monitorenter
    //   25: aload_0
    //   26: getfield memoizedBytes : Lcom/android/framework/protobuf/ByteString;
    //   29: ifnull -> 41
    //   32: aload_0
    //   33: getfield memoizedBytes : Lcom/android/framework/protobuf/ByteString;
    //   36: astore_1
    //   37: aload_0
    //   38: monitorexit
    //   39: aload_1
    //   40: areturn
    //   41: aload_0
    //   42: getfield value : Lcom/android/framework/protobuf/MessageLite;
    //   45: ifnonnull -> 58
    //   48: aload_0
    //   49: getstatic com/android/framework/protobuf/ByteString.EMPTY : Lcom/android/framework/protobuf/ByteString;
    //   52: putfield memoizedBytes : Lcom/android/framework/protobuf/ByteString;
    //   55: goto -> 71
    //   58: aload_0
    //   59: aload_0
    //   60: getfield value : Lcom/android/framework/protobuf/MessageLite;
    //   63: invokeinterface toByteString : ()Lcom/android/framework/protobuf/ByteString;
    //   68: putfield memoizedBytes : Lcom/android/framework/protobuf/ByteString;
    //   71: aload_0
    //   72: getfield memoizedBytes : Lcom/android/framework/protobuf/ByteString;
    //   75: astore_1
    //   76: aload_0
    //   77: monitorexit
    //   78: aload_1
    //   79: areturn
    //   80: astore_1
    //   81: aload_0
    //   82: monitorexit
    //   83: aload_1
    //   84: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #370	-> 0
    //   #371	-> 7
    //   #375	-> 12
    //   #376	-> 21
    //   #378	-> 23
    //   #379	-> 25
    //   #380	-> 32
    //   #382	-> 41
    //   #383	-> 48
    //   #385	-> 58
    //   #387	-> 71
    //   #388	-> 80
    // Exception table:
    //   from	to	target	type
    //   25	32	80	finally
    //   32	39	80	finally
    //   41	48	80	finally
    //   48	55	80	finally
    //   58	71	80	finally
    //   71	78	80	finally
    //   81	83	80	finally
  }
  
  void writeTo(Writer paramWriter, int paramInt) throws IOException {
    if (this.memoizedBytes != null) {
      paramWriter.writeBytes(paramInt, this.memoizedBytes);
    } else {
      ByteString byteString = this.delayedBytes;
      if (byteString != null) {
        paramWriter.writeBytes(paramInt, byteString);
      } else if (this.value != null) {
        paramWriter.writeMessage(paramInt, this.value);
      } else {
        paramWriter.writeBytes(paramInt, ByteString.EMPTY);
      } 
    } 
  }
  
  protected void ensureInitialized(MessageLite paramMessageLite) {
    // Byte code:
    //   0: aload_0
    //   1: getfield value : Lcom/android/framework/protobuf/MessageLite;
    //   4: ifnull -> 8
    //   7: return
    //   8: aload_0
    //   9: monitorenter
    //   10: aload_0
    //   11: getfield value : Lcom/android/framework/protobuf/MessageLite;
    //   14: ifnull -> 20
    //   17: aload_0
    //   18: monitorexit
    //   19: return
    //   20: aload_0
    //   21: getfield delayedBytes : Lcom/android/framework/protobuf/ByteString;
    //   24: ifnull -> 66
    //   27: aload_1
    //   28: invokeinterface getParserForType : ()Lcom/android/framework/protobuf/Parser;
    //   33: aload_0
    //   34: getfield delayedBytes : Lcom/android/framework/protobuf/ByteString;
    //   37: aload_0
    //   38: getfield extensionRegistry : Lcom/android/framework/protobuf/ExtensionRegistryLite;
    //   41: invokeinterface parseFrom : (Lcom/android/framework/protobuf/ByteString;Lcom/android/framework/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;
    //   46: checkcast com/android/framework/protobuf/MessageLite
    //   49: astore_2
    //   50: aload_0
    //   51: aload_2
    //   52: putfield value : Lcom/android/framework/protobuf/MessageLite;
    //   55: aload_0
    //   56: aload_0
    //   57: getfield delayedBytes : Lcom/android/framework/protobuf/ByteString;
    //   60: putfield memoizedBytes : Lcom/android/framework/protobuf/ByteString;
    //   63: goto -> 78
    //   66: aload_0
    //   67: aload_1
    //   68: putfield value : Lcom/android/framework/protobuf/MessageLite;
    //   71: aload_0
    //   72: getstatic com/android/framework/protobuf/ByteString.EMPTY : Lcom/android/framework/protobuf/ByteString;
    //   75: putfield memoizedBytes : Lcom/android/framework/protobuf/ByteString;
    //   78: goto -> 94
    //   81: astore_2
    //   82: aload_0
    //   83: aload_1
    //   84: putfield value : Lcom/android/framework/protobuf/MessageLite;
    //   87: aload_0
    //   88: getstatic com/android/framework/protobuf/ByteString.EMPTY : Lcom/android/framework/protobuf/ByteString;
    //   91: putfield memoizedBytes : Lcom/android/framework/protobuf/ByteString;
    //   94: aload_0
    //   95: monitorexit
    //   96: return
    //   97: astore_1
    //   98: aload_0
    //   99: monitorexit
    //   100: aload_1
    //   101: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #406	-> 0
    //   #407	-> 7
    //   #409	-> 8
    //   #410	-> 10
    //   #411	-> 17
    //   #414	-> 20
    //   #416	-> 27
    //   #417	-> 27
    //   #418	-> 50
    //   #419	-> 55
    //   #420	-> 63
    //   #421	-> 66
    //   #422	-> 71
    //   #429	-> 78
    //   #424	-> 81
    //   #427	-> 82
    //   #428	-> 87
    //   #430	-> 94
    //   #431	-> 96
    //   #430	-> 97
    // Exception table:
    //   from	to	target	type
    //   10	17	97	finally
    //   17	19	97	finally
    //   20	27	81	com/android/framework/protobuf/InvalidProtocolBufferException
    //   20	27	97	finally
    //   27	50	81	com/android/framework/protobuf/InvalidProtocolBufferException
    //   27	50	97	finally
    //   50	55	81	com/android/framework/protobuf/InvalidProtocolBufferException
    //   50	55	97	finally
    //   55	63	81	com/android/framework/protobuf/InvalidProtocolBufferException
    //   55	63	97	finally
    //   66	71	81	com/android/framework/protobuf/InvalidProtocolBufferException
    //   66	71	97	finally
    //   71	78	81	com/android/framework/protobuf/InvalidProtocolBufferException
    //   71	78	97	finally
    //   82	87	97	finally
    //   87	94	97	finally
    //   94	96	97	finally
    //   98	100	97	finally
  }
  
  private static void checkArguments(ExtensionRegistryLite paramExtensionRegistryLite, ByteString paramByteString) {
    if (paramExtensionRegistryLite != null) {
      if (paramByteString != null)
        return; 
      throw new NullPointerException("found null ByteString");
    } 
    throw new NullPointerException("found null ExtensionRegistry");
  }
}
