package com.android.framework.protobuf;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public final class MapFieldLite<K, V> extends LinkedHashMap<K, V> {
  private static final MapFieldLite EMPTY_MAP_FIELD;
  
  private boolean isMutable;
  
  private MapFieldLite() {
    this.isMutable = true;
  }
  
  private MapFieldLite(Map<K, V> paramMap) {
    super(paramMap);
    this.isMutable = true;
  }
  
  static {
    MapFieldLite mapFieldLite = new MapFieldLite();
    mapFieldLite.makeImmutable();
  }
  
  public static <K, V> MapFieldLite<K, V> emptyMapField() {
    return EMPTY_MAP_FIELD;
  }
  
  public void mergeFrom(MapFieldLite<K, V> paramMapFieldLite) {
    ensureMutable();
    if (!paramMapFieldLite.isEmpty())
      putAll(paramMapFieldLite); 
  }
  
  public Set<Map.Entry<K, V>> entrySet() {
    Set<Map.Entry<K, V>> set;
    if (isEmpty()) {
      set = Collections.emptySet();
    } else {
      set = super.entrySet();
    } 
    return set;
  }
  
  public void clear() {
    ensureMutable();
    super.clear();
  }
  
  public V put(K paramK, V paramV) {
    ensureMutable();
    Internal.checkNotNull(paramK);
    Internal.checkNotNull(paramV);
    return super.put(paramK, paramV);
  }
  
  public V put(Map.Entry<K, V> paramEntry) {
    return put(paramEntry.getKey(), paramEntry.getValue());
  }
  
  public void putAll(Map<? extends K, ? extends V> paramMap) {
    ensureMutable();
    checkForNullKeysAndValues(paramMap);
    super.putAll(paramMap);
  }
  
  public V remove(Object paramObject) {
    ensureMutable();
    return super.remove(paramObject);
  }
  
  private static void checkForNullKeysAndValues(Map<?, ?> paramMap) {
    for (Object object : paramMap.keySet()) {
      Internal.checkNotNull(object);
      Internal.checkNotNull(paramMap.get(object));
    } 
  }
  
  private static boolean equals(Object paramObject1, Object paramObject2) {
    if (paramObject1 instanceof byte[] && paramObject2 instanceof byte[])
      return Arrays.equals((byte[])paramObject1, (byte[])paramObject2); 
    return paramObject1.equals(paramObject2);
  }
  
  static <K, V> boolean equals(Map<K, V> paramMap1, Map<K, V> paramMap2) {
    if (paramMap1 == paramMap2)
      return true; 
    if (paramMap1.size() != paramMap2.size())
      return false; 
    for (Map.Entry<K, V> entry : paramMap1.entrySet()) {
      if (!paramMap2.containsKey(entry.getKey()))
        return false; 
      if (!equals(entry.getValue(), paramMap2.get(entry.getKey())))
        return false; 
    } 
    return true;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool;
    if (paramObject instanceof Map && equals(this, (Map<?, ?>)paramObject)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static int calculateHashCodeForObject(Object paramObject) {
    if (paramObject instanceof byte[])
      return Internal.hashCode((byte[])paramObject); 
    if (!(paramObject instanceof Internal.EnumLite))
      return paramObject.hashCode(); 
    throw new UnsupportedOperationException();
  }
  
  static <K, V> int calculateHashCodeForMap(Map<K, V> paramMap) {
    int i = 0;
    for (Map.Entry<K, V> entry : paramMap.entrySet())
      i += calculateHashCodeForObject(entry.getKey()) ^ calculateHashCodeForObject(entry.getValue()); 
    return i;
  }
  
  public int hashCode() {
    return calculateHashCodeForMap(this);
  }
  
  private static Object copy(Object paramObject) {
    if (paramObject instanceof byte[]) {
      paramObject = paramObject;
      return Arrays.copyOf((byte[])paramObject, paramObject.length);
    } 
    return paramObject;
  }
  
  static <K, V> Map<K, V> copy(Map<K, V> paramMap) {
    LinkedHashMap<Object, Object> linkedHashMap = new LinkedHashMap<>();
    for (Map.Entry<K, V> entry : paramMap.entrySet())
      linkedHashMap.put(entry.getKey(), copy(entry.getValue())); 
    return (Map)linkedHashMap;
  }
  
  public MapFieldLite<K, V> mutableCopy() {
    MapFieldLite<K, V> mapFieldLite;
    if (isEmpty()) {
      mapFieldLite = new MapFieldLite();
    } else {
      mapFieldLite = new MapFieldLite(this);
    } 
    return mapFieldLite;
  }
  
  public void makeImmutable() {
    this.isMutable = false;
  }
  
  public boolean isMutable() {
    return this.isMutable;
  }
  
  private void ensureMutable() {
    if (isMutable())
      return; 
    throw new UnsupportedOperationException();
  }
}
