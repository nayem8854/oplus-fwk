package com.android.framework.protobuf;

final class ExtensionRegistryFactory {
  static final Class<?> EXTENSION_REGISTRY_CLASS = reflectExtensionRegistry();
  
  static final String FULL_REGISTRY_CLASS_NAME = "com.android.framework.protobuf.ExtensionRegistry";
  
  static Class<?> reflectExtensionRegistry() {
    try {
      return Class.forName("com.android.framework.protobuf.ExtensionRegistry");
    } catch (ClassNotFoundException classNotFoundException) {
      return null;
    } 
  }
  
  public static ExtensionRegistryLite create() {
    if (EXTENSION_REGISTRY_CLASS != null)
      try {
        return invokeSubclassFactory("newInstance");
      } catch (Exception exception) {} 
    return new ExtensionRegistryLite();
  }
  
  public static ExtensionRegistryLite createEmpty() {
    if (EXTENSION_REGISTRY_CLASS != null)
      try {
        return invokeSubclassFactory("getEmptyRegistry");
      } catch (Exception exception) {} 
    return ExtensionRegistryLite.EMPTY_REGISTRY_LITE;
  }
  
  static boolean isFullRegistry(ExtensionRegistryLite paramExtensionRegistryLite) {
    boolean bool;
    Class<?> clazz = EXTENSION_REGISTRY_CLASS;
    if (clazz != null && 
      clazz.isAssignableFrom(paramExtensionRegistryLite.getClass())) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static final ExtensionRegistryLite invokeSubclassFactory(String paramString) throws Exception {
    Class<?> clazz = EXTENSION_REGISTRY_CLASS;
    return 
      (ExtensionRegistryLite)clazz.getDeclaredMethod(paramString, new Class[0]).invoke(null, new Object[0]);
  }
}
