package com.android.framework.protobuf;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public abstract class ByteString implements Iterable<Byte>, Serializable {
  static final int CONCATENATE_BY_COPY_SIZE = 128;
  
  public static final ByteString EMPTY = new LiteralByteString(Internal.EMPTY_BYTE_ARRAY);
  
  static final int MAX_READ_FROM_CHUNK_SIZE = 8192;
  
  static final int MIN_READ_FROM_CHUNK_SIZE = 256;
  
  private static final int UNSIGNED_BYTE_MASK = 255;
  
  private static final Comparator<ByteString> UNSIGNED_LEXICOGRAPHICAL_COMPARATOR;
  
  private static final ByteArrayCopier byteArrayCopier;
  
  class SystemByteArrayCopier implements ByteArrayCopier {
    private SystemByteArrayCopier() {}
    
    public byte[] copyFrom(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) {
      byte[] arrayOfByte = new byte[param1Int2];
      System.arraycopy(param1ArrayOfbyte, param1Int1, arrayOfByte, 0, param1Int2);
      return arrayOfByte;
    }
  }
  
  class ArraysByteArrayCopier implements ByteArrayCopier {
    private ArraysByteArrayCopier() {}
    
    public byte[] copyFrom(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) {
      return Arrays.copyOfRange(param1ArrayOfbyte, param1Int1, param1Int1 + param1Int2);
    }
  }
  
  static {
    if (Android.isOnAndroidDevice()) {
      SystemByteArrayCopier systemByteArrayCopier = new SystemByteArrayCopier();
    } else {
      arraysByteArrayCopier = new ArraysByteArrayCopier();
    } 
    byteArrayCopier = arraysByteArrayCopier;
    UNSIGNED_LEXICOGRAPHICAL_COMPARATOR = new Comparator<ByteString>() {
        public int compare(ByteString param1ByteString1, ByteString param1ByteString2) {
          ByteString.ByteIterator byteIterator1 = param1ByteString1.iterator();
          ByteString.ByteIterator byteIterator2 = param1ByteString2.iterator();
          while (byteIterator1.hasNext() && byteIterator2.hasNext()) {
            int i = Integer.compare(ByteString.toInt(byteIterator1.nextByte()), ByteString.toInt(byteIterator2.nextByte()));
            if (i != 0)
              return i; 
          } 
          return Integer.compare(param1ByteString1.size(), param1ByteString2.size());
        }
      };
  }
  
  private int hash = 0;
  
  class AbstractByteIterator implements ByteIterator {
    public final Byte next() {
      return Byte.valueOf(nextByte());
    }
    
    public final void remove() {
      throw new UnsupportedOperationException();
    }
  }
  
  private static int toInt(byte paramByte) {
    return paramByte & 0xFF;
  }
  
  public static Comparator<ByteString> unsignedLexicographicalComparator() {
    return UNSIGNED_LEXICOGRAPHICAL_COMPARATOR;
  }
  
  public static ByteString copyFrom(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    checkRange(paramInt1, paramInt1 + paramInt2, paramArrayOfbyte.length);
    return new LiteralByteString(byteArrayCopier.copyFrom(paramArrayOfbyte, paramInt1, paramInt2));
  }
  
  public static ByteString copyFrom(byte[] paramArrayOfbyte) {
    return copyFrom(paramArrayOfbyte, 0, paramArrayOfbyte.length);
  }
  
  static ByteString wrap(ByteBuffer paramByteBuffer) {
    if (paramByteBuffer.hasArray()) {
      int i = paramByteBuffer.arrayOffset();
      return wrap(paramByteBuffer.array(), paramByteBuffer.position() + i, paramByteBuffer.remaining());
    } 
    return new NioByteString(paramByteBuffer);
  }
  
  static ByteString wrap(byte[] paramArrayOfbyte) {
    return new LiteralByteString(paramArrayOfbyte);
  }
  
  static ByteString wrap(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    return new BoundedByteString(paramArrayOfbyte, paramInt1, paramInt2);
  }
  
  public static ByteString copyFrom(ByteBuffer paramByteBuffer, int paramInt) {
    checkRange(0, paramInt, paramByteBuffer.remaining());
    byte[] arrayOfByte = new byte[paramInt];
    paramByteBuffer.get(arrayOfByte);
    return new LiteralByteString(arrayOfByte);
  }
  
  public static ByteString copyFrom(ByteBuffer paramByteBuffer) {
    return copyFrom(paramByteBuffer, paramByteBuffer.remaining());
  }
  
  public static ByteString copyFrom(String paramString1, String paramString2) throws UnsupportedEncodingException {
    return new LiteralByteString(paramString1.getBytes(paramString2));
  }
  
  public static ByteString copyFrom(String paramString, Charset paramCharset) {
    return new LiteralByteString(paramString.getBytes(paramCharset));
  }
  
  public static ByteString copyFromUtf8(String paramString) {
    return new LiteralByteString(paramString.getBytes(Internal.UTF_8));
  }
  
  public static ByteString readFrom(InputStream paramInputStream) throws IOException {
    return readFrom(paramInputStream, 256, 8192);
  }
  
  public static ByteString readFrom(InputStream paramInputStream, int paramInt) throws IOException {
    return readFrom(paramInputStream, paramInt, paramInt);
  }
  
  public static ByteString readFrom(InputStream paramInputStream, int paramInt1, int paramInt2) throws IOException {
    ArrayList<ByteString> arrayList = new ArrayList();
    while (true) {
      ByteString byteString = readChunk(paramInputStream, paramInt1);
      if (byteString == null)
        return copyFrom(arrayList); 
      arrayList.add(byteString);
      paramInt1 = Math.min(paramInt1 * 2, paramInt2);
    } 
  }
  
  private static ByteString readChunk(InputStream paramInputStream, int paramInt) throws IOException {
    byte[] arrayOfByte = new byte[paramInt];
    int i = 0;
    while (i < paramInt) {
      int j = paramInputStream.read(arrayOfByte, i, paramInt - i);
      if (j == -1)
        break; 
      i += j;
    } 
    if (i == 0)
      return null; 
    return copyFrom(arrayOfByte, 0, i);
  }
  
  public final ByteString concat(ByteString paramByteString) {
    if (Integer.MAX_VALUE - size() >= paramByteString.size())
      return RopeByteString.concatenate(this, paramByteString); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ByteString would be too long: ");
    stringBuilder.append(size());
    stringBuilder.append("+");
    stringBuilder.append(paramByteString.size());
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static ByteString copyFrom(Iterable<ByteString> paramIterable) {
    int i;
    if (!(paramIterable instanceof Collection)) {
      i = 0;
      Iterator<ByteString> iterator = paramIterable.iterator();
      while (iterator.hasNext()) {
        iterator.next();
        i++;
      } 
    } else {
      i = ((Collection)paramIterable).size();
    } 
    if (i == 0)
      return EMPTY; 
    return balancedConcat(paramIterable.iterator(), i);
  }
  
  private static ByteString balancedConcat(Iterator<ByteString> paramIterator, int paramInt) {
    if (paramInt >= 1) {
      ByteString byteString;
      if (paramInt == 1) {
        byteString = paramIterator.next();
      } else {
        int i = paramInt >>> 1;
        ByteString byteString1 = balancedConcat((Iterator<ByteString>)byteString, i);
        byteString = balancedConcat((Iterator<ByteString>)byteString, paramInt - i);
        byteString = byteString1.concat(byteString);
      } 
      return byteString;
    } 
    throw new IllegalArgumentException(String.format("length (%s) must be >= 1", new Object[] { Integer.valueOf(paramInt) }));
  }
  
  public void copyTo(byte[] paramArrayOfbyte, int paramInt) {
    copyTo(paramArrayOfbyte, 0, paramInt, size());
  }
  
  @Deprecated
  public final void copyTo(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3) {
    checkRange(paramInt1, paramInt1 + paramInt3, size());
    checkRange(paramInt2, paramInt2 + paramInt3, paramArrayOfbyte.length);
    if (paramInt3 > 0)
      copyToInternal(paramArrayOfbyte, paramInt1, paramInt2, paramInt3); 
  }
  
  class LeafByteString extends ByteString {
    abstract boolean equalsRange(ByteString param1ByteString, int param1Int1, int param1Int2);
    
    protected final int getTreeDepth() {
      return 0;
    }
    
    protected final boolean isBalanced() {
      return true;
    }
    
    void writeToReverse(ByteOutput param1ByteOutput) throws IOException {
      writeTo(param1ByteOutput);
    }
  }
  
  public static Output newOutput(int paramInt) {
    return new Output(paramInt);
  }
  
  public static Output newOutput() {
    return new Output(128);
  }
  
  public static final class Output extends OutputStream {
    private static final byte[] EMPTY_BYTE_ARRAY = new byte[0];
    
    private byte[] buffer;
    
    private int bufferPos;
    
    private final ArrayList<ByteString> flushedBuffers;
    
    private int flushedBuffersTotalBytes;
    
    private final int initialCapacity;
    
    Output(int param1Int) {
      if (param1Int >= 0) {
        this.initialCapacity = param1Int;
        this.flushedBuffers = new ArrayList<>();
        this.buffer = new byte[param1Int];
        return;
      } 
      throw new IllegalArgumentException("Buffer size < 0");
    }
    
    public void write(int param1Int) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield bufferPos : I
      //   6: aload_0
      //   7: getfield buffer : [B
      //   10: arraylength
      //   11: if_icmpne -> 19
      //   14: aload_0
      //   15: iconst_1
      //   16: invokespecial flushFullBuffer : (I)V
      //   19: aload_0
      //   20: getfield buffer : [B
      //   23: astore_2
      //   24: aload_0
      //   25: getfield bufferPos : I
      //   28: istore_3
      //   29: aload_0
      //   30: iload_3
      //   31: iconst_1
      //   32: iadd
      //   33: putfield bufferPos : I
      //   36: aload_2
      //   37: iload_3
      //   38: iload_1
      //   39: i2b
      //   40: bastore
      //   41: aload_0
      //   42: monitorexit
      //   43: return
      //   44: astore_2
      //   45: aload_0
      //   46: monitorexit
      //   47: aload_2
      //   48: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1010	-> 2
      //   #1011	-> 14
      //   #1013	-> 19
      //   #1014	-> 41
      //   #1009	-> 44
      // Exception table:
      //   from	to	target	type
      //   2	14	44	finally
      //   14	19	44	finally
      //   19	36	44	finally
    }
    
    public void write(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: iload_3
      //   3: aload_0
      //   4: getfield buffer : [B
      //   7: arraylength
      //   8: aload_0
      //   9: getfield bufferPos : I
      //   12: isub
      //   13: if_icmpgt -> 43
      //   16: aload_1
      //   17: iload_2
      //   18: aload_0
      //   19: getfield buffer : [B
      //   22: aload_0
      //   23: getfield bufferPos : I
      //   26: iload_3
      //   27: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
      //   30: aload_0
      //   31: aload_0
      //   32: getfield bufferPos : I
      //   35: iload_3
      //   36: iadd
      //   37: putfield bufferPos : I
      //   40: goto -> 99
      //   43: aload_0
      //   44: getfield buffer : [B
      //   47: arraylength
      //   48: aload_0
      //   49: getfield bufferPos : I
      //   52: isub
      //   53: istore #4
      //   55: aload_1
      //   56: iload_2
      //   57: aload_0
      //   58: getfield buffer : [B
      //   61: aload_0
      //   62: getfield bufferPos : I
      //   65: iload #4
      //   67: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
      //   70: iload_3
      //   71: iload #4
      //   73: isub
      //   74: istore_3
      //   75: aload_0
      //   76: iload_3
      //   77: invokespecial flushFullBuffer : (I)V
      //   80: aload_1
      //   81: iload_2
      //   82: iload #4
      //   84: iadd
      //   85: aload_0
      //   86: getfield buffer : [B
      //   89: iconst_0
      //   90: iload_3
      //   91: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
      //   94: aload_0
      //   95: iload_3
      //   96: putfield bufferPos : I
      //   99: aload_0
      //   100: monitorexit
      //   101: return
      //   102: astore_1
      //   103: aload_0
      //   104: monitorexit
      //   105: aload_1
      //   106: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1018	-> 2
      //   #1020	-> 16
      //   #1021	-> 30
      //   #1024	-> 43
      //   #1025	-> 55
      //   #1026	-> 70
      //   #1027	-> 70
      //   #1030	-> 75
      //   #1031	-> 80
      //   #1032	-> 94
      //   #1034	-> 99
      //   #1017	-> 102
      // Exception table:
      //   from	to	target	type
      //   2	16	102	finally
      //   16	30	102	finally
      //   30	40	102	finally
      //   43	55	102	finally
      //   55	70	102	finally
      //   75	80	102	finally
      //   80	94	102	finally
      //   94	99	102	finally
    }
    
    public ByteString toByteString() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: invokespecial flushLastBuffer : ()V
      //   6: aload_0
      //   7: getfield flushedBuffers : Ljava/util/ArrayList;
      //   10: invokestatic copyFrom : (Ljava/lang/Iterable;)Lcom/android/framework/protobuf/ByteString;
      //   13: astore_1
      //   14: aload_0
      //   15: monitorexit
      //   16: aload_1
      //   17: areturn
      //   18: astore_1
      //   19: aload_0
      //   20: monitorexit
      //   21: aload_1
      //   22: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1043	-> 2
      //   #1044	-> 6
      //   #1042	-> 18
      // Exception table:
      //   from	to	target	type
      //   2	6	18	finally
      //   6	14	18	finally
    }
    
    private byte[] copyArray(byte[] param1ArrayOfbyte, int param1Int) {
      byte[] arrayOfByte = new byte[param1Int];
      System.arraycopy(param1ArrayOfbyte, 0, arrayOfByte, 0, Math.min(param1ArrayOfbyte.length, param1Int));
      return arrayOfByte;
    }
    
    public void writeTo(OutputStream param1OutputStream) throws IOException {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield flushedBuffers : Ljava/util/ArrayList;
      //   6: aload_0
      //   7: getfield flushedBuffers : Ljava/util/ArrayList;
      //   10: invokevirtual size : ()I
      //   13: anewarray com/android/framework/protobuf/ByteString
      //   16: invokevirtual toArray : ([Ljava/lang/Object;)[Ljava/lang/Object;
      //   19: checkcast [Lcom/android/framework/protobuf/ByteString;
      //   22: astore_2
      //   23: aload_0
      //   24: getfield buffer : [B
      //   27: astore_3
      //   28: aload_0
      //   29: getfield bufferPos : I
      //   32: istore #4
      //   34: aload_0
      //   35: monitorexit
      //   36: aload_2
      //   37: arraylength
      //   38: istore #5
      //   40: iconst_0
      //   41: istore #6
      //   43: iload #6
      //   45: iload #5
      //   47: if_icmpge -> 68
      //   50: aload_2
      //   51: iload #6
      //   53: aaload
      //   54: astore #7
      //   56: aload #7
      //   58: aload_1
      //   59: invokevirtual writeTo : (Ljava/io/OutputStream;)V
      //   62: iinc #6, 1
      //   65: goto -> 43
      //   68: aload_1
      //   69: aload_0
      //   70: aload_3
      //   71: iload #4
      //   73: invokespecial copyArray : ([BI)[B
      //   76: invokevirtual write : ([B)V
      //   79: return
      //   80: astore_1
      //   81: aload_0
      //   82: monitorexit
      //   83: aload_1
      //   84: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1065	-> 0
      //   #1068	-> 2
      //   #1069	-> 23
      //   #1070	-> 28
      //   #1071	-> 34
      //   #1072	-> 36
      //   #1073	-> 56
      //   #1072	-> 62
      //   #1076	-> 68
      //   #1077	-> 79
      //   #1071	-> 80
      // Exception table:
      //   from	to	target	type
      //   2	23	80	finally
      //   23	28	80	finally
      //   28	34	80	finally
      //   34	36	80	finally
      //   81	83	80	finally
    }
    
    public int size() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield flushedBuffersTotalBytes : I
      //   6: istore_1
      //   7: aload_0
      //   8: getfield bufferPos : I
      //   11: istore_2
      //   12: aload_0
      //   13: monitorexit
      //   14: iload_1
      //   15: iload_2
      //   16: iadd
      //   17: ireturn
      //   18: astore_3
      //   19: aload_0
      //   20: monitorexit
      //   21: aload_3
      //   22: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1085	-> 2
      //   #1085	-> 18
      // Exception table:
      //   from	to	target	type
      //   2	12	18	finally
    }
    
    public void reset() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield flushedBuffers : Ljava/util/ArrayList;
      //   6: invokevirtual clear : ()V
      //   9: aload_0
      //   10: iconst_0
      //   11: putfield flushedBuffersTotalBytes : I
      //   14: aload_0
      //   15: iconst_0
      //   16: putfield bufferPos : I
      //   19: aload_0
      //   20: monitorexit
      //   21: return
      //   22: astore_1
      //   23: aload_0
      //   24: monitorexit
      //   25: aload_1
      //   26: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1093	-> 2
      //   #1094	-> 9
      //   #1095	-> 14
      //   #1096	-> 19
      //   #1092	-> 22
      // Exception table:
      //   from	to	target	type
      //   2	9	22	finally
      //   9	14	22	finally
      //   14	19	22	finally
    }
    
    public String toString() {
      String str = Integer.toHexString(System.identityHashCode(this));
      int i = size();
      return String.format("<ByteString.Output@%s size=%d>", new Object[] { str, Integer.valueOf(i) });
    }
    
    private void flushFullBuffer(int param1Int) {
      this.flushedBuffers.add(new ByteString.LiteralByteString(this.buffer));
      int i = this.flushedBuffersTotalBytes + this.buffer.length;
      param1Int = Math.max(this.initialCapacity, Math.max(param1Int, i >>> 1));
      this.buffer = new byte[param1Int];
      this.bufferPos = 0;
    }
    
    private void flushLastBuffer() {
      int i = this.bufferPos;
      byte[] arrayOfByte = this.buffer;
      if (i < arrayOfByte.length) {
        if (i > 0) {
          arrayOfByte = copyArray(arrayOfByte, i);
          this.flushedBuffers.add(new ByteString.LiteralByteString(arrayOfByte));
        } 
      } else {
        this.flushedBuffers.add(new ByteString.LiteralByteString(this.buffer));
        this.buffer = EMPTY_BYTE_ARRAY;
      } 
      this.flushedBuffersTotalBytes += this.bufferPos;
      this.bufferPos = 0;
    }
  }
  
  static CodedBuilder newCodedBuilder(int paramInt) {
    return new CodedBuilder(paramInt);
  }
  
  static final class CodedBuilder {
    private final byte[] buffer;
    
    private final CodedOutputStream output;
    
    private CodedBuilder(int param1Int) {
      byte[] arrayOfByte = new byte[param1Int];
      this.output = CodedOutputStream.newInstance(arrayOfByte);
    }
    
    public ByteString build() {
      this.output.checkNoSpaceLeft();
      return new ByteString.LiteralByteString(this.buffer);
    }
    
    public CodedOutputStream getCodedOutput() {
      return this.output;
    }
  }
  
  static void checkIndex(int paramInt1, int paramInt2) {
    if ((paramInt2 - paramInt1 + 1 | paramInt1) < 0) {
      if (paramInt1 < 0) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Index < 0: ");
        stringBuilder1.append(paramInt1);
        throw new ArrayIndexOutOfBoundsException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Index > length: ");
      stringBuilder.append(paramInt1);
      stringBuilder.append(", ");
      stringBuilder.append(paramInt2);
      throw new ArrayIndexOutOfBoundsException(stringBuilder.toString());
    } 
  }
  
  static int checkRange(int paramInt1, int paramInt2, int paramInt3) {
    int i = paramInt2 - paramInt1;
    if ((paramInt1 | paramInt2 | i | paramInt3 - paramInt2) < 0) {
      if (paramInt1 >= 0) {
        if (paramInt2 < paramInt1) {
          StringBuilder stringBuilder2 = new StringBuilder();
          stringBuilder2.append("Beginning index larger than ending index: ");
          stringBuilder2.append(paramInt1);
          stringBuilder2.append(", ");
          stringBuilder2.append(paramInt2);
          throw new IndexOutOfBoundsException(stringBuilder2.toString());
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("End index: ");
        stringBuilder1.append(paramInt2);
        stringBuilder1.append(" >= ");
        stringBuilder1.append(paramInt3);
        throw new IndexOutOfBoundsException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Beginning index: ");
      stringBuilder.append(paramInt1);
      stringBuilder.append(" < 0");
      throw new IndexOutOfBoundsException(stringBuilder.toString());
    } 
    return i;
  }
  
  public abstract ByteBuffer asReadOnlyByteBuffer();
  
  public abstract List<ByteBuffer> asReadOnlyByteBufferList();
  
  public abstract byte byteAt(int paramInt);
  
  public abstract void copyTo(ByteBuffer paramByteBuffer);
  
  static {
    ArraysByteArrayCopier arraysByteArrayCopier;
  }
  
  public ByteIterator iterator() {
    return (ByteIterator)new Object(this);
  }
  
  public final boolean isEmpty() {
    boolean bool;
    if (size() == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final ByteString substring(int paramInt) {
    return substring(paramInt, size());
  }
  
  public final boolean startsWith(ByteString paramByteString) {
    int i = size(), j = paramByteString.size();
    boolean bool1 = false, bool2 = bool1;
    if (i >= j) {
      bool2 = bool1;
      if (substring(0, paramByteString.size()).equals(paramByteString))
        bool2 = true; 
    } 
    return bool2;
  }
  
  public final boolean endsWith(ByteString paramByteString) {
    boolean bool;
    if (size() >= paramByteString.size() && substring(size() - paramByteString.size()).equals(paramByteString)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final byte[] toByteArray() {
    int i = size();
    if (i == 0)
      return Internal.EMPTY_BYTE_ARRAY; 
    byte[] arrayOfByte = new byte[i];
    copyToInternal(arrayOfByte, 0, 0, i);
    return arrayOfByte;
  }
  
  final void writeTo(OutputStream paramOutputStream, int paramInt1, int paramInt2) throws IOException {
    checkRange(paramInt1, paramInt1 + paramInt2, size());
    if (paramInt2 > 0)
      writeToInternal(paramOutputStream, paramInt1, paramInt2); 
  }
  
  public final String toString(String paramString) throws UnsupportedEncodingException {
    try {
      return toString(Charset.forName(paramString));
    } catch (UnsupportedCharsetException unsupportedCharsetException) {
      UnsupportedEncodingException unsupportedEncodingException = new UnsupportedEncodingException(paramString);
      unsupportedEncodingException.initCause(unsupportedCharsetException);
      throw unsupportedEncodingException;
    } 
  }
  
  public final String toString(Charset paramCharset) {
    String str;
    if (size() == 0) {
      str = "";
    } else {
      str = toStringInternal((Charset)str);
    } 
    return str;
  }
  
  public final String toStringUtf8() {
    return toString(Internal.UTF_8);
  }
  
  public final int hashCode() {
    int i = this.hash;
    int j = i;
    if (i == 0) {
      j = size();
      i = partialHash(j, 0, j);
      j = i;
      if (i == 0)
        j = 1; 
      this.hash = j;
    } 
    return j;
  }
  
  protected final int peekCachedHashCode() {
    return this.hash;
  }
  
  public final String toString() {
    String str = Integer.toHexString(System.identityHashCode(this));
    int i = size();
    return String.format("<ByteString@%s size=%d>", new Object[] { str, Integer.valueOf(i) });
  }
  
  protected abstract void copyToInternal(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3);
  
  public abstract boolean equals(Object paramObject);
  
  protected abstract int getTreeDepth();
  
  abstract byte internalByteAt(int paramInt);
  
  protected abstract boolean isBalanced();
  
  public abstract boolean isValidUtf8();
  
  public abstract CodedInputStream newCodedInput();
  
  public abstract InputStream newInput();
  
  protected abstract int partialHash(int paramInt1, int paramInt2, int paramInt3);
  
  protected abstract int partialIsValidUtf8(int paramInt1, int paramInt2, int paramInt3);
  
  public abstract int size();
  
  public abstract ByteString substring(int paramInt1, int paramInt2);
  
  protected abstract String toStringInternal(Charset paramCharset);
  
  abstract void writeTo(ByteOutput paramByteOutput) throws IOException;
  
  public abstract void writeTo(OutputStream paramOutputStream) throws IOException;
  
  abstract void writeToInternal(OutputStream paramOutputStream, int paramInt1, int paramInt2) throws IOException;
  
  abstract void writeToReverse(ByteOutput paramByteOutput) throws IOException;
  
  class LiteralByteString extends LeafByteString {
    private static final long serialVersionUID = 1L;
    
    protected final byte[] bytes;
    
    LiteralByteString(ByteString this$0) {
      if (this$0 != null) {
        this.bytes = (byte[])this$0;
        return;
      } 
      throw null;
    }
    
    public byte byteAt(int param1Int) {
      return this.bytes[param1Int];
    }
    
    byte internalByteAt(int param1Int) {
      return this.bytes[param1Int];
    }
    
    public int size() {
      return this.bytes.length;
    }
    
    public final ByteString substring(int param1Int1, int param1Int2) {
      param1Int2 = checkRange(param1Int1, param1Int2, size());
      if (param1Int2 == 0)
        return ByteString.EMPTY; 
      return new ByteString.BoundedByteString(this.bytes, getOffsetIntoBytes() + param1Int1, param1Int2);
    }
    
    protected void copyToInternal(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2, int param1Int3) {
      System.arraycopy(this.bytes, param1Int1, param1ArrayOfbyte, param1Int2, param1Int3);
    }
    
    public final void copyTo(ByteBuffer param1ByteBuffer) {
      param1ByteBuffer.put(this.bytes, getOffsetIntoBytes(), size());
    }
    
    public final ByteBuffer asReadOnlyByteBuffer() {
      return ByteBuffer.wrap(this.bytes, getOffsetIntoBytes(), size()).asReadOnlyBuffer();
    }
    
    public final List<ByteBuffer> asReadOnlyByteBufferList() {
      return Collections.singletonList(asReadOnlyByteBuffer());
    }
    
    public final void writeTo(OutputStream param1OutputStream) throws IOException {
      param1OutputStream.write(toByteArray());
    }
    
    final void writeToInternal(OutputStream param1OutputStream, int param1Int1, int param1Int2) throws IOException {
      param1OutputStream.write(this.bytes, getOffsetIntoBytes() + param1Int1, param1Int2);
    }
    
    final void writeTo(ByteOutput param1ByteOutput) throws IOException {
      param1ByteOutput.writeLazy(this.bytes, getOffsetIntoBytes(), size());
    }
    
    protected final String toStringInternal(Charset param1Charset) {
      return new String(this.bytes, getOffsetIntoBytes(), size(), param1Charset);
    }
    
    public final boolean isValidUtf8() {
      int i = getOffsetIntoBytes();
      return Utf8.isValidUtf8(this.bytes, i, size() + i);
    }
    
    protected final int partialIsValidUtf8(int param1Int1, int param1Int2, int param1Int3) {
      param1Int2 = getOffsetIntoBytes() + param1Int2;
      return Utf8.partialIsValidUtf8(param1Int1, this.bytes, param1Int2, param1Int2 + param1Int3);
    }
    
    public final boolean equals(Object param1Object) {
      if (param1Object == this)
        return true; 
      if (!(param1Object instanceof ByteString))
        return false; 
      if (size() != ((ByteString)param1Object).size())
        return false; 
      if (size() == 0)
        return true; 
      if (param1Object instanceof LiteralByteString) {
        LiteralByteString literalByteString = (LiteralByteString)param1Object;
        int i = peekCachedHashCode();
        int j = literalByteString.peekCachedHashCode();
        if (i != 0 && j != 0 && i != j)
          return false; 
        return equalsRange((LiteralByteString)param1Object, 0, size());
      } 
      return param1Object.equals(this);
    }
    
    final boolean equalsRange(ByteString param1ByteString, int param1Int1, int param1Int2) {
      if (param1Int2 <= param1ByteString.size()) {
        byte[] arrayOfByte;
        if (param1Int1 + param1Int2 <= param1ByteString.size()) {
          if (param1ByteString instanceof LiteralByteString) {
            LiteralByteString literalByteString = (LiteralByteString)param1ByteString;
            arrayOfByte = this.bytes;
            byte[] arrayOfByte1 = literalByteString.bytes;
            int i = getOffsetIntoBytes();
            int j = getOffsetIntoBytes();
            param1Int1 = literalByteString.getOffsetIntoBytes() + param1Int1;
            for (; j < i + param1Int2; 
              j++, param1Int1++) {
              if (arrayOfByte[j] != arrayOfByte1[param1Int1])
                return false; 
            } 
            return true;
          } 
          return arrayOfByte.substring(param1Int1, param1Int1 + param1Int2).equals(substring(0, param1Int2));
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Ran off end of other: ");
        stringBuilder1.append(param1Int1);
        stringBuilder1.append(", ");
        stringBuilder1.append(param1Int2);
        stringBuilder1.append(", ");
        stringBuilder1.append(arrayOfByte.size());
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Length too large: ");
      stringBuilder.append(param1Int2);
      stringBuilder.append(size());
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    protected final int partialHash(int param1Int1, int param1Int2, int param1Int3) {
      return Internal.partialHash(param1Int1, this.bytes, getOffsetIntoBytes() + param1Int2, param1Int3);
    }
    
    public final InputStream newInput() {
      return new ByteArrayInputStream(this.bytes, getOffsetIntoBytes(), size());
    }
    
    public final CodedInputStream newCodedInput() {
      byte[] arrayOfByte = this.bytes;
      int i = getOffsetIntoBytes(), j = size();
      return CodedInputStream.newInstance(arrayOfByte, i, j, true);
    }
    
    protected int getOffsetIntoBytes() {
      return 0;
    }
  }
  
  class BoundedByteString extends LiteralByteString {
    private static final long serialVersionUID = 1L;
    
    private final int bytesLength;
    
    private final int bytesOffset;
    
    BoundedByteString(ByteString this$0, int param1Int1, int param1Int2) {
      super((byte[])this$0);
      checkRange(param1Int1, param1Int1 + param1Int2, this$0.length);
      this.bytesOffset = param1Int1;
      this.bytesLength = param1Int2;
    }
    
    public byte byteAt(int param1Int) {
      checkIndex(param1Int, size());
      return this.bytes[this.bytesOffset + param1Int];
    }
    
    byte internalByteAt(int param1Int) {
      return this.bytes[this.bytesOffset + param1Int];
    }
    
    public int size() {
      return this.bytesLength;
    }
    
    protected int getOffsetIntoBytes() {
      return this.bytesOffset;
    }
    
    protected void copyToInternal(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2, int param1Int3) {
      byte[] arrayOfByte = this.bytes;
      int i = getOffsetIntoBytes();
      System.arraycopy(arrayOfByte, i + param1Int1, param1ArrayOfbyte, param1Int2, param1Int3);
    }
    
    Object writeReplace() {
      return ByteString.wrap(toByteArray());
    }
    
    private void readObject(ObjectInputStream param1ObjectInputStream) throws IOException {
      throw new InvalidObjectException("BoundedByteStream instances are not to be serialized directly");
    }
  }
  
  private static interface ByteArrayCopier {
    byte[] copyFrom(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2);
  }
  
  public static interface ByteIterator extends Iterator<Byte> {
    byte nextByte();
  }
}
