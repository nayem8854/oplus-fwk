package com.android.framework.protobuf;

abstract class BufferAllocator {
  private static final BufferAllocator UNPOOLED = (BufferAllocator)new Object();
  
  public static BufferAllocator unpooled() {
    return UNPOOLED;
  }
  
  public abstract AllocatedBuffer allocateDirectBuffer(int paramInt);
  
  public abstract AllocatedBuffer allocateHeapBuffer(int paramInt);
}
