package com.android.framework.protobuf.nano;

import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ReadOnlyBufferException;

public final class CodedOutputByteBufferNano {
  public static final int LITTLE_ENDIAN_32_SIZE = 4;
  
  public static final int LITTLE_ENDIAN_64_SIZE = 8;
  
  private static final int MAX_UTF8_EXPANSION = 3;
  
  private final ByteBuffer buffer;
  
  private CodedOutputByteBufferNano(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    this(ByteBuffer.wrap(paramArrayOfbyte, paramInt1, paramInt2));
  }
  
  private CodedOutputByteBufferNano(ByteBuffer paramByteBuffer) {
    this.buffer = paramByteBuffer;
    paramByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
  }
  
  public static CodedOutputByteBufferNano newInstance(byte[] paramArrayOfbyte) {
    return newInstance(paramArrayOfbyte, 0, paramArrayOfbyte.length);
  }
  
  public static CodedOutputByteBufferNano newInstance(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    return new CodedOutputByteBufferNano(paramArrayOfbyte, paramInt1, paramInt2);
  }
  
  public void writeDouble(int paramInt, double paramDouble) throws IOException {
    writeTag(paramInt, 1);
    writeDoubleNoTag(paramDouble);
  }
  
  public void writeFloat(int paramInt, float paramFloat) throws IOException {
    writeTag(paramInt, 5);
    writeFloatNoTag(paramFloat);
  }
  
  public void writeUInt64(int paramInt, long paramLong) throws IOException {
    writeTag(paramInt, 0);
    writeUInt64NoTag(paramLong);
  }
  
  public void writeInt64(int paramInt, long paramLong) throws IOException {
    writeTag(paramInt, 0);
    writeInt64NoTag(paramLong);
  }
  
  public void writeInt32(int paramInt1, int paramInt2) throws IOException {
    writeTag(paramInt1, 0);
    writeInt32NoTag(paramInt2);
  }
  
  public void writeFixed64(int paramInt, long paramLong) throws IOException {
    writeTag(paramInt, 1);
    writeFixed64NoTag(paramLong);
  }
  
  public void writeFixed32(int paramInt1, int paramInt2) throws IOException {
    writeTag(paramInt1, 5);
    writeFixed32NoTag(paramInt2);
  }
  
  public void writeBool(int paramInt, boolean paramBoolean) throws IOException {
    writeTag(paramInt, 0);
    writeBoolNoTag(paramBoolean);
  }
  
  public void writeString(int paramInt, String paramString) throws IOException {
    writeTag(paramInt, 2);
    writeStringNoTag(paramString);
  }
  
  public void writeGroup(int paramInt, MessageNano paramMessageNano) throws IOException {
    writeTag(paramInt, 3);
    writeGroupNoTag(paramMessageNano);
    writeTag(paramInt, 4);
  }
  
  public void writeMessage(int paramInt, MessageNano paramMessageNano) throws IOException {
    writeTag(paramInt, 2);
    writeMessageNoTag(paramMessageNano);
  }
  
  public void writeBytes(int paramInt, byte[] paramArrayOfbyte) throws IOException {
    writeTag(paramInt, 2);
    writeBytesNoTag(paramArrayOfbyte);
  }
  
  public void writeUInt32(int paramInt1, int paramInt2) throws IOException {
    writeTag(paramInt1, 0);
    writeUInt32NoTag(paramInt2);
  }
  
  public void writeEnum(int paramInt1, int paramInt2) throws IOException {
    writeTag(paramInt1, 0);
    writeEnumNoTag(paramInt2);
  }
  
  public void writeSFixed32(int paramInt1, int paramInt2) throws IOException {
    writeTag(paramInt1, 5);
    writeSFixed32NoTag(paramInt2);
  }
  
  public void writeSFixed64(int paramInt, long paramLong) throws IOException {
    writeTag(paramInt, 1);
    writeSFixed64NoTag(paramLong);
  }
  
  public void writeSInt32(int paramInt1, int paramInt2) throws IOException {
    writeTag(paramInt1, 0);
    writeSInt32NoTag(paramInt2);
  }
  
  public void writeSInt64(int paramInt, long paramLong) throws IOException {
    writeTag(paramInt, 0);
    writeSInt64NoTag(paramLong);
  }
  
  public void writeDoubleNoTag(double paramDouble) throws IOException {
    writeRawLittleEndian64(Double.doubleToLongBits(paramDouble));
  }
  
  public void writeFloatNoTag(float paramFloat) throws IOException {
    writeRawLittleEndian32(Float.floatToIntBits(paramFloat));
  }
  
  public void writeUInt64NoTag(long paramLong) throws IOException {
    writeRawVarint64(paramLong);
  }
  
  public void writeInt64NoTag(long paramLong) throws IOException {
    writeRawVarint64(paramLong);
  }
  
  public void writeInt32NoTag(int paramInt) throws IOException {
    if (paramInt >= 0) {
      writeRawVarint32(paramInt);
    } else {
      writeRawVarint64(paramInt);
    } 
  }
  
  public void writeFixed64NoTag(long paramLong) throws IOException {
    writeRawLittleEndian64(paramLong);
  }
  
  public void writeFixed32NoTag(int paramInt) throws IOException {
    writeRawLittleEndian32(paramInt);
  }
  
  public void writeBoolNoTag(boolean paramBoolean) throws IOException {
    writeRawByte(paramBoolean);
  }
  
  public void writeStringNoTag(String paramString) throws IOException {
    try {
      OutOfSpaceException outOfSpaceException;
      int i = computeRawVarint32Size(paramString.length());
      int j = computeRawVarint32Size(paramString.length() * 3);
      if (i == j) {
        int k = this.buffer.position();
        if (this.buffer.remaining() >= i) {
          this.buffer.position(k + i);
          encode(paramString, this.buffer);
          j = this.buffer.position();
          this.buffer.position(k);
          writeRawVarint32(j - k - i);
          this.buffer.position(j);
        } else {
          outOfSpaceException = new OutOfSpaceException();
          this(k + i, this.buffer.limit());
          throw outOfSpaceException;
        } 
      } else {
        writeRawVarint32(encodedLength((CharSequence)outOfSpaceException));
        encode((CharSequence)outOfSpaceException, this.buffer);
      } 
      return;
    } catch (BufferOverflowException bufferOverflowException) {
      int i = this.buffer.position();
      ByteBuffer byteBuffer = this.buffer;
      OutOfSpaceException outOfSpaceException = new OutOfSpaceException(i, byteBuffer.limit());
      outOfSpaceException.initCause(bufferOverflowException);
      throw outOfSpaceException;
    } 
  }
  
  private static int encodedLength(CharSequence paramCharSequence) {
    int m, n, i = paramCharSequence.length();
    int j = i;
    int k = 0;
    while (true) {
      m = j;
      n = k;
      if (k < i) {
        m = j;
        n = k;
        if (paramCharSequence.charAt(k) < '') {
          k++;
          continue;
        } 
      } 
      break;
    } 
    while (true) {
      k = m;
      if (n < i) {
        k = paramCharSequence.charAt(n);
        if (k < 2048) {
          m += 127 - k >>> 31;
          n++;
        } 
        k = m + encodedLengthGeneral(paramCharSequence, n);
      } 
      break;
    } 
    if (k >= i)
      return k; 
    paramCharSequence = new StringBuilder();
    paramCharSequence.append("UTF-8 length does not fit in int: ");
    paramCharSequence.append(k + 4294967296L);
    throw new IllegalArgumentException(paramCharSequence.toString());
  }
  
  private static int encodedLengthGeneral(CharSequence paramCharSequence, int paramInt) {
    int i = paramCharSequence.length();
    int j = 0;
    for (; paramInt < i; paramInt = k + 1) {
      int k;
      char c = paramCharSequence.charAt(paramInt);
      if (c < 'ࠀ') {
        j += 127 - c >>> 31;
        k = paramInt;
      } else {
        int m = j + 2;
        j = m;
        k = paramInt;
        if ('?' <= c) {
          j = m;
          k = paramInt;
          if (c <= '?') {
            j = Character.codePointAt(paramCharSequence, paramInt);
            if (j >= 65536) {
              k = paramInt + 1;
              j = m;
            } else {
              paramCharSequence = new StringBuilder();
              paramCharSequence.append("Unpaired surrogate at index ");
              paramCharSequence.append(paramInt);
              throw new IllegalArgumentException(paramCharSequence.toString());
            } 
          } 
        } 
      } 
    } 
    return j;
  }
  
  private static void encode(CharSequence paramCharSequence, ByteBuffer paramByteBuffer) {
    if (!paramByteBuffer.isReadOnly()) {
      BufferOverflowException bufferOverflowException;
      if (paramByteBuffer.hasArray()) {
        try {
          byte[] arrayOfByte = paramByteBuffer.array();
          int i = paramByteBuffer.arrayOffset(), j = paramByteBuffer.position();
          int k = paramByteBuffer.remaining();
          i = encode(paramCharSequence, arrayOfByte, i + j, k);
          paramByteBuffer.position(i - paramByteBuffer.arrayOffset());
        } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) {
          bufferOverflowException = new BufferOverflowException();
          bufferOverflowException.initCause(arrayIndexOutOfBoundsException);
          throw bufferOverflowException;
        } 
      } else {
        encodeDirect((CharSequence)bufferOverflowException, (ByteBuffer)arrayIndexOutOfBoundsException);
      } 
      return;
    } 
    throw new ReadOnlyBufferException();
  }
  
  private static void encodeDirect(CharSequence paramCharSequence, ByteBuffer paramByteBuffer) {
    int i = paramCharSequence.length();
    for (byte b = 0; b < i; b++) {
      char c = paramCharSequence.charAt(b);
      if (c < '') {
        paramByteBuffer.put((byte)c);
      } else if (c < 'ࠀ') {
        paramByteBuffer.put((byte)(c >>> 6 | 0x3C0));
        paramByteBuffer.put((byte)(0x80 | c & 0x3F));
      } else if (c < '?' || '?' < c) {
        paramByteBuffer.put((byte)(c >>> 12 | 0x1E0));
        paramByteBuffer.put((byte)(c >>> 6 & 0x3F | 0x80));
        paramByteBuffer.put((byte)(0x80 | c & 0x3F));
      } else {
        int j = b;
        if (b + 1 != paramCharSequence.length()) {
          b++;
          char c1 = paramCharSequence.charAt(b);
          j = b;
          if (Character.isSurrogatePair(c, c1)) {
            j = Character.toCodePoint(c, c1);
            paramByteBuffer.put((byte)(j >>> 18 | 0xF0));
            paramByteBuffer.put((byte)(j >>> 12 & 0x3F | 0x80));
            paramByteBuffer.put((byte)(j >>> 6 & 0x3F | 0x80));
            paramByteBuffer.put((byte)(0x80 | j & 0x3F));
          } else {
            paramCharSequence = new StringBuilder();
            paramCharSequence.append("Unpaired surrogate at index ");
            paramCharSequence.append(j - 1);
            throw new IllegalArgumentException(paramCharSequence.toString());
          } 
        } else {
          paramCharSequence = new StringBuilder();
          paramCharSequence.append("Unpaired surrogate at index ");
          paramCharSequence.append(j - 1);
          throw new IllegalArgumentException(paramCharSequence.toString());
        } 
      } 
    } 
  }
  
  private static int encode(CharSequence paramCharSequence, byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    int i = paramCharSequence.length();
    int j = 0;
    int k = paramInt1 + paramInt2;
    paramInt2 = j;
    while (paramInt2 < i && paramInt2 + paramInt1 < k) {
      j = paramCharSequence.charAt(paramInt2);
      if (j < 128) {
        paramArrayOfbyte[paramInt1 + paramInt2] = (byte)j;
        paramInt2++;
      } 
    } 
    if (paramInt2 == i)
      return paramInt1 + i; 
    paramInt1 += paramInt2;
    for (; paramInt2 < i; paramInt2++) {
      char c = paramCharSequence.charAt(paramInt2);
      if (c < '' && paramInt1 < k) {
        paramArrayOfbyte[paramInt1] = (byte)c;
        paramInt1++;
      } else if (c < 'ࠀ' && paramInt1 <= k - 2) {
        j = paramInt1 + 1;
        paramArrayOfbyte[paramInt1] = (byte)(c >>> 6 | 0x3C0);
        paramInt1 = j + 1;
        paramArrayOfbyte[j] = (byte)(c & 0x3F | 0x80);
      } else if ((c < '?' || '?' < c) && paramInt1 <= k - 3) {
        j = paramInt1 + 1;
        paramArrayOfbyte[paramInt1] = (byte)(c >>> 12 | 0x1E0);
        paramInt1 = j + 1;
        paramArrayOfbyte[j] = (byte)(c >>> 6 & 0x3F | 0x80);
        paramArrayOfbyte[paramInt1] = (byte)(c & 0x3F | 0x80);
        paramInt1++;
      } else if (paramInt1 <= k - 4) {
        j = paramInt2;
        if (paramInt2 + 1 != paramCharSequence.length()) {
          paramInt2++;
          char c1 = paramCharSequence.charAt(paramInt2);
          j = paramInt2;
          if (Character.isSurrogatePair(c, c1)) {
            j = Character.toCodePoint(c, c1);
            int m = paramInt1 + 1;
            paramArrayOfbyte[paramInt1] = (byte)(j >>> 18 | 0xF0);
            paramInt1 = m + 1;
            paramArrayOfbyte[m] = (byte)(j >>> 12 & 0x3F | 0x80);
            m = paramInt1 + 1;
            paramArrayOfbyte[paramInt1] = (byte)(j >>> 6 & 0x3F | 0x80);
            paramInt1 = m + 1;
            paramArrayOfbyte[m] = (byte)(j & 0x3F | 0x80);
          } else {
            paramCharSequence = new StringBuilder();
            paramCharSequence.append("Unpaired surrogate at index ");
            paramCharSequence.append(j - 1);
            throw new IllegalArgumentException(paramCharSequence.toString());
          } 
        } else {
          paramCharSequence = new StringBuilder();
          paramCharSequence.append("Unpaired surrogate at index ");
          paramCharSequence.append(j - 1);
          throw new IllegalArgumentException(paramCharSequence.toString());
        } 
      } else {
        paramCharSequence = new StringBuilder();
        paramCharSequence.append("Failed writing ");
        paramCharSequence.append(c);
        paramCharSequence.append(" at index ");
        paramCharSequence.append(paramInt1);
        throw new ArrayIndexOutOfBoundsException(paramCharSequence.toString());
      } 
    } 
    return paramInt1;
  }
  
  public void writeGroupNoTag(MessageNano paramMessageNano) throws IOException {
    paramMessageNano.writeTo(this);
  }
  
  public void writeMessageNoTag(MessageNano paramMessageNano) throws IOException {
    writeRawVarint32(paramMessageNano.getCachedSize());
    paramMessageNano.writeTo(this);
  }
  
  public void writeBytesNoTag(byte[] paramArrayOfbyte) throws IOException {
    writeRawVarint32(paramArrayOfbyte.length);
    writeRawBytes(paramArrayOfbyte);
  }
  
  public void writeUInt32NoTag(int paramInt) throws IOException {
    writeRawVarint32(paramInt);
  }
  
  public void writeEnumNoTag(int paramInt) throws IOException {
    writeRawVarint32(paramInt);
  }
  
  public void writeSFixed32NoTag(int paramInt) throws IOException {
    writeRawLittleEndian32(paramInt);
  }
  
  public void writeSFixed64NoTag(long paramLong) throws IOException {
    writeRawLittleEndian64(paramLong);
  }
  
  public void writeSInt32NoTag(int paramInt) throws IOException {
    writeRawVarint32(encodeZigZag32(paramInt));
  }
  
  public void writeSInt64NoTag(long paramLong) throws IOException {
    writeRawVarint64(encodeZigZag64(paramLong));
  }
  
  public static int computeDoubleSize(int paramInt, double paramDouble) {
    return computeTagSize(paramInt) + computeDoubleSizeNoTag(paramDouble);
  }
  
  public static int computeFloatSize(int paramInt, float paramFloat) {
    return computeTagSize(paramInt) + computeFloatSizeNoTag(paramFloat);
  }
  
  public static int computeUInt64Size(int paramInt, long paramLong) {
    return computeTagSize(paramInt) + computeUInt64SizeNoTag(paramLong);
  }
  
  public static int computeInt64Size(int paramInt, long paramLong) {
    return computeTagSize(paramInt) + computeInt64SizeNoTag(paramLong);
  }
  
  public static int computeInt32Size(int paramInt1, int paramInt2) {
    return computeTagSize(paramInt1) + computeInt32SizeNoTag(paramInt2);
  }
  
  public static int computeFixed64Size(int paramInt, long paramLong) {
    return computeTagSize(paramInt) + computeFixed64SizeNoTag(paramLong);
  }
  
  public static int computeFixed32Size(int paramInt1, int paramInt2) {
    return computeTagSize(paramInt1) + computeFixed32SizeNoTag(paramInt2);
  }
  
  public static int computeBoolSize(int paramInt, boolean paramBoolean) {
    return computeTagSize(paramInt) + computeBoolSizeNoTag(paramBoolean);
  }
  
  public static int computeStringSize(int paramInt, String paramString) {
    return computeTagSize(paramInt) + computeStringSizeNoTag(paramString);
  }
  
  public static int computeGroupSize(int paramInt, MessageNano paramMessageNano) {
    return computeTagSize(paramInt) * 2 + computeGroupSizeNoTag(paramMessageNano);
  }
  
  public static int computeMessageSize(int paramInt, MessageNano paramMessageNano) {
    return computeTagSize(paramInt) + computeMessageSizeNoTag(paramMessageNano);
  }
  
  public static int computeBytesSize(int paramInt, byte[] paramArrayOfbyte) {
    return computeTagSize(paramInt) + computeBytesSizeNoTag(paramArrayOfbyte);
  }
  
  public static int computeUInt32Size(int paramInt1, int paramInt2) {
    return computeTagSize(paramInt1) + computeUInt32SizeNoTag(paramInt2);
  }
  
  public static int computeEnumSize(int paramInt1, int paramInt2) {
    return computeTagSize(paramInt1) + computeEnumSizeNoTag(paramInt2);
  }
  
  public static int computeSFixed32Size(int paramInt1, int paramInt2) {
    return computeTagSize(paramInt1) + computeSFixed32SizeNoTag(paramInt2);
  }
  
  public static int computeSFixed64Size(int paramInt, long paramLong) {
    return computeTagSize(paramInt) + computeSFixed64SizeNoTag(paramLong);
  }
  
  public static int computeSInt32Size(int paramInt1, int paramInt2) {
    return computeTagSize(paramInt1) + computeSInt32SizeNoTag(paramInt2);
  }
  
  public static int computeSInt64Size(int paramInt, long paramLong) {
    return computeTagSize(paramInt) + computeSInt64SizeNoTag(paramLong);
  }
  
  public static int computeDoubleSizeNoTag(double paramDouble) {
    return 8;
  }
  
  public static int computeFloatSizeNoTag(float paramFloat) {
    return 4;
  }
  
  public static int computeUInt64SizeNoTag(long paramLong) {
    return computeRawVarint64Size(paramLong);
  }
  
  public static int computeInt64SizeNoTag(long paramLong) {
    return computeRawVarint64Size(paramLong);
  }
  
  public static int computeInt32SizeNoTag(int paramInt) {
    if (paramInt >= 0)
      return computeRawVarint32Size(paramInt); 
    return 10;
  }
  
  public static int computeFixed64SizeNoTag(long paramLong) {
    return 8;
  }
  
  public static int computeFixed32SizeNoTag(int paramInt) {
    return 4;
  }
  
  public static int computeBoolSizeNoTag(boolean paramBoolean) {
    return 1;
  }
  
  public static int computeStringSizeNoTag(String paramString) {
    int i = encodedLength(paramString);
    return computeRawVarint32Size(i) + i;
  }
  
  public static int computeGroupSizeNoTag(MessageNano paramMessageNano) {
    return paramMessageNano.getSerializedSize();
  }
  
  public static int computeMessageSizeNoTag(MessageNano paramMessageNano) {
    int i = paramMessageNano.getSerializedSize();
    return computeRawVarint32Size(i) + i;
  }
  
  public static int computeBytesSizeNoTag(byte[] paramArrayOfbyte) {
    return computeRawVarint32Size(paramArrayOfbyte.length) + paramArrayOfbyte.length;
  }
  
  public static int computeUInt32SizeNoTag(int paramInt) {
    return computeRawVarint32Size(paramInt);
  }
  
  public static int computeEnumSizeNoTag(int paramInt) {
    return computeRawVarint32Size(paramInt);
  }
  
  public static int computeSFixed32SizeNoTag(int paramInt) {
    return 4;
  }
  
  public static int computeSFixed64SizeNoTag(long paramLong) {
    return 8;
  }
  
  public static int computeSInt32SizeNoTag(int paramInt) {
    return computeRawVarint32Size(encodeZigZag32(paramInt));
  }
  
  public static int computeSInt64SizeNoTag(long paramLong) {
    return computeRawVarint64Size(encodeZigZag64(paramLong));
  }
  
  public int spaceLeft() {
    return this.buffer.remaining();
  }
  
  public void checkNoSpaceLeft() {
    if (spaceLeft() == 0)
      return; 
    throw new IllegalStateException("Did not write as much data as expected.");
  }
  
  public int position() {
    return this.buffer.position();
  }
  
  public void reset() {
    this.buffer.clear();
  }
  
  public static class OutOfSpaceException extends IOException {
    private static final long serialVersionUID = -6947486886997889499L;
    
    OutOfSpaceException(int param1Int1, int param1Int2) {
      super(stringBuilder.toString());
    }
  }
  
  public void writeRawByte(byte paramByte) throws IOException {
    if (this.buffer.hasRemaining()) {
      this.buffer.put(paramByte);
      return;
    } 
    throw new OutOfSpaceException(this.buffer.position(), this.buffer.limit());
  }
  
  public void writeRawByte(int paramInt) throws IOException {
    writeRawByte((byte)paramInt);
  }
  
  public void writeRawBytes(byte[] paramArrayOfbyte) throws IOException {
    writeRawBytes(paramArrayOfbyte, 0, paramArrayOfbyte.length);
  }
  
  public void writeRawBytes(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws IOException {
    if (this.buffer.remaining() >= paramInt2) {
      this.buffer.put(paramArrayOfbyte, paramInt1, paramInt2);
      return;
    } 
    throw new OutOfSpaceException(this.buffer.position(), this.buffer.limit());
  }
  
  public void writeTag(int paramInt1, int paramInt2) throws IOException {
    writeRawVarint32(WireFormatNano.makeTag(paramInt1, paramInt2));
  }
  
  public static int computeTagSize(int paramInt) {
    return computeRawVarint32Size(WireFormatNano.makeTag(paramInt, 0));
  }
  
  public void writeRawVarint32(int paramInt) throws IOException {
    while (true) {
      if ((paramInt & 0xFFFFFF80) == 0) {
        writeRawByte(paramInt);
        return;
      } 
      writeRawByte(paramInt & 0x7F | 0x80);
      paramInt >>>= 7;
    } 
  }
  
  public static int computeRawVarint32Size(int paramInt) {
    if ((paramInt & 0xFFFFFF80) == 0)
      return 1; 
    if ((paramInt & 0xFFFFC000) == 0)
      return 2; 
    if ((0xFFE00000 & paramInt) == 0)
      return 3; 
    if ((0xF0000000 & paramInt) == 0)
      return 4; 
    return 5;
  }
  
  public void writeRawVarint64(long paramLong) throws IOException {
    while (true) {
      if ((0xFFFFFFFFFFFFFF80L & paramLong) == 0L) {
        writeRawByte((int)paramLong);
        return;
      } 
      writeRawByte((int)paramLong & 0x7F | 0x80);
      paramLong >>>= 7L;
    } 
  }
  
  public static int computeRawVarint64Size(long paramLong) {
    if ((0xFFFFFFFFFFFFFF80L & paramLong) == 0L)
      return 1; 
    if ((0xFFFFFFFFFFFFC000L & paramLong) == 0L)
      return 2; 
    if ((0xFFFFFFFFFFE00000L & paramLong) == 0L)
      return 3; 
    if ((0xFFFFFFFFF0000000L & paramLong) == 0L)
      return 4; 
    if ((0xFFFFFFF800000000L & paramLong) == 0L)
      return 5; 
    if ((0xFFFFFC0000000000L & paramLong) == 0L)
      return 6; 
    if ((0xFFFE000000000000L & paramLong) == 0L)
      return 7; 
    if ((0xFF00000000000000L & paramLong) == 0L)
      return 8; 
    if ((Long.MIN_VALUE & paramLong) == 0L)
      return 9; 
    return 10;
  }
  
  public void writeRawLittleEndian32(int paramInt) throws IOException {
    if (this.buffer.remaining() >= 4) {
      this.buffer.putInt(paramInt);
      return;
    } 
    throw new OutOfSpaceException(this.buffer.position(), this.buffer.limit());
  }
  
  public void writeRawLittleEndian64(long paramLong) throws IOException {
    if (this.buffer.remaining() >= 8) {
      this.buffer.putLong(paramLong);
      return;
    } 
    throw new OutOfSpaceException(this.buffer.position(), this.buffer.limit());
  }
  
  public static int encodeZigZag32(int paramInt) {
    return paramInt << 1 ^ paramInt >> 31;
  }
  
  public static long encodeZigZag64(long paramLong) {
    return paramLong << 1L ^ paramLong >> 63L;
  }
  
  static int computeFieldSize(int paramInt1, int paramInt2, Object paramObject) {
    switch (paramInt2) {
      default:
        paramObject = new StringBuilder();
        paramObject.append("Unknown type: ");
        paramObject.append(paramInt2);
        throw new IllegalArgumentException(paramObject.toString());
      case 18:
        return computeSInt64Size(paramInt1, ((Long)paramObject).longValue());
      case 17:
        return computeSInt32Size(paramInt1, ((Integer)paramObject).intValue());
      case 16:
        return computeSFixed64Size(paramInt1, ((Long)paramObject).longValue());
      case 15:
        return computeSFixed32Size(paramInt1, ((Integer)paramObject).intValue());
      case 14:
        return computeEnumSize(paramInt1, ((Integer)paramObject).intValue());
      case 13:
        return computeUInt32Size(paramInt1, ((Integer)paramObject).intValue());
      case 12:
        return computeBytesSize(paramInt1, (byte[])paramObject);
      case 11:
        return computeMessageSize(paramInt1, (MessageNano)paramObject);
      case 10:
        return computeGroupSize(paramInt1, (MessageNano)paramObject);
      case 9:
        return computeStringSize(paramInt1, (String)paramObject);
      case 8:
        return computeBoolSize(paramInt1, ((Boolean)paramObject).booleanValue());
      case 7:
        return computeFixed32Size(paramInt1, ((Integer)paramObject).intValue());
      case 6:
        return computeFixed64Size(paramInt1, ((Long)paramObject).longValue());
      case 5:
        return computeInt32Size(paramInt1, ((Integer)paramObject).intValue());
      case 4:
        return computeUInt64Size(paramInt1, ((Long)paramObject).longValue());
      case 3:
        return computeInt64Size(paramInt1, ((Long)paramObject).longValue());
      case 2:
        return computeFloatSize(paramInt1, ((Float)paramObject).floatValue());
      case 1:
        break;
    } 
    return computeDoubleSize(paramInt1, ((Double)paramObject).doubleValue());
  }
  
  void writeField(int paramInt1, int paramInt2, Object paramObject) throws IOException {
    switch (paramInt2) {
      default:
        paramObject = new StringBuilder();
        paramObject.append("Unknown type: ");
        paramObject.append(paramInt2);
        throw new IOException(paramObject.toString());
      case 18:
        paramObject = paramObject;
        writeSInt64(paramInt1, paramObject.longValue());
        return;
      case 17:
        paramObject = paramObject;
        writeSInt32(paramInt1, paramObject.intValue());
        return;
      case 16:
        paramObject = paramObject;
        writeSFixed64(paramInt1, paramObject.longValue());
        return;
      case 15:
        paramObject = paramObject;
        writeSFixed32(paramInt1, paramObject.intValue());
        return;
      case 14:
        paramObject = paramObject;
        writeEnum(paramInt1, paramObject.intValue());
        return;
      case 13:
        paramObject = paramObject;
        writeUInt32(paramInt1, paramObject.intValue());
        return;
      case 12:
        paramObject = paramObject;
        writeBytes(paramInt1, (byte[])paramObject);
        return;
      case 11:
        paramObject = paramObject;
        writeMessage(paramInt1, (MessageNano)paramObject);
        return;
      case 10:
        paramObject = paramObject;
        writeGroup(paramInt1, (MessageNano)paramObject);
        return;
      case 9:
        paramObject = paramObject;
        writeString(paramInt1, (String)paramObject);
        return;
      case 8:
        paramObject = paramObject;
        writeBool(paramInt1, paramObject.booleanValue());
        return;
      case 7:
        paramObject = paramObject;
        writeFixed32(paramInt1, paramObject.intValue());
        return;
      case 6:
        paramObject = paramObject;
        writeFixed64(paramInt1, paramObject.longValue());
        return;
      case 5:
        paramObject = paramObject;
        writeInt32(paramInt1, paramObject.intValue());
        return;
      case 4:
        paramObject = paramObject;
        writeUInt64(paramInt1, paramObject.longValue());
        return;
      case 3:
        paramObject = paramObject;
        writeInt64(paramInt1, paramObject.longValue());
        return;
      case 2:
        paramObject = paramObject;
        writeFloat(paramInt1, paramObject.floatValue());
        return;
      case 1:
        break;
    } 
    paramObject = paramObject;
    writeDouble(paramInt1, paramObject.doubleValue());
  }
}
