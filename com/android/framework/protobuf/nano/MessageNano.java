package com.android.framework.protobuf.nano;

import java.io.IOException;
import java.util.Arrays;

public abstract class MessageNano {
  protected volatile int cachedSize = -1;
  
  public int getCachedSize() {
    if (this.cachedSize < 0)
      getSerializedSize(); 
    return this.cachedSize;
  }
  
  public int getSerializedSize() {
    int i = computeSerializedSize();
    this.cachedSize = i;
    return i;
  }
  
  protected int computeSerializedSize() {
    return 0;
  }
  
  public void writeTo(CodedOutputByteBufferNano paramCodedOutputByteBufferNano) throws IOException {}
  
  public static final byte[] toByteArray(MessageNano paramMessageNano) {
    byte[] arrayOfByte = new byte[paramMessageNano.getSerializedSize()];
    toByteArray(paramMessageNano, arrayOfByte, 0, arrayOfByte.length);
    return arrayOfByte;
  }
  
  public static final void toByteArray(MessageNano paramMessageNano, byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    try {
      CodedOutputByteBufferNano codedOutputByteBufferNano = CodedOutputByteBufferNano.newInstance(paramArrayOfbyte, paramInt1, paramInt2);
      paramMessageNano.writeTo(codedOutputByteBufferNano);
      codedOutputByteBufferNano.checkNoSpaceLeft();
      return;
    } catch (IOException iOException) {
      throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", iOException);
    } 
  }
  
  public static final <T extends MessageNano> T mergeFrom(T paramT, byte[] paramArrayOfbyte) throws InvalidProtocolBufferNanoException {
    return mergeFrom(paramT, paramArrayOfbyte, 0, paramArrayOfbyte.length);
  }
  
  public static final <T extends MessageNano> T mergeFrom(T paramT, byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws InvalidProtocolBufferNanoException {
    try {
      CodedInputByteBufferNano codedInputByteBufferNano = CodedInputByteBufferNano.newInstance(paramArrayOfbyte, paramInt1, paramInt2);
      paramT.mergeFrom(codedInputByteBufferNano);
      codedInputByteBufferNano.checkLastTagWas(0);
      return paramT;
    } catch (InvalidProtocolBufferNanoException invalidProtocolBufferNanoException) {
      throw invalidProtocolBufferNanoException;
    } catch (IOException iOException) {
      throw new RuntimeException("Reading from a byte array threw an IOException (should never happen).");
    } 
  }
  
  public static final boolean messageNanoEquals(MessageNano paramMessageNano1, MessageNano paramMessageNano2) {
    if (paramMessageNano1 == paramMessageNano2)
      return true; 
    if (paramMessageNano1 == null || paramMessageNano2 == null)
      return false; 
    if (paramMessageNano1.getClass() != paramMessageNano2.getClass())
      return false; 
    int i = paramMessageNano1.getSerializedSize();
    if (paramMessageNano2.getSerializedSize() != i)
      return false; 
    byte[] arrayOfByte1 = new byte[i];
    byte[] arrayOfByte2 = new byte[i];
    toByteArray(paramMessageNano1, arrayOfByte1, 0, i);
    toByteArray(paramMessageNano2, arrayOfByte2, 0, i);
    return Arrays.equals(arrayOfByte1, arrayOfByte2);
  }
  
  public String toString() {
    return MessageNanoPrinter.print(this);
  }
  
  public MessageNano clone() throws CloneNotSupportedException {
    return (MessageNano)super.clone();
  }
  
  public abstract MessageNano mergeFrom(CodedInputByteBufferNano paramCodedInputByteBufferNano) throws IOException;
}
