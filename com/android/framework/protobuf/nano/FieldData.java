package com.android.framework.protobuf.nano;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

class FieldData implements Cloneable {
  private Extension<?, ?> cachedExtension;
  
  private List<UnknownFieldData> unknownFieldData;
  
  private Object value;
  
  <T> FieldData(Extension<?, T> paramExtension, T paramT) {
    this.cachedExtension = paramExtension;
    this.value = paramT;
  }
  
  FieldData() {
    this.unknownFieldData = new ArrayList<>();
  }
  
  void addUnknownField(UnknownFieldData paramUnknownFieldData) {
    this.unknownFieldData.add(paramUnknownFieldData);
  }
  
  UnknownFieldData getUnknownField(int paramInt) {
    List<UnknownFieldData> list = this.unknownFieldData;
    if (list == null)
      return null; 
    if (paramInt < list.size())
      return this.unknownFieldData.get(paramInt); 
    return null;
  }
  
  int getUnknownFieldSize() {
    List<UnknownFieldData> list = this.unknownFieldData;
    if (list == null)
      return 0; 
    return list.size();
  }
  
  <T> T getValue(Extension<?, T> paramExtension) {
    if (this.value != null) {
      if (this.cachedExtension != paramExtension)
        throw new IllegalStateException("Tried to getExtension with a differernt Extension."); 
    } else {
      this.cachedExtension = paramExtension;
      this.value = paramExtension.getValueFrom(this.unknownFieldData);
      this.unknownFieldData = null;
    } 
    return (T)this.value;
  }
  
  <T> void setValue(Extension<?, T> paramExtension, T paramT) {
    this.cachedExtension = paramExtension;
    this.value = paramT;
    this.unknownFieldData = null;
  }
  
  int computeSerializedSize() {
    int j, i = 0;
    Object object = this.value;
    if (object != null) {
      j = this.cachedExtension.computeSerializedSize(object);
    } else {
      Iterator<UnknownFieldData> iterator = this.unknownFieldData.iterator();
      while (true) {
        j = i;
        if (iterator.hasNext()) {
          object = iterator.next();
          i += object.computeSerializedSize();
          continue;
        } 
        break;
      } 
    } 
    return j;
  }
  
  void writeTo(CodedOutputByteBufferNano paramCodedOutputByteBufferNano) throws IOException {
    Object<UnknownFieldData> object = (Object<UnknownFieldData>)this.value;
    if (object != null) {
      this.cachedExtension.writeTo(object, paramCodedOutputByteBufferNano);
    } else {
      for (UnknownFieldData unknownFieldData : this.unknownFieldData)
        unknownFieldData.writeTo(paramCodedOutputByteBufferNano); 
    } 
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject == this)
      return true; 
    if (!(paramObject instanceof FieldData))
      return false; 
    paramObject = paramObject;
    if (this.value != null && ((FieldData)paramObject).value != null) {
      Extension<?, ?> extension = this.cachedExtension;
      if (extension != ((FieldData)paramObject).cachedExtension)
        return false; 
      if (!extension.clazz.isArray())
        return this.value.equals(((FieldData)paramObject).value); 
      Object object = this.value;
      if (object instanceof byte[])
        return Arrays.equals((byte[])object, (byte[])((FieldData)paramObject).value); 
      if (object instanceof int[])
        return Arrays.equals((int[])object, (int[])((FieldData)paramObject).value); 
      if (object instanceof long[])
        return Arrays.equals((long[])object, (long[])((FieldData)paramObject).value); 
      if (object instanceof float[])
        return Arrays.equals((float[])object, (float[])((FieldData)paramObject).value); 
      if (object instanceof double[])
        return Arrays.equals((double[])object, (double[])((FieldData)paramObject).value); 
      if (object instanceof boolean[])
        return Arrays.equals((boolean[])object, (boolean[])((FieldData)paramObject).value); 
      return Arrays.deepEquals((Object[])object, (Object[])((FieldData)paramObject).value);
    } 
    List<UnknownFieldData> list = this.unknownFieldData;
    if (list != null) {
      List<UnknownFieldData> list1 = ((FieldData)paramObject).unknownFieldData;
      if (list1 != null)
        return list.equals(list1); 
    } 
    try {
      return Arrays.equals(toByteArray(), paramObject.toByteArray());
    } catch (IOException iOException) {
      throw new IllegalStateException(iOException);
    } 
  }
  
  public int hashCode() {
    try {
      int i = Arrays.hashCode(toByteArray());
      return 17 * 31 + i;
    } catch (IOException iOException) {
      throw new IllegalStateException(iOException);
    } 
  }
  
  private byte[] toByteArray() throws IOException {
    byte[] arrayOfByte = new byte[computeSerializedSize()];
    CodedOutputByteBufferNano codedOutputByteBufferNano = CodedOutputByteBufferNano.newInstance(arrayOfByte);
    writeTo(codedOutputByteBufferNano);
    return arrayOfByte;
  }
  
  public final FieldData clone() {
    FieldData fieldData = new FieldData();
    try {
      fieldData.cachedExtension = this.cachedExtension;
      if (this.unknownFieldData == null) {
        fieldData.unknownFieldData = null;
      } else {
        fieldData.unknownFieldData.addAll(this.unknownFieldData);
      } 
      if (this.value != null)
        if (this.value instanceof MessageNano) {
          fieldData.value = ((MessageNano)this.value).clone();
        } else if (this.value instanceof byte[]) {
          fieldData.value = ((byte[])this.value).clone();
        } else if (this.value instanceof byte[][]) {
          byte[][] arrayOfByte1 = (byte[][])this.value;
          byte[][] arrayOfByte2 = new byte[arrayOfByte1.length][];
          fieldData.value = arrayOfByte2;
          for (byte b = 0; b < arrayOfByte1.length; b++)
            arrayOfByte2[b] = (byte[])arrayOfByte1[b].clone(); 
        } else if (this.value instanceof boolean[]) {
          fieldData.value = ((boolean[])this.value).clone();
        } else if (this.value instanceof int[]) {
          fieldData.value = ((int[])this.value).clone();
        } else if (this.value instanceof long[]) {
          fieldData.value = ((long[])this.value).clone();
        } else if (this.value instanceof float[]) {
          fieldData.value = ((float[])this.value).clone();
        } else if (this.value instanceof double[]) {
          fieldData.value = ((double[])this.value).clone();
        } else if (this.value instanceof MessageNano[]) {
          MessageNano[] arrayOfMessageNano2 = (MessageNano[])this.value;
          MessageNano[] arrayOfMessageNano1 = new MessageNano[arrayOfMessageNano2.length];
          fieldData.value = arrayOfMessageNano1;
          for (byte b = 0; b < arrayOfMessageNano2.length; b++)
            arrayOfMessageNano1[b] = arrayOfMessageNano2[b].clone(); 
        }  
      return fieldData;
    } catch (CloneNotSupportedException cloneNotSupportedException) {
      throw new AssertionError(cloneNotSupportedException);
    } 
  }
}
