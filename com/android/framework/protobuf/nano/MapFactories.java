package com.android.framework.protobuf.nano;

import java.util.HashMap;
import java.util.Map;

public final class MapFactories {
  static void setMapFactory(MapFactory paramMapFactory) {
    mapFactory = paramMapFactory;
  }
  
  public static MapFactory getMapFactory() {
    return mapFactory;
  }
  
  class DefaultMapFactory implements MapFactory {
    private DefaultMapFactory() {}
    
    public <K, V> Map<K, V> forMap(Map<K, V> param1Map) {
      if (param1Map == null)
        return new HashMap<>(); 
      return param1Map;
    }
  }
  
  private static volatile MapFactory mapFactory = new DefaultMapFactory();
  
  public static interface MapFactory {
    <K, V> Map<K, V> forMap(Map<K, V> param1Map);
  }
}
