package com.android.framework.protobuf.nano;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;

public final class InternalNano {
  static final Charset ISO_8859_1;
  
  public static final Object LAZY_INIT_LOCK;
  
  public static final int TYPE_BOOL = 8;
  
  public static final int TYPE_BYTES = 12;
  
  public static final int TYPE_DOUBLE = 1;
  
  public static final int TYPE_ENUM = 14;
  
  public static final int TYPE_FIXED32 = 7;
  
  public static final int TYPE_FIXED64 = 6;
  
  public static final int TYPE_FLOAT = 2;
  
  public static final int TYPE_GROUP = 10;
  
  public static final int TYPE_INT32 = 5;
  
  public static final int TYPE_INT64 = 3;
  
  public static final int TYPE_MESSAGE = 11;
  
  public static final int TYPE_SFIXED32 = 15;
  
  public static final int TYPE_SFIXED64 = 16;
  
  public static final int TYPE_SINT32 = 17;
  
  public static final int TYPE_SINT64 = 18;
  
  public static final int TYPE_STRING = 9;
  
  public static final int TYPE_UINT32 = 13;
  
  public static final int TYPE_UINT64 = 4;
  
  static final Charset UTF_8 = Charset.forName("UTF-8");
  
  static {
    ISO_8859_1 = Charset.forName("ISO-8859-1");
    LAZY_INIT_LOCK = new Object();
  }
  
  public static String stringDefaultValue(String paramString) {
    return new String(paramString.getBytes(ISO_8859_1), UTF_8);
  }
  
  public static byte[] bytesDefaultValue(String paramString) {
    return paramString.getBytes(ISO_8859_1);
  }
  
  public static byte[] copyFromUtf8(String paramString) {
    return paramString.getBytes(UTF_8);
  }
  
  public static boolean equals(int[] paramArrayOfint1, int[] paramArrayOfint2) {
    if (paramArrayOfint1 == null || paramArrayOfint1.length == 0)
      return (paramArrayOfint2 == null || paramArrayOfint2.length == 0); 
    return Arrays.equals(paramArrayOfint1, paramArrayOfint2);
  }
  
  public static boolean equals(long[] paramArrayOflong1, long[] paramArrayOflong2) {
    if (paramArrayOflong1 == null || paramArrayOflong1.length == 0)
      return (paramArrayOflong2 == null || paramArrayOflong2.length == 0); 
    return Arrays.equals(paramArrayOflong1, paramArrayOflong2);
  }
  
  public static boolean equals(float[] paramArrayOffloat1, float[] paramArrayOffloat2) {
    if (paramArrayOffloat1 == null || paramArrayOffloat1.length == 0)
      return (paramArrayOffloat2 == null || paramArrayOffloat2.length == 0); 
    return Arrays.equals(paramArrayOffloat1, paramArrayOffloat2);
  }
  
  public static boolean equals(double[] paramArrayOfdouble1, double[] paramArrayOfdouble2) {
    if (paramArrayOfdouble1 == null || paramArrayOfdouble1.length == 0)
      return (paramArrayOfdouble2 == null || paramArrayOfdouble2.length == 0); 
    return Arrays.equals(paramArrayOfdouble1, paramArrayOfdouble2);
  }
  
  public static boolean equals(boolean[] paramArrayOfboolean1, boolean[] paramArrayOfboolean2) {
    if (paramArrayOfboolean1 == null || paramArrayOfboolean1.length == 0)
      return (paramArrayOfboolean2 == null || paramArrayOfboolean2.length == 0); 
    return Arrays.equals(paramArrayOfboolean1, paramArrayOfboolean2);
  }
  
  public static boolean equals(byte[][] paramArrayOfbyte1, byte[][] paramArrayOfbyte2) {
    int i, k;
    byte b = 0;
    if (paramArrayOfbyte1 == null) {
      i = 0;
    } else {
      i = paramArrayOfbyte1.length;
    } 
    int j = 0;
    if (paramArrayOfbyte2 == null) {
      k = 0;
    } else {
      k = paramArrayOfbyte2.length;
    } 
    while (true) {
      byte b1;
      int m = j;
      if (b < i) {
        m = j;
        if (paramArrayOfbyte1[b] == null) {
          b++;
          continue;
        } 
      } 
      while (m < k && paramArrayOfbyte2[m] == null)
        m++; 
      if (b >= i) {
        j = 1;
      } else {
        j = 0;
      } 
      if (m >= k) {
        b1 = 1;
      } else {
        b1 = 0;
      } 
      if (j != 0 && b1)
        return true; 
      if (j != b1)
        return false; 
      if (!Arrays.equals(paramArrayOfbyte1[b], paramArrayOfbyte2[m]))
        return false; 
      b++;
      j = m + 1;
    } 
  }
  
  public static boolean equals(Object[] paramArrayOfObject1, Object[] paramArrayOfObject2) {
    int i, k;
    byte b = 0;
    if (paramArrayOfObject1 == null) {
      i = 0;
    } else {
      i = paramArrayOfObject1.length;
    } 
    int j = 0;
    if (paramArrayOfObject2 == null) {
      k = 0;
    } else {
      k = paramArrayOfObject2.length;
    } 
    while (true) {
      byte b1;
      int m = j;
      if (b < i) {
        m = j;
        if (paramArrayOfObject1[b] == null) {
          b++;
          continue;
        } 
      } 
      while (m < k && paramArrayOfObject2[m] == null)
        m++; 
      if (b >= i) {
        j = 1;
      } else {
        j = 0;
      } 
      if (m >= k) {
        b1 = 1;
      } else {
        b1 = 0;
      } 
      if (j != 0 && b1)
        return true; 
      if (j != b1)
        return false; 
      if (!paramArrayOfObject1[b].equals(paramArrayOfObject2[m]))
        return false; 
      b++;
      j = m + 1;
    } 
  }
  
  public static int hashCode(int[] paramArrayOfint) {
    return (paramArrayOfint == null || paramArrayOfint.length == 0) ? 0 : Arrays.hashCode(paramArrayOfint);
  }
  
  public static int hashCode(long[] paramArrayOflong) {
    return (paramArrayOflong == null || paramArrayOflong.length == 0) ? 0 : Arrays.hashCode(paramArrayOflong);
  }
  
  public static int hashCode(float[] paramArrayOffloat) {
    return (paramArrayOffloat == null || paramArrayOffloat.length == 0) ? 0 : Arrays.hashCode(paramArrayOffloat);
  }
  
  public static int hashCode(double[] paramArrayOfdouble) {
    return (paramArrayOfdouble == null || paramArrayOfdouble.length == 0) ? 0 : Arrays.hashCode(paramArrayOfdouble);
  }
  
  public static int hashCode(boolean[] paramArrayOfboolean) {
    return (paramArrayOfboolean == null || paramArrayOfboolean.length == 0) ? 0 : Arrays.hashCode(paramArrayOfboolean);
  }
  
  public static int hashCode(byte[][] paramArrayOfbyte) {
    int j, i = 0;
    byte b = 0;
    if (paramArrayOfbyte == null) {
      j = 0;
    } else {
      j = paramArrayOfbyte.length;
    } 
    for (; b < j; b++, i = k) {
      byte[] arrayOfByte = paramArrayOfbyte[b];
      int k = i;
      if (arrayOfByte != null)
        k = i * 31 + Arrays.hashCode(arrayOfByte); 
    } 
    return i;
  }
  
  public static int hashCode(Object[] paramArrayOfObject) {
    int j, i = 0;
    byte b = 0;
    if (paramArrayOfObject == null) {
      j = 0;
    } else {
      j = paramArrayOfObject.length;
    } 
    for (; b < j; b++, i = k) {
      Object object = paramArrayOfObject[b];
      int k = i;
      if (object != null)
        k = i * 31 + object.hashCode(); 
    } 
    return i;
  }
  
  private static Object primitiveDefaultValue(int paramInt) {
    StringBuilder stringBuilder;
    switch (paramInt) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("Type: ");
        stringBuilder.append(paramInt);
        stringBuilder.append(" is not a primitive type.");
        throw new IllegalArgumentException(stringBuilder.toString());
      case 12:
        return WireFormatNano.EMPTY_BYTES;
      case 9:
        return "";
      case 8:
        return Boolean.FALSE;
      case 5:
      case 7:
      case 13:
      case 14:
      case 15:
      case 17:
        return Integer.valueOf(0);
      case 3:
      case 4:
      case 6:
      case 16:
      case 18:
        return Long.valueOf(0L);
      case 2:
        return Float.valueOf(0.0F);
      case 1:
        break;
    } 
    return Double.valueOf(0.0D);
  }
  
  public static final <K, V> Map<K, V> mergeMapEntry(CodedInputByteBufferNano paramCodedInputByteBufferNano, Map<K, V> paramMap, MapFactories.MapFactory paramMapFactory, int paramInt1, int paramInt2, V paramV, int paramInt3, int paramInt4) throws IOException {
    // Byte code:
    //   0: aload_2
    //   1: aload_1
    //   2: invokeinterface forMap : (Ljava/util/Map;)Ljava/util/Map;
    //   7: astore #8
    //   9: aload_0
    //   10: invokevirtual readRawVarint32 : ()I
    //   13: istore #9
    //   15: aload_0
    //   16: iload #9
    //   18: invokevirtual pushLimit : (I)I
    //   21: istore #10
    //   23: aconst_null
    //   24: astore_1
    //   25: aload_0
    //   26: invokevirtual readTag : ()I
    //   29: istore #9
    //   31: iload #9
    //   33: ifne -> 39
    //   36: goto -> 119
    //   39: iload #9
    //   41: iload #6
    //   43: if_icmpne -> 59
    //   46: aload_0
    //   47: iload_3
    //   48: invokevirtual readPrimitiveField : (I)Ljava/lang/Object;
    //   51: astore_2
    //   52: aload #5
    //   54: astore #11
    //   56: goto -> 168
    //   59: iload #9
    //   61: iload #7
    //   63: if_icmpne -> 104
    //   66: iload #4
    //   68: bipush #11
    //   70: if_icmpne -> 91
    //   73: aload_0
    //   74: aload #5
    //   76: checkcast com/android/framework/protobuf/nano/MessageNano
    //   79: invokevirtual readMessage : (Lcom/android/framework/protobuf/nano/MessageNano;)V
    //   82: aload_1
    //   83: astore_2
    //   84: aload #5
    //   86: astore #11
    //   88: goto -> 168
    //   91: aload_0
    //   92: iload #4
    //   94: invokevirtual readPrimitiveField : (I)Ljava/lang/Object;
    //   97: astore #11
    //   99: aload_1
    //   100: astore_2
    //   101: goto -> 168
    //   104: aload_1
    //   105: astore_2
    //   106: aload #5
    //   108: astore #11
    //   110: aload_0
    //   111: iload #9
    //   113: invokevirtual skipField : (I)Z
    //   116: ifne -> 168
    //   119: aload_0
    //   120: iconst_0
    //   121: invokevirtual checkLastTagWas : (I)V
    //   124: aload_0
    //   125: iload #10
    //   127: invokevirtual popLimit : (I)V
    //   130: aload_1
    //   131: astore_0
    //   132: aload_1
    //   133: ifnonnull -> 141
    //   136: iload_3
    //   137: invokestatic primitiveDefaultValue : (I)Ljava/lang/Object;
    //   140: astore_0
    //   141: aload #5
    //   143: astore_1
    //   144: aload #5
    //   146: ifnonnull -> 155
    //   149: iload #4
    //   151: invokestatic primitiveDefaultValue : (I)Ljava/lang/Object;
    //   154: astore_1
    //   155: aload #8
    //   157: aload_0
    //   158: aload_1
    //   159: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   164: pop
    //   165: aload #8
    //   167: areturn
    //   168: aload_2
    //   169: astore_1
    //   170: aload #11
    //   172: astore #5
    //   174: goto -> 25
    // Line number table:
    //   Java source line number -> byte code offset
    //   #400	-> 0
    //   #401	-> 9
    //   #402	-> 15
    //   #403	-> 23
    //   #405	-> 25
    //   #406	-> 31
    //   #407	-> 36
    //   #409	-> 39
    //   #410	-> 46
    //   #411	-> 59
    //   #412	-> 66
    //   #413	-> 73
    //   #415	-> 91
    //   #418	-> 104
    //   #419	-> 119
    //   #423	-> 119
    //   #424	-> 124
    //   #426	-> 130
    //   #428	-> 136
    //   #431	-> 141
    //   #433	-> 149
    //   #436	-> 155
    //   #437	-> 165
    //   #422	-> 168
  }
  
  public static <K, V> void serializeMapField(CodedOutputByteBufferNano paramCodedOutputByteBufferNano, Map<K, V> paramMap, int paramInt1, int paramInt2, int paramInt3) throws IOException {
    for (Map.Entry<K, V> entry : paramMap.entrySet()) {
      paramMap = (Map<K, V>)entry.getKey();
      entry = (Map.Entry<K, V>)entry.getValue();
      if (paramMap != null && entry != null) {
        int i = CodedOutputByteBufferNano.computeFieldSize(1, paramInt2, paramMap);
        int j = CodedOutputByteBufferNano.computeFieldSize(2, paramInt3, entry);
        paramCodedOutputByteBufferNano.writeTag(paramInt1, 2);
        paramCodedOutputByteBufferNano.writeRawVarint32(i + j);
        paramCodedOutputByteBufferNano.writeField(1, paramInt2, paramMap);
        paramCodedOutputByteBufferNano.writeField(2, paramInt3, entry);
        continue;
      } 
      throw new IllegalStateException("keys and values in maps cannot be null");
    } 
  }
  
  public static <K, V> int computeMapFieldSize(Map<K, V> paramMap, int paramInt1, int paramInt2, int paramInt3) {
    int i = 0;
    int j = CodedOutputByteBufferNano.computeTagSize(paramInt1);
    for (Iterator<Map.Entry> iterator = paramMap.entrySet().iterator(); iterator.hasNext(); ) {
      Map.Entry entry = iterator.next();
      paramMap = (Map<K, V>)entry.getKey();
      entry = (Map.Entry)entry.getValue();
      if (paramMap != null && entry != null) {
        i = CodedOutputByteBufferNano.computeFieldSize(1, paramInt2, paramMap);
        i += CodedOutputByteBufferNano.computeFieldSize(2, paramInt3, entry);
        paramInt1 += j + i + CodedOutputByteBufferNano.computeRawVarint32Size(i);
        continue;
      } 
      throw new IllegalStateException("keys and values in maps cannot be null");
    } 
    return paramInt1;
  }
  
  public static <K, V> boolean equals(Map<K, V> paramMap1, Map<K, V> paramMap2) {
    boolean bool1 = true, bool2 = true;
    if (paramMap1 == paramMap2)
      return true; 
    if (paramMap1 == null) {
      if (paramMap2.size() != 0)
        bool2 = false; 
      return bool2;
    } 
    if (paramMap2 == null) {
      if (paramMap1.size() == 0) {
        bool2 = bool1;
      } else {
        bool2 = false;
      } 
      return bool2;
    } 
    if (paramMap1.size() != paramMap2.size())
      return false; 
    for (Map.Entry<K, V> entry : paramMap1.entrySet()) {
      if (!paramMap2.containsKey(entry.getKey()))
        return false; 
      if (!equalsMapValue(entry.getValue(), paramMap2.get(entry.getKey())))
        return false; 
    } 
    return true;
  }
  
  private static boolean equalsMapValue(Object paramObject1, Object paramObject2) {
    if (paramObject1 != null && paramObject2 != null) {
      if (paramObject1 instanceof byte[] && paramObject2 instanceof byte[])
        return Arrays.equals((byte[])paramObject1, (byte[])paramObject2); 
      return paramObject1.equals(paramObject2);
    } 
    throw new IllegalStateException("keys and values in maps cannot be null");
  }
  
  public static <K, V> int hashCode(Map<K, V> paramMap) {
    if (paramMap == null)
      return 0; 
    int i = 0;
    for (Map.Entry<K, V> entry : paramMap.entrySet()) {
      int j = hashCodeForMap(entry.getKey());
      i += j ^ hashCodeForMap(entry.getValue());
    } 
    return i;
  }
  
  private static int hashCodeForMap(Object paramObject) {
    if (paramObject instanceof byte[])
      return Arrays.hashCode((byte[])paramObject); 
    return paramObject.hashCode();
  }
  
  public static void cloneUnknownFieldData(ExtendableMessageNano paramExtendableMessageNano1, ExtendableMessageNano paramExtendableMessageNano2) {
    if (paramExtendableMessageNano1.unknownFieldData != null)
      paramExtendableMessageNano2.unknownFieldData = paramExtendableMessageNano1.unknownFieldData.clone(); 
  }
}
