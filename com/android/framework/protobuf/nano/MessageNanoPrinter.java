package com.android.framework.protobuf.nano;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

public final class MessageNanoPrinter {
  private static final String INDENT = "  ";
  
  private static final int MAX_STRING_LEN = 200;
  
  public static <T extends MessageNano> String print(T paramT) {
    if (paramT == null)
      return ""; 
    StringBuffer stringBuffer = new StringBuffer();
    try {
      StringBuffer stringBuffer1 = new StringBuffer();
      this();
      print(null, paramT, stringBuffer1, stringBuffer);
      return stringBuffer.toString();
    } catch (IllegalAccessException illegalAccessException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error printing proto: ");
      stringBuilder.append(illegalAccessException.getMessage());
      return stringBuilder.toString();
    } catch (InvocationTargetException invocationTargetException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error printing proto: ");
      stringBuilder.append(invocationTargetException.getMessage());
      return stringBuilder.toString();
    } 
  }
  
  private static void print(String paramString, Object paramObject, StringBuffer paramStringBuffer1, StringBuffer paramStringBuffer2) throws IllegalAccessException, InvocationTargetException {
    if (paramObject != null)
      if (paramObject instanceof MessageNano) {
        int i = paramStringBuffer1.length();
        if (paramString != null) {
          paramStringBuffer2.append(paramStringBuffer1);
          paramStringBuffer2.append(deCamelCaseify(paramString));
          paramStringBuffer2.append(" <\n");
          paramStringBuffer1.append("  ");
        } 
        Class<?> clazz = paramObject.getClass();
        for (Field field : clazz.getFields()) {
          int j = field.getModifiers();
          String str = field.getName();
          if (!"cachedSize".equals(str))
            if ((j & 0x1) == 1 && (j & 0x8) != 8)
              if (!str.startsWith("_") && 
                !str.endsWith("_")) {
                Class<?> clazz1 = field.getType();
                Object object = field.get(paramObject);
                if (clazz1.isArray()) {
                  clazz1 = clazz1.getComponentType();
                  if (clazz1 == byte.class) {
                    print(str, object, paramStringBuffer1, paramStringBuffer2);
                  } else {
                    if (object == null) {
                      j = 0;
                    } else {
                      j = Array.getLength(object);
                    } 
                    for (byte b = 0; b < j; b++) {
                      Object object1 = Array.get(object, b);
                      print(str, object1, paramStringBuffer1, paramStringBuffer2);
                    } 
                  } 
                } else {
                  print(str, object, paramStringBuffer1, paramStringBuffer2);
                } 
              }   
        } 
        for (Method method : clazz.getMethods()) {
          String str = method.getName();
          if (str.startsWith("set")) {
            str = str.substring(3);
            try {
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("has");
              stringBuilder.append(str);
              String str1 = stringBuilder.toString();
              try {
                Method method1 = clazz.getMethod(str1, new Class[0]);
                if (((Boolean)method1.invoke(paramObject, new Object[0])).booleanValue())
                  try {
                    StringBuilder stringBuilder1 = new StringBuilder();
                    this();
                    stringBuilder1.append("get");
                    stringBuilder1.append(str);
                    String str2 = stringBuilder1.toString();
                    try {
                      Method method2 = clazz.getMethod(str2, new Class[0]);
                      print(str, method2.invoke(paramObject, new Object[0]), paramStringBuffer1, paramStringBuffer2);
                    } catch (NoSuchMethodException noSuchMethodException) {}
                  } catch (NoSuchMethodException noSuchMethodException) {} 
              } catch (NoSuchMethodException noSuchMethodException) {}
            } catch (NoSuchMethodException noSuchMethodException) {}
          } 
        } 
        if (paramString != null) {
          paramStringBuffer1.setLength(i);
          paramStringBuffer2.append(paramStringBuffer1);
          paramStringBuffer2.append(">\n");
        } 
      } else if (paramObject instanceof Map) {
        paramObject = paramObject;
        paramString = deCamelCaseify(paramString);
        for (paramObject = paramObject.entrySet().iterator(); paramObject.hasNext(); ) {
          Map.Entry entry = paramObject.next();
          paramStringBuffer2.append(paramStringBuffer1);
          paramStringBuffer2.append(paramString);
          paramStringBuffer2.append(" <\n");
          int i = paramStringBuffer1.length();
          paramStringBuffer1.append("  ");
          print("key", entry.getKey(), paramStringBuffer1, paramStringBuffer2);
          print("value", entry.getValue(), paramStringBuffer1, paramStringBuffer2);
          paramStringBuffer1.setLength(i);
          paramStringBuffer2.append(paramStringBuffer1);
          paramStringBuffer2.append(">\n");
        } 
      } else {
        paramString = deCamelCaseify(paramString);
        paramStringBuffer2.append(paramStringBuffer1);
        paramStringBuffer2.append(paramString);
        paramStringBuffer2.append(": ");
        if (paramObject instanceof String) {
          paramString = sanitizeString((String)paramObject);
          paramStringBuffer2.append("\"");
          paramStringBuffer2.append(paramString);
          paramStringBuffer2.append("\"");
        } else if (paramObject instanceof byte[]) {
          appendQuotedBytes((byte[])paramObject, paramStringBuffer2);
        } else {
          paramStringBuffer2.append(paramObject);
        } 
        paramStringBuffer2.append("\n");
      }  
  }
  
  private static String deCamelCaseify(String paramString) {
    StringBuffer stringBuffer = new StringBuffer();
    for (byte b = 0; b < paramString.length(); b++) {
      char c = paramString.charAt(b);
      if (b == 0) {
        stringBuffer.append(Character.toLowerCase(c));
      } else if (Character.isUpperCase(c)) {
        stringBuffer.append('_');
        stringBuffer.append(Character.toLowerCase(c));
      } else {
        stringBuffer.append(c);
      } 
    } 
    return stringBuffer.toString();
  }
  
  private static String sanitizeString(String paramString) {
    String str = paramString;
    if (!paramString.startsWith("http")) {
      str = paramString;
      if (paramString.length() > 200) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(paramString.substring(0, 200));
        stringBuilder.append("[...]");
        str = stringBuilder.toString();
      } 
    } 
    return escapeString(str);
  }
  
  private static String escapeString(String paramString) {
    int i = paramString.length();
    StringBuilder stringBuilder = new StringBuilder(i);
    for (byte b = 0; b < i; b++) {
      char c = paramString.charAt(b);
      if (c >= ' ' && c <= '~' && c != '"' && c != '\'') {
        stringBuilder.append(c);
      } else {
        stringBuilder.append(String.format("\\u%04x", new Object[] { Integer.valueOf(c) }));
      } 
    } 
    return stringBuilder.toString();
  }
  
  private static void appendQuotedBytes(byte[] paramArrayOfbyte, StringBuffer paramStringBuffer) {
    if (paramArrayOfbyte == null) {
      paramStringBuffer.append("\"\"");
      return;
    } 
    paramStringBuffer.append('"');
    for (byte b = 0; b < paramArrayOfbyte.length; b++) {
      int i = paramArrayOfbyte[b] & 0xFF;
      if (i == 92 || i == 34) {
        paramStringBuffer.append('\\');
        paramStringBuffer.append((char)i);
      } else if (i >= 32 && i < 127) {
        paramStringBuffer.append((char)i);
      } else {
        paramStringBuffer.append(String.format("\\%03o", new Object[] { Integer.valueOf(i) }));
      } 
    } 
    paramStringBuffer.append('"');
  }
}
