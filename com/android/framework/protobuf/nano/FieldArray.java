package com.android.framework.protobuf.nano;

public final class FieldArray implements Cloneable {
  private static final FieldData DELETED = new FieldData();
  
  private FieldData[] mData;
  
  private int[] mFieldNumbers;
  
  private boolean mGarbage = false;
  
  private int mSize;
  
  FieldArray() {
    this(10);
  }
  
  FieldArray(int paramInt) {
    paramInt = idealIntArraySize(paramInt);
    this.mFieldNumbers = new int[paramInt];
    this.mData = new FieldData[paramInt];
    this.mSize = 0;
  }
  
  FieldData get(int paramInt) {
    paramInt = binarySearch(paramInt);
    if (paramInt >= 0) {
      FieldData[] arrayOfFieldData = this.mData;
      if (arrayOfFieldData[paramInt] != DELETED)
        return arrayOfFieldData[paramInt]; 
    } 
    return null;
  }
  
  void remove(int paramInt) {
    paramInt = binarySearch(paramInt);
    if (paramInt >= 0) {
      FieldData arrayOfFieldData[] = this.mData, fieldData1 = arrayOfFieldData[paramInt], fieldData2 = DELETED;
      if (fieldData1 != fieldData2) {
        arrayOfFieldData[paramInt] = fieldData2;
        this.mGarbage = true;
      } 
    } 
  }
  
  private void gc() {
    int i = this.mSize;
    int j = 0;
    int[] arrayOfInt = this.mFieldNumbers;
    FieldData[] arrayOfFieldData = this.mData;
    for (int k = 0; k < i; k++, j = m) {
      FieldData fieldData = arrayOfFieldData[k];
      int m = j;
      if (fieldData != DELETED) {
        if (k != j) {
          arrayOfInt[j] = arrayOfInt[k];
          arrayOfFieldData[j] = fieldData;
          arrayOfFieldData[k] = null;
        } 
        m = j + 1;
      } 
    } 
    this.mGarbage = false;
    this.mSize = j;
  }
  
  void put(int paramInt, FieldData paramFieldData) {
    int i = binarySearch(paramInt);
    if (i >= 0) {
      this.mData[i] = paramFieldData;
    } else {
      int j = i ^ 0xFFFFFFFF;
      if (j < this.mSize) {
        FieldData[] arrayOfFieldData = this.mData;
        if (arrayOfFieldData[j] == DELETED) {
          this.mFieldNumbers[j] = paramInt;
          arrayOfFieldData[j] = paramFieldData;
          return;
        } 
      } 
      i = j;
      if (this.mGarbage) {
        i = j;
        if (this.mSize >= this.mFieldNumbers.length) {
          gc();
          i = binarySearch(paramInt) ^ 0xFFFFFFFF;
        } 
      } 
      j = this.mSize;
      if (j >= this.mFieldNumbers.length) {
        j = idealIntArraySize(j + 1);
        int[] arrayOfInt1 = new int[j];
        FieldData[] arrayOfFieldData1 = new FieldData[j];
        int[] arrayOfInt2 = this.mFieldNumbers;
        System.arraycopy(arrayOfInt2, 0, arrayOfInt1, 0, arrayOfInt2.length);
        FieldData[] arrayOfFieldData2 = this.mData;
        System.arraycopy(arrayOfFieldData2, 0, arrayOfFieldData1, 0, arrayOfFieldData2.length);
        this.mFieldNumbers = arrayOfInt1;
        this.mData = arrayOfFieldData1;
      } 
      j = this.mSize;
      if (j - i != 0) {
        int[] arrayOfInt = this.mFieldNumbers;
        System.arraycopy(arrayOfInt, i, arrayOfInt, i + 1, j - i);
        FieldData[] arrayOfFieldData = this.mData;
        System.arraycopy(arrayOfFieldData, i, arrayOfFieldData, i + 1, this.mSize - i);
      } 
      this.mFieldNumbers[i] = paramInt;
      this.mData[i] = paramFieldData;
      this.mSize++;
    } 
  }
  
  int size() {
    if (this.mGarbage)
      gc(); 
    return this.mSize;
  }
  
  public boolean isEmpty() {
    boolean bool;
    if (size() == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  FieldData dataAt(int paramInt) {
    if (this.mGarbage)
      gc(); 
    return this.mData[paramInt];
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (paramObject == this)
      return true; 
    if (!(paramObject instanceof FieldArray))
      return false; 
    FieldArray fieldArray = (FieldArray)paramObject;
    if (size() != fieldArray.size())
      return false; 
    if (arrayEquals(this.mFieldNumbers, fieldArray.mFieldNumbers, this.mSize)) {
      paramObject = this.mData;
      FieldData[] arrayOfFieldData = fieldArray.mData;
      int i = this.mSize;
      if (arrayEquals((FieldData[])paramObject, arrayOfFieldData, i))
        return null; 
    } 
    return false;
  }
  
  public int hashCode() {
    if (this.mGarbage)
      gc(); 
    int i = 17;
    for (byte b = 0; b < this.mSize; b++) {
      int j = this.mFieldNumbers[b];
      i = (i * 31 + j) * 31 + this.mData[b].hashCode();
    } 
    return i;
  }
  
  private int idealIntArraySize(int paramInt) {
    return idealByteArraySize(paramInt * 4) / 4;
  }
  
  private int idealByteArraySize(int paramInt) {
    for (byte b = 4; b < 32; b++) {
      if (paramInt <= (1 << b) - 12)
        return (1 << b) - 12; 
    } 
    return paramInt;
  }
  
  private int binarySearch(int paramInt) {
    int i = 0;
    int j = this.mSize - 1;
    while (i <= j) {
      int k = i + j >>> 1;
      int m = this.mFieldNumbers[k];
      if (m < paramInt) {
        i = k + 1;
        continue;
      } 
      if (m > paramInt) {
        j = k - 1;
        continue;
      } 
      return k;
    } 
    return i ^ 0xFFFFFFFF;
  }
  
  private boolean arrayEquals(int[] paramArrayOfint1, int[] paramArrayOfint2, int paramInt) {
    for (byte b = 0; b < paramInt; b++) {
      if (paramArrayOfint1[b] != paramArrayOfint2[b])
        return false; 
    } 
    return true;
  }
  
  private boolean arrayEquals(FieldData[] paramArrayOfFieldData1, FieldData[] paramArrayOfFieldData2, int paramInt) {
    for (byte b = 0; b < paramInt; b++) {
      if (!paramArrayOfFieldData1[b].equals(paramArrayOfFieldData2[b]))
        return false; 
    } 
    return true;
  }
  
  public final FieldArray clone() {
    int i = size();
    FieldArray fieldArray = new FieldArray(i);
    System.arraycopy(this.mFieldNumbers, 0, fieldArray.mFieldNumbers, 0, i);
    for (byte b = 0; b < i; b++) {
      FieldData[] arrayOfFieldData = this.mData;
      if (arrayOfFieldData[b] != null)
        fieldArray.mData[b] = arrayOfFieldData[b].clone(); 
    } 
    fieldArray.mSize = i;
    return fieldArray;
  }
}
