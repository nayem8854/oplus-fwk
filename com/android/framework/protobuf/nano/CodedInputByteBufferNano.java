package com.android.framework.protobuf.nano;

import java.io.IOException;

public final class CodedInputByteBufferNano {
  private static final int DEFAULT_RECURSION_LIMIT = 64;
  
  private static final int DEFAULT_SIZE_LIMIT = 67108864;
  
  private final byte[] buffer;
  
  private int bufferPos;
  
  private int bufferSize;
  
  private int bufferSizeAfterLimit;
  
  private int bufferStart;
  
  public static CodedInputByteBufferNano newInstance(byte[] paramArrayOfbyte) {
    return newInstance(paramArrayOfbyte, 0, paramArrayOfbyte.length);
  }
  
  public static CodedInputByteBufferNano newInstance(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    return new CodedInputByteBufferNano(paramArrayOfbyte, paramInt1, paramInt2);
  }
  
  public int readTag() throws IOException {
    if (isAtEnd()) {
      this.lastTag = 0;
      return 0;
    } 
    int i = readRawVarint32();
    if (i != 0)
      return i; 
    throw InvalidProtocolBufferNanoException.invalidTag();
  }
  
  public void checkLastTagWas(int paramInt) throws InvalidProtocolBufferNanoException {
    if (this.lastTag == paramInt)
      return; 
    throw InvalidProtocolBufferNanoException.invalidEndTag();
  }
  
  public boolean skipField(int paramInt) throws IOException {
    int i = WireFormatNano.getTagWireType(paramInt);
    if (i != 0) {
      if (i != 1) {
        if (i != 2) {
          if (i != 3) {
            if (i != 4) {
              if (i == 5) {
                readRawLittleEndian32();
                return true;
              } 
              throw InvalidProtocolBufferNanoException.invalidWireType();
            } 
            return false;
          } 
          skipMessage();
          paramInt = WireFormatNano.makeTag(WireFormatNano.getTagFieldNumber(paramInt), 4);
          checkLastTagWas(paramInt);
          return true;
        } 
        skipRawBytes(readRawVarint32());
        return true;
      } 
      readRawLittleEndian64();
      return true;
    } 
    readInt32();
    return true;
  }
  
  public void skipMessage() throws IOException {
    int i;
    do {
      i = readTag();
    } while (i != 0 && skipField(i));
  }
  
  public double readDouble() throws IOException {
    return Double.longBitsToDouble(readRawLittleEndian64());
  }
  
  public float readFloat() throws IOException {
    return Float.intBitsToFloat(readRawLittleEndian32());
  }
  
  public long readUInt64() throws IOException {
    return readRawVarint64();
  }
  
  public long readInt64() throws IOException {
    return readRawVarint64();
  }
  
  public int readInt32() throws IOException {
    return readRawVarint32();
  }
  
  public long readFixed64() throws IOException {
    return readRawLittleEndian64();
  }
  
  public int readFixed32() throws IOException {
    return readRawLittleEndian32();
  }
  
  public boolean readBool() throws IOException {
    boolean bool;
    if (readRawVarint32() != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public String readString() throws IOException {
    int i = readRawVarint32();
    if (i <= this.bufferSize - this.bufferPos && i > 0) {
      String str = new String(this.buffer, this.bufferPos, i, InternalNano.UTF_8);
      this.bufferPos += i;
      return str;
    } 
    return new String(readRawBytes(i), InternalNano.UTF_8);
  }
  
  public void readGroup(MessageNano paramMessageNano, int paramInt) throws IOException {
    int i = this.recursionDepth;
    if (i < this.recursionLimit) {
      this.recursionDepth = i + 1;
      paramMessageNano.mergeFrom(this);
      paramInt = WireFormatNano.makeTag(paramInt, 4);
      checkLastTagWas(paramInt);
      this.recursionDepth--;
      return;
    } 
    throw InvalidProtocolBufferNanoException.recursionLimitExceeded();
  }
  
  public void readMessage(MessageNano paramMessageNano) throws IOException {
    int i = readRawVarint32();
    if (this.recursionDepth < this.recursionLimit) {
      i = pushLimit(i);
      this.recursionDepth++;
      paramMessageNano.mergeFrom(this);
      checkLastTagWas(0);
      this.recursionDepth--;
      popLimit(i);
      return;
    } 
    throw InvalidProtocolBufferNanoException.recursionLimitExceeded();
  }
  
  public byte[] readBytes() throws IOException {
    int i = readRawVarint32();
    int j = this.bufferSize, k = this.bufferPos;
    if (i <= j - k && i > 0) {
      byte[] arrayOfByte = new byte[i];
      System.arraycopy(this.buffer, k, arrayOfByte, 0, i);
      this.bufferPos += i;
      return arrayOfByte;
    } 
    if (i == 0)
      return WireFormatNano.EMPTY_BYTES; 
    return readRawBytes(i);
  }
  
  public int readUInt32() throws IOException {
    return readRawVarint32();
  }
  
  public int readEnum() throws IOException {
    return readRawVarint32();
  }
  
  public int readSFixed32() throws IOException {
    return readRawLittleEndian32();
  }
  
  public long readSFixed64() throws IOException {
    return readRawLittleEndian64();
  }
  
  public int readSInt32() throws IOException {
    return decodeZigZag32(readRawVarint32());
  }
  
  public long readSInt64() throws IOException {
    return decodeZigZag64(readRawVarint64());
  }
  
  public int readRawVarint32() throws IOException {
    byte b1 = readRawByte();
    if (b1 >= 0)
      return b1; 
    int i = b1 & Byte.MAX_VALUE;
    byte b2 = readRawByte();
    if (b2 >= 0) {
      i |= b2 << 7;
    } else {
      int j = i | (b2 & Byte.MAX_VALUE) << 7;
      i = readRawByte();
      if (i >= 0) {
        i = j | i << 14;
      } else {
        j |= (i & 0x7F) << 14;
        i = readRawByte();
        if (i >= 0) {
          i = j | i << 21;
        } else {
          byte b = readRawByte();
          j = j | (i & 0x7F) << 21 | b << 28;
          i = j;
          if (b < 0) {
            for (i = 0; i < 5; i++) {
              if (readRawByte() >= 0)
                return j; 
            } 
            throw InvalidProtocolBufferNanoException.malformedVarint();
          } 
        } 
      } 
    } 
    return i;
  }
  
  public long readRawVarint64() throws IOException {
    byte b = 0;
    long l = 0L;
    while (b < 64) {
      byte b1 = readRawByte();
      l |= (b1 & Byte.MAX_VALUE) << b;
      if ((b1 & 0x80) == 0)
        return l; 
      b += 7;
    } 
    throw InvalidProtocolBufferNanoException.malformedVarint();
  }
  
  public int readRawLittleEndian32() throws IOException {
    byte b1 = readRawByte();
    byte b2 = readRawByte();
    byte b3 = readRawByte();
    byte b4 = readRawByte();
    return b1 & 0xFF | (b2 & 0xFF) << 8 | (b3 & 0xFF) << 16 | (b4 & 0xFF) << 24;
  }
  
  public long readRawLittleEndian64() throws IOException {
    byte b1 = readRawByte();
    byte b2 = readRawByte();
    byte b3 = readRawByte();
    byte b4 = readRawByte();
    byte b5 = readRawByte();
    byte b6 = readRawByte();
    byte b7 = readRawByte();
    byte b8 = readRawByte();
    return b1 & 0xFFL | (b2 & 0xFFL) << 8L | (b3 & 0xFFL) << 16L | (b4 & 0xFFL) << 24L | (b5 & 0xFFL) << 32L | (b6 & 0xFFL) << 40L | (b7 & 0xFFL) << 48L | (0xFFL & b8) << 56L;
  }
  
  public static int decodeZigZag32(int paramInt) {
    return paramInt >>> 1 ^ -(paramInt & 0x1);
  }
  
  public static long decodeZigZag64(long paramLong) {
    return paramLong >>> 1L ^ -(0x1L & paramLong);
  }
  
  private int currentLimit = Integer.MAX_VALUE;
  
  private int lastTag;
  
  private int recursionDepth;
  
  private int recursionLimit = 64;
  
  private int sizeLimit = 67108864;
  
  private CodedInputByteBufferNano(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    this.buffer = paramArrayOfbyte;
    this.bufferStart = paramInt1;
    this.bufferSize = paramInt1 + paramInt2;
    this.bufferPos = paramInt1;
  }
  
  public int setRecursionLimit(int paramInt) {
    if (paramInt >= 0) {
      int i = this.recursionLimit;
      this.recursionLimit = paramInt;
      return i;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Recursion limit cannot be negative: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public int setSizeLimit(int paramInt) {
    if (paramInt >= 0) {
      int i = this.sizeLimit;
      this.sizeLimit = paramInt;
      return i;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Size limit cannot be negative: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void resetSizeCounter() {}
  
  public int pushLimit(int paramInt) throws InvalidProtocolBufferNanoException {
    if (paramInt >= 0) {
      paramInt += this.bufferPos;
      int i = this.currentLimit;
      if (paramInt <= i) {
        this.currentLimit = paramInt;
        recomputeBufferSizeAfterLimit();
        return i;
      } 
      throw InvalidProtocolBufferNanoException.truncatedMessage();
    } 
    throw InvalidProtocolBufferNanoException.negativeSize();
  }
  
  private void recomputeBufferSizeAfterLimit() {
    int i = this.bufferSize + this.bufferSizeAfterLimit;
    int j = this.bufferSize;
    int k = this.currentLimit;
    if (j > k) {
      this.bufferSizeAfterLimit = j -= k;
      this.bufferSize = i - j;
    } else {
      this.bufferSizeAfterLimit = 0;
    } 
  }
  
  public void popLimit(int paramInt) {
    this.currentLimit = paramInt;
    recomputeBufferSizeAfterLimit();
  }
  
  public int getBytesUntilLimit() {
    int i = this.currentLimit;
    if (i == Integer.MAX_VALUE)
      return -1; 
    int j = this.bufferPos;
    return i - j;
  }
  
  public boolean isAtEnd() {
    boolean bool;
    if (this.bufferPos == this.bufferSize) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getPosition() {
    return this.bufferPos - this.bufferStart;
  }
  
  public byte[] getData(int paramInt1, int paramInt2) {
    if (paramInt2 == 0)
      return WireFormatNano.EMPTY_BYTES; 
    byte[] arrayOfByte = new byte[paramInt2];
    int i = this.bufferStart;
    System.arraycopy(this.buffer, i + paramInt1, arrayOfByte, 0, paramInt2);
    return arrayOfByte;
  }
  
  public void rewindToPosition(int paramInt) {
    int i = this.bufferPos, j = this.bufferStart;
    if (paramInt <= i - j) {
      if (paramInt >= 0) {
        this.bufferPos = j + paramInt;
        return;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Bad position ");
      stringBuilder1.append(paramInt);
      throw new IllegalArgumentException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Position ");
    stringBuilder.append(paramInt);
    stringBuilder.append(" is beyond current ");
    stringBuilder.append(this.bufferPos - this.bufferStart);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public byte readRawByte() throws IOException {
    int i = this.bufferPos;
    if (i != this.bufferSize) {
      byte[] arrayOfByte = this.buffer;
      this.bufferPos = i + 1;
      return arrayOfByte[i];
    } 
    throw InvalidProtocolBufferNanoException.truncatedMessage();
  }
  
  public byte[] readRawBytes(int paramInt) throws IOException {
    if (paramInt >= 0) {
      int i = this.bufferPos, j = this.currentLimit;
      if (i + paramInt <= j) {
        if (paramInt <= this.bufferSize - i) {
          byte[] arrayOfByte = new byte[paramInt];
          System.arraycopy(this.buffer, i, arrayOfByte, 0, paramInt);
          this.bufferPos += paramInt;
          return arrayOfByte;
        } 
        throw InvalidProtocolBufferNanoException.truncatedMessage();
      } 
      skipRawBytes(j - i);
      throw InvalidProtocolBufferNanoException.truncatedMessage();
    } 
    throw InvalidProtocolBufferNanoException.negativeSize();
  }
  
  public void skipRawBytes(int paramInt) throws IOException {
    if (paramInt >= 0) {
      int i = this.bufferPos, j = this.currentLimit;
      if (i + paramInt <= j) {
        if (paramInt <= this.bufferSize - i) {
          this.bufferPos = i + paramInt;
          return;
        } 
        throw InvalidProtocolBufferNanoException.truncatedMessage();
      } 
      skipRawBytes(j - i);
      throw InvalidProtocolBufferNanoException.truncatedMessage();
    } 
    throw InvalidProtocolBufferNanoException.negativeSize();
  }
  
  Object readPrimitiveField(int paramInt) throws IOException {
    StringBuilder stringBuilder;
    switch (paramInt) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown type ");
        stringBuilder.append(paramInt);
        throw new IllegalArgumentException(stringBuilder.toString());
      case 18:
        return Long.valueOf(readSInt64());
      case 17:
        return Integer.valueOf(readSInt32());
      case 16:
        return Long.valueOf(readSFixed64());
      case 15:
        return Integer.valueOf(readSFixed32());
      case 14:
        return Integer.valueOf(readEnum());
      case 13:
        return Integer.valueOf(readUInt32());
      case 12:
        return readBytes();
      case 9:
        return readString();
      case 8:
        return Boolean.valueOf(readBool());
      case 7:
        return Integer.valueOf(readFixed32());
      case 6:
        return Long.valueOf(readFixed64());
      case 5:
        return Integer.valueOf(readInt32());
      case 4:
        return Long.valueOf(readUInt64());
      case 3:
        return Long.valueOf(readInt64());
      case 2:
        return Float.valueOf(readFloat());
      case 1:
        break;
    } 
    return Double.valueOf(readDouble());
  }
}
