package com.android.framework.protobuf.nano;

import java.io.IOException;

public abstract class ExtendableMessageNano<M extends ExtendableMessageNano<M>> extends MessageNano {
  protected FieldArray unknownFieldData;
  
  protected int computeSerializedSize() {
    int i = 0, j = 0;
    if (this.unknownFieldData != null) {
      byte b = 0;
      while (true) {
        i = j;
        if (b < this.unknownFieldData.size()) {
          FieldData fieldData = this.unknownFieldData.dataAt(b);
          j += fieldData.computeSerializedSize();
          b++;
          continue;
        } 
        break;
      } 
    } 
    return i;
  }
  
  public void writeTo(CodedOutputByteBufferNano paramCodedOutputByteBufferNano) throws IOException {
    if (this.unknownFieldData == null)
      return; 
    for (byte b = 0; b < this.unknownFieldData.size(); b++) {
      FieldData fieldData = this.unknownFieldData.dataAt(b);
      fieldData.writeTo(paramCodedOutputByteBufferNano);
    } 
  }
  
  public final boolean hasExtension(Extension<M, ?> paramExtension) {
    FieldArray fieldArray = this.unknownFieldData;
    boolean bool = false;
    if (fieldArray == null)
      return false; 
    FieldData fieldData = fieldArray.get(WireFormatNano.getTagFieldNumber(paramExtension.tag));
    if (fieldData != null)
      bool = true; 
    return bool;
  }
  
  public final <T> T getExtension(Extension<M, T> paramExtension) {
    FieldArray fieldArray = this.unknownFieldData;
    Extension extension = null;
    if (fieldArray == null)
      return null; 
    FieldData fieldData = fieldArray.get(WireFormatNano.getTagFieldNumber(paramExtension.tag));
    if (fieldData == null) {
      paramExtension = extension;
    } else {
      paramExtension = fieldData.getValue((Extension)paramExtension);
    } 
    return (T)paramExtension;
  }
  
  public final <T> M setExtension(Extension<M, T> paramExtension, T paramT) {
    FieldArray fieldArray;
    int i = WireFormatNano.getTagFieldNumber(paramExtension.tag);
    if (paramT == null) {
      fieldArray = this.unknownFieldData;
      if (fieldArray != null) {
        fieldArray.remove(i);
        if (this.unknownFieldData.isEmpty())
          this.unknownFieldData = null; 
      } 
    } else {
      FieldData fieldData = null;
      FieldArray fieldArray1 = this.unknownFieldData;
      if (fieldArray1 == null) {
        this.unknownFieldData = new FieldArray();
      } else {
        fieldData = fieldArray1.get(i);
      } 
      if (fieldData == null) {
        this.unknownFieldData.put(i, new FieldData((Extension<?, T>)fieldArray, paramT));
      } else {
        fieldData.setValue((Extension<?, T>)fieldArray, paramT);
      } 
    } 
    return (M)this;
  }
  
  protected final boolean storeUnknownField(CodedInputByteBufferNano paramCodedInputByteBufferNano, int paramInt) throws IOException {
    FieldData fieldData1;
    int i = paramCodedInputByteBufferNano.getPosition();
    if (!paramCodedInputByteBufferNano.skipField(paramInt))
      return false; 
    int j = WireFormatNano.getTagFieldNumber(paramInt);
    int k = paramCodedInputByteBufferNano.getPosition();
    byte[] arrayOfByte = paramCodedInputByteBufferNano.getData(i, k - i);
    UnknownFieldData unknownFieldData = new UnknownFieldData(paramInt, arrayOfByte);
    arrayOfByte = null;
    FieldArray fieldArray = this.unknownFieldData;
    if (fieldArray == null) {
      this.unknownFieldData = new FieldArray();
    } else {
      fieldData1 = fieldArray.get(j);
    } 
    FieldData fieldData2 = fieldData1;
    if (fieldData1 == null) {
      fieldData2 = new FieldData();
      this.unknownFieldData.put(j, fieldData2);
    } 
    fieldData2.addUnknownField(unknownFieldData);
    return true;
  }
  
  public M clone() throws CloneNotSupportedException {
    ExtendableMessageNano extendableMessageNano = (ExtendableMessageNano)super.clone();
    InternalNano.cloneUnknownFieldData(this, extendableMessageNano);
    return (M)extendableMessageNano;
  }
}
