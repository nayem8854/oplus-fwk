package com.android.framework.protobuf.nano;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Extension<M extends ExtendableMessageNano<M>, T> {
  public static final int TYPE_BOOL = 8;
  
  public static final int TYPE_BYTES = 12;
  
  public static final int TYPE_DOUBLE = 1;
  
  public static final int TYPE_ENUM = 14;
  
  public static final int TYPE_FIXED32 = 7;
  
  public static final int TYPE_FIXED64 = 6;
  
  public static final int TYPE_FLOAT = 2;
  
  public static final int TYPE_GROUP = 10;
  
  public static final int TYPE_INT32 = 5;
  
  public static final int TYPE_INT64 = 3;
  
  public static final int TYPE_MESSAGE = 11;
  
  public static final int TYPE_SFIXED32 = 15;
  
  public static final int TYPE_SFIXED64 = 16;
  
  public static final int TYPE_SINT32 = 17;
  
  public static final int TYPE_SINT64 = 18;
  
  public static final int TYPE_STRING = 9;
  
  public static final int TYPE_UINT32 = 13;
  
  public static final int TYPE_UINT64 = 4;
  
  protected final Class<T> clazz;
  
  protected final boolean repeated;
  
  public final int tag;
  
  protected final int type;
  
  @Deprecated
  public static <M extends ExtendableMessageNano<M>, T extends MessageNano> Extension<M, T> createMessageTyped(int paramInt1, Class<T> paramClass, int paramInt2) {
    return new Extension<>(paramInt1, paramClass, paramInt2, false);
  }
  
  public static <M extends ExtendableMessageNano<M>, T extends MessageNano> Extension<M, T> createMessageTyped(int paramInt, Class<T> paramClass, long paramLong) {
    return new Extension<>(paramInt, paramClass, (int)paramLong, false);
  }
  
  public static <M extends ExtendableMessageNano<M>, T extends MessageNano> Extension<M, T[]> createRepeatedMessageTyped(int paramInt, Class<T[]> paramClass, long paramLong) {
    return new Extension<>(paramInt, paramClass, (int)paramLong, true);
  }
  
  public static <M extends ExtendableMessageNano<M>, T> Extension<M, T> createPrimitiveTyped(int paramInt, Class<T> paramClass, long paramLong) {
    return new PrimitiveExtension<>(paramInt, paramClass, (int)paramLong, false, 0, 0);
  }
  
  public static <M extends ExtendableMessageNano<M>, T> Extension<M, T> createRepeatedPrimitiveTyped(int paramInt, Class<T> paramClass, long paramLong1, long paramLong2, long paramLong3) {
    return new PrimitiveExtension<>(paramInt, paramClass, (int)paramLong1, true, (int)paramLong2, (int)paramLong3);
  }
  
  private Extension(int paramInt1, Class<T> paramClass, int paramInt2, boolean paramBoolean) {
    this.type = paramInt1;
    this.clazz = paramClass;
    this.tag = paramInt2;
    this.repeated = paramBoolean;
  }
  
  final T getValueFrom(List<UnknownFieldData> paramList) {
    if (paramList == null)
      return null; 
    if (this.repeated) {
      paramList = (List<UnknownFieldData>)getRepeatedValueFrom(paramList);
    } else {
      paramList = (List<UnknownFieldData>)getSingularValueFrom(paramList);
    } 
    return (T)paramList;
  }
  
  private T getRepeatedValueFrom(List<UnknownFieldData> paramList) {
    ArrayList<Object> arrayList = new ArrayList();
    byte b;
    for (b = 0; b < paramList.size(); b++) {
      UnknownFieldData unknownFieldData = paramList.get(b);
      if (unknownFieldData.bytes.length != 0)
        readDataInto(unknownFieldData, arrayList); 
    } 
    int i = arrayList.size();
    if (i == 0)
      return null; 
    Class<T> clazz = this.clazz;
    clazz = (Class<T>)clazz.cast(Array.newInstance(clazz.getComponentType(), i));
    for (b = 0; b < i; b++)
      Array.set(clazz, b, arrayList.get(b)); 
    return (T)clazz;
  }
  
  private T getSingularValueFrom(List<UnknownFieldData> paramList) {
    if (paramList.isEmpty())
      return null; 
    UnknownFieldData unknownFieldData = paramList.get(paramList.size() - 1);
    return this.clazz.cast(readData(CodedInputByteBufferNano.newInstance(unknownFieldData.bytes)));
  }
  
  protected Object readData(CodedInputByteBufferNano paramCodedInputByteBufferNano) {
    Class<T> clazz;
    if (this.repeated) {
      clazz = (Class)this.clazz.getComponentType();
    } else {
      clazz = this.clazz;
    } 
    try {
      IllegalArgumentException illegalArgumentException;
      int i = this.type;
      if (i != 10) {
        if (i == 11) {
          MessageNano messageNano1 = (MessageNano)clazz.newInstance();
          paramCodedInputByteBufferNano.readMessage(messageNano1);
          return messageNano1;
        } 
        illegalArgumentException = new IllegalArgumentException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Unknown type ");
        stringBuilder.append(this.type);
        this(stringBuilder.toString());
        throw illegalArgumentException;
      } 
      MessageNano messageNano = (MessageNano)clazz.newInstance();
      illegalArgumentException.readGroup(messageNano, WireFormatNano.getTagFieldNumber(this.tag));
      return messageNano;
    } catch (InstantiationException instantiationException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error creating instance of class ");
      stringBuilder.append(clazz);
      throw new IllegalArgumentException(stringBuilder.toString(), instantiationException);
    } catch (IllegalAccessException illegalAccessException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error creating instance of class ");
      stringBuilder.append(clazz);
      throw new IllegalArgumentException(stringBuilder.toString(), illegalAccessException);
    } catch (IOException iOException) {
      throw new IllegalArgumentException("Error reading extension field", iOException);
    } 
  }
  
  protected void readDataInto(UnknownFieldData paramUnknownFieldData, List<Object> paramList) {
    paramList.add(readData(CodedInputByteBufferNano.newInstance(paramUnknownFieldData.bytes)));
  }
  
  void writeTo(Object paramObject, CodedOutputByteBufferNano paramCodedOutputByteBufferNano) throws IOException {
    if (this.repeated) {
      writeRepeatedData(paramObject, paramCodedOutputByteBufferNano);
    } else {
      writeSingularData(paramObject, paramCodedOutputByteBufferNano);
    } 
  }
  
  protected void writeSingularData(Object paramObject, CodedOutputByteBufferNano paramCodedOutputByteBufferNano) {
    try {
      IllegalArgumentException illegalArgumentException;
      paramCodedOutputByteBufferNano.writeRawVarint32(this.tag);
      int i = this.type;
      if (i != 10) {
        if (i == 11) {
          paramObject = paramObject;
          paramCodedOutputByteBufferNano.writeMessageNoTag((MessageNano)paramObject);
        } else {
          illegalArgumentException = new IllegalArgumentException();
          paramObject = new StringBuilder();
          super();
          paramObject.append("Unknown type ");
          paramObject.append(this.type);
          this(paramObject.toString());
          throw illegalArgumentException;
        } 
      } else {
        paramObject = paramObject;
        i = WireFormatNano.getTagFieldNumber(this.tag);
        illegalArgumentException.writeGroupNoTag((MessageNano)paramObject);
        illegalArgumentException.writeTag(i, 4);
      } 
      return;
    } catch (IOException iOException) {
      throw new IllegalStateException(iOException);
    } 
  }
  
  protected void writeRepeatedData(Object paramObject, CodedOutputByteBufferNano paramCodedOutputByteBufferNano) {
    int i = Array.getLength(paramObject);
    for (byte b = 0; b < i; b++) {
      Object object = Array.get(paramObject, b);
      if (object != null)
        writeSingularData(object, paramCodedOutputByteBufferNano); 
    } 
  }
  
  int computeSerializedSize(Object paramObject) {
    if (this.repeated)
      return computeRepeatedSerializedSize(paramObject); 
    return computeSingularSerializedSize(paramObject);
  }
  
  protected int computeRepeatedSerializedSize(Object paramObject) {
    int i = 0;
    int j = Array.getLength(paramObject);
    for (byte b = 0; b < j; b++, i = k) {
      Object object = Array.get(paramObject, b);
      int k = i;
      if (object != null)
        k = i + computeSingularSerializedSize(Array.get(paramObject, b)); 
    } 
    return i;
  }
  
  protected int computeSingularSerializedSize(Object paramObject) {
    int i = WireFormatNano.getTagFieldNumber(this.tag);
    int j = this.type;
    if (j != 10) {
      if (j == 11) {
        paramObject = paramObject;
        return CodedOutputByteBufferNano.computeMessageSize(i, (MessageNano)paramObject);
      } 
      paramObject = new StringBuilder();
      paramObject.append("Unknown type ");
      paramObject.append(this.type);
      throw new IllegalArgumentException(paramObject.toString());
    } 
    paramObject = paramObject;
    return CodedOutputByteBufferNano.computeGroupSize(i, (MessageNano)paramObject);
  }
  
  class PrimitiveExtension<M extends ExtendableMessageNano<M>, T> extends Extension<M, T> {
    private final int nonPackedTag;
    
    private final int packedTag;
    
    public PrimitiveExtension(Extension this$0, Class<T> param1Class, int param1Int1, boolean param1Boolean, int param1Int2, int param1Int3) {
      super(this$0, param1Class, param1Int1, param1Boolean);
      this.nonPackedTag = param1Int2;
      this.packedTag = param1Int3;
    }
    
    protected Object readData(CodedInputByteBufferNano param1CodedInputByteBufferNano) {
      try {
        return param1CodedInputByteBufferNano.readPrimitiveField(this.type);
      } catch (IOException iOException) {
        throw new IllegalArgumentException("Error reading extension field", iOException);
      } 
    }
    
    protected void readDataInto(UnknownFieldData param1UnknownFieldData, List<Object> param1List) {
      if (param1UnknownFieldData.tag == this.nonPackedTag) {
        param1List.add(readData(CodedInputByteBufferNano.newInstance(param1UnknownFieldData.bytes)));
      } else {
        byte[] arrayOfByte = param1UnknownFieldData.bytes;
        CodedInputByteBufferNano codedInputByteBufferNano = CodedInputByteBufferNano.newInstance(arrayOfByte);
        try {
          codedInputByteBufferNano.pushLimit(codedInputByteBufferNano.readRawVarint32());
          while (!codedInputByteBufferNano.isAtEnd())
            param1List.add(readData(codedInputByteBufferNano)); 
          return;
        } catch (IOException iOException) {
          throw new IllegalArgumentException("Error reading extension field", iOException);
        } 
      } 
    }
    
    protected final void writeSingularData(Object param1Object, CodedOutputByteBufferNano param1CodedOutputByteBufferNano) {
      try {
        IllegalArgumentException illegalArgumentException;
        param1CodedOutputByteBufferNano.writeRawVarint32(this.tag);
        switch (this.type) {
          default:
            illegalArgumentException = new IllegalArgumentException();
            param1Object = new StringBuilder();
            super();
            param1Object.append("Unknown type ");
            param1Object.append(this.type);
            this(param1Object.toString());
            throw illegalArgumentException;
          case 18:
            param1Object = param1Object;
            illegalArgumentException.writeSInt64NoTag(param1Object.longValue());
            return;
          case 17:
            param1Object = param1Object;
            illegalArgumentException.writeSInt32NoTag(param1Object.intValue());
            return;
          case 16:
            param1Object = param1Object;
            illegalArgumentException.writeSFixed64NoTag(param1Object.longValue());
            return;
          case 15:
            param1Object = param1Object;
            illegalArgumentException.writeSFixed32NoTag(param1Object.intValue());
            return;
          case 14:
            param1Object = param1Object;
            illegalArgumentException.writeEnumNoTag(param1Object.intValue());
            return;
          case 13:
            param1Object = param1Object;
            illegalArgumentException.writeUInt32NoTag(param1Object.intValue());
            return;
          case 12:
            param1Object = param1Object;
            illegalArgumentException.writeBytesNoTag((byte[])param1Object);
            return;
          case 9:
            param1Object = param1Object;
            illegalArgumentException.writeStringNoTag((String)param1Object);
            return;
          case 8:
            param1Object = param1Object;
            illegalArgumentException.writeBoolNoTag(param1Object.booleanValue());
            return;
          case 7:
            param1Object = param1Object;
            illegalArgumentException.writeFixed32NoTag(param1Object.intValue());
            return;
          case 6:
            param1Object = param1Object;
            illegalArgumentException.writeFixed64NoTag(param1Object.longValue());
            return;
          case 5:
            param1Object = param1Object;
            illegalArgumentException.writeInt32NoTag(param1Object.intValue());
            return;
          case 4:
            param1Object = param1Object;
            illegalArgumentException.writeUInt64NoTag(param1Object.longValue());
            return;
          case 3:
            param1Object = param1Object;
            illegalArgumentException.writeInt64NoTag(param1Object.longValue());
            return;
          case 2:
            param1Object = param1Object;
            illegalArgumentException.writeFloatNoTag(param1Object.floatValue());
            return;
          case 1:
            break;
        } 
        param1Object = param1Object;
        illegalArgumentException.writeDoubleNoTag(param1Object.doubleValue());
        return;
      } catch (IOException iOException) {
        throw new IllegalStateException(iOException);
      } 
    }
    
    protected void writeRepeatedData(Object param1Object, CodedOutputByteBufferNano param1CodedOutputByteBufferNano) {
      if (this.tag == this.nonPackedTag) {
        super.writeRepeatedData(param1Object, param1CodedOutputByteBufferNano);
      } else {
        if (this.tag == this.packedTag) {
          int i = Array.getLength(param1Object);
          int j = computePackedDataSize(param1Object);
          try {
            StringBuilder stringBuilder;
            param1CodedOutputByteBufferNano.writeRawVarint32(this.tag);
            param1CodedOutputByteBufferNano.writeRawVarint32(j);
            switch (this.type) {
              default:
                param1Object = new IllegalArgumentException();
                stringBuilder = new StringBuilder();
                this();
                stringBuilder.append("Unpackable type ");
                stringBuilder.append(this.type);
                super(stringBuilder.toString());
                throw param1Object;
              case 18:
                for (j = 0; j < i; j++)
                  stringBuilder.writeSInt64NoTag(Array.getLong(param1Object, j)); 
                return;
              case 17:
                for (j = 0; j < i; j++)
                  stringBuilder.writeSInt32NoTag(Array.getInt(param1Object, j)); 
                return;
              case 16:
                for (j = 0; j < i; j++)
                  stringBuilder.writeSFixed64NoTag(Array.getLong(param1Object, j)); 
                return;
              case 15:
                for (j = 0; j < i; j++)
                  stringBuilder.writeSFixed32NoTag(Array.getInt(param1Object, j)); 
                return;
              case 14:
                for (j = 0; j < i; j++)
                  stringBuilder.writeEnumNoTag(Array.getInt(param1Object, j)); 
                return;
              case 13:
                for (j = 0; j < i; j++)
                  stringBuilder.writeUInt32NoTag(Array.getInt(param1Object, j)); 
                return;
              case 8:
                for (j = 0; j < i; j++)
                  stringBuilder.writeBoolNoTag(Array.getBoolean(param1Object, j)); 
                return;
              case 7:
                for (j = 0; j < i; j++)
                  stringBuilder.writeFixed32NoTag(Array.getInt(param1Object, j)); 
                return;
              case 6:
                for (j = 0; j < i; j++)
                  stringBuilder.writeFixed64NoTag(Array.getLong(param1Object, j)); 
                return;
              case 5:
                for (j = 0; j < i; j++)
                  stringBuilder.writeInt32NoTag(Array.getInt(param1Object, j)); 
                return;
              case 4:
                for (j = 0; j < i; j++)
                  stringBuilder.writeUInt64NoTag(Array.getLong(param1Object, j)); 
                return;
              case 3:
                for (j = 0; j < i; j++)
                  stringBuilder.writeInt64NoTag(Array.getLong(param1Object, j)); 
                return;
              case 2:
                for (j = 0; j < i; j++)
                  stringBuilder.writeFloatNoTag(Array.getFloat(param1Object, j)); 
                return;
              case 1:
                break;
            } 
            for (j = 0; j < i; j++)
              stringBuilder.writeDoubleNoTag(Array.getDouble(param1Object, j)); 
            return;
          } catch (IOException iOException) {
            throw new IllegalStateException(iOException);
          } 
        } 
        param1Object = new StringBuilder();
        param1Object.append("Unexpected repeated extension tag ");
        param1Object.append(this.tag);
        param1Object.append(", unequal to both non-packed variant ");
        param1Object.append(this.nonPackedTag);
        param1Object.append(" and packed variant ");
        param1Object.append(this.packedTag);
        throw new IllegalArgumentException(param1Object.toString());
      } 
    }
    
    private int computePackedDataSize(Object param1Object) {
      byte b2;
      int i = 0;
      boolean bool1 = false, bool2 = false, bool3 = false, bool4 = false;
      int j = 0;
      byte b1 = 0;
      int k = Array.getLength(param1Object);
      switch (this.type) {
        default:
          param1Object = new StringBuilder();
          param1Object.append("Unexpected non-packable type ");
          param1Object.append(this.type);
          throw new IllegalArgumentException(param1Object.toString());
        case 18:
          for (b2 = 0, j = b1; b2 < k; j += CodedOutputByteBufferNano.computeSInt64SizeNoTag(l), b2++)
            long l = Array.getLong(param1Object, b2); 
          return j;
        case 17:
          for (b2 = 0, j = i; b2 < k; j += CodedOutputByteBufferNano.computeSInt32SizeNoTag(i), b2++)
            i = Array.getInt(param1Object, b2); 
          return j;
        case 14:
          for (b2 = 0, j = bool1; b2 < k; j += CodedOutputByteBufferNano.computeEnumSizeNoTag(i), b2++)
            i = Array.getInt(param1Object, b2); 
          return j;
        case 13:
          for (b2 = 0, j = bool2; b2 < k; j += CodedOutputByteBufferNano.computeUInt32SizeNoTag(i), b2++)
            i = Array.getInt(param1Object, b2); 
          return j;
        case 8:
          j = k;
          return j;
        case 5:
          for (b2 = 0, j = bool3; b2 < k; j += CodedOutputByteBufferNano.computeInt32SizeNoTag(i), b2++)
            i = Array.getInt(param1Object, b2); 
          return j;
        case 4:
          for (b2 = 0, j = bool4; b2 < k; j += CodedOutputByteBufferNano.computeUInt64SizeNoTag(l), b2++)
            long l = Array.getLong(param1Object, b2); 
          return j;
        case 3:
          for (b2 = 0; b2 < k; j += CodedOutputByteBufferNano.computeInt64SizeNoTag(l), b2++)
            long l = Array.getLong(param1Object, b2); 
          return j;
        case 2:
        case 7:
        case 15:
          j = k * 4;
          return j;
        case 1:
        case 6:
        case 16:
          break;
      } 
      j = k * 8;
      return j;
    }
    
    protected int computeRepeatedSerializedSize(Object param1Object) {
      if (this.tag == this.nonPackedTag)
        return super.computeRepeatedSerializedSize(param1Object); 
      if (this.tag == this.packedTag) {
        int i = computePackedDataSize(param1Object);
        int j = CodedOutputByteBufferNano.computeRawVarint32Size(i);
        return CodedOutputByteBufferNano.computeRawVarint32Size(this.tag) + j + i;
      } 
      param1Object = new StringBuilder();
      param1Object.append("Unexpected repeated extension tag ");
      param1Object.append(this.tag);
      param1Object.append(", unequal to both non-packed variant ");
      param1Object.append(this.nonPackedTag);
      param1Object.append(" and packed variant ");
      param1Object.append(this.packedTag);
      throw new IllegalArgumentException(param1Object.toString());
    }
    
    protected final int computeSingularSerializedSize(Object param1Object) {
      long l;
      int j, i = WireFormatNano.getTagFieldNumber(this.tag);
      switch (this.type) {
        default:
          param1Object = new StringBuilder();
          param1Object.append("Unknown type ");
          param1Object.append(this.type);
          throw new IllegalArgumentException(param1Object.toString());
        case 18:
          param1Object = param1Object;
          return CodedOutputByteBufferNano.computeSInt64Size(i, param1Object.longValue());
        case 17:
          param1Object = param1Object;
          return CodedOutputByteBufferNano.computeSInt32Size(i, param1Object.intValue());
        case 16:
          param1Object = param1Object;
          l = param1Object.longValue();
          return CodedOutputByteBufferNano.computeSFixed64Size(i, l);
        case 15:
          param1Object = param1Object;
          j = param1Object.intValue();
          return CodedOutputByteBufferNano.computeSFixed32Size(i, j);
        case 14:
          param1Object = param1Object;
          return CodedOutputByteBufferNano.computeEnumSize(i, param1Object.intValue());
        case 13:
          param1Object = param1Object;
          return CodedOutputByteBufferNano.computeUInt32Size(i, param1Object.intValue());
        case 12:
          param1Object = param1Object;
          return CodedOutputByteBufferNano.computeBytesSize(i, (byte[])param1Object);
        case 9:
          param1Object = param1Object;
          return CodedOutputByteBufferNano.computeStringSize(i, (String)param1Object);
        case 8:
          param1Object = param1Object;
          return CodedOutputByteBufferNano.computeBoolSize(i, param1Object.booleanValue());
        case 7:
          param1Object = param1Object;
          return CodedOutputByteBufferNano.computeFixed32Size(i, param1Object.intValue());
        case 6:
          param1Object = param1Object;
          return CodedOutputByteBufferNano.computeFixed64Size(i, param1Object.longValue());
        case 5:
          param1Object = param1Object;
          return CodedOutputByteBufferNano.computeInt32Size(i, param1Object.intValue());
        case 4:
          param1Object = param1Object;
          return CodedOutputByteBufferNano.computeUInt64Size(i, param1Object.longValue());
        case 3:
          param1Object = param1Object;
          return CodedOutputByteBufferNano.computeInt64Size(i, param1Object.longValue());
        case 2:
          param1Object = param1Object;
          return CodedOutputByteBufferNano.computeFloatSize(i, param1Object.floatValue());
        case 1:
          break;
      } 
      param1Object = param1Object;
      return CodedOutputByteBufferNano.computeDoubleSize(i, param1Object.doubleValue());
    }
  }
}
