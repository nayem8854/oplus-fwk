package com.android.framework.protobuf.nano;

import java.io.IOException;
import java.util.Arrays;

final class UnknownFieldData {
  final byte[] bytes;
  
  final int tag;
  
  UnknownFieldData(int paramInt, byte[] paramArrayOfbyte) {
    this.tag = paramInt;
    this.bytes = paramArrayOfbyte;
  }
  
  int computeSerializedSize() {
    int i = CodedOutputByteBufferNano.computeRawVarint32Size(this.tag);
    int j = this.bytes.length;
    return 0 + i + j;
  }
  
  void writeTo(CodedOutputByteBufferNano paramCodedOutputByteBufferNano) throws IOException {
    paramCodedOutputByteBufferNano.writeRawVarint32(this.tag);
    paramCodedOutputByteBufferNano.writeRawBytes(this.bytes);
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (paramObject == this)
      return true; 
    if (!(paramObject instanceof UnknownFieldData))
      return false; 
    paramObject = paramObject;
    if (this.tag != ((UnknownFieldData)paramObject).tag || !Arrays.equals(this.bytes, ((UnknownFieldData)paramObject).bytes))
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    int i = this.tag;
    int j = Arrays.hashCode(this.bytes);
    return (17 * 31 + i) * 31 + j;
  }
}
