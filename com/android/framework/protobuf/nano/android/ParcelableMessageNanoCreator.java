package com.android.framework.protobuf.nano.android;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.android.framework.protobuf.nano.InvalidProtocolBufferNanoException;
import com.android.framework.protobuf.nano.MessageNano;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;

public final class ParcelableMessageNanoCreator<T extends MessageNano> implements Parcelable.Creator<T> {
  private static final String TAG = "PMNCreator";
  
  private final Class<T> mClazz;
  
  public ParcelableMessageNanoCreator(Class<T> paramClass) {
    this.mClazz = paramClass;
  }
  
  public T createFromParcel(Parcel paramParcel) {
    MessageNano messageNano1, messageNano8;
    String str = paramParcel.readString();
    byte[] arrayOfByte = paramParcel.createByteArray();
    paramParcel = null;
    MessageNano messageNano2 = null, messageNano3 = null, messageNano4 = null, messageNano5 = null, messageNano6 = null;
    MessageNano messageNano7 = messageNano6;
    Parcel parcel = paramParcel;
    MessageNano messageNano9 = messageNano2, messageNano10 = messageNano3, messageNano11 = messageNano4, messageNano12 = messageNano5;
    try {
      Class<?> clazz = Class.forName(str, false, getClass().getClassLoader());
      messageNano7 = messageNano6;
      parcel = paramParcel;
      messageNano9 = messageNano2;
      messageNano10 = messageNano3;
      messageNano11 = messageNano4;
      messageNano12 = messageNano5;
      clazz = clazz.asSubclass(MessageNano.class);
      messageNano7 = messageNano6;
      parcel = paramParcel;
      messageNano9 = messageNano2;
      messageNano10 = messageNano3;
      messageNano11 = messageNano4;
      messageNano12 = messageNano5;
      clazz = clazz.getConstructor(new Class[0]).newInstance(new Object[0]);
      messageNano7 = messageNano6;
      parcel = paramParcel;
      messageNano9 = messageNano2;
      messageNano10 = messageNano3;
      messageNano11 = messageNano4;
      messageNano12 = messageNano5;
      messageNano1 = (MessageNano)clazz;
      messageNano7 = messageNano1;
      messageNano8 = messageNano1;
      messageNano9 = messageNano1;
      messageNano10 = messageNano1;
      messageNano11 = messageNano1;
      messageNano12 = messageNano1;
      MessageNano.mergeFrom(messageNano1, arrayOfByte);
    } catch (ClassNotFoundException classNotFoundException) {
      Log.e("PMNCreator", "Exception trying to create proto from parcel", classNotFoundException);
      messageNano1 = messageNano12;
    } catch (NoSuchMethodException noSuchMethodException) {
      Log.e("PMNCreator", "Exception trying to create proto from parcel", noSuchMethodException);
      messageNano1 = messageNano11;
    } catch (InvocationTargetException invocationTargetException) {
      Log.e("PMNCreator", "Exception trying to create proto from parcel", invocationTargetException);
      messageNano1 = messageNano10;
    } catch (IllegalAccessException illegalAccessException) {
      Log.e("PMNCreator", "Exception trying to create proto from parcel", illegalAccessException);
      messageNano1 = messageNano9;
    } catch (InstantiationException instantiationException) {
      Log.e("PMNCreator", "Exception trying to create proto from parcel", instantiationException);
      messageNano1 = messageNano8;
    } catch (InvalidProtocolBufferNanoException invalidProtocolBufferNanoException) {
      Log.e("PMNCreator", "Exception trying to create proto from parcel", invalidProtocolBufferNanoException);
      messageNano1 = messageNano7;
    } 
    return (T)messageNano1;
  }
  
  public T[] newArray(int paramInt) {
    return (T[])Array.newInstance(this.mClazz, paramInt);
  }
  
  static <T extends MessageNano> void writeToParcel(Class<T> paramClass, MessageNano paramMessageNano, Parcel paramParcel) {
    paramParcel.writeString(paramClass.getName());
    paramParcel.writeByteArray(MessageNano.toByteArray(paramMessageNano));
  }
}
