package com.android.framework.protobuf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

final class FieldSet<FieldDescriptorType extends FieldSet.FieldDescriptorLite<FieldDescriptorType>> {
  private boolean isImmutable;
  
  private boolean hasLazyField = false;
  
  private final SmallSortedMap<FieldDescriptorType, Object> fields;
  
  private FieldSet() {
    this.fields = SmallSortedMap.newFieldMap(16);
  }
  
  private FieldSet(boolean paramBoolean) {
    this.fields = SmallSortedMap.newFieldMap(0);
    makeImmutable();
  }
  
  public static <T extends FieldDescriptorLite<T>> FieldSet<T> newFieldSet() {
    return new FieldSet<>();
  }
  
  public static <T extends FieldDescriptorLite<T>> FieldSet<T> emptySet() {
    return DEFAULT_INSTANCE;
  }
  
  private static final FieldSet DEFAULT_INSTANCE = new FieldSet(true);
  
  boolean isEmpty() {
    return this.fields.isEmpty();
  }
  
  public void makeImmutable() {
    if (this.isImmutable)
      return; 
    this.fields.makeImmutable();
    this.isImmutable = true;
  }
  
  public boolean isImmutable() {
    return this.isImmutable;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof FieldSet))
      return false; 
    paramObject = paramObject;
    return this.fields.equals(((FieldSet)paramObject).fields);
  }
  
  public int hashCode() {
    return this.fields.hashCode();
  }
  
  public FieldSet<FieldDescriptorType> clone() {
    FieldSet<FieldDescriptorLite> fieldSet = newFieldSet();
    for (byte b = 0; b < this.fields.getNumArrayEntries(); b++) {
      Map.Entry<FieldDescriptorType, Object> entry = this.fields.getArrayEntryAt(b);
      FieldDescriptorLite fieldDescriptorLite = (FieldDescriptorLite)entry.getKey();
      fieldSet.setField(fieldDescriptorLite, entry.getValue());
    } 
    for (Map.Entry<FieldDescriptorType, Object> entry : this.fields.getOverflowEntries()) {
      FieldDescriptorLite fieldDescriptorLite = (FieldDescriptorLite)entry.getKey();
      fieldSet.setField(fieldDescriptorLite, entry.getValue());
    } 
    fieldSet.hasLazyField = this.hasLazyField;
    return (FieldSet)fieldSet;
  }
  
  public void clear() {
    this.fields.clear();
    this.hasLazyField = false;
  }
  
  public Map<FieldDescriptorType, Object> getAllFields() {
    Map<FieldDescriptorType, Object> map;
    if (this.hasLazyField) {
      SmallSortedMap<FieldDescriptorLite, Object> smallSortedMap = SmallSortedMap.newFieldMap(16);
      for (byte b = 0; b < this.fields.getNumArrayEntries(); b++)
        cloneFieldEntry((Map)smallSortedMap, this.fields.getArrayEntryAt(b)); 
      for (Map.Entry<FieldDescriptorType, Object> entry : this.fields.getOverflowEntries())
        cloneFieldEntry((Map)smallSortedMap, entry); 
      if (this.fields.isImmutable())
        smallSortedMap.makeImmutable(); 
      return (Map)smallSortedMap;
    } 
    if (this.fields.isImmutable()) {
      map = this.fields;
    } else {
      map = Collections.unmodifiableMap(this.fields);
    } 
    return map;
  }
  
  private void cloneFieldEntry(Map<FieldDescriptorType, Object> paramMap, Map.Entry<FieldDescriptorType, Object> paramEntry) {
    FieldDescriptorLite fieldDescriptorLite = (FieldDescriptorLite)paramEntry.getKey();
    paramEntry = (Map.Entry<FieldDescriptorType, Object>)paramEntry.getValue();
    if (paramEntry instanceof LazyField) {
      paramMap.put((FieldDescriptorType)fieldDescriptorLite, ((LazyField)paramEntry).getValue());
    } else {
      paramMap.put((FieldDescriptorType)fieldDescriptorLite, paramEntry);
    } 
  }
  
  public Iterator<Map.Entry<FieldDescriptorType, Object>> iterator() {
    if (this.hasLazyField)
      return new LazyField.LazyIterator<>(this.fields.entrySet().iterator()); 
    return this.fields.entrySet().iterator();
  }
  
  Iterator<Map.Entry<FieldDescriptorType, Object>> descendingIterator() {
    if (this.hasLazyField)
      return new LazyField.LazyIterator<>(this.fields.descendingEntrySet().iterator()); 
    return this.fields.descendingEntrySet().iterator();
  }
  
  public boolean hasField(FieldDescriptorType paramFieldDescriptorType) {
    if (!paramFieldDescriptorType.isRepeated()) {
      boolean bool;
      if (this.fields.get(paramFieldDescriptorType) != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    } 
    throw new IllegalArgumentException("hasField() can only be called on non-repeated fields.");
  }
  
  public Object getField(FieldDescriptorType paramFieldDescriptorType) {
    paramFieldDescriptorType = (FieldDescriptorType)this.fields.get(paramFieldDescriptorType);
    if (paramFieldDescriptorType instanceof LazyField)
      return ((LazyField)paramFieldDescriptorType).getValue(); 
    return paramFieldDescriptorType;
  }
  
  public void setField(FieldDescriptorType paramFieldDescriptorType, Object paramObject) {
    if (paramFieldDescriptorType.isRepeated()) {
      if (paramObject instanceof List) {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll((List)paramObject);
        for (paramObject = arrayList.iterator(); paramObject.hasNext(); ) {
          Object object = paramObject.next();
          verifyType(paramFieldDescriptorType.getLiteType(), object);
        } 
        paramObject = arrayList;
      } else {
        throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
      } 
    } else {
      verifyType(paramFieldDescriptorType.getLiteType(), paramObject);
    } 
    if (paramObject instanceof LazyField)
      this.hasLazyField = true; 
    this.fields.put(paramFieldDescriptorType, paramObject);
  }
  
  public void clearField(FieldDescriptorType paramFieldDescriptorType) {
    this.fields.remove(paramFieldDescriptorType);
    if (this.fields.isEmpty())
      this.hasLazyField = false; 
  }
  
  public int getRepeatedFieldCount(FieldDescriptorType paramFieldDescriptorType) {
    if (paramFieldDescriptorType.isRepeated()) {
      paramFieldDescriptorType = (FieldDescriptorType)getField(paramFieldDescriptorType);
      if (paramFieldDescriptorType == null)
        return 0; 
      return ((List)paramFieldDescriptorType).size();
    } 
    throw new IllegalArgumentException("getRepeatedField() can only be called on repeated fields.");
  }
  
  public Object getRepeatedField(FieldDescriptorType paramFieldDescriptorType, int paramInt) {
    if (paramFieldDescriptorType.isRepeated()) {
      paramFieldDescriptorType = (FieldDescriptorType)getField(paramFieldDescriptorType);
      if (paramFieldDescriptorType != null)
        return ((List)paramFieldDescriptorType).get(paramInt); 
      throw new IndexOutOfBoundsException();
    } 
    throw new IllegalArgumentException("getRepeatedField() can only be called on repeated fields.");
  }
  
  public void setRepeatedField(FieldDescriptorType paramFieldDescriptorType, int paramInt, Object paramObject) {
    if (paramFieldDescriptorType.isRepeated()) {
      Object object = getField(paramFieldDescriptorType);
      if (object != null) {
        verifyType(paramFieldDescriptorType.getLiteType(), paramObject);
        ((List<Object>)object).set(paramInt, paramObject);
        return;
      } 
      throw new IndexOutOfBoundsException();
    } 
    throw new IllegalArgumentException("getRepeatedField() can only be called on repeated fields.");
  }
  
  public void addRepeatedField(FieldDescriptorType paramFieldDescriptorType, Object paramObject) {
    if (paramFieldDescriptorType.isRepeated()) {
      List<Object> list;
      verifyType(paramFieldDescriptorType.getLiteType(), paramObject);
      Object object = getField(paramFieldDescriptorType);
      if (object == null) {
        object = new ArrayList();
        this.fields.put(paramFieldDescriptorType, object);
        paramFieldDescriptorType = (FieldDescriptorType)object;
      } else {
        list = (List)object;
      } 
      list.add(paramObject);
      return;
    } 
    throw new IllegalArgumentException("addRepeatedField() can only be called on repeated fields.");
  }
  
  private static void verifyType(WireFormat.FieldType paramFieldType, Object paramObject) {
    // Byte code:
    //   0: aload_1
    //   1: invokestatic checkNotNull : (Ljava/lang/Object;)Ljava/lang/Object;
    //   4: pop
    //   5: iconst_0
    //   6: istore_2
    //   7: getstatic com/android/framework/protobuf/FieldSet$1.$SwitchMap$com$google$protobuf$WireFormat$JavaType : [I
    //   10: aload_0
    //   11: invokevirtual getJavaType : ()Lcom/android/framework/protobuf/WireFormat$JavaType;
    //   14: invokevirtual ordinal : ()I
    //   17: iaload
    //   18: istore_3
    //   19: iconst_0
    //   20: istore #4
    //   22: iconst_0
    //   23: istore #5
    //   25: iconst_0
    //   26: istore #6
    //   28: iload_3
    //   29: tableswitch default -> 80, 1 -> 199, 2 -> 190, 3 -> 181, 4 -> 172, 5 -> 163, 6 -> 154, 7 -> 130, 8 -> 110, 9 -> 86
    //   80: iload_2
    //   81: istore #4
    //   83: goto -> 205
    //   86: aload_1
    //   87: instanceof com/android/framework/protobuf/MessageLite
    //   90: ifne -> 104
    //   93: iload #6
    //   95: istore #4
    //   97: aload_1
    //   98: instanceof com/android/framework/protobuf/LazyField
    //   101: ifeq -> 107
    //   104: iconst_1
    //   105: istore #4
    //   107: goto -> 205
    //   110: aload_1
    //   111: instanceof java/lang/Integer
    //   114: ifne -> 124
    //   117: aload_1
    //   118: instanceof com/android/framework/protobuf/Internal$EnumLite
    //   121: ifeq -> 127
    //   124: iconst_1
    //   125: istore #4
    //   127: goto -> 205
    //   130: aload_1
    //   131: instanceof com/android/framework/protobuf/ByteString
    //   134: ifne -> 148
    //   137: iload #5
    //   139: istore #4
    //   141: aload_1
    //   142: instanceof [B
    //   145: ifeq -> 151
    //   148: iconst_1
    //   149: istore #4
    //   151: goto -> 205
    //   154: aload_1
    //   155: instanceof java/lang/String
    //   158: istore #4
    //   160: goto -> 205
    //   163: aload_1
    //   164: instanceof java/lang/Boolean
    //   167: istore #4
    //   169: goto -> 205
    //   172: aload_1
    //   173: instanceof java/lang/Double
    //   176: istore #4
    //   178: goto -> 205
    //   181: aload_1
    //   182: instanceof java/lang/Float
    //   185: istore #4
    //   187: goto -> 205
    //   190: aload_1
    //   191: instanceof java/lang/Long
    //   194: istore #4
    //   196: goto -> 205
    //   199: aload_1
    //   200: instanceof java/lang/Integer
    //   203: istore #4
    //   205: iload #4
    //   207: ifeq -> 211
    //   210: return
    //   211: new java/lang/IllegalArgumentException
    //   214: dup
    //   215: ldc_w 'Wrong object type used with protocol message reflection.'
    //   218: invokespecial <init> : (Ljava/lang/String;)V
    //   221: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #377	-> 0
    //   #379	-> 5
    //   #380	-> 7
    //   #408	-> 86
    //   #404	-> 110
    //   #405	-> 127
    //   #400	-> 130
    //   #401	-> 151
    //   #397	-> 154
    //   #398	-> 160
    //   #394	-> 163
    //   #395	-> 169
    //   #391	-> 172
    //   #392	-> 178
    //   #388	-> 181
    //   #389	-> 187
    //   #385	-> 190
    //   #386	-> 196
    //   #382	-> 199
    //   #383	-> 205
    //   #412	-> 205
    //   #423	-> 210
    //   #420	-> 211
  }
  
  public boolean isInitialized() {
    for (byte b = 0; b < this.fields.getNumArrayEntries(); b++) {
      if (!isInitialized(this.fields.getArrayEntryAt(b)))
        return false; 
    } 
    for (Map.Entry<FieldDescriptorType, Object> entry : this.fields.getOverflowEntries()) {
      if (!isInitialized(entry))
        return false; 
    } 
    return true;
  }
  
  private boolean isInitialized(Map.Entry<FieldDescriptorType, Object> paramEntry) {
    FieldDescriptorLite fieldDescriptorLite = (FieldDescriptorLite)paramEntry.getKey();
    if (fieldDescriptorLite.getLiteJavaType() == WireFormat.JavaType.MESSAGE)
      if (fieldDescriptorLite.isRepeated()) {
        for (MessageLite messageLite : paramEntry.getValue()) {
          if (!messageLite.isInitialized())
            return false; 
        } 
      } else {
        paramEntry = (Map.Entry<FieldDescriptorType, Object>)paramEntry.getValue();
        if (paramEntry instanceof MessageLite) {
          if (!((MessageLite)paramEntry).isInitialized())
            return false; 
        } else {
          if (paramEntry instanceof LazyField)
            return true; 
          throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        } 
      }  
    return true;
  }
  
  static int getWireFormatForFieldType(WireFormat.FieldType paramFieldType, boolean paramBoolean) {
    if (paramBoolean)
      return 2; 
    return paramFieldType.getWireType();
  }
  
  public void mergeFrom(FieldSet<FieldDescriptorType> paramFieldSet) {
    for (byte b = 0; b < paramFieldSet.fields.getNumArrayEntries(); b++)
      mergeFromField(paramFieldSet.fields.getArrayEntryAt(b)); 
    for (Map.Entry<FieldDescriptorType, Object> entry : paramFieldSet.fields.getOverflowEntries())
      mergeFromField(entry); 
  }
  
  private Object cloneIfMutable(Object paramObject) {
    if (paramObject instanceof byte[]) {
      paramObject = paramObject;
      byte[] arrayOfByte = new byte[paramObject.length];
      System.arraycopy(paramObject, 0, arrayOfByte, 0, paramObject.length);
      return arrayOfByte;
    } 
    return paramObject;
  }
  
  private void mergeFromField(Map.Entry<FieldDescriptorType, Object> paramEntry) {
    FieldDescriptorLite fieldDescriptorLite = (FieldDescriptorLite)paramEntry.getKey();
    Object object2 = paramEntry.getValue();
    Object object1 = object2;
    if (object2 instanceof LazyField)
      object1 = ((LazyField)object2).getValue(); 
    if (fieldDescriptorLite.isRepeated()) {
      object = getField((FieldDescriptorType)fieldDescriptorLite);
      object2 = object;
      if (object == null)
        object2 = new ArrayList(); 
      for (Object object : object1)
        ((List<Object>)object2).add(cloneIfMutable(object)); 
      this.fields.put((FieldDescriptorType)fieldDescriptorLite, object2);
    } else if (fieldDescriptorLite.getLiteJavaType() == WireFormat.JavaType.MESSAGE) {
      object2 = getField((FieldDescriptorType)fieldDescriptorLite);
      if (object2 == null) {
        this.fields.put((FieldDescriptorType)fieldDescriptorLite, cloneIfMutable(object1));
      } else {
        object2 = object2;
        object1 = fieldDescriptorLite.internalMergeFrom(object2.toBuilder(), (MessageLite)object1);
        object1 = object1.build();
        this.fields.put((FieldDescriptorType)fieldDescriptorLite, object1);
      } 
    } else {
      this.fields.put((FieldDescriptorType)fieldDescriptorLite, cloneIfMutable(object1));
    } 
  }
  
  public static Object readPrimitiveField(CodedInputStream paramCodedInputStream, WireFormat.FieldType paramFieldType, boolean paramBoolean) throws IOException {
    if (paramBoolean)
      return WireFormat.readPrimitiveField(paramCodedInputStream, paramFieldType, WireFormat.Utf8Validation.STRICT); 
    return WireFormat.readPrimitiveField(paramCodedInputStream, paramFieldType, WireFormat.Utf8Validation.LOOSE);
  }
  
  public void writeTo(CodedOutputStream paramCodedOutputStream) throws IOException {
    for (byte b = 0; b < this.fields.getNumArrayEntries(); b++) {
      Map.Entry<FieldDescriptorType, Object> entry = this.fields.getArrayEntryAt(b);
      writeField((FieldDescriptorLite)entry.getKey(), entry.getValue(), paramCodedOutputStream);
    } 
    for (Map.Entry<FieldDescriptorType, Object> entry : this.fields.getOverflowEntries())
      writeField((FieldDescriptorLite)entry.getKey(), entry.getValue(), paramCodedOutputStream); 
  }
  
  public void writeMessageSetTo(CodedOutputStream paramCodedOutputStream) throws IOException {
    for (byte b = 0; b < this.fields.getNumArrayEntries(); b++)
      writeMessageSetTo(this.fields.getArrayEntryAt(b), paramCodedOutputStream); 
    for (Map.Entry<FieldDescriptorType, Object> entry : this.fields.getOverflowEntries())
      writeMessageSetTo(entry, paramCodedOutputStream); 
  }
  
  private void writeMessageSetTo(Map.Entry<FieldDescriptorType, Object> paramEntry, CodedOutputStream paramCodedOutputStream) throws IOException {
    Object object = (FieldDescriptorLite)paramEntry.getKey();
    if (object.getLiteJavaType() == WireFormat.JavaType.MESSAGE && 
      !object.isRepeated() && 
      !object.isPacked()) {
      Object object1 = paramEntry.getValue();
      object = object1;
      if (object1 instanceof LazyField)
        object = ((LazyField)object1).getValue(); 
      paramCodedOutputStream.writeMessageSetExtension(((FieldDescriptorLite)paramEntry.getKey()).getNumber(), (MessageLite)object);
    } else {
      writeField((FieldDescriptorLite<?>)object, paramEntry.getValue(), paramCodedOutputStream);
    } 
  }
  
  static void writeElement(CodedOutputStream paramCodedOutputStream, WireFormat.FieldType paramFieldType, int paramInt, Object paramObject) throws IOException {
    if (paramFieldType == WireFormat.FieldType.GROUP) {
      paramCodedOutputStream.writeGroup(paramInt, (MessageLite)paramObject);
    } else {
      paramCodedOutputStream.writeTag(paramInt, getWireFormatForFieldType(paramFieldType, false));
      writeElementNoTag(paramCodedOutputStream, paramFieldType, paramObject);
    } 
  }
  
  static void writeElementNoTag(CodedOutputStream paramCodedOutputStream, WireFormat.FieldType paramFieldType, Object paramObject) throws IOException {
    switch (paramFieldType) {
      default:
        return;
      case ENUM:
        if (paramObject instanceof Internal.EnumLite) {
          paramCodedOutputStream.writeEnumNoTag(((Internal.EnumLite)paramObject).getNumber());
        } else {
          paramCodedOutputStream.writeEnumNoTag(((Integer)paramObject).intValue());
        } 
      case SINT64:
        paramCodedOutputStream.writeSInt64NoTag(((Long)paramObject).longValue());
      case SINT32:
        paramCodedOutputStream.writeSInt32NoTag(((Integer)paramObject).intValue());
      case SFIXED64:
        paramCodedOutputStream.writeSFixed64NoTag(((Long)paramObject).longValue());
      case SFIXED32:
        paramCodedOutputStream.writeSFixed32NoTag(((Integer)paramObject).intValue());
      case UINT32:
        paramCodedOutputStream.writeUInt32NoTag(((Integer)paramObject).intValue());
      case BYTES:
        if (paramObject instanceof ByteString) {
          paramCodedOutputStream.writeBytesNoTag((ByteString)paramObject);
        } else {
          paramCodedOutputStream.writeByteArrayNoTag((byte[])paramObject);
        } 
      case STRING:
        if (paramObject instanceof ByteString) {
          paramCodedOutputStream.writeBytesNoTag((ByteString)paramObject);
        } else {
          paramCodedOutputStream.writeStringNoTag((String)paramObject);
        } 
      case MESSAGE:
        paramCodedOutputStream.writeMessageNoTag((MessageLite)paramObject);
      case GROUP:
        paramCodedOutputStream.writeGroupNoTag((MessageLite)paramObject);
      case BOOL:
        paramCodedOutputStream.writeBoolNoTag(((Boolean)paramObject).booleanValue());
      case FIXED32:
        paramCodedOutputStream.writeFixed32NoTag(((Integer)paramObject).intValue());
      case FIXED64:
        paramCodedOutputStream.writeFixed64NoTag(((Long)paramObject).longValue());
      case INT32:
        paramCodedOutputStream.writeInt32NoTag(((Integer)paramObject).intValue());
      case UINT64:
        paramCodedOutputStream.writeUInt64NoTag(((Long)paramObject).longValue());
      case INT64:
        paramCodedOutputStream.writeInt64NoTag(((Long)paramObject).longValue());
      case FLOAT:
        paramCodedOutputStream.writeFloatNoTag(((Float)paramObject).floatValue());
      case DOUBLE:
        break;
    } 
    paramCodedOutputStream.writeDoubleNoTag(((Double)paramObject).doubleValue());
  }
  
  public static void writeField(FieldDescriptorLite<?> paramFieldDescriptorLite, Object paramObject, CodedOutputStream paramCodedOutputStream) throws IOException {
    WireFormat.FieldType fieldType = paramFieldDescriptorLite.getLiteType();
    int i = paramFieldDescriptorLite.getNumber();
    if (paramFieldDescriptorLite.isRepeated()) {
      paramObject = paramObject;
      if (paramFieldDescriptorLite.isPacked()) {
        paramCodedOutputStream.writeTag(i, 2);
        i = 0;
        for (Object object : paramObject)
          i += computeElementSizeNoTag(fieldType, object); 
        paramCodedOutputStream.writeRawVarint32(i);
        for (paramObject = paramObject.iterator(); paramObject.hasNext(); ) {
          paramFieldDescriptorLite = paramObject.next();
          writeElementNoTag(paramCodedOutputStream, fieldType, paramFieldDescriptorLite);
        } 
      } else {
        for (Object paramObject : paramObject)
          writeElement(paramCodedOutputStream, fieldType, i, paramObject); 
      } 
    } else if (paramObject instanceof LazyField) {
      writeElement(paramCodedOutputStream, fieldType, i, ((LazyField)paramObject).getValue());
    } else {
      writeElement(paramCodedOutputStream, fieldType, i, paramObject);
    } 
  }
  
  public int getSerializedSize() {
    int i = 0;
    for (byte b = 0; b < this.fields.getNumArrayEntries(); b++) {
      Map.Entry<FieldDescriptorType, Object> entry = this.fields.getArrayEntryAt(b);
      i += computeFieldSize((FieldDescriptorLite)entry.getKey(), entry.getValue());
    } 
    for (Map.Entry<FieldDescriptorType, Object> entry : this.fields.getOverflowEntries())
      i += computeFieldSize((FieldDescriptorLite)entry.getKey(), entry.getValue()); 
    return i;
  }
  
  public int getMessageSetSerializedSize() {
    int i = 0;
    for (byte b = 0; b < this.fields.getNumArrayEntries(); b++)
      i += getMessageSetSerializedSize(this.fields.getArrayEntryAt(b)); 
    for (Map.Entry<FieldDescriptorType, Object> entry : this.fields.getOverflowEntries())
      i += getMessageSetSerializedSize(entry); 
    return i;
  }
  
  private int getMessageSetSerializedSize(Map.Entry<FieldDescriptorType, Object> paramEntry) {
    FieldDescriptorLite<?> fieldDescriptorLite = (FieldDescriptorLite)paramEntry.getKey();
    Object object = paramEntry.getValue();
    if (fieldDescriptorLite.getLiteJavaType() == WireFormat.JavaType.MESSAGE && 
      !fieldDescriptorLite.isRepeated() && 
      !fieldDescriptorLite.isPacked()) {
      LazyField lazyField;
      if (object instanceof LazyField) {
        int j = ((FieldDescriptorLite)paramEntry.getKey()).getNumber();
        lazyField = (LazyField)object;
        return CodedOutputStream.computeLazyFieldMessageSetExtensionSize(j, lazyField);
      } 
      int i = ((FieldDescriptorLite)lazyField.getKey()).getNumber();
      MessageLite messageLite = (MessageLite)object;
      return CodedOutputStream.computeMessageSetExtensionSize(i, messageLite);
    } 
    return computeFieldSize(fieldDescriptorLite, object);
  }
  
  static int computeElementSize(WireFormat.FieldType paramFieldType, int paramInt, Object paramObject) {
    int i = CodedOutputStream.computeTagSize(paramInt);
    paramInt = i;
    if (paramFieldType == WireFormat.FieldType.GROUP)
      paramInt = i * 2; 
    return computeElementSizeNoTag(paramFieldType, paramObject) + paramInt;
  }
  
  static int computeElementSizeNoTag(WireFormat.FieldType paramFieldType, Object paramObject) {
    switch (paramFieldType) {
      default:
        throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
      case ENUM:
        if (paramObject instanceof Internal.EnumLite)
          return CodedOutputStream.computeEnumSizeNoTag(((Internal.EnumLite)paramObject).getNumber()); 
        return CodedOutputStream.computeEnumSizeNoTag(((Integer)paramObject).intValue());
      case SINT64:
        return CodedOutputStream.computeSInt64SizeNoTag(((Long)paramObject).longValue());
      case SINT32:
        return CodedOutputStream.computeSInt32SizeNoTag(((Integer)paramObject).intValue());
      case SFIXED64:
        return CodedOutputStream.computeSFixed64SizeNoTag(((Long)paramObject).longValue());
      case SFIXED32:
        return CodedOutputStream.computeSFixed32SizeNoTag(((Integer)paramObject).intValue());
      case UINT32:
        return CodedOutputStream.computeUInt32SizeNoTag(((Integer)paramObject).intValue());
      case BYTES:
        if (paramObject instanceof ByteString)
          return CodedOutputStream.computeBytesSizeNoTag((ByteString)paramObject); 
        return CodedOutputStream.computeByteArraySizeNoTag((byte[])paramObject);
      case STRING:
        if (paramObject instanceof ByteString)
          return CodedOutputStream.computeBytesSizeNoTag((ByteString)paramObject); 
        return CodedOutputStream.computeStringSizeNoTag((String)paramObject);
      case MESSAGE:
        if (paramObject instanceof LazyField)
          return CodedOutputStream.computeLazyFieldSizeNoTag((LazyField)paramObject); 
        return CodedOutputStream.computeMessageSizeNoTag((MessageLite)paramObject);
      case GROUP:
        return CodedOutputStream.computeGroupSizeNoTag((MessageLite)paramObject);
      case BOOL:
        return CodedOutputStream.computeBoolSizeNoTag(((Boolean)paramObject).booleanValue());
      case FIXED32:
        return CodedOutputStream.computeFixed32SizeNoTag(((Integer)paramObject).intValue());
      case FIXED64:
        return CodedOutputStream.computeFixed64SizeNoTag(((Long)paramObject).longValue());
      case INT32:
        return CodedOutputStream.computeInt32SizeNoTag(((Integer)paramObject).intValue());
      case UINT64:
        return CodedOutputStream.computeUInt64SizeNoTag(((Long)paramObject).longValue());
      case INT64:
        return CodedOutputStream.computeInt64SizeNoTag(((Long)paramObject).longValue());
      case FLOAT:
        return CodedOutputStream.computeFloatSizeNoTag(((Float)paramObject).floatValue());
      case DOUBLE:
        break;
    } 
    return CodedOutputStream.computeDoubleSizeNoTag(((Double)paramObject).doubleValue());
  }
  
  public static int computeFieldSize(FieldDescriptorLite<?> paramFieldDescriptorLite, Object paramObject) {
    WireFormat.FieldType fieldType = paramFieldDescriptorLite.getLiteType();
    int i = paramFieldDescriptorLite.getNumber();
    if (paramFieldDescriptorLite.isRepeated()) {
      if (paramFieldDescriptorLite.isPacked()) {
        int k = 0;
        for (paramObject = ((List)paramObject).iterator(); paramObject.hasNext(); ) {
          paramFieldDescriptorLite = paramObject.next();
          k += computeElementSizeNoTag(fieldType, paramFieldDescriptorLite);
        } 
        int m = CodedOutputStream.computeTagSize(i);
        i = CodedOutputStream.computeRawVarint32Size(k);
        return m + k + i;
      } 
      int j = 0;
      for (Object paramObject : paramObject)
        j += computeElementSize(fieldType, i, paramObject); 
      return j;
    } 
    return computeElementSize(fieldType, i, paramObject);
  }
  
  public static interface FieldDescriptorLite<T extends FieldDescriptorLite<T>> extends Comparable<T> {
    Internal.EnumLiteMap<?> getEnumType();
    
    WireFormat.JavaType getLiteJavaType();
    
    WireFormat.FieldType getLiteType();
    
    int getNumber();
    
    MessageLite.Builder internalMergeFrom(MessageLite.Builder param1Builder, MessageLite param1MessageLite);
    
    boolean isPacked();
    
    boolean isRepeated();
  }
}
