package com.android.framework.protobuf;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;

abstract class BinaryReader implements Reader {
  private static final int FIXED32_MULTIPLE_MASK = 3;
  
  private static final int FIXED64_MULTIPLE_MASK = 7;
  
  public static BinaryReader newInstance(ByteBuffer paramByteBuffer, boolean paramBoolean) {
    if (paramByteBuffer.hasArray())
      return new SafeHeapReader(paramByteBuffer, paramBoolean); 
    throw new IllegalArgumentException("Direct buffers not yet supported");
  }
  
  private BinaryReader() {}
  
  public boolean shouldDiscardUnknownFields() {
    return false;
  }
  
  public abstract int getTotalBytesRead();
  
  class SafeHeapReader extends BinaryReader {
    private final byte[] buffer;
    
    private final boolean bufferIsImmutable;
    
    private int endGroupTag;
    
    private final int initialPos;
    
    private int limit;
    
    private int pos;
    
    private int tag;
    
    public SafeHeapReader(BinaryReader this$0, boolean param1Boolean) {
      this.bufferIsImmutable = param1Boolean;
      this.buffer = this$0.array();
      int i = this$0.arrayOffset() + this$0.position();
      this.initialPos = i;
      this.limit = this$0.arrayOffset() + this$0.limit();
    }
    
    private boolean isAtEnd() {
      boolean bool;
      if (this.pos == this.limit) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public int getTotalBytesRead() {
      return this.pos - this.initialPos;
    }
    
    public int getFieldNumber() throws IOException {
      if (isAtEnd())
        return Integer.MAX_VALUE; 
      int i = readVarint32();
      if (i == this.endGroupTag)
        return Integer.MAX_VALUE; 
      return WireFormat.getTagFieldNumber(i);
    }
    
    public int getTag() {
      return this.tag;
    }
    
    public boolean skipField() throws IOException {
      if (!isAtEnd()) {
        int i = this.tag;
        if (i != this.endGroupTag) {
          i = WireFormat.getTagWireType(i);
          if (i != 0) {
            if (i != 1) {
              if (i != 2) {
                if (i != 3) {
                  if (i == 5) {
                    skipBytes(4);
                    return true;
                  } 
                  throw InvalidProtocolBufferException.invalidWireType();
                } 
                skipGroup();
                return true;
              } 
              skipBytes(readVarint32());
              return true;
            } 
            skipBytes(8);
            return true;
          } 
          skipVarint();
          return true;
        } 
      } 
      return false;
    }
    
    public double readDouble() throws IOException {
      requireWireType(1);
      return Double.longBitsToDouble(readLittleEndian64());
    }
    
    public float readFloat() throws IOException {
      requireWireType(5);
      return Float.intBitsToFloat(readLittleEndian32());
    }
    
    public long readUInt64() throws IOException {
      requireWireType(0);
      return readVarint64();
    }
    
    public long readInt64() throws IOException {
      requireWireType(0);
      return readVarint64();
    }
    
    public int readInt32() throws IOException {
      requireWireType(0);
      return readVarint32();
    }
    
    public long readFixed64() throws IOException {
      requireWireType(1);
      return readLittleEndian64();
    }
    
    public int readFixed32() throws IOException {
      requireWireType(5);
      return readLittleEndian32();
    }
    
    public boolean readBool() throws IOException {
      boolean bool = false;
      requireWireType(0);
      if (readVarint32() != 0)
        bool = true; 
      return bool;
    }
    
    public String readString() throws IOException {
      return readStringInternal(false);
    }
    
    public String readStringRequireUtf8() throws IOException {
      return readStringInternal(true);
    }
    
    public String readStringInternal(boolean param1Boolean) throws IOException {
      requireWireType(2);
      int i = readVarint32();
      if (i == 0)
        return ""; 
      requireBytes(i);
      if (param1Boolean) {
        byte[] arrayOfByte = this.buffer;
        int j = this.pos;
        if (!Utf8.isValidUtf8(arrayOfByte, j, j + i))
          throw InvalidProtocolBufferException.invalidUtf8(); 
      } 
      String str = new String(this.buffer, this.pos, i, Internal.UTF_8);
      this.pos += i;
      return str;
    }
    
    public <T> T readMessage(Class<T> param1Class, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      requireWireType(2);
      return readMessage(Protobuf.getInstance().schemaFor(param1Class), param1ExtensionRegistryLite);
    }
    
    public <T> T readMessageBySchemaWithCheck(Schema<T> param1Schema, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      requireWireType(2);
      return readMessage(param1Schema, param1ExtensionRegistryLite);
    }
    
    private <T> T readMessage(Schema<T> param1Schema, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      int i = readVarint32();
      requireBytes(i);
      int j = this.limit;
      i = this.pos + i;
      this.limit = i;
      try {
        T t = param1Schema.newInstance();
        param1Schema.mergeFrom(t, this, param1ExtensionRegistryLite);
        param1Schema.makeImmutable(t);
        int k = this.pos;
        if (k == i)
          return t; 
        throw InvalidProtocolBufferException.parseFailure();
      } finally {
        this.limit = j;
      } 
    }
    
    public <T> T readGroup(Class<T> param1Class, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      requireWireType(3);
      return readGroup(Protobuf.getInstance().schemaFor(param1Class), param1ExtensionRegistryLite);
    }
    
    public <T> T readGroupBySchemaWithCheck(Schema<T> param1Schema, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      requireWireType(3);
      return readGroup(param1Schema, param1ExtensionRegistryLite);
    }
    
    private <T> T readGroup(Schema<T> param1Schema, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      int i = this.endGroupTag;
      this.endGroupTag = WireFormat.makeTag(WireFormat.getTagFieldNumber(this.tag), 4);
      try {
        T t = param1Schema.newInstance();
        param1Schema.mergeFrom(t, this, param1ExtensionRegistryLite);
        param1Schema.makeImmutable(t);
        int j = this.tag, k = this.endGroupTag;
        if (j == k)
          return t; 
        throw InvalidProtocolBufferException.parseFailure();
      } finally {
        this.endGroupTag = i;
      } 
    }
    
    public ByteString readBytes() throws IOException {
      ByteString byteString;
      requireWireType(2);
      int i = readVarint32();
      if (i == 0)
        return ByteString.EMPTY; 
      requireBytes(i);
      if (this.bufferIsImmutable) {
        byteString = ByteString.wrap(this.buffer, this.pos, i);
      } else {
        byteString = ByteString.copyFrom(this.buffer, this.pos, i);
      } 
      this.pos += i;
      return byteString;
    }
    
    public int readUInt32() throws IOException {
      requireWireType(0);
      return readVarint32();
    }
    
    public int readEnum() throws IOException {
      requireWireType(0);
      return readVarint32();
    }
    
    public int readSFixed32() throws IOException {
      requireWireType(5);
      return readLittleEndian32();
    }
    
    public long readSFixed64() throws IOException {
      requireWireType(1);
      return readLittleEndian64();
    }
    
    public int readSInt32() throws IOException {
      requireWireType(0);
      return CodedInputStream.decodeZigZag32(readVarint32());
    }
    
    public long readSInt64() throws IOException {
      requireWireType(0);
      return CodedInputStream.decodeZigZag64(readVarint64());
    }
    
    public void readDoubleList(List<Double> param1List) throws IOException {
      if (param1List instanceof DoubleArrayList) {
        param1List = param1List;
        int i = WireFormat.getTagWireType(this.tag);
        if (i != 1) {
          if (i == 2) {
            int j = readVarint32();
            verifyPackedFixed64Length(j);
            i = this.pos;
            while (this.pos < i + j)
              param1List.addDouble(Double.longBitsToDouble(readLittleEndian64_NoCheck())); 
          } else {
            throw InvalidProtocolBufferException.invalidWireType();
          } 
        } else {
          int j;
          do {
            param1List.addDouble(readDouble());
            if (isAtEnd())
              return; 
            i = this.pos;
            j = readVarint32();
          } while (j == this.tag);
          this.pos = i;
          return;
        } 
      } else {
        int j, i = WireFormat.getTagWireType(this.tag);
        if (i != 1) {
          if (i == 2) {
            j = readVarint32();
            verifyPackedFixed64Length(j);
            i = this.pos;
            while (this.pos < i + j)
              param1List.add(Double.valueOf(Double.longBitsToDouble(readLittleEndian64_NoCheck()))); 
            return;
          } 
          throw InvalidProtocolBufferException.invalidWireType();
        } 
        do {
          param1List.add(Double.valueOf(readDouble()));
          if (isAtEnd())
            return; 
          i = this.pos;
          j = readVarint32();
        } while (j == this.tag);
        this.pos = i;
        return;
      } 
    }
    
    public void readFloatList(List<Float> param1List) throws IOException {
      if (param1List instanceof FloatArrayList) {
        param1List = param1List;
        int i = WireFormat.getTagWireType(this.tag);
        if (i != 2) {
          if (i == 5) {
            int k;
            do {
              param1List.addFloat(readFloat());
              if (isAtEnd())
                return; 
              i = this.pos;
              k = readVarint32();
            } while (k == this.tag);
            this.pos = i;
            return;
          } 
          throw InvalidProtocolBufferException.invalidWireType();
        } 
        int j = readVarint32();
        verifyPackedFixed32Length(j);
        i = this.pos;
        while (this.pos < i + j)
          param1List.addFloat(Float.intBitsToFloat(readLittleEndian32_NoCheck())); 
      } else {
        int i = WireFormat.getTagWireType(this.tag);
        if (i != 2) {
          if (i == 5) {
            int k;
            do {
              param1List.add(Float.valueOf(readFloat()));
              if (isAtEnd())
                return; 
              k = this.pos;
              i = readVarint32();
            } while (i == this.tag);
            this.pos = k;
            return;
          } 
          throw InvalidProtocolBufferException.invalidWireType();
        } 
        i = readVarint32();
        verifyPackedFixed32Length(i);
        int j = this.pos;
        while (this.pos < j + i)
          param1List.add(Float.valueOf(Float.intBitsToFloat(readLittleEndian32_NoCheck()))); 
      } 
    }
    
    public void readUInt64List(List<Long> param1List) throws IOException {
      if (param1List instanceof LongArrayList) {
        param1List = param1List;
        int i = WireFormat.getTagWireType(this.tag);
        if (i != 0) {
          if (i == 2) {
            i = readVarint32();
            i = this.pos + i;
            while (this.pos < i)
              param1List.addLong(readVarint64()); 
            requirePosition(i);
          } else {
            throw InvalidProtocolBufferException.invalidWireType();
          } 
        } else {
          int j;
          do {
            param1List.addLong(readUInt64());
            if (isAtEnd())
              return; 
            i = this.pos;
            j = readVarint32();
          } while (j == this.tag);
          this.pos = i;
          return;
        } 
      } else {
        int j, i = WireFormat.getTagWireType(this.tag);
        if (i != 0) {
          if (i == 2) {
            i = readVarint32();
            i = this.pos + i;
            while (this.pos < i)
              param1List.add(Long.valueOf(readVarint64())); 
            requirePosition(i);
            return;
          } 
          throw InvalidProtocolBufferException.invalidWireType();
        } 
        do {
          param1List.add(Long.valueOf(readUInt64()));
          if (isAtEnd())
            return; 
          j = this.pos;
          i = readVarint32();
        } while (i == this.tag);
        this.pos = j;
        return;
      } 
    }
    
    public void readInt64List(List<Long> param1List) throws IOException {
      if (param1List instanceof LongArrayList) {
        param1List = param1List;
        int i = WireFormat.getTagWireType(this.tag);
        if (i != 0) {
          if (i == 2) {
            i = readVarint32();
            i = this.pos + i;
            while (this.pos < i)
              param1List.addLong(readVarint64()); 
            requirePosition(i);
          } else {
            throw InvalidProtocolBufferException.invalidWireType();
          } 
        } else {
          int j;
          do {
            param1List.addLong(readInt64());
            if (isAtEnd())
              return; 
            j = this.pos;
            i = readVarint32();
          } while (i == this.tag);
          this.pos = j;
          return;
        } 
      } else {
        int j, i = WireFormat.getTagWireType(this.tag);
        if (i != 0) {
          if (i == 2) {
            i = readVarint32();
            i = this.pos + i;
            while (this.pos < i)
              param1List.add(Long.valueOf(readVarint64())); 
            requirePosition(i);
            return;
          } 
          throw InvalidProtocolBufferException.invalidWireType();
        } 
        do {
          param1List.add(Long.valueOf(readInt64()));
          if (isAtEnd())
            return; 
          j = this.pos;
          i = readVarint32();
        } while (i == this.tag);
        this.pos = j;
        return;
      } 
    }
    
    public void readInt32List(List<Integer> param1List) throws IOException {
      if (param1List instanceof IntArrayList) {
        param1List = param1List;
        int i = WireFormat.getTagWireType(this.tag);
        if (i != 0) {
          if (i == 2) {
            i = readVarint32();
            i = this.pos + i;
            while (this.pos < i)
              param1List.addInt(readVarint32()); 
            requirePosition(i);
          } else {
            throw InvalidProtocolBufferException.invalidWireType();
          } 
        } else {
          int j;
          do {
            param1List.addInt(readInt32());
            if (isAtEnd())
              return; 
            i = this.pos;
            j = readVarint32();
          } while (j == this.tag);
          this.pos = i;
          return;
        } 
      } else {
        int j, i = WireFormat.getTagWireType(this.tag);
        if (i != 0) {
          if (i == 2) {
            i = readVarint32();
            i = this.pos + i;
            while (this.pos < i)
              param1List.add(Integer.valueOf(readVarint32())); 
            requirePosition(i);
            return;
          } 
          throw InvalidProtocolBufferException.invalidWireType();
        } 
        do {
          param1List.add(Integer.valueOf(readInt32()));
          if (isAtEnd())
            return; 
          i = this.pos;
          j = readVarint32();
        } while (j == this.tag);
        this.pos = i;
        return;
      } 
    }
    
    public void readFixed64List(List<Long> param1List) throws IOException {
      if (param1List instanceof LongArrayList) {
        param1List = param1List;
        int i = WireFormat.getTagWireType(this.tag);
        if (i != 1) {
          if (i == 2) {
            i = readVarint32();
            verifyPackedFixed64Length(i);
            int j = this.pos;
            while (this.pos < j + i)
              param1List.addLong(readLittleEndian64_NoCheck()); 
          } else {
            throw InvalidProtocolBufferException.invalidWireType();
          } 
        } else {
          int j;
          do {
            param1List.addLong(readFixed64());
            if (isAtEnd())
              return; 
            i = this.pos;
            j = readVarint32();
          } while (j == this.tag);
          this.pos = i;
          return;
        } 
      } else {
        int j, i = WireFormat.getTagWireType(this.tag);
        if (i != 1) {
          if (i == 2) {
            j = readVarint32();
            verifyPackedFixed64Length(j);
            i = this.pos;
            while (this.pos < i + j)
              param1List.add(Long.valueOf(readLittleEndian64_NoCheck())); 
            return;
          } 
          throw InvalidProtocolBufferException.invalidWireType();
        } 
        do {
          param1List.add(Long.valueOf(readFixed64()));
          if (isAtEnd())
            return; 
          i = this.pos;
          j = readVarint32();
        } while (j == this.tag);
        this.pos = i;
        return;
      } 
    }
    
    public void readFixed32List(List<Integer> param1List) throws IOException {
      if (param1List instanceof IntArrayList) {
        param1List = param1List;
        int i = WireFormat.getTagWireType(this.tag);
        if (i != 2) {
          if (i == 5) {
            int k;
            do {
              param1List.addInt(readFixed32());
              if (isAtEnd())
                return; 
              k = this.pos;
              i = readVarint32();
            } while (i == this.tag);
            this.pos = k;
            return;
          } 
          throw InvalidProtocolBufferException.invalidWireType();
        } 
        i = readVarint32();
        verifyPackedFixed32Length(i);
        int j = this.pos;
        while (this.pos < j + i)
          param1List.addInt(readLittleEndian32_NoCheck()); 
      } else {
        int i = WireFormat.getTagWireType(this.tag);
        if (i != 2) {
          if (i == 5) {
            int k;
            do {
              param1List.add(Integer.valueOf(readFixed32()));
              if (isAtEnd())
                return; 
              i = this.pos;
              k = readVarint32();
            } while (k == this.tag);
            this.pos = i;
            return;
          } 
          throw InvalidProtocolBufferException.invalidWireType();
        } 
        int j = readVarint32();
        verifyPackedFixed32Length(j);
        i = this.pos;
        while (this.pos < i + j)
          param1List.add(Integer.valueOf(readLittleEndian32_NoCheck())); 
      } 
    }
    
    public void readBoolList(List<Boolean> param1List) throws IOException {
      if (param1List instanceof BooleanArrayList) {
        param1List = param1List;
        int i = WireFormat.getTagWireType(this.tag);
        if (i != 0) {
          if (i == 2) {
            i = readVarint32();
            i = this.pos + i;
            while (this.pos < i) {
              boolean bool;
              if (readVarint32() != 0) {
                bool = true;
              } else {
                bool = false;
              } 
              param1List.addBoolean(bool);
            } 
            requirePosition(i);
          } else {
            throw InvalidProtocolBufferException.invalidWireType();
          } 
        } else {
          int j;
          do {
            param1List.addBoolean(readBool());
            if (isAtEnd())
              return; 
            i = this.pos;
            j = readVarint32();
          } while (j == this.tag);
          this.pos = i;
          return;
        } 
      } else {
        int j, i = WireFormat.getTagWireType(this.tag);
        if (i != 0) {
          if (i == 2) {
            i = readVarint32();
            i = this.pos + i;
            while (this.pos < i) {
              boolean bool;
              if (readVarint32() != 0) {
                bool = true;
              } else {
                bool = false;
              } 
              param1List.add(Boolean.valueOf(bool));
            } 
            requirePosition(i);
            return;
          } 
          throw InvalidProtocolBufferException.invalidWireType();
        } 
        do {
          param1List.add(Boolean.valueOf(readBool()));
          if (isAtEnd())
            return; 
          j = this.pos;
          i = readVarint32();
        } while (i == this.tag);
        this.pos = j;
        return;
      } 
    }
    
    public void readStringList(List<String> param1List) throws IOException {
      readStringListInternal(param1List, false);
    }
    
    public void readStringListRequireUtf8(List<String> param1List) throws IOException {
      readStringListInternal(param1List, true);
    }
    
    public void readStringListInternal(List<String> param1List, boolean param1Boolean) throws IOException {
      if (WireFormat.getTagWireType(this.tag) == 2) {
        int i, j;
        if (param1List instanceof LazyStringList && !param1Boolean) {
          param1List = param1List;
          do {
            param1List.add(readBytes());
            if (isAtEnd())
              return; 
            i = this.pos;
            j = readVarint32();
          } while (j == this.tag);
          this.pos = i;
          return;
        } 
        do {
          param1List.add(readStringInternal(param1Boolean));
          if (isAtEnd())
            return; 
          j = this.pos;
          i = readVarint32();
        } while (i == this.tag);
        this.pos = j;
        return;
      } 
      throw InvalidProtocolBufferException.invalidWireType();
    }
    
    public <T> void readMessageList(List<T> param1List, Class<T> param1Class, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      Schema<T> schema = Protobuf.getInstance().schemaFor(param1Class);
      readMessageList(param1List, schema, param1ExtensionRegistryLite);
    }
    
    public <T> void readMessageList(List<T> param1List, Schema<T> param1Schema, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      if (WireFormat.getTagWireType(this.tag) == 2) {
        int j, k, i = this.tag;
        do {
          param1List.add(readMessage(param1Schema, param1ExtensionRegistryLite));
          if (isAtEnd())
            return; 
          j = this.pos;
          k = readVarint32();
        } while (k == i);
        this.pos = j;
        return;
      } 
      throw InvalidProtocolBufferException.invalidWireType();
    }
    
    public <T> void readGroupList(List<T> param1List, Class<T> param1Class, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      Schema<T> schema = Protobuf.getInstance().schemaFor(param1Class);
      readGroupList(param1List, schema, param1ExtensionRegistryLite);
    }
    
    public <T> void readGroupList(List<T> param1List, Schema<T> param1Schema, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      if (WireFormat.getTagWireType(this.tag) == 3) {
        int j, k, i = this.tag;
        do {
          param1List.add(readGroup(param1Schema, param1ExtensionRegistryLite));
          if (isAtEnd())
            return; 
          j = this.pos;
          k = readVarint32();
        } while (k == i);
        this.pos = j;
        return;
      } 
      throw InvalidProtocolBufferException.invalidWireType();
    }
    
    public void readBytesList(List<ByteString> param1List) throws IOException {
      if (WireFormat.getTagWireType(this.tag) == 2) {
        int i, j;
        do {
          param1List.add(readBytes());
          if (isAtEnd())
            return; 
          i = this.pos;
          j = readVarint32();
        } while (j == this.tag);
        this.pos = i;
        return;
      } 
      throw InvalidProtocolBufferException.invalidWireType();
    }
    
    public void readUInt32List(List<Integer> param1List) throws IOException {
      if (param1List instanceof IntArrayList) {
        param1List = param1List;
        int i = WireFormat.getTagWireType(this.tag);
        if (i != 0) {
          if (i == 2) {
            i = readVarint32();
            int j = this.pos;
            while (this.pos < j + i)
              param1List.addInt(readVarint32()); 
          } else {
            throw InvalidProtocolBufferException.invalidWireType();
          } 
        } else {
          int j;
          do {
            param1List.addInt(readUInt32());
            if (isAtEnd())
              return; 
            j = this.pos;
            i = readVarint32();
          } while (i == this.tag);
          this.pos = j;
          return;
        } 
      } else {
        int j, i = WireFormat.getTagWireType(this.tag);
        if (i != 0) {
          if (i == 2) {
            i = readVarint32();
            j = this.pos;
            while (this.pos < j + i)
              param1List.add(Integer.valueOf(readVarint32())); 
            return;
          } 
          throw InvalidProtocolBufferException.invalidWireType();
        } 
        do {
          param1List.add(Integer.valueOf(readUInt32()));
          if (isAtEnd())
            return; 
          j = this.pos;
          i = readVarint32();
        } while (i == this.tag);
        this.pos = j;
        return;
      } 
    }
    
    public void readEnumList(List<Integer> param1List) throws IOException {
      if (param1List instanceof IntArrayList) {
        param1List = param1List;
        int i = WireFormat.getTagWireType(this.tag);
        if (i != 0) {
          if (i == 2) {
            i = readVarint32();
            int j = this.pos;
            while (this.pos < j + i)
              param1List.addInt(readVarint32()); 
          } else {
            throw InvalidProtocolBufferException.invalidWireType();
          } 
        } else {
          int j;
          do {
            param1List.addInt(readEnum());
            if (isAtEnd())
              return; 
            j = this.pos;
            i = readVarint32();
          } while (i == this.tag);
          this.pos = j;
          return;
        } 
      } else {
        int j, i = WireFormat.getTagWireType(this.tag);
        if (i != 0) {
          if (i == 2) {
            j = readVarint32();
            i = this.pos;
            while (this.pos < i + j)
              param1List.add(Integer.valueOf(readVarint32())); 
            return;
          } 
          throw InvalidProtocolBufferException.invalidWireType();
        } 
        do {
          param1List.add(Integer.valueOf(readEnum()));
          if (isAtEnd())
            return; 
          j = this.pos;
          i = readVarint32();
        } while (i == this.tag);
        this.pos = j;
        return;
      } 
    }
    
    public void readSFixed32List(List<Integer> param1List) throws IOException {
      if (param1List instanceof IntArrayList) {
        param1List = param1List;
        int i = WireFormat.getTagWireType(this.tag);
        if (i != 2) {
          if (i == 5) {
            int k;
            do {
              param1List.addInt(readSFixed32());
              if (isAtEnd())
                return; 
              k = this.pos;
              i = readVarint32();
            } while (i == this.tag);
            this.pos = k;
            return;
          } 
          throw InvalidProtocolBufferException.invalidWireType();
        } 
        i = readVarint32();
        verifyPackedFixed32Length(i);
        int j = this.pos;
        while (this.pos < j + i)
          param1List.addInt(readLittleEndian32_NoCheck()); 
      } else {
        int i = WireFormat.getTagWireType(this.tag);
        if (i != 2) {
          if (i == 5) {
            int k;
            do {
              param1List.add(Integer.valueOf(readSFixed32()));
              if (isAtEnd())
                return; 
              i = this.pos;
              k = readVarint32();
            } while (k == this.tag);
            this.pos = i;
            return;
          } 
          throw InvalidProtocolBufferException.invalidWireType();
        } 
        i = readVarint32();
        verifyPackedFixed32Length(i);
        int j = this.pos;
        while (this.pos < j + i)
          param1List.add(Integer.valueOf(readLittleEndian32_NoCheck())); 
      } 
    }
    
    public void readSFixed64List(List<Long> param1List) throws IOException {
      if (param1List instanceof LongArrayList) {
        param1List = param1List;
        int i = WireFormat.getTagWireType(this.tag);
        if (i != 1) {
          if (i == 2) {
            int j = readVarint32();
            verifyPackedFixed64Length(j);
            i = this.pos;
            while (this.pos < i + j)
              param1List.addLong(readLittleEndian64_NoCheck()); 
          } else {
            throw InvalidProtocolBufferException.invalidWireType();
          } 
        } else {
          int j;
          do {
            param1List.addLong(readSFixed64());
            if (isAtEnd())
              return; 
            i = this.pos;
            j = readVarint32();
          } while (j == this.tag);
          this.pos = i;
          return;
        } 
      } else {
        int j, i = WireFormat.getTagWireType(this.tag);
        if (i != 1) {
          if (i == 2) {
            j = readVarint32();
            verifyPackedFixed64Length(j);
            i = this.pos;
            while (this.pos < i + j)
              param1List.add(Long.valueOf(readLittleEndian64_NoCheck())); 
            return;
          } 
          throw InvalidProtocolBufferException.invalidWireType();
        } 
        do {
          param1List.add(Long.valueOf(readSFixed64()));
          if (isAtEnd())
            return; 
          j = this.pos;
          i = readVarint32();
        } while (i == this.tag);
        this.pos = j;
        return;
      } 
    }
    
    public void readSInt32List(List<Integer> param1List) throws IOException {
      if (param1List instanceof IntArrayList) {
        param1List = param1List;
        int i = WireFormat.getTagWireType(this.tag);
        if (i != 0) {
          if (i == 2) {
            int j = readVarint32();
            i = this.pos;
            while (this.pos < i + j)
              param1List.addInt(CodedInputStream.decodeZigZag32(readVarint32())); 
          } else {
            throw InvalidProtocolBufferException.invalidWireType();
          } 
        } else {
          int j;
          do {
            param1List.addInt(readSInt32());
            if (isAtEnd())
              return; 
            i = this.pos;
            j = readVarint32();
          } while (j == this.tag);
          this.pos = i;
          return;
        } 
      } else {
        int j, i = WireFormat.getTagWireType(this.tag);
        if (i != 0) {
          if (i == 2) {
            j = readVarint32();
            i = this.pos;
            while (this.pos < i + j)
              param1List.add(Integer.valueOf(CodedInputStream.decodeZigZag32(readVarint32()))); 
            return;
          } 
          throw InvalidProtocolBufferException.invalidWireType();
        } 
        do {
          param1List.add(Integer.valueOf(readSInt32()));
          if (isAtEnd())
            return; 
          i = this.pos;
          j = readVarint32();
        } while (j == this.tag);
        this.pos = i;
        return;
      } 
    }
    
    public void readSInt64List(List<Long> param1List) throws IOException {
      if (param1List instanceof LongArrayList) {
        param1List = param1List;
        int i = WireFormat.getTagWireType(this.tag);
        if (i != 0) {
          if (i == 2) {
            i = readVarint32();
            int j = this.pos;
            while (this.pos < j + i)
              param1List.addLong(CodedInputStream.decodeZigZag64(readVarint64())); 
          } else {
            throw InvalidProtocolBufferException.invalidWireType();
          } 
        } else {
          int j;
          do {
            param1List.addLong(readSInt64());
            if (isAtEnd())
              return; 
            j = this.pos;
            i = readVarint32();
          } while (i == this.tag);
          this.pos = j;
          return;
        } 
      } else {
        int j, i = WireFormat.getTagWireType(this.tag);
        if (i != 0) {
          if (i == 2) {
            j = readVarint32();
            i = this.pos;
            while (this.pos < i + j)
              param1List.add(Long.valueOf(CodedInputStream.decodeZigZag64(readVarint64()))); 
            return;
          } 
          throw InvalidProtocolBufferException.invalidWireType();
        } 
        do {
          param1List.add(Long.valueOf(readSInt64()));
          if (isAtEnd())
            return; 
          j = this.pos;
          i = readVarint32();
        } while (i == this.tag);
        this.pos = j;
        return;
      } 
    }
    
    public <K, V> void readMap(Map<K, V> param1Map, MapEntryLite.Metadata<K, V> param1Metadata, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      requireWireType(2);
      int i = readVarint32();
      requireBytes(i);
      int j = this.limit;
      int k = this.pos;
      this.limit = k + i;
      try {
        K k1 = param1Metadata.defaultKey;
        V v = param1Metadata.defaultValue;
        while (true) {
          k = getFieldNumber();
          if (k == Integer.MAX_VALUE) {
            param1Map.put(k1, v);
            return;
          } 
          if (k != 1) {
            if (k != 2)
              try {
                if (skipField())
                  continue; 
                InvalidProtocolBufferException invalidProtocolBufferException = new InvalidProtocolBufferException();
                this("Unable to parse map entry.");
                throw invalidProtocolBufferException;
              } catch (InvalidWireTypeException invalidWireTypeException) {
                if (skipField())
                  continue; 
                InvalidProtocolBufferException invalidProtocolBufferException = new InvalidProtocolBufferException();
                this("Unable to parse map entry.");
                throw invalidProtocolBufferException;
              }  
            WireFormat.FieldType fieldType = param1Metadata.valueType;
            V v1 = param1Metadata.defaultValue;
            Class<?> clazz = v1.getClass();
            Object object1 = readField(fieldType, clazz, param1ExtensionRegistryLite);
            v = (V)object1;
            continue;
          } 
          Object object = readField(param1Metadata.keyType, null, null);
        } 
      } finally {
        this.limit = j;
      } 
    }
    
    private Object readField(WireFormat.FieldType param1FieldType, Class<?> param1Class, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      switch (BinaryReader.null.$SwitchMap$com$google$protobuf$WireFormat$FieldType[param1FieldType.ordinal()]) {
        default:
          throw new RuntimeException("unsupported field type.");
        case 17:
          return Long.valueOf(readUInt64());
        case 16:
          return Integer.valueOf(readUInt32());
        case 15:
          return readStringRequireUtf8();
        case 14:
          return Long.valueOf(readSInt64());
        case 13:
          return Integer.valueOf(readSInt32());
        case 12:
          return Long.valueOf(readSFixed64());
        case 11:
          return Integer.valueOf(readSFixed32());
        case 10:
          return readMessage(param1Class, param1ExtensionRegistryLite);
        case 9:
          return Long.valueOf(readInt64());
        case 8:
          return Integer.valueOf(readInt32());
        case 7:
          return Float.valueOf(readFloat());
        case 6:
          return Long.valueOf(readFixed64());
        case 5:
          return Integer.valueOf(readFixed32());
        case 4:
          return Integer.valueOf(readEnum());
        case 3:
          return Double.valueOf(readDouble());
        case 2:
          return readBytes();
        case 1:
          break;
      } 
      return Boolean.valueOf(readBool());
    }
    
    private int readVarint32() throws IOException {
      int i = this.pos;
      int j = this.limit;
      if (j != this.pos) {
        byte[] arrayOfByte = this.buffer;
        int k = i + 1;
        i = arrayOfByte[i];
        if (i >= 0) {
          this.pos = k;
          return i;
        } 
        if (j - k < 9)
          return (int)readVarint64SlowPath(); 
        j = k + 1;
        i = arrayOfByte[k] << 7 ^ i;
        if (i < 0) {
          k = i ^ 0xFFFFFF80;
        } else {
          k = j + 1;
          i = arrayOfByte[j] << 14 ^ i;
          if (i >= 0) {
            i ^= 0x3F80;
            j = k;
            k = i;
          } else {
            j = k + 1;
            k = arrayOfByte[k] << 21 ^ i;
            if (k < 0) {
              k = 0xFFE03F80 ^ k;
            } else {
              i = j + 1;
              byte b = arrayOfByte[j];
              k = k ^ b << 28 ^ 0xFE03F80;
              j = i;
              if (b < 0) {
                int m = i + 1;
                if (arrayOfByte[i] < 0) {
                  j = i = m + 1;
                  if (arrayOfByte[m] < 0) {
                    j = m = i + 1;
                    if (arrayOfByte[i] < 0) {
                      j = i = m + 1;
                      if (arrayOfByte[m] < 0) {
                        j = i + 1;
                        if (arrayOfByte[i] < 0)
                          throw InvalidProtocolBufferException.malformedVarint(); 
                      } 
                    } 
                  } 
                } 
              } 
            } 
          } 
        } 
        this.pos = j;
        return k;
      } 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public long readVarint64() throws IOException {
      int i = this.pos;
      int j = this.limit;
      if (j != i) {
        long l;
        byte[] arrayOfByte = this.buffer;
        int k = i + 1;
        i = arrayOfByte[i];
        if (i >= 0) {
          this.pos = k;
          return i;
        } 
        if (j - k < 9)
          return readVarint64SlowPath(); 
        j = k + 1;
        i = arrayOfByte[k] << 7 ^ i;
        if (i < 0) {
          l = (i ^ 0xFFFFFF80);
          k = j;
        } else {
          k = j + 1;
          i = arrayOfByte[j] << 14 ^ i;
          if (i >= 0) {
            l = (i ^ 0x3F80);
          } else {
            j = k + 1;
            k = arrayOfByte[k] << 21 ^ i;
            if (k < 0) {
              l = (0xFFE03F80 ^ k);
              k = j;
            } else {
              l = k;
              k = j + 1;
              l ^= arrayOfByte[j] << 28L;
              if (l >= 0L) {
                l = 0xFE03F80L ^ l;
              } else {
                j = k + 1;
                l = arrayOfByte[k] << 35L ^ l;
                if (l < 0L) {
                  l = 0xFFFFFFF80FE03F80L ^ l;
                  k = j;
                } else {
                  i = j + 1;
                  l = arrayOfByte[j] << 42L ^ l;
                  if (l >= 0L) {
                    l = 0x3F80FE03F80L ^ l;
                    k = i;
                  } else {
                    k = i + 1;
                    long l1 = arrayOfByte[i] << 49L ^ l;
                    if (l1 < 0L) {
                      l = 0xFFFE03F80FE03F80L ^ l1;
                    } else {
                      j = k + 1;
                      l = arrayOfByte[k];
                      l = l << 56L ^ l1 ^ 0xFE03F80FE03F80L;
                      if (l < 0L) {
                        k = j + 1;
                        if (arrayOfByte[j] < 0L)
                          throw InvalidProtocolBufferException.malformedVarint(); 
                      } else {
                        k = j;
                      } 
                    } 
                  } 
                } 
              } 
            } 
          } 
        } 
        this.pos = k;
        return l;
      } 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    private long readVarint64SlowPath() throws IOException {
      long l = 0L;
      for (byte b = 0; b < 64; b += 7) {
        byte b1 = readByte();
        l |= (b1 & Byte.MAX_VALUE) << b;
        if ((b1 & 0x80) == 0)
          return l; 
      } 
      throw InvalidProtocolBufferException.malformedVarint();
    }
    
    private byte readByte() throws IOException {
      int i = this.pos;
      if (i != this.limit) {
        byte[] arrayOfByte = this.buffer;
        this.pos = i + 1;
        return arrayOfByte[i];
      } 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    private int readLittleEndian32() throws IOException {
      requireBytes(4);
      return readLittleEndian32_NoCheck();
    }
    
    private long readLittleEndian64() throws IOException {
      requireBytes(8);
      return readLittleEndian64_NoCheck();
    }
    
    private int readLittleEndian32_NoCheck() {
      int i = this.pos;
      byte[] arrayOfByte = this.buffer;
      this.pos = i + 4;
      return arrayOfByte[i] & 0xFF | (arrayOfByte[i + 1] & 0xFF) << 8 | (arrayOfByte[i + 2] & 0xFF) << 16 | (arrayOfByte[i + 3] & 0xFF) << 24;
    }
    
    private long readLittleEndian64_NoCheck() {
      int i = this.pos;
      byte[] arrayOfByte = this.buffer;
      this.pos = i + 8;
      return arrayOfByte[i] & 0xFFL | (arrayOfByte[i + 1] & 0xFFL) << 8L | (arrayOfByte[i + 2] & 0xFFL) << 16L | (arrayOfByte[i + 3] & 0xFFL) << 24L | (arrayOfByte[i + 4] & 0xFFL) << 32L | (arrayOfByte[i + 5] & 0xFFL) << 40L | (arrayOfByte[i + 6] & 0xFFL) << 48L | (0xFFL & arrayOfByte[i + 7]) << 56L;
    }
    
    private void skipVarint() throws IOException {
      if (this.limit - this.pos >= 10) {
        byte[] arrayOfByte = this.buffer;
        int i = this.pos;
        for (byte b = 0; b < 10; b++, i = j) {
          int j = i + 1;
          if (arrayOfByte[i] >= 0) {
            this.pos = j;
            return;
          } 
        } 
      } 
      skipVarintSlowPath();
    }
    
    private void skipVarintSlowPath() throws IOException {
      for (byte b = 0; b < 10; b++) {
        if (readByte() >= 0)
          return; 
      } 
      throw InvalidProtocolBufferException.malformedVarint();
    }
    
    private void skipBytes(int param1Int) throws IOException {
      requireBytes(param1Int);
      this.pos += param1Int;
    }
    
    private void skipGroup() throws IOException {
      int i = this.endGroupTag;
      this.endGroupTag = WireFormat.makeTag(WireFormat.getTagFieldNumber(this.tag), 4);
      do {
      
      } while (getFieldNumber() != Integer.MAX_VALUE && skipField());
      if (this.tag == this.endGroupTag) {
        this.endGroupTag = i;
        return;
      } 
      throw InvalidProtocolBufferException.parseFailure();
    }
    
    private void requireBytes(int param1Int) throws IOException {
      if (param1Int >= 0 && param1Int <= this.limit - this.pos)
        return; 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    private void requireWireType(int param1Int) throws IOException {
      if (WireFormat.getTagWireType(this.tag) == param1Int)
        return; 
      throw InvalidProtocolBufferException.invalidWireType();
    }
    
    private void verifyPackedFixed64Length(int param1Int) throws IOException {
      requireBytes(param1Int);
      if ((param1Int & 0x7) == 0)
        return; 
      throw InvalidProtocolBufferException.parseFailure();
    }
    
    private void verifyPackedFixed32Length(int param1Int) throws IOException {
      requireBytes(param1Int);
      if ((param1Int & 0x3) == 0)
        return; 
      throw InvalidProtocolBufferException.parseFailure();
    }
    
    private void requirePosition(int param1Int) throws IOException {
      if (this.pos == param1Int)
        return; 
      throw InvalidProtocolBufferException.truncatedMessage();
    }
  }
}
