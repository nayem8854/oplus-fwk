package com.android.framework.protobuf;

final class ManifestSchemaFactory implements SchemaFactory {
  public ManifestSchemaFactory() {
    this(getDefaultMessageInfoFactory());
  }
  
  private ManifestSchemaFactory(MessageInfoFactory paramMessageInfoFactory) {
    this.messageInfoFactory = Internal.<MessageInfoFactory>checkNotNull(paramMessageInfoFactory, "messageInfoFactory");
  }
  
  public <T> Schema<T> createSchema(Class<T> paramClass) {
    ExtensionSchema<?> extensionSchema;
    MessageLite messageLite;
    SchemaUtil.requireGeneratedMessage(paramClass);
    MessageInfo messageInfo = this.messageInfoFactory.messageInfoFor(paramClass);
    if (messageInfo.isMessageSetWireFormat()) {
      if (GeneratedMessageLite.class.isAssignableFrom(paramClass)) {
        UnknownFieldSchema<?, ?> unknownFieldSchema1 = SchemaUtil.unknownFieldSetLiteSchema();
        ExtensionSchema<?> extensionSchema1 = ExtensionSchemas.lite();
        messageLite = messageInfo.getDefaultInstance();
        return MessageSetSchema.newSchema(unknownFieldSchema1, extensionSchema1, messageLite);
      } 
      UnknownFieldSchema<?, ?> unknownFieldSchema = SchemaUtil.proto2UnknownFieldSetSchema();
      extensionSchema = ExtensionSchemas.full();
      messageLite = messageLite.getDefaultInstance();
      return MessageSetSchema.newSchema(unknownFieldSchema, extensionSchema, messageLite);
    } 
    return newSchema((Class)extensionSchema, (MessageInfo)messageLite);
  }
  
  private static <T> Schema<T> newSchema(Class<T> paramClass, MessageInfo paramMessageInfo) {
    MessageSchema<T> messageSchema;
    if (GeneratedMessageLite.class.isAssignableFrom(paramClass)) {
      if (isProto2(paramMessageInfo)) {
        NewInstanceSchema newInstanceSchema = NewInstanceSchemas.lite();
        ListFieldSchema listFieldSchema = ListFieldSchema.lite();
        UnknownFieldSchema<?, ?> unknownFieldSchema = SchemaUtil.unknownFieldSetLiteSchema();
        ExtensionSchema<?> extensionSchema = ExtensionSchemas.lite();
        MapFieldSchema mapFieldSchema = MapFieldSchemas.lite();
        messageSchema = MessageSchema.newSchema(paramClass, paramMessageInfo, newInstanceSchema, listFieldSchema, unknownFieldSchema, extensionSchema, mapFieldSchema);
      } else {
        NewInstanceSchema newInstanceSchema = NewInstanceSchemas.lite();
        ListFieldSchema listFieldSchema = ListFieldSchema.lite();
        UnknownFieldSchema<?, ?> unknownFieldSchema = SchemaUtil.unknownFieldSetLiteSchema();
        MapFieldSchema mapFieldSchema = MapFieldSchemas.lite();
        messageSchema = MessageSchema.newSchema((Class<T>)messageSchema, paramMessageInfo, newInstanceSchema, listFieldSchema, unknownFieldSchema, null, mapFieldSchema);
      } 
      return messageSchema;
    } 
    if (isProto2(paramMessageInfo)) {
      NewInstanceSchema newInstanceSchema = NewInstanceSchemas.full();
      ListFieldSchema listFieldSchema = ListFieldSchema.full();
      UnknownFieldSchema<?, ?> unknownFieldSchema = SchemaUtil.proto2UnknownFieldSetSchema();
      ExtensionSchema<?> extensionSchema = ExtensionSchemas.full();
      MapFieldSchema mapFieldSchema = MapFieldSchemas.full();
      messageSchema = MessageSchema.newSchema((Class<T>)messageSchema, paramMessageInfo, newInstanceSchema, listFieldSchema, unknownFieldSchema, extensionSchema, mapFieldSchema);
    } else {
      NewInstanceSchema newInstanceSchema = NewInstanceSchemas.full();
      ListFieldSchema listFieldSchema = ListFieldSchema.full();
      UnknownFieldSchema<?, ?> unknownFieldSchema = SchemaUtil.proto3UnknownFieldSetSchema();
      MapFieldSchema mapFieldSchema = MapFieldSchemas.full();
      messageSchema = MessageSchema.newSchema((Class<T>)messageSchema, paramMessageInfo, newInstanceSchema, listFieldSchema, unknownFieldSchema, null, mapFieldSchema);
    } 
    return messageSchema;
  }
  
  private static boolean isProto2(MessageInfo paramMessageInfo) {
    boolean bool;
    if (paramMessageInfo.getSyntax() == ProtoSyntax.PROTO2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static MessageInfoFactory getDefaultMessageInfoFactory() {
    return 
      new CompositeMessageInfoFactory(new MessageInfoFactory[] { GeneratedMessageInfoFactory.getInstance(), getDescriptorMessageInfoFactory() });
  }
  
  private static class CompositeMessageInfoFactory implements MessageInfoFactory {
    private MessageInfoFactory[] factories;
    
    CompositeMessageInfoFactory(MessageInfoFactory... param1VarArgs) {
      this.factories = param1VarArgs;
    }
    
    public boolean isSupported(Class<?> param1Class) {
      for (MessageInfoFactory messageInfoFactory : this.factories) {
        if (messageInfoFactory.isSupported(param1Class))
          return true; 
      } 
      return false;
    }
    
    public MessageInfo messageInfoFor(Class<?> param1Class) {
      for (MessageInfoFactory messageInfoFactory : this.factories) {
        if (messageInfoFactory.isSupported(param1Class))
          return messageInfoFactory.messageInfoFor(param1Class); 
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("No factory is available for message type: ");
      stringBuilder.append(param1Class.getName());
      throw new UnsupportedOperationException(stringBuilder.toString());
    }
  }
  
  private static final MessageInfoFactory EMPTY_FACTORY = new MessageInfoFactory() {
      public boolean isSupported(Class<?> param1Class) {
        return false;
      }
      
      public MessageInfo messageInfoFor(Class<?> param1Class) {
        throw new IllegalStateException("This should never be called.");
      }
    };
  
  private final MessageInfoFactory messageInfoFactory;
  
  private static MessageInfoFactory getDescriptorMessageInfoFactory() {
    try {
      Class<?> clazz = Class.forName("com.android.framework.protobuf.DescriptorMessageInfoFactory");
      return (MessageInfoFactory)clazz.getDeclaredMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
    } catch (Exception exception) {
      return EMPTY_FACTORY;
    } 
  }
}
