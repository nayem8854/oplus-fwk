package com.android.framework.protobuf;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ExtensionRegistryLite {
  static final ExtensionRegistryLite EMPTY_REGISTRY_LITE;
  
  static final String EXTENSION_CLASS_NAME = "com.android.framework.protobuf.Extension";
  
  private static boolean doFullRuntimeInheritanceCheck;
  
  private static volatile boolean eagerlyParseMessageSets = false;
  
  private static volatile ExtensionRegistryLite emptyRegistry;
  
  private static final Class<?> extensionClass;
  
  private final Map<ObjectIntPair, GeneratedMessageLite.GeneratedExtension<?, ?>> extensionsByNumber;
  
  static {
    doFullRuntimeInheritanceCheck = true;
    extensionClass = resolveExtensionClass();
    EMPTY_REGISTRY_LITE = new ExtensionRegistryLite(true);
  }
  
  static Class<?> resolveExtensionClass() {
    try {
      return Class.forName("com.android.framework.protobuf.Extension");
    } catch (ClassNotFoundException classNotFoundException) {
      return null;
    } 
  }
  
  public static boolean isEagerlyParseMessageSets() {
    return eagerlyParseMessageSets;
  }
  
  public static void setEagerlyParseMessageSets(boolean paramBoolean) {
    eagerlyParseMessageSets = paramBoolean;
  }
  
  public static ExtensionRegistryLite newInstance() {
    ExtensionRegistryLite extensionRegistryLite;
    if (doFullRuntimeInheritanceCheck) {
      extensionRegistryLite = ExtensionRegistryFactory.create();
    } else {
      extensionRegistryLite = new ExtensionRegistryLite();
    } 
    return extensionRegistryLite;
  }
  
  public static ExtensionRegistryLite getEmptyRegistry() {
    // Byte code:
    //   0: getstatic com/android/framework/protobuf/ExtensionRegistryLite.emptyRegistry : Lcom/android/framework/protobuf/ExtensionRegistryLite;
    //   3: astore_0
    //   4: aload_0
    //   5: astore_1
    //   6: aload_0
    //   7: ifnonnull -> 56
    //   10: ldc com/android/framework/protobuf/ExtensionRegistryLite
    //   12: monitorenter
    //   13: getstatic com/android/framework/protobuf/ExtensionRegistryLite.emptyRegistry : Lcom/android/framework/protobuf/ExtensionRegistryLite;
    //   16: astore_0
    //   17: aload_0
    //   18: astore_1
    //   19: aload_0
    //   20: ifnonnull -> 44
    //   23: getstatic com/android/framework/protobuf/ExtensionRegistryLite.doFullRuntimeInheritanceCheck : Z
    //   26: ifeq -> 36
    //   29: invokestatic createEmpty : ()Lcom/android/framework/protobuf/ExtensionRegistryLite;
    //   32: astore_1
    //   33: goto -> 40
    //   36: getstatic com/android/framework/protobuf/ExtensionRegistryLite.EMPTY_REGISTRY_LITE : Lcom/android/framework/protobuf/ExtensionRegistryLite;
    //   39: astore_1
    //   40: aload_1
    //   41: putstatic com/android/framework/protobuf/ExtensionRegistryLite.emptyRegistry : Lcom/android/framework/protobuf/ExtensionRegistryLite;
    //   44: ldc com/android/framework/protobuf/ExtensionRegistryLite
    //   46: monitorexit
    //   47: goto -> 56
    //   50: astore_1
    //   51: ldc com/android/framework/protobuf/ExtensionRegistryLite
    //   53: monitorexit
    //   54: aload_1
    //   55: athrow
    //   56: aload_1
    //   57: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #126	-> 0
    //   #127	-> 4
    //   #128	-> 10
    //   #129	-> 13
    //   #130	-> 17
    //   #133	-> 23
    //   #134	-> 29
    //   #135	-> 36
    //   #137	-> 44
    //   #139	-> 56
    // Exception table:
    //   from	to	target	type
    //   13	17	50	finally
    //   23	29	50	finally
    //   29	33	50	finally
    //   36	40	50	finally
    //   40	44	50	finally
    //   44	47	50	finally
    //   51	54	50	finally
  }
  
  public ExtensionRegistryLite getUnmodifiable() {
    return new ExtensionRegistryLite(this);
  }
  
  public <ContainingType extends MessageLite> GeneratedMessageLite.GeneratedExtension<ContainingType, ?> findLiteExtensionByNumber(ContainingType paramContainingType, int paramInt) {
    Map<ObjectIntPair, GeneratedMessageLite.GeneratedExtension<?, ?>> map = this.extensionsByNumber;
    ObjectIntPair objectIntPair = new ObjectIntPair(paramContainingType, paramInt);
    return (GeneratedMessageLite.GeneratedExtension)map.get(objectIntPair);
  }
  
  public final void add(GeneratedMessageLite.GeneratedExtension<?, ?> paramGeneratedExtension) {
    Map<ObjectIntPair, GeneratedMessageLite.GeneratedExtension<?, ?>> map = this.extensionsByNumber;
    ObjectIntPair objectIntPair = new ObjectIntPair(paramGeneratedExtension.getContainingTypeDefaultInstance(), paramGeneratedExtension.getNumber());
    map.put(objectIntPair, paramGeneratedExtension);
  }
  
  public final void add(ExtensionLite<?, ?> paramExtensionLite) {
    if (GeneratedMessageLite.GeneratedExtension.class.isAssignableFrom(paramExtensionLite.getClass()))
      add((GeneratedMessageLite.GeneratedExtension<?, ?>)paramExtensionLite); 
    if (doFullRuntimeInheritanceCheck && ExtensionRegistryFactory.isFullRegistry(this))
      try {
        getClass().getMethod("add", new Class[] { extensionClass }).invoke(this, new Object[] { paramExtensionLite });
      } catch (Exception exception) {
        throw new IllegalArgumentException(String.format("Could not invoke ExtensionRegistry#add for %s", new Object[] { paramExtensionLite }), exception);
      }  
  }
  
  ExtensionRegistryLite() {
    this.extensionsByNumber = new HashMap<>();
  }
  
  ExtensionRegistryLite(ExtensionRegistryLite paramExtensionRegistryLite) {
    if (paramExtensionRegistryLite == EMPTY_REGISTRY_LITE) {
      this.extensionsByNumber = Collections.emptyMap();
    } else {
      this.extensionsByNumber = Collections.unmodifiableMap(paramExtensionRegistryLite.extensionsByNumber);
    } 
  }
  
  ExtensionRegistryLite(boolean paramBoolean) {
    this.extensionsByNumber = Collections.emptyMap();
  }
  
  private static final class ObjectIntPair {
    private final int number;
    
    private final Object object;
    
    ObjectIntPair(Object param1Object, int param1Int) {
      this.object = param1Object;
      this.number = param1Int;
    }
    
    public int hashCode() {
      return System.identityHashCode(this.object) * 65535 + this.number;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = param1Object instanceof ObjectIntPair;
      boolean bool1 = false;
      if (!bool)
        return false; 
      param1Object = param1Object;
      bool = bool1;
      if (this.object == ((ObjectIntPair)param1Object).object) {
        bool = bool1;
        if (this.number == ((ObjectIntPair)param1Object).number)
          bool = true; 
      } 
      return bool;
    }
  }
}
