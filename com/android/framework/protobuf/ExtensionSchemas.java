package com.android.framework.protobuf;

final class ExtensionSchemas {
  private static final ExtensionSchema<?> FULL_SCHEMA;
  
  private static final ExtensionSchema<?> LITE_SCHEMA = new ExtensionSchemaLite();
  
  static {
    FULL_SCHEMA = loadSchemaForFullRuntime();
  }
  
  private static ExtensionSchema<?> loadSchemaForFullRuntime() {
    try {
      Class<?> clazz = Class.forName("com.android.framework.protobuf.ExtensionSchemaFull");
      return clazz.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
    } catch (Exception exception) {
      return null;
    } 
  }
  
  static ExtensionSchema<?> lite() {
    return LITE_SCHEMA;
  }
  
  static ExtensionSchema<?> full() {
    ExtensionSchema<?> extensionSchema = FULL_SCHEMA;
    if (extensionSchema != null)
      return extensionSchema; 
    throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
  }
}
