package com.android.framework.protobuf;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class GeneratedMessageLite<MessageType extends GeneratedMessageLite<MessageType, BuilderType>, BuilderType extends GeneratedMessageLite.Builder<MessageType, BuilderType>> extends AbstractMessageLite<MessageType, BuilderType> {
  protected UnknownFieldSetLite unknownFields = UnknownFieldSetLite.getDefaultInstance();
  
  protected int memoizedSerializedSize = -1;
  
  public final Parser<MessageType> getParserForType() {
    return (Parser<MessageType>)dynamicMethod(MethodToInvoke.GET_PARSER);
  }
  
  public final MessageType getDefaultInstanceForType() {
    return (MessageType)dynamicMethod(MethodToInvoke.GET_DEFAULT_INSTANCE);
  }
  
  public final BuilderType newBuilderForType() {
    return (BuilderType)dynamicMethod(MethodToInvoke.NEW_BUILDER);
  }
  
  public String toString() {
    return MessageLiteToString.toString(this, super.toString());
  }
  
  public int hashCode() {
    if (this.memoizedHashCode != 0)
      return this.memoizedHashCode; 
    this.memoizedHashCode = Protobuf.getInstance().<GeneratedMessageLite>schemaFor(this).hashCode(this);
    return this.memoizedHashCode;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (!getDefaultInstanceForType().getClass().isInstance(paramObject))
      return false; 
    return Protobuf.getInstance().<GeneratedMessageLite>schemaFor(this).equals(this, (GeneratedMessageLite)paramObject);
  }
  
  private final void ensureUnknownFieldsInitialized() {
    if (this.unknownFields == UnknownFieldSetLite.getDefaultInstance())
      this.unknownFields = UnknownFieldSetLite.newInstance(); 
  }
  
  protected boolean parseUnknownField(int paramInt, CodedInputStream paramCodedInputStream) throws IOException {
    if (WireFormat.getTagWireType(paramInt) == 4)
      return false; 
    ensureUnknownFieldsInitialized();
    return this.unknownFields.mergeFieldFrom(paramInt, paramCodedInputStream);
  }
  
  protected void mergeVarintField(int paramInt1, int paramInt2) {
    ensureUnknownFieldsInitialized();
    this.unknownFields.mergeVarintField(paramInt1, paramInt2);
  }
  
  protected void mergeLengthDelimitedField(int paramInt, ByteString paramByteString) {
    ensureUnknownFieldsInitialized();
    this.unknownFields.mergeLengthDelimitedField(paramInt, paramByteString);
  }
  
  protected void makeImmutable() {
    Protobuf.getInstance().<GeneratedMessageLite>schemaFor(this).makeImmutable(this);
  }
  
  protected final <MessageType extends GeneratedMessageLite<MessageType, BuilderType>, BuilderType extends Builder<MessageType, BuilderType>> BuilderType createBuilder() {
    return (BuilderType)dynamicMethod(MethodToInvoke.NEW_BUILDER);
  }
  
  protected final <MessageType extends GeneratedMessageLite<MessageType, BuilderType>, BuilderType extends Builder<MessageType, BuilderType>> BuilderType createBuilder(MessageType paramMessageType) {
    return createBuilder().mergeFrom(paramMessageType);
  }
  
  public final boolean isInitialized() {
    return isInitialized(this, Boolean.TRUE.booleanValue());
  }
  
  public final BuilderType toBuilder() {
    Builder builder = (Builder)dynamicMethod(MethodToInvoke.NEW_BUILDER);
    builder.mergeFrom(this);
    return (BuilderType)builder;
  }
  
  class MethodToInvoke extends Enum<MethodToInvoke> {
    private static final MethodToInvoke[] $VALUES;
    
    public static final MethodToInvoke BUILD_MESSAGE_INFO = new MethodToInvoke("BUILD_MESSAGE_INFO", 2);
    
    public static final MethodToInvoke GET_DEFAULT_INSTANCE;
    
    public static MethodToInvoke[] values() {
      return (MethodToInvoke[])$VALUES.clone();
    }
    
    public static MethodToInvoke valueOf(String param1String) {
      return Enum.<MethodToInvoke>valueOf(MethodToInvoke.class, param1String);
    }
    
    private MethodToInvoke(GeneratedMessageLite this$0, int param1Int) {
      super((String)this$0, param1Int);
    }
    
    public static final MethodToInvoke GET_MEMOIZED_IS_INITIALIZED = new MethodToInvoke("GET_MEMOIZED_IS_INITIALIZED", 0);
    
    public static final MethodToInvoke GET_PARSER;
    
    public static final MethodToInvoke NEW_BUILDER;
    
    public static final MethodToInvoke NEW_MUTABLE_INSTANCE = new MethodToInvoke("NEW_MUTABLE_INSTANCE", 3);
    
    public static final MethodToInvoke SET_MEMOIZED_IS_INITIALIZED = new MethodToInvoke("SET_MEMOIZED_IS_INITIALIZED", 1);
    
    static {
      NEW_BUILDER = new MethodToInvoke("NEW_BUILDER", 4);
      GET_DEFAULT_INSTANCE = new MethodToInvoke("GET_DEFAULT_INSTANCE", 5);
      MethodToInvoke methodToInvoke = new MethodToInvoke("GET_PARSER", 6);
      $VALUES = new MethodToInvoke[] { GET_MEMOIZED_IS_INITIALIZED, SET_MEMOIZED_IS_INITIALIZED, BUILD_MESSAGE_INFO, NEW_MUTABLE_INSTANCE, NEW_BUILDER, GET_DEFAULT_INSTANCE, methodToInvoke };
    }
  }
  
  protected Object dynamicMethod(MethodToInvoke paramMethodToInvoke, Object paramObject) {
    return dynamicMethod(paramMethodToInvoke, paramObject, (Object)null);
  }
  
  protected Object dynamicMethod(MethodToInvoke paramMethodToInvoke) {
    return dynamicMethod(paramMethodToInvoke, (Object)null, (Object)null);
  }
  
  int getMemoizedSerializedSize() {
    return this.memoizedSerializedSize;
  }
  
  void setMemoizedSerializedSize(int paramInt) {
    this.memoizedSerializedSize = paramInt;
  }
  
  public void writeTo(CodedOutputStream paramCodedOutputStream) throws IOException {
    Protobuf protobuf = Protobuf.getInstance();
    Schema<GeneratedMessageLite> schema = protobuf.schemaFor(this);
    schema.writeTo(this, CodedOutputStreamWriter.forCodedOutput(paramCodedOutputStream));
  }
  
  public int getSerializedSize() {
    if (this.memoizedSerializedSize == -1)
      this.memoizedSerializedSize = Protobuf.getInstance().<GeneratedMessageLite>schemaFor(this).getSerializedSize(this); 
    return this.memoizedSerializedSize;
  }
  
  Object buildMessageInfo() throws Exception {
    return dynamicMethod(MethodToInvoke.BUILD_MESSAGE_INFO);
  }
  
  private static Map<Object, GeneratedMessageLite<?, ?>> defaultInstanceMap = new ConcurrentHashMap<>();
  
  static <T extends GeneratedMessageLite<?, ?>> T getDefaultInstance(Class<T> paramClass) {
    GeneratedMessageLite<?, ?> generatedMessageLite1 = defaultInstanceMap.get(paramClass);
    GeneratedMessageLite<?, ?> generatedMessageLite2 = generatedMessageLite1;
    if (generatedMessageLite1 == null)
      try {
        Class.forName(paramClass.getName(), true, paramClass.getClassLoader());
        generatedMessageLite2 = defaultInstanceMap.get(paramClass);
      } catch (ClassNotFoundException classNotFoundException) {
        throw new IllegalStateException("Class initialization cannot fail.", classNotFoundException);
      }  
    generatedMessageLite1 = generatedMessageLite2;
    if (generatedMessageLite2 == null) {
      generatedMessageLite1 = (GeneratedMessageLite<?, ?>)((GeneratedMessageLite)UnsafeUtil.<GeneratedMessageLite>allocateInstance((Class<GeneratedMessageLite>)classNotFoundException)).getDefaultInstanceForType();
      if (generatedMessageLite1 != null) {
        defaultInstanceMap.put(classNotFoundException, generatedMessageLite1);
      } else {
        throw new IllegalStateException();
      } 
    } 
    return (T)generatedMessageLite1;
  }
  
  protected static <T extends GeneratedMessageLite<?, ?>> void registerDefaultInstance(Class<T> paramClass, T paramT) {
    defaultInstanceMap.put(paramClass, (GeneratedMessageLite<?, ?>)paramT);
  }
  
  protected static Object newMessageInfo(MessageLite paramMessageLite, String paramString, Object[] paramArrayOfObject) {
    return new RawMessageInfo(paramMessageLite, paramString, paramArrayOfObject);
  }
  
  protected final void mergeUnknownFields(UnknownFieldSetLite paramUnknownFieldSetLite) {
    this.unknownFields = UnknownFieldSetLite.mutableCopyOf(this.unknownFields, paramUnknownFieldSetLite);
  }
  
  public static abstract class Builder<MessageType extends GeneratedMessageLite<MessageType, BuilderType>, BuilderType extends Builder<MessageType, BuilderType>> extends AbstractMessageLite.Builder<MessageType, BuilderType> {
    private final MessageType defaultInstance;
    
    protected MessageType instance;
    
    protected boolean isBuilt;
    
    protected Builder(MessageType param1MessageType) {
      this.defaultInstance = param1MessageType;
      GeneratedMessageLite.MethodToInvoke methodToInvoke = GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE;
      this.instance = (MessageType)param1MessageType.dynamicMethod(methodToInvoke);
      this.isBuilt = false;
    }
    
    protected void copyOnWrite() {
      if (this.isBuilt) {
        MessageType messageType = this.instance;
        GeneratedMessageLite.MethodToInvoke methodToInvoke = GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE;
        GeneratedMessageLite generatedMessageLite = (GeneratedMessageLite)messageType.dynamicMethod(methodToInvoke);
        mergeFromInstance((MessageType)generatedMessageLite, this.instance);
        this.instance = (MessageType)generatedMessageLite;
        this.isBuilt = false;
      } 
    }
    
    public final boolean isInitialized() {
      return GeneratedMessageLite.isInitialized(this.instance, false);
    }
    
    public final BuilderType clear() {
      this.instance = (MessageType)this.instance.dynamicMethod(GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE);
      return (BuilderType)this;
    }
    
    public BuilderType clone() {
      Object object = getDefaultInstanceForType().newBuilderForType();
      object.mergeFrom(buildPartial());
      return (BuilderType)object;
    }
    
    public MessageType buildPartial() {
      if (this.isBuilt)
        return this.instance; 
      this.instance.makeImmutable();
      this.isBuilt = true;
      return this.instance;
    }
    
    public final MessageType build() {
      MessageType messageType = buildPartial();
      if (messageType.isInitialized())
        return messageType; 
      throw newUninitializedMessageException(messageType);
    }
    
    protected BuilderType internalMergeFrom(MessageType param1MessageType) {
      return mergeFrom(param1MessageType);
    }
    
    public BuilderType mergeFrom(MessageType param1MessageType) {
      copyOnWrite();
      mergeFromInstance(this.instance, param1MessageType);
      return (BuilderType)this;
    }
    
    private void mergeFromInstance(MessageType param1MessageType1, MessageType param1MessageType2) {
      Protobuf.getInstance().<MessageType>schemaFor(param1MessageType1).mergeFrom(param1MessageType1, param1MessageType2);
    }
    
    public MessageType getDefaultInstanceForType() {
      return this.defaultInstance;
    }
    
    public BuilderType mergeFrom(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2, ExtensionRegistryLite param1ExtensionRegistryLite) throws InvalidProtocolBufferException {
      copyOnWrite();
      try {
        Schema<MessageType> schema = Protobuf.getInstance().schemaFor(this.instance);
        MessageType messageType = this.instance;
        ArrayDecoders.Registers registers = new ArrayDecoders.Registers();
        this(param1ExtensionRegistryLite);
        schema.mergeFrom(messageType, param1ArrayOfbyte, param1Int1, param1Int1 + param1Int2, registers);
        return (BuilderType)this;
      } catch (InvalidProtocolBufferException invalidProtocolBufferException) {
        throw invalidProtocolBufferException;
      } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
        throw InvalidProtocolBufferException.truncatedMessage();
      } catch (IOException iOException) {
        throw new RuntimeException("Reading from byte array should not throw IOException.", iOException);
      } 
    }
    
    public BuilderType mergeFrom(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws InvalidProtocolBufferException {
      return mergeFrom(param1ArrayOfbyte, param1Int1, param1Int2, ExtensionRegistryLite.getEmptyRegistry());
    }
    
    public BuilderType mergeFrom(CodedInputStream param1CodedInputStream, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      copyOnWrite();
      try {
        Schema<MessageType> schema = Protobuf.getInstance().schemaFor(this.instance);
        MessageType messageType = this.instance;
        CodedInputStreamReader codedInputStreamReader = CodedInputStreamReader.forCodedInput(param1CodedInputStream);
        schema.mergeFrom(messageType, codedInputStreamReader, param1ExtensionRegistryLite);
        return (BuilderType)this;
      } catch (RuntimeException runtimeException) {
        if (runtimeException.getCause() instanceof IOException)
          throw (IOException)runtimeException.getCause(); 
        throw runtimeException;
      } 
    }
  }
  
  class ExtendableMessage<MessageType extends ExtendableMessage<MessageType, BuilderType>, BuilderType extends ExtendableBuilder<MessageType, BuilderType>> extends GeneratedMessageLite<MessageType, BuilderType> implements ExtendableMessageOrBuilder<MessageType, BuilderType> {
    protected FieldSet<GeneratedMessageLite.ExtensionDescriptor> extensions = FieldSet.emptySet();
    
    protected final void mergeExtensionFields(MessageType param1MessageType) {
      if (this.extensions.isImmutable())
        this.extensions = this.extensions.clone(); 
      this.extensions.mergeFrom(((ExtendableMessage)param1MessageType).extensions);
    }
    
    protected <MessageType extends MessageLite> boolean parseUnknownField(MessageType param1MessageType, CodedInputStream param1CodedInputStream, ExtensionRegistryLite param1ExtensionRegistryLite, int param1Int) throws IOException {
      int i = WireFormat.getTagFieldNumber(param1Int);
      GeneratedMessageLite.GeneratedExtension<MessageType, ?> generatedExtension = param1ExtensionRegistryLite.findLiteExtensionByNumber(param1MessageType, i);
      return parseExtension(param1CodedInputStream, param1ExtensionRegistryLite, generatedExtension, param1Int, i);
    }
    
    private boolean parseExtension(CodedInputStream param1CodedInputStream, ExtensionRegistryLite param1ExtensionRegistryLite, GeneratedMessageLite.GeneratedExtension<?, ?> param1GeneratedExtension, int param1Int1, int param1Int2) throws IOException {
      // Byte code:
      //   0: iload #4
      //   2: invokestatic getTagWireType : (I)I
      //   5: istore #6
      //   7: iconst_0
      //   8: istore #7
      //   10: iconst_0
      //   11: istore #8
      //   13: aload_3
      //   14: ifnonnull -> 23
      //   17: iconst_1
      //   18: istore #7
      //   20: goto -> 113
      //   23: aload_3
      //   24: getfield descriptor : Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;
      //   27: astore #9
      //   29: aload #9
      //   31: invokevirtual getLiteType : ()Lcom/android/framework/protobuf/WireFormat$FieldType;
      //   34: astore #9
      //   36: iload #6
      //   38: aload #9
      //   40: iconst_0
      //   41: invokestatic getWireFormatForFieldType : (Lcom/android/framework/protobuf/WireFormat$FieldType;Z)I
      //   44: if_icmpne -> 53
      //   47: iconst_0
      //   48: istore #8
      //   50: goto -> 113
      //   53: aload_3
      //   54: getfield descriptor : Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;
      //   57: getfield isRepeated : Z
      //   60: ifeq -> 110
      //   63: aload_3
      //   64: getfield descriptor : Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;
      //   67: getfield type : Lcom/android/framework/protobuf/WireFormat$FieldType;
      //   70: astore #9
      //   72: aload #9
      //   74: invokevirtual isPackable : ()Z
      //   77: ifeq -> 110
      //   80: aload_3
      //   81: getfield descriptor : Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;
      //   84: astore #9
      //   86: aload #9
      //   88: invokevirtual getLiteType : ()Lcom/android/framework/protobuf/WireFormat$FieldType;
      //   91: astore #9
      //   93: iload #6
      //   95: aload #9
      //   97: iconst_1
      //   98: invokestatic getWireFormatForFieldType : (Lcom/android/framework/protobuf/WireFormat$FieldType;Z)I
      //   101: if_icmpne -> 110
      //   104: iconst_1
      //   105: istore #8
      //   107: goto -> 113
      //   110: iconst_1
      //   111: istore #7
      //   113: iload #7
      //   115: ifeq -> 126
      //   118: aload_0
      //   119: iload #4
      //   121: aload_1
      //   122: invokevirtual parseUnknownField : (ILcom/android/framework/protobuf/CodedInputStream;)Z
      //   125: ireturn
      //   126: aload_0
      //   127: invokevirtual ensureExtensionsAreMutable : ()Lcom/android/framework/protobuf/FieldSet;
      //   130: pop
      //   131: iload #8
      //   133: ifeq -> 277
      //   136: aload_1
      //   137: invokevirtual readRawVarint32 : ()I
      //   140: istore #4
      //   142: aload_1
      //   143: iload #4
      //   145: invokevirtual pushLimit : (I)I
      //   148: istore #5
      //   150: aload_3
      //   151: getfield descriptor : Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;
      //   154: invokevirtual getLiteType : ()Lcom/android/framework/protobuf/WireFormat$FieldType;
      //   157: getstatic com/android/framework/protobuf/WireFormat$FieldType.ENUM : Lcom/android/framework/protobuf/WireFormat$FieldType;
      //   160: if_acmpne -> 229
      //   163: aload_1
      //   164: invokevirtual getBytesUntilLimit : ()I
      //   167: ifle -> 268
      //   170: aload_1
      //   171: invokevirtual readEnum : ()I
      //   174: istore #4
      //   176: aload_3
      //   177: getfield descriptor : Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;
      //   180: invokevirtual getEnumType : ()Lcom/android/framework/protobuf/Internal$EnumLiteMap;
      //   183: iload #4
      //   185: invokeinterface findValueByNumber : (I)Lcom/android/framework/protobuf/Internal$EnumLite;
      //   190: astore #10
      //   192: aload #10
      //   194: ifnonnull -> 199
      //   197: iconst_1
      //   198: ireturn
      //   199: aload_0
      //   200: getfield extensions : Lcom/android/framework/protobuf/FieldSet;
      //   203: astore #9
      //   205: aload_3
      //   206: getfield descriptor : Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;
      //   209: astore_2
      //   210: aload_3
      //   211: aload #10
      //   213: invokevirtual singularToFieldSetType : (Ljava/lang/Object;)Ljava/lang/Object;
      //   216: astore #10
      //   218: aload #9
      //   220: aload_2
      //   221: aload #10
      //   223: invokevirtual addRepeatedField : (Lcom/android/framework/protobuf/FieldSet$FieldDescriptorLite;Ljava/lang/Object;)V
      //   226: goto -> 163
      //   229: aload_1
      //   230: invokevirtual getBytesUntilLimit : ()I
      //   233: ifle -> 268
      //   236: aload_3
      //   237: getfield descriptor : Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;
      //   240: astore_2
      //   241: aload_2
      //   242: invokevirtual getLiteType : ()Lcom/android/framework/protobuf/WireFormat$FieldType;
      //   245: astore_2
      //   246: aload_1
      //   247: aload_2
      //   248: iconst_0
      //   249: invokestatic readPrimitiveField : (Lcom/android/framework/protobuf/CodedInputStream;Lcom/android/framework/protobuf/WireFormat$FieldType;Z)Ljava/lang/Object;
      //   252: astore_2
      //   253: aload_0
      //   254: getfield extensions : Lcom/android/framework/protobuf/FieldSet;
      //   257: aload_3
      //   258: getfield descriptor : Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;
      //   261: aload_2
      //   262: invokevirtual addRepeatedField : (Lcom/android/framework/protobuf/FieldSet$FieldDescriptorLite;Ljava/lang/Object;)V
      //   265: goto -> 229
      //   268: aload_1
      //   269: iload #5
      //   271: invokevirtual popLimit : (I)V
      //   274: goto -> 529
      //   277: getstatic com/android/framework/protobuf/GeneratedMessageLite$1.$SwitchMap$com$google$protobuf$WireFormat$JavaType : [I
      //   280: aload_3
      //   281: getfield descriptor : Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;
      //   284: invokevirtual getLiteJavaType : ()Lcom/android/framework/protobuf/WireFormat$JavaType;
      //   287: invokevirtual ordinal : ()I
      //   290: iaload
      //   291: istore #4
      //   293: iload #4
      //   295: iconst_1
      //   296: if_icmpeq -> 363
      //   299: iload #4
      //   301: iconst_2
      //   302: if_icmpeq -> 325
      //   305: aload_3
      //   306: getfield descriptor : Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;
      //   309: astore_2
      //   310: aload_2
      //   311: invokevirtual getLiteType : ()Lcom/android/framework/protobuf/WireFormat$FieldType;
      //   314: astore_2
      //   315: aload_1
      //   316: aload_2
      //   317: iconst_0
      //   318: invokestatic readPrimitiveField : (Lcom/android/framework/protobuf/CodedInputStream;Lcom/android/framework/protobuf/WireFormat$FieldType;Z)Ljava/lang/Object;
      //   321: astore_1
      //   322: goto -> 476
      //   325: aload_1
      //   326: invokevirtual readEnum : ()I
      //   329: istore #4
      //   331: aload_3
      //   332: getfield descriptor : Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;
      //   335: invokevirtual getEnumType : ()Lcom/android/framework/protobuf/Internal$EnumLiteMap;
      //   338: iload #4
      //   340: invokeinterface findValueByNumber : (I)Lcom/android/framework/protobuf/Internal$EnumLite;
      //   345: astore_1
      //   346: aload_1
      //   347: ifnonnull -> 360
      //   350: aload_0
      //   351: iload #5
      //   353: iload #4
      //   355: invokevirtual mergeVarintField : (II)V
      //   358: iconst_1
      //   359: ireturn
      //   360: goto -> 476
      //   363: aconst_null
      //   364: astore #10
      //   366: aload #10
      //   368: astore #9
      //   370: aload_3
      //   371: getfield descriptor : Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;
      //   374: invokevirtual isRepeated : ()Z
      //   377: ifne -> 414
      //   380: aload_0
      //   381: getfield extensions : Lcom/android/framework/protobuf/FieldSet;
      //   384: aload_3
      //   385: getfield descriptor : Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;
      //   388: invokevirtual getField : (Lcom/android/framework/protobuf/FieldSet$FieldDescriptorLite;)Ljava/lang/Object;
      //   391: checkcast com/android/framework/protobuf/MessageLite
      //   394: astore #11
      //   396: aload #10
      //   398: astore #9
      //   400: aload #11
      //   402: ifnull -> 414
      //   405: aload #11
      //   407: invokeinterface toBuilder : ()Lcom/android/framework/protobuf/MessageLite$Builder;
      //   412: astore #9
      //   414: aload #9
      //   416: astore #10
      //   418: aload #9
      //   420: ifnonnull -> 434
      //   423: aload_3
      //   424: invokevirtual getMessageDefaultInstance : ()Lcom/android/framework/protobuf/MessageLite;
      //   427: invokeinterface newBuilderForType : ()Lcom/android/framework/protobuf/MessageLite$Builder;
      //   432: astore #10
      //   434: aload_3
      //   435: getfield descriptor : Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;
      //   438: invokevirtual getLiteType : ()Lcom/android/framework/protobuf/WireFormat$FieldType;
      //   441: getstatic com/android/framework/protobuf/WireFormat$FieldType.GROUP : Lcom/android/framework/protobuf/WireFormat$FieldType;
      //   444: if_acmpne -> 461
      //   447: aload_1
      //   448: aload_3
      //   449: invokevirtual getNumber : ()I
      //   452: aload #10
      //   454: aload_2
      //   455: invokevirtual readGroup : (ILcom/android/framework/protobuf/MessageLite$Builder;Lcom/android/framework/protobuf/ExtensionRegistryLite;)V
      //   458: goto -> 468
      //   461: aload_1
      //   462: aload #10
      //   464: aload_2
      //   465: invokevirtual readMessage : (Lcom/android/framework/protobuf/MessageLite$Builder;Lcom/android/framework/protobuf/ExtensionRegistryLite;)V
      //   468: aload #10
      //   470: invokeinterface build : ()Lcom/android/framework/protobuf/MessageLite;
      //   475: astore_1
      //   476: aload_3
      //   477: getfield descriptor : Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;
      //   480: invokevirtual isRepeated : ()Z
      //   483: ifeq -> 513
      //   486: aload_0
      //   487: getfield extensions : Lcom/android/framework/protobuf/FieldSet;
      //   490: astore_2
      //   491: aload_3
      //   492: getfield descriptor : Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;
      //   495: astore #9
      //   497: aload_3
      //   498: aload_1
      //   499: invokevirtual singularToFieldSetType : (Ljava/lang/Object;)Ljava/lang/Object;
      //   502: astore_1
      //   503: aload_2
      //   504: aload #9
      //   506: aload_1
      //   507: invokevirtual addRepeatedField : (Lcom/android/framework/protobuf/FieldSet$FieldDescriptorLite;Ljava/lang/Object;)V
      //   510: goto -> 529
      //   513: aload_0
      //   514: getfield extensions : Lcom/android/framework/protobuf/FieldSet;
      //   517: aload_3
      //   518: getfield descriptor : Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;
      //   521: aload_3
      //   522: aload_1
      //   523: invokevirtual singularToFieldSetType : (Ljava/lang/Object;)Ljava/lang/Object;
      //   526: invokevirtual setField : (Lcom/android/framework/protobuf/FieldSet$FieldDescriptorLite;Ljava/lang/Object;)V
      //   529: iconst_1
      //   530: ireturn
      // Line number table:
      //   Java source line number -> byte code offset
      //   #540	-> 0
      //   #541	-> 7
      //   #542	-> 10
      //   #543	-> 13
      //   #544	-> 17
      //   #545	-> 23
      //   #547	-> 29
      //   #546	-> 36
      //   #548	-> 47
      //   #549	-> 53
      //   #550	-> 72
      //   #553	-> 86
      //   #552	-> 93
      //   #554	-> 104
      //   #556	-> 110
      //   #559	-> 113
      //   #560	-> 118
      //   #563	-> 126
      //   #565	-> 131
      //   #566	-> 136
      //   #567	-> 142
      //   #568	-> 150
      //   #569	-> 163
      //   #570	-> 170
      //   #571	-> 176
      //   #572	-> 192
      //   #575	-> 197
      //   #577	-> 199
      //   #578	-> 210
      //   #577	-> 218
      //   #579	-> 226
      //   #581	-> 229
      //   #582	-> 236
      //   #584	-> 241
      //   #583	-> 246
      //   #585	-> 253
      //   #586	-> 265
      //   #588	-> 268
      //   #589	-> 274
      //   #591	-> 277
      //   #623	-> 305
      //   #625	-> 310
      //   #624	-> 315
      //   #613	-> 325
      //   #614	-> 331
      //   #617	-> 346
      //   #618	-> 350
      //   #619	-> 358
      //   #617	-> 360
      //   #594	-> 363
      //   #595	-> 366
      //   #596	-> 380
      //   #597	-> 396
      //   #598	-> 405
      //   #601	-> 414
      //   #602	-> 423
      //   #604	-> 434
      //   #605	-> 447
      //   #607	-> 461
      //   #609	-> 468
      //   #610	-> 476
      //   #629	-> 476
      //   #630	-> 486
      //   #631	-> 497
      //   #630	-> 503
      //   #633	-> 513
      //   #636	-> 529
    }
    
    protected <MessageType extends MessageLite> boolean parseUnknownFieldAsMessageSet(MessageType param1MessageType, CodedInputStream param1CodedInputStream, ExtensionRegistryLite param1ExtensionRegistryLite, int param1Int) throws IOException {
      if (param1Int == WireFormat.MESSAGE_SET_ITEM_TAG) {
        mergeMessageSetExtensionFromCodedStream(param1MessageType, param1CodedInputStream, param1ExtensionRegistryLite);
        return true;
      } 
      int i = WireFormat.getTagWireType(param1Int);
      if (i == 2)
        return parseUnknownField(param1MessageType, param1CodedInputStream, param1ExtensionRegistryLite, param1Int); 
      return param1CodedInputStream.skipField(param1Int);
    }
    
    private <MessageType extends MessageLite> void mergeMessageSetExtensionFromCodedStream(MessageType param1MessageType, CodedInputStream param1CodedInputStream, ExtensionRegistryLite param1ExtensionRegistryLite) throws IOException {
      // Byte code:
      //   0: iconst_0
      //   1: istore #4
      //   3: aconst_null
      //   4: astore #5
      //   6: aconst_null
      //   7: astore #6
      //   9: aload_2
      //   10: invokevirtual readTag : ()I
      //   13: istore #7
      //   15: iload #7
      //   17: ifne -> 23
      //   20: goto -> 146
      //   23: iload #7
      //   25: getstatic com/android/framework/protobuf/WireFormat.MESSAGE_SET_TYPE_ID_TAG : I
      //   28: if_icmpne -> 74
      //   31: aload_2
      //   32: invokevirtual readUInt32 : ()I
      //   35: istore #4
      //   37: iload #4
      //   39: istore #8
      //   41: aload #5
      //   43: astore #9
      //   45: aload #6
      //   47: astore #10
      //   49: iload #4
      //   51: ifeq -> 194
      //   54: aload_3
      //   55: aload_1
      //   56: iload #4
      //   58: invokevirtual findLiteExtensionByNumber : (Lcom/android/framework/protobuf/MessageLite;I)Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;
      //   61: astore #10
      //   63: iload #4
      //   65: istore #8
      //   67: aload #5
      //   69: astore #9
      //   71: goto -> 194
      //   74: iload #7
      //   76: getstatic com/android/framework/protobuf/WireFormat.MESSAGE_SET_MESSAGE_TAG : I
      //   79: if_icmpne -> 125
      //   82: iload #4
      //   84: ifeq -> 108
      //   87: aload #6
      //   89: ifnull -> 108
      //   92: aload_0
      //   93: aload_2
      //   94: aload #6
      //   96: aload_3
      //   97: iload #4
      //   99: invokespecial eagerlyMergeMessageSetExtension : (Lcom/android/framework/protobuf/CodedInputStream;Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;Lcom/android/framework/protobuf/ExtensionRegistryLite;I)V
      //   102: aconst_null
      //   103: astore #5
      //   105: goto -> 9
      //   108: aload_2
      //   109: invokevirtual readBytes : ()Lcom/android/framework/protobuf/ByteString;
      //   112: astore #9
      //   114: iload #4
      //   116: istore #8
      //   118: aload #6
      //   120: astore #10
      //   122: goto -> 194
      //   125: iload #4
      //   127: istore #8
      //   129: aload #5
      //   131: astore #9
      //   133: aload #6
      //   135: astore #10
      //   137: aload_2
      //   138: iload #7
      //   140: invokevirtual skipField : (I)Z
      //   143: ifne -> 194
      //   146: aload_2
      //   147: getstatic com/android/framework/protobuf/WireFormat.MESSAGE_SET_ITEM_END_TAG : I
      //   150: invokevirtual checkLastTagWas : (I)V
      //   153: aload #5
      //   155: ifnull -> 193
      //   158: iload #4
      //   160: ifeq -> 193
      //   163: aload #6
      //   165: ifnull -> 180
      //   168: aload_0
      //   169: aload #5
      //   171: aload_3
      //   172: aload #6
      //   174: invokespecial mergeMessageSetExtensionFromBytes : (Lcom/android/framework/protobuf/ByteString;Lcom/android/framework/protobuf/ExtensionRegistryLite;Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;)V
      //   177: goto -> 193
      //   180: aload #5
      //   182: ifnull -> 193
      //   185: aload_0
      //   186: iload #4
      //   188: aload #5
      //   190: invokevirtual mergeLengthDelimitedField : (ILcom/android/framework/protobuf/ByteString;)V
      //   193: return
      //   194: iload #8
      //   196: istore #4
      //   198: aload #9
      //   200: astore #5
      //   202: aload #10
      //   204: astore #6
      //   206: goto -> 9
      // Line number table:
      //   Java source line number -> byte code offset
      //   #697	-> 0
      //   #698	-> 3
      //   #699	-> 6
      //   #704	-> 9
      //   #705	-> 15
      //   #706	-> 20
      //   #709	-> 23
      //   #710	-> 31
      //   #711	-> 37
      //   #712	-> 54
      //   #715	-> 74
      //   #716	-> 82
      //   #717	-> 87
      //   #720	-> 92
      //   #721	-> 102
      //   #722	-> 105
      //   #726	-> 108
      //   #729	-> 125
      //   #730	-> 146
      //   #734	-> 146
      //   #737	-> 153
      //   #738	-> 163
      //   #739	-> 168
      //   #741	-> 180
      //   #742	-> 185
      //   #746	-> 193
      //   #733	-> 194
    }
    
    private void eagerlyMergeMessageSetExtension(CodedInputStream param1CodedInputStream, GeneratedMessageLite.GeneratedExtension<?, ?> param1GeneratedExtension, ExtensionRegistryLite param1ExtensionRegistryLite, int param1Int) throws IOException {
      int i = WireFormat.makeTag(param1Int, 2);
      parseExtension(param1CodedInputStream, param1ExtensionRegistryLite, param1GeneratedExtension, i, param1Int);
    }
    
    private void mergeMessageSetExtensionFromBytes(ByteString param1ByteString, ExtensionRegistryLite param1ExtensionRegistryLite, GeneratedMessageLite.GeneratedExtension<?, ?> param1GeneratedExtension) throws IOException {
      MessageLite.Builder builder1 = null;
      MessageLite messageLite2 = (MessageLite)this.extensions.getField(param1GeneratedExtension.descriptor);
      if (messageLite2 != null)
        builder1 = messageLite2.toBuilder(); 
      MessageLite.Builder builder2 = builder1;
      if (builder1 == null)
        builder2 = param1GeneratedExtension.getMessageDefaultInstance().newBuilderForType(); 
      builder2.mergeFrom(param1ByteString, param1ExtensionRegistryLite);
      MessageLite messageLite1 = builder2.build();
      FieldSet<GeneratedMessageLite.ExtensionDescriptor> fieldSet = ensureExtensionsAreMutable();
      GeneratedMessageLite.ExtensionDescriptor extensionDescriptor = param1GeneratedExtension.descriptor;
      fieldSet.setField(extensionDescriptor, param1GeneratedExtension.singularToFieldSetType(messageLite1));
    }
    
    FieldSet<GeneratedMessageLite.ExtensionDescriptor> ensureExtensionsAreMutable() {
      if (this.extensions.isImmutable())
        this.extensions = this.extensions.clone(); 
      return this.extensions;
    }
    
    private void verifyExtensionContainingType(GeneratedMessageLite.GeneratedExtension<MessageType, ?> param1GeneratedExtension) {
      if (param1GeneratedExtension.getContainingTypeDefaultInstance() == getDefaultInstanceForType())
        return; 
      throw new IllegalArgumentException("This extension is for a different message type.  Please make sure that you are not suppressing any generics type warnings.");
    }
    
    public final <Type> boolean hasExtension(ExtensionLite<MessageType, Type> param1ExtensionLite) {
      param1ExtensionLite = GeneratedMessageLite.checkIsLite(param1ExtensionLite);
      verifyExtensionContainingType((GeneratedMessageLite.GeneratedExtension<MessageType, ?>)param1ExtensionLite);
      return this.extensions.hasField(((GeneratedMessageLite.GeneratedExtension)param1ExtensionLite).descriptor);
    }
    
    public final <Type> int getExtensionCount(ExtensionLite<MessageType, List<Type>> param1ExtensionLite) {
      param1ExtensionLite = (ExtensionLite)GeneratedMessageLite.checkIsLite((ExtensionLite)param1ExtensionLite);
      verifyExtensionContainingType((GeneratedMessageLite.GeneratedExtension<MessageType, ?>)param1ExtensionLite);
      return this.extensions.getRepeatedFieldCount(((GeneratedMessageLite.GeneratedExtension)param1ExtensionLite).descriptor);
    }
    
    public final <Type> Type getExtension(ExtensionLite<MessageType, Type> param1ExtensionLite) {
      param1ExtensionLite = GeneratedMessageLite.checkIsLite(param1ExtensionLite);
      verifyExtensionContainingType((GeneratedMessageLite.GeneratedExtension<MessageType, ?>)param1ExtensionLite);
      Object object = this.extensions.getField(((GeneratedMessageLite.GeneratedExtension)param1ExtensionLite).descriptor);
      if (object == null)
        return ((GeneratedMessageLite.GeneratedExtension)param1ExtensionLite).defaultValue; 
      return (Type)param1ExtensionLite.fromFieldSetType(object);
    }
    
    public final <Type> Type getExtension(ExtensionLite<MessageType, List<Type>> param1ExtensionLite, int param1Int) {
      param1ExtensionLite = (ExtensionLite)GeneratedMessageLite.checkIsLite((ExtensionLite)param1ExtensionLite);
      verifyExtensionContainingType((GeneratedMessageLite.GeneratedExtension<MessageType, ?>)param1ExtensionLite);
      FieldSet<GeneratedMessageLite.ExtensionDescriptor> fieldSet = this.extensions;
      GeneratedMessageLite.ExtensionDescriptor extensionDescriptor = ((GeneratedMessageLite.GeneratedExtension)param1ExtensionLite).descriptor;
      Object object = fieldSet.getRepeatedField(extensionDescriptor, param1Int);
      return (Type)param1ExtensionLite.singularFromFieldSetType(object);
    }
    
    protected boolean extensionsAreInitialized() {
      return this.extensions.isInitialized();
    }
    
    class ExtensionWriter {
      private final Iterator<Map.Entry<GeneratedMessageLite.ExtensionDescriptor, Object>> iter;
      
      private final boolean messageSetWireFormat;
      
      private Map.Entry<GeneratedMessageLite.ExtensionDescriptor, Object> next;
      
      final GeneratedMessageLite.ExtendableMessage this$0;
      
      private ExtensionWriter(boolean param2Boolean) {
        Iterator<Map.Entry<GeneratedMessageLite.ExtensionDescriptor, Object>> iterator = GeneratedMessageLite.ExtendableMessage.this.extensions.iterator();
        if (iterator.hasNext())
          this.next = this.iter.next(); 
        this.messageSetWireFormat = param2Boolean;
      }
      
      public void writeUntil(int param2Int, CodedOutputStream param2CodedOutputStream) throws IOException {
        while (true) {
          Map.Entry<GeneratedMessageLite.ExtensionDescriptor, Object> entry = this.next;
          if (entry != null && ((GeneratedMessageLite.ExtensionDescriptor)entry.getKey()).getNumber() < param2Int) {
            GeneratedMessageLite.ExtensionDescriptor extensionDescriptor = this.next.getKey();
            if (this.messageSetWireFormat && 
              extensionDescriptor.getLiteJavaType() == WireFormat.JavaType.MESSAGE && 
              !extensionDescriptor.isRepeated()) {
              param2CodedOutputStream.writeMessageSetExtension(extensionDescriptor.getNumber(), (MessageLite)this.next.getValue());
            } else {
              FieldSet.writeField(extensionDescriptor, this.next.getValue(), param2CodedOutputStream);
            } 
            if (this.iter.hasNext()) {
              this.next = this.iter.next();
              continue;
            } 
            this.next = null;
            continue;
          } 
          break;
        } 
      }
    }
    
    protected ExtensionWriter newExtensionWriter() {
      return new ExtensionWriter(false);
    }
    
    protected ExtensionWriter newMessageSetExtensionWriter() {
      return new ExtensionWriter(true);
    }
    
    protected int extensionsSerializedSize() {
      return this.extensions.getSerializedSize();
    }
    
    protected int extensionsSerializedSizeAsMessageSet() {
      return this.extensions.getMessageSetSerializedSize();
    }
  }
  
  class ExtendableBuilder<MessageType extends ExtendableMessage<MessageType, BuilderType>, BuilderType extends ExtendableBuilder<MessageType, BuilderType>> extends Builder<MessageType, BuilderType> implements ExtendableMessageOrBuilder<MessageType, BuilderType> {
    protected ExtendableBuilder(GeneratedMessageLite this$0) {
      super((MessageType)this$0);
    }
    
    void internalSetExtensionSet(FieldSet<GeneratedMessageLite.ExtensionDescriptor> param1FieldSet) {
      copyOnWrite();
      ((GeneratedMessageLite.ExtendableMessage)this.instance).extensions = param1FieldSet;
    }
    
    protected void copyOnWrite() {
      if (!this.isBuilt)
        return; 
      super.copyOnWrite();
      ((GeneratedMessageLite.ExtendableMessage)this.instance).extensions = ((GeneratedMessageLite.ExtendableMessage)this.instance).extensions.clone();
    }
    
    private FieldSet<GeneratedMessageLite.ExtensionDescriptor> ensureExtensionsAreMutable() {
      FieldSet<GeneratedMessageLite.ExtensionDescriptor> fieldSet1 = ((GeneratedMessageLite.ExtendableMessage)this.instance).extensions;
      FieldSet<GeneratedMessageLite.ExtensionDescriptor> fieldSet2 = fieldSet1;
      if (fieldSet1.isImmutable()) {
        fieldSet2 = fieldSet1.clone();
        ((GeneratedMessageLite.ExtendableMessage)this.instance).extensions = fieldSet2;
      } 
      return fieldSet2;
    }
    
    public final MessageType buildPartial() {
      if (this.isBuilt)
        return this.instance; 
      ((GeneratedMessageLite.ExtendableMessage)this.instance).extensions.makeImmutable();
      return super.buildPartial();
    }
    
    private void verifyExtensionContainingType(GeneratedMessageLite.GeneratedExtension<MessageType, ?> param1GeneratedExtension) {
      if (param1GeneratedExtension.getContainingTypeDefaultInstance() == getDefaultInstanceForType())
        return; 
      throw new IllegalArgumentException("This extension is for a different message type.  Please make sure that you are not suppressing any generics type warnings.");
    }
    
    public final <Type> boolean hasExtension(ExtensionLite<MessageType, Type> param1ExtensionLite) {
      return ((GeneratedMessageLite.ExtendableMessage)this.instance).hasExtension(param1ExtensionLite);
    }
    
    public final <Type> int getExtensionCount(ExtensionLite<MessageType, List<Type>> param1ExtensionLite) {
      return ((GeneratedMessageLite.ExtendableMessage)this.instance).getExtensionCount(param1ExtensionLite);
    }
    
    public final <Type> Type getExtension(ExtensionLite<MessageType, Type> param1ExtensionLite) {
      return (Type)((GeneratedMessageLite.ExtendableMessage)this.instance).getExtension(param1ExtensionLite);
    }
    
    public final <Type> Type getExtension(ExtensionLite<MessageType, List<Type>> param1ExtensionLite, int param1Int) {
      return (Type)((GeneratedMessageLite.ExtendableMessage)this.instance).getExtension(param1ExtensionLite, param1Int);
    }
    
    public final <Type> BuilderType setExtension(ExtensionLite<MessageType, Type> param1ExtensionLite, Type param1Type) {
      GeneratedMessageLite.GeneratedExtension<MessageType, ?> generatedExtension = GeneratedMessageLite.checkIsLite(param1ExtensionLite);
      verifyExtensionContainingType(generatedExtension);
      copyOnWrite();
      FieldSet<GeneratedMessageLite.ExtensionDescriptor> fieldSet = ensureExtensionsAreMutable();
      GeneratedMessageLite.ExtensionDescriptor extensionDescriptor = generatedExtension.descriptor;
      fieldSet.setField(extensionDescriptor, generatedExtension.toFieldSetType(param1Type));
      return (BuilderType)this;
    }
    
    public final <Type> BuilderType setExtension(ExtensionLite<MessageType, List<Type>> param1ExtensionLite, int param1Int, Type param1Type) {
      GeneratedMessageLite.GeneratedExtension<MessageType, ?> generatedExtension = GeneratedMessageLite.checkIsLite((ExtensionLite)param1ExtensionLite);
      verifyExtensionContainingType(generatedExtension);
      copyOnWrite();
      FieldSet<GeneratedMessageLite.ExtensionDescriptor> fieldSet = ensureExtensionsAreMutable();
      GeneratedMessageLite.ExtensionDescriptor extensionDescriptor = generatedExtension.descriptor;
      param1Type = (Type)generatedExtension.singularToFieldSetType(param1Type);
      fieldSet.setRepeatedField(extensionDescriptor, param1Int, param1Type);
      return (BuilderType)this;
    }
    
    public final <Type> BuilderType addExtension(ExtensionLite<MessageType, List<Type>> param1ExtensionLite, Type param1Type) {
      GeneratedMessageLite.GeneratedExtension<MessageType, ?> generatedExtension = GeneratedMessageLite.checkIsLite((ExtensionLite)param1ExtensionLite);
      verifyExtensionContainingType(generatedExtension);
      copyOnWrite();
      FieldSet<GeneratedMessageLite.ExtensionDescriptor> fieldSet = ensureExtensionsAreMutable();
      GeneratedMessageLite.ExtensionDescriptor extensionDescriptor = generatedExtension.descriptor;
      fieldSet.addRepeatedField(extensionDescriptor, generatedExtension.singularToFieldSetType(param1Type));
      return (BuilderType)this;
    }
    
    public final <Type> BuilderType clearExtension(ExtensionLite<MessageType, ?> param1ExtensionLite) {
      param1ExtensionLite = GeneratedMessageLite.checkIsLite((ExtensionLite)param1ExtensionLite);
      verifyExtensionContainingType((GeneratedMessageLite.GeneratedExtension<MessageType, ?>)param1ExtensionLite);
      copyOnWrite();
      ensureExtensionsAreMutable().clearField(((GeneratedMessageLite.GeneratedExtension)param1ExtensionLite).descriptor);
      return (BuilderType)this;
    }
  }
  
  public static <ContainingType extends MessageLite, Type> GeneratedExtension<ContainingType, Type> newSingularGeneratedExtension(ContainingType paramContainingType, Type paramType, MessageLite paramMessageLite, Internal.EnumLiteMap<?> paramEnumLiteMap, int paramInt, WireFormat.FieldType paramFieldType, Class paramClass) {
    return new GeneratedExtension<>(paramContainingType, paramType, paramMessageLite, new ExtensionDescriptor(paramEnumLiteMap, paramInt, paramFieldType, false, false), paramClass);
  }
  
  public static <ContainingType extends MessageLite, Type> GeneratedExtension<ContainingType, Type> newRepeatedGeneratedExtension(ContainingType paramContainingType, MessageLite paramMessageLite, Internal.EnumLiteMap<?> paramEnumLiteMap, int paramInt, WireFormat.FieldType paramFieldType, boolean paramBoolean, Class paramClass) {
    List<?> list = Collections.emptyList();
    return new GeneratedExtension<>(paramContainingType, (Type)list, paramMessageLite, new ExtensionDescriptor(paramEnumLiteMap, paramInt, paramFieldType, true, paramBoolean), paramClass);
  }
  
  class ExtensionDescriptor implements FieldSet.FieldDescriptorLite<ExtensionDescriptor> {
    final Internal.EnumLiteMap<?> enumTypeMap;
    
    final boolean isPacked;
    
    final boolean isRepeated;
    
    final int number;
    
    final WireFormat.FieldType type;
    
    ExtensionDescriptor(GeneratedMessageLite this$0, int param1Int, WireFormat.FieldType param1FieldType, boolean param1Boolean1, boolean param1Boolean2) {
      this.enumTypeMap = (Internal.EnumLiteMap<?>)this$0;
      this.number = param1Int;
      this.type = param1FieldType;
      this.isRepeated = param1Boolean1;
      this.isPacked = param1Boolean2;
    }
    
    public int getNumber() {
      return this.number;
    }
    
    public WireFormat.FieldType getLiteType() {
      return this.type;
    }
    
    public WireFormat.JavaType getLiteJavaType() {
      return this.type.getJavaType();
    }
    
    public boolean isRepeated() {
      return this.isRepeated;
    }
    
    public boolean isPacked() {
      return this.isPacked;
    }
    
    public Internal.EnumLiteMap<?> getEnumType() {
      return this.enumTypeMap;
    }
    
    public MessageLite.Builder internalMergeFrom(MessageLite.Builder param1Builder, MessageLite param1MessageLite) {
      return ((GeneratedMessageLite.Builder<GeneratedMessageLite, MessageLite.Builder>)param1Builder).mergeFrom((GeneratedMessageLite)param1MessageLite);
    }
    
    public int compareTo(ExtensionDescriptor param1ExtensionDescriptor) {
      return this.number - param1ExtensionDescriptor.number;
    }
  }
  
  static Method getMethodOrDie(Class paramClass, String paramString, Class... paramVarArgs) {
    try {
      return paramClass.getMethod(paramString, paramVarArgs);
    } catch (NoSuchMethodException noSuchMethodException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Generated message class \"");
      stringBuilder.append(paramClass.getName());
      stringBuilder.append("\" missing method \"");
      stringBuilder.append(paramString);
      stringBuilder.append("\".");
      throw new RuntimeException(stringBuilder.toString(), noSuchMethodException);
    } 
  }
  
  static Object invokeOrDie(Method paramMethod, Object paramObject, Object... paramVarArgs) {
    try {
      return paramMethod.invoke(paramObject, paramVarArgs);
    } catch (IllegalAccessException illegalAccessException) {
      throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", illegalAccessException);
    } catch (InvocationTargetException invocationTargetException) {
      Throwable throwable = invocationTargetException.getCause();
      if (!(throwable instanceof RuntimeException)) {
        if (throwable instanceof Error)
          throw (Error)throwable; 
        throw new RuntimeException("Unexpected exception thrown by generated accessor method.", throwable);
      } 
      throw (RuntimeException)throwable;
    } 
  }
  
  class GeneratedExtension<ContainingType extends MessageLite, Type> extends ExtensionLite<ContainingType, Type> {
    final ContainingType containingTypeDefaultInstance;
    
    final Type defaultValue;
    
    final GeneratedMessageLite.ExtensionDescriptor descriptor;
    
    final MessageLite messageDefaultInstance;
    
    GeneratedExtension(GeneratedMessageLite this$0, Type param1Type, MessageLite param1MessageLite, GeneratedMessageLite.ExtensionDescriptor param1ExtensionDescriptor, Class param1Class) {
      if (this$0 != null) {
        if (param1ExtensionDescriptor.getLiteType() != WireFormat.FieldType.MESSAGE || param1MessageLite != null) {
          this.containingTypeDefaultInstance = (ContainingType)this$0;
          this.defaultValue = param1Type;
          this.messageDefaultInstance = param1MessageLite;
          this.descriptor = param1ExtensionDescriptor;
          return;
        } 
        throw new IllegalArgumentException("Null messageDefaultInstance");
      } 
      throw new IllegalArgumentException("Null containingTypeDefaultInstance");
    }
    
    public ContainingType getContainingTypeDefaultInstance() {
      return this.containingTypeDefaultInstance;
    }
    
    public int getNumber() {
      return this.descriptor.getNumber();
    }
    
    public MessageLite getMessageDefaultInstance() {
      return this.messageDefaultInstance;
    }
    
    Object fromFieldSetType(Object param1Object) {
      if (this.descriptor.isRepeated()) {
        if (this.descriptor.getLiteJavaType() == WireFormat.JavaType.ENUM) {
          ArrayList<Object> arrayList = new ArrayList();
          for (Object param1Object : param1Object)
            arrayList.add(singularFromFieldSetType(param1Object)); 
          return arrayList;
        } 
        return param1Object;
      } 
      return singularFromFieldSetType(param1Object);
    }
    
    Object singularFromFieldSetType(Object param1Object) {
      if (this.descriptor.getLiteJavaType() == WireFormat.JavaType.ENUM)
        return this.descriptor.enumTypeMap.findValueByNumber(((Integer)param1Object).intValue()); 
      return param1Object;
    }
    
    Object toFieldSetType(Object param1Object) {
      if (this.descriptor.isRepeated()) {
        if (this.descriptor.getLiteJavaType() == WireFormat.JavaType.ENUM) {
          ArrayList<Object> arrayList = new ArrayList();
          for (param1Object = ((List)param1Object).iterator(); param1Object.hasNext(); ) {
            Object object = param1Object.next();
            arrayList.add(singularToFieldSetType(object));
          } 
          return arrayList;
        } 
        return param1Object;
      } 
      return singularToFieldSetType(param1Object);
    }
    
    Object singularToFieldSetType(Object param1Object) {
      if (this.descriptor.getLiteJavaType() == WireFormat.JavaType.ENUM)
        return Integer.valueOf(((Internal.EnumLite)param1Object).getNumber()); 
      return param1Object;
    }
    
    public WireFormat.FieldType getLiteType() {
      return this.descriptor.getLiteType();
    }
    
    public boolean isRepeated() {
      return this.descriptor.isRepeated;
    }
    
    public Type getDefaultValue() {
      return this.defaultValue;
    }
  }
  
  class SerializedForm implements Serializable {
    private static final long serialVersionUID = 0L;
    
    private final byte[] asBytes;
    
    private final Class<?> messageClass;
    
    private final String messageClassName;
    
    public static SerializedForm of(MessageLite param1MessageLite) {
      return new SerializedForm(param1MessageLite);
    }
    
    SerializedForm(GeneratedMessageLite this$0) {
      Class<?> clazz = this$0.getClass();
      this.messageClassName = clazz.getName();
      this.asBytes = this$0.toByteArray();
    }
    
    protected Object readResolve() throws ObjectStreamException {
      try {
        Class<?> clazz = resolveMessageClass();
        Field field = clazz.getDeclaredField("DEFAULT_INSTANCE");
        field.setAccessible(true);
        null = (MessageLite)field.get(null);
        return null.newBuilderForType().mergeFrom(this.asBytes).buildPartial();
      } catch (ClassNotFoundException classNotFoundException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unable to find proto buffer class: ");
        stringBuilder.append(this.messageClassName);
        throw new RuntimeException(stringBuilder.toString(), classNotFoundException);
      } catch (NoSuchFieldException noSuchFieldException) {
        return readResolveFallback();
      } catch (SecurityException securityException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unable to call DEFAULT_INSTANCE in ");
        stringBuilder.append(this.messageClassName);
        throw new RuntimeException(stringBuilder.toString(), securityException);
      } catch (IllegalAccessException illegalAccessException) {
        throw new RuntimeException("Unable to call parsePartialFrom", illegalAccessException);
      } catch (InvalidProtocolBufferException invalidProtocolBufferException) {
        throw new RuntimeException("Unable to understand proto buffer", invalidProtocolBufferException);
      } 
    }
    
    @Deprecated
    private Object readResolveFallback() throws ObjectStreamException {
      try {
        Class<?> clazz = resolveMessageClass();
        Field field = clazz.getDeclaredField("defaultInstance");
        field.setAccessible(true);
        MessageLite messageLite = (MessageLite)field.get(null);
        MessageLite.Builder builder2 = messageLite.newBuilderForType();
        byte[] arrayOfByte = this.asBytes;
        MessageLite.Builder builder1 = builder2.mergeFrom(arrayOfByte);
        return builder1.buildPartial();
      } catch (ClassNotFoundException classNotFoundException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unable to find proto buffer class: ");
        stringBuilder.append(this.messageClassName);
        throw new RuntimeException(stringBuilder.toString(), classNotFoundException);
      } catch (NoSuchFieldException noSuchFieldException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unable to find defaultInstance in ");
        stringBuilder.append(this.messageClassName);
        throw new RuntimeException(stringBuilder.toString(), noSuchFieldException);
      } catch (SecurityException securityException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unable to call defaultInstance in ");
        stringBuilder.append(this.messageClassName);
        throw new RuntimeException(stringBuilder.toString(), securityException);
      } catch (IllegalAccessException illegalAccessException) {
        throw new RuntimeException("Unable to call parsePartialFrom", illegalAccessException);
      } catch (InvalidProtocolBufferException invalidProtocolBufferException) {
        throw new RuntimeException("Unable to understand proto buffer", invalidProtocolBufferException);
      } 
    }
    
    private Class<?> resolveMessageClass() throws ClassNotFoundException {
      Class<?> clazz = this.messageClass;
      if (clazz == null)
        clazz = Class.forName(this.messageClassName); 
      return clazz;
    }
  }
  
  private static <MessageType extends ExtendableMessage<MessageType, BuilderType>, BuilderType extends ExtendableBuilder<MessageType, BuilderType>, T> GeneratedExtension<MessageType, T> checkIsLite(ExtensionLite<MessageType, T> paramExtensionLite) {
    if (paramExtensionLite.isLite())
      return (GeneratedExtension<MessageType, T>)paramExtensionLite; 
    throw new IllegalArgumentException("Expected a lite extension.");
  }
  
  protected static final <T extends GeneratedMessageLite<T, ?>> boolean isInitialized(T paramT, boolean paramBoolean) {
    MethodToInvoke methodToInvoke = MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED;
    byte b = ((Byte)paramT.dynamicMethod(methodToInvoke)).byteValue();
    if (b == 1)
      return true; 
    if (b == 0)
      return false; 
    boolean bool = Protobuf.getInstance().<T>schemaFor(paramT).isInitialized(paramT);
    if (paramBoolean) {
      MethodToInvoke methodToInvoke1 = MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED;
      if (bool) {
        T t = paramT;
      } else {
        methodToInvoke = null;
      } 
      paramT.dynamicMethod(methodToInvoke1, methodToInvoke);
    } 
    return bool;
  }
  
  protected static Internal.IntList emptyIntList() {
    return IntArrayList.emptyList();
  }
  
  protected static Internal.IntList mutableCopy(Internal.IntList paramIntList) {
    int i = paramIntList.size();
    if (i == 0) {
      i = 10;
    } else {
      i *= 2;
    } 
    return paramIntList.mutableCopyWithCapacity(i);
  }
  
  protected static Internal.LongList emptyLongList() {
    return LongArrayList.emptyList();
  }
  
  protected static Internal.LongList mutableCopy(Internal.LongList paramLongList) {
    int i = paramLongList.size();
    if (i == 0) {
      i = 10;
    } else {
      i *= 2;
    } 
    return paramLongList.mutableCopyWithCapacity(i);
  }
  
  protected static Internal.FloatList emptyFloatList() {
    return FloatArrayList.emptyList();
  }
  
  protected static Internal.FloatList mutableCopy(Internal.FloatList paramFloatList) {
    int i = paramFloatList.size();
    if (i == 0) {
      i = 10;
    } else {
      i *= 2;
    } 
    return paramFloatList.mutableCopyWithCapacity(i);
  }
  
  protected static Internal.DoubleList emptyDoubleList() {
    return DoubleArrayList.emptyList();
  }
  
  protected static Internal.DoubleList mutableCopy(Internal.DoubleList paramDoubleList) {
    int i = paramDoubleList.size();
    if (i == 0) {
      i = 10;
    } else {
      i *= 2;
    } 
    return paramDoubleList.mutableCopyWithCapacity(i);
  }
  
  protected static Internal.BooleanList emptyBooleanList() {
    return BooleanArrayList.emptyList();
  }
  
  protected static Internal.BooleanList mutableCopy(Internal.BooleanList paramBooleanList) {
    int i = paramBooleanList.size();
    if (i == 0) {
      i = 10;
    } else {
      i *= 2;
    } 
    return paramBooleanList.mutableCopyWithCapacity(i);
  }
  
  protected static <E> Internal.ProtobufList<E> emptyProtobufList() {
    return ProtobufArrayList.emptyList();
  }
  
  protected static <E> Internal.ProtobufList<E> mutableCopy(Internal.ProtobufList<E> paramProtobufList) {
    int i = paramProtobufList.size();
    if (i == 0) {
      i = 10;
    } else {
      i *= 2;
    } 
    return paramProtobufList.mutableCopyWithCapacity(i);
  }
  
  class DefaultInstanceBasedParser<T extends GeneratedMessageLite<T, ?>> extends AbstractParser<T> {
    private final T defaultInstance;
    
    public DefaultInstanceBasedParser(GeneratedMessageLite this$0) {
      this.defaultInstance = (T)this$0;
    }
    
    public T parsePartialFrom(CodedInputStream param1CodedInputStream, ExtensionRegistryLite param1ExtensionRegistryLite) throws InvalidProtocolBufferException {
      return GeneratedMessageLite.parsePartialFrom(this.defaultInstance, param1CodedInputStream, param1ExtensionRegistryLite);
    }
    
    public T parsePartialFrom(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2, ExtensionRegistryLite param1ExtensionRegistryLite) throws InvalidProtocolBufferException {
      return GeneratedMessageLite.parsePartialFrom(this.defaultInstance, param1ArrayOfbyte, param1Int1, param1Int2, param1ExtensionRegistryLite);
    }
  }
  
  static <T extends GeneratedMessageLite<T, ?>> T parsePartialFrom(T paramT, CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite) throws InvalidProtocolBufferException {
    GeneratedMessageLite generatedMessageLite = (GeneratedMessageLite)paramT.dynamicMethod(MethodToInvoke.NEW_MUTABLE_INSTANCE);
    try {
      Schema<GeneratedMessageLite> schema = Protobuf.getInstance().schemaFor(generatedMessageLite);
      schema.mergeFrom(generatedMessageLite, CodedInputStreamReader.forCodedInput(paramCodedInputStream), paramExtensionRegistryLite);
      schema.makeImmutable(generatedMessageLite);
      return (T)generatedMessageLite;
    } catch (IOException iOException) {
      if (iOException.getCause() instanceof InvalidProtocolBufferException)
        throw (InvalidProtocolBufferException)iOException.getCause(); 
      throw (new InvalidProtocolBufferException(iOException.getMessage())).setUnfinishedMessage(generatedMessageLite);
    } catch (RuntimeException runtimeException) {
      if (runtimeException.getCause() instanceof InvalidProtocolBufferException)
        throw (InvalidProtocolBufferException)runtimeException.getCause(); 
      throw runtimeException;
    } 
  }
  
  static <T extends GeneratedMessageLite<T, ?>> T parsePartialFrom(T paramT, byte[] paramArrayOfbyte, int paramInt1, int paramInt2, ExtensionRegistryLite paramExtensionRegistryLite) throws InvalidProtocolBufferException {
    GeneratedMessageLite generatedMessageLite = (GeneratedMessageLite)paramT.dynamicMethod(MethodToInvoke.NEW_MUTABLE_INSTANCE);
    try {
      Schema<GeneratedMessageLite> schema = Protobuf.getInstance().schemaFor(generatedMessageLite);
      ArrayDecoders.Registers registers = new ArrayDecoders.Registers();
      this(paramExtensionRegistryLite);
      schema.mergeFrom(generatedMessageLite, paramArrayOfbyte, paramInt1, paramInt1 + paramInt2, registers);
      schema.makeImmutable(generatedMessageLite);
      if (generatedMessageLite.memoizedHashCode == 0)
        return (T)generatedMessageLite; 
      RuntimeException runtimeException = new RuntimeException();
      this();
      throw runtimeException;
    } catch (IOException iOException) {
      if (iOException.getCause() instanceof InvalidProtocolBufferException)
        throw (InvalidProtocolBufferException)iOException.getCause(); 
      throw (new InvalidProtocolBufferException(iOException.getMessage())).setUnfinishedMessage(generatedMessageLite);
    } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
      throw InvalidProtocolBufferException.truncatedMessage().setUnfinishedMessage(generatedMessageLite);
    } 
  }
  
  protected static <T extends GeneratedMessageLite<T, ?>> T parsePartialFrom(T paramT, CodedInputStream paramCodedInputStream) throws InvalidProtocolBufferException {
    return parsePartialFrom(paramT, paramCodedInputStream, ExtensionRegistryLite.getEmptyRegistry());
  }
  
  private static <T extends GeneratedMessageLite<T, ?>> T checkMessageInitialized(T paramT) throws InvalidProtocolBufferException {
    if (paramT == null || paramT.isInitialized())
      return paramT; 
    UninitializedMessageException uninitializedMessageException = paramT.newUninitializedMessageException();
    InvalidProtocolBufferException invalidProtocolBufferException = uninitializedMessageException.asInvalidProtocolBufferException();
    throw invalidProtocolBufferException.setUnfinishedMessage(paramT);
  }
  
  protected static <T extends GeneratedMessageLite<T, ?>> T parseFrom(T paramT, ByteBuffer paramByteBuffer, ExtensionRegistryLite paramExtensionRegistryLite) throws InvalidProtocolBufferException {
    paramT = parseFrom(paramT, CodedInputStream.newInstance(paramByteBuffer), paramExtensionRegistryLite);
    return checkMessageInitialized(paramT);
  }
  
  protected static <T extends GeneratedMessageLite<T, ?>> T parseFrom(T paramT, ByteBuffer paramByteBuffer) throws InvalidProtocolBufferException {
    return parseFrom(paramT, paramByteBuffer, ExtensionRegistryLite.getEmptyRegistry());
  }
  
  protected static <T extends GeneratedMessageLite<T, ?>> T parseFrom(T paramT, ByteString paramByteString) throws InvalidProtocolBufferException {
    paramT = parseFrom(paramT, paramByteString, ExtensionRegistryLite.getEmptyRegistry());
    return checkMessageInitialized(paramT);
  }
  
  protected static <T extends GeneratedMessageLite<T, ?>> T parseFrom(T paramT, ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite) throws InvalidProtocolBufferException {
    return checkMessageInitialized(parsePartialFrom(paramT, paramByteString, paramExtensionRegistryLite));
  }
  
  private static <T extends GeneratedMessageLite<T, ?>> T parsePartialFrom(T paramT, ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite) throws InvalidProtocolBufferException {
    try {
      CodedInputStream codedInputStream = paramByteString.newCodedInput();
      paramT = parsePartialFrom(paramT, codedInputStream, paramExtensionRegistryLite);
      try {
        codedInputStream.checkLastTagWas(0);
        return paramT;
      } catch (InvalidProtocolBufferException invalidProtocolBufferException) {
        throw invalidProtocolBufferException.setUnfinishedMessage(paramT);
      } 
    } catch (InvalidProtocolBufferException invalidProtocolBufferException) {
      throw invalidProtocolBufferException;
    } 
  }
  
  private static <T extends GeneratedMessageLite<T, ?>> T parsePartialFrom(T paramT, byte[] paramArrayOfbyte, ExtensionRegistryLite paramExtensionRegistryLite) throws InvalidProtocolBufferException {
    int i = paramArrayOfbyte.length;
    paramT = parsePartialFrom(paramT, paramArrayOfbyte, 0, i, paramExtensionRegistryLite);
    return checkMessageInitialized(paramT);
  }
  
  protected static <T extends GeneratedMessageLite<T, ?>> T parseFrom(T paramT, byte[] paramArrayOfbyte) throws InvalidProtocolBufferException {
    int i = paramArrayOfbyte.length;
    ExtensionRegistryLite extensionRegistryLite = ExtensionRegistryLite.getEmptyRegistry();
    return checkMessageInitialized(parsePartialFrom(paramT, paramArrayOfbyte, 0, i, extensionRegistryLite));
  }
  
  protected static <T extends GeneratedMessageLite<T, ?>> T parseFrom(T paramT, byte[] paramArrayOfbyte, ExtensionRegistryLite paramExtensionRegistryLite) throws InvalidProtocolBufferException {
    int i = paramArrayOfbyte.length;
    paramT = parsePartialFrom(paramT, paramArrayOfbyte, 0, i, paramExtensionRegistryLite);
    return checkMessageInitialized(paramT);
  }
  
  protected static <T extends GeneratedMessageLite<T, ?>> T parseFrom(T paramT, InputStream paramInputStream) throws InvalidProtocolBufferException {
    CodedInputStream codedInputStream = CodedInputStream.newInstance(paramInputStream);
    ExtensionRegistryLite extensionRegistryLite = ExtensionRegistryLite.getEmptyRegistry();
    paramT = parsePartialFrom(paramT, codedInputStream, extensionRegistryLite);
    return checkMessageInitialized(paramT);
  }
  
  protected static <T extends GeneratedMessageLite<T, ?>> T parseFrom(T paramT, InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite) throws InvalidProtocolBufferException {
    paramT = parsePartialFrom(paramT, CodedInputStream.newInstance(paramInputStream), paramExtensionRegistryLite);
    return checkMessageInitialized(paramT);
  }
  
  protected static <T extends GeneratedMessageLite<T, ?>> T parseFrom(T paramT, CodedInputStream paramCodedInputStream) throws InvalidProtocolBufferException {
    return parseFrom(paramT, paramCodedInputStream, ExtensionRegistryLite.getEmptyRegistry());
  }
  
  protected static <T extends GeneratedMessageLite<T, ?>> T parseFrom(T paramT, CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite) throws InvalidProtocolBufferException {
    return checkMessageInitialized(parsePartialFrom(paramT, paramCodedInputStream, paramExtensionRegistryLite));
  }
  
  protected static <T extends GeneratedMessageLite<T, ?>> T parseDelimitedFrom(T paramT, InputStream paramInputStream) throws InvalidProtocolBufferException {
    ExtensionRegistryLite extensionRegistryLite = ExtensionRegistryLite.getEmptyRegistry();
    paramT = parsePartialDelimitedFrom(paramT, paramInputStream, extensionRegistryLite);
    return checkMessageInitialized(paramT);
  }
  
  protected static <T extends GeneratedMessageLite<T, ?>> T parseDelimitedFrom(T paramT, InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite) throws InvalidProtocolBufferException {
    paramT = parsePartialDelimitedFrom(paramT, paramInputStream, paramExtensionRegistryLite);
    return checkMessageInitialized(paramT);
  }
  
  private static <T extends GeneratedMessageLite<T, ?>> T parsePartialDelimitedFrom(T paramT, InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite) throws InvalidProtocolBufferException {
    try {
      int i = paramInputStream.read();
      if (i == -1)
        return null; 
      i = CodedInputStream.readRawVarint32(i, paramInputStream);
      paramInputStream = new AbstractMessageLite.Builder.LimitedInputStream(paramInputStream, i);
      CodedInputStream codedInputStream = CodedInputStream.newInstance(paramInputStream);
      paramT = parsePartialFrom(paramT, codedInputStream, paramExtensionRegistryLite);
      try {
        codedInputStream.checkLastTagWas(0);
        return paramT;
      } catch (InvalidProtocolBufferException invalidProtocolBufferException) {
        throw invalidProtocolBufferException.setUnfinishedMessage(paramT);
      } 
    } catch (IOException iOException) {
      throw new InvalidProtocolBufferException(iOException.getMessage());
    } 
  }
  
  protected abstract Object dynamicMethod(MethodToInvoke paramMethodToInvoke, Object paramObject1, Object paramObject2);
  
  class ExtendableMessageOrBuilder<MessageType extends ExtendableMessage<MessageType, BuilderType>, BuilderType extends ExtendableBuilder<MessageType, BuilderType>> implements MessageLiteOrBuilder {
    public abstract <Type> Type getExtension(ExtensionLite<MessageType, Type> param1ExtensionLite);
    
    public abstract <Type> Type getExtension(ExtensionLite<MessageType, List<Type>> param1ExtensionLite, int param1Int);
    
    public abstract <Type> int getExtensionCount(ExtensionLite<MessageType, List<Type>> param1ExtensionLite);
    
    public abstract <Type> boolean hasExtension(ExtensionLite<MessageType, Type> param1ExtensionLite);
  }
}
