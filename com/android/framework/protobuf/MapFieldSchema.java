package com.android.framework.protobuf;

import java.util.Map;

interface MapFieldSchema {
  Map<?, ?> forMapData(Object paramObject);
  
  MapEntryLite.Metadata<?, ?> forMapMetadata(Object paramObject);
  
  Map<?, ?> forMutableMapData(Object paramObject);
  
  int getSerializedSize(int paramInt, Object paramObject1, Object paramObject2);
  
  boolean isImmutable(Object paramObject);
  
  Object mergeFrom(Object paramObject1, Object paramObject2);
  
  Object newMapField(Object paramObject);
  
  Object toImmutable(Object paramObject);
}
