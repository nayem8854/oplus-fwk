package com.android.framework.protobuf;

import java.lang.reflect.Field;

final class OneofInfo {
  private final Field caseField;
  
  private final int id;
  
  private final Field valueField;
  
  public OneofInfo(int paramInt, Field paramField1, Field paramField2) {
    this.id = paramInt;
    this.caseField = paramField1;
    this.valueField = paramField2;
  }
  
  public int getId() {
    return this.id;
  }
  
  public Field getCaseField() {
    return this.caseField;
  }
  
  public Field getValueField() {
    return this.valueField;
  }
}
