package com.android.framework.protobuf;

final class TextFormatEscaper {
  static String escapeBytes(ByteSequence paramByteSequence) {
    StringBuilder stringBuilder = new StringBuilder(paramByteSequence.size());
    for (byte b = 0; b < paramByteSequence.size(); b++) {
      byte b1 = paramByteSequence.byteAt(b);
      if (b1 != 34) {
        if (b1 != 39) {
          if (b1 != 92) {
            switch (b1) {
              default:
                if (b1 >= 32 && b1 <= 126) {
                  stringBuilder.append((char)b1);
                  break;
                } 
                stringBuilder.append('\\');
                stringBuilder.append((char)((b1 >>> 6 & 0x3) + 48));
                stringBuilder.append((char)((b1 >>> 3 & 0x7) + 48));
                stringBuilder.append((char)((b1 & 0x7) + 48));
                break;
              case 13:
                stringBuilder.append("\\r");
                break;
              case 12:
                stringBuilder.append("\\f");
                break;
              case 11:
                stringBuilder.append("\\v");
                break;
              case 10:
                stringBuilder.append("\\n");
                break;
              case 9:
                stringBuilder.append("\\t");
                break;
              case 8:
                stringBuilder.append("\\b");
                break;
              case 7:
                stringBuilder.append("\\a");
                break;
            } 
          } else {
            stringBuilder.append("\\\\");
          } 
        } else {
          stringBuilder.append("\\'");
        } 
      } else {
        stringBuilder.append("\\\"");
      } 
    } 
    return stringBuilder.toString();
  }
  
  static String escapeBytes(ByteString paramByteString) {
    return escapeBytes((ByteSequence)new Object(paramByteString));
  }
  
  static String escapeBytes(byte[] paramArrayOfbyte) {
    return escapeBytes((ByteSequence)new Object(paramArrayOfbyte));
  }
  
  static String escapeText(String paramString) {
    return escapeBytes(ByteString.copyFromUtf8(paramString));
  }
  
  static String escapeDoubleQuotesAndBackslashes(String paramString) {
    return paramString.replace("\\", "\\\\").replace("\"", "\\\"");
  }
  
  private static interface ByteSequence {
    byte byteAt(int param1Int);
    
    int size();
  }
}
