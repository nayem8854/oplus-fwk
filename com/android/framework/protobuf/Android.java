package com.android.framework.protobuf;

final class Android {
  private static final boolean IS_ROBOLECTRIC;
  
  private static final Class<?> MEMORY_CLASS = getClassForName("libcore.io.Memory");
  
  static {
    boolean bool;
  }
  
  static {
    if (getClassForName("org.robolectric.Robolectric") != null) {
      bool = true;
    } else {
      bool = false;
    } 
    IS_ROBOLECTRIC = bool;
  }
  
  static boolean isOnAndroidDevice() {
    boolean bool;
    if (MEMORY_CLASS != null && !IS_ROBOLECTRIC) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  static Class<?> getMemoryClass() {
    return MEMORY_CLASS;
  }
  
  private static <T> Class<T> getClassForName(String paramString) {
    try {
      return (Class)Class.forName(paramString);
    } finally {
      paramString = null;
    } 
  }
}
