package com.android.framework.protobuf;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

final class MessageSetSchema<T> implements Schema<T> {
  private final MessageLite defaultInstance;
  
  private final ExtensionSchema<?> extensionSchema;
  
  private final boolean hasExtensions;
  
  private final UnknownFieldSchema<?, ?> unknownFieldSchema;
  
  private MessageSetSchema(UnknownFieldSchema<?, ?> paramUnknownFieldSchema, ExtensionSchema<?> paramExtensionSchema, MessageLite paramMessageLite) {
    this.unknownFieldSchema = paramUnknownFieldSchema;
    this.hasExtensions = paramExtensionSchema.hasExtensions(paramMessageLite);
    this.extensionSchema = paramExtensionSchema;
    this.defaultInstance = paramMessageLite;
  }
  
  static <T> MessageSetSchema<T> newSchema(UnknownFieldSchema<?, ?> paramUnknownFieldSchema, ExtensionSchema<?> paramExtensionSchema, MessageLite paramMessageLite) {
    return new MessageSetSchema<>(paramUnknownFieldSchema, paramExtensionSchema, paramMessageLite);
  }
  
  public T newInstance() {
    return (T)this.defaultInstance.newBuilderForType().buildPartial();
  }
  
  public boolean equals(T paramT1, T paramT2) {
    Object object1 = this.unknownFieldSchema.getFromMessage(paramT1);
    Object object2 = this.unknownFieldSchema.getFromMessage(paramT2);
    if (!object1.equals(object2))
      return false; 
    if (this.hasExtensions) {
      FieldSet<?> fieldSet1 = this.extensionSchema.getExtensions(paramT1);
      FieldSet<?> fieldSet2 = this.extensionSchema.getExtensions(paramT2);
      return fieldSet1.equals(fieldSet2);
    } 
    return true;
  }
  
  public int hashCode(T paramT) {
    int i = this.unknownFieldSchema.getFromMessage(paramT).hashCode();
    int j = i;
    if (this.hasExtensions) {
      FieldSet<?> fieldSet = this.extensionSchema.getExtensions(paramT);
      j = i * 53 + fieldSet.hashCode();
    } 
    return j;
  }
  
  public void mergeFrom(T paramT1, T paramT2) {
    SchemaUtil.mergeUnknownFields(this.unknownFieldSchema, paramT1, paramT2);
    if (this.hasExtensions)
      SchemaUtil.mergeExtensions(this.extensionSchema, paramT1, paramT2); 
  }
  
  public void writeTo(T paramT, Writer paramWriter) throws IOException {
    FieldSet<?> fieldSet = this.extensionSchema.getExtensions(paramT);
    Iterator<Map.Entry<?, Object>> iterator = fieldSet.iterator();
    while (iterator.hasNext()) {
      Map.Entry entry = iterator.next();
      FieldSet.FieldDescriptorLite fieldDescriptorLite = (FieldSet.FieldDescriptorLite)entry.getKey();
      if (fieldDescriptorLite.getLiteJavaType() == WireFormat.JavaType.MESSAGE && !fieldDescriptorLite.isRepeated() && !fieldDescriptorLite.isPacked()) {
        ByteString byteString;
        if (entry instanceof LazyField.LazyEntry) {
          int i = fieldDescriptorLite.getNumber();
          byteString = ((LazyField.LazyEntry)entry).getField().toByteString();
          paramWriter.writeMessageSetItem(i, byteString);
          continue;
        } 
        paramWriter.writeMessageSetItem(fieldDescriptorLite.getNumber(), byteString.getValue());
        continue;
      } 
      throw new IllegalStateException("Found invalid MessageSet item.");
    } 
    writeUnknownFieldsHelper(this.unknownFieldSchema, paramT, paramWriter);
  }
  
  private <UT, UB> void writeUnknownFieldsHelper(UnknownFieldSchema<UT, UB> paramUnknownFieldSchema, T paramT, Writer paramWriter) throws IOException {
    paramUnknownFieldSchema.writeAsMessageSetTo(paramUnknownFieldSchema.getFromMessage(paramT), paramWriter);
  }
  
  public void mergeFrom(T paramT, byte[] paramArrayOfbyte, int paramInt1, int paramInt2, ArrayDecoders.Registers paramRegisters) throws IOException {
    UnknownFieldSetLite unknownFieldSetLite = ((GeneratedMessageLite)paramT).unknownFields;
    if (unknownFieldSetLite == UnknownFieldSetLite.getDefaultInstance()) {
      unknownFieldSetLite = UnknownFieldSetLite.newInstance();
      ((GeneratedMessageLite)paramT).unknownFields = unknownFieldSetLite;
    } 
    GeneratedMessageLite.ExtendableMessage extendableMessage = (GeneratedMessageLite.ExtendableMessage)paramT;
    FieldSet<GeneratedMessageLite.ExtensionDescriptor> fieldSet = extendableMessage.ensureExtensionsAreMutable();
    extendableMessage = null;
    while (paramInt1 < paramInt2) {
      GeneratedMessageLite.GeneratedExtension generatedExtension;
      paramInt1 = ArrayDecoders.decodeVarint32(paramArrayOfbyte, paramInt1, paramRegisters);
      int i = paramRegisters.int1;
      if (i != WireFormat.MESSAGE_SET_ITEM_TAG) {
        if (WireFormat.getTagWireType(i) == 2) {
          ExtensionSchema<?> extensionSchema = this.extensionSchema;
          ExtensionRegistryLite extensionRegistryLite = paramRegisters.extensionRegistry;
          MessageLite messageLite = this.defaultInstance;
          int j = WireFormat.getTagFieldNumber(i);
          generatedExtension = (GeneratedMessageLite.GeneratedExtension)extensionSchema.findExtensionByNumber(extensionRegistryLite, messageLite, j);
          if (generatedExtension != null) {
            Protobuf protobuf = Protobuf.getInstance();
            Class<?> clazz = generatedExtension.getMessageDefaultInstance().getClass();
            Schema<?> schema = protobuf.schemaFor(clazz);
            paramInt1 = ArrayDecoders.decodeMessageField(schema, paramArrayOfbyte, paramInt1, paramInt2, paramRegisters);
            fieldSet.setField(generatedExtension.descriptor, paramRegisters.object1);
            continue;
          } 
          paramInt1 = ArrayDecoders.decodeUnknownField(i, paramArrayOfbyte, paramInt1, paramInt2, unknownFieldSetLite, paramRegisters);
          continue;
        } 
        paramInt1 = ArrayDecoders.skipField(i, paramArrayOfbyte, paramInt1, paramInt2, paramRegisters);
        continue;
      } 
      i = 0;
      ByteString byteString = null;
      while (paramInt1 < paramInt2) {
        paramInt1 = ArrayDecoders.decodeVarint32(paramArrayOfbyte, paramInt1, paramRegisters);
        int k = paramRegisters.int1;
        int j = WireFormat.getTagFieldNumber(k);
        int m = WireFormat.getTagWireType(k);
        if (j != 2) {
          if (j == 3) {
            if (generatedExtension != null) {
              Protobuf protobuf = Protobuf.getInstance();
              Class<?> clazz = generatedExtension.getMessageDefaultInstance().getClass();
              Schema<?> schema = protobuf.schemaFor(clazz);
              paramInt1 = ArrayDecoders.decodeMessageField(schema, paramArrayOfbyte, paramInt1, paramInt2, paramRegisters);
              fieldSet.setField(generatedExtension.descriptor, paramRegisters.object1);
              continue;
            } 
            if (m == 2) {
              paramInt1 = ArrayDecoders.decodeBytes(paramArrayOfbyte, paramInt1, paramRegisters);
              byteString = (ByteString)paramRegisters.object1;
              continue;
            } 
          } 
        } else if (m == 0) {
          paramInt1 = ArrayDecoders.decodeVarint32(paramArrayOfbyte, paramInt1, paramRegisters);
          i = paramRegisters.int1;
          ExtensionSchema<?> extensionSchema = this.extensionSchema;
          ExtensionRegistryLite extensionRegistryLite = paramRegisters.extensionRegistry;
          MessageLite messageLite = this.defaultInstance;
          GeneratedMessageLite.GeneratedExtension generatedExtension1 = (GeneratedMessageLite.GeneratedExtension)extensionSchema.findExtensionByNumber(extensionRegistryLite, messageLite, i);
          continue;
        } 
        if (k == WireFormat.MESSAGE_SET_ITEM_END_TAG)
          break; 
        paramInt1 = ArrayDecoders.skipField(k, paramArrayOfbyte, paramInt1, paramInt2, paramRegisters);
      } 
      if (byteString != null) {
        i = WireFormat.makeTag(i, 2);
        unknownFieldSetLite.storeField(i, byteString);
      } 
    } 
    if (paramInt1 == paramInt2)
      return; 
    throw InvalidProtocolBufferException.parseFailure();
  }
  
  public void mergeFrom(T paramT, Reader paramReader, ExtensionRegistryLite paramExtensionRegistryLite) throws IOException {
    mergeFromHelper(this.unknownFieldSchema, this.extensionSchema, paramT, paramReader, paramExtensionRegistryLite);
  }
  
  private <UT, UB, ET extends FieldSet.FieldDescriptorLite<ET>> void mergeFromHelper(UnknownFieldSchema<UT, UB> paramUnknownFieldSchema, ExtensionSchema<ET> paramExtensionSchema, T paramT, Reader paramReader, ExtensionRegistryLite paramExtensionRegistryLite) throws IOException {
    UB uB = paramUnknownFieldSchema.getBuilderFromMessage(paramT);
    FieldSet<ET> fieldSet = paramExtensionSchema.getMutableExtensions(paramT);
    try {
      while (true) {
        int i = paramReader.getFieldNumber();
        if (i == Integer.MAX_VALUE)
          return; 
        boolean bool = parseMessageSetItemOrUnknownField(paramReader, paramExtensionRegistryLite, paramExtensionSchema, fieldSet, paramUnknownFieldSchema, uB);
        if (bool)
          continue; 
        break;
      } 
      return;
    } finally {
      paramUnknownFieldSchema.setBuilderToMessage(paramT, uB);
    } 
  }
  
  public void makeImmutable(T paramT) {
    this.unknownFieldSchema.makeImmutable(paramT);
    this.extensionSchema.makeImmutable(paramT);
  }
  
  private <UT, UB, ET extends FieldSet.FieldDescriptorLite<ET>> boolean parseMessageSetItemOrUnknownField(Reader paramReader, ExtensionRegistryLite paramExtensionRegistryLite, ExtensionSchema<ET> paramExtensionSchema, FieldSet<ET> paramFieldSet, UnknownFieldSchema<UT, UB> paramUnknownFieldSchema, UB paramUB) throws IOException {
    int i = paramReader.getTag();
    if (i != WireFormat.MESSAGE_SET_ITEM_TAG) {
      if (WireFormat.getTagWireType(i) == 2) {
        MessageLite messageLite = this.defaultInstance;
        i = WireFormat.getTagFieldNumber(i);
        Object object1 = paramExtensionSchema.findExtensionByNumber(paramExtensionRegistryLite, messageLite, i);
        if (object1 != null) {
          paramExtensionSchema.parseLengthPrefixedMessageSetItem(paramReader, object1, paramExtensionRegistryLite, paramFieldSet);
          return true;
        } 
        return paramUnknownFieldSchema.mergeOneFieldFrom(paramUB, paramReader);
      } 
      return paramReader.skipField();
    } 
    i = 0;
    ByteString byteString = null;
    Object object = null;
    while (true) {
      int j = paramReader.getFieldNumber();
      if (j == Integer.MAX_VALUE)
        break; 
      j = paramReader.getTag();
      if (j == WireFormat.MESSAGE_SET_TYPE_ID_TAG) {
        i = paramReader.readUInt32();
        object = this.defaultInstance;
        object = paramExtensionSchema.findExtensionByNumber(paramExtensionRegistryLite, (MessageLite)object, i);
        continue;
      } 
      if (j == WireFormat.MESSAGE_SET_MESSAGE_TAG) {
        if (object != null) {
          paramExtensionSchema.parseLengthPrefixedMessageSetItem(paramReader, object, paramExtensionRegistryLite, paramFieldSet);
          continue;
        } 
        byteString = paramReader.readBytes();
        continue;
      } 
      if (!paramReader.skipField())
        break; 
    } 
    if (paramReader.getTag() == WireFormat.MESSAGE_SET_ITEM_END_TAG) {
      if (byteString != null)
        if (object != null) {
          paramExtensionSchema.parseMessageSetItem(byteString, object, paramExtensionRegistryLite, paramFieldSet);
        } else {
          paramUnknownFieldSchema.addLengthDelimited(paramUB, i, byteString);
        }  
      return true;
    } 
    throw InvalidProtocolBufferException.invalidEndTag();
  }
  
  public final boolean isInitialized(T paramT) {
    FieldSet<?> fieldSet = this.extensionSchema.getExtensions(paramT);
    return fieldSet.isInitialized();
  }
  
  public int getSerializedSize(T paramT) {
    int i = 0 + getUnknownFieldsSerializedSize(this.unknownFieldSchema, paramT);
    int j = i;
    if (this.hasExtensions)
      j = i + this.extensionSchema.getExtensions(paramT).getMessageSetSerializedSize(); 
    return j;
  }
  
  private <UT, UB> int getUnknownFieldsSerializedSize(UnknownFieldSchema<UT, UB> paramUnknownFieldSchema, T paramT) {
    paramT = (T)paramUnknownFieldSchema.getFromMessage(paramT);
    return paramUnknownFieldSchema.getSerializedSizeAsMessageSet((UT)paramT);
  }
}
