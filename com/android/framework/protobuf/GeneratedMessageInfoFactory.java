package com.android.framework.protobuf;

class GeneratedMessageInfoFactory implements MessageInfoFactory {
  private static final GeneratedMessageInfoFactory instance = new GeneratedMessageInfoFactory();
  
  public static GeneratedMessageInfoFactory getInstance() {
    return instance;
  }
  
  public boolean isSupported(Class<?> paramClass) {
    return GeneratedMessageLite.class.isAssignableFrom(paramClass);
  }
  
  public MessageInfo messageInfoFor(Class<?> paramClass) {
    if (GeneratedMessageLite.class.isAssignableFrom(paramClass))
      try {
        Class<? extends GeneratedMessageLite> clazz = paramClass.asSubclass(GeneratedMessageLite.class);
        clazz = GeneratedMessageLite.getDefaultInstance(clazz);
        return (MessageInfo)clazz.buildMessageInfo();
      } catch (Exception exception) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Unable to get message info for ");
        stringBuilder1.append(paramClass.getName());
        throw new RuntimeException(stringBuilder1.toString(), exception);
      }  
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unsupported message type: ");
    stringBuilder.append(paramClass.getName());
    throw new IllegalArgumentException(stringBuilder.toString());
  }
}
