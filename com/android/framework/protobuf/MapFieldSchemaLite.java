package com.android.framework.protobuf;

import java.util.Map;

class MapFieldSchemaLite implements MapFieldSchema {
  public Map<?, ?> forMutableMapData(Object paramObject) {
    return (MapFieldLite)paramObject;
  }
  
  public MapEntryLite.Metadata<?, ?> forMapMetadata(Object paramObject) {
    return ((MapEntryLite<?, ?>)paramObject).getMetadata();
  }
  
  public Map<?, ?> forMapData(Object paramObject) {
    return (MapFieldLite)paramObject;
  }
  
  public boolean isImmutable(Object paramObject) {
    return ((MapFieldLite)paramObject).isMutable() ^ true;
  }
  
  public Object toImmutable(Object paramObject) {
    ((MapFieldLite)paramObject).makeImmutable();
    return paramObject;
  }
  
  public Object newMapField(Object paramObject) {
    return MapFieldLite.emptyMapField().mutableCopy();
  }
  
  public Object mergeFrom(Object paramObject1, Object paramObject2) {
    return mergeFromLite(paramObject1, paramObject2);
  }
  
  private static <K, V> MapFieldLite<K, V> mergeFromLite(Object paramObject1, Object paramObject2) {
    MapFieldLite mapFieldLite = (MapFieldLite)paramObject1;
    paramObject2 = paramObject2;
    paramObject1 = mapFieldLite;
    if (!paramObject2.isEmpty()) {
      paramObject1 = mapFieldLite;
      if (!mapFieldLite.isMutable())
        paramObject1 = mapFieldLite.mutableCopy(); 
      paramObject1.mergeFrom((MapFieldLite)paramObject2);
    } 
    return (MapFieldLite<K, V>)paramObject1;
  }
  
  public int getSerializedSize(int paramInt, Object paramObject1, Object paramObject2) {
    return getSerializedSizeLite(paramInt, paramObject1, paramObject2);
  }
  
  private static <K, V> int getSerializedSizeLite(int paramInt, Object paramObject1, Object paramObject2) {
    MapFieldLite mapFieldLite = (MapFieldLite)paramObject1;
    paramObject1 = paramObject2;
    if (mapFieldLite.isEmpty())
      return 0; 
    int i = 0;
    for (Object paramObject2 : mapFieldLite.entrySet())
      i += paramObject1.computeMessageSize(paramInt, paramObject2.getKey(), paramObject2.getValue()); 
    return i;
  }
}
