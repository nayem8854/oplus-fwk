package com.android.id.impl;

import android.app.IOplusNotificationManager;
import android.app.OplusNotificationManager;
import android.content.Context;
import android.os.Binder;
import java.lang.reflect.Method;

public class IdProviderImpl {
  public String getOpenid(Context paramContext, String paramString) {
    try {
      OplusNotificationManager oplusNotificationManager = new OplusNotificationManager();
      this();
      Method method = IOplusNotificationManager.class.getDeclaredMethod("getOpenid", new Class[] { String.class, int.class, String.class });
      return (String)method.invoke(oplusNotificationManager, new Object[] { paramContext.getPackageName(), Integer.valueOf(Binder.getCallingUid()), paramString });
    } catch (Exception exception) {
      exception.printStackTrace();
      return null;
    } 
  }
  
  public String getGUID(Context paramContext) {
    return getOpenid(paramContext, "GUID");
  }
  
  public String getOUID(Context paramContext) {
    return getOpenid(paramContext, "OUID");
  }
  
  public String getDUID(Context paramContext) {
    return getOpenid(paramContext, "DUID");
  }
  
  public String getAUID(Context paramContext) {
    return getOpenid(paramContext, "AUID");
  }
  
  public String getAPID(Context paramContext) {
    return getOpenid(paramContext, "APID");
  }
  
  public boolean checkGetOpenid(Context paramContext, String paramString) {
    try {
      OplusNotificationManager oplusNotificationManager = new OplusNotificationManager();
      this();
      Method method = IOplusNotificationManager.class.getDeclaredMethod("checkGetOpenid", new Class[] { String.class, int.class, String.class });
      return ((Boolean)method.invoke(oplusNotificationManager, new Object[] { paramContext.getPackageName(), Integer.valueOf(Binder.getCallingUid()), paramString })).booleanValue();
    } catch (Exception exception) {
      exception.printStackTrace();
      return false;
    } 
  }
  
  public boolean checkGetGUID(Context paramContext) {
    return checkGetOpenid(paramContext, "GUID");
  }
  
  public boolean checkGetAPID(Context paramContext) {
    return checkGetOpenid(paramContext, "APID");
  }
}
