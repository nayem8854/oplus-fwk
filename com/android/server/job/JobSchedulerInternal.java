package com.android.server.job;

import android.app.job.JobInfo;
import android.util.proto.ProtoOutputStream;
import java.util.List;

public interface JobSchedulerInternal extends IOplusJobSchedulerInternalEx {
  void addBackingUpUid(int paramInt);
  
  void cancelJobsForUid(int paramInt, String paramString);
  
  void clearAllBackingUpUids();
  
  String getMediaBackupPackage();
  
  JobStorePersistStats getPersistStats();
  
  List<JobInfo> getSystemScheduledPendingJobs();
  
  void removeBackingUpUid(int paramInt);
  
  void reportAppUsage(String paramString, int paramInt);
  
  class JobStorePersistStats {
    public int countAllJobsLoaded = -1;
    
    public int countSystemServerJobsLoaded = -1;
    
    public int countSystemSyncManagerJobsLoaded = -1;
    
    public int countAllJobsSaved = -1;
    
    public int countSystemServerJobsSaved = -1;
    
    public int countSystemSyncManagerJobsSaved = -1;
    
    public JobStorePersistStats(JobSchedulerInternal this$0) {
      this.countAllJobsLoaded = ((JobStorePersistStats)this$0).countAllJobsLoaded;
      this.countSystemServerJobsLoaded = ((JobStorePersistStats)this$0).countSystemServerJobsLoaded;
      this.countSystemSyncManagerJobsLoaded = ((JobStorePersistStats)this$0).countSystemSyncManagerJobsLoaded;
      this.countAllJobsSaved = ((JobStorePersistStats)this$0).countAllJobsSaved;
      this.countSystemServerJobsSaved = ((JobStorePersistStats)this$0).countSystemServerJobsSaved;
      this.countSystemSyncManagerJobsSaved = ((JobStorePersistStats)this$0).countSystemSyncManagerJobsSaved;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("FirstLoad: ");
      stringBuilder.append(this.countAllJobsLoaded);
      stringBuilder.append("/");
      stringBuilder.append(this.countSystemServerJobsLoaded);
      stringBuilder.append("/");
      stringBuilder.append(this.countSystemSyncManagerJobsLoaded);
      stringBuilder.append(" LastSave: ");
      stringBuilder.append(this.countAllJobsSaved);
      stringBuilder.append("/");
      stringBuilder.append(this.countSystemServerJobsSaved);
      stringBuilder.append("/");
      stringBuilder.append(this.countSystemSyncManagerJobsSaved);
      return stringBuilder.toString();
    }
    
    public void dumpDebug(ProtoOutputStream param1ProtoOutputStream, long param1Long) {
      param1Long = param1ProtoOutputStream.start(param1Long);
      long l = param1ProtoOutputStream.start(1146756268033L);
      param1ProtoOutputStream.write(1120986464257L, this.countAllJobsLoaded);
      param1ProtoOutputStream.write(1120986464258L, this.countSystemServerJobsLoaded);
      param1ProtoOutputStream.write(1120986464259L, this.countSystemSyncManagerJobsLoaded);
      param1ProtoOutputStream.end(l);
      l = param1ProtoOutputStream.start(1146756268034L);
      param1ProtoOutputStream.write(1120986464257L, this.countAllJobsSaved);
      param1ProtoOutputStream.write(1120986464258L, this.countSystemServerJobsSaved);
      param1ProtoOutputStream.write(1120986464259L, this.countSystemSyncManagerJobsSaved);
      param1ProtoOutputStream.end(l);
      param1ProtoOutputStream.end(param1Long);
    }
    
    public JobStorePersistStats() {}
  }
}
