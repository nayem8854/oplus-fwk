package com.android.server.job;

public interface IOplusJobSchedulerInternalEx {
  public static final int PENDING_FAIL = 1;
  
  public static final int PENDING_PROCESSING = 2;
  
  public static final int RESTORE_IGNORE = 1;
  
  public static final int RESTORE_SUCCESS = 2;
  
  default int pendingJobs(int paramInt) {
    return 1;
  }
  
  default int restoreJobs(int paramInt) {
    return 1;
  }
  
  void stopStrictModeOnJob();
}
