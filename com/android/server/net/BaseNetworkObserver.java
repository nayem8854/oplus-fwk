package com.android.server.net;

import android.net.INetworkManagementEventObserver;
import android.net.LinkAddress;
import android.net.RouteInfo;

public class BaseNetworkObserver extends INetworkManagementEventObserver.Stub {
  public void interfaceStatusChanged(String paramString, boolean paramBoolean) {}
  
  public void interfaceRemoved(String paramString) {}
  
  public void addressUpdated(String paramString, LinkAddress paramLinkAddress) {}
  
  public void addressRemoved(String paramString, LinkAddress paramLinkAddress) {}
  
  public void interfaceLinkStateChanged(String paramString, boolean paramBoolean) {}
  
  public void interfaceAdded(String paramString) {}
  
  public void interfaceClassDataActivityChanged(String paramString, boolean paramBoolean, long paramLong) {}
  
  public void limitReached(String paramString1, String paramString2) {}
  
  public void interfaceDnsServerInfo(String paramString, long paramLong, String[] paramArrayOfString) {}
  
  public void routeUpdated(RouteInfo paramRouteInfo) {}
  
  public void routeRemoved(RouteInfo paramRouteInfo) {}
}
