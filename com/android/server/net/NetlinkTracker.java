package com.android.server.net;

import android.net.LinkAddress;
import android.net.LinkProperties;
import android.net.RouteInfo;

public class NetlinkTracker extends BaseNetworkObserver {
  private static final boolean DBG = false;
  
  private final String TAG;
  
  private final Callback mCallback;
  
  private DnsServerRepository mDnsServerRepository;
  
  private final String mInterfaceName;
  
  private final LinkProperties mLinkProperties;
  
  public NetlinkTracker(String paramString, Callback paramCallback) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("NetlinkTracker/");
    stringBuilder.append(paramString);
    this.TAG = stringBuilder.toString();
    this.mInterfaceName = paramString;
    this.mCallback = paramCallback;
    LinkProperties linkProperties = new LinkProperties();
    linkProperties.setInterfaceName(this.mInterfaceName);
    this.mDnsServerRepository = new DnsServerRepository();
  }
  
  private void maybeLog(String paramString1, String paramString2, LinkAddress paramLinkAddress) {}
  
  private void maybeLog(String paramString, Object paramObject) {}
  
  public void interfaceRemoved(String paramString) {
    maybeLog("interfaceRemoved", paramString);
    if (this.mInterfaceName.equals(paramString)) {
      clearLinkProperties();
      this.mCallback.update();
    } 
  }
  
  public void addressUpdated(String paramString, LinkAddress paramLinkAddress) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mInterfaceName : Ljava/lang/String;
    //   4: aload_1
    //   5: invokevirtual equals : (Ljava/lang/Object;)Z
    //   8: ifeq -> 53
    //   11: aload_0
    //   12: ldc 'addressUpdated'
    //   14: aload_1
    //   15: aload_2
    //   16: invokespecial maybeLog : (Ljava/lang/String;Ljava/lang/String;Landroid/net/LinkAddress;)V
    //   19: aload_0
    //   20: monitorenter
    //   21: aload_0
    //   22: getfield mLinkProperties : Landroid/net/LinkProperties;
    //   25: aload_2
    //   26: invokevirtual addLinkAddress : (Landroid/net/LinkAddress;)Z
    //   29: istore_3
    //   30: aload_0
    //   31: monitorexit
    //   32: iload_3
    //   33: ifeq -> 53
    //   36: aload_0
    //   37: getfield mCallback : Lcom/android/server/net/NetlinkTracker$Callback;
    //   40: invokeinterface update : ()V
    //   45: goto -> 53
    //   48: astore_1
    //   49: aload_0
    //   50: monitorexit
    //   51: aload_1
    //   52: athrow
    //   53: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #121	-> 0
    //   #122	-> 11
    //   #124	-> 19
    //   #125	-> 21
    //   #126	-> 30
    //   #127	-> 32
    //   #128	-> 36
    //   #126	-> 48
    //   #131	-> 53
    // Exception table:
    //   from	to	target	type
    //   21	30	48	finally
    //   30	32	48	finally
    //   49	51	48	finally
  }
  
  public void addressRemoved(String paramString, LinkAddress paramLinkAddress) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mInterfaceName : Ljava/lang/String;
    //   4: aload_1
    //   5: invokevirtual equals : (Ljava/lang/Object;)Z
    //   8: ifeq -> 53
    //   11: aload_0
    //   12: ldc 'addressRemoved'
    //   14: aload_1
    //   15: aload_2
    //   16: invokespecial maybeLog : (Ljava/lang/String;Ljava/lang/String;Landroid/net/LinkAddress;)V
    //   19: aload_0
    //   20: monitorenter
    //   21: aload_0
    //   22: getfield mLinkProperties : Landroid/net/LinkProperties;
    //   25: aload_2
    //   26: invokevirtual removeLinkAddress : (Landroid/net/LinkAddress;)Z
    //   29: istore_3
    //   30: aload_0
    //   31: monitorexit
    //   32: iload_3
    //   33: ifeq -> 53
    //   36: aload_0
    //   37: getfield mCallback : Lcom/android/server/net/NetlinkTracker$Callback;
    //   40: invokeinterface update : ()V
    //   45: goto -> 53
    //   48: astore_1
    //   49: aload_0
    //   50: monitorexit
    //   51: aload_1
    //   52: athrow
    //   53: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #135	-> 0
    //   #136	-> 11
    //   #138	-> 19
    //   #139	-> 21
    //   #140	-> 30
    //   #141	-> 32
    //   #142	-> 36
    //   #140	-> 48
    //   #145	-> 53
    // Exception table:
    //   from	to	target	type
    //   21	30	48	finally
    //   30	32	48	finally
    //   49	51	48	finally
  }
  
  public void routeUpdated(RouteInfo paramRouteInfo) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mInterfaceName : Ljava/lang/String;
    //   4: aload_1
    //   5: invokevirtual getInterface : ()Ljava/lang/String;
    //   8: invokevirtual equals : (Ljava/lang/Object;)Z
    //   11: ifeq -> 55
    //   14: aload_0
    //   15: ldc 'routeUpdated'
    //   17: aload_1
    //   18: invokespecial maybeLog : (Ljava/lang/String;Ljava/lang/Object;)V
    //   21: aload_0
    //   22: monitorenter
    //   23: aload_0
    //   24: getfield mLinkProperties : Landroid/net/LinkProperties;
    //   27: aload_1
    //   28: invokevirtual addRoute : (Landroid/net/RouteInfo;)Z
    //   31: istore_2
    //   32: aload_0
    //   33: monitorexit
    //   34: iload_2
    //   35: ifeq -> 55
    //   38: aload_0
    //   39: getfield mCallback : Lcom/android/server/net/NetlinkTracker$Callback;
    //   42: invokeinterface update : ()V
    //   47: goto -> 55
    //   50: astore_1
    //   51: aload_0
    //   52: monitorexit
    //   53: aload_1
    //   54: athrow
    //   55: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #149	-> 0
    //   #150	-> 14
    //   #152	-> 21
    //   #153	-> 23
    //   #154	-> 32
    //   #155	-> 34
    //   #156	-> 38
    //   #154	-> 50
    //   #159	-> 55
    // Exception table:
    //   from	to	target	type
    //   23	32	50	finally
    //   32	34	50	finally
    //   51	53	50	finally
  }
  
  public void routeRemoved(RouteInfo paramRouteInfo) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mInterfaceName : Ljava/lang/String;
    //   4: aload_1
    //   5: invokevirtual getInterface : ()Ljava/lang/String;
    //   8: invokevirtual equals : (Ljava/lang/Object;)Z
    //   11: ifeq -> 55
    //   14: aload_0
    //   15: ldc 'routeRemoved'
    //   17: aload_1
    //   18: invokespecial maybeLog : (Ljava/lang/String;Ljava/lang/Object;)V
    //   21: aload_0
    //   22: monitorenter
    //   23: aload_0
    //   24: getfield mLinkProperties : Landroid/net/LinkProperties;
    //   27: aload_1
    //   28: invokevirtual removeRoute : (Landroid/net/RouteInfo;)Z
    //   31: istore_2
    //   32: aload_0
    //   33: monitorexit
    //   34: iload_2
    //   35: ifeq -> 55
    //   38: aload_0
    //   39: getfield mCallback : Lcom/android/server/net/NetlinkTracker$Callback;
    //   42: invokeinterface update : ()V
    //   47: goto -> 55
    //   50: astore_1
    //   51: aload_0
    //   52: monitorexit
    //   53: aload_1
    //   54: athrow
    //   55: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #163	-> 0
    //   #164	-> 14
    //   #166	-> 21
    //   #167	-> 23
    //   #168	-> 32
    //   #169	-> 34
    //   #170	-> 38
    //   #168	-> 50
    //   #173	-> 55
    // Exception table:
    //   from	to	target	type
    //   23	32	50	finally
    //   32	34	50	finally
    //   51	53	50	finally
  }
  
  public void interfaceDnsServerInfo(String paramString, long paramLong, String[] paramArrayOfString) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mInterfaceName : Ljava/lang/String;
    //   4: aload_1
    //   5: invokevirtual equals : (Ljava/lang/Object;)Z
    //   8: ifeq -> 71
    //   11: aload_0
    //   12: ldc 'interfaceDnsServerInfo'
    //   14: aload #4
    //   16: invokestatic toString : ([Ljava/lang/Object;)Ljava/lang/String;
    //   19: invokespecial maybeLog : (Ljava/lang/String;Ljava/lang/Object;)V
    //   22: aload_0
    //   23: getfield mDnsServerRepository : Lcom/android/server/net/DnsServerRepository;
    //   26: lload_2
    //   27: aload #4
    //   29: invokevirtual addServers : (J[Ljava/lang/String;)Z
    //   32: istore #5
    //   34: iload #5
    //   36: ifeq -> 71
    //   39: aload_0
    //   40: monitorenter
    //   41: aload_0
    //   42: getfield mDnsServerRepository : Lcom/android/server/net/DnsServerRepository;
    //   45: aload_0
    //   46: getfield mLinkProperties : Landroid/net/LinkProperties;
    //   49: invokevirtual setDnsServersOn : (Landroid/net/LinkProperties;)V
    //   52: aload_0
    //   53: monitorexit
    //   54: aload_0
    //   55: getfield mCallback : Lcom/android/server/net/NetlinkTracker$Callback;
    //   58: invokeinterface update : ()V
    //   63: goto -> 71
    //   66: astore_1
    //   67: aload_0
    //   68: monitorexit
    //   69: aload_1
    //   70: athrow
    //   71: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #177	-> 0
    //   #178	-> 11
    //   #179	-> 22
    //   #180	-> 34
    //   #181	-> 39
    //   #182	-> 41
    //   #183	-> 52
    //   #184	-> 54
    //   #183	-> 66
    //   #187	-> 71
    // Exception table:
    //   from	to	target	type
    //   41	52	66	finally
    //   52	54	66	finally
    //   67	69	66	finally
  }
  
  public LinkProperties getLinkProperties() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new android/net/LinkProperties
    //   5: dup
    //   6: aload_0
    //   7: getfield mLinkProperties : Landroid/net/LinkProperties;
    //   10: invokespecial <init> : (Landroid/net/LinkProperties;)V
    //   13: astore_1
    //   14: aload_0
    //   15: monitorexit
    //   16: aload_1
    //   17: areturn
    //   18: astore_1
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_1
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #194	-> 2
    //   #194	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	14	18	finally
  }
  
  public void clearLinkProperties() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new com/android/server/net/DnsServerRepository
    //   5: astore_1
    //   6: aload_1
    //   7: invokespecial <init> : ()V
    //   10: aload_0
    //   11: aload_1
    //   12: putfield mDnsServerRepository : Lcom/android/server/net/DnsServerRepository;
    //   15: aload_0
    //   16: getfield mLinkProperties : Landroid/net/LinkProperties;
    //   19: invokevirtual clear : ()V
    //   22: aload_0
    //   23: getfield mLinkProperties : Landroid/net/LinkProperties;
    //   26: aload_0
    //   27: getfield mInterfaceName : Ljava/lang/String;
    //   30: invokevirtual setInterfaceName : (Ljava/lang/String;)V
    //   33: aload_0
    //   34: monitorexit
    //   35: return
    //   36: astore_1
    //   37: aload_0
    //   38: monitorexit
    //   39: aload_1
    //   40: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #202	-> 2
    //   #203	-> 15
    //   #204	-> 22
    //   #205	-> 33
    //   #201	-> 36
    // Exception table:
    //   from	to	target	type
    //   2	15	36	finally
    //   15	22	36	finally
    //   22	33	36	finally
  }
  
  class Callback {
    public abstract void update();
  }
}
