package com.android.server.net;

import android.net.INetdEventCallback;

public class BaseNetdEventCallback extends INetdEventCallback.Stub {
  public void onDnsEvent(int paramInt1, int paramInt2, int paramInt3, String paramString, String[] paramArrayOfString, int paramInt4, long paramLong, int paramInt5) {}
  
  public void onNat64PrefixEvent(int paramInt1, boolean paramBoolean, String paramString, int paramInt2) {}
  
  public void onPrivateDnsValidationEvent(int paramInt, String paramString1, String paramString2, boolean paramBoolean) {}
  
  public void onConnectEvent(String paramString, int paramInt1, long paramLong, int paramInt2) {}
}
