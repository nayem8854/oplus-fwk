package com.android.server.net;

import android.net.LinkProperties;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

class DnsServerRepository {
  private Set<InetAddress> mCurrentServers = new HashSet<>();
  
  private ArrayList<DnsServerEntry> mAllServers = new ArrayList<>(12);
  
  private HashMap<InetAddress, DnsServerEntry> mIndex = new HashMap<>(12);
  
  public static final int NUM_CURRENT_SERVERS = 3;
  
  public static final int NUM_SERVERS = 12;
  
  public static final String TAG = "DnsServerRepository";
  
  public void setDnsServersOn(LinkProperties paramLinkProperties) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_1
    //   3: aload_0
    //   4: getfield mCurrentServers : Ljava/util/Set;
    //   7: invokevirtual setDnsServers : (Ljava/util/Collection;)V
    //   10: aload_0
    //   11: monitorexit
    //   12: return
    //   13: astore_1
    //   14: aload_0
    //   15: monitorexit
    //   16: aload_1
    //   17: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #301	-> 2
    //   #302	-> 10
    //   #300	-> 13
    // Exception table:
    //   from	to	target	type
    //   2	10	13	finally
  }
  
  public boolean addServers(long paramLong, String[] paramArrayOfString) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: invokestatic currentTimeMillis : ()J
    //   5: lstore #4
    //   7: ldc2_w 1000
    //   10: lload_1
    //   11: lmul
    //   12: lload #4
    //   14: ladd
    //   15: lstore_1
    //   16: aload_3
    //   17: arraylength
    //   18: istore #6
    //   20: iconst_0
    //   21: istore #7
    //   23: iload #7
    //   25: iload #6
    //   27: if_icmpge -> 106
    //   30: aload_3
    //   31: iload #7
    //   33: aaload
    //   34: astore #8
    //   36: aload #8
    //   38: invokestatic parseNumericAddress : (Ljava/lang/String;)Ljava/net/InetAddress;
    //   41: astore #9
    //   43: aload_0
    //   44: aload #9
    //   46: lload_1
    //   47: invokespecial updateExistingEntry : (Ljava/net/InetAddress;J)Z
    //   50: ifne -> 100
    //   53: lload_1
    //   54: lload #4
    //   56: lcmp
    //   57: ifle -> 100
    //   60: new com/android/server/net/DnsServerEntry
    //   63: astore #8
    //   65: aload #8
    //   67: aload #9
    //   69: lload_1
    //   70: invokespecial <init> : (Ljava/net/InetAddress;J)V
    //   73: aload_0
    //   74: getfield mAllServers : Ljava/util/ArrayList;
    //   77: aload #8
    //   79: invokevirtual add : (Ljava/lang/Object;)Z
    //   82: pop
    //   83: aload_0
    //   84: getfield mIndex : Ljava/util/HashMap;
    //   87: aload #9
    //   89: aload #8
    //   91: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   94: pop
    //   95: goto -> 100
    //   98: astore #8
    //   100: iinc #7, 1
    //   103: goto -> 23
    //   106: aload_0
    //   107: getfield mAllServers : Ljava/util/ArrayList;
    //   110: invokestatic sort : (Ljava/util/List;)V
    //   113: aload_0
    //   114: invokespecial updateCurrentServers : ()Z
    //   117: istore #10
    //   119: aload_0
    //   120: monitorexit
    //   121: iload #10
    //   123: ireturn
    //   124: astore_3
    //   125: aload_0
    //   126: monitorexit
    //   127: aload_3
    //   128: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #313	-> 2
    //   #314	-> 7
    //   #318	-> 16
    //   #321	-> 36
    //   #324	-> 43
    //   #326	-> 43
    //   #329	-> 53
    //   #330	-> 60
    //   #331	-> 73
    //   #332	-> 83
    //   #322	-> 98
    //   #323	-> 100
    //   #318	-> 100
    //   #338	-> 106
    //   #341	-> 113
    //   #312	-> 124
    // Exception table:
    //   from	to	target	type
    //   2	7	124	finally
    //   16	20	124	finally
    //   36	43	98	java/lang/IllegalArgumentException
    //   36	43	124	finally
    //   43	53	124	finally
    //   60	73	124	finally
    //   73	83	124	finally
    //   83	95	124	finally
    //   106	113	124	finally
    //   113	119	124	finally
  }
  
  private boolean updateExistingEntry(InetAddress paramInetAddress, long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mIndex : Ljava/util/HashMap;
    //   6: aload_1
    //   7: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   10: checkcast com/android/server/net/DnsServerEntry
    //   13: astore_1
    //   14: aload_1
    //   15: ifnull -> 27
    //   18: aload_1
    //   19: lload_2
    //   20: putfield expiry : J
    //   23: aload_0
    //   24: monitorexit
    //   25: iconst_1
    //   26: ireturn
    //   27: aload_0
    //   28: monitorexit
    //   29: iconst_0
    //   30: ireturn
    //   31: astore_1
    //   32: aload_0
    //   33: monitorexit
    //   34: aload_1
    //   35: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #345	-> 2
    //   #346	-> 14
    //   #347	-> 18
    //   #348	-> 23
    //   #350	-> 27
    //   #344	-> 31
    // Exception table:
    //   from	to	target	type
    //   2	14	31	finally
    //   18	23	31	finally
  }
  
  private boolean updateCurrentServers() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: invokestatic currentTimeMillis : ()J
    //   5: lstore_1
    //   6: iconst_0
    //   7: istore_3
    //   8: aload_0
    //   9: getfield mAllServers : Ljava/util/ArrayList;
    //   12: invokevirtual size : ()I
    //   15: iconst_1
    //   16: isub
    //   17: istore #4
    //   19: iload #4
    //   21: iflt -> 101
    //   24: iload #4
    //   26: bipush #12
    //   28: if_icmpge -> 51
    //   31: aload_0
    //   32: getfield mAllServers : Ljava/util/ArrayList;
    //   35: iload #4
    //   37: invokevirtual get : (I)Ljava/lang/Object;
    //   40: checkcast com/android/server/net/DnsServerEntry
    //   43: getfield expiry : J
    //   46: lload_1
    //   47: lcmp
    //   48: ifge -> 101
    //   51: aload_0
    //   52: getfield mAllServers : Ljava/util/ArrayList;
    //   55: iload #4
    //   57: invokevirtual remove : (I)Ljava/lang/Object;
    //   60: checkcast com/android/server/net/DnsServerEntry
    //   63: astore #5
    //   65: aload_0
    //   66: getfield mIndex : Ljava/util/HashMap;
    //   69: aload #5
    //   71: getfield address : Ljava/net/InetAddress;
    //   74: invokevirtual remove : (Ljava/lang/Object;)Ljava/lang/Object;
    //   77: pop
    //   78: iload_3
    //   79: aload_0
    //   80: getfield mCurrentServers : Ljava/util/Set;
    //   83: aload #5
    //   85: getfield address : Ljava/net/InetAddress;
    //   88: invokeinterface remove : (Ljava/lang/Object;)Z
    //   93: ior
    //   94: istore_3
    //   95: iinc #4, -1
    //   98: goto -> 19
    //   101: aload_0
    //   102: getfield mAllServers : Ljava/util/ArrayList;
    //   105: invokevirtual iterator : ()Ljava/util/Iterator;
    //   108: astore #5
    //   110: aload #5
    //   112: invokeinterface hasNext : ()Z
    //   117: ifeq -> 169
    //   120: aload #5
    //   122: invokeinterface next : ()Ljava/lang/Object;
    //   127: checkcast com/android/server/net/DnsServerEntry
    //   130: astore #6
    //   132: aload_0
    //   133: getfield mCurrentServers : Ljava/util/Set;
    //   136: invokeinterface size : ()I
    //   141: iconst_3
    //   142: if_icmpge -> 169
    //   145: aload_0
    //   146: getfield mCurrentServers : Ljava/util/Set;
    //   149: aload #6
    //   151: getfield address : Ljava/net/InetAddress;
    //   154: invokeinterface add : (Ljava/lang/Object;)Z
    //   159: istore #7
    //   161: iload_3
    //   162: iload #7
    //   164: ior
    //   165: istore_3
    //   166: goto -> 110
    //   169: aload_0
    //   170: monitorexit
    //   171: iload_3
    //   172: ireturn
    //   173: astore #5
    //   175: aload_0
    //   176: monitorexit
    //   177: aload #5
    //   179: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #354	-> 2
    //   #355	-> 6
    //   #358	-> 8
    //   #359	-> 24
    //   #360	-> 51
    //   #361	-> 65
    //   #362	-> 78
    //   #358	-> 95
    //   #371	-> 101
    //   #372	-> 132
    //   #373	-> 145
    //   #377	-> 166
    //   #378	-> 169
    //   #353	-> 173
    // Exception table:
    //   from	to	target	type
    //   2	6	173	finally
    //   8	19	173	finally
    //   31	51	173	finally
    //   51	65	173	finally
    //   65	78	173	finally
    //   78	95	173	finally
    //   101	110	173	finally
    //   110	132	173	finally
    //   132	145	173	finally
    //   145	161	173	finally
  }
}
