package com.android.server.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkRequest;
import android.util.Log;

public class NetworkPinner extends ConnectivityManager.NetworkCallback {
  private static final String TAG = NetworkPinner.class.getSimpleName();
  
  private static ConnectivityManager sCM;
  
  private static Callback sCallback;
  
  protected static final Object sLock = new Object();
  
  protected static Network sNetwork;
  
  private static void maybeInitConnectivityManager(Context paramContext) {
    if (sCM == null) {
      ConnectivityManager connectivityManager = (ConnectivityManager)paramContext.getSystemService("connectivity");
      if (connectivityManager == null)
        throw new IllegalStateException("Bad luck, ConnectivityService not started."); 
    } 
  }
  
  private static class Callback extends ConnectivityManager.NetworkCallback {
    private Callback() {}
    
    public void onAvailable(Network param1Network) {
      synchronized (NetworkPinner.sLock) {
        if (this != NetworkPinner.sCallback)
          return; 
        if (NetworkPinner.sCM.getBoundNetworkForProcess() == null && NetworkPinner.sNetwork == null) {
          NetworkPinner.sCM.bindProcessToNetwork(param1Network);
          NetworkPinner.sNetwork = param1Network;
          String str = NetworkPinner.TAG;
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Wifi alternate reality enabled on network ");
          stringBuilder.append(param1Network);
          Log.d(str, stringBuilder.toString());
        } 
        NetworkPinner.sLock.notify();
        return;
      } 
    }
    
    public void onLost(Network param1Network) {
      synchronized (NetworkPinner.sLock) {
        if (this != NetworkPinner.sCallback)
          return; 
        if (param1Network.equals(NetworkPinner.sNetwork) && param1Network.equals(NetworkPinner.sCM.getBoundNetworkForProcess())) {
          NetworkPinner.unpin();
          String str = NetworkPinner.TAG;
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Wifi alternate reality disabled on network ");
          stringBuilder.append(param1Network);
          Log.d(str, stringBuilder.toString());
        } 
        NetworkPinner.sLock.notify();
        return;
      } 
    }
  }
  
  public static void pin(Context paramContext, NetworkRequest paramNetworkRequest) {
    synchronized (sLock) {
      if (sCallback == null) {
        maybeInitConnectivityManager(paramContext);
        Callback callback = new Callback();
        this();
        sCallback = callback;
        try {
          sCM.registerNetworkCallback(paramNetworkRequest, callback);
        } catch (SecurityException securityException) {
          Log.d(TAG, "Failed to register network callback", securityException);
          sCallback = null;
        } 
      } 
      return;
    } 
  }
  
  public static void unpin() {
    synchronized (sLock) {
      Callback callback = sCallback;
      if (callback != null) {
        try {
          sCM.bindProcessToNetwork(null);
          sCM.unregisterNetworkCallback(sCallback);
        } catch (SecurityException securityException) {
          Log.d(TAG, "Failed to unregister network callback", securityException);
        } 
        sCallback = null;
        sNetwork = null;
      } 
      return;
    } 
  }
}
