package com.android.server.deviceidle;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public interface IDeviceIdleConstraint {
  public static final int ACTIVE = 0;
  
  public static final int SENSING_OR_ABOVE = 1;
  
  void startMonitoring();
  
  void stopMonitoring();
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface MinimumState {}
}
