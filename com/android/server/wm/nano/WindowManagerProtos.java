package com.android.server.wm.nano;

import com.android.framework.protobuf.nano.CodedInputByteBufferNano;
import com.android.framework.protobuf.nano.CodedOutputByteBufferNano;
import com.android.framework.protobuf.nano.InternalNano;
import com.android.framework.protobuf.nano.InvalidProtocolBufferNanoException;
import com.android.framework.protobuf.nano.MessageNano;
import com.android.framework.protobuf.nano.WireFormatNano;
import java.io.IOException;

public interface WindowManagerProtos {
  class TaskSnapshotProto extends MessageNano {
    private static volatile TaskSnapshotProto[] _emptyArray;
    
    public long id;
    
    public int insetBottom;
    
    public int insetLeft;
    
    public int insetRight;
    
    public int insetTop;
    
    public boolean isRealSnapshot;
    
    public boolean isTranslucent;
    
    public float legacyScale;
    
    public int orientation;
    
    public int rotation;
    
    public int systemUiVisibility;
    
    public int taskHeight;
    
    public int taskWidth;
    
    public String topActivityComponent;
    
    public int windowingMode;
    
    public static TaskSnapshotProto[] emptyArray() {
      if (_emptyArray == null)
        synchronized (InternalNano.LAZY_INIT_LOCK) {
          if (_emptyArray == null)
            _emptyArray = new TaskSnapshotProto[0]; 
        }  
      return _emptyArray;
    }
    
    public TaskSnapshotProto() {
      clear();
    }
    
    public TaskSnapshotProto clear() {
      this.orientation = 0;
      this.insetLeft = 0;
      this.insetTop = 0;
      this.insetRight = 0;
      this.insetBottom = 0;
      this.isRealSnapshot = false;
      this.windowingMode = 0;
      this.systemUiVisibility = 0;
      this.isTranslucent = false;
      this.topActivityComponent = "";
      this.legacyScale = 0.0F;
      this.id = 0L;
      this.rotation = 0;
      this.taskWidth = 0;
      this.taskHeight = 0;
      this.cachedSize = -1;
      return this;
    }
    
    public void writeTo(CodedOutputByteBufferNano param1CodedOutputByteBufferNano) throws IOException {
      int i = this.orientation;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(1, i); 
      i = this.insetLeft;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(2, i); 
      i = this.insetTop;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(3, i); 
      i = this.insetRight;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(4, i); 
      i = this.insetBottom;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(5, i); 
      boolean bool = this.isRealSnapshot;
      if (bool)
        param1CodedOutputByteBufferNano.writeBool(6, bool); 
      i = this.windowingMode;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(7, i); 
      i = this.systemUiVisibility;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(8, i); 
      bool = this.isTranslucent;
      if (bool)
        param1CodedOutputByteBufferNano.writeBool(9, bool); 
      if (!this.topActivityComponent.equals(""))
        param1CodedOutputByteBufferNano.writeString(10, this.topActivityComponent); 
      i = Float.floatToIntBits(this.legacyScale);
      if (i != Float.floatToIntBits(0.0F))
        param1CodedOutputByteBufferNano.writeFloat(11, this.legacyScale); 
      long l = this.id;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(12, l); 
      i = this.rotation;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(13, i); 
      i = this.taskWidth;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(14, i); 
      i = this.taskHeight;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(15, i); 
      super.writeTo(param1CodedOutputByteBufferNano);
    }
    
    protected int computeSerializedSize() {
      int i = super.computeSerializedSize();
      int j = this.orientation, k = i;
      if (j != 0)
        k = i + CodedOutputByteBufferNano.computeInt32Size(1, j); 
      j = this.insetLeft;
      i = k;
      if (j != 0)
        i = k + CodedOutputByteBufferNano.computeInt32Size(2, j); 
      j = this.insetTop;
      k = i;
      if (j != 0)
        k = i + CodedOutputByteBufferNano.computeInt32Size(3, j); 
      j = this.insetRight;
      i = k;
      if (j != 0)
        i = k + CodedOutputByteBufferNano.computeInt32Size(4, j); 
      j = this.insetBottom;
      k = i;
      if (j != 0)
        k = i + CodedOutputByteBufferNano.computeInt32Size(5, j); 
      boolean bool = this.isRealSnapshot;
      i = k;
      if (bool)
        i = k + CodedOutputByteBufferNano.computeBoolSize(6, bool); 
      j = this.windowingMode;
      k = i;
      if (j != 0)
        k = i + CodedOutputByteBufferNano.computeInt32Size(7, j); 
      j = this.systemUiVisibility;
      i = k;
      if (j != 0)
        i = k + CodedOutputByteBufferNano.computeInt32Size(8, j); 
      bool = this.isTranslucent;
      k = i;
      if (bool)
        k = i + CodedOutputByteBufferNano.computeBoolSize(9, bool); 
      i = k;
      if (!this.topActivityComponent.equals("")) {
        String str = this.topActivityComponent;
        i = k + CodedOutputByteBufferNano.computeStringSize(10, str);
      } 
      j = Float.floatToIntBits(this.legacyScale);
      k = i;
      if (j != Float.floatToIntBits(0.0F)) {
        float f = this.legacyScale;
        k = i + CodedOutputByteBufferNano.computeFloatSize(11, f);
      } 
      long l = this.id;
      j = k;
      if (l != 0L)
        j = k + CodedOutputByteBufferNano.computeInt64Size(12, l); 
      k = this.rotation;
      i = j;
      if (k != 0)
        i = j + CodedOutputByteBufferNano.computeInt32Size(13, k); 
      j = this.taskWidth;
      k = i;
      if (j != 0)
        k = i + CodedOutputByteBufferNano.computeInt32Size(14, j); 
      j = this.taskHeight;
      i = k;
      if (j != 0)
        i = k + CodedOutputByteBufferNano.computeInt32Size(15, j); 
      return i;
    }
    
    public TaskSnapshotProto mergeFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      while (true) {
        int i = param1CodedInputByteBufferNano.readTag();
        switch (i) {
          default:
            if (!WireFormatNano.parseUnknownField(param1CodedInputByteBufferNano, i))
              return this; 
            continue;
          case 120:
            this.taskHeight = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 112:
            this.taskWidth = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 104:
            this.rotation = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 96:
            this.id = param1CodedInputByteBufferNano.readInt64();
            continue;
          case 93:
            this.legacyScale = param1CodedInputByteBufferNano.readFloat();
            continue;
          case 82:
            this.topActivityComponent = param1CodedInputByteBufferNano.readString();
            continue;
          case 72:
            this.isTranslucent = param1CodedInputByteBufferNano.readBool();
            continue;
          case 64:
            this.systemUiVisibility = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 56:
            this.windowingMode = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 48:
            this.isRealSnapshot = param1CodedInputByteBufferNano.readBool();
            continue;
          case 40:
            this.insetBottom = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 32:
            this.insetRight = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 24:
            this.insetTop = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 16:
            this.insetLeft = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 8:
            this.orientation = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 0:
            break;
        } 
        break;
      } 
      return this;
    }
    
    public static TaskSnapshotProto parseFrom(byte[] param1ArrayOfbyte) throws InvalidProtocolBufferNanoException {
      return (TaskSnapshotProto)MessageNano.mergeFrom(new TaskSnapshotProto(), param1ArrayOfbyte);
    }
    
    public static TaskSnapshotProto parseFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      return (new TaskSnapshotProto()).mergeFrom(param1CodedInputByteBufferNano);
    }
  }
}
