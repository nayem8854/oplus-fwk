package com.android.server.wm;

public final class DisplayContentProto {
  public static final long ABOVE_APP_WINDOWS = 2246267895814L;
  
  public static final long APP_TRANSITION = 1146756268048L;
  
  public static final long BELOW_APP_WINDOWS = 2246267895815L;
  
  public static final long CHANGING_APPS = 2246267895827L;
  
  public static final long CLOSING_APPS = 2246267895826L;
  
  public static final long DISPLAY_FRAMES = 1146756268045L;
  
  public static final long DISPLAY_INFO = 1146756268042L;
  
  public static final long DISPLAY_READY = 1133871366170L;
  
  public static final long DOCKED_STACK_DIVIDER_CONTROLLER = 1146756268036L;
  
  public static final long DPI = 1120986464265L;
  
  public static final long FOCUSED_APP = 1138166333455L;
  
  public static final long FOCUSED_ROOT_TASK_ID = 1120986464279L;
  
  public static final long ID = 1120986464258L;
  
  public static final long IME_WINDOWS = 2246267895816L;
  
  public static final long OPENING_APPS = 2246267895825L;
  
  public static final long OVERLAY_WINDOWS = 2246267895828L;
  
  public static final long PINNED_STACK_CONTROLLER = 1146756268037L;
  
  public static final long RESUMED_ACTIVITY = 1146756268056L;
  
  public static final long ROOT_DISPLAY_AREA = 1146756268053L;
  
  public static final long ROTATION = 1120986464267L;
  
  public static final long SCREEN_ROTATION_ANIMATION = 1146756268044L;
  
  public static final long SINGLE_TASK_INSTANCE = 1133871366166L;
  
  public static final long SURFACE_SIZE = 1120986464270L;
  
  public static final long TASKS = 2246267895833L;
  
  public static final long WINDOW_CONTAINER = 1146756268033L;
}
