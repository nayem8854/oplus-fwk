package com.android.server;

import android.content.Context;
import android.os.DropBoxManager;
import android.os.FileUtils;
import android.os.Handler;
import android.os.OplusManager;
import android.os.Process;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.util.Slog;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import oplus.util.OplusStatistics;

public class OplusBootReceiver {
  public static final int LOG_SIZE = 4194304;
  
  static final String MINI_DUMP = "/data/system/dropbox/minidump.bin";
  
  private static final String OCP = "/proc/oppoVersion/ocp";
  
  private static final String PMIC_OCP_STATUS = "/sys/pmic_info/ocp_status";
  
  private static final String PMIC_POFF_REASON = "/sys/pmic_info/poff_reason";
  
  private static final String PMIC_PON_REASON = "/sys/pmic_info/pon_reason";
  
  private static final String TAG = "OplusBootReceiver";
  
  private static final String UNKNOW_REBOOT_PFF = "/sys/power/poff_reason";
  
  private static final String UNKNOW_REBOOT_PON = "/sys/power/pon_reason";
  
  public void init(Context paramContext) {
    this.mContext = paramContext;
  }
  
  public void incrementCriticalDataAndRecordRebootBlocked() {
    Slog.v("OplusBootReceiver", "send delayed message");
    this.mGetStateHandler.sendEmptyMessageDelayed(1001, 5000L);
    int i = OplusManager.TYPE_REBOOT;
    Context context = this.mContext;
    String str = context.getResources().getString(201589197);
    if (OplusManager.incrementCriticalData(i, str) == -2)
      Slog.e("OplusBootReceiver", "increment reboot times failed!!"); 
    str = isRebootExceptionFromBolckException();
    if (str != null) {
      i = OplusManager.TYPE_REBOOT_FROM_BLOCKED;
      Context context1 = this.mContext;
      String str1 = context1.getResources().getString(201589198);
      OplusManager.writeLogToPartition(i, str, "ANDROID", "reboot_from_blocked", str1);
      OplusManager.sendQualityDCSEvent(OplusManager.QualityEventId.EV_STABILITY_REBOOT_FROM_BLOCKED, null);
    } 
  }
  
  public void syncCacheToEmmc() {
    try {
      OplusManager.syncCacheToEmmc();
      Slog.v("OplusBootReceiver", "syncCacheToEmmc");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("sync criticallog failed e + ");
      stringBuilder.append(exception.toString());
      Slog.e("OplusBootReceiver", stringBuilder.toString());
    } 
  }
  
  public void disableBlackMonitor() {
    Thread thread = new Thread() {
        final OplusBootReceiver this$0;
        
        public void run() {
          while (true) {
            try {
              File file = new File();
              this("/proc/blackCheckStatus");
              String str = FileUtils.readTextFile(file, 10, null);
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("/proc/blackCheckStatus shows ");
              stringBuilder.append(str);
              Slog.i("OplusBootReceiver", stringBuilder.toString());
              if (!"0".equals(str)) {
                HashMap<Object, Object> hashMap = new HashMap<>();
                this();
                hashMap.put("count", str);
                OplusStatistics.onCommon(OplusBootReceiver.this.mContext, "20120", "black_screen_monitor", (Map)hashMap, false);
                continue;
              } 
              SystemClock.sleep(60000L);
            } catch (Exception exception) {
              SystemClock.sleep(60000L);
            } 
          } 
        }
      };
    thread.start();
    thread = new Thread() {
        final OplusBootReceiver this$0;
        
        public void run() {
          while (true) {
            try {
              File file = new File();
              this("/proc/brightCheckStatus");
              String str = FileUtils.readTextFile(file, 10, null);
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("/proc/brightCheckStatus  shows ");
              stringBuilder.append(str);
              Slog.i("OplusBootReceiver", stringBuilder.toString());
              if (!"0".equals(str)) {
                HashMap<Object, Object> hashMap = new HashMap<>();
                this();
                hashMap.put("count", str);
                OplusStatistics.onCommon(OplusBootReceiver.this.mContext, "20120", "bright_screen_monitor", (Map)hashMap, false);
                continue;
              } 
              SystemClock.sleep(60000L);
            } catch (Exception exception) {
              SystemClock.sleep(60000L);
            } 
          } 
        }
      };
    thread.start();
  }
  
  public String getOTAVersionString() {
    return SystemProperties.get("ro.build.version.ota", "");
  }
  
  public void printIsPanic() {
    boolean bool = isKernelPanic();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Aha! Boot reason is kernel panic ");
    stringBuilder.append(bool);
    stringBuilder.append("!!!");
    Slog.d("OplusBootReceiver", stringBuilder.toString());
  }
  
  public String setLastKmsgFooter() {
    String str;
    if (isKernelPanic()) {
      StringBuilder stringBuilder = new StringBuilder(512);
      stringBuilder.append("\n");
      stringBuilder.append("Boot info:\n");
      stringBuilder.append("Last boot reason: ");
      stringBuilder.append("panic");
      stringBuilder.append("\n");
      str = stringBuilder.toString();
    } else {
      StringBuilder stringBuilder = new StringBuilder(512);
      stringBuilder.append("\n");
      stringBuilder.append("Boot info:\n");
      stringBuilder.append("Last boot reason: ");
      stringBuilder.append("normal");
      stringBuilder.append("\n");
      str = stringBuilder.toString();
    } 
    return str;
  }
  
  public void writeLogToPartitionAndDeleteFolderFilesThread(DropBoxManager paramDropBoxManager, HashMap<String, Long> paramHashMap, String paramString) throws IOException {
    boolean bool = isKernelPanic();
    if (bool) {
      int j = OplusManager.TYPE_PANIC;
      Context context = this.mContext;
      String str = context.getResources().getString(201589190);
      OplusManager.writeLogToPartition(j, "kernel_panic", "KERNEL", "panic", str);
      HashMap<Object, Object> hashMap = new HashMap<>();
      hashMap.put("reason", "kernel panic");
      OplusManager.onStamp(OplusManager.StampId.AD_KE, hashMap);
      OplusManager.sendQualityDCSEvent(OplusManager.QualityEventId.EV_STABILITY_KERNEL_PANIC, null);
    } else {
      String str = isRebootExceptionFromBolckException();
      boolean bool1 = SystemProperties.getBoolean("ro.build.release_type", false);
      if (str != null && bool1 && 
        paramDropBoxManager != null) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Kernel reboot from Block Exception!  lastReboot = ");
        stringBuilder.append(str);
        paramDropBoxManager.addText("SYSTEM_LAST_KMSG", stringBuilder.toString());
      } 
    } 
    SystemProperties.set("persist.sys.systemserver.pid", Integer.toString(Process.myPid()));
    paramString = SystemProperties.get("persist.sys.oppo.reboot", "");
    String str2 = SystemProperties.get("persist.sys.oppo.fatal", "");
    String str1 = SystemProperties.get("oppo.device.firstboot", "");
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("persist.sys.oppo.reboot = ");
    stringBuilder2.append(paramString);
    Slog.v("OplusBootReceiver", stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("persist.sys.oppo.fatal = ");
    stringBuilder2.append(str2);
    Slog.v("OplusBootReceiver", stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("oppo.device.firstboot = ");
    stringBuilder2.append(str1);
    Slog.v("OplusBootReceiver", stringBuilder2.toString());
    int i = OplusBootAeeLogUtil.checkMtkHwtState(this.mContext);
    if (!isKernelPanic() && i == -1 && 
      "1".equals(str2) && 
      !"1".equals(str1))
      if (!"normal".equals(paramString) && 
        paramDropBoxManager != null)
        if (SystemProperties.get("persist.sys.oppo.fb_upgraded", "").equals("1")) {
          i = OplusManager.TYPE_ANDROID_UNKNOWN_REBOOT;
          Context context = this.mContext;
          String str = context.getResources().getString(201589207);
          OplusManager.writeLogToPartition(i, "unknow reboot", "KERNEL", "panic", str);
          OplusManager.sendQualityDCSEvent(OplusManager.QualityEventId.EV_STABILITY_UNKNOWN_REBOOT, null);
        }   
    try {
      Thread thread = new Thread();
      Runnable runnable = new Runnable() {
          final OplusBootReceiver this$0;
          
          final String val$fatalValue;
          
          final String val$firstValue;
          
          final boolean val$isPanic;
          
          final String val$rebootValue;
          
          public void run() {
            if (OplusBootReceiver.isMtkPlatform()) {
              OplusBootReceiver.this.readUnknowRebootStatusforMTK();
              OplusBootAeeLogUtil.prepareMtkLog(false, null);
            } else {
              OplusBootReceiver.this.readUnknowRebootStatus(isPanic, rebootValue, fatalValue, firstValue);
              OplusBootReceiver.this.readPmicStatus();
            } 
            OplusBootReceiver.this.checkPwkSt();
            OplusBootReceiver.this.updateDeviceInfo();
          }
        };
      try {
        super(this, bool, paramString, str2, str1);
        this(runnable);
        thread.start();
      } catch (Exception null) {}
    } catch (Exception exception) {}
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("read pmic  e: ");
    stringBuilder1.append(exception.toString());
    Slog.e("OplusBootReceiver", stringBuilder1.toString());
  }
  
  public void addFile(DropBoxManager paramDropBoxManager, HashMap<String, Long> paramHashMap, String paramString, Context paramContext) throws IOException {
    StringBuilder stringBuilder1;
    String str1 = isLastSystemServerRebootFormBolckException();
    int i = Process.myPid();
    int j = SystemProperties.getInt("persist.sys.systemserver.pid", -1);
    if (i == j) {
      Slog.e("OplusBootReceiver", "system_server_current_pid == system_server_previous_pid");
      return;
    } 
    if (isMtkPlatform()) {
      if (paramDropBoxManager != null)
        paramDropBoxManager.addText("SYSTEM_SERVER", paramString); 
      try {
        Thread thread = new Thread();
        Runnable runnable = new Runnable() {
            final OplusBootReceiver this$0;
            
            final String val$headers;
            
            final int val$system_server_current_pid;
            
            public void run() {
              OplusBootAeeLogUtil.prepareMtkLog(true, headers);
              SystemProperties.set("persist.sys.systemserver.pid", Integer.toString(system_server_current_pid));
            }
          };
        try {
          super(this, paramString, i);
          this(runnable);
          thread.start();
        } catch (Exception null) {}
      } catch (Exception exception) {}
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("addFile prepareMtkLog  e: ");
      stringBuilder1.append(exception.toString());
      Slog.e("OplusBootReceiver", stringBuilder1.toString());
    } 
    String str2 = SystemProperties.get("persist.sys.panic.file", "");
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("system_server crashed! --- systemcrashFile =");
    stringBuilder2.append(str2);
    Slog.e("OplusBootReceiver", stringBuilder2.toString());
    File file = new File(str2);
    if (file.exists()) {
      SystemProperties.set("persist.sys.send.file", str2);
      if (file.getName().startsWith("system_server_watchdog", 0)) {
        requestAddFileToDropBox((DropBoxManager)exception, (HashMap<String, Long>)stringBuilder1, paramString, str2, 4194304, "SYSTEM_SERVER_WATCHDOG");
      } else if (file.getName().startsWith("tombstone", 0)) {
        requestAddFileToDropBox((DropBoxManager)exception, (HashMap<String, Long>)stringBuilder1, paramString, str2, 4194304, "SYSTEM_TOMBSTONE_CRASH");
      } else if (str2.endsWith(".gz")) {
        if (exception != null)
          exception.addText("SYSTEM_SERVER_GZ", "LOG FOR GZ"); 
      } else {
        requestAddFileToDropBox((DropBoxManager)exception, (HashMap<String, Long>)stringBuilder1, paramString, str2, 4194304, "SYSTEM_SERVER");
      } 
    } else {
      dumpEnvironment();
      if (str1 != null) {
        if (exception != null) {
          stringBuilder1 = new StringBuilder();
          stringBuilder1.append(paramString);
          stringBuilder1.append("system_Server reboot from Block Exception! system_server_current_pid = ");
          stringBuilder1.append(i);
          stringBuilder1.append(",system_server_previous_pid = ");
          stringBuilder1.append(j);
          stringBuilder1.append(" lastSystemReboot = ");
          stringBuilder1.append(str1);
          exception.addText("SYSTEM_SERVER", stringBuilder1.toString());
        } 
      } else if (exception != null) {
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append(paramString);
        stringBuilder1.append("system_Server crash but can not get efficacious log! system_server_current_pid = ");
        stringBuilder1.append(i);
        stringBuilder1.append(",system_server_previous_pid = ");
        stringBuilder1.append(j);
        exception.addText("SYSTEM_SERVER", stringBuilder1.toString());
      } 
    } 
    SystemProperties.set("persist.sys.systemserver.pid", Integer.toString(i));
    if (str1 != null) {
      i = OplusManager.TYPE_ANDROID_SYSTEM_REBOOT_FROM_BLOCKED;
      String str = paramContext.getResources().getString(201589205);
      OplusManager.writeLogToPartition(i, str1, "ANDROID", "reboot_from_blocked", str);
      OplusManager.sendQualityDCSEvent(OplusManager.QualityEventId.EV_STABILITY_ANDROID_REBOOT_FROM_BLOCKED, null);
    } 
    HashMap<Object, Object> hashMap = new HashMap<>();
    hashMap.put("reason", "system server crash");
    OplusManager.onStamp(OplusManager.StampId.AD_JE, hashMap);
  }
  
  public void resetPanicFile() {
    SystemProperties.set("persist.sys.panic.file", "");
  }
  
  private static boolean isKernelPanic() {
    if (isMtkPlatform()) {
      String str1 = SystemProperties.get("ro.boot.bootreason", null);
      if ("kernel_panic".equals(str1) || "modem".equals(str1))
        return true; 
      return false;
    } 
    String str = SystemProperties.get("sys.oppo.panic", "false");
    if ("true".equals(str))
      return true; 
    return false;
  }
  
  static void resetPanicState() {
    SystemProperties.set("sys.oppo.panic", "false");
    SystemProperties.set("persist.sys.oppo.fb_upgraded", "1");
  }
  
  private static boolean isMtkPlatform() {
    return SystemProperties.get("ro.board.platform", "oppo").toLowerCase().startsWith("mt");
  }
  
  private void dumpEnvironment() {
    SystemProperties.set("sys.dumpenvironment.finished", "0");
    SystemProperties.set("ctl.start", "dumpenvironment");
    long l = SystemClock.elapsedRealtime();
    while (SystemProperties.getInt("sys.dumpenvironment.finished", 0) != 1 && SystemClock.elapsedRealtime() - l < SystemProperties.getInt("ro.dumpenvironment.time", 4000))
      SystemClock.sleep(100L); 
  }
  
  public final int MSG_UPDATESTATE = 1001;
  
  private Context mContext;
  
  Handler mGetStateHandler = (Handler)new Object(this);
  
  private final String mLastExceptionProc = "/proc/sys/kernel/hung_task_kill";
  
  private final String mLastExceptionProperty = "persist.hungtask.oppo.kill";
  
  private String isLastSystemServerRebootFormBolckException() {
    if (!(new File("/proc/sys/kernel/hung_task_kill")).exists()) {
      Slog.v("OplusBootReceiver", "reboot file is not exists");
      return null;
    } 
    try {
      BufferedReader bufferedReader = new BufferedReader();
      FileReader fileReader = new FileReader();
      this("/proc/sys/kernel/hung_task_kill");
      this(fileReader);
      String str = bufferedReader.readLine();
      bufferedReader.close();
      if (str != null) {
        boolean bool = str.trim().isEmpty();
        if (!bool)
          return str; 
      } 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
    return null;
  }
  
  private String isRebootExceptionFromBolckException() {
    String str = SystemProperties.get("persist.hungtask.oppo.kill");
    if (str != null && !str.isEmpty())
      return str; 
    return null;
  }
  
  private String readUnknowRebootStatus(boolean paramBoolean, String paramString1, String paramString2, String paramString3) {
    HashMap<Object, Object> hashMap;
    String str = "";
    try {
      FileInputStream fileInputStream1 = new FileInputStream();
      this("/sys/power/pon_reason");
      FileInputStream fileInputStream2 = new FileInputStream();
      str = "";
      try {
        this("/sys/power/poff_reason");
        FileInputStream fileInputStream = new FileInputStream();
        this("/proc/oppoVersion/ocp");
        int i = fileInputStream1.available();
        int j = fileInputStream1.available();
        try {
          HashMap<Object, Object> hashMap2;
          byte[] arrayOfByte1 = new byte[i];
          byte[] arrayOfByte2 = new byte[j];
          byte[] arrayOfByte3 = new byte[30];
          fileInputStream1.read(arrayOfByte1);
          fileInputStream2.read(arrayOfByte2);
          fileInputStream.read(arrayOfByte3);
          int k;
          for (k = arrayOfByte1.length, j = 0, i = 0; i < k; ) {
            byte b = arrayOfByte1[i];
            if (b != 0) {
              j++;
              i++;
            } 
          } 
          String str1 = str;
          try {
            int m;
            for (m = arrayOfByte2.length, i = 0, k = 0; k < m; ) {
              byte b = arrayOfByte2[k];
              if (b != 0) {
                i++;
                k++;
              } 
            } 
            int n;
            for (str1 = str, n = arrayOfByte3.length, m = 0, k = 0; k < n; ) {
              byte b = arrayOfByte3[k];
              if (b != 0) {
                m++;
                k++;
              } 
            } 
            str1 = str;
            StringBuilder stringBuilder1 = new StringBuilder();
            str1 = str;
            this();
            str1 = str;
            stringBuilder1.append("ponHigh = ");
            str1 = str;
            stringBuilder1.append(j);
            str1 = str;
            stringBuilder1.append(" pffHigh=");
            str1 = str;
            stringBuilder1.append(i);
            str1 = str;
            stringBuilder1.append(" ocpHigh = ");
            str1 = str;
            stringBuilder1.append(m);
            str1 = str;
            Slog.v("OplusBootReceiver", stringBuilder1.toString());
            str1 = str;
            StringBuffer stringBuffer = new StringBuffer();
            str1 = str;
            this();
            str1 = str;
            StringBuilder stringBuilder2 = new StringBuilder();
            str1 = str;
            this();
            str1 = str;
            String str2 = new String();
            str1 = str;
            this(arrayOfByte1, 0, j);
            str1 = str;
            stringBuilder2.append(str2.trim());
            str1 = str;
            stringBuilder2.append(" ");
            str1 = str;
            str2 = new String();
            str1 = str;
            this(arrayOfByte2, 0, i);
            str1 = str;
            stringBuilder2.append(str2.trim());
            str1 = str;
            stringBuffer.append(stringBuilder2.toString());
            str1 = str;
            str = stringBuffer.toString();
            try {
              fileInputStream1.close();
              fileInputStream2.close();
              fileInputStream.close();
              str1 = new String();
              this(arrayOfByte2, 0, i);
              String str4 = str1.trim();
              str1 = new String();
              this(arrayOfByte1, 0, j);
              String str5 = str1.trim();
              str1 = new String();
              this(arrayOfByte3, 0, m);
              String str6 = str1.trim();
              String str3 = str4.substring(str4.indexOf('[') + 1, str4.indexOf(']'));
              String str7 = str5.substring(str5.indexOf('[') + 1, str5.indexOf(']'));
              StringBuilder stringBuilder3 = new StringBuilder();
              this();
              stringBuilder3.append("poffCode = ");
              stringBuilder3.append(str3);
              stringBuilder3.append(" ponCode ");
              stringBuilder3.append(str7);
              stringBuilder3.append(" ocp = ");
              stringBuilder3.append(str6);
              Slog.v("OplusBootReceiver", stringBuilder3.toString());
              boolean bool = "ocp: 0 0 0 0".equals(str6);
              if (!bool)
                try {
                  if (!"ocp: 0 0x0 0 0x0".equals(str6)) {
                    hashMap2 = new HashMap<>();
                    this();
                    hashMap2.put("reason", str6);
                    OplusManager.onStamp(OplusManager.StampId.AD_OCP, hashMap2);
                  } 
                } catch (Exception exception2) {
                  paramString1 = str;
                }  
              HashMap<Object, Object> hashMap3 = new HashMap<>();
              this();
              hashMap3.put("logType", "21");
              hashMap3.put("module", "Android");
              hashMap3.put("poff", str4);
              hashMap3.put("pon", str5);
              hashMap3.put("poffCode", str3);
              hashMap3.put("ponCode", str7);
              String str8 = str;
              hashMap3.put("otaVersion", SystemProperties.get("ro.build.version.ota", ""));
              str8 = str;
              hashMap3.put("issue", "reboot");
              str8 = str;
              hashMap3.put("count", "1");
              str8 = str;
              hashMap3.put("ocp", str6);
              str8 = str;
              hashMap3.put("isPanic", Boolean.toString(paramBoolean));
              try {
                hashMap3.put("reboot", paramString1);
                try {
                  hashMap3.put("fatal", exception2);
                  try {
                    hashMap3.put("firstBoot", paramString3);
                    try {
                      Context context = this.mContext;
                      try {
                        OplusStatistics.onCommon(context, "CriticalLog", "reboot", (Map)hashMap3, false);
                        hashMap2 = new HashMap<>();
                        this();
                        hashMap2.put("poff", str4);
                        hashMap2.put("pon", str5);
                        hashMap2.put("poffCode", str3);
                        hashMap2.put("ponCode", str7);
                        hashMap2.put("ocp", str6);
                        hashMap2.put("isPanic", Boolean.toString(paramBoolean));
                        try {
                          hashMap2.put("reboot", paramString1);
                          try {
                            hashMap2.put("fatal", exception2);
                            try {
                              hashMap2.put("firstBoot", paramString3);
                              OplusManager.sendQualityDCSEvent(OplusManager.QualityEventId.EV_STABILITY_REBOOT, hashMap2);
                              paramString1 = str;
                            } catch (Exception null) {}
                          } catch (Exception null) {}
                        } catch (Exception null) {}
                      } catch (Exception null) {}
                    } catch (Exception null) {}
                  } catch (Exception null) {}
                } catch (Exception null) {}
              } catch (Exception null) {}
            } catch (Exception exception2) {
              paramString1 = str;
            } 
          } catch (Exception null) {
            hashMap = hashMap2;
          } 
        } catch (Exception null) {}
      } catch (Exception exception1) {}
      exception = exception1;
      HashMap<Object, Object> hashMap1 = hashMap;
    } catch (Exception exception) {
      HashMap<Object, Object> hashMap1 = hashMap;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("readUnknowRebootStatus:");
    stringBuilder.append(exception.toString());
    Slog.v("OplusBootReceiver", stringBuilder.toString());
    exception.printStackTrace();
  }
  
  private String readPmicStatus() {
    // Byte code:
    //   0: new java/io/FileReader
    //   3: astore_1
    //   4: aload_1
    //   5: ldc '/sys/pmic_info/pon_reason'
    //   7: invokespecial <init> : (Ljava/lang/String;)V
    //   10: new java/io/FileReader
    //   13: astore_2
    //   14: aload_2
    //   15: ldc '/sys/pmic_info/poff_reason'
    //   17: invokespecial <init> : (Ljava/lang/String;)V
    //   20: new java/io/FileReader
    //   23: astore_3
    //   24: aload_3
    //   25: ldc '/sys/pmic_info/ocp_status'
    //   27: invokespecial <init> : (Ljava/lang/String;)V
    //   30: new java/io/BufferedReader
    //   33: astore #4
    //   35: aload #4
    //   37: aload_1
    //   38: invokespecial <init> : (Ljava/io/Reader;)V
    //   41: new java/io/BufferedReader
    //   44: astore #5
    //   46: aload #5
    //   48: aload_2
    //   49: invokespecial <init> : (Ljava/io/Reader;)V
    //   52: new java/io/BufferedReader
    //   55: astore #6
    //   57: aload #6
    //   59: aload_3
    //   60: invokespecial <init> : (Ljava/io/Reader;)V
    //   63: ldc_w '0x80'
    //   66: astore #7
    //   68: new java/util/HashMap
    //   71: astore #8
    //   73: aload #8
    //   75: invokespecial <init> : ()V
    //   78: aload #5
    //   80: invokevirtual readLine : ()Ljava/lang/String;
    //   83: astore #9
    //   85: aload #9
    //   87: ifnull -> 412
    //   90: aload #9
    //   92: ldc_w '\|'
    //   95: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
    //   98: astore #10
    //   100: new java/lang/StringBuilder
    //   103: astore #9
    //   105: aload #9
    //   107: invokespecial <init> : ()V
    //   110: aload #9
    //   112: ldc_w 'PMIC'
    //   115: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   118: pop
    //   119: aload #9
    //   121: aload #10
    //   123: iconst_1
    //   124: aaload
    //   125: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   128: pop
    //   129: aload #9
    //   131: invokevirtual toString : ()Ljava/lang/String;
    //   134: astore #11
    //   136: aload #10
    //   138: iconst_2
    //   139: aaload
    //   140: astore #9
    //   142: aload #10
    //   144: iconst_3
    //   145: aaload
    //   146: astore #12
    //   148: aload #10
    //   150: iconst_4
    //   151: aaload
    //   152: astore #10
    //   154: new java/lang/StringBuilder
    //   157: astore #13
    //   159: aload #13
    //   161: invokespecial <init> : ()V
    //   164: aload #13
    //   166: aload #11
    //   168: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   171: pop
    //   172: aload #13
    //   174: ldc_w '_L1_poffcode'
    //   177: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   180: pop
    //   181: aload #8
    //   183: aload #13
    //   185: invokevirtual toString : ()Ljava/lang/String;
    //   188: aload #9
    //   190: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   195: pop
    //   196: new java/lang/StringBuilder
    //   199: astore #13
    //   201: aload #13
    //   203: invokespecial <init> : ()V
    //   206: aload #13
    //   208: aload #11
    //   210: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   213: pop
    //   214: aload #13
    //   216: ldc_w '_L2_poffcode'
    //   219: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   222: pop
    //   223: aload #8
    //   225: aload #13
    //   227: invokevirtual toString : ()Ljava/lang/String;
    //   230: aload #12
    //   232: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   237: pop
    //   238: new java/lang/StringBuilder
    //   241: astore #13
    //   243: aload #13
    //   245: invokespecial <init> : ()V
    //   248: aload #13
    //   250: aload #11
    //   252: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   255: pop
    //   256: aload #13
    //   258: ldc_w '_poff'
    //   261: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   264: pop
    //   265: aload #8
    //   267: aload #13
    //   269: invokevirtual toString : ()Ljava/lang/String;
    //   272: aload #10
    //   274: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   279: pop
    //   280: ldc_w 'PMIC0'
    //   283: aload #11
    //   285: invokevirtual equals : (Ljava/lang/Object;)Z
    //   288: ifeq -> 295
    //   291: aload #9
    //   293: astore #7
    //   295: new java/lang/StringBuilder
    //   298: astore #13
    //   300: aload #13
    //   302: invokespecial <init> : ()V
    //   305: aload #13
    //   307: aload #11
    //   309: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   312: pop
    //   313: aload #13
    //   315: ldc_w '_L1_poffcode='
    //   318: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   321: pop
    //   322: aload #13
    //   324: aload #9
    //   326: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   329: pop
    //   330: aload #13
    //   332: ldc_w ' '
    //   335: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   338: pop
    //   339: aload #13
    //   341: aload #11
    //   343: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   346: pop
    //   347: aload #13
    //   349: ldc_w '_L2_poffcode='
    //   352: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   355: pop
    //   356: aload #13
    //   358: aload #12
    //   360: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   363: pop
    //   364: aload #13
    //   366: ldc_w ' '
    //   369: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   372: pop
    //   373: aload #13
    //   375: aload #11
    //   377: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   380: pop
    //   381: aload #13
    //   383: ldc_w '_poff='
    //   386: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   389: pop
    //   390: aload #13
    //   392: aload #10
    //   394: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   397: pop
    //   398: ldc 'OplusBootReceiver'
    //   400: aload #13
    //   402: invokevirtual toString : ()Ljava/lang/String;
    //   405: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   408: pop
    //   409: goto -> 78
    //   412: aload #6
    //   414: astore #5
    //   416: aload #5
    //   418: invokevirtual readLine : ()Ljava/lang/String;
    //   421: astore #6
    //   423: aload #6
    //   425: ifnull -> 571
    //   428: aload #6
    //   430: ldc_w '\|'
    //   433: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
    //   436: astore #9
    //   438: new java/lang/StringBuilder
    //   441: astore #6
    //   443: aload #6
    //   445: invokespecial <init> : ()V
    //   448: aload #6
    //   450: ldc_w 'PMIC'
    //   453: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   456: pop
    //   457: aload #6
    //   459: aload #9
    //   461: iconst_1
    //   462: aaload
    //   463: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   466: pop
    //   467: aload #6
    //   469: invokevirtual toString : ()Ljava/lang/String;
    //   472: astore #6
    //   474: aload #9
    //   476: iconst_2
    //   477: aaload
    //   478: astore #9
    //   480: new java/lang/StringBuilder
    //   483: astore #11
    //   485: aload #11
    //   487: invokespecial <init> : ()V
    //   490: aload #11
    //   492: aload #6
    //   494: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   497: pop
    //   498: aload #11
    //   500: ldc_w '_ocp'
    //   503: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   506: pop
    //   507: aload #8
    //   509: aload #11
    //   511: invokevirtual toString : ()Ljava/lang/String;
    //   514: aload #9
    //   516: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   521: pop
    //   522: new java/lang/StringBuilder
    //   525: astore #11
    //   527: aload #11
    //   529: invokespecial <init> : ()V
    //   532: aload #11
    //   534: aload #6
    //   536: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   539: pop
    //   540: aload #11
    //   542: ldc_w '_ocp='
    //   545: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   548: pop
    //   549: aload #11
    //   551: aload #9
    //   553: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   556: pop
    //   557: ldc 'OplusBootReceiver'
    //   559: aload #11
    //   561: invokevirtual toString : ()Ljava/lang/String;
    //   564: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   567: pop
    //   568: goto -> 416
    //   571: aload #4
    //   573: astore #5
    //   575: aload #5
    //   577: invokevirtual readLine : ()Ljava/lang/String;
    //   580: astore #4
    //   582: aload #4
    //   584: ifnull -> 812
    //   587: aload #4
    //   589: ldc_w '\|'
    //   592: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
    //   595: astore #9
    //   597: new java/lang/StringBuilder
    //   600: astore #4
    //   602: aload #4
    //   604: invokespecial <init> : ()V
    //   607: aload #4
    //   609: ldc_w 'PMIC'
    //   612: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   615: pop
    //   616: aload #4
    //   618: aload #9
    //   620: iconst_1
    //   621: aaload
    //   622: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   625: pop
    //   626: aload #4
    //   628: invokevirtual toString : ()Ljava/lang/String;
    //   631: astore #4
    //   633: aload #9
    //   635: iconst_2
    //   636: aaload
    //   637: astore #6
    //   639: aload #9
    //   641: iconst_3
    //   642: aaload
    //   643: astore #9
    //   645: new java/lang/StringBuilder
    //   648: astore #11
    //   650: aload #11
    //   652: invokespecial <init> : ()V
    //   655: aload #11
    //   657: aload #4
    //   659: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   662: pop
    //   663: aload #11
    //   665: ldc_w '_poncode'
    //   668: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   671: pop
    //   672: aload #8
    //   674: aload #11
    //   676: invokevirtual toString : ()Ljava/lang/String;
    //   679: aload #6
    //   681: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   686: pop
    //   687: new java/lang/StringBuilder
    //   690: astore #11
    //   692: aload #11
    //   694: invokespecial <init> : ()V
    //   697: aload #11
    //   699: aload #4
    //   701: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   704: pop
    //   705: aload #11
    //   707: ldc_w '_pon'
    //   710: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   713: pop
    //   714: aload #8
    //   716: aload #11
    //   718: invokevirtual toString : ()Ljava/lang/String;
    //   721: aload #9
    //   723: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   728: pop
    //   729: new java/lang/StringBuilder
    //   732: astore #11
    //   734: aload #11
    //   736: invokespecial <init> : ()V
    //   739: aload #11
    //   741: aload #4
    //   743: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   746: pop
    //   747: aload #11
    //   749: ldc_w '_poncode='
    //   752: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   755: pop
    //   756: aload #11
    //   758: aload #6
    //   760: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   763: pop
    //   764: aload #11
    //   766: ldc_w ' '
    //   769: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   772: pop
    //   773: aload #11
    //   775: aload #4
    //   777: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   780: pop
    //   781: aload #11
    //   783: ldc_w '_pon='
    //   786: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   789: pop
    //   790: aload #11
    //   792: aload #9
    //   794: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   797: pop
    //   798: ldc 'OplusBootReceiver'
    //   800: aload #11
    //   802: invokevirtual toString : ()Ljava/lang/String;
    //   805: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   808: pop
    //   809: goto -> 575
    //   812: aload_1
    //   813: invokevirtual close : ()V
    //   816: aload_2
    //   817: invokevirtual close : ()V
    //   820: aload_3
    //   821: invokevirtual close : ()V
    //   824: aload_0
    //   825: getfield mContext : Landroid/content/Context;
    //   828: ldc_w 'BootStats'
    //   831: ldc_w 'PmicMonitor'
    //   834: aload #8
    //   836: iconst_0
    //   837: invokestatic onCommon : (Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Z)V
    //   840: ldc 'persist.sys.oppo.longpwkts'
    //   842: invokestatic get : (Ljava/lang/String;)Ljava/lang/String;
    //   845: astore #5
    //   847: ldc_w '0x80'
    //   850: aload #7
    //   852: invokevirtual equals : (Ljava/lang/Object;)Z
    //   855: ifne -> 917
    //   858: aload #5
    //   860: ifnull -> 917
    //   863: aload #5
    //   865: invokevirtual isEmpty : ()Z
    //   868: ifeq -> 917
    //   871: getstatic android/os/OplusManager$StampId.AD_PMICREASON : Ljava/lang/String;
    //   874: aload #8
    //   876: invokestatic onStamp : (Ljava/lang/String;Ljava/util/Map;)V
    //   879: new java/lang/StringBuilder
    //   882: astore #5
    //   884: aload #5
    //   886: invokespecial <init> : ()V
    //   889: aload #5
    //   891: ldc_w 'Save pmic poff/pon reason to stamp,PMIC0_L1_Poffcode is '
    //   894: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   897: pop
    //   898: aload #5
    //   900: aload #7
    //   902: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   905: pop
    //   906: ldc 'OplusBootReceiver'
    //   908: aload #5
    //   910: invokevirtual toString : ()Ljava/lang/String;
    //   913: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   916: pop
    //   917: goto -> 972
    //   920: astore #7
    //   922: goto -> 927
    //   925: astore #7
    //   927: new java/lang/StringBuilder
    //   930: dup
    //   931: invokespecial <init> : ()V
    //   934: astore #5
    //   936: aload #5
    //   938: ldc_w 'readPmicStatus:'
    //   941: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   944: pop
    //   945: aload #5
    //   947: aload #7
    //   949: invokevirtual toString : ()Ljava/lang/String;
    //   952: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   955: pop
    //   956: ldc 'OplusBootReceiver'
    //   958: aload #5
    //   960: invokevirtual toString : ()Ljava/lang/String;
    //   963: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   966: pop
    //   967: aload #7
    //   969: invokevirtual printStackTrace : ()V
    //   972: ldc ''
    //   974: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #625	-> 0
    //   #627	-> 0
    //   #628	-> 10
    //   #629	-> 20
    //   #630	-> 30
    //   #631	-> 41
    //   #632	-> 52
    //   #633	-> 63
    //   #636	-> 68
    //   #637	-> 78
    //   #639	-> 90
    //   #640	-> 100
    //   #641	-> 136
    //   #642	-> 142
    //   #643	-> 148
    //   #645	-> 154
    //   #646	-> 196
    //   #647	-> 238
    //   #649	-> 280
    //   #650	-> 291
    //   #652	-> 295
    //   #653	-> 409
    //   #637	-> 412
    //   #655	-> 416
    //   #657	-> 428
    //   #658	-> 438
    //   #659	-> 474
    //   #661	-> 480
    //   #662	-> 522
    //   #663	-> 568
    //   #655	-> 571
    //   #665	-> 575
    //   #667	-> 587
    //   #668	-> 597
    //   #669	-> 633
    //   #670	-> 639
    //   #672	-> 645
    //   #673	-> 687
    //   #675	-> 729
    //   #676	-> 809
    //   #678	-> 812
    //   #679	-> 816
    //   #680	-> 820
    //   #681	-> 824
    //   #683	-> 840
    //   #684	-> 847
    //   #685	-> 871
    //   #686	-> 879
    //   #692	-> 917
    //   #689	-> 920
    //   #690	-> 927
    //   #691	-> 967
    //   #693	-> 972
    // Exception table:
    //   from	to	target	type
    //   0	10	925	java/lang/Exception
    //   10	20	925	java/lang/Exception
    //   20	30	925	java/lang/Exception
    //   30	41	925	java/lang/Exception
    //   41	52	925	java/lang/Exception
    //   52	63	925	java/lang/Exception
    //   68	78	925	java/lang/Exception
    //   78	85	925	java/lang/Exception
    //   90	100	925	java/lang/Exception
    //   100	136	925	java/lang/Exception
    //   154	196	925	java/lang/Exception
    //   196	238	925	java/lang/Exception
    //   238	280	925	java/lang/Exception
    //   280	291	925	java/lang/Exception
    //   295	409	925	java/lang/Exception
    //   416	423	925	java/lang/Exception
    //   428	438	925	java/lang/Exception
    //   438	474	925	java/lang/Exception
    //   480	522	925	java/lang/Exception
    //   522	568	925	java/lang/Exception
    //   575	582	925	java/lang/Exception
    //   587	597	925	java/lang/Exception
    //   597	633	925	java/lang/Exception
    //   645	687	925	java/lang/Exception
    //   687	729	925	java/lang/Exception
    //   729	809	925	java/lang/Exception
    //   812	816	925	java/lang/Exception
    //   816	820	925	java/lang/Exception
    //   820	824	925	java/lang/Exception
    //   824	840	920	java/lang/Exception
    //   840	847	920	java/lang/Exception
    //   847	858	920	java/lang/Exception
    //   863	871	920	java/lang/Exception
    //   871	879	920	java/lang/Exception
    //   879	917	920	java/lang/Exception
  }
  
  private String readUnknowRebootStatusforMTK() {
    try {
      HashMap<Object, Object> hashMap = new HashMap<>();
      this();
      hashMap.put("logType", "21");
      hashMap.put("module", "Android");
      hashMap.put("otaVersion", SystemProperties.get("ro.build.version.ota", ""));
      hashMap.put("issue", "reboot");
      hashMap.put("count", "1");
      OplusStatistics.onCommon(this.mContext, "CriticalLog", "reboot", (Map)hashMap, false);
      OplusManager.sendQualityDCSEvent(OplusManager.QualityEventId.EV_STABILITY_REBOOT, null);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("readUnknowRebootStatusforMTK:");
      stringBuilder.append(exception.toString());
      Slog.v("OplusBootReceiver", stringBuilder.toString());
      exception.printStackTrace();
    } 
    return "";
  }
  
  private void deleteFolderFilesThread(String paramString) {
    try {
      Thread thread = new Thread();
      Runnable runnable = new Runnable() {
          final OplusBootReceiver this$0;
          
          final String val$path;
          
          public void run() {
            File file = new File(path);
            if (file.exists() && file.isDirectory()) {
              File[] arrayOfFile = file.listFiles();
              if (arrayOfFile != null) {
                int i;
                byte b;
                for (i = arrayOfFile.length, b = 0; b < i; ) {
                  file = arrayOfFile[b];
                  if (file.exists() && file.isFile())
                    file.delete(); 
                  b++;
                } 
              } 
            } else {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("");
              stringBuilder.append(path);
              stringBuilder.append(" not exists");
              Slog.v("OplusBootReceiver", stringBuilder.toString());
            } 
          }
        };
      super(this, paramString);
      this(runnable);
      thread.start();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("deleteFolderFilesThread e: ");
      stringBuilder.append(exception.toString());
      Slog.e("OplusBootReceiver", stringBuilder.toString());
    } 
  }
  
  private void checkPwkSt() {
    String str = SystemProperties.get("persist.sys.oppo.longpwkts");
    if (str != null && !str.isEmpty()) {
      HashMap<Object, Object> hashMap = new HashMap<>();
      hashMap.put("pwkts", str);
      OplusManager.onStamp(OplusManager.StampId.AD_BATTERYOFF, hashMap);
      SystemProperties.set("persist.sys.oppo.longpwkts", "");
    } 
  }
  
  private void updateDeviceInfo() {
    OplusManager.onDeleteStampId(OplusManager.StampId.AD_DEVICE);
    HashMap<Object, Object> hashMap = new HashMap<>();
    String str1 = SystemProperties.get("ro.product.model", "null");
    String str2 = getOppoID();
    String str3 = SystemProperties.get("ro.build.version.ota", "null");
    String str4 = SystemProperties.get("ro.product.androidver", "null");
    String str5 = "no";
    if (!"enforcing".equals(SystemProperties.get("ro.boot.veritymode", "")))
      str5 = "yes"; 
    hashMap.put("modle", str1);
    hashMap.put("oppoID", str2);
    hashMap.put("otaVersion", str3);
    hashMap.put("androidVer", str4);
    hashMap.put("memInfo", "memInfo");
    hashMap.put("root", str5);
    OplusManager.onStamp(OplusManager.StampId.AD_DEVICE, hashMap);
  }
  
  private String getOppoID() {
    try {
      Class<?> clazz = Class.forName("com.android.id.impl.IdProviderImpl");
      Object object = clazz.newInstance();
      Method method = clazz.getMethod("getGUID", new Class[] { Context.class });
      if (object != null && method != null) {
        object = method.invoke(object, new Object[] { this.mContext });
        if (object != null)
          return (String)object; 
      } 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
    return "";
  }
  
  private OplusBootReceiverCallback mOplusBootReceiverCallback = null;
  
  public void setOplusBootReceiverCallback(OplusBootReceiverCallback paramOplusBootReceiverCallback) {
    this.mOplusBootReceiverCallback = paramOplusBootReceiverCallback;
  }
  
  private void requestAddFileToDropBox(DropBoxManager paramDropBoxManager, HashMap<String, Long> paramHashMap, String paramString1, String paramString2, int paramInt, String paramString3) {
    OplusBootReceiverCallback oplusBootReceiverCallback = this.mOplusBootReceiverCallback;
    if (oplusBootReceiverCallback != null) {
      try {
        oplusBootReceiverCallback.onAddFileToDropBox(paramDropBoxManager, paramHashMap, paramString1, paramString2, paramInt, paramString3);
      } catch (Exception exception) {
        Slog.e("OplusBootReceiver", "requestAddFileToDropBox failed!", exception);
      } 
    } else {
      Slog.e("OplusBootReceiver", "requestAddFileToDropBox failed for callback uninit!");
    } 
  }
  
  public static interface OplusBootReceiverCallback {
    void onAddFileToDropBox(DropBoxManager param1DropBoxManager, HashMap<String, Long> param1HashMap, String param1String1, String param1String2, int param1Int, String param1String3) throws IOException;
  }
}
