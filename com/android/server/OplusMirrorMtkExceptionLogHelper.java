package com.android.server;

import com.oplus.reflect.RefClass;
import com.oplus.reflect.RefMethod;

public class OplusMirrorMtkExceptionLogHelper {
  public static Class<?> TYPE = RefClass.load(OplusMirrorMtkExceptionLogHelper.class, "com.android.server.MtkExceptionLogHelper");
  
  public static RefMethod<Void> generateSystemCrashLog;
}
