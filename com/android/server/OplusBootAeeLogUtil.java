package com.android.server;

import android.content.Context;
import android.os.OplusUsageManager;
import android.os.Process;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.util.Slog;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.UUID;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipOutputStream;

public class OplusBootAeeLogUtil {
  private static final String TAG = "OppoBootReceiver_OppoBootAeeLogUtil";
  
  private static final String mLastExceptionProc = "/proc/sys/kernel/hung_task_kill";
  
  private static final String mLastExceptionProperty = "persist.hungtask.oppo.kill";
  
  private static String isLastSystemServerRebootFormBolckException() {
    if (!(new File("/proc/sys/kernel/hung_task_kill")).exists()) {
      Slog.v("OppoBootReceiver_OppoBootAeeLogUtil", "reboot file is not exists");
      return null;
    } 
    try {
      BufferedReader bufferedReader = new BufferedReader();
      FileReader fileReader = new FileReader();
      this("/proc/sys/kernel/hung_task_kill");
      this(fileReader);
      String str = bufferedReader.readLine();
      bufferedReader.close();
      if (str != null) {
        boolean bool = str.trim().isEmpty();
        if (!bool)
          return str; 
      } 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
    return null;
  }
  
  private static boolean isMtkPlatform() {
    return SystemProperties.get("ro.board.platform", "oppo").toLowerCase().startsWith("mt");
  }
  
  public static int checkMtkHwtState(Context paramContext) {
    // Byte code:
    //   0: iconst_m1
    //   1: istore_1
    //   2: invokestatic isMtkPlatform : ()Z
    //   5: ifne -> 10
    //   8: iconst_m1
    //   9: ireturn
    //   10: ldc 'vendor.debug.mtk.aeev.db'
    //   12: aconst_null
    //   13: invokestatic get : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   16: astore_2
    //   17: new java/lang/StringBuilder
    //   20: dup
    //   21: invokespecial <init> : ()V
    //   24: astore_3
    //   25: aload_3
    //   26: ldc 'aee db path is '
    //   28: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   31: pop
    //   32: aload_3
    //   33: aload_2
    //   34: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   37: pop
    //   38: ldc 'OppoBootReceiver_OppoBootAeeLogUtil'
    //   40: aload_3
    //   41: invokevirtual toString : ()Ljava/lang/String;
    //   44: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   47: pop
    //   48: ldc ''
    //   50: astore_3
    //   51: iload_1
    //   52: istore #4
    //   54: aload_2
    //   55: ifnull -> 228
    //   58: aload_2
    //   59: ldc 'HWT'
    //   61: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   64: ifne -> 88
    //   67: aload_2
    //   68: ldc 'HW_Reboot'
    //   70: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   73: ifne -> 88
    //   76: iload_1
    //   77: istore #4
    //   79: aload_2
    //   80: ldc 'HANG'
    //   82: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   85: ifeq -> 228
    //   88: aload_2
    //   89: ldc 'HWT'
    //   91: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   94: ifeq -> 107
    //   97: getstatic android/os/OplusManager.TYPE_ANDROID_REBOOT_HWT : I
    //   100: istore_1
    //   101: ldc 'HWT'
    //   103: astore_3
    //   104: goto -> 142
    //   107: aload_2
    //   108: ldc 'HW_Reboot'
    //   110: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   113: ifeq -> 126
    //   116: getstatic android/os/OplusManager.TYPE_ANDROID_REBOOT_HARDWARE_REBOOT : I
    //   119: istore_1
    //   120: ldc 'Hardware Reboot'
    //   122: astore_3
    //   123: goto -> 142
    //   126: aload_2
    //   127: ldc 'HANG'
    //   129: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   132: ifeq -> 142
    //   135: getstatic android/os/OplusManager.TYPE_ANDROID_REBOOT_HANG : I
    //   138: istore_1
    //   139: ldc 'HANG'
    //   141: astore_3
    //   142: new java/lang/StringBuilder
    //   145: dup
    //   146: invokespecial <init> : ()V
    //   149: astore_2
    //   150: aload_2
    //   151: ldc 'aee db type is '
    //   153: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   156: pop
    //   157: aload_2
    //   158: iload_1
    //   159: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   162: pop
    //   163: aload_2
    //   164: ldc ', issue is '
    //   166: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   169: pop
    //   170: aload_2
    //   171: aload_3
    //   172: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   175: pop
    //   176: ldc 'OppoBootReceiver_OppoBootAeeLogUtil'
    //   178: aload_2
    //   179: invokevirtual toString : ()Ljava/lang/String;
    //   182: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   185: pop
    //   186: iload_1
    //   187: istore #4
    //   189: iload_1
    //   190: iconst_m1
    //   191: if_icmpeq -> 228
    //   194: iload_1
    //   195: istore #4
    //   197: aload_3
    //   198: invokevirtual isEmpty : ()Z
    //   201: ifne -> 228
    //   204: aload_0
    //   205: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   208: ldc 201589190
    //   210: invokevirtual getString : (I)Ljava/lang/String;
    //   213: astore_0
    //   214: iload_1
    //   215: ldc 'HWT_HardwareReboot_HANG'
    //   217: ldc 'ANDROID'
    //   219: aload_3
    //   220: aload_0
    //   221: invokestatic writeLogToPartition : (ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    //   224: pop
    //   225: iload_1
    //   226: istore #4
    //   228: iload #4
    //   230: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #83	-> 0
    //   #84	-> 2
    //   #86	-> 8
    //   #88	-> 10
    //   #89	-> 17
    //   #91	-> 48
    //   #92	-> 51
    //   #93	-> 88
    //   #94	-> 97
    //   #95	-> 101
    //   #96	-> 107
    //   #97	-> 116
    //   #98	-> 120
    //   #99	-> 126
    //   #100	-> 135
    //   #101	-> 139
    //   #104	-> 142
    //   #105	-> 186
    //   #106	-> 204
    //   #110	-> 204
    //   #106	-> 214
    //   #113	-> 228
  }
  
  public static void prepareMtkLog(boolean paramBoolean, String paramString) {
    StringBuilder stringBuilder;
    if (!isMtkPlatform())
      return; 
    String str1 = UUID.randomUUID().toString().replace("-", "").substring(0, 15);
    String str2 = parseAeeLogPath(paramBoolean);
    String str3 = parseAeeTag(paramBoolean, str2);
    if (paramBoolean) {
      int i = Process.myPid();
      int j = SystemProperties.getInt("persist.sys.systemserver.pid", -1);
      if (i == j) {
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("may not crash, system_server_current_pid == system_server_previous_pid = ");
        stringBuilder2.append(i);
        Slog.e("OppoBootReceiver_OppoBootAeeLogUtil", stringBuilder2.toString());
      } else {
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("android restart maybe crash or killed, system_server_current_pid = ");
        stringBuilder2.append(i);
        stringBuilder2.append(" system_server_previous_pid = ");
        stringBuilder2.append(j);
        Slog.d("OppoBootReceiver_OppoBootAeeLogUtil", stringBuilder2.toString());
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(str2);
      stringBuilder1.append("/ZZ_INTERNAL");
      if (str2 != null && (new File(stringBuilder1.toString())).exists()) {
        SystemProperties.set("sys.mtk.last.aee.db", str2);
        packageAeeLogs(str3, str2, str1);
      } else {
        str3 = isLastSystemServerRebootFormBolckException();
        if (str3 != null) {
          stringBuilder = new StringBuilder();
          stringBuilder.append(paramString);
          stringBuilder.append("system_Server reboot from Block Exception! system_server_current_pid = ");
          stringBuilder.append(i);
          stringBuilder.append(", system_server_previous_pid = ");
          stringBuilder.append(j);
          stringBuilder.append(", lastSystemReboot = ");
          stringBuilder.append(str3);
          paramString = stringBuilder.toString();
        } else {
          StringBuilder stringBuilder2 = new StringBuilder();
          stringBuilder2.append(paramString);
          stringBuilder2.append("system_Server crash but can not get efficacious log! system_server_current_pid = ");
          stringBuilder2.append(i);
          stringBuilder2.append(", system_server_previous_pid = ");
          stringBuilder2.append(j);
          paramString = stringBuilder2.toString();
        } 
        generateSystemCrashLog(paramString);
        waitForStringPropertyReady("vendor.debug.mtk.aee.status", "free", "free", 60);
        waitForStringPropertyReady("vendor.debug.mtk.aee.status64", "free", "free", 60);
        str3 = parseAeeLogPath(paramBoolean);
        paramString = parseAeeTag(paramBoolean, str3);
        if (str3 != null && paramString != null) {
          packageAeeLogs(paramString, str3, str1);
        } else {
          Slog.e("OppoBootReceiver_OppoBootAeeLogUtil", "prepareMtkLog failed for aeePath or aeeType illegal!");
        } 
      } 
    } else if (stringBuilder != null && str3 != null) {
      packageAeeLogs(str3, (String)stringBuilder, str1);
    } else {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("prepareMtkLog is not unnormal reboot. aeePath is ");
      stringBuilder1.append((String)stringBuilder);
      stringBuilder1.append(" aeeType is ");
      stringBuilder1.append(str3);
      stringBuilder1.append(" isAndroidReboot = ");
      stringBuilder1.append(paramBoolean);
      Slog.e("OppoBootReceiver_OppoBootAeeLogUtil", stringBuilder1.toString());
    } 
  }
  
  private static void packageAeeLogs(String paramString1, String paramString2, String paramString3) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("/data/oppo/log/DCS/de/AEE_DB/");
    stringBuilder.append(paramString1);
    stringBuilder.append("@");
    stringBuilder.append(paramString3);
    stringBuilder.append("@");
    stringBuilder.append(SystemProperties.get("ro.build.version.ota"));
    stringBuilder.append("@");
    stringBuilder.append(System.currentTimeMillis());
    stringBuilder.append(".dat.gz");
    paramString3 = stringBuilder.toString();
    stringBuilder = new StringBuilder();
    stringBuilder.append("prepare zip! aeeType is ");
    stringBuilder.append(paramString1);
    stringBuilder.append(" aeePath is ");
    stringBuilder.append(paramString2);
    Slog.v("OppoBootReceiver_OppoBootAeeLogUtil", stringBuilder.toString());
    try {
      zipFolder(paramString2, "/data/oppo/log/DCS/de/AEE_DB/aee.zip");
      gzipFile("/data/oppo/log/DCS/de/AEE_DB/aee.zip", paramString3);
      File file = new File();
      this("/data/oppo/log/DCS/de/AEE_DB/aee.zip");
      file.delete();
      file = new File();
      this(paramString3);
      if (file.exists()) {
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append("package end, delete file ");
        stringBuilder1.append(paramString2);
        Slog.v("OppoBootReceiver_OppoBootAeeLogUtil", stringBuilder1.toString());
        File file1 = new File();
        this(paramString2);
        deleteDir(file1.getAbsoluteFile());
      } 
      SystemProperties.set("sys.backup.minidump.tag", paramString1);
      SystemProperties.set("ctl.start", "backup_minidumplog");
    } catch (Exception exception) {
      Slog.e("OppoBootReceiver_OppoBootAeeLogUtil", "dumpEnvironmentGzFile failed!");
      exception.printStackTrace();
    } 
  }
  
  private static void waitForIntPropertyReady(String paramString, int paramInt1, int paramInt2, int paramInt3) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("waitForPropertyReady!int ");
    stringBuilder.append(paramString);
    Slog.d("OppoBootReceiver_OppoBootAeeLogUtil", stringBuilder.toString());
    SystemClock.sleep(2000L);
    for (byte b = 0; b < paramInt3 * 2 && 
      SystemProperties.getInt(paramString, paramInt1) != paramInt2; b++)
      SystemClock.sleep(500L); 
    SystemClock.sleep(1000L);
    stringBuilder = new StringBuilder();
    stringBuilder.append("waitForPropertyReady end!int ");
    stringBuilder.append(paramString);
    Slog.d("OppoBootReceiver_OppoBootAeeLogUtil", stringBuilder.toString());
  }
  
  private static void waitForStringPropertyReady(String paramString1, String paramString2, String paramString3, int paramInt) {
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("waitForPropertyReady!String ");
    stringBuilder2.append(paramString1);
    Slog.d("OppoBootReceiver_OppoBootAeeLogUtil", stringBuilder2.toString());
    SystemClock.sleep(2000L);
    for (paramInt = 0; paramInt < 40; paramInt++) {
      SystemClock.sleep(500L);
      if (SystemProperties.get(paramString1, paramString2).equals(paramString3))
        break; 
    } 
    SystemClock.sleep(1000L);
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("waitForPropertyReady end!String ");
    stringBuilder1.append(paramString1);
    Slog.d("OppoBootReceiver_OppoBootAeeLogUtil", stringBuilder1.toString());
  }
  
  private static String parseAeeTag(boolean paramBoolean, String paramString) {
    if (paramString == null)
      return null; 
    int i = paramString.lastIndexOf(".");
    if (i == -1)
      return null; 
    paramString = paramString.substring(i + 1);
    if (paramString.equals("NE"))
      return "AEE_SYSTEM_TOMBSTONE_CRASH"; 
    if (paramString.equals("JE"))
      return "AEE_SYSTEM_SERVER"; 
    if (paramString.equals("SWT"))
      return "AEE_SYSTEM_SERVER_WATCHDOG"; 
    if (paramString.equals("KE") || paramString.equals("HWT") || paramString.equals("Hardware Reboot") || paramString.equals("HANG"))
      return "AEE_SYSTEM_LAST_KMSG"; 
    if (paramBoolean)
      return "AEE_SYSTEM_SERVER"; 
    return "AEE_SYSTEM_LAST_KMSG";
  }
  
  private static String parseAeeLogPath(boolean paramBoolean) {
    if (paramBoolean) {
      str1 = "vendor.debug.mtk.aee.db";
    } else {
      str1 = "vendor.debug.mtk.aeev.db";
    } 
    String str1 = SystemProperties.get(str1);
    if (str1 == null || str1.equals("")) {
      Slog.i("OppoBootReceiver_OppoBootAeeLogUtil", " parserAeeLogPath aeeDBProp is null");
      return null;
    } 
    if (str1.indexOf(":") == -1) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("parserAeeLogPath aeeDBProp ");
      stringBuilder.append(str1);
      stringBuilder.append(" is not null but inavailable");
      Slog.w("OppoBootReceiver_OppoBootAeeLogUtil", stringBuilder.toString());
      return null;
    } 
    String str2 = str1.substring(str1.indexOf(":") + 1);
    str1 = str2;
    if (!paramBoolean) {
      moveVendorAeeToData(str2);
      str1 = str2.replaceFirst("/data/vendor/aee_exp", "/data/oppo/log/aee_exp");
    } 
    return str1;
  }
  
  private static boolean moveVendorAeeToData(String paramString) {
    OplusUsageManager oplusUsageManager = OplusUsageManager.getOplusUsageManager();
    if (oplusUsageManager == null) {
      Slog.e("OppoBootReceiver_OppoBootAeeLogUtil", "moveVendorAeeToData can not find usageManager");
      return false;
    } 
    String str = paramString.replaceFirst("/data/vendor/aee_exp", "/data/oppo/log/aee_exp");
    return oplusUsageManager.readEntireOplusDir(paramString, str, true);
  }
  
  private static void zipFolder(String paramString1, String paramString2) {
    ZipOutputStream zipOutputStream;
    File file = new File(paramString1);
    File[] arrayOfFile = file.listFiles();
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("Zip directory: ");
    stringBuilder1.append(paramString1);
    stringBuilder1.append(" to ");
    stringBuilder1.append(paramString2);
    Slog.d("OppoBootReceiver_OppoBootAeeLogUtil", stringBuilder1.toString());
    StringBuilder stringBuilder2 = null;
    String str = null;
    paramString1 = str;
    stringBuilder1 = stringBuilder2;
    try {
      ZipOutputStream zipOutputStream3 = new ZipOutputStream();
      paramString1 = str;
      stringBuilder1 = stringBuilder2;
      FileOutputStream fileOutputStream = new FileOutputStream();
      paramString1 = str;
      stringBuilder1 = stringBuilder2;
      this(paramString2);
      paramString1 = str;
      stringBuilder1 = stringBuilder2;
      this(fileOutputStream);
      ZipOutputStream zipOutputStream2 = zipOutputStream3;
      ZipOutputStream zipOutputStream1 = zipOutputStream2;
      zipOutputStream = zipOutputStream2;
      byte[] arrayOfByte = new byte[1024];
      int i;
      byte b;
      for (zipOutputStream1 = zipOutputStream2, zipOutputStream = zipOutputStream2, i = arrayOfFile.length, b = 0; b < i; ) {
        File file1 = arrayOfFile[b];
        if (file1 != null) {
          zipOutputStream1 = zipOutputStream2;
          zipOutputStream = zipOutputStream2;
          boolean bool = file1.canRead();
          if (bool) {
            zipOutputStream1 = zipOutputStream2;
            zipOutputStream = zipOutputStream2;
            try {
              BufferedInputStream bufferedInputStream = new BufferedInputStream();
              zipOutputStream1 = zipOutputStream2;
              zipOutputStream = zipOutputStream2;
              FileInputStream fileInputStream = new FileInputStream();
              zipOutputStream1 = zipOutputStream2;
              zipOutputStream = zipOutputStream2;
              this(file1);
              zipOutputStream1 = zipOutputStream2;
            } catch (Exception exception) {
              zipOutputStream1 = zipOutputStream2;
              zipOutputStream = zipOutputStream2;
              exception.printStackTrace();
            } 
          } 
        } 
        b++;
      } 
      try {
        zipOutputStream2.close();
      } catch (IOException null) {}
      return;
    } catch (IOException iOException1) {
      ZipOutputStream zipOutputStream1 = zipOutputStream;
      Slog.e("OppoBootReceiver_OppoBootAeeLogUtil", "error zipping up profile data", iOException1);
      if (zipOutputStream != null)
        try {
          zipOutputStream.close();
        } catch (IOException iOException) {} 
      return;
    } finally {}
    if (iOException != null)
      try {
        iOException.close();
      } catch (IOException iOException1) {} 
    throw paramString2;
  }
  
  public static void gzipFile(String paramString1, String paramString2) {
    byte[] arrayOfByte = new byte[1024];
    try {
      FileOutputStream fileOutputStream = new FileOutputStream();
      this(paramString2);
      GZIPOutputStream gZIPOutputStream = new GZIPOutputStream();
      this(fileOutputStream);
      FileInputStream fileInputStream = new FileInputStream();
      this(paramString1);
      while (true) {
        int i = fileInputStream.read(arrayOfByte);
        if (i > 0) {
          gZIPOutputStream.write(arrayOfByte, 0, i);
          continue;
        } 
        break;
      } 
      fileInputStream.close();
      gZIPOutputStream.finish();
      gZIPOutputStream.close();
      Slog.d("OppoBootReceiver_OppoBootAeeLogUtil", "The file was compressed successfully!");
    } catch (IOException iOException) {
      iOException.printStackTrace();
    } 
  }
  
  private static void deleteDir(File paramFile) {
    if (!paramFile.exists())
      return; 
    File[] arrayOfFile = paramFile.listFiles();
    if (paramFile.isDirectory() && arrayOfFile != null && arrayOfFile.length > 0) {
      int i;
      byte b;
      for (i = arrayOfFile.length, b = 0; b < i; ) {
        File file = arrayOfFile[b];
        deleteDir(file);
        b++;
      } 
      deleteFile(paramFile);
    } else {
      deleteFile(paramFile);
    } 
  }
  
  private static void deleteFile(File paramFile) {
    if (paramFile.delete()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("file: ");
      stringBuilder.append(paramFile);
      stringBuilder.append(" delete succeed");
      Slog.w("OppoBootReceiver_OppoBootAeeLogUtil", stringBuilder.toString());
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("file: ");
      stringBuilder.append(paramFile);
      stringBuilder.append(" delete failed");
      Slog.e("OppoBootReceiver_OppoBootAeeLogUtil", stringBuilder.toString());
    } 
  }
  
  private static void generateSystemCrashLog(String paramString) {
    Slog.w("OppoBootReceiver_OppoBootAeeLogUtil", "system_server unknown reboot call");
    try {
      boolean bool = SystemProperties.get("ro.vendor.have_aee_feature").equals("1");
      if (bool)
        try {
          if (OplusMirrorMtkExceptionLogHelper.generateSystemCrashLog != null) {
            OplusMirrorMtkExceptionLogHelper.generateSystemCrashLog.call(paramString, new Object[0]);
          } else {
            Slog.e("OppoBootReceiver_OppoBootAeeLogUtil", "generateSystemCrashLog failed for method empty.");
          } 
        } catch (Exception exception) {
          Slog.e("OppoBootReceiver_OppoBootAeeLogUtil", "generateSystemCrashLog failed!", exception);
        }  
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("generateSystemCrashLog :");
      stringBuilder.append(exception.toString());
      Slog.e("OppoBootReceiver_OppoBootAeeLogUtil", stringBuilder.toString());
    } 
    Slog.w("OppoBootReceiver_OppoBootAeeLogUtil", "system_server unknown reboot call end");
  }
}
