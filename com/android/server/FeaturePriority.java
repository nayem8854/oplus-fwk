package com.android.server;

public enum FeaturePriority {
  END, PRIORITY_COMPANY, PRIORITY_COUNTRY, PRIORITY_ENGINEER, PRIORITY_OPERATOR, PRIORITY_PRODUCT, PRIORITY_STOCK, PRIORITY_SYSTEM;
  
  private static final FeaturePriority[] $VALUES;
  
  static {
    PRIORITY_STOCK = new FeaturePriority("PRIORITY_STOCK", 1);
    PRIORITY_PRODUCT = new FeaturePriority("PRIORITY_PRODUCT", 2);
    PRIORITY_COUNTRY = new FeaturePriority("PRIORITY_COUNTRY", 3);
    PRIORITY_OPERATOR = new FeaturePriority("PRIORITY_OPERATOR", 4);
    PRIORITY_COMPANY = new FeaturePriority("PRIORITY_COMPANY", 5);
    PRIORITY_ENGINEER = new FeaturePriority("PRIORITY_ENGINEER", 6);
    FeaturePriority featurePriority = new FeaturePriority("END", 7);
    $VALUES = new FeaturePriority[] { PRIORITY_SYSTEM, PRIORITY_STOCK, PRIORITY_PRODUCT, PRIORITY_COUNTRY, PRIORITY_OPERATOR, PRIORITY_COMPANY, PRIORITY_ENGINEER, featurePriority };
  }
}
