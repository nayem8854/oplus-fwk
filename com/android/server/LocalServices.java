package com.android.server;

import android.util.ArrayMap;

public final class LocalServices {
  private static final ArrayMap<Class<?>, Object> sLocalServiceObjects = new ArrayMap();
  
  public static <T> T getService(Class<T> paramClass) {
    synchronized (sLocalServiceObjects) {
      return (T)sLocalServiceObjects.get(paramClass);
    } 
  }
  
  public static <T> void addService(Class<T> paramClass, T paramT) {
    synchronized (sLocalServiceObjects) {
      if (!sLocalServiceObjects.containsKey(paramClass)) {
        sLocalServiceObjects.put(paramClass, paramT);
        return;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("Overriding service registration");
      throw illegalStateException;
    } 
  }
  
  public static <T> void removeServiceForTest(Class<T> paramClass) {
    synchronized (sLocalServiceObjects) {
      sLocalServiceObjects.remove(paramClass);
      return;
    } 
  }
}
