package com.android.server.backup;

import android.app.backup.BlobBackupHelper;
import android.app.slice.ISliceManager;
import android.content.Context;
import android.os.IBinder;
import android.os.ServiceManager;
import android.util.Log;
import android.util.Slog;

public class SliceBackupHelper extends BlobBackupHelper {
  static final int BLOB_VERSION = 1;
  
  static final boolean DEBUG = Log.isLoggable("SliceBackupHelper", 3);
  
  static final String KEY_SLICES = "slices";
  
  static final String TAG = "SliceBackupHelper";
  
  public SliceBackupHelper(Context paramContext) {
    super(1, new String[] { "slices" });
  }
  
  protected byte[] getBackupPayload(String paramString) {
    byte[] arrayOfByte = null;
    if ("slices".equals(paramString))
      try {
        IBinder iBinder = ServiceManager.getService("slice");
        ISliceManager iSliceManager = ISliceManager.Stub.asInterface(iBinder);
        arrayOfByte = iSliceManager.getBackupPayload(0);
      } catch (Exception exception) {
        Slog.e("SliceBackupHelper", "Couldn't communicate with slice manager");
        arrayOfByte = null;
      }  
    return arrayOfByte;
  }
  
  protected void applyRestoredPayload(String paramString, byte[] paramArrayOfbyte) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Got restore of ");
      stringBuilder.append(paramString);
      Slog.v("SliceBackupHelper", stringBuilder.toString());
    } 
    if ("slices".equals(paramString))
      try {
        IBinder iBinder = ServiceManager.getService("slice");
        ISliceManager iSliceManager = ISliceManager.Stub.asInterface(iBinder);
        iSliceManager.applyRestore(paramArrayOfbyte, 0);
      } catch (Exception exception) {
        Slog.e("SliceBackupHelper", "Couldn't communicate with slice manager");
      }  
  }
}
