package com.android.server.backup;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.backup.BackupDataInputStream;
import android.app.backup.BackupDataOutput;
import android.app.backup.BackupHelper;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncAdapterType;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AccountSyncSettingsBackupHelper implements BackupHelper {
  private static final boolean DEBUG = false;
  
  private static final String JSON_FORMAT_ENCODING = "UTF-8";
  
  private static final String JSON_FORMAT_HEADER_KEY = "account_data";
  
  private static final int JSON_FORMAT_VERSION = 1;
  
  private static final String KEY_ACCOUNTS = "accounts";
  
  private static final String KEY_ACCOUNT_AUTHORITIES = "authorities";
  
  private static final String KEY_ACCOUNT_NAME = "name";
  
  private static final String KEY_ACCOUNT_TYPE = "type";
  
  private static final String KEY_AUTHORITY_NAME = "name";
  
  private static final String KEY_AUTHORITY_SYNC_ENABLED = "syncEnabled";
  
  private static final String KEY_AUTHORITY_SYNC_STATE = "syncState";
  
  private static final String KEY_MASTER_SYNC_ENABLED = "masterSyncEnabled";
  
  private static final String KEY_VERSION = "version";
  
  private static final int MD5_BYTE_SIZE = 16;
  
  private static final String STASH_FILE = "/backup/unadded_account_syncsettings.json";
  
  private static final int STATE_VERSION = 1;
  
  private static final int SYNC_REQUEST_LATCH_TIMEOUT_SECONDS = 1;
  
  private static final String TAG = "AccountSyncSettingsBackupHelper";
  
  private AccountManager mAccountManager;
  
  private Context mContext;
  
  private final int mUserId;
  
  public AccountSyncSettingsBackupHelper(Context paramContext, int paramInt) {
    this.mContext = paramContext;
    this.mAccountManager = AccountManager.get(paramContext);
    this.mUserId = paramInt;
  }
  
  public void performBackup(ParcelFileDescriptor paramParcelFileDescriptor1, BackupDataOutput paramBackupDataOutput, ParcelFileDescriptor paramParcelFileDescriptor2) {
    try {
      JSONObject jSONObject = serializeAccountSyncSettingsToJSON(this.mUserId);
      byte[] arrayOfByte2 = jSONObject.toString().getBytes("UTF-8");
      byte[] arrayOfByte3 = readOldMd5Checksum(paramParcelFileDescriptor1);
      byte[] arrayOfByte1 = generateMd5Checksum(arrayOfByte2);
      if (!Arrays.equals(arrayOfByte3, arrayOfByte1)) {
        int i = arrayOfByte2.length;
        paramBackupDataOutput.writeEntityHeader("account_data", i);
        paramBackupDataOutput.writeEntityData(arrayOfByte2, i);
        Log.i("AccountSyncSettingsBackupHelper", "Backup successful.");
      } else {
        Log.i("AccountSyncSettingsBackupHelper", "Old and new MD5 checksums match. Skipping backup.");
      } 
      writeNewMd5Checksum(paramParcelFileDescriptor2, arrayOfByte1);
    } catch (JSONException|IOException|NoSuchAlgorithmException jSONException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Couldn't backup account sync settings\n");
      stringBuilder.append(jSONException);
      Log.e("AccountSyncSettingsBackupHelper", stringBuilder.toString());
    } 
  }
  
  private JSONObject serializeAccountSyncSettingsToJSON(int paramInt) throws JSONException {
    Account[] arrayOfAccount = this.mAccountManager.getAccountsAsUser(paramInt);
    SyncAdapterType[] arrayOfSyncAdapterType = ContentResolver.getSyncAdapterTypesAsUser(paramInt);
    HashMap<Object, Object> hashMap = new HashMap<>();
    int i, j, k;
    for (i = arrayOfSyncAdapterType.length, j = 0, k = 0; k < i; ) {
      SyncAdapterType syncAdapterType = arrayOfSyncAdapterType[k];
      if (syncAdapterType.isUserVisible()) {
        if (!hashMap.containsKey(syncAdapterType.accountType))
          hashMap.put(syncAdapterType.accountType, new ArrayList()); 
        ((List<String>)hashMap.get(syncAdapterType.accountType)).add(syncAdapterType.authority);
      } 
      k++;
    } 
    JSONObject jSONObject = new JSONObject();
    jSONObject.put("version", 1);
    jSONObject.put("masterSyncEnabled", ContentResolver.getMasterSyncAutomaticallyAsUser(paramInt));
    JSONArray jSONArray = new JSONArray();
    for (i = arrayOfAccount.length, k = j; k < i; ) {
      Account account = arrayOfAccount[k];
      List list = (List)hashMap.get(account.type);
      if (list != null && !list.isEmpty()) {
        JSONObject jSONObject1 = new JSONObject();
        jSONObject1.put("name", account.name);
        jSONObject1.put("type", account.type);
        JSONArray jSONArray1 = new JSONArray();
        for (String str : list) {
          j = ContentResolver.getIsSyncableAsUser(account, str, paramInt);
          boolean bool = ContentResolver.getSyncAutomaticallyAsUser(account, str, paramInt);
          JSONObject jSONObject2 = new JSONObject();
          jSONObject2.put("name", str);
          jSONObject2.put("syncState", j);
          jSONObject2.put("syncEnabled", bool);
          jSONArray1.put(jSONObject2);
        } 
        jSONObject1.put("authorities", jSONArray1);
        jSONArray.put(jSONObject1);
      } 
      k++;
    } 
    jSONObject.put("accounts", jSONArray);
    return jSONObject;
  }
  
  private byte[] readOldMd5Checksum(ParcelFileDescriptor paramParcelFileDescriptor) throws IOException {
    DataInputStream dataInputStream = new DataInputStream(new FileInputStream(paramParcelFileDescriptor.getFileDescriptor()));
    byte[] arrayOfByte = new byte[16];
    try {
      int i = dataInputStream.readInt();
      if (i <= 1) {
        for (i = 0; i < 16; i++)
          arrayOfByte[i] = dataInputStream.readByte(); 
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Backup state version is: ");
        stringBuilder.append(i);
        stringBuilder.append(" (support only up to version ");
        stringBuilder.append(1);
        stringBuilder.append(")");
        Log.i("AccountSyncSettingsBackupHelper", stringBuilder.toString());
      } 
    } catch (EOFException eOFException) {}
    return arrayOfByte;
  }
  
  private void writeNewMd5Checksum(ParcelFileDescriptor paramParcelFileDescriptor, byte[] paramArrayOfbyte) throws IOException {
    DataOutputStream dataOutputStream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(paramParcelFileDescriptor.getFileDescriptor())));
    dataOutputStream.writeInt(1);
    dataOutputStream.write(paramArrayOfbyte);
  }
  
  private byte[] generateMd5Checksum(byte[] paramArrayOfbyte) throws NoSuchAlgorithmException {
    if (paramArrayOfbyte == null)
      return null; 
    MessageDigest messageDigest = MessageDigest.getInstance("MD5");
    return messageDigest.digest(paramArrayOfbyte);
  }
  
  public void restoreEntity(BackupDataInputStream paramBackupDataInputStream) {
    byte[] arrayOfByte = new byte[paramBackupDataInputStream.size()];
    try {
      paramBackupDataInputStream.read(arrayOfByte);
      String str = new String();
      this(arrayOfByte, "UTF-8");
      JSONObject jSONObject = new JSONObject();
      this(str);
      boolean bool1 = jSONObject.getBoolean("masterSyncEnabled");
      null = jSONObject.getJSONArray("accounts");
      boolean bool2 = ContentResolver.getMasterSyncAutomaticallyAsUser(this.mUserId);
      if (bool2)
        ContentResolver.setMasterSyncAutomaticallyAsUser(false, this.mUserId); 
      try {
        restoreFromJsonArray(null, this.mUserId);
        ContentResolver.setMasterSyncAutomaticallyAsUser(bool1, this.mUserId);
      } finally {
        ContentResolver.setMasterSyncAutomaticallyAsUser(bool1, this.mUserId);
      } 
    } catch (IOException|JSONException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Couldn't restore account sync settings\n");
      stringBuilder.append(iOException);
      Log.e("AccountSyncSettingsBackupHelper", stringBuilder.toString());
    } 
  }
  
  private void restoreFromJsonArray(JSONArray paramJSONArray, int paramInt) throws JSONException {
    Set<Account> set = getAccounts(paramInt);
    JSONArray jSONArray = new JSONArray();
    for (byte b = 0; b < paramJSONArray.length(); b++) {
      JSONObject jSONObject = (JSONObject)paramJSONArray.get(b);
      String str1 = jSONObject.getString("name");
      String str2 = jSONObject.getString("type");
      try {
        Account account = new Account();
        this(str1, str2);
        if (set.contains(account)) {
          restoreExistingAccountSyncSettingsFromJSON(jSONObject, paramInt);
        } else {
          jSONArray.put(jSONObject);
        } 
      } catch (IllegalArgumentException illegalArgumentException) {}
    } 
    if (jSONArray.length() > 0) {
      try {
        FileOutputStream fileOutputStream = new FileOutputStream();
        this(getStashFile(paramInt));
        try {
          String str = jSONArray.toString();
          DataOutputStream dataOutputStream = new DataOutputStream();
          this(fileOutputStream);
          dataOutputStream.writeUTF(str);
        } finally {
          try {
            fileOutputStream.close();
          } finally {
            fileOutputStream = null;
          } 
        } 
      } catch (IOException iOException) {
        Log.e("AccountSyncSettingsBackupHelper", "unable to write the sync settings to the stash file", iOException);
      } 
    } else {
      File file = getStashFile(paramInt);
      if (file.exists())
        file.delete(); 
    } 
  }
  
  private void accountAddedInternal(int paramInt) {
    try {
      FileInputStream fileInputStream = new FileInputStream();
      this(getStashFile(paramInt));
      try {
        DataInputStream dataInputStream = new DataInputStream();
        this(fileInputStream);
        String str = dataInputStream.readUTF();
        fileInputStream.close();
        return;
      } finally {
        try {
          jSONException.close();
        } finally {
          jSONException = null;
        } 
      } 
    } catch (FileNotFoundException fileNotFoundException) {
      return;
    } catch (IOException iOException) {
      return;
    } 
  }
  
  public static void accountAdded(Context paramContext, int paramInt) {
    AccountSyncSettingsBackupHelper accountSyncSettingsBackupHelper = new AccountSyncSettingsBackupHelper(paramContext, paramInt);
    accountSyncSettingsBackupHelper.accountAddedInternal(paramInt);
  }
  
  private Set<Account> getAccounts(int paramInt) {
    Account[] arrayOfAccount = this.mAccountManager.getAccountsAsUser(paramInt);
    HashSet<Account> hashSet = new HashSet();
    for (int i = arrayOfAccount.length; paramInt < i; ) {
      Account account = arrayOfAccount[paramInt];
      hashSet.add(account);
      paramInt++;
    } 
    return hashSet;
  }
  
  private void restoreExistingAccountSyncSettingsFromJSON(JSONObject paramJSONObject, int paramInt) throws JSONException {
    JSONArray jSONArray = paramJSONObject.getJSONArray("authorities");
    String str2 = paramJSONObject.getString("name");
    String str1 = paramJSONObject.getString("type");
    Account account = new Account(str2, str1);
    for (byte b = 0; b < jSONArray.length(); b++) {
      JSONObject jSONObject = (JSONObject)jSONArray.get(b);
      str2 = jSONObject.getString("name");
      boolean bool = jSONObject.getBoolean("syncEnabled");
      int i = jSONObject.getInt("syncState");
      ContentResolver.setSyncAutomaticallyAsUser(account, str2, bool, paramInt);
      if (!bool) {
        if (i == 0) {
          i = 0;
        } else {
          i = 2;
        } 
        ContentResolver.setIsSyncableAsUser(account, str2, i, paramInt);
      } 
    } 
  }
  
  public void writeNewStateDescription(ParcelFileDescriptor paramParcelFileDescriptor) {}
  
  private static File getStashFile(int paramInt) {
    File file;
    if (paramInt == 0) {
      file = Environment.getDataDirectory();
    } else {
      file = Environment.getDataSystemCeDirectory(paramInt);
    } 
    return new File(file, "/backup/unadded_account_syncsettings.json");
  }
}
