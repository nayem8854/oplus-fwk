package com.android.server.backup;

import android.app.INotificationManager;
import android.app.backup.BlobBackupHelper;
import android.os.IBinder;
import android.os.ServiceManager;
import android.util.Log;
import android.util.Slog;

public class NotificationBackupHelper extends BlobBackupHelper {
  static final int BLOB_VERSION = 1;
  
  static final boolean DEBUG = Log.isLoggable("NotifBackupHelper", 3);
  
  static final String KEY_NOTIFICATIONS = "notifications";
  
  static final String TAG = "NotifBackupHelper";
  
  private final int mUserId;
  
  public NotificationBackupHelper(int paramInt) {
    super(1, new String[] { "notifications" });
    this.mUserId = paramInt;
  }
  
  protected byte[] getBackupPayload(String paramString) {
    byte[] arrayOfByte = null;
    if ("notifications".equals(paramString))
      try {
        IBinder iBinder = ServiceManager.getService("notification");
        INotificationManager iNotificationManager = INotificationManager.Stub.asInterface(iBinder);
        arrayOfByte = iNotificationManager.getBackupPayload(this.mUserId);
      } catch (Exception exception) {
        Slog.e("NotifBackupHelper", "Couldn't communicate with notification manager");
        arrayOfByte = null;
      }  
    return arrayOfByte;
  }
  
  protected void applyRestoredPayload(String paramString, byte[] paramArrayOfbyte) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Got restore of ");
      stringBuilder.append(paramString);
      Slog.v("NotifBackupHelper", stringBuilder.toString());
    } 
    if ("notifications".equals(paramString))
      try {
        IBinder iBinder = ServiceManager.getService("notification");
        INotificationManager iNotificationManager = INotificationManager.Stub.asInterface(iBinder);
        iNotificationManager.applyRestore(paramArrayOfbyte, this.mUserId);
      } catch (Exception exception) {
        Slog.e("NotifBackupHelper", "Couldn't communicate with notification manager");
      }  
  }
}
