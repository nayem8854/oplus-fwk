package com.android.server.backup;

import android.accounts.AccountManagerInternal;
import android.app.backup.BlobBackupHelper;
import android.util.Slog;
import com.android.server.LocalServices;

public class AccountManagerBackupHelper extends BlobBackupHelper {
  private static final boolean DEBUG = false;
  
  private static final String KEY_ACCOUNT_ACCESS_GRANTS = "account_access_grants";
  
  private static final int STATE_VERSION = 1;
  
  private static final String TAG = "AccountsBackup";
  
  public AccountManagerBackupHelper() {
    super(1, new String[] { "account_access_grants" });
  }
  
  protected byte[] getBackupPayload(String paramString) {
    AccountManagerInternal accountManagerInternal = LocalServices.<AccountManagerInternal>getService(AccountManagerInternal.class);
    byte b = -1;
    try {
      StringBuilder stringBuilder;
      if (paramString.hashCode() == 1544100736 && paramString.equals("account_access_grants"))
        b = 0; 
      if (b != 0) {
        stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Unexpected backup key ");
        stringBuilder.append(paramString);
        Slog.w("AccountsBackup", stringBuilder.toString());
      } else {
        return stringBuilder.backupAccountAccessPermissions(0);
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to store payload ");
      stringBuilder.append(paramString);
      Slog.e("AccountsBackup", stringBuilder.toString());
    } 
    return new byte[0];
  }
  
  protected void applyRestoredPayload(String paramString, byte[] paramArrayOfbyte) {
    AccountManagerInternal accountManagerInternal = LocalServices.<AccountManagerInternal>getService(AccountManagerInternal.class);
    byte b = -1;
    try {
      StringBuilder stringBuilder;
      if (paramString.hashCode() == 1544100736 && paramString.equals("account_access_grants"))
        b = 0; 
      if (b != 0) {
        stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Unexpected restore key ");
        stringBuilder.append(paramString);
        Slog.w("AccountsBackup", stringBuilder.toString());
      } else {
        accountManagerInternal.restoreAccountAccessPermissions((byte[])stringBuilder, 0);
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to restore key ");
      stringBuilder.append(paramString);
      Slog.w("AccountsBackup", stringBuilder.toString());
    } 
  }
}
