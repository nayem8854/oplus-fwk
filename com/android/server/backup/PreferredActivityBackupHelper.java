package com.android.server.backup;

import android.app.AppGlobals;
import android.app.backup.BlobBackupHelper;
import android.content.pm.IPackageManager;
import android.util.Slog;

public class PreferredActivityBackupHelper extends BlobBackupHelper {
  private static final boolean DEBUG = false;
  
  private static final String KEY_DEFAULT_APPS = "default-apps";
  
  private static final String KEY_INTENT_VERIFICATION = "intent-verification";
  
  private static final String KEY_PREFERRED = "preferred-activity";
  
  private static final int STATE_VERSION = 3;
  
  private static final String TAG = "PreferredBackup";
  
  public PreferredActivityBackupHelper() {
    super(3, new String[] { "preferred-activity", "default-apps", "intent-verification" });
  }
  
  protected byte[] getBackupPayload(String paramString) {
    IPackageManager iPackageManager = AppGlobals.getPackageManager();
    byte b = -1;
    try {
      StringBuilder stringBuilder;
      int i = paramString.hashCode();
      if (i != -696985986) {
        if (i != -429170260) {
          if (i == 1336142555 && paramString.equals("preferred-activity"))
            b = 0; 
        } else if (paramString.equals("intent-verification")) {
          b = 2;
        } 
      } else if (paramString.equals("default-apps")) {
        b = 1;
      } 
      if (b != 0) {
        if (b != 1) {
          if (b != 2) {
            stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("Unexpected backup key ");
            stringBuilder.append(paramString);
            Slog.w("PreferredBackup", stringBuilder.toString());
          } else {
            return stringBuilder.getIntentFilterVerificationBackup(0);
          } 
        } else {
          return stringBuilder.getDefaultAppsBackup(0);
        } 
      } else {
        return stringBuilder.getPreferredActivityBackup(0);
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to store payload ");
      stringBuilder.append(paramString);
      Slog.e("PreferredBackup", stringBuilder.toString());
    } 
    return null;
  }
  
  protected void applyRestoredPayload(String paramString, byte[] paramArrayOfbyte) {
    IPackageManager iPackageManager = AppGlobals.getPackageManager();
    byte b = -1;
    try {
      StringBuilder stringBuilder;
      int i = paramString.hashCode();
      if (i != -696985986) {
        if (i != -429170260) {
          if (i == 1336142555 && paramString.equals("preferred-activity"))
            b = 0; 
        } else if (paramString.equals("intent-verification")) {
          b = 2;
        } 
      } else if (paramString.equals("default-apps")) {
        b = 1;
      } 
      if (b != 0) {
        if (b != 1) {
          if (b != 2) {
            stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("Unexpected restore key ");
            stringBuilder.append(paramString);
            Slog.w("PreferredBackup", stringBuilder.toString());
          } else {
            iPackageManager.restoreIntentFilterVerification((byte[])stringBuilder, 0);
          } 
        } else {
          iPackageManager.restoreDefaultApps((byte[])stringBuilder, 0);
        } 
      } else {
        iPackageManager.restorePreferredActivities((byte[])stringBuilder, 0);
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to restore key ");
      stringBuilder.append(paramString);
      Slog.w("PreferredBackup", stringBuilder.toString());
    } 
  }
}
