package com.android.server.backup;

import android.app.backup.BlobBackupHelper;
import android.os.UserHandle;
import android.permission.PermissionManagerInternal;
import android.util.Slog;
import com.android.server.LocalServices;

public class PermissionBackupHelper extends BlobBackupHelper {
  private static final boolean DEBUG = false;
  
  private static final String KEY_PERMISSIONS = "permissions";
  
  private static final int STATE_VERSION = 1;
  
  private static final String TAG = "PermissionBackup";
  
  private final PermissionManagerInternal mPermissionManager;
  
  private final UserHandle mUser;
  
  public PermissionBackupHelper(int paramInt) {
    super(1, new String[] { "permissions" });
    this.mUser = UserHandle.of(paramInt);
    this.mPermissionManager = LocalServices.<PermissionManagerInternal>getService(PermissionManagerInternal.class);
  }
  
  protected byte[] getBackupPayload(String paramString) {
    byte b = -1;
    try {
      if (paramString.hashCode() == 1133704324 && paramString.equals("permissions"))
        b = 0; 
      if (b != 0) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Unexpected backup key ");
        stringBuilder.append(paramString);
        Slog.w("PermissionBackup", stringBuilder.toString());
      } else {
        return this.mPermissionManager.backupRuntimePermissions(this.mUser);
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to store payload ");
      stringBuilder.append(paramString);
      Slog.e("PermissionBackup", stringBuilder.toString());
    } 
    return null;
  }
  
  protected void applyRestoredPayload(String paramString, byte[] paramArrayOfbyte) {
    byte b = -1;
    try {
      StringBuilder stringBuilder;
      if (paramString.hashCode() == 1133704324 && paramString.equals("permissions"))
        b = 0; 
      if (b != 0) {
        stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Unexpected restore key ");
        stringBuilder.append(paramString);
        Slog.w("PermissionBackup", stringBuilder.toString());
      } else {
        this.mPermissionManager.restoreRuntimePermissions((byte[])stringBuilder, this.mUser);
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to restore key ");
      stringBuilder.append(paramString);
      Slog.w("PermissionBackup", stringBuilder.toString());
    } 
  }
}
