package com.android.server.backup;

import android.app.backup.BlobBackupHelper;
import android.content.pm.IShortcutService;
import android.os.IBinder;
import android.os.ServiceManager;
import android.util.Slog;

public class ShortcutBackupHelper extends BlobBackupHelper {
  private static final int BLOB_VERSION = 1;
  
  private static final String KEY_USER_FILE = "shortcutuser.xml";
  
  private static final String TAG = "ShortcutBackupAgent";
  
  public ShortcutBackupHelper() {
    super(1, new String[] { "shortcutuser.xml" });
  }
  
  private IShortcutService getShortcutService() {
    IBinder iBinder = ServiceManager.getService("shortcut");
    return IShortcutService.Stub.asInterface(iBinder);
  }
  
  protected byte[] getBackupPayload(String paramString) {
    byte b;
    if (paramString.hashCode() == -792920646 && paramString.equals("shortcutuser.xml")) {
      b = 0;
    } else {
      b = -1;
    } 
    if (b != 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unknown key: ");
      stringBuilder.append(paramString);
      Slog.w("ShortcutBackupAgent", stringBuilder.toString());
    } else {
      try {
        return getShortcutService().getBackupPayload(0);
      } catch (Exception exception) {
        Slog.wtf("ShortcutBackupAgent", "Backup failed", exception);
      } 
    } 
    return null;
  }
  
  protected void applyRestoredPayload(String paramString, byte[] paramArrayOfbyte) {
    StringBuilder stringBuilder;
    byte b;
    if (paramString.hashCode() == -792920646 && paramString.equals("shortcutuser.xml")) {
      b = 0;
    } else {
      b = -1;
    } 
    if (b != 0) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Unknown key: ");
      stringBuilder.append(paramString);
      Slog.w("ShortcutBackupAgent", stringBuilder.toString());
    } else {
      try {
        getShortcutService().applyRestore((byte[])stringBuilder, 0);
      } catch (Exception exception) {
        Slog.wtf("ShortcutBackupAgent", "Restore failed", exception);
      } 
    } 
  }
}
