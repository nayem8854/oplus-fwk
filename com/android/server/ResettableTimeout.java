package com.android.server;

import android.os.ConditionVariable;

abstract class ResettableTimeout {
  public void go(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokestatic uptimeMillis : ()J
    //   6: lload_1
    //   7: ladd
    //   8: putfield mOffAt : J
    //   11: aload_0
    //   12: getfield mThread : Ljava/lang/Thread;
    //   15: ifnonnull -> 65
    //   18: iconst_0
    //   19: istore_3
    //   20: aload_0
    //   21: getfield mLock : Landroid/os/ConditionVariable;
    //   24: invokevirtual close : ()V
    //   27: new com/android/server/ResettableTimeout$T
    //   30: astore #4
    //   32: aload #4
    //   34: aload_0
    //   35: aconst_null
    //   36: invokespecial <init> : (Lcom/android/server/ResettableTimeout;Lcom/android/server/ResettableTimeout$1;)V
    //   39: aload_0
    //   40: aload #4
    //   42: putfield mThread : Ljava/lang/Thread;
    //   45: aload #4
    //   47: invokevirtual start : ()V
    //   50: aload_0
    //   51: getfield mLock : Landroid/os/ConditionVariable;
    //   54: invokevirtual block : ()V
    //   57: aload_0
    //   58: iconst_0
    //   59: putfield mOffCalled : Z
    //   62: goto -> 74
    //   65: iconst_1
    //   66: istore_3
    //   67: aload_0
    //   68: getfield mThread : Ljava/lang/Thread;
    //   71: invokevirtual interrupt : ()V
    //   74: aload_0
    //   75: iload_3
    //   76: invokevirtual on : (Z)V
    //   79: aload_0
    //   80: monitorexit
    //   81: return
    //   82: astore #4
    //   84: aload_0
    //   85: monitorexit
    //   86: aload #4
    //   88: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #54	-> 0
    //   #55	-> 2
    //   #64	-> 11
    //   #65	-> 18
    //   #66	-> 20
    //   #67	-> 27
    //   #68	-> 45
    //   #69	-> 50
    //   #70	-> 57
    //   #72	-> 65
    //   #74	-> 67
    //   #76	-> 74
    //   #77	-> 79
    //   #78	-> 81
    //   #77	-> 82
    // Exception table:
    //   from	to	target	type
    //   2	11	82	finally
    //   11	18	82	finally
    //   20	27	82	finally
    //   27	45	82	finally
    //   45	50	82	finally
    //   50	57	82	finally
    //   57	62	82	finally
    //   67	74	82	finally
    //   74	79	82	finally
    //   79	81	82	finally
    //   84	86	82	finally
  }
  
  public void cancel() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: lconst_0
    //   4: putfield mOffAt : J
    //   7: aload_0
    //   8: getfield mThread : Ljava/lang/Thread;
    //   11: ifnull -> 26
    //   14: aload_0
    //   15: getfield mThread : Ljava/lang/Thread;
    //   18: invokevirtual interrupt : ()V
    //   21: aload_0
    //   22: aconst_null
    //   23: putfield mThread : Ljava/lang/Thread;
    //   26: aload_0
    //   27: getfield mOffCalled : Z
    //   30: ifne -> 42
    //   33: aload_0
    //   34: iconst_1
    //   35: putfield mOffCalled : Z
    //   38: aload_0
    //   39: invokevirtual off : ()V
    //   42: aload_0
    //   43: monitorexit
    //   44: return
    //   45: astore_1
    //   46: aload_0
    //   47: monitorexit
    //   48: aload_1
    //   49: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #85	-> 0
    //   #86	-> 2
    //   #87	-> 7
    //   #88	-> 14
    //   #89	-> 21
    //   #91	-> 26
    //   #92	-> 33
    //   #93	-> 38
    //   #95	-> 42
    //   #96	-> 44
    //   #95	-> 45
    // Exception table:
    //   from	to	target	type
    //   2	7	45	finally
    //   7	14	45	finally
    //   14	21	45	finally
    //   21	26	45	finally
    //   26	33	45	finally
    //   33	38	45	finally
    //   38	42	45	finally
    //   42	44	45	finally
    //   46	48	45	finally
  }
  
  private class T extends Thread {
    final ResettableTimeout this$0;
    
    private T() {}
    
    public void run() {
      // Byte code:
      //   0: aload_0
      //   1: getfield this$0 : Lcom/android/server/ResettableTimeout;
      //   4: invokestatic access$100 : (Lcom/android/server/ResettableTimeout;)Landroid/os/ConditionVariable;
      //   7: invokevirtual open : ()V
      //   10: aload_0
      //   11: monitorenter
      //   12: aload_0
      //   13: getfield this$0 : Lcom/android/server/ResettableTimeout;
      //   16: invokestatic access$200 : (Lcom/android/server/ResettableTimeout;)J
      //   19: invokestatic uptimeMillis : ()J
      //   22: lsub
      //   23: lstore_1
      //   24: lload_1
      //   25: lconst_0
      //   26: lcmp
      //   27: ifgt -> 58
      //   30: aload_0
      //   31: getfield this$0 : Lcom/android/server/ResettableTimeout;
      //   34: iconst_1
      //   35: invokestatic access$302 : (Lcom/android/server/ResettableTimeout;Z)Z
      //   38: pop
      //   39: aload_0
      //   40: getfield this$0 : Lcom/android/server/ResettableTimeout;
      //   43: invokevirtual off : ()V
      //   46: aload_0
      //   47: getfield this$0 : Lcom/android/server/ResettableTimeout;
      //   50: aconst_null
      //   51: invokestatic access$402 : (Lcom/android/server/ResettableTimeout;Ljava/lang/Thread;)Ljava/lang/Thread;
      //   54: pop
      //   55: aload_0
      //   56: monitorexit
      //   57: return
      //   58: aload_0
      //   59: monitorexit
      //   60: lload_1
      //   61: invokestatic sleep : (J)V
      //   64: goto -> 68
      //   67: astore_3
      //   68: goto -> 10
      //   71: astore_3
      //   72: aload_0
      //   73: monitorexit
      //   74: aload_3
      //   75: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #102	-> 0
      //   #105	-> 10
      //   #106	-> 12
      //   #107	-> 24
      //   #108	-> 30
      //   #109	-> 39
      //   #110	-> 46
      //   #111	-> 55
      //   #120	-> 57
      //   #113	-> 58
      //   #115	-> 60
      //   #118	-> 64
      //   #117	-> 67
      //   #119	-> 68
      //   #113	-> 71
      // Exception table:
      //   from	to	target	type
      //   12	24	71	finally
      //   30	39	71	finally
      //   39	46	71	finally
      //   46	55	71	finally
      //   55	57	71	finally
      //   58	60	71	finally
      //   60	64	67	java/lang/InterruptedException
      //   72	74	71	finally
    }
  }
  
  private ConditionVariable mLock = new ConditionVariable();
  
  private volatile long mOffAt;
  
  private volatile boolean mOffCalled;
  
  private Thread mThread;
  
  public abstract void off();
  
  public abstract void on(boolean paramBoolean);
}
