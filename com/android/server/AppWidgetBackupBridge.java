package com.android.server;

import java.util.List;

public class AppWidgetBackupBridge {
  private static WidgetBackupProvider sAppWidgetService;
  
  public static void register(WidgetBackupProvider paramWidgetBackupProvider) {
    sAppWidgetService = paramWidgetBackupProvider;
  }
  
  public static List<String> getWidgetParticipants(int paramInt) {
    WidgetBackupProvider widgetBackupProvider = sAppWidgetService;
    if (widgetBackupProvider != null) {
      List<String> list = widgetBackupProvider.getWidgetParticipants(paramInt);
    } else {
      widgetBackupProvider = null;
    } 
    return (List<String>)widgetBackupProvider;
  }
  
  public static byte[] getWidgetState(String paramString, int paramInt) {
    WidgetBackupProvider widgetBackupProvider = sAppWidgetService;
    if (widgetBackupProvider != null) {
      byte[] arrayOfByte = widgetBackupProvider.getWidgetState(paramString, paramInt);
    } else {
      paramString = null;
    } 
    return (byte[])paramString;
  }
  
  public static void restoreStarting(int paramInt) {
    WidgetBackupProvider widgetBackupProvider = sAppWidgetService;
    if (widgetBackupProvider != null)
      widgetBackupProvider.restoreStarting(paramInt); 
  }
  
  public static void restoreWidgetState(String paramString, byte[] paramArrayOfbyte, int paramInt) {
    WidgetBackupProvider widgetBackupProvider = sAppWidgetService;
    if (widgetBackupProvider != null)
      widgetBackupProvider.restoreWidgetState(paramString, paramArrayOfbyte, paramInt); 
  }
  
  public static void restoreFinished(int paramInt) {
    WidgetBackupProvider widgetBackupProvider = sAppWidgetService;
    if (widgetBackupProvider != null)
      widgetBackupProvider.restoreFinished(paramInt); 
  }
}
