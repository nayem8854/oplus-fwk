package com.android.server;

import android.content.pm.FeatureInfo;
import android.util.ArrayMap;
import java.io.File;

public class OplusBaseSystemConfig {
  public ArrayMap<String, FeatureInfo> loadOplusAvailableFeatures(String paramString) {
    return new ArrayMap();
  }
  
  protected boolean filterOplusFeatureFile(File paramFile) {
    return false;
  }
  
  protected boolean determineIfOplusCustomPartition(File paramFile) {
    return false;
  }
  
  protected boolean removeFeatureWithPriority(String paramString) {
    return true;
  }
  
  protected void addCustomFeature(String paramString, int paramInt, FeaturePriority paramFeaturePriority) {}
  
  protected void addCustomUnAvailableFeature(String paramString, FeaturePriority paramFeaturePriority) {}
  
  protected FeaturePriority getFeaturePriorityFromPath(String paramString) {
    return FeaturePriority.PRIORITY_SYSTEM;
  }
}
