package com.android.server;

import android.content.pm.FeatureInfo;
import android.os.Environment;
import android.os.OplusBaseEnvironment;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.util.Slog;
import android.util.Xml;
import com.android.internal.util.XmlUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import libcore.io.IoUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class OplusSystemConfig extends SystemConfig {
  private static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private static final boolean DEBUG_LOAD_FEATURE = SystemProperties.getBoolean("persist.debug.loadfeature", false);
  
  private Method readPermissionsFromXmlMethod = null;
  
  private Method readPermissionsMethod = null;
  
  private Method addFeatureMethod = null;
  
  private Method removeFeatureMethod = null;
  
  OplusSystemConfig() {
    initFiledAndMethod();
    addDynamicFeatureNotInXml();
    removeUnavailableFeature();
    readOppoCustomFeatures();
  }
  
  public ArrayMap<String, FeatureInfo> loadOplusAvailableFeatures(String paramString) {
    StringBuilder stringBuilder1, stringBuilder2;
    File file1 = Environment.buildPath(Environment.getRootDirectory(), new String[] { "etc", "oppoRegionFeatures" });
    if (!file1.exists() || !file1.isDirectory()) {
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("No directory ");
      stringBuilder1.append(file1);
      stringBuilder1.append(", skipping");
      Slog.w("SystemConfig", stringBuilder1.toString());
      return null;
    } 
    if (!file1.canRead()) {
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Directory ");
      stringBuilder1.append(file1);
      stringBuilder1.append(" cannot be read");
      Slog.w("SystemConfig", stringBuilder1.toString());
      return null;
    } 
    File file2 = null;
    File[] arrayOfFile = file1.listFiles();
    int i = arrayOfFile.length;
    byte b = 0;
    while (true) {
      file1 = file2;
      if (b < i) {
        file1 = arrayOfFile[b];
        if (file1.getPath().contains(stringBuilder1))
          break; 
        b++;
        continue;
      } 
      break;
    } 
    if (file1 == null) {
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append("path not exist : ");
      stringBuilder2.append((String)stringBuilder1);
      Slog.w("SystemConfig", stringBuilder2.toString());
      return null;
    } 
    return readOppoFeature((File)stringBuilder2);
  }
  
  private ArrayMap<String, FeatureInfo> readOppoFeature(File paramFile) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("readOppoFeature ");
      stringBuilder.append(paramFile.getPath());
      Slog.d("SystemConfig", stringBuilder.toString());
    } 
    ArrayMap<String, FeatureInfo> arrayMap = new ArrayMap();
    try {
      FileReader fileReader = new FileReader(paramFile);
      try {
        int i;
        XmlPullParser xmlPullParser = Xml.newPullParser();
        xmlPullParser.setInput(fileReader);
        while (true) {
          i = xmlPullParser.next();
          if (i != 2 && i != 1)
            continue; 
          break;
        } 
        if (i == 2) {
          while (true) {
            XmlUtils.nextElement(xmlPullParser);
            if (xmlPullParser.getEventType() == 1)
              break; 
            String str = xmlPullParser.getName();
            if ("feature".equals(str)) {
              String str1;
              str = xmlPullParser.getAttributeValue(null, "name");
              if (str == null) {
                StringBuilder stringBuilder = new StringBuilder();
                this();
                stringBuilder.append("<feature> without name in ");
                stringBuilder.append(paramFile);
                stringBuilder.append(" at ");
                stringBuilder.append(xmlPullParser.getPositionDescription());
                str1 = stringBuilder.toString();
                Slog.w("SystemConfig", str1);
              } else {
                StringBuilder stringBuilder = new StringBuilder();
                this();
                stringBuilder.append("Got feature ");
                stringBuilder.append(str1);
                Slog.i("SystemConfig", stringBuilder.toString());
                FeatureInfo featureInfo = new FeatureInfo();
                this();
                featureInfo.name = str1;
                arrayMap.put(str1, featureInfo);
              } 
              XmlUtils.skipCurrentTag(xmlPullParser);
              continue;
            } 
            XmlUtils.skipCurrentTag(xmlPullParser);
          } 
        } else {
          XmlPullParserException xmlPullParserException = new XmlPullParserException();
          this("No start tag found");
          throw xmlPullParserException;
        } 
      } catch (XmlPullParserException xmlPullParserException) {
        Slog.w("SystemConfig", "Got exception parsing permissions.", (Throwable)xmlPullParserException);
      } catch (IOException iOException) {
        Slog.w("SystemConfig", "Got exception parsing permissions.", iOException);
      } finally {}
      IoUtils.closeQuietly(fileReader);
      return arrayMap;
    } catch (FileNotFoundException fileNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Couldn't find or open permissions file ");
      stringBuilder.append(paramFile);
      Slog.w("SystemConfig", stringBuilder.toString());
      return null;
    } 
  }
  
  protected boolean filterOplusFeatureFile(File paramFile) {
    return (filterRomFeatureFile(paramFile) || filterCommonSoftFeatureFile(paramFile));
  }
  
  private boolean filterRomFeatureFile(File paramFile) {
    if (DEBUG_LOAD_FEATURE) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("filterRomFeatureFile ");
      stringBuilder.append(paramFile.getPath());
      Slog.d("SystemConfig", stringBuilder.toString());
    } 
    String str2 = SystemProperties.get("ro.rom.featrue", "allnet");
    String str1 = SystemProperties.get("ro.rom.test.featrue", "allnetcmccdeeptest");
    if (!"/system/etc/permissions/com.oppo.rom.allnetcttest.xml".equals(paramFile.getPath()) && 
      !"/system/etc/permissions/com.oppo.rom.allnetcutest.xml".equals(paramFile.getPath()) && 
      paramFile.getPath().startsWith("/system/etc/permissions/com.oppo.rom.")) {
      String str = paramFile.getPath();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str2);
      stringBuilder.append(".xml");
      if (!str.endsWith(stringBuilder.toString())) {
        str2 = paramFile.getPath();
        stringBuilder = new StringBuilder();
        stringBuilder.append(str1);
        stringBuilder.append(".xml");
        if (!str2.endsWith(stringBuilder.toString()) && 
          !"/system_ext/etc/permissions/com.oppo.rom.xml".equals(paramFile.getPath())) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("scan feature file : ");
          stringBuilder1.append(paramFile.getPath());
          stringBuilder1.append(",ignore!!!");
          Slog.i("SystemConfig", stringBuilder1.toString());
          return true;
        } 
      } 
    } 
    return false;
  }
  
  private boolean filterCommonSoftFeatureFile(File paramFile) {
    if (paramFile.getPath().startsWith("/system/etc/permissions/com.oppo.features.allnet.common.")) {
      String str = paramFile.getPath();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(SystemProperties.get("ro.commonsoft.product", "oppo"));
      stringBuilder.append(".xml");
      if (!str.endsWith(stringBuilder.toString())) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("scan feature file : ");
        stringBuilder1.append(paramFile.getPath());
        stringBuilder1.append(",ignore!!!");
        Slog.i("SystemConfig", stringBuilder1.toString());
        return true;
      } 
    } 
    return false;
  }
  
  void addDynamicFeatureNotInXml() {
    if (!this.mAvailableFeatures.containsKey("oppo.specialversion.exp.sellmode")) {
      File file = new File("/data/format_unclear/screensavers/sale_mode.fea");
      if (file.exists())
        try {
          this.addFeatureMethod.invoke(this, new Object[] { "oppo.specialversion.exp.sellmode", Integer.valueOf(0) });
        } catch (IllegalAccessException illegalAccessException) {
          illegalAccessException.printStackTrace();
        } catch (InvocationTargetException invocationTargetException) {
          invocationTargetException.printStackTrace();
        }  
    } 
  }
  
  private void removeUnavailableFeature() {
    if (SystemProperties.getBoolean("persist.sys.assert.panic.multi.user.entrance", false))
      try {
        this.removeFeatureMethod.invoke(this, new Object[] { "oppo.multiuser.entry.unsupport" });
      } catch (IllegalAccessException illegalAccessException) {
        illegalAccessException.printStackTrace();
      } catch (InvocationTargetException invocationTargetException) {
        invocationTargetException.printStackTrace();
      }  
  }
  
  private void initFiledAndMethod() {
    try {
      Method method = SystemConfig.class.getDeclaredMethod("readPermissionsFromXml", new Class[] { File.class, int.class });
      method.setAccessible(true);
      this.readPermissionsMethod = method = SystemConfig.class.getDeclaredMethod("readPermissions", new Class[] { File.class, int.class });
      method.setAccessible(true);
      this.addFeatureMethod = method = SystemConfig.class.getDeclaredMethod("addFeature", new Class[] { String.class, int.class });
      method.setAccessible(true);
      this.removeFeatureMethod = method = SystemConfig.class.getDeclaredMethod("removeFeature", new Class[] { String.class });
      method.setAccessible(true);
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("addFeatureMethod = ");
      stringBuilder.append(this.addFeatureMethod);
      stringBuilder.append(" removeFeatureMethod = ");
      stringBuilder.append(this.removeFeatureMethod);
      stringBuilder.append(" readPermissionsFromXmlMethod = ");
      stringBuilder.append(this.readPermissionsFromXmlMethod);
      Log.i("SystemConfig", stringBuilder.toString());
    } catch (NoSuchMethodException noSuchMethodException) {
      noSuchMethodException.printStackTrace();
      Log.i("SystemConfig", "NoSucheMethod");
    } 
  }
  
  private void readOppoCustomFeatures() {
    HashSet<File> hashSet = new HashSet();
    hashSet.add(OplusBaseEnvironment.getMyPreloadDirectory());
    hashSet.add(OplusBaseEnvironment.getMyHeytapDirectory());
    hashSet.add(OplusBaseEnvironment.getMyStockDirectory());
    hashSet.add(OplusBaseEnvironment.getMyProductDirectory());
    hashSet.add(OplusBaseEnvironment.getMyCountryDirectory());
    hashSet.add(OplusBaseEnvironment.getMyOperatorDirectory());
    hashSet.add(OplusBaseEnvironment.getMyCompanyDirectory());
    hashSet.add(OplusBaseEnvironment.getMyEngineeringDirectory());
    readTargetPathFeatures(hashSet, -1, new String[] { "etc", "sysconfig" });
    readTargetPathFeatures(hashSet, 21, new String[] { "etc", "permissions" });
  }
  
  private void readTargetPathFeatures(Set<File> paramSet, int paramInt, String... paramVarArgs) {
    if (paramSet != null)
      for (File file : paramSet) {
        if (DEBUG_LOAD_FEATURE) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Scaning  ");
          stringBuilder.append(file.getPath());
          stringBuilder.append(Arrays.toString((Object[])paramVarArgs));
          Slog.w("SystemConfig", stringBuilder.toString());
        } 
        try {
          if (file.exists())
            this.readPermissionsMethod.invoke(this, new Object[] { Environment.buildPath(file, paramVarArgs), Integer.valueOf(paramInt) }); 
        } catch (IllegalAccessException illegalAccessException) {
          illegalAccessException.printStackTrace();
        } catch (InvocationTargetException invocationTargetException) {
          invocationTargetException.printStackTrace();
        } 
      }  
  }
  
  protected boolean determineIfOplusCustomPartition(File paramFile) {
    int i;
    boolean bool = false;
    if (paramFile != null) {
      Path path = paramFile.toPath();
      StringBuilder stringBuilder8 = new StringBuilder();
      stringBuilder8.append(OplusBaseEnvironment.getMyPreloadDirectory().toPath());
      stringBuilder8.append("/");
      String str8 = stringBuilder8.toString();
      boolean bool1 = path.startsWith(str8);
      path = paramFile.toPath();
      StringBuilder stringBuilder7 = new StringBuilder();
      stringBuilder7.append(OplusBaseEnvironment.getMyHeytapDirectory().toPath());
      stringBuilder7.append("/");
      String str7 = stringBuilder7.toString();
      boolean bool2 = path.startsWith(str7);
      path = paramFile.toPath();
      StringBuilder stringBuilder6 = new StringBuilder();
      stringBuilder6.append(OplusBaseEnvironment.getMyStockDirectory().toPath());
      stringBuilder6.append("/");
      String str6 = stringBuilder6.toString();
      boolean bool3 = path.startsWith(str6);
      path = paramFile.toPath();
      StringBuilder stringBuilder5 = new StringBuilder();
      stringBuilder5.append(OplusBaseEnvironment.getMyProductDirectory().toPath());
      stringBuilder5.append("/");
      String str5 = stringBuilder5.toString();
      boolean bool4 = path.startsWith(str5);
      path = paramFile.toPath();
      StringBuilder stringBuilder4 = new StringBuilder();
      stringBuilder4.append(OplusBaseEnvironment.getMyCountryDirectory().toPath());
      stringBuilder4.append("/");
      String str4 = stringBuilder4.toString();
      bool = path.startsWith(str4);
      path = paramFile.toPath();
      StringBuilder stringBuilder3 = new StringBuilder();
      stringBuilder3.append(OplusBaseEnvironment.getMyOperatorDirectory().toPath());
      stringBuilder3.append("/");
      String str3 = stringBuilder3.toString();
      boolean bool5 = path.startsWith(str3);
      path = paramFile.toPath();
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append(OplusBaseEnvironment.getMyCompanyDirectory().toPath());
      stringBuilder2.append("/");
      String str2 = stringBuilder2.toString();
      boolean bool6 = path.startsWith(str2);
      path = paramFile.toPath();
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(OplusBaseEnvironment.getMyEngineeringDirectory().toPath());
      stringBuilder1.append("/");
      String str1 = stringBuilder1.toString();
      int j = false | bool1 | bool2 | bool3 | bool4 | bool | bool5 | bool6 | path.startsWith(str1);
      i = j;
      if (DEBUG_LOAD_FEATURE) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Scaning  ");
        stringBuilder.append(paramFile.toPath());
        stringBuilder.append(Boolean.toString(j));
        Slog.w("SystemConfig", stringBuilder.toString());
        i = j;
      } 
    } 
    return i;
  }
  
  static {
    HashMap<Object, Object> hashMap = new HashMap<>();
    hashMap.put(Environment.getRootDirectory().getPath(), FeaturePriority.PRIORITY_SYSTEM);
    mFeatureDirPriorityMap.put(Environment.getVendorDirectory().getPath(), FeaturePriority.PRIORITY_SYSTEM);
    mFeatureDirPriorityMap.put(Environment.getOdmDirectory().getPath(), FeaturePriority.PRIORITY_SYSTEM);
    mFeatureDirPriorityMap.put(Environment.getOemDirectory().getPath(), FeaturePriority.PRIORITY_SYSTEM);
    mFeatureDirPriorityMap.put(Environment.getProductDirectory().getPath(), FeaturePriority.PRIORITY_SYSTEM);
    mFeatureDirPriorityMap.put(Environment.getSystemExtDirectory().getPath(), FeaturePriority.PRIORITY_SYSTEM);
    mFeatureDirPriorityMap.put(OplusBaseEnvironment.getMyPreloadDirectory().getPath(), FeaturePriority.PRIORITY_STOCK);
    mFeatureDirPriorityMap.put(OplusBaseEnvironment.getMyStockDirectory().getPath(), FeaturePriority.PRIORITY_STOCK);
    mFeatureDirPriorityMap.put(OplusBaseEnvironment.getMyHeytapDirectory().getPath(), FeaturePriority.PRIORITY_STOCK);
    mFeatureDirPriorityMap.put(OplusBaseEnvironment.getMyProductDirectory().getPath(), FeaturePriority.PRIORITY_PRODUCT);
    mFeatureDirPriorityMap.put(OplusBaseEnvironment.getMyCountryDirectory().getPath(), FeaturePriority.PRIORITY_COUNTRY);
    mFeatureDirPriorityMap.put(OplusBaseEnvironment.getMyOperatorDirectory().getPath(), FeaturePriority.PRIORITY_OPERATOR);
    mFeatureDirPriorityMap.put(OplusBaseEnvironment.getMyCompanyDirectory().getPath(), FeaturePriority.PRIORITY_COMPANY);
    mFeatureDirPriorityMap.put(OplusBaseEnvironment.getMyEngineeringDirectory().getPath(), FeaturePriority.PRIORITY_ENGINEER);
  }
  
  protected class CustomFeatureInfo extends FeatureInfo {
    public FeaturePriority priority;
    
    final OplusSystemConfig this$0;
  }
  
  static final ArrayMap<String, CustomFeatureInfo> mCustomAvailableFeatures = new ArrayMap();
  
  static final ArrayMap<String, FeaturePriority> mCustomUnavailableFeatures = new ArrayMap();
  
  private static final String FEATURE_EXP_SELLMODE = "oppo.specialversion.exp.sellmode";
  
  private static final int OPPO_ALLOW_ALL = -1;
  
  private static final int OPPO_ALLOW_APP_CONFIGS = 8;
  
  private static final int OPPO_ALLOW_ASSOCIATIONS = 128;
  
  private static final int OPPO_ALLOW_FEATURES = 1;
  
  private static final int OPPO_ALLOW_HIDDENAPI_WHITELISTING = 64;
  
  private static final int OPPO_ALLOW_LIBS = 2;
  
  private static final int OPPO_ALLOW_OEM_PERMISSIONS = 32;
  
  private static final int OPPO_ALLOW_PERMISSIONS = 4;
  
  private static final int OPPO_ALLOW_PRIVAPP_PERMISSIONS = 16;
  
  private static final String PATH_SELLMODE_FLAG = "/data/format_unclear/screensavers/sale_mode.fea";
  
  private static final String PERMISSION_XML_CTTEST = "/system/etc/permissions/com.oppo.rom.allnetcttest.xml";
  
  private static final String PERMISSION_XML_CUTEST = "/system/etc/permissions/com.oppo.rom.allnetcutest.xml";
  
  private static final String PERMISSION_XML_ROM = "/system_ext/etc/permissions/com.oppo.rom.xml";
  
  private static final String PROPERTY_DEF_VALUE_ROM_FEATURE = "allnet";
  
  private static final String PROPERTY_DEF_VALUE_ROM_TEST_FEATURE = "allnetcmccdeeptest";
  
  private static final String PROPERTY_NAME_ROM_FEATURE = "ro.rom.featrue";
  
  private static final String PROPERTY_NAME_ROM_TEST_FEATURE = "ro.rom.test.featrue";
  
  private static final String TAG = "SystemConfig";
  
  protected static HashMap<String, FeaturePriority> mFeatureDirPriorityMap;
  
  protected boolean removeFeatureWithPriority(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("trying to removeFeatureWithPriority for ");
    stringBuilder.append(paramString);
    Slog.d("wbpm", stringBuilder.toString());
    CustomFeatureInfo customFeatureInfo = (CustomFeatureInfo)mCustomAvailableFeatures.get(paramString);
    if (customFeatureInfo != null) {
      StringBuilder stringBuilder1;
      FeaturePriority featurePriority = (FeaturePriority)mCustomUnavailableFeatures.get(paramString);
      if (featurePriority != null && featurePriority.compareTo(customFeatureInfo.priority) >= 0) {
        if (mCustomAvailableFeatures.remove(paramString) != null) {
          StringBuilder stringBuilder2 = new StringBuilder();
          stringBuilder2.append("Removed unavailable feature ");
          stringBuilder2.append(paramString);
          stringBuilder2.append(",priority=");
          stringBuilder2.append(customFeatureInfo.priority);
          stringBuilder2.append(", unavailablePriority=");
          stringBuilder2.append(featurePriority);
          Slog.d("wbpm", stringBuilder2.toString());
          return true;
        } 
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("mCustomAvailableFeatures.remove ");
        stringBuilder1.append(paramString);
        stringBuilder1.append(" failed");
        Slog.d("wbpm", stringBuilder1.toString());
      } else {
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("can't remove unavailable feature ");
        stringBuilder2.append(paramString);
        stringBuilder2.append(",priority=");
        stringBuilder2.append(customFeatureInfo.priority);
        stringBuilder2.append(", unavailablePriority=");
        stringBuilder2.append(stringBuilder1);
        Slog.d("wbpm", stringBuilder2.toString());
        return false;
      } 
    } else {
      stringBuilder = new StringBuilder();
      stringBuilder.append("removeFeatureWithPriority can't find featureinfo for ");
      stringBuilder.append(paramString);
      Slog.d("wbpm", stringBuilder.toString());
    } 
    return true;
  }
  
  protected void addCustomFeature(String paramString, int paramInt, FeaturePriority paramFeaturePriority) {
    CustomFeatureInfo customFeatureInfo = (CustomFeatureInfo)mCustomAvailableFeatures.get(paramString);
    if (customFeatureInfo == null) {
      customFeatureInfo = new CustomFeatureInfo();
      customFeatureInfo.name = paramString;
      customFeatureInfo.version = paramInt;
      customFeatureInfo.priority = paramFeaturePriority;
      mCustomAvailableFeatures.put(paramString, customFeatureInfo);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("addCustomFeature name=");
      stringBuilder.append(paramString);
      stringBuilder.append("/");
      stringBuilder.append(customFeatureInfo.priority);
      Slog.d("wbpm1", stringBuilder.toString());
    } else {
      customFeatureInfo.version = Math.max(customFeatureInfo.version, paramInt);
    } 
  }
  
  protected void addCustomUnAvailableFeature(String paramString, FeaturePriority paramFeaturePriority) {
    FeaturePriority featurePriority = (FeaturePriority)mCustomUnavailableFeatures.get(paramString);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("addCustomUnAvailableFeature name=");
    stringBuilder.append(paramString);
    stringBuilder.append("/");
    stringBuilder.append(paramFeaturePriority);
    stringBuilder.append(", existPriority=");
    stringBuilder.append(featurePriority);
    Slog.d("wbpm", stringBuilder.toString());
    if (featurePriority == null || paramFeaturePriority.compareTo(featurePriority) >= 0)
      mCustomUnavailableFeatures.put(paramString, paramFeaturePriority); 
  }
  
  protected FeaturePriority getFeaturePriorityFromPath(String paramString) {
    if (TextUtils.isEmpty(paramString) || !paramString.startsWith("/"))
      return FeaturePriority.PRIORITY_SYSTEM; 
    String[] arrayOfString = paramString.split("/");
    if (arrayOfString.length < 2)
      return FeaturePriority.PRIORITY_SYSTEM; 
    String str = arrayOfString[1];
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("/");
    stringBuilder.append(str);
    str = stringBuilder.toString();
    if (mFeatureDirPriorityMap.get(str) != null)
      return mFeatureDirPriorityMap.get(str); 
    return FeaturePriority.PRIORITY_SYSTEM;
  }
}
