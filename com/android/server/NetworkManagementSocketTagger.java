package com.android.server;

import android.os.StrictMode;
import android.os.SystemProperties;
import android.util.Log;
import android.util.Slog;
import dalvik.system.SocketTagger;
import java.io.FileDescriptor;
import java.net.SocketException;

public final class NetworkManagementSocketTagger extends SocketTagger {
  private static final boolean LOGD = false;
  
  public static final String PROP_QTAGUID_ENABLED = "net.qtaguid_enabled";
  
  private static final String TAG = "NetworkManagementSocketTagger";
  
  private static ThreadLocal<SocketTags> threadSocketTags = new ThreadLocal<SocketTags>() {
      protected NetworkManagementSocketTagger.SocketTags initialValue() {
        return new NetworkManagementSocketTagger.SocketTags();
      }
    };
  
  public static void install() {
    SocketTagger.set(new NetworkManagementSocketTagger());
  }
  
  public static int setThreadSocketStatsTag(int paramInt) {
    int i = ((SocketTags)threadSocketTags.get()).statsTag;
    ((SocketTags)threadSocketTags.get()).statsTag = paramInt;
    return i;
  }
  
  public static int getThreadSocketStatsTag() {
    return ((SocketTags)threadSocketTags.get()).statsTag;
  }
  
  public static int setThreadSocketStatsUid(int paramInt) {
    int i = ((SocketTags)threadSocketTags.get()).statsUid;
    ((SocketTags)threadSocketTags.get()).statsUid = paramInt;
    return i;
  }
  
  public static int getThreadSocketStatsUid() {
    return ((SocketTags)threadSocketTags.get()).statsUid;
  }
  
  public void tag(FileDescriptor paramFileDescriptor) throws SocketException {
    SocketTags socketTags = threadSocketTags.get();
    if (socketTags.statsTag == -1 && StrictMode.vmUntaggedSocketEnabled())
      StrictMode.onUntaggedSocket(); 
    tagSocketFd(paramFileDescriptor, socketTags.statsTag, socketTags.statsUid);
  }
  
  private void tagSocketFd(FileDescriptor paramFileDescriptor, int paramInt1, int paramInt2) {
    if (paramInt1 == -1 && paramInt2 == -1)
      return; 
    if (SystemProperties.getBoolean("net.qtaguid_enabled", false)) {
      int i = native_tagSocketFd(paramFileDescriptor, paramInt1, paramInt2);
      if (i < 0) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("tagSocketFd(");
        stringBuilder.append(paramFileDescriptor.getInt$());
        stringBuilder.append(", ");
        stringBuilder.append(paramInt1);
        stringBuilder.append(", ");
        stringBuilder.append(paramInt2);
        stringBuilder.append(") failed with errno");
        stringBuilder.append(i);
        Log.i("NetworkManagementSocketTagger", stringBuilder.toString());
      } 
    } 
  }
  
  public void untag(FileDescriptor paramFileDescriptor) throws SocketException {
    unTagSocketFd(paramFileDescriptor);
  }
  
  private void unTagSocketFd(FileDescriptor paramFileDescriptor) {
    SocketTags socketTags = threadSocketTags.get();
    if (socketTags.statsTag == -1 && socketTags.statsUid == -1)
      return; 
    if (SystemProperties.getBoolean("net.qtaguid_enabled", false)) {
      int i = native_untagSocketFd(paramFileDescriptor);
      if (i < 0) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("untagSocket(");
        stringBuilder.append(paramFileDescriptor.getInt$());
        stringBuilder.append(") failed with errno ");
        stringBuilder.append(i);
        Log.w("NetworkManagementSocketTagger", stringBuilder.toString());
      } 
    } 
  }
  
  public static class SocketTags {
    public int statsTag;
    
    public int statsUid;
    
    public SocketTags() {
      this.statsTag = -1;
      this.statsUid = -1;
    }
  }
  
  public static void setKernelCounterSet(int paramInt1, int paramInt2) {
    if (SystemProperties.getBoolean("net.qtaguid_enabled", false)) {
      int i = native_setCounterSet(paramInt2, paramInt1);
      if (i < 0) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("setKernelCountSet(");
        stringBuilder.append(paramInt1);
        stringBuilder.append(", ");
        stringBuilder.append(paramInt2);
        stringBuilder.append(") failed with errno ");
        stringBuilder.append(i);
        Log.w("NetworkManagementSocketTagger", stringBuilder.toString());
      } 
    } 
  }
  
  public static void resetKernelUidStats(int paramInt) {
    if (SystemProperties.getBoolean("net.qtaguid_enabled", false)) {
      int i = native_deleteTagData(0, paramInt);
      if (i < 0) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("problem clearing counters for uid ");
        stringBuilder.append(paramInt);
        stringBuilder.append(" : errno ");
        stringBuilder.append(i);
        Slog.w("NetworkManagementSocketTagger", stringBuilder.toString());
      } 
    } 
  }
  
  public static int kernelToTag(String paramString) {
    int i = paramString.length();
    if (i > 10)
      return Long.decode(paramString.substring(0, i - 8)).intValue(); 
    return 0;
  }
  
  private static native int native_deleteTagData(int paramInt1, int paramInt2);
  
  private static native int native_setCounterSet(int paramInt1, int paramInt2);
  
  private static native int native_tagSocketFd(FileDescriptor paramFileDescriptor, int paramInt1, int paramInt2);
  
  private static native int native_untagSocketFd(FileDescriptor paramFileDescriptor);
}
