package com.android.server.connectivity.metrics.nano;

import com.android.framework.protobuf.nano.CodedInputByteBufferNano;
import com.android.framework.protobuf.nano.CodedOutputByteBufferNano;
import com.android.framework.protobuf.nano.InternalNano;
import com.android.framework.protobuf.nano.InvalidProtocolBufferNanoException;
import com.android.framework.protobuf.nano.MessageNano;
import com.android.framework.protobuf.nano.WireFormatNano;
import java.io.IOException;

public interface IpConnectivityLogClass {
  public static final int BLUETOOTH = 1;
  
  public static final int CELLULAR = 2;
  
  public static final int ETHERNET = 3;
  
  public static final int LOWPAN = 9;
  
  public static final int MULTIPLE = 6;
  
  public static final int NONE = 5;
  
  public static final int UNKNOWN = 0;
  
  public static final int WIFI = 4;
  
  public static final int WIFI_NAN = 8;
  
  public static final int WIFI_P2P = 7;
  
  class NetworkId extends MessageNano {
    private static volatile NetworkId[] _emptyArray;
    
    public int networkId;
    
    public static NetworkId[] emptyArray() {
      if (_emptyArray == null)
        synchronized (InternalNano.LAZY_INIT_LOCK) {
          if (_emptyArray == null)
            _emptyArray = new NetworkId[0]; 
        }  
      return _emptyArray;
    }
    
    public NetworkId() {
      clear();
    }
    
    public NetworkId clear() {
      this.networkId = 0;
      this.cachedSize = -1;
      return this;
    }
    
    public void writeTo(CodedOutputByteBufferNano param1CodedOutputByteBufferNano) throws IOException {
      int i = this.networkId;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(1, i); 
      super.writeTo(param1CodedOutputByteBufferNano);
    }
    
    protected int computeSerializedSize() {
      int i = super.computeSerializedSize();
      int j = this.networkId, k = i;
      if (j != 0)
        k = i + CodedOutputByteBufferNano.computeInt32Size(1, j); 
      return k;
    }
    
    public NetworkId mergeFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      while (true) {
        int i = param1CodedInputByteBufferNano.readTag();
        if (i != 0) {
          if (i != 8) {
            if (!WireFormatNano.parseUnknownField(param1CodedInputByteBufferNano, i))
              return this; 
            continue;
          } 
          this.networkId = param1CodedInputByteBufferNano.readInt32();
          continue;
        } 
        break;
      } 
      return this;
    }
    
    public static NetworkId parseFrom(byte[] param1ArrayOfbyte) throws InvalidProtocolBufferNanoException {
      return (NetworkId)MessageNano.mergeFrom(new NetworkId(), param1ArrayOfbyte);
    }
    
    public static NetworkId parseFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      return (new NetworkId()).mergeFrom(param1CodedInputByteBufferNano);
    }
  }
  
  class Pair extends MessageNano {
    private static volatile Pair[] _emptyArray;
    
    public int key;
    
    public int value;
    
    public static Pair[] emptyArray() {
      if (_emptyArray == null)
        synchronized (InternalNano.LAZY_INIT_LOCK) {
          if (_emptyArray == null)
            _emptyArray = new Pair[0]; 
        }  
      return _emptyArray;
    }
    
    public Pair() {
      clear();
    }
    
    public Pair clear() {
      this.key = 0;
      this.value = 0;
      this.cachedSize = -1;
      return this;
    }
    
    public void writeTo(CodedOutputByteBufferNano param1CodedOutputByteBufferNano) throws IOException {
      int i = this.key;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(1, i); 
      i = this.value;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(2, i); 
      super.writeTo(param1CodedOutputByteBufferNano);
    }
    
    protected int computeSerializedSize() {
      int i = super.computeSerializedSize();
      int j = this.key, k = i;
      if (j != 0)
        k = i + CodedOutputByteBufferNano.computeInt32Size(1, j); 
      j = this.value;
      i = k;
      if (j != 0)
        i = k + CodedOutputByteBufferNano.computeInt32Size(2, j); 
      return i;
    }
    
    public Pair mergeFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      while (true) {
        int i = param1CodedInputByteBufferNano.readTag();
        if (i != 0) {
          if (i != 8) {
            if (i != 16) {
              if (!WireFormatNano.parseUnknownField(param1CodedInputByteBufferNano, i))
                return this; 
              continue;
            } 
            this.value = param1CodedInputByteBufferNano.readInt32();
            continue;
          } 
          this.key = param1CodedInputByteBufferNano.readInt32();
          continue;
        } 
        break;
      } 
      return this;
    }
    
    public static Pair parseFrom(byte[] param1ArrayOfbyte) throws InvalidProtocolBufferNanoException {
      return (Pair)MessageNano.mergeFrom(new Pair(), param1ArrayOfbyte);
    }
    
    public static Pair parseFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      return (new Pair()).mergeFrom(param1CodedInputByteBufferNano);
    }
  }
  
  class DefaultNetworkEvent extends MessageNano {
    public static final int DISCONNECT = 3;
    
    public static final int DUAL = 3;
    
    public static final int INVALIDATION = 2;
    
    public static final int IPV4 = 1;
    
    public static final int IPV6 = 2;
    
    public static final int NONE = 0;
    
    public static final int OUTSCORED = 1;
    
    public static final int UNKNOWN = 0;
    
    private static volatile DefaultNetworkEvent[] _emptyArray;
    
    public long defaultNetworkDurationMs;
    
    public long finalScore;
    
    public long initialScore;
    
    public int ipSupport;
    
    public IpConnectivityLogClass.NetworkId networkId;
    
    public long noDefaultNetworkDurationMs;
    
    public int previousDefaultNetworkLinkLayer;
    
    public IpConnectivityLogClass.NetworkId previousNetworkId;
    
    public int previousNetworkIpSupport;
    
    public int[] transportTypes;
    
    public long validationDurationMs;
    
    public static DefaultNetworkEvent[] emptyArray() {
      if (_emptyArray == null)
        synchronized (InternalNano.LAZY_INIT_LOCK) {
          if (_emptyArray == null)
            _emptyArray = new DefaultNetworkEvent[0]; 
        }  
      return _emptyArray;
    }
    
    public DefaultNetworkEvent() {
      clear();
    }
    
    public DefaultNetworkEvent clear() {
      this.defaultNetworkDurationMs = 0L;
      this.validationDurationMs = 0L;
      this.initialScore = 0L;
      this.finalScore = 0L;
      this.ipSupport = 0;
      this.previousDefaultNetworkLinkLayer = 0;
      this.networkId = null;
      this.previousNetworkId = null;
      this.previousNetworkIpSupport = 0;
      this.transportTypes = WireFormatNano.EMPTY_INT_ARRAY;
      this.noDefaultNetworkDurationMs = 0L;
      this.cachedSize = -1;
      return this;
    }
    
    public void writeTo(CodedOutputByteBufferNano param1CodedOutputByteBufferNano) throws IOException {
      IpConnectivityLogClass.NetworkId networkId = this.networkId;
      if (networkId != null)
        param1CodedOutputByteBufferNano.writeMessage(1, networkId); 
      networkId = this.previousNetworkId;
      if (networkId != null)
        param1CodedOutputByteBufferNano.writeMessage(2, networkId); 
      int i = this.previousNetworkIpSupport;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(3, i); 
      int[] arrayOfInt = this.transportTypes;
      if (arrayOfInt != null && arrayOfInt.length > 0) {
        i = 0;
        while (true) {
          arrayOfInt = this.transportTypes;
          if (i < arrayOfInt.length) {
            param1CodedOutputByteBufferNano.writeInt32(4, arrayOfInt[i]);
            i++;
            continue;
          } 
          break;
        } 
      } 
      long l = this.defaultNetworkDurationMs;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(5, l); 
      l = this.noDefaultNetworkDurationMs;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(6, l); 
      l = this.initialScore;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(7, l); 
      l = this.finalScore;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(8, l); 
      i = this.ipSupport;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(9, i); 
      i = this.previousDefaultNetworkLinkLayer;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(10, i); 
      l = this.validationDurationMs;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(11, l); 
      super.writeTo(param1CodedOutputByteBufferNano);
    }
    
    protected int computeSerializedSize() {
      int i = super.computeSerializedSize();
      IpConnectivityLogClass.NetworkId networkId = this.networkId;
      int j = i;
      if (networkId != null)
        j = i + CodedOutputByteBufferNano.computeMessageSize(1, networkId); 
      networkId = this.previousNetworkId;
      i = j;
      if (networkId != null)
        i = j + CodedOutputByteBufferNano.computeMessageSize(2, networkId); 
      int k = this.previousNetworkIpSupport;
      j = i;
      if (k != 0)
        j = i + CodedOutputByteBufferNano.computeInt32Size(3, k); 
      int[] arrayOfInt = this.transportTypes;
      i = j;
      if (arrayOfInt != null) {
        i = j;
        if (arrayOfInt.length > 0) {
          k = 0;
          i = 0;
          while (true) {
            arrayOfInt = this.transportTypes;
            if (i < arrayOfInt.length) {
              int m = arrayOfInt[i];
              k += CodedOutputByteBufferNano.computeInt32SizeNoTag(m);
              i++;
              continue;
            } 
            break;
          } 
          i = j + k + arrayOfInt.length * 1;
        } 
      } 
      long l = this.defaultNetworkDurationMs;
      j = i;
      if (l != 0L)
        j = i + CodedOutputByteBufferNano.computeInt64Size(5, l); 
      l = this.noDefaultNetworkDurationMs;
      i = j;
      if (l != 0L)
        i = j + CodedOutputByteBufferNano.computeInt64Size(6, l); 
      l = this.initialScore;
      j = i;
      if (l != 0L)
        j = i + CodedOutputByteBufferNano.computeInt64Size(7, l); 
      l = this.finalScore;
      i = j;
      if (l != 0L)
        i = j + CodedOutputByteBufferNano.computeInt64Size(8, l); 
      k = this.ipSupport;
      j = i;
      if (k != 0)
        j = i + CodedOutputByteBufferNano.computeInt32Size(9, k); 
      k = this.previousDefaultNetworkLinkLayer;
      i = j;
      if (k != 0)
        i = j + CodedOutputByteBufferNano.computeInt32Size(10, k); 
      l = this.validationDurationMs;
      j = i;
      if (l != 0L)
        j = i + CodedOutputByteBufferNano.computeInt64Size(11, l); 
      return j;
    }
    
    public DefaultNetworkEvent mergeFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      while (true) {
        int j, k, arrayOfInt[], i = param1CodedInputByteBufferNano.readTag();
        switch (i) {
          default:
            if (!WireFormatNano.parseUnknownField(param1CodedInputByteBufferNano, i))
              return this; 
            continue;
          case 88:
            this.validationDurationMs = param1CodedInputByteBufferNano.readInt64();
            continue;
          case 80:
            i = param1CodedInputByteBufferNano.readInt32();
            switch (i) {
              case 0:
              case 1:
              case 2:
              case 3:
              case 4:
              case 5:
              case 6:
              case 7:
              case 8:
              case 9:
                this.previousDefaultNetworkLinkLayer = i;
                break;
            } 
            continue;
          case 72:
            i = param1CodedInputByteBufferNano.readInt32();
            if (i != 0 && i != 1 && i != 2 && i != 3)
              continue; 
            this.ipSupport = i;
            continue;
          case 64:
            this.finalScore = param1CodedInputByteBufferNano.readInt64();
            continue;
          case 56:
            this.initialScore = param1CodedInputByteBufferNano.readInt64();
            continue;
          case 48:
            this.noDefaultNetworkDurationMs = param1CodedInputByteBufferNano.readInt64();
            continue;
          case 40:
            this.defaultNetworkDurationMs = param1CodedInputByteBufferNano.readInt64();
            continue;
          case 34:
            i = param1CodedInputByteBufferNano.readRawVarint32();
            j = param1CodedInputByteBufferNano.pushLimit(i);
            k = 0;
            i = param1CodedInputByteBufferNano.getPosition();
            while (param1CodedInputByteBufferNano.getBytesUntilLimit() > 0) {
              param1CodedInputByteBufferNano.readInt32();
              k++;
            } 
            param1CodedInputByteBufferNano.rewindToPosition(i);
            arrayOfInt = this.transportTypes;
            if (arrayOfInt == null) {
              i = 0;
            } else {
              i = arrayOfInt.length;
            } 
            arrayOfInt = new int[i + k];
            k = i;
            if (i != 0) {
              System.arraycopy(this.transportTypes, 0, arrayOfInt, 0, i);
              k = i;
            } 
            for (; k < arrayOfInt.length; k++)
              arrayOfInt[k] = param1CodedInputByteBufferNano.readInt32(); 
            this.transportTypes = arrayOfInt;
            param1CodedInputByteBufferNano.popLimit(j);
            continue;
          case 32:
            k = WireFormatNano.getRepeatedFieldArrayLength(param1CodedInputByteBufferNano, 32);
            arrayOfInt = this.transportTypes;
            if (arrayOfInt == null) {
              i = 0;
            } else {
              i = arrayOfInt.length;
            } 
            arrayOfInt = new int[i + k];
            k = i;
            if (i != 0) {
              System.arraycopy(this.transportTypes, 0, arrayOfInt, 0, i);
              k = i;
            } 
            for (; k < arrayOfInt.length - 1; k++) {
              arrayOfInt[k] = param1CodedInputByteBufferNano.readInt32();
              param1CodedInputByteBufferNano.readTag();
            } 
            arrayOfInt[k] = param1CodedInputByteBufferNano.readInt32();
            this.transportTypes = arrayOfInt;
            continue;
          case 24:
            i = param1CodedInputByteBufferNano.readInt32();
            if (i != 0 && i != 1 && i != 2 && i != 3)
              continue; 
            this.previousNetworkIpSupport = i;
            continue;
          case 18:
            if (this.previousNetworkId == null)
              this.previousNetworkId = new IpConnectivityLogClass.NetworkId(); 
            param1CodedInputByteBufferNano.readMessage(this.previousNetworkId);
            continue;
          case 10:
            if (this.networkId == null)
              this.networkId = new IpConnectivityLogClass.NetworkId(); 
            param1CodedInputByteBufferNano.readMessage(this.networkId);
            continue;
          case 0:
            break;
        } 
        break;
      } 
      return this;
    }
    
    public static DefaultNetworkEvent parseFrom(byte[] param1ArrayOfbyte) throws InvalidProtocolBufferNanoException {
      return (DefaultNetworkEvent)MessageNano.mergeFrom(new DefaultNetworkEvent(), param1ArrayOfbyte);
    }
    
    public static DefaultNetworkEvent parseFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      return (new DefaultNetworkEvent()).mergeFrom(param1CodedInputByteBufferNano);
    }
  }
  
  class IpReachabilityEvent extends MessageNano {
    private static volatile IpReachabilityEvent[] _emptyArray;
    
    public int eventType;
    
    public String ifName;
    
    public static IpReachabilityEvent[] emptyArray() {
      if (_emptyArray == null)
        synchronized (InternalNano.LAZY_INIT_LOCK) {
          if (_emptyArray == null)
            _emptyArray = new IpReachabilityEvent[0]; 
        }  
      return _emptyArray;
    }
    
    public IpReachabilityEvent() {
      clear();
    }
    
    public IpReachabilityEvent clear() {
      this.ifName = "";
      this.eventType = 0;
      this.cachedSize = -1;
      return this;
    }
    
    public void writeTo(CodedOutputByteBufferNano param1CodedOutputByteBufferNano) throws IOException {
      if (!this.ifName.equals(""))
        param1CodedOutputByteBufferNano.writeString(1, this.ifName); 
      int i = this.eventType;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(2, i); 
      super.writeTo(param1CodedOutputByteBufferNano);
    }
    
    protected int computeSerializedSize() {
      int i = super.computeSerializedSize();
      int j = i;
      if (!this.ifName.equals("")) {
        String str = this.ifName;
        j = i + CodedOutputByteBufferNano.computeStringSize(1, str);
      } 
      int k = this.eventType;
      i = j;
      if (k != 0)
        i = j + CodedOutputByteBufferNano.computeInt32Size(2, k); 
      return i;
    }
    
    public IpReachabilityEvent mergeFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      while (true) {
        int i = param1CodedInputByteBufferNano.readTag();
        if (i != 0) {
          if (i != 10) {
            if (i != 16) {
              if (!WireFormatNano.parseUnknownField(param1CodedInputByteBufferNano, i))
                return this; 
              continue;
            } 
            this.eventType = param1CodedInputByteBufferNano.readInt32();
            continue;
          } 
          this.ifName = param1CodedInputByteBufferNano.readString();
          continue;
        } 
        break;
      } 
      return this;
    }
    
    public static IpReachabilityEvent parseFrom(byte[] param1ArrayOfbyte) throws InvalidProtocolBufferNanoException {
      return (IpReachabilityEvent)MessageNano.mergeFrom(new IpReachabilityEvent(), param1ArrayOfbyte);
    }
    
    public static IpReachabilityEvent parseFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      return (new IpReachabilityEvent()).mergeFrom(param1CodedInputByteBufferNano);
    }
  }
  
  class NetworkEvent extends MessageNano {
    private static volatile NetworkEvent[] _emptyArray;
    
    public int eventType;
    
    public int latencyMs;
    
    public IpConnectivityLogClass.NetworkId networkId;
    
    public static NetworkEvent[] emptyArray() {
      if (_emptyArray == null)
        synchronized (InternalNano.LAZY_INIT_LOCK) {
          if (_emptyArray == null)
            _emptyArray = new NetworkEvent[0]; 
        }  
      return _emptyArray;
    }
    
    public NetworkEvent() {
      clear();
    }
    
    public NetworkEvent clear() {
      this.networkId = null;
      this.eventType = 0;
      this.latencyMs = 0;
      this.cachedSize = -1;
      return this;
    }
    
    public void writeTo(CodedOutputByteBufferNano param1CodedOutputByteBufferNano) throws IOException {
      IpConnectivityLogClass.NetworkId networkId = this.networkId;
      if (networkId != null)
        param1CodedOutputByteBufferNano.writeMessage(1, networkId); 
      int i = this.eventType;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(2, i); 
      i = this.latencyMs;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(3, i); 
      super.writeTo(param1CodedOutputByteBufferNano);
    }
    
    protected int computeSerializedSize() {
      int i = super.computeSerializedSize();
      IpConnectivityLogClass.NetworkId networkId = this.networkId;
      int j = i;
      if (networkId != null)
        j = i + CodedOutputByteBufferNano.computeMessageSize(1, networkId); 
      int k = this.eventType;
      i = j;
      if (k != 0)
        i = j + CodedOutputByteBufferNano.computeInt32Size(2, k); 
      k = this.latencyMs;
      j = i;
      if (k != 0)
        j = i + CodedOutputByteBufferNano.computeInt32Size(3, k); 
      return j;
    }
    
    public NetworkEvent mergeFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      while (true) {
        int i = param1CodedInputByteBufferNano.readTag();
        if (i != 0) {
          if (i != 10) {
            if (i != 16) {
              if (i != 24) {
                if (!WireFormatNano.parseUnknownField(param1CodedInputByteBufferNano, i))
                  return this; 
                continue;
              } 
              this.latencyMs = param1CodedInputByteBufferNano.readInt32();
              continue;
            } 
            this.eventType = param1CodedInputByteBufferNano.readInt32();
            continue;
          } 
          if (this.networkId == null)
            this.networkId = new IpConnectivityLogClass.NetworkId(); 
          param1CodedInputByteBufferNano.readMessage(this.networkId);
          continue;
        } 
        break;
      } 
      return this;
    }
    
    public static NetworkEvent parseFrom(byte[] param1ArrayOfbyte) throws InvalidProtocolBufferNanoException {
      return (NetworkEvent)MessageNano.mergeFrom(new NetworkEvent(), param1ArrayOfbyte);
    }
    
    public static NetworkEvent parseFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      return (new NetworkEvent()).mergeFrom(param1CodedInputByteBufferNano);
    }
  }
  
  class ValidationProbeEvent extends MessageNano {
    private static volatile ValidationProbeEvent[] _emptyArray;
    
    public int latencyMs;
    
    public IpConnectivityLogClass.NetworkId networkId;
    
    public int probeResult;
    
    public int probeType;
    
    public static ValidationProbeEvent[] emptyArray() {
      if (_emptyArray == null)
        synchronized (InternalNano.LAZY_INIT_LOCK) {
          if (_emptyArray == null)
            _emptyArray = new ValidationProbeEvent[0]; 
        }  
      return _emptyArray;
    }
    
    public ValidationProbeEvent() {
      clear();
    }
    
    public ValidationProbeEvent clear() {
      this.networkId = null;
      this.latencyMs = 0;
      this.probeType = 0;
      this.probeResult = 0;
      this.cachedSize = -1;
      return this;
    }
    
    public void writeTo(CodedOutputByteBufferNano param1CodedOutputByteBufferNano) throws IOException {
      IpConnectivityLogClass.NetworkId networkId = this.networkId;
      if (networkId != null)
        param1CodedOutputByteBufferNano.writeMessage(1, networkId); 
      int i = this.latencyMs;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(2, i); 
      i = this.probeType;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(3, i); 
      i = this.probeResult;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(4, i); 
      super.writeTo(param1CodedOutputByteBufferNano);
    }
    
    protected int computeSerializedSize() {
      int i = super.computeSerializedSize();
      IpConnectivityLogClass.NetworkId networkId = this.networkId;
      int j = i;
      if (networkId != null)
        j = i + CodedOutputByteBufferNano.computeMessageSize(1, networkId); 
      int k = this.latencyMs;
      i = j;
      if (k != 0)
        i = j + CodedOutputByteBufferNano.computeInt32Size(2, k); 
      k = this.probeType;
      j = i;
      if (k != 0)
        j = i + CodedOutputByteBufferNano.computeInt32Size(3, k); 
      k = this.probeResult;
      i = j;
      if (k != 0)
        i = j + CodedOutputByteBufferNano.computeInt32Size(4, k); 
      return i;
    }
    
    public ValidationProbeEvent mergeFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      while (true) {
        int i = param1CodedInputByteBufferNano.readTag();
        if (i != 0) {
          if (i != 10) {
            if (i != 16) {
              if (i != 24) {
                if (i != 32) {
                  if (!WireFormatNano.parseUnknownField(param1CodedInputByteBufferNano, i))
                    return this; 
                  continue;
                } 
                this.probeResult = param1CodedInputByteBufferNano.readInt32();
                continue;
              } 
              this.probeType = param1CodedInputByteBufferNano.readInt32();
              continue;
            } 
            this.latencyMs = param1CodedInputByteBufferNano.readInt32();
            continue;
          } 
          if (this.networkId == null)
            this.networkId = new IpConnectivityLogClass.NetworkId(); 
          param1CodedInputByteBufferNano.readMessage(this.networkId);
          continue;
        } 
        break;
      } 
      return this;
    }
    
    public static ValidationProbeEvent parseFrom(byte[] param1ArrayOfbyte) throws InvalidProtocolBufferNanoException {
      return (ValidationProbeEvent)MessageNano.mergeFrom(new ValidationProbeEvent(), param1ArrayOfbyte);
    }
    
    public static ValidationProbeEvent parseFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      return (new ValidationProbeEvent()).mergeFrom(param1CodedInputByteBufferNano);
    }
  }
  
  class DNSLookupBatch extends MessageNano {
    private static volatile DNSLookupBatch[] _emptyArray;
    
    public int[] eventTypes;
    
    public long getaddrinfoErrorCount;
    
    public IpConnectivityLogClass.Pair[] getaddrinfoErrors;
    
    public long getaddrinfoQueryCount;
    
    public long gethostbynameErrorCount;
    
    public IpConnectivityLogClass.Pair[] gethostbynameErrors;
    
    public long gethostbynameQueryCount;
    
    public int[] latenciesMs;
    
    public IpConnectivityLogClass.NetworkId networkId;
    
    public int[] returnCodes;
    
    public static DNSLookupBatch[] emptyArray() {
      if (_emptyArray == null)
        synchronized (InternalNano.LAZY_INIT_LOCK) {
          if (_emptyArray == null)
            _emptyArray = new DNSLookupBatch[0]; 
        }  
      return _emptyArray;
    }
    
    public DNSLookupBatch() {
      clear();
    }
    
    public DNSLookupBatch clear() {
      this.latenciesMs = WireFormatNano.EMPTY_INT_ARRAY;
      this.getaddrinfoQueryCount = 0L;
      this.gethostbynameQueryCount = 0L;
      this.getaddrinfoErrorCount = 0L;
      this.gethostbynameErrorCount = 0L;
      this.getaddrinfoErrors = IpConnectivityLogClass.Pair.emptyArray();
      this.gethostbynameErrors = IpConnectivityLogClass.Pair.emptyArray();
      this.networkId = null;
      this.eventTypes = WireFormatNano.EMPTY_INT_ARRAY;
      this.returnCodes = WireFormatNano.EMPTY_INT_ARRAY;
      this.cachedSize = -1;
      return this;
    }
    
    public void writeTo(CodedOutputByteBufferNano param1CodedOutputByteBufferNano) throws IOException {
      IpConnectivityLogClass.NetworkId networkId = this.networkId;
      if (networkId != null)
        param1CodedOutputByteBufferNano.writeMessage(1, networkId); 
      int[] arrayOfInt = this.eventTypes;
      if (arrayOfInt != null && arrayOfInt.length > 0) {
        byte b = 0;
        while (true) {
          arrayOfInt = this.eventTypes;
          if (b < arrayOfInt.length) {
            param1CodedOutputByteBufferNano.writeInt32(2, arrayOfInt[b]);
            b++;
            continue;
          } 
          break;
        } 
      } 
      arrayOfInt = this.returnCodes;
      if (arrayOfInt != null && arrayOfInt.length > 0) {
        byte b = 0;
        while (true) {
          arrayOfInt = this.returnCodes;
          if (b < arrayOfInt.length) {
            param1CodedOutputByteBufferNano.writeInt32(3, arrayOfInt[b]);
            b++;
            continue;
          } 
          break;
        } 
      } 
      arrayOfInt = this.latenciesMs;
      if (arrayOfInt != null && arrayOfInt.length > 0) {
        byte b = 0;
        while (true) {
          arrayOfInt = this.latenciesMs;
          if (b < arrayOfInt.length) {
            param1CodedOutputByteBufferNano.writeInt32(4, arrayOfInt[b]);
            b++;
            continue;
          } 
          break;
        } 
      } 
      long l = this.getaddrinfoQueryCount;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(5, l); 
      l = this.gethostbynameQueryCount;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(6, l); 
      l = this.getaddrinfoErrorCount;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(7, l); 
      l = this.gethostbynameErrorCount;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(8, l); 
      IpConnectivityLogClass.Pair[] arrayOfPair = this.getaddrinfoErrors;
      if (arrayOfPair != null && arrayOfPair.length > 0) {
        byte b = 0;
        while (true) {
          arrayOfPair = this.getaddrinfoErrors;
          if (b < arrayOfPair.length) {
            IpConnectivityLogClass.Pair pair = arrayOfPair[b];
            if (pair != null)
              param1CodedOutputByteBufferNano.writeMessage(9, pair); 
            b++;
            continue;
          } 
          break;
        } 
      } 
      arrayOfPair = this.gethostbynameErrors;
      if (arrayOfPair != null && arrayOfPair.length > 0) {
        byte b = 0;
        while (true) {
          arrayOfPair = this.gethostbynameErrors;
          if (b < arrayOfPair.length) {
            IpConnectivityLogClass.Pair pair = arrayOfPair[b];
            if (pair != null)
              param1CodedOutputByteBufferNano.writeMessage(10, pair); 
            b++;
            continue;
          } 
          break;
        } 
      } 
      super.writeTo(param1CodedOutputByteBufferNano);
    }
    
    protected int computeSerializedSize() {
      int i = super.computeSerializedSize();
      IpConnectivityLogClass.NetworkId networkId = this.networkId;
      int j = i;
      if (networkId != null)
        j = i + CodedOutputByteBufferNano.computeMessageSize(1, networkId); 
      int[] arrayOfInt = this.eventTypes;
      i = j;
      if (arrayOfInt != null) {
        i = j;
        if (arrayOfInt.length > 0) {
          int m = 0;
          i = 0;
          while (true) {
            arrayOfInt = this.eventTypes;
            if (i < arrayOfInt.length) {
              int n = arrayOfInt[i];
              m += CodedOutputByteBufferNano.computeInt32SizeNoTag(n);
              i++;
              continue;
            } 
            break;
          } 
          i = j + m + arrayOfInt.length * 1;
        } 
      } 
      arrayOfInt = this.returnCodes;
      j = i;
      if (arrayOfInt != null) {
        j = i;
        if (arrayOfInt.length > 0) {
          int m = 0;
          j = 0;
          while (true) {
            arrayOfInt = this.returnCodes;
            if (j < arrayOfInt.length) {
              int n = arrayOfInt[j];
              m += CodedOutputByteBufferNano.computeInt32SizeNoTag(n);
              j++;
              continue;
            } 
            break;
          } 
          j = i + m + arrayOfInt.length * 1;
        } 
      } 
      arrayOfInt = this.latenciesMs;
      int k = j;
      if (arrayOfInt != null) {
        k = j;
        if (arrayOfInt.length > 0) {
          k = 0;
          i = 0;
          while (true) {
            arrayOfInt = this.latenciesMs;
            if (i < arrayOfInt.length) {
              int m = arrayOfInt[i];
              k += CodedOutputByteBufferNano.computeInt32SizeNoTag(m);
              i++;
              continue;
            } 
            break;
          } 
          k = j + k + arrayOfInt.length * 1;
        } 
      } 
      long l = this.getaddrinfoQueryCount;
      i = k;
      if (l != 0L)
        i = k + CodedOutputByteBufferNano.computeInt64Size(5, l); 
      l = this.gethostbynameQueryCount;
      j = i;
      if (l != 0L)
        j = i + CodedOutputByteBufferNano.computeInt64Size(6, l); 
      l = this.getaddrinfoErrorCount;
      k = j;
      if (l != 0L)
        k = j + CodedOutputByteBufferNano.computeInt64Size(7, l); 
      l = this.gethostbynameErrorCount;
      i = k;
      if (l != 0L)
        i = k + CodedOutputByteBufferNano.computeInt64Size(8, l); 
      IpConnectivityLogClass.Pair[] arrayOfPair = this.getaddrinfoErrors;
      j = i;
      if (arrayOfPair != null) {
        j = i;
        if (arrayOfPair.length > 0) {
          k = 0;
          while (true) {
            arrayOfPair = this.getaddrinfoErrors;
            j = i;
            if (k < arrayOfPair.length) {
              IpConnectivityLogClass.Pair pair = arrayOfPair[k];
              j = i;
              if (pair != null)
                j = i + CodedOutputByteBufferNano.computeMessageSize(9, pair); 
              k++;
              i = j;
              continue;
            } 
            break;
          } 
        } 
      } 
      arrayOfPair = this.gethostbynameErrors;
      k = j;
      if (arrayOfPair != null) {
        k = j;
        if (arrayOfPair.length > 0) {
          i = 0;
          while (true) {
            arrayOfPair = this.gethostbynameErrors;
            k = j;
            if (i < arrayOfPair.length) {
              IpConnectivityLogClass.Pair pair = arrayOfPair[i];
              k = j;
              if (pair != null)
                k = j + CodedOutputByteBufferNano.computeMessageSize(10, pair); 
              i++;
              j = k;
              continue;
            } 
            break;
          } 
        } 
      } 
      return k;
    }
    
    public DNSLookupBatch mergeFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      while (true) {
        int j;
        IpConnectivityLogClass.Pair[] arrayOfPair;
        int arrayOfInt[], k, i = param1CodedInputByteBufferNano.readTag();
        switch (i) {
          default:
            if (!WireFormatNano.parseUnknownField(param1CodedInputByteBufferNano, i))
              return this; 
            continue;
          case 82:
            j = WireFormatNano.getRepeatedFieldArrayLength(param1CodedInputByteBufferNano, 82);
            arrayOfPair = this.gethostbynameErrors;
            if (arrayOfPair == null) {
              i = 0;
            } else {
              i = arrayOfPair.length;
            } 
            arrayOfPair = new IpConnectivityLogClass.Pair[i + j];
            j = i;
            if (i != 0) {
              System.arraycopy(this.gethostbynameErrors, 0, arrayOfPair, 0, i);
              j = i;
            } 
            for (; j < arrayOfPair.length - 1; j++) {
              arrayOfPair[j] = new IpConnectivityLogClass.Pair();
              param1CodedInputByteBufferNano.readMessage(arrayOfPair[j]);
              param1CodedInputByteBufferNano.readTag();
            } 
            arrayOfPair[j] = new IpConnectivityLogClass.Pair();
            param1CodedInputByteBufferNano.readMessage(arrayOfPair[j]);
            this.gethostbynameErrors = arrayOfPair;
            continue;
          case 74:
            j = WireFormatNano.getRepeatedFieldArrayLength(param1CodedInputByteBufferNano, 74);
            arrayOfPair = this.getaddrinfoErrors;
            if (arrayOfPair == null) {
              i = 0;
            } else {
              i = arrayOfPair.length;
            } 
            arrayOfPair = new IpConnectivityLogClass.Pair[i + j];
            j = i;
            if (i != 0) {
              System.arraycopy(this.getaddrinfoErrors, 0, arrayOfPair, 0, i);
              j = i;
            } 
            for (; j < arrayOfPair.length - 1; j++) {
              arrayOfPair[j] = new IpConnectivityLogClass.Pair();
              param1CodedInputByteBufferNano.readMessage(arrayOfPair[j]);
              param1CodedInputByteBufferNano.readTag();
            } 
            arrayOfPair[j] = new IpConnectivityLogClass.Pair();
            param1CodedInputByteBufferNano.readMessage(arrayOfPair[j]);
            this.getaddrinfoErrors = arrayOfPair;
            continue;
          case 64:
            this.gethostbynameErrorCount = param1CodedInputByteBufferNano.readInt64();
            continue;
          case 56:
            this.getaddrinfoErrorCount = param1CodedInputByteBufferNano.readInt64();
            continue;
          case 48:
            this.gethostbynameQueryCount = param1CodedInputByteBufferNano.readInt64();
            continue;
          case 40:
            this.getaddrinfoQueryCount = param1CodedInputByteBufferNano.readInt64();
            continue;
          case 34:
            i = param1CodedInputByteBufferNano.readRawVarint32();
            k = param1CodedInputByteBufferNano.pushLimit(i);
            j = 0;
            i = param1CodedInputByteBufferNano.getPosition();
            while (param1CodedInputByteBufferNano.getBytesUntilLimit() > 0) {
              param1CodedInputByteBufferNano.readInt32();
              j++;
            } 
            param1CodedInputByteBufferNano.rewindToPosition(i);
            arrayOfInt = this.latenciesMs;
            if (arrayOfInt == null) {
              i = 0;
            } else {
              i = arrayOfInt.length;
            } 
            arrayOfInt = new int[i + j];
            j = i;
            if (i != 0) {
              System.arraycopy(this.latenciesMs, 0, arrayOfInt, 0, i);
              j = i;
            } 
            for (; j < arrayOfInt.length; j++)
              arrayOfInt[j] = param1CodedInputByteBufferNano.readInt32(); 
            this.latenciesMs = arrayOfInt;
            param1CodedInputByteBufferNano.popLimit(k);
            continue;
          case 32:
            j = WireFormatNano.getRepeatedFieldArrayLength(param1CodedInputByteBufferNano, 32);
            arrayOfInt = this.latenciesMs;
            if (arrayOfInt == null) {
              i = 0;
            } else {
              i = arrayOfInt.length;
            } 
            arrayOfInt = new int[i + j];
            j = i;
            if (i != 0) {
              System.arraycopy(this.latenciesMs, 0, arrayOfInt, 0, i);
              j = i;
            } 
            for (; j < arrayOfInt.length - 1; j++) {
              arrayOfInt[j] = param1CodedInputByteBufferNano.readInt32();
              param1CodedInputByteBufferNano.readTag();
            } 
            arrayOfInt[j] = param1CodedInputByteBufferNano.readInt32();
            this.latenciesMs = arrayOfInt;
            continue;
          case 26:
            i = param1CodedInputByteBufferNano.readRawVarint32();
            k = param1CodedInputByteBufferNano.pushLimit(i);
            j = 0;
            i = param1CodedInputByteBufferNano.getPosition();
            while (param1CodedInputByteBufferNano.getBytesUntilLimit() > 0) {
              param1CodedInputByteBufferNano.readInt32();
              j++;
            } 
            param1CodedInputByteBufferNano.rewindToPosition(i);
            arrayOfInt = this.returnCodes;
            if (arrayOfInt == null) {
              i = 0;
            } else {
              i = arrayOfInt.length;
            } 
            arrayOfInt = new int[i + j];
            j = i;
            if (i != 0) {
              System.arraycopy(this.returnCodes, 0, arrayOfInt, 0, i);
              j = i;
            } 
            for (; j < arrayOfInt.length; j++)
              arrayOfInt[j] = param1CodedInputByteBufferNano.readInt32(); 
            this.returnCodes = arrayOfInt;
            param1CodedInputByteBufferNano.popLimit(k);
            continue;
          case 24:
            j = WireFormatNano.getRepeatedFieldArrayLength(param1CodedInputByteBufferNano, 24);
            arrayOfInt = this.returnCodes;
            if (arrayOfInt == null) {
              i = 0;
            } else {
              i = arrayOfInt.length;
            } 
            arrayOfInt = new int[i + j];
            j = i;
            if (i != 0) {
              System.arraycopy(this.returnCodes, 0, arrayOfInt, 0, i);
              j = i;
            } 
            for (; j < arrayOfInt.length - 1; j++) {
              arrayOfInt[j] = param1CodedInputByteBufferNano.readInt32();
              param1CodedInputByteBufferNano.readTag();
            } 
            arrayOfInt[j] = param1CodedInputByteBufferNano.readInt32();
            this.returnCodes = arrayOfInt;
            continue;
          case 18:
            i = param1CodedInputByteBufferNano.readRawVarint32();
            k = param1CodedInputByteBufferNano.pushLimit(i);
            j = 0;
            i = param1CodedInputByteBufferNano.getPosition();
            while (param1CodedInputByteBufferNano.getBytesUntilLimit() > 0) {
              param1CodedInputByteBufferNano.readInt32();
              j++;
            } 
            param1CodedInputByteBufferNano.rewindToPosition(i);
            arrayOfInt = this.eventTypes;
            if (arrayOfInt == null) {
              i = 0;
            } else {
              i = arrayOfInt.length;
            } 
            arrayOfInt = new int[i + j];
            j = i;
            if (i != 0) {
              System.arraycopy(this.eventTypes, 0, arrayOfInt, 0, i);
              j = i;
            } 
            for (; j < arrayOfInt.length; j++)
              arrayOfInt[j] = param1CodedInputByteBufferNano.readInt32(); 
            this.eventTypes = arrayOfInt;
            param1CodedInputByteBufferNano.popLimit(k);
            continue;
          case 16:
            j = WireFormatNano.getRepeatedFieldArrayLength(param1CodedInputByteBufferNano, 16);
            arrayOfInt = this.eventTypes;
            if (arrayOfInt == null) {
              i = 0;
            } else {
              i = arrayOfInt.length;
            } 
            arrayOfInt = new int[i + j];
            j = i;
            if (i != 0) {
              System.arraycopy(this.eventTypes, 0, arrayOfInt, 0, i);
              j = i;
            } 
            for (; j < arrayOfInt.length - 1; j++) {
              arrayOfInt[j] = param1CodedInputByteBufferNano.readInt32();
              param1CodedInputByteBufferNano.readTag();
            } 
            arrayOfInt[j] = param1CodedInputByteBufferNano.readInt32();
            this.eventTypes = arrayOfInt;
            continue;
          case 10:
            if (this.networkId == null)
              this.networkId = new IpConnectivityLogClass.NetworkId(); 
            param1CodedInputByteBufferNano.readMessage(this.networkId);
            continue;
          case 0:
            break;
        } 
        break;
      } 
      return this;
    }
    
    public static DNSLookupBatch parseFrom(byte[] param1ArrayOfbyte) throws InvalidProtocolBufferNanoException {
      return (DNSLookupBatch)MessageNano.mergeFrom(new DNSLookupBatch(), param1ArrayOfbyte);
    }
    
    public static DNSLookupBatch parseFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      return (new DNSLookupBatch()).mergeFrom(param1CodedInputByteBufferNano);
    }
  }
  
  class DNSLatencies extends MessageNano {
    private static volatile DNSLatencies[] _emptyArray;
    
    public int aCount;
    
    public int aaaaCount;
    
    public int[] latenciesMs;
    
    public int queryCount;
    
    public int returnCode;
    
    public int type;
    
    public static DNSLatencies[] emptyArray() {
      if (_emptyArray == null)
        synchronized (InternalNano.LAZY_INIT_LOCK) {
          if (_emptyArray == null)
            _emptyArray = new DNSLatencies[0]; 
        }  
      return _emptyArray;
    }
    
    public DNSLatencies() {
      clear();
    }
    
    public DNSLatencies clear() {
      this.type = 0;
      this.returnCode = 0;
      this.queryCount = 0;
      this.aCount = 0;
      this.aaaaCount = 0;
      this.latenciesMs = WireFormatNano.EMPTY_INT_ARRAY;
      this.cachedSize = -1;
      return this;
    }
    
    public void writeTo(CodedOutputByteBufferNano param1CodedOutputByteBufferNano) throws IOException {
      int i = this.type;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(1, i); 
      i = this.returnCode;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(2, i); 
      i = this.queryCount;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(3, i); 
      i = this.aCount;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(4, i); 
      i = this.aaaaCount;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(5, i); 
      int[] arrayOfInt = this.latenciesMs;
      if (arrayOfInt != null && arrayOfInt.length > 0) {
        i = 0;
        while (true) {
          arrayOfInt = this.latenciesMs;
          if (i < arrayOfInt.length) {
            param1CodedOutputByteBufferNano.writeInt32(6, arrayOfInt[i]);
            i++;
            continue;
          } 
          break;
        } 
      } 
      super.writeTo(param1CodedOutputByteBufferNano);
    }
    
    protected int computeSerializedSize() {
      int i = super.computeSerializedSize();
      int j = this.type, k = i;
      if (j != 0)
        k = i + CodedOutputByteBufferNano.computeInt32Size(1, j); 
      j = this.returnCode;
      i = k;
      if (j != 0)
        i = k + CodedOutputByteBufferNano.computeInt32Size(2, j); 
      k = this.queryCount;
      j = i;
      if (k != 0)
        j = i + CodedOutputByteBufferNano.computeInt32Size(3, k); 
      i = this.aCount;
      k = j;
      if (i != 0)
        k = j + CodedOutputByteBufferNano.computeInt32Size(4, i); 
      j = this.aaaaCount;
      i = k;
      if (j != 0)
        i = k + CodedOutputByteBufferNano.computeInt32Size(5, j); 
      int[] arrayOfInt = this.latenciesMs;
      k = i;
      if (arrayOfInt != null) {
        k = i;
        if (arrayOfInt.length > 0) {
          j = 0;
          k = 0;
          while (true) {
            arrayOfInt = this.latenciesMs;
            if (k < arrayOfInt.length) {
              int m = arrayOfInt[k];
              j += CodedOutputByteBufferNano.computeInt32SizeNoTag(m);
              k++;
              continue;
            } 
            break;
          } 
          k = i + j + arrayOfInt.length * 1;
        } 
      } 
      return k;
    }
    
    public DNSLatencies mergeFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      while (true) {
        int i = param1CodedInputByteBufferNano.readTag();
        if (i != 0) {
          if (i != 8) {
            if (i != 16) {
              if (i != 24) {
                if (i != 32) {
                  if (i != 40) {
                    if (i != 48) {
                      if (i != 50) {
                        if (!WireFormatNano.parseUnknownField(param1CodedInputByteBufferNano, i))
                          return this; 
                        continue;
                      } 
                      i = param1CodedInputByteBufferNano.readRawVarint32();
                      int k = param1CodedInputByteBufferNano.pushLimit(i);
                      int m = 0;
                      i = param1CodedInputByteBufferNano.getPosition();
                      while (param1CodedInputByteBufferNano.getBytesUntilLimit() > 0) {
                        param1CodedInputByteBufferNano.readInt32();
                        m++;
                      } 
                      param1CodedInputByteBufferNano.rewindToPosition(i);
                      int[] arrayOfInt1 = this.latenciesMs;
                      if (arrayOfInt1 == null) {
                        i = 0;
                      } else {
                        i = arrayOfInt1.length;
                      } 
                      arrayOfInt1 = new int[i + m];
                      m = i;
                      if (i != 0) {
                        System.arraycopy(this.latenciesMs, 0, arrayOfInt1, 0, i);
                        m = i;
                      } 
                      for (; m < arrayOfInt1.length; m++)
                        arrayOfInt1[m] = param1CodedInputByteBufferNano.readInt32(); 
                      this.latenciesMs = arrayOfInt1;
                      param1CodedInputByteBufferNano.popLimit(k);
                      continue;
                    } 
                    int j = WireFormatNano.getRepeatedFieldArrayLength(param1CodedInputByteBufferNano, 48);
                    int[] arrayOfInt = this.latenciesMs;
                    if (arrayOfInt == null) {
                      i = 0;
                    } else {
                      i = arrayOfInt.length;
                    } 
                    arrayOfInt = new int[i + j];
                    j = i;
                    if (i != 0) {
                      System.arraycopy(this.latenciesMs, 0, arrayOfInt, 0, i);
                      j = i;
                    } 
                    for (; j < arrayOfInt.length - 1; j++) {
                      arrayOfInt[j] = param1CodedInputByteBufferNano.readInt32();
                      param1CodedInputByteBufferNano.readTag();
                    } 
                    arrayOfInt[j] = param1CodedInputByteBufferNano.readInt32();
                    this.latenciesMs = arrayOfInt;
                    continue;
                  } 
                  this.aaaaCount = param1CodedInputByteBufferNano.readInt32();
                  continue;
                } 
                this.aCount = param1CodedInputByteBufferNano.readInt32();
                continue;
              } 
              this.queryCount = param1CodedInputByteBufferNano.readInt32();
              continue;
            } 
            this.returnCode = param1CodedInputByteBufferNano.readInt32();
            continue;
          } 
          this.type = param1CodedInputByteBufferNano.readInt32();
          continue;
        } 
        break;
      } 
      return this;
    }
    
    public static DNSLatencies parseFrom(byte[] param1ArrayOfbyte) throws InvalidProtocolBufferNanoException {
      return (DNSLatencies)MessageNano.mergeFrom(new DNSLatencies(), param1ArrayOfbyte);
    }
    
    public static DNSLatencies parseFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      return (new DNSLatencies()).mergeFrom(param1CodedInputByteBufferNano);
    }
  }
  
  class ConnectStatistics extends MessageNano {
    private static volatile ConnectStatistics[] _emptyArray;
    
    public int connectBlockingCount;
    
    public int connectCount;
    
    public IpConnectivityLogClass.Pair[] errnosCounters;
    
    public int ipv6AddrCount;
    
    public int[] latenciesMs;
    
    public int[] nonBlockingLatenciesMs;
    
    public static ConnectStatistics[] emptyArray() {
      if (_emptyArray == null)
        synchronized (InternalNano.LAZY_INIT_LOCK) {
          if (_emptyArray == null)
            _emptyArray = new ConnectStatistics[0]; 
        }  
      return _emptyArray;
    }
    
    public ConnectStatistics() {
      clear();
    }
    
    public ConnectStatistics clear() {
      this.connectCount = 0;
      this.connectBlockingCount = 0;
      this.ipv6AddrCount = 0;
      this.latenciesMs = WireFormatNano.EMPTY_INT_ARRAY;
      this.nonBlockingLatenciesMs = WireFormatNano.EMPTY_INT_ARRAY;
      this.errnosCounters = IpConnectivityLogClass.Pair.emptyArray();
      this.cachedSize = -1;
      return this;
    }
    
    public void writeTo(CodedOutputByteBufferNano param1CodedOutputByteBufferNano) throws IOException {
      int i = this.connectCount;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(1, i); 
      i = this.ipv6AddrCount;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(2, i); 
      int[] arrayOfInt2 = this.latenciesMs;
      if (arrayOfInt2 != null && arrayOfInt2.length > 0) {
        i = 0;
        while (true) {
          arrayOfInt2 = this.latenciesMs;
          if (i < arrayOfInt2.length) {
            param1CodedOutputByteBufferNano.writeInt32(3, arrayOfInt2[i]);
            i++;
            continue;
          } 
          break;
        } 
      } 
      IpConnectivityLogClass.Pair[] arrayOfPair = this.errnosCounters;
      if (arrayOfPair != null && arrayOfPair.length > 0) {
        i = 0;
        while (true) {
          arrayOfPair = this.errnosCounters;
          if (i < arrayOfPair.length) {
            IpConnectivityLogClass.Pair pair = arrayOfPair[i];
            if (pair != null)
              param1CodedOutputByteBufferNano.writeMessage(4, pair); 
            i++;
            continue;
          } 
          break;
        } 
      } 
      i = this.connectBlockingCount;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(5, i); 
      int[] arrayOfInt1 = this.nonBlockingLatenciesMs;
      if (arrayOfInt1 != null && arrayOfInt1.length > 0) {
        i = 0;
        while (true) {
          arrayOfInt1 = this.nonBlockingLatenciesMs;
          if (i < arrayOfInt1.length) {
            param1CodedOutputByteBufferNano.writeInt32(6, arrayOfInt1[i]);
            i++;
            continue;
          } 
          break;
        } 
      } 
      super.writeTo(param1CodedOutputByteBufferNano);
    }
    
    protected int computeSerializedSize() {
      int i = super.computeSerializedSize();
      int j = this.connectCount, k = i;
      if (j != 0)
        k = i + CodedOutputByteBufferNano.computeInt32Size(1, j); 
      j = this.ipv6AddrCount;
      i = k;
      if (j != 0)
        i = k + CodedOutputByteBufferNano.computeInt32Size(2, j); 
      int[] arrayOfInt2 = this.latenciesMs;
      k = i;
      if (arrayOfInt2 != null) {
        k = i;
        if (arrayOfInt2.length > 0) {
          j = 0;
          k = 0;
          while (true) {
            arrayOfInt2 = this.latenciesMs;
            if (k < arrayOfInt2.length) {
              int m = arrayOfInt2[k];
              j += CodedOutputByteBufferNano.computeInt32SizeNoTag(m);
              k++;
              continue;
            } 
            break;
          } 
          k = i + j + arrayOfInt2.length * 1;
        } 
      } 
      IpConnectivityLogClass.Pair[] arrayOfPair = this.errnosCounters;
      i = k;
      if (arrayOfPair != null) {
        i = k;
        if (arrayOfPair.length > 0) {
          j = 0;
          while (true) {
            arrayOfPair = this.errnosCounters;
            i = k;
            if (j < arrayOfPair.length) {
              IpConnectivityLogClass.Pair pair = arrayOfPair[j];
              i = k;
              if (pair != null)
                i = k + CodedOutputByteBufferNano.computeMessageSize(4, pair); 
              j++;
              k = i;
              continue;
            } 
            break;
          } 
        } 
      } 
      j = this.connectBlockingCount;
      k = i;
      if (j != 0)
        k = i + CodedOutputByteBufferNano.computeInt32Size(5, j); 
      int[] arrayOfInt1 = this.nonBlockingLatenciesMs;
      i = k;
      if (arrayOfInt1 != null) {
        i = k;
        if (arrayOfInt1.length > 0) {
          j = 0;
          i = 0;
          while (true) {
            arrayOfInt1 = this.nonBlockingLatenciesMs;
            if (i < arrayOfInt1.length) {
              int m = arrayOfInt1[i];
              j += CodedOutputByteBufferNano.computeInt32SizeNoTag(m);
              i++;
              continue;
            } 
            break;
          } 
          i = k + j + arrayOfInt1.length * 1;
        } 
      } 
      return i;
    }
    
    public ConnectStatistics mergeFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      while (true) {
        int i = param1CodedInputByteBufferNano.readTag();
        if (i != 0) {
          if (i != 8) {
            if (i != 16) {
              if (i != 24) {
                if (i != 26) {
                  if (i != 34) {
                    if (i != 40) {
                      if (i != 48) {
                        if (i != 50) {
                          if (!WireFormatNano.parseUnknownField(param1CodedInputByteBufferNano, i))
                            return this; 
                          continue;
                        } 
                        i = param1CodedInputByteBufferNano.readRawVarint32();
                        int i2 = param1CodedInputByteBufferNano.pushLimit(i);
                        int i3 = 0;
                        i = param1CodedInputByteBufferNano.getPosition();
                        while (param1CodedInputByteBufferNano.getBytesUntilLimit() > 0) {
                          param1CodedInputByteBufferNano.readInt32();
                          i3++;
                        } 
                        param1CodedInputByteBufferNano.rewindToPosition(i);
                        int[] arrayOfInt3 = this.nonBlockingLatenciesMs;
                        if (arrayOfInt3 == null) {
                          i = 0;
                        } else {
                          i = arrayOfInt3.length;
                        } 
                        arrayOfInt3 = new int[i + i3];
                        i3 = i;
                        if (i != 0) {
                          System.arraycopy(this.nonBlockingLatenciesMs, 0, arrayOfInt3, 0, i);
                          i3 = i;
                        } 
                        for (; i3 < arrayOfInt3.length; i3++)
                          arrayOfInt3[i3] = param1CodedInputByteBufferNano.readInt32(); 
                        this.nonBlockingLatenciesMs = arrayOfInt3;
                        param1CodedInputByteBufferNano.popLimit(i2);
                        continue;
                      } 
                      int i1 = WireFormatNano.getRepeatedFieldArrayLength(param1CodedInputByteBufferNano, 48);
                      int[] arrayOfInt2 = this.nonBlockingLatenciesMs;
                      if (arrayOfInt2 == null) {
                        i = 0;
                      } else {
                        i = arrayOfInt2.length;
                      } 
                      arrayOfInt2 = new int[i + i1];
                      i1 = i;
                      if (i != 0) {
                        System.arraycopy(this.nonBlockingLatenciesMs, 0, arrayOfInt2, 0, i);
                        i1 = i;
                      } 
                      for (; i1 < arrayOfInt2.length - 1; i1++) {
                        arrayOfInt2[i1] = param1CodedInputByteBufferNano.readInt32();
                        param1CodedInputByteBufferNano.readTag();
                      } 
                      arrayOfInt2[i1] = param1CodedInputByteBufferNano.readInt32();
                      this.nonBlockingLatenciesMs = arrayOfInt2;
                      continue;
                    } 
                    this.connectBlockingCount = param1CodedInputByteBufferNano.readInt32();
                    continue;
                  } 
                  int n = WireFormatNano.getRepeatedFieldArrayLength(param1CodedInputByteBufferNano, 34);
                  IpConnectivityLogClass.Pair[] arrayOfPair = this.errnosCounters;
                  if (arrayOfPair == null) {
                    i = 0;
                  } else {
                    i = arrayOfPair.length;
                  } 
                  arrayOfPair = new IpConnectivityLogClass.Pair[i + n];
                  n = i;
                  if (i != 0) {
                    System.arraycopy(this.errnosCounters, 0, arrayOfPair, 0, i);
                    n = i;
                  } 
                  for (; n < arrayOfPair.length - 1; n++) {
                    arrayOfPair[n] = new IpConnectivityLogClass.Pair();
                    param1CodedInputByteBufferNano.readMessage(arrayOfPair[n]);
                    param1CodedInputByteBufferNano.readTag();
                  } 
                  arrayOfPair[n] = new IpConnectivityLogClass.Pair();
                  param1CodedInputByteBufferNano.readMessage(arrayOfPair[n]);
                  this.errnosCounters = arrayOfPair;
                  continue;
                } 
                i = param1CodedInputByteBufferNano.readRawVarint32();
                int k = param1CodedInputByteBufferNano.pushLimit(i);
                int m = 0;
                i = param1CodedInputByteBufferNano.getPosition();
                while (param1CodedInputByteBufferNano.getBytesUntilLimit() > 0) {
                  param1CodedInputByteBufferNano.readInt32();
                  m++;
                } 
                param1CodedInputByteBufferNano.rewindToPosition(i);
                int[] arrayOfInt1 = this.latenciesMs;
                if (arrayOfInt1 == null) {
                  i = 0;
                } else {
                  i = arrayOfInt1.length;
                } 
                arrayOfInt1 = new int[i + m];
                m = i;
                if (i != 0) {
                  System.arraycopy(this.latenciesMs, 0, arrayOfInt1, 0, i);
                  m = i;
                } 
                for (; m < arrayOfInt1.length; m++)
                  arrayOfInt1[m] = param1CodedInputByteBufferNano.readInt32(); 
                this.latenciesMs = arrayOfInt1;
                param1CodedInputByteBufferNano.popLimit(k);
                continue;
              } 
              int j = WireFormatNano.getRepeatedFieldArrayLength(param1CodedInputByteBufferNano, 24);
              int[] arrayOfInt = this.latenciesMs;
              if (arrayOfInt == null) {
                i = 0;
              } else {
                i = arrayOfInt.length;
              } 
              arrayOfInt = new int[i + j];
              j = i;
              if (i != 0) {
                System.arraycopy(this.latenciesMs, 0, arrayOfInt, 0, i);
                j = i;
              } 
              for (; j < arrayOfInt.length - 1; j++) {
                arrayOfInt[j] = param1CodedInputByteBufferNano.readInt32();
                param1CodedInputByteBufferNano.readTag();
              } 
              arrayOfInt[j] = param1CodedInputByteBufferNano.readInt32();
              this.latenciesMs = arrayOfInt;
              continue;
            } 
            this.ipv6AddrCount = param1CodedInputByteBufferNano.readInt32();
            continue;
          } 
          this.connectCount = param1CodedInputByteBufferNano.readInt32();
          continue;
        } 
        break;
      } 
      return this;
    }
    
    public static ConnectStatistics parseFrom(byte[] param1ArrayOfbyte) throws InvalidProtocolBufferNanoException {
      return (ConnectStatistics)MessageNano.mergeFrom(new ConnectStatistics(), param1ArrayOfbyte);
    }
    
    public static ConnectStatistics parseFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      return (new ConnectStatistics()).mergeFrom(param1CodedInputByteBufferNano);
    }
  }
  
  class DHCPEvent extends MessageNano {
    public static final int ERROR_CODE_FIELD_NUMBER = 3;
    
    public static final int STATE_TRANSITION_FIELD_NUMBER = 2;
    
    private static volatile DHCPEvent[] _emptyArray;
    
    public int durationMs;
    
    public String ifName;
    
    private int valueCase_ = 0;
    
    private Object value_;
    
    public int getValueCase() {
      return this.valueCase_;
    }
    
    public DHCPEvent clearValue() {
      this.valueCase_ = 0;
      this.value_ = null;
      return this;
    }
    
    public static DHCPEvent[] emptyArray() {
      if (_emptyArray == null)
        synchronized (InternalNano.LAZY_INIT_LOCK) {
          if (_emptyArray == null)
            _emptyArray = new DHCPEvent[0]; 
        }  
      return _emptyArray;
    }
    
    public boolean hasStateTransition() {
      boolean bool;
      if (this.valueCase_ == 2) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public String getStateTransition() {
      if (this.valueCase_ == 2)
        return (String)this.value_; 
      return "";
    }
    
    public DHCPEvent setStateTransition(String param1String) {
      this.valueCase_ = 2;
      this.value_ = param1String;
      return this;
    }
    
    public boolean hasErrorCode() {
      boolean bool;
      if (this.valueCase_ == 3) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public int getErrorCode() {
      if (this.valueCase_ == 3)
        return ((Integer)this.value_).intValue(); 
      return 0;
    }
    
    public DHCPEvent setErrorCode(int param1Int) {
      this.valueCase_ = 3;
      this.value_ = Integer.valueOf(param1Int);
      return this;
    }
    
    public DHCPEvent() {
      clear();
    }
    
    public DHCPEvent clear() {
      this.ifName = "";
      this.durationMs = 0;
      clearValue();
      this.cachedSize = -1;
      return this;
    }
    
    public void writeTo(CodedOutputByteBufferNano param1CodedOutputByteBufferNano) throws IOException {
      if (!this.ifName.equals(""))
        param1CodedOutputByteBufferNano.writeString(1, this.ifName); 
      if (this.valueCase_ == 2)
        param1CodedOutputByteBufferNano.writeString(2, (String)this.value_); 
      if (this.valueCase_ == 3) {
        Integer integer = (Integer)this.value_;
        int j = integer.intValue();
        param1CodedOutputByteBufferNano.writeInt32(3, j);
      } 
      int i = this.durationMs;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(4, i); 
      super.writeTo(param1CodedOutputByteBufferNano);
    }
    
    protected int computeSerializedSize() {
      int i = super.computeSerializedSize();
      int j = i;
      if (!this.ifName.equals("")) {
        String str = this.ifName;
        j = i + CodedOutputByteBufferNano.computeStringSize(1, str);
      } 
      i = j;
      if (this.valueCase_ == 2) {
        String str = (String)this.value_;
        i = j + CodedOutputByteBufferNano.computeStringSize(2, str);
      } 
      j = i;
      if (this.valueCase_ == 3) {
        Integer integer = (Integer)this.value_;
        j = integer.intValue();
        j = i + CodedOutputByteBufferNano.computeInt32Size(3, j);
      } 
      int k = this.durationMs;
      i = j;
      if (k != 0)
        i = j + CodedOutputByteBufferNano.computeInt32Size(4, k); 
      return i;
    }
    
    public DHCPEvent mergeFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      while (true) {
        int i = param1CodedInputByteBufferNano.readTag();
        if (i != 0) {
          if (i != 10) {
            if (i != 18) {
              if (i != 24) {
                if (i != 32) {
                  if (!WireFormatNano.parseUnknownField(param1CodedInputByteBufferNano, i))
                    return this; 
                  continue;
                } 
                this.durationMs = param1CodedInputByteBufferNano.readInt32();
                continue;
              } 
              this.value_ = Integer.valueOf(param1CodedInputByteBufferNano.readInt32());
              this.valueCase_ = 3;
              continue;
            } 
            this.value_ = param1CodedInputByteBufferNano.readString();
            this.valueCase_ = 2;
            continue;
          } 
          this.ifName = param1CodedInputByteBufferNano.readString();
          continue;
        } 
        break;
      } 
      return this;
    }
    
    public static DHCPEvent parseFrom(byte[] param1ArrayOfbyte) throws InvalidProtocolBufferNanoException {
      return (DHCPEvent)MessageNano.mergeFrom(new DHCPEvent(), param1ArrayOfbyte);
    }
    
    public static DHCPEvent parseFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      return (new DHCPEvent()).mergeFrom(param1CodedInputByteBufferNano);
    }
  }
  
  class ApfProgramEvent extends MessageNano {
    private static volatile ApfProgramEvent[] _emptyArray;
    
    public int currentRas;
    
    public boolean dropMulticast;
    
    public long effectiveLifetime;
    
    public int filteredRas;
    
    public boolean hasIpv4Addr;
    
    public long lifetime;
    
    public int programLength;
    
    public static ApfProgramEvent[] emptyArray() {
      if (_emptyArray == null)
        synchronized (InternalNano.LAZY_INIT_LOCK) {
          if (_emptyArray == null)
            _emptyArray = new ApfProgramEvent[0]; 
        }  
      return _emptyArray;
    }
    
    public ApfProgramEvent() {
      clear();
    }
    
    public ApfProgramEvent clear() {
      this.lifetime = 0L;
      this.effectiveLifetime = 0L;
      this.filteredRas = 0;
      this.currentRas = 0;
      this.programLength = 0;
      this.dropMulticast = false;
      this.hasIpv4Addr = false;
      this.cachedSize = -1;
      return this;
    }
    
    public void writeTo(CodedOutputByteBufferNano param1CodedOutputByteBufferNano) throws IOException {
      long l = this.lifetime;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(1, l); 
      int i = this.filteredRas;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(2, i); 
      i = this.currentRas;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(3, i); 
      i = this.programLength;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(4, i); 
      boolean bool = this.dropMulticast;
      if (bool)
        param1CodedOutputByteBufferNano.writeBool(5, bool); 
      bool = this.hasIpv4Addr;
      if (bool)
        param1CodedOutputByteBufferNano.writeBool(6, bool); 
      l = this.effectiveLifetime;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(7, l); 
      super.writeTo(param1CodedOutputByteBufferNano);
    }
    
    protected int computeSerializedSize() {
      int i = super.computeSerializedSize();
      long l = this.lifetime;
      int j = i;
      if (l != 0L)
        j = i + CodedOutputByteBufferNano.computeInt64Size(1, l); 
      int k = this.filteredRas;
      i = j;
      if (k != 0)
        i = j + CodedOutputByteBufferNano.computeInt32Size(2, k); 
      k = this.currentRas;
      j = i;
      if (k != 0)
        j = i + CodedOutputByteBufferNano.computeInt32Size(3, k); 
      k = this.programLength;
      i = j;
      if (k != 0)
        i = j + CodedOutputByteBufferNano.computeInt32Size(4, k); 
      boolean bool = this.dropMulticast;
      j = i;
      if (bool)
        j = i + CodedOutputByteBufferNano.computeBoolSize(5, bool); 
      bool = this.hasIpv4Addr;
      i = j;
      if (bool)
        i = j + CodedOutputByteBufferNano.computeBoolSize(6, bool); 
      l = this.effectiveLifetime;
      j = i;
      if (l != 0L)
        j = i + CodedOutputByteBufferNano.computeInt64Size(7, l); 
      return j;
    }
    
    public ApfProgramEvent mergeFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      while (true) {
        int i = param1CodedInputByteBufferNano.readTag();
        if (i != 0) {
          if (i != 8) {
            if (i != 16) {
              if (i != 24) {
                if (i != 32) {
                  if (i != 40) {
                    if (i != 48) {
                      if (i != 56) {
                        if (!WireFormatNano.parseUnknownField(param1CodedInputByteBufferNano, i))
                          return this; 
                        continue;
                      } 
                      this.effectiveLifetime = param1CodedInputByteBufferNano.readInt64();
                      continue;
                    } 
                    this.hasIpv4Addr = param1CodedInputByteBufferNano.readBool();
                    continue;
                  } 
                  this.dropMulticast = param1CodedInputByteBufferNano.readBool();
                  continue;
                } 
                this.programLength = param1CodedInputByteBufferNano.readInt32();
                continue;
              } 
              this.currentRas = param1CodedInputByteBufferNano.readInt32();
              continue;
            } 
            this.filteredRas = param1CodedInputByteBufferNano.readInt32();
            continue;
          } 
          this.lifetime = param1CodedInputByteBufferNano.readInt64();
          continue;
        } 
        break;
      } 
      return this;
    }
    
    public static ApfProgramEvent parseFrom(byte[] param1ArrayOfbyte) throws InvalidProtocolBufferNanoException {
      return (ApfProgramEvent)MessageNano.mergeFrom(new ApfProgramEvent(), param1ArrayOfbyte);
    }
    
    public static ApfProgramEvent parseFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      return (new ApfProgramEvent()).mergeFrom(param1CodedInputByteBufferNano);
    }
  }
  
  class ApfStatistics extends MessageNano {
    private static volatile ApfStatistics[] _emptyArray;
    
    public int droppedRas;
    
    public long durationMs;
    
    public IpConnectivityLogClass.Pair[] hardwareCounters;
    
    public int matchingRas;
    
    public int maxProgramSize;
    
    public int parseErrors;
    
    public int programUpdates;
    
    public int programUpdatesAll;
    
    public int programUpdatesAllowingMulticast;
    
    public int receivedRas;
    
    public int totalPacketDropped;
    
    public int totalPacketProcessed;
    
    public int zeroLifetimeRas;
    
    public static ApfStatistics[] emptyArray() {
      if (_emptyArray == null)
        synchronized (InternalNano.LAZY_INIT_LOCK) {
          if (_emptyArray == null)
            _emptyArray = new ApfStatistics[0]; 
        }  
      return _emptyArray;
    }
    
    public ApfStatistics() {
      clear();
    }
    
    public ApfStatistics clear() {
      this.durationMs = 0L;
      this.receivedRas = 0;
      this.matchingRas = 0;
      this.droppedRas = 0;
      this.zeroLifetimeRas = 0;
      this.parseErrors = 0;
      this.programUpdates = 0;
      this.maxProgramSize = 0;
      this.programUpdatesAll = 0;
      this.programUpdatesAllowingMulticast = 0;
      this.totalPacketProcessed = 0;
      this.totalPacketDropped = 0;
      this.hardwareCounters = IpConnectivityLogClass.Pair.emptyArray();
      this.cachedSize = -1;
      return this;
    }
    
    public void writeTo(CodedOutputByteBufferNano param1CodedOutputByteBufferNano) throws IOException {
      long l = this.durationMs;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(1, l); 
      int i = this.receivedRas;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(2, i); 
      i = this.matchingRas;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(3, i); 
      i = this.droppedRas;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(5, i); 
      i = this.zeroLifetimeRas;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(6, i); 
      i = this.parseErrors;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(7, i); 
      i = this.programUpdates;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(8, i); 
      i = this.maxProgramSize;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(9, i); 
      i = this.programUpdatesAll;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(10, i); 
      i = this.programUpdatesAllowingMulticast;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(11, i); 
      i = this.totalPacketProcessed;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(12, i); 
      i = this.totalPacketDropped;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(13, i); 
      IpConnectivityLogClass.Pair[] arrayOfPair = this.hardwareCounters;
      if (arrayOfPair != null && arrayOfPair.length > 0) {
        i = 0;
        while (true) {
          arrayOfPair = this.hardwareCounters;
          if (i < arrayOfPair.length) {
            IpConnectivityLogClass.Pair pair = arrayOfPair[i];
            if (pair != null)
              param1CodedOutputByteBufferNano.writeMessage(14, pair); 
            i++;
            continue;
          } 
          break;
        } 
      } 
      super.writeTo(param1CodedOutputByteBufferNano);
    }
    
    protected int computeSerializedSize() {
      int i = super.computeSerializedSize();
      long l = this.durationMs;
      int j = i;
      if (l != 0L)
        j = i + CodedOutputByteBufferNano.computeInt64Size(1, l); 
      int k = this.receivedRas;
      i = j;
      if (k != 0)
        i = j + CodedOutputByteBufferNano.computeInt32Size(2, k); 
      k = this.matchingRas;
      j = i;
      if (k != 0)
        j = i + CodedOutputByteBufferNano.computeInt32Size(3, k); 
      k = this.droppedRas;
      i = j;
      if (k != 0)
        i = j + CodedOutputByteBufferNano.computeInt32Size(5, k); 
      k = this.zeroLifetimeRas;
      j = i;
      if (k != 0)
        j = i + CodedOutputByteBufferNano.computeInt32Size(6, k); 
      k = this.parseErrors;
      i = j;
      if (k != 0)
        i = j + CodedOutputByteBufferNano.computeInt32Size(7, k); 
      k = this.programUpdates;
      j = i;
      if (k != 0)
        j = i + CodedOutputByteBufferNano.computeInt32Size(8, k); 
      k = this.maxProgramSize;
      i = j;
      if (k != 0)
        i = j + CodedOutputByteBufferNano.computeInt32Size(9, k); 
      k = this.programUpdatesAll;
      j = i;
      if (k != 0)
        j = i + CodedOutputByteBufferNano.computeInt32Size(10, k); 
      k = this.programUpdatesAllowingMulticast;
      i = j;
      if (k != 0)
        i = j + CodedOutputByteBufferNano.computeInt32Size(11, k); 
      k = this.totalPacketProcessed;
      j = i;
      if (k != 0)
        j = i + CodedOutputByteBufferNano.computeInt32Size(12, k); 
      k = this.totalPacketDropped;
      i = j;
      if (k != 0)
        i = j + CodedOutputByteBufferNano.computeInt32Size(13, k); 
      IpConnectivityLogClass.Pair[] arrayOfPair = this.hardwareCounters;
      k = i;
      if (arrayOfPair != null) {
        k = i;
        if (arrayOfPair.length > 0) {
          j = 0;
          while (true) {
            arrayOfPair = this.hardwareCounters;
            k = i;
            if (j < arrayOfPair.length) {
              IpConnectivityLogClass.Pair pair = arrayOfPair[j];
              k = i;
              if (pair != null)
                k = i + CodedOutputByteBufferNano.computeMessageSize(14, pair); 
              j++;
              i = k;
              continue;
            } 
            break;
          } 
        } 
      } 
      return k;
    }
    
    public ApfStatistics mergeFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      while (true) {
        int j;
        IpConnectivityLogClass.Pair[] arrayOfPair;
        int i = param1CodedInputByteBufferNano.readTag();
        switch (i) {
          default:
            if (!WireFormatNano.parseUnknownField(param1CodedInputByteBufferNano, i))
              return this; 
            continue;
          case 114:
            j = WireFormatNano.getRepeatedFieldArrayLength(param1CodedInputByteBufferNano, 114);
            arrayOfPair = this.hardwareCounters;
            if (arrayOfPair == null) {
              i = 0;
            } else {
              i = arrayOfPair.length;
            } 
            arrayOfPair = new IpConnectivityLogClass.Pair[i + j];
            j = i;
            if (i != 0) {
              System.arraycopy(this.hardwareCounters, 0, arrayOfPair, 0, i);
              j = i;
            } 
            for (; j < arrayOfPair.length - 1; j++) {
              arrayOfPair[j] = new IpConnectivityLogClass.Pair();
              param1CodedInputByteBufferNano.readMessage(arrayOfPair[j]);
              param1CodedInputByteBufferNano.readTag();
            } 
            arrayOfPair[j] = new IpConnectivityLogClass.Pair();
            param1CodedInputByteBufferNano.readMessage(arrayOfPair[j]);
            this.hardwareCounters = arrayOfPair;
            continue;
          case 104:
            this.totalPacketDropped = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 96:
            this.totalPacketProcessed = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 88:
            this.programUpdatesAllowingMulticast = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 80:
            this.programUpdatesAll = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 72:
            this.maxProgramSize = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 64:
            this.programUpdates = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 56:
            this.parseErrors = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 48:
            this.zeroLifetimeRas = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 40:
            this.droppedRas = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 24:
            this.matchingRas = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 16:
            this.receivedRas = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 8:
            this.durationMs = param1CodedInputByteBufferNano.readInt64();
            continue;
          case 0:
            break;
        } 
        break;
      } 
      return this;
    }
    
    public static ApfStatistics parseFrom(byte[] param1ArrayOfbyte) throws InvalidProtocolBufferNanoException {
      return (ApfStatistics)MessageNano.mergeFrom(new ApfStatistics(), param1ArrayOfbyte);
    }
    
    public static ApfStatistics parseFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      return (new ApfStatistics()).mergeFrom(param1CodedInputByteBufferNano);
    }
  }
  
  class RaEvent extends MessageNano {
    private static volatile RaEvent[] _emptyArray;
    
    public long dnsslLifetime;
    
    public long prefixPreferredLifetime;
    
    public long prefixValidLifetime;
    
    public long rdnssLifetime;
    
    public long routeInfoLifetime;
    
    public long routerLifetime;
    
    public static RaEvent[] emptyArray() {
      if (_emptyArray == null)
        synchronized (InternalNano.LAZY_INIT_LOCK) {
          if (_emptyArray == null)
            _emptyArray = new RaEvent[0]; 
        }  
      return _emptyArray;
    }
    
    public RaEvent() {
      clear();
    }
    
    public RaEvent clear() {
      this.routerLifetime = 0L;
      this.prefixValidLifetime = 0L;
      this.prefixPreferredLifetime = 0L;
      this.routeInfoLifetime = 0L;
      this.rdnssLifetime = 0L;
      this.dnsslLifetime = 0L;
      this.cachedSize = -1;
      return this;
    }
    
    public void writeTo(CodedOutputByteBufferNano param1CodedOutputByteBufferNano) throws IOException {
      long l = this.routerLifetime;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(1, l); 
      l = this.prefixValidLifetime;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(2, l); 
      l = this.prefixPreferredLifetime;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(3, l); 
      l = this.routeInfoLifetime;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(4, l); 
      l = this.rdnssLifetime;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(5, l); 
      l = this.dnsslLifetime;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(6, l); 
      super.writeTo(param1CodedOutputByteBufferNano);
    }
    
    protected int computeSerializedSize() {
      int i = super.computeSerializedSize();
      long l = this.routerLifetime;
      int j = i;
      if (l != 0L)
        j = i + CodedOutputByteBufferNano.computeInt64Size(1, l); 
      l = this.prefixValidLifetime;
      i = j;
      if (l != 0L)
        i = j + CodedOutputByteBufferNano.computeInt64Size(2, l); 
      l = this.prefixPreferredLifetime;
      j = i;
      if (l != 0L)
        j = i + CodedOutputByteBufferNano.computeInt64Size(3, l); 
      l = this.routeInfoLifetime;
      i = j;
      if (l != 0L)
        i = j + CodedOutputByteBufferNano.computeInt64Size(4, l); 
      l = this.rdnssLifetime;
      j = i;
      if (l != 0L)
        j = i + CodedOutputByteBufferNano.computeInt64Size(5, l); 
      l = this.dnsslLifetime;
      i = j;
      if (l != 0L)
        i = j + CodedOutputByteBufferNano.computeInt64Size(6, l); 
      return i;
    }
    
    public RaEvent mergeFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      while (true) {
        int i = param1CodedInputByteBufferNano.readTag();
        if (i != 0) {
          if (i != 8) {
            if (i != 16) {
              if (i != 24) {
                if (i != 32) {
                  if (i != 40) {
                    if (i != 48) {
                      if (!WireFormatNano.parseUnknownField(param1CodedInputByteBufferNano, i))
                        return this; 
                      continue;
                    } 
                    this.dnsslLifetime = param1CodedInputByteBufferNano.readInt64();
                    continue;
                  } 
                  this.rdnssLifetime = param1CodedInputByteBufferNano.readInt64();
                  continue;
                } 
                this.routeInfoLifetime = param1CodedInputByteBufferNano.readInt64();
                continue;
              } 
              this.prefixPreferredLifetime = param1CodedInputByteBufferNano.readInt64();
              continue;
            } 
            this.prefixValidLifetime = param1CodedInputByteBufferNano.readInt64();
            continue;
          } 
          this.routerLifetime = param1CodedInputByteBufferNano.readInt64();
          continue;
        } 
        break;
      } 
      return this;
    }
    
    public static RaEvent parseFrom(byte[] param1ArrayOfbyte) throws InvalidProtocolBufferNanoException {
      return (RaEvent)MessageNano.mergeFrom(new RaEvent(), param1ArrayOfbyte);
    }
    
    public static RaEvent parseFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      return (new RaEvent()).mergeFrom(param1CodedInputByteBufferNano);
    }
  }
  
  class IpProvisioningEvent extends MessageNano {
    private static volatile IpProvisioningEvent[] _emptyArray;
    
    public int eventType;
    
    public String ifName;
    
    public int latencyMs;
    
    public static IpProvisioningEvent[] emptyArray() {
      if (_emptyArray == null)
        synchronized (InternalNano.LAZY_INIT_LOCK) {
          if (_emptyArray == null)
            _emptyArray = new IpProvisioningEvent[0]; 
        }  
      return _emptyArray;
    }
    
    public IpProvisioningEvent() {
      clear();
    }
    
    public IpProvisioningEvent clear() {
      this.ifName = "";
      this.eventType = 0;
      this.latencyMs = 0;
      this.cachedSize = -1;
      return this;
    }
    
    public void writeTo(CodedOutputByteBufferNano param1CodedOutputByteBufferNano) throws IOException {
      if (!this.ifName.equals(""))
        param1CodedOutputByteBufferNano.writeString(1, this.ifName); 
      int i = this.eventType;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(2, i); 
      i = this.latencyMs;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(3, i); 
      super.writeTo(param1CodedOutputByteBufferNano);
    }
    
    protected int computeSerializedSize() {
      int i = super.computeSerializedSize();
      int j = i;
      if (!this.ifName.equals("")) {
        String str = this.ifName;
        j = i + CodedOutputByteBufferNano.computeStringSize(1, str);
      } 
      int k = this.eventType;
      i = j;
      if (k != 0)
        i = j + CodedOutputByteBufferNano.computeInt32Size(2, k); 
      k = this.latencyMs;
      j = i;
      if (k != 0)
        j = i + CodedOutputByteBufferNano.computeInt32Size(3, k); 
      return j;
    }
    
    public IpProvisioningEvent mergeFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      while (true) {
        int i = param1CodedInputByteBufferNano.readTag();
        if (i != 0) {
          if (i != 10) {
            if (i != 16) {
              if (i != 24) {
                if (!WireFormatNano.parseUnknownField(param1CodedInputByteBufferNano, i))
                  return this; 
                continue;
              } 
              this.latencyMs = param1CodedInputByteBufferNano.readInt32();
              continue;
            } 
            this.eventType = param1CodedInputByteBufferNano.readInt32();
            continue;
          } 
          this.ifName = param1CodedInputByteBufferNano.readString();
          continue;
        } 
        break;
      } 
      return this;
    }
    
    public static IpProvisioningEvent parseFrom(byte[] param1ArrayOfbyte) throws InvalidProtocolBufferNanoException {
      return (IpProvisioningEvent)MessageNano.mergeFrom(new IpProvisioningEvent(), param1ArrayOfbyte);
    }
    
    public static IpProvisioningEvent parseFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      return (new IpProvisioningEvent()).mergeFrom(param1CodedInputByteBufferNano);
    }
  }
  
  class NetworkStats extends MessageNano {
    private static volatile NetworkStats[] _emptyArray;
    
    public long durationMs;
    
    public boolean everValidated;
    
    public int ipSupport;
    
    public int noConnectivityReports;
    
    public boolean portalFound;
    
    public int validationAttempts;
    
    public IpConnectivityLogClass.Pair[] validationEvents;
    
    public IpConnectivityLogClass.Pair[] validationStates;
    
    public static NetworkStats[] emptyArray() {
      if (_emptyArray == null)
        synchronized (InternalNano.LAZY_INIT_LOCK) {
          if (_emptyArray == null)
            _emptyArray = new NetworkStats[0]; 
        }  
      return _emptyArray;
    }
    
    public NetworkStats() {
      clear();
    }
    
    public NetworkStats clear() {
      this.durationMs = 0L;
      this.ipSupport = 0;
      this.everValidated = false;
      this.portalFound = false;
      this.noConnectivityReports = 0;
      this.validationAttempts = 0;
      this.validationEvents = IpConnectivityLogClass.Pair.emptyArray();
      this.validationStates = IpConnectivityLogClass.Pair.emptyArray();
      this.cachedSize = -1;
      return this;
    }
    
    public void writeTo(CodedOutputByteBufferNano param1CodedOutputByteBufferNano) throws IOException {
      long l = this.durationMs;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(1, l); 
      int i = this.ipSupport;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(2, i); 
      boolean bool = this.everValidated;
      if (bool)
        param1CodedOutputByteBufferNano.writeBool(3, bool); 
      bool = this.portalFound;
      if (bool)
        param1CodedOutputByteBufferNano.writeBool(4, bool); 
      i = this.noConnectivityReports;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(5, i); 
      i = this.validationAttempts;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(6, i); 
      IpConnectivityLogClass.Pair[] arrayOfPair = this.validationEvents;
      if (arrayOfPair != null && arrayOfPair.length > 0) {
        i = 0;
        while (true) {
          arrayOfPair = this.validationEvents;
          if (i < arrayOfPair.length) {
            IpConnectivityLogClass.Pair pair = arrayOfPair[i];
            if (pair != null)
              param1CodedOutputByteBufferNano.writeMessage(7, pair); 
            i++;
            continue;
          } 
          break;
        } 
      } 
      arrayOfPair = this.validationStates;
      if (arrayOfPair != null && arrayOfPair.length > 0) {
        i = 0;
        while (true) {
          arrayOfPair = this.validationStates;
          if (i < arrayOfPair.length) {
            IpConnectivityLogClass.Pair pair = arrayOfPair[i];
            if (pair != null)
              param1CodedOutputByteBufferNano.writeMessage(8, pair); 
            i++;
            continue;
          } 
          break;
        } 
      } 
      super.writeTo(param1CodedOutputByteBufferNano);
    }
    
    protected int computeSerializedSize() {
      int i = super.computeSerializedSize();
      long l = this.durationMs;
      int j = i;
      if (l != 0L)
        j = i + CodedOutputByteBufferNano.computeInt64Size(1, l); 
      int k = this.ipSupport;
      i = j;
      if (k != 0)
        i = j + CodedOutputByteBufferNano.computeInt32Size(2, k); 
      boolean bool = this.everValidated;
      j = i;
      if (bool)
        j = i + CodedOutputByteBufferNano.computeBoolSize(3, bool); 
      bool = this.portalFound;
      i = j;
      if (bool)
        i = j + CodedOutputByteBufferNano.computeBoolSize(4, bool); 
      k = this.noConnectivityReports;
      j = i;
      if (k != 0)
        j = i + CodedOutputByteBufferNano.computeInt32Size(5, k); 
      k = this.validationAttempts;
      i = j;
      if (k != 0)
        i = j + CodedOutputByteBufferNano.computeInt32Size(6, k); 
      IpConnectivityLogClass.Pair[] arrayOfPair = this.validationEvents;
      j = i;
      if (arrayOfPair != null) {
        j = i;
        if (arrayOfPair.length > 0) {
          k = 0;
          while (true) {
            arrayOfPair = this.validationEvents;
            j = i;
            if (k < arrayOfPair.length) {
              IpConnectivityLogClass.Pair pair = arrayOfPair[k];
              j = i;
              if (pair != null)
                j = i + CodedOutputByteBufferNano.computeMessageSize(7, pair); 
              k++;
              i = j;
              continue;
            } 
            break;
          } 
        } 
      } 
      arrayOfPair = this.validationStates;
      k = j;
      if (arrayOfPair != null) {
        k = j;
        if (arrayOfPair.length > 0) {
          i = 0;
          while (true) {
            arrayOfPair = this.validationStates;
            k = j;
            if (i < arrayOfPair.length) {
              IpConnectivityLogClass.Pair pair = arrayOfPair[i];
              k = j;
              if (pair != null)
                k = j + CodedOutputByteBufferNano.computeMessageSize(8, pair); 
              i++;
              j = k;
              continue;
            } 
            break;
          } 
        } 
      } 
      return k;
    }
    
    public NetworkStats mergeFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      while (true) {
        int i = param1CodedInputByteBufferNano.readTag();
        if (i != 0) {
          if (i != 8) {
            if (i != 16) {
              if (i != 24) {
                if (i != 32) {
                  if (i != 40) {
                    if (i != 48) {
                      if (i != 58) {
                        if (i != 66) {
                          if (!WireFormatNano.parseUnknownField(param1CodedInputByteBufferNano, i))
                            return this; 
                          continue;
                        } 
                        int k = WireFormatNano.getRepeatedFieldArrayLength(param1CodedInputByteBufferNano, 66);
                        IpConnectivityLogClass.Pair[] arrayOfPair1 = this.validationStates;
                        if (arrayOfPair1 == null) {
                          i = 0;
                        } else {
                          i = arrayOfPair1.length;
                        } 
                        arrayOfPair1 = new IpConnectivityLogClass.Pair[i + k];
                        k = i;
                        if (i != 0) {
                          System.arraycopy(this.validationStates, 0, arrayOfPair1, 0, i);
                          k = i;
                        } 
                        for (; k < arrayOfPair1.length - 1; k++) {
                          arrayOfPair1[k] = new IpConnectivityLogClass.Pair();
                          param1CodedInputByteBufferNano.readMessage(arrayOfPair1[k]);
                          param1CodedInputByteBufferNano.readTag();
                        } 
                        arrayOfPair1[k] = new IpConnectivityLogClass.Pair();
                        param1CodedInputByteBufferNano.readMessage(arrayOfPair1[k]);
                        this.validationStates = arrayOfPair1;
                        continue;
                      } 
                      int j = WireFormatNano.getRepeatedFieldArrayLength(param1CodedInputByteBufferNano, 58);
                      IpConnectivityLogClass.Pair[] arrayOfPair = this.validationEvents;
                      if (arrayOfPair == null) {
                        i = 0;
                      } else {
                        i = arrayOfPair.length;
                      } 
                      arrayOfPair = new IpConnectivityLogClass.Pair[i + j];
                      j = i;
                      if (i != 0) {
                        System.arraycopy(this.validationEvents, 0, arrayOfPair, 0, i);
                        j = i;
                      } 
                      for (; j < arrayOfPair.length - 1; j++) {
                        arrayOfPair[j] = new IpConnectivityLogClass.Pair();
                        param1CodedInputByteBufferNano.readMessage(arrayOfPair[j]);
                        param1CodedInputByteBufferNano.readTag();
                      } 
                      arrayOfPair[j] = new IpConnectivityLogClass.Pair();
                      param1CodedInputByteBufferNano.readMessage(arrayOfPair[j]);
                      this.validationEvents = arrayOfPair;
                      continue;
                    } 
                    this.validationAttempts = param1CodedInputByteBufferNano.readInt32();
                    continue;
                  } 
                  this.noConnectivityReports = param1CodedInputByteBufferNano.readInt32();
                  continue;
                } 
                this.portalFound = param1CodedInputByteBufferNano.readBool();
                continue;
              } 
              this.everValidated = param1CodedInputByteBufferNano.readBool();
              continue;
            } 
            i = param1CodedInputByteBufferNano.readInt32();
            if (i != 0 && i != 1 && i != 2 && i != 3)
              continue; 
            this.ipSupport = i;
            continue;
          } 
          this.durationMs = param1CodedInputByteBufferNano.readInt64();
          continue;
        } 
        break;
      } 
      return this;
    }
    
    public static NetworkStats parseFrom(byte[] param1ArrayOfbyte) throws InvalidProtocolBufferNanoException {
      return (NetworkStats)MessageNano.mergeFrom(new NetworkStats(), param1ArrayOfbyte);
    }
    
    public static NetworkStats parseFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      return (new NetworkStats()).mergeFrom(param1CodedInputByteBufferNano);
    }
  }
  
  class WakeupStats extends MessageNano {
    private static volatile WakeupStats[] _emptyArray;
    
    public long applicationWakeups;
    
    public long durationSec;
    
    public IpConnectivityLogClass.Pair[] ethertypeCounts;
    
    public IpConnectivityLogClass.Pair[] ipNextHeaderCounts;
    
    public long l2BroadcastCount;
    
    public long l2MulticastCount;
    
    public long l2UnicastCount;
    
    public long noUidWakeups;
    
    public long nonApplicationWakeups;
    
    public long rootWakeups;
    
    public long systemWakeups;
    
    public long totalWakeups;
    
    public static WakeupStats[] emptyArray() {
      if (_emptyArray == null)
        synchronized (InternalNano.LAZY_INIT_LOCK) {
          if (_emptyArray == null)
            _emptyArray = new WakeupStats[0]; 
        }  
      return _emptyArray;
    }
    
    public WakeupStats() {
      clear();
    }
    
    public WakeupStats clear() {
      this.durationSec = 0L;
      this.totalWakeups = 0L;
      this.rootWakeups = 0L;
      this.systemWakeups = 0L;
      this.applicationWakeups = 0L;
      this.nonApplicationWakeups = 0L;
      this.noUidWakeups = 0L;
      this.ethertypeCounts = IpConnectivityLogClass.Pair.emptyArray();
      this.ipNextHeaderCounts = IpConnectivityLogClass.Pair.emptyArray();
      this.l2UnicastCount = 0L;
      this.l2MulticastCount = 0L;
      this.l2BroadcastCount = 0L;
      this.cachedSize = -1;
      return this;
    }
    
    public void writeTo(CodedOutputByteBufferNano param1CodedOutputByteBufferNano) throws IOException {
      long l = this.durationSec;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(1, l); 
      l = this.totalWakeups;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(2, l); 
      l = this.rootWakeups;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(3, l); 
      l = this.systemWakeups;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(4, l); 
      l = this.applicationWakeups;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(5, l); 
      l = this.nonApplicationWakeups;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(6, l); 
      l = this.noUidWakeups;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(7, l); 
      IpConnectivityLogClass.Pair[] arrayOfPair = this.ethertypeCounts;
      if (arrayOfPair != null && arrayOfPair.length > 0) {
        byte b = 0;
        while (true) {
          arrayOfPair = this.ethertypeCounts;
          if (b < arrayOfPair.length) {
            IpConnectivityLogClass.Pair pair = arrayOfPair[b];
            if (pair != null)
              param1CodedOutputByteBufferNano.writeMessage(8, pair); 
            b++;
            continue;
          } 
          break;
        } 
      } 
      arrayOfPair = this.ipNextHeaderCounts;
      if (arrayOfPair != null && arrayOfPair.length > 0) {
        byte b = 0;
        while (true) {
          arrayOfPair = this.ipNextHeaderCounts;
          if (b < arrayOfPair.length) {
            IpConnectivityLogClass.Pair pair = arrayOfPair[b];
            if (pair != null)
              param1CodedOutputByteBufferNano.writeMessage(9, pair); 
            b++;
            continue;
          } 
          break;
        } 
      } 
      l = this.l2UnicastCount;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(10, l); 
      l = this.l2MulticastCount;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(11, l); 
      l = this.l2BroadcastCount;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(12, l); 
      super.writeTo(param1CodedOutputByteBufferNano);
    }
    
    protected int computeSerializedSize() {
      int i = super.computeSerializedSize();
      long l = this.durationSec;
      int j = i;
      if (l != 0L)
        j = i + CodedOutputByteBufferNano.computeInt64Size(1, l); 
      l = this.totalWakeups;
      i = j;
      if (l != 0L)
        i = j + CodedOutputByteBufferNano.computeInt64Size(2, l); 
      l = this.rootWakeups;
      j = i;
      if (l != 0L)
        j = i + CodedOutputByteBufferNano.computeInt64Size(3, l); 
      l = this.systemWakeups;
      i = j;
      if (l != 0L)
        i = j + CodedOutputByteBufferNano.computeInt64Size(4, l); 
      l = this.applicationWakeups;
      j = i;
      if (l != 0L)
        j = i + CodedOutputByteBufferNano.computeInt64Size(5, l); 
      l = this.nonApplicationWakeups;
      int k = j;
      if (l != 0L)
        k = j + CodedOutputByteBufferNano.computeInt64Size(6, l); 
      l = this.noUidWakeups;
      i = k;
      if (l != 0L)
        i = k + CodedOutputByteBufferNano.computeInt64Size(7, l); 
      IpConnectivityLogClass.Pair[] arrayOfPair = this.ethertypeCounts;
      j = i;
      if (arrayOfPair != null) {
        j = i;
        if (arrayOfPair.length > 0) {
          k = 0;
          while (true) {
            arrayOfPair = this.ethertypeCounts;
            j = i;
            if (k < arrayOfPair.length) {
              IpConnectivityLogClass.Pair pair = arrayOfPair[k];
              j = i;
              if (pair != null)
                j = i + CodedOutputByteBufferNano.computeMessageSize(8, pair); 
              k++;
              i = j;
              continue;
            } 
            break;
          } 
        } 
      } 
      arrayOfPair = this.ipNextHeaderCounts;
      i = j;
      if (arrayOfPair != null) {
        i = j;
        if (arrayOfPair.length > 0) {
          k = 0;
          while (true) {
            arrayOfPair = this.ipNextHeaderCounts;
            i = j;
            if (k < arrayOfPair.length) {
              IpConnectivityLogClass.Pair pair = arrayOfPair[k];
              i = j;
              if (pair != null)
                i = j + CodedOutputByteBufferNano.computeMessageSize(9, pair); 
              k++;
              j = i;
              continue;
            } 
            break;
          } 
        } 
      } 
      l = this.l2UnicastCount;
      j = i;
      if (l != 0L)
        j = i + CodedOutputByteBufferNano.computeInt64Size(10, l); 
      l = this.l2MulticastCount;
      i = j;
      if (l != 0L)
        i = j + CodedOutputByteBufferNano.computeInt64Size(11, l); 
      l = this.l2BroadcastCount;
      j = i;
      if (l != 0L)
        j = i + CodedOutputByteBufferNano.computeInt64Size(12, l); 
      return j;
    }
    
    public WakeupStats mergeFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      while (true) {
        int j;
        IpConnectivityLogClass.Pair[] arrayOfPair;
        int i = param1CodedInputByteBufferNano.readTag();
        switch (i) {
          default:
            if (!WireFormatNano.parseUnknownField(param1CodedInputByteBufferNano, i))
              return this; 
            continue;
          case 96:
            this.l2BroadcastCount = param1CodedInputByteBufferNano.readInt64();
            continue;
          case 88:
            this.l2MulticastCount = param1CodedInputByteBufferNano.readInt64();
            continue;
          case 80:
            this.l2UnicastCount = param1CodedInputByteBufferNano.readInt64();
            continue;
          case 74:
            j = WireFormatNano.getRepeatedFieldArrayLength(param1CodedInputByteBufferNano, 74);
            arrayOfPair = this.ipNextHeaderCounts;
            if (arrayOfPair == null) {
              i = 0;
            } else {
              i = arrayOfPair.length;
            } 
            arrayOfPair = new IpConnectivityLogClass.Pair[i + j];
            j = i;
            if (i != 0) {
              System.arraycopy(this.ipNextHeaderCounts, 0, arrayOfPair, 0, i);
              j = i;
            } 
            for (; j < arrayOfPair.length - 1; j++) {
              arrayOfPair[j] = new IpConnectivityLogClass.Pair();
              param1CodedInputByteBufferNano.readMessage(arrayOfPair[j]);
              param1CodedInputByteBufferNano.readTag();
            } 
            arrayOfPair[j] = new IpConnectivityLogClass.Pair();
            param1CodedInputByteBufferNano.readMessage(arrayOfPair[j]);
            this.ipNextHeaderCounts = arrayOfPair;
            continue;
          case 66:
            j = WireFormatNano.getRepeatedFieldArrayLength(param1CodedInputByteBufferNano, 66);
            arrayOfPair = this.ethertypeCounts;
            if (arrayOfPair == null) {
              i = 0;
            } else {
              i = arrayOfPair.length;
            } 
            arrayOfPair = new IpConnectivityLogClass.Pair[i + j];
            j = i;
            if (i != 0) {
              System.arraycopy(this.ethertypeCounts, 0, arrayOfPair, 0, i);
              j = i;
            } 
            for (; j < arrayOfPair.length - 1; j++) {
              arrayOfPair[j] = new IpConnectivityLogClass.Pair();
              param1CodedInputByteBufferNano.readMessage(arrayOfPair[j]);
              param1CodedInputByteBufferNano.readTag();
            } 
            arrayOfPair[j] = new IpConnectivityLogClass.Pair();
            param1CodedInputByteBufferNano.readMessage(arrayOfPair[j]);
            this.ethertypeCounts = arrayOfPair;
            continue;
          case 56:
            this.noUidWakeups = param1CodedInputByteBufferNano.readInt64();
            continue;
          case 48:
            this.nonApplicationWakeups = param1CodedInputByteBufferNano.readInt64();
            continue;
          case 40:
            this.applicationWakeups = param1CodedInputByteBufferNano.readInt64();
            continue;
          case 32:
            this.systemWakeups = param1CodedInputByteBufferNano.readInt64();
            continue;
          case 24:
            this.rootWakeups = param1CodedInputByteBufferNano.readInt64();
            continue;
          case 16:
            this.totalWakeups = param1CodedInputByteBufferNano.readInt64();
            continue;
          case 8:
            this.durationSec = param1CodedInputByteBufferNano.readInt64();
            continue;
          case 0:
            break;
        } 
        break;
      } 
      return this;
    }
    
    public static WakeupStats parseFrom(byte[] param1ArrayOfbyte) throws InvalidProtocolBufferNanoException {
      return (WakeupStats)MessageNano.mergeFrom(new WakeupStats(), param1ArrayOfbyte);
    }
    
    public static WakeupStats parseFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      return (new WakeupStats()).mergeFrom(param1CodedInputByteBufferNano);
    }
  }
  
  class IpConnectivityEvent extends MessageNano {
    public static final int APF_PROGRAM_EVENT_FIELD_NUMBER = 9;
    
    public static final int APF_STATISTICS_FIELD_NUMBER = 10;
    
    public static final int CONNECT_STATISTICS_FIELD_NUMBER = 14;
    
    public static final int DEFAULT_NETWORK_EVENT_FIELD_NUMBER = 2;
    
    public static final int DHCP_EVENT_FIELD_NUMBER = 6;
    
    public static final int DNS_LATENCIES_FIELD_NUMBER = 13;
    
    public static final int DNS_LOOKUP_BATCH_FIELD_NUMBER = 5;
    
    public static final int IP_PROVISIONING_EVENT_FIELD_NUMBER = 7;
    
    public static final int IP_REACHABILITY_EVENT_FIELD_NUMBER = 3;
    
    public static final int NETWORK_EVENT_FIELD_NUMBER = 4;
    
    public static final int NETWORK_STATS_FIELD_NUMBER = 19;
    
    public static final int RA_EVENT_FIELD_NUMBER = 11;
    
    public static final int VALIDATION_PROBE_EVENT_FIELD_NUMBER = 8;
    
    public static final int WAKEUP_STATS_FIELD_NUMBER = 20;
    
    private static volatile IpConnectivityEvent[] _emptyArray;
    
    private int eventCase_ = 0;
    
    private Object event_;
    
    public String ifName;
    
    public int linkLayer;
    
    public int networkId;
    
    public long timeMs;
    
    public long transports;
    
    public int getEventCase() {
      return this.eventCase_;
    }
    
    public IpConnectivityEvent clearEvent() {
      this.eventCase_ = 0;
      this.event_ = null;
      return this;
    }
    
    public static IpConnectivityEvent[] emptyArray() {
      if (_emptyArray == null)
        synchronized (InternalNano.LAZY_INIT_LOCK) {
          if (_emptyArray == null)
            _emptyArray = new IpConnectivityEvent[0]; 
        }  
      return _emptyArray;
    }
    
    public boolean hasDefaultNetworkEvent() {
      boolean bool;
      if (this.eventCase_ == 2) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public IpConnectivityLogClass.DefaultNetworkEvent getDefaultNetworkEvent() {
      if (this.eventCase_ == 2)
        return (IpConnectivityLogClass.DefaultNetworkEvent)this.event_; 
      return null;
    }
    
    public IpConnectivityEvent setDefaultNetworkEvent(IpConnectivityLogClass.DefaultNetworkEvent param1DefaultNetworkEvent) {
      if (param1DefaultNetworkEvent != null) {
        this.eventCase_ = 2;
        this.event_ = param1DefaultNetworkEvent;
        return this;
      } 
      throw null;
    }
    
    public boolean hasIpReachabilityEvent() {
      boolean bool;
      if (this.eventCase_ == 3) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public IpConnectivityLogClass.IpReachabilityEvent getIpReachabilityEvent() {
      if (this.eventCase_ == 3)
        return (IpConnectivityLogClass.IpReachabilityEvent)this.event_; 
      return null;
    }
    
    public IpConnectivityEvent setIpReachabilityEvent(IpConnectivityLogClass.IpReachabilityEvent param1IpReachabilityEvent) {
      if (param1IpReachabilityEvent != null) {
        this.eventCase_ = 3;
        this.event_ = param1IpReachabilityEvent;
        return this;
      } 
      throw null;
    }
    
    public boolean hasNetworkEvent() {
      boolean bool;
      if (this.eventCase_ == 4) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public IpConnectivityLogClass.NetworkEvent getNetworkEvent() {
      if (this.eventCase_ == 4)
        return (IpConnectivityLogClass.NetworkEvent)this.event_; 
      return null;
    }
    
    public IpConnectivityEvent setNetworkEvent(IpConnectivityLogClass.NetworkEvent param1NetworkEvent) {
      if (param1NetworkEvent != null) {
        this.eventCase_ = 4;
        this.event_ = param1NetworkEvent;
        return this;
      } 
      throw null;
    }
    
    public boolean hasDnsLookupBatch() {
      boolean bool;
      if (this.eventCase_ == 5) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public IpConnectivityLogClass.DNSLookupBatch getDnsLookupBatch() {
      if (this.eventCase_ == 5)
        return (IpConnectivityLogClass.DNSLookupBatch)this.event_; 
      return null;
    }
    
    public IpConnectivityEvent setDnsLookupBatch(IpConnectivityLogClass.DNSLookupBatch param1DNSLookupBatch) {
      if (param1DNSLookupBatch != null) {
        this.eventCase_ = 5;
        this.event_ = param1DNSLookupBatch;
        return this;
      } 
      throw null;
    }
    
    public boolean hasDnsLatencies() {
      boolean bool;
      if (this.eventCase_ == 13) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public IpConnectivityLogClass.DNSLatencies getDnsLatencies() {
      if (this.eventCase_ == 13)
        return (IpConnectivityLogClass.DNSLatencies)this.event_; 
      return null;
    }
    
    public IpConnectivityEvent setDnsLatencies(IpConnectivityLogClass.DNSLatencies param1DNSLatencies) {
      if (param1DNSLatencies != null) {
        this.eventCase_ = 13;
        this.event_ = param1DNSLatencies;
        return this;
      } 
      throw null;
    }
    
    public boolean hasConnectStatistics() {
      boolean bool;
      if (this.eventCase_ == 14) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public IpConnectivityLogClass.ConnectStatistics getConnectStatistics() {
      if (this.eventCase_ == 14)
        return (IpConnectivityLogClass.ConnectStatistics)this.event_; 
      return null;
    }
    
    public IpConnectivityEvent setConnectStatistics(IpConnectivityLogClass.ConnectStatistics param1ConnectStatistics) {
      if (param1ConnectStatistics != null) {
        this.eventCase_ = 14;
        this.event_ = param1ConnectStatistics;
        return this;
      } 
      throw null;
    }
    
    public boolean hasDhcpEvent() {
      boolean bool;
      if (this.eventCase_ == 6) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public IpConnectivityLogClass.DHCPEvent getDhcpEvent() {
      if (this.eventCase_ == 6)
        return (IpConnectivityLogClass.DHCPEvent)this.event_; 
      return null;
    }
    
    public IpConnectivityEvent setDhcpEvent(IpConnectivityLogClass.DHCPEvent param1DHCPEvent) {
      if (param1DHCPEvent != null) {
        this.eventCase_ = 6;
        this.event_ = param1DHCPEvent;
        return this;
      } 
      throw null;
    }
    
    public boolean hasIpProvisioningEvent() {
      boolean bool;
      if (this.eventCase_ == 7) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public IpConnectivityLogClass.IpProvisioningEvent getIpProvisioningEvent() {
      if (this.eventCase_ == 7)
        return (IpConnectivityLogClass.IpProvisioningEvent)this.event_; 
      return null;
    }
    
    public IpConnectivityEvent setIpProvisioningEvent(IpConnectivityLogClass.IpProvisioningEvent param1IpProvisioningEvent) {
      if (param1IpProvisioningEvent != null) {
        this.eventCase_ = 7;
        this.event_ = param1IpProvisioningEvent;
        return this;
      } 
      throw null;
    }
    
    public boolean hasValidationProbeEvent() {
      boolean bool;
      if (this.eventCase_ == 8) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public IpConnectivityLogClass.ValidationProbeEvent getValidationProbeEvent() {
      if (this.eventCase_ == 8)
        return (IpConnectivityLogClass.ValidationProbeEvent)this.event_; 
      return null;
    }
    
    public IpConnectivityEvent setValidationProbeEvent(IpConnectivityLogClass.ValidationProbeEvent param1ValidationProbeEvent) {
      if (param1ValidationProbeEvent != null) {
        this.eventCase_ = 8;
        this.event_ = param1ValidationProbeEvent;
        return this;
      } 
      throw null;
    }
    
    public boolean hasApfProgramEvent() {
      boolean bool;
      if (this.eventCase_ == 9) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public IpConnectivityLogClass.ApfProgramEvent getApfProgramEvent() {
      if (this.eventCase_ == 9)
        return (IpConnectivityLogClass.ApfProgramEvent)this.event_; 
      return null;
    }
    
    public IpConnectivityEvent setApfProgramEvent(IpConnectivityLogClass.ApfProgramEvent param1ApfProgramEvent) {
      if (param1ApfProgramEvent != null) {
        this.eventCase_ = 9;
        this.event_ = param1ApfProgramEvent;
        return this;
      } 
      throw null;
    }
    
    public boolean hasApfStatistics() {
      boolean bool;
      if (this.eventCase_ == 10) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public IpConnectivityLogClass.ApfStatistics getApfStatistics() {
      if (this.eventCase_ == 10)
        return (IpConnectivityLogClass.ApfStatistics)this.event_; 
      return null;
    }
    
    public IpConnectivityEvent setApfStatistics(IpConnectivityLogClass.ApfStatistics param1ApfStatistics) {
      if (param1ApfStatistics != null) {
        this.eventCase_ = 10;
        this.event_ = param1ApfStatistics;
        return this;
      } 
      throw null;
    }
    
    public boolean hasRaEvent() {
      boolean bool;
      if (this.eventCase_ == 11) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public IpConnectivityLogClass.RaEvent getRaEvent() {
      if (this.eventCase_ == 11)
        return (IpConnectivityLogClass.RaEvent)this.event_; 
      return null;
    }
    
    public IpConnectivityEvent setRaEvent(IpConnectivityLogClass.RaEvent param1RaEvent) {
      if (param1RaEvent != null) {
        this.eventCase_ = 11;
        this.event_ = param1RaEvent;
        return this;
      } 
      throw null;
    }
    
    public boolean hasNetworkStats() {
      boolean bool;
      if (this.eventCase_ == 19) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public IpConnectivityLogClass.NetworkStats getNetworkStats() {
      if (this.eventCase_ == 19)
        return (IpConnectivityLogClass.NetworkStats)this.event_; 
      return null;
    }
    
    public IpConnectivityEvent setNetworkStats(IpConnectivityLogClass.NetworkStats param1NetworkStats) {
      if (param1NetworkStats != null) {
        this.eventCase_ = 19;
        this.event_ = param1NetworkStats;
        return this;
      } 
      throw null;
    }
    
    public boolean hasWakeupStats() {
      boolean bool;
      if (this.eventCase_ == 20) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public IpConnectivityLogClass.WakeupStats getWakeupStats() {
      if (this.eventCase_ == 20)
        return (IpConnectivityLogClass.WakeupStats)this.event_; 
      return null;
    }
    
    public IpConnectivityEvent setWakeupStats(IpConnectivityLogClass.WakeupStats param1WakeupStats) {
      if (param1WakeupStats != null) {
        this.eventCase_ = 20;
        this.event_ = param1WakeupStats;
        return this;
      } 
      throw null;
    }
    
    public IpConnectivityEvent() {
      clear();
    }
    
    public IpConnectivityEvent clear() {
      this.timeMs = 0L;
      this.linkLayer = 0;
      this.networkId = 0;
      this.ifName = "";
      this.transports = 0L;
      clearEvent();
      this.cachedSize = -1;
      return this;
    }
    
    public void writeTo(CodedOutputByteBufferNano param1CodedOutputByteBufferNano) throws IOException {
      long l = this.timeMs;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(1, l); 
      if (this.eventCase_ == 2)
        param1CodedOutputByteBufferNano.writeMessage(2, (MessageNano)this.event_); 
      if (this.eventCase_ == 3)
        param1CodedOutputByteBufferNano.writeMessage(3, (MessageNano)this.event_); 
      if (this.eventCase_ == 4)
        param1CodedOutputByteBufferNano.writeMessage(4, (MessageNano)this.event_); 
      if (this.eventCase_ == 5)
        param1CodedOutputByteBufferNano.writeMessage(5, (MessageNano)this.event_); 
      if (this.eventCase_ == 6)
        param1CodedOutputByteBufferNano.writeMessage(6, (MessageNano)this.event_); 
      if (this.eventCase_ == 7)
        param1CodedOutputByteBufferNano.writeMessage(7, (MessageNano)this.event_); 
      if (this.eventCase_ == 8)
        param1CodedOutputByteBufferNano.writeMessage(8, (MessageNano)this.event_); 
      if (this.eventCase_ == 9)
        param1CodedOutputByteBufferNano.writeMessage(9, (MessageNano)this.event_); 
      if (this.eventCase_ == 10)
        param1CodedOutputByteBufferNano.writeMessage(10, (MessageNano)this.event_); 
      if (this.eventCase_ == 11)
        param1CodedOutputByteBufferNano.writeMessage(11, (MessageNano)this.event_); 
      if (this.eventCase_ == 13)
        param1CodedOutputByteBufferNano.writeMessage(13, (MessageNano)this.event_); 
      if (this.eventCase_ == 14)
        param1CodedOutputByteBufferNano.writeMessage(14, (MessageNano)this.event_); 
      int i = this.linkLayer;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(15, i); 
      i = this.networkId;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(16, i); 
      if (!this.ifName.equals(""))
        param1CodedOutputByteBufferNano.writeString(17, this.ifName); 
      l = this.transports;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(18, l); 
      if (this.eventCase_ == 19)
        param1CodedOutputByteBufferNano.writeMessage(19, (MessageNano)this.event_); 
      if (this.eventCase_ == 20)
        param1CodedOutputByteBufferNano.writeMessage(20, (MessageNano)this.event_); 
      super.writeTo(param1CodedOutputByteBufferNano);
    }
    
    protected int computeSerializedSize() {
      int i = super.computeSerializedSize();
      long l = this.timeMs;
      int j = i;
      if (l != 0L)
        j = i + CodedOutputByteBufferNano.computeInt64Size(1, l); 
      i = j;
      if (this.eventCase_ == 2) {
        MessageNano messageNano = (MessageNano)this.event_;
        i = j + CodedOutputByteBufferNano.computeMessageSize(2, messageNano);
      } 
      j = i;
      if (this.eventCase_ == 3) {
        MessageNano messageNano = (MessageNano)this.event_;
        j = i + CodedOutputByteBufferNano.computeMessageSize(3, messageNano);
      } 
      i = j;
      if (this.eventCase_ == 4) {
        MessageNano messageNano = (MessageNano)this.event_;
        i = j + CodedOutputByteBufferNano.computeMessageSize(4, messageNano);
      } 
      j = i;
      if (this.eventCase_ == 5) {
        MessageNano messageNano = (MessageNano)this.event_;
        j = i + CodedOutputByteBufferNano.computeMessageSize(5, messageNano);
      } 
      i = j;
      if (this.eventCase_ == 6) {
        MessageNano messageNano = (MessageNano)this.event_;
        i = j + CodedOutputByteBufferNano.computeMessageSize(6, messageNano);
      } 
      int k = i;
      if (this.eventCase_ == 7) {
        MessageNano messageNano = (MessageNano)this.event_;
        k = i + CodedOutputByteBufferNano.computeMessageSize(7, messageNano);
      } 
      j = k;
      if (this.eventCase_ == 8) {
        MessageNano messageNano = (MessageNano)this.event_;
        j = k + CodedOutputByteBufferNano.computeMessageSize(8, messageNano);
      } 
      i = j;
      if (this.eventCase_ == 9) {
        MessageNano messageNano = (MessageNano)this.event_;
        i = j + CodedOutputByteBufferNano.computeMessageSize(9, messageNano);
      } 
      j = i;
      if (this.eventCase_ == 10) {
        MessageNano messageNano = (MessageNano)this.event_;
        j = i + CodedOutputByteBufferNano.computeMessageSize(10, messageNano);
      } 
      i = j;
      if (this.eventCase_ == 11) {
        MessageNano messageNano = (MessageNano)this.event_;
        i = j + CodedOutputByteBufferNano.computeMessageSize(11, messageNano);
      } 
      j = i;
      if (this.eventCase_ == 13) {
        MessageNano messageNano = (MessageNano)this.event_;
        j = i + CodedOutputByteBufferNano.computeMessageSize(13, messageNano);
      } 
      i = j;
      if (this.eventCase_ == 14) {
        MessageNano messageNano = (MessageNano)this.event_;
        i = j + CodedOutputByteBufferNano.computeMessageSize(14, messageNano);
      } 
      k = this.linkLayer;
      j = i;
      if (k != 0)
        j = i + CodedOutputByteBufferNano.computeInt32Size(15, k); 
      k = this.networkId;
      i = j;
      if (k != 0)
        i = j + CodedOutputByteBufferNano.computeInt32Size(16, k); 
      j = i;
      if (!this.ifName.equals("")) {
        String str = this.ifName;
        j = i + CodedOutputByteBufferNano.computeStringSize(17, str);
      } 
      l = this.transports;
      i = j;
      if (l != 0L)
        i = j + CodedOutputByteBufferNano.computeInt64Size(18, l); 
      j = i;
      if (this.eventCase_ == 19) {
        MessageNano messageNano = (MessageNano)this.event_;
        j = i + CodedOutputByteBufferNano.computeMessageSize(19, messageNano);
      } 
      i = j;
      if (this.eventCase_ == 20) {
        MessageNano messageNano = (MessageNano)this.event_;
        i = j + CodedOutputByteBufferNano.computeMessageSize(20, messageNano);
      } 
      return i;
    }
    
    public IpConnectivityEvent mergeFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      while (true) {
        int i = param1CodedInputByteBufferNano.readTag();
        switch (i) {
          default:
            if (!WireFormatNano.parseUnknownField(param1CodedInputByteBufferNano, i))
              return this; 
            continue;
          case 162:
            if (this.eventCase_ != 20)
              this.event_ = new IpConnectivityLogClass.WakeupStats(); 
            param1CodedInputByteBufferNano.readMessage((MessageNano)this.event_);
            this.eventCase_ = 20;
            continue;
          case 154:
            if (this.eventCase_ != 19)
              this.event_ = new IpConnectivityLogClass.NetworkStats(); 
            param1CodedInputByteBufferNano.readMessage((MessageNano)this.event_);
            this.eventCase_ = 19;
            continue;
          case 144:
            this.transports = param1CodedInputByteBufferNano.readInt64();
            continue;
          case 138:
            this.ifName = param1CodedInputByteBufferNano.readString();
            continue;
          case 128:
            this.networkId = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 120:
            i = param1CodedInputByteBufferNano.readInt32();
            switch (i) {
              case 0:
              case 1:
              case 2:
              case 3:
              case 4:
              case 5:
              case 6:
              case 7:
              case 8:
              case 9:
                this.linkLayer = i;
                break;
            } 
            continue;
          case 114:
            if (this.eventCase_ != 14)
              this.event_ = new IpConnectivityLogClass.ConnectStatistics(); 
            param1CodedInputByteBufferNano.readMessage((MessageNano)this.event_);
            this.eventCase_ = 14;
            continue;
          case 106:
            if (this.eventCase_ != 13)
              this.event_ = new IpConnectivityLogClass.DNSLatencies(); 
            param1CodedInputByteBufferNano.readMessage((MessageNano)this.event_);
            this.eventCase_ = 13;
            continue;
          case 90:
            if (this.eventCase_ != 11)
              this.event_ = new IpConnectivityLogClass.RaEvent(); 
            param1CodedInputByteBufferNano.readMessage((MessageNano)this.event_);
            this.eventCase_ = 11;
            continue;
          case 82:
            if (this.eventCase_ != 10)
              this.event_ = new IpConnectivityLogClass.ApfStatistics(); 
            param1CodedInputByteBufferNano.readMessage((MessageNano)this.event_);
            this.eventCase_ = 10;
            continue;
          case 74:
            if (this.eventCase_ != 9)
              this.event_ = new IpConnectivityLogClass.ApfProgramEvent(); 
            param1CodedInputByteBufferNano.readMessage((MessageNano)this.event_);
            this.eventCase_ = 9;
            continue;
          case 66:
            if (this.eventCase_ != 8)
              this.event_ = new IpConnectivityLogClass.ValidationProbeEvent(); 
            param1CodedInputByteBufferNano.readMessage((MessageNano)this.event_);
            this.eventCase_ = 8;
            continue;
          case 58:
            if (this.eventCase_ != 7)
              this.event_ = new IpConnectivityLogClass.IpProvisioningEvent(); 
            param1CodedInputByteBufferNano.readMessage((MessageNano)this.event_);
            this.eventCase_ = 7;
            continue;
          case 50:
            if (this.eventCase_ != 6)
              this.event_ = new IpConnectivityLogClass.DHCPEvent(); 
            param1CodedInputByteBufferNano.readMessage((MessageNano)this.event_);
            this.eventCase_ = 6;
            continue;
          case 42:
            if (this.eventCase_ != 5)
              this.event_ = new IpConnectivityLogClass.DNSLookupBatch(); 
            param1CodedInputByteBufferNano.readMessage((MessageNano)this.event_);
            this.eventCase_ = 5;
            continue;
          case 34:
            if (this.eventCase_ != 4)
              this.event_ = new IpConnectivityLogClass.NetworkEvent(); 
            param1CodedInputByteBufferNano.readMessage((MessageNano)this.event_);
            this.eventCase_ = 4;
            continue;
          case 26:
            if (this.eventCase_ != 3)
              this.event_ = new IpConnectivityLogClass.IpReachabilityEvent(); 
            param1CodedInputByteBufferNano.readMessage((MessageNano)this.event_);
            this.eventCase_ = 3;
            continue;
          case 18:
            if (this.eventCase_ != 2)
              this.event_ = new IpConnectivityLogClass.DefaultNetworkEvent(); 
            param1CodedInputByteBufferNano.readMessage((MessageNano)this.event_);
            this.eventCase_ = 2;
            continue;
          case 8:
            this.timeMs = param1CodedInputByteBufferNano.readInt64();
            continue;
          case 0:
            break;
        } 
        break;
      } 
      return this;
    }
    
    public static IpConnectivityEvent parseFrom(byte[] param1ArrayOfbyte) throws InvalidProtocolBufferNanoException {
      return (IpConnectivityEvent)MessageNano.mergeFrom(new IpConnectivityEvent(), param1ArrayOfbyte);
    }
    
    public static IpConnectivityEvent parseFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      return (new IpConnectivityEvent()).mergeFrom(param1CodedInputByteBufferNano);
    }
  }
  
  class IpConnectivityLog extends MessageNano {
    private static volatile IpConnectivityLog[] _emptyArray;
    
    public int droppedEvents;
    
    public IpConnectivityLogClass.IpConnectivityEvent[] events;
    
    public int version;
    
    public static IpConnectivityLog[] emptyArray() {
      if (_emptyArray == null)
        synchronized (InternalNano.LAZY_INIT_LOCK) {
          if (_emptyArray == null)
            _emptyArray = new IpConnectivityLog[0]; 
        }  
      return _emptyArray;
    }
    
    public IpConnectivityLog() {
      clear();
    }
    
    public IpConnectivityLog clear() {
      this.events = IpConnectivityLogClass.IpConnectivityEvent.emptyArray();
      this.droppedEvents = 0;
      this.version = 0;
      this.cachedSize = -1;
      return this;
    }
    
    public void writeTo(CodedOutputByteBufferNano param1CodedOutputByteBufferNano) throws IOException {
      IpConnectivityLogClass.IpConnectivityEvent[] arrayOfIpConnectivityEvent = this.events;
      if (arrayOfIpConnectivityEvent != null && arrayOfIpConnectivityEvent.length > 0) {
        byte b = 0;
        while (true) {
          arrayOfIpConnectivityEvent = this.events;
          if (b < arrayOfIpConnectivityEvent.length) {
            IpConnectivityLogClass.IpConnectivityEvent ipConnectivityEvent = arrayOfIpConnectivityEvent[b];
            if (ipConnectivityEvent != null)
              param1CodedOutputByteBufferNano.writeMessage(1, ipConnectivityEvent); 
            b++;
            continue;
          } 
          break;
        } 
      } 
      int i = this.droppedEvents;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(2, i); 
      i = this.version;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(3, i); 
      super.writeTo(param1CodedOutputByteBufferNano);
    }
    
    protected int computeSerializedSize() {
      int i = super.computeSerializedSize();
      IpConnectivityLogClass.IpConnectivityEvent[] arrayOfIpConnectivityEvent = this.events;
      int j = i;
      if (arrayOfIpConnectivityEvent != null) {
        j = i;
        if (arrayOfIpConnectivityEvent.length > 0) {
          byte b = 0;
          while (true) {
            arrayOfIpConnectivityEvent = this.events;
            j = i;
            if (b < arrayOfIpConnectivityEvent.length) {
              IpConnectivityLogClass.IpConnectivityEvent ipConnectivityEvent = arrayOfIpConnectivityEvent[b];
              j = i;
              if (ipConnectivityEvent != null)
                j = i + CodedOutputByteBufferNano.computeMessageSize(1, ipConnectivityEvent); 
              b++;
              i = j;
              continue;
            } 
            break;
          } 
        } 
      } 
      int k = this.droppedEvents;
      i = j;
      if (k != 0)
        i = j + CodedOutputByteBufferNano.computeInt32Size(2, k); 
      k = this.version;
      j = i;
      if (k != 0)
        j = i + CodedOutputByteBufferNano.computeInt32Size(3, k); 
      return j;
    }
    
    public IpConnectivityLog mergeFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      while (true) {
        int i = param1CodedInputByteBufferNano.readTag();
        if (i != 0) {
          if (i != 10) {
            if (i != 16) {
              if (i != 24) {
                if (!WireFormatNano.parseUnknownField(param1CodedInputByteBufferNano, i))
                  return this; 
                continue;
              } 
              this.version = param1CodedInputByteBufferNano.readInt32();
              continue;
            } 
            this.droppedEvents = param1CodedInputByteBufferNano.readInt32();
            continue;
          } 
          int j = WireFormatNano.getRepeatedFieldArrayLength(param1CodedInputByteBufferNano, 10);
          IpConnectivityLogClass.IpConnectivityEvent[] arrayOfIpConnectivityEvent = this.events;
          if (arrayOfIpConnectivityEvent == null) {
            i = 0;
          } else {
            i = arrayOfIpConnectivityEvent.length;
          } 
          arrayOfIpConnectivityEvent = new IpConnectivityLogClass.IpConnectivityEvent[i + j];
          j = i;
          if (i != 0) {
            System.arraycopy(this.events, 0, arrayOfIpConnectivityEvent, 0, i);
            j = i;
          } 
          for (; j < arrayOfIpConnectivityEvent.length - 1; j++) {
            arrayOfIpConnectivityEvent[j] = new IpConnectivityLogClass.IpConnectivityEvent();
            param1CodedInputByteBufferNano.readMessage(arrayOfIpConnectivityEvent[j]);
            param1CodedInputByteBufferNano.readTag();
          } 
          arrayOfIpConnectivityEvent[j] = new IpConnectivityLogClass.IpConnectivityEvent();
          param1CodedInputByteBufferNano.readMessage(arrayOfIpConnectivityEvent[j]);
          this.events = arrayOfIpConnectivityEvent;
          continue;
        } 
        break;
      } 
      return this;
    }
    
    public static IpConnectivityLog parseFrom(byte[] param1ArrayOfbyte) throws InvalidProtocolBufferNanoException {
      return (IpConnectivityLog)MessageNano.mergeFrom(new IpConnectivityLog(), param1ArrayOfbyte);
    }
    
    public static IpConnectivityLog parseFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      return (new IpConnectivityLog()).mergeFrom(param1CodedInputByteBufferNano);
    }
  }
}
