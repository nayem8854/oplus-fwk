package com.android.server;

public final class VibratorServiceDumpProto {
  public static final long CURRENT_EXTERNAL_VIBRATION = 1146756268035L;
  
  public static final long CURRENT_VIBRATION = 1146756268033L;
  
  public static final long HAPTIC_FEEDBACK_INTENSITY = 1120986464262L;
  
  public static final long IS_VIBRATING = 1133871366146L;
  
  public static final long LOW_POWER_MODE = 1133871366149L;
  
  public static final long NOTIFICATION_INTENSITY = 1120986464263L;
  
  public static final long PREVIOUS_ALARM_VIBRATIONS = 2246267895819L;
  
  public static final long PREVIOUS_NOTIFICATION_VIBRATIONS = 2246267895818L;
  
  public static final long PREVIOUS_RING_VIBRATIONS = 2246267895817L;
  
  public static final long PREVIOUS_VIBRATIONS = 2246267895820L;
  
  public static final long RING_INTENSITY = 1120986464264L;
  
  public static final long VIBRATOR_UNDER_EXTERNAL_CONTROL = 1133871366148L;
}
