package com.android.server;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.DropBoxManager;
import android.os.Environment;
import android.os.FileObserver;
import android.os.FileUtils;
import android.os.RecoverySystem;
import android.os.SystemProperties;
import android.os.storage.StorageManager;
import android.provider.Downloads;
import android.text.TextUtils;
import android.util.AtomicFile;
import android.util.EventLog;
import android.util.Slog;
import com.android.internal.logging.MetricsLogger;
import com.android.internal.util.FastXmlSerializer;
import com.android.internal.util.FrameworkStatsLog;
import com.oplus.debug.ASSERT;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.xmlpull.v1.XmlPullParserException;

public class BootReceiver extends BroadcastReceiver {
  static {
    int i;
    if (SystemProperties.getInt("ro.debuggable", 0) == 1) {
      i = 196608;
    } else {
      i = 65536;
    } 
    LASTK_LOG_SIZE = i;
  }
  
  private static final File TOMBSTONE_DIR = new File("/data/tombstones");
  
  private static FileObserver sTombstoneObserver = null;
  
  static {
    sFile = new AtomicFile(new File(Environment.getDataSystemDirectory(), "log-files.xml"), "log-files");
    lastHeaderFile = new File(Environment.getDataSystemDirectory(), "last-header.txt");
    MOUNT_DURATION_PROPS_POSTFIX = new String[] { "early", "default", "late" };
    LAST_KMSG_FILES = new String[] { "/sys/fs/pstore/console-ramoops", "/proc/last_kmsg" };
  }
  
  private OplusBootReceiver mOplusBootReceiver = new OplusBootReceiver();
  
  private OplusBootReceiver.OplusBootReceiverCallback mCallback = new OplusBootReceiver.OplusBootReceiverCallback() {
      final BootReceiver this$0;
      
      public void onAddFileToDropBox(DropBoxManager param1DropBoxManager, HashMap<String, Long> param1HashMap, String param1String1, String param1String2, int param1Int, String param1String3) throws IOException {
        BootReceiver.addFileToDropBox(param1DropBoxManager, param1HashMap, param1String1, param1String2, param1Int, param1String3);
      }
    };
  
  private static final String FSCK_FS_MODIFIED = "FILE SYSTEM WAS MODIFIED";
  
  private static final String FSCK_PASS_PATTERN = "Pass ([1-9]E?):";
  
  private static final String FSCK_TREE_OPTIMIZATION_PATTERN = "Inode [0-9]+ extent tree.*could be shorter";
  
  private static final int FS_STAT_FS_FIXED = 1024;
  
  private static final String FS_STAT_PATTERN = "fs_stat,[^,]*/([^/,]+),(0x[0-9a-fA-F]+)";
  
  private static final int GMSCORE_LASTK_LOG_SIZE = 196608;
  
  private static final int LASTK_LOG_SIZE;
  
  private static final String LAST_HEADER_FILE = "last-header.txt";
  
  private static final String[] LAST_KMSG_FILES;
  
  private static final String LAST_SHUTDOWN_TIME_PATTERN = "powerctl_shutdown_time_ms:([0-9]+):([0-9]+)";
  
  private static final String LOG_FILES_FILE = "log-files.xml";
  
  private static final int LOG_SIZE = 4194304;
  
  private static final String METRIC_SHUTDOWN_TIME_START = "begin_shutdown";
  
  private static final String METRIC_SYSTEM_SERVER = "shutdown_system_server";
  
  private static final String[] MOUNT_DURATION_PROPS_POSTFIX;
  
  private static final String OLD_UPDATER_CLASS = "com.google.android.systemupdater.SystemUpdateReceiver";
  
  private static final String OLD_UPDATER_PACKAGE = "com.google.android.systemupdater";
  
  private static final String SHUTDOWN_METRICS_FILE = "/data/system/shutdown-metrics.txt";
  
  private static final String SHUTDOWN_TRON_METRICS_PREFIX = "shutdown_";
  
  private static final String TAG = "BootReceiver";
  
  private static final String TAG_TOMBSTONE = "SYSTEM_TOMBSTONE";
  
  private static final String TAG_TRUNCATED = "[[TRUNCATED]]\n";
  
  private static final int UMOUNT_STATUS_NOT_AVAILABLE = 4;
  
  private static final File lastHeaderFile;
  
  private static final AtomicFile sFile;
  
  public void onReceive(Context paramContext, Intent paramIntent) {
    this.mOplusBootReceiver.init(paramContext);
    this.mOplusBootReceiver.setOplusBootReceiverCallback(this.mCallback);
    Object object = new Object(this, paramContext);
    object.start();
  }
  
  private void removeOldUpdatePackages(Context paramContext) {
    Downloads.removeAllDownloadsByPackage(paramContext, "com.google.android.systemupdater", "com.google.android.systemupdater.SystemUpdateReceiver");
  }
  
  private String getPreviousBootHeaders() {
    try {
      return FileUtils.readTextFile(lastHeaderFile, 0, null);
    } catch (IOException iOException) {
      return null;
    } 
  }
  
  private String getCurrentBootHeaders() throws IOException {
    StringBuilder stringBuilder = new StringBuilder(512);
    stringBuilder.append("Build: ");
    stringBuilder.append(Build.FINGERPRINT);
    stringBuilder.append("\n");
    stringBuilder.append("Hardware: ");
    stringBuilder.append(Build.BOARD);
    stringBuilder.append("\n");
    stringBuilder.append("Revision: ");
    stringBuilder.append(SystemProperties.get("ro.revision", ""));
    stringBuilder.append("\n");
    stringBuilder.append("OTA_Version: ");
    OplusBootReceiver oplusBootReceiver = this.mOplusBootReceiver;
    stringBuilder.append(oplusBootReceiver.getOTAVersionString());
    stringBuilder.append("\n");
    stringBuilder.append("Bootloader: ");
    stringBuilder.append(Build.BOOTLOADER);
    stringBuilder.append("\n");
    stringBuilder.append("Radio: ");
    stringBuilder.append(Build.getRadioVersion());
    stringBuilder.append("\n");
    stringBuilder.append("Kernel: ");
    File file = new File("/proc/version");
    stringBuilder.append(FileUtils.readTextFile(file, 1024, "...\n"));
    stringBuilder.append("\n");
    return stringBuilder.toString();
  }
  
  private String getBootHeadersToLogAndUpdate() throws IOException {
    StringBuilder stringBuilder1;
    String str1 = getPreviousBootHeaders();
    String str2 = getCurrentBootHeaders();
    try {
      FileUtils.stringToFile(lastHeaderFile, str2);
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error writing ");
      stringBuilder.append(lastHeaderFile);
      Slog.e("BootReceiver", stringBuilder.toString(), iOException);
    } 
    if (str1 == null) {
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("isPrevious: false\n");
      stringBuilder1.append(str2);
      return stringBuilder1.toString();
    } 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("isPrevious: true\n");
    stringBuilder2.append((String)stringBuilder1);
    return stringBuilder2.toString();
  }
  
  private void logBootEvents(Context paramContext) throws IOException {
    String str1;
    final DropBoxManager db = (DropBoxManager)paramContext.getSystemService("dropbox");
    final String headers = getBootHeadersToLogAndUpdate();
    this.mOplusBootReceiver.printIsPanic();
    String str3 = RecoverySystem.handleAftermath(paramContext);
    if (str3 != null && dropBoxManager != null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str2);
      stringBuilder.append(str3);
      dropBoxManager.addText("SYSTEM_RECOVERY_LOG", stringBuilder.toString());
    } 
    this.mOplusBootReceiver.setLastKmsgFooter();
    HashMap<String, Long> hashMap = readTimestamps();
    if (SystemProperties.getLong("ro.runtime.firstboot", 0L) == 0L) {
      if (!StorageManager.inCryptKeeperBounce()) {
        str1 = Long.toString(System.currentTimeMillis());
        SystemProperties.set("ro.runtime.firstboot", str1);
      } 
      if (dropBoxManager != null)
        dropBoxManager.addText("SYSTEM_BOOT", str2); 
      this.mOplusBootReceiver.writeLogToPartitionAndDeleteFolderFilesThread(dropBoxManager, hashMap, str2);
      addFileToDropBox(dropBoxManager, hashMap, str2, "/cache/recovery/log", -4194304, "SYSTEM_RECOVERY_LOG");
      addFileToDropBox(dropBoxManager, hashMap, str2, "/cache/recovery/last_kmsg", -4194304, "SYSTEM_RECOVERY_KMSG");
      addAuditErrorsToDropBox(dropBoxManager, hashMap, str2, -4194304, "SYSTEM_AUDIT");
    } else {
      if (dropBoxManager != null)
        dropBoxManager.addText("SYSTEM_RESTART", str2); 
      this.mOplusBootReceiver.addFile(dropBoxManager, hashMap, str2, (Context)str1);
    } 
    this.mOplusBootReceiver.resetPanicFile();
    logFsShutdownTime();
    logFsMountTime();
    addFsckErrorsToDropBoxAndLogFsStat(dropBoxManager, hashMap, str2, -4194304, "SYSTEM_FSCK");
    logSystemServerShutdownTimeMetrics();
    File[] arrayOfFile = TOMBSTONE_DIR.listFiles();
    for (byte b = 0; arrayOfFile != null && b < arrayOfFile.length; b++) {
      if (arrayOfFile[b].isFile())
        addFileToDropBox(dropBoxManager, hashMap, str2, arrayOfFile[b].getPath(), 4194304, "SYSTEM_TOMBSTONE"); 
    } 
    writeTimestamps(hashMap);
    FileObserver fileObserver = new FileObserver(TOMBSTONE_DIR.getPath(), 256) {
        final BootReceiver this$0;
        
        final DropBoxManager val$db;
        
        final String val$headers;
        
        public void onEvent(int param1Int, String param1String) {
          HashMap<String, Long> hashMap = BootReceiver.readTimestamps();
          try {
            File file = new File();
            this(BootReceiver.TOMBSTONE_DIR, param1String);
            if (file.isFile() && file.getName().startsWith("tombstone_")) {
              BootReceiver.addFileToDropBox(db, hashMap, headers, file.getPath(), 4194304, "SYSTEM_TOMBSTONE");
              ASSERT.CopyTombstone(file.getPath());
            } 
          } catch (IOException iOException) {
            Slog.e("BootReceiver", "Can't log tombstone", iOException);
          } 
          BootReceiver.this.writeTimestamps(hashMap);
        }
      };
    fileObserver.startWatching();
    OplusBootReceiver.resetPanicState();
  }
  
  private static void addLastkToDropBox(DropBoxManager paramDropBoxManager, HashMap<String, Long> paramHashMap, String paramString1, String paramString2, String paramString3, int paramInt, String paramString4) throws IOException {
    int i = paramString1.length() + "[[TRUNCATED]]\n".length() + paramString2.length();
    if (LASTK_LOG_SIZE + i > 196608)
      if (196608 > i) {
        paramInt = -(196608 - i);
      } else {
        paramInt = 0;
      }  
    addFileWithFootersToDropBox(paramDropBoxManager, paramHashMap, paramString1, paramString2, paramString3, paramInt, paramString4);
  }
  
  static void addFileToDropBox(DropBoxManager paramDropBoxManager, HashMap<String, Long> paramHashMap, String paramString1, String paramString2, int paramInt, String paramString3) throws IOException {
    addFileWithFootersToDropBox(paramDropBoxManager, paramHashMap, paramString1, "", paramString2, paramInt, paramString3);
  }
  
  private static void addFileWithFootersToDropBox(DropBoxManager paramDropBoxManager, HashMap<String, Long> paramHashMap, String paramString1, String paramString2, String paramString3, int paramInt, String paramString4) throws IOException {
    if (paramDropBoxManager == null || !paramDropBoxManager.isTagEnabled(paramString4))
      return; 
    File file = new File(paramString3);
    long l = file.lastModified();
    if (l <= 0L)
      return; 
    if (paramHashMap.containsKey(paramString3) && ((Long)paramHashMap.get(paramString3)).longValue() == l && !paramString4.equals("SYSTEM_TOMBSTONE_CRASH"))
      return; 
    paramHashMap.put(paramString3, Long.valueOf(l));
    String str = FileUtils.readTextFile(file, paramInt, "[[TRUNCATED]]\n");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString1);
    stringBuilder.append(str);
    stringBuilder.append(paramString2);
    paramString1 = stringBuilder.toString();
    if (paramString4.equals("SYSTEM_TOMBSTONE") && str.contains(">>> system_server <<<"))
      addTextToDropBox(paramDropBoxManager, "system_server_native_crash", paramString1, paramString3, paramInt); 
    if (paramString4.equals("SYSTEM_TOMBSTONE"))
      FrameworkStatsLog.write(186); 
    addTextToDropBox(paramDropBoxManager, paramString4, paramString1, paramString3, paramInt);
  }
  
  private static void addTextToDropBox(DropBoxManager paramDropBoxManager, String paramString1, String paramString2, String paramString3, int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Copying ");
    stringBuilder.append(paramString3);
    stringBuilder.append(" to DropBox (");
    stringBuilder.append(paramString1);
    stringBuilder.append(")");
    Slog.i("BootReceiver", stringBuilder.toString());
    paramDropBoxManager.addText(paramString1, paramString2);
    EventLog.writeEvent(81002, new Object[] { paramString3, Integer.valueOf(paramInt), paramString1 });
  }
  
  private static void addAuditErrorsToDropBox(DropBoxManager paramDropBoxManager, HashMap<String, Long> paramHashMap, String paramString1, int paramInt, String paramString2) throws IOException {
    if (paramDropBoxManager == null || !paramDropBoxManager.isTagEnabled(paramString2))
      return; 
    Slog.i("BootReceiver", "Copying audit failures to DropBox");
    File file = new File("/proc/last_kmsg");
    long l1 = file.lastModified();
    long l2 = l1;
    if (l1 <= 0L) {
      file = new File("/sys/fs/pstore/console-ramoops");
      l1 = file.lastModified();
      l2 = l1;
      if (l1 <= 0L) {
        file = new File("/sys/fs/pstore/console-ramoops-0");
        l2 = file.lastModified();
      } 
    } 
    if (l2 <= 0L)
      return; 
    if (paramHashMap.containsKey(paramString2) && ((Long)paramHashMap.get(paramString2)).longValue() == l2)
      return; 
    paramHashMap.put(paramString2, Long.valueOf(l2));
    String str = FileUtils.readTextFile(file, paramInt, "[[TRUNCATED]]\n");
    StringBuilder stringBuilder1 = new StringBuilder();
    String[] arrayOfString;
    int i;
    for (arrayOfString = str.split("\n"), i = arrayOfString.length, paramInt = 0; paramInt < i; ) {
      String str1 = arrayOfString[paramInt];
      if (str1.contains("audit")) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(str1);
        stringBuilder.append("\n");
        stringBuilder1.append(stringBuilder.toString());
      } 
      paramInt++;
    } 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("Copied ");
    stringBuilder2.append(stringBuilder1.toString().length());
    stringBuilder2.append(" worth of audits to DropBox");
    Slog.i("BootReceiver", stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(paramString1);
    stringBuilder2.append(stringBuilder1.toString());
    paramDropBoxManager.addText(paramString2, stringBuilder2.toString());
  }
  
  private static void addFsckErrorsToDropBoxAndLogFsStat(DropBoxManager paramDropBoxManager, HashMap<String, Long> paramHashMap, String paramString1, int paramInt, String paramString2) throws IOException {
    boolean bool1;
    if (paramDropBoxManager == null || !paramDropBoxManager.isTagEnabled(paramString2)) {
      bool1 = false;
    } else {
      bool1 = true;
    } 
    Slog.i("BootReceiver", "Checking for fsck errors");
    File file = new File("/dev/fscklogs/log");
    long l = file.lastModified();
    if (l <= 0L)
      return; 
    String str = FileUtils.readTextFile(file, paramInt, "[[TRUNCATED]]\n");
    Pattern pattern = Pattern.compile("fs_stat,[^,]*/([^/,]+),(0x[0-9a-fA-F]+)");
    String[] arrayOfString = str.split("\n");
    byte b1 = 0;
    int i;
    boolean bool2;
    byte b2, b3;
    for (i = arrayOfString.length, bool2 = false, b2 = 0, b3 = 0; b2 < i; ) {
      str = arrayOfString[b2];
      if (str.contains("FILE SYSTEM WAS MODIFIED")) {
        bool2 = true;
      } else if (str.contains("fs_stat")) {
        Matcher matcher = pattern.matcher(str);
        if (matcher.find()) {
          handleFsckFsStat(matcher, arrayOfString, b1, b3);
          b1 = b3;
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("cannot parse fs_stat:");
          stringBuilder.append(str);
          Slog.w("BootReceiver", stringBuilder.toString());
        } 
      } 
      b3++;
      b2++;
    } 
    if (bool1 && bool2)
      addFileToDropBox(paramDropBoxManager, paramHashMap, paramString1, "/dev/fscklogs/log", paramInt, paramString2); 
    file.delete();
  }
  
  private static void logFsMountTime() {
    for (String str : MOUNT_DURATION_PROPS_POSTFIX) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ro.boottime.init.mount_all.");
      stringBuilder.append(str);
      int i = SystemProperties.getInt(stringBuilder.toString(), 0);
      if (i != 0) {
        byte b = -1;
        int j = str.hashCode();
        if (j != 3314342) {
          if (j != 96278371) {
            if (j == 1544803905 && str.equals("default"))
              b = 1; 
          } else if (str.equals("early")) {
            b = 0;
          } 
        } else if (str.equals("late")) {
          b = 2;
        } 
        if (b != 0) {
          if (b != 1) {
            if (b != 2)
              continue; 
            b = 12;
          } else {
            b = 10;
          } 
        } else {
          b = 11;
        } 
        FrameworkStatsLog.write(239, b, i);
      } 
      continue;
    } 
  }
  
  private static void logSystemServerShutdownTimeMetrics() {
    File file = new File("/data/system/shutdown-metrics.txt");
    String str1 = null;
    String str2 = str1;
    if (file.exists())
      try {
        str2 = FileUtils.readTextFile(file, 0, null);
      } catch (IOException iOException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Problem reading ");
        stringBuilder.append(file);
        Slog.e("BootReceiver", stringBuilder.toString(), iOException);
        str2 = str1;
      }  
    if (!TextUtils.isEmpty(str2)) {
      String str3 = null;
      String str4 = null;
      String str5 = null;
      str1 = null;
      String[] arrayOfString = str2.split(",");
      int i;
      byte b;
      for (i = arrayOfString.length, b = 0; b < i; ) {
        String str7, str8, str6 = arrayOfString[b];
        String[] arrayOfString1 = str6.split(":");
        if (arrayOfString1.length != 2) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Wrong format of shutdown metrics - ");
          stringBuilder.append(str2);
          Slog.e("BootReceiver", stringBuilder.toString());
          str7 = str3;
          str8 = str4;
        } else {
          str6 = str1;
          if (arrayOfString1[0].startsWith("shutdown_")) {
            logTronShutdownMetric(arrayOfString1[0], arrayOfString1[1]);
            str6 = str1;
            if (arrayOfString1[0].equals("shutdown_system_server"))
              str6 = arrayOfString1[1]; 
          } 
          if (arrayOfString1[0].equals("reboot")) {
            str7 = arrayOfString1[1];
            str8 = str4;
            str1 = str6;
          } else if (arrayOfString1[0].equals("reason")) {
            str8 = arrayOfString1[1];
            str7 = str3;
            str1 = str6;
          } else {
            str7 = str3;
            str8 = str4;
            str1 = str6;
            if (arrayOfString1[0].equals("begin_shutdown")) {
              str5 = arrayOfString1[1];
              str1 = str6;
              str8 = str4;
              str7 = str3;
            } 
          } 
        } 
        b++;
        str3 = str7;
        str4 = str8;
      } 
      logStatsdShutdownAtom(str3, str4, str5, str1);
    } 
    file.delete();
  }
  
  private static void logTronShutdownMetric(String paramString1, String paramString2) {
    try {
      int i = Integer.parseInt(paramString2);
      if (i >= 0)
        MetricsLogger.histogram(null, paramString1, i); 
      return;
    } catch (NumberFormatException numberFormatException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Cannot parse metric ");
      stringBuilder.append(paramString1);
      stringBuilder.append(" int value - ");
      stringBuilder.append(paramString2);
      Slog.e("BootReceiver", stringBuilder.toString());
      return;
    } 
  }
  
  private static void logStatsdShutdownAtom(String paramString1, String paramString2, String paramString3, String paramString4) {
    boolean bool;
    long l3;
    String str = "<EMPTY>";
    long l1 = 0L;
    long l2 = 0L;
    if (paramString1 != null) {
      if (paramString1.equals("y")) {
        bool = true;
      } else {
        if (!paramString1.equals("n")) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Unexpected value for reboot : ");
          stringBuilder.append(paramString1);
          Slog.e("BootReceiver", stringBuilder.toString());
        } 
        bool = false;
      } 
    } else {
      Slog.e("BootReceiver", "No value received for reboot");
      bool = false;
    } 
    if (paramString2 != null) {
      paramString1 = paramString2;
    } else {
      Slog.e("BootReceiver", "No value received for shutdown reason");
      paramString1 = str;
    } 
    if (paramString3 != null) {
      try {
        l1 = l3 = Long.parseLong(paramString3);
        l3 = l1;
      } catch (NumberFormatException numberFormatException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Cannot parse shutdown start time: ");
        stringBuilder.append(paramString3);
        Slog.e("BootReceiver", stringBuilder.toString());
        l3 = l1;
      } 
    } else {
      Slog.e("BootReceiver", "No value received for shutdown start time");
      l3 = l1;
    } 
    if (paramString4 != null) {
      try {
        l1 = Long.parseLong(paramString4);
      } catch (NumberFormatException numberFormatException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Cannot parse shutdown duration: ");
        stringBuilder.append(paramString3);
        Slog.e("BootReceiver", stringBuilder.toString());
        l1 = l2;
      } 
    } else {
      Slog.e("BootReceiver", "No value received for shutdown duration");
      l1 = l2;
    } 
    FrameworkStatsLog.write(56, bool, paramString1, l3, l1);
  }
  
  private static void logFsShutdownTime() {
    File file;
    String str = null;
    String[] arrayOfString = LAST_KMSG_FILES;
    int i = arrayOfString.length, j = 0;
    while (true) {
      String str1 = str;
      if (j < i) {
        str1 = arrayOfString[j];
        file = new File(str1);
        if (!file.exists()) {
          j++;
          continue;
        } 
      } 
      break;
    } 
    if (file == null)
      return; 
    try {
      str = FileUtils.readTextFile(file, -16384, null);
      Pattern pattern = Pattern.compile("powerctl_shutdown_time_ms:([0-9]+):([0-9]+)", 8);
      Matcher matcher = pattern.matcher(str);
      if (matcher.find()) {
        j = Integer.parseInt(matcher.group(1));
        FrameworkStatsLog.write(239, 9, j);
        j = Integer.parseInt(matcher.group(2));
        FrameworkStatsLog.write(242, 2, j);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("boot_fs_shutdown,");
        stringBuilder.append(matcher.group(1));
        stringBuilder.append(",");
        stringBuilder.append(matcher.group(2));
        Slog.i("BootReceiver", stringBuilder.toString());
      } else {
        FrameworkStatsLog.write(242, 2, 4);
        Slog.w("BootReceiver", "boot_fs_shutdown, string not found");
      } 
      return;
    } catch (IOException iOException) {
      Slog.w("BootReceiver", "cannot read last msg", iOException);
      return;
    } 
  }
  
  public static int fixFsckFsStat(String paramString, int paramInt1, String[] paramArrayOfString, int paramInt2, int paramInt3) {
    int i = paramInt1;
    if ((i & 0x400) != 0) {
      Pattern pattern1 = Pattern.compile("Pass ([1-9]E?):");
      Pattern pattern2 = Pattern.compile("Inode [0-9]+ extent tree.*could be shorter");
      String str = "";
      boolean bool1 = false;
      paramInt1 = 0;
      int j = 0;
      boolean bool2 = false;
      String[] arrayOfString = null;
      while (true) {
        if (paramInt2 < paramInt3) {
          String str1 = paramArrayOfString[paramInt2];
          if (!str1.contains("FILE SYSTEM WAS MODIFIED")) {
            Matcher matcher;
            boolean bool3, bool4;
            if (str1.startsWith("Pass ")) {
              matcher = pattern1.matcher(str1);
              if (matcher.find())
                str = matcher.group(1); 
              bool3 = bool1;
              bool4 = j;
            } else if (matcher.startsWith("Inode ")) {
              Matcher matcher1 = pattern2.matcher((CharSequence)matcher);
              if (matcher1.find() && str.equals("1")) {
                bool3 = true;
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("fs_stat, partition:");
                stringBuilder.append(paramString);
                stringBuilder.append(" found tree optimization:");
                stringBuilder.append((String)matcher);
                Slog.i("BootReceiver", stringBuilder.toString());
                bool4 = j;
              } else {
                paramInt2 = 1;
                Matcher matcher2 = matcher;
                break;
              } 
            } else {
              Matcher matcher1;
              if (matcher.startsWith("[QUOTA WARNING]") && str.equals("5")) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("fs_stat, partition:");
                stringBuilder.append(paramString);
                stringBuilder.append(" found quota warning:");
                stringBuilder.append((String)matcher);
                Slog.i("BootReceiver", stringBuilder.toString());
                boolean bool = true;
                paramInt1 = 1;
                bool3 = bool1;
                bool4 = j;
                if (!bool1) {
                  matcher1 = matcher;
                  paramInt1 = bool;
                  paramInt2 = bool2;
                  break;
                } 
              } else {
                if (matcher.startsWith("Update quota info") && str.equals("5"))
                  continue; 
                if (matcher.startsWith("Timestamp(s) on inode") && 
                  matcher.contains("beyond 2310-04-04 are likely pre-1970") && 
                  str.equals("1")) {
                  StringBuilder stringBuilder = new StringBuilder();
                  stringBuilder.append("fs_stat, partition:");
                  stringBuilder.append(paramString);
                  stringBuilder.append(" found timestamp adjustment:");
                  stringBuilder.append((String)matcher);
                  Slog.i("BootReceiver", stringBuilder.toString());
                  j = paramInt2;
                  if (matcher1[paramInt2 + 1].contains("Fix? yes"))
                    j = paramInt2 + 1; 
                  bool4 = true;
                  bool3 = bool1;
                  paramInt2 = j;
                } else {
                  String str2 = matcher.trim();
                  if (!str2.isEmpty() && !str.isEmpty()) {
                    paramInt2 = 1;
                    String str3 = str2;
                    break;
                  } 
                  continue;
                } 
              } 
            } 
            paramInt2++;
            bool1 = bool3;
            j = bool4;
            continue;
          } 
        } 
        paramArrayOfString = arrayOfString;
        paramInt2 = bool2;
        break;
        k = j;
        bool = bool1;
      } 
      if (paramInt2 != 0) {
        paramInt1 = i;
        if (paramArrayOfString != null) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("fs_stat, partition:");
          stringBuilder.append(paramString);
          stringBuilder.append(" fix:");
          stringBuilder.append((String)paramArrayOfString);
          Slog.i("BootReceiver", stringBuilder.toString());
          paramInt1 = i;
        } 
      } else if (paramInt1 != 0 && !bool1) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("fs_stat, got quota fix without tree optimization, partition:");
        stringBuilder.append(paramString);
        Slog.i("BootReceiver", stringBuilder.toString());
        paramInt1 = i;
      } else {
        if (!bool1 || paramInt1 == 0) {
          paramInt1 = i;
          if (j != 0) {
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.append("fs_stat, partition:");
            stringBuilder1.append(paramString);
            stringBuilder1.append(" fix ignored");
            Slog.i("BootReceiver", stringBuilder1.toString());
            paramInt1 = i & 0xFFFFFBFF;
          } 
          return paramInt1;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("fs_stat, partition:");
        stringBuilder.append(paramString);
        stringBuilder.append(" fix ignored");
        Slog.i("BootReceiver", stringBuilder.toString());
        paramInt1 = i & 0xFFFFFBFF;
      } 
    } else {
      paramInt1 = i;
    } 
    return paramInt1;
  }
  
  private static void handleFsckFsStat(Matcher paramMatcher, String[] paramArrayOfString, int paramInt1, int paramInt2) {
    StringBuilder stringBuilder;
    String str = paramMatcher.group(1);
    try {
      int i = Integer.decode(paramMatcher.group(2)).intValue();
      paramInt1 = fixFsckFsStat(str, i, paramArrayOfString, paramInt1, paramInt2);
      if ("userdata".equals(str) || "data".equals(str))
        FrameworkStatsLog.write(242, 3, paramInt1); 
      stringBuilder = new StringBuilder();
      stringBuilder.append("fs_stat, partition:");
      stringBuilder.append(str);
      stringBuilder.append(" stat:0x");
      stringBuilder.append(Integer.toHexString(paramInt1));
      Slog.i("BootReceiver", stringBuilder.toString());
      return;
    } catch (NumberFormatException numberFormatException) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("cannot parse fs_stat: partition:");
      stringBuilder1.append(str);
      stringBuilder1.append(" stat:");
      stringBuilder1.append(stringBuilder.group(2));
      Slog.w("BootReceiver", stringBuilder1.toString());
      return;
    } 
  }
  
  private static HashMap<String, Long> readTimestamps() {
    synchronized (sFile) {
      HashMap<Object, Object> hashMap = new HashMap<>();
      this();
      byte b1 = 0;
      boolean bool1 = false, bool2 = false, bool3 = false, bool4 = false;
      byte b2 = 0;
      int i = b2, j = b1;
      boolean bool5 = bool1, bool6 = bool2, bool7 = bool3, bool8 = bool4;
      try {
        FileInputStream fileInputStream = sFile.openRead();
        try {
        
        } finally {
          if (fileInputStream != null)
            try {
              fileInputStream.close();
            } finally {
              fileInputStream = null;
              i = b2;
              j = b1;
              bool5 = bool1;
              bool6 = bool2;
              bool7 = bool3;
              bool8 = bool4;
            }  
          i = b2;
          j = b1;
          bool5 = bool1;
          bool6 = bool2;
          bool7 = bool3;
          bool8 = bool4;
        } 
      } catch (FileNotFoundException fileNotFoundException) {
        i = bool8;
        StringBuilder stringBuilder = new StringBuilder();
        i = bool8;
        this();
        i = bool8;
        stringBuilder.append("No existing last log timestamp file ");
        i = bool8;
        stringBuilder.append(sFile.getBaseFile());
        i = bool8;
        stringBuilder.append("; starting empty");
        i = bool8;
        Slog.i("BootReceiver", stringBuilder.toString());
        if (!bool8) {
          hashMap.clear();
          return (HashMap)hashMap;
        } 
      } catch (IOException iOException) {
        i = bool7;
        StringBuilder stringBuilder = new StringBuilder();
        i = bool7;
        this();
        i = bool7;
        stringBuilder.append("Failed parsing ");
        i = bool7;
        stringBuilder.append(iOException);
        i = bool7;
        Slog.w("BootReceiver", stringBuilder.toString());
        if (!bool7) {
          hashMap.clear();
          return (HashMap)hashMap;
        } 
      } catch (IllegalStateException illegalStateException) {
        i = bool6;
        StringBuilder stringBuilder = new StringBuilder();
        i = bool6;
        this();
        i = bool6;
        stringBuilder.append("Failed parsing ");
        i = bool6;
        stringBuilder.append(illegalStateException);
        i = bool6;
        Slog.w("BootReceiver", stringBuilder.toString());
        if (!bool6) {
          hashMap.clear();
          return (HashMap)hashMap;
        } 
      } catch (NullPointerException nullPointerException) {
        i = bool5;
        StringBuilder stringBuilder = new StringBuilder();
        i = bool5;
        this();
        i = bool5;
        stringBuilder.append("Failed parsing ");
        i = bool5;
        stringBuilder.append(nullPointerException);
        i = bool5;
        Slog.w("BootReceiver", stringBuilder.toString());
        if (!bool5) {
          hashMap.clear();
          return (HashMap)hashMap;
        } 
      } catch (XmlPullParserException xmlPullParserException) {
        i = j;
        StringBuilder stringBuilder = new StringBuilder();
        i = j;
        this();
        i = j;
        stringBuilder.append("Failed parsing ");
        i = j;
        stringBuilder.append(xmlPullParserException);
        i = j;
        Slog.w("BootReceiver", stringBuilder.toString());
        if (j == 0) {
          hashMap.clear();
          return (HashMap)hashMap;
        } 
      } finally {
        Exception exception;
      } 
      return (HashMap)hashMap;
    } 
  }
  
  private void writeTimestamps(HashMap<String, Long> paramHashMap) {
    AtomicFile atomicFile = sFile;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{android/util/AtomicFile}, name=null} */
    try {
      FileOutputStream fileOutputStream = sFile.startWrite();
      try {
        FastXmlSerializer fastXmlSerializer = new FastXmlSerializer();
        this();
        fastXmlSerializer.setOutput(fileOutputStream, StandardCharsets.UTF_8.name());
        fastXmlSerializer.startDocument(null, Boolean.valueOf(true));
        fastXmlSerializer.startTag(null, "log-files");
        Iterator<String> iterator = paramHashMap.keySet().iterator();
        while (iterator.hasNext()) {
          String str = iterator.next();
          fastXmlSerializer.startTag(null, "log");
          fastXmlSerializer.attribute(null, "filename", str);
          fastXmlSerializer.attribute(null, "timestamp", ((Long)paramHashMap.get(str)).toString());
          fastXmlSerializer.endTag(null, "log");
        } 
        fastXmlSerializer.endTag(null, "log-files");
        fastXmlSerializer.endDocument();
        sFile.finishWrite(fileOutputStream);
      } catch (IOException iOException) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Failed to write timestamp file, using the backup: ");
        stringBuilder.append(iOException);
        Slog.w("BootReceiver", stringBuilder.toString());
        sFile.failWrite(fileOutputStream);
      } 
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{android/util/AtomicFile}, name=null} */
      return;
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Failed to write timestamp file: ");
      stringBuilder.append(iOException);
      Slog.w("BootReceiver", stringBuilder.toString());
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{android/util/AtomicFile}, name=null} */
      return;
    } finally {}
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{android/util/AtomicFile}, name=null} */
    throw paramHashMap;
  }
}
