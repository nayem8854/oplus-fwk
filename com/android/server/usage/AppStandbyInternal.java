package com.android.server.usage;

import android.app.usage.AppStandbyInfo;
import android.app.usage.UsageEvents;
import android.content.Context;
import android.os.Looper;
import com.android.internal.util.IndentingPrintWriter;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Set;

public interface AppStandbyInternal {
  static AppStandbyInternal newAppStandbyController(ClassLoader paramClassLoader, Context paramContext, Looper paramLooper) {
    try {
      Class<?> clazz = Class.forName("com.android.server.usage.AppStandbyController", true, paramClassLoader);
      Constructor<?> constructor = clazz.getConstructor(new Class[] { Context.class, Looper.class });
      return (AppStandbyInternal)constructor.newInstance(new Object[] { paramContext, paramLooper });
    } catch (NoSuchMethodException|InstantiationException|IllegalAccessException|java.lang.reflect.InvocationTargetException|ClassNotFoundException noSuchMethodException) {
      throw new RuntimeException("Unable to instantiate AppStandbyController!", noSuchMethodException);
    } 
  }
  
  void addActiveDeviceAdmin(String paramString, int paramInt);
  
  void addListener(AppIdleStateChangeListener paramAppIdleStateChangeListener);
  
  void clearCarrierPrivilegedApps();
  
  void dumpState(String[] paramArrayOfString, PrintWriter paramPrintWriter);
  
  void dumpUsers(IndentingPrintWriter paramIndentingPrintWriter, int[] paramArrayOfint, List<String> paramList);
  
  void flushToDisk();
  
  int getAppId(String paramString);
  
  int getAppStandbyBucket(String paramString, int paramInt, long paramLong, boolean paramBoolean);
  
  List<AppStandbyInfo> getAppStandbyBuckets(int paramInt);
  
  int[] getIdleUidsForUser(int paramInt);
  
  long getTimeSinceLastJobRun(String paramString, int paramInt);
  
  void initializeDefaultsForSystemApps(int paramInt);
  
  boolean isAppIdleEnabled();
  
  boolean isAppIdleFiltered(String paramString, int paramInt1, int paramInt2, long paramLong);
  
  boolean isAppIdleFiltered(String paramString, int paramInt, long paramLong, boolean paramBoolean);
  
  boolean isInParole();
  
  void onAdminDataAvailable();
  
  void onBootPhase(int paramInt);
  
  void onUserRemoved(int paramInt);
  
  void postCheckIdleStates(int paramInt);
  
  void postOneTimeCheckIdleStates();
  
  void postReportContentProviderUsage(String paramString1, String paramString2, int paramInt);
  
  void postReportExemptedSyncStart(String paramString, int paramInt);
  
  void postReportSyncScheduled(String paramString, int paramInt, boolean paramBoolean);
  
  void removeListener(AppIdleStateChangeListener paramAppIdleStateChangeListener);
  
  void reportEvent(UsageEvents.Event paramEvent, int paramInt);
  
  void restrictApp(String paramString, int paramInt1, int paramInt2);
  
  void setActiveAdminApps(Set<String> paramSet, int paramInt);
  
  void setAppIdleAsync(String paramString, boolean paramBoolean, int paramInt);
  
  void setAppStandbyBucket(String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  void setAppStandbyBuckets(List<AppStandbyInfo> paramList, int paramInt1, int paramInt2, int paramInt3);
  
  void setLastJobRunTime(String paramString, int paramInt, long paramLong);
  
  public static abstract class AppIdleStateChangeListener {
    public abstract void onAppIdleStateChanged(String param1String, int param1Int1, boolean param1Boolean, int param1Int2, int param1Int3);
    
    public void onParoleStateChanged(boolean param1Boolean) {}
    
    public void onUserInteractionStarted(String param1String, int param1Int) {}
  }
}
