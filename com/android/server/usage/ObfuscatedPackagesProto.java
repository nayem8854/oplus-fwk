package com.android.server.usage;

public final class ObfuscatedPackagesProto {
  public static final long COUNTER = 1120986464257L;
  
  public static final long PACKAGES_MAP = 2246267895810L;
  
  public final class PackagesMap {
    public static final long PACKAGE_TOKEN = 1120986464257L;
    
    public static final long STRINGS = 2237677961218L;
    
    final ObfuscatedPackagesProto this$0;
  }
}
