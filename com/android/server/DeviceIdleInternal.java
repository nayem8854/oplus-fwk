package com.android.server;

import com.android.server.deviceidle.IDeviceIdleConstraint;

public interface DeviceIdleInternal {
  void addPowerSaveTempWhitelistApp(int paramInt1, String paramString1, long paramLong, int paramInt2, boolean paramBoolean, String paramString2);
  
  void addPowerSaveTempWhitelistAppDirect(int paramInt, long paramLong, boolean paramBoolean, String paramString);
  
  void exitIdle(String paramString);
  
  long getNotificationWhitelistDuration();
  
  int[] getPowerSaveTempWhitelistAppIds();
  
  int[] getPowerSaveWhitelistUserAppIds();
  
  boolean isAppOnWhitelist(int paramInt);
  
  boolean isInSmartDozeEearlyTime();
  
  boolean isInSmartDozeMotionMaintance();
  
  void onConstraintStateChanged(IDeviceIdleConstraint paramIDeviceIdleConstraint, boolean paramBoolean);
  
  void registerDeviceIdleConstraint(IDeviceIdleConstraint paramIDeviceIdleConstraint, String paramString, int paramInt);
  
  void registerStationaryListener(StationaryListener paramStationaryListener);
  
  void setAlarmsActive(boolean paramBoolean);
  
  void setJobsActive(boolean paramBoolean);
  
  void setSmartDozeInit();
  
  void smartDozeExitAlarmExemption();
  
  void smartDozeMoveGpsExemption(boolean paramBoolean);
  
  void unregisterDeviceIdleConstraint(IDeviceIdleConstraint paramIDeviceIdleConstraint);
  
  void unregisterStationaryListener(StationaryListener paramStationaryListener);
  
  public static interface StationaryListener {
    void onDeviceStationaryChanged(boolean param1Boolean);
  }
}
