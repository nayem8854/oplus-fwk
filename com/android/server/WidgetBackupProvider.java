package com.android.server;

import java.util.List;

public interface WidgetBackupProvider {
  List<String> getWidgetParticipants(int paramInt);
  
  byte[] getWidgetState(String paramString, int paramInt);
  
  void restoreFinished(int paramInt);
  
  void restoreStarting(int paramInt);
  
  void restoreWidgetState(String paramString, byte[] paramArrayOfbyte, int paramInt);
}
