package com.android.ims;

import android.os.Parcel;
import android.os.Parcelable;

public class RcsTypeIdPair implements Parcelable {
  public RcsTypeIdPair(int paramInt1, int paramInt2) {
    this.mType = paramInt1;
    this.mId = paramInt2;
  }
  
  public int getType() {
    return this.mType;
  }
  
  public void setType(int paramInt) {
    this.mType = paramInt;
  }
  
  public int getId() {
    return this.mId;
  }
  
  public void setId(int paramInt) {
    this.mId = paramInt;
  }
  
  public RcsTypeIdPair(Parcel paramParcel) {
    this.mType = paramParcel.readInt();
    this.mId = paramParcel.readInt();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mType);
    paramParcel.writeInt(this.mId);
  }
  
  public static final Parcelable.Creator<RcsTypeIdPair> CREATOR = new Parcelable.Creator<RcsTypeIdPair>() {
      public RcsTypeIdPair createFromParcel(Parcel param1Parcel) {
        return new RcsTypeIdPair(param1Parcel);
      }
      
      public RcsTypeIdPair[] newArray(int param1Int) {
        return new RcsTypeIdPair[param1Int];
      }
    };
  
  private int mId;
  
  private int mType;
}
