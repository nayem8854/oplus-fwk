package com.android.ims;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ImsConfigListener extends IInterface {
  void onGetFeatureResponse(int paramInt1, int paramInt2, int paramInt3, int paramInt4) throws RemoteException;
  
  void onGetVideoQuality(int paramInt1, int paramInt2) throws RemoteException;
  
  void onSetFeatureResponse(int paramInt1, int paramInt2, int paramInt3, int paramInt4) throws RemoteException;
  
  void onSetVideoQuality(int paramInt) throws RemoteException;
  
  class Default implements ImsConfigListener {
    public void onGetFeatureResponse(int param1Int1, int param1Int2, int param1Int3, int param1Int4) throws RemoteException {}
    
    public void onSetFeatureResponse(int param1Int1, int param1Int2, int param1Int3, int param1Int4) throws RemoteException {}
    
    public void onGetVideoQuality(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onSetVideoQuality(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ImsConfigListener {
    private static final String DESCRIPTOR = "com.android.ims.ImsConfigListener";
    
    static final int TRANSACTION_onGetFeatureResponse = 1;
    
    static final int TRANSACTION_onGetVideoQuality = 3;
    
    static final int TRANSACTION_onSetFeatureResponse = 2;
    
    static final int TRANSACTION_onSetVideoQuality = 4;
    
    public Stub() {
      attachInterface(this, "com.android.ims.ImsConfigListener");
    }
    
    public static ImsConfigListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.ims.ImsConfigListener");
      if (iInterface != null && iInterface instanceof ImsConfigListener)
        return (ImsConfigListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "onSetVideoQuality";
          } 
          return "onGetVideoQuality";
        } 
        return "onSetFeatureResponse";
      } 
      return "onGetFeatureResponse";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("com.android.ims.ImsConfigListener");
              return true;
            } 
            param1Parcel1.enforceInterface("com.android.ims.ImsConfigListener");
            param1Int1 = param1Parcel1.readInt();
            onSetVideoQuality(param1Int1);
            return true;
          } 
          param1Parcel1.enforceInterface("com.android.ims.ImsConfigListener");
          param1Int2 = param1Parcel1.readInt();
          param1Int1 = param1Parcel1.readInt();
          onGetVideoQuality(param1Int2, param1Int1);
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.ims.ImsConfigListener");
        int k = param1Parcel1.readInt();
        param1Int1 = param1Parcel1.readInt();
        param1Int2 = param1Parcel1.readInt();
        int m = param1Parcel1.readInt();
        onSetFeatureResponse(k, param1Int1, param1Int2, m);
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.ims.ImsConfigListener");
      int i = param1Parcel1.readInt();
      param1Int2 = param1Parcel1.readInt();
      param1Int1 = param1Parcel1.readInt();
      int j = param1Parcel1.readInt();
      onGetFeatureResponse(i, param1Int2, param1Int1, j);
      return true;
    }
    
    private static class Proxy implements ImsConfigListener {
      public static ImsConfigListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.ims.ImsConfigListener";
      }
      
      public void onGetFeatureResponse(int param2Int1, int param2Int2, int param2Int3, int param2Int4) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.ImsConfigListener");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          parcel.writeInt(param2Int4);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && ImsConfigListener.Stub.getDefaultImpl() != null) {
            ImsConfigListener.Stub.getDefaultImpl().onGetFeatureResponse(param2Int1, param2Int2, param2Int3, param2Int4);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSetFeatureResponse(int param2Int1, int param2Int2, int param2Int3, int param2Int4) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.ImsConfigListener");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          parcel.writeInt(param2Int4);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && ImsConfigListener.Stub.getDefaultImpl() != null) {
            ImsConfigListener.Stub.getDefaultImpl().onSetFeatureResponse(param2Int1, param2Int2, param2Int3, param2Int4);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onGetVideoQuality(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.ImsConfigListener");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && ImsConfigListener.Stub.getDefaultImpl() != null) {
            ImsConfigListener.Stub.getDefaultImpl().onGetVideoQuality(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSetVideoQuality(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.ImsConfigListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && ImsConfigListener.Stub.getDefaultImpl() != null) {
            ImsConfigListener.Stub.getDefaultImpl().onSetVideoQuality(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ImsConfigListener param1ImsConfigListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ImsConfigListener != null) {
          Proxy.sDefaultImpl = param1ImsConfigListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ImsConfigListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
