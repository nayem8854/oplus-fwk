package com.android.ims;

@Deprecated
public class ImsException extends Exception {
  private int mCode;
  
  public ImsException() {}
  
  public ImsException(String paramString, int paramInt) {
    super(stringBuilder.toString());
    this.mCode = paramInt;
  }
  
  public ImsException(String paramString, Throwable paramThrowable, int paramInt) {
    super(paramString, paramThrowable);
    this.mCode = paramInt;
  }
  
  public int getCode() {
    return this.mCode;
  }
}
