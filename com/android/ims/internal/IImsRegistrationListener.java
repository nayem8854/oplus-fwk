package com.android.ims.internal;

import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.telephony.ims.ImsReasonInfo;

public interface IImsRegistrationListener extends IInterface {
  void registrationAssociatedUriChanged(Uri[] paramArrayOfUri) throws RemoteException;
  
  void registrationChangeFailed(int paramInt, ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void registrationConnected() throws RemoteException;
  
  void registrationConnectedWithRadioTech(int paramInt) throws RemoteException;
  
  void registrationDisconnected(ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void registrationFeatureCapabilityChanged(int paramInt, int[] paramArrayOfint1, int[] paramArrayOfint2) throws RemoteException;
  
  void registrationProgressing() throws RemoteException;
  
  void registrationProgressingWithRadioTech(int paramInt) throws RemoteException;
  
  void registrationResumed() throws RemoteException;
  
  void registrationServiceCapabilityChanged(int paramInt1, int paramInt2) throws RemoteException;
  
  void registrationSuspended() throws RemoteException;
  
  void voiceMessageCountUpdate(int paramInt) throws RemoteException;
  
  class Default implements IImsRegistrationListener {
    public void registrationConnected() throws RemoteException {}
    
    public void registrationProgressing() throws RemoteException {}
    
    public void registrationConnectedWithRadioTech(int param1Int) throws RemoteException {}
    
    public void registrationProgressingWithRadioTech(int param1Int) throws RemoteException {}
    
    public void registrationDisconnected(ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void registrationResumed() throws RemoteException {}
    
    public void registrationSuspended() throws RemoteException {}
    
    public void registrationServiceCapabilityChanged(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void registrationFeatureCapabilityChanged(int param1Int, int[] param1ArrayOfint1, int[] param1ArrayOfint2) throws RemoteException {}
    
    public void voiceMessageCountUpdate(int param1Int) throws RemoteException {}
    
    public void registrationAssociatedUriChanged(Uri[] param1ArrayOfUri) throws RemoteException {}
    
    public void registrationChangeFailed(int param1Int, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsRegistrationListener {
    private static final String DESCRIPTOR = "com.android.ims.internal.IImsRegistrationListener";
    
    static final int TRANSACTION_registrationAssociatedUriChanged = 11;
    
    static final int TRANSACTION_registrationChangeFailed = 12;
    
    static final int TRANSACTION_registrationConnected = 1;
    
    static final int TRANSACTION_registrationConnectedWithRadioTech = 3;
    
    static final int TRANSACTION_registrationDisconnected = 5;
    
    static final int TRANSACTION_registrationFeatureCapabilityChanged = 9;
    
    static final int TRANSACTION_registrationProgressing = 2;
    
    static final int TRANSACTION_registrationProgressingWithRadioTech = 4;
    
    static final int TRANSACTION_registrationResumed = 6;
    
    static final int TRANSACTION_registrationServiceCapabilityChanged = 8;
    
    static final int TRANSACTION_registrationSuspended = 7;
    
    static final int TRANSACTION_voiceMessageCountUpdate = 10;
    
    public Stub() {
      attachInterface(this, "com.android.ims.internal.IImsRegistrationListener");
    }
    
    public static IImsRegistrationListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.ims.internal.IImsRegistrationListener");
      if (iInterface != null && iInterface instanceof IImsRegistrationListener)
        return (IImsRegistrationListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 12:
          return "registrationChangeFailed";
        case 11:
          return "registrationAssociatedUriChanged";
        case 10:
          return "voiceMessageCountUpdate";
        case 9:
          return "registrationFeatureCapabilityChanged";
        case 8:
          return "registrationServiceCapabilityChanged";
        case 7:
          return "registrationSuspended";
        case 6:
          return "registrationResumed";
        case 5:
          return "registrationDisconnected";
        case 4:
          return "registrationProgressingWithRadioTech";
        case 3:
          return "registrationConnectedWithRadioTech";
        case 2:
          return "registrationProgressing";
        case 1:
          break;
      } 
      return "registrationConnected";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      int[] arrayOfInt;
      if (param1Int1 != 1598968902) {
        Uri[] arrayOfUri;
        int[] arrayOfInt1;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 12:
            param1Parcel1.enforceInterface("com.android.ims.internal.IImsRegistrationListener");
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            registrationChangeFailed(param1Int1, (ImsReasonInfo)param1Parcel1);
            return true;
          case 11:
            param1Parcel1.enforceInterface("com.android.ims.internal.IImsRegistrationListener");
            arrayOfUri = (Uri[])param1Parcel1.createTypedArray(Uri.CREATOR);
            registrationAssociatedUriChanged(arrayOfUri);
            return true;
          case 10:
            arrayOfUri.enforceInterface("com.android.ims.internal.IImsRegistrationListener");
            param1Int1 = arrayOfUri.readInt();
            voiceMessageCountUpdate(param1Int1);
            return true;
          case 9:
            arrayOfUri.enforceInterface("com.android.ims.internal.IImsRegistrationListener");
            param1Int1 = arrayOfUri.readInt();
            arrayOfInt = arrayOfUri.createIntArray();
            arrayOfInt1 = arrayOfUri.createIntArray();
            registrationFeatureCapabilityChanged(param1Int1, arrayOfInt, arrayOfInt1);
            return true;
          case 8:
            arrayOfInt1.enforceInterface("com.android.ims.internal.IImsRegistrationListener");
            param1Int2 = arrayOfInt1.readInt();
            param1Int1 = arrayOfInt1.readInt();
            registrationServiceCapabilityChanged(param1Int2, param1Int1);
            return true;
          case 7:
            arrayOfInt1.enforceInterface("com.android.ims.internal.IImsRegistrationListener");
            registrationSuspended();
            return true;
          case 6:
            arrayOfInt1.enforceInterface("com.android.ims.internal.IImsRegistrationListener");
            registrationResumed();
            return true;
          case 5:
            arrayOfInt1.enforceInterface("com.android.ims.internal.IImsRegistrationListener");
            if (arrayOfInt1.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)arrayOfInt1);
            } else {
              arrayOfInt1 = null;
            } 
            registrationDisconnected((ImsReasonInfo)arrayOfInt1);
            return true;
          case 4:
            arrayOfInt1.enforceInterface("com.android.ims.internal.IImsRegistrationListener");
            param1Int1 = arrayOfInt1.readInt();
            registrationProgressingWithRadioTech(param1Int1);
            return true;
          case 3:
            arrayOfInt1.enforceInterface("com.android.ims.internal.IImsRegistrationListener");
            param1Int1 = arrayOfInt1.readInt();
            registrationConnectedWithRadioTech(param1Int1);
            return true;
          case 2:
            arrayOfInt1.enforceInterface("com.android.ims.internal.IImsRegistrationListener");
            registrationProgressing();
            return true;
          case 1:
            break;
        } 
        arrayOfInt1.enforceInterface("com.android.ims.internal.IImsRegistrationListener");
        registrationConnected();
        return true;
      } 
      arrayOfInt.writeString("com.android.ims.internal.IImsRegistrationListener");
      return true;
    }
    
    private static class Proxy implements IImsRegistrationListener {
      public static IImsRegistrationListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.ims.internal.IImsRegistrationListener";
      }
      
      public void registrationConnected() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsRegistrationListener");
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IImsRegistrationListener.Stub.getDefaultImpl() != null) {
            IImsRegistrationListener.Stub.getDefaultImpl().registrationConnected();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void registrationProgressing() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsRegistrationListener");
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IImsRegistrationListener.Stub.getDefaultImpl() != null) {
            IImsRegistrationListener.Stub.getDefaultImpl().registrationProgressing();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void registrationConnectedWithRadioTech(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsRegistrationListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IImsRegistrationListener.Stub.getDefaultImpl() != null) {
            IImsRegistrationListener.Stub.getDefaultImpl().registrationConnectedWithRadioTech(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void registrationProgressingWithRadioTech(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsRegistrationListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IImsRegistrationListener.Stub.getDefaultImpl() != null) {
            IImsRegistrationListener.Stub.getDefaultImpl().registrationProgressingWithRadioTech(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void registrationDisconnected(ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsRegistrationListener");
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IImsRegistrationListener.Stub.getDefaultImpl() != null) {
            IImsRegistrationListener.Stub.getDefaultImpl().registrationDisconnected(param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void registrationResumed() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsRegistrationListener");
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IImsRegistrationListener.Stub.getDefaultImpl() != null) {
            IImsRegistrationListener.Stub.getDefaultImpl().registrationResumed();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void registrationSuspended() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsRegistrationListener");
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IImsRegistrationListener.Stub.getDefaultImpl() != null) {
            IImsRegistrationListener.Stub.getDefaultImpl().registrationSuspended();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void registrationServiceCapabilityChanged(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsRegistrationListener");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IImsRegistrationListener.Stub.getDefaultImpl() != null) {
            IImsRegistrationListener.Stub.getDefaultImpl().registrationServiceCapabilityChanged(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void registrationFeatureCapabilityChanged(int param2Int, int[] param2ArrayOfint1, int[] param2ArrayOfint2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsRegistrationListener");
          parcel.writeInt(param2Int);
          parcel.writeIntArray(param2ArrayOfint1);
          parcel.writeIntArray(param2ArrayOfint2);
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IImsRegistrationListener.Stub.getDefaultImpl() != null) {
            IImsRegistrationListener.Stub.getDefaultImpl().registrationFeatureCapabilityChanged(param2Int, param2ArrayOfint1, param2ArrayOfint2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void voiceMessageCountUpdate(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsRegistrationListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel, null, 1);
          if (!bool && IImsRegistrationListener.Stub.getDefaultImpl() != null) {
            IImsRegistrationListener.Stub.getDefaultImpl().voiceMessageCountUpdate(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void registrationAssociatedUriChanged(Uri[] param2ArrayOfUri) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsRegistrationListener");
          parcel.writeTypedArray((Parcelable[])param2ArrayOfUri, 0);
          boolean bool = this.mRemote.transact(11, parcel, null, 1);
          if (!bool && IImsRegistrationListener.Stub.getDefaultImpl() != null) {
            IImsRegistrationListener.Stub.getDefaultImpl().registrationAssociatedUriChanged(param2ArrayOfUri);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void registrationChangeFailed(int param2Int, ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsRegistrationListener");
          parcel.writeInt(param2Int);
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(12, parcel, null, 1);
          if (!bool && IImsRegistrationListener.Stub.getDefaultImpl() != null) {
            IImsRegistrationListener.Stub.getDefaultImpl().registrationChangeFailed(param2Int, param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsRegistrationListener param1IImsRegistrationListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsRegistrationListener != null) {
          Proxy.sDefaultImpl = param1IImsRegistrationListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsRegistrationListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
