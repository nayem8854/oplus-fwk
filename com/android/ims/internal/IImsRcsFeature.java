package com.android.ims.internal;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IImsRcsFeature extends IInterface {
  class Default implements IImsRcsFeature {
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsRcsFeature {
    private static final String DESCRIPTOR = "com.android.ims.internal.IImsRcsFeature";
    
    public Stub() {
      attachInterface(this, "com.android.ims.internal.IImsRcsFeature");
    }
    
    public static IImsRcsFeature asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.ims.internal.IImsRcsFeature");
      if (iInterface != null && iInterface instanceof IImsRcsFeature)
        return (IImsRcsFeature)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      return null;
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902)
        return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
      param1Parcel2.writeString("com.android.ims.internal.IImsRcsFeature");
      return true;
    }
    
    private static class Proxy implements IImsRcsFeature {
      public static IImsRcsFeature sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.ims.internal.IImsRcsFeature";
      }
    }
    
    public static boolean setDefaultImpl(IImsRcsFeature param1IImsRcsFeature) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsRcsFeature != null) {
          Proxy.sDefaultImpl = param1IImsRcsFeature;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsRcsFeature getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
