package com.android.ims.internal;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IImsFeatureStatusCallback extends IInterface {
  void notifyImsFeatureStatus(int paramInt) throws RemoteException;
  
  class Default implements IImsFeatureStatusCallback {
    public void notifyImsFeatureStatus(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsFeatureStatusCallback {
    private static final String DESCRIPTOR = "com.android.ims.internal.IImsFeatureStatusCallback";
    
    static final int TRANSACTION_notifyImsFeatureStatus = 1;
    
    public Stub() {
      attachInterface(this, "com.android.ims.internal.IImsFeatureStatusCallback");
    }
    
    public static IImsFeatureStatusCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.ims.internal.IImsFeatureStatusCallback");
      if (iInterface != null && iInterface instanceof IImsFeatureStatusCallback)
        return (IImsFeatureStatusCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "notifyImsFeatureStatus";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.ims.internal.IImsFeatureStatusCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.ims.internal.IImsFeatureStatusCallback");
      param1Int1 = param1Parcel1.readInt();
      notifyImsFeatureStatus(param1Int1);
      return true;
    }
    
    class Proxy implements IImsFeatureStatusCallback {
      public static IImsFeatureStatusCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IImsFeatureStatusCallback.Stub this$0) {
        this.mRemote = (IBinder)this$0;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.ims.internal.IImsFeatureStatusCallback";
      }
      
      public void notifyImsFeatureStatus(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsFeatureStatusCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IImsFeatureStatusCallback.Stub.getDefaultImpl() != null) {
            IImsFeatureStatusCallback.Stub.getDefaultImpl().notifyImsFeatureStatus(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsFeatureStatusCallback param1IImsFeatureStatusCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsFeatureStatusCallback != null) {
          Proxy.sDefaultImpl = param1IImsFeatureStatusCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsFeatureStatusCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
