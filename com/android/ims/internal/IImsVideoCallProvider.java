package com.android.ims.internal;

import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telecom.VideoProfile;
import android.view.Surface;

public interface IImsVideoCallProvider extends IInterface {
  void requestCallDataUsage() throws RemoteException;
  
  void requestCameraCapabilities() throws RemoteException;
  
  void sendSessionModifyRequest(VideoProfile paramVideoProfile1, VideoProfile paramVideoProfile2) throws RemoteException;
  
  void sendSessionModifyResponse(VideoProfile paramVideoProfile) throws RemoteException;
  
  void setCallback(IImsVideoCallCallback paramIImsVideoCallCallback) throws RemoteException;
  
  void setCamera(String paramString, int paramInt) throws RemoteException;
  
  void setDeviceOrientation(int paramInt) throws RemoteException;
  
  void setDisplaySurface(Surface paramSurface) throws RemoteException;
  
  void setPauseImage(Uri paramUri) throws RemoteException;
  
  void setPreviewSurface(Surface paramSurface) throws RemoteException;
  
  void setZoom(float paramFloat) throws RemoteException;
  
  class Default implements IImsVideoCallProvider {
    public void setCallback(IImsVideoCallCallback param1IImsVideoCallCallback) throws RemoteException {}
    
    public void setCamera(String param1String, int param1Int) throws RemoteException {}
    
    public void setPreviewSurface(Surface param1Surface) throws RemoteException {}
    
    public void setDisplaySurface(Surface param1Surface) throws RemoteException {}
    
    public void setDeviceOrientation(int param1Int) throws RemoteException {}
    
    public void setZoom(float param1Float) throws RemoteException {}
    
    public void sendSessionModifyRequest(VideoProfile param1VideoProfile1, VideoProfile param1VideoProfile2) throws RemoteException {}
    
    public void sendSessionModifyResponse(VideoProfile param1VideoProfile) throws RemoteException {}
    
    public void requestCameraCapabilities() throws RemoteException {}
    
    public void requestCallDataUsage() throws RemoteException {}
    
    public void setPauseImage(Uri param1Uri) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsVideoCallProvider {
    private static final String DESCRIPTOR = "com.android.ims.internal.IImsVideoCallProvider";
    
    static final int TRANSACTION_requestCallDataUsage = 10;
    
    static final int TRANSACTION_requestCameraCapabilities = 9;
    
    static final int TRANSACTION_sendSessionModifyRequest = 7;
    
    static final int TRANSACTION_sendSessionModifyResponse = 8;
    
    static final int TRANSACTION_setCallback = 1;
    
    static final int TRANSACTION_setCamera = 2;
    
    static final int TRANSACTION_setDeviceOrientation = 5;
    
    static final int TRANSACTION_setDisplaySurface = 4;
    
    static final int TRANSACTION_setPauseImage = 11;
    
    static final int TRANSACTION_setPreviewSurface = 3;
    
    static final int TRANSACTION_setZoom = 6;
    
    public Stub() {
      attachInterface(this, "com.android.ims.internal.IImsVideoCallProvider");
    }
    
    public static IImsVideoCallProvider asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.ims.internal.IImsVideoCallProvider");
      if (iInterface != null && iInterface instanceof IImsVideoCallProvider)
        return (IImsVideoCallProvider)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 11:
          return "setPauseImage";
        case 10:
          return "requestCallDataUsage";
        case 9:
          return "requestCameraCapabilities";
        case 8:
          return "sendSessionModifyResponse";
        case 7:
          return "sendSessionModifyRequest";
        case 6:
          return "setZoom";
        case 5:
          return "setDeviceOrientation";
        case 4:
          return "setDisplaySurface";
        case 3:
          return "setPreviewSurface";
        case 2:
          return "setCamera";
        case 1:
          break;
      } 
      return "setCallback";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        float f;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 11:
            param1Parcel1.enforceInterface("com.android.ims.internal.IImsVideoCallProvider");
            if (param1Parcel1.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setPauseImage((Uri)param1Parcel1);
            return true;
          case 10:
            param1Parcel1.enforceInterface("com.android.ims.internal.IImsVideoCallProvider");
            requestCallDataUsage();
            return true;
          case 9:
            param1Parcel1.enforceInterface("com.android.ims.internal.IImsVideoCallProvider");
            requestCameraCapabilities();
            return true;
          case 8:
            param1Parcel1.enforceInterface("com.android.ims.internal.IImsVideoCallProvider");
            if (param1Parcel1.readInt() != 0) {
              VideoProfile videoProfile = (VideoProfile)VideoProfile.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            sendSessionModifyResponse((VideoProfile)param1Parcel1);
            return true;
          case 7:
            param1Parcel1.enforceInterface("com.android.ims.internal.IImsVideoCallProvider");
            if (param1Parcel1.readInt() != 0) {
              VideoProfile videoProfile = (VideoProfile)VideoProfile.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              VideoProfile videoProfile = (VideoProfile)VideoProfile.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            sendSessionModifyRequest((VideoProfile)param1Parcel2, (VideoProfile)param1Parcel1);
            return true;
          case 6:
            param1Parcel1.enforceInterface("com.android.ims.internal.IImsVideoCallProvider");
            f = param1Parcel1.readFloat();
            setZoom(f);
            return true;
          case 5:
            param1Parcel1.enforceInterface("com.android.ims.internal.IImsVideoCallProvider");
            param1Int1 = param1Parcel1.readInt();
            setDeviceOrientation(param1Int1);
            return true;
          case 4:
            param1Parcel1.enforceInterface("com.android.ims.internal.IImsVideoCallProvider");
            if (param1Parcel1.readInt() != 0) {
              Surface surface = (Surface)Surface.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setDisplaySurface((Surface)param1Parcel1);
            return true;
          case 3:
            param1Parcel1.enforceInterface("com.android.ims.internal.IImsVideoCallProvider");
            if (param1Parcel1.readInt() != 0) {
              Surface surface = (Surface)Surface.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setPreviewSurface((Surface)param1Parcel1);
            return true;
          case 2:
            param1Parcel1.enforceInterface("com.android.ims.internal.IImsVideoCallProvider");
            str = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            setCamera(str, param1Int1);
            return true;
          case 1:
            break;
        } 
        param1Parcel1.enforceInterface("com.android.ims.internal.IImsVideoCallProvider");
        IImsVideoCallCallback iImsVideoCallCallback = IImsVideoCallCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
        setCallback(iImsVideoCallCallback);
        return true;
      } 
      str.writeString("com.android.ims.internal.IImsVideoCallProvider");
      return true;
    }
    
    private static class Proxy implements IImsVideoCallProvider {
      public static IImsVideoCallProvider sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.ims.internal.IImsVideoCallProvider";
      }
      
      public void setCallback(IImsVideoCallCallback param2IImsVideoCallCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsVideoCallProvider");
          if (param2IImsVideoCallCallback != null) {
            iBinder = param2IImsVideoCallCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IImsVideoCallProvider.Stub.getDefaultImpl() != null) {
            IImsVideoCallProvider.Stub.getDefaultImpl().setCallback(param2IImsVideoCallCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setCamera(String param2String, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsVideoCallProvider");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IImsVideoCallProvider.Stub.getDefaultImpl() != null) {
            IImsVideoCallProvider.Stub.getDefaultImpl().setCamera(param2String, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setPreviewSurface(Surface param2Surface) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsVideoCallProvider");
          if (param2Surface != null) {
            parcel.writeInt(1);
            param2Surface.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IImsVideoCallProvider.Stub.getDefaultImpl() != null) {
            IImsVideoCallProvider.Stub.getDefaultImpl().setPreviewSurface(param2Surface);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setDisplaySurface(Surface param2Surface) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsVideoCallProvider");
          if (param2Surface != null) {
            parcel.writeInt(1);
            param2Surface.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IImsVideoCallProvider.Stub.getDefaultImpl() != null) {
            IImsVideoCallProvider.Stub.getDefaultImpl().setDisplaySurface(param2Surface);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setDeviceOrientation(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsVideoCallProvider");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IImsVideoCallProvider.Stub.getDefaultImpl() != null) {
            IImsVideoCallProvider.Stub.getDefaultImpl().setDeviceOrientation(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setZoom(float param2Float) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsVideoCallProvider");
          parcel.writeFloat(param2Float);
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IImsVideoCallProvider.Stub.getDefaultImpl() != null) {
            IImsVideoCallProvider.Stub.getDefaultImpl().setZoom(param2Float);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void sendSessionModifyRequest(VideoProfile param2VideoProfile1, VideoProfile param2VideoProfile2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsVideoCallProvider");
          if (param2VideoProfile1 != null) {
            parcel.writeInt(1);
            param2VideoProfile1.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2VideoProfile2 != null) {
            parcel.writeInt(1);
            param2VideoProfile2.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IImsVideoCallProvider.Stub.getDefaultImpl() != null) {
            IImsVideoCallProvider.Stub.getDefaultImpl().sendSessionModifyRequest(param2VideoProfile1, param2VideoProfile2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void sendSessionModifyResponse(VideoProfile param2VideoProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsVideoCallProvider");
          if (param2VideoProfile != null) {
            parcel.writeInt(1);
            param2VideoProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IImsVideoCallProvider.Stub.getDefaultImpl() != null) {
            IImsVideoCallProvider.Stub.getDefaultImpl().sendSessionModifyResponse(param2VideoProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void requestCameraCapabilities() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsVideoCallProvider");
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IImsVideoCallProvider.Stub.getDefaultImpl() != null) {
            IImsVideoCallProvider.Stub.getDefaultImpl().requestCameraCapabilities();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void requestCallDataUsage() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsVideoCallProvider");
          boolean bool = this.mRemote.transact(10, parcel, null, 1);
          if (!bool && IImsVideoCallProvider.Stub.getDefaultImpl() != null) {
            IImsVideoCallProvider.Stub.getDefaultImpl().requestCallDataUsage();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setPauseImage(Uri param2Uri) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsVideoCallProvider");
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(11, parcel, null, 1);
          if (!bool && IImsVideoCallProvider.Stub.getDefaultImpl() != null) {
            IImsVideoCallProvider.Stub.getDefaultImpl().setPauseImage(param2Uri);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsVideoCallProvider param1IImsVideoCallProvider) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsVideoCallProvider != null) {
          Proxy.sDefaultImpl = param1IImsVideoCallProvider;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsVideoCallProvider getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
