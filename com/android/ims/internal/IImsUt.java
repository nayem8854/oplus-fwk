package com.android.ims.internal;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IImsUt extends IInterface {
  void close() throws RemoteException;
  
  int queryCFForServiceClass(int paramInt1, String paramString, int paramInt2) throws RemoteException;
  
  int queryCLIP() throws RemoteException;
  
  int queryCLIR() throws RemoteException;
  
  int queryCOLP() throws RemoteException;
  
  int queryCOLR() throws RemoteException;
  
  int queryCallBarring(int paramInt) throws RemoteException;
  
  int queryCallBarringForServiceClass(int paramInt1, int paramInt2) throws RemoteException;
  
  int queryCallForward(int paramInt, String paramString) throws RemoteException;
  
  int queryCallWaiting() throws RemoteException;
  
  void setListener(IImsUtListener paramIImsUtListener) throws RemoteException;
  
  int transact(Bundle paramBundle) throws RemoteException;
  
  int updateCLIP(boolean paramBoolean) throws RemoteException;
  
  int updateCLIR(int paramInt) throws RemoteException;
  
  int updateCOLP(boolean paramBoolean) throws RemoteException;
  
  int updateCOLR(int paramInt) throws RemoteException;
  
  int updateCallBarring(int paramInt1, int paramInt2, String[] paramArrayOfString) throws RemoteException;
  
  int updateCallBarringForServiceClass(int paramInt1, int paramInt2, String[] paramArrayOfString, int paramInt3) throws RemoteException;
  
  int updateCallBarringWithPassword(int paramInt1, int paramInt2, String[] paramArrayOfString, int paramInt3, String paramString) throws RemoteException;
  
  int updateCallForward(int paramInt1, int paramInt2, String paramString, int paramInt3, int paramInt4) throws RemoteException;
  
  int updateCallWaiting(boolean paramBoolean, int paramInt) throws RemoteException;
  
  class Default implements IImsUt {
    public void close() throws RemoteException {}
    
    public int queryCallBarring(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int queryCallForward(int param1Int, String param1String) throws RemoteException {
      return 0;
    }
    
    public int queryCallWaiting() throws RemoteException {
      return 0;
    }
    
    public int queryCLIR() throws RemoteException {
      return 0;
    }
    
    public int queryCLIP() throws RemoteException {
      return 0;
    }
    
    public int queryCOLR() throws RemoteException {
      return 0;
    }
    
    public int queryCOLP() throws RemoteException {
      return 0;
    }
    
    public int transact(Bundle param1Bundle) throws RemoteException {
      return 0;
    }
    
    public int updateCallBarring(int param1Int1, int param1Int2, String[] param1ArrayOfString) throws RemoteException {
      return 0;
    }
    
    public int updateCallForward(int param1Int1, int param1Int2, String param1String, int param1Int3, int param1Int4) throws RemoteException {
      return 0;
    }
    
    public int updateCallWaiting(boolean param1Boolean, int param1Int) throws RemoteException {
      return 0;
    }
    
    public int updateCLIR(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int updateCLIP(boolean param1Boolean) throws RemoteException {
      return 0;
    }
    
    public int updateCOLR(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int updateCOLP(boolean param1Boolean) throws RemoteException {
      return 0;
    }
    
    public void setListener(IImsUtListener param1IImsUtListener) throws RemoteException {}
    
    public int queryCallBarringForServiceClass(int param1Int1, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public int updateCallBarringForServiceClass(int param1Int1, int param1Int2, String[] param1ArrayOfString, int param1Int3) throws RemoteException {
      return 0;
    }
    
    public int updateCallBarringWithPassword(int param1Int1, int param1Int2, String[] param1ArrayOfString, int param1Int3, String param1String) throws RemoteException {
      return 0;
    }
    
    public int queryCFForServiceClass(int param1Int1, String param1String, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsUt {
    private static final String DESCRIPTOR = "com.android.ims.internal.IImsUt";
    
    static final int TRANSACTION_close = 1;
    
    static final int TRANSACTION_queryCFForServiceClass = 21;
    
    static final int TRANSACTION_queryCLIP = 6;
    
    static final int TRANSACTION_queryCLIR = 5;
    
    static final int TRANSACTION_queryCOLP = 8;
    
    static final int TRANSACTION_queryCOLR = 7;
    
    static final int TRANSACTION_queryCallBarring = 2;
    
    static final int TRANSACTION_queryCallBarringForServiceClass = 18;
    
    static final int TRANSACTION_queryCallForward = 3;
    
    static final int TRANSACTION_queryCallWaiting = 4;
    
    static final int TRANSACTION_setListener = 17;
    
    static final int TRANSACTION_transact = 9;
    
    static final int TRANSACTION_updateCLIP = 14;
    
    static final int TRANSACTION_updateCLIR = 13;
    
    static final int TRANSACTION_updateCOLP = 16;
    
    static final int TRANSACTION_updateCOLR = 15;
    
    static final int TRANSACTION_updateCallBarring = 10;
    
    static final int TRANSACTION_updateCallBarringForServiceClass = 19;
    
    static final int TRANSACTION_updateCallBarringWithPassword = 20;
    
    static final int TRANSACTION_updateCallForward = 11;
    
    static final int TRANSACTION_updateCallWaiting = 12;
    
    public Stub() {
      attachInterface(this, "com.android.ims.internal.IImsUt");
    }
    
    public static IImsUt asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.ims.internal.IImsUt");
      if (iInterface != null && iInterface instanceof IImsUt)
        return (IImsUt)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 21:
          return "queryCFForServiceClass";
        case 20:
          return "updateCallBarringWithPassword";
        case 19:
          return "updateCallBarringForServiceClass";
        case 18:
          return "queryCallBarringForServiceClass";
        case 17:
          return "setListener";
        case 16:
          return "updateCOLP";
        case 15:
          return "updateCOLR";
        case 14:
          return "updateCLIP";
        case 13:
          return "updateCLIR";
        case 12:
          return "updateCallWaiting";
        case 11:
          return "updateCallForward";
        case 10:
          return "updateCallBarring";
        case 9:
          return "transact";
        case 8:
          return "queryCOLP";
        case 7:
          return "queryCOLR";
        case 6:
          return "queryCLIP";
        case 5:
          return "queryCLIR";
        case 4:
          return "queryCallWaiting";
        case 3:
          return "queryCallForward";
        case 2:
          return "queryCallBarring";
        case 1:
          break;
      } 
      return "close";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        String str2;
        IImsUtListener iImsUtListener;
        String arrayOfString1[], str1, str4, arrayOfString2[], str3;
        int i, j;
        boolean bool1 = false, bool2 = false, bool3 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 21:
            param1Parcel1.enforceInterface("com.android.ims.internal.IImsUt");
            param1Int1 = param1Parcel1.readInt();
            str4 = param1Parcel1.readString();
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = queryCFForServiceClass(param1Int1, str4, param1Int2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 20:
            param1Parcel1.enforceInterface("com.android.ims.internal.IImsUt");
            i = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            arrayOfString2 = param1Parcel1.createStringArray();
            param1Int2 = param1Parcel1.readInt();
            str2 = param1Parcel1.readString();
            param1Int1 = updateCallBarringWithPassword(i, param1Int1, arrayOfString2, param1Int2, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 19:
            str2.enforceInterface("com.android.ims.internal.IImsUt");
            i = str2.readInt();
            param1Int2 = str2.readInt();
            arrayOfString2 = str2.createStringArray();
            param1Int1 = str2.readInt();
            param1Int1 = updateCallBarringForServiceClass(i, param1Int2, arrayOfString2, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 18:
            str2.enforceInterface("com.android.ims.internal.IImsUt");
            param1Int1 = str2.readInt();
            param1Int2 = str2.readInt();
            param1Int1 = queryCallBarringForServiceClass(param1Int1, param1Int2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 17:
            str2.enforceInterface("com.android.ims.internal.IImsUt");
            iImsUtListener = IImsUtListener.Stub.asInterface(str2.readStrongBinder());
            setListener(iImsUtListener);
            param1Parcel2.writeNoException();
            return true;
          case 16:
            iImsUtListener.enforceInterface("com.android.ims.internal.IImsUt");
            if (iImsUtListener.readInt() != 0)
              bool3 = true; 
            param1Int1 = updateCOLP(bool3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 15:
            iImsUtListener.enforceInterface("com.android.ims.internal.IImsUt");
            param1Int1 = iImsUtListener.readInt();
            param1Int1 = updateCOLR(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 14:
            iImsUtListener.enforceInterface("com.android.ims.internal.IImsUt");
            bool3 = bool1;
            if (iImsUtListener.readInt() != 0)
              bool3 = true; 
            param1Int1 = updateCLIP(bool3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 13:
            iImsUtListener.enforceInterface("com.android.ims.internal.IImsUt");
            param1Int1 = iImsUtListener.readInt();
            param1Int1 = updateCLIR(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 12:
            iImsUtListener.enforceInterface("com.android.ims.internal.IImsUt");
            bool3 = bool2;
            if (iImsUtListener.readInt() != 0)
              bool3 = true; 
            param1Int1 = iImsUtListener.readInt();
            param1Int1 = updateCallWaiting(bool3, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 11:
            iImsUtListener.enforceInterface("com.android.ims.internal.IImsUt");
            param1Int1 = iImsUtListener.readInt();
            j = iImsUtListener.readInt();
            str3 = iImsUtListener.readString();
            i = iImsUtListener.readInt();
            param1Int2 = iImsUtListener.readInt();
            param1Int1 = updateCallForward(param1Int1, j, str3, i, param1Int2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 10:
            iImsUtListener.enforceInterface("com.android.ims.internal.IImsUt");
            param1Int1 = iImsUtListener.readInt();
            param1Int2 = iImsUtListener.readInt();
            arrayOfString1 = iImsUtListener.createStringArray();
            param1Int1 = updateCallBarring(param1Int1, param1Int2, arrayOfString1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 9:
            arrayOfString1.enforceInterface("com.android.ims.internal.IImsUt");
            if (arrayOfString1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              arrayOfString1 = null;
            } 
            param1Int1 = transact((Bundle)arrayOfString1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 8:
            arrayOfString1.enforceInterface("com.android.ims.internal.IImsUt");
            param1Int1 = queryCOLP();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 7:
            arrayOfString1.enforceInterface("com.android.ims.internal.IImsUt");
            param1Int1 = queryCOLR();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 6:
            arrayOfString1.enforceInterface("com.android.ims.internal.IImsUt");
            param1Int1 = queryCLIP();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 5:
            arrayOfString1.enforceInterface("com.android.ims.internal.IImsUt");
            param1Int1 = queryCLIR();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 4:
            arrayOfString1.enforceInterface("com.android.ims.internal.IImsUt");
            param1Int1 = queryCallWaiting();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 3:
            arrayOfString1.enforceInterface("com.android.ims.internal.IImsUt");
            param1Int1 = arrayOfString1.readInt();
            str1 = arrayOfString1.readString();
            param1Int1 = queryCallForward(param1Int1, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 2:
            str1.enforceInterface("com.android.ims.internal.IImsUt");
            param1Int1 = str1.readInt();
            param1Int1 = queryCallBarring(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("com.android.ims.internal.IImsUt");
        close();
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("com.android.ims.internal.IImsUt");
      return true;
    }
    
    private static class Proxy implements IImsUt {
      public static IImsUt sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.ims.internal.IImsUt";
      }
      
      public void close() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsUt");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IImsUt.Stub.getDefaultImpl() != null) {
            IImsUt.Stub.getDefaultImpl().close();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int queryCallBarring(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsUt");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IImsUt.Stub.getDefaultImpl() != null) {
            param2Int = IImsUt.Stub.getDefaultImpl().queryCallBarring(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int queryCallForward(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsUt");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IImsUt.Stub.getDefaultImpl() != null) {
            param2Int = IImsUt.Stub.getDefaultImpl().queryCallForward(param2Int, param2String);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int queryCallWaiting() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsUt");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IImsUt.Stub.getDefaultImpl() != null)
            return IImsUt.Stub.getDefaultImpl().queryCallWaiting(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int queryCLIR() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsUt");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IImsUt.Stub.getDefaultImpl() != null)
            return IImsUt.Stub.getDefaultImpl().queryCLIR(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int queryCLIP() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsUt");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IImsUt.Stub.getDefaultImpl() != null)
            return IImsUt.Stub.getDefaultImpl().queryCLIP(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int queryCOLR() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsUt");
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IImsUt.Stub.getDefaultImpl() != null)
            return IImsUt.Stub.getDefaultImpl().queryCOLR(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int queryCOLP() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsUt");
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IImsUt.Stub.getDefaultImpl() != null)
            return IImsUt.Stub.getDefaultImpl().queryCOLP(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int transact(Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsUt");
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IImsUt.Stub.getDefaultImpl() != null)
            return IImsUt.Stub.getDefaultImpl().transact(param2Bundle); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int updateCallBarring(int param2Int1, int param2Int2, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsUt");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IImsUt.Stub.getDefaultImpl() != null) {
            param2Int1 = IImsUt.Stub.getDefaultImpl().updateCallBarring(param2Int1, param2Int2, param2ArrayOfString);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int updateCallForward(int param2Int1, int param2Int2, String param2String, int param2Int3, int param2Int4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsUt");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int3);
          parcel1.writeInt(param2Int4);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IImsUt.Stub.getDefaultImpl() != null) {
            param2Int1 = IImsUt.Stub.getDefaultImpl().updateCallForward(param2Int1, param2Int2, param2String, param2Int3, param2Int4);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int updateCallWaiting(boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsUt");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool1 && IImsUt.Stub.getDefaultImpl() != null) {
            param2Int = IImsUt.Stub.getDefaultImpl().updateCallWaiting(param2Boolean, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int updateCLIR(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsUt");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IImsUt.Stub.getDefaultImpl() != null) {
            param2Int = IImsUt.Stub.getDefaultImpl().updateCLIR(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int updateCLIP(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsUt");
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IImsUt.Stub.getDefaultImpl() != null) {
            i = IImsUt.Stub.getDefaultImpl().updateCLIP(param2Boolean);
            return i;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          return i;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int updateCOLR(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsUt");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IImsUt.Stub.getDefaultImpl() != null) {
            param2Int = IImsUt.Stub.getDefaultImpl().updateCOLR(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int updateCOLP(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsUt");
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IImsUt.Stub.getDefaultImpl() != null) {
            i = IImsUt.Stub.getDefaultImpl().updateCOLP(param2Boolean);
            return i;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          return i;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setListener(IImsUtListener param2IImsUtListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsUt");
          if (param2IImsUtListener != null) {
            iBinder = param2IImsUtListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IImsUt.Stub.getDefaultImpl() != null) {
            IImsUt.Stub.getDefaultImpl().setListener(param2IImsUtListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int queryCallBarringForServiceClass(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsUt");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IImsUt.Stub.getDefaultImpl() != null) {
            param2Int1 = IImsUt.Stub.getDefaultImpl().queryCallBarringForServiceClass(param2Int1, param2Int2);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int updateCallBarringForServiceClass(int param2Int1, int param2Int2, String[] param2ArrayOfString, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsUt");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeStringArray(param2ArrayOfString);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IImsUt.Stub.getDefaultImpl() != null) {
            param2Int1 = IImsUt.Stub.getDefaultImpl().updateCallBarringForServiceClass(param2Int1, param2Int2, param2ArrayOfString, param2Int3);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int updateCallBarringWithPassword(int param2Int1, int param2Int2, String[] param2ArrayOfString, int param2Int3, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsUt");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeStringArray(param2ArrayOfString);
          parcel1.writeInt(param2Int3);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IImsUt.Stub.getDefaultImpl() != null) {
            param2Int1 = IImsUt.Stub.getDefaultImpl().updateCallBarringWithPassword(param2Int1, param2Int2, param2ArrayOfString, param2Int3, param2String);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int queryCFForServiceClass(int param2Int1, String param2String, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsUt");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IImsUt.Stub.getDefaultImpl() != null) {
            param2Int1 = IImsUt.Stub.getDefaultImpl().queryCFForServiceClass(param2Int1, param2String, param2Int2);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsUt param1IImsUt) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsUt != null) {
          Proxy.sDefaultImpl = param1IImsUt;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsUt getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
