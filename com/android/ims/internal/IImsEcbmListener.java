package com.android.ims.internal;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IImsEcbmListener extends IInterface {
  void enteredECBM() throws RemoteException;
  
  void exitedECBM() throws RemoteException;
  
  class Default implements IImsEcbmListener {
    public void enteredECBM() throws RemoteException {}
    
    public void exitedECBM() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsEcbmListener {
    private static final String DESCRIPTOR = "com.android.ims.internal.IImsEcbmListener";
    
    static final int TRANSACTION_enteredECBM = 1;
    
    static final int TRANSACTION_exitedECBM = 2;
    
    public Stub() {
      attachInterface(this, "com.android.ims.internal.IImsEcbmListener");
    }
    
    public static IImsEcbmListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.ims.internal.IImsEcbmListener");
      if (iInterface != null && iInterface instanceof IImsEcbmListener)
        return (IImsEcbmListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "exitedECBM";
      } 
      return "enteredECBM";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("com.android.ims.internal.IImsEcbmListener");
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.ims.internal.IImsEcbmListener");
        exitedECBM();
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.ims.internal.IImsEcbmListener");
      enteredECBM();
      return true;
    }
    
    private static class Proxy implements IImsEcbmListener {
      public static IImsEcbmListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.ims.internal.IImsEcbmListener";
      }
      
      public void enteredECBM() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsEcbmListener");
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IImsEcbmListener.Stub.getDefaultImpl() != null) {
            IImsEcbmListener.Stub.getDefaultImpl().enteredECBM();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void exitedECBM() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsEcbmListener");
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IImsEcbmListener.Stub.getDefaultImpl() != null) {
            IImsEcbmListener.Stub.getDefaultImpl().exitedECBM();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsEcbmListener param1IImsEcbmListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsEcbmListener != null) {
          Proxy.sDefaultImpl = param1IImsEcbmListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsEcbmListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
