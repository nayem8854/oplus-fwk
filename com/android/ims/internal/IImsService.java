package com.android.ims.internal;

import android.app.PendingIntent;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Message;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.ims.ImsCallProfile;

public interface IImsService extends IInterface {
  void addRegistrationListener(int paramInt1, int paramInt2, IImsRegistrationListener paramIImsRegistrationListener) throws RemoteException;
  
  void close(int paramInt) throws RemoteException;
  
  ImsCallProfile createCallProfile(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  IImsCallSession createCallSession(int paramInt, ImsCallProfile paramImsCallProfile, IImsCallSessionListener paramIImsCallSessionListener) throws RemoteException;
  
  IImsConfig getConfigInterface(int paramInt) throws RemoteException;
  
  IImsEcbm getEcbmInterface(int paramInt) throws RemoteException;
  
  IImsMultiEndpoint getMultiEndpointInterface(int paramInt) throws RemoteException;
  
  IImsCallSession getPendingCallSession(int paramInt, String paramString) throws RemoteException;
  
  IImsUt getUtInterface(int paramInt) throws RemoteException;
  
  boolean isConnected(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  boolean isOpened(int paramInt) throws RemoteException;
  
  int open(int paramInt1, int paramInt2, PendingIntent paramPendingIntent, IImsRegistrationListener paramIImsRegistrationListener) throws RemoteException;
  
  void setRegistrationListener(int paramInt, IImsRegistrationListener paramIImsRegistrationListener) throws RemoteException;
  
  void setUiTTYMode(int paramInt1, int paramInt2, Message paramMessage) throws RemoteException;
  
  void turnOffIms(int paramInt) throws RemoteException;
  
  void turnOnIms(int paramInt) throws RemoteException;
  
  class Default implements IImsService {
    public int open(int param1Int1, int param1Int2, PendingIntent param1PendingIntent, IImsRegistrationListener param1IImsRegistrationListener) throws RemoteException {
      return 0;
    }
    
    public void close(int param1Int) throws RemoteException {}
    
    public boolean isConnected(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {
      return false;
    }
    
    public boolean isOpened(int param1Int) throws RemoteException {
      return false;
    }
    
    public void setRegistrationListener(int param1Int, IImsRegistrationListener param1IImsRegistrationListener) throws RemoteException {}
    
    public void addRegistrationListener(int param1Int1, int param1Int2, IImsRegistrationListener param1IImsRegistrationListener) throws RemoteException {}
    
    public ImsCallProfile createCallProfile(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {
      return null;
    }
    
    public IImsCallSession createCallSession(int param1Int, ImsCallProfile param1ImsCallProfile, IImsCallSessionListener param1IImsCallSessionListener) throws RemoteException {
      return null;
    }
    
    public IImsCallSession getPendingCallSession(int param1Int, String param1String) throws RemoteException {
      return null;
    }
    
    public IImsUt getUtInterface(int param1Int) throws RemoteException {
      return null;
    }
    
    public IImsConfig getConfigInterface(int param1Int) throws RemoteException {
      return null;
    }
    
    public void turnOnIms(int param1Int) throws RemoteException {}
    
    public void turnOffIms(int param1Int) throws RemoteException {}
    
    public IImsEcbm getEcbmInterface(int param1Int) throws RemoteException {
      return null;
    }
    
    public void setUiTTYMode(int param1Int1, int param1Int2, Message param1Message) throws RemoteException {}
    
    public IImsMultiEndpoint getMultiEndpointInterface(int param1Int) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsService {
    private static final String DESCRIPTOR = "com.android.ims.internal.IImsService";
    
    static final int TRANSACTION_addRegistrationListener = 6;
    
    static final int TRANSACTION_close = 2;
    
    static final int TRANSACTION_createCallProfile = 7;
    
    static final int TRANSACTION_createCallSession = 8;
    
    static final int TRANSACTION_getConfigInterface = 11;
    
    static final int TRANSACTION_getEcbmInterface = 14;
    
    static final int TRANSACTION_getMultiEndpointInterface = 16;
    
    static final int TRANSACTION_getPendingCallSession = 9;
    
    static final int TRANSACTION_getUtInterface = 10;
    
    static final int TRANSACTION_isConnected = 3;
    
    static final int TRANSACTION_isOpened = 4;
    
    static final int TRANSACTION_open = 1;
    
    static final int TRANSACTION_setRegistrationListener = 5;
    
    static final int TRANSACTION_setUiTTYMode = 15;
    
    static final int TRANSACTION_turnOffIms = 13;
    
    static final int TRANSACTION_turnOnIms = 12;
    
    public Stub() {
      attachInterface(this, "com.android.ims.internal.IImsService");
    }
    
    public static IImsService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.ims.internal.IImsService");
      if (iInterface != null && iInterface instanceof IImsService)
        return (IImsService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 16:
          return "getMultiEndpointInterface";
        case 15:
          return "setUiTTYMode";
        case 14:
          return "getEcbmInterface";
        case 13:
          return "turnOffIms";
        case 12:
          return "turnOnIms";
        case 11:
          return "getConfigInterface";
        case 10:
          return "getUtInterface";
        case 9:
          return "getPendingCallSession";
        case 8:
          return "createCallSession";
        case 7:
          return "createCallProfile";
        case 6:
          return "addRegistrationListener";
        case 5:
          return "setRegistrationListener";
        case 4:
          return "isOpened";
        case 3:
          return "isConnected";
        case 2:
          return "close";
        case 1:
          break;
      } 
      return "open";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool2;
        int j;
        boolean bool1;
        IBinder iBinder3;
        String str;
        IImsUt iImsUt1;
        IBinder iBinder2;
        IImsCallSessionListener iImsCallSessionListener;
        IImsCallSession iImsCallSession1;
        IBinder iBinder1;
        ImsCallProfile imsCallProfile;
        IImsEcbm iImsEcbm;
        IImsConfig iImsConfig;
        IImsUt iImsUt2;
        IImsCallSession iImsCallSession2;
        int k;
        IBinder iBinder4 = null, iBinder5 = null, iBinder6 = null;
        IImsMultiEndpoint iImsMultiEndpoint = null;
        IImsCallSession iImsCallSession3 = null;
        Parcel parcel = null;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 16:
            param1Parcel1.enforceInterface("com.android.ims.internal.IImsService");
            param1Int1 = param1Parcel1.readInt();
            iImsMultiEndpoint = getMultiEndpointInterface(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel1 = parcel;
            if (iImsMultiEndpoint != null)
              iBinder3 = iImsMultiEndpoint.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder3);
            return true;
          case 15:
            iBinder3.enforceInterface("com.android.ims.internal.IImsService");
            param1Int1 = iBinder3.readInt();
            param1Int2 = iBinder3.readInt();
            if (iBinder3.readInt() != 0) {
              Message message = (Message)Message.CREATOR.createFromParcel((Parcel)iBinder3);
            } else {
              iBinder3 = null;
            } 
            setUiTTYMode(param1Int1, param1Int2, (Message)iBinder3);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            iBinder3.enforceInterface("com.android.ims.internal.IImsService");
            param1Int1 = iBinder3.readInt();
            iImsEcbm = getEcbmInterface(param1Int1);
            param1Parcel2.writeNoException();
            iBinder3 = iBinder4;
            if (iImsEcbm != null)
              iBinder3 = iImsEcbm.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder3);
            return true;
          case 13:
            iBinder3.enforceInterface("com.android.ims.internal.IImsService");
            param1Int1 = iBinder3.readInt();
            turnOffIms(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            iBinder3.enforceInterface("com.android.ims.internal.IImsService");
            param1Int1 = iBinder3.readInt();
            turnOnIms(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            iBinder3.enforceInterface("com.android.ims.internal.IImsService");
            param1Int1 = iBinder3.readInt();
            iImsConfig = getConfigInterface(param1Int1);
            param1Parcel2.writeNoException();
            iBinder3 = iBinder5;
            if (iImsConfig != null)
              iBinder3 = iImsConfig.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder3);
            return true;
          case 10:
            iBinder3.enforceInterface("com.android.ims.internal.IImsService");
            param1Int1 = iBinder3.readInt();
            iImsUt2 = getUtInterface(param1Int1);
            param1Parcel2.writeNoException();
            iBinder3 = iBinder6;
            if (iImsUt2 != null)
              iBinder3 = iImsUt2.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder3);
            return true;
          case 9:
            iBinder3.enforceInterface("com.android.ims.internal.IImsService");
            param1Int1 = iBinder3.readInt();
            str = iBinder3.readString();
            iImsCallSession3 = getPendingCallSession(param1Int1, str);
            param1Parcel2.writeNoException();
            iImsUt1 = iImsUt2;
            if (iImsCallSession3 != null)
              iBinder2 = iImsCallSession3.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder2);
            return true;
          case 8:
            iBinder2.enforceInterface("com.android.ims.internal.IImsService");
            param1Int1 = iBinder2.readInt();
            if (iBinder2.readInt() != 0) {
              ImsCallProfile imsCallProfile1 = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)iBinder2);
            } else {
              iImsUt2 = null;
            } 
            iImsCallSessionListener = IImsCallSessionListener.Stub.asInterface(iBinder2.readStrongBinder());
            iImsCallSession2 = createCallSession(param1Int1, (ImsCallProfile)iImsUt2, iImsCallSessionListener);
            param1Parcel2.writeNoException();
            iImsCallSession1 = iImsCallSession3;
            if (iImsCallSession2 != null)
              iBinder1 = iImsCallSession2.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder1);
            return true;
          case 7:
            iBinder1.enforceInterface("com.android.ims.internal.IImsService");
            param1Int1 = iBinder1.readInt();
            param1Int2 = iBinder1.readInt();
            k = iBinder1.readInt();
            imsCallProfile = createCallProfile(param1Int1, param1Int2, k);
            param1Parcel2.writeNoException();
            if (imsCallProfile != null) {
              param1Parcel2.writeInt(1);
              imsCallProfile.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 6:
            imsCallProfile.enforceInterface("com.android.ims.internal.IImsService");
            param1Int1 = imsCallProfile.readInt();
            param1Int2 = imsCallProfile.readInt();
            iImsRegistrationListener = IImsRegistrationListener.Stub.asInterface(imsCallProfile.readStrongBinder());
            addRegistrationListener(param1Int1, param1Int2, iImsRegistrationListener);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            iImsRegistrationListener.enforceInterface("com.android.ims.internal.IImsService");
            param1Int1 = iImsRegistrationListener.readInt();
            iImsRegistrationListener = IImsRegistrationListener.Stub.asInterface(iImsRegistrationListener.readStrongBinder());
            setRegistrationListener(param1Int1, iImsRegistrationListener);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            iImsRegistrationListener.enforceInterface("com.android.ims.internal.IImsService");
            param1Int1 = iImsRegistrationListener.readInt();
            bool2 = isOpened(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 3:
            iImsRegistrationListener.enforceInterface("com.android.ims.internal.IImsService");
            param1Int2 = iImsRegistrationListener.readInt();
            k = iImsRegistrationListener.readInt();
            j = iImsRegistrationListener.readInt();
            bool1 = isConnected(param1Int2, k, j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 2:
            iImsRegistrationListener.enforceInterface("com.android.ims.internal.IImsService");
            i = iImsRegistrationListener.readInt();
            close(i);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        iImsRegistrationListener.enforceInterface("com.android.ims.internal.IImsService");
        param1Int2 = iImsRegistrationListener.readInt();
        int i = iImsRegistrationListener.readInt();
        if (iImsRegistrationListener.readInt() != 0) {
          PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)iImsRegistrationListener);
        } else {
          iImsCallSession2 = null;
        } 
        IImsRegistrationListener iImsRegistrationListener = IImsRegistrationListener.Stub.asInterface(iImsRegistrationListener.readStrongBinder());
        i = open(param1Int2, i, (PendingIntent)iImsCallSession2, iImsRegistrationListener);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(i);
        return true;
      } 
      param1Parcel2.writeString("com.android.ims.internal.IImsService");
      return true;
    }
    
    private static class Proxy implements IImsService {
      public static IImsService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.ims.internal.IImsService";
      }
      
      public int open(int param2Int1, int param2Int2, PendingIntent param2PendingIntent, IImsRegistrationListener param2IImsRegistrationListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2PendingIntent != null) {
            parcel1.writeInt(1);
            param2PendingIntent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2IImsRegistrationListener != null) {
            iBinder = param2IImsRegistrationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IImsService.Stub.getDefaultImpl() != null) {
            param2Int1 = IImsService.Stub.getDefaultImpl().open(param2Int1, param2Int2, param2PendingIntent, param2IImsRegistrationListener);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void close(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IImsService.Stub.getDefaultImpl() != null) {
            IImsService.Stub.getDefaultImpl().close(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isConnected(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IImsService.Stub.getDefaultImpl() != null) {
            bool1 = IImsService.Stub.getDefaultImpl().isConnected(param2Int1, param2Int2, param2Int3);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isOpened(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IImsService.Stub.getDefaultImpl() != null) {
            bool1 = IImsService.Stub.getDefaultImpl().isOpened(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setRegistrationListener(int param2Int, IImsRegistrationListener param2IImsRegistrationListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsService");
          parcel1.writeInt(param2Int);
          if (param2IImsRegistrationListener != null) {
            iBinder = param2IImsRegistrationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IImsService.Stub.getDefaultImpl() != null) {
            IImsService.Stub.getDefaultImpl().setRegistrationListener(param2Int, param2IImsRegistrationListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addRegistrationListener(int param2Int1, int param2Int2, IImsRegistrationListener param2IImsRegistrationListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2IImsRegistrationListener != null) {
            iBinder = param2IImsRegistrationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IImsService.Stub.getDefaultImpl() != null) {
            IImsService.Stub.getDefaultImpl().addRegistrationListener(param2Int1, param2Int2, param2IImsRegistrationListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ImsCallProfile createCallProfile(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ImsCallProfile imsCallProfile;
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IImsService.Stub.getDefaultImpl() != null) {
            imsCallProfile = IImsService.Stub.getDefaultImpl().createCallProfile(param2Int1, param2Int2, param2Int3);
            return imsCallProfile;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel(parcel2);
          } else {
            imsCallProfile = null;
          } 
          return imsCallProfile;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IImsCallSession createCallSession(int param2Int, ImsCallProfile param2ImsCallProfile, IImsCallSessionListener param2IImsCallSessionListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsService");
          parcel1.writeInt(param2Int);
          if (param2ImsCallProfile != null) {
            parcel1.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2IImsCallSessionListener != null) {
            iBinder = param2IImsCallSessionListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IImsService.Stub.getDefaultImpl() != null)
            return IImsService.Stub.getDefaultImpl().createCallSession(param2Int, param2ImsCallProfile, param2IImsCallSessionListener); 
          parcel2.readException();
          return IImsCallSession.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IImsCallSession getPendingCallSession(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IImsService.Stub.getDefaultImpl() != null)
            return IImsService.Stub.getDefaultImpl().getPendingCallSession(param2Int, param2String); 
          parcel2.readException();
          return IImsCallSession.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IImsUt getUtInterface(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IImsService.Stub.getDefaultImpl() != null)
            return IImsService.Stub.getDefaultImpl().getUtInterface(param2Int); 
          parcel2.readException();
          return IImsUt.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IImsConfig getConfigInterface(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IImsService.Stub.getDefaultImpl() != null)
            return IImsService.Stub.getDefaultImpl().getConfigInterface(param2Int); 
          parcel2.readException();
          return IImsConfig.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void turnOnIms(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IImsService.Stub.getDefaultImpl() != null) {
            IImsService.Stub.getDefaultImpl().turnOnIms(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void turnOffIms(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IImsService.Stub.getDefaultImpl() != null) {
            IImsService.Stub.getDefaultImpl().turnOffIms(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IImsEcbm getEcbmInterface(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IImsService.Stub.getDefaultImpl() != null)
            return IImsService.Stub.getDefaultImpl().getEcbmInterface(param2Int); 
          parcel2.readException();
          return IImsEcbm.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUiTTYMode(int param2Int1, int param2Int2, Message param2Message) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2Message != null) {
            parcel1.writeInt(1);
            param2Message.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IImsService.Stub.getDefaultImpl() != null) {
            IImsService.Stub.getDefaultImpl().setUiTTYMode(param2Int1, param2Int2, param2Message);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IImsMultiEndpoint getMultiEndpointInterface(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IImsService.Stub.getDefaultImpl() != null)
            return IImsService.Stub.getDefaultImpl().getMultiEndpointInterface(param2Int); 
          parcel2.readException();
          return IImsMultiEndpoint.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsService param1IImsService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsService != null) {
          Proxy.sDefaultImpl = param1IImsService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
