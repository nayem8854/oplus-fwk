package com.android.ims.internal.uce.uceservice;

import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;

public class ImsUceManager {
  private IUceService mUceService = null;
  
  private UceServiceDeathRecipient mDeathReceipient = new UceServiceDeathRecipient();
  
  private Context mContext;
  
  private static ImsUceManager sUceManager;
  
  private static final Object sLock = new Object();
  
  public static final int UCE_SERVICE_STATUS_READY = 3;
  
  public static final int UCE_SERVICE_STATUS_ON = 1;
  
  public static final int UCE_SERVICE_STATUS_FAILURE = 0;
  
  public static final int UCE_SERVICE_STATUS_CLOSED = 2;
  
  private static final String UCE_SERVICE = "uce";
  
  private static final String LOG_TAG = "ImsUceManager";
  
  public static final String ACTION_UCE_SERVICE_UP = "com.android.ims.internal.uce.UCE_SERVICE_UP";
  
  public static final String ACTION_UCE_SERVICE_DOWN = "com.android.ims.internal.uce.UCE_SERVICE_DOWN";
  
  public static ImsUceManager getInstance(Context paramContext) {
    synchronized (sLock) {
      if (sUceManager == null && paramContext != null) {
        ImsUceManager imsUceManager = new ImsUceManager();
        this(paramContext);
        sUceManager = imsUceManager;
      } 
      return sUceManager;
    } 
  }
  
  private ImsUceManager(Context paramContext) {
    this.mContext = paramContext;
    createUceService(true);
  }
  
  public IUceService getUceServiceInstance() {
    return this.mUceService;
  }
  
  private String getUceServiceName() {
    return "uce";
  }
  
  public void createUceService(boolean paramBoolean) {
    if (paramBoolean) {
      IBinder iBinder1 = ServiceManager.checkService(getUceServiceName());
      if (iBinder1 == null)
        return; 
    } 
    IBinder iBinder = ServiceManager.getService(getUceServiceName());
    if (iBinder != null)
      try {
        iBinder.linkToDeath(this.mDeathReceipient, 0);
      } catch (RemoteException remoteException) {} 
    this.mUceService = IUceService.Stub.asInterface(iBinder);
  }
  
  class UceServiceDeathRecipient implements IBinder.DeathRecipient {
    final ImsUceManager this$0;
    
    private UceServiceDeathRecipient() {}
    
    public void binderDied() {
      ImsUceManager.access$102(ImsUceManager.this, null);
      if (ImsUceManager.this.mContext != null) {
        Intent intent = new Intent("com.android.ims.internal.uce.UCE_SERVICE_DOWN");
        ImsUceManager.this.mContext.sendBroadcast(new Intent(intent));
      } 
    }
  }
}
