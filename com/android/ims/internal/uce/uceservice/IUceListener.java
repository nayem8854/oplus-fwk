package com.android.ims.internal.uce.uceservice;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IUceListener extends IInterface {
  void setStatus(int paramInt) throws RemoteException;
  
  class Default implements IUceListener {
    public void setStatus(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IUceListener {
    private static final String DESCRIPTOR = "com.android.ims.internal.uce.uceservice.IUceListener";
    
    static final int TRANSACTION_setStatus = 1;
    
    public Stub() {
      attachInterface(this, "com.android.ims.internal.uce.uceservice.IUceListener");
    }
    
    public static IUceListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.ims.internal.uce.uceservice.IUceListener");
      if (iInterface != null && iInterface instanceof IUceListener)
        return (IUceListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "setStatus";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.ims.internal.uce.uceservice.IUceListener");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.ims.internal.uce.uceservice.IUceListener");
      param1Int1 = param1Parcel1.readInt();
      setStatus(param1Int1);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IUceListener {
      public static IUceListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.ims.internal.uce.uceservice.IUceListener";
      }
      
      public void setStatus(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.uceservice.IUceListener");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IUceListener.Stub.getDefaultImpl() != null) {
            IUceListener.Stub.getDefaultImpl().setStatus(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IUceListener param1IUceListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IUceListener != null) {
          Proxy.sDefaultImpl = param1IUceListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IUceListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
