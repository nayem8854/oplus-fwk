package com.android.ims.internal.uce.uceservice;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.android.ims.internal.uce.common.UceLong;
import com.android.ims.internal.uce.options.IOptionsListener;
import com.android.ims.internal.uce.options.IOptionsService;
import com.android.ims.internal.uce.presence.IPresenceListener;
import com.android.ims.internal.uce.presence.IPresenceService;

public interface IUceService extends IInterface {
  int createOptionsService(IOptionsListener paramIOptionsListener, UceLong paramUceLong) throws RemoteException;
  
  int createOptionsServiceForSubscription(IOptionsListener paramIOptionsListener, UceLong paramUceLong, String paramString) throws RemoteException;
  
  int createPresenceService(IPresenceListener paramIPresenceListener, UceLong paramUceLong) throws RemoteException;
  
  int createPresenceServiceForSubscription(IPresenceListener paramIPresenceListener, UceLong paramUceLong, String paramString) throws RemoteException;
  
  void destroyOptionsService(int paramInt) throws RemoteException;
  
  void destroyPresenceService(int paramInt) throws RemoteException;
  
  IOptionsService getOptionsService() throws RemoteException;
  
  IOptionsService getOptionsServiceForSubscription(String paramString) throws RemoteException;
  
  IPresenceService getPresenceService() throws RemoteException;
  
  IPresenceService getPresenceServiceForSubscription(String paramString) throws RemoteException;
  
  boolean getServiceStatus() throws RemoteException;
  
  boolean isServiceStarted() throws RemoteException;
  
  boolean startService(IUceListener paramIUceListener) throws RemoteException;
  
  boolean stopService() throws RemoteException;
  
  class Default implements IUceService {
    public boolean startService(IUceListener param1IUceListener) throws RemoteException {
      return false;
    }
    
    public boolean stopService() throws RemoteException {
      return false;
    }
    
    public boolean isServiceStarted() throws RemoteException {
      return false;
    }
    
    public int createOptionsService(IOptionsListener param1IOptionsListener, UceLong param1UceLong) throws RemoteException {
      return 0;
    }
    
    public int createOptionsServiceForSubscription(IOptionsListener param1IOptionsListener, UceLong param1UceLong, String param1String) throws RemoteException {
      return 0;
    }
    
    public void destroyOptionsService(int param1Int) throws RemoteException {}
    
    public int createPresenceService(IPresenceListener param1IPresenceListener, UceLong param1UceLong) throws RemoteException {
      return 0;
    }
    
    public int createPresenceServiceForSubscription(IPresenceListener param1IPresenceListener, UceLong param1UceLong, String param1String) throws RemoteException {
      return 0;
    }
    
    public void destroyPresenceService(int param1Int) throws RemoteException {}
    
    public boolean getServiceStatus() throws RemoteException {
      return false;
    }
    
    public IPresenceService getPresenceService() throws RemoteException {
      return null;
    }
    
    public IPresenceService getPresenceServiceForSubscription(String param1String) throws RemoteException {
      return null;
    }
    
    public IOptionsService getOptionsService() throws RemoteException {
      return null;
    }
    
    public IOptionsService getOptionsServiceForSubscription(String param1String) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IUceService {
    private static final String DESCRIPTOR = "com.android.ims.internal.uce.uceservice.IUceService";
    
    static final int TRANSACTION_createOptionsService = 4;
    
    static final int TRANSACTION_createOptionsServiceForSubscription = 5;
    
    static final int TRANSACTION_createPresenceService = 7;
    
    static final int TRANSACTION_createPresenceServiceForSubscription = 8;
    
    static final int TRANSACTION_destroyOptionsService = 6;
    
    static final int TRANSACTION_destroyPresenceService = 9;
    
    static final int TRANSACTION_getOptionsService = 13;
    
    static final int TRANSACTION_getOptionsServiceForSubscription = 14;
    
    static final int TRANSACTION_getPresenceService = 11;
    
    static final int TRANSACTION_getPresenceServiceForSubscription = 12;
    
    static final int TRANSACTION_getServiceStatus = 10;
    
    static final int TRANSACTION_isServiceStarted = 3;
    
    static final int TRANSACTION_startService = 1;
    
    static final int TRANSACTION_stopService = 2;
    
    public Stub() {
      attachInterface(this, "com.android.ims.internal.uce.uceservice.IUceService");
    }
    
    public static IUceService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.ims.internal.uce.uceservice.IUceService");
      if (iInterface != null && iInterface instanceof IUceService)
        return (IUceService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 14:
          return "getOptionsServiceForSubscription";
        case 13:
          return "getOptionsService";
        case 12:
          return "getPresenceServiceForSubscription";
        case 11:
          return "getPresenceService";
        case 10:
          return "getServiceStatus";
        case 9:
          return "destroyPresenceService";
        case 8:
          return "createPresenceServiceForSubscription";
        case 7:
          return "createPresenceService";
        case 6:
          return "destroyOptionsService";
        case 5:
          return "createOptionsServiceForSubscription";
        case 4:
          return "createOptionsService";
        case 3:
          return "isServiceStarted";
        case 2:
          return "stopService";
        case 1:
          break;
      } 
      return "startService";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool2;
        int i;
        String str3;
        IBinder iBinder2;
        String str2;
        IOptionsService iOptionsService1;
        IBinder iBinder1;
        String str1;
        IPresenceService iPresenceService1;
        IPresenceListener iPresenceListener1;
        IOptionsListener iOptionsListener1;
        IPresenceService iPresenceService2;
        IPresenceListener iPresenceListener2;
        IOptionsListener iOptionsListener2;
        IBinder iBinder3 = null;
        IOptionsService iOptionsService2 = null;
        IBinder iBinder4 = null;
        String str4 = null;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 14:
            param1Parcel1.enforceInterface("com.android.ims.internal.uce.uceservice.IUceService");
            str3 = param1Parcel1.readString();
            iOptionsService2 = getOptionsServiceForSubscription(str3);
            param1Parcel2.writeNoException();
            str3 = str4;
            if (iOptionsService2 != null)
              iBinder2 = iOptionsService2.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder2);
            return true;
          case 13:
            iBinder2.enforceInterface("com.android.ims.internal.uce.uceservice.IUceService");
            iOptionsService2 = getOptionsService();
            param1Parcel2.writeNoException();
            iBinder2 = iBinder3;
            if (iOptionsService2 != null)
              iBinder2 = iOptionsService2.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder2);
            return true;
          case 12:
            iBinder2.enforceInterface("com.android.ims.internal.uce.uceservice.IUceService");
            str2 = iBinder2.readString();
            iPresenceService2 = getPresenceServiceForSubscription(str2);
            param1Parcel2.writeNoException();
            iOptionsService1 = iOptionsService2;
            if (iPresenceService2 != null)
              iBinder1 = iPresenceService2.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder1);
            return true;
          case 11:
            iBinder1.enforceInterface("com.android.ims.internal.uce.uceservice.IUceService");
            iPresenceService1 = getPresenceService();
            param1Parcel2.writeNoException();
            iBinder1 = iBinder4;
            if (iPresenceService1 != null)
              iBinder1 = iPresenceService1.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder1);
            return true;
          case 10:
            iBinder1.enforceInterface("com.android.ims.internal.uce.uceservice.IUceService");
            bool2 = getServiceStatus();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 9:
            iBinder1.enforceInterface("com.android.ims.internal.uce.uceservice.IUceService");
            i = iBinder1.readInt();
            destroyPresenceService(i);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            iBinder1.enforceInterface("com.android.ims.internal.uce.uceservice.IUceService");
            iPresenceListener2 = IPresenceListener.Stub.asInterface(iBinder1.readStrongBinder());
            if (iBinder1.readInt() != 0) {
              UceLong uceLong = (UceLong)UceLong.CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              iPresenceService1 = null;
            } 
            str1 = iBinder1.readString();
            i = createPresenceServiceForSubscription(iPresenceListener2, (UceLong)iPresenceService1, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            if (iPresenceService1 != null) {
              param1Parcel2.writeInt(1);
              iPresenceService1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 7:
            str1.enforceInterface("com.android.ims.internal.uce.uceservice.IUceService");
            iPresenceListener1 = IPresenceListener.Stub.asInterface(str1.readStrongBinder());
            if (str1.readInt() != 0) {
              UceLong uceLong = (UceLong)UceLong.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            i = createPresenceService(iPresenceListener1, (UceLong)str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            if (str1 != null) {
              param1Parcel2.writeInt(1);
              str1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 6:
            str1.enforceInterface("com.android.ims.internal.uce.uceservice.IUceService");
            i = str1.readInt();
            destroyOptionsService(i);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            str1.enforceInterface("com.android.ims.internal.uce.uceservice.IUceService");
            iOptionsListener2 = IOptionsListener.Stub.asInterface(str1.readStrongBinder());
            if (str1.readInt() != 0) {
              UceLong uceLong = (UceLong)UceLong.CREATOR.createFromParcel((Parcel)str1);
            } else {
              iPresenceListener1 = null;
            } 
            str1 = str1.readString();
            i = createOptionsServiceForSubscription(iOptionsListener2, (UceLong)iPresenceListener1, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            if (iPresenceListener1 != null) {
              param1Parcel2.writeInt(1);
              iPresenceListener1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 4:
            str1.enforceInterface("com.android.ims.internal.uce.uceservice.IUceService");
            iOptionsListener1 = IOptionsListener.Stub.asInterface(str1.readStrongBinder());
            if (str1.readInt() != 0) {
              UceLong uceLong = (UceLong)UceLong.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            i = createOptionsService(iOptionsListener1, (UceLong)str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            if (str1 != null) {
              param1Parcel2.writeInt(1);
              str1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 3:
            str1.enforceInterface("com.android.ims.internal.uce.uceservice.IUceService");
            bool1 = isServiceStarted();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 2:
            str1.enforceInterface("com.android.ims.internal.uce.uceservice.IUceService");
            bool1 = stopService();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("com.android.ims.internal.uce.uceservice.IUceService");
        IUceListener iUceListener = IUceListener.Stub.asInterface(str1.readStrongBinder());
        boolean bool1 = startService(iUceListener);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      param1Parcel2.writeString("com.android.ims.internal.uce.uceservice.IUceService");
      return true;
    }
    
    private static class Proxy implements IUceService {
      public static IUceService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.ims.internal.uce.uceservice.IUceService";
      }
      
      public boolean startService(IUceListener param2IUceListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.uceservice.IUceService");
          if (param2IUceListener != null) {
            iBinder = param2IUceListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IUceService.Stub.getDefaultImpl() != null) {
            bool1 = IUceService.Stub.getDefaultImpl().startService(param2IUceListener);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean stopService() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.uceservice.IUceService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IUceService.Stub.getDefaultImpl() != null) {
            bool1 = IUceService.Stub.getDefaultImpl().stopService();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isServiceStarted() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.uceservice.IUceService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IUceService.Stub.getDefaultImpl() != null) {
            bool1 = IUceService.Stub.getDefaultImpl().isServiceStarted();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int createOptionsService(IOptionsListener param2IOptionsListener, UceLong param2UceLong) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.uceservice.IUceService");
          if (param2IOptionsListener != null) {
            iBinder = param2IOptionsListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2UceLong != null) {
            parcel1.writeInt(1);
            param2UceLong.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IUceService.Stub.getDefaultImpl() != null)
            return IUceService.Stub.getDefaultImpl().createOptionsService(param2IOptionsListener, param2UceLong); 
          parcel2.readException();
          int i = parcel2.readInt();
          if (parcel2.readInt() != 0)
            param2UceLong.readFromParcel(parcel2); 
          return i;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int createOptionsServiceForSubscription(IOptionsListener param2IOptionsListener, UceLong param2UceLong, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.uceservice.IUceService");
          if (param2IOptionsListener != null) {
            iBinder = param2IOptionsListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2UceLong != null) {
            parcel1.writeInt(1);
            param2UceLong.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IUceService.Stub.getDefaultImpl() != null)
            return IUceService.Stub.getDefaultImpl().createOptionsServiceForSubscription(param2IOptionsListener, param2UceLong, param2String); 
          parcel2.readException();
          int i = parcel2.readInt();
          if (parcel2.readInt() != 0)
            param2UceLong.readFromParcel(parcel2); 
          return i;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void destroyOptionsService(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.uceservice.IUceService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IUceService.Stub.getDefaultImpl() != null) {
            IUceService.Stub.getDefaultImpl().destroyOptionsService(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int createPresenceService(IPresenceListener param2IPresenceListener, UceLong param2UceLong) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.uceservice.IUceService");
          if (param2IPresenceListener != null) {
            iBinder = param2IPresenceListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2UceLong != null) {
            parcel1.writeInt(1);
            param2UceLong.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IUceService.Stub.getDefaultImpl() != null)
            return IUceService.Stub.getDefaultImpl().createPresenceService(param2IPresenceListener, param2UceLong); 
          parcel2.readException();
          int i = parcel2.readInt();
          if (parcel2.readInt() != 0)
            param2UceLong.readFromParcel(parcel2); 
          return i;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int createPresenceServiceForSubscription(IPresenceListener param2IPresenceListener, UceLong param2UceLong, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.uceservice.IUceService");
          if (param2IPresenceListener != null) {
            iBinder = param2IPresenceListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2UceLong != null) {
            parcel1.writeInt(1);
            param2UceLong.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IUceService.Stub.getDefaultImpl() != null)
            return IUceService.Stub.getDefaultImpl().createPresenceServiceForSubscription(param2IPresenceListener, param2UceLong, param2String); 
          parcel2.readException();
          int i = parcel2.readInt();
          if (parcel2.readInt() != 0)
            param2UceLong.readFromParcel(parcel2); 
          return i;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void destroyPresenceService(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.uceservice.IUceService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IUceService.Stub.getDefaultImpl() != null) {
            IUceService.Stub.getDefaultImpl().destroyPresenceService(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getServiceStatus() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.uceservice.IUceService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(10, parcel1, parcel2, 0);
          if (!bool2 && IUceService.Stub.getDefaultImpl() != null) {
            bool1 = IUceService.Stub.getDefaultImpl().getServiceStatus();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IPresenceService getPresenceService() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.uceservice.IUceService");
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IUceService.Stub.getDefaultImpl() != null)
            return IUceService.Stub.getDefaultImpl().getPresenceService(); 
          parcel2.readException();
          return IPresenceService.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IPresenceService getPresenceServiceForSubscription(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.uceservice.IUceService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IUceService.Stub.getDefaultImpl() != null)
            return IUceService.Stub.getDefaultImpl().getPresenceServiceForSubscription(param2String); 
          parcel2.readException();
          return IPresenceService.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IOptionsService getOptionsService() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.uceservice.IUceService");
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IUceService.Stub.getDefaultImpl() != null)
            return IUceService.Stub.getDefaultImpl().getOptionsService(); 
          parcel2.readException();
          return IOptionsService.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IOptionsService getOptionsServiceForSubscription(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.uceservice.IUceService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IUceService.Stub.getDefaultImpl() != null)
            return IUceService.Stub.getDefaultImpl().getOptionsServiceForSubscription(param2String); 
          parcel2.readException();
          return IOptionsService.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IUceService param1IUceService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IUceService != null) {
          Proxy.sDefaultImpl = param1IUceService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IUceService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
