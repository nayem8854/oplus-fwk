package com.android.ims.internal.uce.common;

import android.os.Parcel;
import android.os.Parcelable;

public class CapInfo implements Parcelable {
  private boolean mImSupported = false;
  
  private boolean mFtSupported = false;
  
  private boolean mFtThumbSupported = false;
  
  private boolean mFtSnFSupported = false;
  
  private boolean mFtHttpSupported = false;
  
  private boolean mIsSupported = false;
  
  private boolean mVsDuringCSSupported = false;
  
  private boolean mVsSupported = false;
  
  private boolean mSpSupported = false;
  
  private boolean mCdViaPresenceSupported = false;
  
  private boolean mIpVoiceSupported = false;
  
  private boolean mIpVideoSupported = false;
  
  private boolean mGeoPullFtSupported = false;
  
  private boolean mGeoPullSupported = false;
  
  private boolean mGeoPushSupported = false;
  
  private boolean mSmSupported = false;
  
  private boolean mFullSnFGroupChatSupported = false;
  
  private boolean mRcsIpVoiceCallSupported = false;
  
  private boolean mRcsIpVideoCallSupported = false;
  
  private boolean mRcsIpVideoOnlyCallSupported = false;
  
  private boolean mGeoSmsSupported = false;
  
  private boolean mCallComposerSupported = false;
  
  private boolean mPostCallSupported = false;
  
  private boolean mSharedMapSupported = false;
  
  private boolean mSharedSketchSupported = false;
  
  private boolean mChatbotSupported = false;
  
  private boolean mChatbotRoleSupported = false;
  
  private boolean mSmChatbotSupported = false;
  
  private boolean mMmtelCallComposerSupported = false;
  
  private String[] mExts = new String[10];
  
  private long mCapTimestamp = 0L;
  
  public boolean isImSupported() {
    return this.mImSupported;
  }
  
  public void setImSupported(boolean paramBoolean) {
    this.mImSupported = paramBoolean;
  }
  
  public boolean isFtThumbSupported() {
    return this.mFtThumbSupported;
  }
  
  public void setFtThumbSupported(boolean paramBoolean) {
    this.mFtThumbSupported = paramBoolean;
  }
  
  public boolean isFtSnFSupported() {
    return this.mFtSnFSupported;
  }
  
  public void setFtSnFSupported(boolean paramBoolean) {
    this.mFtSnFSupported = paramBoolean;
  }
  
  public boolean isFtHttpSupported() {
    return this.mFtHttpSupported;
  }
  
  public void setFtHttpSupported(boolean paramBoolean) {
    this.mFtHttpSupported = paramBoolean;
  }
  
  public boolean isFtSupported() {
    return this.mFtSupported;
  }
  
  public void setFtSupported(boolean paramBoolean) {
    this.mFtSupported = paramBoolean;
  }
  
  public boolean isIsSupported() {
    return this.mIsSupported;
  }
  
  public void setIsSupported(boolean paramBoolean) {
    this.mIsSupported = paramBoolean;
  }
  
  public boolean isVsDuringCSSupported() {
    return this.mVsDuringCSSupported;
  }
  
  public void setVsDuringCSSupported(boolean paramBoolean) {
    this.mVsDuringCSSupported = paramBoolean;
  }
  
  public boolean isVsSupported() {
    return this.mVsSupported;
  }
  
  public void setVsSupported(boolean paramBoolean) {
    this.mVsSupported = paramBoolean;
  }
  
  public boolean isSpSupported() {
    return this.mSpSupported;
  }
  
  public void setSpSupported(boolean paramBoolean) {
    this.mSpSupported = paramBoolean;
  }
  
  public boolean isCdViaPresenceSupported() {
    return this.mCdViaPresenceSupported;
  }
  
  public void setCdViaPresenceSupported(boolean paramBoolean) {
    this.mCdViaPresenceSupported = paramBoolean;
  }
  
  public boolean isIpVoiceSupported() {
    return this.mIpVoiceSupported;
  }
  
  public void setIpVoiceSupported(boolean paramBoolean) {
    this.mIpVoiceSupported = paramBoolean;
  }
  
  public boolean isIpVideoSupported() {
    return this.mIpVideoSupported;
  }
  
  public void setIpVideoSupported(boolean paramBoolean) {
    this.mIpVideoSupported = paramBoolean;
  }
  
  public boolean isGeoPullFtSupported() {
    return this.mGeoPullFtSupported;
  }
  
  public void setGeoPullFtSupported(boolean paramBoolean) {
    this.mGeoPullFtSupported = paramBoolean;
  }
  
  public boolean isGeoPullSupported() {
    return this.mGeoPullSupported;
  }
  
  public void setGeoPullSupported(boolean paramBoolean) {
    this.mGeoPullSupported = paramBoolean;
  }
  
  public boolean isGeoPushSupported() {
    return this.mGeoPushSupported;
  }
  
  public void setGeoPushSupported(boolean paramBoolean) {
    this.mGeoPushSupported = paramBoolean;
  }
  
  public boolean isSmSupported() {
    return this.mSmSupported;
  }
  
  public void setSmSupported(boolean paramBoolean) {
    this.mSmSupported = paramBoolean;
  }
  
  public boolean isFullSnFGroupChatSupported() {
    return this.mFullSnFGroupChatSupported;
  }
  
  public boolean isRcsIpVoiceCallSupported() {
    return this.mRcsIpVoiceCallSupported;
  }
  
  public boolean isRcsIpVideoCallSupported() {
    return this.mRcsIpVideoCallSupported;
  }
  
  public boolean isRcsIpVideoOnlyCallSupported() {
    return this.mRcsIpVideoOnlyCallSupported;
  }
  
  public void setFullSnFGroupChatSupported(boolean paramBoolean) {
    this.mFullSnFGroupChatSupported = paramBoolean;
  }
  
  public void setRcsIpVoiceCallSupported(boolean paramBoolean) {
    this.mRcsIpVoiceCallSupported = paramBoolean;
  }
  
  public void setRcsIpVideoCallSupported(boolean paramBoolean) {
    this.mRcsIpVideoCallSupported = paramBoolean;
  }
  
  public void setRcsIpVideoOnlyCallSupported(boolean paramBoolean) {
    this.mRcsIpVideoOnlyCallSupported = paramBoolean;
  }
  
  public boolean isGeoSmsSupported() {
    return this.mGeoSmsSupported;
  }
  
  public void setGeoSmsSupported(boolean paramBoolean) {
    this.mGeoSmsSupported = paramBoolean;
  }
  
  public boolean isCallComposerSupported() {
    return this.mCallComposerSupported;
  }
  
  public void setCallComposerSupported(boolean paramBoolean) {
    this.mCallComposerSupported = paramBoolean;
  }
  
  public boolean isPostCallSupported() {
    return this.mPostCallSupported;
  }
  
  public void setPostCallSupported(boolean paramBoolean) {
    this.mPostCallSupported = paramBoolean;
  }
  
  public boolean isSharedMapSupported() {
    return this.mSharedMapSupported;
  }
  
  public void setSharedMapSupported(boolean paramBoolean) {
    this.mSharedMapSupported = paramBoolean;
  }
  
  public boolean isSharedSketchSupported() {
    return this.mSharedSketchSupported;
  }
  
  public void setSharedSketchSupported(boolean paramBoolean) {
    this.mSharedSketchSupported = paramBoolean;
  }
  
  public boolean isChatbotSupported() {
    return this.mChatbotSupported;
  }
  
  public void setChatbotSupported(boolean paramBoolean) {
    this.mChatbotSupported = paramBoolean;
  }
  
  public boolean isChatbotRoleSupported() {
    return this.mChatbotRoleSupported;
  }
  
  public void setChatbotRoleSupported(boolean paramBoolean) {
    this.mChatbotRoleSupported = paramBoolean;
  }
  
  public boolean isSmChatbotSupported() {
    return this.mSmChatbotSupported;
  }
  
  public void setSmChatbotSupported(boolean paramBoolean) {
    this.mSmChatbotSupported = paramBoolean;
  }
  
  public boolean isMmtelCallComposerSupported() {
    return this.mMmtelCallComposerSupported;
  }
  
  public void setMmtelCallComposerSupported(boolean paramBoolean) {
    this.mMmtelCallComposerSupported = paramBoolean;
  }
  
  public String[] getExts() {
    return this.mExts;
  }
  
  public void setExts(String[] paramArrayOfString) {
    this.mExts = paramArrayOfString;
  }
  
  public long getCapTimestamp() {
    return this.mCapTimestamp;
  }
  
  public void setCapTimestamp(long paramLong) {
    this.mCapTimestamp = paramLong;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mImSupported);
    paramParcel.writeInt(this.mFtSupported);
    paramParcel.writeInt(this.mFtThumbSupported);
    paramParcel.writeInt(this.mFtSnFSupported);
    paramParcel.writeInt(this.mFtHttpSupported);
    paramParcel.writeInt(this.mIsSupported);
    paramParcel.writeInt(this.mVsDuringCSSupported);
    paramParcel.writeInt(this.mVsSupported);
    paramParcel.writeInt(this.mSpSupported);
    paramParcel.writeInt(this.mCdViaPresenceSupported);
    paramParcel.writeInt(this.mIpVoiceSupported);
    paramParcel.writeInt(this.mIpVideoSupported);
    paramParcel.writeInt(this.mGeoPullFtSupported);
    paramParcel.writeInt(this.mGeoPullSupported);
    paramParcel.writeInt(this.mGeoPushSupported);
    paramParcel.writeInt(this.mSmSupported);
    paramParcel.writeInt(this.mFullSnFGroupChatSupported);
    paramParcel.writeInt(this.mGeoSmsSupported);
    paramParcel.writeInt(this.mCallComposerSupported);
    paramParcel.writeInt(this.mPostCallSupported);
    paramParcel.writeInt(this.mSharedMapSupported);
    paramParcel.writeInt(this.mSharedSketchSupported);
    paramParcel.writeInt(this.mChatbotSupported);
    paramParcel.writeInt(this.mChatbotRoleSupported);
    paramParcel.writeInt(this.mSmChatbotSupported);
    paramParcel.writeInt(this.mMmtelCallComposerSupported);
    paramParcel.writeInt(this.mRcsIpVoiceCallSupported);
    paramParcel.writeInt(this.mRcsIpVideoCallSupported);
    paramParcel.writeInt(this.mRcsIpVideoOnlyCallSupported);
    paramParcel.writeStringArray(this.mExts);
    paramParcel.writeLong(this.mCapTimestamp);
  }
  
  public static final Parcelable.Creator<CapInfo> CREATOR = new Parcelable.Creator<CapInfo>() {
      public CapInfo createFromParcel(Parcel param1Parcel) {
        return new CapInfo(param1Parcel);
      }
      
      public CapInfo[] newArray(int param1Int) {
        return new CapInfo[param1Int];
      }
    };
  
  private CapInfo(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    boolean bool2;
    int i = paramParcel.readInt();
    boolean bool1 = false;
    if (i == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mImSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mFtSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mFtThumbSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mFtSnFSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mFtHttpSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mIsSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mVsDuringCSSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mVsSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mSpSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mCdViaPresenceSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mIpVoiceSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mIpVideoSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mGeoPullFtSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mGeoPullSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mGeoPushSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mSmSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mFullSnFGroupChatSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mGeoSmsSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mCallComposerSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mPostCallSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mSharedMapSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mSharedSketchSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mChatbotSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mChatbotRoleSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mSmChatbotSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mMmtelCallComposerSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mRcsIpVoiceCallSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = false;
    } else {
      bool2 = true;
    } 
    this.mRcsIpVideoCallSupported = bool2;
    if (paramParcel.readInt() == 0) {
      bool2 = bool1;
    } else {
      bool2 = true;
    } 
    this.mRcsIpVideoOnlyCallSupported = bool2;
    this.mExts = paramParcel.createStringArray();
    this.mCapTimestamp = paramParcel.readLong();
  }
  
  public CapInfo() {}
}
