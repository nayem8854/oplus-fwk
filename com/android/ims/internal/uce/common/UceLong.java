package com.android.ims.internal.uce.common;

import android.os.Parcel;
import android.os.Parcelable;

public class UceLong implements Parcelable {
  private long mUceLong;
  
  private int mClientId = 1001;
  
  public long getUceLong() {
    return this.mUceLong;
  }
  
  public void setUceLong(long paramLong) {
    this.mUceLong = paramLong;
  }
  
  public int getClientId() {
    return this.mClientId;
  }
  
  public void setClientId(int paramInt) {
    this.mClientId = paramInt;
  }
  
  public static UceLong getUceLongInstance() {
    return new UceLong();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcel(paramParcel);
  }
  
  private void writeToParcel(Parcel paramParcel) {
    paramParcel.writeLong(this.mUceLong);
    paramParcel.writeInt(this.mClientId);
  }
  
  public static final Parcelable.Creator<UceLong> CREATOR = new Parcelable.Creator<UceLong>() {
      public UceLong createFromParcel(Parcel param1Parcel) {
        return new UceLong(param1Parcel);
      }
      
      public UceLong[] newArray(int param1Int) {
        return new UceLong[param1Int];
      }
    };
  
  private UceLong(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mUceLong = paramParcel.readLong();
    this.mClientId = paramParcel.readInt();
  }
  
  public UceLong() {}
}
