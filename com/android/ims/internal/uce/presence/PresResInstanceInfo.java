package com.android.ims.internal.uce.presence;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

public class PresResInstanceInfo implements Parcelable {
  private String mId = "";
  
  private String mReason = "";
  
  private String mPresentityUri = "";
  
  public int getResInstanceState() {
    return this.mResInstanceState;
  }
  
  public void setResInstanceState(int paramInt) {
    this.mResInstanceState = paramInt;
  }
  
  public String getResId() {
    return this.mId;
  }
  
  public void setResId(String paramString) {
    this.mId = paramString;
  }
  
  public String getReason() {
    return this.mReason;
  }
  
  public void setReason(String paramString) {
    this.mReason = paramString;
  }
  
  public String getPresentityUri() {
    return this.mPresentityUri;
  }
  
  public void setPresentityUri(String paramString) {
    this.mPresentityUri = paramString;
  }
  
  public PresTupleInfo[] getTupleInfo() {
    return this.mTupleInfoArray;
  }
  
  public void setTupleInfo(PresTupleInfo[] paramArrayOfPresTupleInfo) {
    this.mTupleInfoArray = new PresTupleInfo[paramArrayOfPresTupleInfo.length];
    this.mTupleInfoArray = paramArrayOfPresTupleInfo;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mId);
    paramParcel.writeString(this.mReason);
    paramParcel.writeInt(this.mResInstanceState);
    paramParcel.writeString(this.mPresentityUri);
    paramParcel.writeParcelableArray((Parcelable[])this.mTupleInfoArray, paramInt);
  }
  
  public static final Parcelable.Creator<PresResInstanceInfo> CREATOR = new Parcelable.Creator<PresResInstanceInfo>() {
      public PresResInstanceInfo createFromParcel(Parcel param1Parcel) {
        return new PresResInstanceInfo(param1Parcel);
      }
      
      public PresResInstanceInfo[] newArray(int param1Int) {
        return new PresResInstanceInfo[param1Int];
      }
    };
  
  public static final int UCE_PRES_RES_INSTANCE_STATE_ACTIVE = 0;
  
  public static final int UCE_PRES_RES_INSTANCE_STATE_PENDING = 1;
  
  public static final int UCE_PRES_RES_INSTANCE_STATE_TERMINATED = 2;
  
  public static final int UCE_PRES_RES_INSTANCE_STATE_UNKNOWN = 3;
  
  public static final int UCE_PRES_RES_INSTANCE_UNKNOWN = 4;
  
  private int mResInstanceState;
  
  private PresTupleInfo[] mTupleInfoArray;
  
  private PresResInstanceInfo(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mId = paramParcel.readString();
    this.mReason = paramParcel.readString();
    this.mResInstanceState = paramParcel.readInt();
    this.mPresentityUri = paramParcel.readString();
    ClassLoader classLoader = PresTupleInfo.class.getClassLoader();
    Parcelable[] arrayOfParcelable = paramParcel.readParcelableArray(classLoader);
    this.mTupleInfoArray = new PresTupleInfo[0];
    if (arrayOfParcelable != null)
      this.mTupleInfoArray = Arrays.<PresTupleInfo, Parcelable>copyOf(arrayOfParcelable, arrayOfParcelable.length, PresTupleInfo[].class); 
  }
  
  public PresResInstanceInfo() {}
}
