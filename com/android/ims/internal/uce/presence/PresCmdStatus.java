package com.android.ims.internal.uce.presence;

import android.os.Parcel;
import android.os.Parcelable;
import com.android.ims.internal.uce.common.StatusCode;

public class PresCmdStatus implements Parcelable {
  private PresCmdId mCmdId = new PresCmdId();
  
  private StatusCode mStatus = new StatusCode();
  
  public PresCmdId getCmdId() {
    return this.mCmdId;
  }
  
  public void setCmdId(PresCmdId paramPresCmdId) {
    this.mCmdId = paramPresCmdId;
  }
  
  public int getUserData() {
    return this.mUserData;
  }
  
  public void setUserData(int paramInt) {
    this.mUserData = paramInt;
  }
  
  public StatusCode getStatus() {
    return this.mStatus;
  }
  
  public void setStatus(StatusCode paramStatusCode) {
    this.mStatus = paramStatusCode;
  }
  
  public int getRequestId() {
    return this.mRequestId;
  }
  
  public void setRequestId(int paramInt) {
    this.mRequestId = paramInt;
  }
  
  public PresCmdStatus() {
    this.mStatus = new StatusCode();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mUserData);
    paramParcel.writeInt(this.mRequestId);
    paramParcel.writeParcelable(this.mCmdId, paramInt);
    paramParcel.writeParcelable(this.mStatus, paramInt);
  }
  
  public static final Parcelable.Creator<PresCmdStatus> CREATOR = new Parcelable.Creator<PresCmdStatus>() {
      public PresCmdStatus createFromParcel(Parcel param1Parcel) {
        return new PresCmdStatus(param1Parcel);
      }
      
      public PresCmdStatus[] newArray(int param1Int) {
        return new PresCmdStatus[param1Int];
      }
    };
  
  private int mRequestId;
  
  private int mUserData;
  
  private PresCmdStatus(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mUserData = paramParcel.readInt();
    this.mRequestId = paramParcel.readInt();
    this.mCmdId = (PresCmdId)paramParcel.readParcelable(PresCmdId.class.getClassLoader());
    this.mStatus = (StatusCode)paramParcel.readParcelable(StatusCode.class.getClassLoader());
  }
}
