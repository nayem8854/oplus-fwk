package com.android.ims.internal.uce.presence;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.android.ims.internal.uce.common.StatusCode;
import com.android.ims.internal.uce.common.UceLong;

public interface IPresenceService extends IInterface {
  StatusCode addListener(int paramInt, IPresenceListener paramIPresenceListener, UceLong paramUceLong) throws RemoteException;
  
  StatusCode getContactCap(int paramInt1, String paramString, int paramInt2) throws RemoteException;
  
  StatusCode getContactListCap(int paramInt1, String[] paramArrayOfString, int paramInt2) throws RemoteException;
  
  StatusCode getVersion(int paramInt) throws RemoteException;
  
  StatusCode publishMyCap(int paramInt1, PresCapInfo paramPresCapInfo, int paramInt2) throws RemoteException;
  
  StatusCode reenableService(int paramInt1, int paramInt2) throws RemoteException;
  
  StatusCode removeListener(int paramInt, UceLong paramUceLong) throws RemoteException;
  
  StatusCode setNewFeatureTag(int paramInt1, String paramString, PresServiceInfo paramPresServiceInfo, int paramInt2) throws RemoteException;
  
  class Default implements IPresenceService {
    public StatusCode getVersion(int param1Int) throws RemoteException {
      return null;
    }
    
    public StatusCode addListener(int param1Int, IPresenceListener param1IPresenceListener, UceLong param1UceLong) throws RemoteException {
      return null;
    }
    
    public StatusCode removeListener(int param1Int, UceLong param1UceLong) throws RemoteException {
      return null;
    }
    
    public StatusCode reenableService(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public StatusCode publishMyCap(int param1Int1, PresCapInfo param1PresCapInfo, int param1Int2) throws RemoteException {
      return null;
    }
    
    public StatusCode getContactCap(int param1Int1, String param1String, int param1Int2) throws RemoteException {
      return null;
    }
    
    public StatusCode getContactListCap(int param1Int1, String[] param1ArrayOfString, int param1Int2) throws RemoteException {
      return null;
    }
    
    public StatusCode setNewFeatureTag(int param1Int1, String param1String, PresServiceInfo param1PresServiceInfo, int param1Int2) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPresenceService {
    private static final String DESCRIPTOR = "com.android.ims.internal.uce.presence.IPresenceService";
    
    static final int TRANSACTION_addListener = 2;
    
    static final int TRANSACTION_getContactCap = 6;
    
    static final int TRANSACTION_getContactListCap = 7;
    
    static final int TRANSACTION_getVersion = 1;
    
    static final int TRANSACTION_publishMyCap = 5;
    
    static final int TRANSACTION_reenableService = 4;
    
    static final int TRANSACTION_removeListener = 3;
    
    static final int TRANSACTION_setNewFeatureTag = 8;
    
    public Stub() {
      attachInterface(this, "com.android.ims.internal.uce.presence.IPresenceService");
    }
    
    public static IPresenceService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.ims.internal.uce.presence.IPresenceService");
      if (iInterface != null && iInterface instanceof IPresenceService)
        return (IPresenceService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 8:
          return "setNewFeatureTag";
        case 7:
          return "getContactListCap";
        case 6:
          return "getContactCap";
        case 5:
          return "publishMyCap";
        case 4:
          return "reenableService";
        case 3:
          return "removeListener";
        case 2:
          return "addListener";
        case 1:
          break;
      } 
      return "getVersion";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        String str1;
        PresServiceInfo presServiceInfo;
        String arrayOfString[], str2;
        IPresenceListener iPresenceListener;
        StatusCode statusCode2;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 8:
            param1Parcel1.enforceInterface("com.android.ims.internal.uce.presence.IPresenceService");
            param1Int1 = param1Parcel1.readInt();
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              presServiceInfo = (PresServiceInfo)PresServiceInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              presServiceInfo = null;
            } 
            param1Int2 = param1Parcel1.readInt();
            statusCode1 = setNewFeatureTag(param1Int1, str1, presServiceInfo, param1Int2);
            param1Parcel2.writeNoException();
            if (statusCode1 != null) {
              param1Parcel2.writeInt(1);
              statusCode1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 7:
            statusCode1.enforceInterface("com.android.ims.internal.uce.presence.IPresenceService");
            param1Int2 = statusCode1.readInt();
            arrayOfString = statusCode1.createStringArray();
            param1Int1 = statusCode1.readInt();
            statusCode1 = getContactListCap(param1Int2, arrayOfString, param1Int1);
            param1Parcel2.writeNoException();
            if (statusCode1 != null) {
              param1Parcel2.writeInt(1);
              statusCode1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 6:
            statusCode1.enforceInterface("com.android.ims.internal.uce.presence.IPresenceService");
            param1Int1 = statusCode1.readInt();
            str2 = statusCode1.readString();
            param1Int2 = statusCode1.readInt();
            statusCode1 = getContactCap(param1Int1, str2, param1Int2);
            param1Parcel2.writeNoException();
            if (statusCode1 != null) {
              param1Parcel2.writeInt(1);
              statusCode1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 5:
            statusCode1.enforceInterface("com.android.ims.internal.uce.presence.IPresenceService");
            param1Int1 = statusCode1.readInt();
            if (statusCode1.readInt() != 0) {
              PresCapInfo presCapInfo = (PresCapInfo)PresCapInfo.CREATOR.createFromParcel((Parcel)statusCode1);
            } else {
              str2 = null;
            } 
            param1Int2 = statusCode1.readInt();
            statusCode1 = publishMyCap(param1Int1, (PresCapInfo)str2, param1Int2);
            param1Parcel2.writeNoException();
            if (statusCode1 != null) {
              param1Parcel2.writeInt(1);
              statusCode1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 4:
            statusCode1.enforceInterface("com.android.ims.internal.uce.presence.IPresenceService");
            param1Int1 = statusCode1.readInt();
            param1Int2 = statusCode1.readInt();
            statusCode1 = reenableService(param1Int1, param1Int2);
            param1Parcel2.writeNoException();
            if (statusCode1 != null) {
              param1Parcel2.writeInt(1);
              statusCode1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 3:
            statusCode1.enforceInterface("com.android.ims.internal.uce.presence.IPresenceService");
            param1Int1 = statusCode1.readInt();
            if (statusCode1.readInt() != 0) {
              UceLong uceLong = (UceLong)UceLong.CREATOR.createFromParcel((Parcel)statusCode1);
            } else {
              statusCode1 = null;
            } 
            statusCode1 = removeListener(param1Int1, (UceLong)statusCode1);
            param1Parcel2.writeNoException();
            if (statusCode1 != null) {
              param1Parcel2.writeInt(1);
              statusCode1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 2:
            statusCode1.enforceInterface("com.android.ims.internal.uce.presence.IPresenceService");
            param1Int1 = statusCode1.readInt();
            iPresenceListener = IPresenceListener.Stub.asInterface(statusCode1.readStrongBinder());
            if (statusCode1.readInt() != 0) {
              UceLong uceLong = (UceLong)UceLong.CREATOR.createFromParcel((Parcel)statusCode1);
            } else {
              statusCode1 = null;
            } 
            statusCode2 = addListener(param1Int1, iPresenceListener, (UceLong)statusCode1);
            param1Parcel2.writeNoException();
            if (statusCode2 != null) {
              param1Parcel2.writeInt(1);
              statusCode2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            if (statusCode1 != null) {
              param1Parcel2.writeInt(1);
              statusCode1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 1:
            break;
        } 
        statusCode1.enforceInterface("com.android.ims.internal.uce.presence.IPresenceService");
        param1Int1 = statusCode1.readInt();
        StatusCode statusCode1 = getVersion(param1Int1);
        param1Parcel2.writeNoException();
        if (statusCode1 != null) {
          param1Parcel2.writeInt(1);
          statusCode1.writeToParcel(param1Parcel2, 1);
        } else {
          param1Parcel2.writeInt(0);
        } 
        return true;
      } 
      param1Parcel2.writeString("com.android.ims.internal.uce.presence.IPresenceService");
      return true;
    }
    
    private static class Proxy implements IPresenceService {
      public static IPresenceService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.ims.internal.uce.presence.IPresenceService";
      }
      
      public StatusCode getVersion(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          StatusCode statusCode;
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.presence.IPresenceService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IPresenceService.Stub.getDefaultImpl() != null) {
            statusCode = IPresenceService.Stub.getDefaultImpl().getVersion(param2Int);
            return statusCode;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            statusCode = (StatusCode)StatusCode.CREATOR.createFromParcel(parcel2);
          } else {
            statusCode = null;
          } 
          return statusCode;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public StatusCode addListener(int param2Int, IPresenceListener param2IPresenceListener, UceLong param2UceLong) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.presence.IPresenceService");
          parcel1.writeInt(param2Int);
          if (param2IPresenceListener != null) {
            iBinder = param2IPresenceListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2UceLong != null) {
            parcel1.writeInt(1);
            param2UceLong.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IPresenceService.Stub.getDefaultImpl() != null)
            return IPresenceService.Stub.getDefaultImpl().addListener(param2Int, param2IPresenceListener, param2UceLong); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            StatusCode statusCode = (StatusCode)StatusCode.CREATOR.createFromParcel(parcel2);
          } else {
            param2IPresenceListener = null;
          } 
          if (parcel2.readInt() != 0)
            param2UceLong.readFromParcel(parcel2); 
          return (StatusCode)param2IPresenceListener;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public StatusCode removeListener(int param2Int, UceLong param2UceLong) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.presence.IPresenceService");
          parcel1.writeInt(param2Int);
          if (param2UceLong != null) {
            parcel1.writeInt(1);
            param2UceLong.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IPresenceService.Stub.getDefaultImpl() != null)
            return IPresenceService.Stub.getDefaultImpl().removeListener(param2Int, param2UceLong); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            StatusCode statusCode = (StatusCode)StatusCode.CREATOR.createFromParcel(parcel2);
          } else {
            param2UceLong = null;
          } 
          return (StatusCode)param2UceLong;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public StatusCode reenableService(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          StatusCode statusCode;
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.presence.IPresenceService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IPresenceService.Stub.getDefaultImpl() != null) {
            statusCode = IPresenceService.Stub.getDefaultImpl().reenableService(param2Int1, param2Int2);
            return statusCode;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            statusCode = (StatusCode)StatusCode.CREATOR.createFromParcel(parcel2);
          } else {
            statusCode = null;
          } 
          return statusCode;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public StatusCode publishMyCap(int param2Int1, PresCapInfo param2PresCapInfo, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.presence.IPresenceService");
          parcel1.writeInt(param2Int1);
          if (param2PresCapInfo != null) {
            parcel1.writeInt(1);
            param2PresCapInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IPresenceService.Stub.getDefaultImpl() != null)
            return IPresenceService.Stub.getDefaultImpl().publishMyCap(param2Int1, param2PresCapInfo, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            StatusCode statusCode = (StatusCode)StatusCode.CREATOR.createFromParcel(parcel2);
          } else {
            param2PresCapInfo = null;
          } 
          return (StatusCode)param2PresCapInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public StatusCode getContactCap(int param2Int1, String param2String, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.presence.IPresenceService");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IPresenceService.Stub.getDefaultImpl() != null)
            return IPresenceService.Stub.getDefaultImpl().getContactCap(param2Int1, param2String, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            StatusCode statusCode = (StatusCode)StatusCode.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (StatusCode)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public StatusCode getContactListCap(int param2Int1, String[] param2ArrayOfString, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.presence.IPresenceService");
          parcel1.writeInt(param2Int1);
          parcel1.writeStringArray(param2ArrayOfString);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IPresenceService.Stub.getDefaultImpl() != null)
            return IPresenceService.Stub.getDefaultImpl().getContactListCap(param2Int1, param2ArrayOfString, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            StatusCode statusCode = (StatusCode)StatusCode.CREATOR.createFromParcel(parcel2);
          } else {
            param2ArrayOfString = null;
          } 
          return (StatusCode)param2ArrayOfString;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public StatusCode setNewFeatureTag(int param2Int1, String param2String, PresServiceInfo param2PresServiceInfo, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.presence.IPresenceService");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String);
          if (param2PresServiceInfo != null) {
            parcel1.writeInt(1);
            param2PresServiceInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IPresenceService.Stub.getDefaultImpl() != null)
            return IPresenceService.Stub.getDefaultImpl().setNewFeatureTag(param2Int1, param2String, param2PresServiceInfo, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            StatusCode statusCode = (StatusCode)StatusCode.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (StatusCode)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPresenceService param1IPresenceService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPresenceService != null) {
          Proxy.sDefaultImpl = param1IPresenceService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPresenceService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
