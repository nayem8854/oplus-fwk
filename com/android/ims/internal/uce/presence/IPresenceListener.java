package com.android.ims.internal.uce.presence;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.android.ims.internal.uce.common.StatusCode;

public interface IPresenceListener extends IInterface {
  void capInfoReceived(String paramString, PresTupleInfo[] paramArrayOfPresTupleInfo) throws RemoteException;
  
  void cmdStatus(PresCmdStatus paramPresCmdStatus) throws RemoteException;
  
  void getVersionCb(String paramString) throws RemoteException;
  
  void listCapInfoReceived(PresRlmiInfo paramPresRlmiInfo, PresResInfo[] paramArrayOfPresResInfo) throws RemoteException;
  
  void publishTriggering(PresPublishTriggerType paramPresPublishTriggerType) throws RemoteException;
  
  void serviceAvailable(StatusCode paramStatusCode) throws RemoteException;
  
  void serviceUnAvailable(StatusCode paramStatusCode) throws RemoteException;
  
  void sipResponseReceived(PresSipResponse paramPresSipResponse) throws RemoteException;
  
  void unpublishMessageSent() throws RemoteException;
  
  class Default implements IPresenceListener {
    public void getVersionCb(String param1String) throws RemoteException {}
    
    public void serviceAvailable(StatusCode param1StatusCode) throws RemoteException {}
    
    public void serviceUnAvailable(StatusCode param1StatusCode) throws RemoteException {}
    
    public void publishTriggering(PresPublishTriggerType param1PresPublishTriggerType) throws RemoteException {}
    
    public void cmdStatus(PresCmdStatus param1PresCmdStatus) throws RemoteException {}
    
    public void sipResponseReceived(PresSipResponse param1PresSipResponse) throws RemoteException {}
    
    public void capInfoReceived(String param1String, PresTupleInfo[] param1ArrayOfPresTupleInfo) throws RemoteException {}
    
    public void listCapInfoReceived(PresRlmiInfo param1PresRlmiInfo, PresResInfo[] param1ArrayOfPresResInfo) throws RemoteException {}
    
    public void unpublishMessageSent() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPresenceListener {
    private static final String DESCRIPTOR = "com.android.ims.internal.uce.presence.IPresenceListener";
    
    static final int TRANSACTION_capInfoReceived = 7;
    
    static final int TRANSACTION_cmdStatus = 5;
    
    static final int TRANSACTION_getVersionCb = 1;
    
    static final int TRANSACTION_listCapInfoReceived = 8;
    
    static final int TRANSACTION_publishTriggering = 4;
    
    static final int TRANSACTION_serviceAvailable = 2;
    
    static final int TRANSACTION_serviceUnAvailable = 3;
    
    static final int TRANSACTION_sipResponseReceived = 6;
    
    static final int TRANSACTION_unpublishMessageSent = 9;
    
    public Stub() {
      attachInterface(this, "com.android.ims.internal.uce.presence.IPresenceListener");
    }
    
    public static IPresenceListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.ims.internal.uce.presence.IPresenceListener");
      if (iInterface != null && iInterface instanceof IPresenceListener)
        return (IPresenceListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 9:
          return "unpublishMessageSent";
        case 8:
          return "listCapInfoReceived";
        case 7:
          return "capInfoReceived";
        case 6:
          return "sipResponseReceived";
        case 5:
          return "cmdStatus";
        case 4:
          return "publishTriggering";
        case 3:
          return "serviceUnAvailable";
        case 2:
          return "serviceAvailable";
        case 1:
          break;
      } 
      return "getVersionCb";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        PresResInfo[] arrayOfPresResInfo;
        PresTupleInfo[] arrayOfPresTupleInfo;
        PresRlmiInfo presRlmiInfo;
        String str2;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 9:
            param1Parcel1.enforceInterface("com.android.ims.internal.uce.presence.IPresenceListener");
            unpublishMessageSent();
            param1Parcel2.writeNoException();
            return true;
          case 8:
            param1Parcel1.enforceInterface("com.android.ims.internal.uce.presence.IPresenceListener");
            if (param1Parcel1.readInt() != 0) {
              presRlmiInfo = (PresRlmiInfo)PresRlmiInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              presRlmiInfo = null;
            } 
            arrayOfPresResInfo = (PresResInfo[])param1Parcel1.createTypedArray(PresResInfo.CREATOR);
            listCapInfoReceived(presRlmiInfo, arrayOfPresResInfo);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            arrayOfPresResInfo.enforceInterface("com.android.ims.internal.uce.presence.IPresenceListener");
            str2 = arrayOfPresResInfo.readString();
            arrayOfPresTupleInfo = (PresTupleInfo[])arrayOfPresResInfo.createTypedArray(PresTupleInfo.CREATOR);
            capInfoReceived(str2, arrayOfPresTupleInfo);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            arrayOfPresTupleInfo.enforceInterface("com.android.ims.internal.uce.presence.IPresenceListener");
            if (arrayOfPresTupleInfo.readInt() != 0) {
              PresSipResponse presSipResponse = (PresSipResponse)PresSipResponse.CREATOR.createFromParcel((Parcel)arrayOfPresTupleInfo);
            } else {
              arrayOfPresTupleInfo = null;
            } 
            sipResponseReceived((PresSipResponse)arrayOfPresTupleInfo);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            arrayOfPresTupleInfo.enforceInterface("com.android.ims.internal.uce.presence.IPresenceListener");
            if (arrayOfPresTupleInfo.readInt() != 0) {
              PresCmdStatus presCmdStatus = (PresCmdStatus)PresCmdStatus.CREATOR.createFromParcel((Parcel)arrayOfPresTupleInfo);
            } else {
              arrayOfPresTupleInfo = null;
            } 
            cmdStatus((PresCmdStatus)arrayOfPresTupleInfo);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            arrayOfPresTupleInfo.enforceInterface("com.android.ims.internal.uce.presence.IPresenceListener");
            if (arrayOfPresTupleInfo.readInt() != 0) {
              PresPublishTriggerType presPublishTriggerType = (PresPublishTriggerType)PresPublishTriggerType.CREATOR.createFromParcel((Parcel)arrayOfPresTupleInfo);
            } else {
              arrayOfPresTupleInfo = null;
            } 
            publishTriggering((PresPublishTriggerType)arrayOfPresTupleInfo);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            arrayOfPresTupleInfo.enforceInterface("com.android.ims.internal.uce.presence.IPresenceListener");
            if (arrayOfPresTupleInfo.readInt() != 0) {
              StatusCode statusCode = (StatusCode)StatusCode.CREATOR.createFromParcel((Parcel)arrayOfPresTupleInfo);
            } else {
              arrayOfPresTupleInfo = null;
            } 
            serviceUnAvailable((StatusCode)arrayOfPresTupleInfo);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            arrayOfPresTupleInfo.enforceInterface("com.android.ims.internal.uce.presence.IPresenceListener");
            if (arrayOfPresTupleInfo.readInt() != 0) {
              StatusCode statusCode = (StatusCode)StatusCode.CREATOR.createFromParcel((Parcel)arrayOfPresTupleInfo);
            } else {
              arrayOfPresTupleInfo = null;
            } 
            serviceAvailable((StatusCode)arrayOfPresTupleInfo);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        arrayOfPresTupleInfo.enforceInterface("com.android.ims.internal.uce.presence.IPresenceListener");
        String str1 = arrayOfPresTupleInfo.readString();
        getVersionCb(str1);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("com.android.ims.internal.uce.presence.IPresenceListener");
      return true;
    }
    
    private static class Proxy implements IPresenceListener {
      public static IPresenceListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.ims.internal.uce.presence.IPresenceListener";
      }
      
      public void getVersionCb(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.presence.IPresenceListener");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IPresenceListener.Stub.getDefaultImpl() != null) {
            IPresenceListener.Stub.getDefaultImpl().getVersionCb(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void serviceAvailable(StatusCode param2StatusCode) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.presence.IPresenceListener");
          if (param2StatusCode != null) {
            parcel1.writeInt(1);
            param2StatusCode.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IPresenceListener.Stub.getDefaultImpl() != null) {
            IPresenceListener.Stub.getDefaultImpl().serviceAvailable(param2StatusCode);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void serviceUnAvailable(StatusCode param2StatusCode) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.presence.IPresenceListener");
          if (param2StatusCode != null) {
            parcel1.writeInt(1);
            param2StatusCode.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IPresenceListener.Stub.getDefaultImpl() != null) {
            IPresenceListener.Stub.getDefaultImpl().serviceUnAvailable(param2StatusCode);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void publishTriggering(PresPublishTriggerType param2PresPublishTriggerType) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.presence.IPresenceListener");
          if (param2PresPublishTriggerType != null) {
            parcel1.writeInt(1);
            param2PresPublishTriggerType.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IPresenceListener.Stub.getDefaultImpl() != null) {
            IPresenceListener.Stub.getDefaultImpl().publishTriggering(param2PresPublishTriggerType);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cmdStatus(PresCmdStatus param2PresCmdStatus) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.presence.IPresenceListener");
          if (param2PresCmdStatus != null) {
            parcel1.writeInt(1);
            param2PresCmdStatus.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IPresenceListener.Stub.getDefaultImpl() != null) {
            IPresenceListener.Stub.getDefaultImpl().cmdStatus(param2PresCmdStatus);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sipResponseReceived(PresSipResponse param2PresSipResponse) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.presence.IPresenceListener");
          if (param2PresSipResponse != null) {
            parcel1.writeInt(1);
            param2PresSipResponse.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IPresenceListener.Stub.getDefaultImpl() != null) {
            IPresenceListener.Stub.getDefaultImpl().sipResponseReceived(param2PresSipResponse);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void capInfoReceived(String param2String, PresTupleInfo[] param2ArrayOfPresTupleInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.presence.IPresenceListener");
          parcel1.writeString(param2String);
          parcel1.writeTypedArray((Parcelable[])param2ArrayOfPresTupleInfo, 0);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IPresenceListener.Stub.getDefaultImpl() != null) {
            IPresenceListener.Stub.getDefaultImpl().capInfoReceived(param2String, param2ArrayOfPresTupleInfo);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void listCapInfoReceived(PresRlmiInfo param2PresRlmiInfo, PresResInfo[] param2ArrayOfPresResInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.presence.IPresenceListener");
          if (param2PresRlmiInfo != null) {
            parcel1.writeInt(1);
            param2PresRlmiInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeTypedArray((Parcelable[])param2ArrayOfPresResInfo, 0);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IPresenceListener.Stub.getDefaultImpl() != null) {
            IPresenceListener.Stub.getDefaultImpl().listCapInfoReceived(param2PresRlmiInfo, param2ArrayOfPresResInfo);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unpublishMessageSent() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.presence.IPresenceListener");
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IPresenceListener.Stub.getDefaultImpl() != null) {
            IPresenceListener.Stub.getDefaultImpl().unpublishMessageSent();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPresenceListener param1IPresenceListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPresenceListener != null) {
          Proxy.sDefaultImpl = param1IPresenceListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPresenceListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
