package com.android.ims.internal.uce.presence;

import android.os.Parcel;
import android.os.Parcelable;

public class PresServiceInfo implements Parcelable {
  private int mMediaCap = 0;
  
  private String mServiceID = "";
  
  private String mServiceDesc = "";
  
  private String mServiceVer = "";
  
  public int getMediaType() {
    return this.mMediaCap;
  }
  
  public void setMediaType(int paramInt) {
    this.mMediaCap = paramInt;
  }
  
  public String getServiceId() {
    return this.mServiceID;
  }
  
  public void setServiceId(String paramString) {
    this.mServiceID = paramString;
  }
  
  public String getServiceDesc() {
    return this.mServiceDesc;
  }
  
  public void setServiceDesc(String paramString) {
    this.mServiceDesc = paramString;
  }
  
  public String getServiceVer() {
    return this.mServiceVer;
  }
  
  public void setServiceVer(String paramString) {
    this.mServiceVer = paramString;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mServiceID);
    paramParcel.writeString(this.mServiceDesc);
    paramParcel.writeString(this.mServiceVer);
    paramParcel.writeInt(this.mMediaCap);
  }
  
  public static final Parcelable.Creator<PresServiceInfo> CREATOR = new Parcelable.Creator<PresServiceInfo>() {
      public PresServiceInfo createFromParcel(Parcel param1Parcel) {
        return new PresServiceInfo(param1Parcel);
      }
      
      public PresServiceInfo[] newArray(int param1Int) {
        return new PresServiceInfo[param1Int];
      }
    };
  
  public static final int UCE_PRES_MEDIA_CAP_FULL_AUDIO_AND_VIDEO = 2;
  
  public static final int UCE_PRES_MEDIA_CAP_FULL_AUDIO_ONLY = 1;
  
  public static final int UCE_PRES_MEDIA_CAP_NONE = 0;
  
  public static final int UCE_PRES_MEDIA_CAP_UNKNOWN = 3;
  
  private PresServiceInfo(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mServiceID = paramParcel.readString();
    this.mServiceDesc = paramParcel.readString();
    this.mServiceVer = paramParcel.readString();
    this.mMediaCap = paramParcel.readInt();
  }
  
  public PresServiceInfo() {}
}
