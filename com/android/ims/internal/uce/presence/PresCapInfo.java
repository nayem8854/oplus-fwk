package com.android.ims.internal.uce.presence;

import android.os.Parcel;
import android.os.Parcelable;
import com.android.ims.internal.uce.common.CapInfo;

public class PresCapInfo implements Parcelable {
  private String mContactUri = "";
  
  private CapInfo mCapInfo;
  
  public CapInfo getCapInfo() {
    return this.mCapInfo;
  }
  
  public void setCapInfo(CapInfo paramCapInfo) {
    this.mCapInfo = paramCapInfo;
  }
  
  public String getContactUri() {
    return this.mContactUri;
  }
  
  public void setContactUri(String paramString) {
    this.mContactUri = paramString;
  }
  
  public PresCapInfo() {
    this.mCapInfo = new CapInfo();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mContactUri);
    paramParcel.writeParcelable(this.mCapInfo, paramInt);
  }
  
  public static final Parcelable.Creator<PresCapInfo> CREATOR = new Parcelable.Creator<PresCapInfo>() {
      public PresCapInfo createFromParcel(Parcel param1Parcel) {
        return new PresCapInfo(param1Parcel);
      }
      
      public PresCapInfo[] newArray(int param1Int) {
        return new PresCapInfo[param1Int];
      }
    };
  
  private PresCapInfo(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mContactUri = paramParcel.readString();
    this.mCapInfo = (CapInfo)paramParcel.readParcelable(CapInfo.class.getClassLoader());
  }
}
