package com.android.ims.internal.uce.presence;

import android.os.Parcel;
import android.os.Parcelable;

public class PresSubscriptionState implements Parcelable {
  private int mPresSubscriptionState = 3;
  
  public static final int UCE_PRES_SUBSCRIPTION_STATE_UNKNOWN = 3;
  
  public static final int UCE_PRES_SUBSCRIPTION_STATE_TERMINATED = 2;
  
  public static final int UCE_PRES_SUBSCRIPTION_STATE_PENDING = 1;
  
  public static final int UCE_PRES_SUBSCRIPTION_STATE_ACTIVE = 0;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mPresSubscriptionState);
  }
  
  public static final Parcelable.Creator<PresSubscriptionState> CREATOR = new Parcelable.Creator<PresSubscriptionState>() {
      public PresSubscriptionState createFromParcel(Parcel param1Parcel) {
        return new PresSubscriptionState(param1Parcel);
      }
      
      public PresSubscriptionState[] newArray(int param1Int) {
        return new PresSubscriptionState[param1Int];
      }
    };
  
  private PresSubscriptionState(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mPresSubscriptionState = paramParcel.readInt();
  }
  
  public int getPresSubscriptionStateValue() {
    return this.mPresSubscriptionState;
  }
  
  public void setPresSubscriptionState(int paramInt) {
    this.mPresSubscriptionState = paramInt;
  }
  
  public PresSubscriptionState() {}
}
