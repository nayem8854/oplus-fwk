package com.android.ims.internal.uce.presence;

import android.os.Parcel;
import android.os.Parcelable;

public class PresRlmiInfo implements Parcelable {
  private int mVersion;
  
  private String mUri = "";
  
  private String mSubscriptionTerminatedReason;
  
  private int mSubscriptionExpireTime;
  
  private int mRequestId;
  
  private PresSubscriptionState mPresSubscriptionState;
  
  private String mListName = "";
  
  private boolean mFullState;
  
  public String getUri() {
    return this.mUri;
  }
  
  public void setUri(String paramString) {
    this.mUri = paramString;
  }
  
  public int getVersion() {
    return this.mVersion;
  }
  
  public void setVersion(int paramInt) {
    this.mVersion = paramInt;
  }
  
  public boolean isFullState() {
    return this.mFullState;
  }
  
  public void setFullState(boolean paramBoolean) {
    this.mFullState = paramBoolean;
  }
  
  public String getListName() {
    return this.mListName;
  }
  
  public void setListName(String paramString) {
    this.mListName = paramString;
  }
  
  public int getRequestId() {
    return this.mRequestId;
  }
  
  public void setRequestId(int paramInt) {
    this.mRequestId = paramInt;
  }
  
  public PresSubscriptionState getPresSubscriptionState() {
    return this.mPresSubscriptionState;
  }
  
  public void setPresSubscriptionState(PresSubscriptionState paramPresSubscriptionState) {
    this.mPresSubscriptionState = paramPresSubscriptionState;
  }
  
  public int getSubscriptionExpireTime() {
    return this.mSubscriptionExpireTime;
  }
  
  public void setSubscriptionExpireTime(int paramInt) {
    this.mSubscriptionExpireTime = paramInt;
  }
  
  public String getSubscriptionTerminatedReason() {
    return this.mSubscriptionTerminatedReason;
  }
  
  public void setSubscriptionTerminatedReason(String paramString) {
    this.mSubscriptionTerminatedReason = paramString;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mUri);
    paramParcel.writeInt(this.mVersion);
    paramParcel.writeInt(this.mFullState);
    paramParcel.writeString(this.mListName);
    paramParcel.writeInt(this.mRequestId);
    paramParcel.writeParcelable(this.mPresSubscriptionState, paramInt);
    paramParcel.writeInt(this.mSubscriptionExpireTime);
    paramParcel.writeString(this.mSubscriptionTerminatedReason);
  }
  
  public static final Parcelable.Creator<PresRlmiInfo> CREATOR = new Parcelable.Creator<PresRlmiInfo>() {
      public PresRlmiInfo createFromParcel(Parcel param1Parcel) {
        return new PresRlmiInfo(param1Parcel);
      }
      
      public PresRlmiInfo[] newArray(int param1Int) {
        return new PresRlmiInfo[param1Int];
      }
    };
  
  private PresRlmiInfo(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    boolean bool;
    this.mUri = paramParcel.readString();
    this.mVersion = paramParcel.readInt();
    if (paramParcel.readInt() == 0) {
      bool = false;
    } else {
      bool = true;
    } 
    this.mFullState = bool;
    this.mListName = paramParcel.readString();
    this.mRequestId = paramParcel.readInt();
    ClassLoader classLoader = PresSubscriptionState.class.getClassLoader();
    this.mPresSubscriptionState = (PresSubscriptionState)paramParcel.readParcelable(classLoader);
    this.mSubscriptionExpireTime = paramParcel.readInt();
    this.mSubscriptionTerminatedReason = paramParcel.readString();
  }
  
  public PresRlmiInfo() {}
}
