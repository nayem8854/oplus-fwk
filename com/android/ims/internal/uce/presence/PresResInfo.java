package com.android.ims.internal.uce.presence;

import android.os.Parcel;
import android.os.Parcelable;

public class PresResInfo implements Parcelable {
  private String mResUri = "";
  
  private PresResInstanceInfo mInstanceInfo;
  
  private String mDisplayName = "";
  
  public PresResInstanceInfo getInstanceInfo() {
    return this.mInstanceInfo;
  }
  
  public void setInstanceInfo(PresResInstanceInfo paramPresResInstanceInfo) {
    this.mInstanceInfo = paramPresResInstanceInfo;
  }
  
  public String getResUri() {
    return this.mResUri;
  }
  
  public void setResUri(String paramString) {
    this.mResUri = paramString;
  }
  
  public String getDisplayName() {
    return this.mDisplayName;
  }
  
  public void setDisplayName(String paramString) {
    this.mDisplayName = paramString;
  }
  
  public PresResInfo() {
    this.mInstanceInfo = new PresResInstanceInfo();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mResUri);
    paramParcel.writeString(this.mDisplayName);
    paramParcel.writeParcelable(this.mInstanceInfo, paramInt);
  }
  
  public static final Parcelable.Creator<PresResInfo> CREATOR = new Parcelable.Creator<PresResInfo>() {
      public PresResInfo createFromParcel(Parcel param1Parcel) {
        return new PresResInfo(param1Parcel);
      }
      
      public PresResInfo[] newArray(int param1Int) {
        return new PresResInfo[param1Int];
      }
    };
  
  private PresResInfo(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mResUri = paramParcel.readString();
    this.mDisplayName = paramParcel.readString();
    this.mInstanceInfo = (PresResInstanceInfo)paramParcel.readParcelable(PresResInstanceInfo.class.getClassLoader());
  }
}
