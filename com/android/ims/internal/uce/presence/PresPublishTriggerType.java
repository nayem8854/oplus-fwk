package com.android.ims.internal.uce.presence;

import android.os.Parcel;
import android.os.Parcelable;

public class PresPublishTriggerType implements Parcelable {
  private int mPublishTriggerType = 9;
  
  public static final int UCE_PRES_PUBLISH_TRIGGER_UNKNOWN = 9;
  
  public static final int UCE_PRES_PUBLISH_TRIGGER_MOVE_TO_WLAN = 7;
  
  public static final int UCE_PRES_PUBLISH_TRIGGER_MOVE_TO_NR5G_VOPS_ENABLED = 11;
  
  public static final int UCE_PRES_PUBLISH_TRIGGER_MOVE_TO_NR5G_VOPS_DISABLED = 10;
  
  public static final int UCE_PRES_PUBLISH_TRIGGER_MOVE_TO_LTE_VOPS_ENABLED = 2;
  
  public static final int UCE_PRES_PUBLISH_TRIGGER_MOVE_TO_LTE_VOPS_DISABLED = 1;
  
  public static final int UCE_PRES_PUBLISH_TRIGGER_MOVE_TO_IWLAN = 8;
  
  public static final int UCE_PRES_PUBLISH_TRIGGER_MOVE_TO_HSPAPLUS = 4;
  
  public static final int UCE_PRES_PUBLISH_TRIGGER_MOVE_TO_EHRPD = 3;
  
  public static final int UCE_PRES_PUBLISH_TRIGGER_MOVE_TO_3G = 5;
  
  public static final int UCE_PRES_PUBLISH_TRIGGER_MOVE_TO_2G = 6;
  
  public static final int UCE_PRES_PUBLISH_TRIGGER_ETAG_EXPIRED = 0;
  
  public int getPublishTrigeerType() {
    return this.mPublishTriggerType;
  }
  
  public void setPublishTrigeerType(int paramInt) {
    this.mPublishTriggerType = paramInt;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mPublishTriggerType);
  }
  
  public static final Parcelable.Creator<PresPublishTriggerType> CREATOR = new Parcelable.Creator<PresPublishTriggerType>() {
      public PresPublishTriggerType createFromParcel(Parcel param1Parcel) {
        return new PresPublishTriggerType(param1Parcel);
      }
      
      public PresPublishTriggerType[] newArray(int param1Int) {
        return new PresPublishTriggerType[param1Int];
      }
    };
  
  private PresPublishTriggerType(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mPublishTriggerType = paramParcel.readInt();
  }
  
  public PresPublishTriggerType() {}
}
