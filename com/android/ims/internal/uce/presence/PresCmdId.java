package com.android.ims.internal.uce.presence;

import android.os.Parcel;
import android.os.Parcelable;

public class PresCmdId implements Parcelable {
  private int mCmdId = 6;
  
  public static final int UCE_PRES_CMD_UNKNOWN = 6;
  
  public static final int UCE_PRES_CMD_SETNEWFEATURETAG = 4;
  
  public static final int UCE_PRES_CMD_REENABLE_SERVICE = 5;
  
  public static final int UCE_PRES_CMD_PUBLISHMYCAP = 1;
  
  public static final int UCE_PRES_CMD_GET_VERSION = 0;
  
  public static final int UCE_PRES_CMD_GETCONTACTLISTCAP = 3;
  
  public static final int UCE_PRES_CMD_GETCONTACTCAP = 2;
  
  public int getCmdId() {
    return this.mCmdId;
  }
  
  public void setCmdId(int paramInt) {
    this.mCmdId = paramInt;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mCmdId);
  }
  
  public static final Parcelable.Creator<PresCmdId> CREATOR = new Parcelable.Creator<PresCmdId>() {
      public PresCmdId createFromParcel(Parcel param1Parcel) {
        return new PresCmdId(param1Parcel);
      }
      
      public PresCmdId[] newArray(int param1Int) {
        return new PresCmdId[param1Int];
      }
    };
  
  private PresCmdId(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mCmdId = paramParcel.readInt();
  }
  
  public PresCmdId() {}
}
