package com.android.ims.internal.uce.presence;

import android.os.Parcel;
import android.os.Parcelable;

public class PresTupleInfo implements Parcelable {
  private String mFeatureTag = "";
  
  private String mContactUri = "";
  
  private String mTimestamp = "";
  
  public String getFeatureTag() {
    return this.mFeatureTag;
  }
  
  public void setFeatureTag(String paramString) {
    this.mFeatureTag = paramString;
  }
  
  public String getContactUri() {
    return this.mContactUri;
  }
  
  public void setContactUri(String paramString) {
    this.mContactUri = paramString;
  }
  
  public String getTimestamp() {
    return this.mTimestamp;
  }
  
  public void setTimestamp(String paramString) {
    this.mTimestamp = paramString;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mFeatureTag);
    paramParcel.writeString(this.mContactUri);
    paramParcel.writeString(this.mTimestamp);
  }
  
  public static final Parcelable.Creator<PresTupleInfo> CREATOR = new Parcelable.Creator<PresTupleInfo>() {
      public PresTupleInfo createFromParcel(Parcel param1Parcel) {
        return new PresTupleInfo(param1Parcel);
      }
      
      public PresTupleInfo[] newArray(int param1Int) {
        return new PresTupleInfo[param1Int];
      }
    };
  
  private PresTupleInfo(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mFeatureTag = paramParcel.readString();
    this.mContactUri = paramParcel.readString();
    this.mTimestamp = paramParcel.readString();
  }
  
  public PresTupleInfo() {}
}
