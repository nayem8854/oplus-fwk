package com.android.ims.internal.uce.presence;

import android.os.Parcel;
import android.os.Parcelable;

public class PresSipResponse implements Parcelable {
  private PresCmdId mCmdId = new PresCmdId();
  
  private int mRequestId = 0;
  
  private int mSipResponseCode = 0;
  
  private int mRetryAfter = 0;
  
  private String mReasonPhrase = "";
  
  public PresCmdId getCmdId() {
    return this.mCmdId;
  }
  
  public void setCmdId(PresCmdId paramPresCmdId) {
    this.mCmdId = paramPresCmdId;
  }
  
  public int getRequestId() {
    return this.mRequestId;
  }
  
  public void setRequestId(int paramInt) {
    this.mRequestId = paramInt;
  }
  
  public int getSipResponseCode() {
    return this.mSipResponseCode;
  }
  
  public void setSipResponseCode(int paramInt) {
    this.mSipResponseCode = paramInt;
  }
  
  public String getReasonPhrase() {
    return this.mReasonPhrase;
  }
  
  public void setReasonPhrase(String paramString) {
    this.mReasonPhrase = paramString;
  }
  
  public int getRetryAfter() {
    return this.mRetryAfter;
  }
  
  public void setRetryAfter(int paramInt) {
    this.mRetryAfter = paramInt;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mRequestId);
    paramParcel.writeInt(this.mSipResponseCode);
    paramParcel.writeString(this.mReasonPhrase);
    paramParcel.writeParcelable(this.mCmdId, paramInt);
    paramParcel.writeInt(this.mRetryAfter);
  }
  
  public static final Parcelable.Creator<PresSipResponse> CREATOR = new Parcelable.Creator<PresSipResponse>() {
      public PresSipResponse createFromParcel(Parcel param1Parcel) {
        return new PresSipResponse(param1Parcel);
      }
      
      public PresSipResponse[] newArray(int param1Int) {
        return new PresSipResponse[param1Int];
      }
    };
  
  private PresSipResponse(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mRequestId = paramParcel.readInt();
    this.mSipResponseCode = paramParcel.readInt();
    this.mReasonPhrase = paramParcel.readString();
    this.mCmdId = (PresCmdId)paramParcel.readParcelable(PresCmdId.class.getClassLoader());
    this.mRetryAfter = paramParcel.readInt();
  }
  
  public PresSipResponse() {}
}
