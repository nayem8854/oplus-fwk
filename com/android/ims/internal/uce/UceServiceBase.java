package com.android.ims.internal.uce;

import com.android.ims.internal.uce.common.UceLong;
import com.android.ims.internal.uce.options.IOptionsListener;
import com.android.ims.internal.uce.options.IOptionsService;
import com.android.ims.internal.uce.presence.IPresenceListener;
import com.android.ims.internal.uce.presence.IPresenceService;
import com.android.ims.internal.uce.uceservice.IUceListener;
import com.android.ims.internal.uce.uceservice.IUceService;

public abstract class UceServiceBase {
  private UceServiceBinder mBinder;
  
  class UceServiceBinder extends IUceService.Stub {
    final UceServiceBase this$0;
    
    private UceServiceBinder() {}
    
    public boolean startService(IUceListener param1IUceListener) {
      return UceServiceBase.this.onServiceStart(param1IUceListener);
    }
    
    public boolean stopService() {
      return UceServiceBase.this.onStopService();
    }
    
    public boolean isServiceStarted() {
      return UceServiceBase.this.onIsServiceStarted();
    }
    
    public int createOptionsService(IOptionsListener param1IOptionsListener, UceLong param1UceLong) {
      return UceServiceBase.this.onCreateOptionsService(param1IOptionsListener, param1UceLong);
    }
    
    public int createOptionsServiceForSubscription(IOptionsListener param1IOptionsListener, UceLong param1UceLong, String param1String) {
      return UceServiceBase.this.onCreateOptionsService(param1IOptionsListener, param1UceLong, param1String);
    }
    
    public void destroyOptionsService(int param1Int) {
      UceServiceBase.this.onDestroyOptionsService(param1Int);
    }
    
    public int createPresenceService(IPresenceListener param1IPresenceListener, UceLong param1UceLong) {
      return UceServiceBase.this.onCreatePresService(param1IPresenceListener, param1UceLong);
    }
    
    public int createPresenceServiceForSubscription(IPresenceListener param1IPresenceListener, UceLong param1UceLong, String param1String) {
      return UceServiceBase.this.onCreatePresService(param1IPresenceListener, param1UceLong, param1String);
    }
    
    public void destroyPresenceService(int param1Int) {
      UceServiceBase.this.onDestroyPresService(param1Int);
    }
    
    public boolean getServiceStatus() {
      return UceServiceBase.this.onGetServiceStatus();
    }
    
    public IPresenceService getPresenceService() {
      return UceServiceBase.this.onGetPresenceService();
    }
    
    public IPresenceService getPresenceServiceForSubscription(String param1String) {
      return UceServiceBase.this.onGetPresenceService(param1String);
    }
    
    public IOptionsService getOptionsService() {
      return UceServiceBase.this.onGetOptionsService();
    }
    
    public IOptionsService getOptionsServiceForSubscription(String param1String) {
      return UceServiceBase.this.onGetOptionsService(param1String);
    }
  }
  
  public UceServiceBinder getBinder() {
    if (this.mBinder == null)
      this.mBinder = new UceServiceBinder(); 
    return this.mBinder;
  }
  
  protected boolean onServiceStart(IUceListener paramIUceListener) {
    return false;
  }
  
  protected boolean onStopService() {
    return false;
  }
  
  protected boolean onIsServiceStarted() {
    return false;
  }
  
  protected int onCreateOptionsService(IOptionsListener paramIOptionsListener, UceLong paramUceLong) {
    return 0;
  }
  
  protected int onCreateOptionsService(IOptionsListener paramIOptionsListener, UceLong paramUceLong, String paramString) {
    return 0;
  }
  
  protected void onDestroyOptionsService(int paramInt) {}
  
  protected int onCreatePresService(IPresenceListener paramIPresenceListener, UceLong paramUceLong) {
    return 0;
  }
  
  protected int onCreatePresService(IPresenceListener paramIPresenceListener, UceLong paramUceLong, String paramString) {
    return 0;
  }
  
  protected void onDestroyPresService(int paramInt) {}
  
  protected boolean onGetServiceStatus() {
    return false;
  }
  
  protected IPresenceService onGetPresenceService() {
    return null;
  }
  
  protected IPresenceService onGetPresenceService(String paramString) {
    return null;
  }
  
  protected IOptionsService onGetOptionsService() {
    return null;
  }
  
  protected IOptionsService onGetOptionsService(String paramString) {
    return null;
  }
}
