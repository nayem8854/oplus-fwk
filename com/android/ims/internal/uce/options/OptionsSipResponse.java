package com.android.ims.internal.uce.options;

import android.os.Parcel;
import android.os.Parcelable;

public class OptionsSipResponse implements Parcelable {
  private int mRequestId = 0;
  
  private int mSipResponseCode = 0;
  
  private int mRetryAfter = 0;
  
  private String mReasonPhrase = "";
  
  public OptionsCmdId getCmdId() {
    return this.mCmdId;
  }
  
  public void setCmdId(OptionsCmdId paramOptionsCmdId) {
    this.mCmdId = paramOptionsCmdId;
  }
  
  public int getRequestId() {
    return this.mRequestId;
  }
  
  public void setRequestId(int paramInt) {
    this.mRequestId = paramInt;
  }
  
  public int getSipResponseCode() {
    return this.mSipResponseCode;
  }
  
  public void setSipResponseCode(int paramInt) {
    this.mSipResponseCode = paramInt;
  }
  
  public String getReasonPhrase() {
    return this.mReasonPhrase;
  }
  
  public void setReasonPhrase(String paramString) {
    this.mReasonPhrase = paramString;
  }
  
  public int getRetryAfter() {
    return this.mRetryAfter;
  }
  
  public void setRetryAfter(int paramInt) {
    this.mRetryAfter = paramInt;
  }
  
  public OptionsSipResponse() {
    this.mCmdId = new OptionsCmdId();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mRequestId);
    paramParcel.writeInt(this.mSipResponseCode);
    paramParcel.writeString(this.mReasonPhrase);
    paramParcel.writeParcelable(this.mCmdId, paramInt);
    paramParcel.writeInt(this.mRetryAfter);
  }
  
  public static final Parcelable.Creator<OptionsSipResponse> CREATOR = new Parcelable.Creator<OptionsSipResponse>() {
      public OptionsSipResponse createFromParcel(Parcel param1Parcel) {
        return new OptionsSipResponse(param1Parcel);
      }
      
      public OptionsSipResponse[] newArray(int param1Int) {
        return new OptionsSipResponse[param1Int];
      }
    };
  
  private OptionsCmdId mCmdId;
  
  private OptionsSipResponse(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mRequestId = paramParcel.readInt();
    this.mSipResponseCode = paramParcel.readInt();
    this.mReasonPhrase = paramParcel.readString();
    this.mCmdId = (OptionsCmdId)paramParcel.readParcelable(OptionsCmdId.class.getClassLoader());
    this.mRetryAfter = paramParcel.readInt();
  }
}
