package com.android.ims.internal.uce.options;

import android.os.Parcel;
import android.os.Parcelable;

public class OptionsCmdId implements Parcelable {
  private int mCmdId = 6;
  
  public static final int UCE_OPTIONS_CMD_UNKNOWN = 6;
  
  public static final int UCE_OPTIONS_CMD_SETMYCDINFO = 1;
  
  public static final int UCE_OPTIONS_CMD_RESPONSEINCOMINGOPTIONS = 4;
  
  public static final int UCE_OPTIONS_CMD_GET_VERSION = 5;
  
  public static final int UCE_OPTIONS_CMD_GETMYCDINFO = 0;
  
  public static final int UCE_OPTIONS_CMD_GETCONTACTLISTCAP = 3;
  
  public static final int UCE_OPTIONS_CMD_GETCONTACTCAP = 2;
  
  public int getCmdId() {
    return this.mCmdId;
  }
  
  public void setCmdId(int paramInt) {
    this.mCmdId = paramInt;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mCmdId);
  }
  
  public static final Parcelable.Creator<OptionsCmdId> CREATOR = new Parcelable.Creator<OptionsCmdId>() {
      public OptionsCmdId createFromParcel(Parcel param1Parcel) {
        return new OptionsCmdId(param1Parcel);
      }
      
      public OptionsCmdId[] newArray(int param1Int) {
        return new OptionsCmdId[param1Int];
      }
    };
  
  private OptionsCmdId(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mCmdId = paramParcel.readInt();
  }
  
  public OptionsCmdId() {}
}
