package com.android.ims.internal.uce.options;

import android.os.Parcel;
import android.os.Parcelable;
import com.android.ims.internal.uce.common.CapInfo;
import com.android.ims.internal.uce.common.StatusCode;

public class OptionsCmdStatus implements Parcelable {
  public OptionsCmdId getCmdId() {
    return this.mCmdId;
  }
  
  public void setCmdId(OptionsCmdId paramOptionsCmdId) {
    this.mCmdId = paramOptionsCmdId;
  }
  
  public int getUserData() {
    return this.mUserData;
  }
  
  public void setUserData(int paramInt) {
    this.mUserData = paramInt;
  }
  
  public StatusCode getStatus() {
    return this.mStatus;
  }
  
  public void setStatus(StatusCode paramStatusCode) {
    this.mStatus = paramStatusCode;
  }
  
  public OptionsCmdStatus() {
    this.mStatus = new StatusCode();
    this.mCapInfo = new CapInfo();
    this.mCmdId = new OptionsCmdId();
    this.mUserData = 0;
  }
  
  public CapInfo getCapInfo() {
    return this.mCapInfo;
  }
  
  public void setCapInfo(CapInfo paramCapInfo) {
    this.mCapInfo = paramCapInfo;
  }
  
  public static OptionsCmdStatus getOptionsCmdStatusInstance() {
    return new OptionsCmdStatus();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mUserData);
    paramParcel.writeParcelable(this.mCmdId, paramInt);
    paramParcel.writeParcelable(this.mStatus, paramInt);
    paramParcel.writeParcelable(this.mCapInfo, paramInt);
  }
  
  public static final Parcelable.Creator<OptionsCmdStatus> CREATOR = new Parcelable.Creator<OptionsCmdStatus>() {
      public OptionsCmdStatus createFromParcel(Parcel param1Parcel) {
        return new OptionsCmdStatus(param1Parcel);
      }
      
      public OptionsCmdStatus[] newArray(int param1Int) {
        return new OptionsCmdStatus[param1Int];
      }
    };
  
  private CapInfo mCapInfo;
  
  private OptionsCmdId mCmdId;
  
  private StatusCode mStatus;
  
  private int mUserData;
  
  private OptionsCmdStatus(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mUserData = paramParcel.readInt();
    this.mCmdId = (OptionsCmdId)paramParcel.readParcelable(OptionsCmdId.class.getClassLoader());
    this.mStatus = (StatusCode)paramParcel.readParcelable(StatusCode.class.getClassLoader());
    this.mCapInfo = (CapInfo)paramParcel.readParcelable(CapInfo.class.getClassLoader());
  }
}
