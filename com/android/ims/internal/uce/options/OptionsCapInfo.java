package com.android.ims.internal.uce.options;

import android.os.Parcel;
import android.os.Parcelable;
import com.android.ims.internal.uce.common.CapInfo;

public class OptionsCapInfo implements Parcelable {
  private String mSdp = "";
  
  private CapInfo mCapInfo;
  
  public static OptionsCapInfo getOptionsCapInfoInstance() {
    return new OptionsCapInfo();
  }
  
  public String getSdp() {
    return this.mSdp;
  }
  
  public void setSdp(String paramString) {
    this.mSdp = paramString;
  }
  
  public OptionsCapInfo() {
    this.mCapInfo = new CapInfo();
  }
  
  public CapInfo getCapInfo() {
    return this.mCapInfo;
  }
  
  public void setCapInfo(CapInfo paramCapInfo) {
    this.mCapInfo = paramCapInfo;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mSdp);
    paramParcel.writeParcelable(this.mCapInfo, paramInt);
  }
  
  public static final Parcelable.Creator<OptionsCapInfo> CREATOR = new Parcelable.Creator<OptionsCapInfo>() {
      public OptionsCapInfo createFromParcel(Parcel param1Parcel) {
        return new OptionsCapInfo(param1Parcel);
      }
      
      public OptionsCapInfo[] newArray(int param1Int) {
        return new OptionsCapInfo[param1Int];
      }
    };
  
  private OptionsCapInfo(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mSdp = paramParcel.readString();
    this.mCapInfo = (CapInfo)paramParcel.readParcelable(CapInfo.class.getClassLoader());
  }
}
