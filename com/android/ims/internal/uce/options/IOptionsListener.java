package com.android.ims.internal.uce.options;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.android.ims.internal.uce.common.StatusCode;

public interface IOptionsListener extends IInterface {
  void cmdStatus(OptionsCmdStatus paramOptionsCmdStatus) throws RemoteException;
  
  void getVersionCb(String paramString) throws RemoteException;
  
  void incomingOptions(String paramString, OptionsCapInfo paramOptionsCapInfo, int paramInt) throws RemoteException;
  
  void serviceAvailable(StatusCode paramStatusCode) throws RemoteException;
  
  void serviceUnavailable(StatusCode paramStatusCode) throws RemoteException;
  
  void sipResponseReceived(String paramString, OptionsSipResponse paramOptionsSipResponse, OptionsCapInfo paramOptionsCapInfo) throws RemoteException;
  
  class Default implements IOptionsListener {
    public void getVersionCb(String param1String) throws RemoteException {}
    
    public void serviceAvailable(StatusCode param1StatusCode) throws RemoteException {}
    
    public void serviceUnavailable(StatusCode param1StatusCode) throws RemoteException {}
    
    public void sipResponseReceived(String param1String, OptionsSipResponse param1OptionsSipResponse, OptionsCapInfo param1OptionsCapInfo) throws RemoteException {}
    
    public void cmdStatus(OptionsCmdStatus param1OptionsCmdStatus) throws RemoteException {}
    
    public void incomingOptions(String param1String, OptionsCapInfo param1OptionsCapInfo, int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOptionsListener {
    private static final String DESCRIPTOR = "com.android.ims.internal.uce.options.IOptionsListener";
    
    static final int TRANSACTION_cmdStatus = 5;
    
    static final int TRANSACTION_getVersionCb = 1;
    
    static final int TRANSACTION_incomingOptions = 6;
    
    static final int TRANSACTION_serviceAvailable = 2;
    
    static final int TRANSACTION_serviceUnavailable = 3;
    
    static final int TRANSACTION_sipResponseReceived = 4;
    
    public Stub() {
      attachInterface(this, "com.android.ims.internal.uce.options.IOptionsListener");
    }
    
    public static IOptionsListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.ims.internal.uce.options.IOptionsListener");
      if (iInterface != null && iInterface instanceof IOptionsListener)
        return (IOptionsListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 6:
          return "incomingOptions";
        case 5:
          return "cmdStatus";
        case 4:
          return "sipResponseReceived";
        case 3:
          return "serviceUnavailable";
        case 2:
          return "serviceAvailable";
        case 1:
          break;
      } 
      return "getVersionCb";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        String str2;
        OptionsCapInfo optionsCapInfo;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 6:
            param1Parcel1.enforceInterface("com.android.ims.internal.uce.options.IOptionsListener");
            str2 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              optionsCapInfo = (OptionsCapInfo)OptionsCapInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              optionsCapInfo = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            incomingOptions(str2, optionsCapInfo, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            param1Parcel1.enforceInterface("com.android.ims.internal.uce.options.IOptionsListener");
            if (param1Parcel1.readInt() != 0) {
              OptionsCmdStatus optionsCmdStatus = (OptionsCmdStatus)OptionsCmdStatus.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            cmdStatus((OptionsCmdStatus)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            param1Parcel1.enforceInterface("com.android.ims.internal.uce.options.IOptionsListener");
            str2 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              OptionsSipResponse optionsSipResponse = (OptionsSipResponse)OptionsSipResponse.CREATOR.createFromParcel(param1Parcel1);
            } else {
              optionsCapInfo = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              OptionsCapInfo optionsCapInfo1 = (OptionsCapInfo)OptionsCapInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            sipResponseReceived(str2, (OptionsSipResponse)optionsCapInfo, (OptionsCapInfo)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            param1Parcel1.enforceInterface("com.android.ims.internal.uce.options.IOptionsListener");
            if (param1Parcel1.readInt() != 0) {
              StatusCode statusCode = (StatusCode)StatusCode.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            serviceUnavailable((StatusCode)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            param1Parcel1.enforceInterface("com.android.ims.internal.uce.options.IOptionsListener");
            if (param1Parcel1.readInt() != 0) {
              StatusCode statusCode = (StatusCode)StatusCode.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            serviceAvailable((StatusCode)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        param1Parcel1.enforceInterface("com.android.ims.internal.uce.options.IOptionsListener");
        String str1 = param1Parcel1.readString();
        getVersionCb(str1);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("com.android.ims.internal.uce.options.IOptionsListener");
      return true;
    }
    
    private static class Proxy implements IOptionsListener {
      public static IOptionsListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.ims.internal.uce.options.IOptionsListener";
      }
      
      public void getVersionCb(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.options.IOptionsListener");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOptionsListener.Stub.getDefaultImpl() != null) {
            IOptionsListener.Stub.getDefaultImpl().getVersionCb(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void serviceAvailable(StatusCode param2StatusCode) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.options.IOptionsListener");
          if (param2StatusCode != null) {
            parcel1.writeInt(1);
            param2StatusCode.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOptionsListener.Stub.getDefaultImpl() != null) {
            IOptionsListener.Stub.getDefaultImpl().serviceAvailable(param2StatusCode);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void serviceUnavailable(StatusCode param2StatusCode) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.options.IOptionsListener");
          if (param2StatusCode != null) {
            parcel1.writeInt(1);
            param2StatusCode.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IOptionsListener.Stub.getDefaultImpl() != null) {
            IOptionsListener.Stub.getDefaultImpl().serviceUnavailable(param2StatusCode);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sipResponseReceived(String param2String, OptionsSipResponse param2OptionsSipResponse, OptionsCapInfo param2OptionsCapInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.options.IOptionsListener");
          parcel1.writeString(param2String);
          if (param2OptionsSipResponse != null) {
            parcel1.writeInt(1);
            param2OptionsSipResponse.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2OptionsCapInfo != null) {
            parcel1.writeInt(1);
            param2OptionsCapInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IOptionsListener.Stub.getDefaultImpl() != null) {
            IOptionsListener.Stub.getDefaultImpl().sipResponseReceived(param2String, param2OptionsSipResponse, param2OptionsCapInfo);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cmdStatus(OptionsCmdStatus param2OptionsCmdStatus) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.options.IOptionsListener");
          if (param2OptionsCmdStatus != null) {
            parcel1.writeInt(1);
            param2OptionsCmdStatus.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IOptionsListener.Stub.getDefaultImpl() != null) {
            IOptionsListener.Stub.getDefaultImpl().cmdStatus(param2OptionsCmdStatus);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void incomingOptions(String param2String, OptionsCapInfo param2OptionsCapInfo, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.options.IOptionsListener");
          parcel1.writeString(param2String);
          if (param2OptionsCapInfo != null) {
            parcel1.writeInt(1);
            param2OptionsCapInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IOptionsListener.Stub.getDefaultImpl() != null) {
            IOptionsListener.Stub.getDefaultImpl().incomingOptions(param2String, param2OptionsCapInfo, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOptionsListener param1IOptionsListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOptionsListener != null) {
          Proxy.sDefaultImpl = param1IOptionsListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOptionsListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
