package com.android.ims.internal.uce.options;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.android.ims.internal.uce.common.CapInfo;
import com.android.ims.internal.uce.common.StatusCode;
import com.android.ims.internal.uce.common.UceLong;

public interface IOptionsService extends IInterface {
  StatusCode addListener(int paramInt, IOptionsListener paramIOptionsListener, UceLong paramUceLong) throws RemoteException;
  
  StatusCode getContactCap(int paramInt1, String paramString, int paramInt2) throws RemoteException;
  
  StatusCode getContactListCap(int paramInt1, String[] paramArrayOfString, int paramInt2) throws RemoteException;
  
  StatusCode getMyInfo(int paramInt1, int paramInt2) throws RemoteException;
  
  StatusCode getVersion(int paramInt) throws RemoteException;
  
  StatusCode removeListener(int paramInt, UceLong paramUceLong) throws RemoteException;
  
  StatusCode responseIncomingOptions(int paramInt1, int paramInt2, int paramInt3, String paramString, OptionsCapInfo paramOptionsCapInfo, boolean paramBoolean) throws RemoteException;
  
  StatusCode setMyInfo(int paramInt1, CapInfo paramCapInfo, int paramInt2) throws RemoteException;
  
  class Default implements IOptionsService {
    public StatusCode getVersion(int param1Int) throws RemoteException {
      return null;
    }
    
    public StatusCode addListener(int param1Int, IOptionsListener param1IOptionsListener, UceLong param1UceLong) throws RemoteException {
      return null;
    }
    
    public StatusCode removeListener(int param1Int, UceLong param1UceLong) throws RemoteException {
      return null;
    }
    
    public StatusCode setMyInfo(int param1Int1, CapInfo param1CapInfo, int param1Int2) throws RemoteException {
      return null;
    }
    
    public StatusCode getMyInfo(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public StatusCode getContactCap(int param1Int1, String param1String, int param1Int2) throws RemoteException {
      return null;
    }
    
    public StatusCode getContactListCap(int param1Int1, String[] param1ArrayOfString, int param1Int2) throws RemoteException {
      return null;
    }
    
    public StatusCode responseIncomingOptions(int param1Int1, int param1Int2, int param1Int3, String param1String, OptionsCapInfo param1OptionsCapInfo, boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOptionsService {
    private static final String DESCRIPTOR = "com.android.ims.internal.uce.options.IOptionsService";
    
    static final int TRANSACTION_addListener = 2;
    
    static final int TRANSACTION_getContactCap = 6;
    
    static final int TRANSACTION_getContactListCap = 7;
    
    static final int TRANSACTION_getMyInfo = 5;
    
    static final int TRANSACTION_getVersion = 1;
    
    static final int TRANSACTION_removeListener = 3;
    
    static final int TRANSACTION_responseIncomingOptions = 8;
    
    static final int TRANSACTION_setMyInfo = 4;
    
    public Stub() {
      attachInterface(this, "com.android.ims.internal.uce.options.IOptionsService");
    }
    
    public static IOptionsService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.ims.internal.uce.options.IOptionsService");
      if (iInterface != null && iInterface instanceof IOptionsService)
        return (IOptionsService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 8:
          return "responseIncomingOptions";
        case 7:
          return "getContactListCap";
        case 6:
          return "getContactCap";
        case 5:
          return "getMyInfo";
        case 4:
          return "setMyInfo";
        case 3:
          return "removeListener";
        case 2:
          return "addListener";
        case 1:
          break;
      } 
      return "getVersion";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        int i;
        String str1;
        OptionsCapInfo optionsCapInfo;
        String arrayOfString[], str2;
        IOptionsListener iOptionsListener;
        StatusCode statusCode2;
        boolean bool;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 8:
            param1Parcel1.enforceInterface("com.android.ims.internal.uce.options.IOptionsService");
            i = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              optionsCapInfo = (OptionsCapInfo)OptionsCapInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              optionsCapInfo = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            statusCode1 = responseIncomingOptions(i, param1Int1, param1Int2, str1, optionsCapInfo, bool);
            param1Parcel2.writeNoException();
            if (statusCode1 != null) {
              param1Parcel2.writeInt(1);
              statusCode1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 7:
            statusCode1.enforceInterface("com.android.ims.internal.uce.options.IOptionsService");
            param1Int1 = statusCode1.readInt();
            arrayOfString = statusCode1.createStringArray();
            param1Int2 = statusCode1.readInt();
            statusCode1 = getContactListCap(param1Int1, arrayOfString, param1Int2);
            param1Parcel2.writeNoException();
            if (statusCode1 != null) {
              param1Parcel2.writeInt(1);
              statusCode1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 6:
            statusCode1.enforceInterface("com.android.ims.internal.uce.options.IOptionsService");
            param1Int2 = statusCode1.readInt();
            str2 = statusCode1.readString();
            param1Int1 = statusCode1.readInt();
            statusCode1 = getContactCap(param1Int2, str2, param1Int1);
            param1Parcel2.writeNoException();
            if (statusCode1 != null) {
              param1Parcel2.writeInt(1);
              statusCode1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 5:
            statusCode1.enforceInterface("com.android.ims.internal.uce.options.IOptionsService");
            param1Int1 = statusCode1.readInt();
            param1Int2 = statusCode1.readInt();
            statusCode1 = getMyInfo(param1Int1, param1Int2);
            param1Parcel2.writeNoException();
            if (statusCode1 != null) {
              param1Parcel2.writeInt(1);
              statusCode1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 4:
            statusCode1.enforceInterface("com.android.ims.internal.uce.options.IOptionsService");
            param1Int1 = statusCode1.readInt();
            if (statusCode1.readInt() != 0) {
              CapInfo capInfo = (CapInfo)CapInfo.CREATOR.createFromParcel((Parcel)statusCode1);
            } else {
              str2 = null;
            } 
            param1Int2 = statusCode1.readInt();
            statusCode1 = setMyInfo(param1Int1, (CapInfo)str2, param1Int2);
            param1Parcel2.writeNoException();
            if (statusCode1 != null) {
              param1Parcel2.writeInt(1);
              statusCode1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 3:
            statusCode1.enforceInterface("com.android.ims.internal.uce.options.IOptionsService");
            param1Int1 = statusCode1.readInt();
            if (statusCode1.readInt() != 0) {
              UceLong uceLong = (UceLong)UceLong.CREATOR.createFromParcel((Parcel)statusCode1);
            } else {
              statusCode1 = null;
            } 
            statusCode1 = removeListener(param1Int1, (UceLong)statusCode1);
            param1Parcel2.writeNoException();
            if (statusCode1 != null) {
              param1Parcel2.writeInt(1);
              statusCode1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 2:
            statusCode1.enforceInterface("com.android.ims.internal.uce.options.IOptionsService");
            param1Int1 = statusCode1.readInt();
            iOptionsListener = IOptionsListener.Stub.asInterface(statusCode1.readStrongBinder());
            if (statusCode1.readInt() != 0) {
              UceLong uceLong = (UceLong)UceLong.CREATOR.createFromParcel((Parcel)statusCode1);
            } else {
              statusCode1 = null;
            } 
            statusCode2 = addListener(param1Int1, iOptionsListener, (UceLong)statusCode1);
            param1Parcel2.writeNoException();
            if (statusCode2 != null) {
              param1Parcel2.writeInt(1);
              statusCode2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            if (statusCode1 != null) {
              param1Parcel2.writeInt(1);
              statusCode1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 1:
            break;
        } 
        statusCode1.enforceInterface("com.android.ims.internal.uce.options.IOptionsService");
        param1Int1 = statusCode1.readInt();
        StatusCode statusCode1 = getVersion(param1Int1);
        param1Parcel2.writeNoException();
        if (statusCode1 != null) {
          param1Parcel2.writeInt(1);
          statusCode1.writeToParcel(param1Parcel2, 1);
        } else {
          param1Parcel2.writeInt(0);
        } 
        return true;
      } 
      param1Parcel2.writeString("com.android.ims.internal.uce.options.IOptionsService");
      return true;
    }
    
    private static class Proxy implements IOptionsService {
      public static IOptionsService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.ims.internal.uce.options.IOptionsService";
      }
      
      public StatusCode getVersion(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          StatusCode statusCode;
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.options.IOptionsService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOptionsService.Stub.getDefaultImpl() != null) {
            statusCode = IOptionsService.Stub.getDefaultImpl().getVersion(param2Int);
            return statusCode;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            statusCode = (StatusCode)StatusCode.CREATOR.createFromParcel(parcel2);
          } else {
            statusCode = null;
          } 
          return statusCode;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public StatusCode addListener(int param2Int, IOptionsListener param2IOptionsListener, UceLong param2UceLong) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.options.IOptionsService");
          parcel1.writeInt(param2Int);
          if (param2IOptionsListener != null) {
            iBinder = param2IOptionsListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2UceLong != null) {
            parcel1.writeInt(1);
            param2UceLong.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOptionsService.Stub.getDefaultImpl() != null)
            return IOptionsService.Stub.getDefaultImpl().addListener(param2Int, param2IOptionsListener, param2UceLong); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            StatusCode statusCode = (StatusCode)StatusCode.CREATOR.createFromParcel(parcel2);
          } else {
            param2IOptionsListener = null;
          } 
          if (parcel2.readInt() != 0)
            param2UceLong.readFromParcel(parcel2); 
          return (StatusCode)param2IOptionsListener;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public StatusCode removeListener(int param2Int, UceLong param2UceLong) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.options.IOptionsService");
          parcel1.writeInt(param2Int);
          if (param2UceLong != null) {
            parcel1.writeInt(1);
            param2UceLong.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IOptionsService.Stub.getDefaultImpl() != null)
            return IOptionsService.Stub.getDefaultImpl().removeListener(param2Int, param2UceLong); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            StatusCode statusCode = (StatusCode)StatusCode.CREATOR.createFromParcel(parcel2);
          } else {
            param2UceLong = null;
          } 
          return (StatusCode)param2UceLong;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public StatusCode setMyInfo(int param2Int1, CapInfo param2CapInfo, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.options.IOptionsService");
          parcel1.writeInt(param2Int1);
          if (param2CapInfo != null) {
            parcel1.writeInt(1);
            param2CapInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IOptionsService.Stub.getDefaultImpl() != null)
            return IOptionsService.Stub.getDefaultImpl().setMyInfo(param2Int1, param2CapInfo, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            StatusCode statusCode = (StatusCode)StatusCode.CREATOR.createFromParcel(parcel2);
          } else {
            param2CapInfo = null;
          } 
          return (StatusCode)param2CapInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public StatusCode getMyInfo(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          StatusCode statusCode;
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.options.IOptionsService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IOptionsService.Stub.getDefaultImpl() != null) {
            statusCode = IOptionsService.Stub.getDefaultImpl().getMyInfo(param2Int1, param2Int2);
            return statusCode;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            statusCode = (StatusCode)StatusCode.CREATOR.createFromParcel(parcel2);
          } else {
            statusCode = null;
          } 
          return statusCode;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public StatusCode getContactCap(int param2Int1, String param2String, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.options.IOptionsService");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IOptionsService.Stub.getDefaultImpl() != null)
            return IOptionsService.Stub.getDefaultImpl().getContactCap(param2Int1, param2String, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            StatusCode statusCode = (StatusCode)StatusCode.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (StatusCode)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public StatusCode getContactListCap(int param2Int1, String[] param2ArrayOfString, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.options.IOptionsService");
          parcel1.writeInt(param2Int1);
          parcel1.writeStringArray(param2ArrayOfString);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IOptionsService.Stub.getDefaultImpl() != null)
            return IOptionsService.Stub.getDefaultImpl().getContactListCap(param2Int1, param2ArrayOfString, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            StatusCode statusCode = (StatusCode)StatusCode.CREATOR.createFromParcel(parcel2);
          } else {
            param2ArrayOfString = null;
          } 
          return (StatusCode)param2ArrayOfString;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public StatusCode responseIncomingOptions(int param2Int1, int param2Int2, int param2Int3, String param2String, OptionsCapInfo param2OptionsCapInfo, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.uce.options.IOptionsService");
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeInt(param2Int2);
              try {
                parcel1.writeInt(param2Int3);
                try {
                  parcel1.writeString(param2String);
                  boolean bool = true;
                  if (param2OptionsCapInfo != null) {
                    parcel1.writeInt(1);
                    param2OptionsCapInfo.writeToParcel(parcel1, 0);
                  } else {
                    parcel1.writeInt(0);
                  } 
                  if (!param2Boolean)
                    bool = false; 
                  parcel1.writeInt(bool);
                  try {
                    boolean bool1 = this.mRemote.transact(8, parcel1, parcel2, 0);
                    if (!bool1 && IOptionsService.Stub.getDefaultImpl() != null) {
                      StatusCode statusCode = IOptionsService.Stub.getDefaultImpl().responseIncomingOptions(param2Int1, param2Int2, param2Int3, param2String, param2OptionsCapInfo, param2Boolean);
                      parcel2.recycle();
                      parcel1.recycle();
                      return statusCode;
                    } 
                    parcel2.readException();
                    if (parcel2.readInt() != 0) {
                      StatusCode statusCode = (StatusCode)StatusCode.CREATOR.createFromParcel(parcel2);
                    } else {
                      param2String = null;
                    } 
                    parcel2.recycle();
                    parcel1.recycle();
                    return (StatusCode)param2String;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
    }
    
    public static boolean setDefaultImpl(IOptionsService param1IOptionsService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOptionsService != null) {
          Proxy.sDefaultImpl = param1IOptionsService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOptionsService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
