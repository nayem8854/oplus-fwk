package com.android.ims.internal;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IImsServiceFeatureCallback extends IInterface {
  void imsFeatureCreated(int paramInt1, int paramInt2) throws RemoteException;
  
  void imsFeatureRemoved(int paramInt1, int paramInt2) throws RemoteException;
  
  void imsStatusChanged(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  class Default implements IImsServiceFeatureCallback {
    public void imsFeatureCreated(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void imsFeatureRemoved(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void imsStatusChanged(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsServiceFeatureCallback {
    private static final String DESCRIPTOR = "com.android.ims.internal.IImsServiceFeatureCallback";
    
    static final int TRANSACTION_imsFeatureCreated = 1;
    
    static final int TRANSACTION_imsFeatureRemoved = 2;
    
    static final int TRANSACTION_imsStatusChanged = 3;
    
    public Stub() {
      attachInterface(this, "com.android.ims.internal.IImsServiceFeatureCallback");
    }
    
    public static IImsServiceFeatureCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.ims.internal.IImsServiceFeatureCallback");
      if (iInterface != null && iInterface instanceof IImsServiceFeatureCallback)
        return (IImsServiceFeatureCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "imsStatusChanged";
        } 
        return "imsFeatureRemoved";
      } 
      return "imsFeatureCreated";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("com.android.ims.internal.IImsServiceFeatureCallback");
            return true;
          } 
          param1Parcel1.enforceInterface("com.android.ims.internal.IImsServiceFeatureCallback");
          int i = param1Parcel1.readInt();
          param1Int2 = param1Parcel1.readInt();
          param1Int1 = param1Parcel1.readInt();
          imsStatusChanged(i, param1Int2, param1Int1);
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.ims.internal.IImsServiceFeatureCallback");
        param1Int2 = param1Parcel1.readInt();
        param1Int1 = param1Parcel1.readInt();
        imsFeatureRemoved(param1Int2, param1Int1);
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.ims.internal.IImsServiceFeatureCallback");
      param1Int2 = param1Parcel1.readInt();
      param1Int1 = param1Parcel1.readInt();
      imsFeatureCreated(param1Int2, param1Int1);
      return true;
    }
    
    private static class Proxy implements IImsServiceFeatureCallback {
      public static IImsServiceFeatureCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.ims.internal.IImsServiceFeatureCallback";
      }
      
      public void imsFeatureCreated(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsServiceFeatureCallback");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IImsServiceFeatureCallback.Stub.getDefaultImpl() != null) {
            IImsServiceFeatureCallback.Stub.getDefaultImpl().imsFeatureCreated(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void imsFeatureRemoved(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsServiceFeatureCallback");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IImsServiceFeatureCallback.Stub.getDefaultImpl() != null) {
            IImsServiceFeatureCallback.Stub.getDefaultImpl().imsFeatureRemoved(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void imsStatusChanged(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsServiceFeatureCallback");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IImsServiceFeatureCallback.Stub.getDefaultImpl() != null) {
            IImsServiceFeatureCallback.Stub.getDefaultImpl().imsStatusChanged(param2Int1, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsServiceFeatureCallback param1IImsServiceFeatureCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsServiceFeatureCallback != null) {
          Proxy.sDefaultImpl = param1IImsServiceFeatureCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsServiceFeatureCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
