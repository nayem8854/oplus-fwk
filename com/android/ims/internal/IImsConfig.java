package com.android.ims.internal;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.android.ims.ImsConfigListener;

public interface IImsConfig extends IInterface {
  void getFeatureValue(int paramInt1, int paramInt2, ImsConfigListener paramImsConfigListener) throws RemoteException;
  
  String getProvisionedStringValue(int paramInt) throws RemoteException;
  
  int getProvisionedValue(int paramInt) throws RemoteException;
  
  void getVideoQuality(ImsConfigListener paramImsConfigListener) throws RemoteException;
  
  boolean getVolteProvisioned() throws RemoteException;
  
  void setFeatureValue(int paramInt1, int paramInt2, int paramInt3, ImsConfigListener paramImsConfigListener) throws RemoteException;
  
  int setProvisionedStringValue(int paramInt, String paramString) throws RemoteException;
  
  int setProvisionedValue(int paramInt1, int paramInt2) throws RemoteException;
  
  void setVideoQuality(int paramInt, ImsConfigListener paramImsConfigListener) throws RemoteException;
  
  class Default implements IImsConfig {
    public int getProvisionedValue(int param1Int) throws RemoteException {
      return 0;
    }
    
    public String getProvisionedStringValue(int param1Int) throws RemoteException {
      return null;
    }
    
    public int setProvisionedValue(int param1Int1, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public int setProvisionedStringValue(int param1Int, String param1String) throws RemoteException {
      return 0;
    }
    
    public void getFeatureValue(int param1Int1, int param1Int2, ImsConfigListener param1ImsConfigListener) throws RemoteException {}
    
    public void setFeatureValue(int param1Int1, int param1Int2, int param1Int3, ImsConfigListener param1ImsConfigListener) throws RemoteException {}
    
    public boolean getVolteProvisioned() throws RemoteException {
      return false;
    }
    
    public void getVideoQuality(ImsConfigListener param1ImsConfigListener) throws RemoteException {}
    
    public void setVideoQuality(int param1Int, ImsConfigListener param1ImsConfigListener) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsConfig {
    private static final String DESCRIPTOR = "com.android.ims.internal.IImsConfig";
    
    static final int TRANSACTION_getFeatureValue = 5;
    
    static final int TRANSACTION_getProvisionedStringValue = 2;
    
    static final int TRANSACTION_getProvisionedValue = 1;
    
    static final int TRANSACTION_getVideoQuality = 8;
    
    static final int TRANSACTION_getVolteProvisioned = 7;
    
    static final int TRANSACTION_setFeatureValue = 6;
    
    static final int TRANSACTION_setProvisionedStringValue = 4;
    
    static final int TRANSACTION_setProvisionedValue = 3;
    
    static final int TRANSACTION_setVideoQuality = 9;
    
    public Stub() {
      attachInterface(this, "com.android.ims.internal.IImsConfig");
    }
    
    public static IImsConfig asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.ims.internal.IImsConfig");
      if (iInterface != null && iInterface instanceof IImsConfig)
        return (IImsConfig)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 9:
          return "setVideoQuality";
        case 8:
          return "getVideoQuality";
        case 7:
          return "getVolteProvisioned";
        case 6:
          return "setFeatureValue";
        case 5:
          return "getFeatureValue";
        case 4:
          return "setProvisionedStringValue";
        case 3:
          return "setProvisionedValue";
        case 2:
          return "getProvisionedStringValue";
        case 1:
          break;
      } 
      return "getProvisionedValue";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        ImsConfigListener imsConfigListener;
        String str;
        int j;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 9:
            param1Parcel1.enforceInterface("com.android.ims.internal.IImsConfig");
            param1Int1 = param1Parcel1.readInt();
            imsConfigListener = ImsConfigListener.Stub.asInterface(param1Parcel1.readStrongBinder());
            setVideoQuality(param1Int1, imsConfigListener);
            return true;
          case 8:
            imsConfigListener.enforceInterface("com.android.ims.internal.IImsConfig");
            imsConfigListener = ImsConfigListener.Stub.asInterface(imsConfigListener.readStrongBinder());
            getVideoQuality(imsConfigListener);
            return true;
          case 7:
            imsConfigListener.enforceInterface("com.android.ims.internal.IImsConfig");
            bool = getVolteProvisioned();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 6:
            imsConfigListener.enforceInterface("com.android.ims.internal.IImsConfig");
            param1Int2 = imsConfigListener.readInt();
            j = imsConfigListener.readInt();
            i = imsConfigListener.readInt();
            imsConfigListener = ImsConfigListener.Stub.asInterface(imsConfigListener.readStrongBinder());
            setFeatureValue(param1Int2, j, i, imsConfigListener);
            return true;
          case 5:
            imsConfigListener.enforceInterface("com.android.ims.internal.IImsConfig");
            i = imsConfigListener.readInt();
            param1Int2 = imsConfigListener.readInt();
            imsConfigListener = ImsConfigListener.Stub.asInterface(imsConfigListener.readStrongBinder());
            getFeatureValue(i, param1Int2, imsConfigListener);
            return true;
          case 4:
            imsConfigListener.enforceInterface("com.android.ims.internal.IImsConfig");
            i = imsConfigListener.readInt();
            str = imsConfigListener.readString();
            i = setProvisionedStringValue(i, str);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 3:
            str.enforceInterface("com.android.ims.internal.IImsConfig");
            param1Int2 = str.readInt();
            i = str.readInt();
            i = setProvisionedValue(param1Int2, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 2:
            str.enforceInterface("com.android.ims.internal.IImsConfig");
            i = str.readInt();
            str = getProvisionedStringValue(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str);
            return true;
          case 1:
            break;
        } 
        str.enforceInterface("com.android.ims.internal.IImsConfig");
        int i = str.readInt();
        i = getProvisionedValue(i);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(i);
        return true;
      } 
      param1Parcel2.writeString("com.android.ims.internal.IImsConfig");
      return true;
    }
    
    private static class Proxy implements IImsConfig {
      public static IImsConfig sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.ims.internal.IImsConfig";
      }
      
      public int getProvisionedValue(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsConfig");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IImsConfig.Stub.getDefaultImpl() != null) {
            param2Int = IImsConfig.Stub.getDefaultImpl().getProvisionedValue(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getProvisionedStringValue(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsConfig");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IImsConfig.Stub.getDefaultImpl() != null)
            return IImsConfig.Stub.getDefaultImpl().getProvisionedStringValue(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setProvisionedValue(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsConfig");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IImsConfig.Stub.getDefaultImpl() != null) {
            param2Int1 = IImsConfig.Stub.getDefaultImpl().setProvisionedValue(param2Int1, param2Int2);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setProvisionedStringValue(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsConfig");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IImsConfig.Stub.getDefaultImpl() != null) {
            param2Int = IImsConfig.Stub.getDefaultImpl().setProvisionedStringValue(param2Int, param2String);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void getFeatureValue(int param2Int1, int param2Int2, ImsConfigListener param2ImsConfigListener) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsConfig");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          if (param2ImsConfigListener != null) {
            iBinder = param2ImsConfigListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IImsConfig.Stub.getDefaultImpl() != null) {
            IImsConfig.Stub.getDefaultImpl().getFeatureValue(param2Int1, param2Int2, param2ImsConfigListener);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setFeatureValue(int param2Int1, int param2Int2, int param2Int3, ImsConfigListener param2ImsConfigListener) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsConfig");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          if (param2ImsConfigListener != null) {
            iBinder = param2ImsConfigListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IImsConfig.Stub.getDefaultImpl() != null) {
            IImsConfig.Stub.getDefaultImpl().setFeatureValue(param2Int1, param2Int2, param2Int3, param2ImsConfigListener);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public boolean getVolteProvisioned() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsConfig");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(7, parcel1, parcel2, 0);
          if (!bool2 && IImsConfig.Stub.getDefaultImpl() != null) {
            bool1 = IImsConfig.Stub.getDefaultImpl().getVolteProvisioned();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void getVideoQuality(ImsConfigListener param2ImsConfigListener) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsConfig");
          if (param2ImsConfigListener != null) {
            iBinder = param2ImsConfigListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IImsConfig.Stub.getDefaultImpl() != null) {
            IImsConfig.Stub.getDefaultImpl().getVideoQuality(param2ImsConfigListener);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setVideoQuality(int param2Int, ImsConfigListener param2ImsConfigListener) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsConfig");
          parcel.writeInt(param2Int);
          if (param2ImsConfigListener != null) {
            iBinder = param2ImsConfigListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IImsConfig.Stub.getDefaultImpl() != null) {
            IImsConfig.Stub.getDefaultImpl().setVideoQuality(param2Int, param2ImsConfigListener);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsConfig param1IImsConfig) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsConfig != null) {
          Proxy.sDefaultImpl = param1IImsConfig;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsConfig getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
