package com.android.ims.internal;

import android.app.PendingIntent;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Message;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.ims.ImsCallProfile;

public interface IImsMMTelFeature extends IInterface {
  void addRegistrationListener(IImsRegistrationListener paramIImsRegistrationListener) throws RemoteException;
  
  ImsCallProfile createCallProfile(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  IImsCallSession createCallSession(int paramInt, ImsCallProfile paramImsCallProfile) throws RemoteException;
  
  void endSession(int paramInt) throws RemoteException;
  
  IImsConfig getConfigInterface() throws RemoteException;
  
  IImsEcbm getEcbmInterface() throws RemoteException;
  
  int getFeatureStatus() throws RemoteException;
  
  IImsMultiEndpoint getMultiEndpointInterface() throws RemoteException;
  
  IImsCallSession getPendingCallSession(int paramInt, String paramString) throws RemoteException;
  
  IImsUt getUtInterface() throws RemoteException;
  
  boolean isConnected(int paramInt1, int paramInt2) throws RemoteException;
  
  boolean isOpened() throws RemoteException;
  
  void removeRegistrationListener(IImsRegistrationListener paramIImsRegistrationListener) throws RemoteException;
  
  void setUiTTYMode(int paramInt, Message paramMessage) throws RemoteException;
  
  int startSession(PendingIntent paramPendingIntent, IImsRegistrationListener paramIImsRegistrationListener) throws RemoteException;
  
  void turnOffIms() throws RemoteException;
  
  void turnOnIms() throws RemoteException;
  
  class Default implements IImsMMTelFeature {
    public int startSession(PendingIntent param1PendingIntent, IImsRegistrationListener param1IImsRegistrationListener) throws RemoteException {
      return 0;
    }
    
    public void endSession(int param1Int) throws RemoteException {}
    
    public boolean isConnected(int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public boolean isOpened() throws RemoteException {
      return false;
    }
    
    public int getFeatureStatus() throws RemoteException {
      return 0;
    }
    
    public void addRegistrationListener(IImsRegistrationListener param1IImsRegistrationListener) throws RemoteException {}
    
    public void removeRegistrationListener(IImsRegistrationListener param1IImsRegistrationListener) throws RemoteException {}
    
    public ImsCallProfile createCallProfile(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {
      return null;
    }
    
    public IImsCallSession createCallSession(int param1Int, ImsCallProfile param1ImsCallProfile) throws RemoteException {
      return null;
    }
    
    public IImsCallSession getPendingCallSession(int param1Int, String param1String) throws RemoteException {
      return null;
    }
    
    public IImsUt getUtInterface() throws RemoteException {
      return null;
    }
    
    public IImsConfig getConfigInterface() throws RemoteException {
      return null;
    }
    
    public void turnOnIms() throws RemoteException {}
    
    public void turnOffIms() throws RemoteException {}
    
    public IImsEcbm getEcbmInterface() throws RemoteException {
      return null;
    }
    
    public void setUiTTYMode(int param1Int, Message param1Message) throws RemoteException {}
    
    public IImsMultiEndpoint getMultiEndpointInterface() throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsMMTelFeature {
    private static final String DESCRIPTOR = "com.android.ims.internal.IImsMMTelFeature";
    
    static final int TRANSACTION_addRegistrationListener = 6;
    
    static final int TRANSACTION_createCallProfile = 8;
    
    static final int TRANSACTION_createCallSession = 9;
    
    static final int TRANSACTION_endSession = 2;
    
    static final int TRANSACTION_getConfigInterface = 12;
    
    static final int TRANSACTION_getEcbmInterface = 15;
    
    static final int TRANSACTION_getFeatureStatus = 5;
    
    static final int TRANSACTION_getMultiEndpointInterface = 17;
    
    static final int TRANSACTION_getPendingCallSession = 10;
    
    static final int TRANSACTION_getUtInterface = 11;
    
    static final int TRANSACTION_isConnected = 3;
    
    static final int TRANSACTION_isOpened = 4;
    
    static final int TRANSACTION_removeRegistrationListener = 7;
    
    static final int TRANSACTION_setUiTTYMode = 16;
    
    static final int TRANSACTION_startSession = 1;
    
    static final int TRANSACTION_turnOffIms = 14;
    
    static final int TRANSACTION_turnOnIms = 13;
    
    public Stub() {
      attachInterface(this, "com.android.ims.internal.IImsMMTelFeature");
    }
    
    public static IImsMMTelFeature asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.ims.internal.IImsMMTelFeature");
      if (iInterface != null && iInterface instanceof IImsMMTelFeature)
        return (IImsMMTelFeature)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 17:
          return "getMultiEndpointInterface";
        case 16:
          return "setUiTTYMode";
        case 15:
          return "getEcbmInterface";
        case 14:
          return "turnOffIms";
        case 13:
          return "turnOnIms";
        case 12:
          return "getConfigInterface";
        case 11:
          return "getUtInterface";
        case 10:
          return "getPendingCallSession";
        case 9:
          return "createCallSession";
        case 8:
          return "createCallProfile";
        case 7:
          return "removeRegistrationListener";
        case 6:
          return "addRegistrationListener";
        case 5:
          return "getFeatureStatus";
        case 4:
          return "isOpened";
        case 3:
          return "isConnected";
        case 2:
          return "endSession";
        case 1:
          break;
      } 
      return "startSession";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool2;
        int j;
        boolean bool1;
        IBinder iBinder3;
        String str1;
        IBinder iBinder2;
        IImsCallSession iImsCallSession1;
        IBinder iBinder1;
        ImsCallProfile imsCallProfile;
        IImsCallSession iImsCallSession2;
        IImsEcbm iImsEcbm;
        IImsConfig iImsConfig;
        IImsUt iImsUt;
        IImsCallSession iImsCallSession3;
        int k;
        IBinder iBinder4 = null, iBinder5 = null, iBinder6 = null;
        String str2 = null;
        IImsMultiEndpoint iImsMultiEndpoint = null;
        Parcel parcel = null;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 17:
            param1Parcel1.enforceInterface("com.android.ims.internal.IImsMMTelFeature");
            iImsMultiEndpoint = getMultiEndpointInterface();
            param1Parcel2.writeNoException();
            param1Parcel1 = parcel;
            if (iImsMultiEndpoint != null)
              iBinder3 = iImsMultiEndpoint.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder3);
            return true;
          case 16:
            iBinder3.enforceInterface("com.android.ims.internal.IImsMMTelFeature");
            param1Int1 = iBinder3.readInt();
            if (iBinder3.readInt() != 0) {
              Message message = (Message)Message.CREATOR.createFromParcel((Parcel)iBinder3);
            } else {
              iBinder3 = null;
            } 
            setUiTTYMode(param1Int1, (Message)iBinder3);
            param1Parcel2.writeNoException();
            return true;
          case 15:
            iBinder3.enforceInterface("com.android.ims.internal.IImsMMTelFeature");
            iImsEcbm = getEcbmInterface();
            param1Parcel2.writeNoException();
            iBinder3 = iBinder4;
            if (iImsEcbm != null)
              iBinder3 = iImsEcbm.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder3);
            return true;
          case 14:
            iBinder3.enforceInterface("com.android.ims.internal.IImsMMTelFeature");
            turnOffIms();
            param1Parcel2.writeNoException();
            return true;
          case 13:
            iBinder3.enforceInterface("com.android.ims.internal.IImsMMTelFeature");
            turnOnIms();
            param1Parcel2.writeNoException();
            return true;
          case 12:
            iBinder3.enforceInterface("com.android.ims.internal.IImsMMTelFeature");
            iImsConfig = getConfigInterface();
            param1Parcel2.writeNoException();
            iBinder3 = iBinder5;
            if (iImsConfig != null)
              iBinder3 = iImsConfig.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder3);
            return true;
          case 11:
            iBinder3.enforceInterface("com.android.ims.internal.IImsMMTelFeature");
            iImsUt = getUtInterface();
            param1Parcel2.writeNoException();
            iBinder3 = iBinder6;
            if (iImsUt != null)
              iBinder3 = iImsUt.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder3);
            return true;
          case 10:
            iBinder3.enforceInterface("com.android.ims.internal.IImsMMTelFeature");
            param1Int1 = iBinder3.readInt();
            str1 = iBinder3.readString();
            iImsCallSession3 = getPendingCallSession(param1Int1, str1);
            param1Parcel2.writeNoException();
            str1 = str2;
            if (iImsCallSession3 != null)
              iBinder2 = iImsCallSession3.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder2);
            return true;
          case 9:
            iBinder2.enforceInterface("com.android.ims.internal.IImsMMTelFeature");
            param1Int1 = iBinder2.readInt();
            if (iBinder2.readInt() != 0) {
              ImsCallProfile imsCallProfile1 = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)iBinder2);
            } else {
              iBinder2 = null;
            } 
            iImsCallSession2 = createCallSession(param1Int1, (ImsCallProfile)iBinder2);
            param1Parcel2.writeNoException();
            iImsCallSession1 = iImsCallSession3;
            if (iImsCallSession2 != null)
              iBinder1 = iImsCallSession2.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder1);
            return true;
          case 8:
            iBinder1.enforceInterface("com.android.ims.internal.IImsMMTelFeature");
            param1Int2 = iBinder1.readInt();
            param1Int1 = iBinder1.readInt();
            k = iBinder1.readInt();
            imsCallProfile = createCallProfile(param1Int2, param1Int1, k);
            param1Parcel2.writeNoException();
            if (imsCallProfile != null) {
              param1Parcel2.writeInt(1);
              imsCallProfile.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 7:
            imsCallProfile.enforceInterface("com.android.ims.internal.IImsMMTelFeature");
            iImsRegistrationListener = IImsRegistrationListener.Stub.asInterface(imsCallProfile.readStrongBinder());
            removeRegistrationListener(iImsRegistrationListener);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            iImsRegistrationListener.enforceInterface("com.android.ims.internal.IImsMMTelFeature");
            iImsRegistrationListener = IImsRegistrationListener.Stub.asInterface(iImsRegistrationListener.readStrongBinder());
            addRegistrationListener(iImsRegistrationListener);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            iImsRegistrationListener.enforceInterface("com.android.ims.internal.IImsMMTelFeature");
            param1Int1 = getFeatureStatus();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 4:
            iImsRegistrationListener.enforceInterface("com.android.ims.internal.IImsMMTelFeature");
            bool2 = isOpened();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 3:
            iImsRegistrationListener.enforceInterface("com.android.ims.internal.IImsMMTelFeature");
            j = iImsRegistrationListener.readInt();
            param1Int2 = iImsRegistrationListener.readInt();
            bool1 = isConnected(j, param1Int2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 2:
            iImsRegistrationListener.enforceInterface("com.android.ims.internal.IImsMMTelFeature");
            i = iImsRegistrationListener.readInt();
            endSession(i);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        iImsRegistrationListener.enforceInterface("com.android.ims.internal.IImsMMTelFeature");
        if (iImsRegistrationListener.readInt() != 0) {
          PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)iImsRegistrationListener);
        } else {
          iImsCallSession3 = null;
        } 
        IImsRegistrationListener iImsRegistrationListener = IImsRegistrationListener.Stub.asInterface(iImsRegistrationListener.readStrongBinder());
        int i = startSession((PendingIntent)iImsCallSession3, iImsRegistrationListener);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(i);
        return true;
      } 
      param1Parcel2.writeString("com.android.ims.internal.IImsMMTelFeature");
      return true;
    }
    
    private static class Proxy implements IImsMMTelFeature {
      public static IImsMMTelFeature sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.ims.internal.IImsMMTelFeature";
      }
      
      public int startSession(PendingIntent param2PendingIntent, IImsRegistrationListener param2IImsRegistrationListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsMMTelFeature");
          if (param2PendingIntent != null) {
            parcel1.writeInt(1);
            param2PendingIntent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2IImsRegistrationListener != null) {
            iBinder = param2IImsRegistrationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IImsMMTelFeature.Stub.getDefaultImpl() != null)
            return IImsMMTelFeature.Stub.getDefaultImpl().startSession(param2PendingIntent, param2IImsRegistrationListener); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void endSession(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsMMTelFeature");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IImsMMTelFeature.Stub.getDefaultImpl() != null) {
            IImsMMTelFeature.Stub.getDefaultImpl().endSession(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isConnected(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsMMTelFeature");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IImsMMTelFeature.Stub.getDefaultImpl() != null) {
            bool1 = IImsMMTelFeature.Stub.getDefaultImpl().isConnected(param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isOpened() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsMMTelFeature");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IImsMMTelFeature.Stub.getDefaultImpl() != null) {
            bool1 = IImsMMTelFeature.Stub.getDefaultImpl().isOpened();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getFeatureStatus() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsMMTelFeature");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IImsMMTelFeature.Stub.getDefaultImpl() != null)
            return IImsMMTelFeature.Stub.getDefaultImpl().getFeatureStatus(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addRegistrationListener(IImsRegistrationListener param2IImsRegistrationListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsMMTelFeature");
          if (param2IImsRegistrationListener != null) {
            iBinder = param2IImsRegistrationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IImsMMTelFeature.Stub.getDefaultImpl() != null) {
            IImsMMTelFeature.Stub.getDefaultImpl().addRegistrationListener(param2IImsRegistrationListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeRegistrationListener(IImsRegistrationListener param2IImsRegistrationListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsMMTelFeature");
          if (param2IImsRegistrationListener != null) {
            iBinder = param2IImsRegistrationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IImsMMTelFeature.Stub.getDefaultImpl() != null) {
            IImsMMTelFeature.Stub.getDefaultImpl().removeRegistrationListener(param2IImsRegistrationListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ImsCallProfile createCallProfile(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ImsCallProfile imsCallProfile;
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsMMTelFeature");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IImsMMTelFeature.Stub.getDefaultImpl() != null) {
            imsCallProfile = IImsMMTelFeature.Stub.getDefaultImpl().createCallProfile(param2Int1, param2Int2, param2Int3);
            return imsCallProfile;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel(parcel2);
          } else {
            imsCallProfile = null;
          } 
          return imsCallProfile;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IImsCallSession createCallSession(int param2Int, ImsCallProfile param2ImsCallProfile) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsMMTelFeature");
          parcel1.writeInt(param2Int);
          if (param2ImsCallProfile != null) {
            parcel1.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IImsMMTelFeature.Stub.getDefaultImpl() != null)
            return IImsMMTelFeature.Stub.getDefaultImpl().createCallSession(param2Int, param2ImsCallProfile); 
          parcel2.readException();
          return IImsCallSession.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IImsCallSession getPendingCallSession(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsMMTelFeature");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IImsMMTelFeature.Stub.getDefaultImpl() != null)
            return IImsMMTelFeature.Stub.getDefaultImpl().getPendingCallSession(param2Int, param2String); 
          parcel2.readException();
          return IImsCallSession.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IImsUt getUtInterface() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsMMTelFeature");
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IImsMMTelFeature.Stub.getDefaultImpl() != null)
            return IImsMMTelFeature.Stub.getDefaultImpl().getUtInterface(); 
          parcel2.readException();
          return IImsUt.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IImsConfig getConfigInterface() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsMMTelFeature");
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IImsMMTelFeature.Stub.getDefaultImpl() != null)
            return IImsMMTelFeature.Stub.getDefaultImpl().getConfigInterface(); 
          parcel2.readException();
          return IImsConfig.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void turnOnIms() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsMMTelFeature");
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IImsMMTelFeature.Stub.getDefaultImpl() != null) {
            IImsMMTelFeature.Stub.getDefaultImpl().turnOnIms();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void turnOffIms() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsMMTelFeature");
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IImsMMTelFeature.Stub.getDefaultImpl() != null) {
            IImsMMTelFeature.Stub.getDefaultImpl().turnOffIms();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IImsEcbm getEcbmInterface() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsMMTelFeature");
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IImsMMTelFeature.Stub.getDefaultImpl() != null)
            return IImsMMTelFeature.Stub.getDefaultImpl().getEcbmInterface(); 
          parcel2.readException();
          return IImsEcbm.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUiTTYMode(int param2Int, Message param2Message) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsMMTelFeature");
          parcel1.writeInt(param2Int);
          if (param2Message != null) {
            parcel1.writeInt(1);
            param2Message.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IImsMMTelFeature.Stub.getDefaultImpl() != null) {
            IImsMMTelFeature.Stub.getDefaultImpl().setUiTTYMode(param2Int, param2Message);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IImsMultiEndpoint getMultiEndpointInterface() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsMMTelFeature");
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IImsMMTelFeature.Stub.getDefaultImpl() != null)
            return IImsMMTelFeature.Stub.getDefaultImpl().getMultiEndpointInterface(); 
          parcel2.readException();
          return IImsMultiEndpoint.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsMMTelFeature param1IImsMMTelFeature) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsMMTelFeature != null) {
          Proxy.sDefaultImpl = param1IImsMMTelFeature;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsMMTelFeature getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
