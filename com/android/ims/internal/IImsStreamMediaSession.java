package com.android.ims.internal;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IImsStreamMediaSession extends IInterface {
  void close() throws RemoteException;
  
  class Default implements IImsStreamMediaSession {
    public void close() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsStreamMediaSession {
    private static final String DESCRIPTOR = "com.android.ims.internal.IImsStreamMediaSession";
    
    static final int TRANSACTION_close = 1;
    
    public Stub() {
      attachInterface(this, "com.android.ims.internal.IImsStreamMediaSession");
    }
    
    public static IImsStreamMediaSession asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.ims.internal.IImsStreamMediaSession");
      if (iInterface != null && iInterface instanceof IImsStreamMediaSession)
        return (IImsStreamMediaSession)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "close";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.ims.internal.IImsStreamMediaSession");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.ims.internal.IImsStreamMediaSession");
      close();
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IImsStreamMediaSession {
      public static IImsStreamMediaSession sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.ims.internal.IImsStreamMediaSession";
      }
      
      public void close() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsStreamMediaSession");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IImsStreamMediaSession.Stub.getDefaultImpl() != null) {
            IImsStreamMediaSession.Stub.getDefaultImpl().close();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsStreamMediaSession param1IImsStreamMediaSession) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsStreamMediaSession != null) {
          Proxy.sDefaultImpl = param1IImsStreamMediaSession;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsStreamMediaSession getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
