package com.android.ims.internal;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.ims.ImsExternalCallState;
import java.util.ArrayList;
import java.util.List;

public interface IImsExternalCallStateListener extends IInterface {
  void onImsExternalCallStateUpdate(List<ImsExternalCallState> paramList) throws RemoteException;
  
  class Default implements IImsExternalCallStateListener {
    public void onImsExternalCallStateUpdate(List<ImsExternalCallState> param1List) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsExternalCallStateListener {
    private static final String DESCRIPTOR = "com.android.ims.internal.IImsExternalCallStateListener";
    
    static final int TRANSACTION_onImsExternalCallStateUpdate = 1;
    
    public Stub() {
      attachInterface(this, "com.android.ims.internal.IImsExternalCallStateListener");
    }
    
    public static IImsExternalCallStateListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.ims.internal.IImsExternalCallStateListener");
      if (iInterface != null && iInterface instanceof IImsExternalCallStateListener)
        return (IImsExternalCallStateListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onImsExternalCallStateUpdate";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.ims.internal.IImsExternalCallStateListener");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.ims.internal.IImsExternalCallStateListener");
      ArrayList<ImsExternalCallState> arrayList = param1Parcel1.createTypedArrayList(ImsExternalCallState.CREATOR);
      onImsExternalCallStateUpdate(arrayList);
      return true;
    }
    
    private static class Proxy implements IImsExternalCallStateListener {
      public static IImsExternalCallStateListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.ims.internal.IImsExternalCallStateListener";
      }
      
      public void onImsExternalCallStateUpdate(List<ImsExternalCallState> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsExternalCallStateListener");
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IImsExternalCallStateListener.Stub.getDefaultImpl() != null) {
            IImsExternalCallStateListener.Stub.getDefaultImpl().onImsExternalCallStateUpdate(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsExternalCallStateListener param1IImsExternalCallStateListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsExternalCallStateListener != null) {
          Proxy.sDefaultImpl = param1IImsExternalCallStateListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsExternalCallStateListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
