package com.android.ims.internal;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IImsMultiEndpoint extends IInterface {
  void requestImsExternalCallStateInfo() throws RemoteException;
  
  void setListener(IImsExternalCallStateListener paramIImsExternalCallStateListener) throws RemoteException;
  
  class Default implements IImsMultiEndpoint {
    public void setListener(IImsExternalCallStateListener param1IImsExternalCallStateListener) throws RemoteException {}
    
    public void requestImsExternalCallStateInfo() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsMultiEndpoint {
    private static final String DESCRIPTOR = "com.android.ims.internal.IImsMultiEndpoint";
    
    static final int TRANSACTION_requestImsExternalCallStateInfo = 2;
    
    static final int TRANSACTION_setListener = 1;
    
    public Stub() {
      attachInterface(this, "com.android.ims.internal.IImsMultiEndpoint");
    }
    
    public static IImsMultiEndpoint asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.ims.internal.IImsMultiEndpoint");
      if (iInterface != null && iInterface instanceof IImsMultiEndpoint)
        return (IImsMultiEndpoint)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "requestImsExternalCallStateInfo";
      } 
      return "setListener";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("com.android.ims.internal.IImsMultiEndpoint");
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.ims.internal.IImsMultiEndpoint");
        requestImsExternalCallStateInfo();
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.ims.internal.IImsMultiEndpoint");
      IImsExternalCallStateListener iImsExternalCallStateListener = IImsExternalCallStateListener.Stub.asInterface(param1Parcel1.readStrongBinder());
      setListener(iImsExternalCallStateListener);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IImsMultiEndpoint {
      public static IImsMultiEndpoint sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.ims.internal.IImsMultiEndpoint";
      }
      
      public void setListener(IImsExternalCallStateListener param2IImsExternalCallStateListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsMultiEndpoint");
          if (param2IImsExternalCallStateListener != null) {
            iBinder = param2IImsExternalCallStateListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IImsMultiEndpoint.Stub.getDefaultImpl() != null) {
            IImsMultiEndpoint.Stub.getDefaultImpl().setListener(param2IImsExternalCallStateListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestImsExternalCallStateInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsMultiEndpoint");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IImsMultiEndpoint.Stub.getDefaultImpl() != null) {
            IImsMultiEndpoint.Stub.getDefaultImpl().requestImsExternalCallStateInfo();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsMultiEndpoint param1IImsMultiEndpoint) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsMultiEndpoint != null) {
          Proxy.sDefaultImpl = param1IImsMultiEndpoint;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsMultiEndpoint getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
