package com.android.ims.internal;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IImsServiceController extends IInterface {
  IImsMMTelFeature createEmergencyMMTelFeature(int paramInt, IImsFeatureStatusCallback paramIImsFeatureStatusCallback) throws RemoteException;
  
  IImsMMTelFeature createMMTelFeature(int paramInt, IImsFeatureStatusCallback paramIImsFeatureStatusCallback) throws RemoteException;
  
  IImsRcsFeature createRcsFeature(int paramInt, IImsFeatureStatusCallback paramIImsFeatureStatusCallback) throws RemoteException;
  
  void removeImsFeature(int paramInt1, int paramInt2, IImsFeatureStatusCallback paramIImsFeatureStatusCallback) throws RemoteException;
  
  class Default implements IImsServiceController {
    public IImsMMTelFeature createEmergencyMMTelFeature(int param1Int, IImsFeatureStatusCallback param1IImsFeatureStatusCallback) throws RemoteException {
      return null;
    }
    
    public IImsMMTelFeature createMMTelFeature(int param1Int, IImsFeatureStatusCallback param1IImsFeatureStatusCallback) throws RemoteException {
      return null;
    }
    
    public IImsRcsFeature createRcsFeature(int param1Int, IImsFeatureStatusCallback param1IImsFeatureStatusCallback) throws RemoteException {
      return null;
    }
    
    public void removeImsFeature(int param1Int1, int param1Int2, IImsFeatureStatusCallback param1IImsFeatureStatusCallback) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsServiceController {
    private static final String DESCRIPTOR = "com.android.ims.internal.IImsServiceController";
    
    static final int TRANSACTION_createEmergencyMMTelFeature = 1;
    
    static final int TRANSACTION_createMMTelFeature = 2;
    
    static final int TRANSACTION_createRcsFeature = 3;
    
    static final int TRANSACTION_removeImsFeature = 4;
    
    public Stub() {
      attachInterface(this, "com.android.ims.internal.IImsServiceController");
    }
    
    public static IImsServiceController asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.ims.internal.IImsServiceController");
      if (iInterface != null && iInterface instanceof IImsServiceController)
        return (IImsServiceController)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "removeImsFeature";
          } 
          return "createRcsFeature";
        } 
        return "createMMTelFeature";
      } 
      return "createEmergencyMMTelFeature";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IBinder iBinder2, iBinder1;
      IImsFeatureStatusCallback iImsFeatureStatusCallback2 = null;
      IImsRcsFeature iImsRcsFeature2 = null;
      IImsFeatureStatusCallback iImsFeatureStatusCallback3 = null;
      if (param1Int1 != 1) {
        IBinder iBinder;
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("com.android.ims.internal.IImsServiceController");
              return true;
            } 
            param1Parcel1.enforceInterface("com.android.ims.internal.IImsServiceController");
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            iImsFeatureStatusCallback4 = IImsFeatureStatusCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            removeImsFeature(param1Int2, param1Int1, iImsFeatureStatusCallback4);
            param1Parcel2.writeNoException();
            return true;
          } 
          iImsFeatureStatusCallback4.enforceInterface("com.android.ims.internal.IImsServiceController");
          param1Int1 = iImsFeatureStatusCallback4.readInt();
          IImsFeatureStatusCallback iImsFeatureStatusCallback4 = IImsFeatureStatusCallback.Stub.asInterface(iImsFeatureStatusCallback4.readStrongBinder());
          iImsRcsFeature2 = createRcsFeature(param1Int1, iImsFeatureStatusCallback4);
          param1Parcel2.writeNoException();
          iImsFeatureStatusCallback4 = iImsFeatureStatusCallback3;
          if (iImsRcsFeature2 != null)
            iBinder = iImsRcsFeature2.asBinder(); 
          param1Parcel2.writeStrongBinder(iBinder);
          return true;
        } 
        iBinder.enforceInterface("com.android.ims.internal.IImsServiceController");
        param1Int1 = iBinder.readInt();
        IImsFeatureStatusCallback iImsFeatureStatusCallback = IImsFeatureStatusCallback.Stub.asInterface(iBinder.readStrongBinder());
        IImsMMTelFeature iImsMMTelFeature1 = createMMTelFeature(param1Int1, iImsFeatureStatusCallback);
        param1Parcel2.writeNoException();
        iImsFeatureStatusCallback = iImsFeatureStatusCallback2;
        if (iImsMMTelFeature1 != null)
          iBinder2 = iImsMMTelFeature1.asBinder(); 
        param1Parcel2.writeStrongBinder(iBinder2);
        return true;
      } 
      iBinder2.enforceInterface("com.android.ims.internal.IImsServiceController");
      param1Int1 = iBinder2.readInt();
      IImsFeatureStatusCallback iImsFeatureStatusCallback1 = IImsFeatureStatusCallback.Stub.asInterface(iBinder2.readStrongBinder());
      IImsMMTelFeature iImsMMTelFeature = createEmergencyMMTelFeature(param1Int1, iImsFeatureStatusCallback1);
      param1Parcel2.writeNoException();
      IImsRcsFeature iImsRcsFeature1 = iImsRcsFeature2;
      if (iImsMMTelFeature != null)
        iBinder1 = iImsMMTelFeature.asBinder(); 
      param1Parcel2.writeStrongBinder(iBinder1);
      return true;
    }
    
    private static class Proxy implements IImsServiceController {
      public static IImsServiceController sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.ims.internal.IImsServiceController";
      }
      
      public IImsMMTelFeature createEmergencyMMTelFeature(int param2Int, IImsFeatureStatusCallback param2IImsFeatureStatusCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsServiceController");
          parcel1.writeInt(param2Int);
          if (param2IImsFeatureStatusCallback != null) {
            iBinder = param2IImsFeatureStatusCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IImsServiceController.Stub.getDefaultImpl() != null)
            return IImsServiceController.Stub.getDefaultImpl().createEmergencyMMTelFeature(param2Int, param2IImsFeatureStatusCallback); 
          parcel2.readException();
          return IImsMMTelFeature.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IImsMMTelFeature createMMTelFeature(int param2Int, IImsFeatureStatusCallback param2IImsFeatureStatusCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsServiceController");
          parcel1.writeInt(param2Int);
          if (param2IImsFeatureStatusCallback != null) {
            iBinder = param2IImsFeatureStatusCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IImsServiceController.Stub.getDefaultImpl() != null)
            return IImsServiceController.Stub.getDefaultImpl().createMMTelFeature(param2Int, param2IImsFeatureStatusCallback); 
          parcel2.readException();
          return IImsMMTelFeature.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IImsRcsFeature createRcsFeature(int param2Int, IImsFeatureStatusCallback param2IImsFeatureStatusCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsServiceController");
          parcel1.writeInt(param2Int);
          if (param2IImsFeatureStatusCallback != null) {
            iBinder = param2IImsFeatureStatusCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IImsServiceController.Stub.getDefaultImpl() != null)
            return IImsServiceController.Stub.getDefaultImpl().createRcsFeature(param2Int, param2IImsFeatureStatusCallback); 
          parcel2.readException();
          return IImsRcsFeature.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeImsFeature(int param2Int1, int param2Int2, IImsFeatureStatusCallback param2IImsFeatureStatusCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsServiceController");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2IImsFeatureStatusCallback != null) {
            iBinder = param2IImsFeatureStatusCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IImsServiceController.Stub.getDefaultImpl() != null) {
            IImsServiceController.Stub.getDefaultImpl().removeImsFeature(param2Int1, param2Int2, param2IImsFeatureStatusCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsServiceController param1IImsServiceController) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsServiceController != null) {
          Proxy.sDefaultImpl = param1IImsServiceController;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsServiceController getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
