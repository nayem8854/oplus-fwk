package com.android.ims.internal;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.CallQuality;
import android.telephony.ims.ImsCallProfile;
import android.telephony.ims.ImsConferenceState;
import android.telephony.ims.ImsReasonInfo;
import android.telephony.ims.ImsStreamMediaProfile;
import android.telephony.ims.ImsSuppServiceNotification;

public interface IImsCallSessionListener extends IInterface {
  void callQualityChanged(CallQuality paramCallQuality) throws RemoteException;
  
  void callSessionConferenceExtendFailed(IImsCallSession paramIImsCallSession, ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void callSessionConferenceExtendReceived(IImsCallSession paramIImsCallSession1, IImsCallSession paramIImsCallSession2, ImsCallProfile paramImsCallProfile) throws RemoteException;
  
  void callSessionConferenceExtended(IImsCallSession paramIImsCallSession1, IImsCallSession paramIImsCallSession2, ImsCallProfile paramImsCallProfile) throws RemoteException;
  
  void callSessionConferenceStateUpdated(IImsCallSession paramIImsCallSession, ImsConferenceState paramImsConferenceState) throws RemoteException;
  
  void callSessionHandover(IImsCallSession paramIImsCallSession, int paramInt1, int paramInt2, ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void callSessionHandoverFailed(IImsCallSession paramIImsCallSession, int paramInt1, int paramInt2, ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void callSessionHeld(IImsCallSession paramIImsCallSession, ImsCallProfile paramImsCallProfile) throws RemoteException;
  
  void callSessionHoldFailed(IImsCallSession paramIImsCallSession, ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void callSessionHoldReceived(IImsCallSession paramIImsCallSession, ImsCallProfile paramImsCallProfile) throws RemoteException;
  
  void callSessionInviteParticipantsRequestDelivered(IImsCallSession paramIImsCallSession) throws RemoteException;
  
  void callSessionInviteParticipantsRequestFailed(IImsCallSession paramIImsCallSession, ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void callSessionMayHandover(IImsCallSession paramIImsCallSession, int paramInt1, int paramInt2) throws RemoteException;
  
  void callSessionMergeComplete(IImsCallSession paramIImsCallSession) throws RemoteException;
  
  void callSessionMergeFailed(IImsCallSession paramIImsCallSession, ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void callSessionMergeStarted(IImsCallSession paramIImsCallSession1, IImsCallSession paramIImsCallSession2, ImsCallProfile paramImsCallProfile) throws RemoteException;
  
  void callSessionMultipartyStateChanged(IImsCallSession paramIImsCallSession, boolean paramBoolean) throws RemoteException;
  
  void callSessionProgressing(IImsCallSession paramIImsCallSession, ImsStreamMediaProfile paramImsStreamMediaProfile) throws RemoteException;
  
  void callSessionRemoveParticipantsRequestDelivered(IImsCallSession paramIImsCallSession) throws RemoteException;
  
  void callSessionRemoveParticipantsRequestFailed(IImsCallSession paramIImsCallSession, ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void callSessionResumeFailed(IImsCallSession paramIImsCallSession, ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void callSessionResumeReceived(IImsCallSession paramIImsCallSession, ImsCallProfile paramImsCallProfile) throws RemoteException;
  
  void callSessionResumed(IImsCallSession paramIImsCallSession, ImsCallProfile paramImsCallProfile) throws RemoteException;
  
  void callSessionRttAudioIndicatorChanged(ImsStreamMediaProfile paramImsStreamMediaProfile) throws RemoteException;
  
  void callSessionRttMessageReceived(String paramString) throws RemoteException;
  
  void callSessionRttModifyRequestReceived(IImsCallSession paramIImsCallSession, ImsCallProfile paramImsCallProfile) throws RemoteException;
  
  void callSessionRttModifyResponseReceived(int paramInt) throws RemoteException;
  
  void callSessionStartFailed(IImsCallSession paramIImsCallSession, ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void callSessionStarted(IImsCallSession paramIImsCallSession, ImsCallProfile paramImsCallProfile) throws RemoteException;
  
  void callSessionSuppServiceReceived(IImsCallSession paramIImsCallSession, ImsSuppServiceNotification paramImsSuppServiceNotification) throws RemoteException;
  
  void callSessionTerminated(IImsCallSession paramIImsCallSession, ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void callSessionTransferFailed(ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void callSessionTransferred() throws RemoteException;
  
  void callSessionTtyModeReceived(IImsCallSession paramIImsCallSession, int paramInt) throws RemoteException;
  
  void callSessionUpdateFailed(IImsCallSession paramIImsCallSession, ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void callSessionUpdateReceived(IImsCallSession paramIImsCallSession, ImsCallProfile paramImsCallProfile) throws RemoteException;
  
  void callSessionUpdated(IImsCallSession paramIImsCallSession, ImsCallProfile paramImsCallProfile) throws RemoteException;
  
  void callSessionUssdMessageReceived(IImsCallSession paramIImsCallSession, int paramInt, String paramString) throws RemoteException;
  
  class Default implements IImsCallSessionListener {
    public void callSessionProgressing(IImsCallSession param1IImsCallSession, ImsStreamMediaProfile param1ImsStreamMediaProfile) throws RemoteException {}
    
    public void callSessionStarted(IImsCallSession param1IImsCallSession, ImsCallProfile param1ImsCallProfile) throws RemoteException {}
    
    public void callSessionStartFailed(IImsCallSession param1IImsCallSession, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void callSessionTerminated(IImsCallSession param1IImsCallSession, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void callSessionHeld(IImsCallSession param1IImsCallSession, ImsCallProfile param1ImsCallProfile) throws RemoteException {}
    
    public void callSessionHoldFailed(IImsCallSession param1IImsCallSession, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void callSessionHoldReceived(IImsCallSession param1IImsCallSession, ImsCallProfile param1ImsCallProfile) throws RemoteException {}
    
    public void callSessionResumed(IImsCallSession param1IImsCallSession, ImsCallProfile param1ImsCallProfile) throws RemoteException {}
    
    public void callSessionResumeFailed(IImsCallSession param1IImsCallSession, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void callSessionResumeReceived(IImsCallSession param1IImsCallSession, ImsCallProfile param1ImsCallProfile) throws RemoteException {}
    
    public void callSessionMergeStarted(IImsCallSession param1IImsCallSession1, IImsCallSession param1IImsCallSession2, ImsCallProfile param1ImsCallProfile) throws RemoteException {}
    
    public void callSessionMergeComplete(IImsCallSession param1IImsCallSession) throws RemoteException {}
    
    public void callSessionMergeFailed(IImsCallSession param1IImsCallSession, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void callSessionUpdated(IImsCallSession param1IImsCallSession, ImsCallProfile param1ImsCallProfile) throws RemoteException {}
    
    public void callSessionUpdateFailed(IImsCallSession param1IImsCallSession, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void callSessionUpdateReceived(IImsCallSession param1IImsCallSession, ImsCallProfile param1ImsCallProfile) throws RemoteException {}
    
    public void callSessionConferenceExtended(IImsCallSession param1IImsCallSession1, IImsCallSession param1IImsCallSession2, ImsCallProfile param1ImsCallProfile) throws RemoteException {}
    
    public void callSessionConferenceExtendFailed(IImsCallSession param1IImsCallSession, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void callSessionConferenceExtendReceived(IImsCallSession param1IImsCallSession1, IImsCallSession param1IImsCallSession2, ImsCallProfile param1ImsCallProfile) throws RemoteException {}
    
    public void callSessionInviteParticipantsRequestDelivered(IImsCallSession param1IImsCallSession) throws RemoteException {}
    
    public void callSessionInviteParticipantsRequestFailed(IImsCallSession param1IImsCallSession, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void callSessionRemoveParticipantsRequestDelivered(IImsCallSession param1IImsCallSession) throws RemoteException {}
    
    public void callSessionRemoveParticipantsRequestFailed(IImsCallSession param1IImsCallSession, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void callSessionConferenceStateUpdated(IImsCallSession param1IImsCallSession, ImsConferenceState param1ImsConferenceState) throws RemoteException {}
    
    public void callSessionUssdMessageReceived(IImsCallSession param1IImsCallSession, int param1Int, String param1String) throws RemoteException {}
    
    public void callSessionHandover(IImsCallSession param1IImsCallSession, int param1Int1, int param1Int2, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void callSessionHandoverFailed(IImsCallSession param1IImsCallSession, int param1Int1, int param1Int2, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void callSessionMayHandover(IImsCallSession param1IImsCallSession, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void callSessionTtyModeReceived(IImsCallSession param1IImsCallSession, int param1Int) throws RemoteException {}
    
    public void callSessionMultipartyStateChanged(IImsCallSession param1IImsCallSession, boolean param1Boolean) throws RemoteException {}
    
    public void callSessionSuppServiceReceived(IImsCallSession param1IImsCallSession, ImsSuppServiceNotification param1ImsSuppServiceNotification) throws RemoteException {}
    
    public void callSessionRttModifyRequestReceived(IImsCallSession param1IImsCallSession, ImsCallProfile param1ImsCallProfile) throws RemoteException {}
    
    public void callSessionRttModifyResponseReceived(int param1Int) throws RemoteException {}
    
    public void callSessionRttMessageReceived(String param1String) throws RemoteException {}
    
    public void callSessionRttAudioIndicatorChanged(ImsStreamMediaProfile param1ImsStreamMediaProfile) throws RemoteException {}
    
    public void callSessionTransferred() throws RemoteException {}
    
    public void callSessionTransferFailed(ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void callQualityChanged(CallQuality param1CallQuality) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsCallSessionListener {
    private static final String DESCRIPTOR = "com.android.ims.internal.IImsCallSessionListener";
    
    static final int TRANSACTION_callQualityChanged = 38;
    
    static final int TRANSACTION_callSessionConferenceExtendFailed = 18;
    
    static final int TRANSACTION_callSessionConferenceExtendReceived = 19;
    
    static final int TRANSACTION_callSessionConferenceExtended = 17;
    
    static final int TRANSACTION_callSessionConferenceStateUpdated = 24;
    
    static final int TRANSACTION_callSessionHandover = 26;
    
    static final int TRANSACTION_callSessionHandoverFailed = 27;
    
    static final int TRANSACTION_callSessionHeld = 5;
    
    static final int TRANSACTION_callSessionHoldFailed = 6;
    
    static final int TRANSACTION_callSessionHoldReceived = 7;
    
    static final int TRANSACTION_callSessionInviteParticipantsRequestDelivered = 20;
    
    static final int TRANSACTION_callSessionInviteParticipantsRequestFailed = 21;
    
    static final int TRANSACTION_callSessionMayHandover = 28;
    
    static final int TRANSACTION_callSessionMergeComplete = 12;
    
    static final int TRANSACTION_callSessionMergeFailed = 13;
    
    static final int TRANSACTION_callSessionMergeStarted = 11;
    
    static final int TRANSACTION_callSessionMultipartyStateChanged = 30;
    
    static final int TRANSACTION_callSessionProgressing = 1;
    
    static final int TRANSACTION_callSessionRemoveParticipantsRequestDelivered = 22;
    
    static final int TRANSACTION_callSessionRemoveParticipantsRequestFailed = 23;
    
    static final int TRANSACTION_callSessionResumeFailed = 9;
    
    static final int TRANSACTION_callSessionResumeReceived = 10;
    
    static final int TRANSACTION_callSessionResumed = 8;
    
    static final int TRANSACTION_callSessionRttAudioIndicatorChanged = 35;
    
    static final int TRANSACTION_callSessionRttMessageReceived = 34;
    
    static final int TRANSACTION_callSessionRttModifyRequestReceived = 32;
    
    static final int TRANSACTION_callSessionRttModifyResponseReceived = 33;
    
    static final int TRANSACTION_callSessionStartFailed = 3;
    
    static final int TRANSACTION_callSessionStarted = 2;
    
    static final int TRANSACTION_callSessionSuppServiceReceived = 31;
    
    static final int TRANSACTION_callSessionTerminated = 4;
    
    static final int TRANSACTION_callSessionTransferFailed = 37;
    
    static final int TRANSACTION_callSessionTransferred = 36;
    
    static final int TRANSACTION_callSessionTtyModeReceived = 29;
    
    static final int TRANSACTION_callSessionUpdateFailed = 15;
    
    static final int TRANSACTION_callSessionUpdateReceived = 16;
    
    static final int TRANSACTION_callSessionUpdated = 14;
    
    static final int TRANSACTION_callSessionUssdMessageReceived = 25;
    
    public Stub() {
      attachInterface(this, "com.android.ims.internal.IImsCallSessionListener");
    }
    
    public static IImsCallSessionListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.ims.internal.IImsCallSessionListener");
      if (iInterface != null && iInterface instanceof IImsCallSessionListener)
        return (IImsCallSessionListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 38:
          return "callQualityChanged";
        case 37:
          return "callSessionTransferFailed";
        case 36:
          return "callSessionTransferred";
        case 35:
          return "callSessionRttAudioIndicatorChanged";
        case 34:
          return "callSessionRttMessageReceived";
        case 33:
          return "callSessionRttModifyResponseReceived";
        case 32:
          return "callSessionRttModifyRequestReceived";
        case 31:
          return "callSessionSuppServiceReceived";
        case 30:
          return "callSessionMultipartyStateChanged";
        case 29:
          return "callSessionTtyModeReceived";
        case 28:
          return "callSessionMayHandover";
        case 27:
          return "callSessionHandoverFailed";
        case 26:
          return "callSessionHandover";
        case 25:
          return "callSessionUssdMessageReceived";
        case 24:
          return "callSessionConferenceStateUpdated";
        case 23:
          return "callSessionRemoveParticipantsRequestFailed";
        case 22:
          return "callSessionRemoveParticipantsRequestDelivered";
        case 21:
          return "callSessionInviteParticipantsRequestFailed";
        case 20:
          return "callSessionInviteParticipantsRequestDelivered";
        case 19:
          return "callSessionConferenceExtendReceived";
        case 18:
          return "callSessionConferenceExtendFailed";
        case 17:
          return "callSessionConferenceExtended";
        case 16:
          return "callSessionUpdateReceived";
        case 15:
          return "callSessionUpdateFailed";
        case 14:
          return "callSessionUpdated";
        case 13:
          return "callSessionMergeFailed";
        case 12:
          return "callSessionMergeComplete";
        case 11:
          return "callSessionMergeStarted";
        case 10:
          return "callSessionResumeReceived";
        case 9:
          return "callSessionResumeFailed";
        case 8:
          return "callSessionResumed";
        case 7:
          return "callSessionHoldReceived";
        case 6:
          return "callSessionHoldFailed";
        case 5:
          return "callSessionHeld";
        case 4:
          return "callSessionTerminated";
        case 3:
          return "callSessionStartFailed";
        case 2:
          return "callSessionStarted";
        case 1:
          break;
      } 
      return "callSessionProgressing";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IImsCallSession iImsCallSession;
      if (param1Int1 != 1598968902) {
        String str;
        IImsCallSession iImsCallSession1;
        boolean bool;
        IImsCallSession iImsCallSession2;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 38:
            param1Parcel1.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            if (param1Parcel1.readInt() != 0) {
              CallQuality callQuality = (CallQuality)CallQuality.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            callQualityChanged((CallQuality)param1Parcel1);
            return true;
          case 37:
            param1Parcel1.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            if (param1Parcel1.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            callSessionTransferFailed((ImsReasonInfo)param1Parcel1);
            return true;
          case 36:
            param1Parcel1.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            callSessionTransferred();
            return true;
          case 35:
            param1Parcel1.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            if (param1Parcel1.readInt() != 0) {
              ImsStreamMediaProfile imsStreamMediaProfile = (ImsStreamMediaProfile)ImsStreamMediaProfile.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            callSessionRttAudioIndicatorChanged((ImsStreamMediaProfile)param1Parcel1);
            return true;
          case 34:
            param1Parcel1.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            str = param1Parcel1.readString();
            callSessionRttMessageReceived(str);
            return true;
          case 33:
            str.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            param1Int1 = str.readInt();
            callSessionRttModifyResponseReceived(param1Int1);
            return true;
          case 32:
            str.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(str.readStrongBinder());
            if (str.readInt() != 0) {
              ImsCallProfile imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            callSessionRttModifyRequestReceived(iImsCallSession, (ImsCallProfile)str);
            return true;
          case 31:
            str.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(str.readStrongBinder());
            if (str.readInt() != 0) {
              ImsSuppServiceNotification imsSuppServiceNotification = (ImsSuppServiceNotification)ImsSuppServiceNotification.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            callSessionSuppServiceReceived(iImsCallSession, (ImsSuppServiceNotification)str);
            return true;
          case 30:
            str.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(str.readStrongBinder());
            if (str.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            callSessionMultipartyStateChanged(iImsCallSession, bool);
            return true;
          case 29:
            str.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(str.readStrongBinder());
            param1Int1 = str.readInt();
            callSessionTtyModeReceived(iImsCallSession, param1Int1);
            return true;
          case 28:
            str.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(str.readStrongBinder());
            param1Int1 = str.readInt();
            param1Int2 = str.readInt();
            callSessionMayHandover(iImsCallSession, param1Int1, param1Int2);
            return true;
          case 27:
            str.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(str.readStrongBinder());
            param1Int1 = str.readInt();
            param1Int2 = str.readInt();
            if (str.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            callSessionHandoverFailed(iImsCallSession, param1Int1, param1Int2, (ImsReasonInfo)str);
            return true;
          case 26:
            str.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(str.readStrongBinder());
            param1Int1 = str.readInt();
            param1Int2 = str.readInt();
            if (str.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            callSessionHandover(iImsCallSession, param1Int1, param1Int2, (ImsReasonInfo)str);
            return true;
          case 25:
            str.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(str.readStrongBinder());
            param1Int1 = str.readInt();
            str = str.readString();
            callSessionUssdMessageReceived(iImsCallSession, param1Int1, str);
            return true;
          case 24:
            str.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(str.readStrongBinder());
            if (str.readInt() != 0) {
              ImsConferenceState imsConferenceState = (ImsConferenceState)ImsConferenceState.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            callSessionConferenceStateUpdated(iImsCallSession, (ImsConferenceState)str);
            return true;
          case 23:
            str.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(str.readStrongBinder());
            if (str.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            callSessionRemoveParticipantsRequestFailed(iImsCallSession, (ImsReasonInfo)str);
            return true;
          case 22:
            str.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession1 = IImsCallSession.Stub.asInterface(str.readStrongBinder());
            callSessionRemoveParticipantsRequestDelivered(iImsCallSession1);
            return true;
          case 21:
            iImsCallSession1.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(iImsCallSession1.readStrongBinder());
            if (iImsCallSession1.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionInviteParticipantsRequestFailed(iImsCallSession, (ImsReasonInfo)iImsCallSession1);
            return true;
          case 20:
            iImsCallSession1.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession1 = IImsCallSession.Stub.asInterface(iImsCallSession1.readStrongBinder());
            callSessionInviteParticipantsRequestDelivered(iImsCallSession1);
            return true;
          case 19:
            iImsCallSession1.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(iImsCallSession1.readStrongBinder());
            iImsCallSession2 = IImsCallSession.Stub.asInterface(iImsCallSession1.readStrongBinder());
            if (iImsCallSession1.readInt() != 0) {
              ImsCallProfile imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionConferenceExtendReceived(iImsCallSession, iImsCallSession2, (ImsCallProfile)iImsCallSession1);
            return true;
          case 18:
            iImsCallSession1.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(iImsCallSession1.readStrongBinder());
            if (iImsCallSession1.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionConferenceExtendFailed(iImsCallSession, (ImsReasonInfo)iImsCallSession1);
            return true;
          case 17:
            iImsCallSession1.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(iImsCallSession1.readStrongBinder());
            iImsCallSession2 = IImsCallSession.Stub.asInterface(iImsCallSession1.readStrongBinder());
            if (iImsCallSession1.readInt() != 0) {
              ImsCallProfile imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionConferenceExtended(iImsCallSession, iImsCallSession2, (ImsCallProfile)iImsCallSession1);
            return true;
          case 16:
            iImsCallSession1.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(iImsCallSession1.readStrongBinder());
            if (iImsCallSession1.readInt() != 0) {
              ImsCallProfile imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionUpdateReceived(iImsCallSession, (ImsCallProfile)iImsCallSession1);
            return true;
          case 15:
            iImsCallSession1.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(iImsCallSession1.readStrongBinder());
            if (iImsCallSession1.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionUpdateFailed(iImsCallSession, (ImsReasonInfo)iImsCallSession1);
            return true;
          case 14:
            iImsCallSession1.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(iImsCallSession1.readStrongBinder());
            if (iImsCallSession1.readInt() != 0) {
              ImsCallProfile imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionUpdated(iImsCallSession, (ImsCallProfile)iImsCallSession1);
            return true;
          case 13:
            iImsCallSession1.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(iImsCallSession1.readStrongBinder());
            if (iImsCallSession1.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionMergeFailed(iImsCallSession, (ImsReasonInfo)iImsCallSession1);
            return true;
          case 12:
            iImsCallSession1.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession1 = IImsCallSession.Stub.asInterface(iImsCallSession1.readStrongBinder());
            callSessionMergeComplete(iImsCallSession1);
            return true;
          case 11:
            iImsCallSession1.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession2 = IImsCallSession.Stub.asInterface(iImsCallSession1.readStrongBinder());
            iImsCallSession = IImsCallSession.Stub.asInterface(iImsCallSession1.readStrongBinder());
            if (iImsCallSession1.readInt() != 0) {
              ImsCallProfile imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionMergeStarted(iImsCallSession2, iImsCallSession, (ImsCallProfile)iImsCallSession1);
            return true;
          case 10:
            iImsCallSession1.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(iImsCallSession1.readStrongBinder());
            if (iImsCallSession1.readInt() != 0) {
              ImsCallProfile imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionResumeReceived(iImsCallSession, (ImsCallProfile)iImsCallSession1);
            return true;
          case 9:
            iImsCallSession1.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(iImsCallSession1.readStrongBinder());
            if (iImsCallSession1.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionResumeFailed(iImsCallSession, (ImsReasonInfo)iImsCallSession1);
            return true;
          case 8:
            iImsCallSession1.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(iImsCallSession1.readStrongBinder());
            if (iImsCallSession1.readInt() != 0) {
              ImsCallProfile imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionResumed(iImsCallSession, (ImsCallProfile)iImsCallSession1);
            return true;
          case 7:
            iImsCallSession1.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(iImsCallSession1.readStrongBinder());
            if (iImsCallSession1.readInt() != 0) {
              ImsCallProfile imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionHoldReceived(iImsCallSession, (ImsCallProfile)iImsCallSession1);
            return true;
          case 6:
            iImsCallSession1.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(iImsCallSession1.readStrongBinder());
            if (iImsCallSession1.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionHoldFailed(iImsCallSession, (ImsReasonInfo)iImsCallSession1);
            return true;
          case 5:
            iImsCallSession1.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(iImsCallSession1.readStrongBinder());
            if (iImsCallSession1.readInt() != 0) {
              ImsCallProfile imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionHeld(iImsCallSession, (ImsCallProfile)iImsCallSession1);
            return true;
          case 4:
            iImsCallSession1.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(iImsCallSession1.readStrongBinder());
            if (iImsCallSession1.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionTerminated(iImsCallSession, (ImsReasonInfo)iImsCallSession1);
            return true;
          case 3:
            iImsCallSession1.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(iImsCallSession1.readStrongBinder());
            if (iImsCallSession1.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionStartFailed(iImsCallSession, (ImsReasonInfo)iImsCallSession1);
            return true;
          case 2:
            iImsCallSession1.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
            iImsCallSession = IImsCallSession.Stub.asInterface(iImsCallSession1.readStrongBinder());
            if (iImsCallSession1.readInt() != 0) {
              ImsCallProfile imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)iImsCallSession1);
            } else {
              iImsCallSession1 = null;
            } 
            callSessionStarted(iImsCallSession, (ImsCallProfile)iImsCallSession1);
            return true;
          case 1:
            break;
        } 
        iImsCallSession1.enforceInterface("com.android.ims.internal.IImsCallSessionListener");
        iImsCallSession = IImsCallSession.Stub.asInterface(iImsCallSession1.readStrongBinder());
        if (iImsCallSession1.readInt() != 0) {
          ImsStreamMediaProfile imsStreamMediaProfile = (ImsStreamMediaProfile)ImsStreamMediaProfile.CREATOR.createFromParcel((Parcel)iImsCallSession1);
        } else {
          iImsCallSession1 = null;
        } 
        callSessionProgressing(iImsCallSession, (ImsStreamMediaProfile)iImsCallSession1);
        return true;
      } 
      iImsCallSession.writeString("com.android.ims.internal.IImsCallSessionListener");
      return true;
    }
    
    private static class Proxy implements IImsCallSessionListener {
      public static IImsCallSessionListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.ims.internal.IImsCallSessionListener";
      }
      
      public void callSessionProgressing(IImsCallSession param2IImsCallSession, ImsStreamMediaProfile param2ImsStreamMediaProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ImsStreamMediaProfile != null) {
            parcel.writeInt(1);
            param2ImsStreamMediaProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionProgressing(param2IImsCallSession, param2ImsStreamMediaProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionStarted(IImsCallSession param2IImsCallSession, ImsCallProfile param2ImsCallProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ImsCallProfile != null) {
            parcel.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionStarted(param2IImsCallSession, param2ImsCallProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionStartFailed(IImsCallSession param2IImsCallSession, ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionStartFailed(param2IImsCallSession, param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionTerminated(IImsCallSession param2IImsCallSession, ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionTerminated(param2IImsCallSession, param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionHeld(IImsCallSession param2IImsCallSession, ImsCallProfile param2ImsCallProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ImsCallProfile != null) {
            parcel.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionHeld(param2IImsCallSession, param2ImsCallProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionHoldFailed(IImsCallSession param2IImsCallSession, ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionHoldFailed(param2IImsCallSession, param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionHoldReceived(IImsCallSession param2IImsCallSession, ImsCallProfile param2ImsCallProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ImsCallProfile != null) {
            parcel.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionHoldReceived(param2IImsCallSession, param2ImsCallProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionResumed(IImsCallSession param2IImsCallSession, ImsCallProfile param2ImsCallProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ImsCallProfile != null) {
            parcel.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionResumed(param2IImsCallSession, param2ImsCallProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionResumeFailed(IImsCallSession param2IImsCallSession, ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionResumeFailed(param2IImsCallSession, param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionResumeReceived(IImsCallSession param2IImsCallSession, ImsCallProfile param2ImsCallProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ImsCallProfile != null) {
            parcel.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionResumeReceived(param2IImsCallSession, param2ImsCallProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionMergeStarted(IImsCallSession param2IImsCallSession1, IImsCallSession param2IImsCallSession2, ImsCallProfile param2ImsCallProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession1 != null) {
            iBinder = param2IImsCallSession1.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2IImsCallSession2 != null) {
            iBinder = param2IImsCallSession2.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ImsCallProfile != null) {
            parcel.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(11, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionMergeStarted(param2IImsCallSession1, param2IImsCallSession2, param2ImsCallProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionMergeComplete(IImsCallSession param2IImsCallSession) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(12, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionMergeComplete(param2IImsCallSession);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionMergeFailed(IImsCallSession param2IImsCallSession, ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(13, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionMergeFailed(param2IImsCallSession, param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionUpdated(IImsCallSession param2IImsCallSession, ImsCallProfile param2ImsCallProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ImsCallProfile != null) {
            parcel.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(14, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionUpdated(param2IImsCallSession, param2ImsCallProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionUpdateFailed(IImsCallSession param2IImsCallSession, ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(15, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionUpdateFailed(param2IImsCallSession, param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionUpdateReceived(IImsCallSession param2IImsCallSession, ImsCallProfile param2ImsCallProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ImsCallProfile != null) {
            parcel.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(16, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionUpdateReceived(param2IImsCallSession, param2ImsCallProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionConferenceExtended(IImsCallSession param2IImsCallSession1, IImsCallSession param2IImsCallSession2, ImsCallProfile param2ImsCallProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession1 != null) {
            iBinder = param2IImsCallSession1.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2IImsCallSession2 != null) {
            iBinder = param2IImsCallSession2.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ImsCallProfile != null) {
            parcel.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(17, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionConferenceExtended(param2IImsCallSession1, param2IImsCallSession2, param2ImsCallProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionConferenceExtendFailed(IImsCallSession param2IImsCallSession, ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(18, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionConferenceExtendFailed(param2IImsCallSession, param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionConferenceExtendReceived(IImsCallSession param2IImsCallSession1, IImsCallSession param2IImsCallSession2, ImsCallProfile param2ImsCallProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession1 != null) {
            iBinder = param2IImsCallSession1.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2IImsCallSession2 != null) {
            iBinder = param2IImsCallSession2.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ImsCallProfile != null) {
            parcel.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(19, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionConferenceExtendReceived(param2IImsCallSession1, param2IImsCallSession2, param2ImsCallProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionInviteParticipantsRequestDelivered(IImsCallSession param2IImsCallSession) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(20, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionInviteParticipantsRequestDelivered(param2IImsCallSession);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionInviteParticipantsRequestFailed(IImsCallSession param2IImsCallSession, ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(21, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionInviteParticipantsRequestFailed(param2IImsCallSession, param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionRemoveParticipantsRequestDelivered(IImsCallSession param2IImsCallSession) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(22, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionRemoveParticipantsRequestDelivered(param2IImsCallSession);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionRemoveParticipantsRequestFailed(IImsCallSession param2IImsCallSession, ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(23, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionRemoveParticipantsRequestFailed(param2IImsCallSession, param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionConferenceStateUpdated(IImsCallSession param2IImsCallSession, ImsConferenceState param2ImsConferenceState) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ImsConferenceState != null) {
            parcel.writeInt(1);
            param2ImsConferenceState.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(24, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionConferenceStateUpdated(param2IImsCallSession, param2ImsConferenceState);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionUssdMessageReceived(IImsCallSession param2IImsCallSession, int param2Int, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(25, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionUssdMessageReceived(param2IImsCallSession, param2Int, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionHandover(IImsCallSession param2IImsCallSession, int param2Int1, int param2Int2, ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(26, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionHandover(param2IImsCallSession, param2Int1, param2Int2, param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionHandoverFailed(IImsCallSession param2IImsCallSession, int param2Int1, int param2Int2, ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(27, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionHandoverFailed(param2IImsCallSession, param2Int1, param2Int2, param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionMayHandover(IImsCallSession param2IImsCallSession, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(28, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionMayHandover(param2IImsCallSession, param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionTtyModeReceived(IImsCallSession param2IImsCallSession, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(29, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionTtyModeReceived(param2IImsCallSession, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionMultipartyStateChanged(IImsCallSession param2IImsCallSession, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          boolean bool;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(30, parcel, null, 1);
          if (!bool1 && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionMultipartyStateChanged(param2IImsCallSession, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionSuppServiceReceived(IImsCallSession param2IImsCallSession, ImsSuppServiceNotification param2ImsSuppServiceNotification) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ImsSuppServiceNotification != null) {
            parcel.writeInt(1);
            param2ImsSuppServiceNotification.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(31, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionSuppServiceReceived(param2IImsCallSession, param2ImsSuppServiceNotification);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionRttModifyRequestReceived(IImsCallSession param2IImsCallSession, ImsCallProfile param2ImsCallProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ImsCallProfile != null) {
            parcel.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(32, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionRttModifyRequestReceived(param2IImsCallSession, param2ImsCallProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionRttModifyResponseReceived(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(33, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionRttModifyResponseReceived(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionRttMessageReceived(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(34, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionRttMessageReceived(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionRttAudioIndicatorChanged(ImsStreamMediaProfile param2ImsStreamMediaProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2ImsStreamMediaProfile != null) {
            parcel.writeInt(1);
            param2ImsStreamMediaProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(35, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionRttAudioIndicatorChanged(param2ImsStreamMediaProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionTransferred() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          boolean bool = this.mRemote.transact(36, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionTransferred();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callSessionTransferFailed(ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(37, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callSessionTransferFailed(param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void callQualityChanged(CallQuality param2CallQuality) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsCallSessionListener");
          if (param2CallQuality != null) {
            parcel.writeInt(1);
            param2CallQuality.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(38, parcel, null, 1);
          if (!bool && IImsCallSessionListener.Stub.getDefaultImpl() != null) {
            IImsCallSessionListener.Stub.getDefaultImpl().callQualityChanged(param2CallQuality);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsCallSessionListener param1IImsCallSessionListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsCallSessionListener != null) {
          Proxy.sDefaultImpl = param1IImsCallSessionListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsCallSessionListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
