package com.android.ims.internal;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.telephony.ims.ImsCallForwardInfo;
import android.telephony.ims.ImsReasonInfo;
import android.telephony.ims.ImsSsData;
import android.telephony.ims.ImsSsInfo;

public interface IImsUtListener extends IInterface {
  void lineIdentificationSupplementaryServiceResponse(int paramInt, ImsSsInfo paramImsSsInfo) throws RemoteException;
  
  void onSupplementaryServiceIndication(ImsSsData paramImsSsData) throws RemoteException;
  
  void utConfigurationCallBarringQueried(IImsUt paramIImsUt, int paramInt, ImsSsInfo[] paramArrayOfImsSsInfo) throws RemoteException;
  
  void utConfigurationCallForwardQueried(IImsUt paramIImsUt, int paramInt, ImsCallForwardInfo[] paramArrayOfImsCallForwardInfo) throws RemoteException;
  
  void utConfigurationCallWaitingQueried(IImsUt paramIImsUt, int paramInt, ImsSsInfo[] paramArrayOfImsSsInfo) throws RemoteException;
  
  void utConfigurationQueried(IImsUt paramIImsUt, int paramInt, Bundle paramBundle) throws RemoteException;
  
  void utConfigurationQueryFailed(IImsUt paramIImsUt, int paramInt, ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void utConfigurationUpdateFailed(IImsUt paramIImsUt, int paramInt, ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void utConfigurationUpdated(IImsUt paramIImsUt, int paramInt) throws RemoteException;
  
  class Default implements IImsUtListener {
    public void utConfigurationUpdated(IImsUt param1IImsUt, int param1Int) throws RemoteException {}
    
    public void utConfigurationUpdateFailed(IImsUt param1IImsUt, int param1Int, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void utConfigurationQueried(IImsUt param1IImsUt, int param1Int, Bundle param1Bundle) throws RemoteException {}
    
    public void utConfigurationQueryFailed(IImsUt param1IImsUt, int param1Int, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void lineIdentificationSupplementaryServiceResponse(int param1Int, ImsSsInfo param1ImsSsInfo) throws RemoteException {}
    
    public void utConfigurationCallBarringQueried(IImsUt param1IImsUt, int param1Int, ImsSsInfo[] param1ArrayOfImsSsInfo) throws RemoteException {}
    
    public void utConfigurationCallForwardQueried(IImsUt param1IImsUt, int param1Int, ImsCallForwardInfo[] param1ArrayOfImsCallForwardInfo) throws RemoteException {}
    
    public void utConfigurationCallWaitingQueried(IImsUt param1IImsUt, int param1Int, ImsSsInfo[] param1ArrayOfImsSsInfo) throws RemoteException {}
    
    public void onSupplementaryServiceIndication(ImsSsData param1ImsSsData) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsUtListener {
    private static final String DESCRIPTOR = "com.android.ims.internal.IImsUtListener";
    
    static final int TRANSACTION_lineIdentificationSupplementaryServiceResponse = 5;
    
    static final int TRANSACTION_onSupplementaryServiceIndication = 9;
    
    static final int TRANSACTION_utConfigurationCallBarringQueried = 6;
    
    static final int TRANSACTION_utConfigurationCallForwardQueried = 7;
    
    static final int TRANSACTION_utConfigurationCallWaitingQueried = 8;
    
    static final int TRANSACTION_utConfigurationQueried = 3;
    
    static final int TRANSACTION_utConfigurationQueryFailed = 4;
    
    static final int TRANSACTION_utConfigurationUpdateFailed = 2;
    
    static final int TRANSACTION_utConfigurationUpdated = 1;
    
    public Stub() {
      attachInterface(this, "com.android.ims.internal.IImsUtListener");
    }
    
    public static IImsUtListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.ims.internal.IImsUtListener");
      if (iInterface != null && iInterface instanceof IImsUtListener)
        return (IImsUtListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 9:
          return "onSupplementaryServiceIndication";
        case 8:
          return "utConfigurationCallWaitingQueried";
        case 7:
          return "utConfigurationCallForwardQueried";
        case 6:
          return "utConfigurationCallBarringQueried";
        case 5:
          return "lineIdentificationSupplementaryServiceResponse";
        case 4:
          return "utConfigurationQueryFailed";
        case 3:
          return "utConfigurationQueried";
        case 2:
          return "utConfigurationUpdateFailed";
        case 1:
          break;
      } 
      return "utConfigurationUpdated";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IImsUt iImsUt;
      if (param1Int1 != 1598968902) {
        ImsSsInfo[] arrayOfImsSsInfo2;
        ImsCallForwardInfo[] arrayOfImsCallForwardInfo;
        ImsSsInfo[] arrayOfImsSsInfo1;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 9:
            param1Parcel1.enforceInterface("com.android.ims.internal.IImsUtListener");
            if (param1Parcel1.readInt() != 0) {
              ImsSsData imsSsData = (ImsSsData)ImsSsData.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onSupplementaryServiceIndication((ImsSsData)param1Parcel1);
            return true;
          case 8:
            param1Parcel1.enforceInterface("com.android.ims.internal.IImsUtListener");
            iImsUt = IImsUt.Stub.asInterface(param1Parcel1.readStrongBinder());
            param1Int1 = param1Parcel1.readInt();
            arrayOfImsSsInfo2 = (ImsSsInfo[])param1Parcel1.createTypedArray(ImsSsInfo.CREATOR);
            utConfigurationCallWaitingQueried(iImsUt, param1Int1, arrayOfImsSsInfo2);
            return true;
          case 7:
            arrayOfImsSsInfo2.enforceInterface("com.android.ims.internal.IImsUtListener");
            iImsUt = IImsUt.Stub.asInterface(arrayOfImsSsInfo2.readStrongBinder());
            param1Int1 = arrayOfImsSsInfo2.readInt();
            arrayOfImsCallForwardInfo = (ImsCallForwardInfo[])arrayOfImsSsInfo2.createTypedArray(ImsCallForwardInfo.CREATOR);
            utConfigurationCallForwardQueried(iImsUt, param1Int1, arrayOfImsCallForwardInfo);
            return true;
          case 6:
            arrayOfImsCallForwardInfo.enforceInterface("com.android.ims.internal.IImsUtListener");
            iImsUt = IImsUt.Stub.asInterface(arrayOfImsCallForwardInfo.readStrongBinder());
            param1Int1 = arrayOfImsCallForwardInfo.readInt();
            arrayOfImsSsInfo1 = (ImsSsInfo[])arrayOfImsCallForwardInfo.createTypedArray(ImsSsInfo.CREATOR);
            utConfigurationCallBarringQueried(iImsUt, param1Int1, arrayOfImsSsInfo1);
            return true;
          case 5:
            arrayOfImsSsInfo1.enforceInterface("com.android.ims.internal.IImsUtListener");
            param1Int1 = arrayOfImsSsInfo1.readInt();
            if (arrayOfImsSsInfo1.readInt() != 0) {
              ImsSsInfo imsSsInfo = (ImsSsInfo)ImsSsInfo.CREATOR.createFromParcel((Parcel)arrayOfImsSsInfo1);
            } else {
              arrayOfImsSsInfo1 = null;
            } 
            lineIdentificationSupplementaryServiceResponse(param1Int1, (ImsSsInfo)arrayOfImsSsInfo1);
            return true;
          case 4:
            arrayOfImsSsInfo1.enforceInterface("com.android.ims.internal.IImsUtListener");
            iImsUt = IImsUt.Stub.asInterface(arrayOfImsSsInfo1.readStrongBinder());
            param1Int1 = arrayOfImsSsInfo1.readInt();
            if (arrayOfImsSsInfo1.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)arrayOfImsSsInfo1);
            } else {
              arrayOfImsSsInfo1 = null;
            } 
            utConfigurationQueryFailed(iImsUt, param1Int1, (ImsReasonInfo)arrayOfImsSsInfo1);
            return true;
          case 3:
            arrayOfImsSsInfo1.enforceInterface("com.android.ims.internal.IImsUtListener");
            iImsUt = IImsUt.Stub.asInterface(arrayOfImsSsInfo1.readStrongBinder());
            param1Int1 = arrayOfImsSsInfo1.readInt();
            if (arrayOfImsSsInfo1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)arrayOfImsSsInfo1);
            } else {
              arrayOfImsSsInfo1 = null;
            } 
            utConfigurationQueried(iImsUt, param1Int1, (Bundle)arrayOfImsSsInfo1);
            return true;
          case 2:
            arrayOfImsSsInfo1.enforceInterface("com.android.ims.internal.IImsUtListener");
            iImsUt = IImsUt.Stub.asInterface(arrayOfImsSsInfo1.readStrongBinder());
            param1Int1 = arrayOfImsSsInfo1.readInt();
            if (arrayOfImsSsInfo1.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel((Parcel)arrayOfImsSsInfo1);
            } else {
              arrayOfImsSsInfo1 = null;
            } 
            utConfigurationUpdateFailed(iImsUt, param1Int1, (ImsReasonInfo)arrayOfImsSsInfo1);
            return true;
          case 1:
            break;
        } 
        arrayOfImsSsInfo1.enforceInterface("com.android.ims.internal.IImsUtListener");
        iImsUt = IImsUt.Stub.asInterface(arrayOfImsSsInfo1.readStrongBinder());
        param1Int1 = arrayOfImsSsInfo1.readInt();
        utConfigurationUpdated(iImsUt, param1Int1);
        return true;
      } 
      iImsUt.writeString("com.android.ims.internal.IImsUtListener");
      return true;
    }
    
    private static class Proxy implements IImsUtListener {
      public static IImsUtListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.ims.internal.IImsUtListener";
      }
      
      public void utConfigurationUpdated(IImsUt param2IImsUt, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsUtListener");
          if (param2IImsUt != null) {
            iBinder = param2IImsUt.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IImsUtListener.Stub.getDefaultImpl() != null) {
            IImsUtListener.Stub.getDefaultImpl().utConfigurationUpdated(param2IImsUt, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void utConfigurationUpdateFailed(IImsUt param2IImsUt, int param2Int, ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsUtListener");
          if (param2IImsUt != null) {
            iBinder = param2IImsUt.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int);
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IImsUtListener.Stub.getDefaultImpl() != null) {
            IImsUtListener.Stub.getDefaultImpl().utConfigurationUpdateFailed(param2IImsUt, param2Int, param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void utConfigurationQueried(IImsUt param2IImsUt, int param2Int, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsUtListener");
          if (param2IImsUt != null) {
            iBinder = param2IImsUt.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IImsUtListener.Stub.getDefaultImpl() != null) {
            IImsUtListener.Stub.getDefaultImpl().utConfigurationQueried(param2IImsUt, param2Int, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void utConfigurationQueryFailed(IImsUt param2IImsUt, int param2Int, ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsUtListener");
          if (param2IImsUt != null) {
            iBinder = param2IImsUt.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int);
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IImsUtListener.Stub.getDefaultImpl() != null) {
            IImsUtListener.Stub.getDefaultImpl().utConfigurationQueryFailed(param2IImsUt, param2Int, param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void lineIdentificationSupplementaryServiceResponse(int param2Int, ImsSsInfo param2ImsSsInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsUtListener");
          parcel.writeInt(param2Int);
          if (param2ImsSsInfo != null) {
            parcel.writeInt(1);
            param2ImsSsInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IImsUtListener.Stub.getDefaultImpl() != null) {
            IImsUtListener.Stub.getDefaultImpl().lineIdentificationSupplementaryServiceResponse(param2Int, param2ImsSsInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void utConfigurationCallBarringQueried(IImsUt param2IImsUt, int param2Int, ImsSsInfo[] param2ArrayOfImsSsInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsUtListener");
          if (param2IImsUt != null) {
            iBinder = param2IImsUt.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int);
          parcel.writeTypedArray((Parcelable[])param2ArrayOfImsSsInfo, 0);
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IImsUtListener.Stub.getDefaultImpl() != null) {
            IImsUtListener.Stub.getDefaultImpl().utConfigurationCallBarringQueried(param2IImsUt, param2Int, param2ArrayOfImsSsInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void utConfigurationCallForwardQueried(IImsUt param2IImsUt, int param2Int, ImsCallForwardInfo[] param2ArrayOfImsCallForwardInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsUtListener");
          if (param2IImsUt != null) {
            iBinder = param2IImsUt.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int);
          parcel.writeTypedArray((Parcelable[])param2ArrayOfImsCallForwardInfo, 0);
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IImsUtListener.Stub.getDefaultImpl() != null) {
            IImsUtListener.Stub.getDefaultImpl().utConfigurationCallForwardQueried(param2IImsUt, param2Int, param2ArrayOfImsCallForwardInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void utConfigurationCallWaitingQueried(IImsUt param2IImsUt, int param2Int, ImsSsInfo[] param2ArrayOfImsSsInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.ims.internal.IImsUtListener");
          if (param2IImsUt != null) {
            iBinder = param2IImsUt.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int);
          parcel.writeTypedArray((Parcelable[])param2ArrayOfImsSsInfo, 0);
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IImsUtListener.Stub.getDefaultImpl() != null) {
            IImsUtListener.Stub.getDefaultImpl().utConfigurationCallWaitingQueried(param2IImsUt, param2Int, param2ArrayOfImsSsInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSupplementaryServiceIndication(ImsSsData param2ImsSsData) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.ims.internal.IImsUtListener");
          if (param2ImsSsData != null) {
            parcel.writeInt(1);
            param2ImsSsData.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IImsUtListener.Stub.getDefaultImpl() != null) {
            IImsUtListener.Stub.getDefaultImpl().onSupplementaryServiceIndication(param2ImsSsData);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsUtListener param1IImsUtListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsUtListener != null) {
          Proxy.sDefaultImpl = param1IImsUtListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsUtListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
