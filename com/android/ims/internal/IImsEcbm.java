package com.android.ims.internal;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IImsEcbm extends IInterface {
  void exitEmergencyCallbackMode() throws RemoteException;
  
  void setListener(IImsEcbmListener paramIImsEcbmListener) throws RemoteException;
  
  class Default implements IImsEcbm {
    public void setListener(IImsEcbmListener param1IImsEcbmListener) throws RemoteException {}
    
    public void exitEmergencyCallbackMode() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsEcbm {
    private static final String DESCRIPTOR = "com.android.ims.internal.IImsEcbm";
    
    static final int TRANSACTION_exitEmergencyCallbackMode = 2;
    
    static final int TRANSACTION_setListener = 1;
    
    public Stub() {
      attachInterface(this, "com.android.ims.internal.IImsEcbm");
    }
    
    public static IImsEcbm asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.ims.internal.IImsEcbm");
      if (iInterface != null && iInterface instanceof IImsEcbm)
        return (IImsEcbm)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "exitEmergencyCallbackMode";
      } 
      return "setListener";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("com.android.ims.internal.IImsEcbm");
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.ims.internal.IImsEcbm");
        exitEmergencyCallbackMode();
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.ims.internal.IImsEcbm");
      IImsEcbmListener iImsEcbmListener = IImsEcbmListener.Stub.asInterface(param1Parcel1.readStrongBinder());
      setListener(iImsEcbmListener);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IImsEcbm {
      public static IImsEcbm sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.ims.internal.IImsEcbm";
      }
      
      public void setListener(IImsEcbmListener param2IImsEcbmListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsEcbm");
          if (param2IImsEcbmListener != null) {
            iBinder = param2IImsEcbmListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IImsEcbm.Stub.getDefaultImpl() != null) {
            IImsEcbm.Stub.getDefaultImpl().setListener(param2IImsEcbmListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void exitEmergencyCallbackMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsEcbm");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IImsEcbm.Stub.getDefaultImpl() != null) {
            IImsEcbm.Stub.getDefaultImpl().exitEmergencyCallbackMode();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsEcbm param1IImsEcbm) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsEcbm != null) {
          Proxy.sDefaultImpl = param1IImsEcbm;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsEcbm getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
