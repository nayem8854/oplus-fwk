package com.android.ims.internal;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Message;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.ims.ImsCallProfile;
import android.telephony.ims.ImsStreamMediaProfile;
import android.telephony.ims.aidl.IImsCallSessionListener;

public interface IImsCallSession extends IInterface {
  void accept(int paramInt, ImsStreamMediaProfile paramImsStreamMediaProfile) throws RemoteException;
  
  void close() throws RemoteException;
  
  void consultativeTransfer(IImsCallSession paramIImsCallSession) throws RemoteException;
  
  void deflect(String paramString) throws RemoteException;
  
  void extendToConference(String[] paramArrayOfString) throws RemoteException;
  
  String getCallId() throws RemoteException;
  
  ImsCallProfile getCallProfile() throws RemoteException;
  
  ImsCallProfile getLocalCallProfile() throws RemoteException;
  
  String getProperty(String paramString) throws RemoteException;
  
  ImsCallProfile getRemoteCallProfile() throws RemoteException;
  
  int getState() throws RemoteException;
  
  IImsVideoCallProvider getVideoCallProvider() throws RemoteException;
  
  void hold(ImsStreamMediaProfile paramImsStreamMediaProfile) throws RemoteException;
  
  void inviteParticipants(String[] paramArrayOfString) throws RemoteException;
  
  boolean isInCall() throws RemoteException;
  
  boolean isMultiparty() throws RemoteException;
  
  void merge() throws RemoteException;
  
  void reject(int paramInt) throws RemoteException;
  
  void removeParticipants(String[] paramArrayOfString) throws RemoteException;
  
  void resume(ImsStreamMediaProfile paramImsStreamMediaProfile) throws RemoteException;
  
  void sendDtmf(char paramChar, Message paramMessage) throws RemoteException;
  
  void sendRttMessage(String paramString) throws RemoteException;
  
  void sendRttModifyRequest(ImsCallProfile paramImsCallProfile) throws RemoteException;
  
  void sendRttModifyResponse(boolean paramBoolean) throws RemoteException;
  
  void sendUssd(String paramString) throws RemoteException;
  
  void setListener(IImsCallSessionListener paramIImsCallSessionListener) throws RemoteException;
  
  void setMute(boolean paramBoolean) throws RemoteException;
  
  void start(String paramString, ImsCallProfile paramImsCallProfile) throws RemoteException;
  
  void startConference(String[] paramArrayOfString, ImsCallProfile paramImsCallProfile) throws RemoteException;
  
  void startDtmf(char paramChar) throws RemoteException;
  
  void stopDtmf() throws RemoteException;
  
  void terminate(int paramInt) throws RemoteException;
  
  void transfer(String paramString, boolean paramBoolean) throws RemoteException;
  
  void update(int paramInt, ImsStreamMediaProfile paramImsStreamMediaProfile) throws RemoteException;
  
  class Default implements IImsCallSession {
    public void close() throws RemoteException {}
    
    public String getCallId() throws RemoteException {
      return null;
    }
    
    public ImsCallProfile getCallProfile() throws RemoteException {
      return null;
    }
    
    public ImsCallProfile getLocalCallProfile() throws RemoteException {
      return null;
    }
    
    public ImsCallProfile getRemoteCallProfile() throws RemoteException {
      return null;
    }
    
    public String getProperty(String param1String) throws RemoteException {
      return null;
    }
    
    public int getState() throws RemoteException {
      return 0;
    }
    
    public boolean isInCall() throws RemoteException {
      return false;
    }
    
    public void setListener(IImsCallSessionListener param1IImsCallSessionListener) throws RemoteException {}
    
    public void setMute(boolean param1Boolean) throws RemoteException {}
    
    public void start(String param1String, ImsCallProfile param1ImsCallProfile) throws RemoteException {}
    
    public void startConference(String[] param1ArrayOfString, ImsCallProfile param1ImsCallProfile) throws RemoteException {}
    
    public void accept(int param1Int, ImsStreamMediaProfile param1ImsStreamMediaProfile) throws RemoteException {}
    
    public void deflect(String param1String) throws RemoteException {}
    
    public void reject(int param1Int) throws RemoteException {}
    
    public void transfer(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public void consultativeTransfer(IImsCallSession param1IImsCallSession) throws RemoteException {}
    
    public void terminate(int param1Int) throws RemoteException {}
    
    public void hold(ImsStreamMediaProfile param1ImsStreamMediaProfile) throws RemoteException {}
    
    public void resume(ImsStreamMediaProfile param1ImsStreamMediaProfile) throws RemoteException {}
    
    public void merge() throws RemoteException {}
    
    public void update(int param1Int, ImsStreamMediaProfile param1ImsStreamMediaProfile) throws RemoteException {}
    
    public void extendToConference(String[] param1ArrayOfString) throws RemoteException {}
    
    public void inviteParticipants(String[] param1ArrayOfString) throws RemoteException {}
    
    public void removeParticipants(String[] param1ArrayOfString) throws RemoteException {}
    
    public void sendDtmf(char param1Char, Message param1Message) throws RemoteException {}
    
    public void startDtmf(char param1Char) throws RemoteException {}
    
    public void stopDtmf() throws RemoteException {}
    
    public void sendUssd(String param1String) throws RemoteException {}
    
    public IImsVideoCallProvider getVideoCallProvider() throws RemoteException {
      return null;
    }
    
    public boolean isMultiparty() throws RemoteException {
      return false;
    }
    
    public void sendRttModifyRequest(ImsCallProfile param1ImsCallProfile) throws RemoteException {}
    
    public void sendRttModifyResponse(boolean param1Boolean) throws RemoteException {}
    
    public void sendRttMessage(String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IImsCallSession {
    private static final String DESCRIPTOR = "com.android.ims.internal.IImsCallSession";
    
    static final int TRANSACTION_accept = 13;
    
    static final int TRANSACTION_close = 1;
    
    static final int TRANSACTION_consultativeTransfer = 17;
    
    static final int TRANSACTION_deflect = 14;
    
    static final int TRANSACTION_extendToConference = 23;
    
    static final int TRANSACTION_getCallId = 2;
    
    static final int TRANSACTION_getCallProfile = 3;
    
    static final int TRANSACTION_getLocalCallProfile = 4;
    
    static final int TRANSACTION_getProperty = 6;
    
    static final int TRANSACTION_getRemoteCallProfile = 5;
    
    static final int TRANSACTION_getState = 7;
    
    static final int TRANSACTION_getVideoCallProvider = 30;
    
    static final int TRANSACTION_hold = 19;
    
    static final int TRANSACTION_inviteParticipants = 24;
    
    static final int TRANSACTION_isInCall = 8;
    
    static final int TRANSACTION_isMultiparty = 31;
    
    static final int TRANSACTION_merge = 21;
    
    static final int TRANSACTION_reject = 15;
    
    static final int TRANSACTION_removeParticipants = 25;
    
    static final int TRANSACTION_resume = 20;
    
    static final int TRANSACTION_sendDtmf = 26;
    
    static final int TRANSACTION_sendRttMessage = 34;
    
    static final int TRANSACTION_sendRttModifyRequest = 32;
    
    static final int TRANSACTION_sendRttModifyResponse = 33;
    
    static final int TRANSACTION_sendUssd = 29;
    
    static final int TRANSACTION_setListener = 9;
    
    static final int TRANSACTION_setMute = 10;
    
    static final int TRANSACTION_start = 11;
    
    static final int TRANSACTION_startConference = 12;
    
    static final int TRANSACTION_startDtmf = 27;
    
    static final int TRANSACTION_stopDtmf = 28;
    
    static final int TRANSACTION_terminate = 18;
    
    static final int TRANSACTION_transfer = 16;
    
    static final int TRANSACTION_update = 22;
    
    public Stub() {
      attachInterface(this, "com.android.ims.internal.IImsCallSession");
    }
    
    public static IImsCallSession asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.ims.internal.IImsCallSession");
      if (iInterface != null && iInterface instanceof IImsCallSession)
        return (IImsCallSession)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 34:
          return "sendRttMessage";
        case 33:
          return "sendRttModifyResponse";
        case 32:
          return "sendRttModifyRequest";
        case 31:
          return "isMultiparty";
        case 30:
          return "getVideoCallProvider";
        case 29:
          return "sendUssd";
        case 28:
          return "stopDtmf";
        case 27:
          return "startDtmf";
        case 26:
          return "sendDtmf";
        case 25:
          return "removeParticipants";
        case 24:
          return "inviteParticipants";
        case 23:
          return "extendToConference";
        case 22:
          return "update";
        case 21:
          return "merge";
        case 20:
          return "resume";
        case 19:
          return "hold";
        case 18:
          return "terminate";
        case 17:
          return "consultativeTransfer";
        case 16:
          return "transfer";
        case 15:
          return "reject";
        case 14:
          return "deflect";
        case 13:
          return "accept";
        case 12:
          return "startConference";
        case 11:
          return "start";
        case 10:
          return "setMute";
        case 9:
          return "setListener";
        case 8:
          return "isInCall";
        case 7:
          return "getState";
        case 6:
          return "getProperty";
        case 5:
          return "getRemoteCallProfile";
        case 4:
          return "getLocalCallProfile";
        case 3:
          return "getCallProfile";
        case 2:
          return "getCallId";
        case 1:
          break;
      } 
      return "close";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool2;
        int j;
        boolean bool1;
        int i;
        String str5;
        IImsVideoCallProvider iImsVideoCallProvider;
        String str4, arrayOfString1[];
        IImsCallSession iImsCallSession;
        String str3;
        IImsCallSessionListener iImsCallSessionListener;
        String str2;
        ImsCallProfile imsCallProfile;
        String str1;
        char c;
        String str7, arrayOfString2[], str6;
        boolean bool3 = false, bool4 = false, bool5 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 34:
            param1Parcel1.enforceInterface("com.android.ims.internal.IImsCallSession");
            str5 = param1Parcel1.readString();
            sendRttMessage(str5);
            param1Parcel2.writeNoException();
            return true;
          case 33:
            str5.enforceInterface("com.android.ims.internal.IImsCallSession");
            bool4 = bool5;
            if (str5.readInt() != 0)
              bool4 = true; 
            sendRttModifyResponse(bool4);
            param1Parcel2.writeNoException();
            return true;
          case 32:
            str5.enforceInterface("com.android.ims.internal.IImsCallSession");
            if (str5.readInt() != 0) {
              ImsCallProfile imsCallProfile1 = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)str5);
            } else {
              str5 = null;
            } 
            sendRttModifyRequest((ImsCallProfile)str5);
            param1Parcel2.writeNoException();
            return true;
          case 31:
            str5.enforceInterface("com.android.ims.internal.IImsCallSession");
            bool2 = isMultiparty();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 30:
            str5.enforceInterface("com.android.ims.internal.IImsCallSession");
            iImsVideoCallProvider = getVideoCallProvider();
            param1Parcel2.writeNoException();
            if (iImsVideoCallProvider != null) {
              IBinder iBinder = iImsVideoCallProvider.asBinder();
            } else {
              iImsVideoCallProvider = null;
            } 
            param1Parcel2.writeStrongBinder((IBinder)iImsVideoCallProvider);
            return true;
          case 29:
            iImsVideoCallProvider.enforceInterface("com.android.ims.internal.IImsCallSession");
            str4 = iImsVideoCallProvider.readString();
            sendUssd(str4);
            param1Parcel2.writeNoException();
            return true;
          case 28:
            str4.enforceInterface("com.android.ims.internal.IImsCallSession");
            stopDtmf();
            param1Parcel2.writeNoException();
            return true;
          case 27:
            str4.enforceInterface("com.android.ims.internal.IImsCallSession");
            c = (char)str4.readInt();
            startDtmf(c);
            param1Parcel2.writeNoException();
            return true;
          case 26:
            str4.enforceInterface("com.android.ims.internal.IImsCallSession");
            c = (char)str4.readInt();
            if (str4.readInt() != 0) {
              Message message = (Message)Message.CREATOR.createFromParcel((Parcel)str4);
            } else {
              str4 = null;
            } 
            sendDtmf(c, (Message)str4);
            param1Parcel2.writeNoException();
            return true;
          case 25:
            str4.enforceInterface("com.android.ims.internal.IImsCallSession");
            arrayOfString1 = str4.createStringArray();
            removeParticipants(arrayOfString1);
            param1Parcel2.writeNoException();
            return true;
          case 24:
            arrayOfString1.enforceInterface("com.android.ims.internal.IImsCallSession");
            arrayOfString1 = arrayOfString1.createStringArray();
            inviteParticipants(arrayOfString1);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            arrayOfString1.enforceInterface("com.android.ims.internal.IImsCallSession");
            arrayOfString1 = arrayOfString1.createStringArray();
            extendToConference(arrayOfString1);
            param1Parcel2.writeNoException();
            return true;
          case 22:
            arrayOfString1.enforceInterface("com.android.ims.internal.IImsCallSession");
            j = arrayOfString1.readInt();
            if (arrayOfString1.readInt() != 0) {
              ImsStreamMediaProfile imsStreamMediaProfile = (ImsStreamMediaProfile)ImsStreamMediaProfile.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              arrayOfString1 = null;
            } 
            update(j, (ImsStreamMediaProfile)arrayOfString1);
            param1Parcel2.writeNoException();
            return true;
          case 21:
            arrayOfString1.enforceInterface("com.android.ims.internal.IImsCallSession");
            merge();
            param1Parcel2.writeNoException();
            return true;
          case 20:
            arrayOfString1.enforceInterface("com.android.ims.internal.IImsCallSession");
            if (arrayOfString1.readInt() != 0) {
              ImsStreamMediaProfile imsStreamMediaProfile = (ImsStreamMediaProfile)ImsStreamMediaProfile.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              arrayOfString1 = null;
            } 
            resume((ImsStreamMediaProfile)arrayOfString1);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            arrayOfString1.enforceInterface("com.android.ims.internal.IImsCallSession");
            if (arrayOfString1.readInt() != 0) {
              ImsStreamMediaProfile imsStreamMediaProfile = (ImsStreamMediaProfile)ImsStreamMediaProfile.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              arrayOfString1 = null;
            } 
            hold((ImsStreamMediaProfile)arrayOfString1);
            param1Parcel2.writeNoException();
            return true;
          case 18:
            arrayOfString1.enforceInterface("com.android.ims.internal.IImsCallSession");
            j = arrayOfString1.readInt();
            terminate(j);
            param1Parcel2.writeNoException();
            return true;
          case 17:
            arrayOfString1.enforceInterface("com.android.ims.internal.IImsCallSession");
            iImsCallSession = asInterface(arrayOfString1.readStrongBinder());
            consultativeTransfer(iImsCallSession);
            param1Parcel2.writeNoException();
            return true;
          case 16:
            iImsCallSession.enforceInterface("com.android.ims.internal.IImsCallSession");
            str7 = iImsCallSession.readString();
            bool4 = bool3;
            if (iImsCallSession.readInt() != 0)
              bool4 = true; 
            transfer(str7, bool4);
            param1Parcel2.writeNoException();
            return true;
          case 15:
            iImsCallSession.enforceInterface("com.android.ims.internal.IImsCallSession");
            j = iImsCallSession.readInt();
            reject(j);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            iImsCallSession.enforceInterface("com.android.ims.internal.IImsCallSession");
            str3 = iImsCallSession.readString();
            deflect(str3);
            param1Parcel2.writeNoException();
            return true;
          case 13:
            str3.enforceInterface("com.android.ims.internal.IImsCallSession");
            j = str3.readInt();
            if (str3.readInt() != 0) {
              ImsStreamMediaProfile imsStreamMediaProfile = (ImsStreamMediaProfile)ImsStreamMediaProfile.CREATOR.createFromParcel((Parcel)str3);
            } else {
              str3 = null;
            } 
            accept(j, (ImsStreamMediaProfile)str3);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            str3.enforceInterface("com.android.ims.internal.IImsCallSession");
            arrayOfString2 = str3.createStringArray();
            if (str3.readInt() != 0) {
              ImsCallProfile imsCallProfile1 = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)str3);
            } else {
              str3 = null;
            } 
            startConference(arrayOfString2, (ImsCallProfile)str3);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            str3.enforceInterface("com.android.ims.internal.IImsCallSession");
            str6 = str3.readString();
            if (str3.readInt() != 0) {
              ImsCallProfile imsCallProfile1 = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel((Parcel)str3);
            } else {
              str3 = null;
            } 
            start(str6, (ImsCallProfile)str3);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            str3.enforceInterface("com.android.ims.internal.IImsCallSession");
            if (str3.readInt() != 0)
              bool4 = true; 
            setMute(bool4);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            str3.enforceInterface("com.android.ims.internal.IImsCallSession");
            iImsCallSessionListener = IImsCallSessionListener.Stub.asInterface(str3.readStrongBinder());
            setListener(iImsCallSessionListener);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            iImsCallSessionListener.enforceInterface("com.android.ims.internal.IImsCallSession");
            bool1 = isInCall();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 7:
            iImsCallSessionListener.enforceInterface("com.android.ims.internal.IImsCallSession");
            i = getState();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 6:
            iImsCallSessionListener.enforceInterface("com.android.ims.internal.IImsCallSession");
            str2 = iImsCallSessionListener.readString();
            str2 = getProperty(str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str2);
            return true;
          case 5:
            str2.enforceInterface("com.android.ims.internal.IImsCallSession");
            imsCallProfile = getRemoteCallProfile();
            param1Parcel2.writeNoException();
            if (imsCallProfile != null) {
              param1Parcel2.writeInt(1);
              imsCallProfile.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 4:
            imsCallProfile.enforceInterface("com.android.ims.internal.IImsCallSession");
            imsCallProfile = getLocalCallProfile();
            param1Parcel2.writeNoException();
            if (imsCallProfile != null) {
              param1Parcel2.writeInt(1);
              imsCallProfile.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 3:
            imsCallProfile.enforceInterface("com.android.ims.internal.IImsCallSession");
            imsCallProfile = getCallProfile();
            param1Parcel2.writeNoException();
            if (imsCallProfile != null) {
              param1Parcel2.writeInt(1);
              imsCallProfile.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 2:
            imsCallProfile.enforceInterface("com.android.ims.internal.IImsCallSession");
            str1 = getCallId();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("com.android.ims.internal.IImsCallSession");
        close();
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("com.android.ims.internal.IImsCallSession");
      return true;
    }
    
    private static class Proxy implements IImsCallSession {
      public static IImsCallSession sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.ims.internal.IImsCallSession";
      }
      
      public void close() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null) {
            IImsCallSession.Stub.getDefaultImpl().close();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getCallId() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null)
            return IImsCallSession.Stub.getDefaultImpl().getCallId(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ImsCallProfile getCallProfile() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ImsCallProfile imsCallProfile;
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null) {
            imsCallProfile = IImsCallSession.Stub.getDefaultImpl().getCallProfile();
            return imsCallProfile;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel(parcel2);
          } else {
            imsCallProfile = null;
          } 
          return imsCallProfile;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ImsCallProfile getLocalCallProfile() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ImsCallProfile imsCallProfile;
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null) {
            imsCallProfile = IImsCallSession.Stub.getDefaultImpl().getLocalCallProfile();
            return imsCallProfile;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel(parcel2);
          } else {
            imsCallProfile = null;
          } 
          return imsCallProfile;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ImsCallProfile getRemoteCallProfile() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ImsCallProfile imsCallProfile;
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null) {
            imsCallProfile = IImsCallSession.Stub.getDefaultImpl().getRemoteCallProfile();
            return imsCallProfile;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            imsCallProfile = (ImsCallProfile)ImsCallProfile.CREATOR.createFromParcel(parcel2);
          } else {
            imsCallProfile = null;
          } 
          return imsCallProfile;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getProperty(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null) {
            param2String = IImsCallSession.Stub.getDefaultImpl().getProperty(param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null)
            return IImsCallSession.Stub.getDefaultImpl().getState(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isInCall() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(8, parcel1, parcel2, 0);
          if (!bool2 && IImsCallSession.Stub.getDefaultImpl() != null) {
            bool1 = IImsCallSession.Stub.getDefaultImpl().isInCall();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setListener(IImsCallSessionListener param2IImsCallSessionListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          if (param2IImsCallSessionListener != null) {
            iBinder = param2IImsCallSessionListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null) {
            IImsCallSession.Stub.getDefaultImpl().setListener(param2IImsCallSessionListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setMute(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool1 && IImsCallSession.Stub.getDefaultImpl() != null) {
            IImsCallSession.Stub.getDefaultImpl().setMute(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void start(String param2String, ImsCallProfile param2ImsCallProfile) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          parcel1.writeString(param2String);
          if (param2ImsCallProfile != null) {
            parcel1.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null) {
            IImsCallSession.Stub.getDefaultImpl().start(param2String, param2ImsCallProfile);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startConference(String[] param2ArrayOfString, ImsCallProfile param2ImsCallProfile) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          parcel1.writeStringArray(param2ArrayOfString);
          if (param2ImsCallProfile != null) {
            parcel1.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null) {
            IImsCallSession.Stub.getDefaultImpl().startConference(param2ArrayOfString, param2ImsCallProfile);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void accept(int param2Int, ImsStreamMediaProfile param2ImsStreamMediaProfile) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          parcel1.writeInt(param2Int);
          if (param2ImsStreamMediaProfile != null) {
            parcel1.writeInt(1);
            param2ImsStreamMediaProfile.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null) {
            IImsCallSession.Stub.getDefaultImpl().accept(param2Int, param2ImsStreamMediaProfile);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deflect(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null) {
            IImsCallSession.Stub.getDefaultImpl().deflect(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reject(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null) {
            IImsCallSession.Stub.getDefaultImpl().reject(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void transfer(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool1 && IImsCallSession.Stub.getDefaultImpl() != null) {
            IImsCallSession.Stub.getDefaultImpl().transfer(param2String, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void consultativeTransfer(IImsCallSession param2IImsCallSession) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          if (param2IImsCallSession != null) {
            iBinder = param2IImsCallSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null) {
            IImsCallSession.Stub.getDefaultImpl().consultativeTransfer(param2IImsCallSession);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void terminate(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null) {
            IImsCallSession.Stub.getDefaultImpl().terminate(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void hold(ImsStreamMediaProfile param2ImsStreamMediaProfile) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          if (param2ImsStreamMediaProfile != null) {
            parcel1.writeInt(1);
            param2ImsStreamMediaProfile.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null) {
            IImsCallSession.Stub.getDefaultImpl().hold(param2ImsStreamMediaProfile);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resume(ImsStreamMediaProfile param2ImsStreamMediaProfile) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          if (param2ImsStreamMediaProfile != null) {
            parcel1.writeInt(1);
            param2ImsStreamMediaProfile.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null) {
            IImsCallSession.Stub.getDefaultImpl().resume(param2ImsStreamMediaProfile);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void merge() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null) {
            IImsCallSession.Stub.getDefaultImpl().merge();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void update(int param2Int, ImsStreamMediaProfile param2ImsStreamMediaProfile) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          parcel1.writeInt(param2Int);
          if (param2ImsStreamMediaProfile != null) {
            parcel1.writeInt(1);
            param2ImsStreamMediaProfile.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null) {
            IImsCallSession.Stub.getDefaultImpl().update(param2Int, param2ImsStreamMediaProfile);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void extendToConference(String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null) {
            IImsCallSession.Stub.getDefaultImpl().extendToConference(param2ArrayOfString);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void inviteParticipants(String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null) {
            IImsCallSession.Stub.getDefaultImpl().inviteParticipants(param2ArrayOfString);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeParticipants(String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null) {
            IImsCallSession.Stub.getDefaultImpl().removeParticipants(param2ArrayOfString);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendDtmf(char param2Char, Message param2Message) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          parcel1.writeInt(param2Char);
          if (param2Message != null) {
            parcel1.writeInt(1);
            param2Message.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null) {
            IImsCallSession.Stub.getDefaultImpl().sendDtmf(param2Char, param2Message);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startDtmf(char param2Char) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          parcel1.writeInt(param2Char);
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null) {
            IImsCallSession.Stub.getDefaultImpl().startDtmf(param2Char);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopDtmf() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null) {
            IImsCallSession.Stub.getDefaultImpl().stopDtmf();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendUssd(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null) {
            IImsCallSession.Stub.getDefaultImpl().sendUssd(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IImsVideoCallProvider getVideoCallProvider() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null)
            return IImsCallSession.Stub.getDefaultImpl().getVideoCallProvider(); 
          parcel2.readException();
          return IImsVideoCallProvider.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isMultiparty() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(31, parcel1, parcel2, 0);
          if (!bool2 && IImsCallSession.Stub.getDefaultImpl() != null) {
            bool1 = IImsCallSession.Stub.getDefaultImpl().isMultiparty();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendRttModifyRequest(ImsCallProfile param2ImsCallProfile) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          if (param2ImsCallProfile != null) {
            parcel1.writeInt(1);
            param2ImsCallProfile.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null) {
            IImsCallSession.Stub.getDefaultImpl().sendRttModifyRequest(param2ImsCallProfile);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendRttModifyResponse(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool1 && IImsCallSession.Stub.getDefaultImpl() != null) {
            IImsCallSession.Stub.getDefaultImpl().sendRttModifyResponse(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendRttMessage(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.ims.internal.IImsCallSession");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool && IImsCallSession.Stub.getDefaultImpl() != null) {
            IImsCallSession.Stub.getDefaultImpl().sendRttMessage(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IImsCallSession param1IImsCallSession) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IImsCallSession != null) {
          Proxy.sDefaultImpl = param1IImsCallSession;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IImsCallSession getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
