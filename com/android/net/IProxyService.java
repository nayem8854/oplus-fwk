package com.android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IProxyService extends IInterface {
  String resolvePacFile(String paramString1, String paramString2) throws RemoteException;
  
  void setPacFile(String paramString) throws RemoteException;
  
  void startPacSystem() throws RemoteException;
  
  void stopPacSystem() throws RemoteException;
  
  class Default implements IProxyService {
    public String resolvePacFile(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public void setPacFile(String param1String) throws RemoteException {}
    
    public void startPacSystem() throws RemoteException {}
    
    public void stopPacSystem() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IProxyService {
    private static final String DESCRIPTOR = "com.android.net.IProxyService";
    
    static final int TRANSACTION_resolvePacFile = 1;
    
    static final int TRANSACTION_setPacFile = 2;
    
    static final int TRANSACTION_startPacSystem = 3;
    
    static final int TRANSACTION_stopPacSystem = 4;
    
    public Stub() {
      attachInterface(this, "com.android.net.IProxyService");
    }
    
    public static IProxyService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.net.IProxyService");
      if (iInterface != null && iInterface instanceof IProxyService)
        return (IProxyService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "stopPacSystem";
          } 
          return "startPacSystem";
        } 
        return "setPacFile";
      } 
      return "resolvePacFile";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("com.android.net.IProxyService");
              return true;
            } 
            param1Parcel1.enforceInterface("com.android.net.IProxyService");
            stopPacSystem();
            return true;
          } 
          param1Parcel1.enforceInterface("com.android.net.IProxyService");
          startPacSystem();
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.net.IProxyService");
        str1 = param1Parcel1.readString();
        setPacFile(str1);
        return true;
      } 
      str1.enforceInterface("com.android.net.IProxyService");
      String str2 = str1.readString();
      String str1 = str1.readString();
      str1 = resolvePacFile(str2, str1);
      param1Parcel2.writeNoException();
      param1Parcel2.writeString(str1);
      return true;
    }
    
    private static class Proxy implements IProxyService {
      public static IProxyService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.net.IProxyService";
      }
      
      public String resolvePacFile(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.net.IProxyService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IProxyService.Stub.getDefaultImpl() != null) {
            param2String1 = IProxyService.Stub.getDefaultImpl().resolvePacFile(param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPacFile(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.net.IProxyService");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IProxyService.Stub.getDefaultImpl() != null) {
            IProxyService.Stub.getDefaultImpl().setPacFile(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void startPacSystem() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.net.IProxyService");
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IProxyService.Stub.getDefaultImpl() != null) {
            IProxyService.Stub.getDefaultImpl().startPacSystem();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void stopPacSystem() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.net.IProxyService");
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IProxyService.Stub.getDefaultImpl() != null) {
            IProxyService.Stub.getDefaultImpl().stopPacSystem();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IProxyService param1IProxyService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IProxyService != null) {
          Proxy.sDefaultImpl = param1IProxyService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IProxyService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
