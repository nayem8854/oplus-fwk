package com.android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IProxyPortListener extends IInterface {
  void setProxyPort(int paramInt) throws RemoteException;
  
  class Default implements IProxyPortListener {
    public void setProxyPort(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IProxyPortListener {
    private static final String DESCRIPTOR = "com.android.net.IProxyPortListener";
    
    static final int TRANSACTION_setProxyPort = 1;
    
    public Stub() {
      attachInterface(this, "com.android.net.IProxyPortListener");
    }
    
    public static IProxyPortListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.net.IProxyPortListener");
      if (iInterface != null && iInterface instanceof IProxyPortListener)
        return (IProxyPortListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "setProxyPort";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.net.IProxyPortListener");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.net.IProxyPortListener");
      param1Int1 = param1Parcel1.readInt();
      setProxyPort(param1Int1);
      return true;
    }
    
    private static class Proxy implements IProxyPortListener {
      public static IProxyPortListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.net.IProxyPortListener";
      }
      
      public void setProxyPort(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.net.IProxyPortListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IProxyPortListener.Stub.getDefaultImpl() != null) {
            IProxyPortListener.Stub.getDefaultImpl().setProxyPort(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IProxyPortListener param1IProxyPortListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IProxyPortListener != null) {
          Proxy.sDefaultImpl = param1IProxyPortListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IProxyPortListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
