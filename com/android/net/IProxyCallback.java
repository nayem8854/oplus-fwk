package com.android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IProxyCallback extends IInterface {
  void getProxyPort(IBinder paramIBinder) throws RemoteException;
  
  class Default implements IProxyCallback {
    public void getProxyPort(IBinder param1IBinder) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IProxyCallback {
    private static final String DESCRIPTOR = "com.android.net.IProxyCallback";
    
    static final int TRANSACTION_getProxyPort = 1;
    
    public Stub() {
      attachInterface(this, "com.android.net.IProxyCallback");
    }
    
    public static IProxyCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.net.IProxyCallback");
      if (iInterface != null && iInterface instanceof IProxyCallback)
        return (IProxyCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "getProxyPort";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.net.IProxyCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.net.IProxyCallback");
      IBinder iBinder = param1Parcel1.readStrongBinder();
      getProxyPort(iBinder);
      return true;
    }
    
    private static class Proxy implements IProxyCallback {
      public static IProxyCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.net.IProxyCallback";
      }
      
      public void getProxyPort(IBinder param2IBinder) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.net.IProxyCallback");
          parcel.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IProxyCallback.Stub.getDefaultImpl() != null) {
            IProxyCallback.Stub.getDefaultImpl().getProxyPort(param2IBinder);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IProxyCallback param1IProxyCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IProxyCallback != null) {
          Proxy.sDefaultImpl = param1IProxyCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IProxyCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
