package com.android.net.module.util;

import android.text.TextUtils;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.util.ArrayList;
import java.util.List;

public abstract class DnsPacket {
  public static final int ANSECTION = 1;
  
  public static final int ARSECTION = 3;
  
  public static final int NSSECTION = 2;
  
  private static final int NUM_SECTIONS = 4;
  
  public static final int QDSECTION = 0;
  
  public static class ParseException extends RuntimeException {
    public String reason;
    
    public ParseException(String param1String) {
      super(param1String);
      this.reason = param1String;
    }
    
    public ParseException(String param1String, Throwable param1Throwable) {
      super(param1String, param1Throwable);
      this.reason = param1String;
    }
  }
  
  public class DnsHeader {
    private static final String TAG = "DnsHeader";
    
    public final int flags;
    
    public final int id;
    
    private final int[] mRecordCount;
    
    public final int rcode;
    
    final DnsPacket this$0;
    
    DnsHeader(ByteBuffer param1ByteBuffer) throws BufferUnderflowException {
      this.id = Short.toUnsignedInt(param1ByteBuffer.getShort());
      int i = Short.toUnsignedInt(param1ByteBuffer.getShort());
      this.rcode = i & 0xF;
      this.mRecordCount = new int[4];
      for (i = 0; i < 4; i++)
        this.mRecordCount[i] = Short.toUnsignedInt(param1ByteBuffer.getShort()); 
    }
    
    public int getRecordCount(int param1Int) {
      return this.mRecordCount[param1Int];
    }
  }
  
  public class DnsRecord {
    private static final int MAXLABELCOUNT = 128;
    
    private static final int MAXLABELSIZE = 63;
    
    private static final int MAXNAMESIZE = 255;
    
    private static final int NAME_COMPRESSION = 192;
    
    private static final int NAME_NORMAL = 0;
    
    private static final String TAG = "DnsRecord";
    
    public final String dName;
    
    private final DecimalFormat mByteFormat = new DecimalFormat();
    
    private final FieldPosition mPos = new FieldPosition(0);
    
    private final byte[] mRdata;
    
    public final int nsClass;
    
    public final int nsType;
    
    final DnsPacket this$0;
    
    public final long ttl;
    
    DnsRecord(int param1Int, ByteBuffer param1ByteBuffer) throws BufferUnderflowException, DnsPacket.ParseException {
      String str1 = parseName(param1ByteBuffer, 0);
      if (str1.length() <= 255) {
        this.nsType = Short.toUnsignedInt(param1ByteBuffer.getShort());
        this.nsClass = Short.toUnsignedInt(param1ByteBuffer.getShort());
        if (param1Int != 0) {
          this.ttl = Integer.toUnsignedLong(param1ByteBuffer.getInt());
          param1Int = Short.toUnsignedInt(param1ByteBuffer.getShort());
          byte[] arrayOfByte = new byte[param1Int];
          param1ByteBuffer.get(arrayOfByte);
        } else {
          this.ttl = 0L;
          this.mRdata = null;
        } 
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Parse name fail, name size is too long: ");
      String str2 = this.dName;
      stringBuilder.append(str2.length());
      throw new DnsPacket.ParseException(stringBuilder.toString());
    }
    
    public byte[] getRR() {
      byte[] arrayOfByte = this.mRdata;
      if (arrayOfByte == null) {
        arrayOfByte = null;
      } else {
        arrayOfByte = (byte[])arrayOfByte.clone();
      } 
      return arrayOfByte;
    }
    
    private String labelToString(byte[] param1ArrayOfbyte) {
      StringBuffer stringBuffer = new StringBuffer();
      for (byte b = 0; b < param1ArrayOfbyte.length; b++) {
        int i = Byte.toUnsignedInt(param1ArrayOfbyte[b]);
        if (i <= 32 || i >= 127) {
          stringBuffer.append('\\');
          this.mByteFormat.format(i, stringBuffer, this.mPos);
        } else if (i == 34 || i == 46 || i == 59 || i == 92 || i == 40 || i == 41 || i == 64 || i == 36) {
          stringBuffer.append('\\');
          stringBuffer.append((char)i);
        } else {
          stringBuffer.append((char)i);
        } 
      } 
      return stringBuffer.toString();
    }
    
    private String parseName(ByteBuffer param1ByteBuffer, int param1Int) throws BufferUnderflowException, DnsPacket.ParseException {
      if (param1Int <= 128) {
        int i = Byte.toUnsignedInt(param1ByteBuffer.get());
        int j = i & 0xC0;
        if (i == 0)
          return ""; 
        if (j == 0 || j == 192) {
          if (j == 192) {
            i = ((i & 0xFFFFFF3F) << 8) + Byte.toUnsignedInt(param1ByteBuffer.get());
            j = param1ByteBuffer.position();
            if (i < j - 2) {
              param1ByteBuffer.position(i);
              String str1 = parseName(param1ByteBuffer, param1Int + 1);
              param1ByteBuffer.position(j);
              return str1;
            } 
            throw new DnsPacket.ParseException("Parse compression name fail, invalid compression");
          } 
          byte[] arrayOfByte = new byte[i];
          param1ByteBuffer.get(arrayOfByte);
          String str = labelToString(arrayOfByte);
          if (str.length() <= 63) {
            String str1 = parseName(param1ByteBuffer, param1Int + 1);
            if (TextUtils.isEmpty(str1)) {
              str1 = str;
            } else {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append(str);
              stringBuilder.append(".");
              stringBuilder.append(str1);
              str1 = stringBuilder.toString();
            } 
            return str1;
          } 
          throw new DnsPacket.ParseException("Parse name fail, invalid label length");
        } 
        throw new DnsPacket.ParseException("Parse name fail, bad label type");
      } 
      throw new DnsPacket.ParseException("Failed to parse name, too many labels");
    }
  }
  
  private static final String TAG = DnsPacket.class.getSimpleName();
  
  protected final DnsHeader mHeader;
  
  protected final List<DnsRecord>[] mRecords;
  
  protected DnsPacket(byte[] paramArrayOfbyte) throws ParseException {
    if (paramArrayOfbyte != null)
      try {
        ByteBuffer byteBuffer = ByteBuffer.wrap(paramArrayOfbyte);
        DnsHeader dnsHeader = new DnsHeader();
        this(this, byteBuffer);
        this.mHeader = dnsHeader;
        this.mRecords = (List<DnsRecord>[])new ArrayList[4];
        for (byte b = 0; b < 4; b++) {
          int i = this.mHeader.getRecordCount(b);
          if (i > 0)
            this.mRecords[b] = new ArrayList<>(i); 
          for (byte b1 = 0; b1 < i;) {
            try {
              List<DnsRecord> list = this.mRecords[b];
              DnsRecord dnsRecord = new DnsRecord();
              this(this, b, byteBuffer);
              list.add(dnsRecord);
              b1++;
            } catch (BufferUnderflowException bufferUnderflowException) {
              throw new ParseException("Parse record fail", bufferUnderflowException);
            } 
          } 
        } 
        return;
      } catch (BufferUnderflowException bufferUnderflowException) {
        throw new ParseException("Parse Header fail, bad input data", bufferUnderflowException);
      }  
    throw new ParseException("Parse header failed, null input data");
  }
}
