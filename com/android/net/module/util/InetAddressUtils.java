package com.android.net.module.util;

import android.os.Parcel;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class InetAddressUtils {
  public static void parcelInetAddress(Parcel paramParcel, InetAddress paramInetAddress, int paramInt) {
    if (paramInetAddress != null) {
      byte[] arrayOfByte = paramInetAddress.getAddress();
    } else {
      paramInetAddress = null;
    } 
    paramParcel.writeByteArray((byte[])paramInetAddress);
  }
  
  public static InetAddress unparcelInetAddress(Parcel paramParcel) {
    byte[] arrayOfByte = paramParcel.createByteArray();
    if (arrayOfByte == null)
      return null; 
    try {
      return InetAddress.getByAddress(arrayOfByte);
    } catch (UnknownHostException unknownHostException) {
      return null;
    } 
  }
}
