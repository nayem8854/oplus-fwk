package com.android.net.module.util;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Inet4AddressUtils {
  public static Inet4Address intToInet4AddressHTL(int paramInt) {
    return intToInet4AddressHTH(Integer.reverseBytes(paramInt));
  }
  
  public static Inet4Address intToInet4AddressHTH(int paramInt) {
    byte b1 = (byte)(paramInt >> 24 & 0xFF), b2 = (byte)(paramInt >> 16 & 0xFF), b3 = (byte)(paramInt >> 8 & 0xFF), b4 = (byte)(paramInt & 0xFF);
    try {
      return (Inet4Address)InetAddress.getByAddress(new byte[] { b1, b2, b3, b4 });
    } catch (UnknownHostException unknownHostException) {
      throw new AssertionError();
    } 
  }
  
  public static int inet4AddressToIntHTH(Inet4Address paramInet4Address) throws IllegalArgumentException {
    byte[] arrayOfByte = paramInet4Address.getAddress();
    return (arrayOfByte[0] & 0xFF) << 24 | (arrayOfByte[1] & 0xFF) << 16 | (arrayOfByte[2] & 0xFF) << 8 | arrayOfByte[3] & 0xFF;
  }
  
  public static int inet4AddressToIntHTL(Inet4Address paramInet4Address) {
    return Integer.reverseBytes(inet4AddressToIntHTH(paramInet4Address));
  }
  
  public static int prefixLengthToV4NetmaskIntHTH(int paramInt) throws IllegalArgumentException {
    if (paramInt >= 0 && paramInt <= 32) {
      if (paramInt == 0) {
        paramInt = 0;
      } else {
        paramInt = -1 << 32 - paramInt;
      } 
      return paramInt;
    } 
    throw new IllegalArgumentException("Invalid prefix length (0 <= prefix <= 32)");
  }
  
  public static int prefixLengthToV4NetmaskIntHTL(int paramInt) throws IllegalArgumentException {
    return Integer.reverseBytes(prefixLengthToV4NetmaskIntHTH(paramInt));
  }
  
  public static int netmaskToPrefixLength(Inet4Address paramInet4Address) {
    int i = inet4AddressToIntHTH(paramInet4Address);
    int j = Integer.bitCount(i);
    int k = Integer.numberOfTrailingZeros(i);
    if (k == 32 - j)
      return j; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Non-contiguous netmask: ");
    stringBuilder.append(Integer.toHexString(i));
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static int getImplicitNetmask(Inet4Address paramInet4Address) {
    int i = paramInet4Address.getAddress()[0] & 0xFF;
    if (i < 128)
      return 8; 
    if (i < 192)
      return 16; 
    if (i < 224)
      return 24; 
    return 32;
  }
  
  public static Inet4Address getBroadcastAddress(Inet4Address paramInet4Address, int paramInt) throws IllegalArgumentException {
    int i = inet4AddressToIntHTH(paramInet4Address);
    paramInt = prefixLengthToV4NetmaskIntHTH(paramInt);
    return intToInet4AddressHTH(i | paramInt ^ 0xFFFFFFFF);
  }
  
  public static Inet4Address getPrefixMaskAsInet4Address(int paramInt) throws IllegalArgumentException {
    return intToInet4AddressHTH(prefixLengthToV4NetmaskIntHTH(paramInt));
  }
}
