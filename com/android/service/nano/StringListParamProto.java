package com.android.service.nano;

import com.android.framework.protobuf.nano.CodedInputByteBufferNano;
import com.android.framework.protobuf.nano.CodedOutputByteBufferNano;
import com.android.framework.protobuf.nano.InternalNano;
import com.android.framework.protobuf.nano.InvalidProtocolBufferNanoException;
import com.android.framework.protobuf.nano.MessageNano;
import com.android.framework.protobuf.nano.WireFormatNano;
import java.io.IOException;

public final class StringListParamProto extends MessageNano {
  private static volatile StringListParamProto[] _emptyArray;
  
  public String[] element;
  
  public static StringListParamProto[] emptyArray() {
    if (_emptyArray == null)
      synchronized (InternalNano.LAZY_INIT_LOCK) {
        if (_emptyArray == null)
          _emptyArray = new StringListParamProto[0]; 
      }  
    return _emptyArray;
  }
  
  public StringListParamProto() {
    clear();
  }
  
  public StringListParamProto clear() {
    this.element = WireFormatNano.EMPTY_STRING_ARRAY;
    this.cachedSize = -1;
    return this;
  }
  
  public void writeTo(CodedOutputByteBufferNano paramCodedOutputByteBufferNano) throws IOException {
    String[] arrayOfString = this.element;
    if (arrayOfString != null && arrayOfString.length > 0) {
      byte b = 0;
      while (true) {
        arrayOfString = this.element;
        if (b < arrayOfString.length) {
          String str = arrayOfString[b];
          if (str != null)
            paramCodedOutputByteBufferNano.writeString(1, str); 
          b++;
          continue;
        } 
        break;
      } 
    } 
    super.writeTo(paramCodedOutputByteBufferNano);
  }
  
  protected int computeSerializedSize() {
    int i = super.computeSerializedSize();
    String[] arrayOfString = this.element;
    int j = i;
    if (arrayOfString != null) {
      j = i;
      if (arrayOfString.length > 0) {
        int k = 0;
        int m = 0;
        j = 0;
        while (true) {
          arrayOfString = this.element;
          if (j < arrayOfString.length) {
            String str = arrayOfString[j];
            int n = k, i1 = m;
            if (str != null) {
              n = k + 1;
              i1 = m + CodedOutputByteBufferNano.computeStringSizeNoTag(str);
            } 
            j++;
            k = n;
            m = i1;
            continue;
          } 
          break;
        } 
        j = i + m + k * 1;
      } 
    } 
    return j;
  }
  
  public StringListParamProto mergeFrom(CodedInputByteBufferNano paramCodedInputByteBufferNano) throws IOException {
    while (true) {
      int i = paramCodedInputByteBufferNano.readTag();
      if (i != 0) {
        if (i != 10) {
          if (!WireFormatNano.parseUnknownField(paramCodedInputByteBufferNano, i))
            return this; 
          continue;
        } 
        int j = WireFormatNano.getRepeatedFieldArrayLength(paramCodedInputByteBufferNano, 10);
        String[] arrayOfString = this.element;
        if (arrayOfString == null) {
          i = 0;
        } else {
          i = arrayOfString.length;
        } 
        arrayOfString = new String[i + j];
        j = i;
        if (i != 0) {
          System.arraycopy(this.element, 0, arrayOfString, 0, i);
          j = i;
        } 
        for (; j < arrayOfString.length - 1; j++) {
          arrayOfString[j] = paramCodedInputByteBufferNano.readString();
          paramCodedInputByteBufferNano.readTag();
        } 
        arrayOfString[j] = paramCodedInputByteBufferNano.readString();
        this.element = arrayOfString;
        continue;
      } 
      break;
    } 
    return this;
  }
  
  public static StringListParamProto parseFrom(byte[] paramArrayOfbyte) throws InvalidProtocolBufferNanoException {
    return (StringListParamProto)MessageNano.mergeFrom(new StringListParamProto(), paramArrayOfbyte);
  }
  
  public static StringListParamProto parseFrom(CodedInputByteBufferNano paramCodedInputByteBufferNano) throws IOException {
    return (new StringListParamProto()).mergeFrom(paramCodedInputByteBufferNano);
  }
}
