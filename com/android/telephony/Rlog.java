package com.android.telephony;

import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import com.android.internal.telephony.util.TelephonyUtils;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class Rlog {
  private static final boolean USER_BUILD = TelephonyUtils.IS_USER;
  
  private static int log(int paramInt, String paramString1, String paramString2) {
    return Log.logToRadioBuffer(paramInt, paramString1, paramString2);
  }
  
  public static int v(String paramString1, String paramString2) {
    return log(2, paramString1, paramString2);
  }
  
  public static int v(String paramString1, String paramString2, Throwable paramThrowable) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString2);
    stringBuilder.append('\n');
    stringBuilder.append(Log.getStackTraceString(paramThrowable));
    paramString2 = stringBuilder.toString();
    return log(2, paramString1, paramString2);
  }
  
  public static int d(String paramString1, String paramString2) {
    return log(3, paramString1, paramString2);
  }
  
  public static int d(String paramString1, String paramString2, Throwable paramThrowable) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString2);
    stringBuilder.append('\n');
    stringBuilder.append(Log.getStackTraceString(paramThrowable));
    paramString2 = stringBuilder.toString();
    return log(3, paramString1, paramString2);
  }
  
  public static int i(String paramString1, String paramString2) {
    return log(4, paramString1, paramString2);
  }
  
  public static int i(String paramString1, String paramString2, Throwable paramThrowable) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString2);
    stringBuilder.append('\n');
    stringBuilder.append(Log.getStackTraceString(paramThrowable));
    paramString2 = stringBuilder.toString();
    return log(4, paramString1, paramString2);
  }
  
  public static int w(String paramString1, String paramString2) {
    return log(5, paramString1, paramString2);
  }
  
  public static int w(String paramString1, String paramString2, Throwable paramThrowable) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString2);
    stringBuilder.append('\n');
    stringBuilder.append(Log.getStackTraceString(paramThrowable));
    paramString2 = stringBuilder.toString();
    return log(5, paramString1, paramString2);
  }
  
  public static int w(String paramString, Throwable paramThrowable) {
    return log(5, paramString, Log.getStackTraceString(paramThrowable));
  }
  
  public static int e(String paramString1, String paramString2) {
    return log(6, paramString1, paramString2);
  }
  
  public static int e(String paramString1, String paramString2, Throwable paramThrowable) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString2);
    stringBuilder.append('\n');
    stringBuilder.append(Log.getStackTraceString(paramThrowable));
    paramString2 = stringBuilder.toString();
    return log(6, paramString1, paramString2);
  }
  
  public static int println(int paramInt, String paramString1, String paramString2) {
    return log(paramInt, paramString1, paramString2);
  }
  
  public static boolean isLoggable(String paramString, int paramInt) {
    return Log.isLoggable(paramString, paramInt);
  }
  
  public static String pii(String paramString, Object paramObject) {
    String str = String.valueOf(paramObject);
    if (paramObject == null || TextUtils.isEmpty(str) || isLoggable(paramString, 2))
      return str; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[");
    stringBuilder.append(secureHash(str.getBytes()));
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public static String pii(boolean paramBoolean, Object paramObject) {
    String str = String.valueOf(paramObject);
    if (paramObject == null || TextUtils.isEmpty(str) || paramBoolean)
      return str; 
    paramObject = new StringBuilder();
    paramObject.append("[");
    paramObject.append(secureHash(str.getBytes()));
    paramObject.append("]");
    return paramObject.toString();
  }
  
  private static String secureHash(byte[] paramArrayOfbyte) {
    if (USER_BUILD)
      return "****"; 
    try {
      MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
      paramArrayOfbyte = messageDigest.digest(paramArrayOfbyte);
      return Base64.encodeToString(paramArrayOfbyte, 11);
    } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
      return "####";
    } 
  }
}
