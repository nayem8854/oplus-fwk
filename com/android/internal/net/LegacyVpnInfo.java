package com.android.internal.net;

import android.app.PendingIntent;
import android.net.NetworkInfo;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class LegacyVpnInfo implements Parcelable {
  public int state = -1;
  
  public String key;
  
  public PendingIntent intent;
  
  private static final String TAG = "LegacyVpnInfo";
  
  public static final int STATE_TIMEOUT = 4;
  
  public static final int STATE_INITIALIZING = 1;
  
  public static final int STATE_FAILED = 5;
  
  public static final int STATE_DISCONNECTED = 0;
  
  public static final int STATE_CONNECTING = 2;
  
  public static final int STATE_CONNECTED = 3;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.key);
    paramParcel.writeInt(this.state);
    paramParcel.writeParcelable((Parcelable)this.intent, paramInt);
  }
  
  public static final Parcelable.Creator<LegacyVpnInfo> CREATOR = new Parcelable.Creator<LegacyVpnInfo>() {
      public LegacyVpnInfo createFromParcel(Parcel param1Parcel) {
        LegacyVpnInfo legacyVpnInfo = new LegacyVpnInfo();
        legacyVpnInfo.key = param1Parcel.readString();
        legacyVpnInfo.state = param1Parcel.readInt();
        legacyVpnInfo.intent = (PendingIntent)param1Parcel.readParcelable(null);
        return legacyVpnInfo;
      }
      
      public LegacyVpnInfo[] newArray(int param1Int) {
        return new LegacyVpnInfo[param1Int];
      }
    };
  
  public static int stateFromNetworkInfo(NetworkInfo paramNetworkInfo) {
    int i = null.$SwitchMap$android$net$NetworkInfo$DetailedState[paramNetworkInfo.getDetailedState().ordinal()];
    if (i != 1) {
      if (i != 2) {
        if (i != 3) {
          if (i != 4) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Unhandled state ");
            stringBuilder.append(paramNetworkInfo.getDetailedState());
            stringBuilder.append(" ; treating as disconnected");
            Log.w("LegacyVpnInfo", stringBuilder.toString());
            return 0;
          } 
          return 5;
        } 
        return 0;
      } 
      return 3;
    } 
    return 2;
  }
}
