package com.android.internal.net;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

public class VpnInfo implements Parcelable {
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("VpnInfo{ownerUid=");
    stringBuilder.append(this.ownerUid);
    stringBuilder.append(", vpnIface='");
    stringBuilder.append(this.vpnIface);
    stringBuilder.append('\'');
    stringBuilder.append(", underlyingIfaces='");
    String[] arrayOfString = this.underlyingIfaces;
    stringBuilder.append(Arrays.toString((Object[])arrayOfString));
    stringBuilder.append('\'');
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.ownerUid);
    paramParcel.writeString(this.vpnIface);
    paramParcel.writeStringArray(this.underlyingIfaces);
  }
  
  public static final Parcelable.Creator<VpnInfo> CREATOR = new Parcelable.Creator<VpnInfo>() {
      public VpnInfo createFromParcel(Parcel param1Parcel) {
        VpnInfo vpnInfo = new VpnInfo();
        vpnInfo.ownerUid = param1Parcel.readInt();
        vpnInfo.vpnIface = param1Parcel.readString();
        vpnInfo.underlyingIfaces = param1Parcel.readStringArray();
        return vpnInfo;
      }
      
      public VpnInfo[] newArray(int param1Int) {
        return new VpnInfo[param1Int];
      }
    };
  
  public int ownerUid;
  
  public String[] underlyingIfaces;
  
  public String vpnIface;
}
