package com.android.internal.net;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.net.IpPrefix;
import android.net.LinkAddress;
import android.net.Network;
import android.net.ProxyInfo;
import android.net.RouteInfo;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.UserHandle;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VpnConfig implements Parcelable {
  public static Intent getIntentForConfirmation() {
    Intent intent = new Intent();
    String str = Resources.getSystem().getString(17039865);
    ComponentName componentName = ComponentName.unflattenFromString(str);
    intent.setClassName(componentName.getPackageName(), componentName.getClassName());
    return intent;
  }
  
  public static PendingIntent getIntentForStatusPanel(Context paramContext) {
    Intent intent = new Intent();
    intent.setClassName("com.android.vpndialogs", "com.android.vpndialogs.ManageDialog");
    intent.addFlags(1350565888);
    return PendingIntent.getActivityAsUser(paramContext, 0, intent, 0, null, UserHandle.CURRENT);
  }
  
  public static CharSequence getVpnLabel(Context paramContext, String paramString) throws PackageManager.NameNotFoundException {
    PackageManager packageManager = paramContext.getPackageManager();
    Intent intent = new Intent("android.net.VpnService");
    intent.setPackage(paramString);
    List<ResolveInfo> list = packageManager.queryIntentServices(intent, 0);
    if (list != null && list.size() == 1)
      return ((ResolveInfo)list.get(0)).loadLabel(packageManager); 
    return packageManager.getApplicationInfo(paramString, 0).loadLabel(packageManager);
  }
  
  public int mtu = -1;
  
  public List<LinkAddress> addresses = new ArrayList<>();
  
  public List<RouteInfo> routes = new ArrayList<>();
  
  public long startTime = -1L;
  
  public boolean isMetered = true;
  
  public void updateAllowedFamilies(InetAddress paramInetAddress) {
    if (paramInetAddress instanceof java.net.Inet4Address) {
      this.allowIPv4 = true;
    } else {
      this.allowIPv6 = true;
    } 
  }
  
  public void addLegacyRoutes(String paramString) {
    if (paramString.trim().equals(""))
      return; 
    for (String str : paramString.trim().split(" ")) {
      RouteInfo routeInfo = new RouteInfo(new IpPrefix(str), null);
      this.routes.add(routeInfo);
      updateAllowedFamilies(routeInfo.getDestination().getAddress());
    } 
  }
  
  public void addLegacyAddresses(String paramString) {
    if (paramString.trim().equals(""))
      return; 
    for (String str : paramString.trim().split(" ")) {
      LinkAddress linkAddress = new LinkAddress(str);
      this.addresses.add(linkAddress);
      updateAllowedFamilies(linkAddress.getAddress());
    } 
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.user);
    paramParcel.writeString(this.interfaze);
    paramParcel.writeString(this.session);
    paramParcel.writeInt(this.mtu);
    paramParcel.writeTypedList(this.addresses);
    paramParcel.writeTypedList(this.routes);
    paramParcel.writeStringList(this.dnsServers);
    paramParcel.writeStringList(this.searchDomains);
    paramParcel.writeStringList(this.allowedApplications);
    paramParcel.writeStringList(this.disallowedApplications);
    paramParcel.writeParcelable((Parcelable)this.configureIntent, paramInt);
    paramParcel.writeLong(this.startTime);
    paramParcel.writeInt(this.legacy);
    paramParcel.writeInt(this.blocking);
    paramParcel.writeInt(this.allowBypass);
    paramParcel.writeInt(this.allowIPv4);
    paramParcel.writeInt(this.allowIPv6);
    paramParcel.writeInt(this.isMetered);
    paramParcel.writeTypedArray((Parcelable[])this.underlyingNetworks, paramInt);
    paramParcel.writeParcelable((Parcelable)this.proxyInfo, paramInt);
  }
  
  public static final Parcelable.Creator<VpnConfig> CREATOR = new Parcelable.Creator<VpnConfig>() {
      public VpnConfig createFromParcel(Parcel param1Parcel) {
        boolean bool2;
        VpnConfig vpnConfig = new VpnConfig();
        vpnConfig.user = param1Parcel.readString();
        vpnConfig.interfaze = param1Parcel.readString();
        vpnConfig.session = param1Parcel.readString();
        vpnConfig.mtu = param1Parcel.readInt();
        param1Parcel.readTypedList(vpnConfig.addresses, LinkAddress.CREATOR);
        param1Parcel.readTypedList(vpnConfig.routes, RouteInfo.CREATOR);
        vpnConfig.dnsServers = param1Parcel.createStringArrayList();
        vpnConfig.searchDomains = param1Parcel.createStringArrayList();
        vpnConfig.allowedApplications = param1Parcel.createStringArrayList();
        vpnConfig.disallowedApplications = param1Parcel.createStringArrayList();
        vpnConfig.configureIntent = (PendingIntent)param1Parcel.readParcelable(null);
        vpnConfig.startTime = param1Parcel.readLong();
        int i = param1Parcel.readInt();
        boolean bool1 = true;
        if (i != 0) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        vpnConfig.legacy = bool2;
        if (param1Parcel.readInt() != 0) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        vpnConfig.blocking = bool2;
        if (param1Parcel.readInt() != 0) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        vpnConfig.allowBypass = bool2;
        if (param1Parcel.readInt() != 0) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        vpnConfig.allowIPv4 = bool2;
        if (param1Parcel.readInt() != 0) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        vpnConfig.allowIPv6 = bool2;
        if (param1Parcel.readInt() != 0) {
          bool2 = bool1;
        } else {
          bool2 = false;
        } 
        vpnConfig.isMetered = bool2;
        vpnConfig.underlyingNetworks = (Network[])param1Parcel.createTypedArray(Network.CREATOR);
        vpnConfig.proxyInfo = (ProxyInfo)param1Parcel.readParcelable(null);
        return vpnConfig;
      }
      
      public VpnConfig[] newArray(int param1Int) {
        return new VpnConfig[param1Int];
      }
    };
  
  public static final String DIALOGS_PACKAGE = "com.android.vpndialogs";
  
  public static final String LEGACY_VPN = "[Legacy VPN]";
  
  public static final String SERVICE_INTERFACE = "android.net.VpnService";
  
  public boolean allowBypass;
  
  public boolean allowIPv4;
  
  public boolean allowIPv6;
  
  public List<String> allowedApplications;
  
  public boolean blocking;
  
  public PendingIntent configureIntent;
  
  public List<String> disallowedApplications;
  
  public List<String> dnsServers;
  
  public String interfaze;
  
  public boolean legacy;
  
  public ProxyInfo proxyInfo;
  
  public List<String> searchDomains;
  
  public String session;
  
  public Network[] underlyingNetworks;
  
  public String user;
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("VpnConfig");
    stringBuilder.append("{ user=");
    stringBuilder.append(this.user);
    stringBuilder.append(", interface=");
    stringBuilder.append(this.interfaze);
    stringBuilder.append(", session=");
    stringBuilder.append(this.session);
    stringBuilder.append(", mtu=");
    stringBuilder.append(this.mtu);
    stringBuilder.append(", addresses=");
    stringBuilder.append(toString(this.addresses));
    stringBuilder.append(", routes=");
    stringBuilder.append(toString(this.routes));
    stringBuilder.append(", dns=");
    stringBuilder.append(toString(this.dnsServers));
    stringBuilder.append(", searchDomains=");
    stringBuilder.append(toString(this.searchDomains));
    stringBuilder.append(", allowedApps=");
    stringBuilder.append(toString(this.allowedApplications));
    stringBuilder.append(", disallowedApps=");
    stringBuilder.append(toString(this.disallowedApplications));
    stringBuilder.append(", configureIntent=");
    stringBuilder.append(this.configureIntent);
    stringBuilder.append(", startTime=");
    stringBuilder.append(this.startTime);
    stringBuilder.append(", legacy=");
    stringBuilder.append(this.legacy);
    stringBuilder.append(", blocking=");
    stringBuilder.append(this.blocking);
    stringBuilder.append(", allowBypass=");
    stringBuilder.append(this.allowBypass);
    stringBuilder.append(", allowIPv4=");
    stringBuilder.append(this.allowIPv4);
    stringBuilder.append(", allowIPv6=");
    stringBuilder.append(this.allowIPv6);
    stringBuilder.append(", underlyingNetworks=");
    stringBuilder.append(Arrays.toString((Object[])this.underlyingNetworks));
    stringBuilder.append(", proxyInfo=");
    stringBuilder.append(this.proxyInfo);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  static <T> String toString(List<T> paramList) {
    if (paramList == null)
      return "null"; 
    return Arrays.toString(paramList.toArray());
  }
}
