package com.android.internal.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface INetworkWatchlistManager extends IInterface {
  byte[] getWatchlistConfigHash() throws RemoteException;
  
  void reloadWatchlist() throws RemoteException;
  
  void reportWatchlistIfNecessary() throws RemoteException;
  
  boolean startWatchlistLogging() throws RemoteException;
  
  boolean stopWatchlistLogging() throws RemoteException;
  
  class Default implements INetworkWatchlistManager {
    public boolean startWatchlistLogging() throws RemoteException {
      return false;
    }
    
    public boolean stopWatchlistLogging() throws RemoteException {
      return false;
    }
    
    public void reloadWatchlist() throws RemoteException {}
    
    public void reportWatchlistIfNecessary() throws RemoteException {}
    
    public byte[] getWatchlistConfigHash() throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INetworkWatchlistManager {
    private static final String DESCRIPTOR = "com.android.internal.net.INetworkWatchlistManager";
    
    static final int TRANSACTION_getWatchlistConfigHash = 5;
    
    static final int TRANSACTION_reloadWatchlist = 3;
    
    static final int TRANSACTION_reportWatchlistIfNecessary = 4;
    
    static final int TRANSACTION_startWatchlistLogging = 1;
    
    static final int TRANSACTION_stopWatchlistLogging = 2;
    
    public Stub() {
      attachInterface(this, "com.android.internal.net.INetworkWatchlistManager");
    }
    
    public static INetworkWatchlistManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.net.INetworkWatchlistManager");
      if (iInterface != null && iInterface instanceof INetworkWatchlistManager)
        return (INetworkWatchlistManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "getWatchlistConfigHash";
            } 
            return "reportWatchlistIfNecessary";
          } 
          return "reloadWatchlist";
        } 
        return "stopWatchlistLogging";
      } 
      return "startWatchlistLogging";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      byte[] arrayOfByte;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("com.android.internal.net.INetworkWatchlistManager");
                return true;
              } 
              param1Parcel1.enforceInterface("com.android.internal.net.INetworkWatchlistManager");
              arrayOfByte = getWatchlistConfigHash();
              param1Parcel2.writeNoException();
              param1Parcel2.writeByteArray(arrayOfByte);
              return true;
            } 
            arrayOfByte.enforceInterface("com.android.internal.net.INetworkWatchlistManager");
            reportWatchlistIfNecessary();
            param1Parcel2.writeNoException();
            return true;
          } 
          arrayOfByte.enforceInterface("com.android.internal.net.INetworkWatchlistManager");
          reloadWatchlist();
          param1Parcel2.writeNoException();
          return true;
        } 
        arrayOfByte.enforceInterface("com.android.internal.net.INetworkWatchlistManager");
        boolean bool1 = stopWatchlistLogging();
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      arrayOfByte.enforceInterface("com.android.internal.net.INetworkWatchlistManager");
      boolean bool = startWatchlistLogging();
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(bool);
      return true;
    }
    
    private static class Proxy implements INetworkWatchlistManager {
      public static INetworkWatchlistManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.net.INetworkWatchlistManager";
      }
      
      public boolean startWatchlistLogging() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.net.INetworkWatchlistManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && INetworkWatchlistManager.Stub.getDefaultImpl() != null) {
            bool1 = INetworkWatchlistManager.Stub.getDefaultImpl().startWatchlistLogging();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean stopWatchlistLogging() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.net.INetworkWatchlistManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && INetworkWatchlistManager.Stub.getDefaultImpl() != null) {
            bool1 = INetworkWatchlistManager.Stub.getDefaultImpl().stopWatchlistLogging();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reloadWatchlist() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.net.INetworkWatchlistManager");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && INetworkWatchlistManager.Stub.getDefaultImpl() != null) {
            INetworkWatchlistManager.Stub.getDefaultImpl().reloadWatchlist();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportWatchlistIfNecessary() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.net.INetworkWatchlistManager");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && INetworkWatchlistManager.Stub.getDefaultImpl() != null) {
            INetworkWatchlistManager.Stub.getDefaultImpl().reportWatchlistIfNecessary();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getWatchlistConfigHash() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.net.INetworkWatchlistManager");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && INetworkWatchlistManager.Stub.getDefaultImpl() != null)
            return INetworkWatchlistManager.Stub.getDefaultImpl().getWatchlistConfigHash(); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INetworkWatchlistManager param1INetworkWatchlistManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INetworkWatchlistManager != null) {
          Proxy.sDefaultImpl = param1INetworkWatchlistManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INetworkWatchlistManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
