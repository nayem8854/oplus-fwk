package com.android.internal.net;

import android.net.Ikev2VpnProfile;
import android.net.ProxyInfo;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public final class VpnProfile implements Cloneable, Parcelable {
  public String username;
  
  public int type;
  
  public String server;
  
  public String searchDomains;
  
  public transient boolean saveLogin;
  
  public String routes;
  
  public ProxyInfo proxy;
  
  public String password;
  
  public String name = "";
  
  public boolean mppe;
  
  public int maxMtu;
  
  private List<String> mAllowedAlgorithms;
  
  public String l2tpSecret;
  
  public final String key;
  
  public final boolean isRestrictedToTestNetworks;
  
  public boolean isMetered;
  
  public boolean isBypassable;
  
  public String ipsecUserCert;
  
  public String ipsecServerCert;
  
  public String ipsecSecret;
  
  public String ipsecIdentifier;
  
  public String ipsecCaCert;
  
  public String dnsServers;
  
  public boolean areAuthParamsInline;
  
  static final String VALUE_DELIMITER = "\000";
  
  public static final int TYPE_PPTP = 0;
  
  public static final int TYPE_MAX = 8;
  
  public static final int TYPE_L2TP_IPSEC_RSA = 2;
  
  public static final int TYPE_L2TP_IPSEC_PSK = 1;
  
  public static final int TYPE_IPSEC_XAUTH_RSA = 4;
  
  public static final int TYPE_IPSEC_XAUTH_PSK = 3;
  
  public static final int TYPE_IPSEC_HYBRID_RSA = 5;
  
  public static final int TYPE_IKEV2_IPSEC_USER_PASS = 6;
  
  public static final int TYPE_IKEV2_IPSEC_RSA = 8;
  
  public static final int TYPE_IKEV2_IPSEC_PSK = 7;
  
  private static final String TAG = "VpnProfile";
  
  public static final int PROXY_NONE = 0;
  
  public static final int PROXY_MANUAL = 1;
  
  static final String LIST_DELIMITER = ",";
  
  private static final String ENCODED_NULL_PROXY_INFO = "\000\000\000\000";
  
  public VpnProfile(Parcel paramParcel) {
    boolean bool1 = false;
    this.type = 0;
    this.server = "";
    this.username = "";
    this.password = "";
    this.dnsServers = "";
    this.searchDomains = "";
    this.routes = "";
    this.mppe = true;
    this.l2tpSecret = "";
    this.ipsecIdentifier = "";
    this.ipsecSecret = "";
    this.ipsecUserCert = "";
    this.ipsecCaCert = "";
    this.ipsecServerCert = "";
    this.proxy = null;
    this.mAllowedAlgorithms = new ArrayList<>();
    this.isBypassable = false;
    this.isMetered = false;
    this.maxMtu = 1360;
    this.areAuthParamsInline = false;
    this.saveLogin = false;
    this.key = paramParcel.readString();
    this.name = paramParcel.readString();
    this.type = paramParcel.readInt();
    this.server = paramParcel.readString();
    this.username = paramParcel.readString();
    this.password = paramParcel.readString();
    this.dnsServers = paramParcel.readString();
    this.searchDomains = paramParcel.readString();
    this.routes = paramParcel.readString();
    if (paramParcel.readInt() != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mppe = bool2;
    this.l2tpSecret = paramParcel.readString();
    this.ipsecIdentifier = paramParcel.readString();
    this.ipsecSecret = paramParcel.readString();
    this.ipsecUserCert = paramParcel.readString();
    this.ipsecCaCert = paramParcel.readString();
    this.ipsecServerCert = paramParcel.readString();
    boolean bool2 = bool1;
    if (paramParcel.readInt() != 0)
      bool2 = true; 
    this.saveLogin = bool2;
    this.proxy = (ProxyInfo)paramParcel.readParcelable(null);
    ArrayList<String> arrayList = new ArrayList();
    paramParcel.readList(arrayList, null);
    this.isBypassable = paramParcel.readBoolean();
    this.isMetered = paramParcel.readBoolean();
    this.maxMtu = paramParcel.readInt();
    this.areAuthParamsInline = paramParcel.readBoolean();
    this.isRestrictedToTestNetworks = paramParcel.readBoolean();
  }
  
  public VpnProfile(String paramString, boolean paramBoolean) {
    this.type = 0;
    this.server = "";
    this.username = "";
    this.password = "";
    this.dnsServers = "";
    this.searchDomains = "";
    this.routes = "";
    this.mppe = true;
    this.l2tpSecret = "";
    this.ipsecIdentifier = "";
    this.ipsecSecret = "";
    this.ipsecUserCert = "";
    this.ipsecCaCert = "";
    this.ipsecServerCert = "";
    this.proxy = null;
    this.mAllowedAlgorithms = new ArrayList<>();
    this.isBypassable = false;
    this.isMetered = false;
    this.maxMtu = 1360;
    this.areAuthParamsInline = false;
    this.saveLogin = false;
    this.key = paramString;
    this.isRestrictedToTestNetworks = paramBoolean;
  }
  
  public VpnProfile(String paramString) {
    this(paramString, false);
  }
  
  public List<String> getAllowedAlgorithms() {
    return Collections.unmodifiableList(this.mAllowedAlgorithms);
  }
  
  public void setAllowedAlgorithms(List<String> paramList) {
    validateAllowedAlgorithms(paramList);
    this.mAllowedAlgorithms = paramList;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.key);
    paramParcel.writeString(this.name);
    paramParcel.writeInt(this.type);
    paramParcel.writeString(this.server);
    paramParcel.writeString(this.username);
    paramParcel.writeString(this.password);
    paramParcel.writeString(this.dnsServers);
    paramParcel.writeString(this.searchDomains);
    paramParcel.writeString(this.routes);
    paramParcel.writeInt(this.mppe);
    paramParcel.writeString(this.l2tpSecret);
    paramParcel.writeString(this.ipsecIdentifier);
    paramParcel.writeString(this.ipsecSecret);
    paramParcel.writeString(this.ipsecUserCert);
    paramParcel.writeString(this.ipsecCaCert);
    paramParcel.writeString(this.ipsecServerCert);
    paramParcel.writeInt(this.saveLogin);
    paramParcel.writeParcelable((Parcelable)this.proxy, paramInt);
    paramParcel.writeList(this.mAllowedAlgorithms);
    paramParcel.writeBoolean(this.isBypassable);
    paramParcel.writeBoolean(this.isMetered);
    paramParcel.writeInt(this.maxMtu);
    paramParcel.writeBoolean(this.areAuthParamsInline);
    paramParcel.writeBoolean(this.isRestrictedToTestNetworks);
  }
  
  public static VpnProfile decode(String paramString, byte[] paramArrayOfbyte) {
    if (paramString == null)
      return null; 
    try {
      String str1 = new String();
      this(paramArrayOfbyte, StandardCharsets.UTF_8);
      String[] arrayOfString = str1.split("\000", -1);
      if ((arrayOfString.length < 14 || arrayOfString.length > 19) && arrayOfString.length != 24 && arrayOfString.length != 25)
        return null; 
      if (arrayOfString.length >= 25) {
        bool = Boolean.parseBoolean(arrayOfString[24]);
      } else {
        bool = false;
      } 
      VpnProfile vpnProfile = new VpnProfile();
      this(paramString, bool);
      boolean bool = false;
      vpnProfile.name = arrayOfString[0];
      int i = Integer.parseInt(arrayOfString[1]);
      if (i < 0 || i > 8)
        return null; 
      vpnProfile.server = arrayOfString[2];
      vpnProfile.username = arrayOfString[3];
      vpnProfile.password = arrayOfString[4];
      vpnProfile.dnsServers = arrayOfString[5];
      vpnProfile.searchDomains = arrayOfString[6];
      vpnProfile.routes = arrayOfString[7];
      vpnProfile.mppe = Boolean.parseBoolean(arrayOfString[8]);
      vpnProfile.l2tpSecret = arrayOfString[9];
      vpnProfile.ipsecIdentifier = arrayOfString[10];
      vpnProfile.ipsecSecret = arrayOfString[11];
      vpnProfile.ipsecUserCert = arrayOfString[12];
      vpnProfile.ipsecCaCert = arrayOfString[13];
      i = arrayOfString.length;
      String str2 = "";
      if (i > 14) {
        paramString = arrayOfString[14];
      } else {
        paramString = "";
      } 
      vpnProfile.ipsecServerCert = paramString;
      if (arrayOfString.length > 15) {
        String str;
        ProxyInfo proxyInfo;
        if (arrayOfString.length > 15) {
          paramString = arrayOfString[15];
        } else {
          paramString = "";
        } 
        if (arrayOfString.length > 16) {
          str = arrayOfString[16];
        } else {
          str = "";
        } 
        if (arrayOfString.length > 17) {
          str1 = arrayOfString[17];
        } else {
          str1 = "";
        } 
        if (arrayOfString.length > 18)
          str2 = arrayOfString[18]; 
        if (!paramString.isEmpty() || !str.isEmpty() || !str1.isEmpty()) {
          proxyInfo = new ProxyInfo();
          if (str.isEmpty()) {
            i = 0;
          } else {
            i = Integer.parseInt(str);
          } 
          this(paramString, i, str1);
          vpnProfile.proxy = proxyInfo;
        } else if (!proxyInfo.isEmpty()) {
          ProxyInfo proxyInfo1 = new ProxyInfo();
          this((String)proxyInfo);
          vpnProfile.proxy = proxyInfo1;
        } 
      } 
      if (arrayOfString.length >= 24) {
        vpnProfile.mAllowedAlgorithms = Arrays.asList(arrayOfString[19].split(","));
        vpnProfile.isBypassable = Boolean.parseBoolean(arrayOfString[20]);
        vpnProfile.isMetered = Boolean.parseBoolean(arrayOfString[21]);
        vpnProfile.maxMtu = Integer.parseInt(arrayOfString[22]);
        vpnProfile.areAuthParamsInline = Boolean.parseBoolean(arrayOfString[23]);
      } 
      if (!vpnProfile.username.isEmpty() || !vpnProfile.password.isEmpty())
        bool = true; 
      vpnProfile.saveLogin = bool;
      return vpnProfile;
    } catch (Exception exception) {
      return null;
    } 
  }
  
  public byte[] encode() {
    String str2;
    StringBuilder stringBuilder = new StringBuilder(this.name);
    stringBuilder.append("\000");
    stringBuilder.append(this.type);
    stringBuilder.append("\000");
    stringBuilder.append(this.server);
    stringBuilder.append("\000");
    boolean bool = this.saveLogin;
    String str1 = "";
    if (bool) {
      str2 = this.username;
    } else {
      str2 = "";
    } 
    stringBuilder.append(str2);
    stringBuilder.append("\000");
    if (this.saveLogin) {
      str2 = this.password;
    } else {
      str2 = "";
    } 
    stringBuilder.append(str2);
    stringBuilder.append("\000");
    stringBuilder.append(this.dnsServers);
    stringBuilder.append("\000");
    stringBuilder.append(this.searchDomains);
    stringBuilder.append("\000");
    stringBuilder.append(this.routes);
    stringBuilder.append("\000");
    stringBuilder.append(this.mppe);
    stringBuilder.append("\000");
    stringBuilder.append(this.l2tpSecret);
    stringBuilder.append("\000");
    stringBuilder.append(this.ipsecIdentifier);
    stringBuilder.append("\000");
    stringBuilder.append(this.ipsecSecret);
    stringBuilder.append("\000");
    stringBuilder.append(this.ipsecUserCert);
    stringBuilder.append("\000");
    stringBuilder.append(this.ipsecCaCert);
    stringBuilder.append("\000");
    stringBuilder.append(this.ipsecServerCert);
    if (this.proxy != null) {
      stringBuilder.append("\000");
      if (this.proxy.getHost() != null) {
        str2 = this.proxy.getHost();
      } else {
        str2 = "";
      } 
      stringBuilder.append(str2);
      stringBuilder.append("\000");
      stringBuilder.append(this.proxy.getPort());
      stringBuilder.append("\000");
      if (this.proxy.getExclusionListAsString() != null) {
        str2 = this.proxy.getExclusionListAsString();
      } else {
        str2 = str1;
      } 
      stringBuilder.append(str2);
      stringBuilder.append("\000");
      stringBuilder.append(this.proxy.getPacFileUrl().toString());
    } else {
      stringBuilder.append("\000\000\000\000");
    } 
    stringBuilder.append("\000");
    stringBuilder.append(String.join(",", (Iterable)this.mAllowedAlgorithms));
    stringBuilder.append("\000");
    stringBuilder.append(this.isBypassable);
    stringBuilder.append("\000");
    stringBuilder.append(this.isMetered);
    stringBuilder.append("\000");
    stringBuilder.append(this.maxMtu);
    stringBuilder.append("\000");
    stringBuilder.append(this.areAuthParamsInline);
    stringBuilder.append("\000");
    stringBuilder.append(this.isRestrictedToTestNetworks);
    return stringBuilder.toString().getBytes(StandardCharsets.UTF_8);
  }
  
  public static boolean isLegacyType(int paramInt) {
    if (paramInt != 6 && paramInt != 7 && paramInt != 8)
      return true; 
    return false;
  }
  
  private boolean isValidLockdownLegacyVpnProfile() {
    boolean bool;
    if (isLegacyType(this.type) && isServerAddressNumeric() && hasDns() && 
      areDnsAddressesNumeric()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isValidLockdownPlatformVpnProfile() {
    return Ikev2VpnProfile.isValidVpnProfile(this);
  }
  
  public boolean isValidLockdownProfile() {
    boolean bool;
    if (isTypeValidForLockdown() && (
      isValidLockdownLegacyVpnProfile() || isValidLockdownPlatformVpnProfile())) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isTypeValidForLockdown() {
    boolean bool;
    if (this.type != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isServerAddressNumeric() {
    try {
      InetAddress.parseNumericAddress(this.server);
      return true;
    } catch (IllegalArgumentException illegalArgumentException) {
      return false;
    } 
  }
  
  public boolean hasDns() {
    return TextUtils.isEmpty(this.dnsServers) ^ true;
  }
  
  public boolean areDnsAddressesNumeric() {
    try {
      for (String str : this.dnsServers.split(" +"))
        InetAddress.parseNumericAddress(str); 
      return true;
    } catch (IllegalArgumentException illegalArgumentException) {
      return false;
    } 
  }
  
  public static void validateAllowedAlgorithms(List<String> paramList) {
    for (String str : paramList) {
      if (!str.contains("\000") && !str.contains(","))
        continue; 
      throw new IllegalArgumentException("Algorithm contained illegal ('\000' or ',') character");
    } 
  }
  
  public int hashCode() {
    String str1 = this.key;
    int i = this.type;
    String str2 = this.server, str3 = this.username, str4 = this.password, str5 = this.dnsServers, str6 = this.searchDomains, str7 = this.routes;
    boolean bool1 = this.mppe;
    String str8 = this.l2tpSecret, str9 = this.ipsecIdentifier, str10 = this.ipsecSecret, str11 = this.ipsecUserCert, str12 = this.ipsecCaCert, str13 = this.ipsecServerCert;
    ProxyInfo proxyInfo = this.proxy;
    List<String> list = this.mAllowedAlgorithms;
    boolean bool2 = this.isBypassable;
    boolean bool3 = this.isMetered;
    int j = this.maxMtu;
    boolean bool4 = this.areAuthParamsInline, bool5 = this.isRestrictedToTestNetworks;
    return Objects.hash(new Object[] { 
          str1, Integer.valueOf(i), str2, str3, str4, str5, str6, str7, Boolean.valueOf(bool1), str8, 
          str9, str10, str11, str12, str13, proxyInfo, list, Boolean.valueOf(bool2), Boolean.valueOf(bool3), Integer.valueOf(j), 
          Boolean.valueOf(bool4), Boolean.valueOf(bool5) });
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof VpnProfile;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    if (Objects.equals(this.key, ((VpnProfile)paramObject).key)) {
      String str1 = this.name, str2 = ((VpnProfile)paramObject).name;
      if (Objects.equals(str1, str2) && this.type == ((VpnProfile)paramObject).type) {
        str2 = this.server;
        str1 = ((VpnProfile)paramObject).server;
        if (Objects.equals(str2, str1)) {
          str1 = this.username;
          str2 = ((VpnProfile)paramObject).username;
          if (Objects.equals(str1, str2)) {
            str2 = this.password;
            str1 = ((VpnProfile)paramObject).password;
            if (Objects.equals(str2, str1)) {
              str2 = this.dnsServers;
              str1 = ((VpnProfile)paramObject).dnsServers;
              if (Objects.equals(str2, str1)) {
                str1 = this.searchDomains;
                str2 = ((VpnProfile)paramObject).searchDomains;
                if (Objects.equals(str1, str2)) {
                  str2 = this.routes;
                  str1 = ((VpnProfile)paramObject).routes;
                  if (Objects.equals(str2, str1) && this.mppe == ((VpnProfile)paramObject).mppe) {
                    str2 = this.l2tpSecret;
                    str1 = ((VpnProfile)paramObject).l2tpSecret;
                    if (Objects.equals(str2, str1)) {
                      str2 = this.ipsecIdentifier;
                      str1 = ((VpnProfile)paramObject).ipsecIdentifier;
                      if (Objects.equals(str2, str1)) {
                        str2 = this.ipsecSecret;
                        str1 = ((VpnProfile)paramObject).ipsecSecret;
                        if (Objects.equals(str2, str1)) {
                          str1 = this.ipsecUserCert;
                          str2 = ((VpnProfile)paramObject).ipsecUserCert;
                          if (Objects.equals(str1, str2)) {
                            str2 = this.ipsecCaCert;
                            str1 = ((VpnProfile)paramObject).ipsecCaCert;
                            if (Objects.equals(str2, str1)) {
                              str2 = this.ipsecServerCert;
                              str1 = ((VpnProfile)paramObject).ipsecServerCert;
                              if (Objects.equals(str2, str1)) {
                                ProxyInfo proxyInfo2 = this.proxy, proxyInfo1 = ((VpnProfile)paramObject).proxy;
                                if (Objects.equals(proxyInfo2, proxyInfo1)) {
                                  List<String> list2 = this.mAllowedAlgorithms, list1 = ((VpnProfile)paramObject).mAllowedAlgorithms;
                                  if (Objects.equals(list2, list1) && this.isBypassable == ((VpnProfile)paramObject).isBypassable && this.isMetered == ((VpnProfile)paramObject).isMetered && this.maxMtu == ((VpnProfile)paramObject).maxMtu && this.areAuthParamsInline == ((VpnProfile)paramObject).areAuthParamsInline && this.isRestrictedToTestNetworks == ((VpnProfile)paramObject).isRestrictedToTestNetworks)
                                    bool1 = true; 
                                } 
                              } 
                            } 
                          } 
                        } 
                      } 
                    } 
                  } 
                } 
              } 
            } 
          } 
        } 
      } 
    } 
    return bool1;
  }
  
  public static final Parcelable.Creator<VpnProfile> CREATOR = new Parcelable.Creator<VpnProfile>() {
      public VpnProfile createFromParcel(Parcel param1Parcel) {
        return new VpnProfile(param1Parcel);
      }
      
      public VpnProfile[] newArray(int param1Int) {
        return new VpnProfile[param1Int];
      }
    };
  
  public int describeContents() {
    return 0;
  }
}
