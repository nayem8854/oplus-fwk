package com.android.internal.alsa;

import android.util.Slog;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class AlsaCardsParser {
  protected static final boolean DEBUG = false;
  
  public static final int SCANSTATUS_EMPTY = 2;
  
  public static final int SCANSTATUS_FAIL = 1;
  
  public static final int SCANSTATUS_NOTSCANNED = -1;
  
  public static final int SCANSTATUS_SUCCESS = 0;
  
  private static final String TAG = "AlsaCardsParser";
  
  private static final String kAlsaFolderPath = "/proc/asound";
  
  private static final String kCardsFilePath = "/proc/asound/cards";
  
  private static final String kDeviceAddressPrefix = "/dev/bus/usb/";
  
  private static LineTokenizer mTokenizer = new LineTokenizer(" :[]");
  
  private ArrayList<AlsaCardRecord> mCardRecords = new ArrayList<>();
  
  private int mScanStatus = -1;
  
  public class AlsaCardRecord {
    int mCardNum = -1;
    
    String mField1 = "";
    
    String mCardName = "";
    
    String mCardDescription = "";
    
    private String mUsbDeviceAddress = null;
    
    private static final String TAG = "AlsaCardRecord";
    
    private static final String kUsbCardKeyStr = "at usb-";
    
    final AlsaCardsParser this$0;
    
    public int getCardNum() {
      return this.mCardNum;
    }
    
    public String getCardName() {
      return this.mCardName;
    }
    
    public String getCardDescription() {
      return this.mCardDescription;
    }
    
    public void setDeviceAddress(String param1String) {
      this.mUsbDeviceAddress = param1String;
    }
    
    private boolean parse(String param1String, int param1Int) {
      int i = 0;
      if (param1Int == 0) {
        int j = AlsaCardsParser.mTokenizer.nextToken(param1String, 0);
        i = AlsaCardsParser.mTokenizer.nextDelimiter(param1String, j);
        try {
          this.mCardNum = Integer.parseInt(param1String.substring(j, i));
          param1Int = AlsaCardsParser.mTokenizer.nextToken(param1String, i);
          i = AlsaCardsParser.mTokenizer.nextDelimiter(param1String, param1Int);
          this.mField1 = param1String.substring(param1Int, i);
          param1Int = AlsaCardsParser.mTokenizer.nextToken(param1String, i);
          this.mCardName = param1String.substring(param1Int);
        } catch (NumberFormatException numberFormatException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Failed to parse line ");
          stringBuilder.append(param1Int);
          stringBuilder.append(" of ");
          stringBuilder.append("/proc/asound/cards");
          stringBuilder.append(": ");
          stringBuilder.append(param1String.substring(j, i));
          param1String = stringBuilder.toString();
          Slog.e("AlsaCardRecord", param1String);
          return false;
        } 
      } else if (param1Int == 1) {
        int j = AlsaCardsParser.mTokenizer.nextToken(param1String, 0);
        if (j != -1) {
          int k = param1String.indexOf("at usb-");
          param1Int = i;
          if (k != -1)
            param1Int = 1; 
          if (param1Int != 0)
            this.mCardDescription = param1String.substring(j, k - 1); 
        } 
      } 
      return true;
    }
    
    boolean isUsb() {
      boolean bool;
      if (this.mUsbDeviceAddress != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public String textFormat() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(this.mCardName);
      stringBuilder.append(" : ");
      stringBuilder.append(this.mCardDescription);
      stringBuilder.append(" [addr:");
      stringBuilder.append(this.mUsbDeviceAddress);
      stringBuilder.append("]");
      return stringBuilder.toString();
    }
    
    public void log(int param1Int) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("");
      stringBuilder.append(param1Int);
      stringBuilder.append(" [");
      stringBuilder.append(this.mCardNum);
      stringBuilder.append(" ");
      stringBuilder.append(this.mCardName);
      stringBuilder.append(" : ");
      stringBuilder.append(this.mCardDescription);
      stringBuilder.append(" usb:");
      stringBuilder.append(isUsb());
      String str = stringBuilder.toString();
      Slog.d("AlsaCardRecord", str);
    }
  }
  
  public int scan() {
    this.mCardRecords = new ArrayList<>();
    File file = new File("/proc/asound/cards");
    try {
      FileReader fileReader = new FileReader();
      this(file);
      BufferedReader bufferedReader = new BufferedReader();
      this(fileReader);
      while (true) {
        String str = bufferedReader.readLine();
        if (str != null) {
          AlsaCardRecord alsaCardRecord = new AlsaCardRecord();
          this(this);
          alsaCardRecord.parse(str, 0);
          str = bufferedReader.readLine();
          if (str == null)
            break; 
          alsaCardRecord.parse(str, 1);
          int i = alsaCardRecord.mCardNum;
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("/proc/asound/card");
          stringBuilder.append(i);
          String str1 = stringBuilder.toString();
          File file1 = new File();
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append(str1);
          stringBuilder.append("/usbbus");
          this(stringBuilder.toString());
          if (file1.exists()) {
            FileReader fileReader1 = new FileReader();
            this(file1);
            BufferedReader bufferedReader1 = new BufferedReader();
            this(fileReader1);
            str1 = bufferedReader1.readLine();
            if (str1 != null) {
              StringBuilder stringBuilder1 = new StringBuilder();
              this();
              stringBuilder1.append("/dev/bus/usb/");
              stringBuilder1.append(str1);
              alsaCardRecord.setDeviceAddress(stringBuilder1.toString());
            } 
            fileReader1.close();
          } 
          this.mCardRecords.add(alsaCardRecord);
          continue;
        } 
        break;
      } 
      fileReader.close();
      if (this.mCardRecords.size() > 0) {
        this.mScanStatus = 0;
      } else {
        this.mScanStatus = 2;
      } 
    } catch (FileNotFoundException fileNotFoundException) {
      this.mScanStatus = 1;
    } catch (IOException iOException) {
      this.mScanStatus = 1;
    } 
    return this.mScanStatus;
  }
  
  public int getScanStatus() {
    return this.mScanStatus;
  }
  
  public AlsaCardRecord findCardNumFor(String paramString) {
    for (AlsaCardRecord alsaCardRecord : this.mCardRecords) {
      if (alsaCardRecord.isUsb() && alsaCardRecord.mUsbDeviceAddress.equals(paramString))
        return alsaCardRecord; 
    } 
    return null;
  }
  
  private void Log(String paramString) {}
}
