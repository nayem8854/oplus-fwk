package com.android.internal.alsa;

import android.util.Slog;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class AlsaDevicesParser {
  private static LineTokenizer mTokenizer = new LineTokenizer(" :[]-");
  
  private boolean mHasCaptureDevices = false;
  
  private boolean mHasPlaybackDevices = false;
  
  private boolean mHasMIDIDevices = false;
  
  private int mScanStatus = -1;
  
  public class AlsaDeviceRecord {
    int mCardNum = -1;
    
    int mDeviceNum = -1;
    
    int mDeviceType = -1;
    
    int mDeviceDir = -1;
    
    public static final int kDeviceDir_Capture = 0;
    
    public static final int kDeviceDir_Playback = 1;
    
    public static final int kDeviceDir_Unknown = -1;
    
    public static final int kDeviceType_Audio = 0;
    
    public static final int kDeviceType_Control = 1;
    
    public static final int kDeviceType_MIDI = 2;
    
    public static final int kDeviceType_Unknown = -1;
    
    final AlsaDevicesParser this$0;
    
    public boolean parse(String param1String) {
      int i = 0;
      int j = 0;
      while (true) {
        StringBuilder stringBuilder;
        int k = AlsaDevicesParser.mTokenizer.nextToken(param1String, i);
        if (k == -1)
          return true; 
        int m = AlsaDevicesParser.mTokenizer.nextDelimiter(param1String, k);
        i = m;
        if (m == -1)
          i = param1String.length(); 
        String str = param1String.substring(k, i);
        if (j != 1) {
          if (j != 2) {
            if (j != 3) {
              if (j != 4) {
                if (j != 5) {
                  m = j;
                } else {
                  try {
                    if (str.equals("capture")) {
                      this.mDeviceDir = 0;
                      AlsaDevicesParser.access$202(AlsaDevicesParser.this, true);
                      m = j;
                    } else {
                      m = j;
                      if (str.equals("playback")) {
                        this.mDeviceDir = 1;
                        AlsaDevicesParser.access$302(AlsaDevicesParser.this, true);
                        m = j;
                      } 
                    } 
                    j = m + 1;
                  } catch (NumberFormatException numberFormatException) {
                    stringBuilder = new StringBuilder();
                    stringBuilder.append("Failed to parse token ");
                    stringBuilder.append(j);
                    stringBuilder.append(" of ");
                    stringBuilder.append("/proc/asound/devices");
                    stringBuilder.append(" token: ");
                    stringBuilder.append(str);
                    Slog.e("AlsaDevicesParser", stringBuilder.toString());
                    return false;
                  } 
                  continue;
                } 
              } else if (str.equals("audio")) {
                this.mDeviceType = 0;
                m = j;
              } else {
                m = j;
                if (str.equals("midi")) {
                  this.mDeviceType = 2;
                  AlsaDevicesParser.access$102(AlsaDevicesParser.this, true);
                  m = j;
                } 
              } 
            } else if (str.equals("digital")) {
              m = j;
            } else if (str.equals("control")) {
              this.mDeviceType = 1;
              m = j;
            } else {
              str.equals("raw");
              m = j;
            } 
          } else {
            this.mDeviceNum = Integer.parseInt(str);
            m = j;
          } 
        } else {
          this.mCardNum = Integer.parseInt(str);
          k = stringBuilder.charAt(i);
          m = j;
          if (k != 45)
            m = j + 1; 
        } 
        j = m + 1;
      } 
    }
    
    public String textFormat() {
      StringBuilder stringBuilder1 = new StringBuilder();
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("[");
      stringBuilder2.append(this.mCardNum);
      stringBuilder2.append(":");
      stringBuilder2.append(this.mDeviceNum);
      stringBuilder2.append("]");
      stringBuilder1.append(stringBuilder2.toString());
      int i = this.mDeviceType;
      if (i != 0) {
        if (i != 1) {
          if (i != 2) {
            stringBuilder1.append(" N/A");
          } else {
            stringBuilder1.append(" MIDI");
          } 
        } else {
          stringBuilder1.append(" Control");
        } 
      } else {
        stringBuilder1.append(" Audio");
      } 
      i = this.mDeviceDir;
      if (i != 0) {
        if (i != 1) {
          stringBuilder1.append(" N/A");
        } else {
          stringBuilder1.append(" Playback");
        } 
      } else {
        stringBuilder1.append(" Capture");
      } 
      return stringBuilder1.toString();
    }
  }
  
  private final ArrayList<AlsaDeviceRecord> mDeviceRecords = new ArrayList<>();
  
  protected static final boolean DEBUG = false;
  
  public static final int SCANSTATUS_EMPTY = 2;
  
  public static final int SCANSTATUS_FAIL = 1;
  
  public static final int SCANSTATUS_NOTSCANNED = -1;
  
  public static final int SCANSTATUS_SUCCESS = 0;
  
  private static final String TAG = "AlsaDevicesParser";
  
  private static final String kDevicesFilePath = "/proc/asound/devices";
  
  private static final int kEndIndex_CardNum = 8;
  
  private static final int kEndIndex_DeviceNum = 11;
  
  private static final int kIndex_CardDeviceField = 5;
  
  private static final int kStartIndex_CardNum = 6;
  
  private static final int kStartIndex_DeviceNum = 9;
  
  private static final int kStartIndex_Type = 14;
  
  public int getDefaultDeviceNum(int paramInt) {
    return 0;
  }
  
  public boolean hasPlaybackDevices(int paramInt) {
    for (AlsaDeviceRecord alsaDeviceRecord : this.mDeviceRecords) {
      if (alsaDeviceRecord.mCardNum == paramInt && alsaDeviceRecord.mDeviceType == 0 && alsaDeviceRecord.mDeviceDir == 1)
        return true; 
    } 
    return false;
  }
  
  public boolean hasCaptureDevices(int paramInt) {
    for (AlsaDeviceRecord alsaDeviceRecord : this.mDeviceRecords) {
      if (alsaDeviceRecord.mCardNum == paramInt && alsaDeviceRecord.mDeviceType == 0 && alsaDeviceRecord.mDeviceDir == 0)
        return true; 
    } 
    return false;
  }
  
  public boolean hasMIDIDevices(int paramInt) {
    for (AlsaDeviceRecord alsaDeviceRecord : this.mDeviceRecords) {
      if (alsaDeviceRecord.mCardNum == paramInt && alsaDeviceRecord.mDeviceType == 2)
        return true; 
    } 
    return false;
  }
  
  private boolean isLineDeviceRecord(String paramString) {
    boolean bool;
    if (paramString.charAt(5) == '[') {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int scan() {
    this.mDeviceRecords.clear();
    File file = new File("/proc/asound/devices");
    try {
      FileReader fileReader = new FileReader();
      this(file);
      BufferedReader bufferedReader = new BufferedReader();
      this(fileReader);
      while (true) {
        String str = bufferedReader.readLine();
        if (str != null) {
          if (isLineDeviceRecord(str)) {
            AlsaDeviceRecord alsaDeviceRecord = new AlsaDeviceRecord();
            this(this);
            alsaDeviceRecord.parse(str);
            Slog.i("AlsaDevicesParser", alsaDeviceRecord.textFormat());
            this.mDeviceRecords.add(alsaDeviceRecord);
          } 
          continue;
        } 
        break;
      } 
      fileReader.close();
      if (this.mDeviceRecords.size() > 0) {
        this.mScanStatus = 0;
      } else {
        this.mScanStatus = 2;
      } 
    } catch (FileNotFoundException fileNotFoundException) {
      fileNotFoundException.printStackTrace();
      this.mScanStatus = 1;
    } catch (IOException iOException) {
      iOException.printStackTrace();
      this.mScanStatus = 1;
    } 
    return this.mScanStatus;
  }
  
  public int getScanStatus() {
    return this.mScanStatus;
  }
  
  private void Log(String paramString) {}
}
