package com.android.internal.alsa;

public class LineTokenizer {
  public static final int kTokenNotFound = -1;
  
  private final String mDelimiters;
  
  public LineTokenizer(String paramString) {
    this.mDelimiters = paramString;
  }
  
  int nextToken(String paramString, int paramInt) {
    int j, i = paramString.length();
    while (true) {
      j = -1;
      if (paramInt >= i || 
        this.mDelimiters.indexOf(paramString.charAt(paramInt)) == -1)
        break; 
      paramInt++;
    } 
    if (paramInt < i)
      j = paramInt; 
    return j;
  }
  
  int nextDelimiter(String paramString, int paramInt) {
    int j, i = paramString.length();
    while (true) {
      j = -1;
      if (paramInt >= i || 
        this.mDelimiters.indexOf(paramString.charAt(paramInt)) != -1)
        break; 
      paramInt++;
    } 
    if (paramInt < i)
      j = paramInt; 
    return j;
  }
}
