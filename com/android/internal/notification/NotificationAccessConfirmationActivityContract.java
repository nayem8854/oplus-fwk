package com.android.internal.notification;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Parcelable;

public final class NotificationAccessConfirmationActivityContract {
  private static final ComponentName COMPONENT_NAME = new ComponentName("com.android.settings", "com.android.settings.notification.NotificationAccessConfirmationActivity");
  
  public static final String EXTRA_COMPONENT_NAME = "component_name";
  
  public static final String EXTRA_PACKAGE_TITLE = "package_title";
  
  public static final String EXTRA_USER_ID = "user_id";
  
  public static Intent launcherIntent(int paramInt, ComponentName paramComponentName, String paramString) {
    Intent intent1 = new Intent();
    ComponentName componentName = COMPONENT_NAME;
    Intent intent2 = intent1.setComponent(componentName);
    intent2 = intent2.putExtra("user_id", paramInt);
    null = intent2.putExtra("component_name", (Parcelable)paramComponentName);
    return null.putExtra("package_title", paramString);
  }
}
