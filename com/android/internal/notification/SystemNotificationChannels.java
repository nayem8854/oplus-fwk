package com.android.internal.notification;

import android.app.INotificationManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.ParceledListSlice;
import android.media.AudioAttributes;
import android.os.RemoteException;
import android.provider.Settings;
import java.util.ArrayList;
import java.util.Arrays;

public class SystemNotificationChannels {
  public static String ACCOUNT;
  
  public static String ALERTS;
  
  public static String CAR_MODE;
  
  public static String DEVELOPER;
  
  public static String DEVELOPER_IMPORTANT;
  
  public static String DEVICE_ADMIN;
  
  @Deprecated
  public static String DEVICE_ADMIN_DEPRECATED;
  
  public static String DO_NOT_DISTURB;
  
  public static String FOREGROUND_SERVICE;
  
  public static String HEAVY_WEIGHT_APP;
  
  public static String NETWORK_ALERTS;
  
  public static String NETWORK_AVAILABLE;
  
  public static String NETWORK_STATUS;
  
  public static String PHYSICAL_KEYBOARD;
  
  public static String RETAIL_MODE;
  
  public static String SECURITY;
  
  public static String SYSTEM_CHANGES;
  
  public static String UPDATES;
  
  public static String USB;
  
  public static String VIRTUAL_KEYBOARD = "VIRTUAL_KEYBOARD";
  
  public static String VPN;
  
  static {
    PHYSICAL_KEYBOARD = "PHYSICAL_KEYBOARD";
    SECURITY = "SECURITY";
    CAR_MODE = "CAR_MODE";
    ACCOUNT = "ACCOUNT";
    DEVELOPER = "DEVELOPER";
    DEVELOPER_IMPORTANT = "DEVELOPER_IMPORTANT";
    UPDATES = "UPDATES";
    NETWORK_STATUS = "NETWORK_STATUS";
    NETWORK_ALERTS = "NETWORK_ALERTS";
    NETWORK_AVAILABLE = "NETWORK_AVAILABLE";
    VPN = "VPN";
    DEVICE_ADMIN_DEPRECATED = "DEVICE_ADMIN";
    DEVICE_ADMIN = "DEVICE_ADMIN_ALERTS";
    ALERTS = "ALERTS";
    RETAIL_MODE = "RETAIL_MODE";
    USB = "USB";
    FOREGROUND_SERVICE = "FOREGROUND_SERVICE";
    HEAVY_WEIGHT_APP = "HEAVY_WEIGHT_APP";
    SYSTEM_CHANGES = "SYSTEM_CHANGES";
    DO_NOT_DISTURB = "DO_NOT_DISTURB";
  }
  
  public static void createAll(Context paramContext) {
    NotificationManager notificationManager = (NotificationManager)paramContext.getSystemService(NotificationManager.class);
    ArrayList<NotificationChannel> arrayList = new ArrayList();
    String str18 = VIRTUAL_KEYBOARD;
    NotificationChannel notificationChannel18 = new NotificationChannel(str18, paramContext.getString(17040732), 2);
    notificationChannel18.setBlockable(true);
    arrayList.add(notificationChannel18);
    String str17 = PHYSICAL_KEYBOARD;
    NotificationChannel notificationChannel17 = new NotificationChannel(str17, paramContext.getString(17040723), 3);
    notificationChannel17.setSound(Settings.System.DEFAULT_NOTIFICATION_URI, Notification.AUDIO_ATTRIBUTES_DEFAULT);
    notificationChannel17.setBlockable(true);
    arrayList.add(notificationChannel17);
    String str16 = SECURITY;
    NotificationChannel notificationChannel16 = new NotificationChannel(str16, paramContext.getString(17040725), 2);
    arrayList.add(notificationChannel16);
    String str15 = CAR_MODE;
    NotificationChannel notificationChannel15 = new NotificationChannel(str15, paramContext.getString(17040710), 2);
    notificationChannel15.setBlockable(true);
    arrayList.add(notificationChannel15);
    arrayList.add(newAccountChannel(paramContext));
    String str14 = DEVELOPER;
    NotificationChannel notificationChannel14 = new NotificationChannel(str14, paramContext.getString(17040711), 2);
    notificationChannel14.setBlockable(true);
    arrayList.add(notificationChannel14);
    String str19 = DEVELOPER_IMPORTANT;
    NotificationChannel notificationChannel19 = new NotificationChannel(str19, paramContext.getString(17040712), 4);
    notificationChannel14.setBlockable(true);
    arrayList.add(notificationChannel19);
    String str13 = UPDATES;
    NotificationChannel notificationChannel13 = new NotificationChannel(str13, paramContext.getString(17040730), 2);
    arrayList.add(notificationChannel13);
    String str12 = NETWORK_STATUS;
    NotificationChannel notificationChannel12 = new NotificationChannel(str12, paramContext.getString(17040722), 2);
    notificationChannel12.setBlockable(true);
    arrayList.add(notificationChannel12);
    String str11 = NETWORK_ALERTS;
    NotificationChannel notificationChannel11 = new NotificationChannel(str11, paramContext.getString(17040720), 4);
    notificationChannel11.setBlockable(true);
    arrayList.add(notificationChannel11);
    String str10 = NETWORK_AVAILABLE;
    NotificationChannel notificationChannel10 = new NotificationChannel(str10, paramContext.getString(17040721), 2);
    notificationChannel10.setBlockable(true);
    arrayList.add(notificationChannel10);
    String str9 = VPN;
    NotificationChannel notificationChannel9 = new NotificationChannel(str9, paramContext.getString(17040734), 2);
    arrayList.add(notificationChannel9);
    String str8 = DEVICE_ADMIN;
    NotificationChannel notificationChannel8 = new NotificationChannel(str8, paramContext.getString(17040713), 4);
    arrayList.add(notificationChannel8);
    String str7 = ALERTS;
    NotificationChannel notificationChannel7 = new NotificationChannel(str7, paramContext.getString(17040708), 3);
    arrayList.add(notificationChannel7);
    String str6 = RETAIL_MODE;
    NotificationChannel notificationChannel6 = new NotificationChannel(str6, paramContext.getString(17040724), 2);
    arrayList.add(notificationChannel6);
    String str5 = USB;
    NotificationChannel notificationChannel5 = new NotificationChannel(str5, paramContext.getString(17040731), 1);
    arrayList.add(notificationChannel5);
    String str4 = FOREGROUND_SERVICE;
    NotificationChannel notificationChannel4 = new NotificationChannel(str4, paramContext.getString(17040716), 2);
    notificationChannel4.setBlockable(true);
    arrayList.add(notificationChannel4);
    String str3 = HEAVY_WEIGHT_APP;
    NotificationChannel notificationChannel3 = new NotificationChannel(str3, paramContext.getString(17040717), 3);
    notificationChannel3.setShowBadge(false);
    AudioAttributes.Builder builder = new AudioAttributes.Builder();
    builder = builder.setContentType(4);
    builder = builder.setUsage(10);
    AudioAttributes audioAttributes = builder.build();
    notificationChannel3.setSound(null, audioAttributes);
    arrayList.add(notificationChannel3);
    String str2 = SYSTEM_CHANGES;
    NotificationChannel notificationChannel2 = new NotificationChannel(str2, paramContext.getString(17040729), 2);
    arrayList.add(notificationChannel2);
    String str1 = DO_NOT_DISTURB;
    NotificationChannel notificationChannel1 = new NotificationChannel(str1, paramContext.getString(17040714), 2);
    arrayList.add(notificationChannel1);
    notificationManager.createNotificationChannels(arrayList);
  }
  
  public static void removeDeprecated(Context paramContext) {
    NotificationManager notificationManager = (NotificationManager)paramContext.getSystemService(NotificationManager.class);
    notificationManager.deleteNotificationChannel(DEVICE_ADMIN_DEPRECATED);
  }
  
  public static void createAccountChannelForPackage(String paramString, int paramInt, Context paramContext) {
    INotificationManager iNotificationManager = NotificationManager.getService();
    try {
      ParceledListSlice parceledListSlice = new ParceledListSlice();
      this(Arrays.asList(new NotificationChannel[] { newAccountChannel(paramContext) }));
      iNotificationManager.createNotificationChannelsForPackage(paramString, paramInt, parceledListSlice);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private static NotificationChannel newAccountChannel(Context paramContext) {
    String str = ACCOUNT;
    return 
      
      new NotificationChannel(str, paramContext.getString(17040707), 2);
  }
}
