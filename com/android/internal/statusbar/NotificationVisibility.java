package com.android.internal.statusbar;

import android.os.Parcel;
import android.os.Parcelable;

public class NotificationVisibility implements Parcelable {
  public static final Parcelable.Creator<NotificationVisibility> CREATOR;
  
  private static final int MAX_POOL_SIZE = 25;
  
  private static final String TAG = "NoViz";
  
  private static int sNexrId = 0;
  
  public int count;
  
  int id;
  
  public String key;
  
  public NotificationLocation location;
  
  public int rank;
  
  public boolean visible = true;
  
  class NotificationLocation extends Enum<NotificationLocation> {
    private static final NotificationLocation[] $VALUES;
    
    public static final NotificationLocation LOCATION_BOTTOM_STACK_HIDDEN;
    
    public static final NotificationLocation LOCATION_BOTTOM_STACK_PEEKING;
    
    public static final NotificationLocation LOCATION_FIRST_HEADS_UP = new NotificationLocation("LOCATION_FIRST_HEADS_UP", 1, 1);
    
    public static final NotificationLocation LOCATION_GONE;
    
    public static final NotificationLocation LOCATION_HIDDEN_TOP = new NotificationLocation("LOCATION_HIDDEN_TOP", 2, 2);
    
    public static final NotificationLocation LOCATION_MAIN_AREA = new NotificationLocation("LOCATION_MAIN_AREA", 3, 3);
    
    public static NotificationLocation valueOf(String param1String) {
      return Enum.<NotificationLocation>valueOf(NotificationLocation.class, param1String);
    }
    
    public static NotificationLocation[] values() {
      return (NotificationLocation[])$VALUES.clone();
    }
    
    public static final NotificationLocation LOCATION_UNKNOWN = new NotificationLocation("LOCATION_UNKNOWN", 0, 0);
    
    private final int mMetricsEventNotificationLocation;
    
    static {
      LOCATION_BOTTOM_STACK_PEEKING = new NotificationLocation("LOCATION_BOTTOM_STACK_PEEKING", 4, 4);
      LOCATION_BOTTOM_STACK_HIDDEN = new NotificationLocation("LOCATION_BOTTOM_STACK_HIDDEN", 5, 5);
      NotificationLocation notificationLocation = new NotificationLocation("LOCATION_GONE", 6, 6);
      $VALUES = new NotificationLocation[] { LOCATION_UNKNOWN, LOCATION_FIRST_HEADS_UP, LOCATION_HIDDEN_TOP, LOCATION_MAIN_AREA, LOCATION_BOTTOM_STACK_PEEKING, LOCATION_BOTTOM_STACK_HIDDEN, notificationLocation };
    }
    
    private NotificationLocation(NotificationVisibility this$0, int param1Int1, int param1Int2) {
      super((String)this$0, param1Int1);
      this.mMetricsEventNotificationLocation = param1Int2;
    }
    
    public int toMetricsEventEnum() {
      return this.mMetricsEventNotificationLocation;
    }
  }
  
  private NotificationVisibility() {
    int i = sNexrId;
    sNexrId = i + 1;
    this.id = i;
  }
  
  private NotificationVisibility(String paramString, int paramInt1, int paramInt2, boolean paramBoolean, NotificationLocation paramNotificationLocation) {
    this();
    this.key = paramString;
    this.rank = paramInt1;
    this.count = paramInt2;
    this.visible = paramBoolean;
    this.location = paramNotificationLocation;
  }
  
  public String toString() {
    String str;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("NotificationVisibility(id=");
    stringBuilder.append(this.id);
    stringBuilder.append(" key=");
    stringBuilder.append(this.key);
    stringBuilder.append(" rank=");
    stringBuilder.append(this.rank);
    stringBuilder.append(" count=");
    stringBuilder.append(this.count);
    if (this.visible) {
      str = " visible";
    } else {
      str = "";
    } 
    stringBuilder.append(str);
    stringBuilder.append(" location=");
    NotificationLocation notificationLocation = this.location;
    stringBuilder.append(notificationLocation.name());
    stringBuilder.append(" )");
    return stringBuilder.toString();
  }
  
  public NotificationVisibility clone() {
    return obtain(this.key, this.rank, this.count, this.visible, this.location);
  }
  
  public int hashCode() {
    int i;
    String str = this.key;
    if (str == null) {
      i = 0;
    } else {
      i = str.hashCode();
    } 
    return i;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof NotificationVisibility;
    boolean bool1 = false;
    if (bool) {
      paramObject = paramObject;
      if ((this.key == null && ((NotificationVisibility)paramObject).key == null) || this.key.equals(((NotificationVisibility)paramObject).key))
        bool1 = true; 
      return bool1;
    } 
    return false;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.key);
    paramParcel.writeInt(this.rank);
    paramParcel.writeInt(this.count);
    paramParcel.writeInt(this.visible);
    paramParcel.writeString(this.location.name());
  }
  
  private void readFromParcel(Parcel paramParcel) {
    boolean bool;
    this.key = paramParcel.readString();
    this.rank = paramParcel.readInt();
    this.count = paramParcel.readInt();
    if (paramParcel.readInt() != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    this.visible = bool;
    this.location = NotificationLocation.valueOf(paramParcel.readString());
  }
  
  public static NotificationVisibility obtain(String paramString, int paramInt1, int paramInt2, boolean paramBoolean) {
    return obtain(paramString, paramInt1, paramInt2, paramBoolean, NotificationLocation.LOCATION_UNKNOWN);
  }
  
  public static NotificationVisibility obtain(String paramString, int paramInt1, int paramInt2, boolean paramBoolean, NotificationLocation paramNotificationLocation) {
    NotificationVisibility notificationVisibility = obtain();
    notificationVisibility.key = paramString;
    notificationVisibility.rank = paramInt1;
    notificationVisibility.count = paramInt2;
    notificationVisibility.visible = paramBoolean;
    notificationVisibility.location = paramNotificationLocation;
    return notificationVisibility;
  }
  
  private static NotificationVisibility obtain(Parcel paramParcel) {
    NotificationVisibility notificationVisibility = obtain();
    notificationVisibility.readFromParcel(paramParcel);
    return notificationVisibility;
  }
  
  private static NotificationVisibility obtain() {
    return new NotificationVisibility();
  }
  
  public void recycle() {}
  
  static {
    CREATOR = new Parcelable.Creator<NotificationVisibility>() {
        public NotificationVisibility createFromParcel(Parcel param1Parcel) {
          return NotificationVisibility.obtain(param1Parcel);
        }
        
        public NotificationVisibility[] newArray(int param1Int) {
          return new NotificationVisibility[param1Int];
        }
      };
  }
}
