package com.android.internal.statusbar;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.ArrayMap;
import com.android.internal.view.AppearanceRegion;

public final class RegisterStatusBarResult implements Parcelable {
  public RegisterStatusBarResult(ArrayMap<String, StatusBarIcon> paramArrayMap, int paramInt1, int paramInt2, AppearanceRegion[] paramArrayOfAppearanceRegion, int paramInt3, int paramInt4, boolean paramBoolean1, int paramInt5, IBinder paramIBinder, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, int[] paramArrayOfint) {
    this.mIcons = new ArrayMap(paramArrayMap);
    this.mDisabledFlags1 = paramInt1;
    this.mAppearance = paramInt2;
    this.mAppearanceRegions = paramArrayOfAppearanceRegion;
    this.mImeWindowVis = paramInt3;
    this.mImeBackDisposition = paramInt4;
    this.mShowImeSwitcher = paramBoolean1;
    this.mDisabledFlags2 = paramInt5;
    this.mImeToken = paramIBinder;
    this.mNavbarColorManagedByIme = paramBoolean2;
    this.mAppFullscreen = paramBoolean3;
    this.mAppImmersive = paramBoolean4;
    this.mTransientBarTypes = paramArrayOfint;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeTypedArrayMap(this.mIcons, paramInt);
    paramParcel.writeInt(this.mDisabledFlags1);
    paramParcel.writeInt(this.mAppearance);
    paramParcel.writeParcelableArray((Parcelable[])this.mAppearanceRegions, 0);
    paramParcel.writeInt(this.mImeWindowVis);
    paramParcel.writeInt(this.mImeBackDisposition);
    paramParcel.writeBoolean(this.mShowImeSwitcher);
    paramParcel.writeInt(this.mDisabledFlags2);
    paramParcel.writeStrongBinder(this.mImeToken);
    paramParcel.writeBoolean(this.mNavbarColorManagedByIme);
    paramParcel.writeBoolean(this.mAppFullscreen);
    paramParcel.writeBoolean(this.mAppImmersive);
    paramParcel.writeIntArray(this.mTransientBarTypes);
  }
  
  public static final Parcelable.Creator<RegisterStatusBarResult> CREATOR = new Parcelable.Creator<RegisterStatusBarResult>() {
      public RegisterStatusBarResult createFromParcel(Parcel param1Parcel) {
        Parcelable.Creator<StatusBarIcon> creator = StatusBarIcon.CREATOR;
        ArrayMap<String, StatusBarIcon> arrayMap = param1Parcel.createTypedArrayMap(creator);
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        AppearanceRegion[] arrayOfAppearanceRegion = (AppearanceRegion[])param1Parcel.readParcelableArray(null, AppearanceRegion.class);
        int k = param1Parcel.readInt();
        int m = param1Parcel.readInt();
        boolean bool1 = param1Parcel.readBoolean();
        int n = param1Parcel.readInt();
        IBinder iBinder = param1Parcel.readStrongBinder();
        boolean bool2 = param1Parcel.readBoolean();
        boolean bool3 = param1Parcel.readBoolean();
        boolean bool4 = param1Parcel.readBoolean();
        int[] arrayOfInt = param1Parcel.createIntArray();
        return new RegisterStatusBarResult(arrayMap, i, j, arrayOfAppearanceRegion, k, m, bool1, n, iBinder, bool2, bool3, bool4, arrayOfInt);
      }
      
      public RegisterStatusBarResult[] newArray(int param1Int) {
        return new RegisterStatusBarResult[param1Int];
      }
    };
  
  public final boolean mAppFullscreen;
  
  public final boolean mAppImmersive;
  
  public final int mAppearance;
  
  public final AppearanceRegion[] mAppearanceRegions;
  
  public final int mDisabledFlags1;
  
  public final int mDisabledFlags2;
  
  public final ArrayMap<String, StatusBarIcon> mIcons;
  
  public final int mImeBackDisposition;
  
  public final IBinder mImeToken;
  
  public final int mImeWindowVis;
  
  public final boolean mNavbarColorManagedByIme;
  
  public final boolean mShowImeSwitcher;
  
  public final int[] mTransientBarTypes;
}
