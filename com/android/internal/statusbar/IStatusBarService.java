package com.android.internal.statusbar;

import android.app.Notification;
import android.content.ComponentName;
import android.hardware.biometrics.IBiometricServiceReceiverInternal;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.UserHandle;
import android.text.TextUtils;

public interface IStatusBarService extends IInterface {
  void addTile(ComponentName paramComponentName) throws RemoteException;
  
  void clearInlineReplyUriPermissions(String paramString) throws RemoteException;
  
  void clearNotificationEffects() throws RemoteException;
  
  void clickTile(ComponentName paramComponentName) throws RemoteException;
  
  void collapsePanels() throws RemoteException;
  
  void disable(int paramInt, IBinder paramIBinder, String paramString) throws RemoteException;
  
  void disable2(int paramInt, IBinder paramIBinder, String paramString) throws RemoteException;
  
  void disable2ForUser(int paramInt1, IBinder paramIBinder, String paramString, int paramInt2) throws RemoteException;
  
  void disableForUser(int paramInt1, IBinder paramIBinder, String paramString, int paramInt2) throws RemoteException;
  
  void dismissInattentiveSleepWarning(boolean paramBoolean) throws RemoteException;
  
  void expandNotificationsPanel() throws RemoteException;
  
  void expandSettingsPanel(String paramString) throws RemoteException;
  
  int[] getDisableFlags(IBinder paramIBinder, int paramInt) throws RemoteException;
  
  void grantInlineReplyUriPermission(String paramString1, Uri paramUri, UserHandle paramUserHandle, String paramString2) throws RemoteException;
  
  void handleSystemKey(int paramInt) throws RemoteException;
  
  void hideAuthenticationDialog() throws RemoteException;
  
  void hideCurrentInputMethodForBubbles() throws RemoteException;
  
  boolean isTracing() throws RemoteException;
  
  void onBiometricAuthenticated() throws RemoteException;
  
  void onBiometricError(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void onBiometricHelp(String paramString) throws RemoteException;
  
  void onBubbleNotificationSuppressionChanged(String paramString, boolean paramBoolean) throws RemoteException;
  
  void onClearAllNotifications(int paramInt) throws RemoteException;
  
  void onGlobalActionsHidden() throws RemoteException;
  
  void onGlobalActionsShown() throws RemoteException;
  
  void onNotificationActionClick(String paramString, int paramInt, Notification.Action paramAction, NotificationVisibility paramNotificationVisibility, boolean paramBoolean) throws RemoteException;
  
  void onNotificationBubbleChanged(String paramString, boolean paramBoolean, int paramInt) throws RemoteException;
  
  void onNotificationClear(String paramString1, String paramString2, int paramInt1, int paramInt2, String paramString3, int paramInt3, int paramInt4, NotificationVisibility paramNotificationVisibility) throws RemoteException;
  
  void onNotificationClick(String paramString, NotificationVisibility paramNotificationVisibility) throws RemoteException;
  
  void onNotificationDirectReplied(String paramString) throws RemoteException;
  
  void onNotificationError(String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3, String paramString3, int paramInt4) throws RemoteException;
  
  void onNotificationExpansionChanged(String paramString, boolean paramBoolean1, boolean paramBoolean2, int paramInt) throws RemoteException;
  
  void onNotificationSettingsViewed(String paramString) throws RemoteException;
  
  void onNotificationSmartReplySent(String paramString, int paramInt1, CharSequence paramCharSequence, int paramInt2, boolean paramBoolean) throws RemoteException;
  
  void onNotificationSmartSuggestionsAdded(String paramString, int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  void onNotificationVisibilityChanged(NotificationVisibility[] paramArrayOfNotificationVisibility1, NotificationVisibility[] paramArrayOfNotificationVisibility2) throws RemoteException;
  
  void onPanelHidden() throws RemoteException;
  
  void onPanelRevealed(boolean paramBoolean, int paramInt) throws RemoteException;
  
  void reboot(boolean paramBoolean) throws RemoteException;
  
  RegisterStatusBarResult registerStatusBar(IStatusBar paramIStatusBar) throws RemoteException;
  
  void remTile(ComponentName paramComponentName) throws RemoteException;
  
  void removeIcon(String paramString) throws RemoteException;
  
  void setIcon(String paramString1, String paramString2, int paramInt1, int paramInt2, String paramString3) throws RemoteException;
  
  void setIconVisibility(String paramString, boolean paramBoolean) throws RemoteException;
  
  void setImeWindowStatus(int paramInt1, IBinder paramIBinder, int paramInt2, int paramInt3, boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  void showAuthenticationDialog(Bundle paramBundle, IBiometricServiceReceiverInternal paramIBiometricServiceReceiverInternal, int paramInt1, boolean paramBoolean, int paramInt2, String paramString, long paramLong, int paramInt3) throws RemoteException;
  
  void showInattentiveSleepWarning() throws RemoteException;
  
  void showPinningEnterExitToast(boolean paramBoolean) throws RemoteException;
  
  void showPinningEscapeToast() throws RemoteException;
  
  void shutdown() throws RemoteException;
  
  void startTracing() throws RemoteException;
  
  void stopTracing() throws RemoteException;
  
  void suppressAmbientDisplay(boolean paramBoolean) throws RemoteException;
  
  void togglePanel() throws RemoteException;
  
  class Default implements IStatusBarService {
    public void expandNotificationsPanel() throws RemoteException {}
    
    public void collapsePanels() throws RemoteException {}
    
    public void togglePanel() throws RemoteException {}
    
    public void disable(int param1Int, IBinder param1IBinder, String param1String) throws RemoteException {}
    
    public void disableForUser(int param1Int1, IBinder param1IBinder, String param1String, int param1Int2) throws RemoteException {}
    
    public void disable2(int param1Int, IBinder param1IBinder, String param1String) throws RemoteException {}
    
    public void disable2ForUser(int param1Int1, IBinder param1IBinder, String param1String, int param1Int2) throws RemoteException {}
    
    public int[] getDisableFlags(IBinder param1IBinder, int param1Int) throws RemoteException {
      return null;
    }
    
    public void setIcon(String param1String1, String param1String2, int param1Int1, int param1Int2, String param1String3) throws RemoteException {}
    
    public void setIconVisibility(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public void removeIcon(String param1String) throws RemoteException {}
    
    public void setImeWindowStatus(int param1Int1, IBinder param1IBinder, int param1Int2, int param1Int3, boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {}
    
    public void expandSettingsPanel(String param1String) throws RemoteException {}
    
    public RegisterStatusBarResult registerStatusBar(IStatusBar param1IStatusBar) throws RemoteException {
      return null;
    }
    
    public void onPanelRevealed(boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public void onPanelHidden() throws RemoteException {}
    
    public void clearNotificationEffects() throws RemoteException {}
    
    public void onNotificationClick(String param1String, NotificationVisibility param1NotificationVisibility) throws RemoteException {}
    
    public void onNotificationActionClick(String param1String, int param1Int, Notification.Action param1Action, NotificationVisibility param1NotificationVisibility, boolean param1Boolean) throws RemoteException {}
    
    public void onNotificationError(String param1String1, String param1String2, int param1Int1, int param1Int2, int param1Int3, String param1String3, int param1Int4) throws RemoteException {}
    
    public void onClearAllNotifications(int param1Int) throws RemoteException {}
    
    public void onNotificationClear(String param1String1, String param1String2, int param1Int1, int param1Int2, String param1String3, int param1Int3, int param1Int4, NotificationVisibility param1NotificationVisibility) throws RemoteException {}
    
    public void onNotificationVisibilityChanged(NotificationVisibility[] param1ArrayOfNotificationVisibility1, NotificationVisibility[] param1ArrayOfNotificationVisibility2) throws RemoteException {}
    
    public void onNotificationExpansionChanged(String param1String, boolean param1Boolean1, boolean param1Boolean2, int param1Int) throws RemoteException {}
    
    public void onNotificationDirectReplied(String param1String) throws RemoteException {}
    
    public void onNotificationSmartSuggestionsAdded(String param1String, int param1Int1, int param1Int2, boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {}
    
    public void onNotificationSmartReplySent(String param1String, int param1Int1, CharSequence param1CharSequence, int param1Int2, boolean param1Boolean) throws RemoteException {}
    
    public void onNotificationSettingsViewed(String param1String) throws RemoteException {}
    
    public void onNotificationBubbleChanged(String param1String, boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public void onBubbleNotificationSuppressionChanged(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public void hideCurrentInputMethodForBubbles() throws RemoteException {}
    
    public void grantInlineReplyUriPermission(String param1String1, Uri param1Uri, UserHandle param1UserHandle, String param1String2) throws RemoteException {}
    
    public void clearInlineReplyUriPermissions(String param1String) throws RemoteException {}
    
    public void onGlobalActionsShown() throws RemoteException {}
    
    public void onGlobalActionsHidden() throws RemoteException {}
    
    public void shutdown() throws RemoteException {}
    
    public void reboot(boolean param1Boolean) throws RemoteException {}
    
    public void addTile(ComponentName param1ComponentName) throws RemoteException {}
    
    public void remTile(ComponentName param1ComponentName) throws RemoteException {}
    
    public void clickTile(ComponentName param1ComponentName) throws RemoteException {}
    
    public void handleSystemKey(int param1Int) throws RemoteException {}
    
    public void showPinningEnterExitToast(boolean param1Boolean) throws RemoteException {}
    
    public void showPinningEscapeToast() throws RemoteException {}
    
    public void showAuthenticationDialog(Bundle param1Bundle, IBiometricServiceReceiverInternal param1IBiometricServiceReceiverInternal, int param1Int1, boolean param1Boolean, int param1Int2, String param1String, long param1Long, int param1Int3) throws RemoteException {}
    
    public void onBiometricAuthenticated() throws RemoteException {}
    
    public void onBiometricHelp(String param1String) throws RemoteException {}
    
    public void onBiometricError(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void hideAuthenticationDialog() throws RemoteException {}
    
    public void showInattentiveSleepWarning() throws RemoteException {}
    
    public void dismissInattentiveSleepWarning(boolean param1Boolean) throws RemoteException {}
    
    public void startTracing() throws RemoteException {}
    
    public void stopTracing() throws RemoteException {}
    
    public boolean isTracing() throws RemoteException {
      return false;
    }
    
    public void suppressAmbientDisplay(boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IStatusBarService {
    private static final String DESCRIPTOR = "com.android.internal.statusbar.IStatusBarService";
    
    static final int TRANSACTION_addTile = 38;
    
    static final int TRANSACTION_clearInlineReplyUriPermissions = 33;
    
    static final int TRANSACTION_clearNotificationEffects = 17;
    
    static final int TRANSACTION_clickTile = 40;
    
    static final int TRANSACTION_collapsePanels = 2;
    
    static final int TRANSACTION_disable = 4;
    
    static final int TRANSACTION_disable2 = 6;
    
    static final int TRANSACTION_disable2ForUser = 7;
    
    static final int TRANSACTION_disableForUser = 5;
    
    static final int TRANSACTION_dismissInattentiveSleepWarning = 50;
    
    static final int TRANSACTION_expandNotificationsPanel = 1;
    
    static final int TRANSACTION_expandSettingsPanel = 13;
    
    static final int TRANSACTION_getDisableFlags = 8;
    
    static final int TRANSACTION_grantInlineReplyUriPermission = 32;
    
    static final int TRANSACTION_handleSystemKey = 41;
    
    static final int TRANSACTION_hideAuthenticationDialog = 48;
    
    static final int TRANSACTION_hideCurrentInputMethodForBubbles = 31;
    
    static final int TRANSACTION_isTracing = 53;
    
    static final int TRANSACTION_onBiometricAuthenticated = 45;
    
    static final int TRANSACTION_onBiometricError = 47;
    
    static final int TRANSACTION_onBiometricHelp = 46;
    
    static final int TRANSACTION_onBubbleNotificationSuppressionChanged = 30;
    
    static final int TRANSACTION_onClearAllNotifications = 21;
    
    static final int TRANSACTION_onGlobalActionsHidden = 35;
    
    static final int TRANSACTION_onGlobalActionsShown = 34;
    
    static final int TRANSACTION_onNotificationActionClick = 19;
    
    static final int TRANSACTION_onNotificationBubbleChanged = 29;
    
    static final int TRANSACTION_onNotificationClear = 22;
    
    static final int TRANSACTION_onNotificationClick = 18;
    
    static final int TRANSACTION_onNotificationDirectReplied = 25;
    
    static final int TRANSACTION_onNotificationError = 20;
    
    static final int TRANSACTION_onNotificationExpansionChanged = 24;
    
    static final int TRANSACTION_onNotificationSettingsViewed = 28;
    
    static final int TRANSACTION_onNotificationSmartReplySent = 27;
    
    static final int TRANSACTION_onNotificationSmartSuggestionsAdded = 26;
    
    static final int TRANSACTION_onNotificationVisibilityChanged = 23;
    
    static final int TRANSACTION_onPanelHidden = 16;
    
    static final int TRANSACTION_onPanelRevealed = 15;
    
    static final int TRANSACTION_reboot = 37;
    
    static final int TRANSACTION_registerStatusBar = 14;
    
    static final int TRANSACTION_remTile = 39;
    
    static final int TRANSACTION_removeIcon = 11;
    
    static final int TRANSACTION_setIcon = 9;
    
    static final int TRANSACTION_setIconVisibility = 10;
    
    static final int TRANSACTION_setImeWindowStatus = 12;
    
    static final int TRANSACTION_showAuthenticationDialog = 44;
    
    static final int TRANSACTION_showInattentiveSleepWarning = 49;
    
    static final int TRANSACTION_showPinningEnterExitToast = 42;
    
    static final int TRANSACTION_showPinningEscapeToast = 43;
    
    static final int TRANSACTION_shutdown = 36;
    
    static final int TRANSACTION_startTracing = 51;
    
    static final int TRANSACTION_stopTracing = 52;
    
    static final int TRANSACTION_suppressAmbientDisplay = 54;
    
    static final int TRANSACTION_togglePanel = 3;
    
    public Stub() {
      attachInterface(this, "com.android.internal.statusbar.IStatusBarService");
    }
    
    public static IStatusBarService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.statusbar.IStatusBarService");
      if (iInterface != null && iInterface instanceof IStatusBarService)
        return (IStatusBarService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 54:
          return "suppressAmbientDisplay";
        case 53:
          return "isTracing";
        case 52:
          return "stopTracing";
        case 51:
          return "startTracing";
        case 50:
          return "dismissInattentiveSleepWarning";
        case 49:
          return "showInattentiveSleepWarning";
        case 48:
          return "hideAuthenticationDialog";
        case 47:
          return "onBiometricError";
        case 46:
          return "onBiometricHelp";
        case 45:
          return "onBiometricAuthenticated";
        case 44:
          return "showAuthenticationDialog";
        case 43:
          return "showPinningEscapeToast";
        case 42:
          return "showPinningEnterExitToast";
        case 41:
          return "handleSystemKey";
        case 40:
          return "clickTile";
        case 39:
          return "remTile";
        case 38:
          return "addTile";
        case 37:
          return "reboot";
        case 36:
          return "shutdown";
        case 35:
          return "onGlobalActionsHidden";
        case 34:
          return "onGlobalActionsShown";
        case 33:
          return "clearInlineReplyUriPermissions";
        case 32:
          return "grantInlineReplyUriPermission";
        case 31:
          return "hideCurrentInputMethodForBubbles";
        case 30:
          return "onBubbleNotificationSuppressionChanged";
        case 29:
          return "onNotificationBubbleChanged";
        case 28:
          return "onNotificationSettingsViewed";
        case 27:
          return "onNotificationSmartReplySent";
        case 26:
          return "onNotificationSmartSuggestionsAdded";
        case 25:
          return "onNotificationDirectReplied";
        case 24:
          return "onNotificationExpansionChanged";
        case 23:
          return "onNotificationVisibilityChanged";
        case 22:
          return "onNotificationClear";
        case 21:
          return "onClearAllNotifications";
        case 20:
          return "onNotificationError";
        case 19:
          return "onNotificationActionClick";
        case 18:
          return "onNotificationClick";
        case 17:
          return "clearNotificationEffects";
        case 16:
          return "onPanelHidden";
        case 15:
          return "onPanelRevealed";
        case 14:
          return "registerStatusBar";
        case 13:
          return "expandSettingsPanel";
        case 12:
          return "setImeWindowStatus";
        case 11:
          return "removeIcon";
        case 10:
          return "setIconVisibility";
        case 9:
          return "setIcon";
        case 8:
          return "getDisableFlags";
        case 7:
          return "disable2ForUser";
        case 6:
          return "disable2";
        case 5:
          return "disableForUser";
        case 4:
          return "disable";
        case 3:
          return "togglePanel";
        case 2:
          return "collapsePanels";
        case 1:
          break;
      } 
      return "expandNotificationsPanel";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        int i;
        String str3;
        NotificationVisibility[] arrayOfNotificationVisibility1;
        IStatusBar iStatusBar;
        RegisterStatusBarResult registerStatusBarResult;
        String str2;
        int[] arrayOfInt;
        String str1;
        int j;
        Bundle bundle;
        String str7;
        NotificationVisibility[] arrayOfNotificationVisibility2;
        String str6;
        IBinder iBinder3;
        String str5;
        IBinder iBinder2;
        String str4;
        IBinder iBinder1;
        IBiometricServiceReceiverInternal iBiometricServiceReceiverInternal;
        String str8, str10;
        IBinder iBinder4;
        String str9;
        long l;
        int k;
        boolean bool1 = false, bool2 = false, bool3 = false, bool4 = false, bool5 = false, bool6 = false, bool7 = false, bool8 = false, bool9 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 54:
            param1Parcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            if (param1Parcel1.readInt() != 0)
              bool9 = true; 
            suppressAmbientDisplay(bool9);
            param1Parcel2.writeNoException();
            return true;
          case 53:
            param1Parcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            bool = isTracing();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 52:
            param1Parcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            stopTracing();
            param1Parcel2.writeNoException();
            return true;
          case 51:
            param1Parcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            startTracing();
            param1Parcel2.writeNoException();
            return true;
          case 50:
            param1Parcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            bool9 = bool1;
            if (param1Parcel1.readInt() != 0)
              bool9 = true; 
            dismissInattentiveSleepWarning(bool9);
            param1Parcel2.writeNoException();
            return true;
          case 49:
            param1Parcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            showInattentiveSleepWarning();
            param1Parcel2.writeNoException();
            return true;
          case 48:
            param1Parcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            hideAuthenticationDialog();
            param1Parcel2.writeNoException();
            return true;
          case 47:
            param1Parcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            i = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            j = param1Parcel1.readInt();
            onBiometricError(i, param1Int2, j);
            param1Parcel2.writeNoException();
            return true;
          case 46:
            param1Parcel1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            str3 = param1Parcel1.readString();
            onBiometricHelp(str3);
            param1Parcel2.writeNoException();
            return true;
          case 45:
            str3.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            onBiometricAuthenticated();
            param1Parcel2.writeNoException();
            return true;
          case 44:
            str3.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            if (str3.readInt() != 0) {
              bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)str3);
            } else {
              bundle = null;
            } 
            iBiometricServiceReceiverInternal = IBiometricServiceReceiverInternal.Stub.asInterface(str3.readStrongBinder());
            i = str3.readInt();
            if (str3.readInt() != 0) {
              bool9 = true;
            } else {
              bool9 = false;
            } 
            param1Int2 = str3.readInt();
            str10 = str3.readString();
            l = str3.readLong();
            j = str3.readInt();
            showAuthenticationDialog(bundle, iBiometricServiceReceiverInternal, i, bool9, param1Int2, str10, l, j);
            param1Parcel2.writeNoException();
            return true;
          case 43:
            str3.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            showPinningEscapeToast();
            param1Parcel2.writeNoException();
            return true;
          case 42:
            str3.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            bool9 = bool2;
            if (str3.readInt() != 0)
              bool9 = true; 
            showPinningEnterExitToast(bool9);
            param1Parcel2.writeNoException();
            return true;
          case 41:
            str3.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            i = str3.readInt();
            handleSystemKey(i);
            param1Parcel2.writeNoException();
            return true;
          case 40:
            str3.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            if (str3.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)str3);
            } else {
              str3 = null;
            } 
            clickTile((ComponentName)str3);
            param1Parcel2.writeNoException();
            return true;
          case 39:
            str3.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            if (str3.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)str3);
            } else {
              str3 = null;
            } 
            remTile((ComponentName)str3);
            param1Parcel2.writeNoException();
            return true;
          case 38:
            str3.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            if (str3.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)str3);
            } else {
              str3 = null;
            } 
            addTile((ComponentName)str3);
            param1Parcel2.writeNoException();
            return true;
          case 37:
            str3.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            bool9 = bool3;
            if (str3.readInt() != 0)
              bool9 = true; 
            reboot(bool9);
            param1Parcel2.writeNoException();
            return true;
          case 36:
            str3.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            shutdown();
            param1Parcel2.writeNoException();
            return true;
          case 35:
            str3.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            onGlobalActionsHidden();
            param1Parcel2.writeNoException();
            return true;
          case 34:
            str3.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            onGlobalActionsShown();
            param1Parcel2.writeNoException();
            return true;
          case 33:
            str3.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            str3 = str3.readString();
            clearInlineReplyUriPermissions(str3);
            param1Parcel2.writeNoException();
            return true;
          case 32:
            str3.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            str8 = str3.readString();
            if (str3.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel((Parcel)str3);
            } else {
              bundle = null;
            } 
            if (str3.readInt() != 0) {
              UserHandle userHandle = (UserHandle)UserHandle.CREATOR.createFromParcel((Parcel)str3);
            } else {
              str10 = null;
            } 
            str3 = str3.readString();
            grantInlineReplyUriPermission(str8, (Uri)bundle, (UserHandle)str10, str3);
            param1Parcel2.writeNoException();
            return true;
          case 31:
            str3.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            hideCurrentInputMethodForBubbles();
            param1Parcel2.writeNoException();
            return true;
          case 30:
            str3.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            str7 = str3.readString();
            bool9 = bool4;
            if (str3.readInt() != 0)
              bool9 = true; 
            onBubbleNotificationSuppressionChanged(str7, bool9);
            param1Parcel2.writeNoException();
            return true;
          case 29:
            str3.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            str7 = str3.readString();
            bool9 = bool5;
            if (str3.readInt() != 0)
              bool9 = true; 
            i = str3.readInt();
            onNotificationBubbleChanged(str7, bool9, i);
            param1Parcel2.writeNoException();
            return true;
          case 28:
            str3.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            str3 = str3.readString();
            onNotificationSettingsViewed(str3);
            param1Parcel2.writeNoException();
            return true;
          case 27:
            str3.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            str10 = str3.readString();
            i = str3.readInt();
            if (str3.readInt() != 0) {
              CharSequence charSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel((Parcel)str3);
            } else {
              str7 = null;
            } 
            param1Int2 = str3.readInt();
            if (str3.readInt() != 0) {
              bool9 = true;
            } else {
              bool9 = false;
            } 
            onNotificationSmartReplySent(str10, i, str7, param1Int2, bool9);
            param1Parcel2.writeNoException();
            return true;
          case 26:
            str3.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            str7 = str3.readString();
            i = str3.readInt();
            param1Int2 = str3.readInt();
            if (str3.readInt() != 0) {
              bool9 = true;
            } else {
              bool9 = false;
            } 
            if (str3.readInt() != 0) {
              bool6 = true;
            } else {
              bool6 = false;
            } 
            onNotificationSmartSuggestionsAdded(str7, i, param1Int2, bool9, bool6);
            param1Parcel2.writeNoException();
            return true;
          case 25:
            str3.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            str3 = str3.readString();
            onNotificationDirectReplied(str3);
            param1Parcel2.writeNoException();
            return true;
          case 24:
            str3.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            str7 = str3.readString();
            if (str3.readInt() != 0) {
              bool9 = true;
            } else {
              bool9 = false;
            } 
            if (str3.readInt() != 0)
              bool6 = true; 
            i = str3.readInt();
            onNotificationExpansionChanged(str7, bool9, bool6, i);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            str3.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            arrayOfNotificationVisibility2 = (NotificationVisibility[])str3.createTypedArray(NotificationVisibility.CREATOR);
            arrayOfNotificationVisibility1 = (NotificationVisibility[])str3.createTypedArray(NotificationVisibility.CREATOR);
            onNotificationVisibilityChanged(arrayOfNotificationVisibility2, arrayOfNotificationVisibility1);
            param1Parcel2.writeNoException();
            return true;
          case 22:
            arrayOfNotificationVisibility1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            str6 = arrayOfNotificationVisibility1.readString();
            str10 = arrayOfNotificationVisibility1.readString();
            j = arrayOfNotificationVisibility1.readInt();
            i = arrayOfNotificationVisibility1.readInt();
            str8 = arrayOfNotificationVisibility1.readString();
            k = arrayOfNotificationVisibility1.readInt();
            param1Int2 = arrayOfNotificationVisibility1.readInt();
            if (arrayOfNotificationVisibility1.readInt() != 0) {
              NotificationVisibility notificationVisibility = (NotificationVisibility)NotificationVisibility.CREATOR.createFromParcel((Parcel)arrayOfNotificationVisibility1);
            } else {
              arrayOfNotificationVisibility1 = null;
            } 
            onNotificationClear(str6, str10, j, i, str8, k, param1Int2, (NotificationVisibility)arrayOfNotificationVisibility1);
            param1Parcel2.writeNoException();
            return true;
          case 21:
            arrayOfNotificationVisibility1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            i = arrayOfNotificationVisibility1.readInt();
            onClearAllNotifications(i);
            param1Parcel2.writeNoException();
            return true;
          case 20:
            arrayOfNotificationVisibility1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            str10 = arrayOfNotificationVisibility1.readString();
            str6 = arrayOfNotificationVisibility1.readString();
            j = arrayOfNotificationVisibility1.readInt();
            i = arrayOfNotificationVisibility1.readInt();
            param1Int2 = arrayOfNotificationVisibility1.readInt();
            str8 = arrayOfNotificationVisibility1.readString();
            k = arrayOfNotificationVisibility1.readInt();
            onNotificationError(str10, str6, j, i, param1Int2, str8, k);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            arrayOfNotificationVisibility1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            str8 = arrayOfNotificationVisibility1.readString();
            i = arrayOfNotificationVisibility1.readInt();
            if (arrayOfNotificationVisibility1.readInt() != 0) {
              Notification.Action action = (Notification.Action)Notification.Action.CREATOR.createFromParcel((Parcel)arrayOfNotificationVisibility1);
            } else {
              str6 = null;
            } 
            if (arrayOfNotificationVisibility1.readInt() != 0) {
              NotificationVisibility notificationVisibility = (NotificationVisibility)NotificationVisibility.CREATOR.createFromParcel((Parcel)arrayOfNotificationVisibility1);
            } else {
              str10 = null;
            } 
            if (arrayOfNotificationVisibility1.readInt() != 0) {
              bool9 = true;
            } else {
              bool9 = false;
            } 
            onNotificationActionClick(str8, i, (Notification.Action)str6, (NotificationVisibility)str10, bool9);
            param1Parcel2.writeNoException();
            return true;
          case 18:
            arrayOfNotificationVisibility1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            str6 = arrayOfNotificationVisibility1.readString();
            if (arrayOfNotificationVisibility1.readInt() != 0) {
              NotificationVisibility notificationVisibility = (NotificationVisibility)NotificationVisibility.CREATOR.createFromParcel((Parcel)arrayOfNotificationVisibility1);
            } else {
              arrayOfNotificationVisibility1 = null;
            } 
            onNotificationClick(str6, (NotificationVisibility)arrayOfNotificationVisibility1);
            param1Parcel2.writeNoException();
            return true;
          case 17:
            arrayOfNotificationVisibility1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            clearNotificationEffects();
            param1Parcel2.writeNoException();
            return true;
          case 16:
            arrayOfNotificationVisibility1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            onPanelHidden();
            param1Parcel2.writeNoException();
            return true;
          case 15:
            arrayOfNotificationVisibility1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            bool9 = bool7;
            if (arrayOfNotificationVisibility1.readInt() != 0)
              bool9 = true; 
            i = arrayOfNotificationVisibility1.readInt();
            onPanelRevealed(bool9, i);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            arrayOfNotificationVisibility1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            iStatusBar = IStatusBar.Stub.asInterface(arrayOfNotificationVisibility1.readStrongBinder());
            registerStatusBarResult = registerStatusBar(iStatusBar);
            param1Parcel2.writeNoException();
            if (registerStatusBarResult != null) {
              param1Parcel2.writeInt(1);
              registerStatusBarResult.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 13:
            registerStatusBarResult.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            str2 = registerStatusBarResult.readString();
            expandSettingsPanel(str2);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            str2.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            i = str2.readInt();
            iBinder3 = str2.readStrongBinder();
            param1Int2 = str2.readInt();
            j = str2.readInt();
            if (str2.readInt() != 0) {
              bool9 = true;
            } else {
              bool9 = false;
            } 
            if (str2.readInt() != 0) {
              bool6 = true;
            } else {
              bool6 = false;
            } 
            setImeWindowStatus(i, iBinder3, param1Int2, j, bool9, bool6);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            str2.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            str2 = str2.readString();
            removeIcon(str2);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            str2.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            str5 = str2.readString();
            bool9 = bool8;
            if (str2.readInt() != 0)
              bool9 = true; 
            setIconVisibility(str5, bool9);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            str2.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            str10 = str2.readString();
            str5 = str2.readString();
            param1Int2 = str2.readInt();
            i = str2.readInt();
            str2 = str2.readString();
            setIcon(str10, str5, param1Int2, i, str2);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            str2.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            iBinder2 = str2.readStrongBinder();
            i = str2.readInt();
            arrayOfInt = getDisableFlags(iBinder2, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeIntArray(arrayOfInt);
            return true;
          case 7:
            arrayOfInt.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            i = arrayOfInt.readInt();
            iBinder4 = arrayOfInt.readStrongBinder();
            str4 = arrayOfInt.readString();
            param1Int2 = arrayOfInt.readInt();
            disable2ForUser(i, iBinder4, str4, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            arrayOfInt.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            i = arrayOfInt.readInt();
            iBinder1 = arrayOfInt.readStrongBinder();
            str1 = arrayOfInt.readString();
            disable2(i, iBinder1, str1);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            i = str1.readInt();
            iBinder1 = str1.readStrongBinder();
            str9 = str1.readString();
            param1Int2 = str1.readInt();
            disableForUser(i, iBinder1, str9, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            i = str1.readInt();
            iBinder1 = str1.readStrongBinder();
            str1 = str1.readString();
            disable(i, iBinder1, str1);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            togglePanel();
            param1Parcel2.writeNoException();
            return true;
          case 2:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
            collapsePanels();
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("com.android.internal.statusbar.IStatusBarService");
        expandNotificationsPanel();
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("com.android.internal.statusbar.IStatusBarService");
      return true;
    }
    
    private static class Proxy implements IStatusBarService {
      public static IStatusBarService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.statusbar.IStatusBarService";
      }
      
      public void expandNotificationsPanel() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().expandNotificationsPanel();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void collapsePanels() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().collapsePanels();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void togglePanel() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().togglePanel();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disable(int param2Int, IBinder param2IBinder, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          parcel1.writeInt(param2Int);
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().disable(param2Int, param2IBinder, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disableForUser(int param2Int1, IBinder param2IBinder, String param2String, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          parcel1.writeInt(param2Int1);
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().disableForUser(param2Int1, param2IBinder, param2String, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disable2(int param2Int, IBinder param2IBinder, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          parcel1.writeInt(param2Int);
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().disable2(param2Int, param2IBinder, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disable2ForUser(int param2Int1, IBinder param2IBinder, String param2String, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          parcel1.writeInt(param2Int1);
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().disable2ForUser(param2Int1, param2IBinder, param2String, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] getDisableFlags(IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null)
            return IStatusBarService.Stub.getDefaultImpl().getDisableFlags(param2IBinder, param2Int); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setIcon(String param2String1, String param2String2, int param2Int1, int param2Int2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().setIcon(param2String1, param2String2, param2Int1, param2Int2, param2String3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setIconVisibility(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool1 && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().setIconVisibility(param2String, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeIcon(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().removeIcon(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setImeWindowStatus(int param2Int1, IBinder param2IBinder, int param2Int2, int param2Int3, boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeStrongBinder(param2IBinder);
              try {
                parcel1.writeInt(param2Int2);
                try {
                  boolean bool2;
                  parcel1.writeInt(param2Int3);
                  boolean bool1 = true;
                  if (param2Boolean1) {
                    bool2 = true;
                  } else {
                    bool2 = false;
                  } 
                  parcel1.writeInt(bool2);
                  if (param2Boolean2) {
                    bool2 = bool1;
                  } else {
                    bool2 = false;
                  } 
                  parcel1.writeInt(bool2);
                  try {
                    boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
                    if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
                      IStatusBarService.Stub.getDefaultImpl().setImeWindowStatus(param2Int1, param2IBinder, param2Int2, param2Int3, param2Boolean1, param2Boolean2);
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } 
                    parcel2.readException();
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IBinder;
      }
      
      public void expandSettingsPanel(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().expandSettingsPanel(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public RegisterStatusBarResult registerStatusBar(IStatusBar param2IStatusBar) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          if (param2IStatusBar != null) {
            iBinder = param2IStatusBar.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null)
            return IStatusBarService.Stub.getDefaultImpl().registerStatusBar(param2IStatusBar); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            RegisterStatusBarResult registerStatusBarResult = (RegisterStatusBarResult)RegisterStatusBarResult.CREATOR.createFromParcel(parcel2);
          } else {
            param2IStatusBar = null;
          } 
          return (RegisterStatusBarResult)param2IStatusBar;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onPanelRevealed(boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool1 && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().onPanelRevealed(param2Boolean, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onPanelHidden() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().onPanelHidden();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearNotificationEffects() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().clearNotificationEffects();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onNotificationClick(String param2String, NotificationVisibility param2NotificationVisibility) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          parcel1.writeString(param2String);
          if (param2NotificationVisibility != null) {
            parcel1.writeInt(1);
            param2NotificationVisibility.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().onNotificationClick(param2String, param2NotificationVisibility);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onNotificationActionClick(String param2String, int param2Int, Notification.Action param2Action, NotificationVisibility param2NotificationVisibility, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = true;
          if (param2Action != null) {
            parcel1.writeInt(1);
            param2Action.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2NotificationVisibility != null) {
            parcel1.writeInt(1);
            param2NotificationVisibility.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool1 && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().onNotificationActionClick(param2String, param2Int, param2Action, param2NotificationVisibility, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onNotificationError(String param2String1, String param2String2, int param2Int1, int param2Int2, int param2Int3, String param2String3, int param2Int4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeString(param2String2);
              try {
                parcel1.writeInt(param2Int1);
                try {
                  parcel1.writeInt(param2Int2);
                  try {
                    parcel1.writeInt(param2Int3);
                    parcel1.writeString(param2String3);
                    parcel1.writeInt(param2Int4);
                    boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
                    if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
                      IStatusBarService.Stub.getDefaultImpl().onNotificationError(param2String1, param2String2, param2Int1, param2Int2, param2Int3, param2String3, param2Int4);
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } 
                    parcel2.readException();
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void onClearAllNotifications(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().onClearAllNotifications(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onNotificationClear(String param2String1, String param2String2, int param2Int1, int param2Int2, String param2String3, int param2Int3, int param2Int4, NotificationVisibility param2NotificationVisibility) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeString(param2String2);
              try {
                parcel1.writeInt(param2Int1);
                parcel1.writeInt(param2Int2);
                parcel1.writeString(param2String3);
                parcel1.writeInt(param2Int3);
                parcel1.writeInt(param2Int4);
                if (param2NotificationVisibility != null) {
                  parcel1.writeInt(1);
                  param2NotificationVisibility.writeToParcel(parcel1, 0);
                } else {
                  parcel1.writeInt(0);
                } 
                boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
                if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
                  IStatusBarService.Stub.getDefaultImpl().onNotificationClear(param2String1, param2String2, param2Int1, param2Int2, param2String3, param2Int3, param2Int4, param2NotificationVisibility);
                  parcel2.recycle();
                  parcel1.recycle();
                  return;
                } 
                parcel2.readException();
                parcel2.recycle();
                parcel1.recycle();
                return;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void onNotificationVisibilityChanged(NotificationVisibility[] param2ArrayOfNotificationVisibility1, NotificationVisibility[] param2ArrayOfNotificationVisibility2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          parcel1.writeTypedArray((Parcelable[])param2ArrayOfNotificationVisibility1, 0);
          parcel1.writeTypedArray((Parcelable[])param2ArrayOfNotificationVisibility2, 0);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().onNotificationVisibilityChanged(param2ArrayOfNotificationVisibility1, param2ArrayOfNotificationVisibility2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onNotificationExpansionChanged(String param2String, boolean param2Boolean1, boolean param2Boolean2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          parcel1.writeString(param2String);
          boolean bool1 = true;
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          if (param2Boolean2) {
            bool2 = bool1;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().onNotificationExpansionChanged(param2String, param2Boolean1, param2Boolean2, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onNotificationDirectReplied(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().onNotificationDirectReplied(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onNotificationSmartSuggestionsAdded(String param2String, int param2Int1, int param2Int2, boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool1 = true;
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          if (param2Boolean2) {
            bool2 = bool1;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().onNotificationSmartSuggestionsAdded(param2String, param2Int1, param2Int2, param2Boolean1, param2Boolean2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onNotificationSmartReplySent(String param2String, int param2Int1, CharSequence param2CharSequence, int param2Int2, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          boolean bool = true;
          if (param2CharSequence != null) {
            parcel1.writeInt(1);
            TextUtils.writeToParcel(param2CharSequence, parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int2);
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool1 && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().onNotificationSmartReplySent(param2String, param2Int1, param2CharSequence, param2Int2, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onNotificationSettingsViewed(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().onNotificationSettingsViewed(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onNotificationBubbleChanged(String param2String, boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool1 && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().onNotificationBubbleChanged(param2String, param2Boolean, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onBubbleNotificationSuppressionChanged(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool1 && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().onBubbleNotificationSuppressionChanged(param2String, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void hideCurrentInputMethodForBubbles() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().hideCurrentInputMethodForBubbles();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void grantInlineReplyUriPermission(String param2String1, Uri param2Uri, UserHandle param2UserHandle, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          parcel1.writeString(param2String1);
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2UserHandle != null) {
            parcel1.writeInt(1);
            param2UserHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().grantInlineReplyUriPermission(param2String1, param2Uri, param2UserHandle, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearInlineReplyUriPermissions(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().clearInlineReplyUriPermissions(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onGlobalActionsShown() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          boolean bool = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().onGlobalActionsShown();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onGlobalActionsHidden() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          boolean bool = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().onGlobalActionsHidden();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void shutdown() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().shutdown();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reboot(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool1 && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().reboot(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addTile(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(38, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().addTile(param2ComponentName);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void remTile(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().remTile(param2ComponentName);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clickTile(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(40, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().clickTile(param2ComponentName);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void handleSystemKey(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(41, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().handleSystemKey(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void showPinningEnterExitToast(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(42, parcel1, parcel2, 0);
          if (!bool1 && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().showPinningEnterExitToast(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void showPinningEscapeToast() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          boolean bool = this.mRemote.transact(43, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().showPinningEscapeToast();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void showAuthenticationDialog(Bundle param2Bundle, IBiometricServiceReceiverInternal param2IBiometricServiceReceiverInternal, int param2Int1, boolean param2Boolean, int param2Int2, String param2String, long param2Long, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          boolean bool = true;
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2IBiometricServiceReceiverInternal != null) {
            iBinder = param2IBiometricServiceReceiverInternal.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          try {
            parcel1.writeInt(param2Int1);
            if (!param2Boolean)
              bool = false; 
            parcel1.writeInt(bool);
            try {
              parcel1.writeInt(param2Int2);
              parcel1.writeString(param2String);
              parcel1.writeLong(param2Long);
              parcel1.writeInt(param2Int3);
              boolean bool1 = this.mRemote.transact(44, parcel1, parcel2, 0);
              if (!bool1 && IStatusBarService.Stub.getDefaultImpl() != null) {
                IStatusBarService.Stub.getDefaultImpl().showAuthenticationDialog(param2Bundle, param2IBiometricServiceReceiverInternal, param2Int1, param2Boolean, param2Int2, param2String, param2Long, param2Int3);
                parcel2.recycle();
                parcel1.recycle();
                return;
              } 
              parcel2.readException();
              parcel2.recycle();
              parcel1.recycle();
              return;
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2Bundle;
      }
      
      public void onBiometricAuthenticated() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          boolean bool = this.mRemote.transact(45, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().onBiometricAuthenticated();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onBiometricHelp(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(46, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().onBiometricHelp(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onBiometricError(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(47, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().onBiometricError(param2Int1, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void hideAuthenticationDialog() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          boolean bool = this.mRemote.transact(48, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().hideAuthenticationDialog();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void showInattentiveSleepWarning() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          boolean bool = this.mRemote.transact(49, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().showInattentiveSleepWarning();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void dismissInattentiveSleepWarning(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(50, parcel1, parcel2, 0);
          if (!bool1 && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().dismissInattentiveSleepWarning(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startTracing() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          boolean bool = this.mRemote.transact(51, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().startTracing();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopTracing() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          boolean bool = this.mRemote.transact(52, parcel1, parcel2, 0);
          if (!bool && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().stopTracing();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isTracing() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(53, parcel1, parcel2, 0);
          if (!bool2 && IStatusBarService.Stub.getDefaultImpl() != null) {
            bool1 = IStatusBarService.Stub.getDefaultImpl().isTracing();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void suppressAmbientDisplay(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.statusbar.IStatusBarService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(54, parcel1, parcel2, 0);
          if (!bool1 && IStatusBarService.Stub.getDefaultImpl() != null) {
            IStatusBarService.Stub.getDefaultImpl().suppressAmbientDisplay(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IStatusBarService param1IStatusBarService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IStatusBarService != null) {
          Proxy.sDefaultImpl = param1IStatusBarService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IStatusBarService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
