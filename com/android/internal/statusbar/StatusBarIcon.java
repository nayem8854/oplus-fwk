package com.android.internal.statusbar;

import android.graphics.drawable.Icon;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.UserHandle;
import android.text.TextUtils;

public class StatusBarIcon implements Parcelable {
  public boolean visible = true;
  
  public UserHandle user;
  
  public String pkg;
  
  public int number;
  
  public int iconLevel;
  
  public Icon icon;
  
  public CharSequence contentDescription;
  
  public StatusBarIcon(UserHandle paramUserHandle, String paramString, Icon paramIcon, int paramInt1, int paramInt2, CharSequence paramCharSequence) {
    Icon icon = paramIcon;
    if (paramIcon.getType() == 2) {
      icon = paramIcon;
      if (TextUtils.isEmpty(paramIcon.getResPackage()))
        icon = Icon.createWithResource(paramString, paramIcon.getResId()); 
    } 
    this.pkg = paramString;
    this.user = paramUserHandle;
    this.icon = icon;
    this.iconLevel = paramInt1;
    this.number = paramInt2;
    this.contentDescription = paramCharSequence;
  }
  
  public StatusBarIcon(String paramString, UserHandle paramUserHandle, int paramInt1, int paramInt2, int paramInt3, CharSequence paramCharSequence) {
    this(paramUserHandle, paramString, Icon.createWithResource(paramString, paramInt1), paramInt2, paramInt3, paramCharSequence);
  }
  
  public String toString() {
    String str2;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("StatusBarIcon(icon=");
    stringBuilder.append(this.icon);
    int i = this.iconLevel;
    String str1 = "";
    if (i != 0) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(" level=");
      stringBuilder1.append(this.iconLevel);
      str2 = stringBuilder1.toString();
    } else {
      str2 = "";
    } 
    stringBuilder.append(str2);
    if (this.visible) {
      str2 = " visible";
    } else {
      str2 = "";
    } 
    stringBuilder.append(str2);
    stringBuilder.append(" user=");
    UserHandle userHandle = this.user;
    stringBuilder.append(userHandle.getIdentifier());
    null = str1;
    if (this.number != 0) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(" num=");
      stringBuilder1.append(this.number);
      null = stringBuilder1.toString();
    } 
    stringBuilder.append(null);
    stringBuilder.append(" )");
    return stringBuilder.toString();
  }
  
  public StatusBarIcon clone() {
    StatusBarIcon statusBarIcon = new StatusBarIcon(this.user, this.pkg, this.icon, this.iconLevel, this.number, this.contentDescription);
    statusBarIcon.visible = this.visible;
    return statusBarIcon;
  }
  
  public StatusBarIcon(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    boolean bool;
    this.icon = (Icon)paramParcel.readParcelable(null);
    this.pkg = paramParcel.readString();
    this.user = (UserHandle)paramParcel.readParcelable(null);
    this.iconLevel = paramParcel.readInt();
    if (paramParcel.readInt() != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    this.visible = bool;
    this.number = paramParcel.readInt();
    this.contentDescription = paramParcel.readCharSequence();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable((Parcelable)this.icon, 0);
    paramParcel.writeString(this.pkg);
    paramParcel.writeParcelable((Parcelable)this.user, 0);
    paramParcel.writeInt(this.iconLevel);
    paramParcel.writeInt(this.visible);
    paramParcel.writeInt(this.number);
    paramParcel.writeCharSequence(this.contentDescription);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<StatusBarIcon> CREATOR = new Parcelable.Creator<StatusBarIcon>() {
      public StatusBarIcon createFromParcel(Parcel param1Parcel) {
        return new StatusBarIcon(param1Parcel);
      }
      
      public StatusBarIcon[] newArray(int param1Int) {
        return new StatusBarIcon[param1Int];
      }
    };
}
