package com.android.internal.statusbar;

import android.app.IOplusStatusBar;
import android.os.RemoteException;

public interface IOplusStatusBarService {
  public static final String DESCRIPTOR = "com.android.internal.statusbar.IStatusBarService";
  
  public static final int NOTIFY_CLICK_TOP = 20004;
  
  public static final int OPLUS_CALL_TRANSACTION_INDEX = 20000;
  
  public static final int OPLUS_FIRST_CALL_TRANSACTION = 20001;
  
  public static final int REGISTER_OPLUS_CLICK_TOP = 20003;
  
  public static final int REGISTER_OPLUS_STATUS_BAR = 20002;
  
  public static final int UNREGISTER_OPLUS_CLICK_TOP = 20005;
  
  void registerOplusStatusBar(IOplusStatusBar paramIOplusStatusBar) throws RemoteException;
}
