package com.android.internal.statusbar;

import android.app.ITransientNotificationCallback;
import android.content.ComponentName;
import android.hardware.biometrics.IBiometricServiceReceiverInternal;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.text.TextUtils;
import com.android.internal.view.AppearanceRegion;

public interface IStatusBar extends IInterface {
  void abortTransient(int paramInt, int[] paramArrayOfint) throws RemoteException;
  
  void addQsTile(ComponentName paramComponentName) throws RemoteException;
  
  void animateCollapsePanels() throws RemoteException;
  
  void animateExpandNotificationsPanel() throws RemoteException;
  
  void animateExpandSettingsPanel(String paramString) throws RemoteException;
  
  void appTransitionCancelled(int paramInt) throws RemoteException;
  
  void appTransitionFinished(int paramInt) throws RemoteException;
  
  void appTransitionPending(int paramInt) throws RemoteException;
  
  void appTransitionStarting(int paramInt, long paramLong1, long paramLong2) throws RemoteException;
  
  void cancelPreloadRecentApps() throws RemoteException;
  
  void clickQsTile(ComponentName paramComponentName) throws RemoteException;
  
  void disable(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void dismissInattentiveSleepWarning(boolean paramBoolean) throws RemoteException;
  
  void dismissKeyboardShortcutsMenu() throws RemoteException;
  
  void handleSystemKey(int paramInt) throws RemoteException;
  
  void hideAuthenticationDialog() throws RemoteException;
  
  void hideRecentApps(boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  void hideToast(String paramString, IBinder paramIBinder) throws RemoteException;
  
  void onBiometricAuthenticated() throws RemoteException;
  
  void onBiometricError(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void onBiometricHelp(String paramString) throws RemoteException;
  
  void onCameraLaunchGestureDetected(int paramInt) throws RemoteException;
  
  void onDisplayReady(int paramInt) throws RemoteException;
  
  void onProposedRotationChanged(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void onRecentsAnimationStateChanged(boolean paramBoolean) throws RemoteException;
  
  void onSystemBarAppearanceChanged(int paramInt1, int paramInt2, AppearanceRegion[] paramArrayOfAppearanceRegion, boolean paramBoolean) throws RemoteException;
  
  void preloadRecentApps() throws RemoteException;
  
  void remQsTile(ComponentName paramComponentName) throws RemoteException;
  
  void removeIcon(String paramString) throws RemoteException;
  
  void setIcon(String paramString, StatusBarIcon paramStatusBarIcon) throws RemoteException;
  
  void setImeWindowStatus(int paramInt1, IBinder paramIBinder, int paramInt2, int paramInt3, boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  void setTopAppHidesStatusBar(boolean paramBoolean) throws RemoteException;
  
  void setWindowState(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void showAssistDisclosure() throws RemoteException;
  
  void showAuthenticationDialog(Bundle paramBundle, IBiometricServiceReceiverInternal paramIBiometricServiceReceiverInternal, int paramInt1, boolean paramBoolean, int paramInt2, String paramString, long paramLong, int paramInt3) throws RemoteException;
  
  void showGlobalActionsMenu() throws RemoteException;
  
  void showInattentiveSleepWarning() throws RemoteException;
  
  void showPictureInPictureMenu() throws RemoteException;
  
  void showPinningEnterExitToast(boolean paramBoolean) throws RemoteException;
  
  void showPinningEscapeToast() throws RemoteException;
  
  void showRecentApps(boolean paramBoolean) throws RemoteException;
  
  void showScreenPinningRequest(int paramInt) throws RemoteException;
  
  void showShutdownUi(boolean paramBoolean, String paramString) throws RemoteException;
  
  void showToast(int paramInt1, String paramString, IBinder paramIBinder1, CharSequence paramCharSequence, IBinder paramIBinder2, int paramInt2, ITransientNotificationCallback paramITransientNotificationCallback) throws RemoteException;
  
  void showTransient(int paramInt, int[] paramArrayOfint) throws RemoteException;
  
  void showWirelessChargingAnimation(int paramInt) throws RemoteException;
  
  void startAssist(Bundle paramBundle) throws RemoteException;
  
  void startTracing() throws RemoteException;
  
  void stopTracing() throws RemoteException;
  
  void suppressAmbientDisplay(boolean paramBoolean) throws RemoteException;
  
  void toggleKeyboardShortcutsMenu(int paramInt) throws RemoteException;
  
  void togglePanel() throws RemoteException;
  
  void toggleRecentApps() throws RemoteException;
  
  void toggleSplitScreen() throws RemoteException;
  
  void topAppWindowChanged(int paramInt, boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  class Default implements IStatusBar {
    public void setIcon(String param1String, StatusBarIcon param1StatusBarIcon) throws RemoteException {}
    
    public void removeIcon(String param1String) throws RemoteException {}
    
    public void disable(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void animateExpandNotificationsPanel() throws RemoteException {}
    
    public void animateExpandSettingsPanel(String param1String) throws RemoteException {}
    
    public void animateCollapsePanels() throws RemoteException {}
    
    public void togglePanel() throws RemoteException {}
    
    public void showWirelessChargingAnimation(int param1Int) throws RemoteException {}
    
    public void topAppWindowChanged(int param1Int, boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {}
    
    public void setImeWindowStatus(int param1Int1, IBinder param1IBinder, int param1Int2, int param1Int3, boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {}
    
    public void setWindowState(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void showRecentApps(boolean param1Boolean) throws RemoteException {}
    
    public void hideRecentApps(boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {}
    
    public void toggleRecentApps() throws RemoteException {}
    
    public void toggleSplitScreen() throws RemoteException {}
    
    public void preloadRecentApps() throws RemoteException {}
    
    public void cancelPreloadRecentApps() throws RemoteException {}
    
    public void showScreenPinningRequest(int param1Int) throws RemoteException {}
    
    public void dismissKeyboardShortcutsMenu() throws RemoteException {}
    
    public void toggleKeyboardShortcutsMenu(int param1Int) throws RemoteException {}
    
    public void appTransitionPending(int param1Int) throws RemoteException {}
    
    public void appTransitionCancelled(int param1Int) throws RemoteException {}
    
    public void appTransitionStarting(int param1Int, long param1Long1, long param1Long2) throws RemoteException {}
    
    public void appTransitionFinished(int param1Int) throws RemoteException {}
    
    public void showAssistDisclosure() throws RemoteException {}
    
    public void startAssist(Bundle param1Bundle) throws RemoteException {}
    
    public void onCameraLaunchGestureDetected(int param1Int) throws RemoteException {}
    
    public void showPictureInPictureMenu() throws RemoteException {}
    
    public void showGlobalActionsMenu() throws RemoteException {}
    
    public void onProposedRotationChanged(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void setTopAppHidesStatusBar(boolean param1Boolean) throws RemoteException {}
    
    public void addQsTile(ComponentName param1ComponentName) throws RemoteException {}
    
    public void remQsTile(ComponentName param1ComponentName) throws RemoteException {}
    
    public void clickQsTile(ComponentName param1ComponentName) throws RemoteException {}
    
    public void handleSystemKey(int param1Int) throws RemoteException {}
    
    public void showPinningEnterExitToast(boolean param1Boolean) throws RemoteException {}
    
    public void showPinningEscapeToast() throws RemoteException {}
    
    public void showShutdownUi(boolean param1Boolean, String param1String) throws RemoteException {}
    
    public void showAuthenticationDialog(Bundle param1Bundle, IBiometricServiceReceiverInternal param1IBiometricServiceReceiverInternal, int param1Int1, boolean param1Boolean, int param1Int2, String param1String, long param1Long, int param1Int3) throws RemoteException {}
    
    public void onBiometricAuthenticated() throws RemoteException {}
    
    public void onBiometricHelp(String param1String) throws RemoteException {}
    
    public void onBiometricError(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void hideAuthenticationDialog() throws RemoteException {}
    
    public void onDisplayReady(int param1Int) throws RemoteException {}
    
    public void onRecentsAnimationStateChanged(boolean param1Boolean) throws RemoteException {}
    
    public void onSystemBarAppearanceChanged(int param1Int1, int param1Int2, AppearanceRegion[] param1ArrayOfAppearanceRegion, boolean param1Boolean) throws RemoteException {}
    
    public void showTransient(int param1Int, int[] param1ArrayOfint) throws RemoteException {}
    
    public void abortTransient(int param1Int, int[] param1ArrayOfint) throws RemoteException {}
    
    public void showInattentiveSleepWarning() throws RemoteException {}
    
    public void dismissInattentiveSleepWarning(boolean param1Boolean) throws RemoteException {}
    
    public void showToast(int param1Int1, String param1String, IBinder param1IBinder1, CharSequence param1CharSequence, IBinder param1IBinder2, int param1Int2, ITransientNotificationCallback param1ITransientNotificationCallback) throws RemoteException {}
    
    public void hideToast(String param1String, IBinder param1IBinder) throws RemoteException {}
    
    public void startTracing() throws RemoteException {}
    
    public void stopTracing() throws RemoteException {}
    
    public void suppressAmbientDisplay(boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IStatusBar {
    private static final String DESCRIPTOR = "com.android.internal.statusbar.IStatusBar";
    
    static final int TRANSACTION_abortTransient = 48;
    
    static final int TRANSACTION_addQsTile = 32;
    
    static final int TRANSACTION_animateCollapsePanels = 6;
    
    static final int TRANSACTION_animateExpandNotificationsPanel = 4;
    
    static final int TRANSACTION_animateExpandSettingsPanel = 5;
    
    static final int TRANSACTION_appTransitionCancelled = 22;
    
    static final int TRANSACTION_appTransitionFinished = 24;
    
    static final int TRANSACTION_appTransitionPending = 21;
    
    static final int TRANSACTION_appTransitionStarting = 23;
    
    static final int TRANSACTION_cancelPreloadRecentApps = 17;
    
    static final int TRANSACTION_clickQsTile = 34;
    
    static final int TRANSACTION_disable = 3;
    
    static final int TRANSACTION_dismissInattentiveSleepWarning = 50;
    
    static final int TRANSACTION_dismissKeyboardShortcutsMenu = 19;
    
    static final int TRANSACTION_handleSystemKey = 35;
    
    static final int TRANSACTION_hideAuthenticationDialog = 43;
    
    static final int TRANSACTION_hideRecentApps = 13;
    
    static final int TRANSACTION_hideToast = 52;
    
    static final int TRANSACTION_onBiometricAuthenticated = 40;
    
    static final int TRANSACTION_onBiometricError = 42;
    
    static final int TRANSACTION_onBiometricHelp = 41;
    
    static final int TRANSACTION_onCameraLaunchGestureDetected = 27;
    
    static final int TRANSACTION_onDisplayReady = 44;
    
    static final int TRANSACTION_onProposedRotationChanged = 30;
    
    static final int TRANSACTION_onRecentsAnimationStateChanged = 45;
    
    static final int TRANSACTION_onSystemBarAppearanceChanged = 46;
    
    static final int TRANSACTION_preloadRecentApps = 16;
    
    static final int TRANSACTION_remQsTile = 33;
    
    static final int TRANSACTION_removeIcon = 2;
    
    static final int TRANSACTION_setIcon = 1;
    
    static final int TRANSACTION_setImeWindowStatus = 10;
    
    static final int TRANSACTION_setTopAppHidesStatusBar = 31;
    
    static final int TRANSACTION_setWindowState = 11;
    
    static final int TRANSACTION_showAssistDisclosure = 25;
    
    static final int TRANSACTION_showAuthenticationDialog = 39;
    
    static final int TRANSACTION_showGlobalActionsMenu = 29;
    
    static final int TRANSACTION_showInattentiveSleepWarning = 49;
    
    static final int TRANSACTION_showPictureInPictureMenu = 28;
    
    static final int TRANSACTION_showPinningEnterExitToast = 36;
    
    static final int TRANSACTION_showPinningEscapeToast = 37;
    
    static final int TRANSACTION_showRecentApps = 12;
    
    static final int TRANSACTION_showScreenPinningRequest = 18;
    
    static final int TRANSACTION_showShutdownUi = 38;
    
    static final int TRANSACTION_showToast = 51;
    
    static final int TRANSACTION_showTransient = 47;
    
    static final int TRANSACTION_showWirelessChargingAnimation = 8;
    
    static final int TRANSACTION_startAssist = 26;
    
    static final int TRANSACTION_startTracing = 53;
    
    static final int TRANSACTION_stopTracing = 54;
    
    static final int TRANSACTION_suppressAmbientDisplay = 55;
    
    static final int TRANSACTION_toggleKeyboardShortcutsMenu = 20;
    
    static final int TRANSACTION_togglePanel = 7;
    
    static final int TRANSACTION_toggleRecentApps = 14;
    
    static final int TRANSACTION_toggleSplitScreen = 15;
    
    static final int TRANSACTION_topAppWindowChanged = 9;
    
    public Stub() {
      attachInterface(this, "com.android.internal.statusbar.IStatusBar");
    }
    
    public static IStatusBar asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.statusbar.IStatusBar");
      if (iInterface != null && iInterface instanceof IStatusBar)
        return (IStatusBar)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 55:
          return "suppressAmbientDisplay";
        case 54:
          return "stopTracing";
        case 53:
          return "startTracing";
        case 52:
          return "hideToast";
        case 51:
          return "showToast";
        case 50:
          return "dismissInattentiveSleepWarning";
        case 49:
          return "showInattentiveSleepWarning";
        case 48:
          return "abortTransient";
        case 47:
          return "showTransient";
        case 46:
          return "onSystemBarAppearanceChanged";
        case 45:
          return "onRecentsAnimationStateChanged";
        case 44:
          return "onDisplayReady";
        case 43:
          return "hideAuthenticationDialog";
        case 42:
          return "onBiometricError";
        case 41:
          return "onBiometricHelp";
        case 40:
          return "onBiometricAuthenticated";
        case 39:
          return "showAuthenticationDialog";
        case 38:
          return "showShutdownUi";
        case 37:
          return "showPinningEscapeToast";
        case 36:
          return "showPinningEnterExitToast";
        case 35:
          return "handleSystemKey";
        case 34:
          return "clickQsTile";
        case 33:
          return "remQsTile";
        case 32:
          return "addQsTile";
        case 31:
          return "setTopAppHidesStatusBar";
        case 30:
          return "onProposedRotationChanged";
        case 29:
          return "showGlobalActionsMenu";
        case 28:
          return "showPictureInPictureMenu";
        case 27:
          return "onCameraLaunchGestureDetected";
        case 26:
          return "startAssist";
        case 25:
          return "showAssistDisclosure";
        case 24:
          return "appTransitionFinished";
        case 23:
          return "appTransitionStarting";
        case 22:
          return "appTransitionCancelled";
        case 21:
          return "appTransitionPending";
        case 20:
          return "toggleKeyboardShortcutsMenu";
        case 19:
          return "dismissKeyboardShortcutsMenu";
        case 18:
          return "showScreenPinningRequest";
        case 17:
          return "cancelPreloadRecentApps";
        case 16:
          return "preloadRecentApps";
        case 15:
          return "toggleSplitScreen";
        case 14:
          return "toggleRecentApps";
        case 13:
          return "hideRecentApps";
        case 12:
          return "showRecentApps";
        case 11:
          return "setWindowState";
        case 10:
          return "setImeWindowStatus";
        case 9:
          return "topAppWindowChanged";
        case 8:
          return "showWirelessChargingAnimation";
        case 7:
          return "togglePanel";
        case 6:
          return "animateCollapsePanels";
        case 5:
          return "animateExpandSettingsPanel";
        case 4:
          return "animateExpandNotificationsPanel";
        case 3:
          return "disable";
        case 2:
          return "removeIcon";
        case 1:
          break;
      } 
      return "setIcon";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        IBinder iBinder1;
        ITransientNotificationCallback iTransientNotificationCallback;
        int[] arrayOfInt;
        String str1, str2;
        AppearanceRegion[] arrayOfAppearanceRegion;
        IBinder iBinder2;
        String str3;
        IBiometricServiceReceiverInternal iBiometricServiceReceiverInternal;
        IBinder iBinder3;
        String str4;
        IBinder iBinder4;
        int i;
        long l1, l2;
        boolean bool1 = false, bool2 = false, bool3 = false, bool4 = false, bool5 = false, bool6 = false, bool7 = false, bool8 = false, bool9 = false, bool10 = false, bool11 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 55:
            param1Parcel1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            bool9 = bool11;
            if (param1Parcel1.readInt() != 0)
              bool9 = true; 
            suppressAmbientDisplay(bool9);
            return true;
          case 54:
            param1Parcel1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            stopTracing();
            return true;
          case 53:
            param1Parcel1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            startTracing();
            return true;
          case 52:
            param1Parcel1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            str2 = param1Parcel1.readString();
            iBinder1 = param1Parcel1.readStrongBinder();
            hideToast(str2, iBinder1);
            return true;
          case 51:
            iBinder1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            param1Int2 = iBinder1.readInt();
            str3 = iBinder1.readString();
            iBinder3 = iBinder1.readStrongBinder();
            if (iBinder1.readInt() != 0) {
              CharSequence charSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              str2 = null;
            } 
            iBinder4 = iBinder1.readStrongBinder();
            param1Int1 = iBinder1.readInt();
            iTransientNotificationCallback = ITransientNotificationCallback.Stub.asInterface(iBinder1.readStrongBinder());
            showToast(param1Int2, str3, iBinder3, str2, iBinder4, param1Int1, iTransientNotificationCallback);
            return true;
          case 50:
            iTransientNotificationCallback.enforceInterface("com.android.internal.statusbar.IStatusBar");
            bool9 = bool1;
            if (iTransientNotificationCallback.readInt() != 0)
              bool9 = true; 
            dismissInattentiveSleepWarning(bool9);
            return true;
          case 49:
            iTransientNotificationCallback.enforceInterface("com.android.internal.statusbar.IStatusBar");
            showInattentiveSleepWarning();
            return true;
          case 48:
            iTransientNotificationCallback.enforceInterface("com.android.internal.statusbar.IStatusBar");
            param1Int1 = iTransientNotificationCallback.readInt();
            arrayOfInt = iTransientNotificationCallback.createIntArray();
            abortTransient(param1Int1, arrayOfInt);
            return true;
          case 47:
            arrayOfInt.enforceInterface("com.android.internal.statusbar.IStatusBar");
            param1Int1 = arrayOfInt.readInt();
            arrayOfInt = arrayOfInt.createIntArray();
            showTransient(param1Int1, arrayOfInt);
            return true;
          case 46:
            arrayOfInt.enforceInterface("com.android.internal.statusbar.IStatusBar");
            param1Int1 = arrayOfInt.readInt();
            param1Int2 = arrayOfInt.readInt();
            arrayOfAppearanceRegion = (AppearanceRegion[])arrayOfInt.createTypedArray(AppearanceRegion.CREATOR);
            bool9 = bool2;
            if (arrayOfInt.readInt() != 0)
              bool9 = true; 
            onSystemBarAppearanceChanged(param1Int1, param1Int2, arrayOfAppearanceRegion, bool9);
            return true;
          case 45:
            arrayOfInt.enforceInterface("com.android.internal.statusbar.IStatusBar");
            bool9 = bool3;
            if (arrayOfInt.readInt() != 0)
              bool9 = true; 
            onRecentsAnimationStateChanged(bool9);
            return true;
          case 44:
            arrayOfInt.enforceInterface("com.android.internal.statusbar.IStatusBar");
            param1Int1 = arrayOfInt.readInt();
            onDisplayReady(param1Int1);
            return true;
          case 43:
            arrayOfInt.enforceInterface("com.android.internal.statusbar.IStatusBar");
            hideAuthenticationDialog();
            return true;
          case 42:
            arrayOfInt.enforceInterface("com.android.internal.statusbar.IStatusBar");
            param1Int2 = arrayOfInt.readInt();
            i = arrayOfInt.readInt();
            param1Int1 = arrayOfInt.readInt();
            onBiometricError(param1Int2, i, param1Int1);
            return true;
          case 41:
            arrayOfInt.enforceInterface("com.android.internal.statusbar.IStatusBar");
            str1 = arrayOfInt.readString();
            onBiometricHelp(str1);
            return true;
          case 40:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            onBiometricAuthenticated();
            return true;
          case 39:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            if (str1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)str1);
            } else {
              arrayOfAppearanceRegion = null;
            } 
            iBiometricServiceReceiverInternal = IBiometricServiceReceiverInternal.Stub.asInterface(str1.readStrongBinder());
            param1Int2 = str1.readInt();
            if (str1.readInt() != 0) {
              bool9 = true;
            } else {
              bool9 = false;
            } 
            param1Int1 = str1.readInt();
            str4 = str1.readString();
            l1 = str1.readLong();
            i = str1.readInt();
            showAuthenticationDialog((Bundle)arrayOfAppearanceRegion, iBiometricServiceReceiverInternal, param1Int2, bool9, param1Int1, str4, l1, i);
            return true;
          case 38:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            bool9 = bool4;
            if (str1.readInt() != 0)
              bool9 = true; 
            str1 = str1.readString();
            showShutdownUi(bool9, str1);
            return true;
          case 37:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            showPinningEscapeToast();
            return true;
          case 36:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            bool9 = bool5;
            if (str1.readInt() != 0)
              bool9 = true; 
            showPinningEnterExitToast(bool9);
            return true;
          case 35:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            param1Int1 = str1.readInt();
            handleSystemKey(param1Int1);
            return true;
          case 34:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            if (str1.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            clickQsTile((ComponentName)str1);
            return true;
          case 33:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            if (str1.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            remQsTile((ComponentName)str1);
            return true;
          case 32:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            if (str1.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            addQsTile((ComponentName)str1);
            return true;
          case 31:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            bool9 = bool6;
            if (str1.readInt() != 0)
              bool9 = true; 
            setTopAppHidesStatusBar(bool9);
            return true;
          case 30:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            param1Int1 = str1.readInt();
            bool9 = bool7;
            if (str1.readInt() != 0)
              bool9 = true; 
            onProposedRotationChanged(param1Int1, bool9);
            return true;
          case 29:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            showGlobalActionsMenu();
            return true;
          case 28:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            showPictureInPictureMenu();
            return true;
          case 27:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            param1Int1 = str1.readInt();
            onCameraLaunchGestureDetected(param1Int1);
            return true;
          case 26:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            if (str1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            startAssist((Bundle)str1);
            return true;
          case 25:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            showAssistDisclosure();
            return true;
          case 24:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            param1Int1 = str1.readInt();
            appTransitionFinished(param1Int1);
            return true;
          case 23:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            param1Int1 = str1.readInt();
            l2 = str1.readLong();
            l1 = str1.readLong();
            appTransitionStarting(param1Int1, l2, l1);
            return true;
          case 22:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            param1Int1 = str1.readInt();
            appTransitionCancelled(param1Int1);
            return true;
          case 21:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            param1Int1 = str1.readInt();
            appTransitionPending(param1Int1);
            return true;
          case 20:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            param1Int1 = str1.readInt();
            toggleKeyboardShortcutsMenu(param1Int1);
            return true;
          case 19:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            dismissKeyboardShortcutsMenu();
            return true;
          case 18:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            param1Int1 = str1.readInt();
            showScreenPinningRequest(param1Int1);
            return true;
          case 17:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            cancelPreloadRecentApps();
            return true;
          case 16:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            preloadRecentApps();
            return true;
          case 15:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            toggleSplitScreen();
            return true;
          case 14:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            toggleRecentApps();
            return true;
          case 13:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            if (str1.readInt() != 0) {
              bool9 = true;
            } else {
              bool9 = false;
            } 
            if (str1.readInt() != 0)
              bool8 = true; 
            hideRecentApps(bool9, bool8);
            return true;
          case 12:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            if (str1.readInt() != 0)
              bool9 = true; 
            showRecentApps(bool9);
            return true;
          case 11:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            i = str1.readInt();
            param1Int2 = str1.readInt();
            param1Int1 = str1.readInt();
            setWindowState(i, param1Int2, param1Int1);
            return true;
          case 10:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            param1Int2 = str1.readInt();
            iBinder2 = str1.readStrongBinder();
            param1Int1 = str1.readInt();
            i = str1.readInt();
            if (str1.readInt() != 0) {
              bool9 = true;
            } else {
              bool9 = false;
            } 
            if (str1.readInt() != 0) {
              bool8 = true;
            } else {
              bool8 = false;
            } 
            setImeWindowStatus(param1Int2, iBinder2, param1Int1, i, bool9, bool8);
            return true;
          case 9:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            param1Int1 = str1.readInt();
            if (str1.readInt() != 0) {
              bool9 = true;
            } else {
              bool9 = false;
            } 
            bool8 = bool10;
            if (str1.readInt() != 0)
              bool8 = true; 
            topAppWindowChanged(param1Int1, bool9, bool8);
            return true;
          case 8:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            param1Int1 = str1.readInt();
            showWirelessChargingAnimation(param1Int1);
            return true;
          case 7:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            togglePanel();
            return true;
          case 6:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            animateCollapsePanels();
            return true;
          case 5:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            str1 = str1.readString();
            animateExpandSettingsPanel(str1);
            return true;
          case 4:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            animateExpandNotificationsPanel();
            return true;
          case 3:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            param1Int1 = str1.readInt();
            param1Int2 = str1.readInt();
            i = str1.readInt();
            disable(param1Int1, param1Int2, i);
            return true;
          case 2:
            str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
            str1 = str1.readString();
            removeIcon(str1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("com.android.internal.statusbar.IStatusBar");
        str = str1.readString();
        if (str1.readInt() != 0) {
          StatusBarIcon statusBarIcon = (StatusBarIcon)StatusBarIcon.CREATOR.createFromParcel((Parcel)str1);
        } else {
          str1 = null;
        } 
        setIcon(str, (StatusBarIcon)str1);
        return true;
      } 
      str.writeString("com.android.internal.statusbar.IStatusBar");
      return true;
    }
    
    private static class Proxy implements IStatusBar {
      public static IStatusBar sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.statusbar.IStatusBar";
      }
      
      public void setIcon(String param2String, StatusBarIcon param2StatusBarIcon) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          parcel.writeString(param2String);
          if (param2StatusBarIcon != null) {
            parcel.writeInt(1);
            param2StatusBarIcon.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().setIcon(param2String, param2StatusBarIcon);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void removeIcon(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().removeIcon(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void disable(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().disable(param2Int1, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void animateExpandNotificationsPanel() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().animateExpandNotificationsPanel();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void animateExpandSettingsPanel(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().animateExpandSettingsPanel(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void animateCollapsePanels() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().animateCollapsePanels();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void togglePanel() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().togglePanel();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void showWirelessChargingAnimation(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().showWirelessChargingAnimation(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void topAppWindowChanged(int param2Int, boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          parcel.writeInt(param2Int);
          boolean bool1 = false;
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel.writeInt(bool2);
          boolean bool2 = bool1;
          if (param2Boolean2)
            bool2 = true; 
          parcel.writeInt(bool2);
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().topAppWindowChanged(param2Int, param2Boolean1, param2Boolean2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setImeWindowStatus(int param2Int1, IBinder param2IBinder, int param2Int2, int param2Int3, boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          try {
            parcel.writeInt(param2Int1);
            try {
              parcel.writeStrongBinder(param2IBinder);
              try {
                parcel.writeInt(param2Int2);
                try {
                  parcel.writeInt(param2Int3);
                  boolean bool1 = false;
                  if (param2Boolean1) {
                    bool2 = true;
                  } else {
                    bool2 = false;
                  } 
                  parcel.writeInt(bool2);
                  boolean bool2 = bool1;
                  if (param2Boolean2)
                    bool2 = true; 
                  parcel.writeInt(bool2);
                  try {
                    boolean bool = this.mRemote.transact(10, parcel, null, 1);
                    if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
                      IStatusBar.Stub.getDefaultImpl().setImeWindowStatus(param2Int1, param2IBinder, param2Int2, param2Int3, param2Boolean1, param2Boolean2);
                      parcel.recycle();
                      return;
                    } 
                    parcel.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2IBinder;
      }
      
      public void setWindowState(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(11, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().setWindowState(param2Int1, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void showRecentApps(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(12, parcel, null, 1);
          if (!bool1 && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().showRecentApps(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void hideRecentApps(boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          boolean bool1 = false;
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel.writeInt(bool2);
          boolean bool2 = bool1;
          if (param2Boolean2)
            bool2 = true; 
          parcel.writeInt(bool2);
          boolean bool = this.mRemote.transact(13, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().hideRecentApps(param2Boolean1, param2Boolean2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void toggleRecentApps() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          boolean bool = this.mRemote.transact(14, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().toggleRecentApps();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void toggleSplitScreen() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          boolean bool = this.mRemote.transact(15, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().toggleSplitScreen();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void preloadRecentApps() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          boolean bool = this.mRemote.transact(16, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().preloadRecentApps();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void cancelPreloadRecentApps() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          boolean bool = this.mRemote.transact(17, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().cancelPreloadRecentApps();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void showScreenPinningRequest(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(18, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().showScreenPinningRequest(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void dismissKeyboardShortcutsMenu() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          boolean bool = this.mRemote.transact(19, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().dismissKeyboardShortcutsMenu();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void toggleKeyboardShortcutsMenu(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(20, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().toggleKeyboardShortcutsMenu(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void appTransitionPending(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(21, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().appTransitionPending(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void appTransitionCancelled(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(22, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().appTransitionCancelled(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void appTransitionStarting(int param2Int, long param2Long1, long param2Long2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          parcel.writeInt(param2Int);
          parcel.writeLong(param2Long1);
          parcel.writeLong(param2Long2);
          boolean bool = this.mRemote.transact(23, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().appTransitionStarting(param2Int, param2Long1, param2Long2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void appTransitionFinished(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(24, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().appTransitionFinished(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void showAssistDisclosure() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          boolean bool = this.mRemote.transact(25, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().showAssistDisclosure();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void startAssist(Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(26, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().startAssist(param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onCameraLaunchGestureDetected(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(27, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().onCameraLaunchGestureDetected(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void showPictureInPictureMenu() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          boolean bool = this.mRemote.transact(28, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().showPictureInPictureMenu();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void showGlobalActionsMenu() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          boolean bool = this.mRemote.transact(29, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().showGlobalActionsMenu();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onProposedRotationChanged(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          parcel.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(30, parcel, null, 1);
          if (!bool1 && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().onProposedRotationChanged(param2Int, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setTopAppHidesStatusBar(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(31, parcel, null, 1);
          if (!bool1 && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().setTopAppHidesStatusBar(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void addQsTile(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          if (param2ComponentName != null) {
            parcel.writeInt(1);
            param2ComponentName.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(32, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().addQsTile(param2ComponentName);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void remQsTile(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          if (param2ComponentName != null) {
            parcel.writeInt(1);
            param2ComponentName.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(33, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().remQsTile(param2ComponentName);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void clickQsTile(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          if (param2ComponentName != null) {
            parcel.writeInt(1);
            param2ComponentName.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(34, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().clickQsTile(param2ComponentName);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void handleSystemKey(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(35, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().handleSystemKey(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void showPinningEnterExitToast(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(36, parcel, null, 1);
          if (!bool1 && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().showPinningEnterExitToast(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void showPinningEscapeToast() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          boolean bool = this.mRemote.transact(37, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().showPinningEscapeToast();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void showShutdownUi(boolean param2Boolean, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          parcel.writeString(param2String);
          boolean bool1 = this.mRemote.transact(38, parcel, null, 1);
          if (!bool1 && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().showShutdownUi(param2Boolean, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void showAuthenticationDialog(Bundle param2Bundle, IBiometricServiceReceiverInternal param2IBiometricServiceReceiverInternal, int param2Int1, boolean param2Boolean, int param2Int2, String param2String, long param2Long, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          boolean bool = false;
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IBiometricServiceReceiverInternal != null) {
            iBinder = param2IBiometricServiceReceiverInternal.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          try {
            parcel.writeInt(param2Int1);
            if (param2Boolean)
              bool = true; 
            parcel.writeInt(bool);
            try {
              parcel.writeInt(param2Int2);
              try {
                parcel.writeString(param2String);
                parcel.writeLong(param2Long);
                parcel.writeInt(param2Int3);
                boolean bool1 = this.mRemote.transact(39, parcel, null, 1);
                if (!bool1 && IStatusBar.Stub.getDefaultImpl() != null) {
                  IStatusBar.Stub.getDefaultImpl().showAuthenticationDialog(param2Bundle, param2IBiometricServiceReceiverInternal, param2Int1, param2Boolean, param2Int2, param2String, param2Long, param2Int3);
                  parcel.recycle();
                  return;
                } 
                parcel.recycle();
                return;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2Bundle;
      }
      
      public void onBiometricAuthenticated() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          boolean bool = this.mRemote.transact(40, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().onBiometricAuthenticated();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onBiometricHelp(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(41, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().onBiometricHelp(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onBiometricError(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(42, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().onBiometricError(param2Int1, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void hideAuthenticationDialog() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          boolean bool = this.mRemote.transact(43, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().hideAuthenticationDialog();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDisplayReady(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(44, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().onDisplayReady(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onRecentsAnimationStateChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(45, parcel, null, 1);
          if (!bool1 && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().onRecentsAnimationStateChanged(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSystemBarAppearanceChanged(int param2Int1, int param2Int2, AppearanceRegion[] param2ArrayOfAppearanceRegion, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = false;
          parcel.writeTypedArray((Parcelable[])param2ArrayOfAppearanceRegion, 0);
          if (param2Boolean)
            bool = true; 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(46, parcel, null, 1);
          if (!bool1 && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().onSystemBarAppearanceChanged(param2Int1, param2Int2, param2ArrayOfAppearanceRegion, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void showTransient(int param2Int, int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          parcel.writeInt(param2Int);
          parcel.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(47, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().showTransient(param2Int, param2ArrayOfint);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void abortTransient(int param2Int, int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          parcel.writeInt(param2Int);
          parcel.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(48, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().abortTransient(param2Int, param2ArrayOfint);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void showInattentiveSleepWarning() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          boolean bool = this.mRemote.transact(49, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().showInattentiveSleepWarning();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void dismissInattentiveSleepWarning(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(50, parcel, null, 1);
          if (!bool1 && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().dismissInattentiveSleepWarning(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void showToast(int param2Int1, String param2String, IBinder param2IBinder1, CharSequence param2CharSequence, IBinder param2IBinder2, int param2Int2, ITransientNotificationCallback param2ITransientNotificationCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          try {
            parcel.writeInt(param2Int1);
            try {
              parcel.writeString(param2String);
              try {
                parcel.writeStrongBinder(param2IBinder1);
                if (param2CharSequence != null) {
                  parcel.writeInt(1);
                  TextUtils.writeToParcel(param2CharSequence, parcel, 0);
                } else {
                  parcel.writeInt(0);
                } 
                try {
                  parcel.writeStrongBinder(param2IBinder2);
                  try {
                    IBinder iBinder;
                    parcel.writeInt(param2Int2);
                    if (param2ITransientNotificationCallback != null) {
                      iBinder = param2ITransientNotificationCallback.asBinder();
                    } else {
                      iBinder = null;
                    } 
                    parcel.writeStrongBinder(iBinder);
                    boolean bool = this.mRemote.transact(51, parcel, null, 1);
                    if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
                      IStatusBar.Stub.getDefaultImpl().showToast(param2Int1, param2String, param2IBinder1, param2CharSequence, param2IBinder2, param2Int2, param2ITransientNotificationCallback);
                      parcel.recycle();
                      return;
                    } 
                    parcel.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2String;
      }
      
      public void hideToast(String param2String, IBinder param2IBinder) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          parcel.writeString(param2String);
          parcel.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(52, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().hideToast(param2String, param2IBinder);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void startTracing() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          boolean bool = this.mRemote.transact(53, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().startTracing();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void stopTracing() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          boolean bool = this.mRemote.transact(54, parcel, null, 1);
          if (!bool && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().stopTracing();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void suppressAmbientDisplay(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.statusbar.IStatusBar");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(55, parcel, null, 1);
          if (!bool1 && IStatusBar.Stub.getDefaultImpl() != null) {
            IStatusBar.Stub.getDefaultImpl().suppressAmbientDisplay(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IStatusBar param1IStatusBar) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IStatusBar != null) {
          Proxy.sDefaultImpl = param1IStatusBar;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IStatusBar getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
