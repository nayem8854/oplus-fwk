package com.android.internal.infra;

import android.os.AsyncTask;
import android.os.ParcelFileDescriptor;
import com.android.internal.util.FunctionalUtils;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.Executor;
import libcore.io.IoUtils;

public abstract class RemoteStream<RES, IOSTREAM extends Closeable> extends AndroidFuture<RES> implements Runnable {
  private final FunctionalUtils.ThrowingFunction<IOSTREAM, RES> mHandleStream;
  
  private volatile ParcelFileDescriptor mLocalPipe;
  
  public static <R> AndroidFuture<R> receiveBytes(FunctionalUtils.ThrowingConsumer<ParcelFileDescriptor> paramThrowingConsumer, FunctionalUtils.ThrowingFunction<InputStream, R> paramThrowingFunction) {
    return (AndroidFuture<R>)new Object(paramThrowingConsumer, paramThrowingFunction, AsyncTask.THREAD_POOL_EXECUTOR, true);
  }
  
  public static AndroidFuture<byte[]> receiveBytes(FunctionalUtils.ThrowingConsumer<ParcelFileDescriptor> paramThrowingConsumer) {
    return (AndroidFuture)receiveBytes(paramThrowingConsumer, (FunctionalUtils.ThrowingFunction<InputStream, byte>)_$$Lambda$aeiZbEpH6rq4kD9vJrlAnboJGDM.INSTANCE);
  }
  
  public static byte[] readAll(InputStream paramInputStream) throws IOException {
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    byte[] arrayOfByte = new byte[16384];
    while (true) {
      int i = paramInputStream.read(arrayOfByte);
      if (i == -1)
        return byteArrayOutputStream.toByteArray(); 
      byteArrayOutputStream.write(arrayOfByte, 0, i);
    } 
  }
  
  public static <R> AndroidFuture<R> sendBytes(FunctionalUtils.ThrowingConsumer<ParcelFileDescriptor> paramThrowingConsumer, FunctionalUtils.ThrowingFunction<OutputStream, R> paramThrowingFunction) {
    return (AndroidFuture<R>)new Object(paramThrowingConsumer, paramThrowingFunction, AsyncTask.THREAD_POOL_EXECUTOR, false);
  }
  
  public static AndroidFuture<Void> sendBytes(FunctionalUtils.ThrowingConsumer<ParcelFileDescriptor> paramThrowingConsumer, FunctionalUtils.ThrowingConsumer<OutputStream> paramThrowingConsumer1) {
    return sendBytes(paramThrowingConsumer, new _$$Lambda$RemoteStream$cEAy1MmFFZV4u2Yc69293eEHU20(paramThrowingConsumer1));
  }
  
  public static AndroidFuture<Void> sendBytes(FunctionalUtils.ThrowingConsumer<ParcelFileDescriptor> paramThrowingConsumer, byte[] paramArrayOfbyte) {
    return sendBytes(paramThrowingConsumer, new _$$Lambda$RemoteStream$6uKWoLJ2mpc1mLim72Ru_QU6tYI(paramArrayOfbyte));
  }
  
  private RemoteStream(FunctionalUtils.ThrowingConsumer<ParcelFileDescriptor> paramThrowingConsumer, FunctionalUtils.ThrowingFunction<IOSTREAM, RES> paramThrowingFunction, Executor paramExecutor, boolean paramBoolean) {
    this.mHandleStream = paramThrowingFunction;
    try {
      boolean bool2;
      ParcelFileDescriptor[] arrayOfParcelFileDescriptor = ParcelFileDescriptor.createPipe();
      boolean bool1 = true;
      if (paramBoolean) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      ParcelFileDescriptor parcelFileDescriptor = arrayOfParcelFileDescriptor[bool2];
    } finally {
      paramThrowingConsumer = null;
    } 
  }
  
  public void run() {
    try {
      IOSTREAM iOSTREAM = createStream(this.mLocalPipe);
    } finally {
      Exception exception = null;
    } 
  }
  
  protected void onCompleted(RES paramRES, Throwable paramThrowable) {
    super.onCompleted(paramRES, paramThrowable);
    IoUtils.closeQuietly((AutoCloseable)this.mLocalPipe);
  }
  
  protected abstract IOSTREAM createStream(ParcelFileDescriptor paramParcelFileDescriptor);
}
