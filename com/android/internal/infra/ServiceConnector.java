package com.android.internal.infra;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import android.os.UserHandle;
import android.text.TextUtils;
import android.util.DebugUtils;
import android.util.Log;
import com.android.internal.util.function.pooled.PooledLambda;
import java.io.PrintWriter;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

public interface ServiceConnector<I extends IInterface> {
  AndroidFuture<I> connect();
  
  AndroidFuture<Void> post(VoidJob<I> paramVoidJob);
  
  <R> AndroidFuture<R> postAsync(Job<I, CompletableFuture<R>> paramJob);
  
  <R> AndroidFuture<R> postForResult(Job<I, R> paramJob);
  
  boolean run(VoidJob<I> paramVoidJob);
  
  void unbind();
  
  @FunctionalInterface
  class VoidJob<II> implements Job<II, Void> {
    public Void run(II param1II) throws Exception {
      runNoResult(param1II);
      return null;
    }
    
    public abstract void runNoResult(II param1II) throws Exception;
  }
  
  @FunctionalInterface
  public static interface Job<II, R> {
    R run(II param1II) throws Exception;
  }
  
  class Impl<I extends IInterface> extends ArrayDeque<Job<I, ?>> implements ServiceConnector<I>, ServiceConnection, IBinder.DeathRecipient, Runnable {
    private final Queue<ServiceConnector.Job<I, ?>> mQueue = this;
    
    private final List<CompletionAwareJob<I, ?>> mUnfinishedJobs = new ArrayList<>();
    
    private final ServiceConnection mServiceConnection = this;
    
    private final Runnable mTimeoutDisconnect = this;
    
    private volatile I mService = null;
    
    private boolean mBinding = false;
    
    private boolean mUnbinding = false;
    
    private CompletionAwareJob<I, I> mServiceConnectionFutureCache = null;
    
    static final boolean DEBUG = false;
    
    private static final long DEFAULT_DISCONNECT_TIMEOUT_MS = 15000L;
    
    private static final long DEFAULT_REQUEST_TIMEOUT_MS = 30000L;
    
    static final String LOG_TAG = "ServiceConnector.Impl";
    
    private final Function<IBinder, I> mBinderAsInterface;
    
    private final int mBindingFlags;
    
    protected final Context mContext;
    
    private final Intent mIntent;
    
    private final int mUserId;
    
    public Impl(ServiceConnector this$0, Intent param1Intent, int param1Int1, int param1Int2, Function<IBinder, I> param1Function) {
      this.mContext = (Context)this$0;
      this.mIntent = param1Intent;
      this.mBindingFlags = param1Int1;
      this.mUserId = param1Int2;
      this.mBinderAsInterface = param1Function;
    }
    
    protected Handler getJobHandler() {
      return Handler.getMain();
    }
    
    protected long getAutoDisconnectTimeoutMs() {
      return 15000L;
    }
    
    protected long getRequestTimeoutMs() {
      return 30000L;
    }
    
    protected boolean bindService(ServiceConnection param1ServiceConnection, Handler param1Handler) {
      Context context = this.mContext;
      Intent intent = this.mIntent;
      int i = this.mBindingFlags, j = this.mUserId;
      UserHandle userHandle = UserHandle.of(j);
      return context.bindServiceAsUser(intent, param1ServiceConnection, i | 0x1, param1Handler, userHandle);
    }
    
    protected I binderAsInterface(IBinder param1IBinder) {
      return this.mBinderAsInterface.apply(param1IBinder);
    }
    
    protected void onServiceUnbound() {}
    
    protected void onServiceConnectionStatusChanged(I param1I, boolean param1Boolean) {}
    
    public boolean run(ServiceConnector.VoidJob<I> param1VoidJob) {
      return enqueue(param1VoidJob);
    }
    
    public AndroidFuture<Void> post(ServiceConnector.VoidJob<I> param1VoidJob) {
      return postForResult(param1VoidJob);
    }
    
    public <R> CompletionAwareJob<I, R> postForResult(ServiceConnector.Job<I, R> param1Job) {
      CompletionAwareJob<Object, Object> completionAwareJob = new CompletionAwareJob<>();
      Objects.requireNonNull(param1Job);
      completionAwareJob.mDelegate = (ServiceConnector.Job)param1Job;
      enqueue((CompletionAwareJob)completionAwareJob);
      return (CompletionAwareJob)completionAwareJob;
    }
    
    public <R> AndroidFuture<R> postAsync(ServiceConnector.Job<I, CompletableFuture<R>> param1Job) {
      CompletionAwareJob<Object, Object> completionAwareJob = new CompletionAwareJob<>();
      Objects.requireNonNull(param1Job);
      completionAwareJob.mDelegate = (ServiceConnector.Job)param1Job;
      completionAwareJob.mAsync = true;
      enqueue((CompletionAwareJob)completionAwareJob);
      return completionAwareJob;
    }
    
    public AndroidFuture<I> connect() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mServiceConnectionFutureCache : Lcom/android/internal/infra/ServiceConnector$Impl$CompletionAwareJob;
      //   6: ifnonnull -> 59
      //   9: new com/android/internal/infra/ServiceConnector$Impl$CompletionAwareJob
      //   12: astore_1
      //   13: aload_1
      //   14: aload_0
      //   15: invokespecial <init> : (Lcom/android/internal/infra/ServiceConnector$Impl;)V
      //   18: aload_0
      //   19: aload_1
      //   20: putfield mServiceConnectionFutureCache : Lcom/android/internal/infra/ServiceConnector$Impl$CompletionAwareJob;
      //   23: aload_1
      //   24: getstatic com/android/internal/infra/_$$Lambda$ServiceConnector$Impl$3vLWxkP1Z6JyExzdZboFFp1zM20.INSTANCE : Lcom/android/internal/infra/-$$Lambda$ServiceConnector$Impl$3vLWxkP1Z6JyExzdZboFFp1zM20;
      //   27: putfield mDelegate : Lcom/android/internal/infra/ServiceConnector$Job;
      //   30: aload_0
      //   31: getfield mService : Landroid/os/IInterface;
      //   34: astore_1
      //   35: aload_1
      //   36: ifnull -> 51
      //   39: aload_0
      //   40: getfield mServiceConnectionFutureCache : Lcom/android/internal/infra/ServiceConnector$Impl$CompletionAwareJob;
      //   43: aload_1
      //   44: invokevirtual complete : (Ljava/lang/Object;)Z
      //   47: pop
      //   48: goto -> 59
      //   51: aload_0
      //   52: aload_0
      //   53: getfield mServiceConnectionFutureCache : Lcom/android/internal/infra/ServiceConnector$Impl$CompletionAwareJob;
      //   56: invokespecial enqueue : (Lcom/android/internal/infra/ServiceConnector$Impl$CompletionAwareJob;)V
      //   59: aload_0
      //   60: getfield mServiceConnectionFutureCache : Lcom/android/internal/infra/ServiceConnector$Impl$CompletionAwareJob;
      //   63: astore_1
      //   64: aload_0
      //   65: monitorexit
      //   66: aload_1
      //   67: areturn
      //   68: astore_1
      //   69: aload_0
      //   70: monitorexit
      //   71: aload_1
      //   72: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #371	-> 2
      //   #372	-> 9
      //   #373	-> 23
      //   #374	-> 30
      //   #375	-> 35
      //   #376	-> 39
      //   #378	-> 51
      //   #381	-> 59
      //   #370	-> 68
      // Exception table:
      //   from	to	target	type
      //   2	9	68	finally
      //   9	23	68	finally
      //   23	30	68	finally
      //   30	35	68	finally
      //   39	48	68	finally
      //   51	59	68	finally
      //   59	64	68	finally
    }
    
    private void enqueue(CompletionAwareJob<I, ?> param1CompletionAwareJob) {
      if (!enqueue(param1CompletionAwareJob)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to post a job to handler. Likely ");
        stringBuilder.append(getJobHandler().getLooper());
        stringBuilder.append(" is exiting");
        IllegalStateException illegalStateException = new IllegalStateException(stringBuilder.toString());
        param1CompletionAwareJob.completeExceptionally(illegalStateException);
      } 
    }
    
    private boolean enqueue(ServiceConnector.Job<I, ?> param1Job) {
      cancelTimeout();
      return getJobHandler().sendMessage(PooledLambda.obtainMessage((BiConsumer)_$$Lambda$T7zIZMFnvwrmtbuTMXLaZHHp_9s.INSTANCE, this, param1Job));
    }
    
    void enqueueJobThread(ServiceConnector.Job<I, ?> param1Job) {
      cancelTimeout();
      if (this.mUnbinding) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Service is unbinding. Ignoring ");
        stringBuilder.append(param1Job);
        completeExceptionally(param1Job, new IllegalStateException(stringBuilder.toString()));
      } else if (!this.mQueue.offer(param1Job)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to add to queue: ");
        stringBuilder.append(param1Job);
        completeExceptionally(param1Job, new IllegalStateException(stringBuilder.toString()));
      } else if (isBound()) {
        processQueue();
      } else if (!this.mBinding) {
        if (bindService(this.mServiceConnection, getJobHandler())) {
          this.mBinding = true;
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Failed to bind to service ");
          stringBuilder.append(this.mIntent);
          completeExceptionally(param1Job, new IllegalStateException(stringBuilder.toString()));
        } 
      } 
    }
    
    private void cancelTimeout() {
      Handler.getMain().removeCallbacks(this.mTimeoutDisconnect);
    }
    
    void completeExceptionally(ServiceConnector.Job<?, ?> param1Job, Throwable param1Throwable) {
      CompletionAwareJob completionAwareJob = castOrNull(param1Job, CompletionAwareJob.class);
      if (completionAwareJob != null)
        completionAwareJob.completeExceptionally(param1Throwable); 
      if (completionAwareJob == null) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Job failed: ");
        stringBuilder.append(param1Job);
        Log.e("ServiceConnector.Impl", stringBuilder.toString(), param1Throwable);
      } 
    }
    
    static <BASE, T extends BASE> T castOrNull(BASE param1BASE, Class<T> param1Class) {
      if (!param1Class.isInstance(param1BASE))
        param1BASE = null; 
      return (T)param1BASE;
    }
    
    private void processQueue() {
      while (true) {
        ServiceConnector.Job<I, I> job = (ServiceConnector.Job)this.mQueue.poll();
        if (job != null) {
          CompletionAwareJob<I, ?> completionAwareJob = castOrNull(job, CompletionAwareJob.class);
          try {
            I i = this.mService;
            if (i == null)
              return; 
            i = job.run(i);
          } finally {
            completionAwareJob = null;
          } 
          continue;
        } 
        break;
      } 
      maybeScheduleUnbindTimeout();
    }
    
    private void maybeScheduleUnbindTimeout() {
      if (this.mUnfinishedJobs.isEmpty() && this.mQueue.isEmpty())
        scheduleUnbindTimeout(); 
    }
    
    private void scheduleUnbindTimeout() {
      long l = getAutoDisconnectTimeoutMs();
      if (l > 0L)
        Handler.getMain().postDelayed(this.mTimeoutDisconnect, l); 
    }
    
    private boolean isBound() {
      boolean bool;
      if (this.mService != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public void unbind() {
      this.mUnbinding = true;
      getJobHandler().sendMessage(PooledLambda.obtainMessage((Consumer)_$$Lambda$XuWfs8_IsKaNygi8YjlVGjedkIw.INSTANCE, this));
    }
    
    void unbindJobThread() {
      // Byte code:
      //   0: aload_0
      //   1: invokespecial cancelTimeout : ()V
      //   4: aload_0
      //   5: getfield mService : Landroid/os/IInterface;
      //   8: astore_1
      //   9: aload_1
      //   10: ifnull -> 18
      //   13: iconst_1
      //   14: istore_2
      //   15: goto -> 20
      //   18: iconst_0
      //   19: istore_2
      //   20: iload_2
      //   21: ifeq -> 96
      //   24: aload_0
      //   25: aload_1
      //   26: iconst_0
      //   27: invokevirtual onServiceConnectionStatusChanged : (Landroid/os/IInterface;Z)V
      //   30: aload_0
      //   31: getfield mContext : Landroid/content/Context;
      //   34: aload_0
      //   35: getfield mServiceConnection : Landroid/content/ServiceConnection;
      //   38: invokevirtual unbindService : (Landroid/content/ServiceConnection;)V
      //   41: aload_1
      //   42: invokeinterface asBinder : ()Landroid/os/IBinder;
      //   47: aload_0
      //   48: iconst_0
      //   49: invokeinterface unlinkToDeath : (Landroid/os/IBinder$DeathRecipient;I)Z
      //   54: pop
      //   55: goto -> 91
      //   58: astore_1
      //   59: new java/lang/StringBuilder
      //   62: dup
      //   63: invokespecial <init> : ()V
      //   66: astore_3
      //   67: aload_3
      //   68: ldc_w 'error unlinking to death'
      //   71: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   74: pop
      //   75: aload_3
      //   76: aload_1
      //   77: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   80: pop
      //   81: ldc 'ServiceConnector.Impl'
      //   83: aload_3
      //   84: invokevirtual toString : ()Ljava/lang/String;
      //   87: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
      //   90: pop
      //   91: aload_0
      //   92: aconst_null
      //   93: putfield mService : Landroid/os/IInterface;
      //   96: aload_0
      //   97: iconst_0
      //   98: putfield mBinding : Z
      //   101: aload_0
      //   102: iconst_0
      //   103: putfield mUnbinding : Z
      //   106: aload_0
      //   107: monitorenter
      //   108: aload_0
      //   109: getfield mServiceConnectionFutureCache : Lcom/android/internal/infra/ServiceConnector$Impl$CompletionAwareJob;
      //   112: ifnull -> 129
      //   115: aload_0
      //   116: getfield mServiceConnectionFutureCache : Lcom/android/internal/infra/ServiceConnector$Impl$CompletionAwareJob;
      //   119: iconst_1
      //   120: invokevirtual cancel : (Z)Z
      //   123: pop
      //   124: aload_0
      //   125: aconst_null
      //   126: putfield mServiceConnectionFutureCache : Lcom/android/internal/infra/ServiceConnector$Impl$CompletionAwareJob;
      //   129: aload_0
      //   130: monitorexit
      //   131: aload_0
      //   132: invokevirtual cancelPendingJobs : ()V
      //   135: iload_2
      //   136: ifeq -> 143
      //   139: aload_0
      //   140: invokevirtual onServiceUnbound : ()V
      //   143: return
      //   144: astore_1
      //   145: aload_0
      //   146: monitorexit
      //   147: aload_1
      //   148: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #509	-> 0
      //   #510	-> 4
      //   #511	-> 9
      //   #512	-> 20
      //   #513	-> 24
      //   #514	-> 30
      //   #520	-> 41
      //   #523	-> 55
      //   #521	-> 58
      //   #522	-> 59
      //   #525	-> 91
      //   #527	-> 96
      //   #528	-> 101
      //   #529	-> 106
      //   #530	-> 108
      //   #531	-> 115
      //   #532	-> 124
      //   #534	-> 129
      //   #536	-> 131
      //   #538	-> 135
      //   #539	-> 139
      //   #541	-> 143
      //   #534	-> 144
      // Exception table:
      //   from	to	target	type
      //   41	55	58	java/lang/Exception
      //   108	115	144	finally
      //   115	124	144	finally
      //   124	129	144	finally
      //   129	131	144	finally
      //   145	147	144	finally
    }
    
    protected void cancelPendingJobs() {
      while (true) {
        ServiceConnector.Job job = this.mQueue.poll();
        if (job != null) {
          job = castOrNull(job, CompletionAwareJob.class);
          if (job != null)
            job.cancel(false); 
          continue;
        } 
        break;
      } 
    }
    
    public void onServiceConnected(ComponentName param1ComponentName, IBinder param1IBinder) {
      StringBuilder stringBuilder;
      if (this.mUnbinding) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Ignoring onServiceConnected due to ongoing unbinding: ");
        stringBuilder.append(this);
        Log.i("ServiceConnector.Impl", stringBuilder.toString());
        return;
      } 
      I i = binderAsInterface(param1IBinder);
      this.mService = i;
      this.mBinding = false;
      try {
        param1IBinder.linkToDeath(this, 0);
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("onServiceConnected ");
        stringBuilder1.append(stringBuilder);
        stringBuilder1.append(": ");
        Log.e("ServiceConnector.Impl", stringBuilder1.toString(), (Throwable)remoteException);
      } 
      onServiceConnectionStatusChanged(i, true);
      processQueue();
    }
    
    public void onServiceDisconnected(ComponentName param1ComponentName) {
      this.mBinding = true;
      I i = this.mService;
      if (i != null) {
        onServiceConnectionStatusChanged(i, false);
        this.mService = null;
      } 
    }
    
    public void onBindingDied(ComponentName param1ComponentName) {
      binderDied();
    }
    
    public void binderDied() {
      this.mService = null;
      unbind();
    }
    
    public void run() {
      onTimeout();
    }
    
    private void onTimeout() {
      unbind();
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder("ServiceConnector@");
      stringBuilder.append(System.identityHashCode(this) % 1000);
      stringBuilder.append("(");
      Intent intent = this.mIntent;
      stringBuilder.append(intent);
      stringBuilder.append(", user: ");
      stringBuilder.append(this.mUserId);
      stringBuilder.append(")[");
      stringBuilder = stringBuilder.append(stateToString());
      if (!this.mQueue.isEmpty()) {
        stringBuilder.append(", ");
        stringBuilder.append(this.mQueue.size());
        stringBuilder.append(" pending job(s)");
      } 
      if (!this.mUnfinishedJobs.isEmpty()) {
        stringBuilder.append(", ");
        stringBuilder.append(this.mUnfinishedJobs.size());
        stringBuilder.append(" unfinished async job(s)");
      } 
      stringBuilder.append("]");
      return stringBuilder.toString();
    }
    
    public void dump(String param1String, PrintWriter param1PrintWriter) {
      param1PrintWriter.append(param1String).append("ServiceConnector:").println();
      param1PrintWriter.append(param1String).append("  ").append(String.valueOf(this.mIntent)).println();
      PrintWriter printWriter2 = param1PrintWriter.append(param1String).append("  ");
      printWriter2.append("userId: ").append(String.valueOf(this.mUserId)).println();
      printWriter2 = param1PrintWriter.append(param1String).append("  ");
      printWriter2.append("State: ").append(stateToString()).println();
      printWriter2 = param1PrintWriter.append(param1String).append("  ");
      printWriter2.append("Pending jobs: ").append(String.valueOf(this.mQueue.size())).println();
      PrintWriter printWriter1 = param1PrintWriter.append(param1String).append("  ");
      param1PrintWriter = printWriter1.append("Unfinished async jobs: ");
      List<CompletionAwareJob<I, ?>> list = this.mUnfinishedJobs;
      param1PrintWriter.append(String.valueOf(list.size())).println();
    }
    
    private String stateToString() {
      if (this.mBinding)
        return "Binding..."; 
      if (this.mUnbinding)
        return "Unbinding..."; 
      if (isBound())
        return "Bound"; 
      return "Unbound";
    }
    
    private void logTrace() {
      StringBuilder stringBuilder = new StringBuilder();
      List<String> list = DebugUtils.callersWithin(ServiceConnector.class, 1);
      stringBuilder.append(TextUtils.join(" -> ", list));
      stringBuilder.append("(");
      stringBuilder.append(this);
      stringBuilder.append(")");
      String str = stringBuilder.toString();
      Log.i("ServiceConnector.Impl", str);
    }
    
    class CompletionAwareJob<II, R> extends AndroidFuture<R> implements ServiceConnector.Job<II, R>, BiConsumer<R, Throwable> {
      boolean mAsync = false;
      
      private String mDebugName;
      
      ServiceConnector.Job<II, R> mDelegate;
      
      final ServiceConnector.Impl this$0;
      
      CompletionAwareJob() {
        long l = ServiceConnector.Impl.this.getRequestTimeoutMs();
        if (l > 0L)
          orTimeout(l, TimeUnit.MILLISECONDS); 
      }
      
      public R run(II param2II) throws Exception {
        return this.mDelegate.run(param2II);
      }
      
      public boolean cancel(boolean param2Boolean) {
        if (param2Boolean)
          Log.w("ServiceConnector.Impl", "mayInterruptIfRunning not supported - ignoring"); 
        boolean bool = ServiceConnector.Impl.this.mQueue.remove(this);
        return (super.cancel(param2Boolean) || bool);
      }
      
      public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.mDelegate);
        stringBuilder.append(" wrapped into ");
        stringBuilder.append(super.toString());
        return stringBuilder.toString();
      }
      
      public void accept(R param2R, Throwable param2Throwable) {
        if (param2Throwable != null) {
          completeExceptionally(param2Throwable);
        } else {
          complete(param2R);
        } 
      }
      
      protected void onCompleted(R param2R, Throwable param2Throwable) {
        super.onCompleted(param2R, param2Throwable);
        if (ServiceConnector.Impl.access$100(ServiceConnector.Impl.this).remove(this))
          ServiceConnector.Impl.this.maybeScheduleUnbindTimeout(); 
      }
    }
  }
}
