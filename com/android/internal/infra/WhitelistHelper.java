package com.android.internal.infra;

import android.content.ComponentName;
import android.util.ArrayMap;
import android.util.ArraySet;
import android.util.Log;
import java.io.PrintWriter;
import java.util.List;
import java.util.Objects;

public final class WhitelistHelper {
  private static final String TAG = "WhitelistHelper";
  
  private ArrayMap<String, ArraySet<ComponentName>> mWhitelistedPackages;
  
  public void setWhitelist(ArraySet<String> paramArraySet, ArraySet<ComponentName> paramArraySet1) {
    this.mWhitelistedPackages = null;
    if (paramArraySet == null && paramArraySet1 == null)
      return; 
    if ((paramArraySet == null || !paramArraySet.isEmpty()) && (paramArraySet1 == null || 
      !paramArraySet1.isEmpty())) {
      this.mWhitelistedPackages = new ArrayMap<>();
      if (paramArraySet != null)
        for (byte b = 0; b < paramArraySet.size(); b++)
          this.mWhitelistedPackages.put(paramArraySet.valueAt(b), null);  
      if (paramArraySet1 != null)
        for (byte b = 0; b < paramArraySet1.size(); b++) {
          ComponentName componentName = paramArraySet1.valueAt(b);
          if (componentName == null) {
            Log.w("WhitelistHelper", "setWhitelist(): component is null");
          } else {
            String str = componentName.getPackageName();
            ArraySet<String> arraySet = (ArraySet)this.mWhitelistedPackages.get(str);
            paramArraySet = arraySet;
            if (arraySet == null) {
              paramArraySet = new ArraySet<>();
              this.mWhitelistedPackages.put(str, paramArraySet);
            } 
            paramArraySet.add(componentName);
          } 
        }  
      return;
    } 
    throw new IllegalArgumentException("Packages or Components cannot be empty.");
  }
  
  public void setWhitelist(List<String> paramList, List<ComponentName> paramList1) {
    ArraySet<String> arraySet;
    ArraySet<ComponentName> arraySet1;
    List list = null;
    if (paramList == null) {
      paramList = null;
    } else {
      arraySet = new ArraySet<>(paramList);
    } 
    if (paramList1 == null) {
      paramList1 = list;
    } else {
      arraySet1 = new ArraySet<>(paramList1);
    } 
    setWhitelist(arraySet, arraySet1);
  }
  
  public boolean isWhitelisted(String paramString) {
    Objects.requireNonNull(paramString);
    ArrayMap<String, ArraySet<ComponentName>> arrayMap = this.mWhitelistedPackages;
    boolean bool = false;
    if (arrayMap == null)
      return false; 
    if (arrayMap.containsKey(paramString)) {
      arrayMap = this.mWhitelistedPackages;
      if (arrayMap.get(paramString) == null)
        bool = true; 
    } 
    return bool;
  }
  
  public boolean isWhitelisted(ComponentName paramComponentName) {
    Objects.requireNonNull(paramComponentName);
    String str = paramComponentName.getPackageName();
    ArraySet<ComponentName> arraySet = getWhitelistedComponents(str);
    if (arraySet != null)
      return arraySet.contains(paramComponentName); 
    return isWhitelisted(str);
  }
  
  public ArraySet<ComponentName> getWhitelistedComponents(String paramString) {
    ArraySet<ComponentName> arraySet;
    Objects.requireNonNull(paramString);
    ArrayMap<String, ArraySet<ComponentName>> arrayMap = this.mWhitelistedPackages;
    if (arrayMap == null) {
      paramString = null;
    } else {
      arraySet = arrayMap.get(paramString);
    } 
    return arraySet;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("WhitelistHelper[");
    stringBuilder.append(this.mWhitelistedPackages);
    stringBuilder.append(']');
    return stringBuilder.toString();
  }
  
  public void dump(String paramString1, String paramString2, PrintWriter paramPrintWriter) {
    ArrayMap<String, ArraySet<ComponentName>> arrayMap = this.mWhitelistedPackages;
    if (arrayMap == null || arrayMap.size() == 0) {
      paramPrintWriter.print(paramString1);
      paramPrintWriter.print(paramString2);
      paramPrintWriter.println(": (no whitelisted packages)");
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString1);
    stringBuilder.append("  ");
    String str = stringBuilder.toString();
    int i = this.mWhitelistedPackages.size();
    paramPrintWriter.print(paramString1);
    paramPrintWriter.print(paramString2);
    paramPrintWriter.print(": ");
    paramPrintWriter.print(i);
    paramPrintWriter.println(" packages");
    for (i = 0; i < this.mWhitelistedPackages.size(); i++) {
      paramString1 = this.mWhitelistedPackages.keyAt(i);
      ArraySet arraySet = this.mWhitelistedPackages.valueAt(i);
      paramPrintWriter.print(str);
      paramPrintWriter.print(i);
      paramPrintWriter.print(".");
      paramPrintWriter.print(paramString1);
      paramPrintWriter.print(": ");
      if (arraySet == null) {
        paramPrintWriter.println("(whole package)");
      } else {
        paramPrintWriter.print("[");
        paramPrintWriter.print(arraySet.valueAt(0));
        for (byte b = 1; b < arraySet.size(); b++) {
          paramPrintWriter.print(", ");
          paramPrintWriter.print(arraySet.valueAt(b));
        } 
        paramPrintWriter.println("]");
      } 
    } 
  }
}
