package com.android.internal.infra;

import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Log;
import com.android.internal.util.ConcurrentUtils;
import com.android.internal.util.Preconditions;
import com.android.internal.util.function.TriConsumer;
import com.android.internal.util.function.pooled.PooledLambda;
import com.android.internal.util.function.pooled.PooledRunnable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class AndroidFuture<T> extends CompletableFuture<T> implements Parcelable {
  private static final String LOG_TAG = AndroidFuture.class.getSimpleName();
  
  static {
    EMPTY_STACK_TRACE = new StackTraceElement[0];
    CREATOR = new Parcelable.Creator<AndroidFuture>() {
        public AndroidFuture createFromParcel(Parcel param1Parcel) {
          return new AndroidFuture(param1Parcel);
        }
        
        public AndroidFuture[] newArray(int param1Int) {
          return new AndroidFuture[param1Int];
        }
      };
  }
  
  private final Object mLock = new Object();
  
  private Executor mListenerExecutor = ConcurrentUtils.DIRECT_EXECUTOR;
  
  private Handler mTimeoutHandler = Handler.getMain();
  
  public static final Parcelable.Creator<AndroidFuture> CREATOR;
  
  private static final boolean DEBUG = false;
  
  private static final StackTraceElement[] EMPTY_STACK_TRACE;
  
  private BiConsumer<? super T, ? super Throwable> mListener;
  
  private final IAndroidFuture mRemoteOrigin;
  
  public AndroidFuture() {
    this.mRemoteOrigin = null;
  }
  
  AndroidFuture(Parcel paramParcel) {
    if (paramParcel.readBoolean()) {
      if (paramParcel.readBoolean()) {
        completeExceptionally(unparcelException(paramParcel));
      } else {
        complete((T)paramParcel.readValue(null));
      } 
      this.mRemoteOrigin = null;
    } else {
      this.mRemoteOrigin = IAndroidFuture.Stub.asInterface(paramParcel.readStrongBinder());
    } 
  }
  
  public static <U> AndroidFuture<U> completedFuture(U paramU) {
    AndroidFuture<U> androidFuture = new AndroidFuture();
    androidFuture.complete(paramU);
    return androidFuture;
  }
  
  public boolean complete(T paramT) {
    boolean bool = super.complete(paramT);
    if (bool)
      onCompleted(paramT, (Throwable)null); 
    return bool;
  }
  
  public boolean completeExceptionally(Throwable paramThrowable) {
    boolean bool = super.completeExceptionally(paramThrowable);
    if (bool)
      onCompleted((T)null, paramThrowable); 
    return bool;
  }
  
  public boolean cancel(boolean paramBoolean) {
    paramBoolean = super.cancel(paramBoolean);
    if (paramBoolean)
      try {
        get();
        IllegalStateException illegalStateException = new IllegalStateException();
        this("Expected CancellationException");
        throw illegalStateException;
      } catch (CancellationException cancellationException) {
      
      } finally {
        Exception exception = null;
      }  
    return paramBoolean;
  }
  
  protected void onCompleted(T paramT, Throwable paramThrowable) {
    cancelTimeout();
    synchronized (this.mLock) {
      BiConsumer<? super T, ? super Throwable> biConsumer = this.mListener;
      this.mListener = null;
      if (biConsumer != null)
        callListenerAsync(biConsumer, paramT, paramThrowable); 
      IAndroidFuture iAndroidFuture = this.mRemoteOrigin;
      if (iAndroidFuture != null)
        try {
          iAndroidFuture.complete(this);
        } catch (RemoteException remoteException) {
          Log.e(LOG_TAG, "Failed to propagate completion", (Throwable)remoteException);
        }  
      return;
    } 
  }
  
  public AndroidFuture<T> whenComplete(BiConsumer<? super T, ? super Throwable> paramBiConsumer) {
    return whenCompleteAsync(paramBiConsumer, ConcurrentUtils.DIRECT_EXECUTOR);
  }
  
  public AndroidFuture<T> whenCompleteAsync(BiConsumer<? super T, ? super Throwable> paramBiConsumer, Executor paramExecutor) {
    Preconditions.checkNotNull(paramBiConsumer);
    Preconditions.checkNotNull(paramExecutor);
    synchronized (this.mLock) {
      if (!isDone()) {
        BiConsumer<? super T, ? super Throwable> biConsumer = this.mListener;
        if (biConsumer != null && paramExecutor != this.mListenerExecutor) {
          super.whenCompleteAsync(paramBiConsumer, paramExecutor);
          return this;
        } 
        this.mListenerExecutor = paramExecutor;
        if (biConsumer != null)
          paramBiConsumer = new _$$Lambda$AndroidFuture$dkSvpmqaFOFKPCZgb7C7XLP_QpE(biConsumer, paramBiConsumer); 
        this.mListener = paramBiConsumer;
        return this;
      } 
      Object object = null;
      null = null;
      paramExecutor = null;
      try {
        T t;
      } catch (ExecutionException executionException) {
      
      } finally {
        paramExecutor = null;
      } 
      callListenerAsync(paramBiConsumer, (T)null, (Throwable)paramExecutor);
      return this;
    } 
  }
  
  private void callListenerAsync(BiConsumer<? super T, ? super Throwable> paramBiConsumer, T paramT, Throwable paramThrowable) {
    if (this.mListenerExecutor == ConcurrentUtils.DIRECT_EXECUTOR) {
      callListener(paramBiConsumer, paramT, paramThrowable);
    } else {
      Executor executor = this.mListenerExecutor;
      -$.Lambda.qN_gooelzsUiBhYWznXKzb-8_wA qN_gooelzsUiBhYWznXKzb-8_wA = _$$Lambda$qN_gooelzsUiBhYWznXKzb_8_wA.INSTANCE;
      PooledRunnable pooledRunnable = PooledLambda.obtainRunnable((TriConsumer)qN_gooelzsUiBhYWznXKzb-8_wA, paramBiConsumer, paramT, paramThrowable);
      pooledRunnable = pooledRunnable.recycleOnUse();
      executor.execute((Runnable)pooledRunnable);
    } 
  }
  
  static <TT> void callListener(BiConsumer<? super TT, ? super Throwable> paramBiConsumer, TT paramTT, Throwable paramThrowable) {
    try {
      paramBiConsumer.accept(paramTT, paramThrowable);
    } finally {
      String str;
      Exception exception = null;
    } 
  }
  
  public AndroidFuture<T> orTimeout(long paramLong, TimeUnit paramTimeUnit) {
    Message message = PooledLambda.obtainMessage((Consumer)_$$Lambda$rAXGjry3wPGKviARzTYfDiY7xrs.INSTANCE, this);
    message.obj = this;
    this.mTimeoutHandler.sendMessageDelayed(message, paramTimeUnit.toMillis(paramLong));
    return this;
  }
  
  void triggerTimeout() {
    cancelTimeout();
    if (!isDone())
      completeExceptionally(new TimeoutException()); 
  }
  
  public AndroidFuture<T> cancelTimeout() {
    this.mTimeoutHandler.removeCallbacksAndMessages(this);
    return this;
  }
  
  public AndroidFuture<T> setTimeoutHandler(Handler paramHandler) {
    cancelTimeout();
    this.mTimeoutHandler = (Handler)Preconditions.checkNotNull(paramHandler);
    return this;
  }
  
  public <U> AndroidFuture<U> thenCompose(Function<? super T, ? extends CompletionStage<U>> paramFunction) {
    return thenComposeAsync(paramFunction, ConcurrentUtils.DIRECT_EXECUTOR);
  }
  
  public <U> AndroidFuture<U> thenComposeAsync(Function<? super T, ? extends CompletionStage<U>> paramFunction, Executor paramExecutor) {
    return new ThenComposeAsync<>(this, paramFunction, paramExecutor);
  }
  
  class ThenComposeAsync<T, U> extends AndroidFuture<U> implements BiConsumer<Object, Throwable>, Runnable {
    private final Executor mExecutor;
    
    private volatile Function<? super T, ? extends CompletionStage<U>> mFn;
    
    private volatile T mSourceResult = null;
    
    ThenComposeAsync(AndroidFuture this$0, Function<? super T, ? extends CompletionStage<U>> param1Function, Executor param1Executor) {
      this.mFn = (Function<? super T, ? extends CompletionStage<U>>)Preconditions.checkNotNull(param1Function);
      this.mExecutor = (Executor)Preconditions.checkNotNull(param1Executor);
      this$0.whenComplete(this);
    }
    
    public void accept(Object param1Object, Throwable param1Throwable) {
      if (param1Throwable != null) {
        completeExceptionally(param1Throwable);
      } else if (this.mFn != null) {
        this.mSourceResult = (T)param1Object;
        this.mExecutor.execute(this);
      } else {
        complete((U)param1Object);
      } 
    }
    
    public void run() {
      try {
        CompletionStage completionStage = (CompletionStage)Preconditions.checkNotNull(this.mFn.apply(this.mSourceResult));
        this.mFn = null;
        return;
      } finally {
        null = null;
      } 
    }
  }
  
  public <U> AndroidFuture<U> thenApply(Function<? super T, ? extends U> paramFunction) {
    return thenApplyAsync(paramFunction, ConcurrentUtils.DIRECT_EXECUTOR);
  }
  
  public <U> AndroidFuture<U> thenApplyAsync(Function<? super T, ? extends U> paramFunction, Executor paramExecutor) {
    return new ThenApplyAsync<>(this, paramFunction, paramExecutor);
  }
  
  class ThenApplyAsync<T, U> extends AndroidFuture<U> implements BiConsumer<T, Throwable>, Runnable {
    private final Executor mExecutor;
    
    private final Function<? super T, ? extends U> mFn;
    
    private volatile T mSourceResult = null;
    
    ThenApplyAsync(AndroidFuture this$0, Function<? super T, ? extends U> param1Function, Executor param1Executor) {
      this.mExecutor = (Executor)Preconditions.checkNotNull(param1Executor);
      this.mFn = (Function<? super T, ? extends U>)Preconditions.checkNotNull(param1Function);
      this$0.whenComplete(this);
    }
    
    public void accept(T param1T, Throwable param1Throwable) {
      if (param1Throwable != null) {
        completeExceptionally(param1Throwable);
      } else {
        this.mSourceResult = param1T;
        this.mExecutor.execute(this);
      } 
    }
    
    public void run() {
      try {
        complete(this.mFn.apply(this.mSourceResult));
      } finally {
        Exception exception = null;
      } 
    }
  }
  
  public <U, V> AndroidFuture<V> thenCombine(CompletionStage<? extends U> paramCompletionStage, BiFunction<? super T, ? super U, ? extends V> paramBiFunction) {
    return new ThenCombine<>(this, paramCompletionStage, paramBiFunction);
  }
  
  public AndroidFuture<T> thenCombine(CompletionStage<Void> paramCompletionStage) {
    return thenCombine(paramCompletionStage, (BiFunction<?, ? super Void, ? extends T>)_$$Lambda$AndroidFuture$TQp8DNhjahKV9AOYm36g8Er70s4.INSTANCE);
  }
  
  class ThenCombine<T, U, V> extends AndroidFuture<V> implements BiConsumer<Object, Throwable> {
    private final BiFunction<? super T, ? super U, ? extends V> mCombineResults;
    
    private volatile T mResultT = null;
    
    private volatile CompletionStage<? extends U> mSourceU;
    
    ThenCombine(AndroidFuture this$0, CompletionStage<? extends U> param1CompletionStage, BiFunction<? super T, ? super U, ? extends V> param1BiFunction) {
      this.mSourceU = (CompletionStage<? extends U>)Preconditions.checkNotNull(param1CompletionStage);
      this.mCombineResults = (BiFunction<? super T, ? super U, ? extends V>)Preconditions.checkNotNull(param1BiFunction);
      this$0.whenComplete(this);
    }
    
    public void accept(Object param1Object, Throwable param1Throwable) {
      if (param1Throwable != null) {
        completeExceptionally(param1Throwable);
        return;
      } 
      if (this.mSourceU != null) {
        this.mResultT = (T)param1Object;
        this.mSourceU.whenComplete(this);
      } else {
        try {
          complete(this.mCombineResults.apply(this.mResultT, (U)param1Object));
        } finally {
          param1Object = null;
        } 
      } 
    }
  }
  
  public static <T> AndroidFuture<T> supply(Supplier<T> paramSupplier) {
    return supplyAsync(paramSupplier, ConcurrentUtils.DIRECT_EXECUTOR);
  }
  
  public static <T> AndroidFuture<T> supplyAsync(Supplier<T> paramSupplier, Executor paramExecutor) {
    return new SupplyAsync<>(paramSupplier, paramExecutor);
  }
  
  class SupplyAsync<T> extends AndroidFuture<T> implements Runnable {
    private final Supplier<T> mSupplier;
    
    SupplyAsync(AndroidFuture this$0, Executor param1Executor) {
      this.mSupplier = (Supplier<T>)this$0;
      param1Executor.execute(this);
    }
    
    public void run() {
      try {
        complete(this.mSupplier.get());
      } finally {
        Exception exception = null;
      } 
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    boolean bool = isDone();
    paramParcel.writeBoolean(bool);
    if (bool) {
      try {
      
      } finally {
        Exception exception = null;
        paramParcel.writeBoolean(true);
        parcelException(paramParcel, unwrapExecutionException(exception));
      } 
    } else {
      Object object = new Object(this);
      object = object.asBinder();
      paramParcel.writeStrongBinder((IBinder)object);
    } 
  }
  
  Throwable unwrapExecutionException(Throwable paramThrowable) {
    if (paramThrowable instanceof ExecutionException)
      paramThrowable = paramThrowable.getCause(); 
    return paramThrowable;
  }
  
  private static void parcelException(Parcel paramParcel, Throwable paramThrowable) {
    boolean bool;
    if (paramThrowable == null) {
      bool = true;
    } else {
      bool = false;
    } 
    paramParcel.writeBoolean(bool);
    if (paramThrowable == null)
      return; 
    paramParcel.writeInt(Parcel.getExceptionCode(paramThrowable));
    paramParcel.writeString(paramThrowable.getClass().getName());
    paramParcel.writeString(paramThrowable.getMessage());
    paramParcel.writeStackTrace(paramThrowable);
    parcelException(paramParcel, paramThrowable.getCause());
  }
  
  private static Throwable unparcelException(Parcel paramParcel) {
    String str3;
    if (paramParcel.readBoolean())
      return null; 
    int i = paramParcel.readInt();
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    if (paramParcel.readInt() > 0) {
      str3 = paramParcel.readString();
    } else {
      str3 = "\t<stack trace unavailable>";
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(str2);
    stringBuilder.append("\n");
    stringBuilder.append(str3);
    String str4 = stringBuilder.toString();
    Exception exception1 = paramParcel.createExceptionOrNull(i, str4);
    Exception exception2 = exception1;
    if (exception1 == null) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(str1);
      stringBuilder1.append(": ");
      stringBuilder1.append(str4);
      exception2 = new RuntimeException(stringBuilder1.toString());
    } 
    exception2.setStackTrace(EMPTY_STACK_TRACE);
    Throwable throwable = unparcelException(paramParcel);
    if (throwable != null)
      exception2.initCause(exception2); 
    return exception2;
  }
  
  public int describeContents() {
    return 0;
  }
}
