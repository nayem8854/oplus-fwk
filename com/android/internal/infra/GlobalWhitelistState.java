package com.android.internal.infra;

import android.content.ComponentName;
import android.util.ArraySet;
import android.util.SparseArray;
import java.io.PrintWriter;
import java.util.List;

public class GlobalWhitelistState {
  protected final Object mGlobalWhitelistStateLock = new Object();
  
  protected SparseArray<WhitelistHelper> mWhitelisterHelpers;
  
  public void setWhitelist(int paramInt, List<String> paramList, List<ComponentName> paramList1) {
    synchronized (this.mGlobalWhitelistStateLock) {
      if (this.mWhitelisterHelpers == null) {
        SparseArray<WhitelistHelper> sparseArray = new SparseArray();
        this(1);
        this.mWhitelisterHelpers = sparseArray;
      } 
      WhitelistHelper whitelistHelper2 = this.mWhitelisterHelpers.get(paramInt);
      WhitelistHelper whitelistHelper1 = whitelistHelper2;
      if (whitelistHelper2 == null) {
        whitelistHelper1 = new WhitelistHelper();
        this();
        this.mWhitelisterHelpers.put(paramInt, whitelistHelper1);
      } 
      whitelistHelper1.setWhitelist(paramList, paramList1);
      return;
    } 
  }
  
  public boolean isWhitelisted(int paramInt, String paramString) {
    synchronized (this.mGlobalWhitelistStateLock) {
      SparseArray<WhitelistHelper> sparseArray = this.mWhitelisterHelpers;
      boolean bool = false;
      if (sparseArray == null)
        return false; 
      WhitelistHelper whitelistHelper = this.mWhitelisterHelpers.get(paramInt);
      if (whitelistHelper != null)
        bool = whitelistHelper.isWhitelisted(paramString); 
      return bool;
    } 
  }
  
  public boolean isWhitelisted(int paramInt, ComponentName paramComponentName) {
    synchronized (this.mGlobalWhitelistStateLock) {
      SparseArray<WhitelistHelper> sparseArray = this.mWhitelisterHelpers;
      boolean bool = false;
      if (sparseArray == null)
        return false; 
      WhitelistHelper whitelistHelper = this.mWhitelisterHelpers.get(paramInt);
      if (whitelistHelper != null)
        bool = whitelistHelper.isWhitelisted(paramComponentName); 
      return bool;
    } 
  }
  
  public ArraySet<ComponentName> getWhitelistedComponents(int paramInt, String paramString) {
    synchronized (this.mGlobalWhitelistStateLock) {
      ArraySet<ComponentName> arraySet;
      SparseArray<WhitelistHelper> sparseArray = this.mWhitelisterHelpers;
      String str = null;
      if (sparseArray == null)
        return null; 
      WhitelistHelper whitelistHelper = this.mWhitelisterHelpers.get(paramInt);
      if (whitelistHelper == null) {
        paramString = str;
      } else {
        arraySet = whitelistHelper.getWhitelistedComponents(paramString);
      } 
      return arraySet;
    } 
  }
  
  public void resetWhitelist(int paramInt) {
    synchronized (this.mGlobalWhitelistStateLock) {
      if (this.mWhitelisterHelpers == null)
        return; 
      this.mWhitelisterHelpers.remove(paramInt);
      if (this.mWhitelisterHelpers.size() == 0)
        this.mWhitelisterHelpers = null; 
      return;
    } 
  }
  
  public void dump(String paramString, PrintWriter paramPrintWriter) {
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("State: ");
    synchronized (this.mGlobalWhitelistStateLock) {
      if (this.mWhitelisterHelpers == null) {
        paramPrintWriter.println("empty");
        return;
      } 
      paramPrintWriter.print(this.mWhitelisterHelpers.size());
      paramPrintWriter.println(" services");
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append(paramString);
      stringBuilder.append("  ");
      String str = stringBuilder.toString();
      for (byte b = 0; b < this.mWhitelisterHelpers.size(); b++) {
        int i = this.mWhitelisterHelpers.keyAt(b);
        WhitelistHelper whitelistHelper = this.mWhitelisterHelpers.valueAt(b);
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append("Whitelist for userId ");
        stringBuilder1.append(i);
        whitelistHelper.dump(str, stringBuilder1.toString(), paramPrintWriter);
      } 
      return;
    } 
  }
}
