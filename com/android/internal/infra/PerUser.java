package com.android.internal.infra;

import android.util.SparseArray;
import com.android.internal.util.Preconditions;

public abstract class PerUser<T> extends SparseArray<T> {
  protected abstract T create(int paramInt);
  
  public T forUser(int paramInt) {
    return get(paramInt);
  }
  
  public T get(int paramInt) {
    T t = super.get(paramInt);
    if (t != null)
      return t; 
    t = (T)Preconditions.checkNotNull(create(paramInt));
    put(paramInt, t);
    return t;
  }
}
