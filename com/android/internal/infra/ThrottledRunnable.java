package com.android.internal.infra;

import android.os.Handler;
import android.os.SystemClock;

public class ThrottledRunnable implements Runnable {
  private final Handler mHandler;
  
  private final long mIntervalMillis;
  
  private final Object mLock = new Object();
  
  private final Runnable mRunnable;
  
  private long mScheduledUptimeMillis;
  
  public ThrottledRunnable(Handler paramHandler, long paramLong, Runnable paramRunnable) {
    this.mHandler = paramHandler;
    this.mIntervalMillis = paramLong;
    this.mRunnable = paramRunnable;
  }
  
  public void run() {
    synchronized (this.mLock) {
      if (this.mHandler.hasCallbacks(this.mRunnable))
        return; 
      long l = SystemClock.uptimeMillis();
      if (this.mScheduledUptimeMillis == 0L || l > this.mScheduledUptimeMillis + this.mIntervalMillis) {
        this.mScheduledUptimeMillis = l;
      } else {
        this.mScheduledUptimeMillis += this.mIntervalMillis;
      } 
      this.mHandler.postAtTime(this.mRunnable, this.mScheduledUptimeMillis);
      return;
    } 
  }
}
