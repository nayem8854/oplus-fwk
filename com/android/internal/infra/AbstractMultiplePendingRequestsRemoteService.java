package com.android.internal.infra;

import android.content.ComponentName;
import android.content.Context;
import android.os.Handler;
import android.os.IInterface;
import android.util.Slog;
import java.io.PrintWriter;
import java.util.ArrayList;

public abstract class AbstractMultiplePendingRequestsRemoteService<S extends AbstractMultiplePendingRequestsRemoteService<S, I>, I extends IInterface> extends AbstractRemoteService<S, I> {
  private final int mInitialCapacity;
  
  protected ArrayList<AbstractRemoteService.BasePendingRequest<S, I>> mPendingRequests;
  
  public AbstractMultiplePendingRequestsRemoteService(Context paramContext, String paramString, ComponentName paramComponentName, int paramInt1, AbstractRemoteService.VultureCallback<S> paramVultureCallback, Handler paramHandler, int paramInt2, boolean paramBoolean, int paramInt3) {
    super(paramContext, paramString, paramComponentName, paramInt1, paramVultureCallback, paramHandler, paramInt2, paramBoolean);
    this.mInitialCapacity = paramInt3;
  }
  
  void handlePendingRequests() {
    ArrayList<AbstractRemoteService.BasePendingRequest<S, I>> arrayList = this.mPendingRequests;
    if (arrayList != null) {
      int i = arrayList.size();
      if (this.mVerbose) {
        String str = this.mTag;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Sending ");
        stringBuilder.append(i);
        stringBuilder.append(" pending requests");
        Slog.v(str, stringBuilder.toString());
      } 
      for (byte b = 0; b < i; b++)
        ((AbstractRemoteService.BasePendingRequest)this.mPendingRequests.get(b)).run(); 
      this.mPendingRequests = null;
    } 
  }
  
  protected void handleOnDestroy() {
    ArrayList<AbstractRemoteService.BasePendingRequest<S, I>> arrayList = this.mPendingRequests;
    if (arrayList != null) {
      int i = arrayList.size();
      if (this.mVerbose) {
        String str = this.mTag;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Canceling ");
        stringBuilder.append(i);
        stringBuilder.append(" pending requests");
        Slog.v(str, stringBuilder.toString());
      } 
      for (byte b = 0; b < i; b++)
        ((AbstractRemoteService.BasePendingRequest)this.mPendingRequests.get(b)).cancel(); 
      this.mPendingRequests = null;
    } 
  }
  
  final void handleBindFailure() {
    ArrayList<AbstractRemoteService.BasePendingRequest<S, I>> arrayList = this.mPendingRequests;
    if (arrayList != null) {
      int i = arrayList.size();
      if (this.mVerbose) {
        String str = this.mTag;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Sending failure to ");
        stringBuilder.append(i);
        stringBuilder.append(" pending requests");
        Slog.v(str, stringBuilder.toString());
      } 
      for (byte b = 0; b < i; b++) {
        AbstractRemoteService.BasePendingRequest basePendingRequest = this.mPendingRequests.get(b);
        basePendingRequest.onFailed();
        basePendingRequest.finish();
      } 
      this.mPendingRequests = null;
    } 
  }
  
  public void dump(String paramString, PrintWriter paramPrintWriter) {
    int i;
    super.dump(paramString, paramPrintWriter);
    PrintWriter printWriter = paramPrintWriter.append(paramString).append("initialCapacity=").append(String.valueOf(this.mInitialCapacity));
    printWriter.println();
    ArrayList<AbstractRemoteService.BasePendingRequest<S, I>> arrayList = this.mPendingRequests;
    if (arrayList == null) {
      i = 0;
    } else {
      i = arrayList.size();
    } 
    paramPrintWriter.append(paramString).append("pendingRequests=").append(String.valueOf(i)).println();
  }
  
  void handlePendingRequestWhileUnBound(AbstractRemoteService.BasePendingRequest<S, I> paramBasePendingRequest) {
    if (this.mPendingRequests == null)
      this.mPendingRequests = new ArrayList<>(this.mInitialCapacity); 
    this.mPendingRequests.add(paramBasePendingRequest);
    if (this.mVerbose) {
      String str = this.mTag;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("queued ");
      stringBuilder.append(this.mPendingRequests.size());
      stringBuilder.append(" requests; last=");
      stringBuilder.append(paramBasePendingRequest);
      Slog.v(str, stringBuilder.toString());
    } 
  }
}
