package com.android.internal.infra;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.UserHandle;
import android.util.Slog;
import android.util.TimeUtils;
import com.android.internal.util.function.pooled.PooledLambda;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public abstract class AbstractRemoteService<S extends AbstractRemoteService<S, I>, I extends IInterface> implements IBinder.DeathRecipient {
  protected final String mTag = getClass().getSimpleName();
  
  private final ServiceConnection mServiceConnection = new RemoteServiceConnection();
  
  private final ArrayList<BasePendingRequest<S, I>> mUnfinishedRequests = new ArrayList<>();
  
  protected static final int LAST_PRIVATE_MSG = 2;
  
  private static final int MSG_BIND = 1;
  
  private static final int MSG_UNBIND = 2;
  
  public static final long PERMANENT_BOUND_TIMEOUT_MS = 0L;
  
  private boolean mBinding;
  
  private final int mBindingFlags;
  
  private boolean mCompleted;
  
  protected final ComponentName mComponentName;
  
  private final Context mContext;
  
  private boolean mDestroyed;
  
  protected final Handler mHandler;
  
  private final Intent mIntent;
  
  private long mNextUnbind;
  
  protected I mService;
  
  private boolean mServiceDied;
  
  private final int mUserId;
  
  public final boolean mVerbose;
  
  private final VultureCallback<S> mVultureCallback;
  
  AbstractRemoteService(Context paramContext, String paramString, ComponentName paramComponentName, int paramInt1, VultureCallback<S> paramVultureCallback, Handler paramHandler, int paramInt2, boolean paramBoolean) {
    this.mContext = paramContext;
    this.mVultureCallback = paramVultureCallback;
    this.mVerbose = paramBoolean;
    this.mComponentName = paramComponentName;
    this.mIntent = (new Intent(paramString)).setComponent(this.mComponentName);
    this.mUserId = paramInt1;
    this.mHandler = new Handler(paramHandler.getLooper());
    this.mBindingFlags = paramInt2;
  }
  
  public final void destroy() {
    this.mHandler.sendMessage(PooledLambda.obtainMessage((Consumer)_$$Lambda$AbstractRemoteService$9IBVTCLLZgndvH7fu1P14PW1_1o.INSTANCE, this));
  }
  
  public final boolean isDestroyed() {
    return this.mDestroyed;
  }
  
  public final ComponentName getComponentName() {
    return this.mComponentName;
  }
  
  private void handleOnConnectedStateChangedInternal(boolean paramBoolean) {
    handleOnConnectedStateChanged(paramBoolean);
    if (paramBoolean)
      handlePendingRequests(); 
  }
  
  protected void handleOnConnectedStateChanged(boolean paramBoolean) {}
  
  protected long getRemoteRequestMillis() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("not implemented by ");
    stringBuilder.append(getClass());
    throw new UnsupportedOperationException(stringBuilder.toString());
  }
  
  public final I getServiceInterface() {
    return this.mService;
  }
  
  private void handleDestroy() {
    if (checkIfDestroyed())
      return; 
    handleOnDestroy();
    handleEnsureUnbound();
    this.mDestroyed = true;
  }
  
  public void binderDied() {
    this.mHandler.sendMessage(PooledLambda.obtainMessage((Consumer)_$$Lambda$AbstractRemoteService$ocrHd68Md9x6FfAzVQ6w8MAjFqY.INSTANCE, this));
  }
  
  private void handleBinderDied() {
    if (checkIfDestroyed())
      return; 
    I i = this.mService;
    if (i != null)
      i.asBinder().unlinkToDeath(this, 0); 
    this.mService = null;
    this.mServiceDied = true;
    cancelScheduledUnbind();
    this.mVultureCallback.onServiceDied((S)this);
    handleBindFailure();
  }
  
  public void dump(String paramString, PrintWriter paramPrintWriter) {
    paramPrintWriter.append(paramString).append("service:").println();
    PrintWriter printWriter2 = paramPrintWriter.append(paramString).append("  ").append("userId=");
    int i = this.mUserId;
    printWriter2.append(String.valueOf(i)).println();
    printWriter2 = paramPrintWriter.append(paramString).append("  ").append("componentName=");
    ComponentName componentName = this.mComponentName;
    printWriter2.append(componentName.flattenToString()).println();
    printWriter2 = paramPrintWriter.append(paramString).append("  ").append("destroyed=");
    boolean bool = this.mDestroyed;
    printWriter2.append(String.valueOf(bool)).println();
    PrintWriter printWriter3 = paramPrintWriter.append(paramString).append("  ").append("numUnfinishedRequests=");
    ArrayList<BasePendingRequest<S, I>> arrayList = this.mUnfinishedRequests;
    printWriter3.append(String.valueOf(arrayList.size())).println();
    bool = handleIsBound();
    PrintWriter printWriter1 = paramPrintWriter.append(paramString).append("  ").append("bound=");
    printWriter1.append(String.valueOf(bool));
    long l = getTimeoutIdleBindMillis();
    if (bool)
      if (l > 0L) {
        paramPrintWriter.append(" (unbind in : ");
        TimeUtils.formatDuration(this.mNextUnbind - SystemClock.elapsedRealtime(), paramPrintWriter);
        paramPrintWriter.append(")");
      } else {
        paramPrintWriter.append(" (permanently bound)");
      }  
    paramPrintWriter.println();
    paramPrintWriter.append(paramString).append("mBindingFlags=").println(this.mBindingFlags);
    printWriter1 = paramPrintWriter.append(paramString).append("idleTimeout=");
    l /= 1000L;
    printWriter1.append(Long.toString(l)).append("s\n");
    paramPrintWriter.append(paramString).append("requestTimeout=");
    try {
      paramPrintWriter.append(Long.toString(getRemoteRequestMillis() / 1000L)).append("s\n");
    } catch (UnsupportedOperationException unsupportedOperationException) {
      paramPrintWriter.append("not supported\n");
    } 
    paramPrintWriter.println();
  }
  
  protected void scheduleRequest(BasePendingRequest<S, I> paramBasePendingRequest) {
    this.mHandler.sendMessage(PooledLambda.obtainMessage((BiConsumer)_$$Lambda$7_CJJfrUZBVuXZyYFEWBNh8Mky8.INSTANCE, this, paramBasePendingRequest));
  }
  
  void finishRequest(BasePendingRequest<S, I> paramBasePendingRequest) {
    Handler handler = this.mHandler;
    -$.Lambda.AbstractRemoteService.FcEKfZ-7TXLg6dcCU8EMuMNAy4 fcEKfZ-7TXLg6dcCU8EMuMNAy4 = _$$Lambda$AbstractRemoteService$6FcEKfZ_7TXLg6dcCU8EMuMNAy4.INSTANCE;
    Message message = PooledLambda.obtainMessage((BiConsumer)fcEKfZ-7TXLg6dcCU8EMuMNAy4, this, paramBasePendingRequest);
    handler.sendMessage(message);
  }
  
  private void handleFinishRequest(BasePendingRequest<S, I> paramBasePendingRequest) {
    this.mUnfinishedRequests.remove(paramBasePendingRequest);
    if (this.mUnfinishedRequests.isEmpty())
      scheduleUnbind(); 
  }
  
  protected void scheduleAsyncRequest(AsyncRequest<I> paramAsyncRequest) {
    MyAsyncPendingRequest<AbstractRemoteService, I> myAsyncPendingRequest = new MyAsyncPendingRequest<>(this, paramAsyncRequest);
    Handler handler = this.mHandler;
    -$.Lambda.EbzSql2RHkXox5Myj8A-7kLC4_A ebzSql2RHkXox5Myj8A-7kLC4_A = _$$Lambda$EbzSql2RHkXox5Myj8A_7kLC4_A.INSTANCE;
    Message message = PooledLambda.obtainMessage((BiConsumer)ebzSql2RHkXox5Myj8A-7kLC4_A, this, myAsyncPendingRequest);
    handler.sendMessage(message);
  }
  
  protected void executeAsyncRequest(AsyncRequest<I> paramAsyncRequest) {
    MyAsyncPendingRequest<AbstractRemoteService, I> myAsyncPendingRequest = new MyAsyncPendingRequest<>(this, paramAsyncRequest);
    handlePendingRequest((BasePendingRequest)myAsyncPendingRequest);
  }
  
  private void cancelScheduledUnbind() {
    this.mHandler.removeMessages(2);
  }
  
  protected void scheduleBind() {
    if (this.mHandler.hasMessages(1)) {
      if (this.mVerbose)
        Slog.v(this.mTag, "scheduleBind(): already scheduled"); 
      return;
    } 
    Handler handler = this.mHandler;
    Message message = PooledLambda.obtainMessage((Consumer)_$$Lambda$AbstractRemoteService$YSUzqqi1Pbrg2dlwMGMtKWbGXck.INSTANCE, this);
    message = message.setWhat(1);
    handler.sendMessage(message);
  }
  
  protected void scheduleUnbind() {
    scheduleUnbind(true);
  }
  
  private void scheduleUnbind(boolean paramBoolean) {
    long l = getTimeoutIdleBindMillis();
    if (l <= 0L) {
      if (this.mVerbose) {
        String str = this.mTag;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("not scheduling unbind when value is ");
        stringBuilder.append(l);
        Slog.v(str, stringBuilder.toString());
      } 
      return;
    } 
    if (!paramBoolean)
      l = 0L; 
    cancelScheduledUnbind();
    this.mNextUnbind = SystemClock.elapsedRealtime() + l;
    if (this.mVerbose) {
      String str = this.mTag;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("unbinding in ");
      stringBuilder.append(l);
      stringBuilder.append("ms: ");
      stringBuilder.append(this.mNextUnbind);
      Slog.v(str, stringBuilder.toString());
    } 
    Handler handler = this.mHandler;
    Message message = PooledLambda.obtainMessage((Consumer)_$$Lambda$AbstractRemoteService$MDW40b8CzodE5xRowI9wDEyXEnw.INSTANCE, this);
    message = message.setWhat(2);
    handler.sendMessageDelayed(message, l);
  }
  
  private void handleUnbind() {
    if (checkIfDestroyed())
      return; 
    handleEnsureUnbound();
  }
  
  protected final void handlePendingRequest(BasePendingRequest<S, I> paramBasePendingRequest) {
    if (checkIfDestroyed() || this.mCompleted)
      return; 
    if (!handleIsBound()) {
      if (this.mVerbose) {
        String str = this.mTag;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("handlePendingRequest(): queuing ");
        stringBuilder.append(paramBasePendingRequest);
        Slog.v(str, stringBuilder.toString());
      } 
      handlePendingRequestWhileUnBound(paramBasePendingRequest);
      handleEnsureBound();
    } else {
      if (this.mVerbose) {
        String str = this.mTag;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("handlePendingRequest(): ");
        stringBuilder.append(paramBasePendingRequest);
        Slog.v(str, stringBuilder.toString());
      } 
      this.mUnfinishedRequests.add(paramBasePendingRequest);
      cancelScheduledUnbind();
      paramBasePendingRequest.run();
      if (paramBasePendingRequest.isFinal())
        this.mCompleted = true; 
    } 
  }
  
  private boolean handleIsBound() {
    boolean bool;
    if (this.mService != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void handleEnsureBound() {
    if (handleIsBound() || this.mBinding)
      return; 
    if (this.mVerbose)
      Slog.v(this.mTag, "ensureBound()"); 
    this.mBinding = true;
    int i = 0x4000001 | this.mBindingFlags;
    boolean bool = this.mContext.bindServiceAsUser(this.mIntent, this.mServiceConnection, i, this.mHandler, new UserHandle(this.mUserId));
    if (!bool) {
      String str = this.mTag;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("could not bind to ");
      stringBuilder.append(this.mIntent);
      stringBuilder.append(" using flags ");
      stringBuilder.append(i);
      Slog.w(str, stringBuilder.toString());
      this.mBinding = false;
      if (!this.mServiceDied)
        handleBinderDied(); 
    } 
  }
  
  private void handleEnsureUnbound() {
    if (!handleIsBound() && !this.mBinding)
      return; 
    if (this.mVerbose)
      Slog.v(this.mTag, "ensureUnbound()"); 
    this.mBinding = false;
    if (handleIsBound()) {
      handleOnConnectedStateChangedInternal(false);
      I i = this.mService;
      if (i != null) {
        i.asBinder().unlinkToDeath(this, 0);
        this.mService = null;
      } 
    } 
    this.mNextUnbind = 0L;
    this.mContext.unbindService(this.mServiceConnection);
  }
  
  private class RemoteServiceConnection implements ServiceConnection {
    final AbstractRemoteService this$0;
    
    private RemoteServiceConnection() {}
    
    public void onServiceConnected(ComponentName param1ComponentName, IBinder param1IBinder) {
      if (AbstractRemoteService.this.mVerbose)
        Slog.v(AbstractRemoteService.this.mTag, "onServiceConnected()"); 
      if (AbstractRemoteService.this.mDestroyed || !AbstractRemoteService.this.mBinding) {
        Slog.wtf(AbstractRemoteService.this.mTag, "onServiceConnected() was dispatched after unbindService.");
        return;
      } 
      AbstractRemoteService.access$202(AbstractRemoteService.this, false);
      try {
        param1IBinder.linkToDeath(AbstractRemoteService.this, 0);
        AbstractRemoteService abstractRemoteService = AbstractRemoteService.this;
        abstractRemoteService.mService = (I)abstractRemoteService.getServiceInterface(param1IBinder);
        AbstractRemoteService.this.handleOnConnectedStateChangedInternal(true);
        AbstractRemoteService.access$502(AbstractRemoteService.this, false);
        return;
      } catch (RemoteException remoteException) {
        AbstractRemoteService.this.handleBinderDied();
        return;
      } 
    }
    
    public void onServiceDisconnected(ComponentName param1ComponentName) {
      if (AbstractRemoteService.this.mVerbose)
        Slog.v(AbstractRemoteService.this.mTag, "onServiceDisconnected()"); 
      AbstractRemoteService.access$202(AbstractRemoteService.this, true);
      AbstractRemoteService.this.mService = null;
    }
    
    public void onBindingDied(ComponentName param1ComponentName) {
      if (AbstractRemoteService.this.mVerbose)
        Slog.v(AbstractRemoteService.this.mTag, "onBindingDied()"); 
      AbstractRemoteService.this.scheduleUnbind(false);
    }
  }
  
  private boolean checkIfDestroyed() {
    if (this.mDestroyed && 
      this.mVerbose) {
      String str = this.mTag;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Not handling operation as service for ");
      stringBuilder.append(this.mComponentName);
      stringBuilder.append(" is already destroyed");
      Slog.v(str, stringBuilder.toString());
    } 
    return this.mDestroyed;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(getClass().getSimpleName());
    stringBuilder.append("[");
    stringBuilder.append(this.mComponentName);
    stringBuilder.append(" ");
    stringBuilder.append(System.identityHashCode(this));
    if (this.mService != null) {
      null = " (bound)";
    } else {
      null = " (unbound)";
    } 
    stringBuilder.append(null);
    if (this.mDestroyed) {
      null = " (destroyed)";
    } else {
      null = "";
    } 
    stringBuilder.append(null);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  protected abstract I getServiceInterface(IBinder paramIBinder);
  
  protected abstract long getTimeoutIdleBindMillis();
  
  abstract void handleBindFailure();
  
  protected abstract void handleOnDestroy();
  
  abstract void handlePendingRequestWhileUnBound(BasePendingRequest<S, I> paramBasePendingRequest);
  
  abstract void handlePendingRequests();
  
  class BasePendingRequest<S extends AbstractRemoteService<S, I>, I extends IInterface> implements Runnable {
    final WeakReference<S> mWeakService;
    
    protected final String mTag = getClass().getSimpleName();
    
    protected final Object mLock = new Object();
    
    boolean mCompleted;
    
    boolean mCancelled;
    
    BasePendingRequest(AbstractRemoteService this$0) {
      this.mWeakService = new WeakReference<>((S)this$0);
    }
    
    protected final S getService() {
      return this.mWeakService.get();
    }
    
    protected final boolean finish() {
      synchronized (this.mLock) {
        if (this.mCompleted || this.mCancelled)
          return false; 
        this.mCompleted = true;
        AbstractRemoteService abstractRemoteService = (AbstractRemoteService)this.mWeakService.get();
        if (abstractRemoteService != null)
          abstractRemoteService.finishRequest(this); 
        onFinished();
        return true;
      } 
    }
    
    void onFinished() {}
    
    protected void onFailed() {}
    
    protected final boolean isCancelledLocked() {
      return this.mCancelled;
    }
    
    public boolean cancel() {
      synchronized (this.mLock) {
        if (this.mCancelled || this.mCompleted)
          return false; 
        this.mCancelled = true;
        onCancel();
        return true;
      } 
    }
    
    void onCancel() {}
    
    protected boolean isFinal() {
      return false;
    }
    
    protected boolean isRequestCompleted() {
      synchronized (this.mLock) {
        return this.mCompleted;
      } 
    }
  }
  
  public static abstract class PendingRequest<S extends AbstractRemoteService<S, I>, I extends IInterface> extends BasePendingRequest<S, I> {
    private final Handler mServiceHandler;
    
    private final Runnable mTimeoutTrigger;
    
    protected PendingRequest(S param1S) {
      super(param1S);
      this.mServiceHandler = ((AbstractRemoteService)param1S).mHandler;
      _$$Lambda$AbstractRemoteService$PendingRequest$IBoaBGXZQEXJr69u3aJF_LCJ42Y _$$Lambda$AbstractRemoteService$PendingRequest$IBoaBGXZQEXJr69u3aJF_LCJ42Y = new _$$Lambda$AbstractRemoteService$PendingRequest$IBoaBGXZQEXJr69u3aJF_LCJ42Y(this, (AbstractRemoteService)param1S);
      Handler handler = this.mServiceHandler;
      long l1 = SystemClock.uptimeMillis(), l2 = param1S.getRemoteRequestMillis();
      handler.postAtTime(_$$Lambda$AbstractRemoteService$PendingRequest$IBoaBGXZQEXJr69u3aJF_LCJ42Y, l1 + l2);
    }
    
    final void onFinished() {
      this.mServiceHandler.removeCallbacks(this.mTimeoutTrigger);
    }
    
    final void onCancel() {
      this.mServiceHandler.removeCallbacks(this.mTimeoutTrigger);
    }
    
    protected abstract void onTimeout(S param1S);
  }
  
  private static final class MyAsyncPendingRequest<S extends AbstractRemoteService<S, I>, I extends IInterface> extends BasePendingRequest<S, I> {
    private static final String TAG = MyAsyncPendingRequest.class.getSimpleName();
    
    private final AbstractRemoteService.AsyncRequest<I> mRequest;
    
    protected MyAsyncPendingRequest(S param1S, AbstractRemoteService.AsyncRequest<I> param1AsyncRequest) {
      super(param1S);
      this.mRequest = param1AsyncRequest;
    }
    
    public void run() {
      S s = getService();
      if (s == null)
        return; 
      try {
        this.mRequest.run(((AbstractRemoteService)s).mService);
        finish();
      } catch (RemoteException remoteException) {
        String str = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("exception handling async request (");
        stringBuilder.append(this);
        stringBuilder.append("): ");
        stringBuilder.append(remoteException);
        Slog.w(str, stringBuilder.toString());
        finish();
      } finally {}
    }
  }
  
  class AsyncRequest<I extends IInterface> {
    public abstract void run(I param1I) throws RemoteException;
  }
  
  class VultureCallback<T> {
    public abstract void onServiceDied(T param1T);
  }
}
