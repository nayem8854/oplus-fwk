package com.android.internal.infra;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IAndroidFuture extends IInterface {
  void complete(AndroidFuture paramAndroidFuture) throws RemoteException;
  
  class Default implements IAndroidFuture {
    public void complete(AndroidFuture param1AndroidFuture) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAndroidFuture {
    private static final String DESCRIPTOR = "com.android.internal.infra.IAndroidFuture";
    
    static final int TRANSACTION_complete = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.infra.IAndroidFuture");
    }
    
    public static IAndroidFuture asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.infra.IAndroidFuture");
      if (iInterface != null && iInterface instanceof IAndroidFuture)
        return (IAndroidFuture)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "complete";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.infra.IAndroidFuture");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.infra.IAndroidFuture");
      if (param1Parcel1.readInt() != 0) {
        AndroidFuture androidFuture = (AndroidFuture)AndroidFuture.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      complete((AndroidFuture)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IAndroidFuture {
      public static IAndroidFuture sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.infra.IAndroidFuture";
      }
      
      public void complete(AndroidFuture param2AndroidFuture) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.infra.IAndroidFuture");
          if (param2AndroidFuture != null) {
            parcel.writeInt(1);
            param2AndroidFuture.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IAndroidFuture.Stub.getDefaultImpl() != null) {
            IAndroidFuture.Stub.getDefaultImpl().complete(param2AndroidFuture);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAndroidFuture param1IAndroidFuture) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAndroidFuture != null) {
          Proxy.sDefaultImpl = param1IAndroidFuture;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAndroidFuture getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
