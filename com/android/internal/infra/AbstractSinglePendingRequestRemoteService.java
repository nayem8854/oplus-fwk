package com.android.internal.infra;

import android.content.ComponentName;
import android.content.Context;
import android.os.Handler;
import android.os.IInterface;
import android.util.Slog;
import java.io.PrintWriter;

public abstract class AbstractSinglePendingRequestRemoteService<S extends AbstractSinglePendingRequestRemoteService<S, I>, I extends IInterface> extends AbstractRemoteService<S, I> {
  protected AbstractRemoteService.BasePendingRequest<S, I> mPendingRequest;
  
  public AbstractSinglePendingRequestRemoteService(Context paramContext, String paramString, ComponentName paramComponentName, int paramInt1, AbstractRemoteService.VultureCallback<S> paramVultureCallback, Handler paramHandler, int paramInt2, boolean paramBoolean) {
    super(paramContext, paramString, paramComponentName, paramInt1, paramVultureCallback, paramHandler, paramInt2, paramBoolean);
  }
  
  void handlePendingRequests() {
    if (this.mPendingRequest != null) {
      AbstractRemoteService.BasePendingRequest<S, I> basePendingRequest = this.mPendingRequest;
      this.mPendingRequest = null;
      handlePendingRequest(basePendingRequest);
    } 
  }
  
  protected void handleOnDestroy() {
    AbstractRemoteService.BasePendingRequest<S, I> basePendingRequest = this.mPendingRequest;
    if (basePendingRequest != null) {
      basePendingRequest.cancel();
      this.mPendingRequest = null;
    } 
  }
  
  void handleBindFailure() {
    if (this.mPendingRequest != null) {
      if (this.mVerbose) {
        String str = this.mTag;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Sending failure to ");
        stringBuilder.append(this.mPendingRequest);
        Slog.v(str, stringBuilder.toString());
      } 
      this.mPendingRequest.onFailed();
      this.mPendingRequest = null;
    } 
  }
  
  public void dump(String paramString, PrintWriter paramPrintWriter) {
    boolean bool;
    super.dump(paramString, paramPrintWriter);
    PrintWriter printWriter = paramPrintWriter.append(paramString).append("hasPendingRequest=");
    if (this.mPendingRequest != null) {
      bool = true;
    } else {
      bool = false;
    } 
    printWriter.append(String.valueOf(bool)).println();
  }
  
  void handlePendingRequestWhileUnBound(AbstractRemoteService.BasePendingRequest<S, I> paramBasePendingRequest) {
    if (this.mPendingRequest != null) {
      if (this.mVerbose) {
        String str = this.mTag;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("handlePendingRequestWhileUnBound(): cancelling ");
        stringBuilder.append(this.mPendingRequest);
        stringBuilder.append(" to handle ");
        stringBuilder.append(paramBasePendingRequest);
        Slog.v(str, stringBuilder.toString());
      } 
      this.mPendingRequest.cancel();
    } 
    this.mPendingRequest = paramBasePendingRequest;
  }
}
