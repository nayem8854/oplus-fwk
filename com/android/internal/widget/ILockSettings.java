package com.android.internal.widget;

import android.app.PendingIntent;
import android.app.trust.IStrongAuthTracker;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.security.keystore.recovery.KeyChainProtectionParams;
import android.security.keystore.recovery.KeyChainSnapshot;
import android.security.keystore.recovery.RecoveryCertPath;
import android.security.keystore.recovery.WrappedApplicationKey;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface ILockSettings extends IInterface {
  VerifyCredentialResponse checkCredential(LockscreenCredential paramLockscreenCredential, int paramInt, ICheckCredentialProgressCallback paramICheckCredentialProgressCallback) throws RemoteException;
  
  boolean checkVoldPassword(int paramInt) throws RemoteException;
  
  void closeSession(String paramString) throws RemoteException;
  
  String generateKey(String paramString) throws RemoteException;
  
  String generateKeyWithMetadata(String paramString, byte[] paramArrayOfbyte) throws RemoteException;
  
  boolean getBoolean(String paramString, boolean paramBoolean, int paramInt) throws RemoteException;
  
  int getCredentialType(int paramInt) throws RemoteException;
  
  byte[] getHashFactor(LockscreenCredential paramLockscreenCredential, int paramInt) throws RemoteException;
  
  String getKey(String paramString) throws RemoteException;
  
  KeyChainSnapshot getKeyChainSnapshot() throws RemoteException;
  
  long getLong(String paramString, long paramLong, int paramInt) throws RemoteException;
  
  String getPassword() throws RemoteException;
  
  int[] getRecoverySecretTypes() throws RemoteException;
  
  Map getRecoveryStatus() throws RemoteException;
  
  boolean getSeparateProfileChallengeEnabled(int paramInt) throws RemoteException;
  
  String getString(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  int getStrongAuthForUser(int paramInt) throws RemoteException;
  
  boolean hasPendingEscrowToken(int paramInt) throws RemoteException;
  
  boolean hasSecureLockScreen() throws RemoteException;
  
  String importKey(String paramString, byte[] paramArrayOfbyte) throws RemoteException;
  
  String importKeyWithMetadata(String paramString, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) throws RemoteException;
  
  void initRecoveryServiceWithSigFile(String paramString, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) throws RemoteException;
  
  Map recoverKeyChainSnapshot(String paramString, byte[] paramArrayOfbyte, List<WrappedApplicationKey> paramList) throws RemoteException;
  
  void registerStrongAuthTracker(IStrongAuthTracker paramIStrongAuthTracker) throws RemoteException;
  
  void removeCachedUnifiedChallenge(int paramInt) throws RemoteException;
  
  void removeKey(String paramString) throws RemoteException;
  
  void reportSuccessfulBiometricUnlock(boolean paramBoolean, int paramInt) throws RemoteException;
  
  void requireStrongAuth(int paramInt1, int paramInt2) throws RemoteException;
  
  void resetKeyStore(int paramInt) throws RemoteException;
  
  void sanitizePassword() throws RemoteException;
  
  void scheduleNonStrongBiometricIdleTimeout(int paramInt) throws RemoteException;
  
  void setBoolean(String paramString, boolean paramBoolean, int paramInt) throws RemoteException;
  
  boolean setLockCredential(LockscreenCredential paramLockscreenCredential1, LockscreenCredential paramLockscreenCredential2, int paramInt) throws RemoteException;
  
  void setLong(String paramString, long paramLong, int paramInt) throws RemoteException;
  
  void setRecoverySecretTypes(int[] paramArrayOfint) throws RemoteException;
  
  void setRecoveryStatus(String paramString, int paramInt) throws RemoteException;
  
  void setSeparateProfileChallengeEnabled(int paramInt, boolean paramBoolean, LockscreenCredential paramLockscreenCredential) throws RemoteException;
  
  void setServerParams(byte[] paramArrayOfbyte) throws RemoteException;
  
  void setSnapshotCreatedPendingIntent(PendingIntent paramPendingIntent) throws RemoteException;
  
  void setString(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  byte[] startRecoverySessionWithCertPath(String paramString1, String paramString2, RecoveryCertPath paramRecoveryCertPath, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, List<KeyChainProtectionParams> paramList) throws RemoteException;
  
  void systemReady() throws RemoteException;
  
  boolean tryUnlockWithCachedUnifiedChallenge(int paramInt) throws RemoteException;
  
  void unregisterStrongAuthTracker(IStrongAuthTracker paramIStrongAuthTracker) throws RemoteException;
  
  void userPresent(int paramInt) throws RemoteException;
  
  VerifyCredentialResponse verifyCredential(LockscreenCredential paramLockscreenCredential, long paramLong, int paramInt) throws RemoteException;
  
  VerifyCredentialResponse verifyTiedProfileChallenge(LockscreenCredential paramLockscreenCredential, long paramLong, int paramInt) throws RemoteException;
  
  class Default implements ILockSettings {
    public void setBoolean(String param1String, boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public void setLong(String param1String, long param1Long, int param1Int) throws RemoteException {}
    
    public void setString(String param1String1, String param1String2, int param1Int) throws RemoteException {}
    
    public boolean getBoolean(String param1String, boolean param1Boolean, int param1Int) throws RemoteException {
      return false;
    }
    
    public long getLong(String param1String, long param1Long, int param1Int) throws RemoteException {
      return 0L;
    }
    
    public String getString(String param1String1, String param1String2, int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean setLockCredential(LockscreenCredential param1LockscreenCredential1, LockscreenCredential param1LockscreenCredential2, int param1Int) throws RemoteException {
      return false;
    }
    
    public void resetKeyStore(int param1Int) throws RemoteException {}
    
    public VerifyCredentialResponse checkCredential(LockscreenCredential param1LockscreenCredential, int param1Int, ICheckCredentialProgressCallback param1ICheckCredentialProgressCallback) throws RemoteException {
      return null;
    }
    
    public VerifyCredentialResponse verifyCredential(LockscreenCredential param1LockscreenCredential, long param1Long, int param1Int) throws RemoteException {
      return null;
    }
    
    public VerifyCredentialResponse verifyTiedProfileChallenge(LockscreenCredential param1LockscreenCredential, long param1Long, int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean checkVoldPassword(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getCredentialType(int param1Int) throws RemoteException {
      return 0;
    }
    
    public byte[] getHashFactor(LockscreenCredential param1LockscreenCredential, int param1Int) throws RemoteException {
      return null;
    }
    
    public void setSeparateProfileChallengeEnabled(int param1Int, boolean param1Boolean, LockscreenCredential param1LockscreenCredential) throws RemoteException {}
    
    public boolean getSeparateProfileChallengeEnabled(int param1Int) throws RemoteException {
      return false;
    }
    
    public void registerStrongAuthTracker(IStrongAuthTracker param1IStrongAuthTracker) throws RemoteException {}
    
    public void unregisterStrongAuthTracker(IStrongAuthTracker param1IStrongAuthTracker) throws RemoteException {}
    
    public void requireStrongAuth(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void reportSuccessfulBiometricUnlock(boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public void scheduleNonStrongBiometricIdleTimeout(int param1Int) throws RemoteException {}
    
    public void systemReady() throws RemoteException {}
    
    public void userPresent(int param1Int) throws RemoteException {}
    
    public int getStrongAuthForUser(int param1Int) throws RemoteException {
      return 0;
    }
    
    public boolean hasPendingEscrowToken(int param1Int) throws RemoteException {
      return false;
    }
    
    public void initRecoveryServiceWithSigFile(String param1String, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2) throws RemoteException {}
    
    public KeyChainSnapshot getKeyChainSnapshot() throws RemoteException {
      return null;
    }
    
    public String generateKey(String param1String) throws RemoteException {
      return null;
    }
    
    public String generateKeyWithMetadata(String param1String, byte[] param1ArrayOfbyte) throws RemoteException {
      return null;
    }
    
    public String importKey(String param1String, byte[] param1ArrayOfbyte) throws RemoteException {
      return null;
    }
    
    public String importKeyWithMetadata(String param1String, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2) throws RemoteException {
      return null;
    }
    
    public String getKey(String param1String) throws RemoteException {
      return null;
    }
    
    public void removeKey(String param1String) throws RemoteException {}
    
    public void setSnapshotCreatedPendingIntent(PendingIntent param1PendingIntent) throws RemoteException {}
    
    public void setServerParams(byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public void setRecoveryStatus(String param1String, int param1Int) throws RemoteException {}
    
    public Map getRecoveryStatus() throws RemoteException {
      return null;
    }
    
    public void setRecoverySecretTypes(int[] param1ArrayOfint) throws RemoteException {}
    
    public int[] getRecoverySecretTypes() throws RemoteException {
      return null;
    }
    
    public byte[] startRecoverySessionWithCertPath(String param1String1, String param1String2, RecoveryCertPath param1RecoveryCertPath, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2, List<KeyChainProtectionParams> param1List) throws RemoteException {
      return null;
    }
    
    public Map recoverKeyChainSnapshot(String param1String, byte[] param1ArrayOfbyte, List<WrappedApplicationKey> param1List) throws RemoteException {
      return null;
    }
    
    public void closeSession(String param1String) throws RemoteException {}
    
    public boolean hasSecureLockScreen() throws RemoteException {
      return false;
    }
    
    public boolean tryUnlockWithCachedUnifiedChallenge(int param1Int) throws RemoteException {
      return false;
    }
    
    public void removeCachedUnifiedChallenge(int param1Int) throws RemoteException {}
    
    public void sanitizePassword() throws RemoteException {}
    
    public String getPassword() throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ILockSettings {
    private static final String DESCRIPTOR = "com.android.internal.widget.ILockSettings";
    
    static final int TRANSACTION_checkCredential = 9;
    
    static final int TRANSACTION_checkVoldPassword = 12;
    
    static final int TRANSACTION_closeSession = 42;
    
    static final int TRANSACTION_generateKey = 28;
    
    static final int TRANSACTION_generateKeyWithMetadata = 29;
    
    static final int TRANSACTION_getBoolean = 4;
    
    static final int TRANSACTION_getCredentialType = 13;
    
    static final int TRANSACTION_getHashFactor = 14;
    
    static final int TRANSACTION_getKey = 32;
    
    static final int TRANSACTION_getKeyChainSnapshot = 27;
    
    static final int TRANSACTION_getLong = 5;
    
    static final int TRANSACTION_getPassword = 47;
    
    static final int TRANSACTION_getRecoverySecretTypes = 39;
    
    static final int TRANSACTION_getRecoveryStatus = 37;
    
    static final int TRANSACTION_getSeparateProfileChallengeEnabled = 16;
    
    static final int TRANSACTION_getString = 6;
    
    static final int TRANSACTION_getStrongAuthForUser = 24;
    
    static final int TRANSACTION_hasPendingEscrowToken = 25;
    
    static final int TRANSACTION_hasSecureLockScreen = 43;
    
    static final int TRANSACTION_importKey = 30;
    
    static final int TRANSACTION_importKeyWithMetadata = 31;
    
    static final int TRANSACTION_initRecoveryServiceWithSigFile = 26;
    
    static final int TRANSACTION_recoverKeyChainSnapshot = 41;
    
    static final int TRANSACTION_registerStrongAuthTracker = 17;
    
    static final int TRANSACTION_removeCachedUnifiedChallenge = 45;
    
    static final int TRANSACTION_removeKey = 33;
    
    static final int TRANSACTION_reportSuccessfulBiometricUnlock = 20;
    
    static final int TRANSACTION_requireStrongAuth = 19;
    
    static final int TRANSACTION_resetKeyStore = 8;
    
    static final int TRANSACTION_sanitizePassword = 46;
    
    static final int TRANSACTION_scheduleNonStrongBiometricIdleTimeout = 21;
    
    static final int TRANSACTION_setBoolean = 1;
    
    static final int TRANSACTION_setLockCredential = 7;
    
    static final int TRANSACTION_setLong = 2;
    
    static final int TRANSACTION_setRecoverySecretTypes = 38;
    
    static final int TRANSACTION_setRecoveryStatus = 36;
    
    static final int TRANSACTION_setSeparateProfileChallengeEnabled = 15;
    
    static final int TRANSACTION_setServerParams = 35;
    
    static final int TRANSACTION_setSnapshotCreatedPendingIntent = 34;
    
    static final int TRANSACTION_setString = 3;
    
    static final int TRANSACTION_startRecoverySessionWithCertPath = 40;
    
    static final int TRANSACTION_systemReady = 22;
    
    static final int TRANSACTION_tryUnlockWithCachedUnifiedChallenge = 44;
    
    static final int TRANSACTION_unregisterStrongAuthTracker = 18;
    
    static final int TRANSACTION_userPresent = 23;
    
    static final int TRANSACTION_verifyCredential = 10;
    
    static final int TRANSACTION_verifyTiedProfileChallenge = 11;
    
    public Stub() {
      attachInterface(this, "com.android.internal.widget.ILockSettings");
    }
    
    public static ILockSettings asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.widget.ILockSettings");
      if (iInterface != null && iInterface instanceof ILockSettings)
        return (ILockSettings)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 47:
          return "getPassword";
        case 46:
          return "sanitizePassword";
        case 45:
          return "removeCachedUnifiedChallenge";
        case 44:
          return "tryUnlockWithCachedUnifiedChallenge";
        case 43:
          return "hasSecureLockScreen";
        case 42:
          return "closeSession";
        case 41:
          return "recoverKeyChainSnapshot";
        case 40:
          return "startRecoverySessionWithCertPath";
        case 39:
          return "getRecoverySecretTypes";
        case 38:
          return "setRecoverySecretTypes";
        case 37:
          return "getRecoveryStatus";
        case 36:
          return "setRecoveryStatus";
        case 35:
          return "setServerParams";
        case 34:
          return "setSnapshotCreatedPendingIntent";
        case 33:
          return "removeKey";
        case 32:
          return "getKey";
        case 31:
          return "importKeyWithMetadata";
        case 30:
          return "importKey";
        case 29:
          return "generateKeyWithMetadata";
        case 28:
          return "generateKey";
        case 27:
          return "getKeyChainSnapshot";
        case 26:
          return "initRecoveryServiceWithSigFile";
        case 25:
          return "hasPendingEscrowToken";
        case 24:
          return "getStrongAuthForUser";
        case 23:
          return "userPresent";
        case 22:
          return "systemReady";
        case 21:
          return "scheduleNonStrongBiometricIdleTimeout";
        case 20:
          return "reportSuccessfulBiometricUnlock";
        case 19:
          return "requireStrongAuth";
        case 18:
          return "unregisterStrongAuthTracker";
        case 17:
          return "registerStrongAuthTracker";
        case 16:
          return "getSeparateProfileChallengeEnabled";
        case 15:
          return "setSeparateProfileChallengeEnabled";
        case 14:
          return "getHashFactor";
        case 13:
          return "getCredentialType";
        case 12:
          return "checkVoldPassword";
        case 11:
          return "verifyTiedProfileChallenge";
        case 10:
          return "verifyCredential";
        case 9:
          return "checkCredential";
        case 8:
          return "resetKeyStore";
        case 7:
          return "setLockCredential";
        case 6:
          return "getString";
        case 5:
          return "getLong";
        case 4:
          return "getBoolean";
        case 3:
          return "setString";
        case 2:
          return "setLong";
        case 1:
          break;
      } 
      return "setBoolean";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool6;
        int i1;
        boolean bool5;
        int n;
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        String str6;
        ArrayList<WrappedApplicationKey> arrayList1;
        Map map2;
        ArrayList<KeyChainProtectionParams> arrayList;
        byte[] arrayOfByte7;
        int[] arrayOfInt;
        Map map1;
        byte[] arrayOfByte6;
        String str5;
        byte[] arrayOfByte5;
        String str4;
        byte[] arrayOfByte4;
        String str3;
        byte[] arrayOfByte3;
        String str2;
        KeyChainSnapshot keyChainSnapshot;
        byte[] arrayOfByte2;
        IStrongAuthTracker iStrongAuthTracker;
        byte[] arrayOfByte1;
        VerifyCredentialResponse verifyCredentialResponse2;
        ICheckCredentialProgressCallback iCheckCredentialProgressCallback;
        VerifyCredentialResponse verifyCredentialResponse1;
        String str1, str8;
        byte[] arrayOfByte8;
        String str7;
        byte[] arrayOfByte9;
        String str10;
        byte[] arrayOfByte10, arrayOfByte11;
        long l;
        boolean bool7 = false, bool8 = false, bool9 = false, bool10 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 47:
            param1Parcel1.enforceInterface("com.android.internal.widget.ILockSettings");
            str6 = getPassword();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str6);
            return true;
          case 46:
            str6.enforceInterface("com.android.internal.widget.ILockSettings");
            sanitizePassword();
            param1Parcel2.writeNoException();
            return true;
          case 45:
            str6.enforceInterface("com.android.internal.widget.ILockSettings");
            param1Int1 = str6.readInt();
            removeCachedUnifiedChallenge(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 44:
            str6.enforceInterface("com.android.internal.widget.ILockSettings");
            param1Int1 = str6.readInt();
            bool6 = tryUnlockWithCachedUnifiedChallenge(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 43:
            str6.enforceInterface("com.android.internal.widget.ILockSettings");
            bool6 = hasSecureLockScreen();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 42:
            str6.enforceInterface("com.android.internal.widget.ILockSettings");
            str6 = str6.readString();
            closeSession(str6);
            param1Parcel2.writeNoException();
            return true;
          case 41:
            str6.enforceInterface("com.android.internal.widget.ILockSettings");
            str8 = str6.readString();
            arrayOfByte9 = str6.createByteArray();
            arrayList1 = str6.createTypedArrayList(WrappedApplicationKey.CREATOR);
            map2 = recoverKeyChainSnapshot(str8, arrayOfByte9, arrayList1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeMap(map2);
            return true;
          case 40:
            map2.enforceInterface("com.android.internal.widget.ILockSettings");
            str10 = map2.readString();
            str8 = map2.readString();
            if (map2.readInt() != 0) {
              RecoveryCertPath recoveryCertPath = (RecoveryCertPath)RecoveryCertPath.CREATOR.createFromParcel((Parcel)map2);
            } else {
              arrayOfByte9 = null;
            } 
            arrayOfByte10 = map2.createByteArray();
            arrayOfByte11 = map2.createByteArray();
            arrayList = map2.createTypedArrayList(KeyChainProtectionParams.CREATOR);
            arrayOfByte7 = startRecoverySessionWithCertPath(str10, str8, (RecoveryCertPath)arrayOfByte9, arrayOfByte10, arrayOfByte11, arrayList);
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte7);
            return true;
          case 39:
            arrayOfByte7.enforceInterface("com.android.internal.widget.ILockSettings");
            arrayOfInt = getRecoverySecretTypes();
            param1Parcel2.writeNoException();
            param1Parcel2.writeIntArray(arrayOfInt);
            return true;
          case 38:
            arrayOfInt.enforceInterface("com.android.internal.widget.ILockSettings");
            arrayOfInt = arrayOfInt.createIntArray();
            setRecoverySecretTypes(arrayOfInt);
            param1Parcel2.writeNoException();
            return true;
          case 37:
            arrayOfInt.enforceInterface("com.android.internal.widget.ILockSettings");
            map1 = getRecoveryStatus();
            param1Parcel2.writeNoException();
            param1Parcel2.writeMap(map1);
            return true;
          case 36:
            map1.enforceInterface("com.android.internal.widget.ILockSettings");
            str9 = map1.readString();
            i1 = map1.readInt();
            setRecoveryStatus(str9, i1);
            param1Parcel2.writeNoException();
            return true;
          case 35:
            map1.enforceInterface("com.android.internal.widget.ILockSettings");
            arrayOfByte6 = map1.createByteArray();
            setServerParams(arrayOfByte6);
            param1Parcel2.writeNoException();
            return true;
          case 34:
            arrayOfByte6.enforceInterface("com.android.internal.widget.ILockSettings");
            if (arrayOfByte6.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)arrayOfByte6);
            } else {
              arrayOfByte6 = null;
            } 
            setSnapshotCreatedPendingIntent((PendingIntent)arrayOfByte6);
            param1Parcel2.writeNoException();
            return true;
          case 33:
            arrayOfByte6.enforceInterface("com.android.internal.widget.ILockSettings");
            str5 = arrayOfByte6.readString();
            removeKey(str5);
            param1Parcel2.writeNoException();
            return true;
          case 32:
            str5.enforceInterface("com.android.internal.widget.ILockSettings");
            str5 = str5.readString();
            str5 = getKey(str5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str5);
            return true;
          case 31:
            str5.enforceInterface("com.android.internal.widget.ILockSettings");
            str9 = str5.readString();
            arrayOfByte8 = str5.createByteArray();
            arrayOfByte5 = str5.createByteArray();
            str4 = importKeyWithMetadata(str9, arrayOfByte8, arrayOfByte5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str4);
            return true;
          case 30:
            str4.enforceInterface("com.android.internal.widget.ILockSettings");
            str9 = str4.readString();
            arrayOfByte4 = str4.createByteArray();
            str3 = importKey(str9, arrayOfByte4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str3);
            return true;
          case 29:
            str3.enforceInterface("com.android.internal.widget.ILockSettings");
            str9 = str3.readString();
            arrayOfByte3 = str3.createByteArray();
            str2 = generateKeyWithMetadata(str9, arrayOfByte3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str2);
            return true;
          case 28:
            str2.enforceInterface("com.android.internal.widget.ILockSettings");
            str2 = str2.readString();
            str2 = generateKey(str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str2);
            return true;
          case 27:
            str2.enforceInterface("com.android.internal.widget.ILockSettings");
            keyChainSnapshot = getKeyChainSnapshot();
            param1Parcel2.writeNoException();
            if (keyChainSnapshot != null) {
              param1Parcel2.writeInt(1);
              keyChainSnapshot.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 26:
            keyChainSnapshot.enforceInterface("com.android.internal.widget.ILockSettings");
            str9 = keyChainSnapshot.readString();
            arrayOfByte8 = keyChainSnapshot.createByteArray();
            arrayOfByte2 = keyChainSnapshot.createByteArray();
            initRecoveryServiceWithSigFile(str9, arrayOfByte8, arrayOfByte2);
            param1Parcel2.writeNoException();
            return true;
          case 25:
            arrayOfByte2.enforceInterface("com.android.internal.widget.ILockSettings");
            i1 = arrayOfByte2.readInt();
            bool5 = hasPendingEscrowToken(i1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 24:
            arrayOfByte2.enforceInterface("com.android.internal.widget.ILockSettings");
            n = arrayOfByte2.readInt();
            n = getStrongAuthForUser(n);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(n);
            return true;
          case 23:
            arrayOfByte2.enforceInterface("com.android.internal.widget.ILockSettings");
            n = arrayOfByte2.readInt();
            userPresent(n);
            param1Parcel2.writeNoException();
            return true;
          case 22:
            arrayOfByte2.enforceInterface("com.android.internal.widget.ILockSettings");
            systemReady();
            param1Parcel2.writeNoException();
            return true;
          case 21:
            arrayOfByte2.enforceInterface("com.android.internal.widget.ILockSettings");
            n = arrayOfByte2.readInt();
            scheduleNonStrongBiometricIdleTimeout(n);
            param1Parcel2.writeNoException();
            return true;
          case 20:
            arrayOfByte2.enforceInterface("com.android.internal.widget.ILockSettings");
            bool9 = bool10;
            if (arrayOfByte2.readInt() != 0)
              bool9 = true; 
            n = arrayOfByte2.readInt();
            reportSuccessfulBiometricUnlock(bool9, n);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            arrayOfByte2.enforceInterface("com.android.internal.widget.ILockSettings");
            n = arrayOfByte2.readInt();
            param1Int2 = arrayOfByte2.readInt();
            requireStrongAuth(n, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 18:
            arrayOfByte2.enforceInterface("com.android.internal.widget.ILockSettings");
            iStrongAuthTracker = IStrongAuthTracker.Stub.asInterface(arrayOfByte2.readStrongBinder());
            unregisterStrongAuthTracker(iStrongAuthTracker);
            param1Parcel2.writeNoException();
            return true;
          case 17:
            iStrongAuthTracker.enforceInterface("com.android.internal.widget.ILockSettings");
            iStrongAuthTracker = IStrongAuthTracker.Stub.asInterface(iStrongAuthTracker.readStrongBinder());
            registerStrongAuthTracker(iStrongAuthTracker);
            param1Parcel2.writeNoException();
            return true;
          case 16:
            iStrongAuthTracker.enforceInterface("com.android.internal.widget.ILockSettings");
            n = iStrongAuthTracker.readInt();
            bool4 = getSeparateProfileChallengeEnabled(n);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 15:
            iStrongAuthTracker.enforceInterface("com.android.internal.widget.ILockSettings");
            m = iStrongAuthTracker.readInt();
            bool9 = bool7;
            if (iStrongAuthTracker.readInt() != 0)
              bool9 = true; 
            if (iStrongAuthTracker.readInt() != 0) {
              LockscreenCredential lockscreenCredential = (LockscreenCredential)LockscreenCredential.CREATOR.createFromParcel((Parcel)iStrongAuthTracker);
            } else {
              iStrongAuthTracker = null;
            } 
            setSeparateProfileChallengeEnabled(m, bool9, (LockscreenCredential)iStrongAuthTracker);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            iStrongAuthTracker.enforceInterface("com.android.internal.widget.ILockSettings");
            if (iStrongAuthTracker.readInt() != 0) {
              LockscreenCredential lockscreenCredential = (LockscreenCredential)LockscreenCredential.CREATOR.createFromParcel((Parcel)iStrongAuthTracker);
            } else {
              str9 = null;
            } 
            m = iStrongAuthTracker.readInt();
            arrayOfByte1 = getHashFactor((LockscreenCredential)str9, m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte1);
            return true;
          case 13:
            arrayOfByte1.enforceInterface("com.android.internal.widget.ILockSettings");
            m = arrayOfByte1.readInt();
            m = getCredentialType(m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(m);
            return true;
          case 12:
            arrayOfByte1.enforceInterface("com.android.internal.widget.ILockSettings");
            m = arrayOfByte1.readInt();
            bool3 = checkVoldPassword(m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 11:
            arrayOfByte1.enforceInterface("com.android.internal.widget.ILockSettings");
            if (arrayOfByte1.readInt() != 0) {
              LockscreenCredential lockscreenCredential = (LockscreenCredential)LockscreenCredential.CREATOR.createFromParcel((Parcel)arrayOfByte1);
            } else {
              str9 = null;
            } 
            l = arrayOfByte1.readLong();
            k = arrayOfByte1.readInt();
            verifyCredentialResponse2 = verifyTiedProfileChallenge((LockscreenCredential)str9, l, k);
            param1Parcel2.writeNoException();
            if (verifyCredentialResponse2 != null) {
              param1Parcel2.writeInt(1);
              verifyCredentialResponse2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 10:
            verifyCredentialResponse2.enforceInterface("com.android.internal.widget.ILockSettings");
            if (verifyCredentialResponse2.readInt() != 0) {
              LockscreenCredential lockscreenCredential = (LockscreenCredential)LockscreenCredential.CREATOR.createFromParcel((Parcel)verifyCredentialResponse2);
            } else {
              str9 = null;
            } 
            l = verifyCredentialResponse2.readLong();
            k = verifyCredentialResponse2.readInt();
            verifyCredentialResponse2 = verifyCredential((LockscreenCredential)str9, l, k);
            param1Parcel2.writeNoException();
            if (verifyCredentialResponse2 != null) {
              param1Parcel2.writeInt(1);
              verifyCredentialResponse2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 9:
            verifyCredentialResponse2.enforceInterface("com.android.internal.widget.ILockSettings");
            if (verifyCredentialResponse2.readInt() != 0) {
              LockscreenCredential lockscreenCredential = (LockscreenCredential)LockscreenCredential.CREATOR.createFromParcel((Parcel)verifyCredentialResponse2);
            } else {
              str9 = null;
            } 
            k = verifyCredentialResponse2.readInt();
            iCheckCredentialProgressCallback = ICheckCredentialProgressCallback.Stub.asInterface(verifyCredentialResponse2.readStrongBinder());
            verifyCredentialResponse1 = checkCredential((LockscreenCredential)str9, k, iCheckCredentialProgressCallback);
            param1Parcel2.writeNoException();
            if (verifyCredentialResponse1 != null) {
              param1Parcel2.writeInt(1);
              verifyCredentialResponse1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 8:
            verifyCredentialResponse1.enforceInterface("com.android.internal.widget.ILockSettings");
            k = verifyCredentialResponse1.readInt();
            resetKeyStore(k);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            verifyCredentialResponse1.enforceInterface("com.android.internal.widget.ILockSettings");
            if (verifyCredentialResponse1.readInt() != 0) {
              LockscreenCredential lockscreenCredential = (LockscreenCredential)LockscreenCredential.CREATOR.createFromParcel((Parcel)verifyCredentialResponse1);
            } else {
              str9 = null;
            } 
            if (verifyCredentialResponse1.readInt() != 0) {
              LockscreenCredential lockscreenCredential = (LockscreenCredential)LockscreenCredential.CREATOR.createFromParcel((Parcel)verifyCredentialResponse1);
            } else {
              arrayOfByte8 = null;
            } 
            k = verifyCredentialResponse1.readInt();
            bool2 = setLockCredential((LockscreenCredential)str9, (LockscreenCredential)arrayOfByte8, k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 6:
            verifyCredentialResponse1.enforceInterface("com.android.internal.widget.ILockSettings");
            str9 = verifyCredentialResponse1.readString();
            str7 = verifyCredentialResponse1.readString();
            j = verifyCredentialResponse1.readInt();
            str1 = getString(str9, str7, j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 5:
            str1.enforceInterface("com.android.internal.widget.ILockSettings");
            str9 = str1.readString();
            l = str1.readLong();
            j = str1.readInt();
            l = getLong(str9, l, j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 4:
            str1.enforceInterface("com.android.internal.widget.ILockSettings");
            str9 = str1.readString();
            bool9 = bool8;
            if (str1.readInt() != 0)
              bool9 = true; 
            j = str1.readInt();
            bool1 = getBoolean(str9, bool9, j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 3:
            str1.enforceInterface("com.android.internal.widget.ILockSettings");
            str9 = str1.readString();
            str7 = str1.readString();
            i = str1.readInt();
            setString(str9, str7, i);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            str1.enforceInterface("com.android.internal.widget.ILockSettings");
            str9 = str1.readString();
            l = str1.readLong();
            i = str1.readInt();
            setLong(str9, l, i);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("com.android.internal.widget.ILockSettings");
        String str9 = str1.readString();
        if (str1.readInt() != 0)
          bool9 = true; 
        int i = str1.readInt();
        setBoolean(str9, bool9, i);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("com.android.internal.widget.ILockSettings");
      return true;
    }
    
    private static class Proxy implements ILockSettings {
      public static ILockSettings sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.widget.ILockSettings";
      }
      
      public void setBoolean(String param2String, boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool1 && ILockSettings.Stub.getDefaultImpl() != null) {
            ILockSettings.Stub.getDefaultImpl().setBoolean(param2String, param2Boolean, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setLong(String param2String, long param2Long, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeString(param2String);
          parcel1.writeLong(param2Long);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            ILockSettings.Stub.getDefaultImpl().setLong(param2String, param2Long, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setString(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            ILockSettings.Stub.getDefaultImpl().setString(param2String1, param2String2, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getBoolean(String param2String, boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeString(param2String);
          boolean bool1 = true;
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            param2Boolean = ILockSettings.Stub.getDefaultImpl().getBoolean(param2String, param2Boolean, param2Int);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getLong(String param2String, long param2Long, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeString(param2String);
          parcel1.writeLong(param2Long);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            param2Long = ILockSettings.Stub.getDefaultImpl().getLong(param2String, param2Long, param2Int);
            return param2Long;
          } 
          parcel2.readException();
          param2Long = parcel2.readLong();
          return param2Long;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getString(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            param2String1 = ILockSettings.Stub.getDefaultImpl().getString(param2String1, param2String2, param2Int);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setLockCredential(LockscreenCredential param2LockscreenCredential1, LockscreenCredential param2LockscreenCredential2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          boolean bool1 = true;
          if (param2LockscreenCredential1 != null) {
            parcel1.writeInt(1);
            param2LockscreenCredential1.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2LockscreenCredential2 != null) {
            parcel1.writeInt(1);
            param2LockscreenCredential2.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool2 && ILockSettings.Stub.getDefaultImpl() != null) {
            bool1 = ILockSettings.Stub.getDefaultImpl().setLockCredential(param2LockscreenCredential1, param2LockscreenCredential2, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resetKeyStore(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            ILockSettings.Stub.getDefaultImpl().resetKeyStore(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public VerifyCredentialResponse checkCredential(LockscreenCredential param2LockscreenCredential, int param2Int, ICheckCredentialProgressCallback param2ICheckCredentialProgressCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          if (param2LockscreenCredential != null) {
            parcel1.writeInt(1);
            param2LockscreenCredential.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          if (param2ICheckCredentialProgressCallback != null) {
            iBinder = param2ICheckCredentialProgressCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null)
            return ILockSettings.Stub.getDefaultImpl().checkCredential(param2LockscreenCredential, param2Int, param2ICheckCredentialProgressCallback); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            VerifyCredentialResponse verifyCredentialResponse = (VerifyCredentialResponse)VerifyCredentialResponse.CREATOR.createFromParcel(parcel2);
          } else {
            param2LockscreenCredential = null;
          } 
          return (VerifyCredentialResponse)param2LockscreenCredential;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public VerifyCredentialResponse verifyCredential(LockscreenCredential param2LockscreenCredential, long param2Long, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          if (param2LockscreenCredential != null) {
            parcel1.writeInt(1);
            param2LockscreenCredential.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeLong(param2Long);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null)
            return ILockSettings.Stub.getDefaultImpl().verifyCredential(param2LockscreenCredential, param2Long, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            VerifyCredentialResponse verifyCredentialResponse = (VerifyCredentialResponse)VerifyCredentialResponse.CREATOR.createFromParcel(parcel2);
          } else {
            param2LockscreenCredential = null;
          } 
          return (VerifyCredentialResponse)param2LockscreenCredential;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public VerifyCredentialResponse verifyTiedProfileChallenge(LockscreenCredential param2LockscreenCredential, long param2Long, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          if (param2LockscreenCredential != null) {
            parcel1.writeInt(1);
            param2LockscreenCredential.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeLong(param2Long);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null)
            return ILockSettings.Stub.getDefaultImpl().verifyTiedProfileChallenge(param2LockscreenCredential, param2Long, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            VerifyCredentialResponse verifyCredentialResponse = (VerifyCredentialResponse)VerifyCredentialResponse.CREATOR.createFromParcel(parcel2);
          } else {
            param2LockscreenCredential = null;
          } 
          return (VerifyCredentialResponse)param2LockscreenCredential;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean checkVoldPassword(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(12, parcel1, parcel2, 0);
          if (!bool2 && ILockSettings.Stub.getDefaultImpl() != null) {
            bool1 = ILockSettings.Stub.getDefaultImpl().checkVoldPassword(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getCredentialType(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            param2Int = ILockSettings.Stub.getDefaultImpl().getCredentialType(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getHashFactor(LockscreenCredential param2LockscreenCredential, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          if (param2LockscreenCredential != null) {
            parcel1.writeInt(1);
            param2LockscreenCredential.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null)
            return ILockSettings.Stub.getDefaultImpl().getHashFactor(param2LockscreenCredential, param2Int); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSeparateProfileChallengeEnabled(int param2Int, boolean param2Boolean, LockscreenCredential param2LockscreenCredential) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          if (param2LockscreenCredential != null) {
            parcel1.writeInt(1);
            param2LockscreenCredential.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool1 = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool1 && ILockSettings.Stub.getDefaultImpl() != null) {
            ILockSettings.Stub.getDefaultImpl().setSeparateProfileChallengeEnabled(param2Int, param2Boolean, param2LockscreenCredential);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getSeparateProfileChallengeEnabled(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(16, parcel1, parcel2, 0);
          if (!bool2 && ILockSettings.Stub.getDefaultImpl() != null) {
            bool1 = ILockSettings.Stub.getDefaultImpl().getSeparateProfileChallengeEnabled(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerStrongAuthTracker(IStrongAuthTracker param2IStrongAuthTracker) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          if (param2IStrongAuthTracker != null) {
            iBinder = param2IStrongAuthTracker.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            ILockSettings.Stub.getDefaultImpl().registerStrongAuthTracker(param2IStrongAuthTracker);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterStrongAuthTracker(IStrongAuthTracker param2IStrongAuthTracker) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          if (param2IStrongAuthTracker != null) {
            iBinder = param2IStrongAuthTracker.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            ILockSettings.Stub.getDefaultImpl().unregisterStrongAuthTracker(param2IStrongAuthTracker);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requireStrongAuth(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            ILockSettings.Stub.getDefaultImpl().requireStrongAuth(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportSuccessfulBiometricUnlock(boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool1 && ILockSettings.Stub.getDefaultImpl() != null) {
            ILockSettings.Stub.getDefaultImpl().reportSuccessfulBiometricUnlock(param2Boolean, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void scheduleNonStrongBiometricIdleTimeout(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            ILockSettings.Stub.getDefaultImpl().scheduleNonStrongBiometricIdleTimeout(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void systemReady() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            ILockSettings.Stub.getDefaultImpl().systemReady();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void userPresent(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            ILockSettings.Stub.getDefaultImpl().userPresent(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getStrongAuthForUser(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            param2Int = ILockSettings.Stub.getDefaultImpl().getStrongAuthForUser(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasPendingEscrowToken(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(25, parcel1, parcel2, 0);
          if (!bool2 && ILockSettings.Stub.getDefaultImpl() != null) {
            bool1 = ILockSettings.Stub.getDefaultImpl().hasPendingEscrowToken(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void initRecoveryServiceWithSigFile(String param2String, byte[] param2ArrayOfbyte1, byte[] param2ArrayOfbyte2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeString(param2String);
          parcel1.writeByteArray(param2ArrayOfbyte1);
          parcel1.writeByteArray(param2ArrayOfbyte2);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            ILockSettings.Stub.getDefaultImpl().initRecoveryServiceWithSigFile(param2String, param2ArrayOfbyte1, param2ArrayOfbyte2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public KeyChainSnapshot getKeyChainSnapshot() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          KeyChainSnapshot keyChainSnapshot;
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            keyChainSnapshot = ILockSettings.Stub.getDefaultImpl().getKeyChainSnapshot();
            return keyChainSnapshot;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            keyChainSnapshot = (KeyChainSnapshot)KeyChainSnapshot.CREATOR.createFromParcel(parcel2);
          } else {
            keyChainSnapshot = null;
          } 
          return keyChainSnapshot;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String generateKey(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            param2String = ILockSettings.Stub.getDefaultImpl().generateKey(param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String generateKeyWithMetadata(String param2String, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeString(param2String);
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            param2String = ILockSettings.Stub.getDefaultImpl().generateKeyWithMetadata(param2String, param2ArrayOfbyte);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String importKey(String param2String, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeString(param2String);
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            param2String = ILockSettings.Stub.getDefaultImpl().importKey(param2String, param2ArrayOfbyte);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String importKeyWithMetadata(String param2String, byte[] param2ArrayOfbyte1, byte[] param2ArrayOfbyte2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeString(param2String);
          parcel1.writeByteArray(param2ArrayOfbyte1);
          parcel1.writeByteArray(param2ArrayOfbyte2);
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            param2String = ILockSettings.Stub.getDefaultImpl().importKeyWithMetadata(param2String, param2ArrayOfbyte1, param2ArrayOfbyte2);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getKey(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            param2String = ILockSettings.Stub.getDefaultImpl().getKey(param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeKey(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            ILockSettings.Stub.getDefaultImpl().removeKey(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSnapshotCreatedPendingIntent(PendingIntent param2PendingIntent) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          if (param2PendingIntent != null) {
            parcel1.writeInt(1);
            param2PendingIntent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            ILockSettings.Stub.getDefaultImpl().setSnapshotCreatedPendingIntent(param2PendingIntent);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setServerParams(byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            ILockSettings.Stub.getDefaultImpl().setServerParams(param2ArrayOfbyte);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setRecoveryStatus(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            ILockSettings.Stub.getDefaultImpl().setRecoveryStatus(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Map getRecoveryStatus() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          boolean bool = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null)
            return ILockSettings.Stub.getDefaultImpl().getRecoveryStatus(); 
          parcel2.readException();
          ClassLoader classLoader = getClass().getClassLoader();
          return parcel2.readHashMap(classLoader);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setRecoverySecretTypes(int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(38, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            ILockSettings.Stub.getDefaultImpl().setRecoverySecretTypes(param2ArrayOfint);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] getRecoverySecretTypes() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          boolean bool = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null)
            return ILockSettings.Stub.getDefaultImpl().getRecoverySecretTypes(); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] startRecoverySessionWithCertPath(String param2String1, String param2String2, RecoveryCertPath param2RecoveryCertPath, byte[] param2ArrayOfbyte1, byte[] param2ArrayOfbyte2, List<KeyChainProtectionParams> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeString(param2String2);
              if (param2RecoveryCertPath != null) {
                parcel1.writeInt(1);
                param2RecoveryCertPath.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              try {
                parcel1.writeByteArray(param2ArrayOfbyte1);
                try {
                  parcel1.writeByteArray(param2ArrayOfbyte2);
                  try {
                    parcel1.writeTypedList(param2List);
                    boolean bool = this.mRemote.transact(40, parcel1, parcel2, 0);
                    if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
                      byte[] arrayOfByte1 = ILockSettings.Stub.getDefaultImpl().startRecoverySessionWithCertPath(param2String1, param2String2, param2RecoveryCertPath, param2ArrayOfbyte1, param2ArrayOfbyte2, param2List);
                      parcel2.recycle();
                      parcel1.recycle();
                      return arrayOfByte1;
                    } 
                    parcel2.readException();
                    byte[] arrayOfByte = parcel2.createByteArray();
                    parcel2.recycle();
                    parcel1.recycle();
                    return arrayOfByte;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public Map recoverKeyChainSnapshot(String param2String, byte[] param2ArrayOfbyte, List<WrappedApplicationKey> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeString(param2String);
          parcel1.writeByteArray(param2ArrayOfbyte);
          parcel1.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(41, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null)
            return ILockSettings.Stub.getDefaultImpl().recoverKeyChainSnapshot(param2String, param2ArrayOfbyte, param2List); 
          parcel2.readException();
          ClassLoader classLoader = getClass().getClassLoader();
          return parcel2.readHashMap(classLoader);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void closeSession(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(42, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            ILockSettings.Stub.getDefaultImpl().closeSession(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasSecureLockScreen() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(43, parcel1, parcel2, 0);
          if (!bool2 && ILockSettings.Stub.getDefaultImpl() != null) {
            bool1 = ILockSettings.Stub.getDefaultImpl().hasSecureLockScreen();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean tryUnlockWithCachedUnifiedChallenge(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(44, parcel1, parcel2, 0);
          if (!bool2 && ILockSettings.Stub.getDefaultImpl() != null) {
            bool1 = ILockSettings.Stub.getDefaultImpl().tryUnlockWithCachedUnifiedChallenge(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeCachedUnifiedChallenge(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(45, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            ILockSettings.Stub.getDefaultImpl().removeCachedUnifiedChallenge(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sanitizePassword() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          boolean bool = this.mRemote.transact(46, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null) {
            ILockSettings.Stub.getDefaultImpl().sanitizePassword();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getPassword() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.ILockSettings");
          boolean bool = this.mRemote.transact(47, parcel1, parcel2, 0);
          if (!bool && ILockSettings.Stub.getDefaultImpl() != null)
            return ILockSettings.Stub.getDefaultImpl().getPassword(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ILockSettings param1ILockSettings) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ILockSettings != null) {
          Proxy.sDefaultImpl = param1ILockSettings;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ILockSettings getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
