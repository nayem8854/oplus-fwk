package com.android.internal.widget;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.StateSet;
import android.view.KeyEvent;
import android.widget.TextView;

public class NumericTextView extends TextView {
  private static final double LOG_RADIX = Math.log(10.0D);
  
  private int mMinValue = 0;
  
  private int mMaxValue = 99;
  
  private int mMaxCount = 2;
  
  private boolean mShowLeadingZeroes = true;
  
  private static final int RADIX = 10;
  
  private int mCount;
  
  private OnValueChangedListener mListener;
  
  private int mPreviousValue;
  
  private int mValue;
  
  public NumericTextView(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    int i = getTextColors().getColorForState(StateSet.get(0), 0);
    setHintTextColor(i);
    setFocusable(true);
  }
  
  protected void onFocusChanged(boolean paramBoolean, int paramInt, Rect paramRect) {
    super.onFocusChanged(paramBoolean, paramInt, paramRect);
    if (paramBoolean) {
      this.mPreviousValue = this.mValue;
      this.mValue = 0;
      this.mCount = 0;
      setHint(getText());
      setText("");
    } else {
      if (this.mCount == 0) {
        this.mValue = this.mPreviousValue;
        setText(getHint());
        setHint("");
      } 
      paramInt = this.mValue;
      int i = this.mMinValue;
      if (paramInt < i)
        this.mValue = i; 
      setValue(this.mValue);
      OnValueChangedListener onValueChangedListener = this.mListener;
      if (onValueChangedListener != null)
        onValueChangedListener.onValueChanged(this, this.mValue, true, true); 
    } 
  }
  
  public final void setValue(int paramInt) {
    if (this.mValue != paramInt) {
      this.mValue = paramInt;
      updateDisplayedValue();
    } 
  }
  
  public final int getValue() {
    return this.mValue;
  }
  
  public final void setRange(int paramInt1, int paramInt2) {
    if (this.mMinValue != paramInt1)
      this.mMinValue = paramInt1; 
    if (this.mMaxValue != paramInt2) {
      this.mMaxValue = paramInt2;
      this.mMaxCount = (int)(Math.log(paramInt2) / LOG_RADIX) + 1;
      updateMinimumWidth();
      updateDisplayedValue();
    } 
  }
  
  public final int getRangeMinimum() {
    return this.mMinValue;
  }
  
  public final int getRangeMaximum() {
    return this.mMaxValue;
  }
  
  public final void setShowLeadingZeroes(boolean paramBoolean) {
    if (this.mShowLeadingZeroes != paramBoolean) {
      this.mShowLeadingZeroes = paramBoolean;
      updateDisplayedValue();
    } 
  }
  
  public final boolean getShowLeadingZeroes() {
    return this.mShowLeadingZeroes;
  }
  
  private void updateDisplayedValue() {
    String str;
    if (this.mShowLeadingZeroes) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("%0");
      stringBuilder.append(this.mMaxCount);
      stringBuilder.append("d");
      str = stringBuilder.toString();
    } else {
      str = "%d";
    } 
    setText(String.format(str, new Object[] { Integer.valueOf(this.mValue) }));
  }
  
  private void updateMinimumWidth() {
    CharSequence charSequence = getText();
    int i = 0;
    for (byte b = 0; b < this.mMaxValue; b++, i = k) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("%0");
      stringBuilder.append(this.mMaxCount);
      stringBuilder.append("d");
      setText(String.format(stringBuilder.toString(), new Object[] { Integer.valueOf(b) }));
      measure(0, 0);
      int j = getMeasuredWidth();
      int k = i;
      if (j > i)
        k = j; 
    } 
    setText(charSequence);
    setMinWidth(i);
    setMinimumWidth(i);
  }
  
  public final void setOnDigitEnteredListener(OnValueChangedListener paramOnValueChangedListener) {
    this.mListener = paramOnValueChangedListener;
  }
  
  public final OnValueChangedListener getOnDigitEnteredListener() {
    return this.mListener;
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
    return (!isKeyCodeNumeric(paramInt) && paramInt != 67) ? (
      
      super.onKeyDown(paramInt, paramKeyEvent)) : true;
  }
  
  public boolean onKeyMultiple(int paramInt1, int paramInt2, KeyEvent paramKeyEvent) {
    return (!isKeyCodeNumeric(paramInt1) && paramInt1 != 67) ? (
      
      super.onKeyMultiple(paramInt1, paramInt2, paramKeyEvent)) : true;
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent) {
    return (handleKeyUp(paramInt) || 
      super.onKeyUp(paramInt, paramKeyEvent));
  }
  
  private boolean handleKeyUp(int paramInt) {
    String str;
    boolean bool = false;
    if (paramInt == 67) {
      paramInt = this.mCount;
      if (paramInt > 0) {
        this.mValue /= 10;
        this.mCount = paramInt - 1;
      } 
    } else if (isKeyCodeNumeric(paramInt)) {
      if (this.mCount < this.mMaxCount) {
        paramInt = numericKeyCodeToInt(paramInt);
        paramInt = this.mValue * 10 + paramInt;
        if (paramInt <= this.mMaxValue) {
          this.mValue = paramInt;
          this.mCount++;
        } 
      } 
    } else {
      return false;
    } 
    if (this.mCount > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("%0");
      stringBuilder.append(this.mCount);
      stringBuilder.append("d");
      str = String.format(stringBuilder.toString(), new Object[] { Integer.valueOf(this.mValue) });
    } else {
      str = "";
    } 
    setText(str);
    if (this.mListener != null) {
      boolean bool1;
      if (this.mValue >= this.mMinValue) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      if (this.mCount >= this.mMaxCount || this.mValue * 10 > this.mMaxValue)
        bool = true; 
      this.mListener.onValueChanged(this, this.mValue, bool1, bool);
    } 
    return true;
  }
  
  private static boolean isKeyCodeNumeric(int paramInt) {
    return (paramInt == 7 || paramInt == 8 || paramInt == 9 || paramInt == 10 || paramInt == 11 || paramInt == 12 || paramInt == 13 || paramInt == 14 || paramInt == 15 || paramInt == 16);
  }
  
  private static int numericKeyCodeToInt(int paramInt) {
    return paramInt - 7;
  }
  
  class OnValueChangedListener {
    public abstract void onValueChanged(NumericTextView param1NumericTextView, int param1Int, boolean param1Boolean1, boolean param1Boolean2);
  }
}
