package com.android.internal.widget;

import android.animation.LayoutTransition;
import android.app.ActionBar;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Layout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.CollapsibleActionView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.accessibility.AccessibilityEvent;
import android.widget.ActionMenuPresenter;
import android.widget.ActionMenuView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.android.internal.R;
import com.android.internal.view.menu.ActionMenuItem;
import com.android.internal.view.menu.MenuBuilder;
import com.android.internal.view.menu.MenuItemImpl;
import com.android.internal.view.menu.MenuPresenter;
import com.android.internal.view.menu.MenuView;
import com.android.internal.view.menu.SubMenuBuilder;

public class ActionBarView extends AbsActionBarView implements DecorToolbar {
  private int mDisplayOptions = -1;
  
  private int mDefaultUpDescription = 17039592;
  
  private final View.OnClickListener mExpandedActionViewUpListener = (View.OnClickListener)new Object(this);
  
  private final View.OnClickListener mUpClickListener = (View.OnClickListener)new Object(this);
  
  private static final int DEFAULT_CUSTOM_GRAVITY = 8388627;
  
  public static final int DISPLAY_DEFAULT = 0;
  
  private static final int DISPLAY_RELAYOUT_MASK = 63;
  
  private static final String TAG = "ActionBarView";
  
  private ActionBarContextView mContextView;
  
  private View mCustomNavView;
  
  View mExpandedActionView;
  
  private HomeView mExpandedHomeLayout;
  
  private ExpandedActionViewMenuPresenter mExpandedMenuPresenter;
  
  private CharSequence mHomeDescription;
  
  private int mHomeDescriptionRes;
  
  private HomeView mHomeLayout;
  
  private Drawable mIcon;
  
  private boolean mIncludeTabs;
  
  private final int mIndeterminateProgressStyle;
  
  private ProgressBar mIndeterminateProgressView;
  
  private boolean mIsCollapsible;
  
  private int mItemPadding;
  
  private LinearLayout mListNavLayout;
  
  private Drawable mLogo;
  
  private ActionMenuItem mLogoNavItem;
  
  private boolean mMenuPrepared;
  
  private AdapterView.OnItemSelectedListener mNavItemSelectedListener;
  
  private int mNavigationMode;
  
  private MenuBuilder mOptionsMenu;
  
  private int mProgressBarPadding;
  
  private final int mProgressStyle;
  
  private ProgressBar mProgressView;
  
  private Spinner mSpinner;
  
  private SpinnerAdapter mSpinnerAdapter;
  
  private CharSequence mSubtitle;
  
  private final int mSubtitleStyleRes;
  
  private TextView mSubtitleView;
  
  private ScrollingTabContainerView mTabScrollView;
  
  private Runnable mTabSelector;
  
  private CharSequence mTitle;
  
  private LinearLayout mTitleLayout;
  
  private final int mTitleStyleRes;
  
  private TextView mTitleView;
  
  private ViewGroup mUpGoerFive;
  
  private boolean mUserTitle;
  
  private boolean mWasHomeEnabled;
  
  Window.Callback mWindowCallback;
  
  public ActionBarView(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    setBackgroundResource(0);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ActionBar, 16843470, 0);
    this.mNavigationMode = typedArray.getInt(7, 0);
    this.mTitle = typedArray.getText(5);
    this.mSubtitle = typedArray.getText(9);
    this.mLogo = typedArray.getDrawable(6);
    this.mIcon = typedArray.getDrawable(0);
    LayoutInflater layoutInflater = LayoutInflater.from(paramContext);
    int i = typedArray.getResourceId(16, 17367068);
    ViewGroup viewGroup = (ViewGroup)layoutInflater.inflate(17367071, this, false);
    this.mHomeLayout = (HomeView)layoutInflater.inflate(i, viewGroup, false);
    HomeView homeView = (HomeView)layoutInflater.inflate(i, this.mUpGoerFive, false);
    homeView.setShowUp(true);
    this.mExpandedHomeLayout.setOnClickListener(this.mExpandedActionViewUpListener);
    this.mExpandedHomeLayout.setContentDescription(getResources().getText(this.mDefaultUpDescription));
    Drawable drawable = this.mUpGoerFive.getBackground();
    if (drawable != null)
      this.mExpandedHomeLayout.setBackground(drawable.getConstantState().newDrawable()); 
    this.mExpandedHomeLayout.setEnabled(true);
    this.mExpandedHomeLayout.setFocusable(true);
    this.mTitleStyleRes = typedArray.getResourceId(11, 0);
    this.mSubtitleStyleRes = typedArray.getResourceId(12, 0);
    this.mProgressStyle = typedArray.getResourceId(1, 0);
    this.mIndeterminateProgressStyle = typedArray.getResourceId(14, 0);
    this.mProgressBarPadding = typedArray.getDimensionPixelOffset(15, 0);
    this.mItemPadding = typedArray.getDimensionPixelOffset(17, 0);
    setDisplayOptions(typedArray.getInt(8, 0));
    i = typedArray.getResourceId(10, 0);
    if (i != 0) {
      this.mCustomNavView = layoutInflater.inflate(i, this, false);
      this.mNavigationMode = 0;
      setDisplayOptions(0x10 | this.mDisplayOptions);
    } 
    this.mContentHeight = typedArray.getLayoutDimension(4, 0);
    typedArray.recycle();
    this.mLogoNavItem = new ActionMenuItem(paramContext, 0, 16908332, 0, 0, this.mTitle);
    this.mUpGoerFive.setOnClickListener(this.mUpClickListener);
    this.mUpGoerFive.setClickable(true);
    this.mUpGoerFive.setFocusable(true);
    if (getImportantForAccessibility() == 0)
      setImportantForAccessibility(1); 
  }
  
  protected void onConfigurationChanged(Configuration paramConfiguration) {
    super.onConfigurationChanged(paramConfiguration);
    this.mTitleView = null;
    this.mSubtitleView = null;
    LinearLayout linearLayout = this.mTitleLayout;
    if (linearLayout != null) {
      ViewParent viewParent = linearLayout.getParent();
      ViewGroup viewGroup = this.mUpGoerFive;
      if (viewParent == viewGroup)
        viewGroup.removeView((View)this.mTitleLayout); 
    } 
    this.mTitleLayout = null;
    if ((this.mDisplayOptions & 0x8) != 0)
      initTitle(); 
    int i = this.mHomeDescriptionRes;
    if (i != 0)
      setNavigationContentDescription(i); 
    ScrollingTabContainerView scrollingTabContainerView = this.mTabScrollView;
    if (scrollingTabContainerView != null && this.mIncludeTabs) {
      ViewGroup.LayoutParams layoutParams = scrollingTabContainerView.getLayoutParams();
      if (layoutParams != null) {
        layoutParams.width = -2;
        layoutParams.height = -1;
      } 
      this.mTabScrollView.setAllowCollapse(true);
    } 
  }
  
  public void setWindowCallback(Window.Callback paramCallback) {
    this.mWindowCallback = paramCallback;
  }
  
  public void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    removeCallbacks(this.mTabSelector);
    if (this.mActionMenuPresenter != null) {
      this.mActionMenuPresenter.hideOverflowMenu();
      this.mActionMenuPresenter.hideSubMenus();
    } 
  }
  
  public boolean shouldDelayChildPressedState() {
    return false;
  }
  
  public void initProgress() {
    ProgressBar progressBar = new ProgressBar(this.mContext, null, 0, this.mProgressStyle);
    progressBar.setId(16909326);
    this.mProgressView.setMax(10000);
    this.mProgressView.setVisibility(8);
    addView((View)this.mProgressView);
  }
  
  public void initIndeterminateProgress() {
    ProgressBar progressBar = new ProgressBar(this.mContext, null, 0, this.mIndeterminateProgressStyle);
    progressBar.setId(16909325);
    this.mIndeterminateProgressView.setVisibility(8);
    addView((View)this.mIndeterminateProgressView);
  }
  
  public void setSplitToolbar(boolean paramBoolean) {
    if (this.mSplitActionBar != paramBoolean) {
      if (this.mMenuView != null) {
        ViewGroup viewGroup = (ViewGroup)this.mMenuView.getParent();
        if (viewGroup != null)
          viewGroup.removeView((View)this.mMenuView); 
        if (paramBoolean) {
          if (this.mSplitView != null)
            this.mSplitView.addView((View)this.mMenuView); 
          (this.mMenuView.getLayoutParams()).width = -1;
        } else {
          addView((View)this.mMenuView);
          (this.mMenuView.getLayoutParams()).width = -2;
        } 
        this.mMenuView.requestLayout();
      } 
      if (this.mSplitView != null) {
        byte b;
        ViewGroup viewGroup = this.mSplitView;
        if (paramBoolean) {
          b = 0;
        } else {
          b = 8;
        } 
        viewGroup.setVisibility(b);
      } 
      if (this.mActionMenuPresenter != null)
        if (!paramBoolean) {
          ActionMenuPresenter actionMenuPresenter = this.mActionMenuPresenter;
          boolean bool = getResources().getBoolean(17891336);
          actionMenuPresenter.setExpandedActionViewsExclusive(bool);
        } else {
          this.mActionMenuPresenter.setExpandedActionViewsExclusive(false);
          ActionMenuPresenter actionMenuPresenter = this.mActionMenuPresenter;
          int i = (getContext().getResources().getDisplayMetrics()).widthPixels;
          actionMenuPresenter.setWidthLimit(i, true);
          this.mActionMenuPresenter.setItemLimit(2147483647);
        }  
      super.setSplitToolbar(paramBoolean);
    } 
  }
  
  public boolean isSplit() {
    return this.mSplitActionBar;
  }
  
  public boolean canSplit() {
    return true;
  }
  
  public boolean hasEmbeddedTabs() {
    return this.mIncludeTabs;
  }
  
  public void setEmbeddedTabView(ScrollingTabContainerView paramScrollingTabContainerView) {
    boolean bool;
    ScrollingTabContainerView scrollingTabContainerView = this.mTabScrollView;
    if (scrollingTabContainerView != null)
      removeView((View)scrollingTabContainerView); 
    this.mTabScrollView = paramScrollingTabContainerView;
    if (paramScrollingTabContainerView != null) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mIncludeTabs = bool;
    if (bool && this.mNavigationMode == 2) {
      addView((View)this.mTabScrollView);
      ViewGroup.LayoutParams layoutParams = this.mTabScrollView.getLayoutParams();
      layoutParams.width = -2;
      layoutParams.height = -1;
      paramScrollingTabContainerView.setAllowCollapse(true);
    } 
  }
  
  public void setMenuPrepared() {
    this.mMenuPrepared = true;
  }
  
  public void setMenu(Menu paramMenu, MenuPresenter.Callback paramCallback) {
    ActionMenuView actionMenuView;
    MenuBuilder menuBuilder = this.mOptionsMenu;
    if (paramMenu == menuBuilder)
      return; 
    if (menuBuilder != null) {
      menuBuilder.removeMenuPresenter((MenuPresenter)this.mActionMenuPresenter);
      this.mOptionsMenu.removeMenuPresenter(this.mExpandedMenuPresenter);
    } 
    paramMenu = paramMenu;
    this.mOptionsMenu = (MenuBuilder)paramMenu;
    if (this.mMenuView != null) {
      ViewGroup viewGroup = (ViewGroup)this.mMenuView.getParent();
      if (viewGroup != null)
        viewGroup.removeView((View)this.mMenuView); 
    } 
    if (this.mActionMenuPresenter == null) {
      this.mActionMenuPresenter = new ActionMenuPresenter(this.mContext);
      this.mActionMenuPresenter.setCallback(paramCallback);
      this.mActionMenuPresenter.setId(16908717);
      this.mExpandedMenuPresenter = new ExpandedActionViewMenuPresenter();
    } 
    ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-2, -1);
    if (!this.mSplitActionBar) {
      ActionMenuPresenter actionMenuPresenter = this.mActionMenuPresenter;
      boolean bool = getResources().getBoolean(17891336);
      actionMenuPresenter.setExpandedActionViewsExclusive(bool);
      configPresenters((MenuBuilder)paramMenu);
      actionMenuView = (ActionMenuView)this.mActionMenuPresenter.getMenuView(this);
      ViewGroup viewGroup = (ViewGroup)actionMenuView.getParent();
      if (viewGroup != null && viewGroup != this)
        viewGroup.removeView((View)actionMenuView); 
      addView((View)actionMenuView, layoutParams);
    } else {
      this.mActionMenuPresenter.setExpandedActionViewsExclusive(false);
      ActionMenuPresenter actionMenuPresenter = this.mActionMenuPresenter;
      int i = (getContext().getResources().getDisplayMetrics()).widthPixels;
      actionMenuPresenter.setWidthLimit(i, true);
      this.mActionMenuPresenter.setItemLimit(2147483647);
      layoutParams.width = -1;
      layoutParams.height = -2;
      configPresenters((MenuBuilder)actionMenuView);
      actionMenuView = (ActionMenuView)this.mActionMenuPresenter.getMenuView(this);
      if (this.mSplitView != null) {
        ViewGroup viewGroup = (ViewGroup)actionMenuView.getParent();
        if (viewGroup != null && viewGroup != this.mSplitView)
          viewGroup.removeView((View)actionMenuView); 
        actionMenuView.setVisibility(getAnimatedVisibility());
        this.mSplitView.addView((View)actionMenuView, layoutParams);
      } else {
        actionMenuView.setLayoutParams(layoutParams);
      } 
    } 
    this.mMenuView = actionMenuView;
  }
  
  private void configPresenters(MenuBuilder paramMenuBuilder) {
    if (paramMenuBuilder != null) {
      paramMenuBuilder.addMenuPresenter((MenuPresenter)this.mActionMenuPresenter, this.mPopupContext);
      paramMenuBuilder.addMenuPresenter(this.mExpandedMenuPresenter, this.mPopupContext);
    } else {
      this.mActionMenuPresenter.initForMenu(this.mPopupContext, null);
      this.mExpandedMenuPresenter.initForMenu(this.mPopupContext, null);
      this.mActionMenuPresenter.updateMenuView(true);
      this.mExpandedMenuPresenter.updateMenuView(true);
    } 
  }
  
  public boolean hasExpandedActionView() {
    boolean bool;
    ExpandedActionViewMenuPresenter expandedActionViewMenuPresenter = this.mExpandedMenuPresenter;
    if (expandedActionViewMenuPresenter != null && expandedActionViewMenuPresenter.mCurrentExpandedItem != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void collapseActionView() {
    MenuItemImpl menuItemImpl;
    ExpandedActionViewMenuPresenter expandedActionViewMenuPresenter = this.mExpandedMenuPresenter;
    if (expandedActionViewMenuPresenter == null) {
      expandedActionViewMenuPresenter = null;
    } else {
      menuItemImpl = expandedActionViewMenuPresenter.mCurrentExpandedItem;
    } 
    if (menuItemImpl != null)
      menuItemImpl.collapseActionView(); 
  }
  
  public void setCustomView(View paramView) {
    boolean bool;
    if ((this.mDisplayOptions & 0x10) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    View view = this.mCustomNavView;
    if (view != null && bool)
      removeView(view); 
    this.mCustomNavView = paramView;
    if (paramView != null && bool)
      addView(paramView); 
  }
  
  public CharSequence getTitle() {
    return this.mTitle;
  }
  
  public void setTitle(CharSequence paramCharSequence) {
    this.mUserTitle = true;
    setTitleImpl(paramCharSequence);
  }
  
  public void setWindowTitle(CharSequence paramCharSequence) {
    if (!this.mUserTitle)
      setTitleImpl(paramCharSequence); 
  }
  
  private void setTitleImpl(CharSequence paramCharSequence) {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: putfield mTitle : Ljava/lang/CharSequence;
    //   5: aload_0
    //   6: getfield mTitleView : Landroid/widget/TextView;
    //   9: astore_2
    //   10: aload_2
    //   11: ifnull -> 89
    //   14: aload_2
    //   15: aload_1
    //   16: invokevirtual setText : (Ljava/lang/CharSequence;)V
    //   19: aload_0
    //   20: getfield mExpandedActionView : Landroid/view/View;
    //   23: astore_2
    //   24: bipush #8
    //   26: istore_3
    //   27: aload_2
    //   28: ifnonnull -> 69
    //   31: aload_0
    //   32: getfield mDisplayOptions : I
    //   35: bipush #8
    //   37: iand
    //   38: ifeq -> 69
    //   41: aload_0
    //   42: getfield mTitle : Ljava/lang/CharSequence;
    //   45: astore_2
    //   46: aload_2
    //   47: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   50: ifeq -> 63
    //   53: aload_0
    //   54: getfield mSubtitle : Ljava/lang/CharSequence;
    //   57: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   60: ifne -> 69
    //   63: iconst_1
    //   64: istore #4
    //   66: goto -> 72
    //   69: iconst_0
    //   70: istore #4
    //   72: aload_0
    //   73: getfield mTitleLayout : Landroid/widget/LinearLayout;
    //   76: astore_2
    //   77: iload #4
    //   79: ifeq -> 84
    //   82: iconst_0
    //   83: istore_3
    //   84: aload_2
    //   85: iload_3
    //   86: invokevirtual setVisibility : (I)V
    //   89: aload_0
    //   90: getfield mLogoNavItem : Lcom/android/internal/view/menu/ActionMenuItem;
    //   93: astore_2
    //   94: aload_2
    //   95: ifnull -> 104
    //   98: aload_2
    //   99: aload_1
    //   100: invokevirtual setTitle : (Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    //   103: pop
    //   104: aload_0
    //   105: aload_0
    //   106: getfield mUpGoerFive : Landroid/view/ViewGroup;
    //   109: invokevirtual isEnabled : ()Z
    //   112: invokespecial updateHomeAccessibility : (Z)V
    //   115: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #500	-> 0
    //   #501	-> 5
    //   #502	-> 14
    //   #503	-> 19
    //   #505	-> 46
    //   #506	-> 72
    //   #508	-> 89
    //   #509	-> 98
    //   #511	-> 104
    //   #512	-> 115
  }
  
  public CharSequence getSubtitle() {
    return this.mSubtitle;
  }
  
  public void setSubtitle(CharSequence paramCharSequence) {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: putfield mSubtitle : Ljava/lang/CharSequence;
    //   5: aload_0
    //   6: getfield mSubtitleView : Landroid/widget/TextView;
    //   9: astore_2
    //   10: aload_2
    //   11: ifnull -> 120
    //   14: aload_2
    //   15: aload_1
    //   16: invokevirtual setText : (Ljava/lang/CharSequence;)V
    //   19: aload_0
    //   20: getfield mSubtitleView : Landroid/widget/TextView;
    //   23: astore_2
    //   24: iconst_0
    //   25: istore_3
    //   26: aload_1
    //   27: ifnull -> 36
    //   30: iconst_0
    //   31: istore #4
    //   33: goto -> 40
    //   36: bipush #8
    //   38: istore #4
    //   40: aload_2
    //   41: iload #4
    //   43: invokevirtual setVisibility : (I)V
    //   46: aload_0
    //   47: getfield mExpandedActionView : Landroid/view/View;
    //   50: ifnonnull -> 91
    //   53: aload_0
    //   54: getfield mDisplayOptions : I
    //   57: bipush #8
    //   59: iand
    //   60: ifeq -> 91
    //   63: aload_0
    //   64: getfield mTitle : Ljava/lang/CharSequence;
    //   67: astore_1
    //   68: aload_1
    //   69: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   72: ifeq -> 85
    //   75: aload_0
    //   76: getfield mSubtitle : Ljava/lang/CharSequence;
    //   79: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   82: ifne -> 91
    //   85: iconst_1
    //   86: istore #4
    //   88: goto -> 94
    //   91: iconst_0
    //   92: istore #4
    //   94: aload_0
    //   95: getfield mTitleLayout : Landroid/widget/LinearLayout;
    //   98: astore_1
    //   99: iload #4
    //   101: ifeq -> 110
    //   104: iload_3
    //   105: istore #4
    //   107: goto -> 114
    //   110: bipush #8
    //   112: istore #4
    //   114: aload_1
    //   115: iload #4
    //   117: invokevirtual setVisibility : (I)V
    //   120: aload_0
    //   121: aload_0
    //   122: getfield mUpGoerFive : Landroid/view/ViewGroup;
    //   125: invokevirtual isEnabled : ()Z
    //   128: invokespecial updateHomeAccessibility : (Z)V
    //   131: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #519	-> 0
    //   #520	-> 5
    //   #521	-> 14
    //   #522	-> 19
    //   #523	-> 46
    //   #525	-> 68
    //   #526	-> 94
    //   #528	-> 120
    //   #529	-> 131
  }
  
  public void setHomeButtonEnabled(boolean paramBoolean) {
    setHomeButtonEnabled(paramBoolean, true);
  }
  
  private void setHomeButtonEnabled(boolean paramBoolean1, boolean paramBoolean2) {
    if (paramBoolean2)
      this.mWasHomeEnabled = paramBoolean1; 
    if (this.mExpandedActionView != null)
      return; 
    this.mUpGoerFive.setEnabled(paramBoolean1);
    this.mUpGoerFive.setFocusable(paramBoolean1);
    updateHomeAccessibility(paramBoolean1);
  }
  
  private void updateHomeAccessibility(boolean paramBoolean) {
    if (!paramBoolean) {
      this.mUpGoerFive.setContentDescription(null);
      this.mUpGoerFive.setImportantForAccessibility(2);
    } else {
      this.mUpGoerFive.setImportantForAccessibility(0);
      this.mUpGoerFive.setContentDescription(buildHomeContentDescription());
    } 
  }
  
  private CharSequence buildHomeContentDescription() {
    CharSequence charSequence1;
    if (this.mHomeDescription != null) {
      charSequence1 = this.mHomeDescription;
    } else if ((this.mDisplayOptions & 0x4) != 0) {
      charSequence1 = this.mContext.getResources().getText(this.mDefaultUpDescription);
    } else {
      charSequence1 = this.mContext.getResources().getText(17039589);
    } 
    CharSequence charSequence2 = getTitle();
    CharSequence charSequence3 = getSubtitle();
    if (!TextUtils.isEmpty(charSequence2)) {
      if (!TextUtils.isEmpty(charSequence3)) {
        charSequence1 = getResources().getString(17039591, new Object[] { charSequence2, charSequence3, charSequence1 });
      } else {
        charSequence1 = getResources().getString(17039590, new Object[] { charSequence2, charSequence1 });
      } 
      return charSequence1;
    } 
    return charSequence1;
  }
  
  public void setDisplayOptions(int paramInt) {
    int i = this.mDisplayOptions, j = -1;
    if (i != -1)
      j = paramInt ^ i; 
    this.mDisplayOptions = paramInt;
    if ((j & 0x3F) != 0) {
      boolean bool;
      if ((j & 0x4) != 0) {
        if ((paramInt & 0x4) != 0) {
          bool = true;
        } else {
          bool = false;
        } 
        this.mHomeLayout.setShowUp(bool);
        if (bool)
          setHomeButtonEnabled(true); 
      } 
      if ((j & 0x1) != 0) {
        Drawable drawable;
        if (this.mLogo != null && (paramInt & 0x1) != 0) {
          i = 1;
        } else {
          i = 0;
        } 
        HomeView homeView = this.mHomeLayout;
        if (i != 0) {
          drawable = this.mLogo;
        } else {
          drawable = this.mIcon;
        } 
        homeView.setIcon(drawable);
      } 
      if ((j & 0x8) != 0)
        if ((paramInt & 0x8) != 0) {
          initTitle();
        } else {
          this.mUpGoerFive.removeView((View)this.mTitleLayout);
        }  
      if ((paramInt & 0x2) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      if ((this.mDisplayOptions & 0x4) != 0) {
        i = 1;
      } else {
        i = 0;
      } 
      if (!bool && i != 0) {
        i = 1;
      } else {
        i = 0;
      } 
      this.mHomeLayout.setShowIcon(bool);
      if ((bool || i != 0) && this.mExpandedActionView == null) {
        i = 0;
      } else {
        i = 8;
      } 
      this.mHomeLayout.setVisibility(i);
      if ((j & 0x10) != 0) {
        View view = this.mCustomNavView;
        if (view != null)
          if ((paramInt & 0x10) != 0) {
            addView(view);
          } else {
            removeView(view);
          }  
      } 
      if (this.mTitleLayout != null && (j & 0x20) != 0)
        if ((paramInt & 0x20) != 0) {
          this.mTitleView.setSingleLine(false);
          this.mTitleView.setMaxLines(2);
        } else {
          this.mTitleView.setMaxLines(1);
          this.mTitleView.setSingleLine(true);
        }  
      requestLayout();
    } else {
      invalidate();
    } 
    updateHomeAccessibility(this.mUpGoerFive.isEnabled());
  }
  
  public void setIcon(Drawable paramDrawable) {
    this.mIcon = paramDrawable;
    if (paramDrawable != null && ((this.mDisplayOptions & 0x1) == 0 || this.mLogo == null))
      this.mHomeLayout.setIcon(paramDrawable); 
    if (this.mExpandedActionView != null)
      this.mExpandedHomeLayout.setIcon(this.mIcon.getConstantState().newDrawable(getResources())); 
  }
  
  public void setIcon(int paramInt) {
    Drawable drawable;
    if (paramInt != 0) {
      drawable = this.mContext.getDrawable(paramInt);
    } else {
      drawable = null;
    } 
    setIcon(drawable);
  }
  
  public boolean hasIcon() {
    boolean bool;
    if (this.mIcon != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setLogo(Drawable paramDrawable) {
    this.mLogo = paramDrawable;
    if (paramDrawable != null && (this.mDisplayOptions & 0x1) != 0)
      this.mHomeLayout.setIcon(paramDrawable); 
  }
  
  public void setLogo(int paramInt) {
    Drawable drawable;
    if (paramInt != 0) {
      drawable = this.mContext.getDrawable(paramInt);
    } else {
      drawable = null;
    } 
    setLogo(drawable);
  }
  
  public boolean hasLogo() {
    boolean bool;
    if (this.mLogo != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setNavigationMode(int paramInt) {
    int i = this.mNavigationMode;
    if (paramInt != i) {
      if (i != 1) {
        if (i == 2) {
          ScrollingTabContainerView scrollingTabContainerView = this.mTabScrollView;
          if (scrollingTabContainerView != null && this.mIncludeTabs)
            removeView((View)scrollingTabContainerView); 
        } 
      } else {
        LinearLayout linearLayout = this.mListNavLayout;
        if (linearLayout != null)
          removeView((View)linearLayout); 
      } 
      if (paramInt != 1) {
        if (paramInt == 2) {
          ScrollingTabContainerView scrollingTabContainerView = this.mTabScrollView;
          if (scrollingTabContainerView != null && this.mIncludeTabs)
            addView((View)scrollingTabContainerView); 
        } 
      } else {
        if (this.mSpinner == null) {
          Spinner spinner = new Spinner(this.mContext, null, 16843479);
          spinner.setId(16908711);
          this.mListNavLayout = new LinearLayout(this.mContext, null, 16843508);
          LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -1);
          layoutParams.gravity = 17;
          this.mListNavLayout.addView((View)this.mSpinner, (ViewGroup.LayoutParams)layoutParams);
        } 
        SpinnerAdapter spinnerAdapter2 = this.mSpinner.getAdapter(), spinnerAdapter1 = this.mSpinnerAdapter;
        if (spinnerAdapter2 != spinnerAdapter1)
          this.mSpinner.setAdapter(spinnerAdapter1); 
        this.mSpinner.setOnItemSelectedListener(this.mNavItemSelectedListener);
        addView((View)this.mListNavLayout);
      } 
      this.mNavigationMode = paramInt;
      requestLayout();
    } 
  }
  
  public void setDropdownParams(SpinnerAdapter paramSpinnerAdapter, AdapterView.OnItemSelectedListener paramOnItemSelectedListener) {
    this.mSpinnerAdapter = paramSpinnerAdapter;
    this.mNavItemSelectedListener = paramOnItemSelectedListener;
    Spinner spinner = this.mSpinner;
    if (spinner != null) {
      spinner.setAdapter(paramSpinnerAdapter);
      this.mSpinner.setOnItemSelectedListener(paramOnItemSelectedListener);
    } 
  }
  
  public int getDropdownItemCount() {
    boolean bool;
    SpinnerAdapter spinnerAdapter = this.mSpinnerAdapter;
    if (spinnerAdapter != null) {
      bool = spinnerAdapter.getCount();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setDropdownSelectedPosition(int paramInt) {
    this.mSpinner.setSelection(paramInt);
  }
  
  public int getDropdownSelectedPosition() {
    return this.mSpinner.getSelectedItemPosition();
  }
  
  public View getCustomView() {
    return this.mCustomNavView;
  }
  
  public int getNavigationMode() {
    return this.mNavigationMode;
  }
  
  public int getDisplayOptions() {
    return this.mDisplayOptions;
  }
  
  public ViewGroup getViewGroup() {
    return this;
  }
  
  protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
    return (ViewGroup.LayoutParams)new ActionBar.LayoutParams(8388627);
  }
  
  protected void onFinishInflate() {
    super.onFinishInflate();
    this.mUpGoerFive.addView((View)this.mHomeLayout, 0);
    addView((View)this.mUpGoerFive);
    View view = this.mCustomNavView;
    if (view != null && (this.mDisplayOptions & 0x10) != 0) {
      ViewParent viewParent = view.getParent();
      if (viewParent != this) {
        if (viewParent instanceof ViewGroup)
          ((ViewGroup)viewParent).removeView(this.mCustomNavView); 
        addView(this.mCustomNavView);
      } 
    } 
  }
  
  private void initTitle() {
    if (this.mTitleLayout == null) {
      LayoutInflater layoutInflater = LayoutInflater.from(getContext());
      LinearLayout linearLayout = (LinearLayout)layoutInflater.inflate(17367070, this, false);
      this.mTitleView = (TextView)linearLayout.findViewById(16908713);
      this.mSubtitleView = (TextView)this.mTitleLayout.findViewById(16908712);
      int i = this.mTitleStyleRes;
      if (i != 0)
        this.mTitleView.setTextAppearance(i); 
      CharSequence charSequence = this.mTitle;
      if (charSequence != null)
        this.mTitleView.setText(charSequence); 
      i = this.mSubtitleStyleRes;
      if (i != 0)
        this.mSubtitleView.setTextAppearance(i); 
      charSequence = this.mSubtitle;
      if (charSequence != null) {
        this.mSubtitleView.setText(charSequence);
        this.mSubtitleView.setVisibility(0);
      } 
    } 
    this.mUpGoerFive.addView((View)this.mTitleLayout);
    if (this.mExpandedActionView == null) {
      CharSequence charSequence = this.mTitle;
      if (TextUtils.isEmpty(charSequence) && TextUtils.isEmpty(this.mSubtitle)) {
        this.mTitleLayout.setVisibility(8);
        return;
      } 
      this.mTitleLayout.setVisibility(0);
      return;
    } 
    this.mTitleLayout.setVisibility(8);
  }
  
  public void setContextView(ActionBarContextView paramActionBarContextView) {
    this.mContextView = paramActionBarContextView;
  }
  
  public void setCollapsible(boolean paramBoolean) {
    this.mIsCollapsible = paramBoolean;
  }
  
  public boolean isTitleTruncated() {
    TextView textView = this.mTitleView;
    if (textView == null)
      return false; 
    Layout layout = textView.getLayout();
    if (layout == null)
      return false; 
    int i = layout.getLineCount();
    for (byte b = 0; b < i; b++) {
      if (layout.getEllipsisCount(b) > 0)
        return true; 
    } 
    return false;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual getChildCount : ()I
    //   4: istore_3
    //   5: aload_0
    //   6: getfield mIsCollapsible : Z
    //   9: ifeq -> 178
    //   12: iconst_0
    //   13: istore #4
    //   15: iconst_0
    //   16: istore #5
    //   18: iload #5
    //   20: iload_3
    //   21: if_icmpge -> 102
    //   24: aload_0
    //   25: iload #5
    //   27: invokevirtual getChildAt : (I)Landroid/view/View;
    //   30: astore #6
    //   32: iload #4
    //   34: istore #7
    //   36: aload #6
    //   38: invokevirtual getVisibility : ()I
    //   41: bipush #8
    //   43: if_icmpeq -> 92
    //   46: aload #6
    //   48: aload_0
    //   49: getfield mMenuView : Landroid/widget/ActionMenuView;
    //   52: if_acmpne -> 73
    //   55: aload_0
    //   56: getfield mMenuView : Landroid/widget/ActionMenuView;
    //   59: astore #8
    //   61: iload #4
    //   63: istore #7
    //   65: aload #8
    //   67: invokevirtual getChildCount : ()I
    //   70: ifeq -> 92
    //   73: iload #4
    //   75: istore #7
    //   77: aload #6
    //   79: aload_0
    //   80: getfield mUpGoerFive : Landroid/view/ViewGroup;
    //   83: if_acmpeq -> 92
    //   86: iload #4
    //   88: iconst_1
    //   89: iadd
    //   90: istore #7
    //   92: iinc #5, 1
    //   95: iload #7
    //   97: istore #4
    //   99: goto -> 18
    //   102: aload_0
    //   103: getfield mUpGoerFive : Landroid/view/ViewGroup;
    //   106: invokevirtual getChildCount : ()I
    //   109: istore #9
    //   111: iconst_0
    //   112: istore #5
    //   114: iload #4
    //   116: istore #7
    //   118: iload #5
    //   120: iload #9
    //   122: if_icmpge -> 166
    //   125: aload_0
    //   126: getfield mUpGoerFive : Landroid/view/ViewGroup;
    //   129: iload #5
    //   131: invokevirtual getChildAt : (I)Landroid/view/View;
    //   134: astore #6
    //   136: iload #7
    //   138: istore #4
    //   140: aload #6
    //   142: invokevirtual getVisibility : ()I
    //   145: bipush #8
    //   147: if_icmpeq -> 156
    //   150: iload #7
    //   152: iconst_1
    //   153: iadd
    //   154: istore #4
    //   156: iinc #5, 1
    //   159: iload #4
    //   161: istore #7
    //   163: goto -> 118
    //   166: iload #7
    //   168: ifne -> 178
    //   171: aload_0
    //   172: iconst_0
    //   173: iconst_0
    //   174: invokevirtual setMeasuredDimension : (II)V
    //   177: return
    //   178: iload_1
    //   179: invokestatic getMode : (I)I
    //   182: istore #4
    //   184: iload #4
    //   186: ldc_w 1073741824
    //   189: if_icmpne -> 1483
    //   192: iload_2
    //   193: invokestatic getMode : (I)I
    //   196: istore #4
    //   198: iload #4
    //   200: ldc_w -2147483648
    //   203: if_icmpne -> 1439
    //   206: iload_1
    //   207: invokestatic getSize : (I)I
    //   210: istore #10
    //   212: aload_0
    //   213: getfield mContentHeight : I
    //   216: iflt -> 228
    //   219: aload_0
    //   220: getfield mContentHeight : I
    //   223: istore #7
    //   225: goto -> 234
    //   228: iload_2
    //   229: invokestatic getSize : (I)I
    //   232: istore #7
    //   234: aload_0
    //   235: invokevirtual getPaddingTop : ()I
    //   238: aload_0
    //   239: invokevirtual getPaddingBottom : ()I
    //   242: iadd
    //   243: istore #11
    //   245: aload_0
    //   246: invokevirtual getPaddingLeft : ()I
    //   249: istore_1
    //   250: aload_0
    //   251: invokevirtual getPaddingRight : ()I
    //   254: istore_2
    //   255: iload #7
    //   257: iload #11
    //   259: isub
    //   260: istore #12
    //   262: iload #12
    //   264: ldc_w -2147483648
    //   267: invokestatic makeMeasureSpec : (II)I
    //   270: istore #13
    //   272: iload #12
    //   274: ldc_w 1073741824
    //   277: invokestatic makeMeasureSpec : (II)I
    //   280: istore #14
    //   282: iload #10
    //   284: iload_1
    //   285: isub
    //   286: iload_2
    //   287: isub
    //   288: istore #4
    //   290: iload #4
    //   292: iconst_2
    //   293: idiv
    //   294: istore_1
    //   295: iload_1
    //   296: istore #15
    //   298: aload_0
    //   299: getfield mTitleLayout : Landroid/widget/LinearLayout;
    //   302: astore #6
    //   304: aload #6
    //   306: ifnull -> 335
    //   309: aload #6
    //   311: invokevirtual getVisibility : ()I
    //   314: bipush #8
    //   316: if_icmpeq -> 335
    //   319: aload_0
    //   320: getfield mDisplayOptions : I
    //   323: bipush #8
    //   325: iand
    //   326: ifeq -> 335
    //   329: iconst_1
    //   330: istore #16
    //   332: goto -> 338
    //   335: iconst_0
    //   336: istore #16
    //   338: aload_0
    //   339: getfield mExpandedActionView : Landroid/view/View;
    //   342: ifnull -> 354
    //   345: aload_0
    //   346: getfield mExpandedHomeLayout : Lcom/android/internal/widget/ActionBarView$HomeView;
    //   349: astore #6
    //   351: goto -> 360
    //   354: aload_0
    //   355: getfield mHomeLayout : Lcom/android/internal/widget/ActionBarView$HomeView;
    //   358: astore #6
    //   360: aload #6
    //   362: invokevirtual getLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
    //   365: astore #8
    //   367: aload #8
    //   369: getfield width : I
    //   372: ifge -> 387
    //   375: iload #4
    //   377: ldc_w -2147483648
    //   380: invokestatic makeMeasureSpec : (II)I
    //   383: istore_2
    //   384: goto -> 399
    //   387: aload #8
    //   389: getfield width : I
    //   392: ldc_w 1073741824
    //   395: invokestatic makeMeasureSpec : (II)I
    //   398: istore_2
    //   399: aload #6
    //   401: iload_2
    //   402: iload #14
    //   404: invokevirtual measure : (II)V
    //   407: aload #6
    //   409: invokevirtual getVisibility : ()I
    //   412: bipush #8
    //   414: if_icmpeq -> 429
    //   417: aload #6
    //   419: invokevirtual getParent : ()Landroid/view/ViewParent;
    //   422: aload_0
    //   423: getfield mUpGoerFive : Landroid/view/ViewGroup;
    //   426: if_acmpeq -> 434
    //   429: iload #16
    //   431: ifeq -> 472
    //   434: aload #6
    //   436: invokevirtual getMeasuredWidth : ()I
    //   439: istore #9
    //   441: aload #6
    //   443: invokevirtual getStartOffset : ()I
    //   446: iload #9
    //   448: iadd
    //   449: istore_1
    //   450: iconst_0
    //   451: iload #4
    //   453: iload_1
    //   454: isub
    //   455: invokestatic max : (II)I
    //   458: istore #4
    //   460: iconst_0
    //   461: iload #4
    //   463: iload_1
    //   464: isub
    //   465: invokestatic max : (II)I
    //   468: istore_1
    //   469: goto -> 475
    //   472: iconst_0
    //   473: istore #9
    //   475: iload #4
    //   477: istore #5
    //   479: iload #15
    //   481: istore_2
    //   482: aload_0
    //   483: getfield mMenuView : Landroid/widget/ActionMenuView;
    //   486: ifnull -> 537
    //   489: iload #4
    //   491: istore #5
    //   493: iload #15
    //   495: istore_2
    //   496: aload_0
    //   497: getfield mMenuView : Landroid/widget/ActionMenuView;
    //   500: invokevirtual getParent : ()Landroid/view/ViewParent;
    //   503: aload_0
    //   504: if_acmpne -> 537
    //   507: aload_0
    //   508: aload_0
    //   509: getfield mMenuView : Landroid/widget/ActionMenuView;
    //   512: iload #4
    //   514: iload #14
    //   516: iconst_0
    //   517: invokevirtual measureChildView : (Landroid/view/View;III)I
    //   520: istore #5
    //   522: iconst_0
    //   523: iload #15
    //   525: aload_0
    //   526: getfield mMenuView : Landroid/widget/ActionMenuView;
    //   529: invokevirtual getMeasuredWidth : ()I
    //   532: isub
    //   533: invokestatic max : (II)I
    //   536: istore_2
    //   537: aload_0
    //   538: getfield mIndeterminateProgressView : Landroid/widget/ProgressBar;
    //   541: astore #6
    //   543: aload #6
    //   545: ifnull -> 599
    //   548: aload #6
    //   550: invokevirtual getVisibility : ()I
    //   553: bipush #8
    //   555: if_icmpeq -> 599
    //   558: aload_0
    //   559: aload_0
    //   560: getfield mIndeterminateProgressView : Landroid/widget/ProgressBar;
    //   563: iload #5
    //   565: iload #13
    //   567: iconst_0
    //   568: invokevirtual measureChildView : (Landroid/view/View;III)I
    //   571: istore #4
    //   573: aload_0
    //   574: getfield mIndeterminateProgressView : Landroid/widget/ProgressBar;
    //   577: astore #6
    //   579: aload #6
    //   581: invokevirtual getMeasuredWidth : ()I
    //   584: istore #5
    //   586: iconst_0
    //   587: iload_2
    //   588: iload #5
    //   590: isub
    //   591: invokestatic max : (II)I
    //   594: istore #5
    //   596: goto -> 606
    //   599: iload #5
    //   601: istore #4
    //   603: iload_2
    //   604: istore #5
    //   606: aload_0
    //   607: getfield mExpandedActionView : Landroid/view/View;
    //   610: ifnonnull -> 853
    //   613: aload_0
    //   614: getfield mNavigationMode : I
    //   617: istore_2
    //   618: iload_2
    //   619: iconst_1
    //   620: if_icmpeq -> 742
    //   623: iload_2
    //   624: iconst_2
    //   625: if_icmpeq -> 631
    //   628: goto -> 853
    //   631: aload_0
    //   632: getfield mTabScrollView : Lcom/android/internal/widget/ScrollingTabContainerView;
    //   635: ifnull -> 739
    //   638: aload_0
    //   639: getfield mItemPadding : I
    //   642: istore #15
    //   644: iload #15
    //   646: istore_2
    //   647: iload #16
    //   649: ifeq -> 657
    //   652: iload #15
    //   654: iconst_2
    //   655: imul
    //   656: istore_2
    //   657: iconst_0
    //   658: iload #4
    //   660: iload_2
    //   661: isub
    //   662: invokestatic max : (II)I
    //   665: istore #4
    //   667: iconst_0
    //   668: iload_1
    //   669: iload_2
    //   670: isub
    //   671: invokestatic max : (II)I
    //   674: istore_2
    //   675: aload_0
    //   676: getfield mTabScrollView : Lcom/android/internal/widget/ScrollingTabContainerView;
    //   679: astore #6
    //   681: iload #4
    //   683: ldc_w -2147483648
    //   686: invokestatic makeMeasureSpec : (II)I
    //   689: istore #16
    //   691: iload #12
    //   693: ldc_w 1073741824
    //   696: invokestatic makeMeasureSpec : (II)I
    //   699: istore_1
    //   700: aload #6
    //   702: iload #16
    //   704: iload_1
    //   705: invokevirtual measure : (II)V
    //   708: aload_0
    //   709: getfield mTabScrollView : Lcom/android/internal/widget/ScrollingTabContainerView;
    //   712: invokevirtual getMeasuredWidth : ()I
    //   715: istore #16
    //   717: iconst_0
    //   718: iload #4
    //   720: iload #16
    //   722: isub
    //   723: invokestatic max : (II)I
    //   726: istore_1
    //   727: iconst_0
    //   728: iload_2
    //   729: iload #16
    //   731: isub
    //   732: invokestatic max : (II)I
    //   735: istore_2
    //   736: goto -> 858
    //   739: goto -> 853
    //   742: aload_0
    //   743: getfield mListNavLayout : Landroid/widget/LinearLayout;
    //   746: ifnull -> 850
    //   749: aload_0
    //   750: getfield mItemPadding : I
    //   753: istore #15
    //   755: iload #15
    //   757: istore_2
    //   758: iload #16
    //   760: ifeq -> 768
    //   763: iload #15
    //   765: iconst_2
    //   766: imul
    //   767: istore_2
    //   768: iconst_0
    //   769: iload #4
    //   771: iload_2
    //   772: isub
    //   773: invokestatic max : (II)I
    //   776: istore #4
    //   778: iconst_0
    //   779: iload_1
    //   780: iload_2
    //   781: isub
    //   782: invokestatic max : (II)I
    //   785: istore_2
    //   786: aload_0
    //   787: getfield mListNavLayout : Landroid/widget/LinearLayout;
    //   790: astore #6
    //   792: iload #4
    //   794: ldc_w -2147483648
    //   797: invokestatic makeMeasureSpec : (II)I
    //   800: istore_1
    //   801: iload #12
    //   803: ldc_w 1073741824
    //   806: invokestatic makeMeasureSpec : (II)I
    //   809: istore #16
    //   811: aload #6
    //   813: iload_1
    //   814: iload #16
    //   816: invokevirtual measure : (II)V
    //   819: aload_0
    //   820: getfield mListNavLayout : Landroid/widget/LinearLayout;
    //   823: invokevirtual getMeasuredWidth : ()I
    //   826: istore #16
    //   828: iconst_0
    //   829: iload #4
    //   831: iload #16
    //   833: isub
    //   834: invokestatic max : (II)I
    //   837: istore_1
    //   838: iconst_0
    //   839: iload_2
    //   840: iload #16
    //   842: isub
    //   843: invokestatic max : (II)I
    //   846: istore_2
    //   847: goto -> 858
    //   850: goto -> 853
    //   853: iload_1
    //   854: istore_2
    //   855: iload #4
    //   857: istore_1
    //   858: aconst_null
    //   859: astore #8
    //   861: aload_0
    //   862: getfield mExpandedActionView : Landroid/view/View;
    //   865: ifnull -> 877
    //   868: aload_0
    //   869: getfield mExpandedActionView : Landroid/view/View;
    //   872: astore #6
    //   874: goto -> 908
    //   877: aload #8
    //   879: astore #6
    //   881: aload_0
    //   882: getfield mDisplayOptions : I
    //   885: bipush #16
    //   887: iand
    //   888: ifeq -> 908
    //   891: aload #8
    //   893: astore #6
    //   895: aload_0
    //   896: getfield mCustomNavView : Landroid/view/View;
    //   899: ifnull -> 908
    //   902: aload_0
    //   903: getfield mCustomNavView : Landroid/view/View;
    //   906: astore #6
    //   908: aload #6
    //   910: ifnull -> 1223
    //   913: aload_0
    //   914: aload #6
    //   916: invokevirtual getLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
    //   919: invokevirtual generateLayoutParams : (Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    //   922: astore #17
    //   924: aload #17
    //   926: instanceof android/app/ActionBar$LayoutParams
    //   929: ifeq -> 942
    //   932: aload #17
    //   934: checkcast android/app/ActionBar$LayoutParams
    //   937: astore #8
    //   939: goto -> 945
    //   942: aconst_null
    //   943: astore #8
    //   945: iconst_0
    //   946: istore #16
    //   948: iconst_0
    //   949: istore #15
    //   951: aload #8
    //   953: ifnull -> 993
    //   956: aload #8
    //   958: getfield leftMargin : I
    //   961: istore #16
    //   963: aload #8
    //   965: getfield rightMargin : I
    //   968: istore #4
    //   970: aload #8
    //   972: getfield topMargin : I
    //   975: aload #8
    //   977: getfield bottomMargin : I
    //   980: iadd
    //   981: istore #15
    //   983: iload #16
    //   985: iload #4
    //   987: iadd
    //   988: istore #16
    //   990: goto -> 993
    //   993: aload_0
    //   994: getfield mContentHeight : I
    //   997: ifgt -> 1008
    //   1000: ldc_w -2147483648
    //   1003: istore #4
    //   1005: goto -> 1031
    //   1008: aload #17
    //   1010: getfield height : I
    //   1013: bipush #-2
    //   1015: if_icmpeq -> 1026
    //   1018: ldc_w 1073741824
    //   1021: istore #4
    //   1023: goto -> 1031
    //   1026: ldc_w -2147483648
    //   1029: istore #4
    //   1031: aload #17
    //   1033: getfield height : I
    //   1036: iflt -> 1054
    //   1039: aload #17
    //   1041: getfield height : I
    //   1044: iload #12
    //   1046: invokestatic min : (II)I
    //   1049: istore #12
    //   1051: goto -> 1054
    //   1054: iconst_0
    //   1055: iload #12
    //   1057: iload #15
    //   1059: isub
    //   1060: invokestatic max : (II)I
    //   1063: istore #14
    //   1065: aload #17
    //   1067: getfield width : I
    //   1070: bipush #-2
    //   1072: if_icmpeq -> 1083
    //   1075: ldc_w 1073741824
    //   1078: istore #15
    //   1080: goto -> 1088
    //   1083: ldc_w -2147483648
    //   1086: istore #15
    //   1088: aload #17
    //   1090: getfield width : I
    //   1093: iflt -> 1110
    //   1096: aload #17
    //   1098: getfield width : I
    //   1101: iload_1
    //   1102: invokestatic min : (II)I
    //   1105: istore #12
    //   1107: goto -> 1113
    //   1110: iload_1
    //   1111: istore #12
    //   1113: iconst_0
    //   1114: iload #12
    //   1116: iload #16
    //   1118: isub
    //   1119: invokestatic max : (II)I
    //   1122: istore #13
    //   1124: aload #8
    //   1126: ifnull -> 1139
    //   1129: aload #8
    //   1131: getfield gravity : I
    //   1134: istore #12
    //   1136: goto -> 1143
    //   1139: ldc 8388627
    //   1141: istore #12
    //   1143: iload #12
    //   1145: bipush #7
    //   1147: iand
    //   1148: iconst_1
    //   1149: if_icmpne -> 1178
    //   1152: iload #13
    //   1154: istore #12
    //   1156: aload #17
    //   1158: getfield width : I
    //   1161: iconst_m1
    //   1162: if_icmpne -> 1182
    //   1165: iload_2
    //   1166: iload #5
    //   1168: invokestatic min : (II)I
    //   1171: iconst_2
    //   1172: imul
    //   1173: istore #12
    //   1175: goto -> 1182
    //   1178: iload #13
    //   1180: istore #12
    //   1182: iload #12
    //   1184: iload #15
    //   1186: invokestatic makeMeasureSpec : (II)I
    //   1189: istore #5
    //   1191: iload #14
    //   1193: iload #4
    //   1195: invokestatic makeMeasureSpec : (II)I
    //   1198: istore #4
    //   1200: aload #6
    //   1202: iload #5
    //   1204: iload #4
    //   1206: invokevirtual measure : (II)V
    //   1209: iload_1
    //   1210: aload #6
    //   1212: invokevirtual getMeasuredWidth : ()I
    //   1215: iload #16
    //   1217: iadd
    //   1218: isub
    //   1219: istore_1
    //   1220: goto -> 1223
    //   1223: aload_0
    //   1224: getfield mUpGoerFive : Landroid/view/ViewGroup;
    //   1227: astore #6
    //   1229: aload_0
    //   1230: getfield mContentHeight : I
    //   1233: istore #4
    //   1235: iload #4
    //   1237: ldc_w 1073741824
    //   1240: invokestatic makeMeasureSpec : (II)I
    //   1243: istore #4
    //   1245: aload_0
    //   1246: aload #6
    //   1248: iload_1
    //   1249: iload #9
    //   1251: iadd
    //   1252: iload #4
    //   1254: iconst_0
    //   1255: invokevirtual measureChildView : (Landroid/view/View;III)I
    //   1258: pop
    //   1259: aload_0
    //   1260: getfield mTitleLayout : Landroid/widget/LinearLayout;
    //   1263: astore #6
    //   1265: aload #6
    //   1267: ifnull -> 1282
    //   1270: iconst_0
    //   1271: iload_2
    //   1272: aload #6
    //   1274: invokevirtual getMeasuredWidth : ()I
    //   1277: isub
    //   1278: invokestatic max : (II)I
    //   1281: pop
    //   1282: aload_0
    //   1283: getfield mContentHeight : I
    //   1286: ifgt -> 1349
    //   1289: iconst_0
    //   1290: istore #4
    //   1292: iconst_0
    //   1293: istore_1
    //   1294: iload_1
    //   1295: iload_3
    //   1296: if_icmpge -> 1338
    //   1299: aload_0
    //   1300: iload_1
    //   1301: invokevirtual getChildAt : (I)Landroid/view/View;
    //   1304: astore #6
    //   1306: aload #6
    //   1308: invokevirtual getMeasuredHeight : ()I
    //   1311: iload #11
    //   1313: iadd
    //   1314: istore #5
    //   1316: iload #4
    //   1318: istore_2
    //   1319: iload #5
    //   1321: iload #4
    //   1323: if_icmple -> 1329
    //   1326: iload #5
    //   1328: istore_2
    //   1329: iinc #1, 1
    //   1332: iload_2
    //   1333: istore #4
    //   1335: goto -> 1294
    //   1338: aload_0
    //   1339: iload #10
    //   1341: iload #4
    //   1343: invokevirtual setMeasuredDimension : (II)V
    //   1346: goto -> 1357
    //   1349: aload_0
    //   1350: iload #10
    //   1352: iload #7
    //   1354: invokevirtual setMeasuredDimension : (II)V
    //   1357: aload_0
    //   1358: getfield mContextView : Lcom/android/internal/widget/ActionBarContextView;
    //   1361: astore #6
    //   1363: aload #6
    //   1365: ifnull -> 1377
    //   1368: aload #6
    //   1370: aload_0
    //   1371: invokevirtual getMeasuredHeight : ()I
    //   1374: invokevirtual setContentHeight : (I)V
    //   1377: aload_0
    //   1378: getfield mProgressView : Landroid/widget/ProgressBar;
    //   1381: astore #6
    //   1383: aload #6
    //   1385: ifnull -> 1438
    //   1388: aload #6
    //   1390: invokevirtual getVisibility : ()I
    //   1393: bipush #8
    //   1395: if_icmpeq -> 1438
    //   1398: aload_0
    //   1399: getfield mProgressView : Landroid/widget/ProgressBar;
    //   1402: astore #6
    //   1404: iload #10
    //   1406: aload_0
    //   1407: getfield mProgressBarPadding : I
    //   1410: iconst_2
    //   1411: imul
    //   1412: isub
    //   1413: ldc_w 1073741824
    //   1416: invokestatic makeMeasureSpec : (II)I
    //   1419: istore_1
    //   1420: aload_0
    //   1421: invokevirtual getMeasuredHeight : ()I
    //   1424: ldc_w -2147483648
    //   1427: invokestatic makeMeasureSpec : (II)I
    //   1430: istore_2
    //   1431: aload #6
    //   1433: iload_1
    //   1434: iload_2
    //   1435: invokevirtual measure : (II)V
    //   1438: return
    //   1439: new java/lang/StringBuilder
    //   1442: dup
    //   1443: invokespecial <init> : ()V
    //   1446: astore #6
    //   1448: aload #6
    //   1450: aload_0
    //   1451: invokevirtual getClass : ()Ljava/lang/Class;
    //   1454: invokevirtual getSimpleName : ()Ljava/lang/String;
    //   1457: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1460: pop
    //   1461: aload #6
    //   1463: ldc_w ' can only be used with android:layout_height="wrap_content"'
    //   1466: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1469: pop
    //   1470: new java/lang/IllegalStateException
    //   1473: dup
    //   1474: aload #6
    //   1476: invokevirtual toString : ()Ljava/lang/String;
    //   1479: invokespecial <init> : (Ljava/lang/String;)V
    //   1482: athrow
    //   1483: new java/lang/StringBuilder
    //   1486: dup
    //   1487: invokespecial <init> : ()V
    //   1490: astore #6
    //   1492: aload #6
    //   1494: aload_0
    //   1495: invokevirtual getClass : ()Ljava/lang/Class;
    //   1498: invokevirtual getSimpleName : ()Ljava/lang/String;
    //   1501: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1504: pop
    //   1505: aload #6
    //   1507: ldc_w ' can only be used with android:layout_width="match_parent" (or fill_parent)'
    //   1510: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1513: pop
    //   1514: new java/lang/IllegalStateException
    //   1517: dup
    //   1518: aload #6
    //   1520: invokevirtual toString : ()Ljava/lang/String;
    //   1523: invokespecial <init> : (Ljava/lang/String;)V
    //   1526: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #875	-> 0
    //   #876	-> 5
    //   #877	-> 12
    //   #878	-> 15
    //   #879	-> 24
    //   #880	-> 32
    //   #881	-> 61
    //   #883	-> 86
    //   #878	-> 92
    //   #887	-> 102
    //   #888	-> 111
    //   #889	-> 125
    //   #890	-> 136
    //   #891	-> 150
    //   #888	-> 156
    //   #895	-> 166
    //   #897	-> 171
    //   #898	-> 177
    //   #902	-> 178
    //   #903	-> 184
    //   #908	-> 192
    //   #909	-> 198
    //   #914	-> 206
    //   #916	-> 212
    //   #917	-> 219
    //   #919	-> 234
    //   #920	-> 245
    //   #921	-> 250
    //   #922	-> 255
    //   #923	-> 262
    //   #924	-> 272
    //   #926	-> 282
    //   #927	-> 290
    //   #928	-> 295
    //   #930	-> 298
    //   #933	-> 338
    //   #935	-> 360
    //   #937	-> 367
    //   #938	-> 375
    //   #940	-> 387
    //   #951	-> 399
    //   #953	-> 407
    //   #954	-> 407
    //   #956	-> 434
    //   #957	-> 441
    //   #958	-> 450
    //   #959	-> 460
    //   #954	-> 472
    //   #962	-> 475
    //   #963	-> 507
    //   #964	-> 522
    //   #967	-> 537
    //   #968	-> 548
    //   #969	-> 558
    //   #971	-> 573
    //   #972	-> 579
    //   #971	-> 586
    //   #975	-> 599
    //   #976	-> 613
    //   #991	-> 631
    //   #992	-> 638
    //   #993	-> 657
    //   #994	-> 667
    //   #995	-> 675
    //   #996	-> 681
    //   #997	-> 691
    //   #995	-> 700
    //   #998	-> 708
    //   #999	-> 717
    //   #1000	-> 727
    //   #991	-> 739
    //   #978	-> 742
    //   #979	-> 749
    //   #980	-> 768
    //   #981	-> 778
    //   #982	-> 786
    //   #983	-> 792
    //   #984	-> 801
    //   #982	-> 811
    //   #985	-> 819
    //   #986	-> 828
    //   #987	-> 838
    //   #988	-> 847
    //   #978	-> 850
    //   #975	-> 853
    //   #1006	-> 853
    //   #1007	-> 861
    //   #1008	-> 868
    //   #1009	-> 877
    //   #1011	-> 902
    //   #1014	-> 908
    //   #1015	-> 913
    //   #1016	-> 924
    //   #1017	-> 932
    //   #1019	-> 945
    //   #1020	-> 948
    //   #1021	-> 951
    //   #1022	-> 956
    //   #1023	-> 970
    //   #1021	-> 993
    //   #1029	-> 993
    //   #1030	-> 1000
    //   #1032	-> 1008
    //   #1033	-> 1018
    //   #1035	-> 1031
    //   #1036	-> 1031
    //   #1035	-> 1054
    //   #1038	-> 1065
    //   #1039	-> 1075
    //   #1040	-> 1088
    //   #1041	-> 1088
    //   #1040	-> 1113
    //   #1043	-> 1124
    //   #1048	-> 1143
    //   #1049	-> 1165
    //   #1048	-> 1178
    //   #1052	-> 1182
    //   #1053	-> 1182
    //   #1054	-> 1191
    //   #1052	-> 1200
    //   #1055	-> 1209
    //   #1014	-> 1223
    //   #1062	-> 1223
    //   #1063	-> 1235
    //   #1062	-> 1245
    //   #1064	-> 1259
    //   #1065	-> 1270
    //   #1068	-> 1282
    //   #1069	-> 1289
    //   #1070	-> 1292
    //   #1071	-> 1299
    //   #1072	-> 1306
    //   #1073	-> 1316
    //   #1074	-> 1326
    //   #1070	-> 1329
    //   #1077	-> 1338
    //   #1078	-> 1346
    //   #1079	-> 1349
    //   #1082	-> 1357
    //   #1083	-> 1368
    //   #1086	-> 1377
    //   #1087	-> 1398
    //   #1089	-> 1420
    //   #1087	-> 1431
    //   #1091	-> 1438
    //   #910	-> 1439
    //   #904	-> 1483
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    // Byte code:
    //   0: iload #5
    //   2: iload_3
    //   3: isub
    //   4: aload_0
    //   5: invokevirtual getPaddingTop : ()I
    //   8: isub
    //   9: aload_0
    //   10: invokevirtual getPaddingBottom : ()I
    //   13: isub
    //   14: istore #6
    //   16: iload #6
    //   18: ifgt -> 22
    //   21: return
    //   22: aload_0
    //   23: invokevirtual isLayoutRtl : ()Z
    //   26: istore_1
    //   27: iload_1
    //   28: ifeq -> 37
    //   31: iconst_1
    //   32: istore #5
    //   34: goto -> 40
    //   37: iconst_m1
    //   38: istore #5
    //   40: iload_1
    //   41: ifeq -> 52
    //   44: aload_0
    //   45: invokevirtual getPaddingLeft : ()I
    //   48: istore_3
    //   49: goto -> 62
    //   52: iload #4
    //   54: iload_2
    //   55: isub
    //   56: aload_0
    //   57: invokevirtual getPaddingRight : ()I
    //   60: isub
    //   61: istore_3
    //   62: iload_1
    //   63: ifeq -> 80
    //   66: iload #4
    //   68: iload_2
    //   69: isub
    //   70: aload_0
    //   71: invokevirtual getPaddingRight : ()I
    //   74: isub
    //   75: istore #7
    //   77: goto -> 86
    //   80: aload_0
    //   81: invokevirtual getPaddingLeft : ()I
    //   84: istore #7
    //   86: aload_0
    //   87: invokevirtual getPaddingTop : ()I
    //   90: istore #8
    //   92: aload_0
    //   93: getfield mExpandedActionView : Landroid/view/View;
    //   96: ifnull -> 108
    //   99: aload_0
    //   100: getfield mExpandedHomeLayout : Lcom/android/internal/widget/ActionBarView$HomeView;
    //   103: astore #9
    //   105: goto -> 114
    //   108: aload_0
    //   109: getfield mHomeLayout : Lcom/android/internal/widget/ActionBarView$HomeView;
    //   112: astore #9
    //   114: aload_0
    //   115: getfield mTitleLayout : Landroid/widget/LinearLayout;
    //   118: astore #10
    //   120: aload #10
    //   122: ifnull -> 151
    //   125: aload #10
    //   127: invokevirtual getVisibility : ()I
    //   130: bipush #8
    //   132: if_icmpeq -> 151
    //   135: aload_0
    //   136: getfield mDisplayOptions : I
    //   139: bipush #8
    //   141: iand
    //   142: ifeq -> 151
    //   145: iconst_1
    //   146: istore #4
    //   148: goto -> 154
    //   151: iconst_0
    //   152: istore #4
    //   154: aload #9
    //   156: invokevirtual getParent : ()Landroid/view/ViewParent;
    //   159: aload_0
    //   160: getfield mUpGoerFive : Landroid/view/ViewGroup;
    //   163: if_acmpne -> 199
    //   166: aload #9
    //   168: invokevirtual getVisibility : ()I
    //   171: bipush #8
    //   173: if_icmpeq -> 185
    //   176: aload #9
    //   178: invokevirtual getStartOffset : ()I
    //   181: istore_2
    //   182: goto -> 201
    //   185: iload #4
    //   187: ifeq -> 199
    //   190: aload #9
    //   192: invokevirtual getUpWidth : ()I
    //   195: istore_2
    //   196: goto -> 201
    //   199: iconst_0
    //   200: istore_2
    //   201: aload_0
    //   202: getfield mUpGoerFive : Landroid/view/ViewGroup;
    //   205: astore #9
    //   207: iload #7
    //   209: iload_2
    //   210: iload_1
    //   211: invokestatic next : (IIZ)I
    //   214: istore #11
    //   216: aload_0
    //   217: aload #9
    //   219: iload #11
    //   221: iload #8
    //   223: iload #6
    //   225: iload_1
    //   226: invokevirtual positionChild : (Landroid/view/View;IIIZ)I
    //   229: istore #11
    //   231: iload #7
    //   233: iload #11
    //   235: iadd
    //   236: iload_2
    //   237: iload_1
    //   238: invokestatic next : (IIZ)I
    //   241: istore_2
    //   242: aload_0
    //   243: getfield mExpandedActionView : Landroid/view/View;
    //   246: ifnonnull -> 385
    //   249: aload_0
    //   250: getfield mNavigationMode : I
    //   253: istore #7
    //   255: iload #7
    //   257: iconst_1
    //   258: if_icmpeq -> 328
    //   261: iload #7
    //   263: iconst_2
    //   264: if_icmpeq -> 270
    //   267: goto -> 385
    //   270: aload_0
    //   271: getfield mTabScrollView : Lcom/android/internal/widget/ScrollingTabContainerView;
    //   274: ifnull -> 385
    //   277: iload_2
    //   278: istore #7
    //   280: iload #4
    //   282: ifeq -> 296
    //   285: iload_2
    //   286: aload_0
    //   287: getfield mItemPadding : I
    //   290: iload_1
    //   291: invokestatic next : (IIZ)I
    //   294: istore #7
    //   296: aload_0
    //   297: aload_0
    //   298: getfield mTabScrollView : Lcom/android/internal/widget/ScrollingTabContainerView;
    //   301: iload #7
    //   303: iload #8
    //   305: iload #6
    //   307: iload_1
    //   308: invokevirtual positionChild : (Landroid/view/View;IIIZ)I
    //   311: istore_2
    //   312: iload #7
    //   314: iload_2
    //   315: iadd
    //   316: aload_0
    //   317: getfield mItemPadding : I
    //   320: iload_1
    //   321: invokestatic next : (IIZ)I
    //   324: istore_2
    //   325: goto -> 385
    //   328: aload_0
    //   329: getfield mListNavLayout : Landroid/widget/LinearLayout;
    //   332: ifnull -> 385
    //   335: iload #4
    //   337: ifeq -> 353
    //   340: iload_2
    //   341: aload_0
    //   342: getfield mItemPadding : I
    //   345: iload_1
    //   346: invokestatic next : (IIZ)I
    //   349: istore_2
    //   350: goto -> 353
    //   353: aload_0
    //   354: aload_0
    //   355: getfield mListNavLayout : Landroid/widget/LinearLayout;
    //   358: iload_2
    //   359: iload #8
    //   361: iload #6
    //   363: iload_1
    //   364: invokevirtual positionChild : (Landroid/view/View;IIIZ)I
    //   367: istore #4
    //   369: iload_2
    //   370: iload #4
    //   372: iadd
    //   373: aload_0
    //   374: getfield mItemPadding : I
    //   377: iload_1
    //   378: invokestatic next : (IIZ)I
    //   381: istore_2
    //   382: goto -> 385
    //   385: aload_0
    //   386: getfield mMenuView : Landroid/widget/ActionMenuView;
    //   389: ifnull -> 436
    //   392: aload_0
    //   393: getfield mMenuView : Landroid/widget/ActionMenuView;
    //   396: invokevirtual getParent : ()Landroid/view/ViewParent;
    //   399: aload_0
    //   400: if_acmpne -> 436
    //   403: aload_0
    //   404: aload_0
    //   405: getfield mMenuView : Landroid/widget/ActionMenuView;
    //   408: iload_3
    //   409: iload #8
    //   411: iload #6
    //   413: iload_1
    //   414: iconst_1
    //   415: ixor
    //   416: invokevirtual positionChild : (Landroid/view/View;IIIZ)I
    //   419: pop
    //   420: iload_3
    //   421: aload_0
    //   422: getfield mMenuView : Landroid/widget/ActionMenuView;
    //   425: invokevirtual getMeasuredWidth : ()I
    //   428: iload #5
    //   430: imul
    //   431: iadd
    //   432: istore_3
    //   433: goto -> 436
    //   436: iload_2
    //   437: istore #11
    //   439: aload_0
    //   440: getfield mIndeterminateProgressView : Landroid/widget/ProgressBar;
    //   443: astore #9
    //   445: iload_3
    //   446: istore_2
    //   447: aload #9
    //   449: ifnull -> 494
    //   452: iload_3
    //   453: istore_2
    //   454: aload #9
    //   456: invokevirtual getVisibility : ()I
    //   459: bipush #8
    //   461: if_icmpeq -> 494
    //   464: aload_0
    //   465: aload_0
    //   466: getfield mIndeterminateProgressView : Landroid/widget/ProgressBar;
    //   469: iload_3
    //   470: iload #8
    //   472: iload #6
    //   474: iload_1
    //   475: iconst_1
    //   476: ixor
    //   477: invokevirtual positionChild : (Landroid/view/View;IIIZ)I
    //   480: pop
    //   481: iload_3
    //   482: aload_0
    //   483: getfield mIndeterminateProgressView : Landroid/widget/ProgressBar;
    //   486: invokevirtual getMeasuredWidth : ()I
    //   489: iload #5
    //   491: imul
    //   492: iadd
    //   493: istore_2
    //   494: aconst_null
    //   495: astore #10
    //   497: aload_0
    //   498: getfield mExpandedActionView : Landroid/view/View;
    //   501: ifnull -> 513
    //   504: aload_0
    //   505: getfield mExpandedActionView : Landroid/view/View;
    //   508: astore #9
    //   510: goto -> 544
    //   513: aload #10
    //   515: astore #9
    //   517: aload_0
    //   518: getfield mDisplayOptions : I
    //   521: bipush #16
    //   523: iand
    //   524: ifeq -> 544
    //   527: aload #10
    //   529: astore #9
    //   531: aload_0
    //   532: getfield mCustomNavView : Landroid/view/View;
    //   535: ifnull -> 544
    //   538: aload_0
    //   539: getfield mCustomNavView : Landroid/view/View;
    //   542: astore #9
    //   544: aload #9
    //   546: ifnull -> 1005
    //   549: aload_0
    //   550: invokevirtual getLayoutDirection : ()I
    //   553: istore #12
    //   555: aload #9
    //   557: invokevirtual getLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
    //   560: astore #10
    //   562: aload #10
    //   564: instanceof android/app/ActionBar$LayoutParams
    //   567: ifeq -> 580
    //   570: aload #10
    //   572: checkcast android/app/ActionBar$LayoutParams
    //   575: astore #10
    //   577: goto -> 583
    //   580: aconst_null
    //   581: astore #10
    //   583: aload #10
    //   585: ifnull -> 598
    //   588: aload #10
    //   590: getfield gravity : I
    //   593: istore #7
    //   595: goto -> 602
    //   598: ldc 8388627
    //   600: istore #7
    //   602: aload #9
    //   604: invokevirtual getMeasuredWidth : ()I
    //   607: istore #13
    //   609: iconst_0
    //   610: istore #8
    //   612: iconst_0
    //   613: istore #6
    //   615: iload_2
    //   616: istore_3
    //   617: iload #11
    //   619: istore #4
    //   621: aload #10
    //   623: ifnull -> 664
    //   626: iload #11
    //   628: aload #10
    //   630: invokevirtual getMarginStart : ()I
    //   633: iload_1
    //   634: invokestatic next : (IIZ)I
    //   637: istore #4
    //   639: iload_2
    //   640: aload #10
    //   642: invokevirtual getMarginEnd : ()I
    //   645: iload #5
    //   647: imul
    //   648: iadd
    //   649: istore_3
    //   650: aload #10
    //   652: getfield topMargin : I
    //   655: istore #8
    //   657: aload #10
    //   659: getfield bottomMargin : I
    //   662: istore #6
    //   664: ldc_w 8388615
    //   667: iload #7
    //   669: iand
    //   670: istore_2
    //   671: iload_2
    //   672: iconst_1
    //   673: if_icmpne -> 754
    //   676: aload_0
    //   677: getfield mRight : I
    //   680: aload_0
    //   681: getfield mLeft : I
    //   684: isub
    //   685: iload #13
    //   687: isub
    //   688: iconst_2
    //   689: idiv
    //   690: istore #5
    //   692: iload_1
    //   693: ifeq -> 725
    //   696: iload #5
    //   698: iload #13
    //   700: iadd
    //   701: iload #4
    //   703: if_icmple -> 711
    //   706: iconst_5
    //   707: istore_2
    //   708: goto -> 722
    //   711: iload #5
    //   713: iload_3
    //   714: if_icmpge -> 722
    //   717: iconst_3
    //   718: istore_2
    //   719: goto -> 722
    //   722: goto -> 751
    //   725: iload #5
    //   727: iload #4
    //   729: if_icmpge -> 737
    //   732: iconst_3
    //   733: istore_2
    //   734: goto -> 751
    //   737: iload #5
    //   739: iload #13
    //   741: iadd
    //   742: iload_3
    //   743: if_icmple -> 751
    //   746: iconst_5
    //   747: istore_2
    //   748: goto -> 751
    //   751: goto -> 766
    //   754: iload #7
    //   756: ifne -> 766
    //   759: ldc_w 8388611
    //   762: istore_2
    //   763: goto -> 766
    //   766: iconst_0
    //   767: istore #5
    //   769: iload_2
    //   770: iload #12
    //   772: invokestatic getAbsoluteGravity : (II)I
    //   775: istore_2
    //   776: iload_2
    //   777: iconst_1
    //   778: if_icmpeq -> 833
    //   781: iload_2
    //   782: iconst_3
    //   783: if_icmpeq -> 818
    //   786: iload_2
    //   787: iconst_5
    //   788: if_icmpeq -> 797
    //   791: iload #5
    //   793: istore_2
    //   794: goto -> 848
    //   797: iload_1
    //   798: ifeq -> 810
    //   801: iload #4
    //   803: iload #13
    //   805: isub
    //   806: istore_2
    //   807: goto -> 815
    //   810: iload_3
    //   811: iload #13
    //   813: isub
    //   814: istore_2
    //   815: goto -> 848
    //   818: iload_1
    //   819: ifeq -> 827
    //   822: iload_3
    //   823: istore_2
    //   824: goto -> 830
    //   827: iload #4
    //   829: istore_2
    //   830: goto -> 848
    //   833: aload_0
    //   834: getfield mRight : I
    //   837: aload_0
    //   838: getfield mLeft : I
    //   841: isub
    //   842: iload #13
    //   844: isub
    //   845: iconst_2
    //   846: idiv
    //   847: istore_2
    //   848: iload #7
    //   850: bipush #112
    //   852: iand
    //   853: istore_3
    //   854: iload #7
    //   856: ifne -> 862
    //   859: bipush #16
    //   861: istore_3
    //   862: iconst_0
    //   863: istore #5
    //   865: iload_3
    //   866: bipush #16
    //   868: if_icmpeq -> 922
    //   871: iload_3
    //   872: bipush #48
    //   874: if_icmpeq -> 911
    //   877: iload_3
    //   878: bipush #80
    //   880: if_icmpeq -> 889
    //   883: iload #5
    //   885: istore_3
    //   886: goto -> 964
    //   889: aload_0
    //   890: invokevirtual getHeight : ()I
    //   893: aload_0
    //   894: invokevirtual getPaddingBottom : ()I
    //   897: isub
    //   898: aload #9
    //   900: invokevirtual getMeasuredHeight : ()I
    //   903: isub
    //   904: iload #6
    //   906: isub
    //   907: istore_3
    //   908: goto -> 964
    //   911: aload_0
    //   912: invokevirtual getPaddingTop : ()I
    //   915: iload #8
    //   917: iadd
    //   918: istore_3
    //   919: goto -> 964
    //   922: aload_0
    //   923: invokevirtual getPaddingTop : ()I
    //   926: istore #6
    //   928: aload_0
    //   929: getfield mBottom : I
    //   932: istore #7
    //   934: aload_0
    //   935: getfield mTop : I
    //   938: istore_3
    //   939: aload_0
    //   940: invokevirtual getPaddingBottom : ()I
    //   943: istore #5
    //   945: iload #7
    //   947: iload_3
    //   948: isub
    //   949: iload #5
    //   951: isub
    //   952: iload #6
    //   954: isub
    //   955: aload #9
    //   957: invokevirtual getMeasuredHeight : ()I
    //   960: isub
    //   961: iconst_2
    //   962: idiv
    //   963: istore_3
    //   964: aload #9
    //   966: invokevirtual getMeasuredWidth : ()I
    //   969: istore #5
    //   971: aload #9
    //   973: invokevirtual getMeasuredHeight : ()I
    //   976: istore #7
    //   978: aload #9
    //   980: iload_2
    //   981: iload_3
    //   982: iload_2
    //   983: iload #5
    //   985: iadd
    //   986: iload #7
    //   988: iload_3
    //   989: iadd
    //   990: invokevirtual layout : (IIII)V
    //   993: iload #4
    //   995: iload #5
    //   997: iload_1
    //   998: invokestatic next : (IIZ)I
    //   1001: pop
    //   1002: goto -> 1005
    //   1005: aload_0
    //   1006: getfield mProgressView : Landroid/widget/ProgressBar;
    //   1009: astore #9
    //   1011: aload #9
    //   1013: ifnull -> 1067
    //   1016: aload #9
    //   1018: invokevirtual bringToFront : ()V
    //   1021: aload_0
    //   1022: getfield mProgressView : Landroid/widget/ProgressBar;
    //   1025: invokevirtual getMeasuredHeight : ()I
    //   1028: iconst_2
    //   1029: idiv
    //   1030: istore_2
    //   1031: aload_0
    //   1032: getfield mProgressView : Landroid/widget/ProgressBar;
    //   1035: astore #9
    //   1037: aload_0
    //   1038: getfield mProgressBarPadding : I
    //   1041: istore #5
    //   1043: iload_2
    //   1044: ineg
    //   1045: istore #4
    //   1047: aload #9
    //   1049: invokevirtual getMeasuredWidth : ()I
    //   1052: istore_3
    //   1053: aload #9
    //   1055: iload #5
    //   1057: iload #4
    //   1059: iload_3
    //   1060: iload #5
    //   1062: iadd
    //   1063: iload_2
    //   1064: invokevirtual layout : (IIII)V
    //   1067: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1095	-> 0
    //   #1097	-> 16
    //   #1099	-> 21
    //   #1102	-> 22
    //   #1103	-> 27
    //   #1104	-> 40
    //   #1107	-> 62
    //   #1108	-> 86
    //   #1110	-> 92
    //   #1111	-> 114
    //   #1113	-> 154
    //   #1114	-> 154
    //   #1115	-> 166
    //   #1116	-> 176
    //   #1117	-> 185
    //   #1118	-> 190
    //   #1123	-> 199
    //   #1124	-> 207
    //   #1123	-> 216
    //   #1125	-> 231
    //   #1127	-> 242
    //   #1128	-> 249
    //   #1141	-> 270
    //   #1142	-> 277
    //   #1143	-> 296
    //   #1144	-> 312
    //   #1132	-> 328
    //   #1133	-> 335
    //   #1134	-> 340
    //   #1133	-> 353
    //   #1136	-> 353
    //   #1137	-> 369
    //   #1150	-> 385
    //   #1151	-> 403
    //   #1152	-> 420
    //   #1150	-> 436
    //   #1155	-> 436
    //   #1156	-> 452
    //   #1157	-> 464
    //   #1158	-> 481
    //   #1161	-> 494
    //   #1162	-> 497
    //   #1163	-> 504
    //   #1164	-> 513
    //   #1166	-> 538
    //   #1168	-> 544
    //   #1169	-> 549
    //   #1170	-> 555
    //   #1171	-> 562
    //   #1172	-> 570
    //   #1173	-> 583
    //   #1174	-> 602
    //   #1176	-> 609
    //   #1177	-> 612
    //   #1178	-> 615
    //   #1179	-> 626
    //   #1180	-> 639
    //   #1181	-> 650
    //   #1182	-> 657
    //   #1185	-> 664
    //   #1187	-> 671
    //   #1188	-> 676
    //   #1189	-> 692
    //   #1190	-> 696
    //   #1191	-> 696
    //   #1192	-> 696
    //   #1193	-> 706
    //   #1194	-> 711
    //   #1195	-> 717
    //   #1194	-> 722
    //   #1197	-> 722
    //   #1198	-> 725
    //   #1199	-> 725
    //   #1200	-> 725
    //   #1201	-> 732
    //   #1202	-> 737
    //   #1203	-> 746
    //   #1202	-> 751
    //   #1206	-> 751
    //   #1207	-> 759
    //   #1206	-> 766
    //   #1210	-> 766
    //   #1211	-> 769
    //   #1219	-> 797
    //   #1216	-> 818
    //   #1217	-> 830
    //   #1213	-> 833
    //   #1214	-> 848
    //   #1223	-> 848
    //   #1225	-> 854
    //   #1226	-> 859
    //   #1229	-> 862
    //   #1230	-> 865
    //   #1240	-> 889
    //   #1237	-> 911
    //   #1238	-> 919
    //   #1232	-> 922
    //   #1233	-> 928
    //   #1234	-> 945
    //   #1235	-> 964
    //   #1244	-> 964
    //   #1245	-> 971
    //   #1246	-> 971
    //   #1245	-> 978
    //   #1247	-> 993
    //   #1168	-> 1005
    //   #1250	-> 1005
    //   #1251	-> 1016
    //   #1252	-> 1021
    //   #1253	-> 1031
    //   #1254	-> 1047
    //   #1253	-> 1053
    //   #1256	-> 1067
  }
  
  public ViewGroup.LayoutParams generateLayoutParams(AttributeSet paramAttributeSet) {
    return (ViewGroup.LayoutParams)new ActionBar.LayoutParams(getContext(), paramAttributeSet);
  }
  
  public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    ViewGroup.LayoutParams layoutParams = paramLayoutParams;
    if (paramLayoutParams == null)
      layoutParams = generateDefaultLayoutParams(); 
    return layoutParams;
  }
  
  public Parcelable onSaveInstanceState() {
    Parcelable parcelable = super.onSaveInstanceState();
    SavedState savedState = new SavedState();
    ExpandedActionViewMenuPresenter expandedActionViewMenuPresenter = this.mExpandedMenuPresenter;
    if (expandedActionViewMenuPresenter != null && expandedActionViewMenuPresenter.mCurrentExpandedItem != null)
      savedState.expandedMenuItemId = this.mExpandedMenuPresenter.mCurrentExpandedItem.getItemId(); 
    savedState.isOverflowOpen = isOverflowMenuShowing();
    return (Parcelable)savedState;
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable) {
    SavedState savedState = (SavedState)paramParcelable;
    super.onRestoreInstanceState(savedState.getSuperState());
    if (savedState.expandedMenuItemId != 0 && this.mExpandedMenuPresenter != null) {
      MenuBuilder menuBuilder = this.mOptionsMenu;
      if (menuBuilder != null) {
        MenuItem menuItem = menuBuilder.findItem(savedState.expandedMenuItemId);
        if (menuItem != null)
          menuItem.expandActionView(); 
      } 
    } 
    if (savedState.isOverflowOpen)
      postShowOverflowMenu(); 
  }
  
  public void setNavigationIcon(Drawable paramDrawable) {
    this.mHomeLayout.setUpIndicator(paramDrawable);
  }
  
  public void setDefaultNavigationIcon(Drawable paramDrawable) {
    this.mHomeLayout.setDefaultUpIndicator(paramDrawable);
  }
  
  public void setNavigationIcon(int paramInt) {
    this.mHomeLayout.setUpIndicator(paramInt);
  }
  
  public void setNavigationContentDescription(CharSequence paramCharSequence) {
    this.mHomeDescription = paramCharSequence;
    updateHomeAccessibility(this.mUpGoerFive.isEnabled());
  }
  
  public void setNavigationContentDescription(int paramInt) {
    CharSequence charSequence;
    this.mHomeDescriptionRes = paramInt;
    if (paramInt != 0) {
      charSequence = getResources().getText(paramInt);
    } else {
      charSequence = null;
    } 
    this.mHomeDescription = charSequence;
    updateHomeAccessibility(this.mUpGoerFive.isEnabled());
  }
  
  public void setDefaultNavigationContentDescription(int paramInt) {
    if (this.mDefaultUpDescription == paramInt)
      return; 
    this.mDefaultUpDescription = paramInt;
    updateHomeAccessibility(this.mUpGoerFive.isEnabled());
  }
  
  public void setMenuCallbacks(MenuPresenter.Callback paramCallback, MenuBuilder.Callback paramCallback1) {
    if (this.mActionMenuPresenter != null)
      this.mActionMenuPresenter.setCallback(paramCallback); 
    MenuBuilder menuBuilder = this.mOptionsMenu;
    if (menuBuilder != null)
      menuBuilder.setCallback(paramCallback1); 
  }
  
  public Menu getMenu() {
    return this.mOptionsMenu;
  }
  
  class SavedState extends View.BaseSavedState {
    SavedState() {
      super((Parcelable)this$0);
    }
    
    private SavedState(ActionBarView this$0) {
      super((Parcel)this$0);
      boolean bool;
      this.expandedMenuItemId = this$0.readInt();
      if (this$0.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      this.isOverflowOpen = bool;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeInt(this.expandedMenuItemId);
      param1Parcel.writeInt(this.isOverflowOpen);
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = (Parcelable.Creator<SavedState>)new Object();
    
    int expandedMenuItemId;
    
    boolean isOverflowOpen;
  }
  
  private static class HomeView extends FrameLayout {
    private static final long DEFAULT_TRANSITION_DURATION = 150L;
    
    private Drawable mDefaultUpIndicator;
    
    private ImageView mIconView;
    
    private int mStartOffset;
    
    private Drawable mUpIndicator;
    
    private int mUpIndicatorRes;
    
    private ImageView mUpView;
    
    private int mUpWidth;
    
    public HomeView(Context param1Context) {
      this(param1Context, (AttributeSet)null);
    }
    
    public HomeView(Context param1Context, AttributeSet param1AttributeSet) {
      super(param1Context, param1AttributeSet);
      LayoutTransition layoutTransition = getLayoutTransition();
      if (layoutTransition != null)
        layoutTransition.setDuration(150L); 
    }
    
    public void setShowUp(boolean param1Boolean) {
      byte b;
      ImageView imageView = this.mUpView;
      if (param1Boolean) {
        b = 0;
      } else {
        b = 8;
      } 
      imageView.setVisibility(b);
    }
    
    public void setShowIcon(boolean param1Boolean) {
      byte b;
      ImageView imageView = this.mIconView;
      if (param1Boolean) {
        b = 0;
      } else {
        b = 8;
      } 
      imageView.setVisibility(b);
    }
    
    public void setIcon(Drawable param1Drawable) {
      this.mIconView.setImageDrawable(param1Drawable);
    }
    
    public void setUpIndicator(Drawable param1Drawable) {
      this.mUpIndicator = param1Drawable;
      this.mUpIndicatorRes = 0;
      updateUpIndicator();
    }
    
    public void setDefaultUpIndicator(Drawable param1Drawable) {
      this.mDefaultUpIndicator = param1Drawable;
      updateUpIndicator();
    }
    
    public void setUpIndicator(int param1Int) {
      this.mUpIndicatorRes = param1Int;
      this.mUpIndicator = null;
      updateUpIndicator();
    }
    
    private void updateUpIndicator() {
      Drawable drawable = this.mUpIndicator;
      if (drawable != null) {
        this.mUpView.setImageDrawable(drawable);
      } else if (this.mUpIndicatorRes != 0) {
        this.mUpView.setImageDrawable(getContext().getDrawable(this.mUpIndicatorRes));
      } else {
        this.mUpView.setImageDrawable(this.mDefaultUpIndicator);
      } 
    }
    
    protected void onConfigurationChanged(Configuration param1Configuration) {
      super.onConfigurationChanged(param1Configuration);
      if (this.mUpIndicatorRes != 0)
        updateUpIndicator(); 
    }
    
    public boolean dispatchPopulateAccessibilityEventInternal(AccessibilityEvent param1AccessibilityEvent) {
      onPopulateAccessibilityEvent(param1AccessibilityEvent);
      return true;
    }
    
    public void onPopulateAccessibilityEventInternal(AccessibilityEvent param1AccessibilityEvent) {
      super.onPopulateAccessibilityEventInternal(param1AccessibilityEvent);
      CharSequence charSequence = getContentDescription();
      if (!TextUtils.isEmpty(charSequence))
        param1AccessibilityEvent.getText().add(charSequence); 
    }
    
    public boolean dispatchHoverEvent(MotionEvent param1MotionEvent) {
      return onHoverEvent(param1MotionEvent);
    }
    
    protected void onFinishInflate() {
      this.mUpView = (ImageView)findViewById(16909605);
      this.mIconView = (ImageView)findViewById(16908332);
      this.mDefaultUpIndicator = this.mUpView.getDrawable();
    }
    
    public int getStartOffset() {
      boolean bool;
      if (this.mUpView.getVisibility() == 8) {
        bool = this.mStartOffset;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public int getUpWidth() {
      return this.mUpWidth;
    }
    
    protected void onMeasure(int param1Int1, int param1Int2) {
      // Byte code:
      //   0: aload_0
      //   1: aload_0
      //   2: getfield mUpView : Landroid/widget/ImageView;
      //   5: iload_1
      //   6: iconst_0
      //   7: iload_2
      //   8: iconst_0
      //   9: invokevirtual measureChildWithMargins : (Landroid/view/View;IIII)V
      //   12: aload_0
      //   13: getfield mUpView : Landroid/widget/ImageView;
      //   16: invokevirtual getLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
      //   19: checkcast android/widget/FrameLayout$LayoutParams
      //   22: astore_3
      //   23: aload_3
      //   24: getfield leftMargin : I
      //   27: aload_3
      //   28: getfield rightMargin : I
      //   31: iadd
      //   32: istore #4
      //   34: aload_0
      //   35: getfield mUpView : Landroid/widget/ImageView;
      //   38: invokevirtual getMeasuredWidth : ()I
      //   41: istore #5
      //   43: aload_0
      //   44: iload #5
      //   46: putfield mUpWidth : I
      //   49: aload_0
      //   50: iload #5
      //   52: iload #4
      //   54: iadd
      //   55: putfield mStartOffset : I
      //   58: aload_0
      //   59: getfield mUpView : Landroid/widget/ImageView;
      //   62: invokevirtual getVisibility : ()I
      //   65: bipush #8
      //   67: if_icmpne -> 76
      //   70: iconst_0
      //   71: istore #5
      //   73: goto -> 82
      //   76: aload_0
      //   77: getfield mStartOffset : I
      //   80: istore #5
      //   82: aload_3
      //   83: getfield topMargin : I
      //   86: aload_0
      //   87: getfield mUpView : Landroid/widget/ImageView;
      //   90: invokevirtual getMeasuredHeight : ()I
      //   93: iadd
      //   94: aload_3
      //   95: getfield bottomMargin : I
      //   98: iadd
      //   99: istore #6
      //   101: aload_0
      //   102: getfield mIconView : Landroid/widget/ImageView;
      //   105: invokevirtual getVisibility : ()I
      //   108: bipush #8
      //   110: if_icmpeq -> 202
      //   113: aload_0
      //   114: aload_0
      //   115: getfield mIconView : Landroid/widget/ImageView;
      //   118: iload_1
      //   119: iload #5
      //   121: iload_2
      //   122: iconst_0
      //   123: invokevirtual measureChildWithMargins : (Landroid/view/View;IIII)V
      //   126: aload_0
      //   127: getfield mIconView : Landroid/widget/ImageView;
      //   130: invokevirtual getLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
      //   133: checkcast android/widget/FrameLayout$LayoutParams
      //   136: astore_3
      //   137: iload #5
      //   139: aload_3
      //   140: getfield leftMargin : I
      //   143: aload_0
      //   144: getfield mIconView : Landroid/widget/ImageView;
      //   147: invokevirtual getMeasuredWidth : ()I
      //   150: iadd
      //   151: aload_3
      //   152: getfield rightMargin : I
      //   155: iadd
      //   156: iadd
      //   157: istore #7
      //   159: aload_3
      //   160: getfield topMargin : I
      //   163: istore #4
      //   165: aload_0
      //   166: getfield mIconView : Landroid/widget/ImageView;
      //   169: astore #8
      //   171: aload #8
      //   173: invokevirtual getMeasuredHeight : ()I
      //   176: istore #9
      //   178: aload_3
      //   179: getfield bottomMargin : I
      //   182: istore #5
      //   184: iload #6
      //   186: iload #4
      //   188: iload #9
      //   190: iadd
      //   191: iload #5
      //   193: iadd
      //   194: invokestatic max : (II)I
      //   197: istore #9
      //   199: goto -> 229
      //   202: iload #5
      //   204: istore #7
      //   206: iload #6
      //   208: istore #9
      //   210: iload #4
      //   212: ifge -> 229
      //   215: iload #5
      //   217: iload #4
      //   219: isub
      //   220: istore #7
      //   222: iload #6
      //   224: istore #5
      //   226: goto -> 233
      //   229: iload #9
      //   231: istore #5
      //   233: iload_1
      //   234: invokestatic getMode : (I)I
      //   237: istore #4
      //   239: iload_2
      //   240: invokestatic getMode : (I)I
      //   243: istore #6
      //   245: iload_1
      //   246: invokestatic getSize : (I)I
      //   249: istore #9
      //   251: iload_2
      //   252: invokestatic getSize : (I)I
      //   255: istore_1
      //   256: iload #4
      //   258: ldc -2147483648
      //   260: if_icmpeq -> 280
      //   263: iload #4
      //   265: ldc 1073741824
      //   267: if_icmpeq -> 273
      //   270: goto -> 289
      //   273: iload #9
      //   275: istore #7
      //   277: goto -> 289
      //   280: iload #7
      //   282: iload #9
      //   284: invokestatic min : (II)I
      //   287: istore #7
      //   289: iload #6
      //   291: ldc -2147483648
      //   293: if_icmpeq -> 312
      //   296: iload #6
      //   298: ldc 1073741824
      //   300: if_icmpeq -> 306
      //   303: goto -> 320
      //   306: iload_1
      //   307: istore #5
      //   309: goto -> 320
      //   312: iload #5
      //   314: iload_1
      //   315: invokestatic min : (II)I
      //   318: istore #5
      //   320: aload_0
      //   321: iload #7
      //   323: iload #5
      //   325: invokevirtual setMeasuredDimension : (II)V
      //   328: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1496	-> 0
      //   #1497	-> 12
      //   #1498	-> 23
      //   #1499	-> 34
      //   #1500	-> 49
      //   #1501	-> 58
      //   #1502	-> 82
      //   #1504	-> 101
      //   #1505	-> 113
      //   #1506	-> 126
      //   #1507	-> 137
      //   #1508	-> 159
      //   #1509	-> 171
      //   #1508	-> 184
      //   #1510	-> 202
      //   #1512	-> 215
      //   #1510	-> 229
      //   #1515	-> 233
      //   #1516	-> 239
      //   #1517	-> 245
      //   #1518	-> 251
      //   #1520	-> 256
      //   #1525	-> 273
      //   #1526	-> 277
      //   #1522	-> 280
      //   #1523	-> 289
      //   #1531	-> 289
      //   #1536	-> 306
      //   #1537	-> 309
      //   #1533	-> 312
      //   #1534	-> 320
      //   #1542	-> 320
      //   #1543	-> 328
    }
    
    protected void onLayout(boolean param1Boolean, int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      int i = (param1Int4 - param1Int2) / 2;
      param1Boolean = isLayoutRtl();
      int j = getWidth();
      param1Int4 = 0;
      if (this.mUpView.getVisibility() != 8) {
        FrameLayout.LayoutParams layoutParams1 = (FrameLayout.LayoutParams)this.mUpView.getLayoutParams();
        int n = this.mUpView.getMeasuredHeight();
        param1Int4 = this.mUpView.getMeasuredWidth();
        int i1 = layoutParams1.leftMargin + param1Int4 + layoutParams1.rightMargin;
        int i2 = i - n / 2;
        if (param1Boolean) {
          int i3 = j - param1Int4;
          param1Int4 = j;
          param1Int2 = param1Int3 - i1;
          param1Int3 = i3;
        } else {
          param1Int1 += i1;
          boolean bool = false;
          param1Int2 = param1Int3;
          param1Int3 = bool;
        } 
        this.mUpView.layout(param1Int3, i2, param1Int4, i2 + n);
        param1Int3 = i1;
      } else {
        param1Int2 = param1Int3;
        param1Int3 = param1Int4;
      } 
      FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams)this.mIconView.getLayoutParams();
      param1Int4 = this.mIconView.getMeasuredHeight();
      int m = this.mIconView.getMeasuredWidth();
      param1Int1 = (param1Int2 - param1Int1) / 2;
      int k = Math.max(layoutParams.topMargin, i - param1Int4 / 2);
      param1Int2 = layoutParams.getMarginStart();
      param1Int1 = Math.max(param1Int2, param1Int1 - m / 2);
      if (param1Boolean) {
        param1Int2 = j - param1Int3 - param1Int1;
        param1Int1 = param1Int2 - m;
      } else {
        param1Int1 = param1Int3 + param1Int1;
        param1Int2 = param1Int1 + m;
      } 
      this.mIconView.layout(param1Int1, k, param1Int2, k + param1Int4);
    }
  }
  
  class ExpandedActionViewMenuPresenter implements MenuPresenter {
    MenuItemImpl mCurrentExpandedItem;
    
    MenuBuilder mMenu;
    
    final ActionBarView this$0;
    
    private ExpandedActionViewMenuPresenter() {}
    
    public void initForMenu(Context param1Context, MenuBuilder param1MenuBuilder) {
      MenuBuilder menuBuilder = this.mMenu;
      if (menuBuilder != null) {
        MenuItemImpl menuItemImpl = this.mCurrentExpandedItem;
        if (menuItemImpl != null)
          menuBuilder.collapseItemActionView(menuItemImpl); 
      } 
      this.mMenu = param1MenuBuilder;
    }
    
    public MenuView getMenuView(ViewGroup param1ViewGroup) {
      return null;
    }
    
    public void updateMenuView(boolean param1Boolean) {
      if (this.mCurrentExpandedItem != null) {
        boolean bool1 = false;
        MenuBuilder menuBuilder = this.mMenu;
        boolean bool2 = bool1;
        if (menuBuilder != null) {
          int i = menuBuilder.size();
          byte b = 0;
          while (true) {
            bool2 = bool1;
            if (b < i) {
              MenuItem menuItem = this.mMenu.getItem(b);
              if (menuItem == this.mCurrentExpandedItem) {
                bool2 = true;
                break;
              } 
              b++;
              continue;
            } 
            break;
          } 
        } 
        if (!bool2)
          collapseItemActionView(this.mMenu, this.mCurrentExpandedItem); 
      } 
    }
    
    public void setCallback(MenuPresenter.Callback param1Callback) {}
    
    public boolean onSubMenuSelected(SubMenuBuilder param1SubMenuBuilder) {
      return false;
    }
    
    public void onCloseMenu(MenuBuilder param1MenuBuilder, boolean param1Boolean) {}
    
    public boolean flagActionItems() {
      return false;
    }
    
    public boolean expandItemActionView(MenuBuilder param1MenuBuilder, MenuItemImpl param1MenuItemImpl) {
      ActionBarView.this.mExpandedActionView = param1MenuItemImpl.getActionView();
      ActionBarView.this.mExpandedHomeLayout.setIcon(ActionBarView.this.mIcon.getConstantState().newDrawable(ActionBarView.this.getResources()));
      this.mCurrentExpandedItem = param1MenuItemImpl;
      ViewParent viewParent = ActionBarView.this.mExpandedActionView.getParent();
      ActionBarView actionBarView = ActionBarView.this;
      if (viewParent != actionBarView)
        actionBarView.addView(actionBarView.mExpandedActionView); 
      if (ActionBarView.this.mExpandedHomeLayout.getParent() != ActionBarView.this.mUpGoerFive)
        ActionBarView.this.mUpGoerFive.addView((View)ActionBarView.this.mExpandedHomeLayout); 
      ActionBarView.this.mHomeLayout.setVisibility(8);
      if (ActionBarView.this.mTitleLayout != null)
        ActionBarView.this.mTitleLayout.setVisibility(8); 
      if (ActionBarView.this.mTabScrollView != null)
        ActionBarView.this.mTabScrollView.setVisibility(8); 
      if (ActionBarView.this.mSpinner != null)
        ActionBarView.this.mSpinner.setVisibility(8); 
      if (ActionBarView.this.mCustomNavView != null)
        ActionBarView.this.mCustomNavView.setVisibility(8); 
      ActionBarView.this.setHomeButtonEnabled(false, false);
      ActionBarView.this.requestLayout();
      param1MenuItemImpl.setActionViewExpanded(true);
      if (ActionBarView.this.mExpandedActionView instanceof CollapsibleActionView)
        ((CollapsibleActionView)ActionBarView.this.mExpandedActionView).onActionViewExpanded(); 
      return true;
    }
    
    public boolean collapseItemActionView(MenuBuilder param1MenuBuilder, MenuItemImpl param1MenuItemImpl) {
      if (ActionBarView.this.mExpandedActionView instanceof CollapsibleActionView)
        ((CollapsibleActionView)ActionBarView.this.mExpandedActionView).onActionViewCollapsed(); 
      ActionBarView actionBarView = ActionBarView.this;
      actionBarView.removeView(actionBarView.mExpandedActionView);
      ActionBarView.this.mUpGoerFive.removeView((View)ActionBarView.this.mExpandedHomeLayout);
      ActionBarView.this.mExpandedActionView = null;
      if ((ActionBarView.this.mDisplayOptions & 0x2) != 0)
        ActionBarView.this.mHomeLayout.setVisibility(0); 
      if ((ActionBarView.this.mDisplayOptions & 0x8) != 0)
        if (ActionBarView.this.mTitleLayout == null) {
          ActionBarView.this.initTitle();
        } else {
          ActionBarView.this.mTitleLayout.setVisibility(0);
        }  
      if (ActionBarView.this.mTabScrollView != null)
        ActionBarView.this.mTabScrollView.setVisibility(0); 
      if (ActionBarView.this.mSpinner != null)
        ActionBarView.this.mSpinner.setVisibility(0); 
      if (ActionBarView.this.mCustomNavView != null)
        ActionBarView.this.mCustomNavView.setVisibility(0); 
      ActionBarView.this.mExpandedHomeLayout.setIcon((Drawable)null);
      this.mCurrentExpandedItem = null;
      actionBarView = ActionBarView.this;
      actionBarView.setHomeButtonEnabled(actionBarView.mWasHomeEnabled);
      ActionBarView.this.requestLayout();
      param1MenuItemImpl.setActionViewExpanded(false);
      return true;
    }
    
    public int getId() {
      return 0;
    }
    
    public Parcelable onSaveInstanceState() {
      return null;
    }
    
    public void onRestoreInstanceState(Parcelable param1Parcelable) {}
  }
}
