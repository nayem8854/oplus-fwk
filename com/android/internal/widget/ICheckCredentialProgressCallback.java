package com.android.internal.widget;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ICheckCredentialProgressCallback extends IInterface {
  void onCredentialVerified() throws RemoteException;
  
  class Default implements ICheckCredentialProgressCallback {
    public void onCredentialVerified() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICheckCredentialProgressCallback {
    private static final String DESCRIPTOR = "com.android.internal.widget.ICheckCredentialProgressCallback";
    
    static final int TRANSACTION_onCredentialVerified = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.widget.ICheckCredentialProgressCallback");
    }
    
    public static ICheckCredentialProgressCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.widget.ICheckCredentialProgressCallback");
      if (iInterface != null && iInterface instanceof ICheckCredentialProgressCallback)
        return (ICheckCredentialProgressCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onCredentialVerified";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.widget.ICheckCredentialProgressCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.widget.ICheckCredentialProgressCallback");
      onCredentialVerified();
      return true;
    }
    
    private static class Proxy implements ICheckCredentialProgressCallback {
      public static ICheckCredentialProgressCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.widget.ICheckCredentialProgressCallback";
      }
      
      public void onCredentialVerified() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.widget.ICheckCredentialProgressCallback");
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && ICheckCredentialProgressCallback.Stub.getDefaultImpl() != null) {
            ICheckCredentialProgressCallback.Stub.getDefaultImpl().onCredentialVerified();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICheckCredentialProgressCallback param1ICheckCredentialProgressCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICheckCredentialProgressCallback != null) {
          Proxy.sDefaultImpl = param1ICheckCredentialProgressCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICheckCredentialProgressCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
