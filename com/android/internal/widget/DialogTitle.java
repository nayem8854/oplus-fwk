package com.android.internal.widget;

import android.R;
import android.content.Context;
import android.content.res.TypedArray;
import android.text.Layout;
import android.util.AttributeSet;
import android.widget.TextView;

public class DialogTitle extends TextView {
  public DialogTitle(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  public DialogTitle(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  public DialogTitle(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  public DialogTitle(Context paramContext) {
    super(paramContext);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    super.onMeasure(paramInt1, paramInt2);
    Layout layout = getLayout();
    if (layout != null) {
      int i = layout.getLineCount();
      if (i > 0) {
        i = layout.getEllipsisCount(i - 1);
        if (i > 0) {
          setSingleLine(false);
          setMaxLines(2);
          TypedArray typedArray = this.mContext.obtainStyledAttributes(null, R.styleable.TextAppearance, 16842817, 16973892);
          i = typedArray.getDimensionPixelSize(0, 0);
          if (i != 0)
            setTextSize(0, i); 
          typedArray.recycle();
          super.onMeasure(paramInt1, paramInt2);
        } 
      } 
    } 
  }
}
