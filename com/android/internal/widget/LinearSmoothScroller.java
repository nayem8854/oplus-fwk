package com.android.internal.widget;

import android.content.Context;
import android.graphics.PointF;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

public class LinearSmoothScroller extends RecyclerView.SmoothScroller {
  protected final LinearInterpolator mLinearInterpolator = new LinearInterpolator();
  
  protected final DecelerateInterpolator mDecelerateInterpolator = new DecelerateInterpolator();
  
  protected int mInterimTargetDx = 0;
  
  protected int mInterimTargetDy = 0;
  
  private static final boolean DEBUG = false;
  
  private static final float MILLISECONDS_PER_INCH = 25.0F;
  
  public static final int SNAP_TO_ANY = 0;
  
  public static final int SNAP_TO_END = 1;
  
  public static final int SNAP_TO_START = -1;
  
  private static final String TAG = "LinearSmoothScroller";
  
  private static final float TARGET_SEEK_EXTRA_SCROLL_RATIO = 1.2F;
  
  private static final int TARGET_SEEK_SCROLL_DISTANCE_PX = 10000;
  
  private final float MILLISECONDS_PER_PX;
  
  protected PointF mTargetVector;
  
  public LinearSmoothScroller(Context paramContext) {
    this.MILLISECONDS_PER_PX = calculateSpeedPerPixel(paramContext.getResources().getDisplayMetrics());
  }
  
  protected void onStart() {}
  
  protected void onTargetFound(View paramView, RecyclerView.State paramState, RecyclerView.SmoothScroller.Action paramAction) {
    int i = calculateDxToMakeVisible(paramView, getHorizontalSnapPreference());
    int j = calculateDyToMakeVisible(paramView, getVerticalSnapPreference());
    int k = (int)Math.sqrt((i * i + j * j));
    k = calculateTimeForDeceleration(k);
    if (k > 0)
      paramAction.update(-i, -j, k, (Interpolator)this.mDecelerateInterpolator); 
  }
  
  protected void onSeekTargetStep(int paramInt1, int paramInt2, RecyclerView.State paramState, RecyclerView.SmoothScroller.Action paramAction) {
    if (getChildCount() == 0) {
      stop();
      return;
    } 
    this.mInterimTargetDx = clampApplyScroll(this.mInterimTargetDx, paramInt1);
    this.mInterimTargetDy = paramInt1 = clampApplyScroll(this.mInterimTargetDy, paramInt2);
    if (this.mInterimTargetDx == 0 && paramInt1 == 0)
      updateActionForInterimTarget(paramAction); 
  }
  
  protected void onStop() {
    this.mInterimTargetDy = 0;
    this.mInterimTargetDx = 0;
    this.mTargetVector = null;
  }
  
  protected float calculateSpeedPerPixel(DisplayMetrics paramDisplayMetrics) {
    return 25.0F / paramDisplayMetrics.densityDpi;
  }
  
  protected int calculateTimeForDeceleration(int paramInt) {
    return (int)Math.ceil(calculateTimeForScrolling(paramInt) / 0.3356D);
  }
  
  protected int calculateTimeForScrolling(int paramInt) {
    return (int)Math.ceil((Math.abs(paramInt) * this.MILLISECONDS_PER_PX));
  }
  
  protected int getHorizontalSnapPreference() {
    byte b;
    PointF pointF = this.mTargetVector;
    if (pointF == null || pointF.x == 0.0F)
      return 0; 
    if (this.mTargetVector.x > 0.0F) {
      b = 1;
    } else {
      b = -1;
    } 
    return b;
  }
  
  protected int getVerticalSnapPreference() {
    byte b;
    PointF pointF = this.mTargetVector;
    if (pointF == null || pointF.y == 0.0F)
      return 0; 
    if (this.mTargetVector.y > 0.0F) {
      b = 1;
    } else {
      b = -1;
    } 
    return b;
  }
  
  protected void updateActionForInterimTarget(RecyclerView.SmoothScroller.Action paramAction) {
    PointF pointF = computeScrollVectorForPosition(getTargetPosition());
    if (pointF == null || (pointF.x == 0.0F && pointF.y == 0.0F)) {
      int j = getTargetPosition();
      paramAction.jumpTo(j);
      stop();
      return;
    } 
    normalize(pointF);
    this.mTargetVector = pointF;
    this.mInterimTargetDx = (int)(pointF.x * 10000.0F);
    this.mInterimTargetDy = (int)(pointF.y * 10000.0F);
    int i = calculateTimeForScrolling(10000);
    paramAction.update((int)(this.mInterimTargetDx * 1.2F), (int)(this.mInterimTargetDy * 1.2F), (int)(i * 1.2F), (Interpolator)this.mLinearInterpolator);
  }
  
  private int clampApplyScroll(int paramInt1, int paramInt2) {
    paramInt2 = paramInt1 - paramInt2;
    if (paramInt1 * paramInt2 <= 0)
      return 0; 
    return paramInt2;
  }
  
  public int calculateDtToFit(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    if (paramInt5 != -1) {
      if (paramInt5 != 0) {
        if (paramInt5 == 1)
          return paramInt4 - paramInt2; 
        throw new IllegalArgumentException("snap preference should be one of the constants defined in SmoothScroller, starting with SNAP_");
      } 
      paramInt1 = paramInt3 - paramInt1;
      if (paramInt1 > 0)
        return paramInt1; 
      paramInt1 = paramInt4 - paramInt2;
      if (paramInt1 < 0)
        return paramInt1; 
      return 0;
    } 
    return paramInt3 - paramInt1;
  }
  
  public int calculateDyToMakeVisible(View paramView, int paramInt) {
    RecyclerView.LayoutManager layoutManager = getLayoutManager();
    if (layoutManager == null || !layoutManager.canScrollVertically())
      return 0; 
    RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams)paramView.getLayoutParams();
    int i = layoutManager.getDecoratedTop(paramView), j = layoutParams.topMargin;
    int k = layoutManager.getDecoratedBottom(paramView), m = layoutParams.bottomMargin;
    int n = layoutManager.getPaddingTop();
    int i1 = layoutManager.getHeight(), i2 = layoutManager.getPaddingBottom();
    return calculateDtToFit(i - j, k + m, n, i1 - i2, paramInt);
  }
  
  public int calculateDxToMakeVisible(View paramView, int paramInt) {
    RecyclerView.LayoutManager layoutManager = getLayoutManager();
    if (layoutManager == null || !layoutManager.canScrollHorizontally())
      return 0; 
    RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams)paramView.getLayoutParams();
    int i = layoutManager.getDecoratedLeft(paramView), j = layoutParams.leftMargin;
    int k = layoutManager.getDecoratedRight(paramView), m = layoutParams.rightMargin;
    int n = layoutManager.getPaddingLeft();
    int i1 = layoutManager.getWidth(), i2 = layoutManager.getPaddingRight();
    return calculateDtToFit(i - j, k + m, n, i1 - i2, paramInt);
  }
  
  public PointF computeScrollVectorForPosition(int paramInt) {
    RecyclerView.LayoutManager layoutManager = getLayoutManager();
    if (layoutManager instanceof RecyclerView.SmoothScroller.ScrollVectorProvider) {
      RecyclerView.SmoothScroller.ScrollVectorProvider scrollVectorProvider = (RecyclerView.SmoothScroller.ScrollVectorProvider)layoutManager;
      return 
        scrollVectorProvider.computeScrollVectorForPosition(paramInt);
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("You should override computeScrollVectorForPosition when the LayoutManager does not implement ");
    stringBuilder.append(RecyclerView.SmoothScroller.ScrollVectorProvider.class.getCanonicalName());
    String str = stringBuilder.toString();
    Log.w("LinearSmoothScroller", str);
    return null;
  }
}
