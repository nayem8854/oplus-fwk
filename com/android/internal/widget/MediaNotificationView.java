package com.android.internal.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.NotificationHeaderView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RemoteViews.RemoteView;
import java.util.ArrayList;

@RemoteView
public class MediaNotificationView extends FrameLayout {
  private View mActions;
  
  private NotificationHeaderView mHeader;
  
  private int mImagePushIn;
  
  private ArrayList<VisibilityChangeListener> mListeners;
  
  private View mMainColumn;
  
  private View mMediaContent;
  
  private final int mNotificationContentImageMarginEnd;
  
  private final int mNotificationContentMarginEnd;
  
  private ImageView mRightIcon;
  
  public MediaNotificationView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public MediaNotificationView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public MediaNotificationView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    int i;
    if (this.mRightIcon.getVisibility() != 8) {
      i = 1;
    } else {
      i = 0;
    } 
    if (!i)
      resetHeaderIndention(); 
    super.onMeasure(paramInt1, paramInt2);
    int j = View.MeasureSpec.getMode(paramInt1);
    int k = 0, m = 0;
    this.mImagePushIn = 0;
    if (i && j != 0) {
      k = View.MeasureSpec.getSize(paramInt1);
      i = this.mActions.getMeasuredWidth();
      ImageView imageView = this.mRightIcon;
      ViewGroup.MarginLayoutParams marginLayoutParams2 = (ViewGroup.MarginLayoutParams)imageView.getLayoutParams();
      int n = marginLayoutParams2.getMarginEnd();
      j = k - i - n;
      k = this.mMediaContent.getMeasuredHeight();
      if (j > k) {
        i = k;
      } else {
        i = j;
        if (j < k) {
          i = Math.max(0, j);
          this.mImagePushIn = k - i;
        } 
      } 
      if (marginLayoutParams2.width != k || marginLayoutParams2.height != k) {
        marginLayoutParams2.width = k;
        marginLayoutParams2.height = k;
        this.mRightIcon.setLayoutParams((ViewGroup.LayoutParams)marginLayoutParams2);
        m = 1;
      } 
      View view = this.mMainColumn;
      ViewGroup.MarginLayoutParams marginLayoutParams1 = (ViewGroup.MarginLayoutParams)view.getLayoutParams();
      k = i + n + this.mNotificationContentMarginEnd;
      if (k != marginLayoutParams1.getMarginEnd()) {
        marginLayoutParams1.setMarginEnd(k);
        this.mMainColumn.setLayoutParams((ViewGroup.LayoutParams)marginLayoutParams1);
        m = 1;
      } 
      i += n;
      if (i != this.mHeader.getHeaderTextMarginEnd()) {
        this.mHeader.setHeaderTextMarginEnd(i);
        m = 1;
      } 
      marginLayoutParams1 = (ViewGroup.MarginLayoutParams)this.mHeader.getLayoutParams();
      if (marginLayoutParams1.getMarginEnd() != n) {
        marginLayoutParams1.setMarginEnd(n);
        this.mHeader.setLayoutParams((ViewGroup.LayoutParams)marginLayoutParams1);
        m = 1;
      } 
      if (this.mHeader.getPaddingEnd() != this.mNotificationContentImageMarginEnd) {
        NotificationHeaderView notificationHeaderView1 = this.mHeader;
        m = notificationHeaderView1.getPaddingStart();
        NotificationHeaderView notificationHeaderView2 = this.mHeader;
        i = notificationHeaderView2.getPaddingTop();
        j = this.mNotificationContentImageMarginEnd;
        notificationHeaderView2 = this.mHeader;
        k = notificationHeaderView2.getPaddingBottom();
        notificationHeaderView1.setPaddingRelative(m, i, j, k);
        m = 1;
      } 
    } else {
      m = k;
    } 
    if (m != 0)
      super.onMeasure(paramInt1, paramInt2); 
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    if (this.mImagePushIn > 0) {
      if (getLayoutDirection() == 1)
        this.mImagePushIn *= -1; 
      ImageView imageView1 = this.mRightIcon;
      paramInt4 = imageView1.getLeft();
      paramInt2 = this.mImagePushIn;
      int i = this.mRightIcon.getTop();
      ImageView imageView2 = this.mRightIcon;
      paramInt3 = imageView2.getRight();
      paramInt1 = this.mImagePushIn;
      int j = this.mRightIcon.getBottom();
      imageView1.layout(paramInt4 + paramInt2, i, paramInt3 + paramInt1, j);
    } 
  }
  
  private void resetHeaderIndention() {
    if (this.mHeader.getPaddingEnd() != this.mNotificationContentMarginEnd) {
      NotificationHeaderView notificationHeaderView1 = this.mHeader;
      int i = notificationHeaderView1.getPaddingStart();
      NotificationHeaderView notificationHeaderView2 = this.mHeader;
      int j = notificationHeaderView2.getPaddingTop(), k = this.mNotificationContentMarginEnd;
      notificationHeaderView2 = this.mHeader;
      int m = notificationHeaderView2.getPaddingBottom();
      notificationHeaderView1.setPaddingRelative(i, j, k, m);
    } 
    NotificationHeaderView notificationHeaderView = this.mHeader;
    ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams)notificationHeaderView.getLayoutParams();
    marginLayoutParams.setMarginEnd(0);
    if (marginLayoutParams.getMarginEnd() != 0) {
      marginLayoutParams.setMarginEnd(0);
      this.mHeader.setLayoutParams((ViewGroup.LayoutParams)marginLayoutParams);
    } 
  }
  
  public MediaNotificationView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    this.mNotificationContentMarginEnd = paramContext.getResources().getDimensionPixelSize(17105345);
    this.mNotificationContentImageMarginEnd = paramContext.getResources().getDimensionPixelSize(17105343);
  }
  
  protected void onFinishInflate() {
    super.onFinishInflate();
    this.mRightIcon = (ImageView)findViewById(16909369);
    this.mActions = findViewById(16909154);
    this.mHeader = (NotificationHeaderView)findViewById(16909227);
    this.mMainColumn = findViewById(16909228);
    this.mMediaContent = findViewById(16909235);
  }
  
  public void onVisibilityAggregated(boolean paramBoolean) {
    super.onVisibilityAggregated(paramBoolean);
    if (this.mListeners != null)
      for (byte b = 0; b < this.mListeners.size(); b++)
        ((VisibilityChangeListener)this.mListeners.get(b)).onAggregatedVisibilityChanged(paramBoolean);  
  }
  
  public void addVisibilityListener(VisibilityChangeListener paramVisibilityChangeListener) {
    if (this.mListeners == null)
      this.mListeners = new ArrayList<>(); 
    if (!this.mListeners.contains(paramVisibilityChangeListener))
      this.mListeners.add(paramVisibilityChangeListener); 
  }
  
  public void removeVisibilityListener(VisibilityChangeListener paramVisibilityChangeListener) {
    ArrayList<VisibilityChangeListener> arrayList = this.mListeners;
    if (arrayList != null)
      arrayList.remove(paramVisibilityChangeListener); 
  }
  
  class VisibilityChangeListener {
    public abstract void onAggregatedVisibilityChanged(boolean param1Boolean);
  }
}
