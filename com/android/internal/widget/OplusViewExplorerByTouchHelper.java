package com.android.internal.widget;

import android.graphics.Rect;
import android.os.Bundle;
import android.util.IntArray;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

public class OplusViewExplorerByTouchHelper extends ExploreByTouchHelper {
  private final Rect mTempRect = new Rect();
  
  private View mHostView = null;
  
  private OplusViewTalkBalkInteraction mOplusViewTalkBalkInteraction = null;
  
  private static final String VIEW_LOG_TAG = "ColorViewTouchHelper";
  
  public OplusViewExplorerByTouchHelper(View paramView) {
    super(paramView);
    this.mHostView = paramView;
  }
  
  public void setFocusedVirtualView(int paramInt) {
    getAccessibilityNodeProvider(this.mHostView).performAction(paramInt, 64, null);
  }
  
  public void clearFocusedVirtualView() {
    int i = getFocusedVirtualView();
    if (i != Integer.MIN_VALUE)
      getAccessibilityNodeProvider(this.mHostView).performAction(i, 128, null); 
  }
  
  protected int getVirtualViewAt(float paramFloat1, float paramFloat2) {
    int i = this.mOplusViewTalkBalkInteraction.getVirtualViewAt(paramFloat1, paramFloat2);
    if (i >= 0)
      return i; 
    return Integer.MIN_VALUE;
  }
  
  protected void getVisibleVirtualViews(IntArray paramIntArray) {
    for (byte b = 0; b < this.mOplusViewTalkBalkInteraction.getItemCounts(); b++)
      paramIntArray.add(b); 
  }
  
  protected void onPopulateEventForVirtualView(int paramInt, AccessibilityEvent paramAccessibilityEvent) {
    paramAccessibilityEvent.setContentDescription(this.mOplusViewTalkBalkInteraction.getItemDescription(paramInt));
  }
  
  protected void onPopulateNodeForVirtualView(int paramInt, AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    getItemBounds(paramInt, this.mTempRect);
    paramAccessibilityNodeInfo.setContentDescription(this.mOplusViewTalkBalkInteraction.getItemDescription(paramInt));
    paramAccessibilityNodeInfo.setBoundsInParent(this.mTempRect);
    if (this.mOplusViewTalkBalkInteraction.getClassName() != null)
      paramAccessibilityNodeInfo.setClassName(this.mOplusViewTalkBalkInteraction.getClassName()); 
    paramAccessibilityNodeInfo.addAction(16);
    if (paramInt == this.mOplusViewTalkBalkInteraction.getCurrentPosition())
      paramAccessibilityNodeInfo.setSelected(true); 
    if (paramInt == this.mOplusViewTalkBalkInteraction.getDisablePosition())
      paramAccessibilityNodeInfo.setEnabled(false); 
  }
  
  protected boolean onPerformActionForVirtualView(int paramInt1, int paramInt2, Bundle paramBundle) {
    if (paramInt2 != 16)
      return false; 
    this.mOplusViewTalkBalkInteraction.performAction(paramInt1, 16, false);
    return true;
  }
  
  private void getItemBounds(int paramInt, Rect paramRect) {
    if (paramInt >= 0 && paramInt < this.mOplusViewTalkBalkInteraction.getItemCounts())
      this.mOplusViewTalkBalkInteraction.getItemBounds(paramInt, paramRect); 
  }
  
  public void setOplusViewTalkBalkInteraction(OplusViewTalkBalkInteraction paramOplusViewTalkBalkInteraction) {
    this.mOplusViewTalkBalkInteraction = paramOplusViewTalkBalkInteraction;
  }
  
  class OplusViewTalkBalkInteraction {
    public abstract CharSequence getClassName();
    
    public abstract int getCurrentPosition();
    
    public abstract int getDisablePosition();
    
    public abstract void getItemBounds(int param1Int, Rect param1Rect);
    
    public abstract int getItemCounts();
    
    public abstract CharSequence getItemDescription(int param1Int);
    
    public abstract int getVirtualViewAt(float param1Float1, float param1Float2);
    
    public abstract void performAction(int param1Int1, int param1Int2, boolean param1Boolean);
  }
}
