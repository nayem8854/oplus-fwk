package com.android.internal.widget;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ActionMenuPresenter;
import android.widget.ActionMenuView;
import com.android.internal.R;

public abstract class AbsActionBarView extends ViewGroup {
  private static final int FADE_DURATION = 200;
  
  private static final TimeInterpolator sAlphaInterpolator = (TimeInterpolator)new DecelerateInterpolator();
  
  protected ActionMenuPresenter mActionMenuPresenter;
  
  protected int mContentHeight;
  
  private boolean mEatingHover;
  
  private boolean mEatingTouch;
  
  protected ActionMenuView mMenuView;
  
  protected final Context mPopupContext;
  
  protected boolean mSplitActionBar;
  
  protected ViewGroup mSplitView;
  
  protected boolean mSplitWhenNarrow;
  
  protected final VisibilityAnimListener mVisAnimListener = new VisibilityAnimListener();
  
  protected Animator mVisibilityAnim;
  
  public AbsActionBarView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public AbsActionBarView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public AbsActionBarView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public AbsActionBarView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedValue typedValue = new TypedValue();
    if (paramContext.getTheme().resolveAttribute(16843917, typedValue, true) && typedValue.resourceId != 0) {
      this.mPopupContext = (Context)new ContextThemeWrapper(paramContext, typedValue.resourceId);
    } else {
      this.mPopupContext = paramContext;
    } 
  }
  
  protected void onConfigurationChanged(Configuration paramConfiguration) {
    super.onConfigurationChanged(paramConfiguration);
    TypedArray typedArray = getContext().obtainStyledAttributes(null, R.styleable.ActionBar, 16843470, 0);
    setContentHeight(typedArray.getLayoutDimension(4, 0));
    typedArray.recycle();
    if (this.mSplitWhenNarrow)
      setSplitToolbar(getContext().getResources().getBoolean(17891617)); 
    ActionMenuPresenter actionMenuPresenter = this.mActionMenuPresenter;
    if (actionMenuPresenter != null)
      actionMenuPresenter.onConfigurationChanged(paramConfiguration); 
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.getActionMasked();
    if (i == 0)
      this.mEatingTouch = false; 
    if (!this.mEatingTouch) {
      boolean bool = super.onTouchEvent(paramMotionEvent);
      if (i == 0 && !bool)
        this.mEatingTouch = true; 
    } 
    if (i == 1 || i == 3)
      this.mEatingTouch = false; 
    return true;
  }
  
  public boolean onHoverEvent(MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.getActionMasked();
    if (i == 9)
      this.mEatingHover = false; 
    if (!this.mEatingHover) {
      boolean bool = super.onHoverEvent(paramMotionEvent);
      if (i == 9 && !bool)
        this.mEatingHover = true; 
    } 
    if (i == 10 || i == 3)
      this.mEatingHover = false; 
    return true;
  }
  
  public void setSplitToolbar(boolean paramBoolean) {
    this.mSplitActionBar = paramBoolean;
  }
  
  public void setSplitWhenNarrow(boolean paramBoolean) {
    this.mSplitWhenNarrow = paramBoolean;
  }
  
  public void setContentHeight(int paramInt) {
    this.mContentHeight = paramInt;
    requestLayout();
  }
  
  public int getContentHeight() {
    return this.mContentHeight;
  }
  
  public void setSplitView(ViewGroup paramViewGroup) {
    this.mSplitView = paramViewGroup;
  }
  
  public int getAnimatedVisibility() {
    if (this.mVisibilityAnim != null)
      return this.mVisAnimListener.mFinalVisibility; 
    return getVisibility();
  }
  
  public Animator setupAnimatorToVisibility(int paramInt, long paramLong) {
    Animator animator = this.mVisibilityAnim;
    if (animator != null)
      animator.cancel(); 
    if (paramInt == 0) {
      if (getVisibility() != 0) {
        setAlpha(0.0F);
        if (this.mSplitView != null) {
          ActionMenuView actionMenuView = this.mMenuView;
          if (actionMenuView != null)
            actionMenuView.setAlpha(0.0F); 
        } 
      } 
      ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(this, View.ALPHA, new float[] { 1.0F });
      objectAnimator1.setDuration(paramLong);
      objectAnimator1.setInterpolator(sAlphaInterpolator);
      if (this.mSplitView != null && this.mMenuView != null) {
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator objectAnimator2 = ObjectAnimator.ofFloat(this.mMenuView, View.ALPHA, new float[] { 1.0F });
        objectAnimator2.setDuration(paramLong);
        animatorSet.addListener(this.mVisAnimListener.withFinalVisibility(paramInt));
        animatorSet.play((Animator)objectAnimator1).with((Animator)objectAnimator2);
        return (Animator)animatorSet;
      } 
      objectAnimator1.addListener(this.mVisAnimListener.withFinalVisibility(paramInt));
      return (Animator)objectAnimator1;
    } 
    ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(this, View.ALPHA, new float[] { 0.0F });
    objectAnimator.setDuration(paramLong);
    objectAnimator.setInterpolator(sAlphaInterpolator);
    if (this.mSplitView != null && this.mMenuView != null) {
      AnimatorSet animatorSet = new AnimatorSet();
      ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(this.mMenuView, View.ALPHA, new float[] { 0.0F });
      objectAnimator1.setDuration(paramLong);
      animatorSet.addListener(this.mVisAnimListener.withFinalVisibility(paramInt));
      animatorSet.play((Animator)objectAnimator).with((Animator)objectAnimator1);
      return (Animator)animatorSet;
    } 
    objectAnimator.addListener(this.mVisAnimListener.withFinalVisibility(paramInt));
    return (Animator)objectAnimator;
  }
  
  public void animateToVisibility(int paramInt) {
    Animator animator = setupAnimatorToVisibility(paramInt, 200L);
    animator.start();
  }
  
  public void setVisibility(int paramInt) {
    if (paramInt != getVisibility()) {
      Animator animator = this.mVisibilityAnim;
      if (animator != null)
        animator.end(); 
      super.setVisibility(paramInt);
    } 
  }
  
  public boolean showOverflowMenu() {
    ActionMenuPresenter actionMenuPresenter = this.mActionMenuPresenter;
    if (actionMenuPresenter != null)
      return actionMenuPresenter.showOverflowMenu(); 
    return false;
  }
  
  public void postShowOverflowMenu() {
    post((Runnable)new Object(this));
  }
  
  public boolean hideOverflowMenu() {
    ActionMenuPresenter actionMenuPresenter = this.mActionMenuPresenter;
    if (actionMenuPresenter != null)
      return actionMenuPresenter.hideOverflowMenu(); 
    return false;
  }
  
  public boolean isOverflowMenuShowing() {
    ActionMenuPresenter actionMenuPresenter = this.mActionMenuPresenter;
    if (actionMenuPresenter != null)
      return actionMenuPresenter.isOverflowMenuShowing(); 
    return false;
  }
  
  public boolean isOverflowMenuShowPending() {
    ActionMenuPresenter actionMenuPresenter = this.mActionMenuPresenter;
    if (actionMenuPresenter != null)
      return actionMenuPresenter.isOverflowMenuShowPending(); 
    return false;
  }
  
  public boolean isOverflowReserved() {
    boolean bool;
    ActionMenuPresenter actionMenuPresenter = this.mActionMenuPresenter;
    if (actionMenuPresenter != null && actionMenuPresenter.isOverflowReserved()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean canShowOverflowMenu() {
    boolean bool;
    if (isOverflowReserved() && getVisibility() == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void dismissPopupMenus() {
    ActionMenuPresenter actionMenuPresenter = this.mActionMenuPresenter;
    if (actionMenuPresenter != null)
      actionMenuPresenter.dismissPopupMenus(); 
  }
  
  protected int measureChildView(View paramView, int paramInt1, int paramInt2, int paramInt3) {
    paramView.measure(View.MeasureSpec.makeMeasureSpec(paramInt1, -2147483648), paramInt2);
    paramInt2 = paramView.getMeasuredWidth();
    return Math.max(0, paramInt1 - paramInt2 - paramInt3);
  }
  
  protected static int next(int paramInt1, int paramInt2, boolean paramBoolean) {
    if (paramBoolean) {
      paramInt1 -= paramInt2;
    } else {
      paramInt1 += paramInt2;
    } 
    return paramInt1;
  }
  
  protected int positionChild(View paramView, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean) {
    int i = paramView.getMeasuredWidth();
    int j = paramView.getMeasuredHeight();
    paramInt2 = (paramInt3 - j) / 2 + paramInt2;
    if (paramBoolean) {
      paramView.layout(paramInt1 - i, paramInt2, paramInt1, paramInt2 + j);
    } else {
      paramView.layout(paramInt1, paramInt2, paramInt1 + i, paramInt2 + j);
    } 
    if (paramBoolean) {
      paramInt1 = -i;
    } else {
      paramInt1 = i;
    } 
    return paramInt1;
  }
  
  class VisibilityAnimListener implements Animator.AnimatorListener {
    private boolean mCanceled = false;
    
    int mFinalVisibility;
    
    final AbsActionBarView this$0;
    
    public VisibilityAnimListener withFinalVisibility(int param1Int) {
      this.mFinalVisibility = param1Int;
      return this;
    }
    
    public void onAnimationStart(Animator param1Animator) {
      AbsActionBarView.this.setVisibility(0);
      AbsActionBarView.this.mVisibilityAnim = param1Animator;
      this.mCanceled = false;
    }
    
    public void onAnimationEnd(Animator param1Animator) {
      if (this.mCanceled)
        return; 
      AbsActionBarView.this.mVisibilityAnim = null;
      AbsActionBarView.this.setVisibility(this.mFinalVisibility);
      if (AbsActionBarView.this.mSplitView != null && AbsActionBarView.this.mMenuView != null)
        AbsActionBarView.this.mMenuView.setVisibility(this.mFinalVisibility); 
    }
    
    public void onAnimationCancel(Animator param1Animator) {
      this.mCanceled = true;
    }
    
    public void onAnimationRepeat(Animator param1Animator) {}
  }
}
