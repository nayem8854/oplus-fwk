package com.android.internal.widget;

import android.content.Context;
import android.text.BoringLayout;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.TransformationMethod;
import android.util.AttributeSet;
import android.view.RemotableViewMethod;
import android.view.View;
import android.widget.RemoteViews.RemoteView;
import android.widget.TextView;

@RemoteView
public class ImageFloatingTextView extends TextView {
  private int mResolvedDirection = -1;
  
  private int mMaxLinesForHeight = -1;
  
  private int mLayoutMaxLines = -1;
  
  private int mIndentLines;
  
  private int mImageEndMargin;
  
  public ImageFloatingTextView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public ImageFloatingTextView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public ImageFloatingTextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public ImageFloatingTextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  protected Layout makeSingleLayout(int paramInt1, BoringLayout.Metrics paramMetrics, int paramInt2, Layout.Alignment paramAlignment, boolean paramBoolean1, TextUtils.TruncateAt paramTruncateAt, boolean paramBoolean2) {
    int[] arrayOfInt;
    TransformationMethod transformationMethod = getTransformationMethod();
    CharSequence charSequence2 = getText();
    CharSequence charSequence1 = charSequence2;
    if (transformationMethod != null)
      charSequence1 = transformationMethod.getTransformation(charSequence2, (View)this); 
    if (charSequence1 == null)
      charSequence1 = ""; 
    int i = charSequence1.length();
    TextPaint textPaint = getPaint();
    StaticLayout.Builder builder1 = StaticLayout.Builder.obtain(charSequence1, 0, i, textPaint, paramInt1);
    builder1 = builder1.setAlignment(paramAlignment);
    builder1 = builder1.setTextDirection(getTextDirectionHeuristic());
    builder1 = builder1.setLineSpacing(getLineSpacingExtra(), getLineSpacingMultiplier());
    builder1 = builder1.setIncludePad(getIncludeFontPadding());
    builder1 = builder1.setUseLineSpacingFromFallbacks(true);
    builder1 = builder1.setBreakStrategy(1);
    StaticLayout.Builder builder2 = builder1.setHyphenationFrequency(2);
    if (this.mMaxLinesForHeight > 0) {
      paramInt1 = this.mMaxLinesForHeight;
    } else if (getMaxLines() >= 0) {
      paramInt1 = getMaxLines();
    } else {
      paramInt1 = Integer.MAX_VALUE;
    } 
    builder2.setMaxLines(paramInt1);
    this.mLayoutMaxLines = paramInt1;
    if (paramBoolean1) {
      builder1 = builder2.setEllipsize(paramTruncateAt);
      builder1.setEllipsizedWidth(paramInt2);
    } 
    builder1 = null;
    paramInt1 = this.mIndentLines;
    if (paramInt1 > 0) {
      int[] arrayOfInt1 = new int[paramInt1 + 1];
      paramInt1 = 0;
      while (true) {
        arrayOfInt = arrayOfInt1;
        if (paramInt1 < this.mIndentLines) {
          arrayOfInt1[paramInt1] = this.mImageEndMargin;
          paramInt1++;
          continue;
        } 
        break;
      } 
    } 
    if (this.mResolvedDirection == 1) {
      builder2.setIndents(arrayOfInt, null);
    } else {
      builder2.setIndents(null, arrayOfInt);
    } 
    return (Layout)builder2.build();
  }
  
  @RemotableViewMethod
  public void setImageEndMargin(int paramInt) {
    this.mImageEndMargin = paramInt;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    int i = View.MeasureSpec.getSize(paramInt2) - this.mPaddingTop - this.mPaddingBottom;
    if (getLayout() != null && getLayout().getHeight() != i) {
      this.mMaxLinesForHeight = -1;
      nullLayouts();
    } 
    super.onMeasure(paramInt1, paramInt2);
    Layout layout = getLayout();
    if (layout.getHeight() > i) {
      int j = layout.getLineCount() - 1;
      while (j > 1 && layout.getLineBottom(j - 1) > i)
        j--; 
      i = j;
      if (getMaxLines() > 0)
        i = Math.min(getMaxLines(), j); 
      if (i != this.mLayoutMaxLines) {
        this.mMaxLinesForHeight = i;
        nullLayouts();
        super.onMeasure(paramInt1, paramInt2);
      } 
    } 
  }
  
  public void onRtlPropertiesChanged(int paramInt) {
    super.onRtlPropertiesChanged(paramInt);
    if (paramInt != this.mResolvedDirection && isLayoutDirectionResolved()) {
      this.mResolvedDirection = paramInt;
      if (this.mIndentLines > 0) {
        nullLayouts();
        requestLayout();
      } 
    } 
  }
  
  @RemotableViewMethod
  public void setHasImage(boolean paramBoolean) {
    boolean bool;
    if (paramBoolean) {
      bool = true;
    } else {
      bool = false;
    } 
    setNumIndentLines(bool);
  }
  
  public boolean setNumIndentLines(int paramInt) {
    if (this.mIndentLines != paramInt) {
      this.mIndentLines = paramInt;
      nullLayouts();
      requestLayout();
      return true;
    } 
    return false;
  }
}
