package com.android.internal.widget;

import android.app.admin.DevicePolicyManager;
import android.app.admin.PasswordMetrics;
import android.app.trust.IStrongAuthTracker;
import android.app.trust.TrustManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.UserInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.os.UserManager;
import android.os.storage.IStorageManager;
import android.os.storage.StorageManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.util.SparseIntArray;
import android.util.SparseLongArray;
import com.android.server.LocalServices;
import com.google.android.collect.Lists;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.StringJoiner;
import libcore.util.HexEncoding;

public class LockPatternUtils extends OplusBaseLockPatternUtils {
  @Deprecated
  public static final String BIOMETRIC_WEAK_EVER_CHOSEN_KEY = "lockscreen.biometricweakeverchosen";
  
  public static final int CREDENTIAL_TYPE_NONE = -1;
  
  public static final int CREDENTIAL_TYPE_PASSWORD = 4;
  
  public static final int CREDENTIAL_TYPE_PASSWORD_OR_PIN = 2;
  
  public static final int CREDENTIAL_TYPE_PATTERN = 1;
  
  public static final int CREDENTIAL_TYPE_PIN = 3;
  
  private static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  public static final String DISABLE_LOCKSCREEN_KEY = "lockscreen.disabled";
  
  private static final String ENABLED_TRUST_AGENTS = "lockscreen.enabledtrustagents";
  
  public static final int FAILED_ATTEMPTS_BEFORE_WIPE_GRACE = 5;
  
  public static final long FAILED_ATTEMPT_COUNTDOWN_INTERVAL_MS = 1000L;
  
  private static final boolean FRP_CREDENTIAL_ENABLED = true;
  
  private static final String HISTORY_DELIMITER = ",";
  
  private static final String IS_TRUST_USUALLY_MANAGED = "lockscreen.istrustusuallymanaged";
  
  public static final String LEGACY_LOCK_PATTERN_ENABLED = "legacy_lock_pattern_enabled";
  
  @Deprecated
  public static final String LOCKOUT_PERMANENT_KEY = "lockscreen.lockedoutpermanently";
  
  @Deprecated
  public static final String LOCKSCREEN_BIOMETRIC_WEAK_FALLBACK = "lockscreen.biometric_weak_fallback";
  
  public static final String LOCKSCREEN_OPTIONS = "lockscreen.options";
  
  public static final String LOCKSCREEN_POWER_BUTTON_INSTANTLY_LOCKS = "lockscreen.power_button_instantly_locks";
  
  @Deprecated
  public static final String LOCKSCREEN_WIDGETS_ENABLED = "lockscreen.widgets_enabled";
  
  public static final String LOCK_PASSWORD_SALT_KEY = "lockscreen.password_salt";
  
  private static final String LOCK_SCREEN_DEVICE_OWNER_INFO = "lockscreen.device_owner_info";
  
  private static final String LOCK_SCREEN_OWNER_INFO = "lock_screen_owner_info";
  
  private static final String LOCK_SCREEN_OWNER_INFO_ENABLED = "lock_screen_owner_info_enabled";
  
  public static final int MIN_LOCK_PASSWORD_SIZE = 4;
  
  public static final int MIN_LOCK_PATTERN_SIZE = 4;
  
  public static final int MIN_PATTERN_REGISTER_FAIL = 4;
  
  public static final String PASSWORD_HISTORY_KEY = "lockscreen.passwordhistory";
  
  @Deprecated
  public static final String PASSWORD_TYPE_ALTERNATE_KEY = "lockscreen.password_type_alternate";
  
  public static final String PASSWORD_TYPE_KEY = "lockscreen.password_type";
  
  public static final String PATTERN_EVER_CHOSEN_KEY = "lockscreen.patterneverchosen";
  
  public static final String PROFILE_KEY_NAME_DECRYPT = "profile_key_name_decrypt_";
  
  public static final String PROFILE_KEY_NAME_ENCRYPT = "profile_key_name_encrypt_";
  
  public static final int SYNTHETIC_PASSWORD_ENABLED_BY_DEFAULT = 1;
  
  public static final String SYNTHETIC_PASSWORD_ENABLED_KEY = "enable-sp";
  
  public static final String SYNTHETIC_PASSWORD_HANDLE_KEY = "sp-handle";
  
  public static final String SYNTHETIC_PASSWORD_KEY_PREFIX = "synthetic_password_";
  
  private static final String TAG = "LockPatternUtils";
  
  public static final int USER_FRP = -9999;
  
  private final ContentResolver mContentResolver;
  
  private final Context mContext;
  
  private DevicePolicyManager mDevicePolicyManager;
  
  private final Handler mHandler;
  
  private Boolean mHasSecureLockScreen;
  
  private ILockSettings mLockSettingsService;
  
  private final SparseLongArray mLockoutDeadlines = new SparseLongArray();
  
  private UserManager mUserManager;
  
  public boolean isTrustUsuallyManaged(int paramInt) {
    if (this.mLockSettingsService instanceof ILockSettings.Stub)
      try {
        return getLockSettings().getBoolean("lockscreen.istrustusuallymanaged", false, paramInt);
      } catch (RemoteException remoteException) {
        return false;
      }  
    throw new IllegalStateException("May only be called by TrustManagerService. Use TrustManager.isTrustUsuallyManaged()");
  }
  
  public void setTrustUsuallyManaged(boolean paramBoolean, int paramInt) {
    try {
      getLockSettings().setBoolean("lockscreen.istrustusuallymanaged", paramBoolean, paramInt);
    } catch (RemoteException remoteException) {}
  }
  
  public void userPresent(int paramInt) {
    try {
      getLockSettings().userPresent(paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  class RequestThrottledException extends Exception {
    private int mTimeoutMs;
    
    public RequestThrottledException(LockPatternUtils this$0) {
      this.mTimeoutMs = this$0;
    }
    
    public int getTimeoutMs() {
      return this.mTimeoutMs;
    }
  }
  
  public DevicePolicyManager getDevicePolicyManager() {
    if (this.mDevicePolicyManager == null) {
      Context context = this.mContext;
      DevicePolicyManager devicePolicyManager = (DevicePolicyManager)context.getSystemService("device_policy");
      if (devicePolicyManager == null)
        Log.e("LockPatternUtils", "Can't get DevicePolicyManagerService: is it running?", new IllegalStateException("Stack trace:")); 
    } 
    return this.mDevicePolicyManager;
  }
  
  private UserManager getUserManager() {
    if (this.mUserManager == null)
      this.mUserManager = UserManager.get(this.mContext); 
    return this.mUserManager;
  }
  
  private TrustManager getTrustManager() {
    TrustManager trustManager = (TrustManager)this.mContext.getSystemService("trust");
    if (trustManager == null)
      Log.e("LockPatternUtils", "Can't get TrustManagerService: is it running?", new IllegalStateException("Stack trace:")); 
    return trustManager;
  }
  
  public LockPatternUtils(Context paramContext) {
    this.mContext = paramContext;
    this.mContentResolver = paramContext.getContentResolver();
    Looper looper = Looper.myLooper();
    if (looper != null) {
      Handler handler = new Handler(looper);
    } else {
      looper = null;
    } 
    this.mHandler = (Handler)looper;
  }
  
  public ILockSettings getLockSettings() {
    if (this.mLockSettingsService == null) {
      IBinder iBinder = ServiceManager.getService("lock_settings");
      ILockSettings iLockSettings = ILockSettings.Stub.asInterface(iBinder);
      this.mLockSettingsService = iLockSettings;
    } 
    return this.mLockSettingsService;
  }
  
  public int getRequestedMinimumPasswordLength(int paramInt) {
    return getDevicePolicyManager().getPasswordMinimumLength(null, paramInt);
  }
  
  public int getMaximumPasswordLength(int paramInt) {
    return getDevicePolicyManager().getPasswordMaximumLength(paramInt);
  }
  
  public PasswordMetrics getRequestedPasswordMetrics(int paramInt) {
    return getDevicePolicyManager().getPasswordMinimumMetrics(paramInt);
  }
  
  public int getRequestedPasswordQuality(int paramInt) {
    return getDevicePolicyManager().getPasswordQuality(null, paramInt);
  }
  
  private int getRequestedPasswordHistoryLength(int paramInt) {
    return getDevicePolicyManager().getPasswordHistoryLength(null, paramInt);
  }
  
  public int getRequestedPasswordMinimumLetters(int paramInt) {
    return getDevicePolicyManager().getPasswordMinimumLetters(null, paramInt);
  }
  
  public int getRequestedPasswordMinimumUpperCase(int paramInt) {
    return getDevicePolicyManager().getPasswordMinimumUpperCase(null, paramInt);
  }
  
  public int getRequestedPasswordMinimumLowerCase(int paramInt) {
    return getDevicePolicyManager().getPasswordMinimumLowerCase(null, paramInt);
  }
  
  public int getRequestedPasswordMinimumNumeric(int paramInt) {
    return getDevicePolicyManager().getPasswordMinimumNumeric(null, paramInt);
  }
  
  public int getRequestedPasswordMinimumSymbols(int paramInt) {
    return getDevicePolicyManager().getPasswordMinimumSymbols(null, paramInt);
  }
  
  public int getRequestedPasswordMinimumNonLetter(int paramInt) {
    return getDevicePolicyManager().getPasswordMinimumNonLetter(null, paramInt);
  }
  
  public void reportFailedPasswordAttempt(int paramInt) {
    if (paramInt == -9999 && frpCredentialEnabled(this.mContext))
      return; 
    getDevicePolicyManager().reportFailedPasswordAttempt(paramInt);
    getTrustManager().reportUnlockAttempt(false, paramInt);
  }
  
  public void reportSuccessfulPasswordAttempt(int paramInt) {
    if (paramInt == -9999 && frpCredentialEnabled(this.mContext))
      return; 
    getDevicePolicyManager().reportSuccessfulPasswordAttempt(paramInt);
    getTrustManager().reportUnlockAttempt(true, paramInt);
  }
  
  public void reportPasswordLockout(int paramInt1, int paramInt2) {
    if (paramInt2 == -9999 && frpCredentialEnabled(this.mContext))
      return; 
    getTrustManager().reportUnlockLockout(paramInt1, paramInt2);
  }
  
  public int getCurrentFailedPasswordAttempts(int paramInt) {
    if (paramInt == -9999 && frpCredentialEnabled(this.mContext))
      return 0; 
    return getDevicePolicyManager().getCurrentFailedPasswordAttempts(paramInt);
  }
  
  public int getMaximumFailedPasswordsForWipe(int paramInt) {
    if (paramInt == -9999 && frpCredentialEnabled(this.mContext))
      return 0; 
    return getDevicePolicyManager().getMaximumFailedPasswordsForWipe(null, paramInt);
  }
  
  public byte[] verifyCredential(LockscreenCredential paramLockscreenCredential, long paramLong, int paramInt) throws RequestThrottledException {
    throwIfCalledOnMainThread();
    try {
      VerifyCredentialResponse verifyCredentialResponse = getLockSettings().verifyCredential(paramLockscreenCredential, paramLong, paramInt);
      if (verifyCredentialResponse.getResponseCode() == 0)
        return verifyCredentialResponse.getPayload(); 
      if (verifyCredentialResponse.getResponseCode() != 1)
        return null; 
      RequestThrottledException requestThrottledException = new RequestThrottledException();
      this(verifyCredentialResponse.getTimeout());
      throw requestThrottledException;
    } catch (RemoteException remoteException) {
      Log.e("LockPatternUtils", "failed to verify credential", (Throwable)remoteException);
      return null;
    } 
  }
  
  public boolean checkCredential(LockscreenCredential paramLockscreenCredential, int paramInt, CheckCredentialProgressCallback paramCheckCredentialProgressCallback) throws RequestThrottledException {
    throwIfCalledOnMainThread();
    try {
      ILockSettings iLockSettings = getLockSettings();
      ICheckCredentialProgressCallback iCheckCredentialProgressCallback = wrapCallback(paramCheckCredentialProgressCallback);
      VerifyCredentialResponse verifyCredentialResponse = iLockSettings.checkCredential(paramLockscreenCredential, paramInt, iCheckCredentialProgressCallback);
      if (verifyCredentialResponse.getResponseCode() == 0)
        return true; 
      if (verifyCredentialResponse.getResponseCode() != 1)
        return false; 
      RequestThrottledException requestThrottledException = new RequestThrottledException();
      this(verifyCredentialResponse.getTimeout());
      throw requestThrottledException;
    } catch (RemoteException remoteException) {
      Log.e("LockPatternUtils", "failed to check credential", (Throwable)remoteException);
      return false;
    } 
  }
  
  public byte[] verifyTiedProfileChallenge(LockscreenCredential paramLockscreenCredential, long paramLong, int paramInt) throws RequestThrottledException {
    throwIfCalledOnMainThread();
    try {
      VerifyCredentialResponse verifyCredentialResponse = getLockSettings().verifyTiedProfileChallenge(paramLockscreenCredential, paramLong, paramInt);
      if (verifyCredentialResponse.getResponseCode() == 0)
        return verifyCredentialResponse.getPayload(); 
      if (verifyCredentialResponse.getResponseCode() != 1)
        return null; 
      RequestThrottledException requestThrottledException = new RequestThrottledException();
      this(verifyCredentialResponse.getTimeout());
      throw requestThrottledException;
    } catch (RemoteException remoteException) {
      Log.e("LockPatternUtils", "failed to verify tied profile credential", (Throwable)remoteException);
      return null;
    } 
  }
  
  public boolean checkVoldPassword(int paramInt) {
    try {
      return getLockSettings().checkVoldPassword(paramInt);
    } catch (RemoteException remoteException) {
      Log.e("LockPatternUtils", "failed to check vold password", (Throwable)remoteException);
      return false;
    } 
  }
  
  public byte[] getPasswordHistoryHashFactor(LockscreenCredential paramLockscreenCredential, int paramInt) {
    try {
      return getLockSettings().getHashFactor(paramLockscreenCredential, paramInt);
    } catch (RemoteException remoteException) {
      Log.e("LockPatternUtils", "failed to get hash factor", (Throwable)remoteException);
      return null;
    } 
  }
  
  public boolean checkPasswordHistory(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, int paramInt) {
    if (paramArrayOfbyte1 == null || paramArrayOfbyte1.length == 0) {
      Log.e("LockPatternUtils", "checkPasswordHistory: empty password");
      return false;
    } 
    String str2 = getString("lockscreen.passwordhistory", paramInt);
    if (TextUtils.isEmpty(str2))
      return false; 
    int i = getRequestedPasswordHistoryLength(paramInt);
    if (i == 0)
      return false; 
    String str3 = legacyPasswordToHash(paramArrayOfbyte1, paramInt);
    String str1 = passwordToHistoryHash(paramArrayOfbyte1, paramArrayOfbyte2, paramInt);
    String[] arrayOfString = str2.split(",");
    for (paramInt = 0; paramInt < Math.min(i, arrayOfString.length); paramInt++) {
      if (arrayOfString[paramInt].equals(str3) || arrayOfString[paramInt].equals(str1))
        return true; 
    } 
    return false;
  }
  
  public boolean isPatternEverChosen(int paramInt) {
    return getBoolean("lockscreen.patterneverchosen", false, paramInt);
  }
  
  public void reportPatternWasChosen(int paramInt) {
    setBoolean("lockscreen.patterneverchosen", true, paramInt);
  }
  
  public int getActivePasswordQuality(int paramInt) {
    return getKeyguardStoredPasswordQuality(paramInt);
  }
  
  public void resetKeyStore(int paramInt) {
    try {
      getLockSettings().resetKeyStore(paramInt);
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Couldn't reset keystore ");
      stringBuilder.append(remoteException);
      Log.e("LockPatternUtils", stringBuilder.toString());
    } 
  }
  
  public void setLockScreenDisabled(boolean paramBoolean, int paramInt) {
    setBoolean("lockscreen.disabled", paramBoolean, paramInt);
  }
  
  public boolean isLockScreenDisabled(int paramInt) {
    boolean bool2, bool3;
    boolean bool = isSecure(paramInt);
    boolean bool1 = false;
    if (bool)
      return false; 
    bool = this.mContext.getResources().getBoolean(17891411);
    if (UserManager.isSplitSystemUser() && paramInt == 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    UserInfo userInfo = getUserManager().getUserInfo(paramInt);
    if (UserManager.isDeviceInDemoMode(this.mContext) && userInfo != null && 
      userInfo.isDemo()) {
      bool3 = true;
    } else {
      bool3 = false;
    } 
    if (getBoolean("lockscreen.disabled", false, paramInt) || (bool && !bool2) || bool3)
      bool1 = true; 
    return bool1;
  }
  
  public static boolean isQualityAlphabeticPassword(int paramInt) {
    boolean bool;
    if (paramInt >= 262144) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isQualityNumericPin(int paramInt) {
    return (paramInt == 131072 || paramInt == 196608);
  }
  
  public static int credentialTypeToPasswordQuality(int paramInt) {
    if (paramInt != -1) {
      if (paramInt != 1) {
        if (paramInt != 3) {
          if (paramInt == 4)
            return 262144; 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Unknown type: ");
          stringBuilder.append(paramInt);
          throw new IllegalStateException(stringBuilder.toString());
        } 
        return 131072;
      } 
      return 65536;
    } 
    return 0;
  }
  
  public boolean setLockCredential(LockscreenCredential paramLockscreenCredential1, LockscreenCredential paramLockscreenCredential2, int paramInt) {
    if (hasSecureLockScreen()) {
      paramLockscreenCredential1.checkLength();
      try {
        if (!getLockSettings().setLockCredential(paramLockscreenCredential1, paramLockscreenCredential2, paramInt))
          return false; 
        Settings.System.putIntForUser(this.mContext.getContentResolver(), "PASSWORD_LENGTH", paramLockscreenCredential1.size(), paramInt);
        onPostPasswordChanged(paramLockscreenCredential1, paramInt);
        return true;
      } catch (RemoteException remoteException) {
        throw new RuntimeException("Unable to save lock password", remoteException);
      } 
    } 
    throw new UnsupportedOperationException("This operation requires the lock screen feature.");
  }
  
  private void onPostPasswordChanged(LockscreenCredential paramLockscreenCredential, int paramInt) {
    updateEncryptionPasswordIfNeeded(paramLockscreenCredential, paramInt);
    if (paramLockscreenCredential.isPattern())
      reportPatternWasChosen(paramInt); 
    updatePasswordHistory(paramLockscreenCredential, paramInt);
    reportEnabledTrustAgentsChanged(paramInt);
  }
  
  public void sanitizePassword() {
    try {
      getLockSettings().sanitizePassword();
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Couldn't sanitize password");
      stringBuilder.append(remoteException);
      Log.e("LockPatternUtils", stringBuilder.toString());
    } 
  }
  
  private void updateCryptoUserInfo(int paramInt) {
    String str;
    if (paramInt != 0)
      return; 
    if (isOwnerInfoEnabled(paramInt)) {
      str = getOwnerInfo(paramInt);
    } else {
      str = "";
    } 
    IBinder iBinder = ServiceManager.getService("mount");
    if (iBinder == null) {
      Log.e("LockPatternUtils", "Could not find the mount service to update the user info");
      return;
    } 
    IStorageManager iStorageManager = IStorageManager.Stub.asInterface(iBinder);
    try {
      Log.d("LockPatternUtils", "Setting owner info");
      iStorageManager.setField("OwnerInfo", str);
    } catch (RemoteException remoteException) {
      Log.e("LockPatternUtils", "Error changing user info", (Throwable)remoteException);
    } 
  }
  
  public void setOwnerInfo(String paramString, int paramInt) {
    setString("lock_screen_owner_info", paramString, paramInt);
    updateCryptoUserInfo(paramInt);
  }
  
  public void setOwnerInfoEnabled(boolean paramBoolean, int paramInt) {
    setBoolean("lock_screen_owner_info_enabled", paramBoolean, paramInt);
    updateCryptoUserInfo(paramInt);
  }
  
  public String getOwnerInfo(int paramInt) {
    return getString("lock_screen_owner_info", paramInt);
  }
  
  public boolean isOwnerInfoEnabled(int paramInt) {
    return getBoolean("lock_screen_owner_info_enabled", false, paramInt);
  }
  
  public void setDeviceOwnerInfo(String paramString) {
    String str = paramString;
    if (paramString != null) {
      str = paramString;
      if (paramString.isEmpty())
        str = null; 
    } 
    setString("lockscreen.device_owner_info", str, 0);
  }
  
  public String getDeviceOwnerInfo() {
    return getString("lockscreen.device_owner_info", 0);
  }
  
  public boolean isDeviceOwnerInfoEnabled() {
    boolean bool;
    if (getDeviceOwnerInfo() != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void updateEncryptionPassword(final int type, final byte[] passwordString) {
    if (hasSecureLockScreen()) {
      if (!isDeviceEncryptionEnabled())
        return; 
      final IBinder service = ServiceManager.getService("mount");
      if (iBinder == null) {
        Log.e("LockPatternUtils", "Could not find the mount service to update the encryption password");
        return;
      } 
      if (passwordString != null) {
        String str = new String(passwordString);
      } else {
        passwordString = null;
      } 
      AsyncTask<Void, Void, Void> asyncTask = new AsyncTask<Void, Void, Void>() {
          final LockPatternUtils this$0;
          
          final String val$passwordString;
          
          final IBinder val$service;
          
          final int val$type;
          
          protected Void doInBackground(Void... param1VarArgs) {
            IStorageManager iStorageManager = IStorageManager.Stub.asInterface(service);
            try {
              iStorageManager.changeEncryptionPassword(type, passwordString);
            } catch (RemoteException remoteException) {
              Log.e("LockPatternUtils", "Error changing encryption password", (Throwable)remoteException);
            } 
            return null;
          }
        };
      asyncTask.execute((Object[])new Void[0]);
      return;
    } 
    throw new UnsupportedOperationException("This operation requires the lock screen feature.");
  }
  
  private void updateEncryptionPasswordIfNeeded(LockscreenCredential paramLockscreenCredential, int paramInt) {
    if (paramInt != 0 || !isDeviceEncryptionEnabled())
      return; 
    if (!shouldEncryptWithCredentials(true)) {
      updateEncryptionPassword(1, null);
      return;
    } 
    if (paramLockscreenCredential.isNone())
      setCredentialRequiredToDecrypt(false); 
    updateEncryptionPassword(paramLockscreenCredential.getStorageCryptType(), paramLockscreenCredential.getCredential());
  }
  
  private void updatePasswordHistory(LockscreenCredential paramLockscreenCredential, int paramInt) {
    String str1;
    if (paramLockscreenCredential.isNone())
      return; 
    if (paramLockscreenCredential.isPattern())
      return; 
    String str2 = getString("lockscreen.passwordhistory", paramInt);
    String str3 = str2;
    if (str2 == null)
      str3 = ""; 
    int i = getRequestedPasswordHistoryLength(paramInt);
    if (i == 0) {
      str1 = "";
    } else {
      byte[] arrayOfByte = getPasswordHistoryHashFactor((LockscreenCredential)str1, paramInt);
      String str5 = passwordToHistoryHash(str1.getCredential(), arrayOfByte, paramInt);
      String str4 = str5;
      if (str5 == null) {
        Log.e("LockPatternUtils", "Compute new style password hash failed, fallback to legacy style");
        str4 = legacyPasswordToHash(str1.getCredential(), paramInt);
      } 
      if (TextUtils.isEmpty(str3)) {
        str1 = str4;
      } else {
        String[] arrayOfString = str3.split(",");
        StringJoiner stringJoiner = new StringJoiner(",");
        stringJoiner.add(str4);
        for (byte b = 0; b < i - 1 && b < arrayOfString.length; b++)
          stringJoiner.add(arrayOfString[b]); 
        str1 = stringJoiner.toString();
      } 
    } 
    setString("lockscreen.passwordhistory", str1, paramInt);
  }
  
  public static boolean isDeviceEncryptionEnabled() {
    return StorageManager.isEncrypted();
  }
  
  public static boolean isFileEncryptionEnabled() {
    return StorageManager.isFileEncryptedNativeOrEmulated();
  }
  
  public void clearEncryptionPassword() {
    updateEncryptionPassword(1, null);
  }
  
  @Deprecated
  public int getKeyguardStoredPasswordQuality(int paramInt) {
    return credentialTypeToPasswordQuality(getCredentialTypeForUser(paramInt));
  }
  
  public void setSeparateProfileChallengeEnabled(int paramInt, boolean paramBoolean, LockscreenCredential paramLockscreenCredential) {
    if (!isManagedProfile(paramInt))
      return; 
    try {
      getLockSettings().setSeparateProfileChallengeEnabled(paramInt, paramBoolean, paramLockscreenCredential);
      reportEnabledTrustAgentsChanged(paramInt);
    } catch (RemoteException remoteException) {
      Log.e("LockPatternUtils", "Couldn't update work profile challenge enabled");
    } 
  }
  
  public boolean isSeparateProfileChallengeEnabled(int paramInt) {
    boolean bool;
    if (isManagedProfile(paramInt) && hasSeparateChallenge(paramInt)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isManagedProfileWithUnifiedChallenge(int paramInt) {
    boolean bool;
    if (isManagedProfile(paramInt) && !hasSeparateChallenge(paramInt)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isSeparateProfileChallengeAllowed(int paramInt) {
    boolean bool;
    if (isManagedProfile(paramInt) && 
      getDevicePolicyManager().isSeparateProfileChallengeAllowed(paramInt)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isSeparateProfileChallengeAllowedToUnify(int paramInt) {
    if (getDevicePolicyManager().isProfileActivePasswordSufficientForParent(paramInt)) {
      UserManager userManager = getUserManager();
      UserHandle userHandle = UserHandle.of(paramInt);
      if (!userManager.hasUserRestriction("no_unified_password", userHandle))
        return true; 
    } 
    return false;
  }
  
  private boolean hasSeparateChallenge(int paramInt) {
    try {
      return getLockSettings().getSeparateProfileChallengeEnabled(paramInt);
    } catch (RemoteException remoteException) {
      Log.e("LockPatternUtils", "Couldn't get separate profile challenge enabled");
      return false;
    } 
  }
  
  private boolean isManagedProfile(int paramInt) {
    boolean bool;
    UserInfo userInfo = getUserManager().getUserInfo(paramInt);
    if (userInfo != null && userInfo.isManagedProfile()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static List<LockPatternView.Cell> byteArrayToPattern(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte == null)
      return null; 
    ArrayList<?> arrayList = Lists.newArrayList();
    for (byte b = 0; b < paramArrayOfbyte.length; b++) {
      byte b1 = (byte)(paramArrayOfbyte[b] - 49);
      arrayList.add(LockPatternView.Cell.of(b1 / 3, b1 % 3));
    } 
    return (List)arrayList;
  }
  
  public static byte[] patternToByteArray(List<LockPatternView.Cell> paramList) {
    if (paramList == null)
      return new byte[0]; 
    int i = paramList.size();
    byte[] arrayOfByte = new byte[i];
    for (byte b = 0; b < i; b++) {
      LockPatternView.Cell cell = paramList.get(b);
      arrayOfByte[b] = (byte)(cell.getRow() * 3 + cell.getColumn() + 49);
    } 
    return arrayOfByte;
  }
  
  private String getSalt(int paramInt) {
    long l1 = getLong("lockscreen.password_salt", 0L, paramInt);
    long l2 = l1;
    if (l1 == 0L)
      try {
        l2 = SecureRandom.getInstance("SHA1PRNG").nextLong();
        setLong("lockscreen.password_salt", l2, paramInt);
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Initialized lock password salt for user: ");
        stringBuilder.append(paramInt);
        Log.v("LockPatternUtils", stringBuilder.toString());
      } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
        throw new IllegalStateException("Couldn't get SecureRandom number", noSuchAlgorithmException);
      }  
    return Long.toHexString(l2);
  }
  
  public String legacyPasswordToHash(byte[] paramArrayOfbyte, int paramInt) {
    if (paramArrayOfbyte == null || paramArrayOfbyte.length == 0)
      return null; 
    try {
      byte[] arrayOfByte1 = getSalt(paramInt).getBytes();
      byte[] arrayOfByte2 = Arrays.copyOf(paramArrayOfbyte, paramArrayOfbyte.length + arrayOfByte1.length);
      System.arraycopy(arrayOfByte1, 0, arrayOfByte2, paramArrayOfbyte.length, arrayOfByte1.length);
      arrayOfByte1 = MessageDigest.getInstance("SHA-1").digest(arrayOfByte2);
      paramArrayOfbyte = MessageDigest.getInstance("MD5").digest(arrayOfByte2);
      byte[] arrayOfByte3 = new byte[arrayOfByte1.length + paramArrayOfbyte.length];
      System.arraycopy(arrayOfByte1, 0, arrayOfByte3, 0, arrayOfByte1.length);
      System.arraycopy(paramArrayOfbyte, 0, arrayOfByte3, arrayOfByte1.length, paramArrayOfbyte.length);
      char[] arrayOfChar = HexEncoding.encode(arrayOfByte3);
      Arrays.fill(arrayOfByte2, (byte)0);
      return new String(arrayOfChar);
    } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
      throw new AssertionError("Missing digest algorithm: ", noSuchAlgorithmException);
    } 
  }
  
  private String passwordToHistoryHash(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, int paramInt) {
    if (paramArrayOfbyte1 == null || paramArrayOfbyte1.length == 0 || paramArrayOfbyte2 == null)
      return null; 
    try {
      MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
      messageDigest.update(paramArrayOfbyte2);
      paramArrayOfbyte2 = getSalt(paramInt).getBytes();
      byte[] arrayOfByte = Arrays.copyOf(paramArrayOfbyte1, paramArrayOfbyte1.length + paramArrayOfbyte2.length);
      System.arraycopy(paramArrayOfbyte2, 0, arrayOfByte, paramArrayOfbyte1.length, paramArrayOfbyte2.length);
      messageDigest.update(arrayOfByte);
      Arrays.fill(arrayOfByte, (byte)0);
      return new String(HexEncoding.encode(messageDigest.digest()));
    } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
      throw new AssertionError("Missing digest algorithm: ", noSuchAlgorithmException);
    } 
  }
  
  public int getCredentialTypeForUser(int paramInt) {
    try {
      return getLockSettings().getCredentialType(paramInt);
    } catch (RemoteException remoteException) {
      Log.e("LockPatternUtils", "failed to get credential type", (Throwable)remoteException);
      return -1;
    } 
  }
  
  public boolean isSecure(int paramInt) {
    boolean bool;
    paramInt = getCredentialTypeForUser(paramInt);
    if (paramInt != -1) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isLockPasswordEnabled(int paramInt) {
    paramInt = getCredentialTypeForUser(paramInt);
    return (paramInt == 4 || paramInt == 3);
  }
  
  public boolean isLockPatternEnabled(int paramInt) {
    paramInt = getCredentialTypeForUser(paramInt);
    boolean bool = true;
    if (paramInt != 1)
      bool = false; 
    return bool;
  }
  
  @Deprecated
  public boolean isLegacyLockPatternEnabled(int paramInt) {
    return getBoolean("legacy_lock_pattern_enabled", true, paramInt);
  }
  
  @Deprecated
  public void setLegacyLockPatternEnabled(int paramInt) {
    setBoolean("lock_pattern_autolock", true, paramInt);
  }
  
  public boolean isVisiblePatternEnabled(int paramInt) {
    return getBoolean("lock_pattern_visible_pattern", false, paramInt);
  }
  
  public void setVisiblePatternEnabled(boolean paramBoolean, int paramInt) {
    String str;
    setBoolean("lock_pattern_visible_pattern", paramBoolean, paramInt);
    if (paramInt != 0)
      return; 
    IBinder iBinder = ServiceManager.getService("mount");
    if (iBinder == null) {
      Log.e("LockPatternUtils", "Could not find the mount service to update the user info");
      return;
    } 
    IStorageManager iStorageManager = IStorageManager.Stub.asInterface(iBinder);
    if (paramBoolean) {
      str = "1";
    } else {
      str = "0";
    } 
    try {
      iStorageManager.setField("PatternVisible", str);
    } catch (RemoteException remoteException) {
      Log.e("LockPatternUtils", "Error changing pattern visible state", (Throwable)remoteException);
    } 
  }
  
  public boolean isVisiblePatternEverChosen(int paramInt) {
    boolean bool;
    if (getString("lock_pattern_visible_pattern", paramInt) != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setVisiblePasswordEnabled(boolean paramBoolean, int paramInt) {
    String str;
    if (paramInt != 0)
      return; 
    IBinder iBinder = ServiceManager.getService("mount");
    if (iBinder == null) {
      Log.e("LockPatternUtils", "Could not find the mount service to update the user info");
      return;
    } 
    IStorageManager iStorageManager = IStorageManager.Stub.asInterface(iBinder);
    if (paramBoolean) {
      str = "1";
    } else {
      str = "0";
    } 
    try {
      iStorageManager.setField("PasswordVisible", str);
    } catch (RemoteException remoteException) {
      Log.e("LockPatternUtils", "Error changing password visible state", (Throwable)remoteException);
    } 
  }
  
  public boolean isTactileFeedbackEnabled() {
    ContentResolver contentResolver = this.mContentResolver;
    boolean bool = true;
    if (Settings.System.getIntForUser(contentResolver, "haptic_feedback_enabled", 1, -2) == 0)
      bool = false; 
    return bool;
  }
  
  public long setLockoutAttemptDeadline(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: invokestatic elapsedRealtime : ()J
    //   3: iload_2
    //   4: i2l
    //   5: ladd
    //   6: lstore_3
    //   7: iload_1
    //   8: sipush #-9999
    //   11: if_icmpne -> 16
    //   14: lload_3
    //   15: lreturn
    //   16: aload_0
    //   17: monitorenter
    //   18: aload_0
    //   19: ldc_w 'lockscreen.lockoutattemptdeadline'
    //   22: lload_3
    //   23: iload_1
    //   24: invokespecial setLong : (Ljava/lang/String;JI)V
    //   27: aload_0
    //   28: ldc_w 'lockscreen.lockoutattempttimeoutmss'
    //   31: iload_2
    //   32: i2l
    //   33: iload_1
    //   34: invokespecial setLong : (Ljava/lang/String;JI)V
    //   37: getstatic com/android/internal/widget/LockPatternUtils.DEBUG : Z
    //   40: ifeq -> 96
    //   43: new java/lang/StringBuilder
    //   46: astore #5
    //   48: aload #5
    //   50: invokespecial <init> : ()V
    //   53: aload #5
    //   55: ldc_w 'setLockoutAttemptDeadline: userId ='
    //   58: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   61: pop
    //   62: aload #5
    //   64: iload_1
    //   65: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   68: pop
    //   69: aload #5
    //   71: ldc_w ' timeoutMs ='
    //   74: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   77: pop
    //   78: aload #5
    //   80: iload_2
    //   81: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   84: pop
    //   85: ldc 'LockPatternUtils'
    //   87: aload #5
    //   89: invokevirtual toString : ()Ljava/lang/String;
    //   92: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   95: pop
    //   96: aload_0
    //   97: monitorexit
    //   98: lload_3
    //   99: lreturn
    //   100: astore #5
    //   102: aload_0
    //   103: monitorexit
    //   104: aload #5
    //   106: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1212	-> 0
    //   #1213	-> 7
    //   #1216	-> 14
    //   #1223	-> 16
    //   #1224	-> 18
    //   #1225	-> 27
    //   #1226	-> 37
    //   #1227	-> 43
    //   #1229	-> 96
    //   #1231	-> 98
    //   #1229	-> 100
    // Exception table:
    //   from	to	target	type
    //   18	27	100	finally
    //   27	37	100	finally
    //   37	43	100	finally
    //   43	96	100	finally
    //   96	98	100	finally
    //   102	104	100	finally
  }
  
  public long getLockoutAttemptDeadline(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: ldc_w 'lockscreen.lockoutattemptdeadline'
    //   6: lconst_0
    //   7: iload_1
    //   8: invokespecial getLong : (Ljava/lang/String;JI)J
    //   11: lstore_2
    //   12: aload_0
    //   13: ldc_w 'lockscreen.lockoutattempttimeoutmss'
    //   16: lconst_0
    //   17: iload_1
    //   18: invokespecial getLong : (Ljava/lang/String;JI)J
    //   21: lstore #4
    //   23: invokestatic elapsedRealtime : ()J
    //   26: lstore #6
    //   28: lload_2
    //   29: lload #6
    //   31: lcmp
    //   32: ifge -> 63
    //   35: lload_2
    //   36: lconst_0
    //   37: lcmp
    //   38: ifeq -> 63
    //   41: aload_0
    //   42: ldc_w 'lockscreen.lockoutattemptdeadline'
    //   45: lconst_0
    //   46: iload_1
    //   47: invokespecial setLong : (Ljava/lang/String;JI)V
    //   50: aload_0
    //   51: ldc_w 'lockscreen.lockoutattempttimeoutmss'
    //   54: lconst_0
    //   55: iload_1
    //   56: invokespecial setLong : (Ljava/lang/String;JI)V
    //   59: aload_0
    //   60: monitorexit
    //   61: lconst_0
    //   62: lreturn
    //   63: lload_2
    //   64: lstore #8
    //   66: lload_2
    //   67: lload #6
    //   69: lload #4
    //   71: ladd
    //   72: lcmp
    //   73: ifle -> 157
    //   76: lload #6
    //   78: lload #4
    //   80: ladd
    //   81: lstore_2
    //   82: aload_0
    //   83: ldc_w 'lockscreen.lockoutattemptdeadline'
    //   86: lload_2
    //   87: iload_1
    //   88: invokespecial setLong : (Ljava/lang/String;JI)V
    //   91: lload_2
    //   92: lstore #8
    //   94: getstatic com/android/internal/widget/LockPatternUtils.DEBUG : Z
    //   97: ifeq -> 157
    //   100: new java/lang/StringBuilder
    //   103: astore #10
    //   105: aload #10
    //   107: invokespecial <init> : ()V
    //   110: aload #10
    //   112: ldc_w 'getLockoutAttemptDeadline: now= '
    //   115: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   118: pop
    //   119: aload #10
    //   121: lload #6
    //   123: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   126: pop
    //   127: aload #10
    //   129: ldc_w 'deadline= '
    //   132: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   135: pop
    //   136: aload #10
    //   138: lload_2
    //   139: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   142: pop
    //   143: ldc 'LockPatternUtils'
    //   145: aload #10
    //   147: invokevirtual toString : ()Ljava/lang/String;
    //   150: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   153: pop
    //   154: lload_2
    //   155: lstore #8
    //   157: aload_0
    //   158: monitorexit
    //   159: lload #8
    //   161: lreturn
    //   162: astore #10
    //   164: aload_0
    //   165: monitorexit
    //   166: aload #10
    //   168: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1252	-> 0
    //   #1253	-> 2
    //   #1254	-> 12
    //   #1255	-> 23
    //   #1256	-> 28
    //   #1259	-> 41
    //   #1260	-> 50
    //   #1261	-> 59
    //   #1264	-> 63
    //   #1266	-> 76
    //   #1267	-> 82
    //   #1268	-> 91
    //   #1269	-> 100
    //   #1272	-> 157
    //   #1273	-> 162
    // Exception table:
    //   from	to	target	type
    //   2	12	162	finally
    //   12	23	162	finally
    //   23	28	162	finally
    //   41	50	162	finally
    //   50	59	162	finally
    //   59	61	162	finally
    //   82	91	162	finally
    //   94	100	162	finally
    //   100	154	162	finally
    //   157	159	162	finally
    //   164	166	162	finally
  }
  
  private boolean getBoolean(String paramString, boolean paramBoolean, int paramInt) {
    try {
      return getLockSettings().getBoolean(paramString, paramBoolean, paramInt);
    } catch (RemoteException remoteException) {
      return paramBoolean;
    } 
  }
  
  private void setBoolean(String paramString, boolean paramBoolean, int paramInt) {
    try {
      getLockSettings().setBoolean(paramString, paramBoolean, paramInt);
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Couldn't write boolean ");
      stringBuilder.append(paramString);
      stringBuilder.append(remoteException);
      Log.e("LockPatternUtils", stringBuilder.toString());
    } 
  }
  
  private long getLong(String paramString, long paramLong, int paramInt) {
    try {
      return getLockSettings().getLong(paramString, paramLong, paramInt);
    } catch (RemoteException remoteException) {
      return paramLong;
    } 
  }
  
  private void setLong(String paramString, long paramLong, int paramInt) {
    try {
      getLockSettings().setLong(paramString, paramLong, paramInt);
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Couldn't write long ");
      stringBuilder.append(paramString);
      stringBuilder.append(remoteException);
      Log.e("LockPatternUtils", stringBuilder.toString());
    } 
  }
  
  private String getString(String paramString, int paramInt) {
    try {
      return getLockSettings().getString(paramString, null, paramInt);
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  private void setString(String paramString1, String paramString2, int paramInt) {
    try {
      getLockSettings().setString(paramString1, paramString2, paramInt);
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Couldn't write string ");
      stringBuilder.append(paramString1);
      stringBuilder.append(remoteException);
      Log.e("LockPatternUtils", stringBuilder.toString());
    } 
  }
  
  public void setPowerButtonInstantlyLocks(boolean paramBoolean, int paramInt) {
    setBoolean("lockscreen.power_button_instantly_locks", paramBoolean, paramInt);
  }
  
  public boolean getPowerButtonInstantlyLocks(int paramInt) {
    return getBoolean("lockscreen.power_button_instantly_locks", true, paramInt);
  }
  
  public boolean isPowerButtonInstantlyLocksEverChosen(int paramInt) {
    boolean bool;
    if (getString("lockscreen.power_button_instantly_locks", paramInt) != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setEnabledTrustAgents(Collection<ComponentName> paramCollection, int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    for (ComponentName componentName : paramCollection) {
      if (stringBuilder.length() > 0)
        stringBuilder.append(','); 
      stringBuilder.append(componentName.flattenToShortString());
    } 
    setString("lockscreen.enabledtrustagents", stringBuilder.toString(), paramInt);
    getTrustManager().reportEnabledTrustAgentsChanged(paramInt);
  }
  
  public List<ComponentName> getEnabledTrustAgents(int paramInt) {
    String str = getString("lockscreen.enabledtrustagents", paramInt);
    if (TextUtils.isEmpty(str))
      return null; 
    String[] arrayOfString = str.split(",");
    ArrayList<ComponentName> arrayList = new ArrayList(arrayOfString.length);
    for (int i = arrayOfString.length; paramInt < i; ) {
      String str1 = arrayOfString[paramInt];
      if (!TextUtils.isEmpty(str1))
        arrayList.add(ComponentName.unflattenFromString(str1)); 
      paramInt++;
    } 
    return arrayList;
  }
  
  public void requireCredentialEntry(int paramInt) {
    requireStrongAuth(4, paramInt);
  }
  
  public void requireStrongAuth(int paramInt1, int paramInt2) {
    try {
      getLockSettings().requireStrongAuth(paramInt1, paramInt2);
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error while requesting strong auth: ");
      stringBuilder.append(remoteException);
      Log.e("LockPatternUtils", stringBuilder.toString());
    } 
  }
  
  private void reportEnabledTrustAgentsChanged(int paramInt) {
    getTrustManager().reportEnabledTrustAgentsChanged(paramInt);
  }
  
  public boolean isCredentialRequiredToDecrypt(boolean paramBoolean) {
    int i = Settings.Global.getInt(this.mContentResolver, "require_password_to_decrypt", -1);
    if (i != -1)
      if (i != 0) {
        paramBoolean = true;
      } else {
        paramBoolean = false;
      }  
    return paramBoolean;
  }
  
  public void setCredentialRequiredToDecrypt(boolean paramBoolean) {
    if (getUserManager().isSystemUser() || getUserManager().isPrimaryUser()) {
      if (isDeviceEncryptionEnabled()) {
        ContentResolver contentResolver = this.mContext.getContentResolver();
        Settings.Global.putInt(contentResolver, "require_password_to_decrypt", paramBoolean);
      } 
      return;
    } 
    throw new IllegalStateException("Only the system or primary user may call setCredentialRequiredForDecrypt()");
  }
  
  private boolean isDoNotAskCredentialsOnBootSet() {
    return getDevicePolicyManager().getDoNotAskCredentialsOnBoot();
  }
  
  private boolean shouldEncryptWithCredentials(boolean paramBoolean) {
    if (isCredentialRequiredToDecrypt(paramBoolean) && !isDoNotAskCredentialsOnBootSet()) {
      paramBoolean = true;
    } else {
      paramBoolean = false;
    } 
    return paramBoolean;
  }
  
  private void throwIfCalledOnMainThread() {
    if (!Looper.getMainLooper().isCurrentThread())
      return; 
    throw new IllegalStateException("should not be called from the main thread.");
  }
  
  public void registerStrongAuthTracker(StrongAuthTracker paramStrongAuthTracker) {
    try {
      getLockSettings().registerStrongAuthTracker((IStrongAuthTracker)paramStrongAuthTracker.getStub());
      return;
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Could not register StrongAuthTracker");
    } 
  }
  
  public void unregisterStrongAuthTracker(StrongAuthTracker paramStrongAuthTracker) {
    try {
      getLockSettings().unregisterStrongAuthTracker((IStrongAuthTracker)paramStrongAuthTracker.getStub());
    } catch (RemoteException remoteException) {
      Log.e("LockPatternUtils", "Could not unregister StrongAuthTracker", (Throwable)remoteException);
    } 
  }
  
  public void reportSuccessfulBiometricUnlock(boolean paramBoolean, int paramInt) {
    try {
      getLockSettings().reportSuccessfulBiometricUnlock(paramBoolean, paramInt);
    } catch (RemoteException remoteException) {
      Log.e("LockPatternUtils", "Could not report successful biometric unlock", (Throwable)remoteException);
    } 
  }
  
  public void scheduleNonStrongBiometricIdleTimeout(int paramInt) {
    try {
      getLockSettings().scheduleNonStrongBiometricIdleTimeout(paramInt);
    } catch (RemoteException remoteException) {
      Log.e("LockPatternUtils", "Could not schedule non-strong biometric idle timeout", (Throwable)remoteException);
    } 
  }
  
  public int getStrongAuthForUser(int paramInt) {
    try {
      return getLockSettings().getStrongAuthForUser(paramInt);
    } catch (RemoteException remoteException) {
      Log.e("LockPatternUtils", "Could not get StrongAuth", (Throwable)remoteException);
      return StrongAuthTracker.getDefaultFlags(this.mContext);
    } 
  }
  
  public boolean isTrustAllowedForUser(int paramInt) {
    boolean bool;
    if (getStrongAuthForUser(paramInt) == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isBiometricAllowedForUser(int paramInt) {
    boolean bool2;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("isBiometricAllowedForUser allowed:");
    int i = getStrongAuthForUser(paramInt);
    boolean bool1 = true;
    if ((i & 0xFFFFFFFB) == 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    stringBuilder.append(bool2);
    stringBuilder.append(" userId:");
    stringBuilder.append(paramInt);
    Log.d("LockPatternUtils", stringBuilder.toString());
    if ((getStrongAuthForUser(paramInt) & 0xFFFFFFFB) == 0) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    return bool2;
  }
  
  public boolean isUserInLockdown(int paramInt) {
    boolean bool;
    if (getStrongAuthForUser(paramInt) == 32) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  class WrappedCallback extends ICheckCredentialProgressCallback.Stub {
    private LockPatternUtils.CheckCredentialProgressCallback mCallback;
    
    private Handler mHandler;
    
    WrappedCallback(LockPatternUtils this$0, LockPatternUtils.CheckCredentialProgressCallback param1CheckCredentialProgressCallback) {
      this.mHandler = (Handler)this$0;
      this.mCallback = param1CheckCredentialProgressCallback;
    }
    
    public void onCredentialVerified() throws RemoteException {
      if (this.mHandler == null)
        Log.e("LockPatternUtils", "Handler is null during callback"); 
      this.mHandler.post(new _$$Lambda$LockPatternUtils$WrappedCallback$i9jMZqkjCdhv8ydv_FyQJHm7hSE(this));
      this.mHandler = null;
    }
  }
  
  private ICheckCredentialProgressCallback wrapCallback(CheckCredentialProgressCallback paramCheckCredentialProgressCallback) {
    if (paramCheckCredentialProgressCallback == null)
      return null; 
    if (this.mHandler != null)
      return new WrappedCallback(this.mHandler, paramCheckCredentialProgressCallback); 
    throw new IllegalStateException("Must construct LockPatternUtils on a looper thread to use progress callbacks.");
  }
  
  private LockSettingsInternal getLockSettingsInternal() {
    LockSettingsInternal lockSettingsInternal = LocalServices.<LockSettingsInternal>getService(LockSettingsInternal.class);
    if (lockSettingsInternal != null)
      return lockSettingsInternal; 
    throw new SecurityException("Only available to system server itself");
  }
  
  public long addEscrowToken(byte[] paramArrayOfbyte, int paramInt, EscrowTokenStateChangeCallback paramEscrowTokenStateChangeCallback) {
    return getLockSettingsInternal().addEscrowToken(paramArrayOfbyte, paramInt, paramEscrowTokenStateChangeCallback);
  }
  
  public boolean removeEscrowToken(long paramLong, int paramInt) {
    return getLockSettingsInternal().removeEscrowToken(paramLong, paramInt);
  }
  
  public boolean isEscrowTokenActive(long paramLong, int paramInt) {
    return getLockSettingsInternal().isEscrowTokenActive(paramLong, paramInt);
  }
  
  public boolean setLockCredentialWithToken(LockscreenCredential paramLockscreenCredential, long paramLong, byte[] paramArrayOfbyte, int paramInt) {
    if (hasSecureLockScreen()) {
      paramLockscreenCredential.checkLength();
      LockSettingsInternal lockSettingsInternal = getLockSettingsInternal();
      if (!lockSettingsInternal.setLockCredentialWithToken(paramLockscreenCredential, paramLong, paramArrayOfbyte, paramInt))
        return false; 
      onPostPasswordChanged(paramLockscreenCredential, paramInt);
      return true;
    } 
    throw new UnsupportedOperationException("This operation requires the lock screen feature.");
  }
  
  public boolean unlockUserWithToken(long paramLong, byte[] paramArrayOfbyte, int paramInt) {
    return getLockSettingsInternal().unlockUserWithToken(paramLong, paramArrayOfbyte, paramInt);
  }
  
  class StrongAuthTracker {
    private final IStrongAuthTracker.Stub mStub;
    
    private final SparseIntArray mStrongAuthRequiredForUser = new SparseIntArray();
    
    private final SparseBooleanArray mIsNonStrongBiometricAllowedForUser = new SparseBooleanArray();
    
    private final H mHandler;
    
    private final int mDefaultStrongAuthFlags;
    
    private final boolean mDefaultIsNonStrongBiometricAllowed = true;
    
    public static final int STRONG_AUTH_REQUIRED_FOR_UNATTENDED_UPDATE = 64;
    
    public static final int STRONG_AUTH_REQUIRED_AFTER_USER_LOCKDOWN = 32;
    
    public static final int STRONG_AUTH_REQUIRED_AFTER_TIMEOUT = 16;
    
    public static final int STRONG_AUTH_REQUIRED_AFTER_NON_STRONG_BIOMETRICS_TIMEOUT = 128;
    
    public static final int STRONG_AUTH_REQUIRED_AFTER_LOCKOUT = 8;
    
    public static final int STRONG_AUTH_REQUIRED_AFTER_DPM_LOCK_NOW = 2;
    
    public static final int STRONG_AUTH_REQUIRED_AFTER_BOOT = 1;
    
    public static final int STRONG_AUTH_NOT_REQUIRED = 0;
    
    public static final int SOME_AUTH_REQUIRED_AFTER_USER_REQUEST = 4;
    
    private static final int ALLOWING_BIOMETRIC = 4;
    
    public StrongAuthTracker() {
      this((Context)this$0, Looper.myLooper());
    }
    
    public static int getDefaultFlags(Context param1Context) {
      return param1Context.getResources().getBoolean(17891541);
    }
    
    public int getStrongAuthForUser(int param1Int) {
      return this.mStrongAuthRequiredForUser.get(param1Int, this.mDefaultStrongAuthFlags);
    }
    
    public boolean isTrustAllowedForUser(int param1Int) {
      boolean bool;
      if (getStrongAuthForUser(param1Int) == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean isBiometricAllowedForUser(boolean param1Boolean, int param1Int) {
      boolean bool1;
      if ((getStrongAuthForUser(param1Int) & 0xFFFFFFFB) == 0) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      boolean bool2 = bool1;
      if (!param1Boolean)
        bool2 = bool1 & isNonStrongBiometricAllowedAfterIdleTimeout(param1Int); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isBiometricAllowedForUser allowed:");
      stringBuilder.append(bool2);
      stringBuilder.append(" isStrongBiometric:");
      stringBuilder.append(param1Boolean);
      stringBuilder.append(" userId:");
      stringBuilder.append(param1Int);
      Log.d("LockPatternUtils", stringBuilder.toString());
      return bool2;
    }
    
    public boolean isNonStrongBiometricAllowedAfterIdleTimeout(int param1Int) {
      return this.mIsNonStrongBiometricAllowedForUser.get(param1Int, true);
    }
    
    public void onStrongAuthRequiredChanged(int param1Int) {}
    
    public void onIsNonStrongBiometricAllowedChanged(int param1Int) {}
    
    protected void handleStrongAuthRequiredChanged(int param1Int1, int param1Int2) {
      int i = getStrongAuthForUser(param1Int2);
      if (param1Int1 != i) {
        if (param1Int1 == this.mDefaultStrongAuthFlags) {
          this.mStrongAuthRequiredForUser.delete(param1Int2);
        } else {
          this.mStrongAuthRequiredForUser.put(param1Int2, param1Int1);
        } 
        onStrongAuthRequiredChanged(param1Int2);
      } 
    }
    
    protected void handleIsNonStrongBiometricAllowedChanged(boolean param1Boolean, int param1Int) {
      boolean bool = isNonStrongBiometricAllowedAfterIdleTimeout(param1Int);
      if (param1Boolean != bool) {
        if (param1Boolean == true) {
          this.mIsNonStrongBiometricAllowedForUser.delete(param1Int);
        } else {
          this.mIsNonStrongBiometricAllowedForUser.put(param1Int, param1Boolean);
        } 
        onIsNonStrongBiometricAllowedChanged(param1Int);
      } 
    }
    
    public StrongAuthTracker(Looper param1Looper) {
      this.mStub = (IStrongAuthTracker.Stub)new Object(this);
      this.mHandler = new H(param1Looper);
      this.mDefaultStrongAuthFlags = getDefaultFlags((Context)this$0);
    }
    
    public IStrongAuthTracker.Stub getStub() {
      return this.mStub;
    }
    
    class H extends Handler {
      static final int MSG_ON_IS_NON_STRONG_BIOMETRIC_ALLOWED_CHANGED = 2;
      
      static final int MSG_ON_STRONG_AUTH_REQUIRED_CHANGED = 1;
      
      final LockPatternUtils.StrongAuthTracker this$0;
      
      public H(Looper param2Looper) {
        super(param2Looper);
      }
      
      public void handleMessage(Message param2Message) {
        int i = param2Message.what;
        boolean bool = true;
        if (i != 1) {
          if (i == 2) {
            LockPatternUtils.StrongAuthTracker strongAuthTracker = LockPatternUtils.StrongAuthTracker.this;
            if (param2Message.arg1 != 1)
              bool = false; 
            strongAuthTracker.handleIsNonStrongBiometricAllowedChanged(bool, param2Message.arg2);
          } 
        } else {
          LockPatternUtils.StrongAuthTracker.this.handleStrongAuthRequiredChanged(param2Message.arg1, param2Message.arg2);
        } 
      }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface StrongAuthFlags {}
  }
  
  private class H extends Handler {
    static final int MSG_ON_IS_NON_STRONG_BIOMETRIC_ALLOWED_CHANGED = 2;
    
    static final int MSG_ON_STRONG_AUTH_REQUIRED_CHANGED = 1;
    
    final LockPatternUtils.StrongAuthTracker this$0;
    
    public H(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      int i = param1Message.what;
      boolean bool = true;
      if (i != 1) {
        if (i == 2) {
          LockPatternUtils.StrongAuthTracker strongAuthTracker = this.this$0;
          if (param1Message.arg1 != 1)
            bool = false; 
          strongAuthTracker.handleIsNonStrongBiometricAllowedChanged(bool, param1Message.arg2);
        } 
      } else {
        this.this$0.handleStrongAuthRequiredChanged(param1Message.arg1, param1Message.arg2);
      } 
    }
  }
  
  public void enableSyntheticPassword() {
    setLong("enable-sp", 1L, 0);
  }
  
  public void disableSyntheticPassword() {
    setLong("enable-sp", 0L, 0);
  }
  
  public boolean isSyntheticPasswordEnabled() {
    boolean bool = false;
    if (getLong("enable-sp", 1L, 0) != 0L)
      bool = true; 
    return bool;
  }
  
  public boolean hasPendingEscrowToken(int paramInt) {
    try {
      return getLockSettings().hasPendingEscrowToken(paramInt);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
      return false;
    } 
  }
  
  public boolean hasSecureLockScreen() {
    if (this.mHasSecureLockScreen == null)
      try {
        this.mHasSecureLockScreen = Boolean.valueOf(getLockSettings().hasSecureLockScreen());
      } catch (RemoteException remoteException) {
        remoteException.rethrowFromSystemServer();
      }  
    return this.mHasSecureLockScreen.booleanValue();
  }
  
  public static boolean userOwnsFrpCredential(Context paramContext, UserInfo paramUserInfo) {
    boolean bool;
    if (paramUserInfo != null && paramUserInfo.isPrimary() && paramUserInfo.isAdmin() && frpCredentialEnabled(paramContext)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean frpCredentialEnabled(Context paramContext) {
    return paramContext.getResources().getBoolean(17891439);
  }
  
  public boolean tryUnlockWithCachedUnifiedChallenge(int paramInt) {
    try {
      return getLockSettings().tryUnlockWithCachedUnifiedChallenge(paramInt);
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public void removeCachedUnifiedChallenge(int paramInt) {
    try {
      getLockSettings().removeCachedUnifiedChallenge(paramInt);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean getTimeoutFlag(int paramInt) {
    return getBoolean("lockscreen.timeout_flag", false, paramInt);
  }
  
  public void setTimeoutFlag(boolean paramBoolean, int paramInt) {
    setBoolean("lockscreen.timeout_flag", paramBoolean, paramInt);
  }
  
  class CheckCredentialProgressCallback {
    public abstract void onEarlyMatched();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class CredentialType implements Annotation {}
  
  class EscrowTokenStateChangeCallback {
    public abstract void onEscrowTokenActivated(long param1Long, int param1Int);
  }
}
