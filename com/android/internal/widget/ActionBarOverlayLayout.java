package com.android.internal.widget;

import android.animation.Animator;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Property;
import android.util.SparseArray;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.Window;
import android.view.WindowInsets;
import android.widget.OverScroller;
import android.widget.Toolbar;
import com.android.internal.view.menu.MenuPresenter;

public class ActionBarOverlayLayout extends ViewGroup implements DecorContentParent {
  private int mWindowVisibility = 0;
  
  private final Rect mBaseContentInsets = new Rect();
  
  private final Rect mLastBaseContentInsets = new Rect();
  
  private final Rect mContentInsets = new Rect();
  
  private WindowInsets mBaseInnerInsets = WindowInsets.CONSUMED;
  
  private WindowInsets mLastBaseInnerInsets = WindowInsets.CONSUMED;
  
  private WindowInsets mInnerInsets = WindowInsets.CONSUMED;
  
  private WindowInsets mLastInnerInsets = WindowInsets.CONSUMED;
  
  private final int ACTION_BAR_ANIMATE_DELAY = 600;
  
  private final Animator.AnimatorListener mTopAnimatorListener = (Animator.AnimatorListener)new Object(this);
  
  private final Animator.AnimatorListener mBottomAnimatorListener = (Animator.AnimatorListener)new Object(this);
  
  private final Runnable mRemoveActionBarHideOffset = (Runnable)new Object(this);
  
  private final Runnable mAddActionBarHideOffset = (Runnable)new Object(this);
  
  public static final Property<ActionBarOverlayLayout, Integer> ACTION_BAR_HIDE_OFFSET = (Property<ActionBarOverlayLayout, Integer>)new Object("actionBarHideOffset");
  
  static final int[] ATTRS = new int[] { 16843499, 16842841 };
  
  private static final String TAG = "ActionBarOverlayLayout";
  
  private ActionBarContainer mActionBarBottom;
  
  private int mActionBarHeight;
  
  private ActionBarContainer mActionBarTop;
  
  private ActionBarVisibilityCallback mActionBarVisibilityCallback;
  
  private boolean mAnimatingForFling;
  
  private View mContent;
  
  private ViewPropertyAnimator mCurrentActionBarBottomAnimator;
  
  private ViewPropertyAnimator mCurrentActionBarTopAnimator;
  
  private DecorToolbar mDecorToolbar;
  
  private OverScroller mFlingEstimator;
  
  private boolean mHasNonEmbeddedTabs;
  
  private boolean mHideOnContentScroll;
  
  private int mHideOnContentScrollReference;
  
  private boolean mIgnoreWindowContentOverlay;
  
  private int mLastSystemUiVisibility;
  
  private boolean mOverlayMode;
  
  private Drawable mWindowContentOverlay;
  
  public ActionBarOverlayLayout(Context paramContext) {
    super(paramContext);
    init(paramContext);
  }
  
  public ActionBarOverlayLayout(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    init(paramContext);
  }
  
  private void init(Context paramContext) {
    TypedArray typedArray = getContext().getTheme().obtainStyledAttributes(ATTRS);
    boolean bool1 = false;
    this.mActionBarHeight = typedArray.getDimensionPixelSize(0, 0);
    Drawable drawable = typedArray.getDrawable(1);
    if (drawable == null) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    setWillNotDraw(bool2);
    typedArray.recycle();
    boolean bool2 = bool1;
    if ((paramContext.getApplicationInfo()).targetSdkVersion < 19)
      bool2 = true; 
    this.mIgnoreWindowContentOverlay = bool2;
    this.mFlingEstimator = new OverScroller(paramContext);
  }
  
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    haltActionBarHideOffsetAnimations();
  }
  
  public void setActionBarVisibilityCallback(ActionBarVisibilityCallback paramActionBarVisibilityCallback) {
    this.mActionBarVisibilityCallback = paramActionBarVisibilityCallback;
    if (getWindowToken() != null) {
      this.mActionBarVisibilityCallback.onWindowVisibilityChanged(this.mWindowVisibility);
      if (this.mLastSystemUiVisibility != 0) {
        int i = this.mLastSystemUiVisibility;
        onWindowSystemUiVisibilityChanged(i);
        requestApplyInsets();
      } 
    } 
  }
  
  public void setOverlayMode(boolean paramBoolean) {
    this.mOverlayMode = paramBoolean;
    if (paramBoolean && 
      (getContext().getApplicationInfo()).targetSdkVersion < 19) {
      paramBoolean = true;
    } else {
      paramBoolean = false;
    } 
    this.mIgnoreWindowContentOverlay = paramBoolean;
  }
  
  public boolean isInOverlayMode() {
    return this.mOverlayMode;
  }
  
  public void setHasNonEmbeddedTabs(boolean paramBoolean) {
    this.mHasNonEmbeddedTabs = paramBoolean;
  }
  
  public void setShowingForActionMode(boolean paramBoolean) {
    if (paramBoolean) {
      if ((getWindowSystemUiVisibility() & 0x500) == 1280)
        setDisabledSystemUiVisibility(4); 
    } else {
      setDisabledSystemUiVisibility(0);
    } 
  }
  
  protected void onConfigurationChanged(Configuration paramConfiguration) {
    super.onConfigurationChanged(paramConfiguration);
    init(getContext());
    requestApplyInsets();
  }
  
  public void onWindowSystemUiVisibilityChanged(int paramInt) {
    boolean bool2, bool3;
    super.onWindowSystemUiVisibilityChanged(paramInt);
    pullChildren();
    int i = this.mLastSystemUiVisibility;
    this.mLastSystemUiVisibility = paramInt;
    boolean bool1 = true;
    if ((paramInt & 0x4) == 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if ((paramInt & 0x100) != 0) {
      bool3 = true;
    } else {
      bool3 = false;
    } 
    ActionBarVisibilityCallback actionBarVisibilityCallback = this.mActionBarVisibilityCallback;
    if (actionBarVisibilityCallback != null) {
      if (bool3)
        bool1 = false; 
      actionBarVisibilityCallback.enableContentAnimations(bool1);
      if (bool2 || !bool3) {
        this.mActionBarVisibilityCallback.showForSystem();
      } else {
        this.mActionBarVisibilityCallback.hideForSystem();
      } 
    } 
    if (((i ^ paramInt) & 0x100) != 0 && 
      this.mActionBarVisibilityCallback != null)
      requestApplyInsets(); 
  }
  
  protected void onWindowVisibilityChanged(int paramInt) {
    super.onWindowVisibilityChanged(paramInt);
    this.mWindowVisibility = paramInt;
    ActionBarVisibilityCallback actionBarVisibilityCallback = this.mActionBarVisibilityCallback;
    if (actionBarVisibilityCallback != null)
      actionBarVisibilityCallback.onWindowVisibilityChanged(paramInt); 
  }
  
  private boolean applyInsets(View paramView, Rect paramRect, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4) {
    boolean bool1 = false;
    LayoutParams layoutParams = (LayoutParams)paramView.getLayoutParams();
    boolean bool2 = bool1;
    if (paramBoolean1) {
      bool2 = bool1;
      if (layoutParams.leftMargin != paramRect.left) {
        bool2 = true;
        layoutParams.leftMargin = paramRect.left;
      } 
    } 
    paramBoolean1 = bool2;
    if (paramBoolean2) {
      paramBoolean1 = bool2;
      if (layoutParams.topMargin != paramRect.top) {
        paramBoolean1 = true;
        layoutParams.topMargin = paramRect.top;
      } 
    } 
    paramBoolean2 = paramBoolean1;
    if (paramBoolean4) {
      paramBoolean2 = paramBoolean1;
      if (layoutParams.rightMargin != paramRect.right) {
        paramBoolean2 = true;
        layoutParams.rightMargin = paramRect.right;
      } 
    } 
    paramBoolean1 = paramBoolean2;
    if (paramBoolean3) {
      paramBoolean1 = paramBoolean2;
      if (layoutParams.bottomMargin != paramRect.bottom) {
        paramBoolean1 = true;
        layoutParams.bottomMargin = paramRect.bottom;
      } 
    } 
    return paramBoolean1;
  }
  
  public WindowInsets onApplyWindowInsets(WindowInsets paramWindowInsets) {
    pullChildren();
    getWindowSystemUiVisibility();
    Rect rect = paramWindowInsets.getSystemWindowInsetsAsRect();
    boolean bool1 = applyInsets((View)this.mActionBarTop, rect, true, true, false, true);
    ActionBarContainer actionBarContainer = this.mActionBarBottom;
    boolean bool2 = bool1;
    if (actionBarContainer != null)
      bool2 = bool1 | applyInsets((View)actionBarContainer, rect, true, false, true, true); 
    computeSystemWindowInsets(paramWindowInsets, this.mBaseContentInsets);
    this.mBaseInnerInsets = paramWindowInsets = paramWindowInsets.inset(this.mBaseContentInsets);
    if (!this.mLastBaseInnerInsets.equals(paramWindowInsets)) {
      bool2 = true;
      this.mLastBaseInnerInsets = this.mBaseInnerInsets;
    } 
    if (!this.mLastBaseContentInsets.equals(this.mBaseContentInsets)) {
      bool2 = true;
      this.mLastBaseContentInsets.set(this.mBaseContentInsets);
    } 
    if (bool2)
      requestLayout(); 
    return WindowInsets.CONSUMED;
  }
  
  protected LayoutParams generateDefaultLayoutParams() {
    return new LayoutParams(-1, -1);
  }
  
  public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet) {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    return (ViewGroup.LayoutParams)new LayoutParams(paramLayoutParams);
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    return paramLayoutParams instanceof LayoutParams;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    pullChildren();
    int i = 0;
    int j = 0;
    measureChildWithMargins((View)this.mActionBarTop, paramInt1, 0, paramInt2, 0);
    LayoutParams layoutParams2 = (LayoutParams)this.mActionBarTop.getLayoutParams();
    ActionBarContainer actionBarContainer2 = this.mActionBarTop;
    int k = actionBarContainer2.getMeasuredWidth(), m = layoutParams2.leftMargin, n = layoutParams2.rightMargin;
    int i1 = Math.max(0, k + m + n);
    actionBarContainer2 = this.mActionBarTop;
    m = actionBarContainer2.getMeasuredHeight();
    n = layoutParams2.topMargin;
    k = layoutParams2.bottomMargin;
    n = Math.max(0, m + n + k);
    k = combineMeasuredStates(0, this.mActionBarTop.getMeasuredState());
    ActionBarContainer actionBarContainer1 = this.mActionBarBottom;
    if (actionBarContainer1 != null) {
      measureChildWithMargins((View)actionBarContainer1, paramInt1, 0, paramInt2, 0);
      LayoutParams layoutParams = (LayoutParams)this.mActionBarBottom.getLayoutParams();
      actionBarContainer2 = this.mActionBarBottom;
      int i3 = actionBarContainer2.getMeasuredWidth();
      m = layoutParams.leftMargin;
      i2 = layoutParams.rightMargin;
      i1 = Math.max(i1, i3 + m + i2);
      actionBarContainer2 = this.mActionBarBottom;
      i2 = actionBarContainer2.getMeasuredHeight();
      i3 = layoutParams.topMargin;
      m = layoutParams.bottomMargin;
      n = Math.max(n, i2 + i3 + m);
      k = combineMeasuredStates(k, this.mActionBarBottom.getMeasuredState());
    } 
    m = getWindowSystemUiVisibility();
    if ((m & 0x100) != 0) {
      i2 = 1;
    } else {
      i2 = 0;
    } 
    if (i2) {
      m = this.mActionBarHeight;
      i = m;
      if (this.mHasNonEmbeddedTabs) {
        View view1 = this.mActionBarTop.getTabContainer();
        i = m;
        if (view1 != null)
          i = m + this.mActionBarHeight; 
      } 
    } else if (this.mActionBarTop.getVisibility() != 8) {
      i = this.mActionBarTop.getMeasuredHeight();
    } 
    m = j;
    if (this.mDecorToolbar.isSplit()) {
      actionBarContainer1 = this.mActionBarBottom;
      m = j;
      if (actionBarContainer1 != null)
        if (i2) {
          m = this.mActionBarHeight;
        } else {
          m = actionBarContainer1.getMeasuredHeight();
        }  
    } 
    this.mContentInsets.set(this.mBaseContentInsets);
    this.mInnerInsets = this.mBaseInnerInsets;
    if (!this.mOverlayMode && !i2) {
      Rect rect = this.mContentInsets;
      rect.top += i;
      rect = this.mContentInsets;
      rect.bottom += m;
      this.mInnerInsets = this.mInnerInsets.inset(0, i, 0, m);
    } else {
      WindowInsets windowInsets1 = this.mInnerInsets;
      i2 = windowInsets1.getSystemWindowInsetLeft();
      WindowInsets windowInsets2 = this.mInnerInsets;
      int i4 = windowInsets2.getSystemWindowInsetTop();
      windowInsets2 = this.mInnerInsets;
      int i3 = windowInsets2.getSystemWindowInsetRight();
      windowInsets2 = this.mInnerInsets;
      j = windowInsets2.getSystemWindowInsetBottom();
      this.mInnerInsets = windowInsets1.replaceSystemWindowInsets(i2, i4 + i, i3, j + m);
    } 
    applyInsets(this.mContent, this.mContentInsets, true, true, true, true);
    if (!this.mLastInnerInsets.equals(this.mInnerInsets)) {
      WindowInsets windowInsets = this.mInnerInsets;
      this.mContent.dispatchApplyWindowInsets(windowInsets);
    } 
    measureChildWithMargins(this.mContent, paramInt1, 0, paramInt2, 0);
    LayoutParams layoutParams1 = (LayoutParams)this.mContent.getLayoutParams();
    View view = this.mContent;
    int i2 = view.getMeasuredWidth();
    i = layoutParams1.leftMargin;
    m = layoutParams1.rightMargin;
    i = Math.max(i1, i2 + i + m);
    view = this.mContent;
    m = view.getMeasuredHeight();
    i2 = layoutParams1.topMargin;
    i1 = layoutParams1.bottomMargin;
    i1 = Math.max(n, m + i2 + i1);
    m = combineMeasuredStates(k, this.mContent.getMeasuredState());
    n = getPaddingLeft();
    k = getPaddingRight();
    j = getPaddingTop();
    i2 = getPaddingBottom();
    i1 = Math.max(i1 + j + i2, getSuggestedMinimumHeight());
    i = Math.max(i + n + k, getSuggestedMinimumWidth());
    paramInt1 = resolveSizeAndState(i, paramInt1, m);
    paramInt2 = resolveSizeAndState(i1, paramInt2, m << 16);
    setMeasuredDimension(paramInt1, paramInt2);
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    int i = getChildCount();
    int j = getPaddingLeft();
    getPaddingRight();
    int k = getPaddingTop();
    int m = getPaddingBottom();
    for (paramInt1 = 0; paramInt1 < i; paramInt1++) {
      View view = getChildAt(paramInt1);
      if (view.getVisibility() != 8) {
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        int n = view.getMeasuredWidth();
        int i1 = view.getMeasuredHeight();
        int i2 = layoutParams.leftMargin + j;
        if (view == this.mActionBarBottom) {
          paramInt3 = paramInt4 - paramInt2 - m - i1 - layoutParams.bottomMargin;
        } else {
          paramInt3 = layoutParams.topMargin + k;
        } 
        view.layout(i2, paramInt3, i2 + n, paramInt3 + i1);
      } 
    } 
  }
  
  public void draw(Canvas paramCanvas) {
    super.draw(paramCanvas);
    if (this.mWindowContentOverlay != null && !this.mIgnoreWindowContentOverlay) {
      byte b;
      if (this.mActionBarTop.getVisibility() == 0) {
        b = (int)(this.mActionBarTop.getBottom() + this.mActionBarTop.getTranslationY() + 0.5F);
      } else {
        b = 0;
      } 
      Drawable drawable1 = this.mWindowContentOverlay;
      int i = getWidth();
      Drawable drawable2 = this.mWindowContentOverlay;
      int j = drawable2.getIntrinsicHeight();
      drawable1.setBounds(0, b, i, j + b);
      this.mWindowContentOverlay.draw(paramCanvas);
    } 
  }
  
  public boolean shouldDelayChildPressedState() {
    return false;
  }
  
  public boolean onStartNestedScroll(View paramView1, View paramView2, int paramInt) {
    if ((paramInt & 0x2) == 0 || this.mActionBarTop.getVisibility() != 0)
      return false; 
    return this.mHideOnContentScroll;
  }
  
  public void onNestedScrollAccepted(View paramView1, View paramView2, int paramInt) {
    super.onNestedScrollAccepted(paramView1, paramView2, paramInt);
    this.mHideOnContentScrollReference = getActionBarHideOffset();
    haltActionBarHideOffsetAnimations();
    ActionBarVisibilityCallback actionBarVisibilityCallback = this.mActionBarVisibilityCallback;
    if (actionBarVisibilityCallback != null)
      actionBarVisibilityCallback.onContentScrollStarted(); 
  }
  
  public void onNestedScroll(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.mHideOnContentScrollReference = paramInt1 = this.mHideOnContentScrollReference + paramInt2;
    setActionBarHideOffset(paramInt1);
  }
  
  public void onStopNestedScroll(View paramView) {
    super.onStopNestedScroll(paramView);
    if (this.mHideOnContentScroll && !this.mAnimatingForFling)
      if (this.mHideOnContentScrollReference <= this.mActionBarTop.getHeight()) {
        postRemoveActionBarHideOffset();
      } else {
        postAddActionBarHideOffset();
      }  
    ActionBarVisibilityCallback actionBarVisibilityCallback = this.mActionBarVisibilityCallback;
    if (actionBarVisibilityCallback != null)
      actionBarVisibilityCallback.onContentScrollStopped(); 
  }
  
  public boolean onNestedFling(View paramView, float paramFloat1, float paramFloat2, boolean paramBoolean) {
    if (!this.mHideOnContentScroll || !paramBoolean)
      return false; 
    if (shouldHideActionBarOnFling(paramFloat1, paramFloat2)) {
      addActionBarHideOffset();
    } else {
      removeActionBarHideOffset();
    } 
    this.mAnimatingForFling = true;
    return true;
  }
  
  void pullChildren() {
    if (this.mContent == null) {
      this.mContent = findViewById(16908290);
      this.mActionBarTop = (ActionBarContainer)findViewById(16908710);
      this.mDecorToolbar = getDecorToolbar(findViewById(16908709));
      this.mActionBarBottom = (ActionBarContainer)findViewById(16909466);
    } 
  }
  
  private DecorToolbar getDecorToolbar(View paramView) {
    if (paramView instanceof DecorToolbar)
      return (DecorToolbar)paramView; 
    if (paramView instanceof Toolbar)
      return ((Toolbar)paramView).getWrapper(); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Can't make a decor toolbar out of ");
    stringBuilder.append(paramView.getClass().getSimpleName());
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public void setHideOnContentScrollEnabled(boolean paramBoolean) {
    if (paramBoolean != this.mHideOnContentScroll) {
      this.mHideOnContentScroll = paramBoolean;
      if (!paramBoolean) {
        stopNestedScroll();
        haltActionBarHideOffsetAnimations();
        setActionBarHideOffset(0);
      } 
    } 
  }
  
  public boolean isHideOnContentScrollEnabled() {
    return this.mHideOnContentScroll;
  }
  
  public int getActionBarHideOffset() {
    boolean bool;
    ActionBarContainer actionBarContainer = this.mActionBarTop;
    if (actionBarContainer != null) {
      bool = -((int)actionBarContainer.getTranslationY());
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setActionBarHideOffset(int paramInt) {
    haltActionBarHideOffsetAnimations();
    int i = this.mActionBarTop.getHeight();
    paramInt = Math.max(0, Math.min(paramInt, i));
    this.mActionBarTop.setTranslationY(-paramInt);
    ActionBarContainer actionBarContainer = this.mActionBarBottom;
    if (actionBarContainer != null && actionBarContainer.getVisibility() != 8) {
      float f = paramInt / i;
      paramInt = (int)(this.mActionBarBottom.getHeight() * f);
      this.mActionBarBottom.setTranslationY(paramInt);
    } 
  }
  
  private void haltActionBarHideOffsetAnimations() {
    removeCallbacks(this.mRemoveActionBarHideOffset);
    removeCallbacks(this.mAddActionBarHideOffset);
    ViewPropertyAnimator viewPropertyAnimator = this.mCurrentActionBarTopAnimator;
    if (viewPropertyAnimator != null)
      viewPropertyAnimator.cancel(); 
    viewPropertyAnimator = this.mCurrentActionBarBottomAnimator;
    if (viewPropertyAnimator != null)
      viewPropertyAnimator.cancel(); 
  }
  
  private void postRemoveActionBarHideOffset() {
    haltActionBarHideOffsetAnimations();
    postDelayed(this.mRemoveActionBarHideOffset, 600L);
  }
  
  private void postAddActionBarHideOffset() {
    haltActionBarHideOffsetAnimations();
    postDelayed(this.mAddActionBarHideOffset, 600L);
  }
  
  private void removeActionBarHideOffset() {
    haltActionBarHideOffsetAnimations();
    this.mRemoveActionBarHideOffset.run();
  }
  
  private void addActionBarHideOffset() {
    haltActionBarHideOffsetAnimations();
    this.mAddActionBarHideOffset.run();
  }
  
  private boolean shouldHideActionBarOnFling(float paramFloat1, float paramFloat2) {
    boolean bool;
    this.mFlingEstimator.fling(0, 0, 0, (int)paramFloat2, 0, 0, -2147483648, 2147483647);
    int i = this.mFlingEstimator.getFinalY();
    if (i > this.mActionBarTop.getHeight()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setWindowCallback(Window.Callback paramCallback) {
    pullChildren();
    this.mDecorToolbar.setWindowCallback(paramCallback);
  }
  
  public void setWindowTitle(CharSequence paramCharSequence) {
    pullChildren();
    this.mDecorToolbar.setWindowTitle(paramCharSequence);
  }
  
  public CharSequence getTitle() {
    pullChildren();
    return this.mDecorToolbar.getTitle();
  }
  
  public void initFeature(int paramInt) {
    pullChildren();
    if (paramInt != 2) {
      if (paramInt != 5) {
        if (paramInt == 9)
          setOverlayMode(true); 
      } else {
        this.mDecorToolbar.initIndeterminateProgress();
      } 
    } else {
      this.mDecorToolbar.initProgress();
    } 
  }
  
  public void setUiOptions(int paramInt) {
    boolean bool1;
    boolean bool = false;
    if ((paramInt & 0x1) != 0) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    if (bool1)
      bool = getContext().getResources().getBoolean(17891617); 
    if (bool) {
      pullChildren();
      if (this.mActionBarBottom != null && this.mDecorToolbar.canSplit()) {
        this.mDecorToolbar.setSplitView((ViewGroup)this.mActionBarBottom);
        this.mDecorToolbar.setSplitToolbar(bool);
        this.mDecorToolbar.setSplitWhenNarrow(bool1);
        ActionBarContextView actionBarContextView = (ActionBarContextView)findViewById(16908714);
        actionBarContextView.setSplitView((ViewGroup)this.mActionBarBottom);
        actionBarContextView.setSplitToolbar(bool);
        actionBarContextView.setSplitWhenNarrow(bool1);
      } else if (bool) {
        Log.e("ActionBarOverlayLayout", "Requested split action bar with incompatible window decor! Ignoring request.");
      } 
    } 
  }
  
  public boolean hasIcon() {
    pullChildren();
    return this.mDecorToolbar.hasIcon();
  }
  
  public boolean hasLogo() {
    pullChildren();
    return this.mDecorToolbar.hasLogo();
  }
  
  public void setIcon(int paramInt) {
    pullChildren();
    this.mDecorToolbar.setIcon(paramInt);
  }
  
  public void setIcon(Drawable paramDrawable) {
    pullChildren();
    this.mDecorToolbar.setIcon(paramDrawable);
  }
  
  public void setLogo(int paramInt) {
    pullChildren();
    this.mDecorToolbar.setLogo(paramInt);
  }
  
  public boolean canShowOverflowMenu() {
    pullChildren();
    return this.mDecorToolbar.canShowOverflowMenu();
  }
  
  public boolean isOverflowMenuShowing() {
    pullChildren();
    return this.mDecorToolbar.isOverflowMenuShowing();
  }
  
  public boolean isOverflowMenuShowPending() {
    pullChildren();
    return this.mDecorToolbar.isOverflowMenuShowPending();
  }
  
  public boolean showOverflowMenu() {
    pullChildren();
    return this.mDecorToolbar.showOverflowMenu();
  }
  
  public boolean hideOverflowMenu() {
    pullChildren();
    return this.mDecorToolbar.hideOverflowMenu();
  }
  
  public void setMenuPrepared() {
    pullChildren();
    this.mDecorToolbar.setMenuPrepared();
  }
  
  public void setMenu(Menu paramMenu, MenuPresenter.Callback paramCallback) {
    pullChildren();
    this.mDecorToolbar.setMenu(paramMenu, paramCallback);
  }
  
  public void saveToolbarHierarchyState(SparseArray<Parcelable> paramSparseArray) {
    pullChildren();
    this.mDecorToolbar.saveHierarchyState(paramSparseArray);
  }
  
  public void restoreToolbarHierarchyState(SparseArray<Parcelable> paramSparseArray) {
    pullChildren();
    this.mDecorToolbar.restoreHierarchyState(paramSparseArray);
  }
  
  public void dismissPopups() {
    pullChildren();
    this.mDecorToolbar.dismissPopupMenus();
  }
  
  class ActionBarVisibilityCallback {
    public abstract void enableContentAnimations(boolean param1Boolean);
    
    public abstract void hideForSystem();
    
    public abstract void onContentScrollStarted();
    
    public abstract void onContentScrollStopped();
    
    public abstract void onWindowVisibilityChanged(int param1Int);
    
    public abstract void showForSystem();
  }
  
  class LayoutParams extends ViewGroup.MarginLayoutParams {
    public LayoutParams(ActionBarOverlayLayout this$0, AttributeSet param1AttributeSet) {
      super((Context)this$0, param1AttributeSet);
    }
    
    public LayoutParams(ActionBarOverlayLayout this$0, int param1Int1) {
      super(this$0, param1Int1);
    }
    
    public LayoutParams(ActionBarOverlayLayout this$0) {
      super((ViewGroup.LayoutParams)this$0);
    }
    
    public LayoutParams(ActionBarOverlayLayout this$0) {
      super((ViewGroup.MarginLayoutParams)this$0);
    }
  }
}
