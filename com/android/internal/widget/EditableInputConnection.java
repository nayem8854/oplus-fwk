package com.android.internal.widget;

import android.os.Bundle;
import android.text.Editable;
import android.text.method.KeyListener;
import android.view.View;
import android.view.inputmethod.BaseInputConnection;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.CorrectionInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.widget.TextView;

public class EditableInputConnection extends BaseInputConnection {
  private static final boolean DEBUG = false;
  
  private static final String TAG = "EditableInputConnection";
  
  private int mBatchEditNesting;
  
  private final TextView mTextView;
  
  public EditableInputConnection(TextView paramTextView) {
    super((View)paramTextView, true);
    this.mTextView = paramTextView;
  }
  
  public Editable getEditable() {
    TextView textView = this.mTextView;
    if (textView != null)
      return textView.getEditableText(); 
    return null;
  }
  
  public boolean beginBatchEdit() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mBatchEditNesting : I
    //   6: iflt -> 30
    //   9: aload_0
    //   10: getfield mTextView : Landroid/widget/TextView;
    //   13: invokevirtual beginBatchEdit : ()V
    //   16: aload_0
    //   17: aload_0
    //   18: getfield mBatchEditNesting : I
    //   21: iconst_1
    //   22: iadd
    //   23: putfield mBatchEditNesting : I
    //   26: aload_0
    //   27: monitorexit
    //   28: iconst_1
    //   29: ireturn
    //   30: aload_0
    //   31: monitorexit
    //   32: iconst_0
    //   33: ireturn
    //   34: astore_1
    //   35: aload_0
    //   36: monitorexit
    //   37: aload_1
    //   38: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #60	-> 0
    //   #61	-> 2
    //   #62	-> 9
    //   #63	-> 16
    //   #64	-> 26
    //   #66	-> 30
    //   #67	-> 32
    //   #66	-> 34
    // Exception table:
    //   from	to	target	type
    //   2	9	34	finally
    //   9	16	34	finally
    //   16	26	34	finally
    //   26	28	34	finally
    //   30	32	34	finally
    //   35	37	34	finally
  }
  
  public boolean endBatchEdit() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mBatchEditNesting : I
    //   6: ifle -> 30
    //   9: aload_0
    //   10: getfield mTextView : Landroid/widget/TextView;
    //   13: invokevirtual endBatchEdit : ()V
    //   16: aload_0
    //   17: aload_0
    //   18: getfield mBatchEditNesting : I
    //   21: iconst_1
    //   22: isub
    //   23: putfield mBatchEditNesting : I
    //   26: aload_0
    //   27: monitorexit
    //   28: iconst_1
    //   29: ireturn
    //   30: aload_0
    //   31: monitorexit
    //   32: iconst_0
    //   33: ireturn
    //   34: astore_1
    //   35: aload_0
    //   36: monitorexit
    //   37: aload_1
    //   38: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #72	-> 0
    //   #73	-> 2
    //   #78	-> 9
    //   #79	-> 16
    //   #80	-> 26
    //   #82	-> 30
    //   #83	-> 32
    //   #82	-> 34
    // Exception table:
    //   from	to	target	type
    //   2	9	34	finally
    //   9	16	34	finally
    //   16	26	34	finally
    //   26	28	34	finally
    //   30	32	34	finally
    //   35	37	34	finally
  }
  
  public void closeConnection() {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial closeConnection : ()V
    //   4: aload_0
    //   5: monitorenter
    //   6: aload_0
    //   7: getfield mBatchEditNesting : I
    //   10: ifle -> 21
    //   13: aload_0
    //   14: invokevirtual endBatchEdit : ()Z
    //   17: pop
    //   18: goto -> 6
    //   21: aload_0
    //   22: iconst_m1
    //   23: putfield mBatchEditNesting : I
    //   26: aload_0
    //   27: monitorexit
    //   28: return
    //   29: astore_1
    //   30: aload_0
    //   31: monitorexit
    //   32: aload_1
    //   33: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #88	-> 0
    //   #89	-> 4
    //   #90	-> 6
    //   #91	-> 13
    //   #94	-> 21
    //   #95	-> 26
    //   #96	-> 28
    //   #95	-> 29
    // Exception table:
    //   from	to	target	type
    //   6	13	29	finally
    //   13	18	29	finally
    //   21	26	29	finally
    //   26	28	29	finally
    //   30	32	29	finally
  }
  
  public boolean clearMetaKeyStates(int paramInt) {
    Editable editable = getEditable();
    if (editable == null)
      return false; 
    KeyListener keyListener = this.mTextView.getKeyListener();
    if (keyListener != null)
      try {
        keyListener.clearMetaKeyState((View)this.mTextView, editable, paramInt);
      } catch (AbstractMethodError abstractMethodError) {} 
    return true;
  }
  
  public boolean commitCompletion(CompletionInfo paramCompletionInfo) {
    this.mTextView.beginBatchEdit();
    this.mTextView.onCommitCompletion(paramCompletionInfo);
    this.mTextView.endBatchEdit();
    return true;
  }
  
  public boolean commitCorrection(CorrectionInfo paramCorrectionInfo) {
    this.mTextView.beginBatchEdit();
    this.mTextView.onCommitCorrection(paramCorrectionInfo);
    this.mTextView.endBatchEdit();
    return true;
  }
  
  public boolean performEditorAction(int paramInt) {
    this.mTextView.onEditorAction(paramInt);
    return true;
  }
  
  public boolean performContextMenuAction(int paramInt) {
    this.mTextView.beginBatchEdit();
    this.mTextView.onTextContextMenuItem(paramInt);
    this.mTextView.endBatchEdit();
    return true;
  }
  
  public ExtractedText getExtractedText(ExtractedTextRequest paramExtractedTextRequest, int paramInt) {
    if (this.mTextView != null) {
      ExtractedText extractedText = new ExtractedText();
      if (this.mTextView.extractText(paramExtractedTextRequest, extractedText)) {
        if ((paramInt & 0x1) != 0)
          this.mTextView.setExtracting(paramExtractedTextRequest); 
        return extractedText;
      } 
    } 
    return null;
  }
  
  public boolean performPrivateCommand(String paramString, Bundle paramBundle) {
    this.mTextView.onPrivateIMECommand(paramString, paramBundle);
    return true;
  }
  
  public boolean commitText(CharSequence paramCharSequence, int paramInt) {
    TextView textView = this.mTextView;
    if (textView == null)
      return super.commitText(paramCharSequence, paramInt); 
    textView.resetErrorChangedFlag();
    boolean bool = super.commitText(paramCharSequence, paramInt);
    this.mTextView.hideErrorIfUnchanged();
    return bool;
  }
  
  public boolean requestCursorUpdates(int paramInt) {
    if ((paramInt & 0xFFFFFFFC) != 0)
      return false; 
    if (this.mIMM == null)
      return false; 
    this.mIMM.setUpdateCursorAnchorInfoMode(paramInt);
    if ((paramInt & 0x1) != 0) {
      TextView textView = this.mTextView;
      if (textView != null)
        if (!textView.isInLayout())
          this.mTextView.requestLayout();  
    } 
    return true;
  }
}
