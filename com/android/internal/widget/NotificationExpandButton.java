package com.android.internal.widget;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.RemotableViewMethod;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RemoteViews.RemoteView;

@RemoteView
public class NotificationExpandButton extends ImageView {
  private int mOriginalNotificationColor;
  
  public NotificationExpandButton(Context paramContext) {
    super(paramContext);
  }
  
  public NotificationExpandButton(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  public NotificationExpandButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  public NotificationExpandButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  public void getBoundsOnScreen(Rect paramRect, boolean paramBoolean) {
    super.getBoundsOnScreen(paramRect, paramBoolean);
    extendRectToMinTouchSize(paramRect);
  }
  
  @RemotableViewMethod
  public void setOriginalNotificationColor(int paramInt) {
    this.mOriginalNotificationColor = paramInt;
  }
  
  public int getOriginalNotificationColor() {
    return this.mOriginalNotificationColor;
  }
  
  private void extendRectToMinTouchSize(Rect paramRect) {
    int i = (int)((getResources().getDisplayMetrics()).density * 48.0F);
    paramRect.left = paramRect.centerX() - i / 2;
    paramRect.right = paramRect.left + i;
    paramRect.top = paramRect.centerY() - i / 2;
    paramRect.bottom = paramRect.top + i;
  }
  
  public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
    paramAccessibilityNodeInfo.setClassName(Button.class.getName());
  }
}
