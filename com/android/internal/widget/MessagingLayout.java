package com.android.internal.widget;

import android.app.Notification;
import android.app.Person;
import android.app.RemoteInputHistoryItem;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Icon;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.RemotableViewMethod;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Interpolator;
import android.view.animation.PathInterpolator;
import android.widget.FrameLayout;
import android.widget.RemoteViews.RemoteView;
import android.widget.TextView;
import com.android.internal.graphics.ColorUtils;
import com.android.internal.util.ContrastColorUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.regex.Pattern;

@RemoteView
public class MessagingLayout extends FrameLayout implements ImageMessageConsumer, IMessagingLayout {
  private static final Pattern IGNORABLE_CHAR_PATTERN = Pattern.compile("[\\p{C}\\p{Z}]");
  
  private static final Pattern SPECIAL_CHAR_PATTERN = Pattern.compile("[!@#$%&*()_+=|<>?{}\\[\\]~-]");
  
  static {
    REMOVE_MESSAGE = (Consumer<MessagingMessage>)_$$Lambda$DKD2sNhLnyRFoBkFvfwKyxoEx10.INSTANCE;
    LINEAR_OUT_SLOW_IN = (Interpolator)new PathInterpolator(0.0F, 0.0F, 0.2F, 1.0F);
    FAST_OUT_LINEAR_IN = (Interpolator)new PathInterpolator(0.4F, 0.0F, 1.0F, 1.0F);
    FAST_OUT_SLOW_IN = (Interpolator)new PathInterpolator(0.4F, 0.0F, 0.2F, 1.0F);
    MESSAGING_PROPERTY_ANIMATOR = new MessagingPropertyAnimator();
  }
  
  private List<MessagingMessage> mMessages = new ArrayList<>();
  
  private List<MessagingMessage> mHistoricMessages = new ArrayList<>();
  
  private ArrayList<MessagingGroup> mGroups = new ArrayList<>();
  
  private Paint mPaint = new Paint(1);
  
  private Paint mTextPaint = new Paint();
  
  private ArrayList<MessagingGroup> mAddedGroups = new ArrayList<>();
  
  private static final float COLOR_SHIFT_AMOUNT = 60.0F;
  
  public static final Interpolator FAST_OUT_LINEAR_IN;
  
  public static final Interpolator FAST_OUT_SLOW_IN;
  
  public static final Interpolator LINEAR_OUT_SLOW_IN;
  
  public static final View.OnLayoutChangeListener MESSAGING_PROPERTY_ANIMATOR;
  
  private static final Consumer<MessagingMessage> REMOVE_MESSAGE;
  
  private Icon mAvatarReplacement;
  
  private int mAvatarSize;
  
  private CharSequence mConversationTitle;
  
  private boolean mDisplayImagesAtEnd;
  
  private ImageResolver mImageResolver;
  
  private boolean mIsOneToOne;
  
  private int mLayoutColor;
  
  private int mMessageTextColor;
  
  private MessagingLinearLayout mMessagingLinearLayout;
  
  private CharSequence mNameReplacement;
  
  private int mSenderTextColor;
  
  private boolean mShowHistoricMessages;
  
  private TextView mTitleView;
  
  private Person mUser;
  
  public MessagingLayout(Context paramContext) {
    super(paramContext);
  }
  
  public MessagingLayout(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  public MessagingLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  public MessagingLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  protected void onFinishInflate() {
    super.onFinishInflate();
    this.mMessagingLinearLayout = (MessagingLinearLayout)findViewById(16909242);
    DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
    int i = Math.max(displayMetrics.widthPixels, displayMetrics.heightPixels);
    Rect rect = new Rect(0, 0, i, i);
    this.mMessagingLinearLayout.setClipBounds(rect);
    this.mTitleView = (TextView)findViewById(16908310);
    this.mAvatarSize = getResources().getDimensionPixelSize(17105314);
    this.mTextPaint.setTextAlign(Paint.Align.CENTER);
    this.mTextPaint.setAntiAlias(true);
  }
  
  @RemotableViewMethod
  public void setAvatarReplacement(Icon paramIcon) {
    this.mAvatarReplacement = paramIcon;
  }
  
  @RemotableViewMethod
  public void setNameReplacement(CharSequence paramCharSequence) {
    this.mNameReplacement = paramCharSequence;
  }
  
  @RemotableViewMethod
  public void setIsCollapsed(boolean paramBoolean) {
    this.mDisplayImagesAtEnd = paramBoolean;
  }
  
  @RemotableViewMethod
  public void setLargeIcon(Icon paramIcon) {}
  
  @RemotableViewMethod
  public void setConversationTitle(CharSequence paramCharSequence) {}
  
  @RemotableViewMethod
  public void setData(Bundle paramBundle) {
    Parcelable[] arrayOfParcelable1 = paramBundle.getParcelableArray("android.messages");
    List<Notification.MessagingStyle.Message> list1 = Notification.MessagingStyle.Message.getMessagesFromBundleArray(arrayOfParcelable1);
    Parcelable[] arrayOfParcelable2 = paramBundle.getParcelableArray("android.messages.historic");
    List<Notification.MessagingStyle.Message> list2 = Notification.MessagingStyle.Message.getMessagesFromBundleArray(arrayOfParcelable2);
    setUser((Person)paramBundle.getParcelable("android.messagingUser"));
    this.mConversationTitle = null;
    TextView textView = (TextView)findViewById(16909034);
    if (textView != null)
      this.mConversationTitle = textView.getText(); 
    RemoteInputHistoryItem[] arrayOfRemoteInputHistoryItem = (RemoteInputHistoryItem[])paramBundle.getParcelableArray("android.remoteInputHistoryItems");
    addRemoteInputHistoryToMessages(list1, arrayOfRemoteInputHistoryItem);
    boolean bool = paramBundle.getBoolean("android.remoteInputSpinner", false);
    bind(list1, list2, bool);
  }
  
  public void setImageResolver(ImageResolver paramImageResolver) {
    this.mImageResolver = paramImageResolver;
  }
  
  private void addRemoteInputHistoryToMessages(List<Notification.MessagingStyle.Message> paramList, RemoteInputHistoryItem[] paramArrayOfRemoteInputHistoryItem) {
    if (paramArrayOfRemoteInputHistoryItem == null || paramArrayOfRemoteInputHistoryItem.length == 0)
      return; 
    for (int i = paramArrayOfRemoteInputHistoryItem.length - 1; i >= 0; i--) {
      RemoteInputHistoryItem remoteInputHistoryItem = paramArrayOfRemoteInputHistoryItem[i];
      Notification.MessagingStyle.Message message = new Notification.MessagingStyle.Message(remoteInputHistoryItem.getText(), 0L, (Person)null, true);
      if (remoteInputHistoryItem.getUri() != null)
        message.setData(remoteInputHistoryItem.getMimeType(), remoteInputHistoryItem.getUri()); 
      paramList.add(message);
    } 
  }
  
  private void bind(List<Notification.MessagingStyle.Message> paramList1, List<Notification.MessagingStyle.Message> paramList2, boolean paramBoolean) {
    paramList2 = (List)createMessages(paramList2, true);
    paramList1 = (List)createMessages(paramList1, false);
    ArrayList<MessagingGroup> arrayList = new ArrayList<>(this.mGroups);
    addMessagesToGroups((List)paramList2, (List)paramList1, paramBoolean);
    removeGroups(arrayList);
    this.mMessages.forEach(REMOVE_MESSAGE);
    this.mHistoricMessages.forEach(REMOVE_MESSAGE);
    this.mMessages = (List)paramList1;
    this.mHistoricMessages = (List)paramList2;
    updateHistoricMessageVisibility();
    updateTitleAndNamesDisplay();
  }
  
  private void removeGroups(ArrayList<MessagingGroup> paramArrayList) {
    int i = paramArrayList.size();
    for (byte b = 0; b < i; b++) {
      MessagingGroup messagingGroup = paramArrayList.get(b);
      if (!this.mGroups.contains(messagingGroup)) {
        List<MessagingMessage> list = messagingGroup.getMessages();
        _$$Lambda$MessagingLayout$AR_BLYGwVbm8HbmaOhECHwnOBBg _$$Lambda$MessagingLayout$AR_BLYGwVbm8HbmaOhECHwnOBBg = new _$$Lambda$MessagingLayout$AR_BLYGwVbm8HbmaOhECHwnOBBg(this, messagingGroup);
        boolean bool = messagingGroup.isShown();
        this.mMessagingLinearLayout.removeView((View)messagingGroup);
        if (bool && !MessagingLinearLayout.isGone((View)messagingGroup)) {
          this.mMessagingLinearLayout.addTransientView((View)messagingGroup, 0);
          messagingGroup.removeGroupAnimated(_$$Lambda$MessagingLayout$AR_BLYGwVbm8HbmaOhECHwnOBBg);
        } else {
          _$$Lambda$MessagingLayout$AR_BLYGwVbm8HbmaOhECHwnOBBg.run();
        } 
        this.mMessages.removeAll(list);
        this.mHistoricMessages.removeAll(list);
      } 
    } 
  }
  
  private void updateTitleAndNamesDisplay() {
    ArrayMap arrayMap1 = new ArrayMap();
    ArrayMap arrayMap2 = new ArrayMap();
    byte b;
    for (b = 0; b < this.mGroups.size(); b++) {
      MessagingGroup messagingGroup = this.mGroups.get(b);
      CharSequence charSequence = messagingGroup.getSenderName();
      if (messagingGroup.needsGeneratedAvatar() && !TextUtils.isEmpty(charSequence))
        if (!arrayMap1.containsKey(charSequence)) {
          Pattern pattern = IGNORABLE_CHAR_PATTERN;
          CharSequence charSequence1 = pattern.matcher(charSequence).replaceAll("");
          char c = charSequence1.charAt(0);
          if (arrayMap2.containsKey(Character.valueOf(c))) {
            charSequence1 = (CharSequence)arrayMap2.get(Character.valueOf(c));
            if (charSequence1 != null) {
              arrayMap1.put(charSequence1, findNameSplit((String)charSequence1));
              arrayMap2.put(Character.valueOf(c), null);
            } 
            arrayMap1.put(charSequence, findNameSplit((String)charSequence));
          } else {
            arrayMap1.put(charSequence, Character.toString(c));
            arrayMap2.put(Character.valueOf(c), charSequence1);
          } 
        }  
    } 
    ArrayMap arrayMap3 = new ArrayMap();
    for (b = 0; b < this.mGroups.size(); b++) {
      boolean bool;
      MessagingGroup messagingGroup = this.mGroups.get(b);
      if (messagingGroup.getSender() == this.mUser) {
        bool = true;
      } else {
        bool = false;
      } 
      CharSequence charSequence = messagingGroup.getSenderName();
      if (messagingGroup.needsGeneratedAvatar() && !TextUtils.isEmpty(charSequence) && (!this.mIsOneToOne || this.mAvatarReplacement == null || bool)) {
        String str = (String)arrayMap1.get(charSequence);
        Icon icon = messagingGroup.getAvatarSymbolIfMatching(charSequence, str, this.mLayoutColor);
        if (icon != null)
          arrayMap3.put(charSequence, icon); 
      } 
    } 
    for (b = 0; b < this.mGroups.size(); b++) {
      MessagingGroup messagingGroup = this.mGroups.get(b);
      CharSequence charSequence = messagingGroup.getSenderName();
      if (messagingGroup.needsGeneratedAvatar() && !TextUtils.isEmpty(charSequence))
        if (this.mIsOneToOne && this.mAvatarReplacement != null && messagingGroup.getSender() != this.mUser) {
          messagingGroup.setAvatar(this.mAvatarReplacement);
        } else {
          Icon icon1 = (Icon)arrayMap3.get(charSequence);
          Icon icon2 = icon1;
          if (icon1 == null) {
            icon2 = createAvatarSymbol(charSequence, (String)arrayMap1.get(charSequence), this.mLayoutColor);
            arrayMap3.put(charSequence, icon2);
          } 
          messagingGroup.setCreatedAvatar(icon2, charSequence, (String)arrayMap1.get(charSequence), this.mLayoutColor);
        }  
    } 
  }
  
  public Icon createAvatarSymbol(CharSequence paramCharSequence, String paramString, int paramInt) {
    Paint paint;
    if (!paramString.isEmpty() && !TextUtils.isDigitsOnly(paramString)) {
      Pattern pattern = SPECIAL_CHAR_PATTERN;
      if (!pattern.matcher(paramString).find()) {
        float f2, f3;
        int i = this.mAvatarSize;
        Bitmap bitmap = Bitmap.createBitmap(i, i, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        float f1 = this.mAvatarSize / 2.0F;
        paramInt = findColor(paramCharSequence, paramInt);
        this.mPaint.setColor(paramInt);
        canvas.drawCircle(f1, f1, f1, this.mPaint);
        if (ColorUtils.calculateLuminance(paramInt) > 0.5D) {
          paramInt = 1;
        } else {
          paramInt = 0;
        } 
        paint = this.mTextPaint;
        if (paramInt != 0) {
          paramInt = -16777216;
        } else {
          paramInt = -1;
        } 
        paint.setColor(paramInt);
        paint = this.mTextPaint;
        if (paramString.length() == 1) {
          f2 = this.mAvatarSize;
          f3 = 0.5F;
        } else {
          f2 = this.mAvatarSize;
          f3 = 0.3F;
        } 
        paint.setTextSize(f2 * f3);
        paramInt = (int)(f1 - (this.mTextPaint.descent() + this.mTextPaint.ascent()) / 2.0F);
        canvas.drawText(paramString, f1, paramInt, this.mTextPaint);
        return Icon.createWithBitmap(bitmap);
      } 
    } 
    Icon icon = Icon.createWithResource(getContext(), 17303065);
    icon.setTint(findColor((CharSequence)paint, paramInt));
    return icon;
  }
  
  private int findColor(CharSequence paramCharSequence, int paramInt) {
    double d = ContrastColorUtil.calculateLuminance(paramInt);
    float f = (Math.abs(paramCharSequence.hashCode()) % 5) / 4.0F;
    f = (float)((f - 0.5F) + Math.max(0.30000001192092896D - d, 0.0D));
    f = (float)(f - Math.max(0.30000001192092896D - 1.0D - d, 0.0D));
    return ContrastColorUtil.getShiftedColor(paramInt, (int)(60.0F * f));
  }
  
  private String findNameSplit(String paramString) {
    String[] arrayOfString = paramString.split(" ");
    if (arrayOfString.length > 1) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(Character.toString(arrayOfString[0].charAt(0)));
      String str = arrayOfString[1];
      stringBuilder.append(Character.toString(str.charAt(0)));
      return stringBuilder.toString();
    } 
    return paramString.substring(0, 1);
  }
  
  @RemotableViewMethod
  public void setLayoutColor(int paramInt) {
    this.mLayoutColor = paramInt;
  }
  
  @RemotableViewMethod
  public void setIsOneToOne(boolean paramBoolean) {
    this.mIsOneToOne = paramBoolean;
  }
  
  @RemotableViewMethod
  public void setSenderTextColor(int paramInt) {
    this.mSenderTextColor = paramInt;
  }
  
  @RemotableViewMethod
  public void setNotificationBackgroundColor(int paramInt) {}
  
  @RemotableViewMethod
  public void setMessageTextColor(int paramInt) {
    this.mMessageTextColor = paramInt;
  }
  
  public void setUser(Person paramPerson) {
    this.mUser = paramPerson;
    if (paramPerson.getIcon() == null) {
      Icon icon = Icon.createWithResource(getContext(), 17303065);
      icon.setTint(this.mLayoutColor);
      this.mUser = this.mUser.toBuilder().setIcon(icon).build();
    } 
  }
  
  private void addMessagesToGroups(List<MessagingMessage> paramList1, List<MessagingMessage> paramList2, boolean paramBoolean) {
    ArrayList<List<MessagingMessage>> arrayList = new ArrayList();
    ArrayList<Person> arrayList1 = new ArrayList();
    findGroups(paramList1, paramList2, arrayList, arrayList1);
    createGroupViews(arrayList, arrayList1, paramBoolean);
  }
  
  private void createGroupViews(List<List<MessagingMessage>> paramList, List<Person> paramList1, boolean paramBoolean) {
    this.mGroups.clear();
    for (byte b = 0; b < paramList.size(); b++) {
      CharSequence charSequence;
      List<MessagingMessage> list = paramList.get(b);
      MessagingMessage messagingMessage = null;
      int i;
      boolean bool;
      for (i = list.size(), bool = true; --i >= 0; i--) {
        messagingMessage = list.get(i);
        messagingGroup1 = messagingMessage.getGroup();
        if (messagingGroup1 != null)
          break; 
      } 
      MessagingGroup messagingGroup2 = messagingGroup1;
      if (messagingGroup1 == null) {
        messagingGroup2 = MessagingGroup.createGroup(this.mMessagingLinearLayout);
        this.mAddedGroups.add(messagingGroup2);
      } 
      if (this.mDisplayImagesAtEnd) {
        i = 1;
      } else {
        i = 0;
      } 
      messagingGroup2.setImageDisplayLocation(i);
      messagingGroup2.setIsInConversation(false);
      messagingGroup2.setLayoutColor(this.mLayoutColor);
      messagingGroup2.setTextColors(this.mSenderTextColor, this.mMessageTextColor);
      Person person = paramList1.get(b);
      MessagingGroup messagingGroup3 = null;
      MessagingGroup messagingGroup1 = messagingGroup3;
      if (person != this.mUser) {
        messagingGroup1 = messagingGroup3;
        if (this.mNameReplacement != null)
          charSequence = this.mNameReplacement; 
      } 
      messagingGroup2.setSender(person, charSequence);
      if (b != paramList.size() - 1 || !paramBoolean)
        bool = false; 
      messagingGroup2.setSending(bool);
      this.mGroups.add(messagingGroup2);
      if (this.mMessagingLinearLayout.indexOfChild((View)messagingGroup2) != b) {
        this.mMessagingLinearLayout.removeView((View)messagingGroup2);
        this.mMessagingLinearLayout.addView((View)messagingGroup2, b);
      } 
      messagingGroup2.setMessages(list);
    } 
  }
  
  private void findGroups(List<MessagingMessage> paramList1, List<MessagingMessage> paramList2, List<List<MessagingMessage>> paramList, List<Person> paramList3) {
    CharSequence charSequence = null;
    Person person = null;
    int i = paramList1.size();
    for (byte b = 0; b < paramList2.size() + i; b++) {
      ArrayList<MessagingMessage> arrayList;
      MessagingMessage messagingMessage;
      boolean bool;
      CharSequence charSequence1;
      if (b < i) {
        messagingMessage = paramList1.get(b);
      } else {
        messagingMessage = paramList2.get(b - i);
      } 
      if (person == null) {
        bool = true;
      } else {
        bool = false;
      } 
      Person person1 = messagingMessage.getMessage().getSenderPerson();
      if (person1 == null) {
        charSequence1 = null;
      } else if (person1.getKey() == null) {
        charSequence1 = person1.getName();
      } else {
        charSequence1 = person1.getKey();
      } 
      boolean bool1 = TextUtils.equals(charSequence1, charSequence);
      if ((true ^ bool1 | bool) != 0) {
        ArrayList<MessagingMessage> arrayList1 = new ArrayList();
        paramList.add(arrayList1);
        person = person1;
        if (person1 == null)
          person = this.mUser; 
        paramList3.add(person);
        arrayList = arrayList1;
        CharSequence charSequence2 = charSequence1;
      } 
      arrayList.add(messagingMessage);
    } 
  }
  
  private List<MessagingMessage> createMessages(List<Notification.MessagingStyle.Message> paramList, boolean paramBoolean) {
    ArrayList<MessagingMessage> arrayList = new ArrayList();
    for (byte b = 0; b < paramList.size(); b++) {
      Notification.MessagingStyle.Message message = paramList.get(b);
      MessagingMessage messagingMessage1 = findAndRemoveMatchingMessage(message);
      MessagingMessage messagingMessage2 = messagingMessage1;
      if (messagingMessage1 == null)
        messagingMessage2 = MessagingMessage.createMessage(this, message, this.mImageResolver); 
      messagingMessage2.setIsHistoric(paramBoolean);
      arrayList.add(messagingMessage2);
    } 
    return arrayList;
  }
  
  private MessagingMessage findAndRemoveMatchingMessage(Notification.MessagingStyle.Message paramMessage) {
    byte b;
    for (b = 0; b < this.mMessages.size(); b++) {
      MessagingMessage messagingMessage = this.mMessages.get(b);
      if (messagingMessage.sameAs(paramMessage)) {
        this.mMessages.remove(b);
        return messagingMessage;
      } 
    } 
    for (b = 0; b < this.mHistoricMessages.size(); b++) {
      MessagingMessage messagingMessage = this.mHistoricMessages.get(b);
      if (messagingMessage.sameAs(paramMessage)) {
        this.mHistoricMessages.remove(b);
        return messagingMessage;
      } 
    } 
    return null;
  }
  
  public void showHistoricMessages(boolean paramBoolean) {
    this.mShowHistoricMessages = paramBoolean;
    updateHistoricMessageVisibility();
  }
  
  private void updateHistoricMessageVisibility() {
    int i = this.mHistoricMessages.size();
    byte b = 0;
    while (true) {
      byte b1 = 0;
      if (b < i) {
        MessagingMessage messagingMessage = this.mHistoricMessages.get(b);
        if (!this.mShowHistoricMessages)
          b1 = 8; 
        messagingMessage.setVisibility(b1);
        b++;
        continue;
      } 
      break;
    } 
    int j = this.mGroups.size();
    for (b = 0; b < j; b++) {
      MessagingGroup messagingGroup = this.mGroups.get(b);
      i = 0;
      List<MessagingMessage> list = messagingGroup.getMessages();
      int k = list.size();
      for (byte b1 = 0; b1 < k; b1++, i = m) {
        MessagingMessage messagingMessage = list.get(b1);
        int m = i;
        if (messagingMessage.getVisibility() != 8)
          m = i + 1; 
      } 
      if (i > 0 && messagingGroup.getVisibility() == 8) {
        messagingGroup.setVisibility(0);
      } else if (i == 0 && messagingGroup.getVisibility() != 8) {
        messagingGroup.setVisibility(8);
      } 
    } 
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    if (!this.mAddedGroups.isEmpty())
      getViewTreeObserver().addOnPreDrawListener((ViewTreeObserver.OnPreDrawListener)new Object(this)); 
  }
  
  public MessagingLinearLayout getMessagingLinearLayout() {
    return this.mMessagingLinearLayout;
  }
  
  public ArrayList<MessagingGroup> getMessagingGroups() {
    return this.mGroups;
  }
  
  public void setMessagingClippingDisabled(boolean paramBoolean) {}
}
