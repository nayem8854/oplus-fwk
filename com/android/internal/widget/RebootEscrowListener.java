package com.android.internal.widget;

public interface RebootEscrowListener {
  void onPreparedForReboot(boolean paramBoolean);
}
