package com.android.internal.widget;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.app.Notification;
import android.app.Person;
import android.app.RemoteInputHistoryItem;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.Icon;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.util.ArrayMap;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.RemotableViewMethod;
import android.view.TouchDelegate;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.ViewTreeObserver;
import android.view.animation.Interpolator;
import android.view.animation.PathInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RemoteViews.RemoteView;
import android.widget.TextView;
import com.android.internal.graphics.ColorUtils;
import com.android.internal.util.ContrastColorUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.regex.Pattern;

@RemoteView
public class ConversationLayout extends FrameLayout implements ImageMessageConsumer, IMessagingLayout {
  private static final Pattern IGNORABLE_CHAR_PATTERN = Pattern.compile("[\\p{C}\\p{Z}]");
  
  private static final Pattern SPECIAL_CHAR_PATTERN = Pattern.compile("[!@#$%&*()_+=|<>?{}\\[\\]~-]");
  
  static {
    REMOVE_MESSAGE = (Consumer<MessagingMessage>)_$$Lambda$DKD2sNhLnyRFoBkFvfwKyxoEx10.INSTANCE;
    LINEAR_OUT_SLOW_IN = (Interpolator)new PathInterpolator(0.0F, 0.0F, 0.2F, 1.0F);
    FAST_OUT_LINEAR_IN = (Interpolator)new PathInterpolator(0.4F, 0.0F, 1.0F, 1.0F);
    FAST_OUT_SLOW_IN = (Interpolator)new PathInterpolator(0.4F, 0.0F, 0.2F, 1.0F);
    OVERSHOOT = (Interpolator)new PathInterpolator(0.4F, 0.0F, 0.2F, 1.4F);
    MESSAGING_PROPERTY_ANIMATOR = new MessagingPropertyAnimator();
  }
  
  private List<MessagingMessage> mMessages = new ArrayList<>();
  
  private List<MessagingMessage> mHistoricMessages = new ArrayList<>();
  
  private ArrayList<MessagingGroup> mGroups = new ArrayList<>();
  
  private Paint mPaint = new Paint(1);
  
  private Paint mTextPaint = new Paint();
  
  private ArrayList<MessagingGroup> mAddedGroups = new ArrayList<>();
  
  private boolean mExpandable = true;
  
  private Rect mAppOpsTouchRect = new Rect();
  
  private static final float COLOR_SHIFT_AMOUNT = 60.0F;
  
  public static final Interpolator FAST_OUT_LINEAR_IN;
  
  public static final Interpolator FAST_OUT_SLOW_IN;
  
  public static final int IMPORTANCE_ANIM_GROW_DURATION = 250;
  
  public static final int IMPORTANCE_ANIM_SHRINK_DELAY = 25;
  
  public static final int IMPORTANCE_ANIM_SHRINK_DURATION = 200;
  
  public static final Interpolator LINEAR_OUT_SLOW_IN;
  
  public static final View.OnLayoutChangeListener MESSAGING_PROPERTY_ANIMATOR;
  
  public static final Interpolator OVERSHOOT;
  
  private static final Consumer<MessagingMessage> REMOVE_MESSAGE;
  
  private ViewGroup mActions;
  
  private ObservableTextView mAppName;
  
  private View mAppNameDivider;
  
  private boolean mAppNameGone;
  
  private ViewGroup mAppOps;
  
  private Icon mAvatarReplacement;
  
  private int mAvatarSize;
  
  private int mBadgedSideMargins;
  
  private View mContentContainer;
  
  private int mContentMarginEnd;
  
  private int mConversationAvatarSize;
  
  private int mConversationAvatarSizeExpanded;
  
  private int mConversationContentStart;
  
  private View mConversationFacePile;
  
  private View mConversationHeader;
  
  private Icon mConversationIcon;
  
  private View mConversationIconBadge;
  
  private CachingIconView mConversationIconBadgeBg;
  
  private View mConversationIconContainer;
  
  private int mConversationIconTopPadding;
  
  private int mConversationIconTopPaddingExpandedGroup;
  
  private CachingIconView mConversationIconView;
  
  private TextView mConversationText;
  
  private CharSequence mConversationTitle;
  
  private NotificationExpandButton mExpandButton;
  
  private ViewGroup mExpandButtonAndContentContainer;
  
  private View mExpandButtonContainer;
  
  private int mExpandButtonExpandedTopMargin;
  
  private View mExpandButtonInnerContainer;
  
  private int mExpandedGroupMessagePadding;
  
  private int mExpandedGroupSideMargin;
  
  private int mExpandedGroupSideMarginFacePile;
  
  private int mFacePileAvatarSize;
  
  private int mFacePileAvatarSizeExpandedGroup;
  
  private int mFacePileProtectionWidth;
  
  private int mFacePileProtectionWidthExpanded;
  
  private CharSequence mFallbackChatName;
  
  private CharSequence mFallbackGroupChatName;
  
  private CachingIconView mIcon;
  
  private MessagingLinearLayout mImageMessageContainer;
  
  private ImageResolver mImageResolver;
  
  private CachingIconView mImportanceRingView;
  
  private boolean mImportantConversation;
  
  private int mInternalButtonPadding;
  
  private boolean mIsCollapsed;
  
  private boolean mIsOneToOne;
  
  private Icon mLargeIcon;
  
  private int mLayoutColor;
  
  private int mMessageTextColor;
  
  private Rect mMessagingClipRect;
  
  private MessagingLinearLayout mMessagingLinearLayout;
  
  private float mMinTouchSize;
  
  private CharSequence mNameReplacement;
  
  private int mNotificationBackgroundColor;
  
  private int mNotificationHeaderExpandedPadding;
  
  private int mSenderTextColor;
  
  private Icon mShortcutIcon;
  
  private boolean mShowHistoricMessages;
  
  private TextView mUnreadBadge;
  
  private Person mUser;
  
  public ConversationLayout(Context paramContext) {
    super(paramContext);
  }
  
  public ConversationLayout(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  public ConversationLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  public ConversationLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  protected void onFinishInflate() {
    super.onFinishInflate();
    this.mMessagingLinearLayout = (MessagingLinearLayout)findViewById(16909242);
    this.mActions = (ViewGroup)findViewById(16908721);
    this.mImageMessageContainer = (MessagingLinearLayout)findViewById(16908893);
    DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
    int i = Math.max(displayMetrics.widthPixels, displayMetrics.heightPixels);
    boolean bool = false;
    this.mMessagingClipRect = new Rect(0, 0, i, i);
    setMessagingClippingDisabled(false);
    this.mAvatarSize = getResources().getDimensionPixelSize(17105314);
    this.mTextPaint.setTextAlign(Paint.Align.CENTER);
    this.mTextPaint.setAntiAlias(true);
    this.mConversationIconView = (CachingIconView)findViewById(16908888);
    this.mConversationIconContainer = findViewById(16908892);
    this.mIcon = (CachingIconView)findViewById(16908294);
    this.mAppOps = (ViewGroup)findViewById(16908762);
    this.mMinTouchSize = (getResources().getDisplayMetrics()).density * 48.0F;
    this.mImportanceRingView = (CachingIconView)findViewById(16908891);
    this.mConversationIconBadge = findViewById(16908889);
    this.mConversationIconBadgeBg = (CachingIconView)findViewById(16908890);
    this.mIcon.setOnVisibilityChangedListener(new _$$Lambda$ConversationLayout$LxuUzMJyRDJje4QWVuhQgBm11eI(this));
    this.mIcon.setOnForceHiddenChangedListener(new _$$Lambda$ConversationLayout$qnQHJPfQFRvYMeBH9I284djOMBs(this));
    this.mConversationIconView.setOnForceHiddenChangedListener(new _$$Lambda$ConversationLayout$F3j28C0_vEQLokveY2vXwnPWvEQ(this));
    this.mConversationText = (TextView)findViewById(16908894);
    this.mExpandButtonContainer = findViewById(16908952);
    this.mConversationHeader = findViewById(16908887);
    this.mContentContainer = findViewById(16909224);
    this.mExpandButtonAndContentContainer = (ViewGroup)findViewById(16908951);
    this.mExpandButtonInnerContainer = findViewById(16908953);
    this.mExpandButton = (NotificationExpandButton)findViewById(16908950);
    this.mExpandButtonExpandedTopMargin = getResources().getDimensionPixelSize(17105108);
    this.mNotificationHeaderExpandedPadding = getResources().getDimensionPixelSize(17105113);
    this.mContentMarginEnd = getResources().getDimensionPixelSize(17105345);
    this.mBadgedSideMargins = getResources().getDimensionPixelSize(17105103);
    this.mConversationAvatarSize = getResources().getDimensionPixelSize(17105101);
    this.mConversationAvatarSizeExpanded = getResources().getDimensionPixelSize(17105102);
    this.mConversationIconTopPaddingExpandedGroup = getResources().getDimensionPixelSize(17105115);
    this.mConversationIconTopPadding = getResources().getDimensionPixelSize(17105114);
    this.mExpandedGroupMessagePadding = getResources().getDimensionPixelSize(17105183);
    this.mExpandedGroupSideMargin = getResources().getDimensionPixelSize(17105104);
    this.mExpandedGroupSideMarginFacePile = getResources().getDimensionPixelSize(17105105);
    this.mConversationFacePile = findViewById(16908883);
    this.mFacePileAvatarSize = getResources().getDimensionPixelSize(17105109);
    this.mFacePileAvatarSizeExpandedGroup = getResources().getDimensionPixelSize(17105110);
    this.mFacePileProtectionWidth = getResources().getDimensionPixelSize(17105111);
    this.mFacePileProtectionWidthExpanded = getResources().getDimensionPixelSize(17105112);
    this.mFallbackChatName = getResources().getString(17040004);
    this.mFallbackGroupChatName = getResources().getString(17040003);
    this.mAppName = (ObservableTextView)findViewById(16908761);
    this.mAppNameDivider = findViewById(16908760);
    if (this.mAppName.getVisibility() == 8)
      bool = true; 
    this.mAppNameGone = bool;
    this.mAppName.setOnVisibilityChangedListener(new _$$Lambda$ConversationLayout$odXxvCbEEcPFJRZsPLvTczTOMN0(this));
    this.mUnreadBadge = (TextView)findViewById(16908895);
    this.mConversationContentStart = getResources().getDimensionPixelSize(17105106);
    i = getResources().getDimensionPixelSize(17104966);
    this.mInternalButtonPadding = i + getResources().getDimensionPixelSize(17104963);
  }
  
  private void animateViewForceHidden(CachingIconView paramCachingIconView, boolean paramBoolean) {
    boolean bool;
    Interpolator interpolator;
    if (paramCachingIconView.willBeForceHidden() || paramCachingIconView.isForceHidden()) {
      bool = true;
    } else {
      bool = false;
    } 
    if (paramBoolean == bool)
      return; 
    paramCachingIconView.animate().cancel();
    paramCachingIconView.setWillBeForceHidden(paramBoolean);
    ViewPropertyAnimator viewPropertyAnimator2 = paramCachingIconView.animate();
    float f1 = 0.5F, f2 = 1.0F;
    if (paramBoolean) {
      f3 = 0.5F;
    } else {
      f3 = 1.0F;
    } 
    viewPropertyAnimator2 = viewPropertyAnimator2.scaleX(f3);
    if (paramBoolean) {
      f3 = f1;
    } else {
      f3 = 1.0F;
    } 
    viewPropertyAnimator2 = viewPropertyAnimator2.scaleY(f3);
    float f3 = f2;
    if (paramBoolean)
      f3 = 0.0F; 
    ViewPropertyAnimator viewPropertyAnimator3 = viewPropertyAnimator2.alpha(f3);
    if (paramBoolean) {
      interpolator = MessagingPropertyAnimator.ALPHA_OUT;
    } else {
      interpolator = MessagingPropertyAnimator.ALPHA_IN;
    } 
    ViewPropertyAnimator viewPropertyAnimator1 = viewPropertyAnimator3.setInterpolator((TimeInterpolator)interpolator);
    viewPropertyAnimator1.setDuration(160L);
    if (paramCachingIconView.getVisibility() != 0) {
      paramCachingIconView.setForceHidden(paramBoolean);
    } else {
      paramCachingIconView.animate().withEndAction(new _$$Lambda$ConversationLayout$BlJ8h2OIIfhaE4yvFY3mmLcanoA(paramCachingIconView, paramBoolean));
    } 
    paramCachingIconView.animate().start();
  }
  
  @RemotableViewMethod
  public void setAvatarReplacement(Icon paramIcon) {
    this.mAvatarReplacement = paramIcon;
  }
  
  @RemotableViewMethod
  public void setNameReplacement(CharSequence paramCharSequence) {
    this.mNameReplacement = paramCharSequence;
  }
  
  @RemotableViewMethod
  public void setIsImportantConversation(boolean paramBoolean) {
    setIsImportantConversation(paramBoolean, false);
  }
  
  public void setIsImportantConversation(boolean paramBoolean1, boolean paramBoolean2) {
    this.mImportantConversation = paramBoolean1;
    CachingIconView cachingIconView = this.mImportanceRingView;
    int i = 8;
    if (paramBoolean1 && this.mIcon.getVisibility() != 8)
      i = 0; 
    cachingIconView.setVisibility(i);
    if (paramBoolean2 && paramBoolean1) {
      GradientDrawable gradientDrawable2 = (GradientDrawable)this.mImportanceRingView.getDrawable();
      gradientDrawable2.mutate();
      GradientDrawable gradientDrawable1 = (GradientDrawable)this.mConversationIconBadgeBg.getDrawable();
      gradientDrawable1.mutate();
      Resources resources = getResources();
      int j = resources.getColor(17170731);
      resources = getResources();
      i = resources.getDimensionPixelSize(17105230);
      resources = getResources();
      int k = resources.getDimensionPixelSize(17105228);
      int m = getResources().getDimensionPixelSize(17105229);
      int n = m - i * 2;
      resources = getResources();
      m = resources.getDimensionPixelSize(17105116);
      _$$Lambda$ConversationLayout$91w9mxxWjJCx1_VJg9iaby_YQ30 _$$Lambda$ConversationLayout$91w9mxxWjJCx1_VJg9iaby_YQ30 = new _$$Lambda$ConversationLayout$91w9mxxWjJCx1_VJg9iaby_YQ30(this, gradientDrawable2, j, n);
      ValueAnimator valueAnimator1 = ValueAnimator.ofFloat(new float[] { 0.0F, k });
      valueAnimator1.setInterpolator((TimeInterpolator)LINEAR_OUT_SLOW_IN);
      valueAnimator1.setDuration(250L);
      valueAnimator1.addUpdateListener(_$$Lambda$ConversationLayout$91w9mxxWjJCx1_VJg9iaby_YQ30);
      float f1 = k, f2 = i;
      ValueAnimator valueAnimator2 = ValueAnimator.ofFloat(new float[] { f1, f2 });
      valueAnimator2.setDuration(200L);
      valueAnimator2.setStartDelay(25L);
      valueAnimator2.setInterpolator((TimeInterpolator)OVERSHOOT);
      valueAnimator2.addUpdateListener(_$$Lambda$ConversationLayout$91w9mxxWjJCx1_VJg9iaby_YQ30);
      valueAnimator2.addListener((Animator.AnimatorListener)new Object(this, gradientDrawable1, n, m));
      AnimatorSet animatorSet = new AnimatorSet();
      animatorSet.playSequentially(new Animator[] { (Animator)valueAnimator1, (Animator)valueAnimator2 });
      animatorSet.start();
    } 
  }
  
  public boolean isImportantConversation() {
    return this.mImportantConversation;
  }
  
  @RemotableViewMethod
  public void setIsCollapsed(boolean paramBoolean) {
    int i;
    this.mIsCollapsed = paramBoolean;
    MessagingLinearLayout messagingLinearLayout = this.mMessagingLinearLayout;
    if (paramBoolean) {
      i = 1;
    } else {
      i = Integer.MAX_VALUE;
    } 
    messagingLinearLayout.setMaxDisplayedLines(i);
    updateExpandButton();
    updateContentEndPaddings();
  }
  
  @RemotableViewMethod
  public void setData(Bundle paramBundle) {
    Parcelable[] arrayOfParcelable1 = paramBundle.getParcelableArray("android.messages");
    List<Notification.MessagingStyle.Message> list1 = Notification.MessagingStyle.Message.getMessagesFromBundleArray(arrayOfParcelable1);
    Parcelable[] arrayOfParcelable2 = paramBundle.getParcelableArray("android.messages.historic");
    List<Notification.MessagingStyle.Message> list2 = Notification.MessagingStyle.Message.getMessagesFromBundleArray(arrayOfParcelable2);
    setUser((Person)paramBundle.getParcelable("android.messagingUser"));
    RemoteInputHistoryItem[] arrayOfRemoteInputHistoryItem = (RemoteInputHistoryItem[])paramBundle.getParcelableArray("android.remoteInputHistoryItems");
    addRemoteInputHistoryToMessages(list1, arrayOfRemoteInputHistoryItem);
    boolean bool = paramBundle.getBoolean("android.remoteInputSpinner", false);
    bind(list1, list2, bool);
    int i = paramBundle.getInt("android.conversationUnreadMessageCount");
    setUnreadCount(i);
  }
  
  public void setImageResolver(ImageResolver paramImageResolver) {
    this.mImageResolver = paramImageResolver;
  }
  
  public void setUnreadCount(int paramInt) {
    boolean bool2;
    byte b;
    boolean bool = this.mIsCollapsed;
    boolean bool1 = true;
    if (bool && paramInt > 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    TextView textView = this.mUnreadBadge;
    if (bool2) {
      b = 0;
    } else {
      b = 8;
    } 
    textView.setVisibility(b);
    if (bool2) {
      String str;
      if (paramInt >= 100) {
        str = getResources().getString(17041424, new Object[] { Integer.valueOf(99) });
      } else {
        str = String.format(Locale.getDefault(), "%d", new Object[] { Integer.valueOf(paramInt) });
      } 
      this.mUnreadBadge.setText(str);
      this.mUnreadBadge.setBackgroundTintList(ColorStateList.valueOf(this.mLayoutColor));
      if (ColorUtils.calculateLuminance(this.mLayoutColor) > 0.5D) {
        paramInt = bool1;
      } else {
        paramInt = 0;
      } 
      TextView textView1 = this.mUnreadBadge;
      if (paramInt != 0) {
        paramInt = -16777216;
      } else {
        paramInt = -1;
      } 
      textView1.setTextColor(paramInt);
    } 
  }
  
  private void addRemoteInputHistoryToMessages(List<Notification.MessagingStyle.Message> paramList, RemoteInputHistoryItem[] paramArrayOfRemoteInputHistoryItem) {
    if (paramArrayOfRemoteInputHistoryItem == null || paramArrayOfRemoteInputHistoryItem.length == 0)
      return; 
    for (int i = paramArrayOfRemoteInputHistoryItem.length - 1; i >= 0; i--) {
      RemoteInputHistoryItem remoteInputHistoryItem = paramArrayOfRemoteInputHistoryItem[i];
      Notification.MessagingStyle.Message message = new Notification.MessagingStyle.Message(remoteInputHistoryItem.getText(), 0L, (Person)null, true);
      if (remoteInputHistoryItem.getUri() != null)
        message.setData(remoteInputHistoryItem.getMimeType(), remoteInputHistoryItem.getUri()); 
      paramList.add(message);
    } 
  }
  
  private void bind(List<Notification.MessagingStyle.Message> paramList1, List<Notification.MessagingStyle.Message> paramList2, boolean paramBoolean) {
    paramList2 = (List)createMessages(paramList2, true);
    List<MessagingMessage> list = createMessages(paramList1, false);
    ArrayList<MessagingGroup> arrayList = new ArrayList<>(this.mGroups);
    ArrayList<List<MessagingMessage>> arrayList1 = new ArrayList();
    paramList1 = new ArrayList<>();
    findGroups((List)paramList2, list, arrayList1, (List)paramList1);
    createGroupViews(arrayList1, (List)paramList1, paramBoolean);
    removeGroups(arrayList);
    this.mMessages.forEach(REMOVE_MESSAGE);
    this.mHistoricMessages.forEach(REMOVE_MESSAGE);
    this.mMessages = list;
    this.mHistoricMessages = (List)paramList2;
    updateHistoricMessageVisibility();
    updateTitleAndNamesDisplay();
    updateConversationLayout();
  }
  
  private void updateConversationLayout() {
    CharSequence charSequence1 = this.mConversationTitle;
    this.mConversationIcon = this.mShortcutIcon;
    CharSequence charSequence2 = charSequence1;
    if (this.mIsOneToOne) {
      CharSequence charSequence = getKey(this.mUser);
      int j = this.mGroups.size() - 1;
      while (true) {
        charSequence2 = charSequence1;
        if (j >= 0) {
          MessagingGroup messagingGroup = this.mGroups.get(j);
          Person person = messagingGroup.getSender();
          if ((person != null && !TextUtils.equals(charSequence, getKey(person))) || j == 0) {
            charSequence = charSequence1;
            if (TextUtils.isEmpty(charSequence1))
              charSequence = messagingGroup.getSenderName(); 
            CharSequence charSequence4 = charSequence;
            if (this.mConversationIcon == null) {
              Icon icon1 = messagingGroup.getAvatarIcon();
              Icon icon2 = icon1;
              if (icon1 == null)
                icon2 = createAvatarSymbol(charSequence, "", this.mLayoutColor); 
              this.mConversationIcon = icon2;
              charSequence2 = charSequence;
            } 
            break;
          } 
          j--;
          continue;
        } 
        break;
      } 
    } 
    if (this.mConversationIcon == null)
      this.mConversationIcon = this.mLargeIcon; 
    if (this.mIsOneToOne || this.mConversationIcon != null) {
      this.mConversationIconView.setVisibility(0);
      this.mConversationFacePile.setVisibility(8);
      this.mConversationIconView.setImageIcon(this.mConversationIcon);
    } else {
      this.mConversationIconView.setVisibility(8);
      this.mConversationFacePile.setVisibility(0);
      this.mConversationFacePile = findViewById(16908883);
      bindFacePile();
    } 
    CharSequence charSequence3 = charSequence2;
    if (TextUtils.isEmpty(charSequence2))
      if (this.mIsOneToOne) {
        charSequence3 = this.mFallbackChatName;
      } else {
        charSequence3 = this.mFallbackGroupChatName;
      }  
    this.mConversationText.setText(charSequence3);
    for (int i = this.mGroups.size() - 1; i >= 0; i--) {
      boolean bool;
      MessagingGroup messagingGroup = this.mGroups.get(i);
      charSequence1 = messagingGroup.getSenderName();
      if (this.mIsOneToOne && 
        TextUtils.equals(charSequence3, charSequence1)) {
        bool = true;
      } else {
        bool = false;
      } 
      messagingGroup.setCanHideSenderIfFirst(bool);
    } 
    updateAppName();
    updateIconPositionAndSize();
    updateImageMessages();
    updatePaddingsBasedOnContentAvailability();
    updateActionListPadding();
    updateAppNameDividerVisibility();
  }
  
  private void updateActionListPadding() {
    ViewGroup viewGroup = this.mActions;
    if (viewGroup == null)
      return; 
    View view = viewGroup.getChildAt(0);
    if (view != null) {
      int i = this.mConversationContentStart;
      ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams)view.getLayoutParams();
      int j = marginLayoutParams.getMarginStart();
      int k = this.mInternalButtonPadding;
      ViewGroup viewGroup1 = this.mActions;
      int m = viewGroup1.getPaddingTop();
      ViewGroup viewGroup2 = this.mActions;
      int n = viewGroup2.getPaddingEnd();
      viewGroup2 = this.mActions;
      int i1 = viewGroup2.getPaddingBottom();
      viewGroup1.setPaddingRelative(i - j - k, m, n, i1);
    } 
  }
  
  private void updateImageMessages() {
    View view2;
    ArrayList<MessagingGroup> arrayList1 = null;
    ArrayList<MessagingGroup> arrayList2 = arrayList1;
    if (this.mIsCollapsed) {
      arrayList2 = arrayList1;
      if (this.mGroups.size() > 0) {
        arrayList2 = this.mGroups;
        MessagingGroup messagingGroup = arrayList2.get(arrayList2.size() - 1);
        MessagingImageMessage messagingImageMessage = messagingGroup.getIsolatedMessage();
        ArrayList<MessagingGroup> arrayList = arrayList1;
        if (messagingImageMessage != null)
          view2 = messagingImageMessage.getView(); 
      } 
    } 
    MessagingLinearLayout messagingLinearLayout2 = this.mImageMessageContainer;
    byte b = 0;
    View view1 = messagingLinearLayout2.getChildAt(0);
    if (view1 != view2) {
      this.mImageMessageContainer.removeView(view1);
      if (view2 != null)
        this.mImageMessageContainer.addView(view2); 
    } 
    MessagingLinearLayout messagingLinearLayout1 = this.mImageMessageContainer;
    if (view2 == null)
      b = 8; 
    messagingLinearLayout1.setVisibility(b);
  }
  
  public void bindFacePile(ImageView paramImageView1, ImageView paramImageView2, ImageView paramImageView3) {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: invokespecial applyNotificationBackgroundColor : (Landroid/widget/ImageView;)V
    //   5: aconst_null
    //   6: astore #4
    //   8: aconst_null
    //   9: astore #5
    //   11: aconst_null
    //   12: astore_1
    //   13: aload_0
    //   14: aload_0
    //   15: getfield mUser : Landroid/app/Person;
    //   18: invokespecial getKey : (Landroid/app/Person;)Ljava/lang/CharSequence;
    //   21: astore #6
    //   23: aload_0
    //   24: getfield mGroups : Ljava/util/ArrayList;
    //   27: invokevirtual size : ()I
    //   30: iconst_1
    //   31: isub
    //   32: istore #7
    //   34: aload #4
    //   36: astore #8
    //   38: iload #7
    //   40: iflt -> 199
    //   43: aload_0
    //   44: getfield mGroups : Ljava/util/ArrayList;
    //   47: iload #7
    //   49: invokevirtual get : (I)Ljava/lang/Object;
    //   52: checkcast com/android/internal/widget/MessagingGroup
    //   55: astore #9
    //   57: aload #9
    //   59: invokevirtual getSender : ()Landroid/app/Person;
    //   62: astore #10
    //   64: iconst_0
    //   65: istore #11
    //   67: aload #10
    //   69: ifnull -> 92
    //   72: aload #6
    //   74: aload_0
    //   75: aload #10
    //   77: invokespecial getKey : (Landroid/app/Person;)Ljava/lang/CharSequence;
    //   80: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   83: ifne -> 92
    //   86: iconst_1
    //   87: istore #12
    //   89: goto -> 95
    //   92: iconst_0
    //   93: istore #12
    //   95: aload #10
    //   97: ifnull -> 120
    //   100: aload #5
    //   102: aload_0
    //   103: aload #10
    //   105: invokespecial getKey : (Landroid/app/Person;)Ljava/lang/CharSequence;
    //   108: invokestatic equals : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   111: ifne -> 120
    //   114: iconst_1
    //   115: istore #11
    //   117: goto -> 120
    //   120: iload #12
    //   122: ifeq -> 130
    //   125: iload #11
    //   127: ifne -> 154
    //   130: aload #5
    //   132: astore #8
    //   134: aload_1
    //   135: astore #13
    //   137: iload #7
    //   139: ifne -> 186
    //   142: aload #5
    //   144: astore #8
    //   146: aload_1
    //   147: astore #13
    //   149: aload #5
    //   151: ifnonnull -> 186
    //   154: aload_1
    //   155: ifnonnull -> 176
    //   158: aload #9
    //   160: invokevirtual getAvatarIcon : ()Landroid/graphics/drawable/Icon;
    //   163: astore #13
    //   165: aload_0
    //   166: aload #10
    //   168: invokespecial getKey : (Landroid/app/Person;)Ljava/lang/CharSequence;
    //   171: astore #8
    //   173: goto -> 186
    //   176: aload #9
    //   178: invokevirtual getAvatarIcon : ()Landroid/graphics/drawable/Icon;
    //   181: astore #8
    //   183: goto -> 199
    //   186: iinc #7, -1
    //   189: aload #8
    //   191: astore #5
    //   193: aload #13
    //   195: astore_1
    //   196: goto -> 34
    //   199: aload_1
    //   200: astore #5
    //   202: aload_1
    //   203: ifnonnull -> 222
    //   206: aload_0
    //   207: ldc_w ' '
    //   210: ldc_w ''
    //   213: aload_0
    //   214: getfield mLayoutColor : I
    //   217: invokespecial createAvatarSymbol : (Ljava/lang/CharSequence;Ljava/lang/String;I)Landroid/graphics/drawable/Icon;
    //   220: astore #5
    //   222: aload_2
    //   223: aload #5
    //   225: invokevirtual setImageIcon : (Landroid/graphics/drawable/Icon;)V
    //   228: aload #8
    //   230: astore_1
    //   231: aload #8
    //   233: ifnonnull -> 251
    //   236: aload_0
    //   237: ldc_w ''
    //   240: ldc_w ''
    //   243: aload_0
    //   244: getfield mLayoutColor : I
    //   247: invokespecial createAvatarSymbol : (Ljava/lang/CharSequence;Ljava/lang/String;I)Landroid/graphics/drawable/Icon;
    //   250: astore_1
    //   251: aload_3
    //   252: aload_1
    //   253: invokevirtual setImageIcon : (Landroid/graphics/drawable/Icon;)V
    //   256: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #650	-> 0
    //   #652	-> 5
    //   #653	-> 8
    //   #654	-> 11
    //   #655	-> 13
    //   #656	-> 23
    //   #657	-> 43
    //   #658	-> 57
    //   #659	-> 64
    //   #660	-> 72
    //   #661	-> 95
    //   #662	-> 100
    //   #663	-> 120
    //   #665	-> 154
    //   #666	-> 158
    //   #667	-> 165
    //   #669	-> 176
    //   #670	-> 183
    //   #656	-> 186
    //   #674	-> 199
    //   #675	-> 206
    //   #677	-> 222
    //   #678	-> 228
    //   #679	-> 236
    //   #681	-> 251
    //   #682	-> 256
  }
  
  private void bindFacePile() {
    int i, j, k;
    ImageView imageView1 = (ImageView)this.mConversationFacePile.findViewById(16908885);
    ImageView imageView2 = (ImageView)this.mConversationFacePile.findViewById(16908884);
    ImageView imageView3 = (ImageView)this.mConversationFacePile.findViewById(16908886);
    bindFacePile(imageView1, imageView2, imageView3);
    if (this.mIsCollapsed) {
      i = this.mConversationAvatarSize;
      j = this.mFacePileAvatarSize;
      k = this.mFacePileProtectionWidth * 2 + j;
    } else {
      i = this.mConversationAvatarSizeExpanded;
      j = this.mFacePileAvatarSizeExpandedGroup;
      k = this.mFacePileProtectionWidthExpanded * 2 + j;
    } 
    FrameLayout.LayoutParams layoutParams3 = (FrameLayout.LayoutParams)this.mConversationIconView.getLayoutParams();
    layoutParams3.width = i;
    layoutParams3.height = i;
    this.mConversationFacePile.setLayoutParams((ViewGroup.LayoutParams)layoutParams3);
    layoutParams3 = (FrameLayout.LayoutParams)imageView2.getLayoutParams();
    layoutParams3.width = j;
    layoutParams3.height = j;
    imageView2.setLayoutParams((ViewGroup.LayoutParams)layoutParams3);
    FrameLayout.LayoutParams layoutParams1 = (FrameLayout.LayoutParams)imageView3.getLayoutParams();
    layoutParams1.width = j;
    layoutParams1.height = j;
    imageView3.setLayoutParams((ViewGroup.LayoutParams)layoutParams1);
    FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams)imageView1.getLayoutParams();
    layoutParams2.width = k;
    layoutParams2.height = k;
    imageView1.setLayoutParams((ViewGroup.LayoutParams)layoutParams2);
  }
  
  private void updateAppName() {
    boolean bool;
    ObservableTextView observableTextView = this.mAppName;
    if (this.mIsCollapsed) {
      bool = true;
    } else {
      bool = false;
    } 
    observableTextView.setVisibility(bool);
  }
  
  public boolean shouldHideAppName() {
    return this.mIsCollapsed;
  }
  
  private void updateIconPositionAndSize() {
    int i, j;
    if (this.mIsOneToOne || this.mIsCollapsed) {
      j = this.mBadgedSideMargins;
      i = this.mConversationAvatarSize;
    } else {
      if (this.mConversationFacePile.getVisibility() == 0) {
        i = this.mExpandedGroupSideMarginFacePile;
      } else {
        i = this.mExpandedGroupSideMargin;
      } 
      int k = this.mConversationAvatarSizeExpanded;
      j = i;
      i = k;
    } 
    View view = this.mConversationIconBadge;
    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams)view.getLayoutParams();
    layoutParams.topMargin = j;
    layoutParams.setMarginStart(j);
    this.mConversationIconBadge.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
    if (this.mConversationIconView.getVisibility() == 0) {
      layoutParams = (FrameLayout.LayoutParams)this.mConversationIconView.getLayoutParams();
      layoutParams.width = i;
      layoutParams.height = i;
      this.mConversationIconView.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
    } 
  }
  
  private void updatePaddingsBasedOnContentAvailability() {
    int i;
    if (this.mIsOneToOne || this.mIsCollapsed) {
      i = 0;
    } else {
      i = this.mExpandedGroupMessagePadding;
    } 
    if (this.mIsOneToOne || this.mIsCollapsed) {
      j = this.mConversationIconTopPadding;
    } else {
      j = this.mConversationIconTopPaddingExpandedGroup;
    } 
    View view1 = this.mConversationIconContainer;
    int k = view1.getPaddingStart();
    View view2 = this.mConversationIconContainer;
    int m = view2.getPaddingEnd();
    view2 = this.mConversationIconContainer;
    int n = view2.getPaddingBottom();
    view1.setPaddingRelative(k, j, m, n);
    MessagingLinearLayout messagingLinearLayout1 = this.mMessagingLinearLayout;
    m = messagingLinearLayout1.getPaddingStart();
    MessagingLinearLayout messagingLinearLayout2 = this.mMessagingLinearLayout;
    int j = messagingLinearLayout2.getPaddingEnd();
    messagingLinearLayout2 = this.mMessagingLinearLayout;
    k = messagingLinearLayout2.getPaddingBottom();
    messagingLinearLayout1.setPaddingRelative(m, i, j, k);
  }
  
  @RemotableViewMethod
  public void setLargeIcon(Icon paramIcon) {
    this.mLargeIcon = paramIcon;
  }
  
  @RemotableViewMethod
  public void setShortcutIcon(Icon paramIcon) {
    this.mShortcutIcon = paramIcon;
  }
  
  @RemotableViewMethod
  public void setConversationTitle(CharSequence paramCharSequence) {
    if (paramCharSequence != null) {
      paramCharSequence = paramCharSequence.toString();
    } else {
      paramCharSequence = null;
    } 
    this.mConversationTitle = paramCharSequence;
  }
  
  public CharSequence getConversationTitle() {
    return this.mConversationText.getText();
  }
  
  private void removeGroups(ArrayList<MessagingGroup> paramArrayList) {
    int i = paramArrayList.size();
    for (byte b = 0; b < i; b++) {
      MessagingGroup messagingGroup = paramArrayList.get(b);
      if (!this.mGroups.contains(messagingGroup)) {
        List<MessagingMessage> list = messagingGroup.getMessages();
        _$$Lambda$ConversationLayout$U2MLx3G6SszITnyPuess44Eb_pM _$$Lambda$ConversationLayout$U2MLx3G6SszITnyPuess44Eb_pM = new _$$Lambda$ConversationLayout$U2MLx3G6SszITnyPuess44Eb_pM(this, messagingGroup);
        boolean bool = messagingGroup.isShown();
        this.mMessagingLinearLayout.removeView((View)messagingGroup);
        if (bool && !MessagingLinearLayout.isGone((View)messagingGroup)) {
          this.mMessagingLinearLayout.addTransientView((View)messagingGroup, 0);
          messagingGroup.removeGroupAnimated(_$$Lambda$ConversationLayout$U2MLx3G6SszITnyPuess44Eb_pM);
        } else {
          _$$Lambda$ConversationLayout$U2MLx3G6SszITnyPuess44Eb_pM.run();
        } 
        this.mMessages.removeAll(list);
        this.mHistoricMessages.removeAll(list);
      } 
    } 
  }
  
  private void updateTitleAndNamesDisplay() {
    ArrayMap arrayMap1 = new ArrayMap();
    ArrayMap arrayMap2 = new ArrayMap();
    byte b;
    for (b = 0; b < this.mGroups.size(); b++) {
      MessagingGroup messagingGroup = this.mGroups.get(b);
      CharSequence charSequence = messagingGroup.getSenderName();
      if (messagingGroup.needsGeneratedAvatar() && !TextUtils.isEmpty(charSequence))
        if (!arrayMap1.containsKey(charSequence)) {
          Pattern pattern = IGNORABLE_CHAR_PATTERN;
          CharSequence charSequence1 = pattern.matcher(charSequence).replaceAll("");
          char c = charSequence1.charAt(0);
          if (arrayMap2.containsKey(Character.valueOf(c))) {
            charSequence1 = (CharSequence)arrayMap2.get(Character.valueOf(c));
            if (charSequence1 != null) {
              arrayMap1.put(charSequence1, findNameSplit((String)charSequence1));
              arrayMap2.put(Character.valueOf(c), null);
            } 
            arrayMap1.put(charSequence, findNameSplit((String)charSequence));
          } else {
            arrayMap1.put(charSequence, Character.toString(c));
            arrayMap2.put(Character.valueOf(c), charSequence1);
          } 
        }  
    } 
    ArrayMap arrayMap3 = new ArrayMap();
    for (b = 0; b < this.mGroups.size(); b++) {
      boolean bool;
      MessagingGroup messagingGroup = this.mGroups.get(b);
      if (messagingGroup.getSender() == this.mUser) {
        bool = true;
      } else {
        bool = false;
      } 
      CharSequence charSequence = messagingGroup.getSenderName();
      if (messagingGroup.needsGeneratedAvatar() && !TextUtils.isEmpty(charSequence) && (!this.mIsOneToOne || this.mAvatarReplacement == null || bool)) {
        String str = (String)arrayMap1.get(charSequence);
        Icon icon = messagingGroup.getAvatarSymbolIfMatching(charSequence, str, this.mLayoutColor);
        if (icon != null)
          arrayMap3.put(charSequence, icon); 
      } 
    } 
    for (b = 0; b < this.mGroups.size(); b++) {
      MessagingGroup messagingGroup = this.mGroups.get(b);
      CharSequence charSequence = messagingGroup.getSenderName();
      if (messagingGroup.needsGeneratedAvatar() && !TextUtils.isEmpty(charSequence))
        if (this.mIsOneToOne && this.mAvatarReplacement != null && messagingGroup.getSender() != this.mUser) {
          messagingGroup.setAvatar(this.mAvatarReplacement);
        } else {
          Icon icon2 = (Icon)arrayMap3.get(charSequence);
          Icon icon1 = icon2;
          if (icon2 == null) {
            icon1 = createAvatarSymbol(charSequence, (String)arrayMap1.get(charSequence), this.mLayoutColor);
            arrayMap3.put(charSequence, icon1);
          } 
          messagingGroup.setCreatedAvatar(icon1, charSequence, (String)arrayMap1.get(charSequence), this.mLayoutColor);
        }  
    } 
  }
  
  private Icon createAvatarSymbol(CharSequence paramCharSequence, String paramString, int paramInt) {
    Paint paint;
    if (!paramString.isEmpty() && !TextUtils.isDigitsOnly(paramString)) {
      Pattern pattern = SPECIAL_CHAR_PATTERN;
      if (!pattern.matcher(paramString).find()) {
        float f2, f3;
        int i = this.mAvatarSize;
        Bitmap bitmap = Bitmap.createBitmap(i, i, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        float f1 = this.mAvatarSize / 2.0F;
        paramInt = findColor(paramCharSequence, paramInt);
        this.mPaint.setColor(paramInt);
        canvas.drawCircle(f1, f1, f1, this.mPaint);
        if (ColorUtils.calculateLuminance(paramInt) > 0.5D) {
          paramInt = 1;
        } else {
          paramInt = 0;
        } 
        paint = this.mTextPaint;
        if (paramInt != 0) {
          paramInt = -16777216;
        } else {
          paramInt = -1;
        } 
        paint.setColor(paramInt);
        paint = this.mTextPaint;
        if (paramString.length() == 1) {
          f2 = this.mAvatarSize;
          f3 = 0.5F;
        } else {
          f2 = this.mAvatarSize;
          f3 = 0.3F;
        } 
        paint.setTextSize(f2 * f3);
        paramInt = (int)(f1 - (this.mTextPaint.descent() + this.mTextPaint.ascent()) / 2.0F);
        canvas.drawText(paramString, f1, paramInt, this.mTextPaint);
        return Icon.createWithBitmap(bitmap);
      } 
    } 
    Icon icon = Icon.createWithResource(getContext(), 17303065);
    icon.setTint(findColor((CharSequence)paint, paramInt));
    return icon;
  }
  
  private int findColor(CharSequence paramCharSequence, int paramInt) {
    double d = ContrastColorUtil.calculateLuminance(paramInt);
    float f = (Math.abs(paramCharSequence.hashCode()) % 5) / 4.0F;
    f = (float)((f - 0.5F) + Math.max(0.30000001192092896D - d, 0.0D));
    f = (float)(f - Math.max(0.30000001192092896D - 1.0D - d, 0.0D));
    return ContrastColorUtil.getShiftedColor(paramInt, (int)(60.0F * f));
  }
  
  private String findNameSplit(String paramString) {
    String[] arrayOfString = paramString.split(" ");
    if (arrayOfString.length > 1) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(Character.toString(arrayOfString[0].charAt(0)));
      String str = arrayOfString[1];
      stringBuilder.append(Character.toString(str.charAt(0)));
      return stringBuilder.toString();
    } 
    return paramString.substring(0, 1);
  }
  
  @RemotableViewMethod
  public void setLayoutColor(int paramInt) {
    this.mLayoutColor = paramInt;
  }
  
  @RemotableViewMethod
  public void setIsOneToOne(boolean paramBoolean) {
    this.mIsOneToOne = paramBoolean;
  }
  
  @RemotableViewMethod
  public void setSenderTextColor(int paramInt) {
    this.mSenderTextColor = paramInt;
    this.mConversationText.setTextColor(paramInt);
  }
  
  @RemotableViewMethod
  public void setNotificationBackgroundColor(int paramInt) {
    this.mNotificationBackgroundColor = paramInt;
    applyNotificationBackgroundColor(this.mConversationIconBadgeBg);
  }
  
  private void applyNotificationBackgroundColor(ImageView paramImageView) {
    paramImageView.setImageTintList(ColorStateList.valueOf(this.mNotificationBackgroundColor));
  }
  
  @RemotableViewMethod
  public void setMessageTextColor(int paramInt) {
    this.mMessageTextColor = paramInt;
  }
  
  private void setUser(Person paramPerson) {
    this.mUser = paramPerson;
    if (paramPerson.getIcon() == null) {
      Icon icon = Icon.createWithResource(getContext(), 17303065);
      icon.setTint(this.mLayoutColor);
      this.mUser = this.mUser.toBuilder().setIcon(icon).build();
    } 
  }
  
  private void createGroupViews(List<List<MessagingMessage>> paramList, List<Person> paramList1, boolean paramBoolean) {
    this.mGroups.clear();
    for (byte b = 0; b < paramList.size(); b++) {
      CharSequence charSequence;
      boolean bool2;
      List<MessagingMessage> list = paramList.get(b);
      MessagingMessage messagingMessage = null;
      int i;
      boolean bool1;
      for (i = list.size(), bool1 = true; --i >= 0; i--) {
        messagingMessage = list.get(i);
        messagingGroup1 = messagingMessage.getGroup();
        if (messagingGroup1 != null)
          break; 
      } 
      MessagingGroup messagingGroup2 = messagingGroup1;
      if (messagingGroup1 == null) {
        messagingGroup2 = MessagingGroup.createGroup(this.mMessagingLinearLayout);
        this.mAddedGroups.add(messagingGroup2);
      } 
      if (this.mIsCollapsed) {
        i = 2;
      } else {
        i = 0;
      } 
      messagingGroup2.setImageDisplayLocation(i);
      messagingGroup2.setIsInConversation(true);
      messagingGroup2.setLayoutColor(this.mLayoutColor);
      messagingGroup2.setTextColors(this.mSenderTextColor, this.mMessageTextColor);
      Person person = paramList1.get(b);
      MessagingGroup messagingGroup3 = null;
      MessagingGroup messagingGroup1 = messagingGroup3;
      if (person != this.mUser) {
        messagingGroup1 = messagingGroup3;
        if (this.mNameReplacement != null)
          charSequence = this.mNameReplacement; 
      } 
      if (!this.mIsOneToOne && !this.mIsCollapsed) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      messagingGroup2.setShowingAvatar(bool2);
      messagingGroup2.setSingleLine(this.mIsCollapsed);
      messagingGroup2.setSender(person, charSequence);
      if (b == paramList.size() - 1 && paramBoolean) {
        bool2 = bool1;
      } else {
        bool2 = false;
      } 
      messagingGroup2.setSending(bool2);
      this.mGroups.add(messagingGroup2);
      if (this.mMessagingLinearLayout.indexOfChild((View)messagingGroup2) != b) {
        this.mMessagingLinearLayout.removeView((View)messagingGroup2);
        this.mMessagingLinearLayout.addView((View)messagingGroup2, b);
      } 
      messagingGroup2.setMessages(list);
    } 
  }
  
  private void findGroups(List<MessagingMessage> paramList1, List<MessagingMessage> paramList2, List<List<MessagingMessage>> paramList, List<Person> paramList3) {
    CharSequence charSequence = null;
    Person person = null;
    int i = paramList1.size();
    for (byte b = 0; b < paramList2.size() + i; b++) {
      ArrayList<MessagingMessage> arrayList;
      MessagingMessage messagingMessage;
      boolean bool;
      if (b < i) {
        messagingMessage = paramList1.get(b);
      } else {
        messagingMessage = paramList2.get(b - i);
      } 
      if (person == null) {
        bool = true;
      } else {
        bool = false;
      } 
      Person person1 = messagingMessage.getMessage().getSenderPerson();
      CharSequence charSequence1 = getKey(person1);
      boolean bool1 = TextUtils.equals(charSequence1, charSequence);
      if ((true ^ bool1 | bool) != 0) {
        ArrayList<MessagingMessage> arrayList1 = new ArrayList();
        paramList.add(arrayList1);
        if (person1 == null) {
          person = this.mUser;
        } else {
          person = person1.toBuilder().setName(Objects.toString(person1.getName())).build();
        } 
        paramList3.add(person);
        charSequence = charSequence1;
        arrayList = arrayList1;
      } 
      arrayList.add(messagingMessage);
    } 
  }
  
  private CharSequence getKey(Person paramPerson) {
    CharSequence charSequence;
    if (paramPerson == null) {
      paramPerson = null;
    } else if (paramPerson.getKey() == null) {
      charSequence = paramPerson.getName();
    } else {
      charSequence = charSequence.getKey();
    } 
    return charSequence;
  }
  
  private List<MessagingMessage> createMessages(List<Notification.MessagingStyle.Message> paramList, boolean paramBoolean) {
    ArrayList<MessagingMessage> arrayList = new ArrayList();
    for (byte b = 0; b < paramList.size(); b++) {
      Notification.MessagingStyle.Message message = paramList.get(b);
      MessagingMessage messagingMessage1 = findAndRemoveMatchingMessage(message);
      MessagingMessage messagingMessage2 = messagingMessage1;
      if (messagingMessage1 == null)
        messagingMessage2 = MessagingMessage.createMessage(this, message, this.mImageResolver); 
      messagingMessage2.setIsHistoric(paramBoolean);
      arrayList.add(messagingMessage2);
    } 
    return arrayList;
  }
  
  private MessagingMessage findAndRemoveMatchingMessage(Notification.MessagingStyle.Message paramMessage) {
    byte b;
    for (b = 0; b < this.mMessages.size(); b++) {
      MessagingMessage messagingMessage = this.mMessages.get(b);
      if (messagingMessage.sameAs(paramMessage)) {
        this.mMessages.remove(b);
        return messagingMessage;
      } 
    } 
    for (b = 0; b < this.mHistoricMessages.size(); b++) {
      MessagingMessage messagingMessage = this.mHistoricMessages.get(b);
      if (messagingMessage.sameAs(paramMessage)) {
        this.mHistoricMessages.remove(b);
        return messagingMessage;
      } 
    } 
    return null;
  }
  
  public void showHistoricMessages(boolean paramBoolean) {
    this.mShowHistoricMessages = paramBoolean;
    updateHistoricMessageVisibility();
  }
  
  private void updateHistoricMessageVisibility() {
    int i = this.mHistoricMessages.size();
    byte b = 0;
    while (true) {
      byte b1 = 0;
      if (b < i) {
        MessagingMessage messagingMessage = this.mHistoricMessages.get(b);
        if (!this.mShowHistoricMessages)
          b1 = 8; 
        messagingMessage.setVisibility(b1);
        b++;
        continue;
      } 
      break;
    } 
    int j = this.mGroups.size();
    for (b = 0; b < j; b++) {
      MessagingGroup messagingGroup = this.mGroups.get(b);
      int k = 0;
      List<MessagingMessage> list = messagingGroup.getMessages();
      int m = list.size();
      for (byte b1 = 0; b1 < m; b1++, k = i) {
        MessagingMessage messagingMessage = list.get(b1);
        i = k;
        if (messagingMessage.getVisibility() != 8)
          i = k + 1; 
      } 
      if (k > 0 && messagingGroup.getVisibility() == 8) {
        messagingGroup.setVisibility(0);
      } else if (k == 0 && messagingGroup.getVisibility() != 8) {
        messagingGroup.setVisibility(8);
      } 
    } 
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    if (!this.mAddedGroups.isEmpty())
      getViewTreeObserver().addOnPreDrawListener((ViewTreeObserver.OnPreDrawListener)new Object(this)); 
    if (this.mAppOps.getWidth() > 0) {
      Rect rect = this.mAppOpsTouchRect;
      ViewGroup viewGroup2 = this.mAppOps;
      paramInt2 = viewGroup2.getLeft();
      viewGroup2 = this.mAppOps;
      paramInt3 = viewGroup2.getTop();
      viewGroup2 = this.mAppOps;
      paramInt1 = viewGroup2.getRight();
      viewGroup2 = this.mAppOps;
      paramInt4 = viewGroup2.getBottom();
      rect.set(paramInt2, paramInt3, paramInt1, paramInt4);
      for (paramInt1 = 0; paramInt1 < this.mAppOps.getChildCount(); paramInt1++) {
        View view = this.mAppOps.getChildAt(paramInt1);
        if (view.getVisibility() != 8) {
          float f3 = view.getLeft(), f4 = view.getWidth() / 2.0F, f5 = this.mMinTouchSize;
          f3 = f3 + f4 - f5 / 2.0F;
          Rect rect1 = this.mAppOpsTouchRect;
          f4 = rect1.left;
          ViewGroup viewGroup = this.mAppOps;
          float f6 = viewGroup.getLeft();
          rect1.left = (int)Math.min(f4, f6 + f3);
          rect1 = this.mAppOpsTouchRect;
          f6 = rect1.right;
          viewGroup = this.mAppOps;
          f4 = viewGroup.getLeft();
          rect1.right = (int)Math.max(f6, f4 + f5 + f3);
        } 
      } 
      paramInt1 = 0;
      float f1 = this.mAppOpsTouchRect.height(), f2 = this.mMinTouchSize;
      if (f1 < f2)
        paramInt1 = (int)Math.ceil(((f2 - this.mAppOpsTouchRect.height()) / 2.0F)); 
      this.mAppOpsTouchRect.inset(0, -paramInt1);
      ViewGroup viewGroup1 = (ViewGroup)this.mAppOps.getParent();
      while (viewGroup1 != this) {
        this.mAppOpsTouchRect.offset(viewGroup1.getLeft(), viewGroup1.getTop());
        viewGroup1 = (ViewGroup)viewGroup1.getParent();
      } 
      setTouchDelegate(new TouchDelegate(this.mAppOpsTouchRect, (View)this.mAppOps));
    } 
  }
  
  public MessagingLinearLayout getMessagingLinearLayout() {
    return this.mMessagingLinearLayout;
  }
  
  public ViewGroup getImageMessageContainer() {
    return this.mImageMessageContainer;
  }
  
  public ArrayList<MessagingGroup> getMessagingGroups() {
    return this.mGroups;
  }
  
  private void updateExpandButton() {
    int j, k;
    byte b;
    ConversationLayout conversationLayout;
    int i = 0;
    if (this.mIsCollapsed) {
      j = 17302428;
      k = 17040126;
      b = 17;
      ViewGroup viewGroup = this.mExpandButtonAndContentContainer;
    } else {
      j = 17302369;
      k = 17040127;
      b = 49;
      i = this.mExpandButtonExpandedTopMargin;
      conversationLayout = this;
    } 
    this.mExpandButton.setImageDrawable(getContext().getDrawable(j));
    NotificationExpandButton notificationExpandButton2 = this.mExpandButton;
    notificationExpandButton2.setColorFilter(notificationExpandButton2.getOriginalNotificationColor());
    if (conversationLayout != this.mExpandButtonContainer.getParent()) {
      ((ViewGroup)this.mExpandButtonContainer.getParent()).removeView(this.mExpandButtonContainer);
      conversationLayout.addView(this.mExpandButtonContainer);
    } 
    NotificationExpandButton notificationExpandButton1 = this.mExpandButton;
    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)notificationExpandButton1.getLayoutParams();
    layoutParams.gravity = b;
    layoutParams.topMargin = i;
    this.mExpandButton.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
    this.mExpandButtonInnerContainer.setContentDescription(this.mContext.getText(k));
  }
  
  private void updateContentEndPaddings() {
    int j;
    if (!this.mExpandable) {
      i = 0;
      j = this.mContentMarginEnd;
    } else if (this.mIsCollapsed) {
      i = 0;
      j = 0;
    } else {
      i = this.mNotificationHeaderExpandedPadding;
      j = this.mContentMarginEnd;
    } 
    View view1 = this.mConversationHeader;
    int k = view1.getPaddingStart();
    View view2 = this.mConversationHeader;
    int m = view2.getPaddingTop();
    view2 = this.mConversationHeader;
    int n = view2.getPaddingBottom();
    view1.setPaddingRelative(k, m, i, n);
    view1 = this.mContentContainer;
    m = view1.getPaddingStart();
    view2 = this.mContentContainer;
    int i = view2.getPaddingTop();
    view2 = this.mContentContainer;
    k = view2.getPaddingBottom();
    view1.setPaddingRelative(m, i, j, k);
  }
  
  private void onAppNameVisibilityChanged() {
    boolean bool;
    if (this.mAppName.getVisibility() == 8) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool != this.mAppNameGone) {
      this.mAppNameGone = bool;
      updateAppNameDividerVisibility();
    } 
  }
  
  private void updateAppNameDividerVisibility() {
    boolean bool;
    View view = this.mAppNameDivider;
    if (this.mAppNameGone) {
      bool = true;
    } else {
      bool = false;
    } 
    view.setVisibility(bool);
  }
  
  public void updateExpandability(boolean paramBoolean, View.OnClickListener paramOnClickListener) {
    this.mExpandable = paramBoolean;
    if (paramBoolean) {
      this.mExpandButtonContainer.setVisibility(0);
      this.mExpandButtonInnerContainer.setOnClickListener(paramOnClickListener);
      this.mConversationIconContainer.setOnClickListener(paramOnClickListener);
    } else {
      this.mExpandButtonContainer.setVisibility(8);
      this.mConversationIconContainer.setOnClickListener(null);
    } 
    updateContentEndPaddings();
  }
  
  public void setMessagingClippingDisabled(boolean paramBoolean) {
    Rect rect;
    MessagingLinearLayout messagingLinearLayout = this.mMessagingLinearLayout;
    if (paramBoolean) {
      rect = null;
    } else {
      rect = this.mMessagingClipRect;
    } 
    messagingLinearLayout.setClipBounds(rect);
  }
  
  public CharSequence getConversationSenderName() {
    if (this.mGroups.isEmpty())
      return null; 
    ArrayList<MessagingGroup> arrayList = this.mGroups;
    CharSequence charSequence = ((MessagingGroup)arrayList.get(arrayList.size() - 1)).getSenderName();
    return getResources().getString(17040002, new Object[] { charSequence });
  }
  
  public boolean isOneToOne() {
    return this.mIsOneToOne;
  }
  
  public CharSequence getConversationText() {
    SpannableString spannableString;
    if (this.mMessages.isEmpty())
      return null; 
    List<MessagingMessage> list = this.mMessages;
    MessagingMessage messagingMessage = list.get(list.size() - 1);
    CharSequence charSequence = messagingMessage.getMessage().getText();
    if (charSequence == null && messagingMessage instanceof MessagingImageMessage) {
      charSequence = getResources().getString(17040001);
      spannableString = new SpannableString(charSequence);
      StyleSpan styleSpan = new StyleSpan(2);
      int i = spannableString.length();
      spannableString.setSpan(styleSpan, 0, i, 17);
      return (CharSequence)spannableString;
    } 
    return (CharSequence)spannableString;
  }
  
  public Icon getConversationIcon() {
    return this.mConversationIcon;
  }
}
