package com.android.internal.widget;

import android.util.ArraySet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

public class ViewClippingUtil {
  private static final int CLIP_CHILDREN_TAG = 16908847;
  
  private static final int CLIP_CLIPPING_SET = 16908846;
  
  private static final int CLIP_TO_PADDING = 16908849;
  
  public static void setClippingDeactivated(View paramView, boolean paramBoolean, ClippingParameters paramClippingParameters) {
    if (!paramBoolean && !paramClippingParameters.isClippingEnablingAllowed(paramView))
      return; 
    if (!(paramView.getParent() instanceof ViewGroup))
      return; 
    ViewGroup viewGroup = (ViewGroup)paramView.getParent();
    while (true) {
      if (!paramBoolean && !paramClippingParameters.isClippingEnablingAllowed(paramView))
        return; 
      ArraySet arraySet1 = (ArraySet)viewGroup.getTag(16908846);
      ArraySet arraySet2 = arraySet1;
      if (arraySet1 == null) {
        arraySet2 = new ArraySet();
        viewGroup.setTagInternal(16908846, arraySet2);
      } 
      Boolean bool2 = (Boolean)viewGroup.getTag(16908847);
      Boolean bool1 = bool2;
      if (bool2 == null) {
        bool1 = Boolean.valueOf(viewGroup.getClipChildren());
        viewGroup.setTagInternal(16908847, bool1);
      } 
      Boolean bool3 = (Boolean)viewGroup.getTag(16908849);
      bool2 = bool3;
      if (bool3 == null) {
        bool2 = Boolean.valueOf(viewGroup.getClipToPadding());
        viewGroup.setTagInternal(16908849, bool2);
      } 
      if (!paramBoolean) {
        arraySet2.remove(paramView);
        if (arraySet2.isEmpty()) {
          viewGroup.setClipChildren(bool1.booleanValue());
          viewGroup.setClipToPadding(bool2.booleanValue());
          viewGroup.setTagInternal(16908846, null);
          paramClippingParameters.onClippingStateChanged((View)viewGroup, true);
        } 
      } else {
        arraySet2.add(paramView);
        viewGroup.setClipChildren(false);
        viewGroup.setClipToPadding(false);
        paramClippingParameters.onClippingStateChanged((View)viewGroup, false);
      } 
      if (paramClippingParameters.shouldFinish((View)viewGroup))
        return; 
      ViewParent viewParent = viewGroup.getParent();
      if (viewParent instanceof ViewGroup) {
        ViewGroup viewGroup1 = (ViewGroup)viewParent;
        continue;
      } 
      break;
    } 
  }
  
  public static interface ClippingParameters {
    default boolean isClippingEnablingAllowed(View param1View) {
      return MessagingPropertyAnimator.isAnimatingTranslation(param1View) ^ true;
    }
    
    default void onClippingStateChanged(View param1View, boolean param1Boolean) {}
    
    boolean shouldFinish(View param1View);
  }
}
