package com.android.internal.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import java.io.IOException;
import java.io.InputStream;

public class LocalImageResolver {
  private static final int MAX_SAFE_ICON_SIZE_PX = 480;
  
  public static Drawable resolveImage(Uri paramUri, Context paramContext) throws IOException {
    int i;
    double d;
    BitmapFactory.Options options = getBoundsOptionsForImage(paramUri, paramContext);
    if (options.outWidth == -1 || options.outHeight == -1)
      return null; 
    if (options.outHeight > options.outWidth) {
      i = options.outHeight;
    } else {
      i = options.outWidth;
    } 
    if (i > 480) {
      d = (i / 480);
    } else {
      d = 1.0D;
    } 
    options = new BitmapFactory.Options();
    options.inSampleSize = getPowerOfTwoForSampleRatio(d);
    InputStream inputStream = paramContext.getContentResolver().openInputStream(paramUri);
    Bitmap bitmap = BitmapFactory.decodeStream(inputStream, null, options);
    inputStream.close();
    return (Drawable)new BitmapDrawable(paramContext.getResources(), bitmap);
  }
  
  private static BitmapFactory.Options getBoundsOptionsForImage(Uri paramUri, Context paramContext) throws IOException {
    InputStream inputStream = paramContext.getContentResolver().openInputStream(paramUri);
    BitmapFactory.Options options = new BitmapFactory.Options();
    options.inJustDecodeBounds = true;
    BitmapFactory.decodeStream(inputStream, null, options);
    inputStream.close();
    return options;
  }
  
  private static int getPowerOfTwoForSampleRatio(double paramDouble) {
    int i = Integer.highestOneBit((int)Math.floor(paramDouble));
    return Math.max(1, i);
  }
}
