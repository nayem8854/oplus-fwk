package com.android.internal.widget;

import android.view.View;

public interface LockScreenWidgetCallback {
  boolean isVisible(View paramView);
  
  void requestHide(View paramView);
  
  void requestShow(View paramView);
  
  void userActivity(View paramView);
}
