package com.android.internal.widget;

import android.content.Context;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.RemotableViewMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RemoteViews.RemoteView;

@RemoteView
public class TextProgressBar extends RelativeLayout implements Chronometer.OnChronometerTickListener {
  Chronometer mChronometer = null;
  
  ProgressBar mProgressBar = null;
  
  long mDurationBase = -1L;
  
  int mDuration = -1;
  
  boolean mChronometerFollow = false;
  
  int mChronometerGravity = 0;
  
  static final int CHRONOMETER_ID = 16908308;
  
  static final int PROGRESSBAR_ID = 16908301;
  
  public static final String TAG = "TextProgressBar";
  
  public TextProgressBar(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  public TextProgressBar(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  public TextProgressBar(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  public TextProgressBar(Context paramContext) {
    super(paramContext);
  }
  
  public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams) {
    Chronometer chronometer;
    super.addView(paramView, paramInt, paramLayoutParams);
    paramInt = paramView.getId();
    if (paramInt == 16908308 && paramView instanceof Chronometer) {
      boolean bool;
      this.mChronometer = chronometer = (Chronometer)paramView;
      chronometer.setOnChronometerTickListener(this);
      if (paramLayoutParams.width == -2) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mChronometerFollow = bool;
      this.mChronometerGravity = this.mChronometer.getGravity() & 0x800007;
    } else if (paramInt == 16908301 && chronometer instanceof ProgressBar) {
      this.mProgressBar = (ProgressBar)chronometer;
    } 
  }
  
  @RemotableViewMethod
  public void setDurationBase(long paramLong) {
    this.mDurationBase = paramLong;
    if (this.mProgressBar != null) {
      Chronometer chronometer = this.mChronometer;
      if (chronometer != null) {
        int i = (int)(paramLong - chronometer.getBase());
        if (i <= 0)
          this.mDuration = 1; 
        this.mProgressBar.setMax(this.mDuration);
        return;
      } 
    } 
    throw new RuntimeException("Expecting child ProgressBar with id 'android.R.id.progress' and Chronometer id 'android.R.id.text1'");
  }
  
  public void onChronometerTick(Chronometer paramChronometer) {
    if (this.mProgressBar != null) {
      long l = SystemClock.elapsedRealtime();
      if (l >= this.mDurationBase)
        this.mChronometer.stop(); 
      int i = (int)(this.mDurationBase - l);
      this.mProgressBar.setProgress(this.mDuration - i);
      if (this.mChronometerFollow) {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)this.mProgressBar.getLayoutParams();
        int j = this.mProgressBar.getWidth() - layoutParams.leftMargin + layoutParams.rightMargin;
        i = this.mProgressBar.getProgress();
        ProgressBar progressBar = this.mProgressBar;
        int k = i * j / progressBar.getMax(), m = layoutParams.leftMargin;
        i = 0;
        int n = this.mChronometer.getWidth();
        int i1 = this.mChronometerGravity;
        if (i1 == 8388613) {
          i = -n;
        } else if (i1 == 1) {
          i = -(n / 2);
        } 
        k = k + m + i;
        j = j - layoutParams.rightMargin - n;
        if (k < layoutParams.leftMargin) {
          i = layoutParams.leftMargin;
        } else {
          i = k;
          if (k > j)
            i = j; 
        } 
        layoutParams = (RelativeLayout.LayoutParams)this.mChronometer.getLayoutParams();
        layoutParams.leftMargin = i;
        this.mChronometer.requestLayout();
      } 
      return;
    } 
    throw new RuntimeException("Expecting child ProgressBar with id 'android.R.id.progress'");
  }
}
