package com.android.internal.widget;

import android.app.Notification;
import android.content.Context;
import android.text.Layout;
import android.util.AttributeSet;
import android.util.Pools;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RemoteViews.RemoteView;

@RemoteView
public class MessagingTextMessage extends ImageFloatingTextView implements MessagingMessage {
  private static Pools.SimplePool<MessagingTextMessage> sInstancePool = (Pools.SimplePool<MessagingTextMessage>)new Pools.SynchronizedPool(20);
  
  private final MessagingMessageState mState = new MessagingMessageState((View)this);
  
  public MessagingTextMessage(Context paramContext) {
    super(paramContext);
  }
  
  public MessagingTextMessage(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  public MessagingTextMessage(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  public MessagingTextMessage(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  public MessagingMessageState getState() {
    return this.mState;
  }
  
  public boolean setMessage(Notification.MessagingStyle.Message paramMessage) {
    super.setMessage(paramMessage);
    setText(paramMessage.getText());
    return true;
  }
  
  static MessagingMessage createMessage(IMessagingLayout paramIMessagingLayout, Notification.MessagingStyle.Message paramMessage) {
    MessagingLinearLayout messagingLinearLayout = paramIMessagingLayout.getMessagingLinearLayout();
    MessagingTextMessage messagingTextMessage1 = (MessagingTextMessage)sInstancePool.acquire();
    MessagingTextMessage messagingTextMessage2 = messagingTextMessage1;
    if (messagingTextMessage1 == null) {
      Context context = paramIMessagingLayout.getContext();
      LayoutInflater layoutInflater = LayoutInflater.from(context);
      messagingTextMessage2 = (MessagingTextMessage)layoutInflater.inflate(17367219, messagingLinearLayout, false);
      messagingTextMessage2.addOnLayoutChangeListener(MessagingLayout.MESSAGING_PROPERTY_ANIMATOR);
    } 
    messagingTextMessage2.setMessage(paramMessage);
    return messagingTextMessage2;
  }
  
  public void recycle() {
    super.recycle();
    sInstancePool.release(this);
  }
  
  public static void dropCache() {
    sInstancePool = (Pools.SimplePool<MessagingTextMessage>)new Pools.SynchronizedPool(10);
  }
  
  public int getMeasuredType() {
    int i = getMeasuredHeight();
    if (i < getLayoutHeight() + getPaddingTop() + getPaddingBottom()) {
      i = 1;
    } else {
      i = 0;
    } 
    if (i != 0 && getLineCount() <= 1)
      return 2; 
    Layout layout = getLayout();
    if (layout == null)
      return 2; 
    if (layout.getEllipsisCount(layout.getLineCount() - 1) > 0)
      return 1; 
    return 0;
  }
  
  public void setMaxDisplayedLines(int paramInt) {
    setMaxLines(paramInt);
  }
  
  public int getConsumedLines() {
    return getLineCount();
  }
  
  public int getLayoutHeight() {
    Layout layout = getLayout();
    if (layout == null)
      return 0; 
    return layout.getHeight();
  }
  
  public void setColor(int paramInt) {
    setTextColor(paramInt);
  }
}
