package com.android.internal.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RemoteViews.RemoteView;
import android.widget.TextView;
import java.util.function.Consumer;

@RemoteView
public class ObservableTextView extends TextView {
  private Consumer<Integer> mOnVisibilityChangedListener;
  
  public ObservableTextView(Context paramContext) {
    super(paramContext);
  }
  
  public ObservableTextView(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  public ObservableTextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  public ObservableTextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  protected void onVisibilityChanged(View paramView, int paramInt) {
    super.onVisibilityChanged(paramView, paramInt);
    if (paramView == this) {
      Consumer<Integer> consumer = this.mOnVisibilityChangedListener;
      if (consumer != null)
        consumer.accept(Integer.valueOf(paramInt)); 
    } 
  }
  
  public void setOnVisibilityChangedListener(Consumer<Integer> paramConsumer) {
    this.mOnVisibilityChangedListener = paramConsumer;
  }
}
