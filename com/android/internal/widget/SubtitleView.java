package com.android.internal.widget;

import android.R;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;
import android.view.accessibility.CaptioningManager;

public class SubtitleView extends View {
  private final RectF mLineBounds = new RectF();
  
  private final SpannableStringBuilder mText = new SpannableStringBuilder();
  
  private Layout.Alignment mAlignment = Layout.Alignment.ALIGN_CENTER;
  
  private float mSpacingMult = 1.0F;
  
  private float mSpacingAdd = 0.0F;
  
  private int mInnerPaddingX = 0;
  
  private static final int COLOR_BEVEL_DARK = -2147483648;
  
  private static final int COLOR_BEVEL_LIGHT = -2130706433;
  
  private static final float INNER_PADDING_RATIO = 0.125F;
  
  private int mBackgroundColor;
  
  private final float mCornerRadius;
  
  private int mEdgeColor;
  
  private int mEdgeType;
  
  private int mForegroundColor;
  
  private boolean mHasMeasurements;
  
  private int mLastMeasuredWidth;
  
  private StaticLayout mLayout;
  
  private final float mOutlineWidth;
  
  private Paint mPaint;
  
  private final float mShadowOffsetX;
  
  private final float mShadowOffsetY;
  
  private final float mShadowRadius;
  
  private TextPaint mTextPaint;
  
  public SubtitleView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public SubtitleView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public SubtitleView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public SubtitleView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.TextView, paramInt1, paramInt2);
    CharSequence charSequence = "";
    paramInt2 = 15;
    int i = typedArray.getIndexCount();
    for (paramInt1 = 0; paramInt1 < i; paramInt1++) {
      int j = typedArray.getIndex(paramInt1);
      if (j != 0) {
        if (j != 18) {
          if (j != 53) {
            if (j == 54)
              this.mSpacingMult = typedArray.getFloat(j, this.mSpacingMult); 
          } else {
            this.mSpacingAdd = typedArray.getDimensionPixelSize(j, (int)this.mSpacingAdd);
          } 
        } else {
          charSequence = typedArray.getText(j);
        } 
      } else {
        paramInt2 = typedArray.getDimensionPixelSize(j, paramInt2);
      } 
    } 
    Resources resources = getContext().getResources();
    this.mCornerRadius = resources.getDimensionPixelSize(17105486);
    this.mOutlineWidth = resources.getDimensionPixelSize(17105487);
    this.mShadowRadius = resources.getDimensionPixelSize(17105489);
    float f = resources.getDimensionPixelSize(17105488);
    this.mShadowOffsetY = f;
    TextPaint textPaint = new TextPaint();
    textPaint.setAntiAlias(true);
    this.mTextPaint.setSubpixelText(true);
    Paint paint = new Paint();
    paint.setAntiAlias(true);
    setText(charSequence);
    setTextSize(paramInt2);
  }
  
  public void setText(int paramInt) {
    CharSequence charSequence = getContext().getText(paramInt);
    setText(charSequence);
  }
  
  public void setText(CharSequence paramCharSequence) {
    this.mText.clear();
    this.mText.append(paramCharSequence);
    this.mHasMeasurements = false;
    requestLayout();
    invalidate();
  }
  
  public void setForegroundColor(int paramInt) {
    this.mForegroundColor = paramInt;
    invalidate();
  }
  
  public void setBackgroundColor(int paramInt) {
    this.mBackgroundColor = paramInt;
    invalidate();
  }
  
  public void setEdgeType(int paramInt) {
    this.mEdgeType = paramInt;
    invalidate();
  }
  
  public void setEdgeColor(int paramInt) {
    this.mEdgeColor = paramInt;
    invalidate();
  }
  
  public void setTextSize(float paramFloat) {
    if (this.mTextPaint.getTextSize() != paramFloat) {
      this.mTextPaint.setTextSize(paramFloat);
      this.mInnerPaddingX = (int)(0.125F * paramFloat + 0.5F);
      this.mHasMeasurements = false;
      requestLayout();
      invalidate();
    } 
  }
  
  public void setTypeface(Typeface paramTypeface) {
    if (this.mTextPaint.getTypeface() != paramTypeface) {
      this.mTextPaint.setTypeface(paramTypeface);
      this.mHasMeasurements = false;
      requestLayout();
      invalidate();
    } 
  }
  
  public void setAlignment(Layout.Alignment paramAlignment) {
    if (this.mAlignment != paramAlignment) {
      this.mAlignment = paramAlignment;
      this.mHasMeasurements = false;
      requestLayout();
      invalidate();
    } 
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    paramInt1 = View.MeasureSpec.getSize(paramInt1);
    if (computeMeasurements(paramInt1)) {
      StaticLayout staticLayout = this.mLayout;
      int i = this.mPaddingLeft, j = this.mPaddingRight, k = this.mInnerPaddingX;
      paramInt1 = staticLayout.getWidth();
      paramInt2 = staticLayout.getHeight();
      int m = this.mPaddingTop, n = this.mPaddingBottom;
      setMeasuredDimension(paramInt1 + i + j + k * 2, paramInt2 + m + n);
    } else {
      setMeasuredDimension(16777216, 16777216);
    } 
  }
  
  public void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    computeMeasurements(paramInt3 - paramInt1);
  }
  
  private boolean computeMeasurements(int paramInt) {
    if (this.mHasMeasurements && paramInt == this.mLastMeasuredWidth)
      return true; 
    int i = this.mPaddingLeft, j = this.mPaddingRight, k = this.mInnerPaddingX;
    paramInt -= i + j + k * 2;
    if (paramInt <= 0)
      return false; 
    this.mHasMeasurements = true;
    this.mLastMeasuredWidth = paramInt;
    SpannableStringBuilder spannableStringBuilder = this.mText;
    StaticLayout.Builder builder2 = StaticLayout.Builder.obtain((CharSequence)spannableStringBuilder, 0, spannableStringBuilder.length(), this.mTextPaint, paramInt);
    Layout.Alignment alignment = this.mAlignment;
    StaticLayout.Builder builder1 = builder2.setAlignment(alignment);
    float f1 = this.mSpacingAdd, f2 = this.mSpacingMult;
    builder1 = builder1.setLineSpacing(f1, f2);
    builder1 = builder1.setUseLineSpacingFromFallbacks(true);
    this.mLayout = builder1.build();
    return true;
  }
  
  public void setStyle(int paramInt) {
    CaptioningManager.CaptionStyle captionStyle1;
    Context context = this.mContext;
    ContentResolver contentResolver = context.getContentResolver();
    if (paramInt == -1) {
      captionStyle1 = CaptioningManager.CaptionStyle.getCustomStyle(contentResolver);
    } else {
      captionStyle1 = CaptioningManager.CaptionStyle.PRESETS[paramInt];
    } 
    CaptioningManager.CaptionStyle captionStyle2 = CaptioningManager.CaptionStyle.DEFAULT;
    if (captionStyle1.hasForegroundColor()) {
      paramInt = captionStyle1.foregroundColor;
    } else {
      paramInt = captionStyle2.foregroundColor;
    } 
    this.mForegroundColor = paramInt;
    if (captionStyle1.hasBackgroundColor()) {
      paramInt = captionStyle1.backgroundColor;
    } else {
      paramInt = captionStyle2.backgroundColor;
    } 
    this.mBackgroundColor = paramInt;
    if (captionStyle1.hasEdgeType()) {
      paramInt = captionStyle1.edgeType;
    } else {
      paramInt = captionStyle2.edgeType;
    } 
    this.mEdgeType = paramInt;
    if (captionStyle1.hasEdgeColor()) {
      paramInt = captionStyle1.edgeColor;
    } else {
      paramInt = captionStyle2.edgeColor;
    } 
    this.mEdgeColor = paramInt;
    this.mHasMeasurements = false;
    Typeface typeface = captionStyle1.getTypeface();
    setTypeface(typeface);
    requestLayout();
  }
  
  protected void onDraw(Canvas paramCanvas) {
    StaticLayout staticLayout = this.mLayout;
    if (staticLayout == null)
      return; 
    int i = paramCanvas.save();
    int j = this.mInnerPaddingX;
    paramCanvas.translate((this.mPaddingLeft + j), this.mPaddingTop);
    int k = staticLayout.getLineCount();
    TextPaint textPaint = this.mTextPaint;
    Paint paint = this.mPaint;
    RectF rectF = this.mLineBounds;
    if (Color.alpha(this.mBackgroundColor) > 0) {
      float f1 = this.mCornerRadius;
      float f2 = staticLayout.getLineTop(0);
      paint.setColor(this.mBackgroundColor);
      paint.setStyle(Paint.Style.FILL);
      for (byte b = 0; b < k; b++) {
        rectF.left = staticLayout.getLineLeft(b) - j;
        rectF.right = staticLayout.getLineRight(b) + j;
        rectF.top = f2;
        rectF.bottom = staticLayout.getLineBottom(b);
        f2 = rectF.bottom;
        paramCanvas.drawRoundRect(rectF, f1, f1, paint);
      } 
    } 
    int m = this.mEdgeType;
    j = 1;
    if (m == 1) {
      textPaint.setStrokeJoin(Paint.Join.ROUND);
      textPaint.setStrokeWidth(this.mOutlineWidth);
      textPaint.setColor(this.mEdgeColor);
      textPaint.setStyle(Paint.Style.FILL_AND_STROKE);
      for (m = 0; m < k; m++)
        staticLayout.drawText(paramCanvas, m, m); 
    } else if (m == 2) {
      textPaint.setShadowLayer(this.mShadowRadius, this.mShadowOffsetX, this.mShadowOffsetY, this.mEdgeColor);
    } else if (m == 3 || m == 4) {
      int n;
      if (m != 3)
        j = 0; 
      m = -1;
      if (j != 0) {
        n = -1;
      } else {
        n = this.mEdgeColor;
      } 
      if (j != 0)
        m = this.mEdgeColor; 
      float f = this.mShadowRadius / 2.0F;
      textPaint.setColor(this.mForegroundColor);
      textPaint.setStyle(Paint.Style.FILL);
      textPaint.setShadowLayer(this.mShadowRadius, -f, -f, n);
      for (j = 0; j < k; j++)
        staticLayout.drawText(paramCanvas, j, j); 
      textPaint.setShadowLayer(this.mShadowRadius, f, f, m);
    } 
    textPaint.setColor(this.mForegroundColor);
    textPaint.setStyle(Paint.Style.FILL);
    for (m = 0; m < k; m++)
      staticLayout.drawText(paramCanvas, m, m); 
    textPaint.setShadowLayer(0.0F, 0.0F, 0.0F, 0);
    paramCanvas.restoreToCount(i);
  }
}
