package com.android.internal.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import com.android.internal.R;

public class ButtonBarLayout extends LinearLayout {
  private static final int PEEK_BUTTON_DP = 16;
  
  private boolean mAllowStacking;
  
  private int mLastWidthSize = -1;
  
  private int mMinimumHeight = 0;
  
  public ButtonBarLayout(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ButtonBarLayout);
    this.mAllowStacking = typedArray.getBoolean(0, true);
    typedArray.recycle();
  }
  
  public void setAllowStacking(boolean paramBoolean) {
    if (this.mAllowStacking != paramBoolean) {
      this.mAllowStacking = paramBoolean;
      if (!paramBoolean && getOrientation() == 1)
        setStacked(false); 
      requestLayout();
    } 
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    int i = View.MeasureSpec.getSize(paramInt1);
    if (this.mAllowStacking) {
      if (i > this.mLastWidthSize && isStacked())
        setStacked(false); 
      this.mLastWidthSize = i;
    } 
    int j = 0;
    if (!isStacked() && View.MeasureSpec.getMode(paramInt1) == 1073741824) {
      i = View.MeasureSpec.makeMeasureSpec(i, -2147483648);
      j = 1;
    } else {
      i = paramInt1;
    } 
    super.onMeasure(i, paramInt2);
    i = j;
    if (this.mAllowStacking) {
      i = j;
      if (!isStacked()) {
        int k = getMeasuredWidthAndState();
        i = j;
        if ((0xFF000000 & k) == 16777216) {
          setStacked(true);
          i = 1;
        } 
      } 
    } 
    if (i != 0)
      super.onMeasure(paramInt1, paramInt2); 
    paramInt1 = 0;
    j = getNextVisibleChildIndex(0);
    if (j >= 0) {
      View view = getChildAt(j);
      LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)view.getLayoutParams();
      paramInt2 = 0 + getPaddingTop() + view.getMeasuredHeight() + layoutParams.topMargin + layoutParams.bottomMargin;
      if (isStacked()) {
        j = getNextVisibleChildIndex(j + 1);
        paramInt1 = paramInt2;
        if (j >= 0) {
          float f1 = paramInt2, f2 = getChildAt(j).getPaddingTop();
          paramInt1 = (int)(f1 + f2 + (getResources().getDisplayMetrics()).density * 16.0F);
        } 
      } else {
        paramInt1 = paramInt2 + getPaddingBottom();
      } 
    } 
    if (getMinimumHeight() != paramInt1)
      setMinimumHeight(paramInt1); 
  }
  
  private int getNextVisibleChildIndex(int paramInt) {
    for (int i = getChildCount(); paramInt < i; paramInt++) {
      if (getChildAt(paramInt).getVisibility() == 0)
        return paramInt; 
    } 
    return -1;
  }
  
  public int getMinimumHeight() {
    return Math.max(this.mMinimumHeight, super.getMinimumHeight());
  }
  
  private void setStacked(boolean paramBoolean) {
    byte b;
    setOrientation(paramBoolean);
    if (paramBoolean) {
      b = 8388613;
    } else {
      b = 80;
    } 
    setGravity(b);
    View view = findViewById(16909459);
    if (view != null) {
      byte b1;
      if (paramBoolean) {
        b1 = 8;
      } else {
        b1 = 4;
      } 
      view.setVisibility(b1);
    } 
    int i = getChildCount();
    for (i -= 2; i >= 0; i--)
      bringChildToFront(getChildAt(i)); 
  }
  
  private boolean isStacked() {
    int i = getOrientation();
    boolean bool = true;
    if (i != 1)
      bool = false; 
    return bool;
  }
}
