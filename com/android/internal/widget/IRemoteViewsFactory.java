package com.android.internal.widget;

import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.widget.RemoteViews;

public interface IRemoteViewsFactory extends IInterface {
  int getCount() throws RemoteException;
  
  long getItemId(int paramInt) throws RemoteException;
  
  RemoteViews getLoadingView() throws RemoteException;
  
  RemoteViews getViewAt(int paramInt) throws RemoteException;
  
  int getViewTypeCount() throws RemoteException;
  
  boolean hasStableIds() throws RemoteException;
  
  boolean isCreated() throws RemoteException;
  
  void onDataSetChanged() throws RemoteException;
  
  void onDataSetChangedAsync() throws RemoteException;
  
  void onDestroy(Intent paramIntent) throws RemoteException;
  
  class Default implements IRemoteViewsFactory {
    public void onDataSetChanged() throws RemoteException {}
    
    public void onDataSetChangedAsync() throws RemoteException {}
    
    public void onDestroy(Intent param1Intent) throws RemoteException {}
    
    public int getCount() throws RemoteException {
      return 0;
    }
    
    public RemoteViews getViewAt(int param1Int) throws RemoteException {
      return null;
    }
    
    public RemoteViews getLoadingView() throws RemoteException {
      return null;
    }
    
    public int getViewTypeCount() throws RemoteException {
      return 0;
    }
    
    public long getItemId(int param1Int) throws RemoteException {
      return 0L;
    }
    
    public boolean hasStableIds() throws RemoteException {
      return false;
    }
    
    public boolean isCreated() throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IRemoteViewsFactory {
    private static final String DESCRIPTOR = "com.android.internal.widget.IRemoteViewsFactory";
    
    static final int TRANSACTION_getCount = 4;
    
    static final int TRANSACTION_getItemId = 8;
    
    static final int TRANSACTION_getLoadingView = 6;
    
    static final int TRANSACTION_getViewAt = 5;
    
    static final int TRANSACTION_getViewTypeCount = 7;
    
    static final int TRANSACTION_hasStableIds = 9;
    
    static final int TRANSACTION_isCreated = 10;
    
    static final int TRANSACTION_onDataSetChanged = 1;
    
    static final int TRANSACTION_onDataSetChangedAsync = 2;
    
    static final int TRANSACTION_onDestroy = 3;
    
    public Stub() {
      attachInterface(this, "com.android.internal.widget.IRemoteViewsFactory");
    }
    
    public static IRemoteViewsFactory asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.widget.IRemoteViewsFactory");
      if (iInterface != null && iInterface instanceof IRemoteViewsFactory)
        return (IRemoteViewsFactory)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 10:
          return "isCreated";
        case 9:
          return "hasStableIds";
        case 8:
          return "getItemId";
        case 7:
          return "getViewTypeCount";
        case 6:
          return "getLoadingView";
        case 5:
          return "getViewAt";
        case 4:
          return "getCount";
        case 3:
          return "onDestroy";
        case 2:
          return "onDataSetChangedAsync";
        case 1:
          break;
      } 
      return "onDataSetChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        int i;
        RemoteViews remoteViews;
        long l;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 10:
            param1Parcel1.enforceInterface("com.android.internal.widget.IRemoteViewsFactory");
            bool = isCreated();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 9:
            param1Parcel1.enforceInterface("com.android.internal.widget.IRemoteViewsFactory");
            bool = hasStableIds();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 8:
            param1Parcel1.enforceInterface("com.android.internal.widget.IRemoteViewsFactory");
            i = param1Parcel1.readInt();
            l = getItemId(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 7:
            param1Parcel1.enforceInterface("com.android.internal.widget.IRemoteViewsFactory");
            i = getViewTypeCount();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 6:
            param1Parcel1.enforceInterface("com.android.internal.widget.IRemoteViewsFactory");
            remoteViews = getLoadingView();
            param1Parcel2.writeNoException();
            if (remoteViews != null) {
              param1Parcel2.writeInt(1);
              remoteViews.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 5:
            remoteViews.enforceInterface("com.android.internal.widget.IRemoteViewsFactory");
            i = remoteViews.readInt();
            remoteViews = getViewAt(i);
            param1Parcel2.writeNoException();
            if (remoteViews != null) {
              param1Parcel2.writeInt(1);
              remoteViews.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 4:
            remoteViews.enforceInterface("com.android.internal.widget.IRemoteViewsFactory");
            i = getCount();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 3:
            remoteViews.enforceInterface("com.android.internal.widget.IRemoteViewsFactory");
            if (remoteViews.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)remoteViews);
            } else {
              remoteViews = null;
            } 
            onDestroy((Intent)remoteViews);
            return true;
          case 2:
            remoteViews.enforceInterface("com.android.internal.widget.IRemoteViewsFactory");
            onDataSetChangedAsync();
            return true;
          case 1:
            break;
        } 
        remoteViews.enforceInterface("com.android.internal.widget.IRemoteViewsFactory");
        onDataSetChanged();
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("com.android.internal.widget.IRemoteViewsFactory");
      return true;
    }
    
    private static class Proxy implements IRemoteViewsFactory {
      public static IRemoteViewsFactory sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.widget.IRemoteViewsFactory";
      }
      
      public void onDataSetChanged() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.IRemoteViewsFactory");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IRemoteViewsFactory.Stub.getDefaultImpl() != null) {
            IRemoteViewsFactory.Stub.getDefaultImpl().onDataSetChanged();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onDataSetChangedAsync() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.widget.IRemoteViewsFactory");
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IRemoteViewsFactory.Stub.getDefaultImpl() != null) {
            IRemoteViewsFactory.Stub.getDefaultImpl().onDataSetChangedAsync();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDestroy(Intent param2Intent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.widget.IRemoteViewsFactory");
          if (param2Intent != null) {
            parcel.writeInt(1);
            param2Intent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IRemoteViewsFactory.Stub.getDefaultImpl() != null) {
            IRemoteViewsFactory.Stub.getDefaultImpl().onDestroy(param2Intent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public int getCount() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.IRemoteViewsFactory");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IRemoteViewsFactory.Stub.getDefaultImpl() != null)
            return IRemoteViewsFactory.Stub.getDefaultImpl().getCount(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public RemoteViews getViewAt(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          RemoteViews remoteViews;
          parcel1.writeInterfaceToken("com.android.internal.widget.IRemoteViewsFactory");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IRemoteViewsFactory.Stub.getDefaultImpl() != null) {
            remoteViews = IRemoteViewsFactory.Stub.getDefaultImpl().getViewAt(param2Int);
            return remoteViews;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            remoteViews = (RemoteViews)RemoteViews.CREATOR.createFromParcel(parcel2);
          } else {
            remoteViews = null;
          } 
          return remoteViews;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public RemoteViews getLoadingView() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          RemoteViews remoteViews;
          parcel1.writeInterfaceToken("com.android.internal.widget.IRemoteViewsFactory");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IRemoteViewsFactory.Stub.getDefaultImpl() != null) {
            remoteViews = IRemoteViewsFactory.Stub.getDefaultImpl().getLoadingView();
            return remoteViews;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            remoteViews = (RemoteViews)RemoteViews.CREATOR.createFromParcel(parcel2);
          } else {
            remoteViews = null;
          } 
          return remoteViews;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getViewTypeCount() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.IRemoteViewsFactory");
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IRemoteViewsFactory.Stub.getDefaultImpl() != null)
            return IRemoteViewsFactory.Stub.getDefaultImpl().getViewTypeCount(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getItemId(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.IRemoteViewsFactory");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IRemoteViewsFactory.Stub.getDefaultImpl() != null)
            return IRemoteViewsFactory.Stub.getDefaultImpl().getItemId(param2Int); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasStableIds() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.IRemoteViewsFactory");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(9, parcel1, parcel2, 0);
          if (!bool2 && IRemoteViewsFactory.Stub.getDefaultImpl() != null) {
            bool1 = IRemoteViewsFactory.Stub.getDefaultImpl().hasStableIds();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isCreated() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.widget.IRemoteViewsFactory");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(10, parcel1, parcel2, 0);
          if (!bool2 && IRemoteViewsFactory.Stub.getDefaultImpl() != null) {
            bool1 = IRemoteViewsFactory.Stub.getDefaultImpl().isCreated();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IRemoteViewsFactory param1IRemoteViewsFactory) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IRemoteViewsFactory != null) {
          Proxy.sDefaultImpl = param1IRemoteViewsFactory;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IRemoteViewsFactory getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
