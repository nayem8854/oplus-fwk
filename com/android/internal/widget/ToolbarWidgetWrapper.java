package com.android.internal.widget;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.widget.ActionMenuPresenter;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toolbar;
import com.android.internal.R;
import com.android.internal.view.menu.MenuBuilder;
import com.android.internal.view.menu.MenuPresenter;

public class ToolbarWidgetWrapper implements DecorToolbar {
  private static final int AFFECTS_LOGO_MASK = 3;
  
  private static final long DEFAULT_FADE_DURATION_MS = 200L;
  
  private static final String TAG = "ToolbarWidgetWrapper";
  
  private ActionMenuPresenter mActionMenuPresenter;
  
  private View mCustomView;
  
  private int mDefaultNavigationContentDescription;
  
  private Drawable mDefaultNavigationIcon;
  
  private int mDisplayOpts;
  
  private CharSequence mHomeDescription;
  
  private Drawable mIcon;
  
  private Drawable mLogo;
  
  private boolean mMenuPrepared;
  
  private Drawable mNavIcon;
  
  private int mNavigationMode;
  
  private Spinner mSpinner;
  
  private CharSequence mSubtitle;
  
  private View mTabView;
  
  private CharSequence mTitle;
  
  private boolean mTitleSet;
  
  private Toolbar mToolbar;
  
  private Window.Callback mWindowCallback;
  
  public ToolbarWidgetWrapper(Toolbar paramToolbar, boolean paramBoolean) {
    this(paramToolbar, paramBoolean, 17039592);
  }
  
  public ToolbarWidgetWrapper(Toolbar paramToolbar, boolean paramBoolean, int paramInt) {
    boolean bool;
    this.mNavigationMode = 0;
    this.mDefaultNavigationContentDescription = 0;
    this.mToolbar = paramToolbar;
    this.mTitle = paramToolbar.getTitle();
    this.mSubtitle = paramToolbar.getSubtitle();
    if (this.mTitle != null) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mTitleSet = bool;
    this.mNavIcon = this.mToolbar.getNavigationIcon();
    TypedArray typedArray = paramToolbar.getContext().obtainStyledAttributes(null, R.styleable.ActionBar, 16843470, 0);
    this.mDefaultNavigationIcon = typedArray.getDrawable(13);
    if (paramBoolean) {
      CharSequence charSequence = typedArray.getText(5);
      if (!TextUtils.isEmpty(charSequence))
        setTitle(charSequence); 
      charSequence = typedArray.getText(9);
      if (!TextUtils.isEmpty(charSequence))
        setSubtitle(charSequence); 
      Drawable drawable = typedArray.getDrawable(6);
      if (drawable != null)
        setLogo(drawable); 
      drawable = typedArray.getDrawable(0);
      if (drawable != null)
        setIcon(drawable); 
      if (this.mNavIcon == null) {
        drawable = this.mDefaultNavigationIcon;
        if (drawable != null)
          setNavigationIcon(drawable); 
      } 
      setDisplayOptions(typedArray.getInt(8, 0));
      int i = typedArray.getResourceId(10, 0);
      if (i != 0) {
        setCustomView(LayoutInflater.from(this.mToolbar.getContext()).inflate(i, (ViewGroup)this.mToolbar, false));
        setDisplayOptions(this.mDisplayOpts | 0x10);
      } 
      i = typedArray.getLayoutDimension(4, 0);
      if (i > 0) {
        ViewGroup.LayoutParams layoutParams = this.mToolbar.getLayoutParams();
        layoutParams.height = i;
        this.mToolbar.setLayoutParams(layoutParams);
      } 
      i = typedArray.getDimensionPixelOffset(22, -1);
      int j = typedArray.getDimensionPixelOffset(23, -1);
      if (i >= 0 || j >= 0) {
        Toolbar toolbar = this.mToolbar;
        i = Math.max(i, 0);
        j = Math.max(j, 0);
        toolbar.setContentInsetsRelative(i, j);
      } 
      i = typedArray.getResourceId(11, 0);
      if (i != 0) {
        Toolbar toolbar = this.mToolbar;
        toolbar.setTitleTextAppearance(toolbar.getContext(), i);
      } 
      i = typedArray.getResourceId(12, 0);
      if (i != 0) {
        Toolbar toolbar = this.mToolbar;
        toolbar.setSubtitleTextAppearance(toolbar.getContext(), i);
      } 
      i = typedArray.getResourceId(26, 0);
      if (i != 0)
        this.mToolbar.setPopupTheme(i); 
    } else {
      this.mDisplayOpts = detectDisplayOptions();
    } 
    typedArray.recycle();
    setDefaultNavigationContentDescription(paramInt);
    this.mHomeDescription = this.mToolbar.getNavigationContentDescription();
    this.mToolbar.setNavigationOnClickListener((View.OnClickListener)new Object(this));
  }
  
  public void setDefaultNavigationContentDescription(int paramInt) {
    if (paramInt == this.mDefaultNavigationContentDescription)
      return; 
    this.mDefaultNavigationContentDescription = paramInt;
    if (TextUtils.isEmpty(this.mToolbar.getNavigationContentDescription()))
      setNavigationContentDescription(this.mDefaultNavigationContentDescription); 
  }
  
  private int detectDisplayOptions() {
    int i = 11;
    if (this.mToolbar.getNavigationIcon() != null) {
      i = 0xB | 0x4;
      this.mDefaultNavigationIcon = this.mToolbar.getNavigationIcon();
    } 
    return i;
  }
  
  public ViewGroup getViewGroup() {
    return (ViewGroup)this.mToolbar;
  }
  
  public Context getContext() {
    return this.mToolbar.getContext();
  }
  
  public boolean isSplit() {
    return false;
  }
  
  public boolean hasExpandedActionView() {
    return this.mToolbar.hasExpandedActionView();
  }
  
  public void collapseActionView() {
    this.mToolbar.collapseActionView();
  }
  
  public void setWindowCallback(Window.Callback paramCallback) {
    this.mWindowCallback = paramCallback;
  }
  
  public void setWindowTitle(CharSequence paramCharSequence) {
    if (!this.mTitleSet)
      setTitleInt(paramCharSequence); 
  }
  
  public CharSequence getTitle() {
    return this.mToolbar.getTitle();
  }
  
  public void setTitle(CharSequence paramCharSequence) {
    this.mTitleSet = true;
    setTitleInt(paramCharSequence);
  }
  
  private void setTitleInt(CharSequence paramCharSequence) {
    this.mTitle = paramCharSequence;
    if ((this.mDisplayOpts & 0x8) != 0)
      this.mToolbar.setTitle(paramCharSequence); 
  }
  
  public CharSequence getSubtitle() {
    return this.mToolbar.getSubtitle();
  }
  
  public void setSubtitle(CharSequence paramCharSequence) {
    this.mSubtitle = paramCharSequence;
    if ((this.mDisplayOpts & 0x8) != 0)
      this.mToolbar.setSubtitle(paramCharSequence); 
  }
  
  public void initProgress() {
    Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
  }
  
  public void initIndeterminateProgress() {
    Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
  }
  
  public boolean canSplit() {
    return false;
  }
  
  public void setSplitView(ViewGroup paramViewGroup) {}
  
  public void setSplitToolbar(boolean paramBoolean) {
    if (!paramBoolean)
      return; 
    throw new UnsupportedOperationException("Cannot split an android.widget.Toolbar");
  }
  
  public void setSplitWhenNarrow(boolean paramBoolean) {}
  
  public boolean hasIcon() {
    boolean bool;
    if (this.mIcon != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean hasLogo() {
    boolean bool;
    if (this.mLogo != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setIcon(int paramInt) {
    Drawable drawable;
    if (paramInt != 0) {
      drawable = getContext().getDrawable(paramInt);
    } else {
      drawable = null;
    } 
    setIcon(drawable);
  }
  
  public void setIcon(Drawable paramDrawable) {
    this.mIcon = paramDrawable;
    updateToolbarLogo();
  }
  
  public void setLogo(int paramInt) {
    Drawable drawable;
    if (paramInt != 0) {
      drawable = getContext().getDrawable(paramInt);
    } else {
      drawable = null;
    } 
    setLogo(drawable);
  }
  
  public void setLogo(Drawable paramDrawable) {
    this.mLogo = paramDrawable;
    updateToolbarLogo();
  }
  
  private void updateToolbarLogo() {
    Drawable drawable = null;
    int i = this.mDisplayOpts;
    if ((i & 0x2) != 0)
      if ((i & 0x1) != 0) {
        drawable = this.mLogo;
        if (drawable == null)
          drawable = this.mIcon; 
      } else {
        drawable = this.mIcon;
      }  
    this.mToolbar.setLogo(drawable);
  }
  
  public boolean canShowOverflowMenu() {
    return this.mToolbar.canShowOverflowMenu();
  }
  
  public boolean isOverflowMenuShowing() {
    return this.mToolbar.isOverflowMenuShowing();
  }
  
  public boolean isOverflowMenuShowPending() {
    return this.mToolbar.isOverflowMenuShowPending();
  }
  
  public boolean showOverflowMenu() {
    return this.mToolbar.showOverflowMenu();
  }
  
  public boolean hideOverflowMenu() {
    return this.mToolbar.hideOverflowMenu();
  }
  
  public void setMenuPrepared() {
    this.mMenuPrepared = true;
  }
  
  public void setMenu(Menu paramMenu, MenuPresenter.Callback paramCallback) {
    if (this.mActionMenuPresenter == null) {
      ActionMenuPresenter actionMenuPresenter = new ActionMenuPresenter(this.mToolbar.getContext());
      actionMenuPresenter.setId(16908717);
    } 
    this.mActionMenuPresenter.setCallback(paramCallback);
    this.mToolbar.setMenu((MenuBuilder)paramMenu, this.mActionMenuPresenter);
  }
  
  public void dismissPopupMenus() {
    this.mToolbar.dismissPopupMenus();
  }
  
  public int getDisplayOptions() {
    return this.mDisplayOpts;
  }
  
  public void setDisplayOptions(int paramInt) {
    int i = this.mDisplayOpts;
    i ^= paramInt;
    this.mDisplayOpts = paramInt;
    if (i != 0) {
      if ((i & 0x4) != 0) {
        if ((paramInt & 0x4) != 0)
          updateHomeAccessibility(); 
        updateNavigationIcon();
      } 
      if ((i & 0x3) != 0)
        updateToolbarLogo(); 
      if ((i & 0x8) != 0)
        if ((paramInt & 0x8) != 0) {
          this.mToolbar.setTitle(this.mTitle);
          this.mToolbar.setSubtitle(this.mSubtitle);
        } else {
          this.mToolbar.setTitle(null);
          this.mToolbar.setSubtitle(null);
        }  
      if ((i & 0x10) != 0) {
        View view = this.mCustomView;
        if (view != null)
          if ((paramInt & 0x10) != 0) {
            this.mToolbar.addView(view);
          } else {
            this.mToolbar.removeView(view);
          }  
      } 
    } 
  }
  
  public void setEmbeddedTabView(ScrollingTabContainerView paramScrollingTabContainerView) {
    View view = this.mTabView;
    if (view != null) {
      ViewParent viewParent = view.getParent();
      Toolbar toolbar = this.mToolbar;
      if (viewParent == toolbar)
        toolbar.removeView(this.mTabView); 
    } 
    this.mTabView = (View)paramScrollingTabContainerView;
    if (paramScrollingTabContainerView != null && this.mNavigationMode == 2) {
      this.mToolbar.addView((View)paramScrollingTabContainerView, 0);
      Toolbar.LayoutParams layoutParams = (Toolbar.LayoutParams)this.mTabView.getLayoutParams();
      layoutParams.width = -2;
      layoutParams.height = -2;
      layoutParams.gravity = 8388691;
      paramScrollingTabContainerView.setAllowCollapse(true);
    } 
  }
  
  public boolean hasEmbeddedTabs() {
    boolean bool;
    if (this.mTabView != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isTitleTruncated() {
    return this.mToolbar.isTitleTruncated();
  }
  
  public void setCollapsible(boolean paramBoolean) {
    this.mToolbar.setCollapsible(paramBoolean);
  }
  
  public void setHomeButtonEnabled(boolean paramBoolean) {}
  
  public int getNavigationMode() {
    return this.mNavigationMode;
  }
  
  public void setNavigationMode(int paramInt) {
    int i = this.mNavigationMode;
    if (paramInt != i) {
      if (i != 1) {
        if (i == 2) {
          View view = this.mTabView;
          if (view != null) {
            ViewParent viewParent = view.getParent();
            Toolbar toolbar = this.mToolbar;
            if (viewParent == toolbar)
              toolbar.removeView(this.mTabView); 
          } 
        } 
      } else {
        Spinner spinner = this.mSpinner;
        if (spinner != null) {
          ViewParent viewParent = spinner.getParent();
          Toolbar toolbar = this.mToolbar;
          if (viewParent == toolbar)
            toolbar.removeView((View)this.mSpinner); 
        } 
      } 
      this.mNavigationMode = paramInt;
      if (paramInt != 0)
        if (paramInt != 1) {
          if (paramInt == 2) {
            View view = this.mTabView;
            if (view != null) {
              this.mToolbar.addView(view, 0);
              Toolbar.LayoutParams layoutParams = (Toolbar.LayoutParams)this.mTabView.getLayoutParams();
              layoutParams.width = -2;
              layoutParams.height = -2;
              layoutParams.gravity = 8388691;
            } 
          } else {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Invalid navigation mode ");
            stringBuilder.append(paramInt);
            throw new IllegalArgumentException(stringBuilder.toString());
          } 
        } else {
          ensureSpinner();
          this.mToolbar.addView((View)this.mSpinner, 0);
        }  
    } 
  }
  
  private void ensureSpinner() {
    if (this.mSpinner == null) {
      this.mSpinner = new Spinner(getContext(), null, 16843479);
      Toolbar.LayoutParams layoutParams = new Toolbar.LayoutParams(-2, -2, 8388627);
      this.mSpinner.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
    } 
  }
  
  public void setDropdownParams(SpinnerAdapter paramSpinnerAdapter, AdapterView.OnItemSelectedListener paramOnItemSelectedListener) {
    ensureSpinner();
    this.mSpinner.setAdapter(paramSpinnerAdapter);
    this.mSpinner.setOnItemSelectedListener(paramOnItemSelectedListener);
  }
  
  public void setDropdownSelectedPosition(int paramInt) {
    Spinner spinner = this.mSpinner;
    if (spinner != null) {
      spinner.setSelection(paramInt);
      return;
    } 
    throw new IllegalStateException("Can't set dropdown selected position without an adapter");
  }
  
  public int getDropdownSelectedPosition() {
    boolean bool;
    Spinner spinner = this.mSpinner;
    if (spinner != null) {
      bool = spinner.getSelectedItemPosition();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getDropdownItemCount() {
    boolean bool;
    Spinner spinner = this.mSpinner;
    if (spinner != null) {
      bool = spinner.getCount();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setCustomView(View paramView) {
    View view = this.mCustomView;
    if (view != null && (this.mDisplayOpts & 0x10) != 0)
      this.mToolbar.removeView(view); 
    this.mCustomView = paramView;
    if (paramView != null && (this.mDisplayOpts & 0x10) != 0)
      this.mToolbar.addView(paramView); 
  }
  
  public View getCustomView() {
    return this.mCustomView;
  }
  
  public void animateToVisibility(int paramInt) {
    Animator animator = setupAnimatorToVisibility(paramInt, 200L);
    if (animator != null)
      animator.start(); 
  }
  
  public Animator setupAnimatorToVisibility(int paramInt, long paramLong) {
    if (paramInt == 8) {
      ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(this.mToolbar, View.ALPHA, new float[] { 1.0F, 0.0F });
      objectAnimator.setDuration(paramLong);
      objectAnimator.addListener((Animator.AnimatorListener)new Object(this));
      return (Animator)objectAnimator;
    } 
    if (paramInt == 0) {
      ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(this.mToolbar, View.ALPHA, new float[] { 0.0F, 1.0F });
      objectAnimator.setDuration(paramLong);
      objectAnimator.addListener((Animator.AnimatorListener)new Object(this));
      return (Animator)objectAnimator;
    } 
    return null;
  }
  
  public void setNavigationIcon(Drawable paramDrawable) {
    this.mNavIcon = paramDrawable;
    updateNavigationIcon();
  }
  
  public void setNavigationIcon(int paramInt) {
    Drawable drawable;
    if (paramInt != 0) {
      drawable = this.mToolbar.getContext().getDrawable(paramInt);
    } else {
      drawable = null;
    } 
    setNavigationIcon(drawable);
  }
  
  public void setDefaultNavigationIcon(Drawable paramDrawable) {
    if (this.mDefaultNavigationIcon != paramDrawable) {
      this.mDefaultNavigationIcon = paramDrawable;
      updateNavigationIcon();
    } 
  }
  
  private void updateNavigationIcon() {
    if ((this.mDisplayOpts & 0x4) != 0) {
      Toolbar toolbar = this.mToolbar;
      Drawable drawable = this.mNavIcon;
      if (drawable == null)
        drawable = this.mDefaultNavigationIcon; 
      toolbar.setNavigationIcon(drawable);
    } else {
      this.mToolbar.setNavigationIcon(null);
    } 
  }
  
  public void setNavigationContentDescription(CharSequence paramCharSequence) {
    this.mHomeDescription = paramCharSequence;
    updateHomeAccessibility();
  }
  
  public void setNavigationContentDescription(int paramInt) {
    String str;
    if (paramInt == 0) {
      str = null;
    } else {
      str = getContext().getString(paramInt);
    } 
    setNavigationContentDescription(str);
  }
  
  private void updateHomeAccessibility() {
    if ((this.mDisplayOpts & 0x4) != 0)
      if (TextUtils.isEmpty(this.mHomeDescription)) {
        this.mToolbar.setNavigationContentDescription(this.mDefaultNavigationContentDescription);
      } else {
        this.mToolbar.setNavigationContentDescription(this.mHomeDescription);
      }  
  }
  
  public void saveHierarchyState(SparseArray<Parcelable> paramSparseArray) {
    this.mToolbar.saveHierarchyState(paramSparseArray);
  }
  
  public void restoreHierarchyState(SparseArray<Parcelable> paramSparseArray) {
    this.mToolbar.restoreHierarchyState(paramSparseArray);
  }
  
  public void setBackgroundDrawable(Drawable paramDrawable) {
    this.mToolbar.setBackgroundDrawable(paramDrawable);
  }
  
  public int getHeight() {
    return this.mToolbar.getHeight();
  }
  
  public void setVisibility(int paramInt) {
    this.mToolbar.setVisibility(paramInt);
  }
  
  public int getVisibility() {
    return this.mToolbar.getVisibility();
  }
  
  public void setMenuCallbacks(MenuPresenter.Callback paramCallback, MenuBuilder.Callback paramCallback1) {
    this.mToolbar.setMenuCallbacks(paramCallback, paramCallback1);
  }
  
  public Menu getMenu() {
    return this.mToolbar.getMenu();
  }
}
