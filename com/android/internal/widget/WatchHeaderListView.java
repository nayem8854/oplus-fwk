package com.android.internal.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.function.Predicate;

public class WatchHeaderListView extends ListView {
  private View mTopPanel;
  
  public WatchHeaderListView(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  public WatchHeaderListView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  public WatchHeaderListView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  protected HeaderViewListAdapter wrapHeaderListAdapterInternal(ArrayList<ListView.FixedViewInfo> paramArrayList1, ArrayList<ListView.FixedViewInfo> paramArrayList2, ListAdapter paramListAdapter) {
    return new WatchHeaderListAdapter(paramArrayList1, paramArrayList2, paramListAdapter);
  }
  
  public void addView(View paramView, ViewGroup.LayoutParams paramLayoutParams) {
    if (this.mTopPanel == null) {
      setTopPanel(paramView);
      return;
    } 
    throw new IllegalStateException("WatchHeaderListView can host only one header");
  }
  
  public void setTopPanel(View paramView) {
    this.mTopPanel = paramView;
    wrapAdapterIfNecessary();
  }
  
  public void setAdapter(ListAdapter paramListAdapter) {
    super.setAdapter(paramListAdapter);
    wrapAdapterIfNecessary();
  }
  
  protected View findViewTraversal(int paramInt) {
    View view = super.findViewTraversal(paramInt);
    if (view == null) {
      View view1 = this.mTopPanel;
      if (view1 != null && !view1.isRootNamespace())
        return this.mTopPanel.findViewById(paramInt); 
    } 
    return view;
  }
  
  protected View findViewWithTagTraversal(Object paramObject) {
    View view = super.findViewWithTagTraversal(paramObject);
    if (view == null) {
      View view1 = this.mTopPanel;
      if (view1 != null && !view1.isRootNamespace())
        return this.mTopPanel.findViewWithTag(paramObject); 
    } 
    return view;
  }
  
  protected <T extends View> T findViewByPredicateTraversal(Predicate<View> paramPredicate, View paramView) {
    View view = super.findViewByPredicateTraversal(paramPredicate, paramView);
    if (view == null) {
      View view1 = this.mTopPanel;
      if (view1 != null && view1 != paramView && 
        !view1.isRootNamespace())
        return (T)this.mTopPanel.findViewByPredicate(paramPredicate); 
    } 
    return (T)view;
  }
  
  public int getHeaderViewsCount() {
    int i;
    if (this.mTopPanel == null) {
      i = super.getHeaderViewsCount();
    } else {
      int j = super.getHeaderViewsCount();
      if (this.mTopPanel.getVisibility() == 8) {
        i = 0;
      } else {
        i = 1;
      } 
      i = j + i;
    } 
    return i;
  }
  
  private void wrapAdapterIfNecessary() {
    ListAdapter listAdapter = getAdapter();
    if (listAdapter != null && this.mTopPanel != null) {
      if (!(listAdapter instanceof WatchHeaderListAdapter))
        wrapHeaderListAdapterInternal(); 
      ((WatchHeaderListAdapter)getAdapter()).setTopPanel(this.mTopPanel);
      dispatchDataSetObserverOnChangedInternal();
    } 
  }
  
  class WatchHeaderListAdapter extends HeaderViewListAdapter {
    private View mTopPanel;
    
    public WatchHeaderListAdapter(WatchHeaderListView this$0, ArrayList<ListView.FixedViewInfo> param1ArrayList1, ListAdapter param1ListAdapter) {
      super((ArrayList)this$0, param1ArrayList1, param1ListAdapter);
    }
    
    public void setTopPanel(View param1View) {
      this.mTopPanel = param1View;
    }
    
    private int getTopPanelCount() {
      View view = this.mTopPanel;
      return (view == null || view.getVisibility() == 8) ? 0 : 1;
    }
    
    public int getCount() {
      return super.getCount() + getTopPanelCount();
    }
    
    public boolean areAllItemsEnabled() {
      boolean bool;
      if (getTopPanelCount() == 0 && super.areAllItemsEnabled()) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean isEnabled(int param1Int) {
      boolean bool;
      int i = getTopPanelCount();
      if (param1Int < i) {
        bool = false;
      } else {
        bool = super.isEnabled(param1Int - i);
      } 
      return bool;
    }
    
    public Object getItem(int param1Int) {
      Object object;
      int i = getTopPanelCount();
      if (param1Int < i) {
        object = null;
      } else {
        object = super.getItem(param1Int - i);
      } 
      return object;
    }
    
    public long getItemId(int param1Int) {
      int i = getHeadersCount() + getTopPanelCount();
      if (getWrappedAdapter() != null && param1Int >= i) {
        param1Int -= i;
        i = getWrappedAdapter().getCount();
        if (param1Int < i)
          return getWrappedAdapter().getItemId(param1Int); 
      } 
      return -1L;
    }
    
    public View getView(int param1Int, View param1View, ViewGroup param1ViewGroup) {
      int i = getTopPanelCount();
      if (param1Int < i) {
        param1View = this.mTopPanel;
      } else {
        param1View = super.getView(param1Int - i, param1View, param1ViewGroup);
      } 
      return param1View;
    }
    
    public int getItemViewType(int param1Int) {
      int i = getHeadersCount() + getTopPanelCount();
      if (getWrappedAdapter() != null && param1Int >= i) {
        i = param1Int - i;
        param1Int = getWrappedAdapter().getCount();
        if (i < param1Int)
          return getWrappedAdapter().getItemViewType(i); 
      } 
      return -2;
    }
  }
}
