package com.android.internal.widget;

import android.view.View;

class ScrollbarHelper {
  static int computeScrollOffset(RecyclerView.State paramState, OrientationHelper paramOrientationHelper, View paramView1, View paramView2, RecyclerView.LayoutManager paramLayoutManager, boolean paramBoolean1, boolean paramBoolean2) {
    if (paramLayoutManager.getChildCount() == 0 || paramState.getItemCount() == 0 || paramView1 == null || paramView2 == null)
      return 0; 
    int i = paramLayoutManager.getPosition(paramView1);
    int j = paramLayoutManager.getPosition(paramView2);
    i = Math.min(i, j);
    j = paramLayoutManager.getPosition(paramView1);
    int k = paramLayoutManager.getPosition(paramView2);
    j = Math.max(j, k);
    if (paramBoolean2) {
      i = Math.max(0, paramState.getItemCount() - j - 1);
    } else {
      i = Math.max(0, i);
    } 
    if (!paramBoolean1)
      return i; 
    j = paramOrientationHelper.getDecoratedEnd(paramView2);
    k = paramOrientationHelper.getDecoratedStart(paramView1);
    j = Math.abs(j - k);
    int m = paramLayoutManager.getPosition(paramView1);
    k = paramLayoutManager.getPosition(paramView2);
    k = Math.abs(m - k);
    float f1 = j / (k + 1);
    float f2 = i;
    i = paramOrientationHelper.getStartAfterPadding();
    float f3 = (i - paramOrientationHelper.getDecoratedStart(paramView1));
    return Math.round(f2 * f1 + f3);
  }
  
  static int computeScrollExtent(RecyclerView.State paramState, OrientationHelper paramOrientationHelper, View paramView1, View paramView2, RecyclerView.LayoutManager paramLayoutManager, boolean paramBoolean) {
    if (paramLayoutManager.getChildCount() == 0 || paramState.getItemCount() == 0 || paramView1 == null || paramView2 == null)
      return 0; 
    if (!paramBoolean)
      return Math.abs(paramLayoutManager.getPosition(paramView1) - paramLayoutManager.getPosition(paramView2)) + 1; 
    int i = paramOrientationHelper.getDecoratedEnd(paramView2);
    int j = paramOrientationHelper.getDecoratedStart(paramView1);
    return Math.min(paramOrientationHelper.getTotalSpace(), i - j);
  }
  
  static int computeScrollRange(RecyclerView.State paramState, OrientationHelper paramOrientationHelper, View paramView1, View paramView2, RecyclerView.LayoutManager paramLayoutManager, boolean paramBoolean) {
    if (paramLayoutManager.getChildCount() == 0 || paramState.getItemCount() == 0 || paramView1 == null || paramView2 == null)
      return 0; 
    if (!paramBoolean)
      return paramState.getItemCount(); 
    int i = paramOrientationHelper.getDecoratedEnd(paramView2);
    int j = paramOrientationHelper.getDecoratedStart(paramView1);
    int k = paramLayoutManager.getPosition(paramView1);
    int m = paramLayoutManager.getPosition(paramView2);
    k = Math.abs(k - m);
    return (int)((i - j) / (k + 1) * paramState.getItemCount());
  }
}
