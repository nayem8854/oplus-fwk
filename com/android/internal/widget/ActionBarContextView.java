package com.android.internal.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.widget.ActionMenuPresenter;
import android.widget.ActionMenuView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.internal.R;
import com.android.internal.view.menu.MenuBuilder;
import com.android.internal.view.menu.MenuPresenter;

public class ActionBarContextView extends AbsActionBarView {
  private static final String TAG = "ActionBarContextView";
  
  private View mClose;
  
  private int mCloseItemLayout;
  
  private View mCustomView;
  
  private Drawable mSplitBackground;
  
  private CharSequence mSubtitle;
  
  private int mSubtitleStyleRes;
  
  private TextView mSubtitleView;
  
  private CharSequence mTitle;
  
  private LinearLayout mTitleLayout;
  
  private boolean mTitleOptional;
  
  private int mTitleStyleRes;
  
  private TextView mTitleView;
  
  public ActionBarContextView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public ActionBarContextView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16843668);
  }
  
  public ActionBarContextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public ActionBarContextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ActionMode, paramInt1, paramInt2);
    setBackground(typedArray.getDrawable(0));
    this.mTitleStyleRes = typedArray.getResourceId(2, 0);
    this.mSubtitleStyleRes = typedArray.getResourceId(3, 0);
    this.mContentHeight = typedArray.getLayoutDimension(1, 0);
    this.mSplitBackground = typedArray.getDrawable(4);
    this.mCloseItemLayout = typedArray.getResourceId(5, 17367075);
    typedArray.recycle();
  }
  
  public void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    if (this.mActionMenuPresenter != null) {
      this.mActionMenuPresenter.hideOverflowMenu();
      this.mActionMenuPresenter.hideSubMenus();
    } 
  }
  
  public void setSplitToolbar(boolean paramBoolean) {
    if (this.mSplitActionBar != paramBoolean) {
      if (this.mActionMenuPresenter != null) {
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-2, -1);
        if (!paramBoolean) {
          this.mMenuView = (ActionMenuView)this.mActionMenuPresenter.getMenuView(this);
          this.mMenuView.setBackground(null);
          ViewGroup viewGroup = (ViewGroup)this.mMenuView.getParent();
          if (viewGroup != null)
            viewGroup.removeView((View)this.mMenuView); 
          addView((View)this.mMenuView, layoutParams);
        } else {
          ActionMenuPresenter actionMenuPresenter = this.mActionMenuPresenter;
          int i = (getContext().getResources().getDisplayMetrics()).widthPixels;
          actionMenuPresenter.setWidthLimit(i, true);
          this.mActionMenuPresenter.setItemLimit(2147483647);
          layoutParams.width = -1;
          layoutParams.height = this.mContentHeight;
          this.mMenuView = (ActionMenuView)this.mActionMenuPresenter.getMenuView(this);
          this.mMenuView.setBackground(this.mSplitBackground);
          ViewGroup viewGroup = (ViewGroup)this.mMenuView.getParent();
          if (viewGroup != null)
            viewGroup.removeView((View)this.mMenuView); 
          this.mSplitView.addView((View)this.mMenuView, layoutParams);
        } 
      } 
      super.setSplitToolbar(paramBoolean);
    } 
  }
  
  public void setContentHeight(int paramInt) {
    this.mContentHeight = paramInt;
  }
  
  public void setCustomView(View paramView) {
    View view = this.mCustomView;
    if (view != null)
      removeView(view); 
    this.mCustomView = paramView;
    if (paramView != null) {
      LinearLayout linearLayout = this.mTitleLayout;
      if (linearLayout != null) {
        removeView((View)linearLayout);
        this.mTitleLayout = null;
      } 
    } 
    if (paramView != null)
      addView(paramView); 
    requestLayout();
  }
  
  public void setTitle(CharSequence paramCharSequence) {
    this.mTitle = paramCharSequence;
    initTitle();
  }
  
  public void setSubtitle(CharSequence paramCharSequence) {
    this.mSubtitle = paramCharSequence;
    initTitle();
  }
  
  public CharSequence getTitle() {
    return this.mTitle;
  }
  
  public CharSequence getSubtitle() {
    return this.mSubtitle;
  }
  
  private void initTitle() {
    if (this.mTitleLayout == null) {
      LayoutInflater layoutInflater = LayoutInflater.from(getContext());
      layoutInflater.inflate(17367070, this);
      LinearLayout linearLayout1 = (LinearLayout)getChildAt(getChildCount() - 1);
      this.mTitleView = (TextView)linearLayout1.findViewById(16908713);
      this.mSubtitleView = (TextView)this.mTitleLayout.findViewById(16908712);
      b = this.mTitleStyleRes;
      if (b != 0)
        this.mTitleView.setTextAppearance(b); 
      b = this.mSubtitleStyleRes;
      if (b != 0)
        this.mSubtitleView.setTextAppearance(b); 
    } 
    this.mTitleView.setText(this.mTitle);
    this.mSubtitleView.setText(this.mSubtitle);
    boolean bool = TextUtils.isEmpty(this.mTitle);
    int i = TextUtils.isEmpty(this.mSubtitle) ^ true;
    TextView textView = this.mSubtitleView;
    boolean bool1 = false;
    if (i != 0) {
      b = 0;
    } else {
      b = 8;
    } 
    textView.setVisibility(b);
    LinearLayout linearLayout = this.mTitleLayout;
    byte b = bool1;
    if ((bool ^ true) == 0)
      if (i != 0) {
        b = bool1;
      } else {
        b = 8;
      }  
    linearLayout.setVisibility(b);
    if (this.mTitleLayout.getParent() == null)
      addView((View)this.mTitleLayout); 
  }
  
  public void initForMode(ActionMode paramActionMode) {
    View view = this.mClose;
    if (view == null) {
      LayoutInflater layoutInflater = LayoutInflater.from(this.mContext);
      this.mClose = view = layoutInflater.inflate(this.mCloseItemLayout, this, false);
      addView(view);
    } else if (view.getParent() == null) {
      addView(this.mClose);
    } 
    view = this.mClose.findViewById(16908720);
    view.setOnClickListener((View.OnClickListener)new Object(this, paramActionMode));
    MenuBuilder menuBuilder = (MenuBuilder)paramActionMode.getMenu();
    if (this.mActionMenuPresenter != null)
      this.mActionMenuPresenter.dismissPopupMenus(); 
    this.mActionMenuPresenter = new ActionMenuPresenter(this.mContext);
    this.mActionMenuPresenter.setReserveOverflow(true);
    ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-2, -1);
    if (!this.mSplitActionBar) {
      menuBuilder.addMenuPresenter((MenuPresenter)this.mActionMenuPresenter, this.mPopupContext);
      this.mMenuView = (ActionMenuView)this.mActionMenuPresenter.getMenuView(this);
      this.mMenuView.setBackground(null);
      addView((View)this.mMenuView, layoutParams);
    } else {
      ActionMenuPresenter actionMenuPresenter = this.mActionMenuPresenter;
      int i = (getContext().getResources().getDisplayMetrics()).widthPixels;
      actionMenuPresenter.setWidthLimit(i, true);
      this.mActionMenuPresenter.setItemLimit(2147483647);
      layoutParams.width = -1;
      layoutParams.height = this.mContentHeight;
      menuBuilder.addMenuPresenter((MenuPresenter)this.mActionMenuPresenter, this.mPopupContext);
      this.mMenuView = (ActionMenuView)this.mActionMenuPresenter.getMenuView(this);
      this.mMenuView.setBackgroundDrawable(this.mSplitBackground);
      this.mSplitView.addView((View)this.mMenuView, layoutParams);
    } 
  }
  
  public void closeMode() {
    if (this.mClose == null) {
      killMode();
      return;
    } 
  }
  
  public void killMode() {
    removeAllViews();
    if (this.mSplitView != null)
      this.mSplitView.removeView((View)this.mMenuView); 
    this.mCustomView = null;
    this.mMenuView = null;
  }
  
  public boolean showOverflowMenu() {
    if (this.mActionMenuPresenter != null)
      return this.mActionMenuPresenter.showOverflowMenu(); 
    return false;
  }
  
  public boolean hideOverflowMenu() {
    if (this.mActionMenuPresenter != null)
      return this.mActionMenuPresenter.hideOverflowMenu(); 
    return false;
  }
  
  public boolean isOverflowMenuShowing() {
    if (this.mActionMenuPresenter != null)
      return this.mActionMenuPresenter.isOverflowMenuShowing(); 
    return false;
  }
  
  protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
    return (ViewGroup.LayoutParams)new ViewGroup.MarginLayoutParams(-1, -2);
  }
  
  public ViewGroup.LayoutParams generateLayoutParams(AttributeSet paramAttributeSet) {
    return (ViewGroup.LayoutParams)new ViewGroup.MarginLayoutParams(getContext(), paramAttributeSet);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    int i = View.MeasureSpec.getMode(paramInt1);
    int j = 1073741824;
    if (i == 1073741824) {
      i = View.MeasureSpec.getMode(paramInt2);
      if (i != 0) {
        int k = View.MeasureSpec.getSize(paramInt1);
        if (this.mContentHeight > 0) {
          i = this.mContentHeight;
        } else {
          i = View.MeasureSpec.getSize(paramInt2);
        } 
        int m = getPaddingTop() + getPaddingBottom();
        paramInt1 = k - getPaddingLeft() - getPaddingRight();
        int n = i - m;
        int i1 = View.MeasureSpec.makeMeasureSpec(n, -2147483648);
        View view2 = this.mClose;
        boolean bool = false;
        paramInt2 = paramInt1;
        if (view2 != null) {
          paramInt1 = measureChildView(view2, paramInt1, i1, 0);
          ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams)this.mClose.getLayoutParams();
          paramInt2 = paramInt1 - marginLayoutParams.leftMargin + marginLayoutParams.rightMargin;
        } 
        paramInt1 = paramInt2;
        if (this.mMenuView != null) {
          paramInt1 = paramInt2;
          if (this.mMenuView.getParent() == this)
            paramInt1 = measureChildView((View)this.mMenuView, paramInt2, i1, 0); 
        } 
        LinearLayout linearLayout = this.mTitleLayout;
        paramInt2 = paramInt1;
        if (linearLayout != null) {
          paramInt2 = paramInt1;
          if (this.mCustomView == null)
            if (this.mTitleOptional) {
              paramInt2 = View.MeasureSpec.makeSafeMeasureSpec(k, 0);
              this.mTitleLayout.measure(paramInt2, i1);
              int i2 = this.mTitleLayout.getMeasuredWidth();
              if (i2 <= paramInt1) {
                i1 = 1;
              } else {
                i1 = 0;
              } 
              paramInt2 = paramInt1;
              if (i1 != 0)
                paramInt2 = paramInt1 - i2; 
              linearLayout = this.mTitleLayout;
              if (i1 != 0) {
                paramInt1 = bool;
              } else {
                paramInt1 = 8;
              } 
              linearLayout.setVisibility(paramInt1);
            } else {
              paramInt2 = measureChildView((View)linearLayout, paramInt1, i1, 0);
            }  
        } 
        View view1 = this.mCustomView;
        if (view1 != null) {
          ViewGroup.LayoutParams layoutParams = view1.getLayoutParams();
          if (layoutParams.width != -2) {
            paramInt1 = 1073741824;
          } else {
            paramInt1 = Integer.MIN_VALUE;
          } 
          if (layoutParams.width >= 0)
            paramInt2 = Math.min(layoutParams.width, paramInt2); 
          if (layoutParams.height != -2) {
            i1 = j;
          } else {
            i1 = Integer.MIN_VALUE;
          } 
          if (layoutParams.height >= 0) {
            j = Math.min(layoutParams.height, n);
          } else {
            j = n;
          } 
          View view = this.mCustomView;
          paramInt1 = View.MeasureSpec.makeMeasureSpec(paramInt2, paramInt1);
          paramInt2 = View.MeasureSpec.makeMeasureSpec(j, i1);
          view.measure(paramInt1, paramInt2);
        } 
        if (this.mContentHeight <= 0) {
          i = 0;
          j = getChildCount();
          for (paramInt1 = 0; paramInt1 < j; paramInt1++, i = paramInt2) {
            view1 = getChildAt(paramInt1);
            i1 = view1.getMeasuredHeight() + m;
            paramInt2 = i;
            if (i1 > i)
              paramInt2 = i1; 
          } 
          setMeasuredDimension(k, i);
        } else {
          setMeasuredDimension(k, i);
        } 
        return;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(getClass().getSimpleName());
      stringBuilder1.append(" can only be used with android:layout_height=\"wrap_content\"");
      throw new IllegalStateException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(getClass().getSimpleName());
    stringBuilder.append(" can only be used with android:layout_width=\"match_parent\" (or fill_parent)");
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    int i;
    paramBoolean = isLayoutRtl();
    if (paramBoolean) {
      i = paramInt3 - paramInt1 - getPaddingRight();
    } else {
      i = getPaddingLeft();
    } 
    int j = getPaddingTop();
    int k = paramInt4 - paramInt2 - getPaddingTop() - getPaddingBottom();
    View view2 = this.mClose;
    if (view2 != null && view2.getVisibility() != 8) {
      ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams)this.mClose.getLayoutParams();
      if (paramBoolean) {
        paramInt4 = marginLayoutParams.rightMargin;
      } else {
        paramInt4 = marginLayoutParams.leftMargin;
      } 
      if (paramBoolean) {
        paramInt2 = marginLayoutParams.leftMargin;
      } else {
        paramInt2 = marginLayoutParams.rightMargin;
      } 
      paramInt4 = next(i, paramInt4, paramBoolean);
      i = positionChild(this.mClose, paramInt4, j, k, paramBoolean);
      paramInt2 = next(paramInt4 + i, paramInt2, paramBoolean);
    } else {
      paramInt2 = i;
    } 
    LinearLayout linearLayout = this.mTitleLayout;
    paramInt4 = paramInt2;
    if (linearLayout != null) {
      paramInt4 = paramInt2;
      if (this.mCustomView == null) {
        paramInt4 = paramInt2;
        if (linearLayout.getVisibility() != 8)
          paramInt4 = paramInt2 + positionChild((View)this.mTitleLayout, paramInt2, j, k, paramBoolean); 
      } 
    } 
    View view1 = this.mCustomView;
    if (view1 != null)
      positionChild(view1, paramInt4, j, k, paramBoolean); 
    if (paramBoolean) {
      paramInt1 = getPaddingLeft();
    } else {
      paramInt1 = paramInt3 - paramInt1 - getPaddingRight();
    } 
    if (this.mMenuView != null)
      positionChild((View)this.mMenuView, paramInt1, j, k, paramBoolean ^ true); 
  }
  
  public boolean shouldDelayChildPressedState() {
    return false;
  }
  
  public void onInitializeAccessibilityEventInternal(AccessibilityEvent paramAccessibilityEvent) {
    if (paramAccessibilityEvent.getEventType() == 32) {
      paramAccessibilityEvent.setSource((View)this);
      paramAccessibilityEvent.setClassName(getClass().getName());
      paramAccessibilityEvent.setPackageName(getContext().getPackageName());
      paramAccessibilityEvent.setContentDescription(this.mTitle);
    } else {
      super.onInitializeAccessibilityEventInternal(paramAccessibilityEvent);
    } 
  }
  
  public void setTitleOptional(boolean paramBoolean) {
    if (paramBoolean != this.mTitleOptional)
      requestLayout(); 
    this.mTitleOptional = paramBoolean;
  }
  
  public boolean isTitleOptional() {
    return this.mTitleOptional;
  }
}
