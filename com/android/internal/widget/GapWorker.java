package com.android.internal.widget;

import android.os.Trace;
import android.view.View;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;

final class GapWorker implements Runnable {
  static final ThreadLocal<GapWorker> sGapWorker = new ThreadLocal<>();
  
  ArrayList<RecyclerView> mRecyclerViews = new ArrayList<>();
  
  static class Task {
    public int distanceToItem;
    
    public boolean immediate;
    
    public int position;
    
    public RecyclerView view;
    
    public int viewVelocity;
    
    public void clear() {
      this.immediate = false;
      this.viewVelocity = 0;
      this.distanceToItem = 0;
      this.view = null;
      this.position = 0;
    }
  }
  
  private ArrayList<Task> mTasks = new ArrayList<>();
  
  class LayoutPrefetchRegistryImpl implements RecyclerView.LayoutManager.LayoutPrefetchRegistry {
    int mCount;
    
    int[] mPrefetchArray;
    
    int mPrefetchDx;
    
    int mPrefetchDy;
    
    void setPrefetchVector(int param1Int1, int param1Int2) {
      this.mPrefetchDx = param1Int1;
      this.mPrefetchDy = param1Int2;
    }
    
    void collectPrefetchPositionsFromView(RecyclerView param1RecyclerView, boolean param1Boolean) {
      this.mCount = 0;
      int[] arrayOfInt = this.mPrefetchArray;
      if (arrayOfInt != null)
        Arrays.fill(arrayOfInt, -1); 
      RecyclerView.LayoutManager layoutManager = param1RecyclerView.mLayout;
      if (param1RecyclerView.mAdapter != null && layoutManager != null)
        if (layoutManager.isItemPrefetchEnabled()) {
          if (param1Boolean) {
            if (!param1RecyclerView.mAdapterHelper.hasPendingUpdates())
              layoutManager.collectInitialPrefetchPositions(param1RecyclerView.mAdapter.getItemCount(), this); 
          } else if (!param1RecyclerView.hasPendingAdapterUpdates()) {
            layoutManager.collectAdjacentPrefetchPositions(this.mPrefetchDx, this.mPrefetchDy, param1RecyclerView.mState, this);
          } 
          if (this.mCount > layoutManager.mPrefetchMaxCountObserved) {
            layoutManager.mPrefetchMaxCountObserved = this.mCount;
            layoutManager.mPrefetchMaxObservedInInitialPrefetch = param1Boolean;
            param1RecyclerView.mRecycler.updateViewCacheSize();
          } 
        }  
    }
    
    public void addPosition(int param1Int1, int param1Int2) {
      if (param1Int2 >= 0) {
        int i = this.mCount * 2;
        int[] arrayOfInt = this.mPrefetchArray;
        if (arrayOfInt == null) {
          this.mPrefetchArray = arrayOfInt = new int[4];
          Arrays.fill(arrayOfInt, -1);
        } else if (i >= arrayOfInt.length) {
          arrayOfInt = this.mPrefetchArray;
          int[] arrayOfInt1 = new int[i * 2];
          System.arraycopy(arrayOfInt, 0, arrayOfInt1, 0, arrayOfInt.length);
        } 
        arrayOfInt = this.mPrefetchArray;
        arrayOfInt[i] = param1Int1;
        arrayOfInt[i + 1] = param1Int2;
        this.mCount++;
        return;
      } 
      throw new IllegalArgumentException("Pixel distance must be non-negative");
    }
    
    boolean lastPrefetchIncludedPosition(int param1Int) {
      if (this.mPrefetchArray != null) {
        int i = this.mCount;
        for (byte b = 0; b < i * 2; b += 2) {
          if (this.mPrefetchArray[b] == param1Int)
            return true; 
        } 
      } 
      return false;
    }
    
    void clearPrefetchPositions() {
      int[] arrayOfInt = this.mPrefetchArray;
      if (arrayOfInt != null)
        Arrays.fill(arrayOfInt, -1); 
    }
  }
  
  public void add(RecyclerView paramRecyclerView) {
    this.mRecyclerViews.add(paramRecyclerView);
  }
  
  public void remove(RecyclerView paramRecyclerView) {
    this.mRecyclerViews.remove(paramRecyclerView);
  }
  
  void postFromTraversal(RecyclerView paramRecyclerView, int paramInt1, int paramInt2) {
    if (paramRecyclerView.isAttachedToWindow())
      if (this.mPostTimeNs == 0L) {
        this.mPostTimeNs = paramRecyclerView.getNanoTime();
        paramRecyclerView.post(this);
      }  
    paramRecyclerView.mPrefetchRegistry.setPrefetchVector(paramInt1, paramInt2);
  }
  
  static Comparator<Task> sTaskComparator = new Comparator<Task>() {
      public int compare(GapWorker.Task param1Task1, GapWorker.Task param1Task2) {
        byte b2;
        RecyclerView recyclerView = param1Task1.view;
        byte b = 1;
        byte b1 = 1;
        if (recyclerView == null) {
          i = 1;
        } else {
          i = 0;
        } 
        if (param1Task2.view == null) {
          b2 = 1;
        } else {
          b2 = 0;
        } 
        if (i != b2) {
          if (param1Task1.view == null) {
            i = b1;
          } else {
            i = -1;
          } 
          return i;
        } 
        if (param1Task1.immediate != param1Task2.immediate) {
          i = b;
          if (param1Task1.immediate)
            i = -1; 
          return i;
        } 
        int i = param1Task2.viewVelocity - param1Task1.viewVelocity;
        if (i != 0)
          return i; 
        i = param1Task1.distanceToItem - param1Task2.distanceToItem;
        if (i != 0)
          return i; 
        return 0;
      }
    };
  
  long mFrameIntervalNs;
  
  long mPostTimeNs;
  
  private void buildTaskList() {
    int i = this.mRecyclerViews.size();
    int j = 0;
    byte b;
    for (b = 0; b < i; b++) {
      RecyclerView recyclerView = this.mRecyclerViews.get(b);
      recyclerView.mPrefetchRegistry.collectPrefetchPositionsFromView(recyclerView, false);
      j += recyclerView.mPrefetchRegistry.mCount;
    } 
    this.mTasks.ensureCapacity(j);
    j = 0;
    for (b = 0; b < i; b++) {
      RecyclerView recyclerView = this.mRecyclerViews.get(b);
      LayoutPrefetchRegistryImpl layoutPrefetchRegistryImpl = recyclerView.mPrefetchRegistry;
      int k = Math.abs(layoutPrefetchRegistryImpl.mPrefetchDx), m = layoutPrefetchRegistryImpl.mPrefetchDy;
      m = k + Math.abs(m);
      for (k = 0; k < layoutPrefetchRegistryImpl.mCount * 2; k += 2) {
        Task task;
        boolean bool;
        if (j >= this.mTasks.size()) {
          task = new Task();
          this.mTasks.add(task);
        } else {
          task = this.mTasks.get(j);
        } 
        int n = layoutPrefetchRegistryImpl.mPrefetchArray[k + 1];
        if (n <= m) {
          bool = true;
        } else {
          bool = false;
        } 
        task.immediate = bool;
        task.viewVelocity = m;
        task.distanceToItem = n;
        task.view = recyclerView;
        task.position = layoutPrefetchRegistryImpl.mPrefetchArray[k];
        j++;
      } 
    } 
    Collections.sort(this.mTasks, sTaskComparator);
  }
  
  static boolean isPrefetchPositionAttached(RecyclerView paramRecyclerView, int paramInt) {
    int i = paramRecyclerView.mChildHelper.getUnfilteredChildCount();
    for (byte b = 0; b < i; b++) {
      View view = paramRecyclerView.mChildHelper.getUnfilteredChildAt(b);
      RecyclerView.ViewHolder viewHolder = RecyclerView.getChildViewHolderInt(view);
      if (viewHolder.mPosition == paramInt && !viewHolder.isInvalid())
        return true; 
    } 
    return false;
  }
  
  private RecyclerView.ViewHolder prefetchPositionWithDeadline(RecyclerView paramRecyclerView, int paramInt, long paramLong) {
    if (isPrefetchPositionAttached(paramRecyclerView, paramInt))
      return null; 
    RecyclerView.Recycler recycler = paramRecyclerView.mRecycler;
    RecyclerView.ViewHolder viewHolder = recycler.tryGetViewHolderForPositionByDeadline(paramInt, false, paramLong);
    if (viewHolder != null)
      if (viewHolder.isBound()) {
        recycler.recycleView(viewHolder.itemView);
      } else {
        recycler.addViewHolderToRecycledViewPool(viewHolder, false);
      }  
    return viewHolder;
  }
  
  private void prefetchInnerRecyclerViewWithDeadline(RecyclerView paramRecyclerView, long paramLong) {
    if (paramRecyclerView == null)
      return; 
    if (paramRecyclerView.mDataSetHasChangedAfterLayout) {
      ChildHelper childHelper = paramRecyclerView.mChildHelper;
      if (childHelper.getUnfilteredChildCount() != 0)
        paramRecyclerView.removeAndRecycleViews(); 
    } 
    LayoutPrefetchRegistryImpl layoutPrefetchRegistryImpl = paramRecyclerView.mPrefetchRegistry;
    layoutPrefetchRegistryImpl.collectPrefetchPositionsFromView(paramRecyclerView, true);
    if (layoutPrefetchRegistryImpl.mCount != 0)
      try {
        Trace.beginSection("RV Nested Prefetch");
        paramRecyclerView.mState.prepareForNestedPrefetch(paramRecyclerView.mAdapter);
        for (byte b = 0; b < layoutPrefetchRegistryImpl.mCount * 2; b += 2) {
          int i = layoutPrefetchRegistryImpl.mPrefetchArray[b];
          prefetchPositionWithDeadline(paramRecyclerView, i, paramLong);
        } 
      } finally {
        Trace.endSection();
      }  
  }
  
  private void flushTaskWithDeadline(Task paramTask, long paramLong) {
    long l;
    if (paramTask.immediate) {
      l = Long.MAX_VALUE;
    } else {
      l = paramLong;
    } 
    RecyclerView.ViewHolder viewHolder = prefetchPositionWithDeadline(paramTask.view, paramTask.position, l);
    if (viewHolder != null && viewHolder.mNestedRecyclerView != null)
      prefetchInnerRecyclerViewWithDeadline(viewHolder.mNestedRecyclerView.get(), paramLong); 
  }
  
  private void flushTasksWithDeadline(long paramLong) {
    for (byte b = 0; b < this.mTasks.size(); b++) {
      Task task = this.mTasks.get(b);
      if (task.view == null)
        break; 
      flushTaskWithDeadline(task, paramLong);
      task.clear();
    } 
  }
  
  void prefetch(long paramLong) {
    buildTaskList();
    flushTasksWithDeadline(paramLong);
  }
  
  public void run() {
    try {
      Trace.beginSection("RV Prefetch");
      boolean bool = this.mRecyclerViews.isEmpty();
      if (bool)
        return; 
      TimeUnit timeUnit = TimeUnit.MILLISECONDS;
      ArrayList<RecyclerView> arrayList = this.mRecyclerViews;
      long l1 = ((RecyclerView)arrayList.get(0)).getDrawingTime();
      l1 = timeUnit.toNanos(l1);
      if (l1 == 0L)
        return; 
      long l2 = this.mFrameIntervalNs;
      prefetch(l2 + l1);
      return;
    } finally {
      this.mPostTimeNs = 0L;
      Trace.endSection();
    } 
  }
}
