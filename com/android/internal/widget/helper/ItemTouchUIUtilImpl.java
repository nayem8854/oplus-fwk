package com.android.internal.widget.helper;

import android.graphics.Canvas;
import android.view.View;
import com.android.internal.widget.RecyclerView;

class ItemTouchUIUtilImpl implements ItemTouchUIUtil {
  public void onDraw(Canvas paramCanvas, RecyclerView paramRecyclerView, View paramView, float paramFloat1, float paramFloat2, int paramInt, boolean paramBoolean) {
    if (paramBoolean) {
      Object object = paramView.getTag(16909103);
      if (object == null) {
        float f1 = paramView.getElevation();
        float f2 = findMaxElevation(paramRecyclerView, paramView);
        paramView.setElevation(f2 + 1.0F);
        paramView.setTag(16909103, Float.valueOf(f1));
      } 
    } 
    paramView.setTranslationX(paramFloat1);
    paramView.setTranslationY(paramFloat2);
  }
  
  private float findMaxElevation(RecyclerView paramRecyclerView, View paramView) {
    int i = paramRecyclerView.getChildCount();
    float f = 0.0F;
    for (byte b = 0; b < i; b++, f = f1) {
      float f1;
      View view = paramRecyclerView.getChildAt(b);
      if (view == paramView) {
        f1 = f;
      } else {
        float f2 = view.getElevation();
        f1 = f;
        if (f2 > f)
          f1 = f2; 
      } 
    } 
    return f;
  }
  
  public void clearView(View paramView) {
    Object object = paramView.getTag(16909103);
    if (object != null && object instanceof Float)
      paramView.setElevation(((Float)object).floatValue()); 
    paramView.setTag(16909103, null);
    paramView.setTranslationX(0.0F);
    paramView.setTranslationY(0.0F);
  }
  
  public void onSelected(View paramView) {}
  
  public void onDrawOver(Canvas paramCanvas, RecyclerView paramRecyclerView, View paramView, float paramFloat1, float paramFloat2, int paramInt, boolean paramBoolean) {}
}
