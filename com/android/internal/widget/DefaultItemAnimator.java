package com.android.internal.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewPropertyAnimator;
import java.util.ArrayList;
import java.util.List;

public class DefaultItemAnimator extends SimpleItemAnimator {
  private ArrayList<RecyclerView.ViewHolder> mPendingRemovals = new ArrayList<>();
  
  private ArrayList<RecyclerView.ViewHolder> mPendingAdditions = new ArrayList<>();
  
  private ArrayList<MoveInfo> mPendingMoves = new ArrayList<>();
  
  private ArrayList<ChangeInfo> mPendingChanges = new ArrayList<>();
  
  ArrayList<ArrayList<RecyclerView.ViewHolder>> mAdditionsList = new ArrayList<>();
  
  ArrayList<ArrayList<MoveInfo>> mMovesList = new ArrayList<>();
  
  ArrayList<ArrayList<ChangeInfo>> mChangesList = new ArrayList<>();
  
  ArrayList<RecyclerView.ViewHolder> mAddAnimations = new ArrayList<>();
  
  ArrayList<RecyclerView.ViewHolder> mMoveAnimations = new ArrayList<>();
  
  ArrayList<RecyclerView.ViewHolder> mRemoveAnimations = new ArrayList<>();
  
  ArrayList<RecyclerView.ViewHolder> mChangeAnimations = new ArrayList<>();
  
  private static final boolean DEBUG = false;
  
  private static TimeInterpolator sDefaultInterpolator;
  
  class MoveInfo {
    public int fromX;
    
    public int fromY;
    
    public RecyclerView.ViewHolder holder;
    
    public int toX;
    
    public int toY;
    
    MoveInfo(DefaultItemAnimator this$0, int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      this.holder = (RecyclerView.ViewHolder)this$0;
      this.fromX = param1Int1;
      this.fromY = param1Int2;
      this.toX = param1Int3;
      this.toY = param1Int4;
    }
  }
  
  class ChangeInfo {
    public int fromX;
    
    public int fromY;
    
    public RecyclerView.ViewHolder newHolder;
    
    public RecyclerView.ViewHolder oldHolder;
    
    public int toX;
    
    public int toY;
    
    private ChangeInfo(DefaultItemAnimator this$0, RecyclerView.ViewHolder param1ViewHolder1) {
      this.oldHolder = (RecyclerView.ViewHolder)this$0;
      this.newHolder = param1ViewHolder1;
    }
    
    ChangeInfo(RecyclerView.ViewHolder param1ViewHolder1, int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      this((RecyclerView.ViewHolder)this$0, param1ViewHolder1);
      this.fromX = param1Int1;
      this.fromY = param1Int2;
      this.toX = param1Int3;
      this.toY = param1Int4;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ChangeInfo{oldHolder=");
      stringBuilder.append(this.oldHolder);
      stringBuilder.append(", newHolder=");
      stringBuilder.append(this.newHolder);
      stringBuilder.append(", fromX=");
      stringBuilder.append(this.fromX);
      stringBuilder.append(", fromY=");
      stringBuilder.append(this.fromY);
      stringBuilder.append(", toX=");
      stringBuilder.append(this.toX);
      stringBuilder.append(", toY=");
      stringBuilder.append(this.toY);
      stringBuilder.append('}');
      return stringBuilder.toString();
    }
  }
  
  public void runPendingAnimations() {
    int i = this.mPendingRemovals.isEmpty() ^ true;
    int j = this.mPendingMoves.isEmpty() ^ true;
    int k = this.mPendingChanges.isEmpty() ^ true;
    int m = this.mPendingAdditions.isEmpty() ^ true;
    if (i == 0 && j == 0 && m == 0 && k == 0)
      return; 
    for (RecyclerView.ViewHolder viewHolder : this.mPendingRemovals)
      animateRemoveImpl(viewHolder); 
    this.mPendingRemovals.clear();
    if (j != 0) {
      ArrayList<MoveInfo> arrayList = new ArrayList();
      arrayList.addAll(this.mPendingMoves);
      this.mMovesList.add(arrayList);
      this.mPendingMoves.clear();
      Object object = new Object(this, arrayList);
      if (i != 0) {
        View view = ((MoveInfo)arrayList.get(0)).holder.itemView;
        view.postOnAnimationDelayed((Runnable)object, getRemoveDuration());
      } else {
        object.run();
      } 
    } 
    if (k != 0) {
      ArrayList<ChangeInfo> arrayList = new ArrayList();
      arrayList.addAll(this.mPendingChanges);
      this.mChangesList.add(arrayList);
      this.mPendingChanges.clear();
      Object object = new Object(this, arrayList);
      if (i != 0) {
        RecyclerView.ViewHolder viewHolder = ((ChangeInfo)arrayList.get(0)).oldHolder;
        viewHolder.itemView.postOnAnimationDelayed((Runnable)object, getRemoveDuration());
      } else {
        object.run();
      } 
    } 
    if (m != 0) {
      ArrayList<RecyclerView.ViewHolder> arrayList = new ArrayList();
      arrayList.addAll(this.mPendingAdditions);
      this.mAdditionsList.add(arrayList);
      this.mPendingAdditions.clear();
      Object object = new Object(this, arrayList);
      if (i != 0 || j != 0 || k != 0) {
        long l2, l1 = 0L;
        if (i != 0) {
          l2 = getRemoveDuration();
        } else {
          l2 = 0L;
        } 
        if (j != 0) {
          l3 = getMoveDuration();
        } else {
          l3 = 0L;
        } 
        if (k != 0)
          l1 = getChangeDuration(); 
        long l3 = Math.max(l3, l1);
        View view = ((RecyclerView.ViewHolder)arrayList.get(0)).itemView;
        view.postOnAnimationDelayed((Runnable)object, l3 + l2);
        return;
      } 
      object.run();
    } 
  }
  
  public boolean animateRemove(RecyclerView.ViewHolder paramViewHolder) {
    resetAnimation(paramViewHolder);
    this.mPendingRemovals.add(paramViewHolder);
    return true;
  }
  
  private void animateRemoveImpl(final RecyclerView.ViewHolder holder) {
    final View view = holder.itemView;
    final ViewPropertyAnimator animation = view.animate();
    this.mRemoveAnimations.add(holder);
    ViewPropertyAnimator viewPropertyAnimator1 = viewPropertyAnimator2.setDuration(getRemoveDuration()).alpha(0.0F).setListener((Animator.AnimatorListener)new AnimatorListenerAdapter() {
          final DefaultItemAnimator this$0;
          
          final ViewPropertyAnimator val$animation;
          
          final RecyclerView.ViewHolder val$holder;
          
          final View val$view;
          
          public void onAnimationStart(Animator param1Animator) {
            DefaultItemAnimator.this.dispatchRemoveStarting(holder);
          }
          
          public void onAnimationEnd(Animator param1Animator) {
            animation.setListener(null);
            view.setAlpha(1.0F);
            DefaultItemAnimator.this.dispatchRemoveFinished(holder);
            DefaultItemAnimator.this.mRemoveAnimations.remove(holder);
            DefaultItemAnimator.this.dispatchFinishedWhenDone();
          }
        });
    viewPropertyAnimator1.start();
  }
  
  public boolean animateAdd(RecyclerView.ViewHolder paramViewHolder) {
    resetAnimation(paramViewHolder);
    paramViewHolder.itemView.setAlpha(0.0F);
    this.mPendingAdditions.add(paramViewHolder);
    return true;
  }
  
  void animateAddImpl(final RecyclerView.ViewHolder holder) {
    final View view = holder.itemView;
    final ViewPropertyAnimator animation = view.animate();
    this.mAddAnimations.add(holder);
    ViewPropertyAnimator viewPropertyAnimator3 = viewPropertyAnimator2.alpha(1.0F).setDuration(getAddDuration());
    AnimatorListenerAdapter animatorListenerAdapter = new AnimatorListenerAdapter() {
        final DefaultItemAnimator this$0;
        
        final ViewPropertyAnimator val$animation;
        
        final RecyclerView.ViewHolder val$holder;
        
        final View val$view;
        
        public void onAnimationStart(Animator param1Animator) {
          DefaultItemAnimator.this.dispatchAddStarting(holder);
        }
        
        public void onAnimationCancel(Animator param1Animator) {
          view.setAlpha(1.0F);
        }
        
        public void onAnimationEnd(Animator param1Animator) {
          animation.setListener(null);
          DefaultItemAnimator.this.dispatchAddFinished(holder);
          DefaultItemAnimator.this.mAddAnimations.remove(holder);
          DefaultItemAnimator.this.dispatchFinishedWhenDone();
        }
      };
    ViewPropertyAnimator viewPropertyAnimator1 = viewPropertyAnimator3.setListener((Animator.AnimatorListener)animatorListenerAdapter);
    viewPropertyAnimator1.start();
  }
  
  public boolean animateMove(RecyclerView.ViewHolder paramViewHolder, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    View view = paramViewHolder.itemView;
    paramInt1 = (int)(paramInt1 + paramViewHolder.itemView.getTranslationX());
    paramInt2 = (int)(paramInt2 + paramViewHolder.itemView.getTranslationY());
    resetAnimation(paramViewHolder);
    int i = paramInt3 - paramInt1;
    int j = paramInt4 - paramInt2;
    if (i == 0 && j == 0) {
      dispatchMoveFinished(paramViewHolder);
      return false;
    } 
    if (i != 0)
      view.setTranslationX(-i); 
    if (j != 0)
      view.setTranslationY(-j); 
    this.mPendingMoves.add(new MoveInfo(paramViewHolder, paramInt1, paramInt2, paramInt3, paramInt4));
    return true;
  }
  
  void animateMoveImpl(final RecyclerView.ViewHolder holder, final int deltaX, final int deltaY, int paramInt3, int paramInt4) {
    final View view = holder.itemView;
    deltaX = paramInt3 - deltaX;
    deltaY = paramInt4 - deltaY;
    if (deltaX != 0)
      view.animate().translationX(0.0F); 
    if (deltaY != 0)
      view.animate().translationY(0.0F); 
    final ViewPropertyAnimator animation = view.animate();
    this.mMoveAnimations.add(holder);
    ViewPropertyAnimator viewPropertyAnimator1 = viewPropertyAnimator2.setDuration(getMoveDuration()).setListener((Animator.AnimatorListener)new AnimatorListenerAdapter() {
          final DefaultItemAnimator this$0;
          
          final ViewPropertyAnimator val$animation;
          
          final int val$deltaX;
          
          final int val$deltaY;
          
          final RecyclerView.ViewHolder val$holder;
          
          final View val$view;
          
          public void onAnimationStart(Animator param1Animator) {
            DefaultItemAnimator.this.dispatchMoveStarting(holder);
          }
          
          public void onAnimationCancel(Animator param1Animator) {
            if (deltaX != 0)
              view.setTranslationX(0.0F); 
            if (deltaY != 0)
              view.setTranslationY(0.0F); 
          }
          
          public void onAnimationEnd(Animator param1Animator) {
            animation.setListener(null);
            DefaultItemAnimator.this.dispatchMoveFinished(holder);
            DefaultItemAnimator.this.mMoveAnimations.remove(holder);
            DefaultItemAnimator.this.dispatchFinishedWhenDone();
          }
        });
    viewPropertyAnimator1.start();
  }
  
  public boolean animateChange(RecyclerView.ViewHolder paramViewHolder1, RecyclerView.ViewHolder paramViewHolder2, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (paramViewHolder1 == paramViewHolder2)
      return animateMove(paramViewHolder1, paramInt1, paramInt2, paramInt3, paramInt4); 
    float f1 = paramViewHolder1.itemView.getTranslationX();
    float f2 = paramViewHolder1.itemView.getTranslationY();
    float f3 = paramViewHolder1.itemView.getAlpha();
    resetAnimation(paramViewHolder1);
    int i = (int)((paramInt3 - paramInt1) - f1);
    int j = (int)((paramInt4 - paramInt2) - f2);
    paramViewHolder1.itemView.setTranslationX(f1);
    paramViewHolder1.itemView.setTranslationY(f2);
    paramViewHolder1.itemView.setAlpha(f3);
    if (paramViewHolder2 != null) {
      resetAnimation(paramViewHolder2);
      paramViewHolder2.itemView.setTranslationX(-i);
      paramViewHolder2.itemView.setTranslationY(-j);
      paramViewHolder2.itemView.setAlpha(0.0F);
    } 
    this.mPendingChanges.add(new ChangeInfo(paramViewHolder2, paramInt1, paramInt2, paramInt3, paramInt4));
    return true;
  }
  
  void animateChangeImpl(final ChangeInfo changeInfo) {
    final View view;
    RecyclerView.ViewHolder viewHolder1 = changeInfo.oldHolder;
    final View newView = null;
    if (viewHolder1 == null) {
      viewHolder1 = null;
    } else {
      view1 = viewHolder1.itemView;
    } 
    RecyclerView.ViewHolder viewHolder2 = changeInfo.newHolder;
    if (viewHolder2 != null)
      view2 = viewHolder2.itemView; 
    if (view1 != null) {
      final ViewPropertyAnimator newViewAnimation = view1.animate();
      long l = getChangeDuration();
      viewPropertyAnimator2 = viewPropertyAnimator2.setDuration(l);
      this.mChangeAnimations.add(changeInfo.oldHolder);
      viewPropertyAnimator2.translationX((changeInfo.toX - changeInfo.fromX));
      viewPropertyAnimator2.translationY((changeInfo.toY - changeInfo.fromY));
      ViewPropertyAnimator viewPropertyAnimator1 = viewPropertyAnimator2.alpha(0.0F).setListener((Animator.AnimatorListener)new AnimatorListenerAdapter() {
            final DefaultItemAnimator this$0;
            
            final DefaultItemAnimator.ChangeInfo val$changeInfo;
            
            final ViewPropertyAnimator val$oldViewAnim;
            
            final View val$view;
            
            public void onAnimationStart(Animator param1Animator) {
              DefaultItemAnimator.this.dispatchChangeStarting(changeInfo.oldHolder, true);
            }
            
            public void onAnimationEnd(Animator param1Animator) {
              oldViewAnim.setListener(null);
              view.setAlpha(1.0F);
              view.setTranslationX(0.0F);
              view.setTranslationY(0.0F);
              DefaultItemAnimator.this.dispatchChangeFinished(changeInfo.oldHolder, true);
              DefaultItemAnimator.this.mChangeAnimations.remove(changeInfo.oldHolder);
              DefaultItemAnimator.this.dispatchFinishedWhenDone();
            }
          });
      viewPropertyAnimator1.start();
    } 
    if (view2 != null) {
      final ViewPropertyAnimator newViewAnimation = view2.animate();
      this.mChangeAnimations.add(changeInfo.newHolder);
      ViewPropertyAnimator viewPropertyAnimator3 = viewPropertyAnimator2.translationX(0.0F).translationY(0.0F).setDuration(getChangeDuration());
      ViewPropertyAnimator viewPropertyAnimator1 = viewPropertyAnimator3.alpha(1.0F).setListener((Animator.AnimatorListener)new AnimatorListenerAdapter() {
            final DefaultItemAnimator this$0;
            
            final DefaultItemAnimator.ChangeInfo val$changeInfo;
            
            final View val$newView;
            
            final ViewPropertyAnimator val$newViewAnimation;
            
            public void onAnimationStart(Animator param1Animator) {
              DefaultItemAnimator.this.dispatchChangeStarting(changeInfo.newHolder, false);
            }
            
            public void onAnimationEnd(Animator param1Animator) {
              newViewAnimation.setListener(null);
              newView.setAlpha(1.0F);
              newView.setTranslationX(0.0F);
              newView.setTranslationY(0.0F);
              DefaultItemAnimator.this.dispatchChangeFinished(changeInfo.newHolder, false);
              DefaultItemAnimator.this.mChangeAnimations.remove(changeInfo.newHolder);
              DefaultItemAnimator.this.dispatchFinishedWhenDone();
            }
          });
      viewPropertyAnimator1.start();
    } 
  }
  
  private void endChangeAnimation(List<ChangeInfo> paramList, RecyclerView.ViewHolder paramViewHolder) {
    for (int i = paramList.size() - 1; i >= 0; i--) {
      ChangeInfo changeInfo = paramList.get(i);
      if (endChangeAnimationIfNecessary(changeInfo, paramViewHolder) && 
        changeInfo.oldHolder == null && changeInfo.newHolder == null)
        paramList.remove(changeInfo); 
    } 
  }
  
  private void endChangeAnimationIfNecessary(ChangeInfo paramChangeInfo) {
    if (paramChangeInfo.oldHolder != null)
      endChangeAnimationIfNecessary(paramChangeInfo, paramChangeInfo.oldHolder); 
    if (paramChangeInfo.newHolder != null)
      endChangeAnimationIfNecessary(paramChangeInfo, paramChangeInfo.newHolder); 
  }
  
  private boolean endChangeAnimationIfNecessary(ChangeInfo paramChangeInfo, RecyclerView.ViewHolder paramViewHolder) {
    boolean bool = false;
    if (paramChangeInfo.newHolder == paramViewHolder) {
      paramChangeInfo.newHolder = null;
    } else {
      if (paramChangeInfo.oldHolder == paramViewHolder) {
        paramChangeInfo.oldHolder = null;
        bool = true;
        paramViewHolder.itemView.setAlpha(1.0F);
        paramViewHolder.itemView.setTranslationX(0.0F);
        paramViewHolder.itemView.setTranslationY(0.0F);
        dispatchChangeFinished(paramViewHolder, bool);
        return true;
      } 
      return false;
    } 
    paramViewHolder.itemView.setAlpha(1.0F);
    paramViewHolder.itemView.setTranslationX(0.0F);
    paramViewHolder.itemView.setTranslationY(0.0F);
    dispatchChangeFinished(paramViewHolder, bool);
    return true;
  }
  
  public void endAnimation(RecyclerView.ViewHolder paramViewHolder) {
    View view = paramViewHolder.itemView;
    view.animate().cancel();
    int i;
    for (i = this.mPendingMoves.size() - 1; i >= 0; i--) {
      MoveInfo moveInfo = this.mPendingMoves.get(i);
      if (moveInfo.holder == paramViewHolder) {
        view.setTranslationY(0.0F);
        view.setTranslationX(0.0F);
        dispatchMoveFinished(paramViewHolder);
        this.mPendingMoves.remove(i);
      } 
    } 
    endChangeAnimation(this.mPendingChanges, paramViewHolder);
    if (this.mPendingRemovals.remove(paramViewHolder)) {
      view.setAlpha(1.0F);
      dispatchRemoveFinished(paramViewHolder);
    } 
    if (this.mPendingAdditions.remove(paramViewHolder)) {
      view.setAlpha(1.0F);
      dispatchAddFinished(paramViewHolder);
    } 
    for (i = this.mChangesList.size() - 1; i >= 0; i--) {
      ArrayList<ChangeInfo> arrayList = this.mChangesList.get(i);
      endChangeAnimation(arrayList, paramViewHolder);
      if (arrayList.isEmpty())
        this.mChangesList.remove(i); 
    } 
    for (i = this.mMovesList.size() - 1; i >= 0; i--) {
      ArrayList<MoveInfo> arrayList = this.mMovesList.get(i);
      for (int j = arrayList.size() - 1; j >= 0; j--) {
        MoveInfo moveInfo = arrayList.get(j);
        if (moveInfo.holder == paramViewHolder) {
          view.setTranslationY(0.0F);
          view.setTranslationX(0.0F);
          dispatchMoveFinished(paramViewHolder);
          arrayList.remove(j);
          if (arrayList.isEmpty())
            this.mMovesList.remove(i); 
          break;
        } 
      } 
    } 
    for (i = this.mAdditionsList.size() - 1; i >= 0; i--) {
      ArrayList arrayList = this.mAdditionsList.get(i);
      if (arrayList.remove(paramViewHolder)) {
        view.setAlpha(1.0F);
        dispatchAddFinished(paramViewHolder);
        if (arrayList.isEmpty())
          this.mAdditionsList.remove(i); 
      } 
    } 
    this.mRemoveAnimations.remove(paramViewHolder);
    this.mAddAnimations.remove(paramViewHolder);
    this.mChangeAnimations.remove(paramViewHolder);
    this.mMoveAnimations.remove(paramViewHolder);
    dispatchFinishedWhenDone();
  }
  
  private void resetAnimation(RecyclerView.ViewHolder paramViewHolder) {
    if (sDefaultInterpolator == null)
      sDefaultInterpolator = (new ValueAnimator()).getInterpolator(); 
    paramViewHolder.itemView.animate().setInterpolator(sDefaultInterpolator);
    endAnimation(paramViewHolder);
  }
  
  public boolean isRunning() {
    if (this.mPendingAdditions.isEmpty()) {
      ArrayList<ChangeInfo> arrayList = this.mPendingChanges;
      if (arrayList.isEmpty()) {
        ArrayList<MoveInfo> arrayList1 = this.mPendingMoves;
        if (arrayList1.isEmpty()) {
          ArrayList<RecyclerView.ViewHolder> arrayList2 = this.mPendingRemovals;
          if (arrayList2.isEmpty()) {
            arrayList2 = this.mMoveAnimations;
            if (arrayList2.isEmpty()) {
              arrayList2 = this.mRemoveAnimations;
              if (arrayList2.isEmpty()) {
                arrayList2 = this.mAddAnimations;
                if (arrayList2.isEmpty()) {
                  arrayList2 = this.mChangeAnimations;
                  if (arrayList2.isEmpty()) {
                    ArrayList<ArrayList<MoveInfo>> arrayList3 = this.mMovesList;
                    if (arrayList3.isEmpty()) {
                      ArrayList<ArrayList<RecyclerView.ViewHolder>> arrayList4 = this.mAdditionsList;
                      if (arrayList4.isEmpty()) {
                        ArrayList<ArrayList<ChangeInfo>> arrayList5 = this.mChangesList;
                        return !arrayList5.isEmpty();
                      } 
                    } 
                  } 
                } 
              } 
            } 
          } 
        } 
      } 
    } 
    return true;
  }
  
  void dispatchFinishedWhenDone() {
    if (!isRunning())
      dispatchAnimationsFinished(); 
  }
  
  public void endAnimations() {
    int i = this.mPendingMoves.size();
    for (; --i >= 0; i--) {
      MoveInfo moveInfo = this.mPendingMoves.get(i);
      View view = moveInfo.holder.itemView;
      view.setTranslationY(0.0F);
      view.setTranslationX(0.0F);
      dispatchMoveFinished(moveInfo.holder);
      this.mPendingMoves.remove(i);
    } 
    i = this.mPendingRemovals.size();
    for (; --i >= 0; i--) {
      RecyclerView.ViewHolder viewHolder = this.mPendingRemovals.get(i);
      dispatchRemoveFinished(viewHolder);
      this.mPendingRemovals.remove(i);
    } 
    i = this.mPendingAdditions.size();
    for (; --i >= 0; i--) {
      RecyclerView.ViewHolder viewHolder = this.mPendingAdditions.get(i);
      viewHolder.itemView.setAlpha(1.0F);
      dispatchAddFinished(viewHolder);
      this.mPendingAdditions.remove(i);
    } 
    i = this.mPendingChanges.size();
    for (; --i >= 0; i--)
      endChangeAnimationIfNecessary(this.mPendingChanges.get(i)); 
    this.mPendingChanges.clear();
    if (!isRunning())
      return; 
    i = this.mMovesList.size();
    for (; --i >= 0; i--) {
      ArrayList<MoveInfo> arrayList = this.mMovesList.get(i);
      int j = arrayList.size();
      for (; --j >= 0; j--) {
        MoveInfo moveInfo = arrayList.get(j);
        RecyclerView.ViewHolder viewHolder = moveInfo.holder;
        View view = viewHolder.itemView;
        view.setTranslationY(0.0F);
        view.setTranslationX(0.0F);
        dispatchMoveFinished(moveInfo.holder);
        arrayList.remove(j);
        if (arrayList.isEmpty())
          this.mMovesList.remove(arrayList); 
      } 
    } 
    i = this.mAdditionsList.size();
    for (; --i >= 0; i--) {
      ArrayList<RecyclerView.ViewHolder> arrayList = this.mAdditionsList.get(i);
      int j = arrayList.size();
      for (; --j >= 0; j--) {
        RecyclerView.ViewHolder viewHolder = arrayList.get(j);
        View view = viewHolder.itemView;
        view.setAlpha(1.0F);
        dispatchAddFinished(viewHolder);
        arrayList.remove(j);
        if (arrayList.isEmpty())
          this.mAdditionsList.remove(arrayList); 
      } 
    } 
    i = this.mChangesList.size();
    for (; --i >= 0; i--) {
      ArrayList<ChangeInfo> arrayList = this.mChangesList.get(i);
      int j = arrayList.size();
      for (; --j >= 0; j--) {
        endChangeAnimationIfNecessary(arrayList.get(j));
        if (arrayList.isEmpty())
          this.mChangesList.remove(arrayList); 
      } 
    } 
    cancelAll(this.mRemoveAnimations);
    cancelAll(this.mMoveAnimations);
    cancelAll(this.mAddAnimations);
    cancelAll(this.mChangeAnimations);
    dispatchAnimationsFinished();
  }
  
  void cancelAll(List<RecyclerView.ViewHolder> paramList) {
    for (int i = paramList.size() - 1; i >= 0; i--)
      ((RecyclerView.ViewHolder)paramList.get(i)).itemView.animate().cancel(); 
  }
  
  public boolean canReuseUpdatedViewHolder(RecyclerView.ViewHolder paramViewHolder, List<Object> paramList) {
    return (!paramList.isEmpty() || super.canReuseUpdatedViewHolder(paramViewHolder, paramList));
  }
}
