package com.android.internal.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.LinearLayout;
import com.android.internal.R;

public class WeightedLinearLayout extends LinearLayout {
  private float mMajorWeightMax;
  
  private float mMajorWeightMin;
  
  private float mMinorWeightMax;
  
  private float mMinorWeightMin;
  
  public WeightedLinearLayout(Context paramContext) {
    super(paramContext);
  }
  
  public WeightedLinearLayout(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    int[] arrayOfInt = R.styleable.WeightedLinearLayout;
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, arrayOfInt);
    this.mMajorWeightMin = typedArray.getFloat(1, 0.0F);
    this.mMinorWeightMin = typedArray.getFloat(3, 0.0F);
    this.mMajorWeightMax = typedArray.getFloat(0, 0.0F);
    this.mMinorWeightMax = typedArray.getFloat(2, 0.0F);
    typedArray.recycle();
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    float f1, f2;
    DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
    int i = displayMetrics.widthPixels;
    if (i < displayMetrics.heightPixels) {
      j = 1;
    } else {
      j = 0;
    } 
    int k = View.MeasureSpec.getMode(paramInt1);
    super.onMeasure(paramInt1, paramInt2);
    int m = getMeasuredWidth();
    boolean bool = false;
    int n = View.MeasureSpec.makeMeasureSpec(m, 1073741824);
    if (j) {
      f1 = this.mMinorWeightMin;
    } else {
      f1 = this.mMajorWeightMin;
    } 
    if (j) {
      f2 = this.mMinorWeightMax;
    } else {
      f2 = this.mMajorWeightMax;
    } 
    paramInt1 = bool;
    int j = n;
    if (k == Integer.MIN_VALUE) {
      paramInt1 = (int)(i * f1);
      i = (int)(i * f1);
      if (f1 > 0.0F && m < paramInt1) {
        j = View.MeasureSpec.makeMeasureSpec(paramInt1, 1073741824);
        paramInt1 = 1;
      } else {
        paramInt1 = bool;
        j = n;
        if (f2 > 0.0F) {
          paramInt1 = bool;
          j = n;
          if (m > i) {
            j = View.MeasureSpec.makeMeasureSpec(i, 1073741824);
            paramInt1 = 1;
          } 
        } 
      } 
    } 
    if (paramInt1 != 0)
      super.onMeasure(j, paramInt2); 
  }
}
