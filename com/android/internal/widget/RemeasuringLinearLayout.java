package com.android.internal.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RemoteViews.RemoteView;
import java.util.ArrayList;

@RemoteView
public class RemeasuringLinearLayout extends LinearLayout {
  private ArrayList<View> mMatchParentViews = new ArrayList<>();
  
  public RemeasuringLinearLayout(Context paramContext) {
    super(paramContext);
  }
  
  public RemeasuringLinearLayout(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  public RemeasuringLinearLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  public RemeasuringLinearLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    super.onMeasure(paramInt1, paramInt2);
    int i = getChildCount();
    int j = 0;
    paramInt2 = getOrientation();
    int k = 0;
    if (paramInt2 == 1) {
      paramInt2 = 1;
    } else {
      paramInt2 = 0;
    } 
    if ((getLayoutParams()).height == -2)
      k = 1; 
    int m;
    for (m = 0; m < i; m++, j = n) {
      View view = getChildAt(m);
      int n = j;
      if (view != null)
        if (view.getVisibility() == 8) {
          n = j;
        } else {
          LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)view.getLayoutParams();
          if (!k || layoutParams.height != -1 || paramInt2 != 0) {
            n = view.getMeasuredHeight() + layoutParams.topMargin + layoutParams.bottomMargin;
            if (paramInt2 != 0)
              n = j + n; 
            n = Math.max(j, n);
          } else {
            this.mMatchParentViews.add(view);
            n = j;
          } 
        }  
    } 
    if (this.mMatchParentViews.size() > 0) {
      m = View.MeasureSpec.makeMeasureSpec(j, 1073741824);
      for (View view : this.mMatchParentViews) {
        k = getPaddingStart();
        paramInt2 = getPaddingEnd();
        int n = (view.getLayoutParams()).width;
        view.measure(getChildMeasureSpec(paramInt1, k + paramInt2, n), m);
      } 
    } 
    this.mMatchParentViews.clear();
    setMeasuredDimension(getMeasuredWidth(), j);
  }
}
