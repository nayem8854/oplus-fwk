package com.android.internal.widget;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ListView;
import java.util.ArrayList;

public class WatchListDecorLayout extends FrameLayout implements ViewTreeObserver.OnScrollChangedListener {
  private int mForegroundPaddingLeft = 0;
  
  private int mForegroundPaddingTop = 0;
  
  private int mForegroundPaddingRight = 0;
  
  private int mForegroundPaddingBottom = 0;
  
  private final ArrayList<View> mMatchParentChildren = new ArrayList<>(1);
  
  private View mBottomPanel;
  
  private ListView mListView;
  
  private ViewTreeObserver mObserver;
  
  private int mPendingScroll;
  
  private View mTopPanel;
  
  public WatchListDecorLayout(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  public WatchListDecorLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  public WatchListDecorLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    this.mPendingScroll = 0;
    for (byte b = 0; b < getChildCount(); b++) {
      ViewTreeObserver viewTreeObserver;
      View view = getChildAt(b);
      if (view instanceof ListView) {
        if (this.mListView == null) {
          ListView listView = (ListView)view;
          listView.setNestedScrollingEnabled(true);
          this.mObserver = viewTreeObserver = this.mListView.getViewTreeObserver();
          viewTreeObserver.addOnScrollChangedListener(this);
        } else {
          throw new IllegalArgumentException("only one ListView child allowed");
        } 
      } else {
        int i = ((FrameLayout.LayoutParams)viewTreeObserver.getLayoutParams()).gravity & 0x70;
        if (i == 48 && this.mTopPanel == null) {
          this.mTopPanel = (View)viewTreeObserver;
        } else if (i == 80 && this.mBottomPanel == null) {
          this.mBottomPanel = (View)viewTreeObserver;
        } 
      } 
    } 
  }
  
  public void onDetachedFromWindow() {
    this.mListView = null;
    this.mBottomPanel = null;
    this.mTopPanel = null;
    ViewTreeObserver viewTreeObserver = this.mObserver;
    if (viewTreeObserver != null) {
      if (viewTreeObserver.isAlive())
        this.mObserver.removeOnScrollChangedListener(this); 
      this.mObserver = null;
    } 
  }
  
  private void applyMeasureToChild(View paramView, int paramInt1, int paramInt2) {
    ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams)paramView.getLayoutParams();
    if (marginLayoutParams.width == -1) {
      int i = getMeasuredWidth();
      int j = getPaddingLeftWithForeground(), k = getPaddingRightWithForeground(), m = marginLayoutParams.leftMargin;
      paramInt1 = marginLayoutParams.rightMargin;
      paramInt1 = Math.max(0, i - j - k - m - paramInt1);
      paramInt1 = View.MeasureSpec.makeMeasureSpec(paramInt1, 1073741824);
    } else {
      int k = getPaddingLeftWithForeground(), i = getPaddingRightWithForeground(), m = marginLayoutParams.leftMargin, j = marginLayoutParams.rightMargin, n = marginLayoutParams.width;
      paramInt1 = getChildMeasureSpec(paramInt1, k + i + m + j, n);
    } 
    if (marginLayoutParams.height == -1) {
      int k = getMeasuredHeight();
      int j = getPaddingTopWithForeground();
      paramInt2 = getPaddingBottomWithForeground();
      int i = marginLayoutParams.topMargin, m = marginLayoutParams.bottomMargin;
      paramInt2 = Math.max(0, k - j - paramInt2 - i - m);
      paramInt2 = View.MeasureSpec.makeMeasureSpec(paramInt2, 1073741824);
    } else {
      int k = getPaddingTopWithForeground(), j = getPaddingBottomWithForeground(), m = marginLayoutParams.topMargin, i = marginLayoutParams.bottomMargin, n = marginLayoutParams.height;
      paramInt2 = getChildMeasureSpec(paramInt2, k + j + m + i, n);
    } 
    paramView.measure(paramInt1, paramInt2);
  }
  
  private int measureAndGetHeight(View paramView, int paramInt1, int paramInt2) {
    if (paramView != null) {
      if (paramView.getVisibility() != 8) {
        applyMeasureToChild(this.mBottomPanel, paramInt1, paramInt2);
        return paramView.getMeasuredHeight();
      } 
      if (getMeasureAllChildren())
        applyMeasureToChild(this.mBottomPanel, paramInt1, paramInt2); 
    } 
    return 0;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual getChildCount : ()I
    //   4: istore_3
    //   5: iload_1
    //   6: invokestatic getMode : (I)I
    //   9: ldc 1073741824
    //   11: if_icmpne -> 32
    //   14: iload_2
    //   15: invokestatic getMode : (I)I
    //   18: ldc 1073741824
    //   20: if_icmpeq -> 26
    //   23: goto -> 32
    //   26: iconst_0
    //   27: istore #4
    //   29: goto -> 35
    //   32: iconst_1
    //   33: istore #4
    //   35: aload_0
    //   36: getfield mMatchParentChildren : Ljava/util/ArrayList;
    //   39: invokevirtual clear : ()V
    //   42: iconst_0
    //   43: istore #5
    //   45: iconst_0
    //   46: istore #6
    //   48: iconst_0
    //   49: istore #7
    //   51: iconst_0
    //   52: istore #8
    //   54: iload #8
    //   56: iload_3
    //   57: if_icmpge -> 237
    //   60: aload_0
    //   61: iload #8
    //   63: invokevirtual getChildAt : (I)Landroid/view/View;
    //   66: astore #9
    //   68: aload_0
    //   69: invokevirtual getMeasureAllChildren : ()Z
    //   72: ifne -> 91
    //   75: aload #9
    //   77: invokevirtual getVisibility : ()I
    //   80: bipush #8
    //   82: if_icmpeq -> 88
    //   85: goto -> 91
    //   88: goto -> 231
    //   91: aload_0
    //   92: aload #9
    //   94: iload_1
    //   95: iconst_0
    //   96: iload_2
    //   97: iconst_0
    //   98: invokevirtual measureChildWithMargins : (Landroid/view/View;IIII)V
    //   101: aload #9
    //   103: invokevirtual getLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
    //   106: checkcast android/widget/FrameLayout$LayoutParams
    //   109: astore #10
    //   111: aload #9
    //   113: invokevirtual getMeasuredWidth : ()I
    //   116: istore #11
    //   118: aload #10
    //   120: getfield leftMargin : I
    //   123: istore #12
    //   125: aload #10
    //   127: getfield rightMargin : I
    //   130: istore #13
    //   132: iload #6
    //   134: iload #11
    //   136: iload #12
    //   138: iadd
    //   139: iload #13
    //   141: iadd
    //   142: invokestatic max : (II)I
    //   145: istore #6
    //   147: aload #9
    //   149: invokevirtual getMeasuredHeight : ()I
    //   152: istore #11
    //   154: aload #10
    //   156: getfield topMargin : I
    //   159: istore #13
    //   161: aload #10
    //   163: getfield bottomMargin : I
    //   166: istore #12
    //   168: iload #5
    //   170: iload #11
    //   172: iload #13
    //   174: iadd
    //   175: iload #12
    //   177: iadd
    //   178: invokestatic max : (II)I
    //   181: istore #5
    //   183: iload #7
    //   185: aload #9
    //   187: invokevirtual getMeasuredState : ()I
    //   190: invokestatic combineMeasuredStates : (II)I
    //   193: istore #7
    //   195: iload #4
    //   197: ifeq -> 231
    //   200: aload #10
    //   202: getfield width : I
    //   205: iconst_m1
    //   206: if_icmpeq -> 218
    //   209: aload #10
    //   211: getfield height : I
    //   214: iconst_m1
    //   215: if_icmpne -> 231
    //   218: aload_0
    //   219: getfield mMatchParentChildren : Ljava/util/ArrayList;
    //   222: aload #9
    //   224: invokevirtual add : (Ljava/lang/Object;)Z
    //   227: pop
    //   228: goto -> 231
    //   231: iinc #8, 1
    //   234: goto -> 54
    //   237: aload_0
    //   238: invokespecial getPaddingLeftWithForeground : ()I
    //   241: istore #4
    //   243: aload_0
    //   244: invokespecial getPaddingRightWithForeground : ()I
    //   247: istore #8
    //   249: aload_0
    //   250: invokespecial getPaddingTopWithForeground : ()I
    //   253: istore_3
    //   254: aload_0
    //   255: invokespecial getPaddingBottomWithForeground : ()I
    //   258: istore #12
    //   260: iload #5
    //   262: iload_3
    //   263: iload #12
    //   265: iadd
    //   266: iadd
    //   267: aload_0
    //   268: invokevirtual getSuggestedMinimumHeight : ()I
    //   271: invokestatic max : (II)I
    //   274: istore #5
    //   276: iload #6
    //   278: iload #4
    //   280: iload #8
    //   282: iadd
    //   283: iadd
    //   284: aload_0
    //   285: invokevirtual getSuggestedMinimumWidth : ()I
    //   288: invokestatic max : (II)I
    //   291: istore #6
    //   293: aload_0
    //   294: invokevirtual getForeground : ()Landroid/graphics/drawable/Drawable;
    //   297: astore #10
    //   299: iload #5
    //   301: istore #8
    //   303: iload #6
    //   305: istore #4
    //   307: aload #10
    //   309: ifnull -> 336
    //   312: iload #5
    //   314: aload #10
    //   316: invokevirtual getMinimumHeight : ()I
    //   319: invokestatic max : (II)I
    //   322: istore #8
    //   324: iload #6
    //   326: aload #10
    //   328: invokevirtual getMinimumWidth : ()I
    //   331: invokestatic max : (II)I
    //   334: istore #4
    //   336: iload #4
    //   338: iload_1
    //   339: iload #7
    //   341: invokestatic resolveSizeAndState : (III)I
    //   344: istore #4
    //   346: iload #8
    //   348: iload_2
    //   349: iload #7
    //   351: bipush #16
    //   353: ishl
    //   354: invokestatic resolveSizeAndState : (III)I
    //   357: istore #7
    //   359: aload_0
    //   360: iload #4
    //   362: iload #7
    //   364: invokevirtual setMeasuredDimension : (II)V
    //   367: aload_0
    //   368: getfield mListView : Landroid/widget/ListView;
    //   371: astore #10
    //   373: aload #10
    //   375: ifnull -> 555
    //   378: aload_0
    //   379: getfield mPendingScroll : I
    //   382: istore #7
    //   384: iload #7
    //   386: ifeq -> 401
    //   389: aload #10
    //   391: iload #7
    //   393: invokevirtual scrollListBy : (I)V
    //   396: aload_0
    //   397: iconst_0
    //   398: putfield mPendingScroll : I
    //   401: aload_0
    //   402: getfield mListView : Landroid/widget/ListView;
    //   405: invokevirtual getPaddingTop : ()I
    //   408: istore #4
    //   410: aload_0
    //   411: getfield mTopPanel : Landroid/view/View;
    //   414: astore #10
    //   416: aload_0
    //   417: aload #10
    //   419: iload_1
    //   420: iload_2
    //   421: invokespecial measureAndGetHeight : (Landroid/view/View;II)I
    //   424: istore #7
    //   426: iload #4
    //   428: iload #7
    //   430: invokestatic max : (II)I
    //   433: istore #7
    //   435: aload_0
    //   436: getfield mListView : Landroid/widget/ListView;
    //   439: invokevirtual getPaddingBottom : ()I
    //   442: istore #8
    //   444: aload_0
    //   445: getfield mBottomPanel : Landroid/view/View;
    //   448: astore #10
    //   450: aload_0
    //   451: aload #10
    //   453: iload_1
    //   454: iload_2
    //   455: invokespecial measureAndGetHeight : (Landroid/view/View;II)I
    //   458: istore #4
    //   460: iload #8
    //   462: iload #4
    //   464: invokestatic max : (II)I
    //   467: istore #4
    //   469: iload #7
    //   471: aload_0
    //   472: getfield mListView : Landroid/widget/ListView;
    //   475: invokevirtual getPaddingTop : ()I
    //   478: if_icmpne -> 497
    //   481: aload_0
    //   482: getfield mListView : Landroid/widget/ListView;
    //   485: astore #10
    //   487: iload #4
    //   489: aload #10
    //   491: invokevirtual getPaddingBottom : ()I
    //   494: if_icmpeq -> 555
    //   497: aload_0
    //   498: aload_0
    //   499: getfield mPendingScroll : I
    //   502: aload_0
    //   503: getfield mListView : Landroid/widget/ListView;
    //   506: invokevirtual getPaddingTop : ()I
    //   509: iload #7
    //   511: isub
    //   512: iadd
    //   513: putfield mPendingScroll : I
    //   516: aload_0
    //   517: getfield mListView : Landroid/widget/ListView;
    //   520: astore #10
    //   522: aload #10
    //   524: invokevirtual getPaddingLeft : ()I
    //   527: istore #8
    //   529: aload_0
    //   530: getfield mListView : Landroid/widget/ListView;
    //   533: astore #9
    //   535: aload #9
    //   537: invokevirtual getPaddingRight : ()I
    //   540: istore #5
    //   542: aload #10
    //   544: iload #8
    //   546: iload #7
    //   548: iload #5
    //   550: iload #4
    //   552: invokevirtual setPadding : (IIII)V
    //   555: aload_0
    //   556: getfield mMatchParentChildren : Ljava/util/ArrayList;
    //   559: invokevirtual size : ()I
    //   562: istore #4
    //   564: iload #4
    //   566: iconst_1
    //   567: if_icmple -> 633
    //   570: iconst_0
    //   571: istore #7
    //   573: iload #7
    //   575: iload #4
    //   577: if_icmpge -> 633
    //   580: aload_0
    //   581: getfield mMatchParentChildren : Ljava/util/ArrayList;
    //   584: iload #7
    //   586: invokevirtual get : (I)Ljava/lang/Object;
    //   589: checkcast android/view/View
    //   592: astore #10
    //   594: aload_0
    //   595: getfield mListView : Landroid/widget/ListView;
    //   598: ifnull -> 619
    //   601: aload #10
    //   603: aload_0
    //   604: getfield mTopPanel : Landroid/view/View;
    //   607: if_acmpeq -> 627
    //   610: aload #10
    //   612: aload_0
    //   613: getfield mBottomPanel : Landroid/view/View;
    //   616: if_acmpeq -> 627
    //   619: aload_0
    //   620: aload #10
    //   622: iload_1
    //   623: iload_2
    //   624: invokespecial applyMeasureToChild : (Landroid/view/View;II)V
    //   627: iinc #7, 1
    //   630: goto -> 573
    //   633: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #160	-> 0
    //   #162	-> 5
    //   #163	-> 5
    //   #164	-> 14
    //   #165	-> 35
    //   #167	-> 42
    //   #168	-> 42
    //   #169	-> 42
    //   #171	-> 42
    //   #172	-> 60
    //   #173	-> 68
    //   #174	-> 91
    //   #175	-> 101
    //   #176	-> 111
    //   #177	-> 111
    //   #176	-> 132
    //   #178	-> 147
    //   #179	-> 147
    //   #178	-> 168
    //   #180	-> 183
    //   #181	-> 195
    //   #182	-> 200
    //   #184	-> 218
    //   #181	-> 231
    //   #171	-> 231
    //   #191	-> 237
    //   #192	-> 249
    //   #195	-> 260
    //   #196	-> 276
    //   #199	-> 293
    //   #200	-> 299
    //   #201	-> 312
    //   #202	-> 324
    //   #205	-> 336
    //   #206	-> 346
    //   #205	-> 359
    //   #209	-> 367
    //   #210	-> 378
    //   #211	-> 389
    //   #212	-> 396
    //   #215	-> 401
    //   #216	-> 416
    //   #215	-> 426
    //   #217	-> 435
    //   #218	-> 450
    //   #217	-> 460
    //   #220	-> 469
    //   #221	-> 487
    //   #222	-> 497
    //   #223	-> 516
    //   #224	-> 522
    //   #225	-> 535
    //   #223	-> 542
    //   #229	-> 555
    //   #230	-> 564
    //   #231	-> 570
    //   #232	-> 580
    //   #233	-> 594
    //   #234	-> 619
    //   #231	-> 627
    //   #238	-> 633
  }
  
  public void setForegroundGravity(int paramInt) {
    if (getForegroundGravity() != paramInt) {
      super.setForegroundGravity(paramInt);
      Drawable drawable = getForeground();
      if (getForegroundGravity() == 119 && drawable != null) {
        Rect rect = new Rect();
        if (drawable.getPadding(rect)) {
          this.mForegroundPaddingLeft = rect.left;
          this.mForegroundPaddingTop = rect.top;
          this.mForegroundPaddingRight = rect.right;
          this.mForegroundPaddingBottom = rect.bottom;
        } 
      } else {
        this.mForegroundPaddingLeft = 0;
        this.mForegroundPaddingTop = 0;
        this.mForegroundPaddingRight = 0;
        this.mForegroundPaddingBottom = 0;
      } 
    } 
  }
  
  private int getPaddingLeftWithForeground() {
    int i;
    if (isForegroundInsidePadding()) {
      i = Math.max(this.mPaddingLeft, this.mForegroundPaddingLeft);
    } else {
      i = this.mPaddingLeft + this.mForegroundPaddingLeft;
    } 
    return i;
  }
  
  private int getPaddingRightWithForeground() {
    int i;
    if (isForegroundInsidePadding()) {
      i = Math.max(this.mPaddingRight, this.mForegroundPaddingRight);
    } else {
      i = this.mPaddingRight + this.mForegroundPaddingRight;
    } 
    return i;
  }
  
  private int getPaddingTopWithForeground() {
    int i;
    if (isForegroundInsidePadding()) {
      i = Math.max(this.mPaddingTop, this.mForegroundPaddingTop);
    } else {
      i = this.mPaddingTop + this.mForegroundPaddingTop;
    } 
    return i;
  }
  
  private int getPaddingBottomWithForeground() {
    int i;
    if (isForegroundInsidePadding()) {
      i = Math.max(this.mPaddingBottom, this.mForegroundPaddingBottom);
    } else {
      i = this.mPaddingBottom + this.mForegroundPaddingBottom;
    } 
    return i;
  }
  
  public void onScrollChanged() {
    ListView listView = this.mListView;
    if (listView == null)
      return; 
    if (this.mTopPanel != null)
      if (listView.getChildCount() > 0) {
        if (this.mListView.getFirstVisiblePosition() == 0) {
          View view2 = this.mListView.getChildAt(0);
          View view1 = this.mTopPanel;
          float f1 = view2.getY(), f2 = this.mTopPanel.getHeight(), f3 = this.mTopPanel.getTop();
          setScrolling(view1, f1 - f2 - f3);
        } else {
          View view = this.mTopPanel;
          setScrolling(view, -view.getHeight());
        } 
      } else {
        setScrolling(this.mTopPanel, 0.0F);
      }  
    if (this.mBottomPanel != null)
      if (this.mListView.getChildCount() > 0) {
        if (this.mListView.getLastVisiblePosition() >= this.mListView.getCount() - 1) {
          listView = this.mListView;
          View view2 = listView.getChildAt(listView.getChildCount() - 1);
          View view1 = this.mBottomPanel;
          float f1 = view2.getY(), f3 = view2.getHeight(), f2 = this.mBottomPanel.getTop();
          setScrolling(view1, Math.max(0.0F, f1 + f3 - f2));
        } else {
          View view = this.mBottomPanel;
          setScrolling(view, view.getHeight());
        } 
      } else {
        setScrolling(this.mBottomPanel, 0.0F);
      }  
  }
  
  private void setScrolling(View paramView, float paramFloat) {
    if (paramView.getTranslationY() != paramFloat)
      paramView.setTranslationY(paramFloat); 
  }
}
