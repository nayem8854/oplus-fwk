package com.android.internal.widget;

import android.app.ActivityManager;
import android.app.Notification;
import android.view.View;
import java.util.Objects;

public interface MessagingMessage extends MessagingLinearLayout.MessagingChild {
  public static final String IMAGE_MIME_TYPE_PREFIX = "image/";
  
  static MessagingMessage createMessage(IMessagingLayout paramIMessagingLayout, Notification.MessagingStyle.Message paramMessage, ImageResolver paramImageResolver) {
    if (hasImage(paramMessage) && !ActivityManager.isLowRamDeviceStatic())
      return MessagingImageMessage.createMessage(paramIMessagingLayout, paramMessage, paramImageResolver); 
    return MessagingTextMessage.createMessage(paramIMessagingLayout, paramMessage);
  }
  
  static void dropCache() {
    MessagingTextMessage.dropCache();
    MessagingImageMessage.dropCache();
  }
  
  static boolean hasImage(Notification.MessagingStyle.Message paramMessage) {
    boolean bool;
    if (paramMessage.getDataUri() != null && 
      paramMessage.getDataMimeType() != null && 
      paramMessage.getDataMimeType().startsWith("image/")) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  default boolean setMessage(Notification.MessagingStyle.Message paramMessage) {
    getState().setMessage(paramMessage);
    return true;
  }
  
  default Notification.MessagingStyle.Message getMessage() {
    return getState().getMessage();
  }
  
  default boolean sameAs(Notification.MessagingStyle.Message paramMessage) {
    boolean bool1;
    Notification.MessagingStyle.Message message = getMessage();
    if (!Objects.equals(paramMessage.getText(), message.getText()))
      return false; 
    if (!Objects.equals(paramMessage.getSender(), message.getSender()))
      return false; 
    boolean bool = paramMessage.isRemoteInputHistory();
    if (bool != message.isRemoteInputHistory()) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    if (!bool1 && 
      !Objects.equals(Long.valueOf(paramMessage.getTimestamp()), Long.valueOf(message.getTimestamp())))
      return false; 
    if (!Objects.equals(paramMessage.getDataMimeType(), message.getDataMimeType()))
      return false; 
    if (!Objects.equals(paramMessage.getDataUri(), message.getDataUri()))
      return false; 
    return true;
  }
  
  default boolean sameAs(MessagingMessage paramMessagingMessage) {
    return sameAs(paramMessagingMessage.getMessage());
  }
  
  default void removeMessage() {
    getGroup().removeMessage(this);
  }
  
  default void setMessagingGroup(MessagingGroup paramMessagingGroup) {
    getState().setGroup(paramMessagingGroup);
  }
  
  default void setIsHistoric(boolean paramBoolean) {
    getState().setIsHistoric(paramBoolean);
  }
  
  default MessagingGroup getGroup() {
    return getState().getGroup();
  }
  
  default void setIsHidingAnimated(boolean paramBoolean) {
    getState().setIsHidingAnimated(paramBoolean);
  }
  
  default boolean isHidingAnimated() {
    return getState().isHidingAnimated();
  }
  
  default void hideAnimated() {
    setIsHidingAnimated(true);
    getGroup().performRemoveAnimation(getView(), new _$$Lambda$MessagingMessage$goi5oiwdlMBbUvfJzNl7fGbZ_K0(this));
  }
  
  default boolean hasOverlappingRendering() {
    return false;
  }
  
  default void recycle() {
    getState().recycle();
  }
  
  default View getView() {
    return (View)this;
  }
  
  default void setColor(int paramInt) {}
  
  MessagingMessageState getState();
  
  int getVisibility();
  
  void setVisibility(int paramInt);
}
