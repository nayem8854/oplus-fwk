package com.android.internal.widget;

import android.app.Notification;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Pools;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RemoteViews.RemoteView;
import java.io.IOException;

@RemoteView
public class MessagingImageMessage extends ImageView implements MessagingMessage {
  private static Pools.SimplePool<MessagingImageMessage> sInstancePool = (Pools.SimplePool<MessagingImageMessage>)new Pools.SynchronizedPool(10);
  
  private final MessagingMessageState mState = new MessagingMessageState((View)this);
  
  private final Path mPath = new Path();
  
  private static final String TAG = "MessagingImageMessage";
  
  private int mActualHeight;
  
  private int mActualWidth;
  
  private float mAspectRatio;
  
  private Drawable mDrawable;
  
  private final int mExtraSpacing;
  
  private ImageResolver mImageResolver;
  
  private final int mImageRounding;
  
  private boolean mIsIsolated;
  
  private final int mIsolatedSize;
  
  private final int mMaxImageHeight;
  
  private final int mMinImageHeight;
  
  public MessagingImageMessage(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public MessagingImageMessage(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public MessagingImageMessage(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public MessagingImageMessage(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    this.mMinImageHeight = paramContext.getResources().getDimensionPixelSize(17105319);
    this.mMaxImageHeight = paramContext.getResources().getDimensionPixelSize(17105318);
    this.mImageRounding = paramContext.getResources().getDimensionPixelSize(17105320);
    this.mExtraSpacing = paramContext.getResources().getDimensionPixelSize(17105317);
    setMaxHeight(this.mMaxImageHeight);
    this.mIsolatedSize = getResources().getDimensionPixelSize(17105314);
  }
  
  public MessagingMessageState getState() {
    return this.mState;
  }
  
  public boolean setMessage(Notification.MessagingStyle.Message paramMessage) {
    super.setMessage(paramMessage);
    try {
      Drawable drawable;
      Uri uri = paramMessage.getDataUri();
      if (this.mImageResolver != null) {
        drawable = this.mImageResolver.loadImage(uri);
      } else {
        drawable = LocalImageResolver.resolveImage((Uri)drawable, getContext());
      } 
      if (drawable == null)
        return false; 
      int i = drawable.getIntrinsicHeight();
      if (i == 0) {
        Log.w("MessagingImageMessage", "Drawable with 0 intrinsic height was returned");
        return false;
      } 
      this.mDrawable = drawable;
      this.mAspectRatio = drawable.getIntrinsicWidth() / i;
      setImageDrawable(drawable);
      setContentDescription(paramMessage.getText());
      return true;
    } catch (IOException|SecurityException iOException) {
      iOException.printStackTrace();
      return false;
    } 
  }
  
  static MessagingMessage createMessage(IMessagingLayout paramIMessagingLayout, Notification.MessagingStyle.Message paramMessage, ImageResolver paramImageResolver) {
    MessagingLinearLayout messagingLinearLayout = paramIMessagingLayout.getMessagingLinearLayout();
    MessagingImageMessage messagingImageMessage1 = (MessagingImageMessage)sInstancePool.acquire();
    MessagingImageMessage messagingImageMessage2 = messagingImageMessage1;
    if (messagingImageMessage1 == null) {
      Context context = paramIMessagingLayout.getContext();
      LayoutInflater layoutInflater = LayoutInflater.from(context);
      messagingImageMessage2 = (MessagingImageMessage)layoutInflater.inflate(17367218, messagingLinearLayout, false);
      messagingImageMessage2.addOnLayoutChangeListener(MessagingLayout.MESSAGING_PROPERTY_ANIMATOR);
    } 
    messagingImageMessage2.setImageResolver(paramImageResolver);
    boolean bool = messagingImageMessage2.setMessage(paramMessage);
    if (!bool) {
      messagingImageMessage2.recycle();
      return MessagingTextMessage.createMessage(paramIMessagingLayout, paramMessage);
    } 
    return messagingImageMessage2;
  }
  
  private void setImageResolver(ImageResolver paramImageResolver) {
    this.mImageResolver = paramImageResolver;
  }
  
  protected void onDraw(Canvas paramCanvas) {
    paramCanvas.save();
    paramCanvas.clipPath(getRoundedRectPath());
    float f1 = Math.min(getHeight(), getActualHeight()), f2 = this.mAspectRatio;
    float f3 = getActualWidth();
    int i = (int)Math.max(f1 * f2, f3);
    f3 = Math.min(getWidth(), getActualWidth()) / this.mAspectRatio;
    f2 = getActualHeight();
    int j = (int)Math.max(f3, f2);
    int k = (int)Math.max(j, i / this.mAspectRatio);
    j = (int)((getActualWidth() - i) / 2.0F);
    int m = (int)((getActualHeight() - k) / 2.0F);
    this.mDrawable.setBounds(j, m, j + i, m + k);
    this.mDrawable.draw(paramCanvas);
    paramCanvas.restore();
  }
  
  public Path getRoundedRectPath() {
    int i = getActualWidth();
    int j = getActualHeight();
    this.mPath.reset();
    int k = this.mImageRounding;
    float f1 = k;
    float f2 = k;
    f1 = Math.min(((i - 0) / 2), f1);
    f2 = Math.min(((j - 0) / 2), f2);
    this.mPath.moveTo(false, false + f2);
    this.mPath.quadTo(false, false, false + f1, false);
    this.mPath.lineTo(i - f1, false);
    this.mPath.quadTo(i, false, i, false + f2);
    this.mPath.lineTo(i, j - f2);
    this.mPath.quadTo(i, j, i - f1, j);
    this.mPath.lineTo(false + f1, j);
    this.mPath.quadTo(false, j, false, j - f2);
    this.mPath.close();
    return this.mPath;
  }
  
  public void recycle() {
    super.recycle();
    setImageBitmap(null);
    this.mDrawable = null;
    sInstancePool.release(this);
  }
  
  public static void dropCache() {
    sInstancePool = (Pools.SimplePool<MessagingImageMessage>)new Pools.SynchronizedPool(10);
  }
  
  public int getMeasuredType() {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual getMeasuredHeight : ()I
    //   4: istore_1
    //   5: aload_0
    //   6: getfield mIsIsolated : Z
    //   9: ifeq -> 20
    //   12: aload_0
    //   13: getfield mIsolatedSize : I
    //   16: istore_2
    //   17: goto -> 25
    //   20: aload_0
    //   21: getfield mMinImageHeight : I
    //   24: istore_2
    //   25: iload_1
    //   26: iload_2
    //   27: if_icmpge -> 48
    //   30: aload_0
    //   31: getfield mDrawable : Landroid/graphics/drawable/Drawable;
    //   34: astore_3
    //   35: iload_1
    //   36: aload_3
    //   37: invokevirtual getIntrinsicHeight : ()I
    //   40: if_icmpeq -> 48
    //   43: iconst_1
    //   44: istore_2
    //   45: goto -> 50
    //   48: iconst_0
    //   49: istore_2
    //   50: iload_2
    //   51: ifeq -> 56
    //   54: iconst_2
    //   55: ireturn
    //   56: aload_0
    //   57: getfield mIsIsolated : Z
    //   60: ifne -> 76
    //   63: iload_1
    //   64: aload_0
    //   65: getfield mDrawable : Landroid/graphics/drawable/Drawable;
    //   68: invokevirtual getIntrinsicHeight : ()I
    //   71: if_icmpeq -> 76
    //   74: iconst_1
    //   75: ireturn
    //   76: iconst_0
    //   77: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #202	-> 0
    //   #204	-> 5
    //   #205	-> 12
    //   #207	-> 20
    //   #209	-> 25
    //   #210	-> 35
    //   #211	-> 50
    //   #212	-> 54
    //   #214	-> 56
    //   #215	-> 74
    //   #217	-> 76
  }
  
  public void setMaxDisplayedLines(int paramInt) {}
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    super.onMeasure(paramInt1, paramInt2);
    if (this.mIsIsolated) {
      paramInt1 = View.MeasureSpec.getSize(paramInt1);
      paramInt2 = View.MeasureSpec.getSize(paramInt2);
      setMeasuredDimension(paramInt1, paramInt2);
    } else {
      paramInt1 = View.MeasureSpec.getSize(paramInt1);
      Drawable drawable = this.mDrawable;
      int i = drawable.getIntrinsicWidth();
      paramInt1 = Math.min(paramInt1, i);
      paramInt2 = (int)Math.min(View.MeasureSpec.getSize(paramInt2), paramInt1 / this.mAspectRatio);
      setMeasuredDimension(paramInt1, paramInt2);
    } 
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    setActualWidth(getWidth());
    setActualHeight(getHeight());
  }
  
  public int getConsumedLines() {
    return 3;
  }
  
  public void setActualWidth(int paramInt) {
    this.mActualWidth = paramInt;
    invalidate();
  }
  
  public int getActualWidth() {
    return this.mActualWidth;
  }
  
  public void setActualHeight(int paramInt) {
    this.mActualHeight = paramInt;
    invalidate();
  }
  
  public int getActualHeight() {
    return this.mActualHeight;
  }
  
  public void setIsolated(boolean paramBoolean) {
    if (this.mIsIsolated != paramBoolean) {
      int i;
      this.mIsIsolated = paramBoolean;
      ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams)getLayoutParams();
      if (paramBoolean) {
        i = 0;
      } else {
        i = this.mExtraSpacing;
      } 
      marginLayoutParams.topMargin = i;
      setLayoutParams((ViewGroup.LayoutParams)marginLayoutParams);
    } 
  }
  
  public int getExtraSpacing() {
    return this.mExtraSpacing;
  }
}
