package com.android.internal.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.android.internal.R;

public class ActionBarContainer extends FrameLayout {
  private View mActionBarView;
  
  private View mActionContextView;
  
  private Drawable mBackground;
  
  private int mHeight;
  
  private boolean mIsSplit;
  
  private boolean mIsStacked;
  
  private boolean mIsTransitioning;
  
  private Drawable mSplitBackground;
  
  private Drawable mStackedBackground;
  
  private View mTabContainer;
  
  public ActionBarContainer(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public ActionBarContainer(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    setBackground(new ActionBarBackgroundDrawable());
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ActionBar);
    this.mBackground = typedArray.getDrawable(2);
    this.mStackedBackground = typedArray.getDrawable(18);
    this.mHeight = typedArray.getDimensionPixelSize(4, -1);
    int i = getId();
    boolean bool = true;
    if (i == 16909466) {
      this.mIsSplit = true;
      this.mSplitBackground = typedArray.getDrawable(19);
    } 
    typedArray.recycle();
    if (this.mIsSplit) {
      if (this.mSplitBackground != null)
        bool = false; 
    } else if (this.mBackground != null || this.mStackedBackground != null) {
      bool = false;
    } 
    setWillNotDraw(bool);
  }
  
  public void onFinishInflate() {
    super.onFinishInflate();
    this.mActionBarView = findViewById(16908709);
    this.mActionContextView = findViewById(16908714);
  }
  
  public void setPrimaryBackground(Drawable paramDrawable) {
    Drawable drawable = this.mBackground;
    if (drawable != null) {
      drawable.setCallback(null);
      unscheduleDrawable(this.mBackground);
    } 
    this.mBackground = paramDrawable;
    if (paramDrawable != null) {
      paramDrawable.setCallback((Drawable.Callback)this);
      View view = this.mActionBarView;
      if (view != null) {
        paramDrawable = this.mBackground;
        int i = view.getLeft(), j = this.mActionBarView.getTop();
        view = this.mActionBarView;
        int k = view.getRight(), m = this.mActionBarView.getBottom();
        paramDrawable.setBounds(i, j, k, m);
      } 
    } 
    boolean bool = this.mIsSplit;
    boolean bool1 = true;
    if (bool) {
      if (this.mSplitBackground != null)
        bool1 = false; 
    } else if (this.mBackground != null || this.mStackedBackground != null) {
      bool1 = false;
    } 
    setWillNotDraw(bool1);
    invalidate();
  }
  
  public void setStackedBackground(Drawable paramDrawable) {
    Drawable drawable = this.mStackedBackground;
    if (drawable != null) {
      drawable.setCallback(null);
      unscheduleDrawable(this.mStackedBackground);
    } 
    this.mStackedBackground = paramDrawable;
    if (paramDrawable != null) {
      paramDrawable.setCallback((Drawable.Callback)this);
      if (this.mIsStacked) {
        paramDrawable = this.mStackedBackground;
        if (paramDrawable != null) {
          int i = this.mTabContainer.getLeft(), j = this.mTabContainer.getTop();
          View view = this.mTabContainer;
          int k = view.getRight(), m = this.mTabContainer.getBottom();
          paramDrawable.setBounds(i, j, k, m);
        } 
      } 
    } 
    boolean bool = this.mIsSplit;
    boolean bool1 = true;
    if (bool) {
      if (this.mSplitBackground != null)
        bool1 = false; 
    } else if (this.mBackground != null || this.mStackedBackground != null) {
      bool1 = false;
    } 
    setWillNotDraw(bool1);
    invalidate();
  }
  
  public void setSplitBackground(Drawable paramDrawable) {
    boolean bool2;
    Drawable drawable = this.mSplitBackground;
    if (drawable != null) {
      drawable.setCallback(null);
      unscheduleDrawable(this.mSplitBackground);
    } 
    this.mSplitBackground = paramDrawable;
    boolean bool1 = false;
    if (paramDrawable != null) {
      paramDrawable.setCallback((Drawable.Callback)this);
      if (this.mIsSplit) {
        paramDrawable = this.mSplitBackground;
        if (paramDrawable != null)
          paramDrawable.setBounds(0, 0, getMeasuredWidth(), getMeasuredHeight()); 
      } 
    } 
    if (this.mIsSplit) {
      bool2 = bool1;
      if (this.mSplitBackground == null)
        bool2 = true; 
    } else {
      bool2 = bool1;
      if (this.mBackground == null) {
        bool2 = bool1;
        if (this.mStackedBackground == null)
          bool2 = true; 
      } 
    } 
    setWillNotDraw(bool2);
    invalidate();
  }
  
  public void setVisibility(int paramInt) {
    boolean bool;
    super.setVisibility(paramInt);
    if (paramInt == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    Drawable drawable = this.mBackground;
    if (drawable != null)
      drawable.setVisible(bool, false); 
    drawable = this.mStackedBackground;
    if (drawable != null)
      drawable.setVisible(bool, false); 
    drawable = this.mSplitBackground;
    if (drawable != null)
      drawable.setVisible(bool, false); 
  }
  
  protected boolean verifyDrawable(Drawable paramDrawable) {
    boolean bool;
    if ((paramDrawable == this.mBackground && !this.mIsSplit) || (paramDrawable == this.mStackedBackground && this.mIsStacked) || (paramDrawable == this.mSplitBackground && this.mIsSplit) || 
      super.verifyDrawable(paramDrawable)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected void drawableStateChanged() {
    boolean bool1;
    super.drawableStateChanged();
    int[] arrayOfInt = getDrawableState();
    int i = 0;
    Drawable drawable = this.mBackground;
    int j = i;
    if (drawable != null) {
      j = i;
      if (drawable.isStateful())
        j = false | drawable.setState(arrayOfInt); 
    } 
    drawable = this.mStackedBackground;
    i = j;
    if (drawable != null) {
      i = j;
      if (drawable.isStateful())
        bool1 = j | drawable.setState(arrayOfInt); 
    } 
    drawable = this.mSplitBackground;
    boolean bool2 = bool1;
    if (drawable != null) {
      bool2 = bool1;
      if (drawable.isStateful())
        bool2 = bool1 | drawable.setState(arrayOfInt); 
    } 
    if (bool2)
      invalidate(); 
  }
  
  public void jumpDrawablesToCurrentState() {
    super.jumpDrawablesToCurrentState();
    Drawable drawable = this.mBackground;
    if (drawable != null)
      drawable.jumpToCurrentState(); 
    drawable = this.mStackedBackground;
    if (drawable != null)
      drawable.jumpToCurrentState(); 
    drawable = this.mSplitBackground;
    if (drawable != null)
      drawable.jumpToCurrentState(); 
  }
  
  public void onResolveDrawables(int paramInt) {
    super.onResolveDrawables(paramInt);
    Drawable drawable = this.mBackground;
    if (drawable != null)
      drawable.setLayoutDirection(paramInt); 
    drawable = this.mStackedBackground;
    if (drawable != null)
      drawable.setLayoutDirection(paramInt); 
    drawable = this.mSplitBackground;
    if (drawable != null)
      drawable.setLayoutDirection(paramInt); 
  }
  
  public void setTransitioning(boolean paramBoolean) {
    int i;
    this.mIsTransitioning = paramBoolean;
    if (paramBoolean) {
      i = 393216;
    } else {
      i = 262144;
    } 
    setDescendantFocusability(i);
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent) {
    return (this.mIsTransitioning || super.onInterceptTouchEvent(paramMotionEvent));
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    super.onTouchEvent(paramMotionEvent);
    return true;
  }
  
  public boolean onHoverEvent(MotionEvent paramMotionEvent) {
    super.onHoverEvent(paramMotionEvent);
    return true;
  }
  
  public void setTabContainer(ScrollingTabContainerView paramScrollingTabContainerView) {
    View view = this.mTabContainer;
    if (view != null)
      removeView(view); 
    this.mTabContainer = (View)paramScrollingTabContainerView;
    if (paramScrollingTabContainerView != null) {
      addView((View)paramScrollingTabContainerView);
      ViewGroup.LayoutParams layoutParams = paramScrollingTabContainerView.getLayoutParams();
      layoutParams.width = -1;
      layoutParams.height = -2;
      paramScrollingTabContainerView.setAllowCollapse(false);
    } 
  }
  
  public View getTabContainer() {
    return this.mTabContainer;
  }
  
  public ActionMode startActionModeForChild(View paramView, ActionMode.Callback paramCallback, int paramInt) {
    if (paramInt != 0)
      return super.startActionModeForChild(paramView, paramCallback, paramInt); 
    return null;
  }
  
  private static boolean isCollapsed(View paramView) {
    return (paramView == null || paramView.getVisibility() == 8 || paramView.getMeasuredHeight() == 0);
  }
  
  private int getMeasuredHeightWithMargins(View paramView) {
    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams)paramView.getLayoutParams();
    return paramView.getMeasuredHeight() + layoutParams.topMargin + layoutParams.bottomMargin;
  }
  
  public void onMeasure(int paramInt1, int paramInt2) {
    int i = paramInt2;
    if (this.mActionBarView == null) {
      i = paramInt2;
      if (View.MeasureSpec.getMode(paramInt2) == Integer.MIN_VALUE) {
        int j = this.mHeight;
        i = paramInt2;
        if (j >= 0) {
          paramInt2 = Math.min(j, View.MeasureSpec.getSize(paramInt2));
          i = View.MeasureSpec.makeMeasureSpec(paramInt2, -2147483648);
        } 
      } 
    } 
    super.onMeasure(paramInt1, i);
    if (this.mActionBarView == null)
      return; 
    View view = this.mTabContainer;
    if (view != null && view.getVisibility() != 8) {
      paramInt1 = 0;
      int j = getChildCount();
      for (paramInt2 = 0; paramInt2 < j; paramInt2++) {
        view = getChildAt(paramInt2);
        if (view != this.mTabContainer) {
          int k;
          if (isCollapsed(view)) {
            k = 0;
          } else {
            k = getMeasuredHeightWithMargins(view);
          } 
          paramInt1 = Math.max(paramInt1, k);
        } 
      } 
      paramInt2 = View.MeasureSpec.getMode(i);
      if (paramInt2 == Integer.MIN_VALUE) {
        paramInt2 = View.MeasureSpec.getSize(i);
      } else {
        paramInt2 = Integer.MAX_VALUE;
      } 
      i = getMeasuredWidth();
      view = this.mTabContainer;
      paramInt1 = Math.min(getMeasuredHeightWithMargins(view) + paramInt1, paramInt2);
      setMeasuredDimension(i, paramInt1);
    } 
  }
  
  public void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    Drawable drawable;
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    View view = this.mTabContainer;
    if (view != null && view.getVisibility() != 8) {
      paramBoolean = true;
    } else {
      paramBoolean = false;
    } 
    if (view != null && view.getVisibility() != 8) {
      paramInt4 = getMeasuredHeight();
      FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams)view.getLayoutParams();
      paramInt2 = view.getMeasuredHeight();
      view.layout(paramInt1, paramInt4 - paramInt2 - layoutParams.bottomMargin, paramInt3, paramInt4 - layoutParams.bottomMargin);
    } 
    paramInt1 = 0;
    paramInt2 = 0;
    if (this.mIsSplit) {
      drawable = this.mSplitBackground;
      if (drawable != null) {
        drawable.setBounds(0, 0, getMeasuredWidth(), getMeasuredHeight());
        paramInt1 = 1;
      } 
    } else {
      if (this.mBackground != null) {
        if (this.mActionBarView.getVisibility() == 0) {
          Drawable drawable1 = this.mBackground;
          paramInt2 = this.mActionBarView.getLeft();
          paramInt1 = this.mActionBarView.getTop();
          View view1 = this.mActionBarView;
          paramInt3 = view1.getRight();
          paramInt4 = this.mActionBarView.getBottom();
          drawable1.setBounds(paramInt2, paramInt1, paramInt3, paramInt4);
        } else {
          View view1 = this.mActionContextView;
          if (view1 != null && 
            view1.getVisibility() == 0) {
            Drawable drawable1 = this.mBackground;
            paramInt3 = this.mActionContextView.getLeft();
            paramInt4 = this.mActionContextView.getTop();
            view1 = this.mActionContextView;
            paramInt2 = view1.getRight();
            paramInt1 = this.mActionContextView.getBottom();
            drawable1.setBounds(paramInt3, paramInt4, paramInt2, paramInt1);
          } else {
            this.mBackground.setBounds(0, 0, 0, 0);
          } 
        } 
        paramInt2 = 1;
      } 
      this.mIsStacked = paramBoolean;
      paramInt1 = paramInt2;
      if (paramBoolean) {
        Drawable drawable1 = this.mStackedBackground;
        paramInt1 = paramInt2;
        if (drawable1 != null) {
          paramInt4 = drawable.getLeft();
          paramInt2 = drawable.getTop();
          paramInt1 = drawable.getRight();
          paramInt3 = drawable.getBottom();
          drawable1.setBounds(paramInt4, paramInt2, paramInt1, paramInt3);
          paramInt1 = 1;
        } 
      } 
    } 
    if (paramInt1 != 0)
      invalidate(); 
  }
  
  class ActionBarBackgroundDrawable extends Drawable {
    final ActionBarContainer this$0;
    
    private ActionBarBackgroundDrawable() {}
    
    public void draw(Canvas param1Canvas) {
      if (ActionBarContainer.this.mIsSplit) {
        if (ActionBarContainer.this.mSplitBackground != null)
          ActionBarContainer.this.mSplitBackground.draw(param1Canvas); 
      } else {
        if (ActionBarContainer.this.mBackground != null)
          ActionBarContainer.this.mBackground.draw(param1Canvas); 
        if (ActionBarContainer.this.mStackedBackground != null && ActionBarContainer.this.mIsStacked)
          ActionBarContainer.this.mStackedBackground.draw(param1Canvas); 
      } 
    }
    
    public void getOutline(Outline param1Outline) {
      if (ActionBarContainer.this.mIsSplit) {
        if (ActionBarContainer.this.mSplitBackground != null)
          ActionBarContainer.this.mSplitBackground.getOutline(param1Outline); 
      } else if (ActionBarContainer.this.mBackground != null) {
        ActionBarContainer.this.mBackground.getOutline(param1Outline);
      } 
    }
    
    public void setAlpha(int param1Int) {}
    
    public void setColorFilter(ColorFilter param1ColorFilter) {}
    
    public int getOpacity() {
      if (ActionBarContainer.this.mIsSplit) {
        if (ActionBarContainer.this.mSplitBackground != null) {
          ActionBarContainer actionBarContainer = ActionBarContainer.this;
          if (actionBarContainer.mSplitBackground.getOpacity() == -1)
            return -1; 
        } 
      } else {
        if (ActionBarContainer.this.mIsStacked)
          if (ActionBarContainer.this.mStackedBackground != null) {
            ActionBarContainer actionBarContainer = ActionBarContainer.this;
            if (actionBarContainer.mStackedBackground.getOpacity() != -1)
              return 0; 
          } else {
            return 0;
          }  
        if (!ActionBarContainer.isCollapsed(ActionBarContainer.this.mActionBarView) && ActionBarContainer.this.mBackground != null) {
          ActionBarContainer actionBarContainer = ActionBarContainer.this;
          if (actionBarContainer.mBackground.getOpacity() == -1)
            return -1; 
        } 
      } 
      return 0;
    }
  }
}
