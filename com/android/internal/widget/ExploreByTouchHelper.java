package com.android.internal.widget;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.IntArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;

public abstract class ExploreByTouchHelper extends View.AccessibilityDelegate {
  private static final String DEFAULT_CLASS_NAME = View.class.getName();
  
  public static final int HOST_ID = -1;
  
  public static final int INVALID_ID = -2147483648;
  
  private static final Rect INVALID_PARENT_BOUNDS = new Rect(2147483647, 2147483647, -2147483648, -2147483648);
  
  private final Context mContext;
  
  private int mFocusedVirtualViewId = Integer.MIN_VALUE;
  
  private int mHoveredVirtualViewId = Integer.MIN_VALUE;
  
  private final AccessibilityManager mManager;
  
  private ExploreByTouchNodeProvider mNodeProvider;
  
  private IntArray mTempArray;
  
  private int[] mTempGlobalRect;
  
  private Rect mTempParentRect;
  
  private Rect mTempScreenRect;
  
  private Rect mTempVisibleRect;
  
  private final View mView;
  
  public ExploreByTouchHelper(View paramView) {
    if (paramView != null) {
      this.mView = paramView;
      Context context = paramView.getContext();
      this.mManager = (AccessibilityManager)context.getSystemService("accessibility");
      return;
    } 
    throw new IllegalArgumentException("View may not be null");
  }
  
  public AccessibilityNodeProvider getAccessibilityNodeProvider(View paramView) {
    if (this.mNodeProvider == null)
      this.mNodeProvider = new ExploreByTouchNodeProvider(); 
    return this.mNodeProvider;
  }
  
  public boolean dispatchHoverEvent(MotionEvent paramMotionEvent) {
    boolean bool = this.mManager.isEnabled();
    boolean bool1 = false;
    if (!bool || !this.mManager.isTouchExplorationEnabled())
      return false; 
    int i = paramMotionEvent.getAction();
    if (i != 7 && i != 9) {
      if (i != 10)
        return false; 
      if (this.mHoveredVirtualViewId != Integer.MIN_VALUE) {
        updateHoveredVirtualView(-2147483648);
        return true;
      } 
      return false;
    } 
    i = getVirtualViewAt(paramMotionEvent.getX(), paramMotionEvent.getY());
    updateHoveredVirtualView(i);
    if (i != Integer.MIN_VALUE)
      bool1 = true; 
    return bool1;
  }
  
  public boolean sendEventForVirtualView(int paramInt1, int paramInt2) {
    if (paramInt1 == Integer.MIN_VALUE || !this.mManager.isEnabled())
      return false; 
    ViewParent viewParent = this.mView.getParent();
    if (viewParent == null)
      return false; 
    AccessibilityEvent accessibilityEvent = createEvent(paramInt1, paramInt2);
    return viewParent.requestSendAccessibilityEvent(this.mView, accessibilityEvent);
  }
  
  public void invalidateRoot() {
    invalidateVirtualView(-1, 1);
  }
  
  public void invalidateVirtualView(int paramInt) {
    invalidateVirtualView(paramInt, 0);
  }
  
  public void invalidateVirtualView(int paramInt1, int paramInt2) {
    if (paramInt1 != Integer.MIN_VALUE && this.mManager.isEnabled()) {
      ViewParent viewParent = this.mView.getParent();
      if (viewParent != null) {
        AccessibilityEvent accessibilityEvent = createEvent(paramInt1, 2048);
        accessibilityEvent.setContentChangeTypes(paramInt2);
        viewParent.requestSendAccessibilityEvent(this.mView, accessibilityEvent);
      } 
    } 
  }
  
  public int getFocusedVirtualView() {
    return this.mFocusedVirtualViewId;
  }
  
  private void updateHoveredVirtualView(int paramInt) {
    if (this.mHoveredVirtualViewId == paramInt)
      return; 
    int i = this.mHoveredVirtualViewId;
    this.mHoveredVirtualViewId = paramInt;
    sendEventForVirtualView(paramInt, 128);
    sendEventForVirtualView(i, 256);
  }
  
  private AccessibilityEvent createEvent(int paramInt1, int paramInt2) {
    if (paramInt1 != -1)
      return createEventForChild(paramInt1, paramInt2); 
    return createEventForHost(paramInt2);
  }
  
  private AccessibilityEvent createEventForHost(int paramInt) {
    AccessibilityEvent accessibilityEvent = AccessibilityEvent.obtain(paramInt);
    this.mView.onInitializeAccessibilityEvent(accessibilityEvent);
    onPopulateEventForHost(accessibilityEvent);
    return accessibilityEvent;
  }
  
  private AccessibilityEvent createEventForChild(int paramInt1, int paramInt2) {
    AccessibilityEvent accessibilityEvent = AccessibilityEvent.obtain(paramInt2);
    accessibilityEvent.setEnabled(true);
    accessibilityEvent.setClassName(DEFAULT_CLASS_NAME);
    onPopulateEventForVirtualView(paramInt1, accessibilityEvent);
    if (!accessibilityEvent.getText().isEmpty() || accessibilityEvent.getContentDescription() != null) {
      accessibilityEvent.setPackageName(this.mView.getContext().getPackageName());
      accessibilityEvent.setSource(this.mView, paramInt1);
      return accessibilityEvent;
    } 
    throw new RuntimeException("Callbacks must add text or a content description in populateEventForVirtualViewId()");
  }
  
  private AccessibilityNodeInfo createNode(int paramInt) {
    if (paramInt != -1)
      return createNodeForChild(paramInt); 
    return createNodeForHost();
  }
  
  private AccessibilityNodeInfo createNodeForHost() {
    AccessibilityNodeInfo accessibilityNodeInfo = AccessibilityNodeInfo.obtain(this.mView);
    this.mView.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
    int i = accessibilityNodeInfo.getChildCount();
    onPopulateNodeForHost(accessibilityNodeInfo);
    IntArray intArray = this.mTempArray;
    if (intArray == null) {
      this.mTempArray = new IntArray();
    } else {
      intArray.clear();
    } 
    intArray = this.mTempArray;
    getVisibleVirtualViews(intArray);
    if (i <= 0 || intArray.size() <= 0) {
      int j = intArray.size();
      for (i = 0; i < j; i++)
        accessibilityNodeInfo.addChild(this.mView, intArray.get(i)); 
      return accessibilityNodeInfo;
    } 
    throw new RuntimeException("Views cannot have both real and virtual children");
  }
  
  private AccessibilityNodeInfo createNodeForChild(int paramInt) {
    ensureTempRects();
    Rect rect1 = this.mTempParentRect;
    int[] arrayOfInt = this.mTempGlobalRect;
    Rect rect2 = this.mTempScreenRect;
    AccessibilityNodeInfo accessibilityNodeInfo = AccessibilityNodeInfo.obtain();
    accessibilityNodeInfo.setEnabled(true);
    accessibilityNodeInfo.setClassName(DEFAULT_CLASS_NAME);
    accessibilityNodeInfo.setBoundsInParent(INVALID_PARENT_BOUNDS);
    onPopulateNodeForVirtualView(paramInt, accessibilityNodeInfo);
    if (accessibilityNodeInfo.getText() != null || accessibilityNodeInfo.getContentDescription() != null) {
      accessibilityNodeInfo.getBoundsInParent(rect1);
      if (!rect1.equals(INVALID_PARENT_BOUNDS)) {
        int i = accessibilityNodeInfo.getActions();
        if ((i & 0x40) == 0) {
          if ((i & 0x80) == 0) {
            accessibilityNodeInfo.setPackageName(this.mView.getContext().getPackageName());
            accessibilityNodeInfo.setSource(this.mView, paramInt);
            accessibilityNodeInfo.setParent(this.mView);
            if (this.mFocusedVirtualViewId == paramInt) {
              accessibilityNodeInfo.setAccessibilityFocused(true);
              accessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_CLEAR_ACCESSIBILITY_FOCUS);
            } else {
              accessibilityNodeInfo.setAccessibilityFocused(false);
              accessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_ACCESSIBILITY_FOCUS);
            } 
            if (intersectVisibleToUser(rect1)) {
              accessibilityNodeInfo.setVisibleToUser(true);
              accessibilityNodeInfo.setBoundsInParent(rect1);
            } 
            this.mView.getLocationOnScreen(arrayOfInt);
            i = arrayOfInt[0];
            paramInt = arrayOfInt[1];
            rect2.set(rect1);
            rect2.offset(i, paramInt);
            accessibilityNodeInfo.setBoundsInScreen(rect2);
            return accessibilityNodeInfo;
          } 
          throw new RuntimeException("Callbacks must not add ACTION_CLEAR_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
        } 
        throw new RuntimeException("Callbacks must not add ACTION_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
      } 
      throw new RuntimeException("Callbacks must set parent bounds in populateNodeForVirtualViewId()");
    } 
    throw new RuntimeException("Callbacks must add text or a content description in populateNodeForVirtualViewId()");
  }
  
  private void ensureTempRects() {
    this.mTempGlobalRect = new int[2];
    this.mTempParentRect = new Rect();
    this.mTempScreenRect = new Rect();
  }
  
  private boolean performAction(int paramInt1, int paramInt2, Bundle paramBundle) {
    if (paramInt1 != -1)
      return performActionForChild(paramInt1, paramInt2, paramBundle); 
    return performActionForHost(paramInt2, paramBundle);
  }
  
  private boolean performActionForHost(int paramInt, Bundle paramBundle) {
    return this.mView.performAccessibilityAction(paramInt, paramBundle);
  }
  
  private boolean performActionForChild(int paramInt1, int paramInt2, Bundle paramBundle) {
    if (paramInt2 != 64 && paramInt2 != 128)
      return onPerformActionForVirtualView(paramInt1, paramInt2, paramBundle); 
    return manageFocusForChild(paramInt1, paramInt2);
  }
  
  private boolean manageFocusForChild(int paramInt1, int paramInt2) {
    if (paramInt2 != 64) {
      if (paramInt2 != 128)
        return false; 
      return clearAccessibilityFocus(paramInt1);
    } 
    return requestAccessibilityFocus(paramInt1);
  }
  
  private boolean intersectVisibleToUser(Rect paramRect) {
    if (paramRect == null || paramRect.isEmpty())
      return false; 
    if (this.mView.getWindowVisibility() != 0)
      return false; 
    ViewParent viewParent = this.mView.getParent();
    while (viewParent instanceof View) {
      View view = (View)viewParent;
      if (view.getAlpha() <= 0.0F || view.getVisibility() != 0)
        return false; 
      viewParent = view.getParent();
    } 
    if (viewParent == null)
      return false; 
    if (this.mTempVisibleRect == null)
      this.mTempVisibleRect = new Rect(); 
    Rect rect = this.mTempVisibleRect;
    if (!this.mView.getLocalVisibleRect(rect))
      return false; 
    return paramRect.intersect(rect);
  }
  
  private boolean isAccessibilityFocused(int paramInt) {
    boolean bool;
    if (this.mFocusedVirtualViewId == paramInt) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean requestAccessibilityFocus(int paramInt) {
    Context context = this.mContext;
    AccessibilityManager accessibilityManager = (AccessibilityManager)context.getSystemService("accessibility");
    if (!this.mManager.isEnabled() || 
      !accessibilityManager.isTouchExplorationEnabled())
      return false; 
    if (!isAccessibilityFocused(paramInt)) {
      int i = this.mFocusedVirtualViewId;
      if (i != Integer.MIN_VALUE)
        sendEventForVirtualView(i, 65536); 
      this.mFocusedVirtualViewId = paramInt;
      this.mView.invalidate();
      sendEventForVirtualView(paramInt, 32768);
      return true;
    } 
    return false;
  }
  
  private boolean clearAccessibilityFocus(int paramInt) {
    if (isAccessibilityFocused(paramInt)) {
      this.mFocusedVirtualViewId = Integer.MIN_VALUE;
      this.mView.invalidate();
      sendEventForVirtualView(paramInt, 65536);
      return true;
    } 
    return false;
  }
  
  protected void onPopulateEventForHost(AccessibilityEvent paramAccessibilityEvent) {}
  
  protected void onPopulateNodeForHost(AccessibilityNodeInfo paramAccessibilityNodeInfo) {}
  
  protected abstract int getVirtualViewAt(float paramFloat1, float paramFloat2);
  
  protected abstract void getVisibleVirtualViews(IntArray paramIntArray);
  
  protected abstract boolean onPerformActionForVirtualView(int paramInt1, int paramInt2, Bundle paramBundle);
  
  protected abstract void onPopulateEventForVirtualView(int paramInt, AccessibilityEvent paramAccessibilityEvent);
  
  protected abstract void onPopulateNodeForVirtualView(int paramInt, AccessibilityNodeInfo paramAccessibilityNodeInfo);
  
  private class ExploreByTouchNodeProvider extends AccessibilityNodeProvider {
    final ExploreByTouchHelper this$0;
    
    private ExploreByTouchNodeProvider() {}
    
    public AccessibilityNodeInfo createAccessibilityNodeInfo(int param1Int) {
      return ExploreByTouchHelper.this.createNode(param1Int);
    }
    
    public boolean performAction(int param1Int1, int param1Int2, Bundle param1Bundle) {
      return ExploreByTouchHelper.this.performAction(param1Int1, param1Int2, param1Bundle);
    }
  }
}
