package com.android.internal.widget;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.common.ColorFrameworkFactory;
import android.common.IOplusCommonFeature;
import android.common.OplusFeatureCache;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Property;
import android.util.Size;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.IOplusFtHooks;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.android.internal.util.Preconditions;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public final class FloatingToolbar {
  private static final MenuItem.OnMenuItemClickListener NO_OP_MENUITEM_CLICK_LISTENER = (MenuItem.OnMenuItemClickListener)_$$Lambda$FloatingToolbar$7_enOzxeypZYfdFYr1HzBLfj47k.INSTANCE;
  
  private final Rect mContentRect = new Rect();
  
  private final Rect mPreviousContentRect = new Rect();
  
  private List<MenuItem> mShowingMenuItems = new ArrayList<>();
  
  private MenuItem.OnMenuItemClickListener mMenuItemClickListener = NO_OP_MENUITEM_CLICK_LISTENER;
  
  private boolean mWidthChanged = true;
  
  private final View.OnLayoutChangeListener mOrientationChangeHandler = (View.OnLayoutChangeListener)new Object(this);
  
  private final Comparator<MenuItem> mMenuItemComparator = (Comparator<MenuItem>)_$$Lambda$FloatingToolbar$LutnsyBKrZiroTBekgIjhIyrl40.INSTANCE;
  
  public static final String FLOATING_TOOLBAR_TAG = "floating_toolbar";
  
  private final Context mContext;
  
  private Menu mMenu;
  
  private final FloatingToolbarPopup mPopup;
  
  private int mSuggestedWidth;
  
  private final Window mWindow;
  
  public FloatingToolbar(Window paramWindow) {
    this.mContext = applyDefaultTheme(paramWindow.getContext());
    Objects.requireNonNull(paramWindow);
    this.mWindow = paramWindow;
    this.mPopup = new FloatingToolbarPopup(this.mContext, paramWindow.getDecorView());
  }
  
  public FloatingToolbar setMenu(Menu paramMenu) {
    Objects.requireNonNull(paramMenu);
    this.mMenu = paramMenu;
    return this;
  }
  
  public FloatingToolbar setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener paramOnMenuItemClickListener) {
    if (paramOnMenuItemClickListener != null) {
      this.mMenuItemClickListener = paramOnMenuItemClickListener;
    } else {
      this.mMenuItemClickListener = NO_OP_MENUITEM_CLICK_LISTENER;
    } 
    return this;
  }
  
  public FloatingToolbar setContentRect(Rect paramRect) {
    Rect rect = this.mContentRect;
    Objects.requireNonNull(paramRect);
    rect.set(paramRect);
    return this;
  }
  
  public FloatingToolbar setSuggestedWidth(int paramInt) {
    boolean bool;
    int i = Math.abs(paramInt - this.mSuggestedWidth);
    if (i > this.mSuggestedWidth * 0.2D) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mWidthChanged = bool;
    this.mSuggestedWidth = paramInt;
    return this;
  }
  
  public FloatingToolbar show() {
    registerOrientationHandler();
    doShow();
    return this;
  }
  
  public FloatingToolbar updateLayout() {
    if (this.mPopup.isShowing())
      doShow(); 
    return this;
  }
  
  public void dismiss() {
    unregisterOrientationHandler();
    this.mPopup.dismiss();
  }
  
  public void hide() {
    this.mPopup.hide();
  }
  
  public boolean isShowing() {
    return this.mPopup.isShowing();
  }
  
  public boolean isHidden() {
    return this.mPopup.isHidden();
  }
  
  public void setOutsideTouchable(boolean paramBoolean, PopupWindow.OnDismissListener paramOnDismissListener) {
    if (this.mPopup.setOutsideTouchable(paramBoolean, paramOnDismissListener) && isShowing()) {
      dismiss();
      doShow();
    } 
  }
  
  private void doShow() {
    List<MenuItem> list = getVisibleAndEnabledMenuItems(this.mMenu);
    list.sort(this.mMenuItemComparator);
    if (!isCurrentlyShowing(list) || this.mWidthChanged) {
      this.mPopup.dismiss();
      this.mPopup.layoutMenuItems(list, this.mMenuItemClickListener, this.mSuggestedWidth);
      this.mShowingMenuItems = list;
    } 
    if (!this.mPopup.isShowing()) {
      this.mPopup.show(this.mContentRect);
    } else if (!this.mPreviousContentRect.equals(this.mContentRect)) {
      this.mPopup.updateCoordinates(this.mContentRect);
    } 
    this.mWidthChanged = false;
    this.mPreviousContentRect.set(this.mContentRect);
  }
  
  private boolean isCurrentlyShowing(List<MenuItem> paramList) {
    if (this.mShowingMenuItems == null || paramList.size() != this.mShowingMenuItems.size())
      return false; 
    int i = paramList.size();
    for (byte b = 0; b < i; b++) {
      MenuItem menuItem1 = paramList.get(b);
      MenuItem menuItem2 = this.mShowingMenuItems.get(b);
      if (menuItem1.getItemId() != menuItem2.getItemId() || 
        !TextUtils.equals(menuItem1.getTitle(), menuItem2.getTitle()) || 
        !Objects.equals(menuItem1.getIcon(), menuItem2.getIcon()) || 
        menuItem1.getGroupId() != menuItem2.getGroupId())
        return false; 
    } 
    return true;
  }
  
  private List<MenuItem> getVisibleAndEnabledMenuItems(Menu paramMenu) {
    ArrayList<MenuItem> arrayList = new ArrayList();
    for (byte b = 0; paramMenu != null && b < paramMenu.size(); b++) {
      MenuItem menuItem = paramMenu.getItem(b);
      if (menuItem.isVisible() && menuItem.isEnabled()) {
        SubMenu subMenu = menuItem.getSubMenu();
        if (subMenu != null) {
          arrayList.addAll(getVisibleAndEnabledMenuItems((Menu)subMenu));
        } else {
          arrayList.add(menuItem);
        } 
      } 
    } 
    return arrayList;
  }
  
  private void registerOrientationHandler() {
    unregisterOrientationHandler();
    this.mWindow.getDecorView().addOnLayoutChangeListener(this.mOrientationChangeHandler);
  }
  
  private void unregisterOrientationHandler() {
    this.mWindow.getDecorView().removeOnLayoutChangeListener(this.mOrientationChangeHandler);
  }
  
  private static final class FloatingToolbarPopup {
    private static final int MIN_OVERFLOW_SIZE = ((IOplusFtHooks)ColorFrameworkFactory.getInstance().getFeature((IOplusCommonFeature)IOplusFtHooks.DEFAULT, new Object[0])).getMinOverscrollSize();
    
    static {
    
    }
    
    private final Rect mViewPortOnScreen = new Rect();
    
    private final Point mCoordsOnWindow = new Point();
    
    private final int[] mTmpCoords = new int[2];
    
    private final Region mTouchableRegion = new Region();
    
    private final ViewTreeObserver.OnComputeInternalInsetsListener mInsetsComputer = new _$$Lambda$FloatingToolbar$FloatingToolbarPopup$77YZy6kisO5OnjlgtKp0Zi1V8EY(this);
    
    private final Runnable mPreparePopupContentRTLHelper = new Runnable() {
        final FloatingToolbar.FloatingToolbarPopup this$0;
        
        public void run() {
          FloatingToolbar.FloatingToolbarPopup.this.setPanelsStatesAtRestingPosition();
          FloatingToolbar.FloatingToolbarPopup.this.setContentAreaAsTouchableSurface();
          FloatingToolbar.FloatingToolbarPopup.this.mContentContainer.setAlpha(1.0F);
        }
      };
    
    private boolean mDismissed = true;
    
    private final View.OnClickListener mMenuItemButtonOnClickListener = (View.OnClickListener)new Object(this);
    
    private static final int MAX_OVERFLOW_SIZE = ((IOplusFtHooks)ColorFrameworkFactory.getInstance().getFeature((IOplusCommonFeature)IOplusFtHooks.DEFAULT, new Object[0])).getMaxOverscrollSize();
    
    private final Drawable mArrow;
    
    private final AnimationSet mCloseOverflowAnimation;
    
    private final ViewGroup mContentContainer;
    
    private final Context mContext;
    
    private final AnimatorSet mDismissAnimation;
    
    private final Interpolator mFastOutLinearInInterpolator;
    
    private final Interpolator mFastOutSlowInInterpolator;
    
    private boolean mHidden;
    
    private final AnimatorSet mHideAnimation;
    
    private final int mIconTextSpacing;
    
    private boolean mIsOverflowOpen;
    
    private final int mLineHeight;
    
    private final Interpolator mLinearOutSlowInInterpolator;
    
    private final Interpolator mLogAccelerateInterpolator;
    
    private final ViewGroup mMainPanel;
    
    private Size mMainPanelSize;
    
    private final int mMarginHorizontal;
    
    private final int mMarginVertical;
    
    private MenuItem.OnMenuItemClickListener mOnMenuItemClickListener;
    
    private final AnimationSet mOpenOverflowAnimation;
    
    private boolean mOpenOverflowUpwards;
    
    private final Drawable mOverflow;
    
    private final Animation.AnimationListener mOverflowAnimationListener;
    
    private final ImageButton mOverflowButton;
    
    private final Size mOverflowButtonSize;
    
    private final OverflowPanel mOverflowPanel;
    
    private Size mOverflowPanelSize;
    
    private final OverflowPanelViewHelper mOverflowPanelViewHelper;
    
    private final View mParent;
    
    private final PopupWindow mPopupWindow;
    
    private final AnimatorSet mShowAnimation;
    
    private final AnimatedVectorDrawable mToArrow;
    
    private final AnimatedVectorDrawable mToOverflow;
    
    private int mTransitionDurationScale;
    
    public FloatingToolbarPopup(Context param1Context, View param1View) {
      Objects.requireNonNull(param1View);
      this.mParent = param1View;
      Objects.requireNonNull(param1Context);
      this.mContext = param1Context;
      ViewGroup viewGroup = FloatingToolbar.createContentContainer(param1Context);
      this.mPopupWindow = FloatingToolbar.createPopupWindow(viewGroup);
      Resources resources3 = param1View.getResources();
      this.mMarginHorizontal = resources3.getDimensionPixelSize(17105187);
      Resources resources2 = param1View.getResources();
      this.mMarginVertical = resources2.getDimensionPixelSize(17105200);
      resources2 = param1Context.getResources();
      this.mLineHeight = resources2.getDimensionPixelSize(((IOplusFtHooks)ColorFrameworkFactory.getInstance().getFeature((IOplusCommonFeature)IOplusFtHooks.DEFAULT, new Object[0])).getFloatingToolBarHeightRes());
      Resources resources1 = param1Context.getResources();
      this.mIconTextSpacing = resources1.getDimensionPixelSize(17105188);
      this.mLogAccelerateInterpolator = new LogAccelerateInterpolator();
      this.mFastOutSlowInInterpolator = AnimationUtils.loadInterpolator(this.mContext, 17563661);
      this.mLinearOutSlowInInterpolator = AnimationUtils.loadInterpolator(this.mContext, 17563662);
      this.mFastOutLinearInInterpolator = AnimationUtils.loadInterpolator(this.mContext, 17563663);
      Drawable drawable = ((IOplusFtHooks)ColorFrameworkFactory.getInstance().getFeature((IOplusCommonFeature)IOplusFtHooks.DEFAULT, new Object[0])).getArrowDrawable(this.mContext);
      drawable.setAutoMirrored(true);
      this.mOverflow = drawable = ((IOplusFtHooks)ColorFrameworkFactory.getInstance().getFeature((IOplusCommonFeature)IOplusFtHooks.DEFAULT, new Object[0])).getOverflowDrawable(this.mContext);
      drawable.setAutoMirrored(true);
      AnimatedVectorDrawable animatedVectorDrawable = ((IOplusFtHooks)ColorFrameworkFactory.getInstance().getFeature((IOplusCommonFeature)IOplusFtHooks.DEFAULT, new Object[0])).getToArrowAnim(this.mContext);
      animatedVectorDrawable.setAutoMirrored(true);
      this.mToOverflow = animatedVectorDrawable = ((IOplusFtHooks)ColorFrameworkFactory.getInstance().getFeature((IOplusCommonFeature)IOplusFtHooks.DEFAULT, new Object[0])).getToOverflowAnim(this.mContext);
      animatedVectorDrawable.setAutoMirrored(true);
      ImageButton imageButton = createOverflowButton();
      this.mOverflowButtonSize = measure((View)imageButton);
      this.mMainPanel = createMainPanel();
      this.mOverflowPanelViewHelper = new OverflowPanelViewHelper(this.mContext, this.mIconTextSpacing);
      this.mOverflowPanel = createOverflowPanel();
      this.mOverflowAnimationListener = createOverflowAnimationListener();
      AnimationSet animationSet = new AnimationSet(true);
      animationSet.setAnimationListener(this.mOverflowAnimationListener);
      this.mCloseOverflowAnimation = animationSet = new AnimationSet(true);
      animationSet.setAnimationListener(this.mOverflowAnimationListener);
      this.mShowAnimation = FloatingToolbar.createEnterAnimation((View)this.mContentContainer);
      this.mDismissAnimation = FloatingToolbar.createExitAnimation((View)this.mContentContainer, 150, (Animator.AnimatorListener)new Object(this));
      this.mHideAnimation = FloatingToolbar.createExitAnimation((View)this.mContentContainer, 0, (Animator.AnimatorListener)new Object(this));
    }
    
    public boolean setOutsideTouchable(boolean param1Boolean, PopupWindow.OnDismissListener param1OnDismissListener) {
      boolean bool = false;
      if (this.mPopupWindow.isOutsideTouchable() ^ param1Boolean) {
        this.mPopupWindow.setOutsideTouchable(param1Boolean);
        this.mPopupWindow.setFocusable(param1Boolean ^ true);
        bool = true;
      } 
      this.mPopupWindow.setOnDismissListener(param1OnDismissListener);
      return bool;
    }
    
    public void layoutMenuItems(List<MenuItem> param1List, MenuItem.OnMenuItemClickListener param1OnMenuItemClickListener, int param1Int) {
      this.mOnMenuItemClickListener = param1OnMenuItemClickListener;
      cancelOverflowAnimations();
      clearPanels();
      param1List = layoutMainPanelItems(param1List, getAdjustedToolbarWidth(param1Int));
      if (!param1List.isEmpty())
        layoutOverflowPanelItems(param1List); 
      updatePopupSize();
    }
    
    public void show(Rect param1Rect) {
      Objects.requireNonNull(param1Rect);
      if (isShowing())
        return; 
      this.mHidden = false;
      this.mDismissed = false;
      cancelDismissAndHideAnimations();
      cancelOverflowAnimations();
      refreshCoordinatesAndOverflowDirection(param1Rect);
      preparePopupContent();
      this.mPopupWindow.showAtLocation(this.mParent, 0, this.mCoordsOnWindow.x, this.mCoordsOnWindow.y);
      setTouchableSurfaceInsetsComputer();
      runShowAnimation();
    }
    
    public void dismiss() {
      if (this.mDismissed)
        return; 
      this.mHidden = false;
      this.mDismissed = true;
      this.mHideAnimation.cancel();
      runDismissAnimation();
      setZeroTouchableSurface();
    }
    
    public void hide() {
      if (!isShowing())
        return; 
      this.mHidden = true;
      runHideAnimation();
      setZeroTouchableSurface();
    }
    
    public boolean isShowing() {
      boolean bool;
      if (!this.mDismissed && !this.mHidden) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean isHidden() {
      return this.mHidden;
    }
    
    public void updateCoordinates(Rect param1Rect) {
      Objects.requireNonNull(param1Rect);
      if (!isShowing() || !this.mPopupWindow.isShowing())
        return; 
      cancelOverflowAnimations();
      refreshCoordinatesAndOverflowDirection(param1Rect);
      preparePopupContent();
      PopupWindow popupWindow2 = this.mPopupWindow;
      int i = this.mCoordsOnWindow.x, j = this.mCoordsOnWindow.y;
      PopupWindow popupWindow1 = this.mPopupWindow;
      int k = popupWindow1.getWidth(), m = this.mPopupWindow.getHeight();
      popupWindow2.update(i, j, k, m);
    }
    
    private void refreshCoordinatesAndOverflowDirection(Rect param1Rect) {
      // Byte code:
      //   0: aload_0
      //   1: invokespecial refreshViewPort : ()V
      //   4: aload_1
      //   5: invokevirtual centerX : ()I
      //   8: istore_2
      //   9: aload_0
      //   10: getfield mPopupWindow : Landroid/widget/PopupWindow;
      //   13: invokevirtual getWidth : ()I
      //   16: iconst_2
      //   17: idiv
      //   18: istore_3
      //   19: aload_0
      //   20: getfield mViewPortOnScreen : Landroid/graphics/Rect;
      //   23: getfield right : I
      //   26: istore #4
      //   28: aload_0
      //   29: getfield mPopupWindow : Landroid/widget/PopupWindow;
      //   32: astore #5
      //   34: aload #5
      //   36: invokevirtual getWidth : ()I
      //   39: istore #6
      //   41: iload_2
      //   42: iload_3
      //   43: isub
      //   44: iload #4
      //   46: iload #6
      //   48: isub
      //   49: invokestatic min : (II)I
      //   52: istore_3
      //   53: aload_1
      //   54: getfield top : I
      //   57: aload_0
      //   58: getfield mViewPortOnScreen : Landroid/graphics/Rect;
      //   61: getfield top : I
      //   64: isub
      //   65: istore #4
      //   67: aload_0
      //   68: getfield mViewPortOnScreen : Landroid/graphics/Rect;
      //   71: getfield bottom : I
      //   74: aload_1
      //   75: getfield bottom : I
      //   78: isub
      //   79: istore #7
      //   81: aload_0
      //   82: getfield mMarginVertical : I
      //   85: iconst_2
      //   86: imul
      //   87: istore #6
      //   89: aload_0
      //   90: getfield mLineHeight : I
      //   93: iload #6
      //   95: iadd
      //   96: istore_2
      //   97: aload_0
      //   98: invokespecial hasOverflow : ()Z
      //   101: ifne -> 176
      //   104: iload #4
      //   106: iload_2
      //   107: if_icmplt -> 120
      //   110: aload_1
      //   111: getfield top : I
      //   114: iload_2
      //   115: isub
      //   116: istore_2
      //   117: goto -> 432
      //   120: iload #7
      //   122: iload_2
      //   123: if_icmplt -> 134
      //   126: aload_1
      //   127: getfield bottom : I
      //   130: istore_2
      //   131: goto -> 432
      //   134: iload #7
      //   136: aload_0
      //   137: getfield mLineHeight : I
      //   140: if_icmplt -> 156
      //   143: aload_1
      //   144: getfield bottom : I
      //   147: aload_0
      //   148: getfield mMarginVertical : I
      //   151: isub
      //   152: istore_2
      //   153: goto -> 432
      //   156: aload_0
      //   157: getfield mViewPortOnScreen : Landroid/graphics/Rect;
      //   160: getfield top : I
      //   163: aload_1
      //   164: getfield top : I
      //   167: iload_2
      //   168: isub
      //   169: invokestatic max : (II)I
      //   172: istore_2
      //   173: goto -> 432
      //   176: getstatic com/android/internal/widget/FloatingToolbar$FloatingToolbarPopup.MIN_OVERFLOW_SIZE : I
      //   179: istore #8
      //   181: aload_0
      //   182: iload #8
      //   184: invokespecial calculateOverflowHeight : (I)I
      //   187: iload #6
      //   189: iadd
      //   190: istore #9
      //   192: aload_0
      //   193: getfield mViewPortOnScreen : Landroid/graphics/Rect;
      //   196: getfield bottom : I
      //   199: aload_1
      //   200: getfield top : I
      //   203: isub
      //   204: iload_2
      //   205: iadd
      //   206: istore #10
      //   208: aload_1
      //   209: getfield bottom : I
      //   212: istore #8
      //   214: aload_0
      //   215: getfield mViewPortOnScreen : Landroid/graphics/Rect;
      //   218: getfield top : I
      //   221: istore #11
      //   223: iload #4
      //   225: iload #9
      //   227: if_icmplt -> 266
      //   230: aload_0
      //   231: iload #4
      //   233: iload #6
      //   235: isub
      //   236: invokespecial updateOverflowHeight : (I)V
      //   239: aload_1
      //   240: getfield top : I
      //   243: istore #4
      //   245: aload_0
      //   246: getfield mPopupWindow : Landroid/widget/PopupWindow;
      //   249: invokevirtual getHeight : ()I
      //   252: istore_2
      //   253: aload_0
      //   254: iconst_1
      //   255: putfield mOpenOverflowUpwards : Z
      //   258: iload #4
      //   260: iload_2
      //   261: isub
      //   262: istore_2
      //   263: goto -> 432
      //   266: iload #4
      //   268: iload_2
      //   269: if_icmplt -> 307
      //   272: iload #10
      //   274: iload #9
      //   276: if_icmplt -> 307
      //   279: aload_0
      //   280: iload #10
      //   282: iload #6
      //   284: isub
      //   285: invokespecial updateOverflowHeight : (I)V
      //   288: aload_1
      //   289: getfield top : I
      //   292: istore #4
      //   294: aload_0
      //   295: iconst_0
      //   296: putfield mOpenOverflowUpwards : Z
      //   299: iload #4
      //   301: iload_2
      //   302: isub
      //   303: istore_2
      //   304: goto -> 432
      //   307: iload #7
      //   309: iload #9
      //   311: if_icmplt -> 336
      //   314: aload_0
      //   315: iload #7
      //   317: iload #6
      //   319: isub
      //   320: invokespecial updateOverflowHeight : (I)V
      //   323: aload_1
      //   324: getfield bottom : I
      //   327: istore_2
      //   328: aload_0
      //   329: iconst_0
      //   330: putfield mOpenOverflowUpwards : Z
      //   333: goto -> 432
      //   336: iload #7
      //   338: iload_2
      //   339: if_icmplt -> 405
      //   342: aload_0
      //   343: getfield mViewPortOnScreen : Landroid/graphics/Rect;
      //   346: astore #5
      //   348: aload #5
      //   350: invokevirtual height : ()I
      //   353: iload #9
      //   355: if_icmplt -> 405
      //   358: aload_0
      //   359: iload #8
      //   361: iload #11
      //   363: isub
      //   364: iload_2
      //   365: iadd
      //   366: iload #6
      //   368: isub
      //   369: invokespecial updateOverflowHeight : (I)V
      //   372: aload_1
      //   373: getfield bottom : I
      //   376: istore #6
      //   378: aload_0
      //   379: getfield mPopupWindow : Landroid/widget/PopupWindow;
      //   382: astore_1
      //   383: aload_1
      //   384: invokevirtual getHeight : ()I
      //   387: istore #4
      //   389: aload_0
      //   390: iconst_1
      //   391: putfield mOpenOverflowUpwards : Z
      //   394: iload #6
      //   396: iload_2
      //   397: iadd
      //   398: iload #4
      //   400: isub
      //   401: istore_2
      //   402: goto -> 432
      //   405: aload_0
      //   406: aload_0
      //   407: getfield mViewPortOnScreen : Landroid/graphics/Rect;
      //   410: invokevirtual height : ()I
      //   413: iload #6
      //   415: isub
      //   416: invokespecial updateOverflowHeight : (I)V
      //   419: aload_0
      //   420: getfield mViewPortOnScreen : Landroid/graphics/Rect;
      //   423: getfield top : I
      //   426: istore_2
      //   427: aload_0
      //   428: iconst_0
      //   429: putfield mOpenOverflowUpwards : Z
      //   432: aload_0
      //   433: getfield mParent : Landroid/view/View;
      //   436: invokevirtual getRootView : ()Landroid/view/View;
      //   439: aload_0
      //   440: getfield mTmpCoords : [I
      //   443: invokevirtual getLocationOnScreen : ([I)V
      //   446: aload_0
      //   447: getfield mTmpCoords : [I
      //   450: astore_1
      //   451: aload_1
      //   452: iconst_0
      //   453: iaload
      //   454: istore #7
      //   456: aload_1
      //   457: iconst_1
      //   458: iaload
      //   459: istore #6
      //   461: aload_0
      //   462: getfield mParent : Landroid/view/View;
      //   465: invokevirtual getRootView : ()Landroid/view/View;
      //   468: aload_0
      //   469: getfield mTmpCoords : [I
      //   472: invokevirtual getLocationInWindow : ([I)V
      //   475: aload_0
      //   476: getfield mTmpCoords : [I
      //   479: astore_1
      //   480: aload_1
      //   481: iconst_0
      //   482: iaload
      //   483: istore #8
      //   485: aload_1
      //   486: iconst_1
      //   487: iaload
      //   488: istore #4
      //   490: aload_0
      //   491: getfield mCoordsOnWindow : Landroid/graphics/Point;
      //   494: astore_1
      //   495: iconst_0
      //   496: iload_3
      //   497: iload #7
      //   499: iload #8
      //   501: isub
      //   502: isub
      //   503: invokestatic max : (II)I
      //   506: istore_3
      //   507: iconst_0
      //   508: iload_2
      //   509: iload #6
      //   511: iload #4
      //   513: isub
      //   514: isub
      //   515: invokestatic max : (II)I
      //   518: istore_2
      //   519: aload_1
      //   520: iload_3
      //   521: iload_2
      //   522: invokevirtual set : (II)V
      //   525: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #683	-> 0
      //   #687	-> 4
      //   #688	-> 4
      //   #689	-> 34
      //   #687	-> 41
      //   #693	-> 53
      //   #695	-> 67
      //   #698	-> 81
      //   #699	-> 89
      //   #701	-> 97
      //   #702	-> 104
      //   #704	-> 110
      //   #705	-> 120
      //   #707	-> 126
      //   #708	-> 134
      //   #710	-> 143
      //   #713	-> 156
      //   #719	-> 176
      //   #720	-> 181
      //   #721	-> 192
      //   #723	-> 208
      //   #726	-> 223
      //   #729	-> 230
      //   #730	-> 239
      //   #731	-> 253
      //   #732	-> 266
      //   #737	-> 279
      //   #738	-> 288
      //   #739	-> 294
      //   #740	-> 307
      //   #743	-> 314
      //   #744	-> 323
      //   #745	-> 328
      //   #746	-> 336
      //   #747	-> 348
      //   #751	-> 358
      //   #752	-> 372
      //   #753	-> 383
      //   #754	-> 389
      //   #758	-> 405
      //   #759	-> 419
      //   #760	-> 427
      //   #771	-> 432
      //   #772	-> 446
      //   #773	-> 456
      //   #774	-> 461
      //   #775	-> 475
      //   #776	-> 485
      //   #777	-> 490
      //   #778	-> 490
      //   #779	-> 490
      //   #780	-> 495
      //   #779	-> 519
      //   #781	-> 525
    }
    
    private void runShowAnimation() {
      this.mShowAnimation.start();
    }
    
    private void runDismissAnimation() {
      this.mDismissAnimation.start();
    }
    
    private void runHideAnimation() {
      this.mHideAnimation.start();
    }
    
    private void cancelDismissAndHideAnimations() {
      this.mDismissAnimation.cancel();
      this.mHideAnimation.cancel();
    }
    
    private void cancelOverflowAnimations() {
      this.mContentContainer.clearAnimation();
      this.mMainPanel.animate().cancel();
      this.mOverflowPanel.animate().cancel();
      this.mToArrow.stop();
      this.mToOverflow.stop();
    }
    
    private void openOverflow() {
      int i = this.mOverflowPanelSize.getWidth();
      int j = this.mOverflowPanelSize.getHeight();
      int k = this.mContentContainer.getWidth();
      int m = this.mContentContainer.getHeight();
      float f1 = this.mContentContainer.getY();
      float f2 = this.mContentContainer.getX();
      float f3 = this.mContentContainer.getWidth();
      Object object1 = new Object(this, i, k, f2, f2 + f3);
      Object object2 = new Object(this, j, m, f1);
      f1 = this.mOverflowButton.getX();
      if (isInRTLMode()) {
        f2 = i + f1 - this.mOverflowButton.getWidth();
      } else {
        f2 = f1 - i + this.mOverflowButton.getWidth();
      } 
      Object object3 = new Object(this, f1, f2, k);
      object1.setInterpolator(this.mLogAccelerateInterpolator);
      object1.setDuration(getAdjustedDuration(250));
      object2.setInterpolator(this.mFastOutSlowInInterpolator);
      object2.setDuration(getAdjustedDuration(250));
      object3.setInterpolator(this.mFastOutSlowInInterpolator);
      object3.setDuration(getAdjustedDuration(250));
      this.mOpenOverflowAnimation.getAnimations().clear();
      this.mOpenOverflowAnimation.getAnimations().clear();
      this.mOpenOverflowAnimation.addAnimation((Animation)object1);
      this.mOpenOverflowAnimation.addAnimation((Animation)object2);
      this.mOpenOverflowAnimation.addAnimation((Animation)object3);
      this.mContentContainer.startAnimation((Animation)this.mOpenOverflowAnimation);
      this.mIsOverflowOpen = true;
      object1 = this.mMainPanel.animate();
      object3 = object1.alpha(0.0F).withLayer();
      object1 = this.mLinearOutSlowInInterpolator;
      object1 = object3.setInterpolator((TimeInterpolator)object1);
      object1 = object1.setDuration(250L);
      object1.start();
      this.mOverflowPanel.setAlpha(1.0F);
    }
    
    private void closeOverflow() {
      int i = this.mMainPanelSize.getWidth();
      int j = this.mContentContainer.getWidth();
      float f1 = this.mContentContainer.getX();
      float f2 = this.mContentContainer.getWidth();
      Object object1 = new Object(this, i, j, f1, f1 + f2);
      int k = this.mMainPanelSize.getHeight();
      i = this.mContentContainer.getHeight();
      f1 = this.mContentContainer.getY();
      f2 = this.mContentContainer.getHeight();
      Object object2 = new Object(this, k, i, f1 + f2);
      f2 = this.mOverflowButton.getX();
      if (isInRTLMode()) {
        f1 = f2 - j + this.mOverflowButton.getWidth();
      } else {
        f1 = j + f2 - this.mOverflowButton.getWidth();
      } 
      Object object3 = new Object(this, f2, f1, j);
      object1.setInterpolator(this.mFastOutSlowInInterpolator);
      object1.setDuration(getAdjustedDuration(250));
      object2.setInterpolator(this.mLogAccelerateInterpolator);
      object2.setDuration(getAdjustedDuration(250));
      object3.setInterpolator(this.mFastOutSlowInInterpolator);
      object3.setDuration(getAdjustedDuration(250));
      this.mCloseOverflowAnimation.getAnimations().clear();
      this.mCloseOverflowAnimation.addAnimation((Animation)object1);
      this.mCloseOverflowAnimation.addAnimation((Animation)object2);
      this.mCloseOverflowAnimation.addAnimation((Animation)object3);
      this.mContentContainer.startAnimation((Animation)this.mCloseOverflowAnimation);
      this.mIsOverflowOpen = false;
      object3 = this.mMainPanel.animate();
      object1 = object3.alpha(1.0F).withLayer();
      object3 = this.mFastOutLinearInInterpolator;
      object3 = object1.setInterpolator((TimeInterpolator)object3);
      object3 = object3.setDuration(100L);
      object3.start();
      object3 = this.mOverflowPanel.animate();
      object3 = object3.alpha(0.0F).withLayer();
      object1 = this.mLinearOutSlowInInterpolator;
      object3 = object3.setInterpolator((TimeInterpolator)object1);
      object3 = object3.setDuration(150L);
      object3.start();
    }
    
    private void setPanelsStatesAtRestingPosition() {
      this.mOverflowButton.setEnabled(true);
      this.mOverflowPanel.awakenScrollBars();
      if (this.mIsOverflowOpen) {
        Size size = this.mOverflowPanelSize;
        setSize((View)this.mContentContainer, size);
        this.mMainPanel.setAlpha(0.0F);
        this.mMainPanel.setVisibility(4);
        this.mOverflowPanel.setAlpha(1.0F);
        this.mOverflowPanel.setVisibility(0);
        this.mOverflowButton.setImageDrawable(this.mArrow);
        this.mOverflowButton.setContentDescription(this.mContext.getString(17040242));
        if (isInRTLMode()) {
          this.mContentContainer.setX(this.mMarginHorizontal);
          this.mMainPanel.setX(0.0F);
          ImageButton imageButton = this.mOverflowButton;
          float f = (size.getWidth() - this.mOverflowButtonSize.getWidth());
          imageButton.setX(f);
          this.mOverflowPanel.setX(0.0F);
        } else {
          ViewGroup viewGroup = this.mContentContainer;
          PopupWindow popupWindow = this.mPopupWindow;
          int i = popupWindow.getWidth();
          float f = (i - size.getWidth() - this.mMarginHorizontal);
          viewGroup.setX(f);
          this.mMainPanel.setX(-this.mContentContainer.getX());
          this.mOverflowButton.setX(0.0F);
          this.mOverflowPanel.setX(0.0F);
        } 
        if (this.mOpenOverflowUpwards) {
          this.mContentContainer.setY(this.mMarginVertical);
          ViewGroup viewGroup = this.mMainPanel;
          float f = (size.getHeight() - this.mContentContainer.getHeight());
          viewGroup.setY(f);
          ImageButton imageButton = this.mOverflowButton;
          f = (size.getHeight() - this.mOverflowButtonSize.getHeight());
          imageButton.setY(f);
          this.mOverflowPanel.setY(0.0F);
        } else {
          this.mContentContainer.setY(this.mMarginVertical);
          this.mMainPanel.setY(0.0F);
          this.mOverflowButton.setY(0.0F);
          this.mOverflowPanel.setY(this.mOverflowButtonSize.getHeight());
        } 
      } else {
        Size size = this.mMainPanelSize;
        setSize((View)this.mContentContainer, size);
        this.mMainPanel.setAlpha(1.0F);
        this.mMainPanel.setVisibility(0);
        this.mOverflowPanel.setAlpha(0.0F);
        this.mOverflowPanel.setVisibility(4);
        this.mOverflowButton.setImageDrawable(this.mOverflow);
        this.mOverflowButton.setContentDescription(this.mContext.getString(17040243));
        if (hasOverflow()) {
          if (isInRTLMode()) {
            this.mContentContainer.setX(this.mMarginHorizontal);
            this.mMainPanel.setX(0.0F);
            this.mOverflowButton.setX(0.0F);
            this.mOverflowPanel.setX(0.0F);
          } else {
            ViewGroup viewGroup = this.mContentContainer;
            PopupWindow popupWindow = this.mPopupWindow;
            int i = popupWindow.getWidth();
            float f = (i - size.getWidth() - this.mMarginHorizontal);
            viewGroup.setX(f);
            this.mMainPanel.setX(0.0F);
            ImageButton imageButton = this.mOverflowButton;
            f = (size.getWidth() - this.mOverflowButtonSize.getWidth());
            imageButton.setX(f);
            OverflowPanel overflowPanel = this.mOverflowPanel;
            f = (size.getWidth() - this.mOverflowPanelSize.getWidth());
            overflowPanel.setX(f);
          } 
          if (this.mOpenOverflowUpwards) {
            ViewGroup viewGroup = this.mContentContainer;
            int i = this.mMarginVertical;
            Size size1 = this.mOverflowPanelSize;
            float f = (i + size1.getHeight() - size.getHeight());
            viewGroup.setY(f);
            this.mMainPanel.setY(0.0F);
            this.mOverflowButton.setY(0.0F);
            OverflowPanel overflowPanel = this.mOverflowPanel;
            f = (size.getHeight() - this.mOverflowPanelSize.getHeight());
            overflowPanel.setY(f);
          } else {
            this.mContentContainer.setY(this.mMarginVertical);
            this.mMainPanel.setY(0.0F);
            this.mOverflowButton.setY(0.0F);
            this.mOverflowPanel.setY(this.mOverflowButtonSize.getHeight());
          } 
        } else {
          this.mContentContainer.setX(this.mMarginHorizontal);
          this.mContentContainer.setY(this.mMarginVertical);
          this.mMainPanel.setX(0.0F);
          this.mMainPanel.setY(0.0F);
        } 
      } 
    }
    
    private void updateOverflowHeight(int param1Int) {
      if (hasOverflow()) {
        param1Int = (param1Int - this.mOverflowButtonSize.getHeight()) / this.mLineHeight;
        param1Int = calculateOverflowHeight(param1Int);
        if (this.mOverflowPanelSize.getHeight() != param1Int)
          this.mOverflowPanelSize = new Size(this.mOverflowPanelSize.getWidth(), param1Int); 
        setSize((View)this.mOverflowPanel, this.mOverflowPanelSize);
        if (this.mIsOverflowOpen) {
          setSize((View)this.mContentContainer, this.mOverflowPanelSize);
          if (this.mOpenOverflowUpwards) {
            param1Int = this.mOverflowPanelSize.getHeight() - param1Int;
            ViewGroup viewGroup = this.mContentContainer;
            viewGroup.setY(viewGroup.getY() + param1Int);
            ImageButton imageButton = this.mOverflowButton;
            imageButton.setY(imageButton.getY() - param1Int);
          } 
        } else {
          setSize((View)this.mContentContainer, this.mMainPanelSize);
        } 
        updatePopupSize();
      } 
    }
    
    private void updatePopupSize() {
      int i = 0;
      int j = 0;
      Size size = this.mMainPanelSize;
      if (size != null) {
        i = Math.max(0, size.getWidth());
        j = Math.max(0, this.mMainPanelSize.getHeight());
      } 
      size = this.mOverflowPanelSize;
      int k = i, m = j;
      if (size != null) {
        k = Math.max(i, size.getWidth());
        m = Math.max(j, this.mOverflowPanelSize.getHeight());
      } 
      this.mPopupWindow.setWidth(this.mMarginHorizontal * 2 + k);
      this.mPopupWindow.setHeight(this.mMarginVertical * 2 + m);
      maybeComputeTransitionDurationScale();
    }
    
    private void refreshViewPort() {
      this.mParent.getWindowVisibleDisplayFrame(this.mViewPortOnScreen);
    }
    
    private int getAdjustedToolbarWidth(int param1Int) {
      refreshViewPort();
      int i = this.mViewPortOnScreen.width();
      Resources resources = this.mParent.getResources();
      int j = resources.getDimensionPixelSize(17105187);
      int k = param1Int;
      if (param1Int <= 0) {
        resources = this.mParent.getResources();
        k = resources.getDimensionPixelSize(17105198);
      } 
      return Math.min(k, i - j * 2);
    }
    
    private void setZeroTouchableSurface() {
      this.mTouchableRegion.setEmpty();
    }
    
    private void setContentAreaAsTouchableSurface() {
      int i, j;
      Objects.requireNonNull(this.mMainPanelSize);
      if (this.mIsOverflowOpen) {
        Objects.requireNonNull(this.mOverflowPanelSize);
        i = this.mOverflowPanelSize.getWidth();
        j = this.mOverflowPanelSize.getHeight();
      } else {
        i = this.mMainPanelSize.getWidth();
        j = this.mMainPanelSize.getHeight();
      } 
      Region region = this.mTouchableRegion;
      ViewGroup viewGroup = this.mContentContainer;
      int k = (int)viewGroup.getX();
      viewGroup = this.mContentContainer;
      int m = (int)viewGroup.getY();
      viewGroup = this.mContentContainer;
      int n = (int)viewGroup.getX();
      viewGroup = this.mContentContainer;
      int i1 = (int)viewGroup.getY();
      region.set(k, m, n + i, i1 + j);
    }
    
    private void setTouchableSurfaceInsetsComputer() {
      View view = this.mPopupWindow.getContentView();
      view = view.getRootView();
      ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
      viewTreeObserver.removeOnComputeInternalInsetsListener(this.mInsetsComputer);
      viewTreeObserver.addOnComputeInternalInsetsListener(this.mInsetsComputer);
    }
    
    private boolean isInRTLMode() {
      boolean bool = this.mContext.getApplicationInfo().hasRtlSupport();
      null = true;
      if (bool) {
        Context context = this.mContext;
        if (context.getResources().getConfiguration().getLayoutDirection() == 1)
          return null; 
      } 
      return false;
    }
    
    private boolean hasOverflow() {
      boolean bool;
      if (this.mOverflowPanelSize != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public List<MenuItem> layoutMainPanelItems(List<MenuItem> param1List, int param1Int) {
      Objects.requireNonNull(param1List);
      int i = param1Int;
      LinkedList<MenuItem> linkedList1 = new LinkedList();
      LinkedList<MenuItem> linkedList2 = new LinkedList();
      for (MenuItem menuItem : param1List) {
        if (menuItem.getItemId() != 16908353 && 
          menuItem.requiresOverflow()) {
          linkedList2.add(menuItem);
          continue;
        } 
        linkedList1.add(menuItem);
      } 
      linkedList1.addAll(linkedList2);
      this.mMainPanel.removeAllViews();
      this.mMainPanel.setPaddingRelative(0, 0, 0, 0);
      int j = 1;
      while (!linkedList1.isEmpty()) {
        boolean bool;
        MenuItem menuItem = linkedList1.peek();
        if (!j && menuItem.requiresOverflow())
          break; 
        if (j && menuItem.getItemId() == 16908353) {
          bool = true;
        } else {
          bool = false;
        } 
        View view = FloatingToolbar.createMenuItemButton(this.mContext, menuItem, this.mIconTextSpacing, bool);
        if (!bool && view instanceof LinearLayout)
          ((LinearLayout)view).setGravity(17); 
        if (j) {
          IOplusFtHooks iOplusFtHooks = IOplusFtHooks.DEFAULT;
          int n = ((IOplusFtHooks)OplusFeatureCache.getOrCreate((IOplusCommonFeature)iOplusFtHooks, new Object[0])).getFirstItemPaddingStart(view.getContext(), view.getPaddingStart());
          int i1 = view.getPaddingTop();
          int i2 = view.getPaddingEnd();
          j = view.getPaddingBottom();
          view.setPaddingRelative(n, i1, i2, j);
        } 
        if (linkedList1.size() == 1) {
          j = 1;
        } else {
          j = 0;
        } 
        if (j != 0) {
          int i2 = view.getPaddingStart();
          int n = view.getPaddingTop();
          IOplusFtHooks iOplusFtHooks = IOplusFtHooks.DEFAULT;
          int i3 = ((IOplusFtHooks)OplusFeatureCache.getOrCreate((IOplusCommonFeature)iOplusFtHooks, new Object[0])).getLastItemPaddingEnd(view.getContext(), view.getPaddingEnd());
          int i1 = view.getPaddingBottom();
          view.setPaddingRelative(i2, n, i3, i1);
        } 
        view.measure(0, 0);
        int m = view.getMeasuredWidth();
        int k = Math.min(m, param1Int);
        Size size = this.mOverflowButtonSize;
        if (k <= i - size.getWidth()) {
          m = 1;
        } else {
          m = 0;
        } 
        if (j != 0 && k <= i) {
          j = 1;
        } else {
          j = 0;
        } 
        if (m != 0 || j != 0) {
          setButtonTagAndClickListener(view, menuItem);
          view.setTooltipText(menuItem.getTooltipText());
          this.mMainPanel.addView(view);
          ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
          layoutParams.width = k;
          view.setLayoutParams(layoutParams);
          i -= k;
          linkedList1.pop();
          menuItem.getGroupId();
          j = 0;
        } 
      } 
      if (!linkedList1.isEmpty())
        this.mMainPanel.setPaddingRelative(0, 0, this.mOverflowButtonSize.getWidth(), 0); 
      this.mMainPanelSize = measure((View)this.mMainPanel);
      return linkedList1;
    }
    
    private void layoutOverflowPanelItems(List<MenuItem> param1List) {
      OverflowPanel overflowPanel = this.mOverflowPanel;
      ArrayAdapter arrayAdapter = (ArrayAdapter)overflowPanel.getAdapter();
      arrayAdapter.clear();
      int i = param1List.size();
      int j;
      for (j = 0; j < i; j++)
        arrayAdapter.add(param1List.get(j)); 
      this.mOverflowPanel.setAdapter((ListAdapter)arrayAdapter);
      ((IOplusFtHooks)ColorFrameworkFactory.getInstance().getFeature((IOplusCommonFeature)IOplusFtHooks.DEFAULT, new Object[0])).setOverflowMenuCount(arrayAdapter.getCount());
      if (this.mOpenOverflowUpwards) {
        this.mOverflowPanel.setY(0.0F);
      } else {
        this.mOverflowPanel.setY(this.mOverflowButtonSize.getHeight());
      } 
      j = Math.max(getOverflowWidth(), this.mOverflowButtonSize.getWidth());
      i = calculateOverflowHeight(MAX_OVERFLOW_SIZE);
      Size size = new Size(j, i);
      setSize((View)this.mOverflowPanel, size);
    }
    
    private void preparePopupContent() {
      this.mContentContainer.removeAllViews();
      if (hasOverflow())
        this.mContentContainer.addView((View)this.mOverflowPanel); 
      this.mContentContainer.addView((View)this.mMainPanel);
      if (hasOverflow())
        this.mContentContainer.addView((View)this.mOverflowButton); 
      setPanelsStatesAtRestingPosition();
      setContentAreaAsTouchableSurface();
      if (isInRTLMode()) {
        this.mContentContainer.setAlpha(0.0F);
        this.mContentContainer.post(this.mPreparePopupContentRTLHelper);
      } 
    }
    
    private void clearPanels() {
      this.mOverflowPanelSize = null;
      this.mMainPanelSize = null;
      this.mIsOverflowOpen = false;
      this.mMainPanel.removeAllViews();
      OverflowPanel overflowPanel = this.mOverflowPanel;
      ArrayAdapter arrayAdapter = (ArrayAdapter)overflowPanel.getAdapter();
      arrayAdapter.clear();
      this.mOverflowPanel.setAdapter((ListAdapter)arrayAdapter);
      ((IOplusFtHooks)ColorFrameworkFactory.getInstance().getFeature((IOplusCommonFeature)IOplusFtHooks.DEFAULT, new Object[0])).setOverflowMenuCount(arrayAdapter.getCount());
      this.mContentContainer.removeAllViews();
    }
    
    private void positionContentYCoordinatesIfOpeningOverflowUpwards() {
      if (this.mOpenOverflowUpwards) {
        this.mMainPanel.setY((this.mContentContainer.getHeight() - this.mMainPanelSize.getHeight()));
        this.mOverflowButton.setY((this.mContentContainer.getHeight() - this.mOverflowButton.getHeight()));
        this.mOverflowPanel.setY((this.mContentContainer.getHeight() - this.mOverflowPanelSize.getHeight()));
      } 
    }
    
    private int getOverflowWidth() {
      int i = 0;
      int j = this.mOverflowPanel.getAdapter().getCount();
      for (byte b = 0; b < j; b++) {
        MenuItem menuItem = (MenuItem)this.mOverflowPanel.getAdapter().getItem(b);
        OverflowPanelViewHelper overflowPanelViewHelper = this.mOverflowPanelViewHelper;
        i = Math.max(overflowPanelViewHelper.calculateWidth(menuItem), i);
      } 
      return i;
    }
    
    private int calculateOverflowHeight(int param1Int) {
      int i = MAX_OVERFLOW_SIZE, j = MIN_OVERFLOW_SIZE;
      j = Math.max(j, param1Int);
      OverflowPanel overflowPanel = this.mOverflowPanel;
      param1Int = overflowPanel.getCount();
      param1Int = Math.min(j, param1Int);
      i = Math.min(i, param1Int);
      param1Int = 0;
      if (i < this.mOverflowPanel.getCount())
        param1Int = ((IOplusFtHooks)ColorFrameworkFactory.getInstance().getFeature((IOplusCommonFeature)IOplusFtHooks.DEFAULT, new Object[0])).calOverflowExtension(this.mLineHeight); 
      int k = this.mLineHeight;
      Size size = this.mOverflowButtonSize;
      j = size.getHeight();
      return k * i + j + param1Int;
    }
    
    private void setButtonTagAndClickListener(View param1View, MenuItem param1MenuItem) {
      param1View.setTag(param1MenuItem);
      param1View.setOnClickListener(this.mMenuItemButtonOnClickListener);
    }
    
    private int getAdjustedDuration(int param1Int) {
      int i = this.mTransitionDurationScale;
      if (i < 150)
        return Math.max(param1Int - 50, 0); 
      if (i > 300)
        return param1Int + 50; 
      return (int)(param1Int * ValueAnimator.getDurationScale());
    }
    
    private void maybeComputeTransitionDurationScale() {
      Size size = this.mMainPanelSize;
      if (size != null && this.mOverflowPanelSize != null) {
        int i = size.getWidth() - this.mOverflowPanelSize.getWidth();
        int j = this.mOverflowPanelSize.getHeight() - this.mMainPanelSize.getHeight();
        double d = Math.sqrt((i * i + j * j));
        ViewGroup viewGroup = this.mContentContainer;
        this.mTransitionDurationScale = (int)(d / (viewGroup.getContext().getResources().getDisplayMetrics()).density);
      } 
    }
    
    private ViewGroup createMainPanel() {
      return (ViewGroup)new Object(this, this.mContext);
    }
    
    private ImageButton createOverflowButton() {
      LayoutInflater layoutInflater = LayoutInflater.from(this.mContext);
      ImageButton imageButton = (ImageButton)layoutInflater.inflate(((IOplusFtHooks)ColorFrameworkFactory.getInstance().getFeature((IOplusCommonFeature)IOplusFtHooks.DEFAULT, new Object[0])).getOverflowButtonRes(), null);
      imageButton.setImageDrawable(this.mOverflow);
      imageButton.setOnClickListener(new _$$Lambda$FloatingToolbar$FloatingToolbarPopup$_uEfRwR__1oHxMvRVdmbNRdukDM(this, imageButton));
      return imageButton;
    }
    
    private OverflowPanel createOverflowPanel() {
      OverflowPanel overflowPanel = new OverflowPanel(this);
      overflowPanel.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
      overflowPanel.setDivider(null);
      overflowPanel.setDividerHeight(0);
      ((IOplusFtHooks)ColorFrameworkFactory.getInstance().getFeature((IOplusCommonFeature)IOplusFtHooks.DEFAULT, new Object[0])).setOverflowScrollBarSize(overflowPanel);
      Object object = new Object(this, this.mContext, 0);
      overflowPanel.setAdapter((ListAdapter)object);
      overflowPanel.setOnItemClickListener(new _$$Lambda$FloatingToolbar$FloatingToolbarPopup$E8FwnPCl7gZpcTlX_UaRPIBRnT0(this, overflowPanel));
      return overflowPanel;
    }
    
    private boolean isOverflowAnimating() {
      // Byte code:
      //   0: aload_0
      //   1: getfield mOpenOverflowAnimation : Landroid/view/animation/AnimationSet;
      //   4: invokevirtual hasStarted : ()Z
      //   7: istore_1
      //   8: iconst_1
      //   9: istore_2
      //   10: iload_1
      //   11: ifeq -> 32
      //   14: aload_0
      //   15: getfield mOpenOverflowAnimation : Landroid/view/animation/AnimationSet;
      //   18: astore_3
      //   19: aload_3
      //   20: invokevirtual hasEnded : ()Z
      //   23: ifne -> 32
      //   26: iconst_1
      //   27: istore #4
      //   29: goto -> 35
      //   32: iconst_0
      //   33: istore #4
      //   35: aload_0
      //   36: getfield mCloseOverflowAnimation : Landroid/view/animation/AnimationSet;
      //   39: invokevirtual hasStarted : ()Z
      //   42: ifeq -> 63
      //   45: aload_0
      //   46: getfield mCloseOverflowAnimation : Landroid/view/animation/AnimationSet;
      //   49: astore_3
      //   50: aload_3
      //   51: invokevirtual hasEnded : ()Z
      //   54: ifne -> 63
      //   57: iconst_1
      //   58: istore #5
      //   60: goto -> 66
      //   63: iconst_0
      //   64: istore #5
      //   66: iload_2
      //   67: istore_1
      //   68: iload #4
      //   70: ifne -> 85
      //   73: iload #5
      //   75: ifeq -> 83
      //   78: iload_2
      //   79: istore_1
      //   80: goto -> 85
      //   83: iconst_0
      //   84: istore_1
      //   85: iload_1
      //   86: ireturn
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1523	-> 0
      //   #1524	-> 19
      //   #1525	-> 35
      //   #1526	-> 50
      //   #1527	-> 66
    }
    
    private Animation.AnimationListener createOverflowAnimationListener() {
      return (Animation.AnimationListener)new Object(this);
    }
    
    private static Size measure(View param1View) {
      boolean bool;
      if (param1View.getParent() == null) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkState(bool);
      param1View.measure(0, 0);
      return new Size(param1View.getMeasuredWidth(), param1View.getMeasuredHeight());
    }
    
    private static void setSize(View param1View, int param1Int1, int param1Int2) {
      param1View.setMinimumWidth(param1Int1);
      param1View.setMinimumHeight(param1Int2);
      ViewGroup.LayoutParams layoutParams = param1View.getLayoutParams();
      if (layoutParams == null)
        layoutParams = new ViewGroup.LayoutParams(0, 0); 
      layoutParams.width = param1Int1;
      layoutParams.height = param1Int2;
      param1View.setLayoutParams(layoutParams);
    }
    
    private static void setSize(View param1View, Size param1Size) {
      setSize(param1View, param1Size.getWidth(), param1Size.getHeight());
    }
    
    private static void setWidth(View param1View, int param1Int) {
      ViewGroup.LayoutParams layoutParams = param1View.getLayoutParams();
      setSize(param1View, param1Int, layoutParams.height);
    }
    
    private static void setHeight(View param1View, int param1Int) {
      ViewGroup.LayoutParams layoutParams = param1View.getLayoutParams();
      setSize(param1View, layoutParams.width, param1Int);
    }
    
    class OverflowPanel extends ListView {
      private final FloatingToolbar.FloatingToolbarPopup mPopup;
      
      OverflowPanel(FloatingToolbar.FloatingToolbarPopup this$0) {
        super(this$0.mContext);
        this.mPopup = this$0;
        setScrollBarDefaultDelayBeforeFade(ViewConfiguration.getScrollDefaultDelay() * 3);
        ((IOplusFtHooks)ColorFrameworkFactory.getInstance().getFeature((IOplusCommonFeature)IOplusFtHooks.DEFAULT, new Object[0])).setScrollIndicators(this);
      }
      
      protected void onMeasure(int param2Int1, int param2Int2) {
        int i = this.mPopup.mOverflowPanelSize.getHeight();
        FloatingToolbar.FloatingToolbarPopup floatingToolbarPopup = this.mPopup;
        param2Int2 = floatingToolbarPopup.mOverflowButtonSize.getHeight();
        param2Int2 = View.MeasureSpec.makeMeasureSpec(i - param2Int2, 1073741824);
        super.onMeasure(param2Int1, param2Int2);
      }
      
      public boolean dispatchTouchEvent(MotionEvent param2MotionEvent) {
        if (this.mPopup.isOverflowAnimating())
          return true; 
        return super.dispatchTouchEvent(param2MotionEvent);
      }
      
      protected boolean awakenScrollBars() {
        return super.awakenScrollBars();
      }
    }
    
    class LogAccelerateInterpolator implements Interpolator {
      private static final int BASE = 100;
      
      private LogAccelerateInterpolator() {}
      
      private static final float LOGS_SCALE = 1.0F / computeLog(1.0F, 100);
      
      private static float computeLog(float param2Float, int param2Int) {
        return (float)(1.0D - Math.pow(param2Int, -param2Float));
      }
      
      public float getInterpolation(float param2Float) {
        return 1.0F - computeLog(1.0F - param2Float, 100) * LOGS_SCALE;
      }
    }
    
    private static final class OverflowPanelViewHelper {
      private final View mCalculator;
      
      private final Context mContext;
      
      private final int mIconTextSpacing;
      
      private boolean mOpenOverflowUpwards;
      
      private final int mSidePadding;
      
      public OverflowPanelViewHelper(Context param2Context, int param2Int) {
        Objects.requireNonNull(param2Context);
        this.mContext = param2Context;
        this.mIconTextSpacing = param2Int;
        Resources resources = param2Context.getResources();
        this.mSidePadding = resources.getDimensionPixelSize(17105197);
        this.mCalculator = createMenuButton(null);
      }
      
      public void setOverflowDirection(boolean param2Boolean) {
        this.mOpenOverflowUpwards = param2Boolean;
      }
      
      public View getView(MenuItem param2MenuItem, int param2Int, View param2View) {
        Objects.requireNonNull(param2MenuItem);
        if (param2View != null) {
          int i = this.mIconTextSpacing;
          boolean bool = shouldShowIcon(param2MenuItem);
          FloatingToolbar.updateMenuItemButton(param2View, param2MenuItem, i, bool);
        } else {
          param2View = createMenuButton(param2MenuItem);
        } 
        param2View.setMinimumWidth(param2Int);
        ((IOplusFtHooks)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusFtHooks.DEFAULT, new Object[0])).setConvertViewPadding(param2View, this.mOpenOverflowUpwards, this.mSidePadding, param2Int);
        return param2View;
      }
      
      public int calculateWidth(MenuItem param2MenuItem) {
        View view = this.mCalculator;
        int i = this.mIconTextSpacing;
        boolean bool = shouldShowIcon(param2MenuItem);
        FloatingToolbar.updateMenuItemButton(view, param2MenuItem, i, bool);
        this.mCalculator.measure(0, 0);
        return this.mCalculator.getMeasuredWidth();
      }
      
      private View createMenuButton(MenuItem param2MenuItem) {
        Context context = this.mContext;
        int i = this.mIconTextSpacing;
        boolean bool = shouldShowIcon(param2MenuItem);
        View view = FloatingToolbar.createMenuItemButton(context, param2MenuItem, i, bool);
        i = this.mSidePadding;
        view.setPadding(i, 0, i, 0);
        return view;
      }
      
      private boolean shouldShowIcon(MenuItem param2MenuItem) {
        boolean bool = false;
        if (param2MenuItem != null) {
          if (param2MenuItem.getGroupId() == 16908353)
            bool = true; 
          return bool;
        } 
        return false;
      }
    }
  }
  
  class null implements Runnable {
    final FloatingToolbar.FloatingToolbarPopup this$0;
    
    public void run() {
      this.this$0.setPanelsStatesAtRestingPosition();
      this.this$0.setContentAreaAsTouchableSurface();
      this.this$0.mContentContainer.setAlpha(1.0F);
    }
  }
  
  class OverflowPanel extends ListView {
    private final FloatingToolbar.FloatingToolbarPopup mPopup;
    
    OverflowPanel(FloatingToolbar this$0) {
      super(((FloatingToolbar.FloatingToolbarPopup)this$0).mContext);
      this.mPopup = (FloatingToolbar.FloatingToolbarPopup)this$0;
      setScrollBarDefaultDelayBeforeFade(ViewConfiguration.getScrollDefaultDelay() * 3);
      ((IOplusFtHooks)ColorFrameworkFactory.getInstance().getFeature((IOplusCommonFeature)IOplusFtHooks.DEFAULT, new Object[0])).setScrollIndicators(this);
    }
    
    protected void onMeasure(int param1Int1, int param1Int2) {
      int i = this.mPopup.mOverflowPanelSize.getHeight();
      FloatingToolbar.FloatingToolbarPopup floatingToolbarPopup = this.mPopup;
      param1Int2 = floatingToolbarPopup.mOverflowButtonSize.getHeight();
      param1Int2 = View.MeasureSpec.makeMeasureSpec(i - param1Int2, 1073741824);
      super.onMeasure(param1Int1, param1Int2);
    }
    
    public boolean dispatchTouchEvent(MotionEvent param1MotionEvent) {
      if (this.mPopup.isOverflowAnimating())
        return true; 
      return super.dispatchTouchEvent(param1MotionEvent);
    }
    
    protected boolean awakenScrollBars() {
      return super.awakenScrollBars();
    }
  }
  
  class LogAccelerateInterpolator implements Interpolator {
    private static final int BASE = 100;
    
    private LogAccelerateInterpolator() {}
    
    private static final float LOGS_SCALE = 1.0F / computeLog(1.0F, 100);
    
    private static float computeLog(float param1Float, int param1Int) {
      return (float)(1.0D - Math.pow(param1Int, -param1Float));
    }
    
    public float getInterpolation(float param1Float) {
      return 1.0F - computeLog(1.0F - param1Float, 100) * LOGS_SCALE;
    }
  }
  
  private static final class OverflowPanelViewHelper {
    private final View mCalculator;
    
    private final Context mContext;
    
    private final int mIconTextSpacing;
    
    private boolean mOpenOverflowUpwards;
    
    private final int mSidePadding;
    
    public OverflowPanelViewHelper(Context param1Context, int param1Int) {
      Objects.requireNonNull(param1Context);
      this.mContext = param1Context;
      this.mIconTextSpacing = param1Int;
      Resources resources = param1Context.getResources();
      this.mSidePadding = resources.getDimensionPixelSize(17105197);
      this.mCalculator = createMenuButton(null);
    }
    
    public void setOverflowDirection(boolean param1Boolean) {
      this.mOpenOverflowUpwards = param1Boolean;
    }
    
    public View getView(MenuItem param1MenuItem, int param1Int, View param1View) {
      Objects.requireNonNull(param1MenuItem);
      if (param1View != null) {
        int i = this.mIconTextSpacing;
        boolean bool = shouldShowIcon(param1MenuItem);
        FloatingToolbar.updateMenuItemButton(param1View, param1MenuItem, i, bool);
      } else {
        param1View = createMenuButton(param1MenuItem);
      } 
      param1View.setMinimumWidth(param1Int);
      ((IOplusFtHooks)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusFtHooks.DEFAULT, new Object[0])).setConvertViewPadding(param1View, this.mOpenOverflowUpwards, this.mSidePadding, param1Int);
      return param1View;
    }
    
    public int calculateWidth(MenuItem param1MenuItem) {
      View view = this.mCalculator;
      int i = this.mIconTextSpacing;
      boolean bool = shouldShowIcon(param1MenuItem);
      FloatingToolbar.updateMenuItemButton(view, param1MenuItem, i, bool);
      this.mCalculator.measure(0, 0);
      return this.mCalculator.getMeasuredWidth();
    }
    
    private View createMenuButton(MenuItem param1MenuItem) {
      Context context = this.mContext;
      int i = this.mIconTextSpacing;
      boolean bool = shouldShowIcon(param1MenuItem);
      View view = FloatingToolbar.createMenuItemButton(context, param1MenuItem, i, bool);
      i = this.mSidePadding;
      view.setPadding(i, 0, i, 0);
      return view;
    }
    
    private boolean shouldShowIcon(MenuItem param1MenuItem) {
      boolean bool = false;
      if (param1MenuItem != null) {
        if (param1MenuItem.getGroupId() == 16908353)
          bool = true; 
        return bool;
      } 
      return false;
    }
  }
  
  private static View createMenuItemButton(Context paramContext, MenuItem paramMenuItem, int paramInt, boolean paramBoolean) {
    LayoutInflater layoutInflater = LayoutInflater.from(paramContext);
    View view = layoutInflater.inflate(((IOplusFtHooks)ColorFrameworkFactory.getInstance().getFeature((IOplusCommonFeature)IOplusFtHooks.DEFAULT, new Object[0])).getMenuItemButtonRes(), null);
    if (paramMenuItem != null)
      updateMenuItemButton(view, paramMenuItem, paramInt, paramBoolean); 
    return view;
  }
  
  private static void updateMenuItemButton(View paramView, MenuItem paramMenuItem, int paramInt, boolean paramBoolean) {
    int i = ((IOplusFtHooks)ColorFrameworkFactory.getInstance().getFeature((IOplusCommonFeature)IOplusFtHooks.DEFAULT, new Object[0])).getButtonTextId();
    TextView textView = (TextView)paramView.findViewById(i);
    textView.setEllipsize(null);
    if (TextUtils.isEmpty(paramMenuItem.getTitle())) {
      textView.setVisibility(8);
    } else {
      textView.setVisibility(0);
      textView.setText(paramMenuItem.getTitle());
    } 
    i = ((IOplusFtHooks)ColorFrameworkFactory.getInstance().getFeature((IOplusCommonFeature)IOplusFtHooks.DEFAULT, new Object[0])).getButtonIconId();
    ImageView imageView = (ImageView)paramView.findViewById(i);
    if (paramMenuItem.getIcon() == null || !paramBoolean) {
      imageView.setVisibility(8);
      if (textView != null)
        textView.setPaddingRelative(0, 0, 0, 0); 
    } else {
      imageView.setVisibility(0);
      imageView.setImageDrawable(paramMenuItem.getIcon());
      if (textView != null)
        textView.setPaddingRelative(paramInt, 0, 0, 0); 
    } 
    CharSequence charSequence = paramMenuItem.getContentDescription();
    if (TextUtils.isEmpty(charSequence)) {
      paramView.setContentDescription(paramMenuItem.getTitle());
    } else {
      paramView.setContentDescription(charSequence);
    } 
  }
  
  private static ViewGroup createContentContainer(Context paramContext) {
    LayoutInflater layoutInflater = LayoutInflater.from(paramContext);
    ViewGroup viewGroup = (ViewGroup)layoutInflater.inflate(((IOplusFtHooks)ColorFrameworkFactory.getInstance().getFeature((IOplusCommonFeature)IOplusFtHooks.DEFAULT, new Object[0])).getContentContainerRes(), null);
    viewGroup.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
    viewGroup.setTag("floating_toolbar");
    viewGroup.setClipToOutline(true);
    return viewGroup;
  }
  
  private static PopupWindow createPopupWindow(ViewGroup paramViewGroup) {
    LinearLayout linearLayout = new LinearLayout(paramViewGroup.getContext());
    PopupWindow popupWindow = new PopupWindow((View)linearLayout);
    popupWindow.setClippingEnabled(false);
    popupWindow.setWindowLayoutType(1005);
    popupWindow.setAnimationStyle(0);
    popupWindow.setBackgroundDrawable((Drawable)new ColorDrawable(0));
    paramViewGroup.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
    linearLayout.addView((View)paramViewGroup);
    return popupWindow;
  }
  
  private static AnimatorSet createEnterAnimation(View paramView) {
    AnimatorSet animatorSet = new AnimatorSet();
    Property property = View.ALPHA;
    ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(paramView, property, new float[] { 0.0F, 1.0F }).setDuration(150L);
    animatorSet.playTogether(new Animator[] { (Animator)objectAnimator });
    return animatorSet;
  }
  
  private static AnimatorSet createExitAnimation(View paramView, int paramInt, Animator.AnimatorListener paramAnimatorListener) {
    AnimatorSet animatorSet = new AnimatorSet();
    Property property = View.ALPHA;
    ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(paramView, property, new float[] { 1.0F, 0.0F }).setDuration(100L);
    animatorSet.playTogether(new Animator[] { (Animator)objectAnimator });
    animatorSet.setStartDelay(paramInt);
    animatorSet.addListener(paramAnimatorListener);
    return animatorSet;
  }
  
  private static Context applyDefaultTheme(Context paramContext) {
    int i;
    TypedArray typedArray = paramContext.obtainStyledAttributes(new int[] { 16844176 });
    boolean bool = typedArray.getBoolean(0, true);
    if (bool) {
      i = 16974123;
    } else {
      i = 16974120;
    } 
    typedArray.recycle();
    return (Context)new ContextThemeWrapper(paramContext, i);
  }
}
