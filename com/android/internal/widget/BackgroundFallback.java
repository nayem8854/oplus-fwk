package com.android.internal.widget;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;

public class BackgroundFallback {
  private Drawable mBackgroundFallback;
  
  public void setDrawable(Drawable paramDrawable) {
    this.mBackgroundFallback = paramDrawable;
  }
  
  public Drawable getDrawable() {
    return this.mBackgroundFallback;
  }
  
  public boolean hasFallback() {
    boolean bool;
    if (this.mBackgroundFallback != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void draw(ViewGroup paramViewGroup1, ViewGroup paramViewGroup2, Canvas paramCanvas, View paramView1, View paramView2, View paramView3) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual hasFallback : ()Z
    //   4: ifne -> 8
    //   7: return
    //   8: aload_1
    //   9: invokevirtual getWidth : ()I
    //   12: istore #7
    //   14: aload_1
    //   15: invokevirtual getHeight : ()I
    //   18: istore #8
    //   20: aload_2
    //   21: invokevirtual getLeft : ()I
    //   24: istore #9
    //   26: aload_2
    //   27: invokevirtual getTop : ()I
    //   30: istore #10
    //   32: iload #7
    //   34: istore #11
    //   36: iload #8
    //   38: istore #12
    //   40: iconst_0
    //   41: istore #13
    //   43: iconst_0
    //   44: istore #14
    //   46: aload_2
    //   47: invokevirtual getChildCount : ()I
    //   50: istore #15
    //   52: iconst_0
    //   53: istore #16
    //   55: iload #16
    //   57: iload #15
    //   59: if_icmpge -> 255
    //   62: aload_2
    //   63: iload #16
    //   65: invokevirtual getChildAt : (I)Landroid/view/View;
    //   68: astore_1
    //   69: aload_1
    //   70: invokevirtual getBackground : ()Landroid/graphics/drawable/Drawable;
    //   73: astore #17
    //   75: aload_1
    //   76: aload #4
    //   78: if_acmpne -> 126
    //   81: aload #17
    //   83: ifnonnull -> 177
    //   86: aload_1
    //   87: instanceof android/view/ViewGroup
    //   90: ifeq -> 177
    //   93: aload_1
    //   94: checkcast android/view/ViewGroup
    //   97: astore #17
    //   99: aload #17
    //   101: invokevirtual getChildCount : ()I
    //   104: ifne -> 177
    //   107: iload #11
    //   109: istore #18
    //   111: iload #12
    //   113: istore #19
    //   115: iload #13
    //   117: istore #20
    //   119: iload #14
    //   121: istore #21
    //   123: goto -> 233
    //   126: iload #11
    //   128: istore #18
    //   130: iload #12
    //   132: istore #19
    //   134: iload #13
    //   136: istore #20
    //   138: iload #14
    //   140: istore #21
    //   142: aload_1
    //   143: invokevirtual getVisibility : ()I
    //   146: ifne -> 233
    //   149: aload_0
    //   150: aload #17
    //   152: invokespecial isOpaque : (Landroid/graphics/drawable/Drawable;)Z
    //   155: ifne -> 177
    //   158: iload #11
    //   160: istore #18
    //   162: iload #12
    //   164: istore #19
    //   166: iload #13
    //   168: istore #20
    //   170: iload #14
    //   172: istore #21
    //   174: goto -> 233
    //   177: iload #11
    //   179: aload_1
    //   180: invokevirtual getLeft : ()I
    //   183: iload #9
    //   185: iadd
    //   186: invokestatic min : (II)I
    //   189: istore #18
    //   191: iload #12
    //   193: aload_1
    //   194: invokevirtual getTop : ()I
    //   197: iload #10
    //   199: iadd
    //   200: invokestatic min : (II)I
    //   203: istore #19
    //   205: iload #13
    //   207: aload_1
    //   208: invokevirtual getRight : ()I
    //   211: iload #9
    //   213: iadd
    //   214: invokestatic max : (II)I
    //   217: istore #20
    //   219: iload #14
    //   221: aload_1
    //   222: invokevirtual getBottom : ()I
    //   225: iload #10
    //   227: iadd
    //   228: invokestatic max : (II)I
    //   231: istore #21
    //   233: iinc #16, 1
    //   236: iload #18
    //   238: istore #11
    //   240: iload #19
    //   242: istore #12
    //   244: iload #20
    //   246: istore #13
    //   248: iload #21
    //   250: istore #14
    //   252: goto -> 55
    //   255: iconst_1
    //   256: istore #20
    //   258: iconst_0
    //   259: istore #21
    //   261: iload #14
    //   263: istore #16
    //   265: iload #13
    //   267: istore #15
    //   269: iload #12
    //   271: istore #14
    //   273: iload #21
    //   275: iconst_2
    //   276: if_icmpge -> 633
    //   279: iload #21
    //   281: ifne -> 290
    //   284: aload #5
    //   286: astore_1
    //   287: goto -> 293
    //   290: aload #6
    //   292: astore_1
    //   293: aload_1
    //   294: ifnull -> 588
    //   297: aload_1
    //   298: invokevirtual getVisibility : ()I
    //   301: ifne -> 588
    //   304: aload_1
    //   305: invokevirtual getAlpha : ()F
    //   308: fconst_1
    //   309: fcmpl
    //   310: ifne -> 588
    //   313: aload_0
    //   314: aload_1
    //   315: invokevirtual getBackground : ()Landroid/graphics/drawable/Drawable;
    //   318: invokespecial isOpaque : (Landroid/graphics/drawable/Drawable;)Z
    //   321: ifne -> 327
    //   324: goto -> 588
    //   327: iload #11
    //   329: istore #13
    //   331: aload_1
    //   332: invokevirtual getTop : ()I
    //   335: ifgt -> 378
    //   338: iload #11
    //   340: istore #13
    //   342: aload_1
    //   343: invokevirtual getBottom : ()I
    //   346: iload #8
    //   348: if_icmplt -> 378
    //   351: iload #11
    //   353: istore #13
    //   355: aload_1
    //   356: invokevirtual getLeft : ()I
    //   359: ifgt -> 378
    //   362: iload #11
    //   364: istore #13
    //   366: aload_1
    //   367: invokevirtual getRight : ()I
    //   370: iload #11
    //   372: if_icmplt -> 378
    //   375: iconst_0
    //   376: istore #13
    //   378: iload #15
    //   380: istore #12
    //   382: aload_1
    //   383: invokevirtual getTop : ()I
    //   386: ifgt -> 432
    //   389: iload #15
    //   391: istore #12
    //   393: aload_1
    //   394: invokevirtual getBottom : ()I
    //   397: iload #8
    //   399: if_icmplt -> 432
    //   402: iload #15
    //   404: istore #12
    //   406: aload_1
    //   407: invokevirtual getLeft : ()I
    //   410: iload #15
    //   412: if_icmpgt -> 432
    //   415: iload #15
    //   417: istore #12
    //   419: aload_1
    //   420: invokevirtual getRight : ()I
    //   423: iload #7
    //   425: if_icmplt -> 432
    //   428: iload #7
    //   430: istore #12
    //   432: iload #14
    //   434: istore #11
    //   436: aload_1
    //   437: invokevirtual getTop : ()I
    //   440: ifgt -> 483
    //   443: iload #14
    //   445: istore #11
    //   447: aload_1
    //   448: invokevirtual getBottom : ()I
    //   451: iload #14
    //   453: if_icmplt -> 483
    //   456: iload #14
    //   458: istore #11
    //   460: aload_1
    //   461: invokevirtual getLeft : ()I
    //   464: ifgt -> 483
    //   467: iload #14
    //   469: istore #11
    //   471: aload_1
    //   472: invokevirtual getRight : ()I
    //   475: iload #7
    //   477: if_icmplt -> 483
    //   480: iconst_0
    //   481: istore #11
    //   483: iload #16
    //   485: istore #14
    //   487: aload_1
    //   488: invokevirtual getTop : ()I
    //   491: iload #16
    //   493: if_icmpgt -> 537
    //   496: iload #16
    //   498: istore #14
    //   500: aload_1
    //   501: invokevirtual getBottom : ()I
    //   504: iload #8
    //   506: if_icmplt -> 537
    //   509: iload #16
    //   511: istore #14
    //   513: aload_1
    //   514: invokevirtual getLeft : ()I
    //   517: ifgt -> 537
    //   520: iload #16
    //   522: istore #14
    //   524: aload_1
    //   525: invokevirtual getRight : ()I
    //   528: iload #7
    //   530: if_icmplt -> 537
    //   533: iload #8
    //   535: istore #14
    //   537: aload_1
    //   538: invokevirtual getTop : ()I
    //   541: ifgt -> 559
    //   544: aload_1
    //   545: invokevirtual getBottom : ()I
    //   548: iload #11
    //   550: if_icmplt -> 559
    //   553: iconst_1
    //   554: istore #16
    //   556: goto -> 562
    //   559: iconst_0
    //   560: istore #16
    //   562: iload #20
    //   564: iload #16
    //   566: iand
    //   567: istore #19
    //   569: iload #13
    //   571: istore #15
    //   573: iload #11
    //   575: istore #16
    //   577: iload #14
    //   579: istore #20
    //   581: iload #19
    //   583: istore #13
    //   585: goto -> 607
    //   588: iconst_0
    //   589: istore #13
    //   591: iload #16
    //   593: istore #20
    //   595: iload #15
    //   597: istore #12
    //   599: iload #14
    //   601: istore #16
    //   603: iload #11
    //   605: istore #15
    //   607: iinc #21, 1
    //   610: iload #15
    //   612: istore #11
    //   614: iload #16
    //   616: istore #14
    //   618: iload #12
    //   620: istore #15
    //   622: iload #20
    //   624: istore #16
    //   626: iload #13
    //   628: istore #20
    //   630: goto -> 273
    //   633: iload #14
    //   635: istore #13
    //   637: iload #20
    //   639: ifeq -> 675
    //   642: aload_0
    //   643: aload #5
    //   645: aload #6
    //   647: iload #7
    //   649: invokespecial viewsCoverEntireWidth : (Landroid/view/View;Landroid/view/View;I)Z
    //   652: ifne -> 672
    //   655: iload #14
    //   657: istore #13
    //   659: aload_0
    //   660: aload #6
    //   662: aload #5
    //   664: iload #7
    //   666: invokespecial viewsCoverEntireWidth : (Landroid/view/View;Landroid/view/View;I)Z
    //   669: ifeq -> 675
    //   672: iconst_0
    //   673: istore #13
    //   675: iload #11
    //   677: iload #15
    //   679: if_icmpge -> 809
    //   682: iload #13
    //   684: iload #16
    //   686: if_icmplt -> 692
    //   689: goto -> 809
    //   692: iload #13
    //   694: ifle -> 721
    //   697: aload_0
    //   698: getfield mBackgroundFallback : Landroid/graphics/drawable/Drawable;
    //   701: iconst_0
    //   702: iconst_0
    //   703: iload #7
    //   705: iload #13
    //   707: invokevirtual setBounds : (IIII)V
    //   710: aload_0
    //   711: getfield mBackgroundFallback : Landroid/graphics/drawable/Drawable;
    //   714: aload_3
    //   715: invokevirtual draw : (Landroid/graphics/Canvas;)V
    //   718: goto -> 721
    //   721: iload #11
    //   723: ifle -> 748
    //   726: aload_0
    //   727: getfield mBackgroundFallback : Landroid/graphics/drawable/Drawable;
    //   730: iconst_0
    //   731: iload #13
    //   733: iload #11
    //   735: iload #8
    //   737: invokevirtual setBounds : (IIII)V
    //   740: aload_0
    //   741: getfield mBackgroundFallback : Landroid/graphics/drawable/Drawable;
    //   744: aload_3
    //   745: invokevirtual draw : (Landroid/graphics/Canvas;)V
    //   748: iload #15
    //   750: iload #7
    //   752: if_icmpge -> 778
    //   755: aload_0
    //   756: getfield mBackgroundFallback : Landroid/graphics/drawable/Drawable;
    //   759: iload #15
    //   761: iload #13
    //   763: iload #7
    //   765: iload #8
    //   767: invokevirtual setBounds : (IIII)V
    //   770: aload_0
    //   771: getfield mBackgroundFallback : Landroid/graphics/drawable/Drawable;
    //   774: aload_3
    //   775: invokevirtual draw : (Landroid/graphics/Canvas;)V
    //   778: iload #16
    //   780: iload #8
    //   782: if_icmpge -> 808
    //   785: aload_0
    //   786: getfield mBackgroundFallback : Landroid/graphics/drawable/Drawable;
    //   789: iload #11
    //   791: iload #16
    //   793: iload #15
    //   795: iload #8
    //   797: invokevirtual setBounds : (IIII)V
    //   800: aload_0
    //   801: getfield mBackgroundFallback : Landroid/graphics/drawable/Drawable;
    //   804: aload_3
    //   805: invokevirtual draw : (Landroid/graphics/Canvas;)V
    //   808: return
    //   809: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #59	-> 0
    //   #60	-> 7
    //   #64	-> 8
    //   #65	-> 14
    //   #67	-> 20
    //   #68	-> 26
    //   #70	-> 32
    //   #71	-> 36
    //   #72	-> 40
    //   #73	-> 43
    //   #75	-> 46
    //   #76	-> 52
    //   #77	-> 62
    //   #78	-> 69
    //   #79	-> 75
    //   #82	-> 81
    //   #83	-> 99
    //   #84	-> 107
    //   #86	-> 126
    //   #90	-> 158
    //   #92	-> 177
    //   #93	-> 191
    //   #94	-> 205
    //   #95	-> 219
    //   #76	-> 233
    //   #100	-> 255
    //   #101	-> 258
    //   #102	-> 279
    //   #103	-> 293
    //   #104	-> 304
    //   #110	-> 327
    //   #111	-> 351
    //   #112	-> 375
    //   #115	-> 378
    //   #116	-> 402
    //   #117	-> 428
    //   #120	-> 432
    //   #121	-> 456
    //   #122	-> 480
    //   #125	-> 483
    //   #126	-> 509
    //   #127	-> 533
    //   #130	-> 537
    //   #105	-> 588
    //   #106	-> 591
    //   #101	-> 607
    //   #135	-> 633
    //   #136	-> 655
    //   #137	-> 672
    //   #140	-> 675
    //   #145	-> 692
    //   #146	-> 697
    //   #147	-> 710
    //   #145	-> 721
    //   #149	-> 721
    //   #150	-> 726
    //   #151	-> 740
    //   #153	-> 748
    //   #154	-> 755
    //   #155	-> 770
    //   #157	-> 778
    //   #158	-> 785
    //   #159	-> 800
    //   #161	-> 808
    //   #142	-> 809
  }
  
  private boolean isOpaque(Drawable paramDrawable) {
    boolean bool;
    if (paramDrawable != null && paramDrawable.getOpacity() == -1) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean viewsCoverEntireWidth(View paramView1, View paramView2, int paramInt) {
    boolean bool;
    if (paramView1.getLeft() <= 0 && 
      paramView1.getRight() >= paramView2.getLeft() && 
      paramView2.getRight() >= paramInt) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
}
