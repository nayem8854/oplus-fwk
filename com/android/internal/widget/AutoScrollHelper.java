package com.android.internal.widget;

import android.content.res.Resources;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.AbsListView;

public abstract class AutoScrollHelper implements View.OnTouchListener {
  private final ClampedScroller mScroller = new ClampedScroller();
  
  private final Interpolator mEdgeInterpolator = (Interpolator)new AccelerateInterpolator();
  
  private float[] mRelativeEdges = new float[] { 0.0F, 0.0F };
  
  private float[] mMaximumEdges = new float[] { Float.MAX_VALUE, Float.MAX_VALUE };
  
  private float[] mRelativeVelocity = new float[] { 0.0F, 0.0F };
  
  private float[] mMinimumVelocity = new float[] { 0.0F, 0.0F };
  
  private float[] mMaximumVelocity = new float[] { Float.MAX_VALUE, Float.MAX_VALUE };
  
  private static final int DEFAULT_ACTIVATION_DELAY = ViewConfiguration.getTapTimeout();
  
  private static final int DEFAULT_EDGE_TYPE = 1;
  
  private static final float DEFAULT_MAXIMUM_EDGE = 3.4028235E38F;
  
  private static final int DEFAULT_MAXIMUM_VELOCITY_DIPS = 1575;
  
  private static final int DEFAULT_MINIMUM_VELOCITY_DIPS = 315;
  
  private static final int DEFAULT_RAMP_DOWN_DURATION = 500;
  
  private static final int DEFAULT_RAMP_UP_DURATION = 500;
  
  private static final float DEFAULT_RELATIVE_EDGE = 0.2F;
  
  private static final float DEFAULT_RELATIVE_VELOCITY = 1.0F;
  
  public static final int EDGE_TYPE_INSIDE = 0;
  
  public static final int EDGE_TYPE_INSIDE_EXTEND = 1;
  
  public static final int EDGE_TYPE_OUTSIDE = 2;
  
  private static final int HORIZONTAL = 0;
  
  public static final float NO_MAX = 3.4028235E38F;
  
  public static final float NO_MIN = 0.0F;
  
  public static final float RELATIVE_UNSPECIFIED = 0.0F;
  
  private static final int VERTICAL = 1;
  
  private int mActivationDelay;
  
  private boolean mAlreadyDelayed;
  
  private boolean mAnimating;
  
  private int mEdgeType;
  
  private boolean mEnabled;
  
  private boolean mExclusive;
  
  private boolean mNeedsCancel;
  
  private boolean mNeedsReset;
  
  private Runnable mRunnable;
  
  private final View mTarget;
  
  public AutoScrollHelper(View paramView) {
    this.mTarget = paramView;
    DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
    int i = (int)(displayMetrics.density * 1575.0F + 0.5F);
    int j = (int)(displayMetrics.density * 315.0F + 0.5F);
    setMaximumVelocity(i, i);
    setMinimumVelocity(j, j);
    setEdgeType(1);
    setMaximumEdges(Float.MAX_VALUE, Float.MAX_VALUE);
    setRelativeEdges(0.2F, 0.2F);
    setRelativeVelocity(1.0F, 1.0F);
    setActivationDelay(DEFAULT_ACTIVATION_DELAY);
    setRampUpDuration(500);
    setRampDownDuration(500);
  }
  
  public AutoScrollHelper setEnabled(boolean paramBoolean) {
    if (this.mEnabled && !paramBoolean)
      requestStop(); 
    this.mEnabled = paramBoolean;
    return this;
  }
  
  public boolean isEnabled() {
    return this.mEnabled;
  }
  
  public AutoScrollHelper setExclusive(boolean paramBoolean) {
    this.mExclusive = paramBoolean;
    return this;
  }
  
  public boolean isExclusive() {
    return this.mExclusive;
  }
  
  public AutoScrollHelper setMaximumVelocity(float paramFloat1, float paramFloat2) {
    float[] arrayOfFloat = this.mMaximumVelocity;
    arrayOfFloat[0] = paramFloat1 / 1000.0F;
    arrayOfFloat[1] = paramFloat2 / 1000.0F;
    return this;
  }
  
  public AutoScrollHelper setMinimumVelocity(float paramFloat1, float paramFloat2) {
    float[] arrayOfFloat = this.mMinimumVelocity;
    arrayOfFloat[0] = paramFloat1 / 1000.0F;
    arrayOfFloat[1] = paramFloat2 / 1000.0F;
    return this;
  }
  
  public AutoScrollHelper setRelativeVelocity(float paramFloat1, float paramFloat2) {
    float[] arrayOfFloat = this.mRelativeVelocity;
    arrayOfFloat[0] = paramFloat1 / 1000.0F;
    arrayOfFloat[1] = paramFloat2 / 1000.0F;
    return this;
  }
  
  public AutoScrollHelper setEdgeType(int paramInt) {
    this.mEdgeType = paramInt;
    return this;
  }
  
  public AutoScrollHelper setRelativeEdges(float paramFloat1, float paramFloat2) {
    float[] arrayOfFloat = this.mRelativeEdges;
    arrayOfFloat[0] = paramFloat1;
    arrayOfFloat[1] = paramFloat2;
    return this;
  }
  
  public AutoScrollHelper setMaximumEdges(float paramFloat1, float paramFloat2) {
    float[] arrayOfFloat = this.mMaximumEdges;
    arrayOfFloat[0] = paramFloat1;
    arrayOfFloat[1] = paramFloat2;
    return this;
  }
  
  public AutoScrollHelper setActivationDelay(int paramInt) {
    this.mActivationDelay = paramInt;
    return this;
  }
  
  public AutoScrollHelper setRampUpDuration(int paramInt) {
    this.mScroller.setRampUpDuration(paramInt);
    return this;
  }
  
  public AutoScrollHelper setRampDownDuration(int paramInt) {
    this.mScroller.setRampDownDuration(paramInt);
    return this;
  }
  
  public boolean onTouch(View paramView, MotionEvent paramMotionEvent) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mEnabled : Z
    //   4: istore_3
    //   5: iconst_0
    //   6: istore #4
    //   8: iload_3
    //   9: ifne -> 14
    //   12: iconst_0
    //   13: ireturn
    //   14: aload_2
    //   15: invokevirtual getActionMasked : ()I
    //   18: istore #5
    //   20: iload #5
    //   22: ifeq -> 53
    //   25: iload #5
    //   27: iconst_1
    //   28: if_icmpeq -> 46
    //   31: iload #5
    //   33: iconst_2
    //   34: if_icmpeq -> 63
    //   37: iload #5
    //   39: iconst_3
    //   40: if_icmpeq -> 46
    //   43: goto -> 164
    //   46: aload_0
    //   47: invokespecial requestStop : ()V
    //   50: goto -> 164
    //   53: aload_0
    //   54: iconst_1
    //   55: putfield mNeedsCancel : Z
    //   58: aload_0
    //   59: iconst_0
    //   60: putfield mAlreadyDelayed : Z
    //   63: aload_2
    //   64: invokevirtual getX : ()F
    //   67: fstore #6
    //   69: aload_1
    //   70: invokevirtual getWidth : ()I
    //   73: i2f
    //   74: fstore #7
    //   76: aload_0
    //   77: getfield mTarget : Landroid/view/View;
    //   80: invokevirtual getWidth : ()I
    //   83: i2f
    //   84: fstore #8
    //   86: aload_0
    //   87: iconst_0
    //   88: fload #6
    //   90: fload #7
    //   92: fload #8
    //   94: invokespecial computeTargetVelocity : (IFFF)F
    //   97: fstore #7
    //   99: aload_2
    //   100: invokevirtual getY : ()F
    //   103: fstore #9
    //   105: aload_1
    //   106: invokevirtual getHeight : ()I
    //   109: i2f
    //   110: fstore #8
    //   112: aload_0
    //   113: getfield mTarget : Landroid/view/View;
    //   116: invokevirtual getHeight : ()I
    //   119: i2f
    //   120: fstore #6
    //   122: aload_0
    //   123: iconst_1
    //   124: fload #9
    //   126: fload #8
    //   128: fload #6
    //   130: invokespecial computeTargetVelocity : (IFFF)F
    //   133: fstore #8
    //   135: aload_0
    //   136: getfield mScroller : Lcom/android/internal/widget/AutoScrollHelper$ClampedScroller;
    //   139: fload #7
    //   141: fload #8
    //   143: invokevirtual setTargetVelocity : (FF)V
    //   146: aload_0
    //   147: getfield mAnimating : Z
    //   150: ifne -> 164
    //   153: aload_0
    //   154: invokespecial shouldAnimate : ()Z
    //   157: ifeq -> 164
    //   160: aload_0
    //   161: invokespecial startAnimating : ()V
    //   164: iload #4
    //   166: istore_3
    //   167: aload_0
    //   168: getfield mExclusive : Z
    //   171: ifeq -> 186
    //   174: iload #4
    //   176: istore_3
    //   177: aload_0
    //   178: getfield mAnimating : Z
    //   181: ifeq -> 186
    //   184: iconst_1
    //   185: istore_3
    //   186: iload_3
    //   187: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #455	-> 0
    //   #456	-> 12
    //   #459	-> 14
    //   #460	-> 20
    //   #480	-> 46
    //   #462	-> 53
    //   #463	-> 58
    //   #466	-> 63
    //   #467	-> 63
    //   #466	-> 86
    //   #468	-> 99
    //   #469	-> 99
    //   #468	-> 122
    //   #470	-> 135
    //   #474	-> 146
    //   #475	-> 160
    //   #484	-> 164
  }
  
  private boolean shouldAnimate() {
    boolean bool;
    ClampedScroller clampedScroller = this.mScroller;
    int i = clampedScroller.getVerticalDirection();
    int j = clampedScroller.getHorizontalDirection();
    if ((i != 0 && canTargetScrollVertically(i)) || (j != 0 && 
      canTargetScrollHorizontally(j))) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void startAnimating() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mRunnable : Ljava/lang/Runnable;
    //   4: ifnonnull -> 20
    //   7: aload_0
    //   8: new com/android/internal/widget/AutoScrollHelper$ScrollAnimationRunnable
    //   11: dup
    //   12: aload_0
    //   13: aconst_null
    //   14: invokespecial <init> : (Lcom/android/internal/widget/AutoScrollHelper;Lcom/android/internal/widget/AutoScrollHelper$1;)V
    //   17: putfield mRunnable : Ljava/lang/Runnable;
    //   20: aload_0
    //   21: iconst_1
    //   22: putfield mAnimating : Z
    //   25: aload_0
    //   26: iconst_1
    //   27: putfield mNeedsReset : Z
    //   30: aload_0
    //   31: getfield mAlreadyDelayed : Z
    //   34: ifne -> 62
    //   37: aload_0
    //   38: getfield mActivationDelay : I
    //   41: istore_1
    //   42: iload_1
    //   43: ifle -> 62
    //   46: aload_0
    //   47: getfield mTarget : Landroid/view/View;
    //   50: aload_0
    //   51: getfield mRunnable : Ljava/lang/Runnable;
    //   54: iload_1
    //   55: i2l
    //   56: invokevirtual postOnAnimationDelayed : (Ljava/lang/Runnable;J)V
    //   59: goto -> 71
    //   62: aload_0
    //   63: getfield mRunnable : Ljava/lang/Runnable;
    //   66: invokeinterface run : ()V
    //   71: aload_0
    //   72: iconst_1
    //   73: putfield mAlreadyDelayed : Z
    //   76: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #503	-> 0
    //   #504	-> 7
    //   #507	-> 20
    //   #508	-> 25
    //   #510	-> 30
    //   #511	-> 46
    //   #513	-> 62
    //   #518	-> 71
    //   #519	-> 76
  }
  
  private void requestStop() {
    if (this.mNeedsReset) {
      this.mAnimating = false;
    } else {
      this.mScroller.requestStop();
    } 
  }
  
  private float computeTargetVelocity(int paramInt, float paramFloat1, float paramFloat2, float paramFloat3) {
    float f1 = this.mRelativeEdges[paramInt];
    float f2 = this.mMaximumEdges[paramInt];
    paramFloat1 = getEdgeValue(f1, paramFloat2, f2, paramFloat1);
    if (paramFloat1 == 0.0F)
      return 0.0F; 
    f2 = this.mRelativeVelocity[paramInt];
    paramFloat2 = this.mMinimumVelocity[paramInt];
    f1 = this.mMaximumVelocity[paramInt];
    paramFloat3 = f2 * paramFloat3;
    if (paramFloat1 > 0.0F)
      return constrain(paramFloat1 * paramFloat3, paramFloat2, f1); 
    return -constrain(-paramFloat1 * paramFloat3, paramFloat2, f1);
  }
  
  private float getEdgeValue(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    paramFloat3 = constrain(paramFloat1 * paramFloat2, 0.0F, paramFloat3);
    paramFloat1 = constrainEdgeValue(paramFloat4, paramFloat3);
    paramFloat2 = constrainEdgeValue(paramFloat2 - paramFloat4, paramFloat3);
    paramFloat1 = paramFloat2 - paramFloat1;
    if (paramFloat1 < 0.0F) {
      paramFloat1 = -this.mEdgeInterpolator.getInterpolation(-paramFloat1);
    } else {
      if (paramFloat1 > 0.0F) {
        paramFloat1 = this.mEdgeInterpolator.getInterpolation(paramFloat1);
        return constrain(paramFloat1, -1.0F, 1.0F);
      } 
      return 0.0F;
    } 
    return constrain(paramFloat1, -1.0F, 1.0F);
  }
  
  private float constrainEdgeValue(float paramFloat1, float paramFloat2) {
    if (paramFloat2 == 0.0F)
      return 0.0F; 
    int i = this.mEdgeType;
    if (i != 0 && i != 1) {
      if (i == 2)
        if (paramFloat1 < 0.0F)
          return paramFloat1 / -paramFloat2;  
    } else if (paramFloat1 < paramFloat2) {
      if (paramFloat1 >= 0.0F)
        return 1.0F - paramFloat1 / paramFloat2; 
      if (this.mAnimating && this.mEdgeType == 1)
        return 1.0F; 
    } 
    return 0.0F;
  }
  
  private static int constrain(int paramInt1, int paramInt2, int paramInt3) {
    if (paramInt1 > paramInt3)
      return paramInt3; 
    if (paramInt1 < paramInt2)
      return paramInt2; 
    return paramInt1;
  }
  
  private static float constrain(float paramFloat1, float paramFloat2, float paramFloat3) {
    if (paramFloat1 > paramFloat3)
      return paramFloat3; 
    if (paramFloat1 < paramFloat2)
      return paramFloat2; 
    return paramFloat1;
  }
  
  private void cancelTargetTouch() {
    long l = SystemClock.uptimeMillis();
    MotionEvent motionEvent = MotionEvent.obtain(l, l, 3, 0.0F, 0.0F, 0);
    this.mTarget.onTouchEvent(motionEvent);
    motionEvent.recycle();
  }
  
  public abstract boolean canTargetScrollHorizontally(int paramInt);
  
  public abstract boolean canTargetScrollVertically(int paramInt);
  
  public abstract void scrollTargetBy(int paramInt1, int paramInt2);
  
  class ScrollAnimationRunnable implements Runnable {
    final AutoScrollHelper this$0;
    
    private ScrollAnimationRunnable() {}
    
    public void run() {
      if (!AutoScrollHelper.this.mAnimating)
        return; 
      if (AutoScrollHelper.this.mNeedsReset) {
        AutoScrollHelper.access$202(AutoScrollHelper.this, false);
        AutoScrollHelper.this.mScroller.start();
      } 
      AutoScrollHelper.ClampedScroller clampedScroller = AutoScrollHelper.this.mScroller;
      if (clampedScroller.isFinished() || !AutoScrollHelper.this.shouldAnimate()) {
        AutoScrollHelper.access$102(AutoScrollHelper.this, false);
        return;
      } 
      if (AutoScrollHelper.this.mNeedsCancel) {
        AutoScrollHelper.access$502(AutoScrollHelper.this, false);
        AutoScrollHelper.this.cancelTargetTouch();
      } 
      clampedScroller.computeScrollDelta();
      int i = clampedScroller.getDeltaX();
      int j = clampedScroller.getDeltaY();
      AutoScrollHelper.this.scrollTargetBy(i, j);
      AutoScrollHelper.this.mTarget.postOnAnimation(this);
    }
  }
  
  class ClampedScroller {
    private long mStartTime = Long.MIN_VALUE;
    
    private long mStopTime = -1L;
    
    private long mDeltaTime = 0L;
    
    private int mDeltaX = 0;
    
    private int mDeltaY = 0;
    
    private int mEffectiveRampDown;
    
    private int mRampDownDuration;
    
    private int mRampUpDuration;
    
    private float mStopValue;
    
    private float mTargetVelocityX;
    
    private float mTargetVelocityY;
    
    public void setRampUpDuration(int param1Int) {
      this.mRampUpDuration = param1Int;
    }
    
    public void setRampDownDuration(int param1Int) {
      this.mRampDownDuration = param1Int;
    }
    
    public void start() {
      long l = AnimationUtils.currentAnimationTimeMillis();
      this.mStopTime = -1L;
      this.mDeltaTime = l;
      this.mStopValue = 0.5F;
      this.mDeltaX = 0;
      this.mDeltaY = 0;
    }
    
    public void requestStop() {
      long l = AnimationUtils.currentAnimationTimeMillis();
      this.mEffectiveRampDown = AutoScrollHelper.constrain((int)(l - this.mStartTime), 0, this.mRampDownDuration);
      this.mStopValue = getValueAt(l);
      this.mStopTime = l;
    }
    
    public boolean isFinished() {
      boolean bool;
      if (this.mStopTime > 0L && 
        AnimationUtils.currentAnimationTimeMillis() > this.mStopTime + this.mEffectiveRampDown) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    private float getValueAt(long param1Long) {
      if (param1Long < this.mStartTime)
        return 0.0F; 
      long l = this.mStopTime;
      if (l < 0L || param1Long < l) {
        l = this.mStartTime;
        return AutoScrollHelper.constrain((float)(param1Long - l) / this.mRampUpDuration, 0.0F, 1.0F) * 0.5F;
      } 
      float f1 = this.mStopValue, f2 = (float)(param1Long - l) / this.mEffectiveRampDown;
      f2 = AutoScrollHelper.constrain(f2, 0.0F, 1.0F);
      return 1.0F - f1 + f1 * f2;
    }
    
    private float interpolateValue(float param1Float) {
      return -4.0F * param1Float * param1Float + 4.0F * param1Float;
    }
    
    public void computeScrollDelta() {
      if (this.mDeltaTime != 0L) {
        long l1 = AnimationUtils.currentAnimationTimeMillis();
        float f = getValueAt(l1);
        f = interpolateValue(f);
        long l2 = l1 - this.mDeltaTime;
        this.mDeltaTime = l1;
        this.mDeltaX = (int)((float)l2 * f * this.mTargetVelocityX);
        this.mDeltaY = (int)((float)l2 * f * this.mTargetVelocityY);
        return;
      } 
      throw new RuntimeException("Cannot compute scroll delta before calling start()");
    }
    
    public void setTargetVelocity(float param1Float1, float param1Float2) {
      this.mTargetVelocityX = param1Float1;
      this.mTargetVelocityY = param1Float2;
    }
    
    public int getHorizontalDirection() {
      float f = this.mTargetVelocityX;
      return (int)(f / Math.abs(f));
    }
    
    public int getVerticalDirection() {
      float f = this.mTargetVelocityY;
      return (int)(f / Math.abs(f));
    }
    
    public int getDeltaX() {
      return this.mDeltaX;
    }
    
    public int getDeltaY() {
      return this.mDeltaY;
    }
  }
  
  class AbsListViewAutoScroller extends AutoScrollHelper {
    private final AbsListView mTarget;
    
    public AbsListViewAutoScroller(AutoScrollHelper this$0) {
      super((View)this$0);
      this.mTarget = (AbsListView)this$0;
    }
    
    public void scrollTargetBy(int param1Int1, int param1Int2) {
      this.mTarget.scrollListBy(param1Int2);
    }
    
    public boolean canTargetScrollHorizontally(int param1Int) {
      return false;
    }
    
    public boolean canTargetScrollVertically(int param1Int) {
      AbsListView absListView = this.mTarget;
      int i = absListView.getCount();
      if (i == 0)
        return false; 
      int j = absListView.getChildCount();
      int k = absListView.getFirstVisiblePosition();
      if (param1Int > 0) {
        if (k + j >= i) {
          View view = absListView.getChildAt(j - 1);
          if (view.getBottom() <= absListView.getHeight())
            return false; 
        } 
      } else {
        if (param1Int < 0) {
          if (k <= 0) {
            View view = absListView.getChildAt(0);
            if (view.getTop() >= 0)
              return false; 
          } 
          return true;
        } 
        return false;
      } 
      return true;
    }
  }
}
