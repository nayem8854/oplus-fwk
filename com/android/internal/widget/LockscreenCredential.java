package com.android.internal.widget;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.android.internal.util.Preconditions;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class LockscreenCredential implements Parcelable, AutoCloseable {
  private LockscreenCredential(int paramInt, byte[] paramArrayOfbyte) {
    Objects.requireNonNull(paramArrayOfbyte);
    boolean bool1 = false, bool2 = false;
    if (paramInt == -1) {
      if (paramArrayOfbyte.length == 0)
        bool2 = true; 
      Preconditions.checkArgument(bool2);
    } else {
      if (paramInt == 3 || paramInt == 4 || paramInt == 1) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      Preconditions.checkArgument(bool2);
      bool2 = bool1;
      if (paramArrayOfbyte.length > 0)
        bool2 = true; 
      Preconditions.checkArgument(bool2);
    } 
    this.mType = paramInt;
    this.mCredential = paramArrayOfbyte;
  }
  
  public static LockscreenCredential createNone() {
    return new LockscreenCredential(-1, new byte[0]);
  }
  
  public static LockscreenCredential createPattern(List<LockPatternView.Cell> paramList) {
    return 
      new LockscreenCredential(1, LockPatternUtils.patternToByteArray(paramList));
  }
  
  public static LockscreenCredential createPassword(CharSequence paramCharSequence) {
    return 
      new LockscreenCredential(4, charSequenceToByteArray(paramCharSequence));
  }
  
  public static LockscreenCredential createManagedPassword(byte[] paramArrayOfbyte) {
    int i = paramArrayOfbyte.length;
    return 
      new LockscreenCredential(4, Arrays.copyOf(paramArrayOfbyte, i));
  }
  
  public static LockscreenCredential createPin(CharSequence paramCharSequence) {
    return 
      new LockscreenCredential(3, charSequenceToByteArray(paramCharSequence));
  }
  
  public static LockscreenCredential createPasswordOrNone(CharSequence paramCharSequence) {
    if (TextUtils.isEmpty(paramCharSequence))
      return createNone(); 
    return createPassword(paramCharSequence);
  }
  
  public static LockscreenCredential createPinOrNone(CharSequence paramCharSequence) {
    if (TextUtils.isEmpty(paramCharSequence))
      return createNone(); 
    return createPin(paramCharSequence);
  }
  
  private void ensureNotZeroized() {
    boolean bool;
    if (this.mCredential != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkState(bool, "Credential is already zeroized");
  }
  
  public int getType() {
    ensureNotZeroized();
    return this.mType;
  }
  
  public byte[] getCredential() {
    ensureNotZeroized();
    return this.mCredential;
  }
  
  public int getStorageCryptType() {
    if (isNone())
      return 1; 
    if (isPattern())
      return 2; 
    if (isPin())
      return 3; 
    if (isPassword())
      return 0; 
    throw new IllegalStateException("Unhandled credential type");
  }
  
  public boolean isNone() {
    boolean bool;
    ensureNotZeroized();
    if (this.mType == -1) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isPattern() {
    ensureNotZeroized();
    int i = this.mType;
    boolean bool = true;
    if (i != 1)
      bool = false; 
    return bool;
  }
  
  public boolean isPin() {
    boolean bool;
    ensureNotZeroized();
    if (this.mType == 3) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isPassword() {
    boolean bool;
    ensureNotZeroized();
    if (this.mType == 4) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int size() {
    ensureNotZeroized();
    return this.mCredential.length;
  }
  
  public LockscreenCredential duplicate() {
    int i = this.mType;
    byte[] arrayOfByte = this.mCredential;
    if (arrayOfByte != null) {
      arrayOfByte = Arrays.copyOf(arrayOfByte, arrayOfByte.length);
    } else {
      arrayOfByte = null;
    } 
    return new LockscreenCredential(i, arrayOfByte);
  }
  
  public void zeroize() {
    byte[] arrayOfByte = this.mCredential;
    if (arrayOfByte != null) {
      Arrays.fill(arrayOfByte, (byte)0);
      this.mCredential = null;
    } 
  }
  
  public void checkLength() {
    if (isNone())
      return; 
    if (isPattern()) {
      if (size() >= 4)
        return; 
      throw new IllegalArgumentException("pattern must not be null and at least 4 dots long.");
    } 
    if (isPassword() || isPin()) {
      if (size() >= 4)
        return; 
      throw new IllegalArgumentException("password must not be null and at least of length 4");
    } 
  }
  
  public boolean checkAgainstStoredType(int paramInt) {
    boolean bool1 = true, bool2 = true;
    if (paramInt == 2) {
      bool1 = bool2;
      if (getType() != 4)
        if (getType() == 3) {
          bool1 = bool2;
        } else {
          bool1 = false;
        }  
      return bool1;
    } 
    if (getType() != paramInt)
      bool1 = false; 
    return bool1;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mType);
    paramParcel.writeByteArray(this.mCredential);
  }
  
  public static final Parcelable.Creator<LockscreenCredential> CREATOR = new Parcelable.Creator<LockscreenCredential>() {
      public LockscreenCredential createFromParcel(Parcel param1Parcel) {
        return new LockscreenCredential(param1Parcel.readInt(), param1Parcel.createByteArray());
      }
      
      public LockscreenCredential[] newArray(int param1Int) {
        return new LockscreenCredential[param1Int];
      }
    };
  
  private byte[] mCredential;
  
  private final int mType;
  
  public int describeContents() {
    return 0;
  }
  
  public void close() {
    zeroize();
  }
  
  public int hashCode() {
    return (this.mType + 17) * 31 + this.mCredential.hashCode();
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (paramObject == this)
      return true; 
    if (!(paramObject instanceof LockscreenCredential))
      return false; 
    paramObject = paramObject;
    if (this.mType != ((LockscreenCredential)paramObject).mType || !Arrays.equals(this.mCredential, ((LockscreenCredential)paramObject).mCredential))
      bool = false; 
    return bool;
  }
  
  private static byte[] charSequenceToByteArray(CharSequence paramCharSequence) {
    if (paramCharSequence == null)
      return new byte[0]; 
    byte[] arrayOfByte = new byte[paramCharSequence.length()];
    for (byte b = 0; b < paramCharSequence.length(); b++)
      arrayOfByte[b] = (byte)paramCharSequence.charAt(b); 
    return arrayOfByte;
  }
}
