package com.android.internal.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Region;
import android.hardware.input.InputManager;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.os.Trace;
import android.util.Log;
import android.view.ISystemGestureExclusionListener;
import android.view.IWindowManager;
import android.view.InputDevice;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.WindowInsets;
import android.view.WindowManagerGlobal;
import android.view.WindowManagerPolicyConstants;
import java.util.ArrayList;

public class PointerLocationView extends View implements InputManager.InputDeviceListener, WindowManagerPolicyConstants.PointerEventListener {
  class PointerState {
    private float[] mTraceX = new float[32];
    
    private float[] mTraceY = new float[32];
    
    private boolean[] mTraceCurrent = new boolean[32];
    
    private MotionEvent.PointerCoords mCoords = new MotionEvent.PointerCoords();
    
    private VelocityTracker.Estimator mEstimator = new VelocityTracker.Estimator();
    
    private VelocityTracker.Estimator mAltEstimator = new VelocityTracker.Estimator();
    
    private float mAltXVelocity;
    
    private float mAltYVelocity;
    
    private float mBoundingBottom;
    
    private float mBoundingLeft;
    
    private float mBoundingRight;
    
    private float mBoundingTop;
    
    private boolean mCurDown;
    
    private boolean mHasBoundingBox;
    
    private int mToolType;
    
    private int mTraceCount;
    
    private float mXVelocity;
    
    private float mYVelocity;
    
    public void clearTrace() {
      this.mTraceCount = 0;
    }
    
    public void addTrace(float param1Float1, float param1Float2, boolean param1Boolean) {
      float[] arrayOfFloat = this.mTraceX;
      int i = arrayOfFloat.length;
      int j = this.mTraceCount;
      if (j == i) {
        i *= 2;
        float[] arrayOfFloat1 = new float[i];
        System.arraycopy(arrayOfFloat, 0, arrayOfFloat1, 0, j);
        this.mTraceX = arrayOfFloat1;
        arrayOfFloat = new float[i];
        System.arraycopy(this.mTraceY, 0, arrayOfFloat, 0, this.mTraceCount);
        this.mTraceY = arrayOfFloat;
        boolean[] arrayOfBoolean = new boolean[i];
        System.arraycopy(this.mTraceCurrent, 0, arrayOfBoolean, 0, this.mTraceCount);
        this.mTraceCurrent = arrayOfBoolean;
      } 
      arrayOfFloat = this.mTraceX;
      j = this.mTraceCount;
      arrayOfFloat[j] = param1Float1;
      this.mTraceY[j] = param1Float2;
      this.mTraceCurrent[j] = param1Boolean;
      this.mTraceCount = j + 1;
    }
  }
  
  private final Paint.FontMetricsInt mTextMetrics = new Paint.FontMetricsInt();
  
  private int mHeaderPaddingTop = 0;
  
  private final ArrayList<PointerState> mPointers = new ArrayList<>();
  
  private final MotionEvent.PointerCoords mTempCoords = new MotionEvent.PointerCoords();
  
  private final Region mSystemGestureExclusion = new Region();
  
  private final Region mSystemGestureExclusionRejected = new Region();
  
  private final Path mSystemGestureExclusionPath = new Path();
  
  private final FasterStringBuilder mText = new FasterStringBuilder();
  
  private boolean mPrintCoords = true;
  
  private static boolean DEBUG_INPUT_TRACING = false;
  
  private static final String ALT_STRATEGY_PROPERY_KEY = "debug.velocitytracker.alt";
  
  private static final String GESTURE_EXCLUSION_PROP = "debug.pointerlocation.showexclusion";
  
  private static final String TAG = "Pointer";
  
  private int mActivePointerId;
  
  private final VelocityTracker mAltVelocity;
  
  private boolean mCurDown;
  
  private int mCurNumPointers;
  
  private final Paint mCurrentPointPaint;
  
  private int mHeaderBottom;
  
  private final InputManager mIm;
  
  private int mMaxNumPointers;
  
  private final Paint mPaint;
  
  private final Paint mPathPaint;
  
  private RectF mReusableOvalRect;
  
  private ISystemGestureExclusionListener mSystemGestureExclusionListener;
  
  private final Paint mSystemGestureExclusionPaint;
  
  private final Paint mSystemGestureExclusionRejectedPaint;
  
  private final Paint mTargetPaint;
  
  private final Paint mTextBackgroundPaint;
  
  private final Paint mTextLevelPaint;
  
  private final Paint mTextPaint;
  
  private final ViewConfiguration mVC;
  
  private final VelocityTracker mVelocity;
  
  public PointerLocationView(Context paramContext) {
    super(paramContext);
    this.mReusableOvalRect = new RectF();
    this.mSystemGestureExclusionListener = (ISystemGestureExclusionListener)new Object(this);
    setFocusableInTouchMode(true);
    this.mIm = (InputManager)paramContext.getSystemService(InputManager.class);
    this.mVC = ViewConfiguration.get(paramContext);
    Paint paint = new Paint();
    paint.setAntiAlias(true);
    paint = this.mTextPaint;
    float f = (getResources().getDisplayMetrics()).density;
    paint.setTextSize(f * 10.0F);
    this.mTextPaint.setARGB(255, 0, 0, 0);
    this.mTextBackgroundPaint = paint = new Paint();
    paint.setAntiAlias(false);
    this.mTextBackgroundPaint.setARGB(128, 255, 255, 255);
    this.mTextLevelPaint = paint = new Paint();
    paint.setAntiAlias(false);
    this.mTextLevelPaint.setARGB(192, 255, 0, 0);
    this.mPaint = paint = new Paint();
    paint.setAntiAlias(true);
    this.mPaint.setARGB(255, 255, 255, 255);
    this.mPaint.setStyle(Paint.Style.STROKE);
    this.mPaint.setStrokeWidth(2.0F);
    this.mCurrentPointPaint = paint = new Paint();
    paint.setAntiAlias(true);
    this.mCurrentPointPaint.setARGB(255, 255, 0, 0);
    this.mCurrentPointPaint.setStyle(Paint.Style.STROKE);
    this.mCurrentPointPaint.setStrokeWidth(2.0F);
    this.mTargetPaint = paint = new Paint();
    paint.setAntiAlias(false);
    this.mTargetPaint.setARGB(255, 0, 0, 192);
    this.mPathPaint = paint = new Paint();
    paint.setAntiAlias(false);
    this.mPathPaint.setARGB(255, 0, 96, 255);
    this.mPaint.setStyle(Paint.Style.STROKE);
    this.mPaint.setStrokeWidth(1.0F);
    this.mSystemGestureExclusionPaint = paint = new Paint();
    paint.setARGB(25, 255, 0, 0);
    this.mSystemGestureExclusionPaint.setStyle(Paint.Style.FILL_AND_STROKE);
    this.mSystemGestureExclusionRejectedPaint = paint = new Paint();
    paint.setARGB(25, 0, 0, 255);
    this.mSystemGestureExclusionRejectedPaint.setStyle(Paint.Style.FILL_AND_STROKE);
    PointerState pointerState = new PointerState();
    this.mPointers.add(pointerState);
    this.mActivePointerId = 0;
    this.mVelocity = VelocityTracker.obtain();
    DEBUG_INPUT_TRACING = SystemProperties.getBoolean("persist.sys.tp_input.trace", false);
    String str = SystemProperties.get("debug.velocitytracker.alt");
    if (str.length() != 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Comparing default velocity tracker strategy with ");
      stringBuilder.append(str);
      Log.d("Pointer", stringBuilder.toString());
      this.mAltVelocity = VelocityTracker.obtain(str);
    } else {
      this.mAltVelocity = null;
    } 
  }
  
  public void setPrintCoords(boolean paramBoolean) {
    this.mPrintCoords = paramBoolean;
  }
  
  public WindowInsets onApplyWindowInsets(WindowInsets paramWindowInsets) {
    if (paramWindowInsets.getDisplayCutout() != null) {
      this.mHeaderPaddingTop = paramWindowInsets.getDisplayCutout().getSafeInsetTop();
    } else {
      this.mHeaderPaddingTop = 0;
    } 
    return super.onApplyWindowInsets(paramWindowInsets);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    super.onMeasure(paramInt1, paramInt2);
    this.mTextPaint.getFontMetricsInt(this.mTextMetrics);
    this.mHeaderBottom = this.mHeaderPaddingTop - this.mTextMetrics.ascent + this.mTextMetrics.descent + 2;
  }
  
  private void drawOval(Canvas paramCanvas, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, Paint paramPaint) {
    paramCanvas.save(1);
    paramCanvas.rotate((float)((180.0F * paramFloat5) / Math.PI), paramFloat1, paramFloat2);
    this.mReusableOvalRect.left = paramFloat1 - paramFloat4 / 2.0F;
    this.mReusableOvalRect.right = paramFloat4 / 2.0F + paramFloat1;
    this.mReusableOvalRect.top = paramFloat2 - paramFloat3 / 2.0F;
    this.mReusableOvalRect.bottom = paramFloat3 / 2.0F + paramFloat2;
    paramCanvas.drawOval(this.mReusableOvalRect, paramPaint);
    paramCanvas.restore();
  }
  
  protected void onDraw(Canvas paramCanvas) {
    int i = getWidth();
    int j = i / 7;
    int k = this.mHeaderPaddingTop - this.mTextMetrics.ascent + 1;
    int m = this.mHeaderBottom;
    int n = this.mPointers.size();
    if (!this.mSystemGestureExclusion.isEmpty()) {
      this.mSystemGestureExclusionPath.reset();
      this.mSystemGestureExclusion.getBoundaryPath(this.mSystemGestureExclusionPath);
      paramCanvas.drawPath(this.mSystemGestureExclusionPath, this.mSystemGestureExclusionPaint);
    } 
    if (!this.mSystemGestureExclusionRejected.isEmpty()) {
      this.mSystemGestureExclusionPath.reset();
      this.mSystemGestureExclusionRejected.getBoundaryPath(this.mSystemGestureExclusionPath);
      paramCanvas.drawPath(this.mSystemGestureExclusionPath, this.mSystemGestureExclusionRejectedPaint);
    } 
    int i1 = this.mActivePointerId;
    if (i1 >= 0 && i1 < n) {
      PointerState pointerState = this.mPointers.get(i1);
      paramCanvas.drawRect(0.0F, this.mHeaderPaddingTop, (j - 1), m, this.mTextBackgroundPaint);
      FasterStringBuilder fasterStringBuilder5 = this.mText.clear();
      fasterStringBuilder5 = fasterStringBuilder5.append("P: ").append(this.mCurNumPointers);
      fasterStringBuilder5 = fasterStringBuilder5.append(" / ").append(this.mMaxNumPointers);
      String str5 = fasterStringBuilder5.toString();
      float f1 = k;
      Paint paint4 = this.mTextPaint;
      paramCanvas.drawText(str5, 1.0F, f1, paint4);
      i1 = pointerState.mTraceCount;
      if ((this.mCurDown && pointerState.mCurDown) || i1 == 0) {
        paramCanvas.drawRect(j, this.mHeaderPaddingTop, (j * 2 - 1), m, this.mTextBackgroundPaint);
        FasterStringBuilder fasterStringBuilder7 = this.mText.clear();
        fasterStringBuilder7 = fasterStringBuilder7.append("X: ").append(pointerState.mCoords.x, 1);
        String str7 = fasterStringBuilder7.toString();
        f1 = (j + 1);
        float f = k;
        Paint paint = this.mTextPaint;
        paramCanvas.drawText(str7, f1, f, paint);
        paramCanvas.drawRect((j * 2), this.mHeaderPaddingTop, (j * 3 - 1), m, this.mTextBackgroundPaint);
        FasterStringBuilder fasterStringBuilder6 = this.mText.clear();
        fasterStringBuilder6 = fasterStringBuilder6.append("Y: ").append(pointerState.mCoords.y, 1);
        String str6 = fasterStringBuilder6.toString();
        f = (j * 2 + 1);
        f1 = k;
        paint = this.mTextPaint;
        paramCanvas.drawText(str6, f, f1, paint);
      } else {
        float f8 = pointerState.mTraceX[i1 - 1] - pointerState.mTraceX[0];
        f1 = pointerState.mTraceY[i1 - 1] - pointerState.mTraceY[0];
        float f9 = j, f7 = this.mHeaderPaddingTop, f10 = (j * 2 - 1), f11 = m;
        if (Math.abs(f8) < this.mVC.getScaledTouchSlop()) {
          paint4 = this.mTextBackgroundPaint;
        } else {
          paint4 = this.mTextLevelPaint;
        } 
        paramCanvas.drawRect(f9, f7, f10, f11, paint4);
        FasterStringBuilder fasterStringBuilder7 = this.mText.clear();
        fasterStringBuilder7 = fasterStringBuilder7.append("dX: ").append(f8, 1);
        str5 = fasterStringBuilder7.toString();
        f7 = (j + 1);
        f11 = k;
        Paint paint8 = this.mTextPaint;
        paramCanvas.drawText(str5, f7, f11, paint8);
        f9 = (j * 2);
        f11 = this.mHeaderPaddingTop;
        f7 = (j * 3 - 1);
        f10 = m;
        if (Math.abs(f1) < this.mVC.getScaledTouchSlop()) {
          paint8 = this.mTextBackgroundPaint;
        } else {
          paint8 = this.mTextLevelPaint;
        } 
        paramCanvas.drawRect(f9, f11, f7, f10, paint8);
        FasterStringBuilder fasterStringBuilder6 = this.mText.clear();
        fasterStringBuilder6 = fasterStringBuilder6.append("dY: ").append(f1, 1);
        str5 = fasterStringBuilder6.toString();
        f7 = (j * 2 + 1);
        f1 = k;
        Paint paint7 = this.mTextPaint;
        paramCanvas.drawText(str5, f7, f1, paint7);
      } 
      paramCanvas.drawRect((j * 3), this.mHeaderPaddingTop, (j * 4 - 1), m, this.mTextBackgroundPaint);
      FasterStringBuilder fasterStringBuilder4 = this.mText.clear();
      fasterStringBuilder4 = fasterStringBuilder4.append("Xv: ").append(pointerState.mXVelocity, 3);
      String str3 = fasterStringBuilder4.toString();
      float f2 = (j * 3 + 1);
      f1 = k;
      Paint paint6 = this.mTextPaint;
      paramCanvas.drawText(str3, f2, f1, paint6);
      paramCanvas.drawRect((j * 4), this.mHeaderPaddingTop, (j * 5 - 1), m, this.mTextBackgroundPaint);
      FasterStringBuilder fasterStringBuilder3 = this.mText.clear();
      fasterStringBuilder3 = fasterStringBuilder3.append("Yv: ").append(pointerState.mYVelocity, 3);
      String str4 = fasterStringBuilder3.toString();
      f2 = (j * 4 + 1);
      f1 = k;
      Paint paint3 = this.mTextPaint;
      paramCanvas.drawText(str4, f2, f1, paint3);
      paramCanvas.drawRect((j * 5), this.mHeaderPaddingTop, (j * 6 - 1), m, this.mTextBackgroundPaint);
      f2 = (j * 5);
      float f4 = this.mHeaderPaddingTop;
      f1 = (j * 5);
      float f3 = pointerState.mCoords.pressure, f6 = j, f5 = m;
      paint3 = this.mTextLevelPaint;
      paramCanvas.drawRect(f2, f4, f1 + f3 * f6 - 1.0F, f5, paint3);
      FasterStringBuilder fasterStringBuilder2 = this.mText.clear();
      fasterStringBuilder2 = fasterStringBuilder2.append("Prs: ").append(pointerState.mCoords.pressure, 2);
      String str2 = fasterStringBuilder2.toString();
      f1 = (j * 5 + 1);
      f2 = k;
      Paint paint5 = this.mTextPaint;
      paramCanvas.drawText(str2, f1, f2, paint5);
      paramCanvas.drawRect((j * 6), this.mHeaderPaddingTop, i, m, this.mTextBackgroundPaint);
      f6 = (j * 6);
      f1 = this.mHeaderPaddingTop;
      f5 = (j * 6);
      f2 = pointerState.mCoords.size;
      f3 = j;
      f4 = m;
      Paint paint2 = this.mTextLevelPaint;
      paramCanvas.drawRect(f6, f1, f5 + f2 * f3 - 1.0F, f4, paint2);
      FasterStringBuilder fasterStringBuilder1 = this.mText.clear();
      fasterStringBuilder1 = fasterStringBuilder1.append("Size: ").append(pointerState.mCoords.size, 2);
      String str1 = fasterStringBuilder1.toString();
      f2 = (j * 6 + 1);
      f1 = k;
      Paint paint1 = this.mTextPaint;
      paramCanvas.drawText(str1, f2, f1, paint1);
    } 
    for (byte b = 0; b < n; b++) {
      PointerState pointerState = this.mPointers.get(b);
      int i2 = pointerState.mTraceCount;
      Paint paint = this.mPaint;
      char c = 'ÿ';
      paint.setARGB(255, 128, 255, 255);
      float f1, f2;
      byte b1;
      for (i1 = 0, m = 0, b1 = 0, f1 = 0.0F, f2 = 0.0F; b1 < i2; b1++) {
        float f3 = pointerState.mTraceX[b1];
        float f4 = pointerState.mTraceY[b1];
        if (Float.isNaN(f3)) {
          i1 = 0;
        } else {
          if (i1 != 0) {
            paramCanvas.drawLine(f1, f2, f3, f4, this.mPathPaint);
            if (pointerState.mTraceCurrent[b1]) {
              paint = this.mCurrentPointPaint;
            } else {
              paint = this.mPaint;
            } 
            paramCanvas.drawPoint(f1, f2, paint);
            m = 1;
          } 
          f1 = f3;
          f2 = f4;
          i1 = 1;
        } 
      } 
      if (m != 0) {
        this.mPaint.setARGB(c, c, 64, 128);
        float f4 = pointerState.mXVelocity;
        float f3 = pointerState.mYVelocity;
        paramCanvas.drawLine(f1, f2, f1 + f4 * 16.0F, f2 + f3 * 16.0F, this.mPaint);
        if (this.mAltVelocity != null) {
          this.mPaint.setARGB(c, 64, c, 128);
          f4 = pointerState.mAltXVelocity;
          f3 = pointerState.mAltYVelocity;
          paramCanvas.drawLine(f1, f2, f1 + f4 * 16.0F, f2 + 16.0F * f3, this.mPaint);
        } 
      } 
      if (this.mCurDown && pointerState.mCurDown) {
        if (DEBUG_INPUT_TRACING) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Draw x:");
          stringBuilder.append(pointerState.mCoords.x);
          stringBuilder.append(" y:");
          stringBuilder.append(pointerState.mCoords.y);
          Trace.traceBegin(8L, stringBuilder.toString());
        } 
        paramCanvas.drawLine(0.0F, pointerState.mCoords.y, getWidth(), pointerState.mCoords.y, this.mTargetPaint);
        paramCanvas.drawLine(pointerState.mCoords.x, 0.0F, pointerState.mCoords.x, getHeight(), this.mTargetPaint);
        m = (int)(pointerState.mCoords.pressure * 255.0F);
        this.mPaint.setARGB(c, m, c, 255 - m);
        paramCanvas.drawPoint(pointerState.mCoords.x, pointerState.mCoords.y, this.mPaint);
        this.mPaint.setARGB(c, m, 255 - m, 128);
        f2 = pointerState.mCoords.x;
        f1 = pointerState.mCoords.y;
        float f4 = pointerState.mCoords.touchMajor;
        float f5 = pointerState.mCoords.touchMinor, f3 = pointerState.mCoords.orientation;
        paint = this.mPaint;
        drawOval(paramCanvas, f2, f1, f4, f5, f3, paint);
        this.mPaint.setARGB(255, m, 128, 255 - m);
        f3 = pointerState.mCoords.x;
        f5 = pointerState.mCoords.y;
        f2 = pointerState.mCoords.toolMajor;
        f4 = pointerState.mCoords.toolMinor;
        f1 = pointerState.mCoords.orientation;
        paint = this.mPaint;
        drawOval(paramCanvas, f3, f5, f2, f4, f1, paint);
        f1 = pointerState.mCoords.toolMajor * 0.7F;
        if (f1 < 20.0F)
          f1 = 20.0F; 
        this.mPaint.setARGB(255, m, 255, 0);
        f2 = (float)(Math.sin(pointerState.mCoords.orientation) * f1);
        f1 = (float)(-Math.cos(pointerState.mCoords.orientation) * f1);
        if (pointerState.mToolType == 2 || pointerState.mToolType == 4) {
          f3 = pointerState.mCoords.x;
          f4 = pointerState.mCoords.y;
          f5 = pointerState.mCoords.x;
          float f = pointerState.mCoords.y;
          paint = this.mPaint;
          paramCanvas.drawLine(f3, f4, f5 + f2, f + f1, paint);
        } else {
          float f = pointerState.mCoords.x;
          f4 = pointerState.mCoords.y;
          f3 = pointerState.mCoords.x;
          f5 = pointerState.mCoords.y;
          paint = this.mPaint;
          paramCanvas.drawLine(f - f2, f4 - f1, f3 + f2, f5 + f1, paint);
        } 
        double d = pointerState.mCoords.getAxisValue(25);
        f5 = (float)Math.sin(d);
        f3 = pointerState.mCoords.x;
        f4 = pointerState.mCoords.y;
        paint = this.mPaint;
        paramCanvas.drawCircle(f3 + f2 * f5, f4 + f1 * f5, 3.0F, paint);
        if (pointerState.mHasBoundingBox) {
          f5 = pointerState.mBoundingLeft;
          f4 = pointerState.mBoundingTop;
          f2 = pointerState.mBoundingRight;
          f1 = pointerState.mBoundingBottom;
          paint = this.mPaint;
          paramCanvas.drawRect(f5, f4, f2, f1, paint);
        } 
        if (DEBUG_INPUT_TRACING)
          Trace.traceEnd(8L); 
      } 
    } 
  }
  
  private void logMotionEvent(String paramString, MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.getAction();
    int j = paramMotionEvent.getHistorySize();
    int k = paramMotionEvent.getPointerCount();
    byte b;
    for (b = 0; b < j; b++) {
      for (byte b1 = 0; b1 < k; b1++) {
        int m = paramMotionEvent.getPointerId(b1);
        paramMotionEvent.getHistoricalPointerCoords(b1, b, this.mTempCoords);
        logCoords(paramString, i, b1, this.mTempCoords, m, paramMotionEvent);
      } 
    } 
    for (b = 0; b < k; b++) {
      int m = paramMotionEvent.getPointerId(b);
      paramMotionEvent.getPointerCoords(b, this.mTempCoords);
      logCoords(paramString, i, b, this.mTempCoords, m, paramMotionEvent);
    } 
  }
  
  private void logCoords(String paramString, int paramInt1, int paramInt2, MotionEvent.PointerCoords paramPointerCoords, int paramInt3, MotionEvent paramMotionEvent) {
    String str2;
    int i = paramMotionEvent.getToolType(paramInt2);
    int j = paramMotionEvent.getButtonState();
    switch (paramInt1 & 0xFF) {
      default:
        str2 = Integer.toString(paramInt1);
        break;
      case 10:
        str2 = "HOVER EXIT";
        break;
      case 9:
        str2 = "HOVER ENTER";
        break;
      case 8:
        str2 = "SCROLL";
        break;
      case 7:
        str2 = "HOVER MOVE";
        break;
      case 6:
        if (paramInt2 == (paramInt1 & 0xFF00) >> 8) {
          str2 = "UP";
          break;
        } 
        str2 = "MOVE";
        break;
      case 5:
        if (paramInt2 == (paramInt1 & 0xFF00) >> 8) {
          str2 = "DOWN";
          break;
        } 
        str2 = "MOVE";
        break;
      case 4:
        str2 = "OUTSIDE";
        break;
      case 3:
        str2 = "CANCEL";
        break;
      case 2:
        str2 = "MOVE";
        break;
      case 1:
        str2 = "UP";
        break;
      case 0:
        str2 = "DOWN";
        break;
    } 
    FasterStringBuilder fasterStringBuilder2 = this.mText.clear();
    FasterStringBuilder fasterStringBuilder1 = fasterStringBuilder2.append(paramString).append(" id ").append(paramInt3 + 1);
    fasterStringBuilder1 = fasterStringBuilder1.append(": ");
    fasterStringBuilder1 = fasterStringBuilder1.append(str2);
    fasterStringBuilder1 = fasterStringBuilder1.append(" (").append(paramPointerCoords.x, 3).append(", ").append(paramPointerCoords.y, 3);
    fasterStringBuilder1 = fasterStringBuilder1.append(") Pressure=").append(paramPointerCoords.pressure, 3);
    fasterStringBuilder1 = fasterStringBuilder1.append(" Size=").append(paramPointerCoords.size, 3);
    fasterStringBuilder1 = fasterStringBuilder1.append(" TouchMajor=").append(paramPointerCoords.touchMajor, 3);
    fasterStringBuilder1 = fasterStringBuilder1.append(" TouchMinor=").append(paramPointerCoords.touchMinor, 3);
    fasterStringBuilder1 = fasterStringBuilder1.append(" ToolMajor=").append(paramPointerCoords.toolMajor, 3);
    fasterStringBuilder1 = fasterStringBuilder1.append(" ToolMinor=").append(paramPointerCoords.toolMinor, 3);
    fasterStringBuilder1 = fasterStringBuilder1.append(" Orientation=").append((float)((paramPointerCoords.orientation * 180.0F) / Math.PI), 1);
    fasterStringBuilder1 = fasterStringBuilder1.append("deg");
    fasterStringBuilder1 = fasterStringBuilder1.append(" Tilt=");
    float f = (float)((paramPointerCoords.getAxisValue(25) * 180.0F) / Math.PI);
    fasterStringBuilder1 = fasterStringBuilder1.append(f, 1);
    fasterStringBuilder1 = fasterStringBuilder1.append("deg");
    fasterStringBuilder1 = fasterStringBuilder1.append(" Distance=").append(paramPointerCoords.getAxisValue(24), 1);
    fasterStringBuilder1 = fasterStringBuilder1.append(" VScroll=").append(paramPointerCoords.getAxisValue(9), 1);
    fasterStringBuilder1 = fasterStringBuilder1.append(" HScroll=").append(paramPointerCoords.getAxisValue(10), 1);
    fasterStringBuilder1 = fasterStringBuilder1.append(" BoundingBox=[(");
    fasterStringBuilder1 = fasterStringBuilder1.append(paramMotionEvent.getAxisValue(32), 3);
    fasterStringBuilder1 = fasterStringBuilder1.append(", ").append(paramMotionEvent.getAxisValue(33), 3).append(")");
    fasterStringBuilder1 = fasterStringBuilder1.append(", (").append(paramMotionEvent.getAxisValue(34), 3);
    fasterStringBuilder1 = fasterStringBuilder1.append(", ").append(paramMotionEvent.getAxisValue(35), 3);
    fasterStringBuilder1 = fasterStringBuilder1.append(")]");
    fasterStringBuilder1 = fasterStringBuilder1.append(" ToolType=").append(MotionEvent.toolTypeToString(i));
    fasterStringBuilder1 = fasterStringBuilder1.append(" ButtonState=").append(MotionEvent.buttonStateToString(j));
    String str1 = fasterStringBuilder1.toString();
    Log.i("Pointer", str1);
  }
  
  public void onPointerEvent(MotionEvent paramMotionEvent) {
    // Byte code:
    //   0: getstatic com/android/internal/widget/PointerLocationView.DEBUG_INPUT_TRACING : Z
    //   3: ifeq -> 58
    //   6: new java/lang/StringBuilder
    //   9: dup
    //   10: invokespecial <init> : ()V
    //   13: astore_2
    //   14: aload_2
    //   15: ldc_w 'PointerLocationView#onPointerEvent,x:'
    //   18: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   21: pop
    //   22: aload_2
    //   23: aload_1
    //   24: invokevirtual getRawX : ()F
    //   27: invokevirtual append : (F)Ljava/lang/StringBuilder;
    //   30: pop
    //   31: aload_2
    //   32: ldc_w 'y:'
    //   35: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   38: pop
    //   39: aload_2
    //   40: aload_1
    //   41: invokevirtual getRawY : ()F
    //   44: invokevirtual append : (F)Ljava/lang/StringBuilder;
    //   47: pop
    //   48: ldc2_w 8
    //   51: aload_2
    //   52: invokevirtual toString : ()Ljava/lang/String;
    //   55: invokestatic traceBegin : (JLjava/lang/String;)V
    //   58: aload_1
    //   59: invokevirtual getAction : ()I
    //   62: istore_3
    //   63: aload_0
    //   64: getfield mPointers : Ljava/util/ArrayList;
    //   67: invokevirtual size : ()I
    //   70: istore #4
    //   72: iload_3
    //   73: ifeq -> 91
    //   76: iload_3
    //   77: sipush #255
    //   80: iand
    //   81: iconst_5
    //   82: if_icmpne -> 88
    //   85: goto -> 91
    //   88: goto -> 345
    //   91: iload_3
    //   92: ifne -> 169
    //   95: iconst_0
    //   96: istore #5
    //   98: iload #5
    //   100: iload #4
    //   102: if_icmpge -> 134
    //   105: aload_0
    //   106: getfield mPointers : Ljava/util/ArrayList;
    //   109: iload #5
    //   111: invokevirtual get : (I)Ljava/lang/Object;
    //   114: checkcast com/android/internal/widget/PointerLocationView$PointerState
    //   117: astore_2
    //   118: aload_2
    //   119: invokevirtual clearTrace : ()V
    //   122: aload_2
    //   123: iconst_0
    //   124: invokestatic access$102 : (Lcom/android/internal/widget/PointerLocationView$PointerState;Z)Z
    //   127: pop
    //   128: iinc #5, 1
    //   131: goto -> 98
    //   134: aload_0
    //   135: iconst_1
    //   136: putfield mCurDown : Z
    //   139: aload_0
    //   140: iconst_0
    //   141: putfield mCurNumPointers : I
    //   144: aload_0
    //   145: iconst_0
    //   146: putfield mMaxNumPointers : I
    //   149: aload_0
    //   150: getfield mVelocity : Landroid/view/VelocityTracker;
    //   153: invokevirtual clear : ()V
    //   156: aload_0
    //   157: getfield mAltVelocity : Landroid/view/VelocityTracker;
    //   160: astore_2
    //   161: aload_2
    //   162: ifnull -> 169
    //   165: aload_2
    //   166: invokevirtual clear : ()V
    //   169: aload_0
    //   170: getfield mCurNumPointers : I
    //   173: iconst_1
    //   174: iadd
    //   175: istore #5
    //   177: aload_0
    //   178: iload #5
    //   180: putfield mCurNumPointers : I
    //   183: aload_0
    //   184: getfield mMaxNumPointers : I
    //   187: iload #5
    //   189: if_icmpge -> 198
    //   192: aload_0
    //   193: iload #5
    //   195: putfield mMaxNumPointers : I
    //   198: aload_1
    //   199: iload_3
    //   200: ldc_w 65280
    //   203: iand
    //   204: bipush #8
    //   206: ishr
    //   207: invokevirtual getPointerId : (I)I
    //   210: istore #5
    //   212: iload #4
    //   214: iload #5
    //   216: if_icmpgt -> 242
    //   219: new com/android/internal/widget/PointerLocationView$PointerState
    //   222: dup
    //   223: invokespecial <init> : ()V
    //   226: astore_2
    //   227: aload_0
    //   228: getfield mPointers : Ljava/util/ArrayList;
    //   231: aload_2
    //   232: invokevirtual add : (Ljava/lang/Object;)Z
    //   235: pop
    //   236: iinc #4, 1
    //   239: goto -> 212
    //   242: aload_0
    //   243: getfield mActivePointerId : I
    //   246: istore #6
    //   248: iload #6
    //   250: iflt -> 280
    //   253: iload #6
    //   255: iload #4
    //   257: if_icmpge -> 286
    //   260: aload_0
    //   261: getfield mPointers : Ljava/util/ArrayList;
    //   264: astore_2
    //   265: aload_2
    //   266: iload #6
    //   268: invokevirtual get : (I)Ljava/lang/Object;
    //   271: checkcast com/android/internal/widget/PointerLocationView$PointerState
    //   274: invokestatic access$100 : (Lcom/android/internal/widget/PointerLocationView$PointerState;)Z
    //   277: ifne -> 286
    //   280: aload_0
    //   281: iload #5
    //   283: putfield mActivePointerId : I
    //   286: aload_0
    //   287: getfield mPointers : Ljava/util/ArrayList;
    //   290: iload #5
    //   292: invokevirtual get : (I)Ljava/lang/Object;
    //   295: checkcast com/android/internal/widget/PointerLocationView$PointerState
    //   298: astore_2
    //   299: aload_2
    //   300: iconst_1
    //   301: invokestatic access$102 : (Lcom/android/internal/widget/PointerLocationView$PointerState;Z)Z
    //   304: pop
    //   305: aload_1
    //   306: invokevirtual getDeviceId : ()I
    //   309: invokestatic getDevice : (I)Landroid/view/InputDevice;
    //   312: astore #7
    //   314: aload #7
    //   316: ifnull -> 335
    //   319: aload #7
    //   321: bipush #32
    //   323: invokevirtual getMotionRange : (I)Landroid/view/InputDevice$MotionRange;
    //   326: ifnull -> 335
    //   329: iconst_1
    //   330: istore #8
    //   332: goto -> 338
    //   335: iconst_0
    //   336: istore #8
    //   338: aload_2
    //   339: iload #8
    //   341: invokestatic access$1102 : (Lcom/android/internal/widget/PointerLocationView$PointerState;Z)Z
    //   344: pop
    //   345: aload_1
    //   346: invokevirtual getPointerCount : ()I
    //   349: istore #9
    //   351: aload_0
    //   352: getfield mVelocity : Landroid/view/VelocityTracker;
    //   355: aload_1
    //   356: invokevirtual addMovement : (Landroid/view/MotionEvent;)V
    //   359: aload_0
    //   360: getfield mVelocity : Landroid/view/VelocityTracker;
    //   363: iconst_1
    //   364: invokevirtual computeCurrentVelocity : (I)V
    //   367: aload_0
    //   368: getfield mAltVelocity : Landroid/view/VelocityTracker;
    //   371: astore_2
    //   372: aload_2
    //   373: ifnull -> 389
    //   376: aload_2
    //   377: aload_1
    //   378: invokevirtual addMovement : (Landroid/view/MotionEvent;)V
    //   381: aload_0
    //   382: getfield mAltVelocity : Landroid/view/VelocityTracker;
    //   385: iconst_1
    //   386: invokevirtual computeCurrentVelocity : (I)V
    //   389: aload_1
    //   390: invokevirtual getHistorySize : ()I
    //   393: istore #10
    //   395: iconst_0
    //   396: istore #6
    //   398: iload #4
    //   400: istore #5
    //   402: iload #6
    //   404: istore #4
    //   406: iload #4
    //   408: iload #10
    //   410: if_icmpge -> 567
    //   413: iconst_0
    //   414: istore #11
    //   416: iload #5
    //   418: istore #6
    //   420: iload #10
    //   422: istore #5
    //   424: iload #11
    //   426: istore #10
    //   428: iload #10
    //   430: iload #9
    //   432: if_icmpge -> 553
    //   435: aload_1
    //   436: iload #10
    //   438: invokevirtual getPointerId : (I)I
    //   441: istore #11
    //   443: aload_0
    //   444: getfield mCurDown : Z
    //   447: ifeq -> 473
    //   450: iload #11
    //   452: iload #6
    //   454: if_icmpge -> 473
    //   457: aload_0
    //   458: getfield mPointers : Ljava/util/ArrayList;
    //   461: iload #11
    //   463: invokevirtual get : (I)Ljava/lang/Object;
    //   466: checkcast com/android/internal/widget/PointerLocationView$PointerState
    //   469: astore_2
    //   470: goto -> 475
    //   473: aconst_null
    //   474: astore_2
    //   475: aload_2
    //   476: ifnull -> 488
    //   479: aload_2
    //   480: invokestatic access$200 : (Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;
    //   483: astore #7
    //   485: goto -> 494
    //   488: aload_0
    //   489: getfield mTempCoords : Landroid/view/MotionEvent$PointerCoords;
    //   492: astore #7
    //   494: aload_1
    //   495: iload #10
    //   497: iload #4
    //   499: aload #7
    //   501: invokevirtual getHistoricalPointerCoords : (IILandroid/view/MotionEvent$PointerCoords;)V
    //   504: aload_0
    //   505: getfield mPrintCoords : Z
    //   508: ifeq -> 528
    //   511: aload_0
    //   512: ldc 'Pointer'
    //   514: iload_3
    //   515: iload #10
    //   517: aload #7
    //   519: iload #11
    //   521: aload_1
    //   522: invokespecial logCoords : (Ljava/lang/String;IILandroid/view/MotionEvent$PointerCoords;ILandroid/view/MotionEvent;)V
    //   525: goto -> 528
    //   528: aload_2
    //   529: ifnull -> 547
    //   532: aload_2
    //   533: aload #7
    //   535: getfield x : F
    //   538: aload #7
    //   540: getfield y : F
    //   543: iconst_0
    //   544: invokevirtual addTrace : (FFZ)V
    //   547: iinc #10, 1
    //   550: goto -> 428
    //   553: iinc #4, 1
    //   556: iload #5
    //   558: istore #10
    //   560: iload #6
    //   562: istore #5
    //   564: goto -> 406
    //   567: iconst_0
    //   568: istore #4
    //   570: iload #4
    //   572: iload #9
    //   574: if_icmpge -> 862
    //   577: aload_1
    //   578: iload #4
    //   580: invokevirtual getPointerId : (I)I
    //   583: istore #6
    //   585: aload_0
    //   586: getfield mCurDown : Z
    //   589: ifeq -> 615
    //   592: iload #6
    //   594: iload #5
    //   596: if_icmpge -> 615
    //   599: aload_0
    //   600: getfield mPointers : Ljava/util/ArrayList;
    //   603: iload #6
    //   605: invokevirtual get : (I)Ljava/lang/Object;
    //   608: checkcast com/android/internal/widget/PointerLocationView$PointerState
    //   611: astore_2
    //   612: goto -> 617
    //   615: aconst_null
    //   616: astore_2
    //   617: aload_2
    //   618: ifnull -> 630
    //   621: aload_2
    //   622: invokestatic access$200 : (Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;
    //   625: astore #7
    //   627: goto -> 636
    //   630: aload_0
    //   631: getfield mTempCoords : Landroid/view/MotionEvent$PointerCoords;
    //   634: astore #7
    //   636: aload_1
    //   637: iload #4
    //   639: aload #7
    //   641: invokevirtual getPointerCoords : (ILandroid/view/MotionEvent$PointerCoords;)V
    //   644: aload_0
    //   645: getfield mPrintCoords : Z
    //   648: ifeq -> 668
    //   651: aload_0
    //   652: ldc 'Pointer'
    //   654: iload_3
    //   655: iload #4
    //   657: aload #7
    //   659: iload #6
    //   661: aload_1
    //   662: invokespecial logCoords : (Ljava/lang/String;IILandroid/view/MotionEvent$PointerCoords;ILandroid/view/MotionEvent;)V
    //   665: goto -> 668
    //   668: aload_2
    //   669: ifnull -> 856
    //   672: aload_2
    //   673: aload #7
    //   675: getfield x : F
    //   678: aload #7
    //   680: getfield y : F
    //   683: iconst_1
    //   684: invokevirtual addTrace : (FFZ)V
    //   687: aload_2
    //   688: aload_0
    //   689: getfield mVelocity : Landroid/view/VelocityTracker;
    //   692: iload #6
    //   694: invokevirtual getXVelocity : (I)F
    //   697: invokestatic access$502 : (Lcom/android/internal/widget/PointerLocationView$PointerState;F)F
    //   700: pop
    //   701: aload_2
    //   702: aload_0
    //   703: getfield mVelocity : Landroid/view/VelocityTracker;
    //   706: iload #6
    //   708: invokevirtual getYVelocity : (I)F
    //   711: invokestatic access$602 : (Lcom/android/internal/widget/PointerLocationView$PointerState;F)F
    //   714: pop
    //   715: aload_0
    //   716: getfield mVelocity : Landroid/view/VelocityTracker;
    //   719: iload #6
    //   721: aload_2
    //   722: invokestatic access$1600 : (Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/VelocityTracker$Estimator;
    //   725: invokevirtual getEstimator : (ILandroid/view/VelocityTracker$Estimator;)Z
    //   728: pop
    //   729: aload_0
    //   730: getfield mAltVelocity : Landroid/view/VelocityTracker;
    //   733: astore #7
    //   735: aload #7
    //   737: ifnull -> 780
    //   740: aload_2
    //   741: aload #7
    //   743: iload #6
    //   745: invokevirtual getXVelocity : (I)F
    //   748: invokestatic access$802 : (Lcom/android/internal/widget/PointerLocationView$PointerState;F)F
    //   751: pop
    //   752: aload_2
    //   753: aload_0
    //   754: getfield mAltVelocity : Landroid/view/VelocityTracker;
    //   757: iload #6
    //   759: invokevirtual getYVelocity : (I)F
    //   762: invokestatic access$902 : (Lcom/android/internal/widget/PointerLocationView$PointerState;F)F
    //   765: pop
    //   766: aload_0
    //   767: getfield mAltVelocity : Landroid/view/VelocityTracker;
    //   770: iload #6
    //   772: aload_2
    //   773: invokestatic access$1700 : (Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/VelocityTracker$Estimator;
    //   776: invokevirtual getEstimator : (ILandroid/view/VelocityTracker$Estimator;)Z
    //   779: pop
    //   780: aload_2
    //   781: aload_1
    //   782: iload #4
    //   784: invokevirtual getToolType : (I)I
    //   787: invokestatic access$1002 : (Lcom/android/internal/widget/PointerLocationView$PointerState;I)I
    //   790: pop
    //   791: aload_2
    //   792: invokestatic access$1100 : (Lcom/android/internal/widget/PointerLocationView$PointerState;)Z
    //   795: ifeq -> 853
    //   798: aload_2
    //   799: aload_1
    //   800: bipush #32
    //   802: iload #4
    //   804: invokevirtual getAxisValue : (II)F
    //   807: invokestatic access$1202 : (Lcom/android/internal/widget/PointerLocationView$PointerState;F)F
    //   810: pop
    //   811: aload_2
    //   812: aload_1
    //   813: bipush #33
    //   815: iload #4
    //   817: invokevirtual getAxisValue : (II)F
    //   820: invokestatic access$1302 : (Lcom/android/internal/widget/PointerLocationView$PointerState;F)F
    //   823: pop
    //   824: aload_2
    //   825: aload_1
    //   826: bipush #34
    //   828: iload #4
    //   830: invokevirtual getAxisValue : (II)F
    //   833: invokestatic access$1402 : (Lcom/android/internal/widget/PointerLocationView$PointerState;F)F
    //   836: pop
    //   837: aload_2
    //   838: aload_1
    //   839: bipush #35
    //   841: iload #4
    //   843: invokevirtual getAxisValue : (II)F
    //   846: invokestatic access$1502 : (Lcom/android/internal/widget/PointerLocationView$PointerState;F)F
    //   849: pop
    //   850: goto -> 856
    //   853: goto -> 856
    //   856: iinc #4, 1
    //   859: goto -> 570
    //   862: getstatic com/android/internal/widget/PointerLocationView.DEBUG_INPUT_TRACING : Z
    //   865: ifeq -> 874
    //   868: ldc2_w 8
    //   871: invokestatic traceEnd : (J)V
    //   874: iload_3
    //   875: iconst_1
    //   876: if_icmpeq -> 894
    //   879: iload_3
    //   880: iconst_3
    //   881: if_icmpeq -> 894
    //   884: iload_3
    //   885: sipush #255
    //   888: iand
    //   889: bipush #6
    //   891: if_icmpne -> 1101
    //   894: ldc_w 65280
    //   897: iload_3
    //   898: iand
    //   899: bipush #8
    //   901: ishr
    //   902: istore #9
    //   904: aload_1
    //   905: iload #9
    //   907: invokevirtual getPointerId : (I)I
    //   910: istore #6
    //   912: iload #6
    //   914: iload #5
    //   916: if_icmplt -> 1002
    //   919: new java/lang/StringBuilder
    //   922: dup
    //   923: invokespecial <init> : ()V
    //   926: astore_1
    //   927: aload_1
    //   928: ldc_w 'Got pointer ID out of bounds: id='
    //   931: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   934: pop
    //   935: aload_1
    //   936: iload #6
    //   938: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   941: pop
    //   942: aload_1
    //   943: ldc_w ' arraysize='
    //   946: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   949: pop
    //   950: aload_1
    //   951: iload #5
    //   953: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   956: pop
    //   957: aload_1
    //   958: ldc_w ' pointerindex='
    //   961: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   964: pop
    //   965: aload_1
    //   966: iload #9
    //   968: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   971: pop
    //   972: aload_1
    //   973: ldc_w ' action=0x'
    //   976: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   979: pop
    //   980: aload_1
    //   981: iload_3
    //   982: invokestatic toHexString : (I)Ljava/lang/String;
    //   985: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   988: pop
    //   989: aload_1
    //   990: invokevirtual toString : ()Ljava/lang/String;
    //   993: astore_1
    //   994: ldc 'Pointer'
    //   996: aload_1
    //   997: invokestatic wtf : (Ljava/lang/String;Ljava/lang/String;)I
    //   1000: pop
    //   1001: return
    //   1002: aload_0
    //   1003: getfield mPointers : Ljava/util/ArrayList;
    //   1006: iload #6
    //   1008: invokevirtual get : (I)Ljava/lang/Object;
    //   1011: checkcast com/android/internal/widget/PointerLocationView$PointerState
    //   1014: astore_2
    //   1015: aload_2
    //   1016: iconst_0
    //   1017: invokestatic access$102 : (Lcom/android/internal/widget/PointerLocationView$PointerState;Z)Z
    //   1020: pop
    //   1021: iconst_1
    //   1022: istore #4
    //   1024: iload_3
    //   1025: iconst_1
    //   1026: if_icmpeq -> 1091
    //   1029: iload_3
    //   1030: iconst_3
    //   1031: if_icmpne -> 1037
    //   1034: goto -> 1091
    //   1037: aload_0
    //   1038: aload_0
    //   1039: getfield mCurNumPointers : I
    //   1042: iconst_1
    //   1043: isub
    //   1044: putfield mCurNumPointers : I
    //   1047: aload_0
    //   1048: getfield mActivePointerId : I
    //   1051: iload #6
    //   1053: if_icmpne -> 1077
    //   1056: iload #9
    //   1058: ifne -> 1064
    //   1061: goto -> 1067
    //   1064: iconst_0
    //   1065: istore #4
    //   1067: aload_0
    //   1068: aload_1
    //   1069: iload #4
    //   1071: invokevirtual getPointerId : (I)I
    //   1074: putfield mActivePointerId : I
    //   1077: aload_2
    //   1078: ldc_w NaN
    //   1081: ldc_w NaN
    //   1084: iconst_0
    //   1085: invokevirtual addTrace : (FFZ)V
    //   1088: goto -> 1101
    //   1091: aload_0
    //   1092: iconst_0
    //   1093: putfield mCurDown : Z
    //   1096: aload_0
    //   1097: iconst_0
    //   1098: putfield mCurNumPointers : I
    //   1101: aload_0
    //   1102: invokevirtual invalidate : ()V
    //   1105: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #597	-> 0
    //   #599	-> 58
    //   #600	-> 63
    //   #602	-> 72
    //   #604	-> 91
    //   #606	-> 91
    //   #607	-> 95
    //   #608	-> 105
    //   #609	-> 118
    //   #610	-> 122
    //   #607	-> 128
    //   #612	-> 134
    //   #613	-> 139
    //   #614	-> 144
    //   #615	-> 149
    //   #616	-> 156
    //   #617	-> 165
    //   #621	-> 169
    //   #622	-> 183
    //   #623	-> 192
    //   #626	-> 198
    //   #627	-> 212
    //   #628	-> 219
    //   #629	-> 227
    //   #630	-> 236
    //   #631	-> 239
    //   #633	-> 242
    //   #636	-> 265
    //   #640	-> 280
    //   #643	-> 286
    //   #644	-> 299
    //   #645	-> 305
    //   #646	-> 314
    //   #647	-> 319
    //   #646	-> 338
    //   #650	-> 345
    //   #652	-> 351
    //   #653	-> 359
    //   #654	-> 367
    //   #655	-> 376
    //   #656	-> 381
    //   #659	-> 389
    //   #660	-> 395
    //   #661	-> 413
    //   #662	-> 435
    //   #667	-> 443
    //   #669	-> 475
    //   #670	-> 494
    //   #671	-> 504
    //   #672	-> 511
    //   #671	-> 528
    //   #674	-> 528
    //   #675	-> 532
    //   #661	-> 547
    //   #660	-> 553
    //   #679	-> 567
    //   #680	-> 577
    //   #685	-> 585
    //   #687	-> 617
    //   #688	-> 636
    //   #689	-> 644
    //   #690	-> 651
    //   #689	-> 668
    //   #692	-> 668
    //   #693	-> 672
    //   #694	-> 687
    //   #695	-> 701
    //   #696	-> 715
    //   #697	-> 729
    //   #698	-> 740
    //   #699	-> 752
    //   #700	-> 766
    //   #702	-> 780
    //   #704	-> 791
    //   #705	-> 798
    //   #706	-> 811
    //   #707	-> 824
    //   #708	-> 837
    //   #704	-> 853
    //   #692	-> 856
    //   #679	-> 856
    //   #714	-> 862
    //   #717	-> 874
    //   #720	-> 894
    //   #723	-> 904
    //   #724	-> 912
    //   #725	-> 919
    //   #727	-> 980
    //   #725	-> 994
    //   #728	-> 1001
    //   #730	-> 1002
    //   #731	-> 1015
    //   #733	-> 1021
    //   #738	-> 1037
    //   #739	-> 1047
    //   #740	-> 1056
    //   #742	-> 1077
    //   #733	-> 1091
    //   #735	-> 1091
    //   #736	-> 1096
    //   #746	-> 1101
    //   #747	-> 1105
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    onPointerEvent(paramMotionEvent);
    if (paramMotionEvent.getAction() == 0 && !isFocused())
      requestFocus(); 
    return true;
  }
  
  public boolean onGenericMotionEvent(MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.getSource();
    if ((i & 0x2) != 0) {
      onPointerEvent(paramMotionEvent);
    } else if ((i & 0x10) != 0) {
      logMotionEvent("Joystick", paramMotionEvent);
    } else if ((i & 0x8) != 0) {
      logMotionEvent("Position", paramMotionEvent);
    } else {
      logMotionEvent("Generic", paramMotionEvent);
    } 
    return true;
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
    if (shouldLogKey(paramInt)) {
      paramInt = paramKeyEvent.getRepeatCount();
      if (paramInt == 0) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Key Down: ");
        stringBuilder.append(paramKeyEvent);
        Log.i("Pointer", stringBuilder.toString());
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Key Repeat #");
        stringBuilder.append(paramInt);
        stringBuilder.append(": ");
        stringBuilder.append(paramKeyEvent);
        Log.i("Pointer", stringBuilder.toString());
      } 
      return true;
    } 
    return super.onKeyDown(paramInt, paramKeyEvent);
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent) {
    if (shouldLogKey(paramInt)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Key Up: ");
      stringBuilder.append(paramKeyEvent);
      Log.i("Pointer", stringBuilder.toString());
      return true;
    } 
    return super.onKeyUp(paramInt, paramKeyEvent);
  }
  
  private static boolean shouldLogKey(int paramInt) {
    null = true;
    switch (paramInt) {
      default:
        if (KeyEvent.isGamepadButton(paramInt) || KeyEvent.isModifierKey(paramInt))
          return null; 
        break;
      case 19:
      case 20:
      case 21:
      case 22:
      case 23:
        return true;
    } 
    return false;
  }
  
  public boolean onTrackballEvent(MotionEvent paramMotionEvent) {
    logMotionEvent("Trackball", paramMotionEvent);
    return true;
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    this.mIm.registerInputDeviceListener(this, getHandler());
    if (shouldShowSystemGestureExclusion()) {
      try {
        IWindowManager iWindowManager = WindowManagerGlobal.getWindowManagerService();
        ISystemGestureExclusionListener iSystemGestureExclusionListener = this.mSystemGestureExclusionListener;
        Context context = this.mContext;
        int i = context.getDisplayId();
        iWindowManager.registerSystemGestureExclusionListener(iSystemGestureExclusionListener, i);
        i = systemGestureExclusionOpacity();
        this.mSystemGestureExclusionPaint.setAlpha(i);
        this.mSystemGestureExclusionRejectedPaint.setAlpha(i);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } else {
      this.mSystemGestureExclusion.setEmpty();
    } 
    logInputDevices();
  }
  
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    this.mIm.unregisterInputDeviceListener(this);
    try {
      IWindowManager iWindowManager = WindowManagerGlobal.getWindowManagerService();
      ISystemGestureExclusionListener iSystemGestureExclusionListener = this.mSystemGestureExclusionListener;
      Context context = this.mContext;
      int i = context.getDisplayId();
      iWindowManager.unregisterSystemGestureExclusionListener(iSystemGestureExclusionListener, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void onInputDeviceAdded(int paramInt) {
    logInputDeviceState(paramInt, "Device Added");
  }
  
  public void onInputDeviceChanged(int paramInt) {
    logInputDeviceState(paramInt, "Device Changed");
  }
  
  public void onInputDeviceRemoved(int paramInt) {
    logInputDeviceState(paramInt, "Device Removed");
  }
  
  private void logInputDevices() {
    int[] arrayOfInt = InputDevice.getDeviceIds();
    for (byte b = 0; b < arrayOfInt.length; b++)
      logInputDeviceState(arrayOfInt[b], "Device Enumerated"); 
  }
  
  private void logInputDeviceState(int paramInt, String paramString) {
    InputDevice inputDevice = this.mIm.getInputDevice(paramInt);
    if (inputDevice != null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append(": ");
      stringBuilder.append(inputDevice);
      Log.i("Pointer", stringBuilder.toString());
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append(": ");
      stringBuilder.append(paramInt);
      Log.i("Pointer", stringBuilder.toString());
    } 
  }
  
  private static boolean shouldShowSystemGestureExclusion() {
    boolean bool;
    if (systemGestureExclusionOpacity() > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static int systemGestureExclusionOpacity() {
    byte b = 0;
    int i = SystemProperties.getInt("debug.pointerlocation.showexclusion", 0);
    int j = b;
    if (i >= 0) {
      j = b;
      if (i <= 255)
        j = i; 
    } 
    return j;
  }
  
  class FasterStringBuilder {
    private char[] mChars = new char[64];
    
    private int mLength;
    
    public FasterStringBuilder clear() {
      this.mLength = 0;
      return this;
    }
    
    public FasterStringBuilder append(String param1String) {
      int i = param1String.length();
      int j = reserve(i);
      param1String.getChars(0, i, this.mChars, j);
      this.mLength += i;
      return this;
    }
    
    public FasterStringBuilder append(int param1Int) {
      return append(param1Int, 0);
    }
    
    public FasterStringBuilder append(int param1Int1, int param1Int2) {
      int i, n, i1;
      if (param1Int1 < 0) {
        i = 1;
      } else {
        i = 0;
      } 
      int j = param1Int1;
      if (i) {
        param1Int1 = -param1Int1;
        j = param1Int1;
        if (param1Int1 < 0) {
          append("-2147483648");
          return this;
        } 
      } 
      param1Int1 = reserve(11);
      char[] arrayOfChar = this.mChars;
      if (j == 0) {
        arrayOfChar[param1Int1] = '0';
        this.mLength++;
        return this;
      } 
      int k = param1Int1;
      if (i) {
        arrayOfChar[param1Int1] = '-';
        k = param1Int1 + 1;
      } 
      param1Int1 = 1000000000;
      int m = 10;
      while (true) {
        n = param1Int1;
        i = k;
        i1 = j;
        if (j < param1Int1) {
          n = param1Int1 / 10;
          i = m - 1;
          param1Int1 = n;
          m = i;
          if (i < param1Int2) {
            arrayOfChar[k] = '0';
            k++;
            param1Int1 = n;
            m = i;
          } 
          continue;
        } 
        break;
      } 
      while (true) {
        param1Int2 = i1 / n;
        i1 -= param1Int2 * n;
        n /= 10;
        param1Int1 = i + 1;
        arrayOfChar[i] = (char)(param1Int2 + 48);
        if (n == 0) {
          this.mLength = param1Int1;
          return this;
        } 
        i = param1Int1;
      } 
    }
    
    public FasterStringBuilder append(float param1Float, int param1Int) {
      int i = 1;
      for (byte b = 0; b < param1Int; b++)
        i *= 10; 
      param1Float = (float)(Math.rint((i * param1Float)) / i);
      if ((int)param1Float == 0 && param1Float < 0.0F)
        append("-"); 
      append((int)param1Float);
      if (param1Int != 0) {
        append(".");
        param1Float = Math.abs(param1Float);
        param1Float = (float)(param1Float - Math.floor(param1Float));
        append((int)(i * param1Float), param1Int);
      } 
      return this;
    }
    
    public String toString() {
      return new String(this.mChars, 0, this.mLength);
    }
    
    private int reserve(int param1Int) {
      int i = this.mLength;
      int j = this.mLength;
      char[] arrayOfChar = this.mChars;
      int k = arrayOfChar.length;
      if (j + param1Int > k) {
        char[] arrayOfChar1 = new char[k * 2];
        System.arraycopy(arrayOfChar, 0, arrayOfChar1, 0, i);
        this.mChars = arrayOfChar1;
      } 
      return i;
    }
  }
}
