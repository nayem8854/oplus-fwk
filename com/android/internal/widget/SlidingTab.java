package com.android.internal.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.AudioAttributes;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.internal.R;

public class SlidingTab extends ViewGroup {
  private boolean mHoldLeftOnTransition = true;
  
  private boolean mHoldRightOnTransition = true;
  
  static {
    AudioAttributes.Builder builder = new AudioAttributes.Builder();
    builder = builder.setContentType(4);
    builder = builder.setUsage(13);
    VIBRATION_ATTRIBUTES = builder.build();
  }
  
  private int mGrabbedState = 0;
  
  private boolean mTriggered = false;
  
  private final Animation.AnimationListener mAnimationDoneListener = (Animation.AnimationListener)new Object(this);
  
  private static final int ANIM_DURATION = 250;
  
  private static final int ANIM_TARGET_TIME = 500;
  
  private static final boolean DBG = false;
  
  private static final int HORIZONTAL = 0;
  
  private static final String LOG_TAG = "SlidingTab";
  
  private static final float THRESHOLD = 0.6666667F;
  
  private static final int TRACKING_MARGIN = 50;
  
  private static final int VERTICAL = 1;
  
  private static final long VIBRATE_LONG = 40L;
  
  private static final long VIBRATE_SHORT = 30L;
  
  private static final AudioAttributes VIBRATION_ATTRIBUTES;
  
  private boolean mAnimating;
  
  private Slider mCurrentSlider;
  
  private final float mDensity;
  
  private final Slider mLeftSlider;
  
  private OnTriggerListener mOnTriggerListener;
  
  private final int mOrientation;
  
  private Slider mOtherSlider;
  
  private final Slider mRightSlider;
  
  private float mThreshold;
  
  private final Rect mTmpRect;
  
  private boolean mTracking;
  
  private Vibrator mVibrator;
  
  class OnTriggerListener {
    public static final int LEFT_HANDLE = 1;
    
    public static final int NO_HANDLE = 0;
    
    public static final int RIGHT_HANDLE = 2;
    
    public abstract void onGrabbedStateChange(View param1View, int param1Int);
    
    public abstract void onTrigger(View param1View, int param1Int);
  }
  
  class Slider {
    private final TextView text;
    
    private final ImageView target;
    
    private final ImageView tab;
    
    private int currentState = 0;
    
    private int alignment_value;
    
    private int alignment = 4;
    
    private static final int STATE_PRESSED = 1;
    
    private static final int STATE_NORMAL = 0;
    
    private static final int STATE_ACTIVE = 2;
    
    public static final int ALIGN_UNKNOWN = 4;
    
    public static final int ALIGN_TOP = 2;
    
    public static final int ALIGN_RIGHT = 1;
    
    public static final int ALIGN_LEFT = 0;
    
    public static final int ALIGN_BOTTOM = 3;
    
    Slider(SlidingTab this$0, int param1Int1, int param1Int2, int param1Int3) {
      ImageView imageView2 = new ImageView(this$0.getContext());
      imageView2.setBackgroundResource(param1Int1);
      this.tab.setScaleType(ImageView.ScaleType.CENTER);
      this.tab.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
      TextView textView = new TextView(this$0.getContext());
      textView.setLayoutParams(new ViewGroup.LayoutParams(-2, -1));
      this.text.setBackgroundResource(param1Int2);
      this.text.setTextAppearance(this$0.getContext(), 16974797);
      ImageView imageView1 = new ImageView(this$0.getContext());
      imageView1.setImageResource(param1Int3);
      this.target.setScaleType(ImageView.ScaleType.CENTER);
      this.target.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
      this.target.setVisibility(4);
      this$0.addView((View)this.target);
      this$0.addView((View)this.tab);
      this$0.addView((View)this.text);
    }
    
    void setIcon(int param1Int) {
      this.tab.setImageResource(param1Int);
    }
    
    void setTabBackgroundResource(int param1Int) {
      this.tab.setBackgroundResource(param1Int);
    }
    
    void setBarBackgroundResource(int param1Int) {
      this.text.setBackgroundResource(param1Int);
    }
    
    void setHintText(int param1Int) {
      this.text.setText(param1Int);
    }
    
    void hide() {
      int j, i = this.alignment;
      byte b = 0;
      if (i == 0 || i == 1) {
        j = 1;
      } else {
        j = 0;
      } 
      if (j) {
        if (this.alignment == 0) {
          i = this.alignment_value - this.tab.getRight();
        } else {
          i = this.alignment_value - this.tab.getLeft();
        } 
      } else {
        i = 0;
      } 
      if (j) {
        j = b;
      } else if (this.alignment == 2) {
        j = this.alignment_value - this.tab.getBottom();
      } else {
        j = this.alignment_value - this.tab.getTop();
      } 
      TranslateAnimation translateAnimation = new TranslateAnimation(0.0F, i, 0.0F, j);
      translateAnimation.setDuration(250L);
      translateAnimation.setFillAfter(true);
      this.tab.startAnimation((Animation)translateAnimation);
      this.text.startAnimation((Animation)translateAnimation);
      this.target.setVisibility(4);
    }
    
    void show(boolean param1Boolean) {
      TextView textView = this.text;
      boolean bool = false;
      textView.setVisibility(0);
      this.tab.setVisibility(0);
      if (param1Boolean) {
        int i = this.alignment, j = 1, k = j;
        if (i != 0)
          if (i == 1) {
            k = j;
          } else {
            k = 0;
          }  
        if (k) {
          if (this.alignment == 0) {
            j = this.tab.getWidth();
          } else {
            j = -this.tab.getWidth();
          } 
        } else {
          j = 0;
        } 
        if (k != 0) {
          k = bool;
        } else if (this.alignment == 2) {
          k = this.tab.getHeight();
        } else {
          k = -this.tab.getHeight();
        } 
        TranslateAnimation translateAnimation = new TranslateAnimation(-j, 0.0F, -k, 0.0F);
        translateAnimation.setDuration(250L);
        this.tab.startAnimation((Animation)translateAnimation);
        this.text.startAnimation((Animation)translateAnimation);
      } 
    }
    
    void setState(int param1Int) {
      boolean bool;
      TextView textView = this.text;
      if (param1Int == 1) {
        bool = true;
      } else {
        bool = false;
      } 
      textView.setPressed(bool);
      ImageView imageView = this.tab;
      if (param1Int == 1) {
        bool = true;
      } else {
        bool = false;
      } 
      imageView.setPressed(bool);
      if (param1Int == 2) {
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = 16842914;
        if (this.text.getBackground().isStateful())
          this.text.getBackground().setState(arrayOfInt); 
        if (this.tab.getBackground().isStateful())
          this.tab.getBackground().setState(arrayOfInt); 
        TextView textView1 = this.text;
        textView1.setTextAppearance(textView1.getContext(), 16974796);
      } else {
        TextView textView1 = this.text;
        textView1.setTextAppearance(textView1.getContext(), 16974797);
      } 
      this.currentState = param1Int;
    }
    
    void showTarget() {
      AlphaAnimation alphaAnimation = new AlphaAnimation(0.0F, 1.0F);
      alphaAnimation.setDuration(500L);
      this.target.startAnimation((Animation)alphaAnimation);
      this.target.setVisibility(0);
    }
    
    void reset(boolean param1Boolean) {
      setState(0);
      this.text.setVisibility(0);
      TextView textView = this.text;
      textView.setTextAppearance(textView.getContext(), 16974797);
      this.tab.setVisibility(0);
      this.target.setVisibility(4);
      int i = this.alignment, j = 1, k = j;
      if (i != 0)
        if (i == 1) {
          k = j;
        } else {
          k = 0;
        }  
      if (k) {
        if (this.alignment == 0) {
          j = this.alignment_value - this.tab.getLeft();
        } else {
          j = this.alignment_value - this.tab.getRight();
        } 
      } else {
        j = 0;
      } 
      if (k != 0) {
        i = 0;
      } else if (this.alignment == 2) {
        i = this.alignment_value - this.tab.getTop();
      } else {
        i = this.alignment_value - this.tab.getBottom();
      } 
      if (param1Boolean) {
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0F, j, 0.0F, i);
        translateAnimation.setDuration(250L);
        translateAnimation.setFillAfter(false);
        this.text.startAnimation((Animation)translateAnimation);
        this.tab.startAnimation((Animation)translateAnimation);
      } else {
        if (k != 0) {
          this.text.offsetLeftAndRight(j);
          this.tab.offsetLeftAndRight(j);
        } else {
          this.text.offsetTopAndBottom(i);
          this.tab.offsetTopAndBottom(i);
        } 
        this.text.clearAnimation();
        this.tab.clearAnimation();
        this.target.clearAnimation();
      } 
    }
    
    void setTarget(int param1Int) {
      this.target.setImageResource(param1Int);
    }
    
    void layout(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5) {
      this.alignment = param1Int5;
      Drawable drawable = this.tab.getBackground();
      int i = drawable.getIntrinsicWidth();
      int j = drawable.getIntrinsicHeight();
      drawable = this.target.getDrawable();
      int k = drawable.getIntrinsicWidth();
      int m = drawable.getIntrinsicHeight();
      int n = param1Int3 - param1Int1;
      int i1 = param1Int4 - param1Int2;
      int i2 = (int)(n * 0.6666667F) - k + i / 2;
      int i3 = (int)(n * 0.3333333F) - i / 2;
      int i4 = (n - i) / 2;
      int i5 = i4 + i;
      if (param1Int5 == 0 || param1Int5 == 1) {
        param1Int2 = (i1 - m) / 2;
        m = param1Int2 + m;
        param1Int4 = (i1 - j) / 2;
        i1 = (i1 + j) / 2;
        if (param1Int5 == 0) {
          this.tab.layout(0, param1Int4, i, i1);
          this.text.layout(0 - n, param1Int4, 0, i1);
          this.text.setGravity(5);
          this.target.layout(i2, param1Int2, i2 + k, m);
          this.alignment_value = param1Int1;
        } else {
          this.tab.layout(n - i, param1Int4, n, i1);
          this.text.layout(n, param1Int4, n + n, i1);
          this.target.layout(i3, param1Int2, i3 + k, m);
          this.text.setGravity(48);
          this.alignment_value = param1Int3;
        } 
        return;
      } 
      param1Int1 = (n - k) / 2;
      n = (n + k) / 2;
      k = (int)(i1 * 0.6666667F) + j / 2 - m;
      param1Int3 = (int)(i1 * 0.3333333F) - j / 2;
      if (param1Int5 == 2) {
        this.tab.layout(i4, 0, i5, j);
        this.text.layout(i4, 0 - i1, i5, 0);
        this.target.layout(param1Int1, k, n, k + m);
        this.alignment_value = param1Int2;
      } else {
        this.tab.layout(i4, i1 - j, i5, i1);
        this.text.layout(i4, i1, i5, i1 + i1);
        this.target.layout(param1Int1, param1Int3, n, param1Int3 + m);
        this.alignment_value = param1Int4;
      } 
    }
    
    public void updateDrawableStates() {
      setState(this.currentState);
    }
    
    public void measure(int param1Int1, int param1Int2) {
      int i = View.MeasureSpec.getSize(param1Int1);
      param1Int1 = View.MeasureSpec.getSize(param1Int2);
      ImageView imageView = this.tab;
      param1Int2 = View.MeasureSpec.makeSafeMeasureSpec(i, 0);
      int j = View.MeasureSpec.makeSafeMeasureSpec(param1Int1, 0);
      imageView.measure(param1Int2, j);
      TextView textView = this.text;
      param1Int2 = View.MeasureSpec.makeSafeMeasureSpec(i, 0);
      param1Int1 = View.MeasureSpec.makeSafeMeasureSpec(param1Int1, 0);
      textView.measure(param1Int2, param1Int1);
    }
    
    public int getTabWidth() {
      return this.tab.getMeasuredWidth();
    }
    
    public int getTabHeight() {
      return this.tab.getMeasuredHeight();
    }
    
    public void startAnimation(Animation param1Animation1, Animation param1Animation2) {
      this.tab.startAnimation(param1Animation1);
      this.text.startAnimation(param1Animation2);
    }
    
    public void hideTarget() {
      this.target.clearAnimation();
      this.target.setVisibility(4);
    }
  }
  
  public SlidingTab(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public SlidingTab(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    this.mTmpRect = new Rect();
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.SlidingTab);
    this.mOrientation = typedArray.getInt(0, 0);
    typedArray.recycle();
    Resources resources = getResources();
    this.mDensity = (resources.getDisplayMetrics()).density;
    this.mLeftSlider = new Slider(this, 17302952, 17302935, 17302966);
    this.mRightSlider = new Slider(this, 17302961, 17302944, 17302966);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    View.MeasureSpec.getMode(paramInt1);
    int i = View.MeasureSpec.getSize(paramInt1);
    View.MeasureSpec.getMode(paramInt2);
    int j = View.MeasureSpec.getSize(paramInt2);
    this.mLeftSlider.measure(paramInt1, paramInt2);
    this.mRightSlider.measure(paramInt1, paramInt2);
    int k = this.mLeftSlider.getTabWidth();
    paramInt2 = this.mRightSlider.getTabWidth();
    int m = this.mLeftSlider.getTabHeight();
    paramInt1 = this.mRightSlider.getTabHeight();
    if (isHorizontal()) {
      paramInt2 = Math.max(i, k + paramInt2);
      paramInt1 = Math.max(m, paramInt1);
    } else {
      paramInt2 = Math.max(k, paramInt1);
      paramInt1 = Math.max(j, m + paramInt1);
    } 
    setMeasuredDimension(paramInt2, paramInt1);
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.getAction();
    float f1 = paramMotionEvent.getX();
    float f2 = paramMotionEvent.getY();
    if (this.mAnimating)
      return false; 
    ImageView imageView = this.mLeftSlider.tab;
    imageView.getHitRect(this.mTmpRect);
    boolean bool1 = this.mTmpRect.contains((int)f1, (int)f2);
    imageView = this.mRightSlider.tab;
    imageView.getHitRect(this.mTmpRect);
    boolean bool2 = this.mTmpRect.contains((int)f1, (int)f2);
    if (!this.mTracking && !bool1 && !bool2)
      return false; 
    if (i == 0) {
      this.mTracking = true;
      this.mTriggered = false;
      vibrate(30L);
      f1 = 0.3333333F;
      if (bool1) {
        this.mCurrentSlider = this.mLeftSlider;
        this.mOtherSlider = this.mRightSlider;
        if (isHorizontal())
          f1 = 0.6666667F; 
        this.mThreshold = f1;
        setGrabbedState(1);
      } else {
        this.mCurrentSlider = this.mRightSlider;
        this.mOtherSlider = this.mLeftSlider;
        if (!isHorizontal())
          f1 = 0.6666667F; 
        this.mThreshold = f1;
        setGrabbedState(2);
      } 
      this.mCurrentSlider.setState(1);
      this.mCurrentSlider.showTarget();
      this.mOtherSlider.hide();
    } 
    return true;
  }
  
  public void reset(boolean paramBoolean) {
    this.mLeftSlider.reset(paramBoolean);
    this.mRightSlider.reset(paramBoolean);
    if (!paramBoolean)
      this.mAnimating = false; 
  }
  
  public void setVisibility(int paramInt) {
    if (paramInt != getVisibility() && paramInt == 4)
      reset(false); 
    super.setVisibility(paramInt);
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mTracking : Z
    //   4: istore_2
    //   5: iconst_0
    //   6: istore_3
    //   7: iload_2
    //   8: ifeq -> 313
    //   11: aload_1
    //   12: invokevirtual getAction : ()I
    //   15: istore #4
    //   17: aload_1
    //   18: invokevirtual getX : ()F
    //   21: fstore #5
    //   23: aload_1
    //   24: invokevirtual getY : ()F
    //   27: fstore #6
    //   29: iload #4
    //   31: iconst_1
    //   32: if_icmpeq -> 309
    //   35: iconst_2
    //   36: istore #7
    //   38: iload #4
    //   40: iconst_2
    //   41: if_icmpeq -> 53
    //   44: iload #4
    //   46: iconst_3
    //   47: if_icmpeq -> 309
    //   50: goto -> 313
    //   53: aload_0
    //   54: fload #5
    //   56: fload #6
    //   58: aload_0
    //   59: invokespecial withinView : (FFLandroid/view/View;)Z
    //   62: ifeq -> 309
    //   65: aload_0
    //   66: fload #5
    //   68: fload #6
    //   70: invokespecial moveHandle : (FF)V
    //   73: aload_0
    //   74: invokespecial isHorizontal : ()Z
    //   77: ifeq -> 87
    //   80: fload #5
    //   82: fstore #6
    //   84: goto -> 87
    //   87: aload_0
    //   88: getfield mThreshold : F
    //   91: fstore #5
    //   93: aload_0
    //   94: invokespecial isHorizontal : ()Z
    //   97: ifeq -> 109
    //   100: aload_0
    //   101: invokevirtual getWidth : ()I
    //   104: istore #4
    //   106: goto -> 115
    //   109: aload_0
    //   110: invokevirtual getHeight : ()I
    //   113: istore #4
    //   115: fload #5
    //   117: iload #4
    //   119: i2f
    //   120: fmul
    //   121: fstore #5
    //   123: aload_0
    //   124: invokespecial isHorizontal : ()Z
    //   127: ifeq -> 172
    //   130: aload_0
    //   131: getfield mCurrentSlider : Lcom/android/internal/widget/SlidingTab$Slider;
    //   134: aload_0
    //   135: getfield mLeftSlider : Lcom/android/internal/widget/SlidingTab$Slider;
    //   138: if_acmpne -> 152
    //   141: fload #6
    //   143: fload #5
    //   145: fcmpl
    //   146: ifle -> 166
    //   149: goto -> 160
    //   152: fload #6
    //   154: fload #5
    //   156: fcmpg
    //   157: ifge -> 166
    //   160: iconst_1
    //   161: istore #4
    //   163: goto -> 169
    //   166: iconst_0
    //   167: istore #4
    //   169: goto -> 211
    //   172: aload_0
    //   173: getfield mCurrentSlider : Lcom/android/internal/widget/SlidingTab$Slider;
    //   176: aload_0
    //   177: getfield mLeftSlider : Lcom/android/internal/widget/SlidingTab$Slider;
    //   180: if_acmpne -> 194
    //   183: fload #6
    //   185: fload #5
    //   187: fcmpg
    //   188: ifge -> 208
    //   191: goto -> 202
    //   194: fload #6
    //   196: fload #5
    //   198: fcmpl
    //   199: ifle -> 208
    //   202: iconst_1
    //   203: istore #4
    //   205: goto -> 211
    //   208: iconst_0
    //   209: istore #4
    //   211: aload_0
    //   212: getfield mTriggered : Z
    //   215: ifne -> 313
    //   218: iload #4
    //   220: ifeq -> 313
    //   223: aload_0
    //   224: iconst_1
    //   225: putfield mTriggered : Z
    //   228: aload_0
    //   229: iconst_0
    //   230: putfield mTracking : Z
    //   233: aload_0
    //   234: getfield mCurrentSlider : Lcom/android/internal/widget/SlidingTab$Slider;
    //   237: iconst_2
    //   238: invokevirtual setState : (I)V
    //   241: aload_0
    //   242: getfield mCurrentSlider : Lcom/android/internal/widget/SlidingTab$Slider;
    //   245: aload_0
    //   246: getfield mLeftSlider : Lcom/android/internal/widget/SlidingTab$Slider;
    //   249: if_acmpne -> 258
    //   252: iconst_1
    //   253: istore #4
    //   255: goto -> 261
    //   258: iconst_0
    //   259: istore #4
    //   261: iload #4
    //   263: ifeq -> 272
    //   266: iconst_1
    //   267: istore #7
    //   269: goto -> 272
    //   272: aload_0
    //   273: iload #7
    //   275: invokespecial dispatchTriggerEvent : (I)V
    //   278: iload #4
    //   280: ifeq -> 291
    //   283: aload_0
    //   284: getfield mHoldLeftOnTransition : Z
    //   287: istore_2
    //   288: goto -> 296
    //   291: aload_0
    //   292: getfield mHoldRightOnTransition : Z
    //   295: istore_2
    //   296: aload_0
    //   297: iload_2
    //   298: invokevirtual startAnimating : (Z)V
    //   301: aload_0
    //   302: iconst_0
    //   303: invokespecial setGrabbedState : (I)V
    //   306: goto -> 313
    //   309: aload_0
    //   310: invokespecial cancelGrab : ()V
    //   313: aload_0
    //   314: getfield mTracking : Z
    //   317: ifne -> 330
    //   320: iload_3
    //   321: istore_2
    //   322: aload_0
    //   323: aload_1
    //   324: invokespecial onTouchEvent : (Landroid/view/MotionEvent;)Z
    //   327: ifeq -> 332
    //   330: iconst_1
    //   331: istore_2
    //   332: iload_2
    //   333: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #593	-> 0
    //   #594	-> 11
    //   #595	-> 17
    //   #596	-> 23
    //   #598	-> 29
    //   #600	-> 53
    //   #601	-> 65
    //   #602	-> 73
    //   #603	-> 87
    //   #605	-> 123
    //   #606	-> 130
    //   #607	-> 141
    //   #609	-> 172
    //   #610	-> 183
    //   #612	-> 211
    //   #613	-> 223
    //   #614	-> 228
    //   #615	-> 233
    //   #616	-> 241
    //   #617	-> 261
    //   #618	-> 266
    //   #617	-> 272
    //   #620	-> 278
    //   #621	-> 301
    //   #622	-> 306
    //   #629	-> 309
    //   #634	-> 313
  }
  
  private void cancelGrab() {
    this.mTracking = false;
    this.mTriggered = false;
    this.mOtherSlider.show(true);
    this.mCurrentSlider.reset(false);
    this.mCurrentSlider.hideTarget();
    this.mCurrentSlider = null;
    this.mOtherSlider = null;
    setGrabbedState(0);
  }
  
  void startAnimating(boolean paramBoolean) {
    boolean bool1;
    this.mAnimating = true;
    Slider slider1 = this.mCurrentSlider;
    Slider slider2 = this.mOtherSlider;
    boolean bool = isHorizontal();
    int i = 0, j = 0;
    if (bool) {
      int k = slider1.tab.getRight();
      i = slider1.tab.getWidth();
      bool1 = slider1.tab.getLeft();
      int m = getWidth();
      if (!paramBoolean)
        j = i; 
      if (slider1 == this.mRightSlider) {
        j = -(k + m - j);
      } else {
        j = m - bool1 + m - j;
      } 
      i = 0;
      bool1 = j;
    } else {
      int k = slider1.tab.getTop();
      int m = slider1.tab.getBottom();
      j = slider1.tab.getHeight();
      int n = getHeight();
      if (paramBoolean)
        j = i; 
      bool1 = false;
      if (slider1 == this.mRightSlider) {
        j = k + n - j;
      } else {
        j = -(n - m + n - j);
      } 
      i = j;
    } 
    TranslateAnimation translateAnimation2 = new TranslateAnimation(0.0F, bool1, 0.0F, i);
    translateAnimation2.setDuration(250L);
    translateAnimation2.setInterpolator((Interpolator)new LinearInterpolator());
    translateAnimation2.setFillAfter(true);
    TranslateAnimation translateAnimation1 = new TranslateAnimation(0.0F, bool1, 0.0F, i);
    translateAnimation1.setDuration(250L);
    translateAnimation1.setInterpolator((Interpolator)new LinearInterpolator());
    translateAnimation1.setFillAfter(true);
    translateAnimation2.setAnimationListener((Animation.AnimationListener)new Object(this, paramBoolean, bool1, i));
    slider1.hideTarget();
    slider1.startAnimation((Animation)translateAnimation2, (Animation)translateAnimation1);
  }
  
  private void onAnimationDone() {
    resetView();
    this.mAnimating = false;
  }
  
  private boolean withinView(float paramFloat1, float paramFloat2, View paramView) {
    boolean bool;
    if ((isHorizontal() && paramFloat2 > -50.0F && paramFloat2 < (paramView.getHeight() + 50)) || (
      !isHorizontal() && paramFloat1 > -50.0F && paramFloat1 < (paramView.getWidth() + 50))) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isHorizontal() {
    boolean bool;
    if (this.mOrientation == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void resetView() {
    this.mLeftSlider.reset(false);
    this.mRightSlider.reset(false);
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    byte b;
    if (!paramBoolean)
      return; 
    Slider slider = this.mLeftSlider;
    if (isHorizontal()) {
      b = 0;
    } else {
      b = 3;
    } 
    slider.layout(paramInt1, paramInt2, paramInt3, paramInt4, b);
    slider = this.mRightSlider;
    if (isHorizontal()) {
      b = 1;
    } else {
      b = 2;
    } 
    slider.layout(paramInt1, paramInt2, paramInt3, paramInt4, b);
  }
  
  private void moveHandle(float paramFloat1, float paramFloat2) {
    ImageView imageView = this.mCurrentSlider.tab;
    TextView textView = this.mCurrentSlider.text;
    if (isHorizontal()) {
      int i = (int)paramFloat1 - imageView.getLeft() - imageView.getWidth() / 2;
      imageView.offsetLeftAndRight(i);
      textView.offsetLeftAndRight(i);
    } else {
      int i = (int)paramFloat2 - imageView.getTop() - imageView.getHeight() / 2;
      imageView.offsetTopAndBottom(i);
      textView.offsetTopAndBottom(i);
    } 
    invalidate();
  }
  
  public void setLeftTabResources(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.mLeftSlider.setIcon(paramInt1);
    this.mLeftSlider.setTarget(paramInt2);
    this.mLeftSlider.setBarBackgroundResource(paramInt3);
    this.mLeftSlider.setTabBackgroundResource(paramInt4);
    this.mLeftSlider.updateDrawableStates();
  }
  
  public void setLeftHintText(int paramInt) {
    if (isHorizontal())
      this.mLeftSlider.setHintText(paramInt); 
  }
  
  public void setRightTabResources(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.mRightSlider.setIcon(paramInt1);
    this.mRightSlider.setTarget(paramInt2);
    this.mRightSlider.setBarBackgroundResource(paramInt3);
    this.mRightSlider.setTabBackgroundResource(paramInt4);
    this.mRightSlider.updateDrawableStates();
  }
  
  public void setRightHintText(int paramInt) {
    if (isHorizontal())
      this.mRightSlider.setHintText(paramInt); 
  }
  
  public void setHoldAfterTrigger(boolean paramBoolean1, boolean paramBoolean2) {
    this.mHoldLeftOnTransition = paramBoolean1;
    this.mHoldRightOnTransition = paramBoolean2;
  }
  
  private void vibrate(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mContext : Landroid/content/Context;
    //   6: astore_3
    //   7: aload_3
    //   8: invokevirtual getContentResolver : ()Landroid/content/ContentResolver;
    //   11: astore_3
    //   12: iconst_1
    //   13: istore #4
    //   15: aload_3
    //   16: ldc_w 'haptic_feedback_enabled'
    //   19: iconst_1
    //   20: bipush #-2
    //   22: invokestatic getIntForUser : (Landroid/content/ContentResolver;Ljava/lang/String;II)I
    //   25: ifeq -> 31
    //   28: goto -> 34
    //   31: iconst_0
    //   32: istore #4
    //   34: iload #4
    //   36: ifeq -> 76
    //   39: aload_0
    //   40: getfield mVibrator : Landroid/os/Vibrator;
    //   43: ifnonnull -> 65
    //   46: aload_0
    //   47: invokevirtual getContext : ()Landroid/content/Context;
    //   50: astore_3
    //   51: aload_0
    //   52: aload_3
    //   53: ldc_w 'vibrator'
    //   56: invokevirtual getSystemService : (Ljava/lang/String;)Ljava/lang/Object;
    //   59: checkcast android/os/Vibrator
    //   62: putfield mVibrator : Landroid/os/Vibrator;
    //   65: aload_0
    //   66: getfield mVibrator : Landroid/os/Vibrator;
    //   69: lload_1
    //   70: getstatic com/android/internal/widget/SlidingTab.VIBRATION_ATTRIBUTES : Landroid/media/AudioAttributes;
    //   73: invokevirtual vibrate : (JLandroid/media/AudioAttributes;)V
    //   76: aload_0
    //   77: monitorexit
    //   78: return
    //   79: astore_3
    //   80: aload_0
    //   81: monitorexit
    //   82: aload_3
    //   83: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #837	-> 2
    //   #838	-> 7
    //   #837	-> 12
    //   #840	-> 34
    //   #841	-> 39
    //   #842	-> 46
    //   #843	-> 51
    //   #845	-> 65
    //   #847	-> 76
    //   #836	-> 79
    // Exception table:
    //   from	to	target	type
    //   2	7	79	finally
    //   7	12	79	finally
    //   15	28	79	finally
    //   39	46	79	finally
    //   46	51	79	finally
    //   51	65	79	finally
    //   65	76	79	finally
  }
  
  public void setOnTriggerListener(OnTriggerListener paramOnTriggerListener) {
    this.mOnTriggerListener = paramOnTriggerListener;
  }
  
  private void dispatchTriggerEvent(int paramInt) {
    vibrate(40L);
    OnTriggerListener onTriggerListener = this.mOnTriggerListener;
    if (onTriggerListener != null)
      onTriggerListener.onTrigger((View)this, paramInt); 
  }
  
  protected void onVisibilityChanged(View paramView, int paramInt) {
    super.onVisibilityChanged(paramView, paramInt);
    if (paramView == this && paramInt != 0 && this.mGrabbedState != 0)
      cancelGrab(); 
  }
  
  private void setGrabbedState(int paramInt) {
    if (paramInt != this.mGrabbedState) {
      this.mGrabbedState = paramInt;
      OnTriggerListener onTriggerListener = this.mOnTriggerListener;
      if (onTriggerListener != null)
        onTriggerListener.onGrabbedStateChange((View)this, paramInt); 
    } 
  }
  
  private void log(String paramString) {
    Log.d("SlidingTab", paramString);
  }
}
