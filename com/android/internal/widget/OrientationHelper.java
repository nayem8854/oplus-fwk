package com.android.internal.widget;

import android.graphics.Rect;
import android.view.View;

public abstract class OrientationHelper {
  public static final int HORIZONTAL = 0;
  
  private static final int INVALID_SIZE = -2147483648;
  
  public static final int VERTICAL = 1;
  
  private int mLastTotalSpace = Integer.MIN_VALUE;
  
  protected final RecyclerView.LayoutManager mLayoutManager;
  
  final Rect mTmpRect = new Rect();
  
  private OrientationHelper(RecyclerView.LayoutManager paramLayoutManager) {
    this.mLayoutManager = paramLayoutManager;
  }
  
  public void onLayoutComplete() {
    this.mLastTotalSpace = getTotalSpace();
  }
  
  public int getTotalSpaceChange() {
    int i;
    if (Integer.MIN_VALUE == this.mLastTotalSpace) {
      i = 0;
    } else {
      i = getTotalSpace() - this.mLastTotalSpace;
    } 
    return i;
  }
  
  public static OrientationHelper createOrientationHelper(RecyclerView.LayoutManager paramLayoutManager, int paramInt) {
    if (paramInt != 0) {
      if (paramInt == 1)
        return createVerticalHelper(paramLayoutManager); 
      throw new IllegalArgumentException("invalid orientation");
    } 
    return createHorizontalHelper(paramLayoutManager);
  }
  
  public static OrientationHelper createHorizontalHelper(RecyclerView.LayoutManager paramLayoutManager) {
    return (OrientationHelper)new Object(paramLayoutManager);
  }
  
  public static OrientationHelper createVerticalHelper(RecyclerView.LayoutManager paramLayoutManager) {
    return (OrientationHelper)new Object(paramLayoutManager);
  }
  
  public abstract int getDecoratedEnd(View paramView);
  
  public abstract int getDecoratedMeasurement(View paramView);
  
  public abstract int getDecoratedMeasurementInOther(View paramView);
  
  public abstract int getDecoratedStart(View paramView);
  
  public abstract int getEnd();
  
  public abstract int getEndAfterPadding();
  
  public abstract int getEndPadding();
  
  public abstract int getMode();
  
  public abstract int getModeInOther();
  
  public abstract int getStartAfterPadding();
  
  public abstract int getTotalSpace();
  
  public abstract int getTransformedEndWithDecoration(View paramView);
  
  public abstract int getTransformedStartWithDecoration(View paramView);
  
  public abstract void offsetChild(View paramView, int paramInt);
  
  public abstract void offsetChildren(int paramInt);
}
