package com.android.internal.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.metrics.LogMaker;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.OverScroller;
import android.widget.ScrollView;
import com.android.internal.R;
import com.android.internal.logging.MetricsLogger;

public class ResolverDrawerLayout extends ViewGroup {
  private float mDragRemainder = 0.0F;
  
  private int mActivePointerId = -1;
  
  private final Rect mTempRect = new Rect();
  
  private final ViewTreeObserver.OnTouchModeChangeListener mTouchModeChangeListener = (ViewTreeObserver.OnTouchModeChangeListener)new Object(this);
  
  private static final String TAG = "ResolverDrawerLayout";
  
  private int mAlwaysShowHeight;
  
  private float mCollapseOffset;
  
  private int mCollapsibleHeight;
  
  private int mCollapsibleHeightReserved;
  
  private boolean mDismissLocked;
  
  private boolean mDismissOnScrollerFinished;
  
  private float mInitialTouchX;
  
  private float mInitialTouchY;
  
  private boolean mIsDragging;
  
  private final boolean mIsMaxCollapsedHeightSmallExplicit;
  
  private float mLastTouchY;
  
  private int mMaxCollapsedHeight;
  
  private int mMaxCollapsedHeightSmall;
  
  private int mMaxWidth;
  
  private MetricsLogger mMetricsLogger;
  
  private final float mMinFlingVelocity;
  
  private AbsListView mNestedListChild;
  
  private RecyclerView mNestedRecyclerChild;
  
  private ScrollView mNestedScrollChild;
  
  private OnCollapsedChangedListener mOnCollapsedChangedListener;
  
  private OnDismissedListener mOnDismissedListener;
  
  private boolean mOpenOnClick;
  
  private boolean mOpenOnLayout;
  
  private RunOnDismissedListener mRunOnDismissedListener;
  
  private Drawable mScrollIndicatorDrawable;
  
  private final OverScroller mScroller;
  
  private boolean mShowAtTop;
  
  private boolean mSmallCollapsed;
  
  private int mTopOffset;
  
  private final int mTouchSlop;
  
  private int mUncollapsibleHeight;
  
  private final VelocityTracker mVelocityTracker;
  
  public ResolverDrawerLayout(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public ResolverDrawerLayout(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public ResolverDrawerLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ResolverDrawerLayout, paramInt, 0);
    this.mMaxWidth = typedArray.getDimensionPixelSize(0, -1);
    this.mMaxCollapsedHeight = paramInt = typedArray.getDimensionPixelSize(1, 0);
    this.mMaxCollapsedHeightSmall = typedArray.getDimensionPixelSize(2, paramInt);
    this.mIsMaxCollapsedHeightSmallExplicit = typedArray.hasValue(2);
    this.mShowAtTop = typedArray.getBoolean(3, false);
    typedArray.recycle();
    this.mScrollIndicatorDrawable = this.mContext.getDrawable(17303434);
    this.mScroller = new OverScroller(paramContext, AnimationUtils.loadInterpolator(paramContext, 17563653));
    this.mVelocityTracker = VelocityTracker.obtain();
    ViewConfiguration viewConfiguration = ViewConfiguration.get(paramContext);
    this.mTouchSlop = viewConfiguration.getScaledTouchSlop();
    this.mMinFlingVelocity = viewConfiguration.getScaledMinimumFlingVelocity();
    setImportantForAccessibility(1);
  }
  
  public void setMaxCollapsedHeight(int paramInt) {
    if (paramInt == this.mMaxCollapsedHeight)
      return; 
    this.mMaxCollapsedHeight = paramInt;
    if (!this.mIsMaxCollapsedHeightSmallExplicit)
      this.mMaxCollapsedHeightSmall = paramInt; 
    requestLayout();
  }
  
  public void setSmallCollapsed(boolean paramBoolean) {
    this.mSmallCollapsed = paramBoolean;
    requestLayout();
  }
  
  public boolean isSmallCollapsed() {
    return this.mSmallCollapsed;
  }
  
  public boolean isCollapsed() {
    boolean bool;
    if (this.mCollapseOffset > 0.0F) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setShowAtTop(boolean paramBoolean) {
    this.mShowAtTop = paramBoolean;
    invalidate();
    requestLayout();
  }
  
  public boolean getShowAtTop() {
    return this.mShowAtTop;
  }
  
  public void setCollapsed(boolean paramBoolean) {
    if (!isLaidOut()) {
      this.mOpenOnLayout = paramBoolean ^ true;
    } else {
      boolean bool;
      if (paramBoolean) {
        bool = this.mCollapsibleHeight;
      } else {
        bool = false;
      } 
      smoothScrollTo(bool, 0.0F);
    } 
  }
  
  public void setCollapsibleHeightReserved(int paramInt) {
    int i = this.mCollapsibleHeightReserved;
    this.mCollapsibleHeightReserved = paramInt;
    paramInt -= i;
    if (paramInt != 0 && this.mIsDragging)
      this.mLastTouchY -= paramInt; 
    paramInt = this.mCollapsibleHeight;
    this.mCollapsibleHeight = Math.min(this.mCollapsibleHeight, getMaxCollapsedHeight());
    if (updateCollapseOffset(paramInt, isDragging() ^ true))
      return; 
    invalidate();
  }
  
  public void setDismissLocked(boolean paramBoolean) {
    this.mDismissLocked = paramBoolean;
  }
  
  private boolean isMoving() {
    return (this.mIsDragging || !this.mScroller.isFinished());
  }
  
  private boolean isDragging() {
    return (this.mIsDragging || getNestedScrollAxes() == 2);
  }
  
  private boolean updateCollapseOffset(int paramInt, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mCollapsibleHeight : I
    //   4: istore_3
    //   5: iconst_0
    //   6: istore #4
    //   8: iload_1
    //   9: iload_3
    //   10: if_icmpne -> 15
    //   13: iconst_0
    //   14: ireturn
    //   15: aload_0
    //   16: invokevirtual getShowAtTop : ()Z
    //   19: istore #5
    //   21: fconst_0
    //   22: fstore #6
    //   24: iload #5
    //   26: ifeq -> 36
    //   29: aload_0
    //   30: fconst_0
    //   31: putfield mCollapseOffset : F
    //   34: iconst_0
    //   35: ireturn
    //   36: aload_0
    //   37: invokevirtual isLaidOut : ()Z
    //   40: ifeq -> 138
    //   43: aload_0
    //   44: getfield mCollapseOffset : F
    //   47: fconst_0
    //   48: fcmpl
    //   49: ifeq -> 58
    //   52: iconst_1
    //   53: istore #5
    //   55: goto -> 61
    //   58: iconst_0
    //   59: istore #5
    //   61: iload_2
    //   62: ifeq -> 94
    //   65: aload_0
    //   66: getfield mCollapsibleHeight : I
    //   69: istore_3
    //   70: iload_1
    //   71: iload_3
    //   72: if_icmpge -> 94
    //   75: aload_0
    //   76: getfield mCollapseOffset : F
    //   79: iload_1
    //   80: i2f
    //   81: fcmpl
    //   82: ifne -> 94
    //   85: aload_0
    //   86: iload_3
    //   87: i2f
    //   88: putfield mCollapseOffset : F
    //   91: goto -> 110
    //   94: aload_0
    //   95: aload_0
    //   96: getfield mCollapseOffset : F
    //   99: aload_0
    //   100: getfield mCollapsibleHeight : I
    //   103: i2f
    //   104: invokestatic min : (FF)F
    //   107: putfield mCollapseOffset : F
    //   110: iload #4
    //   112: istore_2
    //   113: aload_0
    //   114: getfield mCollapseOffset : F
    //   117: fconst_0
    //   118: fcmpl
    //   119: ifeq -> 124
    //   122: iconst_1
    //   123: istore_2
    //   124: iload #5
    //   126: iload_2
    //   127: if_icmpeq -> 135
    //   130: aload_0
    //   131: iload_2
    //   132: invokespecial onCollapsedChanged : (Z)V
    //   135: goto -> 161
    //   138: aload_0
    //   139: getfield mOpenOnLayout : Z
    //   142: ifeq -> 148
    //   145: goto -> 155
    //   148: aload_0
    //   149: getfield mCollapsibleHeight : I
    //   152: i2f
    //   153: fstore #6
    //   155: aload_0
    //   156: fload #6
    //   158: putfield mCollapseOffset : F
    //   161: iconst_1
    //   162: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #312	-> 0
    //   #313	-> 13
    //   #316	-> 15
    //   #318	-> 29
    //   #319	-> 34
    //   #322	-> 36
    //   #323	-> 43
    //   #324	-> 61
    //   #327	-> 85
    //   #329	-> 94
    //   #331	-> 110
    //   #332	-> 124
    //   #333	-> 130
    //   #335	-> 135
    //   #337	-> 138
    //   #339	-> 161
  }
  
  private int getMaxCollapsedHeight() {
    int i;
    if (isSmallCollapsed()) {
      i = this.mMaxCollapsedHeightSmall;
    } else {
      i = this.mMaxCollapsedHeight;
    } 
    return i + this.mCollapsibleHeightReserved;
  }
  
  public void setOnDismissedListener(OnDismissedListener paramOnDismissedListener) {
    this.mOnDismissedListener = paramOnDismissedListener;
  }
  
  private boolean isDismissable() {
    boolean bool;
    if (this.mOnDismissedListener != null && !this.mDismissLocked) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setOnCollapsedChangedListener(OnCollapsedChangedListener paramOnCollapsedChangedListener) {
    this.mOnCollapsedChangedListener = paramOnCollapsedChangedListener;
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent) {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual getActionMasked : ()I
    //   4: istore_2
    //   5: iload_2
    //   6: ifne -> 16
    //   9: aload_0
    //   10: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   13: invokevirtual clear : ()V
    //   16: aload_0
    //   17: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   20: aload_1
    //   21: invokevirtual addMovement : (Landroid/view/MotionEvent;)V
    //   24: iconst_0
    //   25: istore_3
    //   26: iload_2
    //   27: ifeq -> 189
    //   30: iload_2
    //   31: iconst_1
    //   32: if_icmpeq -> 182
    //   35: iload_2
    //   36: iconst_2
    //   37: if_icmpeq -> 62
    //   40: iload_2
    //   41: iconst_3
    //   42: if_icmpeq -> 182
    //   45: iload_2
    //   46: bipush #6
    //   48: if_icmpeq -> 54
    //   51: goto -> 254
    //   54: aload_0
    //   55: aload_1
    //   56: invokespecial onSecondaryPointerUp : (Landroid/view/MotionEvent;)V
    //   59: goto -> 254
    //   62: aload_1
    //   63: invokevirtual getX : ()F
    //   66: fstore #4
    //   68: aload_1
    //   69: invokevirtual getY : ()F
    //   72: fstore #5
    //   74: fload #5
    //   76: aload_0
    //   77: getfield mInitialTouchY : F
    //   80: fsub
    //   81: fstore #6
    //   83: fload #6
    //   85: invokestatic abs : (F)F
    //   88: aload_0
    //   89: getfield mTouchSlop : I
    //   92: i2f
    //   93: fcmpl
    //   94: ifle -> 179
    //   97: aload_0
    //   98: fload #4
    //   100: fload #5
    //   102: invokespecial findChildUnder : (FF)Landroid/view/View;
    //   105: ifnull -> 179
    //   108: iconst_2
    //   109: aload_0
    //   110: invokevirtual getNestedScrollAxes : ()I
    //   113: iand
    //   114: ifne -> 179
    //   117: aload_0
    //   118: aload_1
    //   119: iconst_0
    //   120: invokevirtual getPointerId : (I)I
    //   123: putfield mActivePointerId : I
    //   126: aload_0
    //   127: iconst_1
    //   128: putfield mIsDragging : Z
    //   131: aload_0
    //   132: getfield mLastTouchY : F
    //   135: fstore #5
    //   137: aload_0
    //   138: getfield mTouchSlop : I
    //   141: istore_2
    //   142: iload_2
    //   143: i2f
    //   144: fstore #4
    //   146: iload_2
    //   147: i2f
    //   148: fstore #7
    //   150: fload #5
    //   152: fload #6
    //   154: fadd
    //   155: fload #5
    //   157: fload #7
    //   159: fadd
    //   160: invokestatic min : (FF)F
    //   163: fstore #6
    //   165: aload_0
    //   166: fload #5
    //   168: fload #4
    //   170: fsub
    //   171: fload #6
    //   173: invokestatic max : (FF)F
    //   176: putfield mLastTouchY : F
    //   179: goto -> 254
    //   182: aload_0
    //   183: invokespecial resetTouch : ()V
    //   186: goto -> 254
    //   189: aload_1
    //   190: invokevirtual getX : ()F
    //   193: fstore #5
    //   195: aload_1
    //   196: invokevirtual getY : ()F
    //   199: fstore #6
    //   201: aload_0
    //   202: fload #5
    //   204: putfield mInitialTouchX : F
    //   207: aload_0
    //   208: fload #6
    //   210: putfield mLastTouchY : F
    //   213: aload_0
    //   214: fload #6
    //   216: putfield mInitialTouchY : F
    //   219: aload_0
    //   220: fload #5
    //   222: fload #6
    //   224: invokespecial isListChildUnderClipped : (FF)Z
    //   227: ifeq -> 245
    //   230: aload_0
    //   231: getfield mCollapseOffset : F
    //   234: fconst_0
    //   235: fcmpl
    //   236: ifle -> 245
    //   239: iconst_1
    //   240: istore #8
    //   242: goto -> 248
    //   245: iconst_0
    //   246: istore #8
    //   248: aload_0
    //   249: iload #8
    //   251: putfield mOpenOnClick : Z
    //   254: aload_0
    //   255: getfield mIsDragging : Z
    //   258: ifeq -> 265
    //   261: aload_0
    //   262: invokespecial abortAnimation : ()V
    //   265: aload_0
    //   266: getfield mIsDragging : Z
    //   269: ifne -> 282
    //   272: iload_3
    //   273: istore #8
    //   275: aload_0
    //   276: getfield mOpenOnClick : Z
    //   279: ifeq -> 285
    //   282: iconst_1
    //   283: istore #8
    //   285: iload #8
    //   287: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #361	-> 0
    //   #363	-> 5
    //   #364	-> 9
    //   #367	-> 16
    //   #369	-> 24
    //   #394	-> 54
    //   #396	-> 59
    //   #380	-> 62
    //   #381	-> 68
    //   #382	-> 74
    //   #383	-> 83
    //   #384	-> 108
    //   #385	-> 117
    //   #386	-> 126
    //   #387	-> 131
    //   #388	-> 150
    //   #387	-> 165
    //   #391	-> 179
    //   #400	-> 182
    //   #371	-> 189
    //   #372	-> 195
    //   #373	-> 201
    //   #374	-> 207
    //   #375	-> 219
    //   #377	-> 254
    //   #405	-> 254
    //   #406	-> 261
    //   #408	-> 265
  }
  
  private boolean isNestedListChildScrolled() {
    AbsListView absListView = this.mNestedListChild;
    null = false;
    if (absListView != null && 
      absListView.getChildCount() > 0) {
      absListView = this.mNestedListChild;
      if (absListView.getFirstVisiblePosition() <= 0) {
        absListView = this.mNestedListChild;
        return (absListView.getChildAt(0).getTop() < 0) ? true : null;
      } 
    } else {
      return null;
    } 
    return true;
  }
  
  private boolean isNestedRecyclerChildScrolled() {
    RecyclerView recyclerView = this.mNestedRecyclerChild;
    boolean bool = false;
    if (recyclerView != null && recyclerView.getChildCount() > 0) {
      recyclerView = this.mNestedRecyclerChild;
      RecyclerView.ViewHolder viewHolder = recyclerView.findViewHolderForAdapterPosition(0);
      if (viewHolder == null || viewHolder.itemView.getTop() < 0)
        bool = true; 
      return bool;
    } 
    return false;
  }
  
  private boolean isNestedScrollChildScrolled(boolean paramBoolean) {
    ScrollView scrollView = this.mNestedScrollChild;
    boolean bool = false;
    if (scrollView != null && 
      scrollView.getChildCount() > 0 && this.mCollapseOffset == 0.0F) {
      if (paramBoolean) {
        paramBoolean = bool;
        if (this.mNestedScrollChild.getScrollY() > 0)
          paramBoolean = true; 
        return paramBoolean;
      } 
      return true;
    } 
    return false;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual getActionMasked : ()I
    //   4: istore_2
    //   5: aload_0
    //   6: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   9: aload_1
    //   10: invokevirtual addMovement : (Landroid/view/MotionEvent;)V
    //   13: iconst_0
    //   14: istore_3
    //   15: iconst_0
    //   16: istore #4
    //   18: iconst_1
    //   19: istore #5
    //   21: iconst_1
    //   22: istore #6
    //   24: iconst_0
    //   25: istore #7
    //   27: iconst_0
    //   28: istore #8
    //   30: iconst_0
    //   31: istore #9
    //   33: iconst_0
    //   34: istore #10
    //   36: iload_2
    //   37: ifeq -> 923
    //   40: iload_2
    //   41: iconst_1
    //   42: if_icmpeq -> 540
    //   45: iload_2
    //   46: iconst_2
    //   47: if_icmpeq -> 180
    //   50: iload_2
    //   51: iconst_3
    //   52: if_icmpeq -> 130
    //   55: iload_2
    //   56: iconst_5
    //   57: if_icmpeq -> 77
    //   60: iload_2
    //   61: bipush #6
    //   63: if_icmpeq -> 69
    //   66: goto -> 1035
    //   69: aload_0
    //   70: aload_1
    //   71: invokespecial onSecondaryPointerUp : (Landroid/view/MotionEvent;)V
    //   74: goto -> 1035
    //   77: aload_1
    //   78: invokevirtual getActionIndex : ()I
    //   81: istore #8
    //   83: aload_1
    //   84: iload #8
    //   86: invokevirtual getPointerId : (I)I
    //   89: istore #10
    //   91: aload_0
    //   92: iload #10
    //   94: putfield mActivePointerId : I
    //   97: aload_0
    //   98: aload_1
    //   99: iload #8
    //   101: invokevirtual getX : (I)F
    //   104: putfield mInitialTouchX : F
    //   107: aload_1
    //   108: iload #8
    //   110: invokevirtual getY : (I)F
    //   113: fstore #11
    //   115: aload_0
    //   116: fload #11
    //   118: putfield mLastTouchY : F
    //   121: aload_0
    //   122: fload #11
    //   124: putfield mInitialTouchY : F
    //   127: goto -> 1035
    //   130: aload_0
    //   131: getfield mIsDragging : Z
    //   134: ifeq -> 174
    //   137: aload_0
    //   138: getfield mCollapseOffset : F
    //   141: fstore #11
    //   143: aload_0
    //   144: getfield mCollapsibleHeight : I
    //   147: istore #8
    //   149: fload #11
    //   151: iload #8
    //   153: iconst_2
    //   154: idiv
    //   155: i2f
    //   156: fcmpg
    //   157: ifge -> 163
    //   160: goto -> 167
    //   163: iload #8
    //   165: istore #10
    //   167: aload_0
    //   168: iload #10
    //   170: fconst_0
    //   171: invokespecial smoothScrollTo : (IF)V
    //   174: aload_0
    //   175: invokespecial resetTouch : ()V
    //   178: iconst_1
    //   179: ireturn
    //   180: aload_1
    //   181: aload_0
    //   182: getfield mActivePointerId : I
    //   185: invokevirtual findPointerIndex : (I)I
    //   188: istore #8
    //   190: iload #8
    //   192: istore #10
    //   194: iload #8
    //   196: ifge -> 285
    //   199: new java/lang/StringBuilder
    //   202: dup
    //   203: invokespecial <init> : ()V
    //   206: astore #12
    //   208: aload #12
    //   210: ldc_w 'Bad pointer id '
    //   213: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   216: pop
    //   217: aload #12
    //   219: aload_0
    //   220: getfield mActivePointerId : I
    //   223: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   226: pop
    //   227: aload #12
    //   229: ldc_w ', resetting'
    //   232: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   235: pop
    //   236: ldc 'ResolverDrawerLayout'
    //   238: aload #12
    //   240: invokevirtual toString : ()Ljava/lang/String;
    //   243: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   246: pop
    //   247: iconst_0
    //   248: istore #10
    //   250: aload_0
    //   251: aload_1
    //   252: iconst_0
    //   253: invokevirtual getPointerId : (I)I
    //   256: putfield mActivePointerId : I
    //   259: aload_0
    //   260: aload_1
    //   261: invokevirtual getX : ()F
    //   264: putfield mInitialTouchX : F
    //   267: aload_1
    //   268: invokevirtual getY : ()F
    //   271: fstore #11
    //   273: aload_0
    //   274: fload #11
    //   276: putfield mLastTouchY : F
    //   279: aload_0
    //   280: fload #11
    //   282: putfield mInitialTouchY : F
    //   285: aload_1
    //   286: iload #10
    //   288: invokevirtual getX : (I)F
    //   291: fstore #13
    //   293: aload_1
    //   294: iload #10
    //   296: invokevirtual getY : (I)F
    //   299: fstore #11
    //   301: iload #4
    //   303: istore_3
    //   304: aload_0
    //   305: getfield mIsDragging : Z
    //   308: ifne -> 409
    //   311: fload #11
    //   313: aload_0
    //   314: getfield mInitialTouchY : F
    //   317: fsub
    //   318: fstore #14
    //   320: iload #4
    //   322: istore_3
    //   323: fload #14
    //   325: invokestatic abs : (F)F
    //   328: aload_0
    //   329: getfield mTouchSlop : I
    //   332: i2f
    //   333: fcmpl
    //   334: ifle -> 409
    //   337: iload #4
    //   339: istore_3
    //   340: aload_0
    //   341: fload #13
    //   343: fload #11
    //   345: invokespecial findChildUnder : (FF)Landroid/view/View;
    //   348: ifnull -> 409
    //   351: aload_0
    //   352: iconst_1
    //   353: putfield mIsDragging : Z
    //   356: iconst_1
    //   357: istore_3
    //   358: aload_0
    //   359: getfield mLastTouchY : F
    //   362: fstore #13
    //   364: aload_0
    //   365: getfield mTouchSlop : I
    //   368: istore #10
    //   370: iload #10
    //   372: i2f
    //   373: fstore #15
    //   375: iload #10
    //   377: i2f
    //   378: fstore #16
    //   380: fload #13
    //   382: fload #14
    //   384: fadd
    //   385: fload #13
    //   387: fload #16
    //   389: fadd
    //   390: invokestatic min : (FF)F
    //   393: fstore #14
    //   395: aload_0
    //   396: fload #13
    //   398: fload #15
    //   400: fsub
    //   401: fload #14
    //   403: invokestatic max : (FF)F
    //   406: putfield mLastTouchY : F
    //   409: aload_0
    //   410: getfield mIsDragging : Z
    //   413: ifeq -> 531
    //   416: fload #11
    //   418: aload_0
    //   419: getfield mLastTouchY : F
    //   422: fsub
    //   423: fstore #14
    //   425: fload #14
    //   427: fconst_0
    //   428: fcmpl
    //   429: ifle -> 454
    //   432: aload_0
    //   433: invokespecial isNestedListChildScrolled : ()Z
    //   436: ifeq -> 454
    //   439: aload_0
    //   440: getfield mNestedListChild : Landroid/widget/AbsListView;
    //   443: fload #14
    //   445: fneg
    //   446: f2i
    //   447: iconst_0
    //   448: invokevirtual smoothScrollBy : (II)V
    //   451: goto -> 531
    //   454: fload #14
    //   456: fconst_0
    //   457: fcmpl
    //   458: ifle -> 483
    //   461: aload_0
    //   462: invokespecial isNestedRecyclerChildScrolled : ()Z
    //   465: ifeq -> 483
    //   468: aload_0
    //   469: getfield mNestedRecyclerChild : Lcom/android/internal/widget/RecyclerView;
    //   472: iconst_0
    //   473: fload #14
    //   475: fneg
    //   476: f2i
    //   477: invokevirtual scrollBy : (II)V
    //   480: goto -> 531
    //   483: fload #14
    //   485: fconst_0
    //   486: fcmpl
    //   487: ifle -> 497
    //   490: iload #6
    //   492: istore #5
    //   494: goto -> 500
    //   497: iconst_0
    //   498: istore #5
    //   500: aload_0
    //   501: iload #5
    //   503: invokespecial isNestedScrollChildScrolled : (Z)Z
    //   506: ifeq -> 524
    //   509: aload_0
    //   510: getfield mNestedScrollChild : Landroid/widget/ScrollView;
    //   513: iconst_0
    //   514: fload #14
    //   516: fneg
    //   517: f2i
    //   518: invokevirtual scrollBy : (II)V
    //   521: goto -> 531
    //   524: aload_0
    //   525: fload #14
    //   527: invokespecial performDrag : (F)F
    //   530: pop
    //   531: aload_0
    //   532: fload #11
    //   534: putfield mLastTouchY : F
    //   537: goto -> 1035
    //   540: aload_0
    //   541: getfield mIsDragging : Z
    //   544: istore #5
    //   546: aload_0
    //   547: iconst_0
    //   548: putfield mIsDragging : Z
    //   551: iload #5
    //   553: ifne -> 603
    //   556: aload_0
    //   557: aload_0
    //   558: getfield mInitialTouchX : F
    //   561: aload_0
    //   562: getfield mInitialTouchY : F
    //   565: invokespecial findChildUnder : (FF)Landroid/view/View;
    //   568: ifnonnull -> 603
    //   571: aload_0
    //   572: aload_1
    //   573: invokevirtual getX : ()F
    //   576: aload_1
    //   577: invokevirtual getY : ()F
    //   580: invokespecial findChildUnder : (FF)Landroid/view/View;
    //   583: ifnonnull -> 603
    //   586: aload_0
    //   587: invokespecial isDismissable : ()Z
    //   590: ifeq -> 603
    //   593: aload_0
    //   594: invokevirtual dispatchOnDismissed : ()V
    //   597: aload_0
    //   598: invokespecial resetTouch : ()V
    //   601: iconst_1
    //   602: ireturn
    //   603: aload_0
    //   604: getfield mOpenOnClick : Z
    //   607: ifeq -> 660
    //   610: aload_1
    //   611: invokevirtual getX : ()F
    //   614: aload_0
    //   615: getfield mInitialTouchX : F
    //   618: fsub
    //   619: invokestatic abs : (F)F
    //   622: aload_0
    //   623: getfield mTouchSlop : I
    //   626: i2f
    //   627: fcmpg
    //   628: ifge -> 660
    //   631: aload_1
    //   632: invokevirtual getY : ()F
    //   635: aload_0
    //   636: getfield mInitialTouchY : F
    //   639: fsub
    //   640: invokestatic abs : (F)F
    //   643: aload_0
    //   644: getfield mTouchSlop : I
    //   647: i2f
    //   648: fcmpg
    //   649: ifge -> 660
    //   652: aload_0
    //   653: iconst_0
    //   654: fconst_0
    //   655: invokespecial smoothScrollTo : (IF)V
    //   658: iconst_1
    //   659: ireturn
    //   660: aload_0
    //   661: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   664: sipush #1000
    //   667: invokevirtual computeCurrentVelocity : (I)V
    //   670: aload_0
    //   671: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   674: aload_0
    //   675: getfield mActivePointerId : I
    //   678: invokevirtual getYVelocity : (I)F
    //   681: fstore #11
    //   683: fload #11
    //   685: invokestatic abs : (F)F
    //   688: aload_0
    //   689: getfield mMinFlingVelocity : F
    //   692: fcmpl
    //   693: ifle -> 879
    //   696: aload_0
    //   697: invokevirtual getShowAtTop : ()Z
    //   700: ifeq -> 759
    //   703: aload_0
    //   704: invokespecial isDismissable : ()Z
    //   707: ifeq -> 728
    //   710: fload #11
    //   712: fconst_0
    //   713: fcmpg
    //   714: ifge -> 728
    //   717: aload_0
    //   718: invokespecial abortAnimation : ()V
    //   721: aload_0
    //   722: invokespecial dismiss : ()V
    //   725: goto -> 916
    //   728: fload #11
    //   730: fconst_0
    //   731: fcmpg
    //   732: ifge -> 742
    //   735: iload #7
    //   737: istore #10
    //   739: goto -> 748
    //   742: aload_0
    //   743: getfield mCollapsibleHeight : I
    //   746: istore #10
    //   748: aload_0
    //   749: iload #10
    //   751: fload #11
    //   753: invokespecial smoothScrollTo : (IF)V
    //   756: goto -> 916
    //   759: aload_0
    //   760: invokespecial isDismissable : ()Z
    //   763: ifeq -> 815
    //   766: fload #11
    //   768: fconst_0
    //   769: fcmpl
    //   770: ifle -> 815
    //   773: aload_0
    //   774: getfield mCollapseOffset : F
    //   777: fstore #14
    //   779: aload_0
    //   780: getfield mCollapsibleHeight : I
    //   783: istore #10
    //   785: fload #14
    //   787: iload #10
    //   789: i2f
    //   790: fcmpl
    //   791: ifle -> 815
    //   794: aload_0
    //   795: iload #10
    //   797: aload_0
    //   798: getfield mUncollapsibleHeight : I
    //   801: iadd
    //   802: fload #11
    //   804: invokespecial smoothScrollTo : (IF)V
    //   807: aload_0
    //   808: iconst_1
    //   809: putfield mDismissOnScrollerFinished : Z
    //   812: goto -> 916
    //   815: aload_0
    //   816: invokespecial isNestedListChildScrolled : ()Z
    //   819: ifeq -> 833
    //   822: aload_0
    //   823: getfield mNestedListChild : Landroid/widget/AbsListView;
    //   826: iconst_0
    //   827: invokevirtual smoothScrollToPosition : (I)V
    //   830: goto -> 848
    //   833: aload_0
    //   834: invokespecial isNestedRecyclerChildScrolled : ()Z
    //   837: ifeq -> 848
    //   840: aload_0
    //   841: getfield mNestedRecyclerChild : Lcom/android/internal/widget/RecyclerView;
    //   844: iconst_0
    //   845: invokevirtual smoothScrollToPosition : (I)V
    //   848: fload #11
    //   850: fconst_0
    //   851: fcmpg
    //   852: ifge -> 862
    //   855: iload #8
    //   857: istore #10
    //   859: goto -> 868
    //   862: aload_0
    //   863: getfield mCollapsibleHeight : I
    //   866: istore #10
    //   868: aload_0
    //   869: iload #10
    //   871: fload #11
    //   873: invokespecial smoothScrollTo : (IF)V
    //   876: goto -> 916
    //   879: aload_0
    //   880: getfield mCollapseOffset : F
    //   883: fstore #11
    //   885: aload_0
    //   886: getfield mCollapsibleHeight : I
    //   889: istore #10
    //   891: fload #11
    //   893: iload #10
    //   895: iconst_2
    //   896: idiv
    //   897: i2f
    //   898: fcmpg
    //   899: ifge -> 909
    //   902: iload #9
    //   904: istore #10
    //   906: goto -> 909
    //   909: aload_0
    //   910: iload #10
    //   912: fconst_0
    //   913: invokespecial smoothScrollTo : (IF)V
    //   916: aload_0
    //   917: invokespecial resetTouch : ()V
    //   920: goto -> 1035
    //   923: aload_1
    //   924: invokevirtual getX : ()F
    //   927: fstore #14
    //   929: aload_1
    //   930: invokevirtual getY : ()F
    //   933: fstore #11
    //   935: aload_0
    //   936: fload #14
    //   938: putfield mInitialTouchX : F
    //   941: aload_0
    //   942: fload #11
    //   944: putfield mLastTouchY : F
    //   947: aload_0
    //   948: fload #11
    //   950: putfield mInitialTouchY : F
    //   953: aload_0
    //   954: aload_1
    //   955: iconst_0
    //   956: invokevirtual getPointerId : (I)I
    //   959: putfield mActivePointerId : I
    //   962: aload_0
    //   963: aload_0
    //   964: getfield mInitialTouchX : F
    //   967: aload_0
    //   968: getfield mInitialTouchY : F
    //   971: invokespecial findChildUnder : (FF)Landroid/view/View;
    //   974: ifnull -> 983
    //   977: iconst_1
    //   978: istore #10
    //   980: goto -> 986
    //   983: iconst_0
    //   984: istore #10
    //   986: aload_0
    //   987: invokespecial isDismissable : ()Z
    //   990: ifne -> 1008
    //   993: aload_0
    //   994: getfield mCollapsibleHeight : I
    //   997: ifle -> 1003
    //   1000: goto -> 1008
    //   1003: iconst_0
    //   1004: istore_3
    //   1005: goto -> 1010
    //   1008: iconst_1
    //   1009: istore_3
    //   1010: iload #10
    //   1012: ifeq -> 1022
    //   1015: iload_3
    //   1016: ifeq -> 1022
    //   1019: goto -> 1025
    //   1022: iconst_0
    //   1023: istore #5
    //   1025: aload_0
    //   1026: iload #5
    //   1028: putfield mIsDragging : Z
    //   1031: aload_0
    //   1032: invokespecial abortAnimation : ()V
    //   1035: iload_3
    //   1036: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #445	-> 0
    //   #447	-> 5
    //   #449	-> 13
    //   #450	-> 18
    //   #512	-> 69
    //   #514	-> 74
    //   #503	-> 77
    //   #504	-> 83
    //   #505	-> 91
    //   #506	-> 97
    //   #507	-> 107
    //   #509	-> 127
    //   #565	-> 130
    //   #566	-> 137
    //   #567	-> 137
    //   #566	-> 167
    //   #569	-> 174
    //   #570	-> 178
    //   #465	-> 180
    //   #466	-> 190
    //   #467	-> 199
    //   #468	-> 247
    //   #469	-> 250
    //   #470	-> 259
    //   #471	-> 267
    //   #473	-> 285
    //   #474	-> 293
    //   #475	-> 301
    //   #476	-> 311
    //   #477	-> 320
    //   #478	-> 351
    //   #479	-> 358
    //   #480	-> 380
    //   #479	-> 395
    //   #483	-> 409
    //   #484	-> 416
    //   #485	-> 425
    //   #486	-> 439
    //   #487	-> 454
    //   #488	-> 468
    //   #491	-> 483
    //   #492	-> 509
    //   #495	-> 524
    //   #498	-> 531
    //   #500	-> 537
    //   #517	-> 540
    //   #518	-> 546
    //   #519	-> 551
    //   #520	-> 571
    //   #521	-> 586
    //   #522	-> 593
    //   #523	-> 597
    //   #524	-> 601
    //   #527	-> 603
    //   #528	-> 631
    //   #529	-> 652
    //   #530	-> 658
    //   #532	-> 660
    //   #533	-> 670
    //   #534	-> 683
    //   #535	-> 696
    //   #536	-> 703
    //   #537	-> 717
    //   #538	-> 721
    //   #540	-> 728
    //   #543	-> 759
    //   #545	-> 794
    //   #546	-> 807
    //   #548	-> 815
    //   #549	-> 822
    //   #550	-> 833
    //   #551	-> 840
    //   #553	-> 848
    //   #557	-> 879
    //   #558	-> 879
    //   #557	-> 909
    //   #560	-> 916
    //   #562	-> 920
    //   #452	-> 923
    //   #453	-> 929
    //   #454	-> 935
    //   #455	-> 941
    //   #456	-> 953
    //   #457	-> 962
    //   #458	-> 986
    //   #459	-> 1010
    //   #460	-> 1031
    //   #462	-> 1035
    //   #574	-> 1035
  }
  
  private void onSecondaryPointerUp(MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.getActionIndex();
    int j = paramMotionEvent.getPointerId(i);
    if (j == this.mActivePointerId) {
      if (i == 0) {
        i = 1;
      } else {
        i = 0;
      } 
      this.mInitialTouchX = paramMotionEvent.getX(i);
      float f = paramMotionEvent.getY(i);
      this.mInitialTouchY = f;
      this.mActivePointerId = paramMotionEvent.getPointerId(i);
    } 
  }
  
  private void resetTouch() {
    this.mActivePointerId = -1;
    this.mIsDragging = false;
    this.mOpenOnClick = false;
    this.mLastTouchY = 0.0F;
    this.mInitialTouchY = 0.0F;
    this.mInitialTouchX = 0.0F;
    this.mVelocityTracker.clear();
    this.mNestedScrollChild = null;
  }
  
  private void dismiss() {
    RunOnDismissedListener runOnDismissedListener = new RunOnDismissedListener();
    post(runOnDismissedListener);
  }
  
  public void computeScroll() {
    super.computeScroll();
    if (this.mScroller.computeScrollOffset()) {
      boolean bool = this.mScroller.isFinished();
      performDrag(this.mScroller.getCurrY() - this.mCollapseOffset);
      if ((bool ^ true) != 0) {
        postInvalidateOnAnimation();
      } else if (this.mDismissOnScrollerFinished && this.mOnDismissedListener != null) {
        dismiss();
      } 
    } 
  }
  
  private void abortAnimation() {
    this.mScroller.abortAnimation();
    this.mRunOnDismissedListener = null;
    this.mDismissOnScrollerFinished = false;
  }
  
  private float performDrag(float paramFloat) {
    if (getShowAtTop())
      return 0.0F; 
    float f = Math.max(0.0F, Math.min(this.mCollapseOffset + paramFloat, (this.mCollapsibleHeight + this.mUncollapsibleHeight)));
    paramFloat = this.mCollapseOffset;
    if (f != paramFloat) {
      boolean bool1, bool2;
      float f1 = f - paramFloat;
      float f2 = this.mDragRemainder + f1 - (int)f1;
      if (f2 >= 1.0F) {
        this.mDragRemainder = f2 - 1.0F;
        paramFloat = f1 + 1.0F;
      } else {
        paramFloat = f1;
        if (f2 <= -1.0F) {
          this.mDragRemainder = f2 + 1.0F;
          paramFloat = f1 - 1.0F;
        } 
      } 
      int i = getChildCount();
      byte b;
      for (b = 0; b < i; b++) {
        View view = getChildAt(b);
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        if (!layoutParams.ignoreOffset)
          view.offsetTopAndBottom((int)paramFloat); 
      } 
      f1 = this.mCollapseOffset;
      b = 1;
      if (f1 != 0.0F) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      this.mCollapseOffset = f;
      this.mTopOffset = (int)(this.mTopOffset + paramFloat);
      if (f != 0.0F) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if (bool1 != bool2) {
        onCollapsedChanged(bool2);
        MetricsLogger metricsLogger = getMetricsLogger();
        LogMaker logMaker = new LogMaker(1651);
        if (!bool2)
          b = 0; 
        logMaker = logMaker.setSubtype(b);
        metricsLogger.write(logMaker);
      } 
      onScrollChanged(0, (int)f, 0, (int)(f - paramFloat));
      postInvalidateOnAnimation();
      return paramFloat;
    } 
    return 0.0F;
  }
  
  private void onCollapsedChanged(boolean paramBoolean) {
    notifyViewAccessibilityStateChangedIfNeeded(0);
    if (this.mScrollIndicatorDrawable != null)
      setWillNotDraw(paramBoolean ^ true); 
    OnCollapsedChangedListener onCollapsedChangedListener = this.mOnCollapsedChangedListener;
    if (onCollapsedChangedListener != null)
      onCollapsedChangedListener.onCollapsedChanged(paramBoolean); 
  }
  
  void dispatchOnDismissed() {
    OnDismissedListener onDismissedListener = this.mOnDismissedListener;
    if (onDismissedListener != null)
      onDismissedListener.onDismissed(); 
    RunOnDismissedListener runOnDismissedListener = this.mRunOnDismissedListener;
    if (runOnDismissedListener != null) {
      removeCallbacks(runOnDismissedListener);
      this.mRunOnDismissedListener = null;
    } 
  }
  
  private void smoothScrollTo(int paramInt, float paramFloat) {
    abortAnimation();
    int i = (int)this.mCollapseOffset;
    int j = paramInt - i;
    if (j == 0) {
      post(new _$$Lambda$ResolverDrawerLayout$IUl4bnv1vxE_rM1P6SVeo7O5_TQ(this));
      return;
    } 
    int k = getHeight();
    paramInt = k / 2;
    float f1 = Math.min(1.0F, Math.abs(j) * 1.0F / k);
    float f2 = paramInt, f3 = paramInt;
    f1 = distanceInfluenceForSnapDuration(f1);
    paramFloat = Math.abs(paramFloat);
    if (paramFloat > 0.0F) {
      paramInt = Math.round(Math.abs((f2 + f3 * f1) / paramFloat) * 1000.0F) * 4;
    } else {
      paramFloat = Math.abs(j) / k;
      paramInt = (int)((1.0F + paramFloat) * 100.0F);
    } 
    paramInt = Math.min(paramInt, 300);
    this.mScroller.startScroll(0, i, 0, j, paramInt);
    postInvalidateOnAnimation();
  }
  
  private float distanceInfluenceForSnapDuration(float paramFloat) {
    paramFloat = (float)((paramFloat - 0.5F) * 0.4712389167638204D);
    return (float)Math.sin(paramFloat);
  }
  
  private View findChildUnder(float paramFloat1, float paramFloat2) {
    return findChildUnder(this, paramFloat1, paramFloat2);
  }
  
  private static View findChildUnder(ViewGroup paramViewGroup, float paramFloat1, float paramFloat2) {
    int i = paramViewGroup.getChildCount();
    for (; --i >= 0; i--) {
      View view = paramViewGroup.getChildAt(i);
      if (isChildUnder(view, paramFloat1, paramFloat2))
        return view; 
    } 
    return null;
  }
  
  private View findListChildUnder(float paramFloat1, float paramFloat2) {
    View view = findChildUnder(paramFloat1, paramFloat2);
    while (view != null) {
      paramFloat1 -= view.getX();
      paramFloat2 -= view.getY();
      if (view instanceof AbsListView)
        return findChildUnder((ViewGroup)view, paramFloat1, paramFloat2); 
      if (view instanceof ViewGroup) {
        view = findChildUnder((ViewGroup)view, paramFloat1, paramFloat2);
        continue;
      } 
      view = null;
    } 
    return view;
  }
  
  private boolean isListChildUnderClipped(float paramFloat1, float paramFloat2) {
    boolean bool;
    View view = findListChildUnder(paramFloat1, paramFloat2);
    if (view != null && isDescendantClipped(view)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isDescendantClipped(View paramView) {
    Rect rect = this.mTempRect;
    int i = paramView.getWidth(), j = paramView.getHeight();
    boolean bool = false;
    rect.set(0, 0, i, j);
    offsetDescendantRectToMyCoords(paramView, this.mTempRect);
    if (paramView.getParent() != this) {
      View view = paramView;
      ViewParent viewParent = paramView.getParent();
      paramView = view;
      while (viewParent != this) {
        paramView = (View)viewParent;
        viewParent = paramView.getParent();
      } 
    } 
    i = getHeight() - getPaddingBottom();
    int k = getChildCount();
    for (j = indexOfChild(paramView) + 1; j < k; j++) {
      paramView = getChildAt(j);
      if (paramView.getVisibility() != 8)
        i = Math.min(i, paramView.getTop()); 
    } 
    if (this.mTempRect.bottom > i)
      bool = true; 
    return bool;
  }
  
  private static boolean isChildUnder(View paramView, float paramFloat1, float paramFloat2) {
    boolean bool;
    float f1 = paramView.getX();
    float f2 = paramView.getY();
    float f3 = paramView.getWidth();
    float f4 = paramView.getHeight();
    if (paramFloat1 >= f1 && paramFloat2 >= f2 && paramFloat1 < f3 + f1 && paramFloat2 < f4 + f2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void requestChildFocus(View paramView1, View paramView2) {
    super.requestChildFocus(paramView1, paramView2);
    if (!isInTouchMode() && isDescendantClipped(paramView2))
      smoothScrollTo(0, 0.0F); 
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    getViewTreeObserver().addOnTouchModeChangeListener(this.mTouchModeChangeListener);
  }
  
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    getViewTreeObserver().removeOnTouchModeChangeListener(this.mTouchModeChangeListener);
    abortAnimation();
  }
  
  public boolean onStartNestedScroll(View paramView1, View paramView2, int paramInt) {
    if ((paramInt & 0x2) != 0) {
      if (paramView2 instanceof AbsListView)
        this.mNestedListChild = (AbsListView)paramView2; 
      if (paramView2 instanceof RecyclerView)
        this.mNestedRecyclerChild = (RecyclerView)paramView2; 
      if (paramView2 instanceof ScrollView)
        this.mNestedScrollChild = (ScrollView)paramView2; 
      return true;
    } 
    return false;
  }
  
  public void onNestedScrollAccepted(View paramView1, View paramView2, int paramInt) {
    super.onNestedScrollAccepted(paramView1, paramView2, paramInt);
  }
  
  public void onStopNestedScroll(View paramView) {
    super.onStopNestedScroll(paramView);
    if (this.mScroller.isFinished()) {
      float f = this.mCollapseOffset;
      int i = this.mCollapsibleHeight, j = i;
      if (f < (i / 2))
        j = 0; 
      smoothScrollTo(j, 0.0F);
    } 
  }
  
  public void onNestedScroll(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (paramInt4 < 0)
      performDrag(-paramInt4); 
  }
  
  public void onNestedPreScroll(View paramView, int paramInt1, int paramInt2, int[] paramArrayOfint) {
    if (paramInt2 > 0)
      paramArrayOfint[1] = (int)-performDrag(-paramInt2); 
  }
  
  public boolean onNestedPreFling(View paramView, float paramFloat1, float paramFloat2) {
    if (!getShowAtTop() && paramFloat2 > this.mMinFlingVelocity && this.mCollapseOffset != 0.0F) {
      smoothScrollTo(0, paramFloat2);
      return true;
    } 
    return false;
  }
  
  public boolean onNestedFling(View paramView, float paramFloat1, float paramFloat2, boolean paramBoolean) {
    int i = 0, j = 0;
    if (!paramBoolean && Math.abs(paramFloat2) > this.mMinFlingVelocity) {
      if (getShowAtTop()) {
        if (isDismissable() && paramFloat2 > 0.0F) {
          abortAnimation();
          dismiss();
        } else {
          i = j;
          if (paramFloat2 < 0.0F)
            i = this.mCollapsibleHeight; 
          smoothScrollTo(i, paramFloat2);
        } 
      } else {
        if (isDismissable() && paramFloat2 < 0.0F) {
          paramFloat1 = this.mCollapseOffset;
          j = this.mCollapsibleHeight;
          if (paramFloat1 > j) {
            smoothScrollTo(j + this.mUncollapsibleHeight, paramFloat2);
            this.mDismissOnScrollerFinished = true;
            return true;
          } 
        } 
        if (paramFloat2 <= 0.0F)
          i = this.mCollapsibleHeight; 
        smoothScrollTo(i, paramFloat2);
      } 
      return true;
    } 
    return false;
  }
  
  private boolean performAccessibilityActionCommon(int paramInt) {
    if (paramInt != 4096 && paramInt != 262144)
      if (paramInt != 524288) {
        if (paramInt != 1048576) {
          if (paramInt != 16908346)
            return false; 
        } else {
          if (this.mCollapseOffset < (this.mCollapsibleHeight + this.mUncollapsibleHeight) && isDismissable()) {
            smoothScrollTo(this.mCollapsibleHeight + this.mUncollapsibleHeight, 0.0F);
            this.mDismissOnScrollerFinished = true;
            return true;
          } 
          return false;
        } 
      } else {
        float f = this.mCollapseOffset;
        paramInt = this.mCollapsibleHeight;
        if (f < paramInt) {
          smoothScrollTo(paramInt, 0.0F);
          return true;
        } 
        return false;
      }  
    if (this.mCollapseOffset != 0.0F) {
      smoothScrollTo(0, 0.0F);
      return true;
    } 
    return false;
  }
  
  public boolean onNestedPrePerformAccessibilityAction(View paramView, int paramInt, Bundle paramBundle) {
    if (super.onNestedPrePerformAccessibilityAction(paramView, paramInt, paramBundle))
      return true; 
    return performAccessibilityActionCommon(paramInt);
  }
  
  public CharSequence getAccessibilityClassName() {
    return ScrollView.class.getName();
  }
  
  public void onInitializeAccessibilityNodeInfoInternal(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    super.onInitializeAccessibilityNodeInfoInternal(paramAccessibilityNodeInfo);
    if (isEnabled()) {
      if (this.mCollapseOffset != 0.0F) {
        paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_FORWARD);
        paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_EXPAND);
        paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_DOWN);
        paramAccessibilityNodeInfo.setScrollable(true);
      } 
      float f = this.mCollapseOffset;
      int i = this.mCollapsibleHeight;
      if (f < (this.mUncollapsibleHeight + i) && (f < i || 
        isDismissable())) {
        paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_UP);
        paramAccessibilityNodeInfo.setScrollable(true);
      } 
      if (this.mCollapseOffset < this.mCollapsibleHeight)
        paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_COLLAPSE); 
      if (this.mCollapseOffset < (this.mCollapsibleHeight + this.mUncollapsibleHeight) && isDismissable())
        paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_DISMISS); 
    } 
    paramAccessibilityNodeInfo.removeAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_ACCESSIBILITY_FOCUS);
  }
  
  public boolean performAccessibilityActionInternal(int paramInt, Bundle paramBundle) {
    if (paramInt == AccessibilityNodeInfo.AccessibilityAction.ACTION_ACCESSIBILITY_FOCUS.getId())
      return false; 
    if (super.performAccessibilityActionInternal(paramInt, paramBundle))
      return true; 
    return performAccessibilityActionCommon(paramInt);
  }
  
  public void onDrawForeground(Canvas paramCanvas) {
    Drawable drawable = this.mScrollIndicatorDrawable;
    if (drawable != null)
      drawable.draw(paramCanvas); 
    super.onDrawForeground(paramCanvas);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    int i3, i = View.MeasureSpec.getSize(paramInt1);
    int j = View.MeasureSpec.getSize(paramInt2);
    paramInt1 = this.mMaxWidth;
    if (paramInt1 >= 0) {
      paramInt1 = Math.min(i, paramInt1);
    } else {
      paramInt1 = i;
    } 
    int k = View.MeasureSpec.makeMeasureSpec(paramInt1, 1073741824);
    int m = View.MeasureSpec.makeMeasureSpec(j, 1073741824);
    int n = getChildCount();
    paramInt1 = 0;
    paramInt2 = 0;
    while (true) {
      i1 = -1;
      i2 = 8;
      i3 = 0;
      if (paramInt2 < n) {
        View view = getChildAt(paramInt2);
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        if (layoutParams.alwaysShow && view.getVisibility() != 8) {
          if (layoutParams.maxHeight != -1) {
            i1 = j - paramInt1;
            i2 = layoutParams.maxHeight;
            i2 = View.MeasureSpec.makeMeasureSpec(i2, -2147483648);
            if (layoutParams.maxHeight > i1) {
              i1 = layoutParams.maxHeight - i1;
            } else {
              i1 = 0;
            } 
            measureChildWithMargins(view, k, 0, i2, i1);
          } else {
            measureChildWithMargins(view, k, 0, m, paramInt1);
          } 
          paramInt1 += view.getMeasuredHeight();
        } 
        paramInt2++;
        continue;
      } 
      break;
    } 
    this.mAlwaysShowHeight = paramInt1;
    int i4;
    for (i4 = 0, paramInt2 = paramInt1, paramInt1 = i3; i4 < n; i4++) {
      View view = getChildAt(i4);
      LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
      if (!layoutParams.alwaysShow && view.getVisibility() != i2) {
        if (layoutParams.maxHeight != i1) {
          i3 = j - paramInt2;
          int i5 = layoutParams.maxHeight;
          i5 = View.MeasureSpec.makeMeasureSpec(i5, -2147483648);
          if (layoutParams.maxHeight > i3) {
            i3 = layoutParams.maxHeight - i3;
          } else {
            i3 = paramInt1;
          } 
          measureChildWithMargins(view, k, 0, i5, i3);
        } else {
          measureChildWithMargins(view, k, 0, m, paramInt2);
        } 
        paramInt2 += view.getMeasuredHeight();
      } 
    } 
    int i1 = this.mCollapsibleHeight;
    int i2 = this.mAlwaysShowHeight;
    i4 = getMaxCollapsedHeight();
    this.mCollapsibleHeight = i2 = Math.max(paramInt1, paramInt2 - i2 - i4);
    this.mUncollapsibleHeight = paramInt2 - i2;
    updateCollapseOffset(i1, isDragging() ^ true);
    if (getShowAtTop()) {
      this.mTopOffset = paramInt1;
    } else {
      this.mTopOffset = Math.max(paramInt1, j - paramInt2) + (int)this.mCollapseOffset;
    } 
    setMeasuredDimension(i, j);
  }
  
  public int getAlwaysShowHeight() {
    return this.mAlwaysShowHeight;
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    paramInt4 = getWidth();
    View view = null;
    paramInt3 = this.mTopOffset;
    int i = getPaddingLeft();
    int j = getPaddingRight();
    int k = getChildCount();
    for (paramInt2 = 0, paramInt1 = paramInt4; paramInt2 < k; paramInt2++) {
      View view1 = getChildAt(paramInt2);
      LayoutParams layoutParams = (LayoutParams)view1.getLayoutParams();
      if (layoutParams.hasNestedScrollIndicator)
        view = view1; 
      if (view1.getVisibility() != 8) {
        int m = layoutParams.topMargin + paramInt3;
        paramInt3 = m;
        if (layoutParams.ignoreOffset)
          paramInt3 = (int)(m - this.mCollapseOffset); 
        m = view1.getMeasuredHeight() + paramInt3;
        int n = view1.getMeasuredWidth();
        int i1 = (paramInt4 - j - i - n) / 2 + i;
        view1.layout(i1, paramInt3, i1 + n, m);
        paramInt3 = layoutParams.bottomMargin + m;
      } 
    } 
    if (this.mScrollIndicatorDrawable != null)
      if (view != null) {
        paramInt3 = view.getLeft();
        paramInt4 = view.getRight();
        paramInt2 = view.getTop();
        paramInt1 = this.mScrollIndicatorDrawable.getIntrinsicHeight();
        this.mScrollIndicatorDrawable.setBounds(paramInt3, paramInt2 - paramInt1, paramInt4, paramInt2);
        setWillNotDraw(true ^ isCollapsed());
      } else {
        this.mScrollIndicatorDrawable = null;
        setWillNotDraw(true);
      }  
  }
  
  public ViewGroup.LayoutParams generateLayoutParams(AttributeSet paramAttributeSet) {
    return (ViewGroup.LayoutParams)new LayoutParams(getContext(), paramAttributeSet);
  }
  
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    if (paramLayoutParams instanceof LayoutParams)
      return (ViewGroup.LayoutParams)new LayoutParams((LayoutParams)paramLayoutParams); 
    if (paramLayoutParams instanceof ViewGroup.MarginLayoutParams)
      return (ViewGroup.LayoutParams)new LayoutParams((ViewGroup.MarginLayoutParams)paramLayoutParams); 
    return (ViewGroup.LayoutParams)new LayoutParams(paramLayoutParams);
  }
  
  protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
    return (ViewGroup.LayoutParams)new LayoutParams(-1, -2);
  }
  
  protected Parcelable onSaveInstanceState() {
    boolean bool;
    SavedState savedState = new SavedState();
    if (this.mCollapsibleHeight > 0 && this.mCollapseOffset == 0.0F) {
      bool = true;
    } else {
      bool = false;
    } 
    savedState.open = bool;
    SavedState.access$302(savedState, this.mCollapsibleHeightReserved);
    return (Parcelable)savedState;
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable) {
    SavedState savedState = (SavedState)paramParcelable;
    super.onRestoreInstanceState(savedState.getSuperState());
    this.mOpenOnLayout = savedState.open;
    this.mCollapsibleHeightReserved = savedState.mCollapsibleHeightReserved;
  }
  
  class LayoutParams extends ViewGroup.MarginLayoutParams {
    public boolean alwaysShow;
    
    public boolean hasNestedScrollIndicator;
    
    public boolean ignoreOffset;
    
    public int maxHeight;
    
    public LayoutParams(ResolverDrawerLayout this$0, AttributeSet param1AttributeSet) {
      super((Context)this$0, param1AttributeSet);
      TypedArray typedArray = this$0.obtainStyledAttributes(param1AttributeSet, R.styleable.ResolverDrawerLayout_LayoutParams);
      this.alwaysShow = typedArray.getBoolean(1, false);
      this.ignoreOffset = typedArray.getBoolean(3, false);
      this.hasNestedScrollIndicator = typedArray.getBoolean(2, false);
      this.maxHeight = typedArray.getDimensionPixelSize(4, -1);
      typedArray.recycle();
    }
    
    public LayoutParams(ResolverDrawerLayout this$0, int param1Int1) {
      super(this$0, param1Int1);
    }
    
    public LayoutParams(ResolverDrawerLayout this$0) {
      super((ViewGroup.MarginLayoutParams)this$0);
      this.alwaysShow = ((LayoutParams)this$0).alwaysShow;
      this.ignoreOffset = ((LayoutParams)this$0).ignoreOffset;
      this.hasNestedScrollIndicator = ((LayoutParams)this$0).hasNestedScrollIndicator;
      this.maxHeight = ((LayoutParams)this$0).maxHeight;
    }
    
    public LayoutParams(ResolverDrawerLayout this$0) {
      super((ViewGroup.MarginLayoutParams)this$0);
    }
    
    public LayoutParams(ResolverDrawerLayout this$0) {
      super((ViewGroup.LayoutParams)this$0);
    }
  }
  
  class SavedState extends View.BaseSavedState {
    SavedState() {
      super((Parcelable)this$0);
    }
    
    private SavedState(ResolverDrawerLayout this$0) {
      super((Parcel)this$0);
      boolean bool;
      if (this$0.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      this.open = bool;
      this.mCollapsibleHeightReserved = this$0.readInt();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeInt(this.open);
      param1Parcel.writeInt(this.mCollapsibleHeightReserved);
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = (Parcelable.Creator<SavedState>)new Object();
    
    private int mCollapsibleHeightReserved;
    
    boolean open;
  }
  
  class RunOnDismissedListener implements Runnable {
    final ResolverDrawerLayout this$0;
    
    private RunOnDismissedListener() {}
    
    public void run() {
      ResolverDrawerLayout.this.dispatchOnDismissed();
    }
  }
  
  private MetricsLogger getMetricsLogger() {
    if (this.mMetricsLogger == null)
      this.mMetricsLogger = new MetricsLogger(); 
    return this.mMetricsLogger;
  }
  
  class OnCollapsedChangedListener {
    public abstract void onCollapsedChanged(boolean param1Boolean);
  }
  
  class OnDismissedListener {
    public abstract void onDismissed();
  }
}
