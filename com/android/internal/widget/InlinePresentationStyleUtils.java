package com.android.internal.widget;

import android.os.Bundle;
import java.util.Objects;
import java.util.Set;

public final class InlinePresentationStyleUtils {
  public static boolean bundleEquals(Bundle paramBundle1, Bundle paramBundle2) {
    if (paramBundle1 == paramBundle2)
      return true; 
    if (paramBundle1 == null || paramBundle2 == null)
      return false; 
    if (paramBundle1.size() != paramBundle2.size())
      return false; 
    Set set = paramBundle1.keySet();
    for (String str : set) {
      boolean bool;
      Object object1 = paramBundle1.get(str);
      Object object2 = paramBundle2.get(str);
      if (object1 instanceof Bundle && object2 instanceof Bundle) {
        bool = bundleEquals((Bundle)object1, (Bundle)object2);
      } else {
        bool = Objects.equals(object1, object2);
      } 
      if (!bool)
        return false; 
    } 
    return true;
  }
  
  public static void filterContentTypes(Bundle paramBundle) {
    if (paramBundle == null)
      return; 
    for (String str : paramBundle.keySet()) {
      Object object = paramBundle.get(str);
      if (object instanceof Bundle) {
        filterContentTypes((Bundle)object);
        continue;
      } 
      if (object instanceof android.os.IBinder)
        paramBundle.remove(str); 
    } 
  }
}
