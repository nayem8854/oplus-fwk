package com.android.internal.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AccountItemView extends LinearLayout {
  private ImageView mAccountIcon;
  
  private TextView mAccountName;
  
  private TextView mAccountNumber;
  
  public AccountItemView(Context paramContext) {
    this(paramContext, null);
  }
  
  public AccountItemView(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    LayoutInflater layoutInflater = (LayoutInflater)paramContext.getSystemService("layout_inflater");
    View view = layoutInflater.inflate(17367302, null);
    addView(view);
    initViewItem(view);
  }
  
  private void initViewItem(View paramView) {
    this.mAccountIcon = (ImageView)paramView.findViewById(16908294);
    this.mAccountName = (TextView)paramView.findViewById(16908310);
    this.mAccountNumber = (TextView)paramView.findViewById(16908304);
  }
  
  public void setViewItem(AccountViewAdapter.AccountElements paramAccountElements) {
    Drawable drawable = paramAccountElements.getDrawable();
    if (drawable != null) {
      setAccountIcon(drawable);
    } else {
      setAccountIcon(paramAccountElements.getIcon());
    } 
    setAccountName(paramAccountElements.getName());
    setAccountNumber(paramAccountElements.getNumber());
  }
  
  public void setAccountIcon(int paramInt) {
    this.mAccountIcon.setImageResource(paramInt);
  }
  
  public void setAccountIcon(Drawable paramDrawable) {
    this.mAccountIcon.setBackgroundDrawable(paramDrawable);
  }
  
  public void setAccountName(String paramString) {
    setText(this.mAccountName, paramString);
  }
  
  public void setAccountNumber(String paramString) {
    setText(this.mAccountNumber, paramString);
  }
  
  private void setText(TextView paramTextView, String paramString) {
    if (TextUtils.isEmpty(paramString)) {
      paramTextView.setVisibility(8);
    } else {
      paramTextView.setText(paramString);
      paramTextView.setVisibility(0);
    } 
  }
}
