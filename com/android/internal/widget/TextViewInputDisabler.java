package com.android.internal.widget;

import android.text.InputFilter;
import android.widget.TextView;

public class TextViewInputDisabler {
  private InputFilter[] mDefaultFilters;
  
  private InputFilter[] mNoInputFilters = new InputFilter[] { (InputFilter)new Object(this) };
  
  private TextView mTextView;
  
  public TextViewInputDisabler(TextView paramTextView) {
    this.mTextView = paramTextView;
    this.mDefaultFilters = paramTextView.getFilters();
  }
  
  public void setInputEnabled(boolean paramBoolean) {
    InputFilter[] arrayOfInputFilter;
    TextView textView = this.mTextView;
    if (paramBoolean) {
      arrayOfInputFilter = this.mDefaultFilters;
    } else {
      arrayOfInputFilter = this.mNoInputFilters;
    } 
    textView.setFilters(arrayOfInputFilter);
  }
}
