package com.android.internal.widget;

import android.app.Notification;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

public class MessagingMessageState {
  private MessagingGroup mGroup;
  
  private final View mHostView;
  
  private boolean mIsHidingAnimated;
  
  private boolean mIsHistoric;
  
  private Notification.MessagingStyle.Message mMessage;
  
  MessagingMessageState(View paramView) {
    this.mHostView = paramView;
  }
  
  public void setMessage(Notification.MessagingStyle.Message paramMessage) {
    this.mMessage = paramMessage;
  }
  
  public Notification.MessagingStyle.Message getMessage() {
    return this.mMessage;
  }
  
  public void setGroup(MessagingGroup paramMessagingGroup) {
    this.mGroup = paramMessagingGroup;
  }
  
  public MessagingGroup getGroup() {
    return this.mGroup;
  }
  
  public void setIsHistoric(boolean paramBoolean) {
    this.mIsHistoric = paramBoolean;
  }
  
  public void setIsHidingAnimated(boolean paramBoolean) {
    ViewParent viewParent = this.mHostView.getParent();
    this.mIsHidingAnimated = paramBoolean;
    this.mHostView.invalidate();
    if (viewParent instanceof ViewGroup)
      ((ViewGroup)viewParent).invalidate(); 
  }
  
  public boolean isHidingAnimated() {
    return this.mIsHidingAnimated;
  }
  
  public View getHostView() {
    return this.mHostView;
  }
  
  public void recycle() {
    this.mHostView.setAlpha(1.0F);
    this.mHostView.setTranslationY(0.0F);
    MessagingPropertyAnimator.recycle(this.mHostView);
    this.mIsHidingAnimated = false;
    this.mIsHistoric = false;
    this.mGroup = null;
    this.mMessage = null;
  }
}
