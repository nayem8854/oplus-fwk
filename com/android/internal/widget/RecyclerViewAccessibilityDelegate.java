package com.android.internal.widget;

import android.os.Bundle;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

public class RecyclerViewAccessibilityDelegate extends View.AccessibilityDelegate {
  final View.AccessibilityDelegate mItemDelegate;
  
  final RecyclerView mRecyclerView;
  
  public RecyclerViewAccessibilityDelegate(RecyclerView paramRecyclerView) {
    this.mItemDelegate = new View.AccessibilityDelegate() {
        final RecyclerViewAccessibilityDelegate this$0;
        
        public void onInitializeAccessibilityNodeInfo(View param1View, AccessibilityNodeInfo param1AccessibilityNodeInfo) {
          super.onInitializeAccessibilityNodeInfo(param1View, param1AccessibilityNodeInfo);
          if (!RecyclerViewAccessibilityDelegate.this.shouldIgnore() && RecyclerViewAccessibilityDelegate.this.mRecyclerView.getLayoutManager() != null) {
            RecyclerView.LayoutManager layoutManager = RecyclerViewAccessibilityDelegate.this.mRecyclerView.getLayoutManager();
            layoutManager.onInitializeAccessibilityNodeInfoForItem(param1View, param1AccessibilityNodeInfo);
          } 
        }
        
        public boolean performAccessibilityAction(View param1View, int param1Int, Bundle param1Bundle) {
          if (super.performAccessibilityAction(param1View, param1Int, param1Bundle))
            return true; 
          if (!RecyclerViewAccessibilityDelegate.this.shouldIgnore() && RecyclerViewAccessibilityDelegate.this.mRecyclerView.getLayoutManager() != null) {
            RecyclerView.LayoutManager layoutManager = RecyclerViewAccessibilityDelegate.this.mRecyclerView.getLayoutManager();
            return 
              layoutManager.performAccessibilityActionForItem(param1View, param1Int, param1Bundle);
          } 
          return false;
        }
      };
    this.mRecyclerView = paramRecyclerView;
  }
  
  boolean shouldIgnore() {
    return this.mRecyclerView.hasPendingAdapterUpdates();
  }
  
  public boolean performAccessibilityAction(View paramView, int paramInt, Bundle paramBundle) {
    if (super.performAccessibilityAction(paramView, paramInt, paramBundle))
      return true; 
    if (!shouldIgnore() && this.mRecyclerView.getLayoutManager() != null)
      return this.mRecyclerView.getLayoutManager().performAccessibilityAction(paramInt, paramBundle); 
    return false;
  }
  
  public void onInitializeAccessibilityNodeInfo(View paramView, AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    super.onInitializeAccessibilityNodeInfo(paramView, paramAccessibilityNodeInfo);
    paramAccessibilityNodeInfo.setClassName(RecyclerView.class.getName());
    if (!shouldIgnore() && this.mRecyclerView.getLayoutManager() != null)
      this.mRecyclerView.getLayoutManager().onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo); 
  }
  
  public void onInitializeAccessibilityEvent(View paramView, AccessibilityEvent paramAccessibilityEvent) {
    super.onInitializeAccessibilityEvent(paramView, paramAccessibilityEvent);
    paramAccessibilityEvent.setClassName(RecyclerView.class.getName());
    if (paramView instanceof RecyclerView && !shouldIgnore()) {
      RecyclerView recyclerView = (RecyclerView)paramView;
      if (recyclerView.getLayoutManager() != null)
        recyclerView.getLayoutManager().onInitializeAccessibilityEvent(paramAccessibilityEvent); 
    } 
  }
  
  public View.AccessibilityDelegate getItemDelegate() {
    return this.mItemDelegate;
  }
}
