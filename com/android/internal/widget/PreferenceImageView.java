package com.android.internal.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class PreferenceImageView extends ImageView {
  public PreferenceImageView(Context paramContext) {
    this(paramContext, null);
  }
  
  public PreferenceImageView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public PreferenceImageView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public PreferenceImageView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: iload_1
    //   1: invokestatic getMode : (I)I
    //   4: istore_3
    //   5: iload_3
    //   6: ldc -2147483648
    //   8: if_icmpeq -> 18
    //   11: iload_1
    //   12: istore #4
    //   14: iload_3
    //   15: ifne -> 63
    //   18: iload_1
    //   19: invokestatic getSize : (I)I
    //   22: istore #5
    //   24: aload_0
    //   25: invokevirtual getMaxWidth : ()I
    //   28: istore #6
    //   30: iload_1
    //   31: istore #4
    //   33: iload #6
    //   35: ldc 2147483647
    //   37: if_icmpeq -> 63
    //   40: iload #6
    //   42: iload #5
    //   44: if_icmplt -> 54
    //   47: iload_1
    //   48: istore #4
    //   50: iload_3
    //   51: ifne -> 63
    //   54: iload #6
    //   56: ldc -2147483648
    //   58: invokestatic makeMeasureSpec : (II)I
    //   61: istore #4
    //   63: iload_2
    //   64: invokestatic getMode : (I)I
    //   67: istore #5
    //   69: iload #5
    //   71: ldc -2147483648
    //   73: if_icmpeq -> 83
    //   76: iload_2
    //   77: istore_1
    //   78: iload #5
    //   80: ifne -> 124
    //   83: iload_2
    //   84: invokestatic getSize : (I)I
    //   87: istore_3
    //   88: aload_0
    //   89: invokevirtual getMaxHeight : ()I
    //   92: istore #6
    //   94: iload_2
    //   95: istore_1
    //   96: iload #6
    //   98: ldc 2147483647
    //   100: if_icmpeq -> 124
    //   103: iload #6
    //   105: iload_3
    //   106: if_icmplt -> 116
    //   109: iload_2
    //   110: istore_1
    //   111: iload #5
    //   113: ifne -> 124
    //   116: iload #6
    //   118: ldc -2147483648
    //   120: invokestatic makeMeasureSpec : (II)I
    //   123: istore_1
    //   124: aload_0
    //   125: iload #4
    //   127: iload_1
    //   128: invokespecial onMeasure : (II)V
    //   131: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #49	-> 0
    //   #50	-> 5
    //   #51	-> 18
    //   #52	-> 24
    //   #53	-> 30
    //   #55	-> 54
    //   #59	-> 63
    //   #60	-> 69
    //   #61	-> 83
    //   #62	-> 88
    //   #63	-> 94
    //   #65	-> 116
    //   #69	-> 124
    //   #70	-> 131
  }
}
