package com.android.internal.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

public class OplusBaseNotificationActionListLayout extends LinearLayout {
  public int mMaxChildHeight = 0;
  
  public OplusBaseNotificationActionListLayout(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public OplusBaseNotificationActionListLayout(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public OplusBaseNotificationActionListLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public OplusBaseNotificationActionListLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  public void resetCurrentMaxChildHeight(View paramView) {
    if (paramView.getMeasuredHeight() >= this.mMaxChildHeight)
      this.mMaxChildHeight = paramView.getMeasuredHeight(); 
  }
  
  public void setActionLayoutMeasuredDimension(int paramInt1, int paramInt2) {
    if (this.mMaxChildHeight != 0) {
      setMeasuredDimension(resolveSize(getSuggestedMinimumWidth(), paramInt1), this.mMaxChildHeight);
    } else {
      paramInt1 = resolveSize(getSuggestedMinimumWidth(), paramInt1);
      paramInt2 = resolveSize(getSuggestedMinimumHeight(), paramInt2);
      setMeasuredDimension(paramInt1, paramInt2);
    } 
  }
}
