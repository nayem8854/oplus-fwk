package com.android.internal.widget;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

public class LinearLayoutWithDefaultTouchRecepient extends LinearLayout {
  private View mDefaultTouchRecepient;
  
  private final Rect mTempRect = new Rect();
  
  public LinearLayoutWithDefaultTouchRecepient(Context paramContext) {
    super(paramContext);
  }
  
  public LinearLayoutWithDefaultTouchRecepient(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  public void setDefaultTouchRecepient(View paramView) {
    this.mDefaultTouchRecepient = paramView;
  }
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent) {
    if (this.mDefaultTouchRecepient == null)
      return super.dispatchTouchEvent(paramMotionEvent); 
    if (super.dispatchTouchEvent(paramMotionEvent))
      return true; 
    this.mTempRect.set(0, 0, 0, 0);
    offsetRectIntoDescendantCoords(this.mDefaultTouchRecepient, this.mTempRect);
    paramMotionEvent.setLocation(paramMotionEvent.getX() + this.mTempRect.left, paramMotionEvent.getY() + this.mTempRect.top);
    return this.mDefaultTouchRecepient.dispatchTouchEvent(paramMotionEvent);
  }
}
