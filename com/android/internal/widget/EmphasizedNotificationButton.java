package com.android.internal.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.DrawableWrapper;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.RippleDrawable;
import android.util.AttributeSet;
import android.view.RemotableViewMethod;
import android.widget.Button;
import android.widget.RemoteViews.RemoteView;

@RemoteView
public class EmphasizedNotificationButton extends Button {
  private final RippleDrawable mRipple;
  
  private final int mStrokeColor;
  
  private final int mStrokeWidth;
  
  public EmphasizedNotificationButton(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public EmphasizedNotificationButton(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public EmphasizedNotificationButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public EmphasizedNotificationButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    DrawableWrapper drawableWrapper = (DrawableWrapper)getBackground().mutate();
    this.mRipple = (RippleDrawable)drawableWrapper.getDrawable();
    this.mStrokeWidth = getResources().getDimensionPixelSize(17105182);
    this.mStrokeColor = getContext().getColor(17170872);
    this.mRipple.mutate();
  }
  
  @RemotableViewMethod
  public void setRippleColor(ColorStateList paramColorStateList) {
    this.mRipple.setColor(paramColorStateList);
    invalidate();
  }
  
  @RemotableViewMethod
  public void setButtonBackground(ColorStateList paramColorStateList) {
    GradientDrawable gradientDrawable = (GradientDrawable)this.mRipple.getDrawable(0);
    gradientDrawable.setColor(paramColorStateList);
    invalidate();
  }
  
  @RemotableViewMethod
  public void setHasStroke(boolean paramBoolean) {
    RippleDrawable rippleDrawable = this.mRipple;
    int i = 0;
    GradientDrawable gradientDrawable = (GradientDrawable)rippleDrawable.getDrawable(0);
    if (paramBoolean)
      i = this.mStrokeWidth; 
    gradientDrawable.setStroke(i, this.mStrokeColor);
    invalidate();
  }
}
