package com.android.internal.widget;

import android.os.AsyncTask;
import android.os.Trace;
import android.util.Slog;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public final class LockPatternChecker {
  public static interface OnVerifyCallback {
    void onVerified(byte[] param1ArrayOfbyte, int param1Int);
  }
  
  public static interface OnCheckCallback {
    default void onEarlyMatched() {}
    
    void onChecked(boolean param1Boolean, int param1Int);
    
    default void onCancelled() {}
  }
  
  public static AsyncTask<?, ?, ?> verifyCredential(LockPatternUtils paramLockPatternUtils, LockscreenCredential paramLockscreenCredential, long paramLong, int paramInt, OnVerifyCallback paramOnVerifyCallback) {
    paramLockscreenCredential = paramLockscreenCredential.duplicate();
    Object object = new Object(paramLockPatternUtils, paramLockscreenCredential, paramLong, paramInt, paramOnVerifyCallback);
    object.execute((Object[])new Void[0]);
    return (AsyncTask<?, ?, ?>)object;
  }
  
  public static AsyncTask<?, ?, ?> checkCredential(LockPatternUtils paramLockPatternUtils, LockscreenCredential paramLockscreenCredential, int paramInt, OnCheckCallback paramOnCheckCallback) {
    Trace.traceBegin(4L, "checkPattern.task.created");
    paramLockscreenCredential = paramLockscreenCredential.duplicate();
    Object object = new Object(paramLockPatternUtils, paramLockscreenCredential, paramInt, paramOnCheckCallback);
    object.executeOnExecutor(Executors.newCachedThreadPool(new ThreadFactory() {
            private final AtomicInteger mCount = new AtomicInteger(1);
            
            public Thread newThread(Runnable param1Runnable) {
              Slog.d("checkPattern", "executeOnExecutor.ThreadFactory");
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("LockPatternChecker-checkPattern #");
              stringBuilder.append(this.mCount.getAndIncrement());
              param1Runnable = new Thread(param1Runnable, stringBuilder.toString());
              param1Runnable.setPriority(9);
              return (Thread)param1Runnable;
            }
          }), (Object[])new Void[0]);
    Trace.traceEnd(4L);
    return (AsyncTask<?, ?, ?>)object;
  }
  
  public static AsyncTask<?, ?, ?> verifyTiedProfileChallenge(LockPatternUtils paramLockPatternUtils, LockscreenCredential paramLockscreenCredential, long paramLong, int paramInt, OnVerifyCallback paramOnVerifyCallback) {
    paramLockscreenCredential = paramLockscreenCredential.duplicate();
    Object object = new Object(paramLockPatternUtils, paramLockscreenCredential, paramLong, paramInt, paramOnVerifyCallback);
    object.execute((Object[])new Void[0]);
    return (AsyncTask<?, ?, ?>)object;
  }
}
