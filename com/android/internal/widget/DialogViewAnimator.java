package com.android.internal.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ViewAnimator;
import java.util.ArrayList;

public class DialogViewAnimator extends ViewAnimator {
  private final ArrayList<View> mMatchParentChildren = new ArrayList<>(1);
  
  public DialogViewAnimator(Context paramContext) {
    super(paramContext);
  }
  
  public DialogViewAnimator(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    if (View.MeasureSpec.getMode(paramInt1) != 1073741824 || 
      View.MeasureSpec.getMode(paramInt2) != 1073741824) {
      i = 1;
    } else {
      i = 0;
    } 
    int j = getChildCount();
    int k, m, n, i1;
    for (k = 0, m = 0, n = 0, i1 = 0; i1 < j; i1++, m = i4) {
      int i4;
      View view = getChildAt(i1);
      if (getMeasureAllChildren() || view.getVisibility() != 8) {
        boolean bool;
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams)view.getLayoutParams();
        if (layoutParams.width == -1) {
          i5 = 1;
        } else {
          i5 = 0;
        } 
        if (layoutParams.height == -1) {
          bool = true;
        } else {
          bool = false;
        } 
        if (i && (i5 || bool))
          this.mMatchParentChildren.add(view); 
        measureChildWithMargins(view, paramInt1, 0, paramInt2, 0);
        byte b = 0;
        int i6 = b;
        i4 = m;
        if (i) {
          i6 = b;
          i4 = m;
          if (!i5) {
            i4 = Math.max(m, view.getMeasuredWidth() + layoutParams.leftMargin + layoutParams.rightMargin);
            i6 = 0x0 | view.getMeasuredWidthAndState() & 0xFF000000;
          } 
        } 
        int i5 = i6;
        m = k;
        if (i) {
          i5 = i6;
          m = k;
          if (!bool) {
            m = Math.max(k, view.getMeasuredHeight() + layoutParams.topMargin + layoutParams.bottomMargin);
            i5 = i6 | view.getMeasuredHeightAndState() >> 16 & 0xFFFFFF00;
          } 
        } 
        n = combineMeasuredStates(n, i5);
        k = m;
      } else {
        i4 = m;
      } 
    } 
    int i = getPaddingLeft(), i3 = getPaddingRight();
    int i2 = getPaddingTop();
    i1 = getPaddingBottom();
    i2 = Math.max(k + i2 + i1, getSuggestedMinimumHeight());
    i3 = Math.max(m + i + i3, getSuggestedMinimumWidth());
    Drawable drawable = getForeground();
    m = i2;
    k = i3;
    if (drawable != null) {
      m = Math.max(i2, drawable.getMinimumHeight());
      k = Math.max(i3, drawable.getMinimumWidth());
    } 
    k = resolveSizeAndState(k, paramInt1, n);
    m = resolveSizeAndState(m, paramInt2, n << 16);
    setMeasuredDimension(k, m);
    i3 = this.mMatchParentChildren.size();
    for (k = 0; k < i3; k++) {
      View view = this.mMatchParentChildren.get(k);
      ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams)view.getLayoutParams();
      if (marginLayoutParams.width == -1) {
        i1 = getMeasuredWidth();
        i = getPaddingLeft();
        n = getPaddingRight();
        i2 = marginLayoutParams.leftMargin;
        m = marginLayoutParams.rightMargin;
        m = View.MeasureSpec.makeMeasureSpec(i1 - i - n - i2 - m, 1073741824);
      } else {
        m = getPaddingLeft();
        i2 = getPaddingRight();
        i = marginLayoutParams.leftMargin;
        n = marginLayoutParams.rightMargin;
        i1 = marginLayoutParams.width;
        m = getChildMeasureSpec(paramInt1, m + i2 + i + n, i1);
      } 
      if (marginLayoutParams.height == -1) {
        i1 = getMeasuredHeight();
        int i4 = getPaddingTop();
        n = getPaddingBottom();
        i2 = marginLayoutParams.topMargin;
        i = marginLayoutParams.bottomMargin;
        i2 = View.MeasureSpec.makeMeasureSpec(i1 - i4 - n - i2 - i, 1073741824);
      } else {
        i1 = getPaddingTop();
        n = getPaddingBottom();
        i2 = marginLayoutParams.topMargin;
        i = marginLayoutParams.bottomMargin;
        int i4 = marginLayoutParams.height;
        i2 = getChildMeasureSpec(paramInt2, i1 + n + i2 + i, i4);
      } 
      view.measure(m, i2);
    } 
    this.mMatchParentChildren.clear();
  }
}
