package com.android.internal.widget;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.app.ActionBar;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.android.internal.view.ActionBarPolicy;

public class ScrollingTabContainerView extends HorizontalScrollView implements AdapterView.OnItemClickListener {
  protected Animator mVisibilityAnim;
  
  protected final VisibilityAnimListener mVisAnimListener = new VisibilityAnimListener();
  
  private Spinner mTabSpinner;
  
  Runnable mTabSelector;
  
  private LinearLayout mTabLayout;
  
  private TabClickListener mTabClickListener;
  
  int mStackedTabMaxWidth;
  
  private int mSelectedTabIndex;
  
  int mMaxTabWidth;
  
  private int mContentHeight;
  
  private boolean mAllowCollapse;
  
  private static final TimeInterpolator sAlphaInterpolator = (TimeInterpolator)new DecelerateInterpolator();
  
  private static final String TAG = "ScrollingTabContainerView";
  
  private static final int FADE_DURATION = 200;
  
  public ScrollingTabContainerView(Context paramContext) {
    super(paramContext);
    setHorizontalScrollBarEnabled(false);
    ActionBarPolicy actionBarPolicy = ActionBarPolicy.get(paramContext);
    setContentHeight(actionBarPolicy.getTabContainerHeight());
    this.mStackedTabMaxWidth = actionBarPolicy.getStackedTabMaxWidth();
    LinearLayout linearLayout = createTabLayout();
    addView((View)linearLayout, new ViewGroup.LayoutParams(-2, -1));
  }
  
  public void onMeasure(int paramInt1, int paramInt2) {
    boolean bool;
    int i = View.MeasureSpec.getMode(paramInt1);
    paramInt2 = 1;
    if (i == 1073741824) {
      bool = true;
    } else {
      bool = false;
    } 
    setFillViewport(bool);
    int j = this.mTabLayout.getChildCount();
    if (j > 1 && (i == 1073741824 || i == Integer.MIN_VALUE)) {
      if (j > 2) {
        this.mMaxTabWidth = (int)(View.MeasureSpec.getSize(paramInt1) * 0.4F);
      } else {
        this.mMaxTabWidth = View.MeasureSpec.getSize(paramInt1) / 2;
      } 
      this.mMaxTabWidth = Math.min(this.mMaxTabWidth, this.mStackedTabMaxWidth);
    } else {
      this.mMaxTabWidth = -1;
    } 
    j = View.MeasureSpec.makeMeasureSpec(this.mContentHeight, 1073741824);
    if (bool || !this.mAllowCollapse)
      paramInt2 = 0; 
    if (paramInt2 != 0) {
      this.mTabLayout.measure(0, j);
      if (this.mTabLayout.getMeasuredWidth() > View.MeasureSpec.getSize(paramInt1)) {
        performCollapse();
      } else {
        performExpand();
      } 
    } else {
      performExpand();
    } 
    paramInt2 = getMeasuredWidth();
    super.onMeasure(paramInt1, j);
    paramInt1 = getMeasuredWidth();
    if (bool && paramInt2 != paramInt1)
      setTabSelected(this.mSelectedTabIndex); 
  }
  
  private boolean isCollapsed() {
    boolean bool;
    Spinner spinner = this.mTabSpinner;
    if (spinner != null && spinner.getParent() == this) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setAllowCollapse(boolean paramBoolean) {
    this.mAllowCollapse = paramBoolean;
  }
  
  private void performCollapse() {
    if (isCollapsed())
      return; 
    if (this.mTabSpinner == null)
      this.mTabSpinner = createSpinner(); 
    removeView((View)this.mTabLayout);
    addView((View)this.mTabSpinner, new ViewGroup.LayoutParams(-2, -1));
    if (this.mTabSpinner.getAdapter() == null) {
      TabAdapter tabAdapter = new TabAdapter(this.mContext);
      tabAdapter.setDropDownViewContext(this.mTabSpinner.getPopupContext());
      this.mTabSpinner.setAdapter((SpinnerAdapter)tabAdapter);
    } 
    Runnable runnable = this.mTabSelector;
    if (runnable != null) {
      removeCallbacks(runnable);
      this.mTabSelector = null;
    } 
    this.mTabSpinner.setSelection(this.mSelectedTabIndex);
  }
  
  private boolean performExpand() {
    if (!isCollapsed())
      return false; 
    removeView((View)this.mTabSpinner);
    addView((View)this.mTabLayout, new ViewGroup.LayoutParams(-2, -1));
    setTabSelected(this.mTabSpinner.getSelectedItemPosition());
    return false;
  }
  
  public void setTabSelected(int paramInt) {
    this.mSelectedTabIndex = paramInt;
    int i = this.mTabLayout.getChildCount();
    for (byte b = 0; b < i; b++) {
      boolean bool;
      View view = this.mTabLayout.getChildAt(b);
      if (b == paramInt) {
        bool = true;
      } else {
        bool = false;
      } 
      view.setSelected(bool);
      if (bool)
        animateToTab(paramInt); 
    } 
    Spinner spinner = this.mTabSpinner;
    if (spinner != null && paramInt >= 0)
      spinner.setSelection(paramInt); 
  }
  
  public void setContentHeight(int paramInt) {
    this.mContentHeight = paramInt;
    requestLayout();
  }
  
  private LinearLayout createTabLayout() {
    LinearLayout linearLayout = new LinearLayout(getContext(), null, 16843508);
    linearLayout.setMeasureWithLargestChildEnabled(true);
    linearLayout.setGravity(17);
    linearLayout.setLayoutParams((ViewGroup.LayoutParams)new LinearLayout.LayoutParams(-2, -1));
    return linearLayout;
  }
  
  private Spinner createSpinner() {
    Spinner spinner = new Spinner(getContext(), null, 16843479);
    spinner.setLayoutParams((ViewGroup.LayoutParams)new LinearLayout.LayoutParams(-2, -1));
    spinner.setOnItemClickListenerInt(this);
    return spinner;
  }
  
  protected void onConfigurationChanged(Configuration paramConfiguration) {
    super.onConfigurationChanged(paramConfiguration);
    ActionBarPolicy actionBarPolicy = ActionBarPolicy.get(getContext());
    setContentHeight(actionBarPolicy.getTabContainerHeight());
    this.mStackedTabMaxWidth = actionBarPolicy.getStackedTabMaxWidth();
  }
  
  public void animateToVisibility(int paramInt) {
    Animator animator = this.mVisibilityAnim;
    if (animator != null)
      animator.cancel(); 
    if (paramInt == 0) {
      if (getVisibility() != 0)
        setAlpha(0.0F); 
      ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(this, "alpha", new float[] { 1.0F });
      objectAnimator.setDuration(200L);
      objectAnimator.setInterpolator(sAlphaInterpolator);
      objectAnimator.addListener(this.mVisAnimListener.withFinalVisibility(paramInt));
      objectAnimator.start();
    } else {
      ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(this, "alpha", new float[] { 0.0F });
      objectAnimator.setDuration(200L);
      objectAnimator.setInterpolator(sAlphaInterpolator);
      objectAnimator.addListener(this.mVisAnimListener.withFinalVisibility(paramInt));
      objectAnimator.start();
    } 
  }
  
  public void animateToTab(int paramInt) {
    View view = this.mTabLayout.getChildAt(paramInt);
    Runnable runnable = this.mTabSelector;
    if (runnable != null)
      removeCallbacks(runnable); 
    Object object = new Object(this, view);
    post((Runnable)object);
  }
  
  public void onAttachedToWindow() {
    super.onAttachedToWindow();
    Runnable runnable = this.mTabSelector;
    if (runnable != null)
      post(runnable); 
  }
  
  public void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    Runnable runnable = this.mTabSelector;
    if (runnable != null)
      removeCallbacks(runnable); 
  }
  
  private TabView createTabView(Context paramContext, ActionBar.Tab paramTab, boolean paramBoolean) {
    TabView tabView = new TabView(paramContext, paramTab, paramBoolean);
    if (paramBoolean) {
      tabView.setBackgroundDrawable(null);
      tabView.setLayoutParams((ViewGroup.LayoutParams)new AbsListView.LayoutParams(-1, this.mContentHeight));
    } else {
      tabView.setFocusable(true);
      if (this.mTabClickListener == null)
        this.mTabClickListener = new TabClickListener(); 
      tabView.setOnClickListener(this.mTabClickListener);
    } 
    return tabView;
  }
  
  public void addTab(ActionBar.Tab paramTab, boolean paramBoolean) {
    TabView tabView = createTabView(this.mContext, paramTab, false);
    this.mTabLayout.addView((View)tabView, (ViewGroup.LayoutParams)new LinearLayout.LayoutParams(0, -1, 1.0F));
    Spinner spinner = this.mTabSpinner;
    if (spinner != null)
      ((TabAdapter)spinner.getAdapter()).notifyDataSetChanged(); 
    if (paramBoolean)
      tabView.setSelected(true); 
    if (this.mAllowCollapse)
      requestLayout(); 
  }
  
  public void addTab(ActionBar.Tab paramTab, int paramInt, boolean paramBoolean) {
    TabView tabView = createTabView(this.mContext, paramTab, false);
    this.mTabLayout.addView((View)tabView, paramInt, (ViewGroup.LayoutParams)new LinearLayout.LayoutParams(0, -1, 1.0F));
    Spinner spinner = this.mTabSpinner;
    if (spinner != null)
      ((TabAdapter)spinner.getAdapter()).notifyDataSetChanged(); 
    if (paramBoolean)
      tabView.setSelected(true); 
    if (this.mAllowCollapse)
      requestLayout(); 
  }
  
  public void updateTab(int paramInt) {
    ((TabView)this.mTabLayout.getChildAt(paramInt)).update();
    Spinner spinner = this.mTabSpinner;
    if (spinner != null)
      ((TabAdapter)spinner.getAdapter()).notifyDataSetChanged(); 
    if (this.mAllowCollapse)
      requestLayout(); 
  }
  
  public void removeTabAt(int paramInt) {
    this.mTabLayout.removeViewAt(paramInt);
    Spinner spinner = this.mTabSpinner;
    if (spinner != null)
      ((TabAdapter)spinner.getAdapter()).notifyDataSetChanged(); 
    if (this.mAllowCollapse)
      requestLayout(); 
  }
  
  public void removeAllTabs() {
    this.mTabLayout.removeAllViews();
    Spinner spinner = this.mTabSpinner;
    if (spinner != null)
      ((TabAdapter)spinner.getAdapter()).notifyDataSetChanged(); 
    if (this.mAllowCollapse)
      requestLayout(); 
  }
  
  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong) {
    TabView tabView = (TabView)paramView;
    tabView.getTab().select();
  }
  
  class TabView extends LinearLayout {
    private View mCustomView;
    
    private ImageView mIconView;
    
    private ActionBar.Tab mTab;
    
    private TextView mTextView;
    
    final ScrollingTabContainerView this$0;
    
    public TabView(Context param1Context, ActionBar.Tab param1Tab, boolean param1Boolean) {
      super(param1Context, null, 16843507);
      this.mTab = param1Tab;
      if (param1Boolean)
        setGravity(8388627); 
      update();
    }
    
    public void bindTab(ActionBar.Tab param1Tab) {
      this.mTab = param1Tab;
      update();
    }
    
    public void setSelected(boolean param1Boolean) {
      boolean bool;
      if (isSelected() != param1Boolean) {
        bool = true;
      } else {
        bool = false;
      } 
      super.setSelected(param1Boolean);
      if (bool && param1Boolean)
        sendAccessibilityEvent(4); 
    }
    
    public CharSequence getAccessibilityClassName() {
      return ActionBar.Tab.class.getName();
    }
    
    public void onMeasure(int param1Int1, int param1Int2) {
      super.onMeasure(param1Int1, param1Int2);
      if (ScrollingTabContainerView.this.mMaxTabWidth > 0 && getMeasuredWidth() > ScrollingTabContainerView.this.mMaxTabWidth)
        super.onMeasure(View.MeasureSpec.makeMeasureSpec(ScrollingTabContainerView.this.mMaxTabWidth, 1073741824), param1Int2); 
    }
    
    public void update() {
      ActionBar.Tab tab = this.mTab;
      View view = tab.getCustomView();
      ViewParent viewParent = null;
      if (view != null) {
        viewParent = view.getParent();
        if (viewParent != this) {
          if (viewParent != null)
            ((ViewGroup)viewParent).removeView(view); 
          addView(view);
        } 
        this.mCustomView = view;
        TextView textView = this.mTextView;
        if (textView != null)
          textView.setVisibility(8); 
        ImageView imageView = this.mIconView;
        if (imageView != null) {
          imageView.setVisibility(8);
          this.mIconView.setImageDrawable(null);
        } 
      } else {
        CharSequence charSequence2;
        view = this.mCustomView;
        if (view != null) {
          removeView(view);
          this.mCustomView = null;
        } 
        Drawable drawable = tab.getIcon();
        CharSequence charSequence1 = tab.getText();
        if (drawable != null) {
          if (this.mIconView == null) {
            ImageView imageView1 = new ImageView(getContext());
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
            layoutParams.gravity = 16;
            imageView1.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
            addView((View)imageView1, 0);
            this.mIconView = imageView1;
          } 
          this.mIconView.setImageDrawable(drawable);
          this.mIconView.setVisibility(0);
        } else {
          ImageView imageView1 = this.mIconView;
          if (imageView1 != null) {
            imageView1.setVisibility(8);
            this.mIconView.setImageDrawable(null);
          } 
        } 
        int i = TextUtils.isEmpty(charSequence1) ^ true;
        if (i != 0) {
          if (this.mTextView == null) {
            TextView textView = new TextView(getContext(), null, 16843509);
            textView.setEllipsize(TextUtils.TruncateAt.END);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
            layoutParams.gravity = 16;
            textView.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
            addView((View)textView);
            this.mTextView = textView;
          } 
          this.mTextView.setText(charSequence1);
          this.mTextView.setVisibility(0);
        } else {
          TextView textView = this.mTextView;
          if (textView != null) {
            textView.setVisibility(8);
            this.mTextView.setText(null);
          } 
        } 
        ImageView imageView = this.mIconView;
        if (imageView != null)
          imageView.setContentDescription(tab.getContentDescription()); 
        if (i == 0)
          charSequence2 = tab.getContentDescription(); 
        setTooltipText(charSequence2);
      } 
    }
    
    public ActionBar.Tab getTab() {
      return this.mTab;
    }
  }
  
  class TabAdapter extends BaseAdapter {
    private Context mDropDownContext;
    
    final ScrollingTabContainerView this$0;
    
    public TabAdapter(Context param1Context) {
      setDropDownViewContext(param1Context);
    }
    
    public void setDropDownViewContext(Context param1Context) {
      this.mDropDownContext = param1Context;
    }
    
    public int getCount() {
      return ScrollingTabContainerView.this.mTabLayout.getChildCount();
    }
    
    public Object getItem(int param1Int) {
      return ((ScrollingTabContainerView.TabView)ScrollingTabContainerView.this.mTabLayout.getChildAt(param1Int)).getTab();
    }
    
    public long getItemId(int param1Int) {
      return param1Int;
    }
    
    public View getView(int param1Int, View param1View, ViewGroup param1ViewGroup) {
      ScrollingTabContainerView.TabView tabView;
      if (param1View == null) {
        ScrollingTabContainerView scrollingTabContainerView = ScrollingTabContainerView.this;
        tabView = scrollingTabContainerView.createTabView(scrollingTabContainerView.mContext, (ActionBar.Tab)getItem(param1Int), true);
      } else {
        tabView.bindTab((ActionBar.Tab)getItem(param1Int));
      } 
      return (View)tabView;
    }
    
    public View getDropDownView(int param1Int, View param1View, ViewGroup param1ViewGroup) {
      ScrollingTabContainerView.TabView tabView;
      if (param1View == null) {
        ScrollingTabContainerView scrollingTabContainerView = ScrollingTabContainerView.this;
        Context context = this.mDropDownContext;
        ActionBar.Tab tab = (ActionBar.Tab)getItem(param1Int);
        tabView = scrollingTabContainerView.createTabView(context, tab, true);
      } else {
        tabView.bindTab((ActionBar.Tab)getItem(param1Int));
      } 
      return (View)tabView;
    }
  }
  
  class TabClickListener implements View.OnClickListener {
    final ScrollingTabContainerView this$0;
    
    private TabClickListener() {}
    
    public void onClick(View param1View) {
      ScrollingTabContainerView.TabView tabView = (ScrollingTabContainerView.TabView)param1View;
      tabView.getTab().select();
      int i = ScrollingTabContainerView.this.mTabLayout.getChildCount();
      for (byte b = 0; b < i; b++) {
        boolean bool;
        View view = ScrollingTabContainerView.this.mTabLayout.getChildAt(b);
        if (view == param1View) {
          bool = true;
        } else {
          bool = false;
        } 
        view.setSelected(bool);
      } 
    }
  }
  
  class VisibilityAnimListener implements Animator.AnimatorListener {
    private boolean mCanceled;
    
    private int mFinalVisibility;
    
    final ScrollingTabContainerView this$0;
    
    protected VisibilityAnimListener() {
      this.mCanceled = false;
    }
    
    public VisibilityAnimListener withFinalVisibility(int param1Int) {
      this.mFinalVisibility = param1Int;
      return this;
    }
    
    public void onAnimationStart(Animator param1Animator) {
      ScrollingTabContainerView.this.setVisibility(0);
      ScrollingTabContainerView.this.mVisibilityAnim = param1Animator;
      this.mCanceled = false;
    }
    
    public void onAnimationEnd(Animator param1Animator) {
      if (this.mCanceled)
        return; 
      ScrollingTabContainerView.this.mVisibilityAnim = null;
      ScrollingTabContainerView.this.setVisibility(this.mFinalVisibility);
    }
    
    public void onAnimationCancel(Animator param1Animator) {
      this.mCanceled = true;
    }
    
    public void onAnimationRepeat(Animator param1Animator) {}
  }
}
