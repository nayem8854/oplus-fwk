package com.android.internal.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.RemotableViewMethod;
import android.widget.ImageView;
import android.widget.RemoteViews.RemoteView;
import java.util.function.Consumer;

@RemoteView
public class CachingIconView extends ImageView {
  private int mDesiredVisibility;
  
  private boolean mForceHidden;
  
  private int mIconColor;
  
  private boolean mInternalSetDrawable;
  
  private String mLastPackage;
  
  private int mLastResId;
  
  private Consumer<Boolean> mOnForceHiddenChangedListener;
  
  private Consumer<Integer> mOnVisibilityChangedListener;
  
  private boolean mWillBeForceHidden;
  
  public CachingIconView(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  @RemotableViewMethod(asyncImpl = "setImageIconAsync")
  public void setImageIcon(Icon paramIcon) {
    if (!testAndSetCache(paramIcon)) {
      this.mInternalSetDrawable = true;
      super.setImageIcon(paramIcon);
      this.mInternalSetDrawable = false;
    } 
  }
  
  public Runnable setImageIconAsync(Icon paramIcon) {
    resetCache();
    return super.setImageIconAsync(paramIcon);
  }
  
  @RemotableViewMethod(asyncImpl = "setImageResourceAsync")
  public void setImageResource(int paramInt) {
    if (!testAndSetCache(paramInt)) {
      this.mInternalSetDrawable = true;
      super.setImageResource(paramInt);
      this.mInternalSetDrawable = false;
    } 
  }
  
  public Runnable setImageResourceAsync(int paramInt) {
    resetCache();
    return super.setImageResourceAsync(paramInt);
  }
  
  @RemotableViewMethod(asyncImpl = "setImageURIAsync")
  public void setImageURI(Uri paramUri) {
    resetCache();
    super.setImageURI(paramUri);
  }
  
  public Runnable setImageURIAsync(Uri paramUri) {
    resetCache();
    return super.setImageURIAsync(paramUri);
  }
  
  public void setImageDrawable(Drawable paramDrawable) {
    if (!this.mInternalSetDrawable)
      resetCache(); 
    super.setImageDrawable(paramDrawable);
  }
  
  @RemotableViewMethod
  public void setImageBitmap(Bitmap paramBitmap) {
    resetCache();
    super.setImageBitmap(paramBitmap);
  }
  
  protected void onConfigurationChanged(Configuration paramConfiguration) {
    super.onConfigurationChanged(paramConfiguration);
    resetCache();
  }
  
  private boolean testAndSetCache(Icon paramIcon) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: iconst_0
    //   3: istore_2
    //   4: aload_1
    //   5: ifnull -> 77
    //   8: aload_1
    //   9: invokevirtual getType : ()I
    //   12: iconst_2
    //   13: if_icmpne -> 77
    //   16: aload_0
    //   17: aload_1
    //   18: invokespecial normalizeIconPackage : (Landroid/graphics/drawable/Icon;)Ljava/lang/String;
    //   21: astore_3
    //   22: aload_0
    //   23: getfield mLastResId : I
    //   26: ifeq -> 60
    //   29: aload_1
    //   30: invokevirtual getResId : ()I
    //   33: aload_0
    //   34: getfield mLastResId : I
    //   37: if_icmpne -> 60
    //   40: aload_0
    //   41: getfield mLastPackage : Ljava/lang/String;
    //   44: astore #4
    //   46: aload_3
    //   47: aload #4
    //   49: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   52: ifeq -> 60
    //   55: iconst_1
    //   56: istore_2
    //   57: goto -> 60
    //   60: aload_0
    //   61: aload_3
    //   62: putfield mLastPackage : Ljava/lang/String;
    //   65: aload_0
    //   66: aload_1
    //   67: invokevirtual getResId : ()I
    //   70: putfield mLastResId : I
    //   73: aload_0
    //   74: monitorexit
    //   75: iload_2
    //   76: ireturn
    //   77: aload_0
    //   78: invokespecial resetCache : ()V
    //   81: aload_0
    //   82: monitorexit
    //   83: iconst_0
    //   84: ireturn
    //   85: astore_1
    //   86: aload_0
    //   87: monitorexit
    //   88: aload_1
    //   89: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #131	-> 2
    //   #132	-> 16
    //   #134	-> 22
    //   #135	-> 29
    //   #136	-> 46
    //   #138	-> 60
    //   #139	-> 65
    //   #141	-> 73
    //   #143	-> 77
    //   #144	-> 81
    //   #130	-> 85
    // Exception table:
    //   from	to	target	type
    //   8	16	85	finally
    //   16	22	85	finally
    //   22	29	85	finally
    //   29	46	85	finally
    //   46	55	85	finally
    //   60	65	85	finally
    //   65	73	85	finally
    //   77	81	85	finally
  }
  
  private boolean testAndSetCache(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: iload_1
    //   3: ifeq -> 45
    //   6: aload_0
    //   7: getfield mLastResId : I
    //   10: ifne -> 16
    //   13: goto -> 45
    //   16: iload_1
    //   17: aload_0
    //   18: getfield mLastResId : I
    //   21: if_icmpne -> 36
    //   24: aload_0
    //   25: getfield mLastPackage : Ljava/lang/String;
    //   28: ifnonnull -> 36
    //   31: iconst_1
    //   32: istore_2
    //   33: goto -> 38
    //   36: iconst_0
    //   37: istore_2
    //   38: goto -> 47
    //   41: astore_3
    //   42: goto -> 61
    //   45: iconst_0
    //   46: istore_2
    //   47: aload_0
    //   48: aconst_null
    //   49: putfield mLastPackage : Ljava/lang/String;
    //   52: aload_0
    //   53: iload_1
    //   54: putfield mLastResId : I
    //   57: aload_0
    //   58: monitorexit
    //   59: iload_2
    //   60: ireturn
    //   61: aload_0
    //   62: monitorexit
    //   63: aload_3
    //   64: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #153	-> 2
    //   #156	-> 16
    //   #152	-> 41
    //   #154	-> 45
    //   #158	-> 47
    //   #159	-> 52
    //   #160	-> 57
    //   #152	-> 61
    // Exception table:
    //   from	to	target	type
    //   6	13	41	finally
    //   16	31	41	finally
    //   47	52	41	finally
    //   52	57	41	finally
  }
  
  private String normalizeIconPackage(Icon paramIcon) {
    if (paramIcon == null)
      return null; 
    String str = paramIcon.getResPackage();
    if (TextUtils.isEmpty(str))
      return null; 
    if (str.equals(this.mContext.getPackageName()))
      return null; 
    return str;
  }
  
  private void resetCache() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iconst_0
    //   4: putfield mLastResId : I
    //   7: aload_0
    //   8: aconst_null
    //   9: putfield mLastPackage : Ljava/lang/String;
    //   12: aload_0
    //   13: monitorexit
    //   14: return
    //   15: astore_1
    //   16: aload_0
    //   17: monitorexit
    //   18: aload_1
    //   19: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #184	-> 2
    //   #185	-> 7
    //   #186	-> 12
    //   #183	-> 15
    // Exception table:
    //   from	to	target	type
    //   2	7	15	finally
    //   7	12	15	finally
  }
  
  public void setForceHidden(boolean paramBoolean) {
    if (paramBoolean != this.mForceHidden) {
      this.mForceHidden = paramBoolean;
      this.mWillBeForceHidden = false;
      updateVisibility();
      Consumer<Boolean> consumer = this.mOnForceHiddenChangedListener;
      if (consumer != null)
        consumer.accept(Boolean.valueOf(paramBoolean)); 
    } 
  }
  
  @RemotableViewMethod
  public void setVisibility(int paramInt) {
    this.mDesiredVisibility = paramInt;
    updateVisibility();
  }
  
  private void updateVisibility() {
    int i;
    if (this.mDesiredVisibility == 0 && this.mForceHidden) {
      i = 4;
    } else {
      i = this.mDesiredVisibility;
    } 
    Consumer<Integer> consumer = this.mOnVisibilityChangedListener;
    if (consumer != null)
      consumer.accept(Integer.valueOf(i)); 
    super.setVisibility(i);
  }
  
  public void setOnVisibilityChangedListener(Consumer<Integer> paramConsumer) {
    this.mOnVisibilityChangedListener = paramConsumer;
  }
  
  public void setOnForceHiddenChangedListener(Consumer<Boolean> paramConsumer) {
    this.mOnForceHiddenChangedListener = paramConsumer;
  }
  
  public boolean isForceHidden() {
    return this.mForceHidden;
  }
  
  @RemotableViewMethod
  public void setOriginalIconColor(int paramInt) {
    this.mIconColor = paramInt;
  }
  
  public int getOriginalIconColor() {
    return this.mIconColor;
  }
  
  public boolean willBeForceHidden() {
    return this.mWillBeForceHidden;
  }
  
  public void setWillBeForceHidden(boolean paramBoolean) {
    this.mWillBeForceHidden = paramBoolean;
  }
}
