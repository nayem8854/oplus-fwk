package com.android.internal.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.IOplusFtHooks;
import android.widget.ListView;

public class OplusFtHooksImpl implements IOplusFtHooks {
  private static int mConvertViewPosition;
  
  private static int mOverFlowMenuCount;
  
  public int getMinOverscrollSize() {
    return 3;
  }
  
  public int getMaxOverscrollSize() {
    return 3;
  }
  
  public Drawable getArrowDrawable(Context paramContext) {
    Resources resources = paramContext.getResources();
    return 
      resources.getDrawable(201851367, paramContext.getTheme());
  }
  
  public Drawable getOverflowDrawable(Context paramContext) {
    Resources resources = paramContext.getResources();
    return 
      resources.getDrawable(201851365, paramContext.getTheme());
  }
  
  public AnimatedVectorDrawable getToArrowAnim(Context paramContext) {
    Resources resources = paramContext.getResources();
    return 
      (AnimatedVectorDrawable)resources.getDrawable(201851366, paramContext.getTheme());
  }
  
  public AnimatedVectorDrawable getToOverflowAnim(Context paramContext) {
    Resources resources = paramContext.getResources();
    return 
      (AnimatedVectorDrawable)resources.getDrawable(201851368, paramContext.getTheme());
  }
  
  public int getFirstItemPaddingStart(Context paramContext, int paramInt) {
    int i = paramContext.getResources().getDimensionPixelSize(201654548);
    return i + paramInt;
  }
  
  public int getLastItemPaddingEnd(Context paramContext, int paramInt) {
    int i = paramContext.getResources().getDimensionPixelSize(201654547);
    return i + paramInt;
  }
  
  public void setOverflowMenuCount(int paramInt) {
    mOverFlowMenuCount = paramInt;
  }
  
  public int calOverflowExtension(int paramInt) {
    return 0;
  }
  
  public int getOverflowButtonRes() {
    return 202440750;
  }
  
  public void setOverflowScrollBarSize(ListView paramListView) {
    setListViewBackground(paramListView);
    Context context = paramListView.getContext();
    int i = context.getResources().getDimensionPixelSize(201654546);
    paramListView.setScrollBarSize(i);
  }
  
  public void setConvertViewPosition(int paramInt) {
    mConvertViewPosition = paramInt;
  }
  
  public void setConvertViewPadding(View paramView, boolean paramBoolean, int paramInt1, int paramInt2) {
    Resources resources = paramView.getResources();
    int i = resources.getDimensionPixelSize(201654543);
    paramInt2 = Math.max(i, paramInt2);
    paramView.setMinimumWidth(paramInt2);
    if (paramBoolean && mConvertViewPosition == 0) {
      paramView.setPadding(paramInt1, 18, paramInt1, 0);
    } else if (!paramBoolean && mConvertViewPosition == mOverFlowMenuCount - 1) {
      paramView.setPadding(paramInt1, 0, paramInt1, 18);
    } else {
      paramView.setPadding(paramInt1, 0, paramInt1, 0);
    } 
  }
  
  public void setScrollIndicators(ListView paramListView) {}
  
  public int getMenuItemButtonRes() {
    return 202440749;
  }
  
  public int getButtonTextId() {
    return 16909010;
  }
  
  public int getButtonIconId() {
    return 16909008;
  }
  
  public int getContentContainerRes() {
    return 202440748;
  }
  
  private void setListViewBackground(ListView paramListView) {
    ColorDrawable colorDrawable = new ColorDrawable(16777215);
    paramListView.setSelector((Drawable)colorDrawable);
  }
  
  public int getFloatingToolBarHeightRes() {
    return 201654667;
  }
}
