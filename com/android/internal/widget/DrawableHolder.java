package com.android.internal.widget;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.view.animation.DecelerateInterpolator;
import java.util.ArrayList;

public class DrawableHolder implements Animator.AnimatorListener {
  public static final DecelerateInterpolator EASE_OUT_INTERPOLATOR = new DecelerateInterpolator();
  
  private float mX = 0.0F;
  
  private float mY = 0.0F;
  
  private float mScaleX = 1.0F;
  
  private float mScaleY = 1.0F;
  
  private float mAlpha = 1.0F;
  
  private ArrayList<ObjectAnimator> mAnimators = new ArrayList<>();
  
  private ArrayList<ObjectAnimator> mNeedToStart = new ArrayList<>();
  
  private static final boolean DBG = false;
  
  private static final String TAG = "DrawableHolder";
  
  private BitmapDrawable mDrawable;
  
  public DrawableHolder(BitmapDrawable paramBitmapDrawable) {
    this(paramBitmapDrawable, 0.0F, 0.0F);
  }
  
  public DrawableHolder(BitmapDrawable paramBitmapDrawable, float paramFloat1, float paramFloat2) {
    this.mDrawable = paramBitmapDrawable;
    this.mX = paramFloat1;
    this.mY = paramFloat2;
    paramBitmapDrawable.getPaint().setAntiAlias(true);
    paramBitmapDrawable = this.mDrawable;
    paramBitmapDrawable.setBounds(0, 0, paramBitmapDrawable.getIntrinsicWidth(), this.mDrawable.getIntrinsicHeight());
  }
  
  public ObjectAnimator addAnimTo(long paramLong1, long paramLong2, String paramString, float paramFloat, boolean paramBoolean) {
    if (paramBoolean)
      removeAnimationFor(paramString); 
    ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(this, paramString, new float[] { paramFloat });
    objectAnimator.setDuration(paramLong1);
    objectAnimator.setStartDelay(paramLong2);
    objectAnimator.setInterpolator((TimeInterpolator)EASE_OUT_INTERPOLATOR);
    addAnimation(objectAnimator, paramBoolean);
    return objectAnimator;
  }
  
  public void removeAnimationFor(String paramString) {
    ArrayList arrayList = (ArrayList)this.mAnimators.clone();
    for (ObjectAnimator objectAnimator : arrayList) {
      if (paramString.equals(objectAnimator.getPropertyName()))
        objectAnimator.cancel(); 
    } 
  }
  
  public void clearAnimations() {
    for (ObjectAnimator objectAnimator : this.mAnimators)
      objectAnimator.cancel(); 
    this.mAnimators.clear();
  }
  
  private DrawableHolder addAnimation(ObjectAnimator paramObjectAnimator, boolean paramBoolean) {
    if (paramObjectAnimator != null)
      this.mAnimators.add(paramObjectAnimator); 
    this.mNeedToStart.add(paramObjectAnimator);
    return this;
  }
  
  public void draw(Canvas paramCanvas) {
    if (this.mAlpha <= 0.00390625F)
      return; 
    paramCanvas.save(1);
    paramCanvas.translate(this.mX, this.mY);
    paramCanvas.scale(this.mScaleX, this.mScaleY);
    paramCanvas.translate(getWidth() * -0.5F, getHeight() * -0.5F);
    this.mDrawable.setAlpha(Math.round(this.mAlpha * 255.0F));
    this.mDrawable.draw(paramCanvas);
    paramCanvas.restore();
  }
  
  public void startAnimations(ValueAnimator.AnimatorUpdateListener paramAnimatorUpdateListener) {
    for (byte b = 0; b < this.mNeedToStart.size(); b++) {
      ObjectAnimator objectAnimator = this.mNeedToStart.get(b);
      objectAnimator.addUpdateListener(paramAnimatorUpdateListener);
      objectAnimator.addListener(this);
      objectAnimator.start();
    } 
    this.mNeedToStart.clear();
  }
  
  public void setX(float paramFloat) {
    this.mX = paramFloat;
  }
  
  public void setY(float paramFloat) {
    this.mY = paramFloat;
  }
  
  public void setScaleX(float paramFloat) {
    this.mScaleX = paramFloat;
  }
  
  public void setScaleY(float paramFloat) {
    this.mScaleY = paramFloat;
  }
  
  public void setAlpha(float paramFloat) {
    this.mAlpha = paramFloat;
  }
  
  public float getX() {
    return this.mX;
  }
  
  public float getY() {
    return this.mY;
  }
  
  public float getScaleX() {
    return this.mScaleX;
  }
  
  public float getScaleY() {
    return this.mScaleY;
  }
  
  public float getAlpha() {
    return this.mAlpha;
  }
  
  public BitmapDrawable getDrawable() {
    return this.mDrawable;
  }
  
  public int getWidth() {
    return this.mDrawable.getIntrinsicWidth();
  }
  
  public int getHeight() {
    return this.mDrawable.getIntrinsicHeight();
  }
  
  public void onAnimationCancel(Animator paramAnimator) {}
  
  public void onAnimationEnd(Animator paramAnimator) {
    this.mAnimators.remove(paramAnimator);
  }
  
  public void onAnimationRepeat(Animator paramAnimator) {}
  
  public void onAnimationStart(Animator paramAnimator) {}
}
