package com.android.internal.widget;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.util.IntProperty;
import android.util.Property;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.PathInterpolator;

public class MessagingPropertyAnimator implements View.OnLayoutChangeListener {
  public static final Interpolator ALPHA_IN = (Interpolator)new PathInterpolator(0.4F, 0.0F, 1.0F, 1.0F);
  
  public static final Interpolator ALPHA_OUT = (Interpolator)new PathInterpolator(0.0F, 0.0F, 0.8F, 1.0F);
  
  private static final long APPEAR_ANIMATION_LENGTH = 210L;
  
  private static final ViewClippingUtil.ClippingParameters CLIPPING_PARAMETERS = (ViewClippingUtil.ClippingParameters)_$$Lambda$MessagingPropertyAnimator$7coWc0tjIUC7grCXucNFbpYTxDI.INSTANCE;
  
  private static final int TAG_ALPHA_ANIMATOR = 16909500;
  
  private static final int TAG_FIRST_LAYOUT = 16909501;
  
  private static final int TAG_LAYOUT_TOP = 16909502;
  
  private static final int TAG_TOP = 16909504;
  
  private static final int TAG_TOP_ANIMATOR = 16909503;
  
  private static final IntProperty<View> TOP = (IntProperty<View>)new Object("top");
  
  public void onLayoutChange(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8) {
    setLayoutTop(paramView, paramInt2);
    if (isFirstLayout(paramView)) {
      setFirstLayout(paramView, false);
      setTop(paramView, paramInt2);
      return;
    } 
    startTopAnimation(paramView, getTop(paramView), paramInt2, MessagingLayout.FAST_OUT_SLOW_IN);
  }
  
  private static boolean isFirstLayout(View paramView) {
    Boolean bool = (Boolean)paramView.getTag(16909501);
    if (bool == null)
      return true; 
    return bool.booleanValue();
  }
  
  public static void recycle(View paramView) {
    setFirstLayout(paramView, true);
  }
  
  private static void setFirstLayout(View paramView, boolean paramBoolean) {
    paramView.setTagInternal(16909501, Boolean.valueOf(paramBoolean));
  }
  
  private static void setLayoutTop(View paramView, int paramInt) {
    paramView.setTagInternal(16909502, Integer.valueOf(paramInt));
  }
  
  public static int getLayoutTop(View paramView) {
    Integer integer = (Integer)paramView.getTag(16909502);
    if (integer == null)
      return getTop(paramView); 
    return integer.intValue();
  }
  
  public static void startLocalTranslationFrom(View paramView, int paramInt, Interpolator paramInterpolator) {
    startTopAnimation(paramView, getTop(paramView) + paramInt, getLayoutTop(paramView), paramInterpolator);
  }
  
  public static void startLocalTranslationTo(View paramView, int paramInt, Interpolator paramInterpolator) {
    int i = getTop(paramView);
    startTopAnimation(paramView, i, i + paramInt, paramInterpolator);
  }
  
  public static int getTop(View paramView) {
    Integer integer = (Integer)paramView.getTag(16909504);
    if (integer == null)
      return paramView.getTop(); 
    return integer.intValue();
  }
  
  private static void setTop(View paramView, int paramInt) {
    paramView.setTagInternal(16909504, Integer.valueOf(paramInt));
    updateTopAndBottom(paramView);
  }
  
  private static void updateTopAndBottom(View paramView) {
    int i = getTop(paramView);
    int j = paramView.getHeight();
    paramView.setTop(i);
    paramView.setBottom(j + i);
  }
  
  private static void startTopAnimation(View paramView, int paramInt1, int paramInt2, Interpolator paramInterpolator) {
    ObjectAnimator objectAnimator = (ObjectAnimator)paramView.getTag(16909503);
    if (objectAnimator != null)
      objectAnimator.cancel(); 
    if (!paramView.isShown() || paramInt1 == paramInt2 || (
      MessagingLinearLayout.isGone(paramView) && !isHidingAnimated(paramView))) {
      setTop(paramView, paramInt2);
      return;
    } 
    objectAnimator = ObjectAnimator.ofInt(paramView, (Property)TOP, new int[] { paramInt1, paramInt2 });
    setTop(paramView, paramInt1);
    objectAnimator.setInterpolator((TimeInterpolator)paramInterpolator);
    objectAnimator.setDuration(210L);
    objectAnimator.addListener((Animator.AnimatorListener)new Object(paramView));
    setClippingDeactivated(paramView, true);
    paramView.setTagInternal(16909503, objectAnimator);
    objectAnimator.start();
  }
  
  private static boolean isHidingAnimated(View paramView) {
    if (paramView instanceof MessagingLinearLayout.MessagingChild)
      return ((MessagingLinearLayout.MessagingChild)paramView).isHidingAnimated(); 
    return false;
  }
  
  public static void fadeIn(View paramView) {
    ObjectAnimator objectAnimator = (ObjectAnimator)paramView.getTag(16909500);
    if (objectAnimator != null)
      objectAnimator.cancel(); 
    if (paramView.getVisibility() == 4)
      paramView.setVisibility(0); 
    objectAnimator = ObjectAnimator.ofFloat(paramView, View.ALPHA, new float[] { 0.0F, 1.0F });
    paramView.setAlpha(0.0F);
    objectAnimator.setInterpolator((TimeInterpolator)ALPHA_IN);
    objectAnimator.setDuration(210L);
    objectAnimator.addListener((Animator.AnimatorListener)new Object(paramView));
    updateLayerType(paramView, true);
    paramView.setTagInternal(16909500, objectAnimator);
    objectAnimator.start();
  }
  
  private static void updateLayerType(View paramView, boolean paramBoolean) {
    if (paramView.hasOverlappingRendering() && paramBoolean) {
      paramView.setLayerType(2, null);
    } else if (paramView.getLayerType() == 2) {
      paramView.setLayerType(0, null);
    } 
  }
  
  public static void fadeOut(View paramView, Runnable paramRunnable) {
    ObjectAnimator objectAnimator2 = (ObjectAnimator)paramView.getTag(16909500);
    if (objectAnimator2 != null)
      objectAnimator2.cancel(); 
    if (!paramView.isShown() || (MessagingLinearLayout.isGone(paramView) && !isHidingAnimated(paramView))) {
      paramView.setAlpha(0.0F);
      if (paramRunnable != null)
        paramRunnable.run(); 
      return;
    } 
    Property property = View.ALPHA;
    float f = paramView.getAlpha();
    ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(paramView, property, new float[] { f, 0.0F });
    objectAnimator1.setInterpolator((TimeInterpolator)ALPHA_OUT);
    objectAnimator1.setDuration(210L);
    objectAnimator1.addListener((Animator.AnimatorListener)new Object(paramView, paramRunnable));
    updateLayerType(paramView, true);
    paramView.setTagInternal(16909500, objectAnimator1);
    objectAnimator1.start();
  }
  
  public static void setClippingDeactivated(View paramView, boolean paramBoolean) {
    ViewClippingUtil.setClippingDeactivated(paramView, paramBoolean, CLIPPING_PARAMETERS);
  }
  
  public static boolean isAnimatingTranslation(View paramView) {
    boolean bool;
    if (paramView.getTag(16909503) != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isAnimatingAlpha(View paramView) {
    boolean bool;
    if (paramView.getTag(16909500) != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static void setToLaidOutPosition(View paramView) {
    setTop(paramView, getLayoutTop(paramView));
  }
}
