package com.android.internal.widget;

import android.app.admin.PasswordMetrics;

public abstract class LockSettingsInternal {
  public abstract long addEscrowToken(byte[] paramArrayOfbyte, int paramInt, LockPatternUtils.EscrowTokenStateChangeCallback paramEscrowTokenStateChangeCallback);
  
  public abstract boolean armRebootEscrow();
  
  public abstract void clearRebootEscrow();
  
  public abstract PasswordMetrics getUserPasswordMetrics(int paramInt);
  
  public abstract boolean isEscrowTokenActive(long paramLong, int paramInt);
  
  public abstract void prepareRebootEscrow();
  
  public abstract void refreshStrongAuthTimeout(int paramInt);
  
  public abstract boolean removeEscrowToken(long paramLong, int paramInt);
  
  public abstract boolean setLockCredentialWithToken(LockscreenCredential paramLockscreenCredential, long paramLong, byte[] paramArrayOfbyte, int paramInt);
  
  public abstract void setRebootEscrowListener(RebootEscrowListener paramRebootEscrowListener);
  
  public abstract boolean unlockUserWithToken(long paramLong, byte[] paramArrayOfbyte, int paramInt);
}
