package com.android.internal.widget;

import android.util.Pools;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class AdapterHelper implements OpReorderer.Callback {
  private Pools.Pool<UpdateOp> mUpdateOpPool = (Pools.Pool<UpdateOp>)new Pools.SimplePool(30);
  
  final ArrayList<UpdateOp> mPendingUpdates = new ArrayList<>();
  
  final ArrayList<UpdateOp> mPostponedList = new ArrayList<>();
  
  private int mExistingUpdateTypes = 0;
  
  private static final boolean DEBUG = false;
  
  static final int POSITION_TYPE_INVISIBLE = 0;
  
  static final int POSITION_TYPE_NEW_OR_LAID_OUT = 1;
  
  private static final String TAG = "AHT";
  
  final Callback mCallback;
  
  final boolean mDisableRecycler;
  
  Runnable mOnItemProcessedCallback;
  
  final OpReorderer mOpReorderer;
  
  AdapterHelper(Callback paramCallback) {
    this(paramCallback, false);
  }
  
  AdapterHelper(Callback paramCallback, boolean paramBoolean) {
    this.mCallback = paramCallback;
    this.mDisableRecycler = paramBoolean;
    this.mOpReorderer = new OpReorderer(this);
  }
  
  AdapterHelper addUpdateOp(UpdateOp... paramVarArgs) {
    Collections.addAll(this.mPendingUpdates, paramVarArgs);
    return this;
  }
  
  void reset() {
    recycleUpdateOpsAndClearList(this.mPendingUpdates);
    recycleUpdateOpsAndClearList(this.mPostponedList);
    this.mExistingUpdateTypes = 0;
  }
  
  void preProcess() {
    this.mOpReorderer.reorderOps(this.mPendingUpdates);
    int i = this.mPendingUpdates.size();
    for (byte b = 0; b < i; b++) {
      UpdateOp updateOp = this.mPendingUpdates.get(b);
      int j = updateOp.cmd;
      if (j != 1) {
        if (j != 2) {
          if (j != 4) {
            if (j == 8)
              applyMove(updateOp); 
          } else {
            applyUpdate(updateOp);
          } 
        } else {
          applyRemove(updateOp);
        } 
      } else {
        applyAdd(updateOp);
      } 
      Runnable runnable = this.mOnItemProcessedCallback;
      if (runnable != null)
        runnable.run(); 
    } 
    this.mPendingUpdates.clear();
  }
  
  void consumePostponedUpdates() {
    int i = this.mPostponedList.size();
    for (byte b = 0; b < i; b++)
      this.mCallback.onDispatchSecondPass(this.mPostponedList.get(b)); 
    recycleUpdateOpsAndClearList(this.mPostponedList);
    this.mExistingUpdateTypes = 0;
  }
  
  private void applyMove(UpdateOp paramUpdateOp) {
    postponeAndUpdateViewHolders(paramUpdateOp);
  }
  
  private void applyRemove(UpdateOp paramUpdateOp) {
    int i = paramUpdateOp.positionStart;
    int j = 0;
    int k = paramUpdateOp.positionStart + paramUpdateOp.itemCount;
    byte b = -1;
    int m;
    for (m = paramUpdateOp.positionStart; m < k; n++, j = m, b = b1, m = n) {
      byte b1 = 0;
      int n = 0;
      RecyclerView.ViewHolder viewHolder = this.mCallback.findViewHolder(m);
      if (viewHolder != null || canFindInPreLayout(m)) {
        if (b == 0) {
          UpdateOp updateOp1 = obtainUpdateOp(2, i, j, null);
          dispatchAndUpdateViewHolders(updateOp1);
          b1 = 1;
        } 
        b = 1;
        n = b1;
        b1 = b;
      } else {
        if (b == 1) {
          UpdateOp updateOp1 = obtainUpdateOp(2, i, j, null);
          postponeAndUpdateViewHolders(updateOp1);
          n = 1;
        } 
        b1 = 0;
      } 
      if (n != 0) {
        n = m - j;
        k -= j;
        m = 1;
      } else {
        j++;
        n = m;
        m = j;
      } 
    } 
    UpdateOp updateOp = paramUpdateOp;
    if (j != paramUpdateOp.itemCount) {
      recycleUpdateOp(paramUpdateOp);
      updateOp = obtainUpdateOp(2, i, j, null);
    } 
    if (b == 0) {
      dispatchAndUpdateViewHolders(updateOp);
    } else {
      postponeAndUpdateViewHolders(updateOp);
    } 
  }
  
  private void applyUpdate(UpdateOp paramUpdateOp) {
    int i = paramUpdateOp.positionStart;
    int j = 0;
    int k = paramUpdateOp.positionStart, m = paramUpdateOp.itemCount;
    int n = -1;
    for (int i1 = paramUpdateOp.positionStart; i1 < k + m; i1++, n = i3) {
      int i2, i3;
      RecyclerView.ViewHolder viewHolder = this.mCallback.findViewHolder(i1);
      if (viewHolder != null || canFindInPreLayout(i1)) {
        int i4 = i;
        i2 = j;
        if (n == 0) {
          UpdateOp updateOp = obtainUpdateOp(4, i, j, paramUpdateOp.payload);
          dispatchAndUpdateViewHolders(updateOp);
          i2 = 0;
          i4 = i1;
        } 
        i3 = 1;
        i = i4;
      } else {
        i2 = i;
        i3 = j;
        if (n == 1) {
          UpdateOp updateOp = obtainUpdateOp(4, i, j, paramUpdateOp.payload);
          postponeAndUpdateViewHolders(updateOp);
          i3 = 0;
          i2 = i1;
        } 
        j = 0;
        i = i2;
        i2 = i3;
        i3 = j;
      } 
      j = i2 + 1;
    } 
    Object object = paramUpdateOp;
    if (j != paramUpdateOp.itemCount) {
      object = paramUpdateOp.payload;
      recycleUpdateOp(paramUpdateOp);
      object = obtainUpdateOp(4, i, j, object);
    } 
    if (n == 0) {
      dispatchAndUpdateViewHolders((UpdateOp)object);
    } else {
      postponeAndUpdateViewHolders((UpdateOp)object);
    } 
  }
  
  private void dispatchAndUpdateViewHolders(UpdateOp paramUpdateOp) {
    if (paramUpdateOp.cmd != 1 && paramUpdateOp.cmd != 8) {
      byte b1;
      int i = updatePositionWithPostponed(paramUpdateOp.positionStart, paramUpdateOp.cmd);
      int j = 1;
      int k = paramUpdateOp.positionStart;
      int m = paramUpdateOp.cmd;
      if (m != 2) {
        if (m == 4) {
          b1 = 1;
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("op should be remove or update.");
          stringBuilder.append(paramUpdateOp);
          throw new IllegalArgumentException(stringBuilder.toString());
        } 
      } else {
        b1 = 0;
      } 
      for (byte b2 = 1; b2 < paramUpdateOp.itemCount; b2++, j = m) {
        m = paramUpdateOp.positionStart;
        int n = updatePositionWithPostponed(m + b1 * b2, paramUpdateOp.cmd);
        boolean bool1 = false;
        int i1 = paramUpdateOp.cmd;
        boolean bool2 = false;
        m = 0;
        if (i1 != 2) {
          if (i1 != 4) {
            m = bool1;
          } else if (n == i + 1) {
            m = 1;
          } 
        } else {
          m = bool2;
          if (n == i)
            m = 1; 
        } 
        if (m != 0) {
          m = j + 1;
        } else {
          UpdateOp updateOp = obtainUpdateOp(paramUpdateOp.cmd, i, j, paramUpdateOp.payload);
          dispatchFirstPassAndUpdateViewHolders(updateOp, k);
          recycleUpdateOp(updateOp);
          m = k;
          if (paramUpdateOp.cmd == 4)
            m = k + j; 
          i = n;
          j = 1;
          k = m;
          m = j;
        } 
      } 
      Object object = paramUpdateOp.payload;
      recycleUpdateOp(paramUpdateOp);
      if (j > 0) {
        paramUpdateOp = obtainUpdateOp(paramUpdateOp.cmd, i, j, object);
        dispatchFirstPassAndUpdateViewHolders(paramUpdateOp, k);
        recycleUpdateOp(paramUpdateOp);
      } 
      return;
    } 
    throw new IllegalArgumentException("should not dispatch add or move for pre layout");
  }
  
  void dispatchFirstPassAndUpdateViewHolders(UpdateOp paramUpdateOp, int paramInt) {
    this.mCallback.onDispatchFirstPass(paramUpdateOp);
    int i = paramUpdateOp.cmd;
    if (i != 2) {
      if (i == 4) {
        this.mCallback.markViewHoldersUpdated(paramInt, paramUpdateOp.itemCount, paramUpdateOp.payload);
      } else {
        throw new IllegalArgumentException("only remove and update ops can be dispatched in first pass");
      } 
    } else {
      this.mCallback.offsetPositionsForRemovingInvisible(paramInt, paramUpdateOp.itemCount);
    } 
  }
  
  private int updatePositionWithPostponed(int paramInt1, int paramInt2) {
    int i = this.mPostponedList.size();
    for (int j = i - 1; j >= 0; j--, i = paramInt1) {
      UpdateOp updateOp = this.mPostponedList.get(j);
      if (updateOp.cmd == 8) {
        int k;
        if (updateOp.positionStart < updateOp.itemCount) {
          paramInt1 = updateOp.positionStart;
          k = updateOp.itemCount;
        } else {
          paramInt1 = updateOp.itemCount;
          k = updateOp.positionStart;
        } 
        if (i >= paramInt1 && i <= k) {
          if (paramInt1 == updateOp.positionStart) {
            if (paramInt2 == 1) {
              updateOp.itemCount++;
            } else if (paramInt2 == 2) {
              updateOp.itemCount--;
            } 
            paramInt1 = i + 1;
          } else {
            if (paramInt2 == 1) {
              updateOp.positionStart++;
            } else if (paramInt2 == 2) {
              updateOp.positionStart--;
            } 
            paramInt1 = i - 1;
          } 
        } else {
          paramInt1 = i;
          if (i < updateOp.positionStart)
            if (paramInt2 == 1) {
              updateOp.positionStart++;
              updateOp.itemCount++;
              paramInt1 = i;
            } else {
              paramInt1 = i;
              if (paramInt2 == 2) {
                updateOp.positionStart--;
                updateOp.itemCount--;
                paramInt1 = i;
              } 
            }  
        } 
      } else if (updateOp.positionStart <= i) {
        if (updateOp.cmd == 1) {
          paramInt1 = i - updateOp.itemCount;
        } else {
          paramInt1 = i;
          if (updateOp.cmd == 2)
            paramInt1 = i + updateOp.itemCount; 
        } 
      } else if (paramInt2 == 1) {
        updateOp.positionStart++;
        paramInt1 = i;
      } else {
        paramInt1 = i;
        if (paramInt2 == 2) {
          updateOp.positionStart--;
          paramInt1 = i;
        } 
      } 
    } 
    for (paramInt1 = this.mPostponedList.size() - 1; paramInt1 >= 0; paramInt1--) {
      UpdateOp updateOp = this.mPostponedList.get(paramInt1);
      if (updateOp.cmd == 8) {
        if (updateOp.itemCount == updateOp.positionStart || updateOp.itemCount < 0) {
          this.mPostponedList.remove(paramInt1);
          recycleUpdateOp(updateOp);
        } 
      } else if (updateOp.itemCount <= 0) {
        this.mPostponedList.remove(paramInt1);
        recycleUpdateOp(updateOp);
      } 
    } 
    return i;
  }
  
  private boolean canFindInPreLayout(int paramInt) {
    int i = this.mPostponedList.size();
    for (byte b = 0; b < i; b++) {
      UpdateOp updateOp = this.mPostponedList.get(b);
      if (updateOp.cmd == 8) {
        if (findPositionOffset(updateOp.itemCount, b + 1) == paramInt)
          return true; 
      } else if (updateOp.cmd == 1) {
        int j = updateOp.positionStart, k = updateOp.itemCount;
        for (int m = updateOp.positionStart; m < j + k; m++) {
          if (findPositionOffset(m, b + 1) == paramInt)
            return true; 
        } 
      } 
    } 
    return false;
  }
  
  private void applyAdd(UpdateOp paramUpdateOp) {
    postponeAndUpdateViewHolders(paramUpdateOp);
  }
  
  private void postponeAndUpdateViewHolders(UpdateOp paramUpdateOp) {
    this.mPostponedList.add(paramUpdateOp);
    int i = paramUpdateOp.cmd;
    if (i != 1) {
      if (i != 2) {
        if (i != 4) {
          if (i == 8) {
            this.mCallback.offsetPositionsForMove(paramUpdateOp.positionStart, paramUpdateOp.itemCount);
          } else {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Unknown update op type for ");
            stringBuilder.append(paramUpdateOp);
            throw new IllegalArgumentException(stringBuilder.toString());
          } 
        } else {
          this.mCallback.markViewHoldersUpdated(paramUpdateOp.positionStart, paramUpdateOp.itemCount, paramUpdateOp.payload);
        } 
      } else {
        this.mCallback.offsetPositionsForRemovingLaidOutOrNewView(paramUpdateOp.positionStart, paramUpdateOp.itemCount);
      } 
    } else {
      this.mCallback.offsetPositionsForAdd(paramUpdateOp.positionStart, paramUpdateOp.itemCount);
    } 
  }
  
  boolean hasPendingUpdates() {
    boolean bool;
    if (this.mPendingUpdates.size() > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  boolean hasAnyUpdateTypes(int paramInt) {
    boolean bool;
    if ((this.mExistingUpdateTypes & paramInt) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  int findPositionOffset(int paramInt) {
    return findPositionOffset(paramInt, 0);
  }
  
  int findPositionOffset(int paramInt1, int paramInt2) {
    int i = this.mPostponedList.size();
    for (int j = paramInt2; j < i; j++, paramInt2 = paramInt1) {
      UpdateOp updateOp = this.mPostponedList.get(j);
      if (updateOp.cmd == 8) {
        if (updateOp.positionStart == paramInt2) {
          paramInt1 = updateOp.itemCount;
        } else {
          int k = paramInt2;
          if (updateOp.positionStart < paramInt2)
            k = paramInt2 - 1; 
          paramInt1 = k;
          if (updateOp.itemCount <= k)
            paramInt1 = k + 1; 
        } 
      } else {
        paramInt1 = paramInt2;
        if (updateOp.positionStart <= paramInt2)
          if (updateOp.cmd == 2) {
            if (paramInt2 < updateOp.positionStart + updateOp.itemCount)
              return -1; 
            paramInt1 = paramInt2 - updateOp.itemCount;
          } else {
            paramInt1 = paramInt2;
            if (updateOp.cmd == 1)
              paramInt1 = paramInt2 + updateOp.itemCount; 
          }  
      } 
    } 
    return paramInt2;
  }
  
  boolean onItemRangeChanged(int paramInt1, int paramInt2, Object paramObject) {
    boolean bool = false;
    if (paramInt2 < 1)
      return false; 
    this.mPendingUpdates.add(obtainUpdateOp(4, paramInt1, paramInt2, paramObject));
    this.mExistingUpdateTypes |= 0x4;
    if (this.mPendingUpdates.size() == 1)
      bool = true; 
    return bool;
  }
  
  boolean onItemRangeInserted(int paramInt1, int paramInt2) {
    boolean bool = false;
    if (paramInt2 < 1)
      return false; 
    this.mPendingUpdates.add(obtainUpdateOp(1, paramInt1, paramInt2, null));
    this.mExistingUpdateTypes |= 0x1;
    if (this.mPendingUpdates.size() == 1)
      bool = true; 
    return bool;
  }
  
  boolean onItemRangeRemoved(int paramInt1, int paramInt2) {
    boolean bool = false;
    if (paramInt2 < 1)
      return false; 
    this.mPendingUpdates.add(obtainUpdateOp(2, paramInt1, paramInt2, null));
    this.mExistingUpdateTypes |= 0x2;
    if (this.mPendingUpdates.size() == 1)
      bool = true; 
    return bool;
  }
  
  boolean onItemRangeMoved(int paramInt1, int paramInt2, int paramInt3) {
    boolean bool = false;
    if (paramInt1 == paramInt2)
      return false; 
    if (paramInt3 == 1) {
      this.mPendingUpdates.add(obtainUpdateOp(8, paramInt1, paramInt2, null));
      this.mExistingUpdateTypes |= 0x8;
      if (this.mPendingUpdates.size() == 1)
        bool = true; 
      return bool;
    } 
    throw new IllegalArgumentException("Moving more than 1 item is not supported yet");
  }
  
  void consumeUpdatesInOnePass() {
    consumePostponedUpdates();
    int i = this.mPendingUpdates.size();
    for (byte b = 0; b < i; b++) {
      UpdateOp updateOp = this.mPendingUpdates.get(b);
      int j = updateOp.cmd;
      if (j != 1) {
        if (j != 2) {
          if (j != 4) {
            if (j == 8) {
              this.mCallback.onDispatchSecondPass(updateOp);
              this.mCallback.offsetPositionsForMove(updateOp.positionStart, updateOp.itemCount);
            } 
          } else {
            this.mCallback.onDispatchSecondPass(updateOp);
            this.mCallback.markViewHoldersUpdated(updateOp.positionStart, updateOp.itemCount, updateOp.payload);
          } 
        } else {
          this.mCallback.onDispatchSecondPass(updateOp);
          this.mCallback.offsetPositionsForRemovingInvisible(updateOp.positionStart, updateOp.itemCount);
        } 
      } else {
        this.mCallback.onDispatchSecondPass(updateOp);
        this.mCallback.offsetPositionsForAdd(updateOp.positionStart, updateOp.itemCount);
      } 
      Runnable runnable = this.mOnItemProcessedCallback;
      if (runnable != null)
        runnable.run(); 
    } 
    recycleUpdateOpsAndClearList(this.mPendingUpdates);
    this.mExistingUpdateTypes = 0;
  }
  
  public int applyPendingUpdatesToPosition(int paramInt) {
    int i = this.mPendingUpdates.size();
    int j;
    for (byte b = 0; b < i; b++, j = paramInt) {
      UpdateOp updateOp = this.mPendingUpdates.get(b);
      paramInt = updateOp.cmd;
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 8) {
            paramInt = j;
          } else if (updateOp.positionStart == j) {
            paramInt = updateOp.itemCount;
          } else {
            int k = j;
            if (updateOp.positionStart < j)
              k = j - 1; 
            paramInt = k;
            if (updateOp.itemCount <= k)
              paramInt = k + 1; 
          } 
        } else {
          paramInt = j;
          if (updateOp.positionStart <= j) {
            int k = updateOp.positionStart;
            paramInt = updateOp.itemCount;
            if (k + paramInt > j)
              return -1; 
            paramInt = j - updateOp.itemCount;
          } 
        } 
      } else {
        paramInt = j;
        if (updateOp.positionStart <= j)
          paramInt = j + updateOp.itemCount; 
      } 
    } 
    return j;
  }
  
  boolean hasUpdates() {
    boolean bool;
    if (!this.mPostponedList.isEmpty() && !this.mPendingUpdates.isEmpty()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  class Callback {
    public abstract RecyclerView.ViewHolder findViewHolder(int param1Int);
    
    public abstract void markViewHoldersUpdated(int param1Int1, int param1Int2, Object param1Object);
    
    public abstract void offsetPositionsForAdd(int param1Int1, int param1Int2);
    
    public abstract void offsetPositionsForMove(int param1Int1, int param1Int2);
    
    public abstract void offsetPositionsForRemovingInvisible(int param1Int1, int param1Int2);
    
    public abstract void offsetPositionsForRemovingLaidOutOrNewView(int param1Int1, int param1Int2);
    
    public abstract void onDispatchFirstPass(AdapterHelper.UpdateOp param1UpdateOp);
    
    public abstract void onDispatchSecondPass(AdapterHelper.UpdateOp param1UpdateOp);
  }
  
  class UpdateOp {
    static final int ADD = 1;
    
    static final int MOVE = 8;
    
    static final int POOL_SIZE = 30;
    
    static final int REMOVE = 2;
    
    static final int UPDATE = 4;
    
    int cmd;
    
    int itemCount;
    
    Object payload;
    
    int positionStart;
    
    UpdateOp(AdapterHelper this$0, int param1Int1, int param1Int2, Object param1Object) {
      this.cmd = this$0;
      this.positionStart = param1Int1;
      this.itemCount = param1Int2;
      this.payload = param1Object;
    }
    
    String cmdToString() {
      int i = this.cmd;
      if (i != 1) {
        if (i != 2) {
          if (i != 4) {
            if (i != 8)
              return "??"; 
            return "mv";
          } 
          return "up";
        } 
        return "rm";
      } 
      return "add";
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
      stringBuilder.append("[");
      stringBuilder.append(cmdToString());
      stringBuilder.append(",s:");
      stringBuilder.append(this.positionStart);
      stringBuilder.append("c:");
      stringBuilder.append(this.itemCount);
      stringBuilder.append(",p:");
      stringBuilder.append(this.payload);
      stringBuilder.append("]");
      return stringBuilder.toString();
    }
    
    public boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (param1Object == null || getClass() != param1Object.getClass())
        return false; 
      param1Object = param1Object;
      int i = this.cmd;
      if (i != ((UpdateOp)param1Object).cmd)
        return false; 
      if (i == 8 && Math.abs(this.itemCount - this.positionStart) == 1)
        if (this.itemCount == ((UpdateOp)param1Object).positionStart && this.positionStart == ((UpdateOp)param1Object).itemCount)
          return true;  
      if (this.itemCount != ((UpdateOp)param1Object).itemCount)
        return false; 
      if (this.positionStart != ((UpdateOp)param1Object).positionStart)
        return false; 
      Object object = this.payload;
      if (object != null) {
        if (!object.equals(((UpdateOp)param1Object).payload))
          return false; 
      } else if (((UpdateOp)param1Object).payload != null) {
        return false;
      } 
      return true;
    }
    
    public int hashCode() {
      int i = this.cmd;
      int j = this.positionStart;
      int k = this.itemCount;
      return (i * 31 + j) * 31 + k;
    }
  }
  
  public UpdateOp obtainUpdateOp(int paramInt1, int paramInt2, int paramInt3, Object paramObject) {
    UpdateOp updateOp = (UpdateOp)this.mUpdateOpPool.acquire();
    if (updateOp == null) {
      paramObject = new UpdateOp(paramInt1, paramInt2, paramInt3, paramObject);
    } else {
      updateOp.cmd = paramInt1;
      updateOp.positionStart = paramInt2;
      updateOp.itemCount = paramInt3;
      updateOp.payload = paramObject;
      paramObject = updateOp;
    } 
    return (UpdateOp)paramObject;
  }
  
  public void recycleUpdateOp(UpdateOp paramUpdateOp) {
    if (!this.mDisableRecycler) {
      paramUpdateOp.payload = null;
      this.mUpdateOpPool.release(paramUpdateOp);
    } 
  }
  
  void recycleUpdateOpsAndClearList(List<UpdateOp> paramList) {
    int i = paramList.size();
    for (byte b = 0; b < i; b++)
      recycleUpdateOp(paramList.get(b)); 
    paramList.clear();
  }
}
