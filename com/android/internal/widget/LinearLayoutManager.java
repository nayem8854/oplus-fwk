package com.android.internal.widget;

import android.content.Context;
import android.graphics.PointF;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import com.android.internal.widget.helper.ItemTouchHelper;
import java.util.List;

public class LinearLayoutManager extends RecyclerView.LayoutManager implements ItemTouchHelper.ViewDropHandler, RecyclerView.SmoothScroller.ScrollVectorProvider {
  private boolean mReverseLayout = false;
  
  boolean mShouldReverseLayout = false;
  
  private boolean mStackFromEnd = false;
  
  private boolean mSmoothScrollbarEnabled = true;
  
  int mPendingScrollPosition = -1;
  
  int mPendingScrollPositionOffset = Integer.MIN_VALUE;
  
  SavedState mPendingSavedState = null;
  
  final AnchorInfo mAnchorInfo = new AnchorInfo();
  
  private final LayoutChunkResult mLayoutChunkResult = new LayoutChunkResult();
  
  private int mInitialItemPrefetchCount = 2;
  
  static final boolean DEBUG = false;
  
  public static final int HORIZONTAL = 0;
  
  public static final int INVALID_OFFSET = -2147483648;
  
  private static final float MAX_SCROLL_FACTOR = 0.33333334F;
  
  private static final String TAG = "LinearLayoutManager";
  
  public static final int VERTICAL = 1;
  
  private boolean mLastStackFromEnd;
  
  private LayoutState mLayoutState;
  
  int mOrientation;
  
  OrientationHelper mOrientationHelper;
  
  private boolean mRecycleChildrenOnDetach;
  
  public LinearLayoutManager(Context paramContext) {
    this(paramContext, 1, false);
  }
  
  public LinearLayoutManager(Context paramContext, int paramInt, boolean paramBoolean) {
    setOrientation(paramInt);
    setReverseLayout(paramBoolean);
    setAutoMeasureEnabled(true);
  }
  
  public LinearLayoutManager(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    RecyclerView.LayoutManager.Properties properties = getProperties(paramContext, paramAttributeSet, paramInt1, paramInt2);
    setOrientation(properties.orientation);
    setReverseLayout(properties.reverseLayout);
    setStackFromEnd(properties.stackFromEnd);
    setAutoMeasureEnabled(true);
  }
  
  public RecyclerView.LayoutParams generateDefaultLayoutParams() {
    return new RecyclerView.LayoutParams(-2, -2);
  }
  
  public boolean getRecycleChildrenOnDetach() {
    return this.mRecycleChildrenOnDetach;
  }
  
  public void setRecycleChildrenOnDetach(boolean paramBoolean) {
    this.mRecycleChildrenOnDetach = paramBoolean;
  }
  
  public void onDetachedFromWindow(RecyclerView paramRecyclerView, RecyclerView.Recycler paramRecycler) {
    super.onDetachedFromWindow(paramRecyclerView, paramRecycler);
    if (this.mRecycleChildrenOnDetach) {
      removeAndRecycleAllViews(paramRecycler);
      paramRecycler.clear();
    } 
  }
  
  public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent) {
    super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
    if (getChildCount() > 0) {
      paramAccessibilityEvent.setFromIndex(findFirstVisibleItemPosition());
      paramAccessibilityEvent.setToIndex(findLastVisibleItemPosition());
    } 
  }
  
  public Parcelable onSaveInstanceState() {
    if (this.mPendingSavedState != null)
      return new SavedState(this.mPendingSavedState); 
    SavedState savedState = new SavedState();
    if (getChildCount() > 0) {
      ensureLayoutState();
      int i = this.mLastStackFromEnd ^ this.mShouldReverseLayout;
      savedState.mAnchorLayoutFromEnd = i;
      if (i != 0) {
        View view = getChildClosestToEnd();
        int j = this.mOrientationHelper.getEndAfterPadding();
        OrientationHelper orientationHelper = this.mOrientationHelper;
        savedState.mAnchorOffset = j - orientationHelper.getDecoratedEnd(view);
        savedState.mAnchorPosition = getPosition(view);
      } else {
        View view = getChildClosestToStart();
        savedState.mAnchorPosition = getPosition(view);
        int j = this.mOrientationHelper.getDecoratedStart(view);
        OrientationHelper orientationHelper = this.mOrientationHelper;
        savedState.mAnchorOffset = j - orientationHelper.getStartAfterPadding();
      } 
    } else {
      savedState.invalidateAnchor();
    } 
    return savedState;
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable) {
    if (paramParcelable instanceof SavedState) {
      this.mPendingSavedState = (SavedState)paramParcelable;
      requestLayout();
    } 
  }
  
  public boolean canScrollHorizontally() {
    boolean bool;
    if (this.mOrientation == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean canScrollVertically() {
    int i = this.mOrientation;
    boolean bool = true;
    if (i != 1)
      bool = false; 
    return bool;
  }
  
  public void setStackFromEnd(boolean paramBoolean) {
    assertNotInLayoutOrScroll((String)null);
    if (this.mStackFromEnd == paramBoolean)
      return; 
    this.mStackFromEnd = paramBoolean;
    requestLayout();
  }
  
  public boolean getStackFromEnd() {
    return this.mStackFromEnd;
  }
  
  public int getOrientation() {
    return this.mOrientation;
  }
  
  public void setOrientation(int paramInt) {
    if (paramInt == 0 || paramInt == 1) {
      assertNotInLayoutOrScroll((String)null);
      if (paramInt == this.mOrientation)
        return; 
      this.mOrientation = paramInt;
      this.mOrientationHelper = null;
      requestLayout();
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("invalid orientation:");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  private void resolveShouldLayoutReverse() {
    if (this.mOrientation == 1 || !isLayoutRTL()) {
      this.mShouldReverseLayout = this.mReverseLayout;
      return;
    } 
    this.mShouldReverseLayout = this.mReverseLayout ^ true;
  }
  
  public boolean getReverseLayout() {
    return this.mReverseLayout;
  }
  
  public void setReverseLayout(boolean paramBoolean) {
    assertNotInLayoutOrScroll((String)null);
    if (paramBoolean == this.mReverseLayout)
      return; 
    this.mReverseLayout = paramBoolean;
    requestLayout();
  }
  
  public View findViewByPosition(int paramInt) {
    int i = getChildCount();
    if (i == 0)
      return null; 
    int j = getPosition(getChildAt(0));
    j = paramInt - j;
    if (j >= 0 && j < i) {
      View view = getChildAt(j);
      if (getPosition(view) == paramInt)
        return view; 
    } 
    return super.findViewByPosition(paramInt);
  }
  
  protected int getExtraLayoutSpace(RecyclerView.State paramState) {
    if (paramState.hasTargetScrollPosition())
      return this.mOrientationHelper.getTotalSpace(); 
    return 0;
  }
  
  public void smoothScrollToPosition(RecyclerView paramRecyclerView, RecyclerView.State paramState, int paramInt) {
    LinearSmoothScroller linearSmoothScroller = new LinearSmoothScroller(paramRecyclerView.getContext());
    linearSmoothScroller.setTargetPosition(paramInt);
    startSmoothScroll(linearSmoothScroller);
  }
  
  public PointF computeScrollVectorForPosition(int paramInt) {
    if (getChildCount() == 0)
      return null; 
    boolean bool = false;
    int i = getPosition(getChildAt(0));
    boolean bool1 = true;
    if (paramInt < i)
      bool = true; 
    paramInt = bool1;
    if (bool != this.mShouldReverseLayout)
      paramInt = -1; 
    if (this.mOrientation == 0)
      return new PointF(paramInt, 0.0F); 
    return new PointF(0.0F, paramInt);
  }
  
  public void onLayoutChildren(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState) {
    SavedState savedState = this.mPendingSavedState;
    int i = -1;
    if ((savedState != null || this.mPendingScrollPosition != -1) && 
      paramState.getItemCount() == 0) {
      removeAndRecycleAllViews(paramRecycler);
      return;
    } 
    savedState = this.mPendingSavedState;
    if (savedState != null && savedState.hasValidAnchor())
      this.mPendingScrollPosition = this.mPendingSavedState.mAnchorPosition; 
    ensureLayoutState();
    this.mLayoutState.mRecycle = false;
    resolveShouldLayoutReverse();
    if (!this.mAnchorInfo.mValid || this.mPendingScrollPosition != -1 || this.mPendingSavedState != null) {
      this.mAnchorInfo.reset();
      this.mAnchorInfo.mLayoutFromEnd = this.mShouldReverseLayout ^ this.mStackFromEnd;
      updateAnchorInfoForLayout(paramRecycler, paramState, this.mAnchorInfo);
      this.mAnchorInfo.mValid = true;
    } 
    int j = getExtraLayoutSpace(paramState);
    if (this.mLayoutState.mLastScrollDelta >= 0) {
      k = 0;
    } else {
      k = j;
      j = 0;
    } 
    int m = k + this.mOrientationHelper.getStartAfterPadding();
    int n = j + this.mOrientationHelper.getEndPadding();
    int k = n;
    j = m;
    if (paramState.isPreLayout()) {
      int i1 = this.mPendingScrollPosition;
      k = n;
      j = m;
      if (i1 != -1) {
        k = n;
        j = m;
        if (this.mPendingScrollPositionOffset != Integer.MIN_VALUE) {
          View view = findViewByPosition(i1);
          k = n;
          j = m;
          if (view != null) {
            if (this.mShouldReverseLayout) {
              j = this.mOrientationHelper.getEndAfterPadding();
              OrientationHelper orientationHelper = this.mOrientationHelper;
              k = orientationHelper.getDecoratedEnd(view);
              j = j - k - this.mPendingScrollPositionOffset;
            } else {
              j = this.mOrientationHelper.getDecoratedStart(view);
              OrientationHelper orientationHelper = this.mOrientationHelper;
              k = orientationHelper.getStartAfterPadding();
              j = this.mPendingScrollPositionOffset - j - k;
            } 
            if (j > 0) {
              j = m + j;
              k = n;
            } else {
              k = n - j;
              j = m;
            } 
          } 
        } 
      } 
    } 
    if (this.mAnchorInfo.mLayoutFromEnd) {
      if (this.mShouldReverseLayout)
        i = 1; 
    } else if (!this.mShouldReverseLayout) {
      i = 1;
    } 
    onAnchorReady(paramRecycler, paramState, this.mAnchorInfo, i);
    detachAndScrapAttachedViews(paramRecycler);
    this.mLayoutState.mInfinite = resolveIsInfinite();
    this.mLayoutState.mIsPreLayout = paramState.isPreLayout();
    if (this.mAnchorInfo.mLayoutFromEnd) {
      updateLayoutStateToFillStart(this.mAnchorInfo);
      this.mLayoutState.mExtra = j;
      fill(paramRecycler, this.mLayoutState, paramState, false);
      i = this.mLayoutState.mOffset;
      m = this.mLayoutState.mCurrentPosition;
      j = k;
      if (this.mLayoutState.mAvailable > 0)
        j = k + this.mLayoutState.mAvailable; 
      updateLayoutStateToFillEnd(this.mAnchorInfo);
      this.mLayoutState.mExtra = j;
      LayoutState layoutState = this.mLayoutState;
      layoutState.mCurrentPosition += this.mLayoutState.mItemDirection;
      fill(paramRecycler, this.mLayoutState, paramState, false);
      j = this.mLayoutState.mOffset;
      k = i;
      if (this.mLayoutState.mAvailable > 0) {
        k = this.mLayoutState.mAvailable;
        updateLayoutStateToFillStart(m, i);
        this.mLayoutState.mExtra = k;
        fill(paramRecycler, this.mLayoutState, paramState, false);
        k = this.mLayoutState.mOffset;
      } 
    } else {
      updateLayoutStateToFillEnd(this.mAnchorInfo);
      this.mLayoutState.mExtra = k;
      fill(paramRecycler, this.mLayoutState, paramState, false);
      i = this.mLayoutState.mOffset;
      m = this.mLayoutState.mCurrentPosition;
      k = j;
      if (this.mLayoutState.mAvailable > 0)
        k = j + this.mLayoutState.mAvailable; 
      updateLayoutStateToFillStart(this.mAnchorInfo);
      this.mLayoutState.mExtra = k;
      LayoutState layoutState = this.mLayoutState;
      layoutState.mCurrentPosition += this.mLayoutState.mItemDirection;
      fill(paramRecycler, this.mLayoutState, paramState, false);
      j = this.mLayoutState.mOffset;
      if (this.mLayoutState.mAvailable > 0) {
        k = this.mLayoutState.mAvailable;
        updateLayoutStateToFillEnd(m, i);
        this.mLayoutState.mExtra = k;
        fill(paramRecycler, this.mLayoutState, paramState, false);
        i = this.mLayoutState.mOffset;
        k = j;
        j = i;
      } else {
        k = j;
        j = i;
      } 
    } 
    m = k;
    i = j;
    if (getChildCount() > 0)
      if ((this.mShouldReverseLayout ^ this.mStackFromEnd) != 0) {
        i = fixLayoutEndGap(j, paramRecycler, paramState, true);
        m = k + i;
        k = fixLayoutStartGap(m, paramRecycler, paramState, false);
        m += k;
        i = j + i + k;
      } else {
        i = fixLayoutStartGap(k, paramRecycler, paramState, true);
        n = j + i;
        j = fixLayoutEndGap(n, paramRecycler, paramState, false);
        m = k + i + j;
        i = n + j;
      }  
    layoutForPredictiveAnimations(paramRecycler, paramState, m, i);
    if (!paramState.isPreLayout()) {
      this.mOrientationHelper.onLayoutComplete();
    } else {
      this.mAnchorInfo.reset();
    } 
    this.mLastStackFromEnd = this.mStackFromEnd;
  }
  
  public void onLayoutCompleted(RecyclerView.State paramState) {
    super.onLayoutCompleted(paramState);
    this.mPendingSavedState = null;
    this.mPendingScrollPosition = -1;
    this.mPendingScrollPositionOffset = Integer.MIN_VALUE;
    this.mAnchorInfo.reset();
  }
  
  void onAnchorReady(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, AnchorInfo paramAnchorInfo, int paramInt) {}
  
  private void layoutForPredictiveAnimations(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, int paramInt1, int paramInt2) {
    if (!paramState.willRunPredictiveAnimations() || getChildCount() == 0 || paramState.isPreLayout() || 
      !supportsPredictiveItemAnimations())
      return; 
    int i = 0, j = 0;
    List<RecyclerView.ViewHolder> list = paramRecycler.getScrapList();
    int k = list.size();
    int m = getPosition(getChildAt(0));
    for (byte b = 0; b < k; b++) {
      RecyclerView.ViewHolder viewHolder = list.get(b);
      if (!viewHolder.isRemoved()) {
        boolean bool;
        int n = viewHolder.getLayoutPosition();
        byte b1 = 1;
        if (n < m) {
          bool = true;
        } else {
          bool = false;
        } 
        if (bool != this.mShouldReverseLayout)
          b1 = -1; 
        if (b1 == -1) {
          i += this.mOrientationHelper.getDecoratedMeasurement(viewHolder.itemView);
        } else {
          j += this.mOrientationHelper.getDecoratedMeasurement(viewHolder.itemView);
        } 
      } 
    } 
    this.mLayoutState.mScrapList = list;
    if (i > 0) {
      View view = getChildClosestToStart();
      updateLayoutStateToFillStart(getPosition(view), paramInt1);
      this.mLayoutState.mExtra = i;
      this.mLayoutState.mAvailable = 0;
      this.mLayoutState.assignPositionFromScrapList();
      fill(paramRecycler, this.mLayoutState, paramState, false);
    } 
    if (j > 0) {
      View view = getChildClosestToEnd();
      updateLayoutStateToFillEnd(getPosition(view), paramInt2);
      this.mLayoutState.mExtra = j;
      this.mLayoutState.mAvailable = 0;
      this.mLayoutState.assignPositionFromScrapList();
      fill(paramRecycler, this.mLayoutState, paramState, false);
    } 
    this.mLayoutState.mScrapList = null;
  }
  
  private void updateAnchorInfoForLayout(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, AnchorInfo paramAnchorInfo) {
    boolean bool;
    if (updateAnchorFromPendingData(paramState, paramAnchorInfo))
      return; 
    if (updateAnchorFromChildren(paramRecycler, paramState, paramAnchorInfo))
      return; 
    paramAnchorInfo.assignCoordinateFromPadding();
    if (this.mStackFromEnd) {
      bool = paramState.getItemCount() - 1;
    } else {
      bool = false;
    } 
    paramAnchorInfo.mPosition = bool;
  }
  
  private boolean updateAnchorFromChildren(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, AnchorInfo paramAnchorInfo) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual getChildCount : ()I
    //   4: istore #4
    //   6: iconst_0
    //   7: istore #5
    //   9: iload #4
    //   11: ifne -> 16
    //   14: iconst_0
    //   15: ireturn
    //   16: aload_0
    //   17: invokevirtual getFocusedChild : ()Landroid/view/View;
    //   20: astore #6
    //   22: aload #6
    //   24: ifnull -> 45
    //   27: aload_3
    //   28: aload #6
    //   30: aload_2
    //   31: invokevirtual isViewValidAsAnchor : (Landroid/view/View;Lcom/android/internal/widget/RecyclerView$State;)Z
    //   34: ifeq -> 45
    //   37: aload_3
    //   38: aload #6
    //   40: invokevirtual assignFromViewAndKeepVisibleRect : (Landroid/view/View;)V
    //   43: iconst_1
    //   44: ireturn
    //   45: aload_0
    //   46: getfield mLastStackFromEnd : Z
    //   49: aload_0
    //   50: getfield mStackFromEnd : Z
    //   53: if_icmpeq -> 58
    //   56: iconst_0
    //   57: ireturn
    //   58: aload_3
    //   59: getfield mLayoutFromEnd : Z
    //   62: ifeq -> 75
    //   65: aload_0
    //   66: aload_1
    //   67: aload_2
    //   68: invokespecial findReferenceChildClosestToEnd : (Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;)Landroid/view/View;
    //   71: astore_1
    //   72: goto -> 82
    //   75: aload_0
    //   76: aload_1
    //   77: aload_2
    //   78: invokespecial findReferenceChildClosestToStart : (Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;)Landroid/view/View;
    //   81: astore_1
    //   82: aload_1
    //   83: ifnull -> 201
    //   86: aload_3
    //   87: aload_1
    //   88: invokevirtual assignFromView : (Landroid/view/View;)V
    //   91: aload_2
    //   92: invokevirtual isPreLayout : ()Z
    //   95: ifne -> 199
    //   98: aload_0
    //   99: invokevirtual supportsPredictiveItemAnimations : ()Z
    //   102: ifeq -> 199
    //   105: aload_0
    //   106: getfield mOrientationHelper : Lcom/android/internal/widget/OrientationHelper;
    //   109: astore_2
    //   110: aload_2
    //   111: aload_1
    //   112: invokevirtual getDecoratedStart : (Landroid/view/View;)I
    //   115: istore #4
    //   117: aload_0
    //   118: getfield mOrientationHelper : Lcom/android/internal/widget/OrientationHelper;
    //   121: astore_2
    //   122: iload #4
    //   124: aload_2
    //   125: invokevirtual getEndAfterPadding : ()I
    //   128: if_icmpge -> 157
    //   131: aload_0
    //   132: getfield mOrientationHelper : Lcom/android/internal/widget/OrientationHelper;
    //   135: astore_2
    //   136: aload_2
    //   137: aload_1
    //   138: invokevirtual getDecoratedEnd : (Landroid/view/View;)I
    //   141: istore #4
    //   143: aload_0
    //   144: getfield mOrientationHelper : Lcom/android/internal/widget/OrientationHelper;
    //   147: astore_1
    //   148: iload #4
    //   150: aload_1
    //   151: invokevirtual getStartAfterPadding : ()I
    //   154: if_icmpge -> 160
    //   157: iconst_1
    //   158: istore #5
    //   160: iload #5
    //   162: ifeq -> 199
    //   165: aload_3
    //   166: getfield mLayoutFromEnd : Z
    //   169: ifeq -> 184
    //   172: aload_0
    //   173: getfield mOrientationHelper : Lcom/android/internal/widget/OrientationHelper;
    //   176: invokevirtual getEndAfterPadding : ()I
    //   179: istore #5
    //   181: goto -> 193
    //   184: aload_0
    //   185: getfield mOrientationHelper : Lcom/android/internal/widget/OrientationHelper;
    //   188: invokevirtual getStartAfterPadding : ()I
    //   191: istore #5
    //   193: aload_3
    //   194: iload #5
    //   196: putfield mCoordinate : I
    //   199: iconst_1
    //   200: ireturn
    //   201: iconst_0
    //   202: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #751	-> 0
    //   #752	-> 14
    //   #754	-> 16
    //   #755	-> 22
    //   #756	-> 37
    //   #757	-> 43
    //   #759	-> 45
    //   #760	-> 56
    //   #762	-> 58
    //   #763	-> 65
    //   #764	-> 75
    //   #765	-> 82
    //   #766	-> 86
    //   #769	-> 91
    //   #771	-> 105
    //   #772	-> 110
    //   #773	-> 122
    //   #774	-> 136
    //   #775	-> 148
    //   #776	-> 160
    //   #777	-> 165
    //   #778	-> 172
    //   #779	-> 184
    //   #782	-> 199
    //   #784	-> 201
  }
  
  private boolean updateAnchorFromPendingData(RecyclerView.State paramState, AnchorInfo paramAnchorInfo) {
    boolean bool = paramState.isPreLayout();
    boolean bool1 = false;
    if (!bool) {
      int i = this.mPendingScrollPosition;
      if (i != -1) {
        if (i < 0 || i >= paramState.getItemCount()) {
          this.mPendingScrollPosition = -1;
          this.mPendingScrollPositionOffset = Integer.MIN_VALUE;
          return false;
        } 
        paramAnchorInfo.mPosition = this.mPendingScrollPosition;
        SavedState savedState = this.mPendingSavedState;
        if (savedState != null && savedState.hasValidAnchor()) {
          paramAnchorInfo.mLayoutFromEnd = this.mPendingSavedState.mAnchorLayoutFromEnd;
          if (paramAnchorInfo.mLayoutFromEnd) {
            paramAnchorInfo.mCoordinate = this.mOrientationHelper.getEndAfterPadding() - this.mPendingSavedState.mAnchorOffset;
          } else {
            paramAnchorInfo.mCoordinate = this.mOrientationHelper.getStartAfterPadding() + this.mPendingSavedState.mAnchorOffset;
          } 
          return true;
        } 
        if (this.mPendingScrollPositionOffset == Integer.MIN_VALUE) {
          View view = findViewByPosition(this.mPendingScrollPosition);
          if (view != null) {
            OrientationHelper orientationHelper1;
            i = this.mOrientationHelper.getDecoratedMeasurement(view);
            if (i > this.mOrientationHelper.getTotalSpace()) {
              paramAnchorInfo.assignCoordinateFromPadding();
              return true;
            } 
            i = this.mOrientationHelper.getDecoratedStart(view);
            OrientationHelper orientationHelper2 = this.mOrientationHelper;
            int j = orientationHelper2.getStartAfterPadding();
            if (i - j < 0) {
              paramAnchorInfo.mCoordinate = this.mOrientationHelper.getStartAfterPadding();
              paramAnchorInfo.mLayoutFromEnd = false;
              return true;
            } 
            i = this.mOrientationHelper.getEndAfterPadding();
            orientationHelper2 = this.mOrientationHelper;
            j = orientationHelper2.getDecoratedEnd(view);
            if (i - j < 0) {
              paramAnchorInfo.mCoordinate = this.mOrientationHelper.getEndAfterPadding();
              paramAnchorInfo.mLayoutFromEnd = true;
              return true;
            } 
            if (paramAnchorInfo.mLayoutFromEnd) {
              i = this.mOrientationHelper.getDecoratedEnd(view);
              orientationHelper1 = this.mOrientationHelper;
              i += orientationHelper1.getTotalSpaceChange();
            } else {
              i = this.mOrientationHelper.getDecoratedStart((View)orientationHelper1);
            } 
            paramAnchorInfo.mCoordinate = i;
          } else {
            if (getChildCount() > 0) {
              i = getPosition(getChildAt(0));
              if (this.mPendingScrollPosition < i) {
                bool = true;
              } else {
                bool = false;
              } 
              if (bool == this.mShouldReverseLayout)
                bool1 = true; 
              paramAnchorInfo.mLayoutFromEnd = bool1;
            } 
            paramAnchorInfo.assignCoordinateFromPadding();
          } 
          return true;
        } 
        paramAnchorInfo.mLayoutFromEnd = this.mShouldReverseLayout;
        if (this.mShouldReverseLayout) {
          paramAnchorInfo.mCoordinate = this.mOrientationHelper.getEndAfterPadding() - this.mPendingScrollPositionOffset;
        } else {
          paramAnchorInfo.mCoordinate = this.mOrientationHelper.getStartAfterPadding() + this.mPendingScrollPositionOffset;
        } 
        return true;
      } 
    } 
    return false;
  }
  
  private int fixLayoutEndGap(int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, boolean paramBoolean) {
    int i = this.mOrientationHelper.getEndAfterPadding() - paramInt;
    if (i > 0) {
      i = -scrollBy(-i, paramRecycler, paramState);
      if (paramBoolean) {
        paramInt = this.mOrientationHelper.getEndAfterPadding() - paramInt + i;
        if (paramInt > 0) {
          this.mOrientationHelper.offsetChildren(paramInt);
          return paramInt + i;
        } 
      } 
      return i;
    } 
    return 0;
  }
  
  private int fixLayoutStartGap(int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, boolean paramBoolean) {
    int i = paramInt - this.mOrientationHelper.getStartAfterPadding();
    if (i > 0) {
      i = -scrollBy(i, paramRecycler, paramState);
      if (paramBoolean) {
        paramInt = paramInt + i - this.mOrientationHelper.getStartAfterPadding();
        if (paramInt > 0) {
          this.mOrientationHelper.offsetChildren(-paramInt);
          return i - paramInt;
        } 
      } 
      return i;
    } 
    return 0;
  }
  
  private void updateLayoutStateToFillEnd(AnchorInfo paramAnchorInfo) {
    updateLayoutStateToFillEnd(paramAnchorInfo.mPosition, paramAnchorInfo.mCoordinate);
  }
  
  private void updateLayoutStateToFillEnd(int paramInt1, int paramInt2) {
    boolean bool;
    this.mLayoutState.mAvailable = this.mOrientationHelper.getEndAfterPadding() - paramInt2;
    LayoutState layoutState = this.mLayoutState;
    if (this.mShouldReverseLayout) {
      bool = true;
    } else {
      bool = true;
    } 
    layoutState.mItemDirection = bool;
    this.mLayoutState.mCurrentPosition = paramInt1;
    this.mLayoutState.mLayoutDirection = 1;
    this.mLayoutState.mOffset = paramInt2;
    this.mLayoutState.mScrollingOffset = Integer.MIN_VALUE;
  }
  
  private void updateLayoutStateToFillStart(AnchorInfo paramAnchorInfo) {
    updateLayoutStateToFillStart(paramAnchorInfo.mPosition, paramAnchorInfo.mCoordinate);
  }
  
  private void updateLayoutStateToFillStart(int paramInt1, int paramInt2) {
    this.mLayoutState.mAvailable = paramInt2 - this.mOrientationHelper.getStartAfterPadding();
    this.mLayoutState.mCurrentPosition = paramInt1;
    LayoutState layoutState = this.mLayoutState;
    if (this.mShouldReverseLayout) {
      paramInt1 = 1;
    } else {
      paramInt1 = -1;
    } 
    layoutState.mItemDirection = paramInt1;
    this.mLayoutState.mLayoutDirection = -1;
    this.mLayoutState.mOffset = paramInt2;
    this.mLayoutState.mScrollingOffset = Integer.MIN_VALUE;
  }
  
  protected boolean isLayoutRTL() {
    int i = getLayoutDirection();
    boolean bool = true;
    if (i != 1)
      bool = false; 
    return bool;
  }
  
  void ensureLayoutState() {
    if (this.mLayoutState == null)
      this.mLayoutState = createLayoutState(); 
    if (this.mOrientationHelper == null)
      this.mOrientationHelper = OrientationHelper.createOrientationHelper(this, this.mOrientation); 
  }
  
  LayoutState createLayoutState() {
    return new LayoutState();
  }
  
  public void scrollToPosition(int paramInt) {
    this.mPendingScrollPosition = paramInt;
    this.mPendingScrollPositionOffset = Integer.MIN_VALUE;
    SavedState savedState = this.mPendingSavedState;
    if (savedState != null)
      savedState.invalidateAnchor(); 
    requestLayout();
  }
  
  public void scrollToPositionWithOffset(int paramInt1, int paramInt2) {
    this.mPendingScrollPosition = paramInt1;
    this.mPendingScrollPositionOffset = paramInt2;
    SavedState savedState = this.mPendingSavedState;
    if (savedState != null)
      savedState.invalidateAnchor(); 
    requestLayout();
  }
  
  public int scrollHorizontallyBy(int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState) {
    if (this.mOrientation == 1)
      return 0; 
    return scrollBy(paramInt, paramRecycler, paramState);
  }
  
  public int scrollVerticallyBy(int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState) {
    if (this.mOrientation == 0)
      return 0; 
    return scrollBy(paramInt, paramRecycler, paramState);
  }
  
  public int computeHorizontalScrollOffset(RecyclerView.State paramState) {
    return computeScrollOffset(paramState);
  }
  
  public int computeVerticalScrollOffset(RecyclerView.State paramState) {
    return computeScrollOffset(paramState);
  }
  
  public int computeHorizontalScrollExtent(RecyclerView.State paramState) {
    return computeScrollExtent(paramState);
  }
  
  public int computeVerticalScrollExtent(RecyclerView.State paramState) {
    return computeScrollExtent(paramState);
  }
  
  public int computeHorizontalScrollRange(RecyclerView.State paramState) {
    return computeScrollRange(paramState);
  }
  
  public int computeVerticalScrollRange(RecyclerView.State paramState) {
    return computeScrollRange(paramState);
  }
  
  private int computeScrollOffset(RecyclerView.State paramState) {
    if (getChildCount() == 0)
      return 0; 
    ensureLayoutState();
    OrientationHelper orientationHelper = this.mOrientationHelper;
    boolean bool1 = this.mSmoothScrollbarEnabled;
    View view1 = findFirstVisibleChildClosestToStart(bool1 ^ true, true);
    bool1 = this.mSmoothScrollbarEnabled;
    View view2 = findFirstVisibleChildClosestToEnd(bool1 ^ true, true);
    bool1 = this.mSmoothScrollbarEnabled;
    boolean bool2 = this.mShouldReverseLayout;
    return ScrollbarHelper.computeScrollOffset(paramState, orientationHelper, view1, view2, this, bool1, bool2);
  }
  
  private int computeScrollExtent(RecyclerView.State paramState) {
    if (getChildCount() == 0)
      return 0; 
    ensureLayoutState();
    OrientationHelper orientationHelper = this.mOrientationHelper;
    boolean bool = this.mSmoothScrollbarEnabled;
    View view1 = findFirstVisibleChildClosestToStart(bool ^ true, true);
    bool = this.mSmoothScrollbarEnabled;
    View view2 = findFirstVisibleChildClosestToEnd(bool ^ true, true);
    bool = this.mSmoothScrollbarEnabled;
    return ScrollbarHelper.computeScrollExtent(paramState, orientationHelper, view1, view2, this, bool);
  }
  
  private int computeScrollRange(RecyclerView.State paramState) {
    if (getChildCount() == 0)
      return 0; 
    ensureLayoutState();
    OrientationHelper orientationHelper = this.mOrientationHelper;
    boolean bool = this.mSmoothScrollbarEnabled;
    View view1 = findFirstVisibleChildClosestToStart(bool ^ true, true);
    bool = this.mSmoothScrollbarEnabled;
    View view2 = findFirstVisibleChildClosestToEnd(bool ^ true, true);
    bool = this.mSmoothScrollbarEnabled;
    return ScrollbarHelper.computeScrollRange(paramState, orientationHelper, view1, view2, this, bool);
  }
  
  public void setSmoothScrollbarEnabled(boolean paramBoolean) {
    this.mSmoothScrollbarEnabled = paramBoolean;
  }
  
  public boolean isSmoothScrollbarEnabled() {
    return this.mSmoothScrollbarEnabled;
  }
  
  private void updateLayoutState(int paramInt1, int paramInt2, boolean paramBoolean, RecyclerView.State paramState) {
    this.mLayoutState.mInfinite = resolveIsInfinite();
    this.mLayoutState.mExtra = getExtraLayoutSpace(paramState);
    this.mLayoutState.mLayoutDirection = paramInt1;
    byte b = -1;
    if (paramInt1 == 1) {
      LayoutState layoutState = this.mLayoutState;
      layoutState.mExtra += this.mOrientationHelper.getEndPadding();
      View view = getChildClosestToEnd();
      layoutState = this.mLayoutState;
      if (!this.mShouldReverseLayout)
        b = 1; 
      layoutState.mItemDirection = b;
      this.mLayoutState.mCurrentPosition = getPosition(view) + this.mLayoutState.mItemDirection;
      this.mLayoutState.mOffset = this.mOrientationHelper.getDecoratedEnd(view);
      paramInt1 = this.mOrientationHelper.getDecoratedEnd(view);
      OrientationHelper orientationHelper = this.mOrientationHelper;
      paramInt1 -= orientationHelper.getEndAfterPadding();
    } else {
      View view = getChildClosestToStart();
      LayoutState layoutState = this.mLayoutState;
      layoutState.mExtra += this.mOrientationHelper.getStartAfterPadding();
      layoutState = this.mLayoutState;
      if (this.mShouldReverseLayout)
        b = 1; 
      layoutState.mItemDirection = b;
      this.mLayoutState.mCurrentPosition = getPosition(view) + this.mLayoutState.mItemDirection;
      this.mLayoutState.mOffset = this.mOrientationHelper.getDecoratedStart(view);
      paramInt1 = -this.mOrientationHelper.getDecoratedStart(view);
      OrientationHelper orientationHelper = this.mOrientationHelper;
      paramInt1 += orientationHelper.getStartAfterPadding();
    } 
    this.mLayoutState.mAvailable = paramInt2;
    if (paramBoolean) {
      LayoutState layoutState = this.mLayoutState;
      layoutState.mAvailable -= paramInt1;
    } 
    this.mLayoutState.mScrollingOffset = paramInt1;
  }
  
  boolean resolveIsInfinite() {
    if (this.mOrientationHelper.getMode() == 0) {
      OrientationHelper orientationHelper = this.mOrientationHelper;
      if (orientationHelper.getEnd() == 0)
        return true; 
    } 
    return false;
  }
  
  void collectPrefetchPositionsForLayoutState(RecyclerView.State paramState, LayoutState paramLayoutState, RecyclerView.LayoutManager.LayoutPrefetchRegistry paramLayoutPrefetchRegistry) {
    int i = paramLayoutState.mCurrentPosition;
    if (i >= 0 && i < paramState.getItemCount())
      paramLayoutPrefetchRegistry.addPosition(i, paramLayoutState.mScrollingOffset); 
  }
  
  public void collectInitialPrefetchPositions(int paramInt, RecyclerView.LayoutManager.LayoutPrefetchRegistry paramLayoutPrefetchRegistry) {
    boolean bool;
    SavedState savedState = this.mPendingSavedState;
    byte b = -1;
    if (savedState != null && savedState.hasValidAnchor()) {
      bool = this.mPendingSavedState.mAnchorLayoutFromEnd;
      i = this.mPendingSavedState.mAnchorPosition;
    } else {
      resolveShouldLayoutReverse();
      bool = this.mShouldReverseLayout;
      if (this.mPendingScrollPosition == -1) {
        if (bool) {
          i = paramInt - 1;
        } else {
          i = 0;
        } 
      } else {
        i = this.mPendingScrollPosition;
      } 
    } 
    if (!bool)
      b = 1; 
    int j = i;
    for (int i = 0; i < this.mInitialItemPrefetchCount && 
      j >= 0 && j < paramInt; i++) {
      paramLayoutPrefetchRegistry.addPosition(j, 0);
      j += b;
    } 
  }
  
  public void setInitialPrefetchItemCount(int paramInt) {
    this.mInitialItemPrefetchCount = paramInt;
  }
  
  public int getInitialItemPrefetchCount() {
    return this.mInitialItemPrefetchCount;
  }
  
  public void collectAdjacentPrefetchPositions(int paramInt1, int paramInt2, RecyclerView.State paramState, RecyclerView.LayoutManager.LayoutPrefetchRegistry paramLayoutPrefetchRegistry) {
    if (this.mOrientation != 0)
      paramInt1 = paramInt2; 
    if (getChildCount() == 0 || paramInt1 == 0)
      return; 
    if (paramInt1 > 0) {
      paramInt2 = 1;
    } else {
      paramInt2 = -1;
    } 
    paramInt1 = Math.abs(paramInt1);
    updateLayoutState(paramInt2, paramInt1, true, paramState);
    collectPrefetchPositionsForLayoutState(paramState, this.mLayoutState, paramLayoutPrefetchRegistry);
  }
  
  int scrollBy(int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState) {
    byte b;
    if (getChildCount() == 0 || paramInt == 0)
      return 0; 
    this.mLayoutState.mRecycle = true;
    ensureLayoutState();
    if (paramInt > 0) {
      b = 1;
    } else {
      b = -1;
    } 
    int i = Math.abs(paramInt);
    updateLayoutState(b, i, true, paramState);
    int j = this.mLayoutState.mScrollingOffset;
    LayoutState layoutState = this.mLayoutState;
    j += fill(paramRecycler, layoutState, paramState, false);
    if (j < 0)
      return 0; 
    if (i > j)
      paramInt = b * j; 
    this.mOrientationHelper.offsetChildren(-paramInt);
    this.mLayoutState.mLastScrollDelta = paramInt;
    return paramInt;
  }
  
  public void assertNotInLayoutOrScroll(String paramString) {
    if (this.mPendingSavedState == null)
      super.assertNotInLayoutOrScroll(paramString); 
  }
  
  private void recycleChildren(RecyclerView.Recycler paramRecycler, int paramInt1, int paramInt2) {
    if (paramInt1 == paramInt2)
      return; 
    if (paramInt2 > paramInt1) {
      for (; --paramInt2 >= paramInt1; paramInt2--)
        removeAndRecycleViewAt(paramInt2, paramRecycler); 
    } else {
      for (; paramInt1 > paramInt2; paramInt1--)
        removeAndRecycleViewAt(paramInt1, paramRecycler); 
    } 
  }
  
  private void recycleViewsFromStart(RecyclerView.Recycler paramRecycler, int paramInt) {
    if (paramInt < 0)
      return; 
    int i = getChildCount();
    if (this.mShouldReverseLayout) {
      for (int j = i - 1; j >= 0; ) {
        View view = getChildAt(j);
        if (this.mOrientationHelper.getDecoratedEnd(view) <= paramInt) {
          OrientationHelper orientationHelper = this.mOrientationHelper;
          if (orientationHelper.getTransformedEndWithDecoration(view) <= paramInt) {
            j--;
            continue;
          } 
        } 
        recycleChildren(paramRecycler, i - 1, j);
        return;
      } 
    } else {
      for (byte b = 0; b < i; ) {
        View view = getChildAt(b);
        if (this.mOrientationHelper.getDecoratedEnd(view) <= paramInt) {
          OrientationHelper orientationHelper = this.mOrientationHelper;
          if (orientationHelper.getTransformedEndWithDecoration(view) <= paramInt) {
            b++;
            continue;
          } 
        } 
        recycleChildren(paramRecycler, 0, b);
        return;
      } 
    } 
  }
  
  private void recycleViewsFromEnd(RecyclerView.Recycler paramRecycler, int paramInt) {
    int i = getChildCount();
    if (paramInt < 0)
      return; 
    int j = this.mOrientationHelper.getEnd() - paramInt;
    if (this.mShouldReverseLayout) {
      for (paramInt = 0; paramInt < i; ) {
        View view = getChildAt(paramInt);
        if (this.mOrientationHelper.getDecoratedStart(view) >= j) {
          OrientationHelper orientationHelper = this.mOrientationHelper;
          if (orientationHelper.getTransformedStartWithDecoration(view) >= j) {
            paramInt++;
            continue;
          } 
        } 
        recycleChildren(paramRecycler, 0, paramInt);
        return;
      } 
    } else {
      for (paramInt = i - 1; paramInt >= 0; ) {
        View view = getChildAt(paramInt);
        if (this.mOrientationHelper.getDecoratedStart(view) >= j) {
          OrientationHelper orientationHelper = this.mOrientationHelper;
          if (orientationHelper.getTransformedStartWithDecoration(view) >= j) {
            paramInt--;
            continue;
          } 
        } 
        recycleChildren(paramRecycler, i - 1, paramInt);
        return;
      } 
    } 
  }
  
  private void recycleByLayoutState(RecyclerView.Recycler paramRecycler, LayoutState paramLayoutState) {
    if (!paramLayoutState.mRecycle || paramLayoutState.mInfinite)
      return; 
    if (paramLayoutState.mLayoutDirection == -1) {
      recycleViewsFromEnd(paramRecycler, paramLayoutState.mScrollingOffset);
    } else {
      recycleViewsFromStart(paramRecycler, paramLayoutState.mScrollingOffset);
    } 
  }
  
  int fill(RecyclerView.Recycler paramRecycler, LayoutState paramLayoutState, RecyclerView.State paramState, boolean paramBoolean) {
    // Byte code:
    //   0: aload_2
    //   1: getfield mAvailable : I
    //   4: istore #5
    //   6: aload_2
    //   7: getfield mScrollingOffset : I
    //   10: ldc -2147483648
    //   12: if_icmpeq -> 41
    //   15: aload_2
    //   16: getfield mAvailable : I
    //   19: ifge -> 35
    //   22: aload_2
    //   23: aload_2
    //   24: getfield mScrollingOffset : I
    //   27: aload_2
    //   28: getfield mAvailable : I
    //   31: iadd
    //   32: putfield mScrollingOffset : I
    //   35: aload_0
    //   36: aload_1
    //   37: aload_2
    //   38: invokespecial recycleByLayoutState : (Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/LinearLayoutManager$LayoutState;)V
    //   41: aload_2
    //   42: getfield mAvailable : I
    //   45: aload_2
    //   46: getfield mExtra : I
    //   49: iadd
    //   50: istore #6
    //   52: aload_0
    //   53: getfield mLayoutChunkResult : Lcom/android/internal/widget/LinearLayoutManager$LayoutChunkResult;
    //   56: astore #7
    //   58: aload_2
    //   59: getfield mInfinite : Z
    //   62: ifne -> 70
    //   65: iload #6
    //   67: ifle -> 245
    //   70: aload_2
    //   71: aload_3
    //   72: invokevirtual hasMore : (Lcom/android/internal/widget/RecyclerView$State;)Z
    //   75: ifeq -> 245
    //   78: aload #7
    //   80: invokevirtual resetInternal : ()V
    //   83: aload_0
    //   84: aload_1
    //   85: aload_3
    //   86: aload_2
    //   87: aload #7
    //   89: invokevirtual layoutChunk : (Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;Lcom/android/internal/widget/LinearLayoutManager$LayoutState;Lcom/android/internal/widget/LinearLayoutManager$LayoutChunkResult;)V
    //   92: aload #7
    //   94: getfield mFinished : Z
    //   97: ifeq -> 103
    //   100: goto -> 245
    //   103: aload_2
    //   104: aload_2
    //   105: getfield mOffset : I
    //   108: aload #7
    //   110: getfield mConsumed : I
    //   113: aload_2
    //   114: getfield mLayoutDirection : I
    //   117: imul
    //   118: iadd
    //   119: putfield mOffset : I
    //   122: aload #7
    //   124: getfield mIgnoreConsumed : Z
    //   127: ifeq -> 151
    //   130: aload_0
    //   131: getfield mLayoutState : Lcom/android/internal/widget/LinearLayoutManager$LayoutState;
    //   134: getfield mScrapList : Ljava/util/List;
    //   137: ifnonnull -> 151
    //   140: iload #6
    //   142: istore #8
    //   144: aload_3
    //   145: invokevirtual isPreLayout : ()Z
    //   148: ifne -> 175
    //   151: aload_2
    //   152: aload_2
    //   153: getfield mAvailable : I
    //   156: aload #7
    //   158: getfield mConsumed : I
    //   161: isub
    //   162: putfield mAvailable : I
    //   165: iload #6
    //   167: aload #7
    //   169: getfield mConsumed : I
    //   172: isub
    //   173: istore #8
    //   175: aload_2
    //   176: getfield mScrollingOffset : I
    //   179: ldc -2147483648
    //   181: if_icmpeq -> 224
    //   184: aload_2
    //   185: aload_2
    //   186: getfield mScrollingOffset : I
    //   189: aload #7
    //   191: getfield mConsumed : I
    //   194: iadd
    //   195: putfield mScrollingOffset : I
    //   198: aload_2
    //   199: getfield mAvailable : I
    //   202: ifge -> 218
    //   205: aload_2
    //   206: aload_2
    //   207: getfield mScrollingOffset : I
    //   210: aload_2
    //   211: getfield mAvailable : I
    //   214: iadd
    //   215: putfield mScrollingOffset : I
    //   218: aload_0
    //   219: aload_1
    //   220: aload_2
    //   221: invokespecial recycleByLayoutState : (Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/LinearLayoutManager$LayoutState;)V
    //   224: iload #8
    //   226: istore #6
    //   228: iload #4
    //   230: ifeq -> 58
    //   233: iload #8
    //   235: istore #6
    //   237: aload #7
    //   239: getfield mFocusable : Z
    //   242: ifeq -> 58
    //   245: iload #5
    //   247: aload_2
    //   248: getfield mAvailable : I
    //   251: isub
    //   252: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1474	-> 0
    //   #1475	-> 6
    //   #1477	-> 15
    //   #1478	-> 22
    //   #1480	-> 35
    //   #1482	-> 41
    //   #1483	-> 52
    //   #1484	-> 58
    //   #1485	-> 78
    //   #1486	-> 83
    //   #1487	-> 92
    //   #1488	-> 100
    //   #1490	-> 103
    //   #1497	-> 122
    //   #1498	-> 140
    //   #1499	-> 151
    //   #1501	-> 165
    //   #1504	-> 175
    //   #1505	-> 184
    //   #1506	-> 198
    //   #1507	-> 205
    //   #1509	-> 218
    //   #1511	-> 224
    //   #1518	-> 245
  }
  
  void layoutChunk(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, LayoutState paramLayoutState, LayoutChunkResult paramLayoutChunkResult) {
    int i, j, k, m;
    View view = paramLayoutState.next(paramRecycler);
    if (view == null) {
      paramLayoutChunkResult.mFinished = true;
      return;
    } 
    RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams)view.getLayoutParams();
    if (paramLayoutState.mScrapList == null) {
      boolean bool2, bool1 = this.mShouldReverseLayout;
      if (paramLayoutState.mLayoutDirection == -1) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if (bool1 == bool2) {
        addView(view);
      } else {
        addView(view, 0);
      } 
    } else {
      boolean bool2, bool1 = this.mShouldReverseLayout;
      if (paramLayoutState.mLayoutDirection == -1) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if (bool1 == bool2) {
        addDisappearingView(view);
      } else {
        addDisappearingView(view, 0);
      } 
    } 
    measureChildWithMargins(view, 0, 0);
    paramLayoutChunkResult.mConsumed = this.mOrientationHelper.getDecoratedMeasurement(view);
    if (this.mOrientation == 1) {
      if (isLayoutRTL()) {
        i = getWidth() - getPaddingRight();
        j = i - this.mOrientationHelper.getDecoratedMeasurementInOther(view);
      } else {
        j = getPaddingLeft();
        i = this.mOrientationHelper.getDecoratedMeasurementInOther(view) + j;
      } 
      if (paramLayoutState.mLayoutDirection == -1) {
        k = paramLayoutState.mOffset;
        int n = paramLayoutState.mOffset, i1 = paramLayoutChunkResult.mConsumed;
        m = j;
        i1 = n - i1;
        j = i;
        i = k;
        k = i1;
      } else {
        k = paramLayoutState.mOffset;
        int n = paramLayoutState.mOffset;
        m = paramLayoutChunkResult.mConsumed;
        n += m;
        m = j;
        j = i;
        i = n;
      } 
    } else {
      j = getPaddingTop();
      i = this.mOrientationHelper.getDecoratedMeasurementInOther(view) + j;
      if (paramLayoutState.mLayoutDirection == -1) {
        m = paramLayoutState.mOffset;
        k = paramLayoutState.mOffset;
        int n = paramLayoutChunkResult.mConsumed;
        n = k - n;
        k = j;
        j = m;
        m = n;
      } else {
        m = paramLayoutState.mOffset;
        int n = paramLayoutState.mOffset;
        k = paramLayoutChunkResult.mConsumed;
        n += k;
        k = j;
        j = n;
      } 
    } 
    layoutDecoratedWithMargins(view, m, k, j, i);
    if (layoutParams.isItemRemoved() || layoutParams.isItemChanged())
      paramLayoutChunkResult.mIgnoreConsumed = true; 
    paramLayoutChunkResult.mFocusable = view.isFocusable();
  }
  
  boolean shouldMeasureTwice() {
    boolean bool;
    if (getHeightMode() != 1073741824 && 
      getWidthMode() != 1073741824 && 
      hasFlexibleChildInBothOrientations()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  int convertFocusDirectionToLayoutDirection(int paramInt) {
    int i = -1;
    boolean bool1 = true, bool2 = true;
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 17) {
          if (paramInt != 33) {
            if (paramInt != 66) {
              if (paramInt != 130)
                return Integer.MIN_VALUE; 
              if (this.mOrientation == 1) {
                paramInt = bool2;
              } else {
                paramInt = Integer.MIN_VALUE;
              } 
              return paramInt;
            } 
            if (this.mOrientation == 0) {
              paramInt = bool1;
            } else {
              paramInt = Integer.MIN_VALUE;
            } 
            return paramInt;
          } 
          if (this.mOrientation != 1)
            i = Integer.MIN_VALUE; 
          return i;
        } 
        if (this.mOrientation != 0)
          i = Integer.MIN_VALUE; 
        return i;
      } 
      if (this.mOrientation == 1)
        return 1; 
      if (isLayoutRTL())
        return -1; 
      return 1;
    } 
    if (this.mOrientation == 1)
      return -1; 
    if (isLayoutRTL())
      return 1; 
    return -1;
  }
  
  private View getChildClosestToStart() {
    boolean bool;
    if (this.mShouldReverseLayout) {
      bool = getChildCount() - 1;
    } else {
      bool = false;
    } 
    return getChildAt(bool);
  }
  
  private View getChildClosestToEnd() {
    int i;
    if (this.mShouldReverseLayout) {
      i = 0;
    } else {
      i = getChildCount() - 1;
    } 
    return getChildAt(i);
  }
  
  private View findFirstVisibleChildClosestToStart(boolean paramBoolean1, boolean paramBoolean2) {
    if (this.mShouldReverseLayout)
      return findOneVisibleChild(getChildCount() - 1, -1, paramBoolean1, paramBoolean2); 
    return findOneVisibleChild(0, getChildCount(), paramBoolean1, paramBoolean2);
  }
  
  private View findFirstVisibleChildClosestToEnd(boolean paramBoolean1, boolean paramBoolean2) {
    if (this.mShouldReverseLayout)
      return findOneVisibleChild(0, getChildCount(), paramBoolean1, paramBoolean2); 
    return findOneVisibleChild(getChildCount() - 1, -1, paramBoolean1, paramBoolean2);
  }
  
  private View findReferenceChildClosestToEnd(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState) {
    View view;
    if (this.mShouldReverseLayout) {
      view = findFirstReferenceChild(paramRecycler, paramState);
    } else {
      view = findLastReferenceChild((RecyclerView.Recycler)view, paramState);
    } 
    return view;
  }
  
  private View findReferenceChildClosestToStart(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState) {
    View view;
    if (this.mShouldReverseLayout) {
      view = findLastReferenceChild(paramRecycler, paramState);
    } else {
      view = findFirstReferenceChild((RecyclerView.Recycler)view, paramState);
    } 
    return view;
  }
  
  private View findFirstReferenceChild(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState) {
    return findReferenceChild(paramRecycler, paramState, 0, getChildCount(), paramState.getItemCount());
  }
  
  private View findLastReferenceChild(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState) {
    return findReferenceChild(paramRecycler, paramState, getChildCount() - 1, -1, paramState.getItemCount());
  }
  
  View findReferenceChild(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, int paramInt1, int paramInt2, int paramInt3) {
    RecyclerView.State state;
    byte b;
    ensureLayoutState();
    paramState = null;
    paramRecycler = null;
    int i = this.mOrientationHelper.getStartAfterPadding();
    int j = this.mOrientationHelper.getEndAfterPadding();
    if (paramInt2 > paramInt1) {
      b = 1;
    } else {
      b = -1;
    } 
    View view;
    for (; paramInt1 != paramInt2; paramInt1 += b, paramState = state1, view = view2) {
      View view2, view1 = getChildAt(paramInt1);
      int k = getPosition(view1);
      RecyclerView.State state1 = paramState;
      RecyclerView.Recycler recycler = paramRecycler;
      if (k >= 0) {
        state1 = paramState;
        recycler = paramRecycler;
        if (k < paramInt3)
          if (((RecyclerView.LayoutParams)view1.getLayoutParams()).isItemRemoved()) {
            state1 = paramState;
            recycler = paramRecycler;
            if (paramState == null) {
              View view3 = view1;
              recycler = paramRecycler;
            } 
          } else {
            if (this.mOrientationHelper.getDecoratedStart(view1) < j) {
              OrientationHelper orientationHelper = this.mOrientationHelper;
              if (orientationHelper.getDecoratedEnd(view1) >= i)
                return view1; 
            } 
            state1 = paramState;
            recycler = paramRecycler;
            if (paramRecycler == null) {
              view2 = view1;
              state1 = paramState;
            } 
          }  
      } 
    } 
    if (view == null)
      state = paramState; 
    return (View)state;
  }
  
  public int findFirstVisibleItemPosition() {
    int i;
    View view = findOneVisibleChild(0, getChildCount(), false, true);
    if (view == null) {
      i = -1;
    } else {
      i = getPosition(view);
    } 
    return i;
  }
  
  public int findFirstCompletelyVisibleItemPosition() {
    int i;
    View view = findOneVisibleChild(0, getChildCount(), true, false);
    if (view == null) {
      i = -1;
    } else {
      i = getPosition(view);
    } 
    return i;
  }
  
  public int findLastVisibleItemPosition() {
    int i = getChildCount(), j = -1;
    View view = findOneVisibleChild(i - 1, -1, false, true);
    if (view != null)
      j = getPosition(view); 
    return j;
  }
  
  public int findLastCompletelyVisibleItemPosition() {
    int i = getChildCount(), j = -1;
    View view = findOneVisibleChild(i - 1, -1, true, false);
    if (view != null)
      j = getPosition(view); 
    return j;
  }
  
  View findOneVisibleChild(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2) {
    byte b;
    ensureLayoutState();
    int i = this.mOrientationHelper.getStartAfterPadding();
    int j = this.mOrientationHelper.getEndAfterPadding();
    if (paramInt2 > paramInt1) {
      b = 1;
    } else {
      b = -1;
    } 
    View view = null;
    for (; paramInt1 != paramInt2; paramInt1 += b, view = view2) {
      View view1 = getChildAt(paramInt1);
      int k = this.mOrientationHelper.getDecoratedStart(view1);
      int m = this.mOrientationHelper.getDecoratedEnd(view1);
      View view2 = view;
      if (k < j) {
        view2 = view;
        if (m > i)
          if (paramBoolean1) {
            if (k >= i && m <= j)
              return view1; 
            view2 = view;
            if (paramBoolean2) {
              view2 = view;
              if (view == null)
                view2 = view1; 
            } 
          } else {
            return view1;
          }  
      } 
    } 
    return view;
  }
  
  public View onFocusSearchFailed(View paramView, int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState) {
    View view;
    resolveShouldLayoutReverse();
    if (getChildCount() == 0)
      return null; 
    paramInt = convertFocusDirectionToLayoutDirection(paramInt);
    if (paramInt == Integer.MIN_VALUE)
      return null; 
    ensureLayoutState();
    if (paramInt == -1) {
      paramView = findReferenceChildClosestToStart(paramRecycler, paramState);
    } else {
      paramView = findReferenceChildClosestToEnd(paramRecycler, paramState);
    } 
    if (paramView == null)
      return null; 
    ensureLayoutState();
    int i = (int)(this.mOrientationHelper.getTotalSpace() * 0.33333334F);
    updateLayoutState(paramInt, i, false, paramState);
    this.mLayoutState.mScrollingOffset = Integer.MIN_VALUE;
    this.mLayoutState.mRecycle = false;
    fill(paramRecycler, this.mLayoutState, paramState, true);
    if (paramInt == -1) {
      view = getChildClosestToStart();
    } else {
      view = getChildClosestToEnd();
    } 
    if (view == paramView || !view.isFocusable())
      return null; 
    return view;
  }
  
  private void logChildren() {
    Log.d("LinearLayoutManager", "internal representation of views on the screen");
    for (byte b = 0; b < getChildCount(); b++) {
      View view = getChildAt(b);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("item ");
      stringBuilder.append(getPosition(view));
      stringBuilder.append(", coord:");
      OrientationHelper orientationHelper = this.mOrientationHelper;
      stringBuilder.append(orientationHelper.getDecoratedStart(view));
      String str = stringBuilder.toString();
      Log.d("LinearLayoutManager", str);
    } 
    Log.d("LinearLayoutManager", "==============");
  }
  
  void validateChildOrder() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("validating child count ");
    stringBuilder.append(getChildCount());
    Log.d("LinearLayoutManager", stringBuilder.toString());
    int i = getChildCount();
    boolean bool1 = true, bool2 = true;
    if (i < 1)
      return; 
    int j = getPosition(getChildAt(0));
    int k = this.mOrientationHelper.getDecoratedStart(getChildAt(0));
    if (this.mShouldReverseLayout) {
      for (i = 1; i < getChildCount(); ) {
        View view = getChildAt(i);
        int m = getPosition(view);
        int n = this.mOrientationHelper.getDecoratedStart(view);
        if (m < j) {
          logChildren();
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("detected invalid position. loc invalid? ");
          if (n >= k)
            bool2 = false; 
          stringBuilder1.append(bool2);
          throw new RuntimeException(stringBuilder1.toString());
        } 
        if (n <= k) {
          i++;
          continue;
        } 
        logChildren();
        throw new RuntimeException("detected invalid location");
      } 
    } else {
      for (i = 1; i < getChildCount(); ) {
        View view = getChildAt(i);
        int n = getPosition(view);
        int m = this.mOrientationHelper.getDecoratedStart(view);
        if (n < j) {
          logChildren();
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("detected invalid position. loc invalid? ");
          if (m < k) {
            bool2 = bool1;
          } else {
            bool2 = false;
          } 
          stringBuilder1.append(bool2);
          throw new RuntimeException(stringBuilder1.toString());
        } 
        if (m >= k) {
          i++;
          continue;
        } 
        logChildren();
        throw new RuntimeException("detected invalid location");
      } 
    } 
  }
  
  public boolean supportsPredictiveItemAnimations() {
    boolean bool;
    if (this.mPendingSavedState == null && this.mLastStackFromEnd == this.mStackFromEnd) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void prepareForDrop(View paramView1, View paramView2, int paramInt1, int paramInt2) {
    OrientationHelper orientationHelper1, orientationHelper2;
    assertNotInLayoutOrScroll("Cannot drop a view during a scroll or layout calculation");
    ensureLayoutState();
    resolveShouldLayoutReverse();
    paramInt1 = getPosition(paramView1);
    paramInt2 = getPosition(paramView2);
    if (paramInt1 < paramInt2) {
      paramInt1 = 1;
    } else {
      paramInt1 = -1;
    } 
    if (this.mShouldReverseLayout) {
      if (paramInt1 == 1) {
        OrientationHelper orientationHelper = this.mOrientationHelper;
        int i = orientationHelper.getEndAfterPadding();
        orientationHelper = this.mOrientationHelper;
        paramInt1 = orientationHelper.getDecoratedStart(paramView2);
        orientationHelper2 = this.mOrientationHelper;
        int j = orientationHelper2.getDecoratedMeasurement(paramView1);
        scrollToPositionWithOffset(paramInt2, i - paramInt1 + j);
      } else {
        orientationHelper1 = this.mOrientationHelper;
        paramInt1 = orientationHelper1.getEndAfterPadding();
        orientationHelper1 = this.mOrientationHelper;
        int i = orientationHelper1.getDecoratedEnd((View)orientationHelper2);
        scrollToPositionWithOffset(paramInt2, paramInt1 - i);
      } 
    } else if (paramInt1 == -1) {
      scrollToPositionWithOffset(paramInt2, this.mOrientationHelper.getDecoratedStart((View)orientationHelper2));
    } else {
      OrientationHelper orientationHelper = this.mOrientationHelper;
      int i = orientationHelper.getDecoratedEnd((View)orientationHelper2);
      orientationHelper2 = this.mOrientationHelper;
      paramInt1 = orientationHelper2.getDecoratedMeasurement((View)orientationHelper1);
      scrollToPositionWithOffset(paramInt2, i - paramInt1);
    } 
  }
  
  class LayoutState {
    boolean mRecycle = true;
    
    int mExtra = 0;
    
    boolean mIsPreLayout = false;
    
    List<RecyclerView.ViewHolder> mScrapList = null;
    
    static final int INVALID_LAYOUT = -2147483648;
    
    static final int ITEM_DIRECTION_HEAD = -1;
    
    static final int ITEM_DIRECTION_TAIL = 1;
    
    static final int LAYOUT_END = 1;
    
    static final int LAYOUT_START = -1;
    
    static final int SCROLLING_OFFSET_NaN = -2147483648;
    
    static final String TAG = "LLM#LayoutState";
    
    int mAvailable;
    
    int mCurrentPosition;
    
    boolean mInfinite;
    
    int mItemDirection;
    
    int mLastScrollDelta;
    
    int mLayoutDirection;
    
    int mOffset;
    
    int mScrollingOffset;
    
    boolean hasMore(RecyclerView.State param1State) {
      boolean bool;
      int i = this.mCurrentPosition;
      if (i >= 0 && i < param1State.getItemCount()) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    View next(RecyclerView.Recycler param1Recycler) {
      if (this.mScrapList != null)
        return nextViewFromScrapList(); 
      View view = param1Recycler.getViewForPosition(this.mCurrentPosition);
      this.mCurrentPosition += this.mItemDirection;
      return view;
    }
    
    private View nextViewFromScrapList() {
      int i = this.mScrapList.size();
      for (byte b = 0; b < i; b++) {
        View view = ((RecyclerView.ViewHolder)this.mScrapList.get(b)).itemView;
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams)view.getLayoutParams();
        if (!layoutParams.isItemRemoved())
          if (this.mCurrentPosition == layoutParams.getViewLayoutPosition()) {
            assignPositionFromScrapList(view);
            return view;
          }  
      } 
      return null;
    }
    
    public void assignPositionFromScrapList() {
      assignPositionFromScrapList(null);
    }
    
    public void assignPositionFromScrapList(View param1View) {
      param1View = nextViewInLimitedList(param1View);
      if (param1View == null) {
        this.mCurrentPosition = -1;
      } else {
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams)param1View.getLayoutParams();
        this.mCurrentPosition = layoutParams.getViewLayoutPosition();
      } 
    }
    
    public View nextViewInLimitedList(View param1View) {
      View view2;
      int i = this.mScrapList.size();
      View view1 = null;
      int j = Integer.MAX_VALUE;
      byte b = 0;
      while (true) {
        view2 = view1;
        if (b < i) {
          View view = ((RecyclerView.ViewHolder)this.mScrapList.get(b)).itemView;
          RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams)view.getLayoutParams();
          view2 = view1;
          int k = j;
          if (view != param1View)
            if (layoutParams.isItemRemoved()) {
              view2 = view1;
              k = j;
            } else {
              int m = (layoutParams.getViewLayoutPosition() - this.mCurrentPosition) * this.mItemDirection;
              if (m < 0) {
                view2 = view1;
                k = j;
              } else {
                view2 = view1;
                k = j;
                if (m < j) {
                  view1 = view;
                  k = m;
                  view2 = view1;
                  if (m == 0) {
                    view2 = view1;
                    break;
                  } 
                } 
              } 
            }  
          b++;
          view1 = view2;
          j = k;
          continue;
        } 
        break;
      } 
      return view2;
    }
    
    void log() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("avail:");
      stringBuilder.append(this.mAvailable);
      stringBuilder.append(", ind:");
      stringBuilder.append(this.mCurrentPosition);
      stringBuilder.append(", dir:");
      stringBuilder.append(this.mItemDirection);
      stringBuilder.append(", offset:");
      stringBuilder.append(this.mOffset);
      stringBuilder.append(", layoutDir:");
      stringBuilder.append(this.mLayoutDirection);
      Log.d("LLM#LayoutState", stringBuilder.toString());
    }
  }
  
  public static class SavedState implements Parcelable {
    public SavedState() {}
    
    SavedState(Parcel param1Parcel) {
      this.mAnchorPosition = param1Parcel.readInt();
      this.mAnchorOffset = param1Parcel.readInt();
      int i = param1Parcel.readInt();
      boolean bool = true;
      if (i != 1)
        bool = false; 
      this.mAnchorLayoutFromEnd = bool;
    }
    
    public SavedState(SavedState param1SavedState) {
      this.mAnchorPosition = param1SavedState.mAnchorPosition;
      this.mAnchorOffset = param1SavedState.mAnchorOffset;
      this.mAnchorLayoutFromEnd = param1SavedState.mAnchorLayoutFromEnd;
    }
    
    boolean hasValidAnchor() {
      boolean bool;
      if (this.mAnchorPosition >= 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    void invalidateAnchor() {
      this.mAnchorPosition = -1;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.mAnchorPosition);
      param1Parcel.writeInt(this.mAnchorOffset);
      param1Parcel.writeInt(this.mAnchorLayoutFromEnd);
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
        public LinearLayoutManager.SavedState createFromParcel(Parcel param2Parcel) {
          return new LinearLayoutManager.SavedState(param2Parcel);
        }
        
        public LinearLayoutManager.SavedState[] newArray(int param2Int) {
          return new LinearLayoutManager.SavedState[param2Int];
        }
      };
    
    boolean mAnchorLayoutFromEnd;
    
    int mAnchorOffset;
    
    int mAnchorPosition;
  }
  
  class null implements Parcelable.Creator<SavedState> {
    public LinearLayoutManager.SavedState createFromParcel(Parcel param1Parcel) {
      return new LinearLayoutManager.SavedState(param1Parcel);
    }
    
    public LinearLayoutManager.SavedState[] newArray(int param1Int) {
      return new LinearLayoutManager.SavedState[param1Int];
    }
  }
  
  class AnchorInfo {
    int mCoordinate;
    
    boolean mLayoutFromEnd;
    
    int mPosition;
    
    boolean mValid;
    
    final LinearLayoutManager this$0;
    
    AnchorInfo() {
      reset();
    }
    
    void reset() {
      this.mPosition = -1;
      this.mCoordinate = Integer.MIN_VALUE;
      this.mLayoutFromEnd = false;
      this.mValid = false;
    }
    
    void assignCoordinateFromPadding() {
      int i;
      if (this.mLayoutFromEnd) {
        i = LinearLayoutManager.this.mOrientationHelper.getEndAfterPadding();
      } else {
        i = LinearLayoutManager.this.mOrientationHelper.getStartAfterPadding();
      } 
      this.mCoordinate = i;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("AnchorInfo{mPosition=");
      stringBuilder.append(this.mPosition);
      stringBuilder.append(", mCoordinate=");
      stringBuilder.append(this.mCoordinate);
      stringBuilder.append(", mLayoutFromEnd=");
      stringBuilder.append(this.mLayoutFromEnd);
      stringBuilder.append(", mValid=");
      stringBuilder.append(this.mValid);
      stringBuilder.append('}');
      return stringBuilder.toString();
    }
    
    boolean isViewValidAsAnchor(View param1View, RecyclerView.State param1State) {
      boolean bool;
      RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams)param1View.getLayoutParams();
      if (!layoutParams.isItemRemoved() && layoutParams.getViewLayoutPosition() >= 0 && 
        layoutParams.getViewLayoutPosition() < param1State.getItemCount()) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public void assignFromViewAndKeepVisibleRect(View param1View) {
      int i = LinearLayoutManager.this.mOrientationHelper.getTotalSpaceChange();
      if (i >= 0) {
        assignFromView(param1View);
        return;
      } 
      this.mPosition = LinearLayoutManager.this.getPosition(param1View);
      if (this.mLayoutFromEnd) {
        int j = LinearLayoutManager.this.mOrientationHelper.getEndAfterPadding();
        int k = LinearLayoutManager.this.mOrientationHelper.getDecoratedEnd(param1View);
        j = j - i - k;
        this.mCoordinate = LinearLayoutManager.this.mOrientationHelper.getEndAfterPadding() - j;
        if (j > 0) {
          i = LinearLayoutManager.this.mOrientationHelper.getDecoratedMeasurement(param1View);
          int m = this.mCoordinate;
          k = LinearLayoutManager.this.mOrientationHelper.getStartAfterPadding();
          int n = LinearLayoutManager.this.mOrientationHelper.getDecoratedStart(param1View);
          n = Math.min(n - k, 0);
          i = m - i - n + k;
          if (i < 0)
            this.mCoordinate += Math.min(j, -i); 
        } 
      } else {
        int k = LinearLayoutManager.this.mOrientationHelper.getDecoratedStart(param1View);
        int j = k - LinearLayoutManager.this.mOrientationHelper.getStartAfterPadding();
        this.mCoordinate = k;
        if (j > 0) {
          OrientationHelper orientationHelper = LinearLayoutManager.this.mOrientationHelper;
          int n = orientationHelper.getDecoratedMeasurement(param1View);
          int i1 = LinearLayoutManager.this.mOrientationHelper.getEndAfterPadding();
          orientationHelper = LinearLayoutManager.this.mOrientationHelper;
          int i2 = orientationHelper.getDecoratedEnd(param1View);
          int m = LinearLayoutManager.this.mOrientationHelper.getEndAfterPadding();
          i = Math.min(0, i1 - i - i2);
          i = m - i - n + k;
          if (i < 0)
            this.mCoordinate -= Math.min(j, -i); 
        } 
      } 
    }
    
    public void assignFromView(View param1View) {
      if (this.mLayoutFromEnd) {
        int i = LinearLayoutManager.this.mOrientationHelper.getDecoratedEnd(param1View);
        OrientationHelper orientationHelper = LinearLayoutManager.this.mOrientationHelper;
        this.mCoordinate = i + orientationHelper.getTotalSpaceChange();
      } else {
        this.mCoordinate = LinearLayoutManager.this.mOrientationHelper.getDecoratedStart(param1View);
      } 
      this.mPosition = LinearLayoutManager.this.getPosition(param1View);
    }
  }
  
  class LayoutChunkResult {
    public int mConsumed;
    
    public boolean mFinished;
    
    public boolean mFocusable;
    
    public boolean mIgnoreConsumed;
    
    void resetInternal() {
      this.mConsumed = 0;
      this.mFinished = false;
      this.mIgnoreConsumed = false;
      this.mFocusable = false;
    }
  }
}
