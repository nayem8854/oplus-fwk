package com.android.internal.widget;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Slog;
import android.view.GestureDetector;
import android.view.IWindow;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import com.android.internal.policy.OplusZoomWindowDecorViewHelper;
import com.android.internal.policy.PhoneWindow;
import com.oplus.zoomwindow.OplusZoomInputEventInfo;
import com.oplus.zoomwindow.OplusZoomWindowManager;

public class ZoomWindowDecorView extends ViewGroup implements View.OnTouchListener, GestureDetector.OnGestureListener {
  private PhoneWindow mOwner = null;
  
  private boolean mShow = false;
  
  private boolean mDragging = false;
  
  private boolean mLongPress = false;
  
  private Rect mHandleViewRect = new Rect();
  
  private Rect mHandleViewShowTipsRect = new Rect();
  
  private Rect mClosedViewRect = new Rect();
  
  private static final float CLOSED_WIDTH_IN_DP = 3.0F;
  
  private static final float DIFF_DP_VALUE = 0.5F;
  
  private static final float HANDLE_WIDTH_IN_DP = 15.0F;
  
  private static final float HAS_FOCUS_ALPHA = 0.45F;
  
  private static final float NO_FOCUS_ALPHA = 0.12F;
  
  private static final String TAG = "ZoomWindowDecorView";
  
  private boolean mCheckForDragging;
  
  private View mClickTarget;
  
  private int mClosedRectDt;
  
  private View mClosedView;
  
  private int mDragSlop;
  
  private OplusZoomInputEventInfo mEventInfo;
  
  private GestureDetector mGestureDetector;
  
  private View mHandleView;
  
  private boolean mHasFocus;
  
  private OplusZoomWindowDecorViewHelper mHelper;
  
  private int mRectDt;
  
  private int mTouchDownX;
  
  private int mTouchDownY;
  
  private IWindow mWho;
  
  private View mZoomDecor;
  
  public ZoomWindowDecorView(Context paramContext) {
    super(paramContext);
    init(paramContext);
  }
  
  public ZoomWindowDecorView(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    init(paramContext);
  }
  
  public ZoomWindowDecorView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramContext);
  }
  
  private void init(Context paramContext) {
    this.mDragSlop = ViewConfiguration.get(paramContext).getScaledTouchSlop();
    this.mGestureDetector = new GestureDetector(paramContext, this);
    this.mRectDt = dip2px(paramContext, 15.0F);
    this.mClosedRectDt = dip2px(paramContext, 3.0F);
  }
  
  protected void onFinishInflate() {
    super.onFinishInflate();
    this.mZoomDecor = getChildAt(0);
  }
  
  public void setPhoneWindow(PhoneWindow paramPhoneWindow, boolean paramBoolean, OplusZoomWindowDecorViewHelper paramOplusZoomWindowDecorViewHelper) {
    this.mOwner = paramPhoneWindow;
    this.mShow = paramBoolean;
    this.mHandleView = findViewById(201457711);
    this.mClosedView = findViewById(201457688);
    this.mHelper = paramOplusZoomWindowDecorViewHelper;
  }
  
  private static int dip2px(Context paramContext, float paramFloat) {
    float f = (paramContext.getResources().getDisplayMetrics()).density;
    return (int)(paramFloat * f + 0.5F);
  }
  
  public boolean isZoomWindowShowing() {
    return this.mShow;
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent) {
    return true;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    return false;
  }
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.getActionMasked();
    int j = (int)paramMotionEvent.getX();
    int k = (int)paramMotionEvent.getY();
    boolean bool = false;
    if (i != 0) {
      if (i != 1)
        if (i != 2) {
          if (i != 3)
            if (this.mClickTarget != null)
              this.mGestureDetector.onTouchEvent(paramMotionEvent);  
        } else {
          if (this.mClickTarget != null && !this.mDragging && passedSlop(j, k) && !this.mLongPress) {
            this.mDragging = true;
            this.mEventInfo.evActionFlag = 2;
            this.mEventInfo.point.set((int)paramMotionEvent.getRawX(), (int)paramMotionEvent.getRawY());
            OplusZoomWindowManager.getInstance().onInputEvent(this.mEventInfo);
          } 
          if (this.mClickTarget != null)
            this.mGestureDetector.onTouchEvent(paramMotionEvent); 
        }  
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("actionMasked: ");
      stringBuilder.append(i);
      stringBuilder.append(" ,mDragging: ");
      stringBuilder.append(this.mDragging);
      stringBuilder.append(", mLongPress = ");
      stringBuilder.append(this.mLongPress);
      Log.v("ZoomWindowDecorView", stringBuilder.toString());
      if (i == 1 && (this.mDragging || this.mLongPress)) {
        this.mEventInfo.evActionFlag = 4;
        OplusZoomWindowManager.getInstance().onInputEvent(this.mEventInfo);
      } 
      this.mDragging = false;
      this.mLongPress = false;
      if (this.mClickTarget != null) {
        this.mClickTarget = null;
        this.mGestureDetector.onTouchEvent(paramMotionEvent);
        return true;
      } 
    } else {
      this.mLongPress = false;
      this.mDragging = false;
      this.mEventInfo = null;
      if (this.mHandleViewRect.contains(j, k)) {
        this.mClickTarget = this.mHandleView;
      } else if (this.mClosedViewRect.contains(j, k)) {
        this.mClickTarget = this.mClosedView;
      } 
      if (this.mClickTarget != null) {
        this.mTouchDownX = j;
        this.mTouchDownY = k;
        this.mEventInfo = new OplusZoomInputEventInfo();
        if (this.mHelper.mWho != null)
          this.mEventInfo.who = this.mHelper.mWho.asBinder(); 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("event down x: ");
        stringBuilder.append(j);
        stringBuilder.append(", y: ");
        stringBuilder.append(k);
        Log.v("ZoomWindowDecorView", stringBuilder.toString());
      } 
    } 
    if (this.mClickTarget != null)
      this.mGestureDetector.onTouchEvent(paramMotionEvent); 
  }
  
  private boolean passedSlop(int paramInt1, int paramInt2) {
    return (Math.abs(paramInt1 - this.mTouchDownX) > this.mDragSlop || Math.abs(paramInt2 - this.mTouchDownY) > this.mDragSlop);
  }
  
  public void onConfigurationChanged(boolean paramBoolean) {
    this.mShow = paramBoolean;
    updateZoomDecorVisibility();
  }
  
  public void onWindowFocusChangedByRoot(boolean paramBoolean) {
    boolean bool = false;
    if (this.mHasFocus != paramBoolean) {
      bool = true;
      this.mHasFocus = paramBoolean;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("onWindowFocusChangedByRoot hasWindowFocus = ");
    stringBuilder.append(paramBoolean);
    stringBuilder.append("    focusChange = ");
    stringBuilder.append(bool);
    stringBuilder.append("    this = ");
    stringBuilder.append(this);
    Slog.i("ZoomWindowDecorView", stringBuilder.toString());
    if (bool) {
      if (!paramBoolean) {
        this.mHandleView.setAlpha(0.12F);
        this.mClosedView.setAlpha(0.12F);
      } else {
        this.mHandleView.setAlpha(0.45F);
        this.mClosedView.setAlpha(0.45F);
      } 
      invalidateView();
    } 
  }
  
  private void invalidateView() {
    PhoneWindow phoneWindow = this.mOwner;
    if (phoneWindow != null && phoneWindow.getDecorView() != null)
      this.mOwner.getDecorView().invalidate(); 
  }
  
  private void updateZoomDecorVisibility() {
    byte b;
    View view = this.mZoomDecor;
    if (this.mShow) {
      b = 0;
    } else {
      b = 8;
    } 
    view.setVisibility(b);
    this.mZoomDecor.setOnTouchListener(this);
    invalidateView();
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    if (this.mZoomDecor.getVisibility() != 8)
      measureChildWithMargins(this.mZoomDecor, paramInt1, 0, paramInt2, 0); 
    paramInt1 = View.MeasureSpec.getSize(paramInt1);
    paramInt2 = View.MeasureSpec.getSize(paramInt2);
    setMeasuredDimension(paramInt1, paramInt2);
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (this.mZoomDecor.getVisibility() != 8) {
      View view = this.mZoomDecor;
      view.layout(0, 0, view.getMeasuredWidth(), this.mZoomDecor.getMeasuredHeight());
      this.mHandleView.getHitRect(this.mHandleViewRect);
      Rect rect = this.mHandleViewRect;
      paramInt1 = this.mRectDt;
      rect.inset(paramInt1, 0, paramInt1, 0);
      this.mHandleViewShowTipsRect.set(this.mHandleViewRect);
      this.mClosedView.getHitRect(this.mClosedViewRect);
      this.mClosedViewRect.inset(-this.mClosedRectDt, 0, 0, 0);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onLayout: mHandleViewRect = ");
      stringBuilder.append(this.mHandleViewRect);
      stringBuilder.append(", mHandleViewShowTipsRect = ");
      stringBuilder.append(this.mHandleViewShowTipsRect);
      stringBuilder.append(", mClosedViewRect = ");
      stringBuilder.append(this.mClosedViewRect);
      Slog.i("ZoomWindowDecorView", stringBuilder.toString());
    } else {
      this.mHandleViewRect.setEmpty();
      this.mHandleViewShowTipsRect.setEmpty();
      this.mClosedViewRect.setEmpty();
    } 
  }
  
  public ViewGroup.LayoutParams generateLayoutParams(AttributeSet paramAttributeSet) {
    return (ViewGroup.LayoutParams)new ViewGroup.MarginLayoutParams(getContext(), paramAttributeSet);
  }
  
  protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
    return (ViewGroup.LayoutParams)new ViewGroup.MarginLayoutParams(-1, -1);
  }
  
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    return (ViewGroup.LayoutParams)new ViewGroup.MarginLayoutParams(paramLayoutParams);
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    return paramLayoutParams instanceof ViewGroup.MarginLayoutParams;
  }
  
  public boolean onSingleTapUp(MotionEvent paramMotionEvent) {
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("onSingleTapUp: raw (");
    stringBuilder2.append((int)paramMotionEvent.getRawX());
    stringBuilder2.append(", ");
    stringBuilder2.append((int)paramMotionEvent.getRawY());
    stringBuilder2.append(")");
    Log.i("ZoomWindowDecorView", stringBuilder2.toString());
    int i = (int)paramMotionEvent.getX();
    int j = (int)paramMotionEvent.getY();
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("onSingleTapUp: relatively (");
    stringBuilder1.append(i);
    stringBuilder1.append(", ");
    stringBuilder1.append(j);
    stringBuilder1.append(")");
    Log.i("ZoomWindowDecorView", stringBuilder1.toString());
    if (this.mEventInfo != null) {
      Rect rect = this.mClosedViewRect;
      if (rect != null && rect.contains(i, j)) {
        Log.v("ZoomWindowDecorView", "tap in mClosedViewRect");
        this.mEventInfo.evActionFlag = 16;
        OplusZoomWindowManager.getInstance().onInputEvent(this.mEventInfo);
      } else {
        rect = this.mHandleViewShowTipsRect;
        if (rect != null && rect.contains(i, j)) {
          Log.v("ZoomWindowDecorView", "tap in mHandleViewShowTipsRect");
          this.mEventInfo.evActionFlag = 8;
          OplusZoomWindowManager.getInstance().onInputEvent(this.mEventInfo);
        } 
      } 
    } 
    return true;
  }
  
  public boolean onScroll(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2) {
    return false;
  }
  
  public boolean onTouch(View paramView, MotionEvent paramMotionEvent) {
    return true;
  }
  
  public void onLongPress(MotionEvent paramMotionEvent) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("onLongPress: ");
    stringBuilder.append(this.mDragging);
    stringBuilder.append(", (");
    stringBuilder.append((int)paramMotionEvent.getRawX());
    stringBuilder.append(", ");
    stringBuilder.append((int)paramMotionEvent.getRawY());
    stringBuilder.append(")");
    String str = stringBuilder.toString();
    Log.v("ZoomWindowDecorView", str);
    if (!this.mDragging && !this.mLongPress) {
      this.mLongPress = true;
      this.mEventInfo.point.set((int)paramMotionEvent.getRawX(), (int)paramMotionEvent.getRawY());
      this.mOwner.getDecorView().performHapticFeedback(0, 2);
      this.mEventInfo.evActionFlag = 1;
      OplusZoomWindowManager.getInstance().onInputEvent(this.mEventInfo);
    } 
  }
  
  public boolean onFling(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2) {
    return false;
  }
  
  public boolean onDown(MotionEvent paramMotionEvent) {
    return false;
  }
  
  public void onShowPress(MotionEvent paramMotionEvent) {}
  
  public View getZoomDecor() {
    return this.mZoomDecor;
  }
}
