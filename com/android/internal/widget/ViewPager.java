package com.android.internal.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.MathUtils;
import android.view.AbsSavedState;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.Interpolator;
import android.widget.EdgeEffect;
import android.widget.Scroller;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ViewPager extends ViewGroup {
  private static final int[] LAYOUT_ATTRS = new int[] { 16842931 };
  
  class ItemInfo {
    Object object;
    
    float offset;
    
    int position;
    
    boolean scrolling;
    
    float widthFactor;
  }
  
  static {
    COMPARATOR = (Comparator<ItemInfo>)new Object();
    sInterpolator = (Interpolator)new Object();
    sPositionComparator = new ViewPositionComparator();
  }
  
  private final ArrayList<ItemInfo> mItems = new ArrayList<>();
  
  private final ItemInfo mTempItem = new ItemInfo();
  
  private final Rect mTempRect = new Rect();
  
  private int mRestoredCurItem = -1;
  
  private Parcelable mRestoredAdapterState = null;
  
  private ClassLoader mRestoredClassLoader = null;
  
  private int mLeftIncr = -1;
  
  private float mFirstOffset = -3.4028235E38F;
  
  private float mLastOffset = Float.MAX_VALUE;
  
  private int mOffscreenPageLimit = 1;
  
  private int mActivePointerId = -1;
  
  private boolean mFirstLayout = true;
  
  private final Runnable mEndScrollRunnable = (Runnable)new Object(this);
  
  private int mScrollState = 0;
  
  private static final int CLOSE_ENOUGH = 2;
  
  private static final Comparator<ItemInfo> COMPARATOR;
  
  private static final boolean DEBUG = false;
  
  private static final int DEFAULT_GUTTER_SIZE = 16;
  
  private static final int DEFAULT_OFFSCREEN_PAGES = 1;
  
  private static final int DRAW_ORDER_DEFAULT = 0;
  
  private static final int DRAW_ORDER_FORWARD = 1;
  
  private static final int DRAW_ORDER_REVERSE = 2;
  
  private static final int INVALID_POINTER = -1;
  
  private static final int MAX_SCROLL_X = 16777216;
  
  private static final int MAX_SETTLE_DURATION = 600;
  
  private static final int MIN_DISTANCE_FOR_FLING = 25;
  
  private static final int MIN_FLING_VELOCITY = 400;
  
  public static final int SCROLL_STATE_DRAGGING = 1;
  
  public static final int SCROLL_STATE_IDLE = 0;
  
  public static final int SCROLL_STATE_SETTLING = 2;
  
  private static final String TAG = "ViewPager";
  
  private static final boolean USE_CACHE = false;
  
  private static final Interpolator sInterpolator;
  
  private static final ViewPositionComparator sPositionComparator;
  
  private PagerAdapter mAdapter;
  
  private OnAdapterChangeListener mAdapterChangeListener;
  
  private int mBottomPageBounds;
  
  private boolean mCalledSuper;
  
  private int mChildHeightMeasureSpec;
  
  private int mChildWidthMeasureSpec;
  
  private final int mCloseEnough;
  
  private int mCurItem;
  
  private int mDecorChildCount;
  
  private final int mDefaultGutterSize;
  
  private int mDrawingOrder;
  
  private ArrayList<View> mDrawingOrderedChildren;
  
  private int mExpectedAdapterCount;
  
  private final int mFlingDistance;
  
  private int mGutterSize;
  
  private boolean mInLayout;
  
  private float mInitialMotionX;
  
  private float mInitialMotionY;
  
  private OnPageChangeListener mInternalPageChangeListener;
  
  private boolean mIsBeingDragged;
  
  private boolean mIsUnableToDrag;
  
  private float mLastMotionX;
  
  private float mLastMotionY;
  
  private final EdgeEffect mLeftEdge;
  
  private Drawable mMarginDrawable;
  
  private final int mMaximumVelocity;
  
  private final int mMinimumVelocity;
  
  private PagerObserver mObserver;
  
  private OnPageChangeListener mOnPageChangeListener;
  
  private int mPageMargin;
  
  private PageTransformer mPageTransformer;
  
  private boolean mPopulatePending;
  
  private final EdgeEffect mRightEdge;
  
  private final Scroller mScroller;
  
  private boolean mScrollingCacheEnabled;
  
  private int mTopPageBounds;
  
  private final int mTouchSlop;
  
  private VelocityTracker mVelocityTracker;
  
  class SimpleOnPageChangeListener implements OnPageChangeListener {
    public void onPageScrolled(int param1Int1, float param1Float, int param1Int2) {}
    
    public void onPageSelected(int param1Int) {}
    
    public void onPageScrollStateChanged(int param1Int) {}
  }
  
  public ViewPager(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public ViewPager(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public ViewPager(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public ViewPager(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    setWillNotDraw(false);
    setDescendantFocusability(262144);
    setFocusable(true);
    this.mScroller = new Scroller(paramContext, sInterpolator);
    ViewConfiguration viewConfiguration = ViewConfiguration.get(paramContext);
    float f = (paramContext.getResources().getDisplayMetrics()).density;
    this.mTouchSlop = viewConfiguration.getScaledPagingTouchSlop();
    this.mMinimumVelocity = (int)(400.0F * f);
    this.mMaximumVelocity = viewConfiguration.getScaledMaximumFlingVelocity();
    this.mLeftEdge = new EdgeEffect(paramContext);
    this.mRightEdge = new EdgeEffect(paramContext);
    this.mFlingDistance = (int)(25.0F * f);
    this.mCloseEnough = (int)(2.0F * f);
    this.mDefaultGutterSize = (int)(16.0F * f);
    if (getImportantForAccessibility() == 0)
      setImportantForAccessibility(1); 
  }
  
  protected void onDetachedFromWindow() {
    removeCallbacks(this.mEndScrollRunnable);
    super.onDetachedFromWindow();
  }
  
  private void setScrollState(int paramInt) {
    if (this.mScrollState == paramInt)
      return; 
    this.mScrollState = paramInt;
    if (this.mPageTransformer != null) {
      boolean bool;
      if (paramInt != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      enableLayers(bool);
    } 
    OnPageChangeListener onPageChangeListener = this.mOnPageChangeListener;
    if (onPageChangeListener != null)
      onPageChangeListener.onPageScrollStateChanged(paramInt); 
  }
  
  public void setAdapter(PagerAdapter paramPagerAdapter) {
    PagerAdapter pagerAdapter = this.mAdapter;
    if (pagerAdapter != null) {
      pagerAdapter.unregisterDataSetObserver(this.mObserver);
      this.mAdapter.startUpdate(this);
      for (byte b = 0; b < this.mItems.size(); b++) {
        ItemInfo itemInfo = this.mItems.get(b);
        this.mAdapter.destroyItem(this, itemInfo.position, itemInfo.object);
      } 
      this.mAdapter.finishUpdate(this);
      this.mItems.clear();
      removeNonDecorViews();
      this.mCurItem = 0;
      scrollTo(0, 0);
    } 
    pagerAdapter = this.mAdapter;
    this.mAdapter = paramPagerAdapter;
    this.mExpectedAdapterCount = 0;
    if (paramPagerAdapter != null) {
      if (this.mObserver == null)
        this.mObserver = new PagerObserver(); 
      this.mAdapter.registerDataSetObserver(this.mObserver);
      this.mPopulatePending = false;
      boolean bool = this.mFirstLayout;
      this.mFirstLayout = true;
      this.mExpectedAdapterCount = this.mAdapter.getCount();
      if (this.mRestoredCurItem >= 0) {
        this.mAdapter.restoreState(this.mRestoredAdapterState, this.mRestoredClassLoader);
        setCurrentItemInternal(this.mRestoredCurItem, false, true);
        this.mRestoredCurItem = -1;
        this.mRestoredAdapterState = null;
        this.mRestoredClassLoader = null;
      } else if (!bool) {
        populate();
      } else {
        requestLayout();
      } 
    } 
    OnAdapterChangeListener onAdapterChangeListener = this.mAdapterChangeListener;
    if (onAdapterChangeListener != null && pagerAdapter != paramPagerAdapter)
      onAdapterChangeListener.onAdapterChanged(pagerAdapter, paramPagerAdapter); 
  }
  
  private void removeNonDecorViews() {
    for (int i = 0; i < getChildCount(); i = j + 1) {
      View view = getChildAt(i);
      LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
      int j = i;
      if (!layoutParams.isDecor) {
        removeViewAt(i);
        j = i - 1;
      } 
    } 
  }
  
  public PagerAdapter getAdapter() {
    return this.mAdapter;
  }
  
  void setOnAdapterChangeListener(OnAdapterChangeListener paramOnAdapterChangeListener) {
    this.mAdapterChangeListener = paramOnAdapterChangeListener;
  }
  
  private int getPaddedWidth() {
    return getMeasuredWidth() - getPaddingLeft() - getPaddingRight();
  }
  
  public void setCurrentItem(int paramInt) {
    this.mPopulatePending = false;
    setCurrentItemInternal(paramInt, this.mFirstLayout ^ true, false);
  }
  
  public void setCurrentItem(int paramInt, boolean paramBoolean) {
    this.mPopulatePending = false;
    setCurrentItemInternal(paramInt, paramBoolean, false);
  }
  
  public int getCurrentItem() {
    return this.mCurItem;
  }
  
  boolean setCurrentItemInternal(int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    return setCurrentItemInternal(paramInt, paramBoolean1, paramBoolean2, 0);
  }
  
  boolean setCurrentItemInternal(int paramInt1, boolean paramBoolean1, boolean paramBoolean2, int paramInt2) {
    PagerAdapter pagerAdapter = this.mAdapter;
    boolean bool = false;
    if (pagerAdapter == null || pagerAdapter.getCount() <= 0) {
      setScrollingCacheEnabled(false);
      return false;
    } 
    int i = MathUtils.constrain(paramInt1, 0, this.mAdapter.getCount() - 1);
    if (!paramBoolean2 && this.mCurItem == i && this.mItems.size() != 0) {
      setScrollingCacheEnabled(false);
      return false;
    } 
    int j = this.mOffscreenPageLimit;
    paramInt1 = this.mCurItem;
    if (i > paramInt1 + j || i < paramInt1 - j)
      for (paramInt1 = 0; paramInt1 < this.mItems.size(); paramInt1++)
        ((ItemInfo)this.mItems.get(paramInt1)).scrolling = true;  
    paramBoolean2 = bool;
    if (this.mCurItem != i)
      paramBoolean2 = true; 
    if (this.mFirstLayout) {
      this.mCurItem = i;
      if (paramBoolean2) {
        OnPageChangeListener onPageChangeListener = this.mOnPageChangeListener;
        if (onPageChangeListener != null)
          onPageChangeListener.onPageSelected(i); 
      } 
      if (paramBoolean2) {
        OnPageChangeListener onPageChangeListener = this.mInternalPageChangeListener;
        if (onPageChangeListener != null)
          onPageChangeListener.onPageSelected(i); 
      } 
      requestLayout();
    } else {
      populate(i);
      scrollToItem(i, paramBoolean1, paramInt2, paramBoolean2);
    } 
    return true;
  }
  
  private void scrollToItem(int paramInt1, boolean paramBoolean1, int paramInt2, boolean paramBoolean2) {
    int i = getLeftEdgeForItem(paramInt1);
    if (paramBoolean1) {
      smoothScrollTo(i, 0, paramInt2);
      if (paramBoolean2) {
        OnPageChangeListener onPageChangeListener = this.mOnPageChangeListener;
        if (onPageChangeListener != null)
          onPageChangeListener.onPageSelected(paramInt1); 
      } 
      if (paramBoolean2) {
        OnPageChangeListener onPageChangeListener = this.mInternalPageChangeListener;
        if (onPageChangeListener != null)
          onPageChangeListener.onPageSelected(paramInt1); 
      } 
    } else {
      if (paramBoolean2) {
        OnPageChangeListener onPageChangeListener = this.mOnPageChangeListener;
        if (onPageChangeListener != null)
          onPageChangeListener.onPageSelected(paramInt1); 
      } 
      if (paramBoolean2) {
        OnPageChangeListener onPageChangeListener = this.mInternalPageChangeListener;
        if (onPageChangeListener != null)
          onPageChangeListener.onPageSelected(paramInt1); 
      } 
      completeScroll(false);
      scrollTo(i, 0);
      pageScrolled(i);
    } 
  }
  
  private int getLeftEdgeForItem(int paramInt) {
    ItemInfo itemInfo = infoForPosition(paramInt);
    if (itemInfo == null)
      return 0; 
    int i = getPaddedWidth();
    paramInt = (int)(i * MathUtils.constrain(itemInfo.offset, this.mFirstOffset, this.mLastOffset));
    if (isLayoutRtl()) {
      i = (int)(i * itemInfo.widthFactor + 0.5F);
      return 16777216 - i - paramInt;
    } 
    return paramInt;
  }
  
  public void setOnPageChangeListener(OnPageChangeListener paramOnPageChangeListener) {
    this.mOnPageChangeListener = paramOnPageChangeListener;
  }
  
  public void setPageTransformer(boolean paramBoolean, PageTransformer paramPageTransformer) {
    boolean bool1, bool2, bool3;
    byte b = 1;
    if (paramPageTransformer != null) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    if (this.mPageTransformer != null) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if (bool1 != bool2) {
      bool3 = true;
    } else {
      bool3 = false;
    } 
    this.mPageTransformer = paramPageTransformer;
    setChildrenDrawingOrderEnabled(bool1);
    if (bool1) {
      if (paramBoolean)
        b = 2; 
      this.mDrawingOrder = b;
    } else {
      this.mDrawingOrder = 0;
    } 
    if (bool3)
      populate(); 
  }
  
  protected int getChildDrawingOrder(int paramInt1, int paramInt2) {
    if (this.mDrawingOrder == 2) {
      paramInt1 = paramInt1 - 1 - paramInt2;
    } else {
      paramInt1 = paramInt2;
    } 
    paramInt1 = ((LayoutParams)((View)this.mDrawingOrderedChildren.get(paramInt1)).getLayoutParams()).childIndex;
    return paramInt1;
  }
  
  OnPageChangeListener setInternalPageChangeListener(OnPageChangeListener paramOnPageChangeListener) {
    OnPageChangeListener onPageChangeListener = this.mInternalPageChangeListener;
    this.mInternalPageChangeListener = paramOnPageChangeListener;
    return onPageChangeListener;
  }
  
  public int getOffscreenPageLimit() {
    return this.mOffscreenPageLimit;
  }
  
  public void setOffscreenPageLimit(int paramInt) {
    int i = paramInt;
    if (paramInt < 1) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Requested offscreen page limit ");
      stringBuilder.append(paramInt);
      stringBuilder.append(" too small; defaulting to ");
      stringBuilder.append(1);
      Log.w("ViewPager", stringBuilder.toString());
      i = 1;
    } 
    if (i != this.mOffscreenPageLimit) {
      this.mOffscreenPageLimit = i;
      populate();
    } 
  }
  
  public void setPageMargin(int paramInt) {
    int i = this.mPageMargin;
    this.mPageMargin = paramInt;
    int j = getWidth();
    recomputeScrollPosition(j, j, paramInt, i);
    requestLayout();
  }
  
  public int getPageMargin() {
    return this.mPageMargin;
  }
  
  public void setPageMarginDrawable(Drawable paramDrawable) {
    boolean bool;
    this.mMarginDrawable = paramDrawable;
    if (paramDrawable != null)
      refreshDrawableState(); 
    if (paramDrawable == null) {
      bool = true;
    } else {
      bool = false;
    } 
    setWillNotDraw(bool);
    invalidate();
  }
  
  public void setPageMarginDrawable(int paramInt) {
    setPageMarginDrawable(getContext().getDrawable(paramInt));
  }
  
  protected boolean verifyDrawable(Drawable paramDrawable) {
    return (super.verifyDrawable(paramDrawable) || paramDrawable == this.mMarginDrawable);
  }
  
  protected void drawableStateChanged() {
    super.drawableStateChanged();
    Drawable drawable = this.mMarginDrawable;
    if (drawable != null && drawable.isStateful() && 
      drawable.setState(getDrawableState()))
      invalidateDrawable(drawable); 
  }
  
  float distanceInfluenceForSnapDuration(float paramFloat) {
    paramFloat = (float)((paramFloat - 0.5F) * 0.4712389167638204D);
    return (float)Math.sin(paramFloat);
  }
  
  void smoothScrollTo(int paramInt1, int paramInt2) {
    smoothScrollTo(paramInt1, paramInt2, 0);
  }
  
  void smoothScrollTo(int paramInt1, int paramInt2, int paramInt3) {
    if (getChildCount() == 0) {
      setScrollingCacheEnabled(false);
      return;
    } 
    int i = getScrollX();
    int j = getScrollY();
    int k = paramInt1 - i;
    paramInt2 -= j;
    if (k == 0 && paramInt2 == 0) {
      completeScroll(false);
      populate();
      setScrollState(0);
      return;
    } 
    setScrollingCacheEnabled(true);
    setScrollState(2);
    paramInt1 = getPaddedWidth();
    int m = paramInt1 / 2;
    float f1 = Math.min(1.0F, Math.abs(k) * 1.0F / paramInt1);
    float f2 = m, f3 = m;
    f1 = distanceInfluenceForSnapDuration(f1);
    paramInt3 = Math.abs(paramInt3);
    if (paramInt3 > 0) {
      paramInt1 = Math.round(Math.abs((f2 + f3 * f1) / paramInt3) * 1000.0F) * 4;
    } else {
      f3 = paramInt1;
      f2 = this.mAdapter.getPageWidth(this.mCurItem);
      f3 = Math.abs(k) / (this.mPageMargin + f3 * f2);
      paramInt1 = (int)((1.0F + f3) * 100.0F);
    } 
    paramInt1 = Math.min(paramInt1, 600);
    this.mScroller.startScroll(i, j, k, paramInt2, paramInt1);
    postInvalidateOnAnimation();
  }
  
  ItemInfo addNewItem(int paramInt1, int paramInt2) {
    ItemInfo itemInfo = new ItemInfo();
    itemInfo.position = paramInt1;
    itemInfo.object = this.mAdapter.instantiateItem(this, paramInt1);
    itemInfo.widthFactor = this.mAdapter.getPageWidth(paramInt1);
    if (paramInt2 < 0 || paramInt2 >= this.mItems.size()) {
      this.mItems.add(itemInfo);
      return itemInfo;
    } 
    this.mItems.add(paramInt2, itemInfo);
    return itemInfo;
  }
  
  void dataSetChanged() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mAdapter : Lcom/android/internal/widget/PagerAdapter;
    //   4: invokevirtual getCount : ()I
    //   7: istore_1
    //   8: aload_0
    //   9: iload_1
    //   10: putfield mExpectedAdapterCount : I
    //   13: aload_0
    //   14: getfield mItems : Ljava/util/ArrayList;
    //   17: invokevirtual size : ()I
    //   20: aload_0
    //   21: getfield mOffscreenPageLimit : I
    //   24: iconst_2
    //   25: imul
    //   26: iconst_1
    //   27: iadd
    //   28: if_icmpge -> 49
    //   31: aload_0
    //   32: getfield mItems : Ljava/util/ArrayList;
    //   35: astore_2
    //   36: aload_2
    //   37: invokevirtual size : ()I
    //   40: iload_1
    //   41: if_icmpge -> 49
    //   44: iconst_1
    //   45: istore_3
    //   46: goto -> 51
    //   49: iconst_0
    //   50: istore_3
    //   51: aload_0
    //   52: getfield mCurItem : I
    //   55: istore #4
    //   57: iconst_0
    //   58: istore #5
    //   60: iconst_0
    //   61: istore #6
    //   63: iload #6
    //   65: aload_0
    //   66: getfield mItems : Ljava/util/ArrayList;
    //   69: invokevirtual size : ()I
    //   72: if_icmpge -> 308
    //   75: aload_0
    //   76: getfield mItems : Ljava/util/ArrayList;
    //   79: iload #6
    //   81: invokevirtual get : (I)Ljava/lang/Object;
    //   84: checkcast com/android/internal/widget/ViewPager$ItemInfo
    //   87: astore_2
    //   88: aload_0
    //   89: getfield mAdapter : Lcom/android/internal/widget/PagerAdapter;
    //   92: aload_2
    //   93: getfield object : Ljava/lang/Object;
    //   96: invokevirtual getItemPosition : (Ljava/lang/Object;)I
    //   99: istore #7
    //   101: iload #7
    //   103: iconst_m1
    //   104: if_icmpne -> 122
    //   107: iload #4
    //   109: istore #8
    //   111: iload #5
    //   113: istore #9
    //   115: iload #6
    //   117: istore #10
    //   119: goto -> 291
    //   122: iload #7
    //   124: bipush #-2
    //   126: if_icmpne -> 235
    //   129: aload_0
    //   130: getfield mItems : Ljava/util/ArrayList;
    //   133: iload #6
    //   135: invokevirtual remove : (I)Ljava/lang/Object;
    //   138: pop
    //   139: iload #6
    //   141: iconst_1
    //   142: isub
    //   143: istore #7
    //   145: iload #5
    //   147: istore #6
    //   149: iload #5
    //   151: ifne -> 165
    //   154: aload_0
    //   155: getfield mAdapter : Lcom/android/internal/widget/PagerAdapter;
    //   158: aload_0
    //   159: invokevirtual startUpdate : (Landroid/view/ViewGroup;)V
    //   162: iconst_1
    //   163: istore #6
    //   165: aload_0
    //   166: getfield mAdapter : Lcom/android/internal/widget/PagerAdapter;
    //   169: aload_0
    //   170: aload_2
    //   171: getfield position : I
    //   174: aload_2
    //   175: getfield object : Ljava/lang/Object;
    //   178: invokevirtual destroyItem : (Landroid/view/ViewGroup;ILjava/lang/Object;)V
    //   181: iconst_1
    //   182: istore_3
    //   183: iload #4
    //   185: istore #8
    //   187: iload #6
    //   189: istore #9
    //   191: iload #7
    //   193: istore #10
    //   195: aload_0
    //   196: getfield mCurItem : I
    //   199: aload_2
    //   200: getfield position : I
    //   203: if_icmpne -> 291
    //   206: iconst_0
    //   207: aload_0
    //   208: getfield mCurItem : I
    //   211: iload_1
    //   212: iconst_1
    //   213: isub
    //   214: invokestatic min : (II)I
    //   217: invokestatic max : (II)I
    //   220: istore #8
    //   222: iconst_1
    //   223: istore_3
    //   224: iload #6
    //   226: istore #9
    //   228: iload #7
    //   230: istore #10
    //   232: goto -> 291
    //   235: iload #4
    //   237: istore #8
    //   239: iload #5
    //   241: istore #9
    //   243: iload #6
    //   245: istore #10
    //   247: aload_2
    //   248: getfield position : I
    //   251: iload #7
    //   253: if_icmpeq -> 291
    //   256: aload_2
    //   257: getfield position : I
    //   260: aload_0
    //   261: getfield mCurItem : I
    //   264: if_icmpne -> 271
    //   267: iload #7
    //   269: istore #4
    //   271: aload_2
    //   272: iload #7
    //   274: putfield position : I
    //   277: iconst_1
    //   278: istore_3
    //   279: iload #6
    //   281: istore #10
    //   283: iload #5
    //   285: istore #9
    //   287: iload #4
    //   289: istore #8
    //   291: iload #10
    //   293: iconst_1
    //   294: iadd
    //   295: istore #6
    //   297: iload #8
    //   299: istore #4
    //   301: iload #9
    //   303: istore #5
    //   305: goto -> 63
    //   308: iload #5
    //   310: ifeq -> 321
    //   313: aload_0
    //   314: getfield mAdapter : Lcom/android/internal/widget/PagerAdapter;
    //   317: aload_0
    //   318: invokevirtual finishUpdate : (Landroid/view/ViewGroup;)V
    //   321: aload_0
    //   322: getfield mItems : Ljava/util/ArrayList;
    //   325: getstatic com/android/internal/widget/ViewPager.COMPARATOR : Ljava/util/Comparator;
    //   328: invokestatic sort : (Ljava/util/List;Ljava/util/Comparator;)V
    //   331: iload_3
    //   332: ifeq -> 394
    //   335: aload_0
    //   336: invokevirtual getChildCount : ()I
    //   339: istore #5
    //   341: iconst_0
    //   342: istore_3
    //   343: iload_3
    //   344: iload #5
    //   346: if_icmpge -> 381
    //   349: aload_0
    //   350: iload_3
    //   351: invokevirtual getChildAt : (I)Landroid/view/View;
    //   354: astore_2
    //   355: aload_2
    //   356: invokevirtual getLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
    //   359: checkcast com/android/internal/widget/ViewPager$LayoutParams
    //   362: astore_2
    //   363: aload_2
    //   364: getfield isDecor : Z
    //   367: ifne -> 375
    //   370: aload_2
    //   371: fconst_0
    //   372: putfield widthFactor : F
    //   375: iinc #3, 1
    //   378: goto -> 343
    //   381: aload_0
    //   382: iload #4
    //   384: iconst_0
    //   385: iconst_1
    //   386: invokevirtual setCurrentItemInternal : (IZZ)Z
    //   389: pop
    //   390: aload_0
    //   391: invokevirtual requestLayout : ()V
    //   394: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #828	-> 0
    //   #829	-> 8
    //   #830	-> 13
    //   #831	-> 36
    //   #832	-> 51
    //   #834	-> 57
    //   #835	-> 60
    //   #836	-> 75
    //   #837	-> 88
    //   #839	-> 101
    //   #840	-> 107
    //   #843	-> 122
    //   #844	-> 129
    //   #845	-> 139
    //   #847	-> 145
    //   #848	-> 154
    //   #849	-> 162
    //   #852	-> 165
    //   #853	-> 181
    //   #855	-> 183
    //   #857	-> 206
    //   #858	-> 222
    //   #863	-> 235
    //   #864	-> 256
    //   #866	-> 267
    //   #869	-> 271
    //   #870	-> 277
    //   #835	-> 291
    //   #874	-> 308
    //   #875	-> 313
    //   #878	-> 321
    //   #880	-> 331
    //   #882	-> 335
    //   #883	-> 341
    //   #884	-> 349
    //   #885	-> 355
    //   #886	-> 363
    //   #887	-> 370
    //   #883	-> 375
    //   #891	-> 381
    //   #892	-> 390
    //   #894	-> 394
  }
  
  public void populate() {
    populate(this.mCurItem);
  }
  
  void populate(int paramInt) {
    ItemInfo itemInfo;
    byte b;
    int i = this.mCurItem;
    if (i != paramInt) {
      if (i < paramInt) {
        i = 66;
      } else {
        i = 17;
      } 
      itemInfo = infoForPosition(this.mCurItem);
      this.mCurItem = paramInt;
      b = i;
    } else {
      b = 2;
      itemInfo = null;
    } 
    if (this.mAdapter == null) {
      sortChildDrawingOrder();
      return;
    } 
    if (this.mPopulatePending) {
      sortChildDrawingOrder();
      return;
    } 
    if (getWindowToken() == null)
      return; 
    this.mAdapter.startUpdate(this);
    int j = this.mOffscreenPageLimit;
    int k = Math.max(0, this.mCurItem - j);
    int m = this.mAdapter.getCount();
    int n = Math.min(m - 1, this.mCurItem + j);
    if (m == this.mExpectedAdapterCount) {
      ItemInfo itemInfo2, itemInfo1 = null;
      paramInt = 0;
      while (true) {
        itemInfo2 = itemInfo1;
        if (paramInt < this.mItems.size()) {
          ItemInfo itemInfo4 = this.mItems.get(paramInt);
          if (itemInfo4.position >= this.mCurItem) {
            itemInfo2 = itemInfo1;
            if (itemInfo4.position == this.mCurItem)
              itemInfo2 = itemInfo4; 
            break;
          } 
          paramInt++;
          continue;
        } 
        break;
      } 
      ItemInfo itemInfo3 = itemInfo2;
      if (itemInfo2 == null) {
        itemInfo3 = itemInfo2;
        if (m > 0)
          itemInfo3 = addNewItem(this.mCurItem, paramInt); 
      } 
      if (itemInfo3 != null) {
        float f1 = 0.0F;
        int i1 = paramInt - 1;
        if (i1 >= 0) {
          itemInfo2 = this.mItems.get(i1);
        } else {
          itemInfo2 = null;
        } 
        int i2 = getPaddedWidth();
        if (i2 <= 0) {
          f2 = 0.0F;
        } else {
          f2 = 2.0F - itemInfo3.widthFactor + getPaddingLeft() / i2;
        } 
        int i3, i4;
        float f3;
        for (i3 = this.mCurItem - 1, itemInfo1 = itemInfo2, i4 = paramInt, f3 = f2; i3 >= 0; i3--, i4 = paramInt, f1 = f2, i1 = i, itemInfo1 = itemInfo2) {
          if (f1 >= f3 && i3 < k) {
            if (itemInfo1 == null)
              break; 
            paramInt = i4;
            f2 = f1;
            i = i1;
            itemInfo2 = itemInfo1;
            if (i3 == itemInfo1.position) {
              paramInt = i4;
              f2 = f1;
              i = i1;
              itemInfo2 = itemInfo1;
              if (!itemInfo1.scrolling) {
                this.mItems.remove(i1);
                this.mAdapter.destroyItem(this, i3, itemInfo1.object);
                i = i1 - 1;
                paramInt = i4 - 1;
                if (i >= 0) {
                  itemInfo2 = this.mItems.get(i);
                } else {
                  itemInfo2 = null;
                } 
                f2 = f1;
              } 
            } 
          } else if (itemInfo1 != null && i3 == itemInfo1.position) {
            f2 = f1 + itemInfo1.widthFactor;
            i = i1 - 1;
            if (i >= 0) {
              itemInfo2 = this.mItems.get(i);
            } else {
              itemInfo2 = null;
            } 
            paramInt = i4;
          } else {
            itemInfo2 = addNewItem(i3, i1 + 1);
            f2 = f1 + itemInfo2.widthFactor;
            paramInt = i4 + 1;
            if (i1 >= 0) {
              itemInfo2 = this.mItems.get(i1);
            } else {
              itemInfo2 = null;
            } 
            i = i1;
          } 
        } 
        float f2 = itemInfo3.widthFactor;
        paramInt = i4 + 1;
        if (f2 < 2.0F) {
          if (paramInt < this.mItems.size()) {
            itemInfo2 = this.mItems.get(paramInt);
          } else {
            itemInfo2 = null;
          } 
          if (i2 <= 0) {
            f3 = 0.0F;
          } else {
            f3 = getPaddingRight() / i2 + 2.0F;
          } 
          for (i1 = this.mCurItem + 1, i = k, i3 = j; i1 < m; i1++) {
            if (f2 >= f3 && i1 > n) {
              if (itemInfo2 == null)
                break; 
              if (i1 == itemInfo2.position && !itemInfo2.scrolling) {
                this.mItems.remove(paramInt);
                this.mAdapter.destroyItem(this, i1, itemInfo2.object);
                if (paramInt < this.mItems.size()) {
                  itemInfo2 = this.mItems.get(paramInt);
                } else {
                  itemInfo2 = null;
                } 
              } 
            } else if (itemInfo2 != null && i1 == itemInfo2.position) {
              f2 += itemInfo2.widthFactor;
              paramInt++;
              if (paramInt < this.mItems.size()) {
                itemInfo2 = this.mItems.get(paramInt);
              } else {
                itemInfo2 = null;
              } 
            } else {
              itemInfo2 = addNewItem(i1, paramInt);
              paramInt++;
              f2 += itemInfo2.widthFactor;
              if (paramInt < this.mItems.size()) {
                itemInfo2 = this.mItems.get(paramInt);
              } else {
                itemInfo2 = null;
              } 
            } 
          } 
        } 
        calculatePageOffsets(itemInfo3, i4, itemInfo);
      } 
      PagerAdapter pagerAdapter = this.mAdapter;
      paramInt = this.mCurItem;
      if (itemInfo3 != null) {
        object = itemInfo3.object;
      } else {
        itemInfo2 = null;
      } 
      pagerAdapter.setPrimaryItem(this, paramInt, itemInfo2);
      this.mAdapter.finishUpdate(this);
      i = getChildCount();
      for (paramInt = 0; paramInt < i; paramInt++) {
        View view = getChildAt(paramInt);
        object = view.getLayoutParams();
        ((LayoutParams)object).childIndex = paramInt;
        if (!((LayoutParams)object).isDecor && ((LayoutParams)object).widthFactor == 0.0F) {
          ItemInfo itemInfo4 = infoForChild(view);
          if (itemInfo4 != null) {
            ((LayoutParams)object).widthFactor = itemInfo4.widthFactor;
            ((LayoutParams)object).position = itemInfo4.position;
          } 
        } 
      } 
      sortChildDrawingOrder();
      if (hasFocus()) {
        View view = findFocus();
        if (view != null) {
          itemInfo2 = infoForAnyChild(view);
        } else {
          itemInfo2 = null;
        } 
        if (itemInfo2 == null || itemInfo2.position != this.mCurItem)
          for (paramInt = 0; paramInt < getChildCount(); paramInt++) {
            View view1 = getChildAt(paramInt);
            itemInfo2 = infoForChild(view1);
            if (itemInfo2 != null && itemInfo2.position == this.mCurItem) {
              if (view == null) {
                itemInfo2 = null;
              } else {
                object = this.mTempRect;
                view.getFocusedRect(this.mTempRect);
                offsetDescendantRectToMyCoords(view, this.mTempRect);
                offsetRectIntoDescendantCoords(view1, this.mTempRect);
              } 
              if (view1.requestFocus(b, (Rect)object))
                break; 
            } 
          }  
      } 
      return;
    } 
    try {
      object = getResources().getResourceName(getId());
    } catch (android.content.res.Resources.NotFoundException notFoundException) {
      object = Integer.toHexString(getId());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("The application's PagerAdapter changed the adapter's contents without calling PagerAdapter#notifyDataSetChanged! Expected adapter item count: ");
    stringBuilder.append(this.mExpectedAdapterCount);
    stringBuilder.append(", found: ");
    stringBuilder.append(m);
    stringBuilder.append(" Pager id: ");
    stringBuilder.append((String)object);
    stringBuilder.append(" Pager class: ");
    stringBuilder.append(getClass());
    stringBuilder.append(" Problematic adapter: ");
    Object object = this.mAdapter;
    stringBuilder.append(object.getClass());
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  private void sortChildDrawingOrder() {
    if (this.mDrawingOrder != 0) {
      ArrayList<View> arrayList = this.mDrawingOrderedChildren;
      if (arrayList == null) {
        this.mDrawingOrderedChildren = new ArrayList<>();
      } else {
        arrayList.clear();
      } 
      int i = getChildCount();
      for (byte b = 0; b < i; b++) {
        View view = getChildAt(b);
        this.mDrawingOrderedChildren.add(view);
      } 
      Collections.sort(this.mDrawingOrderedChildren, sPositionComparator);
    } 
  }
  
  private void calculatePageOffsets(ItemInfo paramItemInfo1, int paramInt, ItemInfo paramItemInfo2) {
    float f1;
    int i = this.mAdapter.getCount();
    int j = getPaddedWidth();
    if (j > 0) {
      f1 = this.mPageMargin / j;
    } else {
      f1 = 0.0F;
    } 
    if (paramItemInfo2 != null) {
      j = paramItemInfo2.position;
      if (j < paramItemInfo1.position) {
        byte b = 0;
        f2 = paramItemInfo2.offset + paramItemInfo2.widthFactor + f1;
        for (; ++j <= paramItemInfo1.position && b < this.mItems.size(); j = n + 1) {
          float f;
          int n;
          paramItemInfo2 = this.mItems.get(b);
          while (true) {
            f = f2;
            n = j;
            if (j > paramItemInfo2.position) {
              f = f2;
              n = j;
              if (b < this.mItems.size() - 1) {
                b++;
                paramItemInfo2 = this.mItems.get(b);
                continue;
              } 
            } 
            break;
          } 
          while (n < paramItemInfo2.position) {
            f += this.mAdapter.getPageWidth(n) + f1;
            n++;
          } 
          paramItemInfo2.offset = f;
          f2 = f + paramItemInfo2.widthFactor + f1;
        } 
      } else if (j > paramItemInfo1.position) {
        int n = this.mItems.size() - 1;
        f2 = paramItemInfo2.offset;
        for (; --j >= paramItemInfo1.position && n >= 0; j = i1 - 1) {
          float f;
          int i1;
          paramItemInfo2 = this.mItems.get(n);
          while (true) {
            f = f2;
            i1 = j;
            if (j < paramItemInfo2.position) {
              f = f2;
              i1 = j;
              if (n > 0) {
                n--;
                paramItemInfo2 = this.mItems.get(n);
                continue;
              } 
            } 
            break;
          } 
          while (i1 > paramItemInfo2.position) {
            f -= this.mAdapter.getPageWidth(i1) + f1;
            i1--;
          } 
          f2 = f - paramItemInfo2.widthFactor + f1;
          paramItemInfo2.offset = f2;
        } 
      } 
    } 
    int m = this.mItems.size();
    float f3 = paramItemInfo1.offset;
    j = paramItemInfo1.position - 1;
    if (paramItemInfo1.position == 0) {
      f2 = paramItemInfo1.offset;
    } else {
      f2 = -3.4028235E38F;
    } 
    this.mFirstOffset = f2;
    if (paramItemInfo1.position == i - 1) {
      f2 = paramItemInfo1.offset + paramItemInfo1.widthFactor - 1.0F;
    } else {
      f2 = Float.MAX_VALUE;
    } 
    this.mLastOffset = f2;
    int k;
    float f2;
    for (k = paramInt - 1, f2 = f3; k >= 0; k--, j--) {
      paramItemInfo2 = this.mItems.get(k);
      while (j > paramItemInfo2.position) {
        f2 -= this.mAdapter.getPageWidth(j) + f1;
        j--;
      } 
      f2 -= paramItemInfo2.widthFactor + f1;
      paramItemInfo2.offset = f2;
      if (paramItemInfo2.position == 0)
        this.mFirstOffset = f2; 
    } 
    f2 = paramItemInfo1.offset + paramItemInfo1.widthFactor + f1;
    k = paramItemInfo1.position + 1;
    for (j = paramInt + 1, paramInt = k; j < m; j++, paramInt++) {
      paramItemInfo1 = this.mItems.get(j);
      while (paramInt < paramItemInfo1.position) {
        f2 += this.mAdapter.getPageWidth(paramInt) + f1;
        paramInt++;
      } 
      if (paramItemInfo1.position == i - 1)
        this.mLastOffset = paramItemInfo1.widthFactor + f2 - 1.0F; 
      paramItemInfo1.offset = f2;
      f2 += paramItemInfo1.widthFactor + f1;
    } 
  }
  
  class SavedState extends AbsSavedState {
    public SavedState(ViewPager this$0) {
      super((Parcelable)this$0);
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeInt(this.position);
      param1Parcel.writeParcelable(this.adapterState, param1Int);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("FragmentPager.SavedState{");
      stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
      stringBuilder.append(" position=");
      stringBuilder.append(this.position);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = (Parcelable.Creator<SavedState>)new Parcelable.ClassLoaderCreator<SavedState>() {
        public ViewPager.SavedState createFromParcel(Parcel param2Parcel, ClassLoader param2ClassLoader) {
          return new ViewPager.SavedState(param2Parcel, param2ClassLoader);
        }
        
        public ViewPager.SavedState createFromParcel(Parcel param2Parcel) {
          return new ViewPager.SavedState(param2Parcel, null);
        }
        
        public ViewPager.SavedState[] newArray(int param2Int) {
          return new ViewPager.SavedState[param2Int];
        }
      };
    
    Parcelable adapterState;
    
    ClassLoader loader;
    
    int position;
    
    SavedState(ViewPager this$0, ClassLoader param1ClassLoader) {
      super((Parcel)this$0, param1ClassLoader);
      ClassLoader classLoader = param1ClassLoader;
      if (param1ClassLoader == null)
        classLoader = getClass().getClassLoader(); 
      this.position = this$0.readInt();
      this.adapterState = this$0.readParcelable(classLoader);
      this.loader = classLoader;
    }
  }
  
  public Parcelable onSaveInstanceState() {
    Parcelable parcelable = super.onSaveInstanceState();
    SavedState savedState = new SavedState(parcelable);
    savedState.position = this.mCurItem;
    PagerAdapter pagerAdapter = this.mAdapter;
    if (pagerAdapter != null)
      savedState.adapterState = pagerAdapter.saveState(); 
    return (Parcelable)savedState;
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable) {
    if (!(paramParcelable instanceof SavedState)) {
      super.onRestoreInstanceState(paramParcelable);
      return;
    } 
    SavedState savedState = (SavedState)paramParcelable;
    super.onRestoreInstanceState(savedState.getSuperState());
    PagerAdapter pagerAdapter = this.mAdapter;
    if (pagerAdapter != null) {
      pagerAdapter.restoreState(savedState.adapterState, savedState.loader);
      setCurrentItemInternal(savedState.position, false, true);
    } else {
      this.mRestoredCurItem = savedState.position;
      this.mRestoredAdapterState = savedState.adapterState;
      this.mRestoredClassLoader = savedState.loader;
    } 
  }
  
  public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams) {
    ViewGroup.LayoutParams layoutParams = paramLayoutParams;
    if (!checkLayoutParams(paramLayoutParams))
      layoutParams = generateLayoutParams(paramLayoutParams); 
    paramLayoutParams = layoutParams;
    ((LayoutParams)paramLayoutParams).isDecor |= paramView instanceof Decor;
    if (this.mInLayout) {
      if (paramLayoutParams == null || !((LayoutParams)paramLayoutParams).isDecor) {
        ((LayoutParams)paramLayoutParams).needsMeasure = true;
        addViewInLayout(paramView, paramInt, layoutParams);
        return;
      } 
      throw new IllegalStateException("Cannot add pager decor view during layout");
    } 
    super.addView(paramView, paramInt, layoutParams);
  }
  
  public Object getCurrent() {
    Object object = infoForPosition(getCurrentItem());
    if (object == null) {
      object = null;
    } else {
      object = ((ItemInfo)object).object;
    } 
    return object;
  }
  
  public void removeView(View paramView) {
    if (this.mInLayout) {
      removeViewInLayout(paramView);
    } else {
      super.removeView(paramView);
    } 
  }
  
  ItemInfo infoForChild(View paramView) {
    for (byte b = 0; b < this.mItems.size(); b++) {
      ItemInfo itemInfo = this.mItems.get(b);
      if (this.mAdapter.isViewFromObject(paramView, itemInfo.object))
        return itemInfo; 
    } 
    return null;
  }
  
  ItemInfo infoForAnyChild(View paramView) {
    while (true) {
      ViewParent viewParent = paramView.getParent();
      if (viewParent != this) {
        if (viewParent == null || !(viewParent instanceof View))
          return null; 
        paramView = (View)viewParent;
        continue;
      } 
      break;
    } 
    return infoForChild(paramView);
  }
  
  ItemInfo infoForPosition(int paramInt) {
    for (byte b = 0; b < this.mItems.size(); b++) {
      ItemInfo itemInfo = this.mItems.get(b);
      if (itemInfo.position == paramInt)
        return itemInfo; 
    } 
    return null;
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    this.mFirstLayout = true;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    paramInt1 = getDefaultSize(0, paramInt1);
    paramInt2 = getDefaultSize(0, paramInt2);
    setMeasuredDimension(paramInt1, paramInt2);
    int i = getMeasuredWidth();
    int j = i / 10;
    this.mGutterSize = Math.min(j, this.mDefaultGutterSize);
    paramInt1 = i - getPaddingLeft() - getPaddingRight();
    paramInt2 = getMeasuredHeight() - getPaddingTop() - getPaddingBottom();
    int k = getChildCount();
    for (byte b = 0; b < k; b++, paramInt1 = n, paramInt2 = i1) {
      int n, i1;
      View view = getChildAt(b);
      if (view.getVisibility() != 8) {
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        if (layoutParams != null && layoutParams.isDecor) {
          boolean bool;
          int i4;
          n = layoutParams.gravity & 0x7;
          int i2 = layoutParams.gravity & 0x70;
          int i3 = Integer.MIN_VALUE;
          i1 = Integer.MIN_VALUE;
          if (i2 == 48 || i2 == 80) {
            i2 = 1;
          } else {
            i2 = 0;
          } 
          if (n == 3 || n == 5) {
            bool = true;
          } else {
            bool = false;
          } 
          if (i2 != 0) {
            n = 1073741824;
          } else {
            n = i3;
            if (bool) {
              i1 = 1073741824;
              n = i3;
            } 
          } 
          if (layoutParams.width != -2) {
            i4 = 1073741824;
            if (layoutParams.width != -1) {
              n = layoutParams.width;
            } else {
              n = paramInt1;
            } 
          } else {
            i3 = paramInt1;
            i4 = n;
            n = i3;
          } 
          if (layoutParams.height != -2) {
            if (layoutParams.height != -1) {
              i3 = layoutParams.height;
              i1 = 1073741824;
            } else {
              i1 = 1073741824;
              i3 = paramInt2;
            } 
          } else {
            i3 = paramInt2;
          } 
          n = View.MeasureSpec.makeMeasureSpec(n, i4);
          i1 = View.MeasureSpec.makeMeasureSpec(i3, i1);
          view.measure(n, i1);
          if (i2 != 0) {
            i1 = paramInt2 - view.getMeasuredHeight();
            n = paramInt1;
          } else {
            n = paramInt1;
            i1 = paramInt2;
            if (bool) {
              n = paramInt1 - view.getMeasuredWidth();
              i1 = paramInt2;
            } 
          } 
        } else {
          n = paramInt1;
          i1 = paramInt2;
        } 
      } else {
        i1 = paramInt2;
        n = paramInt1;
      } 
    } 
    this.mChildWidthMeasureSpec = View.MeasureSpec.makeMeasureSpec(paramInt1, 1073741824);
    this.mChildHeightMeasureSpec = View.MeasureSpec.makeMeasureSpec(paramInt2, 1073741824);
    this.mInLayout = true;
    populate();
    this.mInLayout = false;
    int m = getChildCount();
    for (paramInt2 = 0; paramInt2 < m; paramInt2++) {
      View view = getChildAt(paramInt2);
      if (view.getVisibility() != 8) {
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        if (layoutParams == null || !layoutParams.isDecor) {
          int n = View.MeasureSpec.makeMeasureSpec((int)(paramInt1 * layoutParams.widthFactor), 1073741824);
          view.measure(n, this.mChildHeightMeasureSpec);
        } 
      } 
    } 
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    if (paramInt1 != paramInt3) {
      paramInt2 = this.mPageMargin;
      recomputeScrollPosition(paramInt1, paramInt3, paramInt2, paramInt2);
    } 
  }
  
  private void recomputeScrollPosition(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (paramInt2 > 0 && !this.mItems.isEmpty()) {
      int i = getPaddingLeft(), j = getPaddingRight();
      int k = getPaddingLeft(), m = getPaddingRight();
      int n = getScrollX();
      float f = n / (paramInt2 - k - m + paramInt4);
      paramInt4 = (int)((paramInt1 - i - j + paramInt3) * f);
      scrollTo(paramInt4, getScrollY());
      if (!this.mScroller.isFinished()) {
        paramInt2 = this.mScroller.getDuration();
        paramInt3 = this.mScroller.timePassed();
        ItemInfo itemInfo = infoForPosition(this.mCurItem);
        this.mScroller.startScroll(paramInt4, 0, (int)(itemInfo.offset * paramInt1), 0, paramInt2 - paramInt3);
      } 
    } else {
      float f;
      ItemInfo itemInfo = infoForPosition(this.mCurItem);
      if (itemInfo != null) {
        f = Math.min(itemInfo.offset, this.mLastOffset);
      } else {
        f = 0.0F;
      } 
      paramInt1 = (int)((paramInt1 - getPaddingLeft() - getPaddingRight()) * f);
      if (paramInt1 != getScrollX()) {
        completeScroll(false);
        scrollTo(paramInt1, getScrollY());
      } 
    } 
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    int i = getChildCount();
    int j = paramInt3 - paramInt1;
    int k = paramInt4 - paramInt2;
    paramInt1 = getPaddingLeft();
    paramInt2 = getPaddingTop();
    int m = getPaddingRight();
    paramInt4 = getPaddingBottom();
    int n = getScrollX();
    int i1 = 0;
    byte b;
    for (b = 0; b < i; b++, paramInt1 = paramInt3, paramInt2 = i4, m = i5, paramInt4 = i6, i1 = i7) {
      View view = getChildAt(b);
      paramInt3 = paramInt1;
      int i4 = paramInt2, i5 = m, i6 = paramInt4, i7 = i1;
      if (view.getVisibility() != 8) {
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        if (layoutParams.isDecor) {
          paramInt3 = layoutParams.gravity & 0x7;
          i5 = layoutParams.gravity & 0x70;
          if (paramInt3 != 1) {
            if (paramInt3 != 3) {
              if (paramInt3 != 5) {
                paramInt3 = paramInt1;
                i4 = paramInt1;
              } else {
                paramInt3 = j - m - view.getMeasuredWidth();
                m += view.getMeasuredWidth();
                i4 = paramInt1;
              } 
            } else {
              paramInt3 = paramInt1;
              i4 = paramInt1 + view.getMeasuredWidth();
            } 
          } else {
            paramInt3 = Math.max((j - view.getMeasuredWidth()) / 2, paramInt1);
            i4 = paramInt1;
          } 
          if (i5 != 16) {
            if (i5 != 48) {
              if (i5 != 80) {
                paramInt1 = paramInt2;
              } else {
                paramInt1 = k - paramInt4 - view.getMeasuredHeight();
                paramInt4 += view.getMeasuredHeight();
              } 
            } else {
              paramInt1 = paramInt2;
              paramInt2 += view.getMeasuredHeight();
            } 
          } else {
            paramInt1 = Math.max((k - view.getMeasuredHeight()) / 2, paramInt2);
          } 
          paramInt3 += n;
          i6 = view.getMeasuredWidth();
          i5 = view.getMeasuredHeight();
          view.layout(paramInt3, paramInt1, i6 + paramInt3, paramInt1 + i5);
          i7 = i1 + 1;
          paramInt3 = i4;
          i4 = paramInt2;
          i5 = m;
          i6 = paramInt4;
        } else {
          i7 = i1;
          i6 = paramInt4;
          i5 = m;
          i4 = paramInt2;
          paramInt3 = paramInt1;
        } 
      } 
    } 
    int i3 = j - paramInt1 - m;
    int i2;
    for (b = 0, paramInt3 = j, i2 = i; b < i2; b++) {
      View view = getChildAt(b);
      if (view.getVisibility() != 8) {
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        if (!layoutParams.isDecor) {
          ItemInfo itemInfo = infoForChild(view);
          if (itemInfo != null) {
            if (layoutParams.needsMeasure) {
              layoutParams.needsMeasure = false;
              int i7 = View.MeasureSpec.makeMeasureSpec((int)(i3 * layoutParams.widthFactor), 1073741824);
              int i6 = View.MeasureSpec.makeMeasureSpec(k - paramInt2 - paramInt4, 1073741824);
              view.measure(i7, i6);
            } 
            int i5 = view.getMeasuredWidth();
            int i4 = (int)(i3 * itemInfo.offset);
            if (isLayoutRtl()) {
              i4 = 16777216 - m - i4 - i5;
            } else {
              i4 = paramInt1 + i4;
            } 
            i = view.getMeasuredHeight();
            view.layout(i4, paramInt2, i4 + i5, paramInt2 + i);
          } 
        } 
      } 
    } 
    this.mTopPageBounds = paramInt2;
    this.mBottomPageBounds = k - paramInt4;
    this.mDecorChildCount = i1;
    if (this.mFirstLayout)
      scrollToItem(this.mCurItem, false, 0, false); 
    this.mFirstLayout = false;
  }
  
  public void computeScroll() {
    if (!this.mScroller.isFinished() && this.mScroller.computeScrollOffset()) {
      int i = getScrollX();
      int j = getScrollY();
      int k = this.mScroller.getCurrX();
      int m = this.mScroller.getCurrY();
      if (i != k || j != m) {
        scrollTo(k, m);
        if (!pageScrolled(k)) {
          this.mScroller.abortAnimation();
          scrollTo(0, m);
        } 
      } 
      postInvalidateOnAnimation();
      return;
    } 
    completeScroll(true);
  }
  
  private boolean pageScrolled(int paramInt) {
    if (this.mItems.size() == 0) {
      this.mCalledSuper = false;
      onPageScrolled(0, 0.0F, 0);
      if (this.mCalledSuper)
        return false; 
      throw new IllegalStateException("onPageScrolled did not call superclass implementation");
    } 
    if (isLayoutRtl())
      paramInt = 16777216 - paramInt; 
    ItemInfo itemInfo = infoForFirstVisiblePage();
    int i = getPaddedWidth();
    int j = this.mPageMargin;
    float f = j / i;
    int k = itemInfo.position;
    f = (paramInt / i - itemInfo.offset) / (itemInfo.widthFactor + f);
    paramInt = (int)((i + j) * f);
    this.mCalledSuper = false;
    onPageScrolled(k, f, paramInt);
    if (this.mCalledSuper)
      return true; 
    throw new IllegalStateException("onPageScrolled did not call superclass implementation");
  }
  
  protected void onPageScrolled(int paramInt1, float paramFloat, int paramInt2) {
    if (this.mDecorChildCount > 0) {
      int i = getScrollX();
      int j = getPaddingLeft();
      int k = getPaddingRight();
      int m = getWidth();
      int n = getChildCount();
      for (byte b = 0; b < n; b++, j = i1, k = i2) {
        int i1, i2;
        View view = getChildAt(b);
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        if (!layoutParams.isDecor) {
          i1 = j;
          i2 = k;
        } else {
          i1 = layoutParams.gravity & 0x7;
          if (i1 != 1) {
            if (i1 != 3) {
              if (i1 != 5) {
                i1 = j;
              } else {
                i1 = m - k - view.getMeasuredWidth();
                k += view.getMeasuredWidth();
              } 
            } else {
              i1 = j;
              j += view.getWidth();
            } 
          } else {
            i1 = Math.max((m - view.getMeasuredWidth()) / 2, j);
          } 
          int i3 = i1 + i - view.getLeft();
          i1 = j;
          i2 = k;
          if (i3 != 0) {
            view.offsetLeftAndRight(i3);
            i2 = k;
            i1 = j;
          } 
        } 
      } 
    } 
    OnPageChangeListener onPageChangeListener = this.mOnPageChangeListener;
    if (onPageChangeListener != null)
      onPageChangeListener.onPageScrolled(paramInt1, paramFloat, paramInt2); 
    onPageChangeListener = this.mInternalPageChangeListener;
    if (onPageChangeListener != null)
      onPageChangeListener.onPageScrolled(paramInt1, paramFloat, paramInt2); 
    if (this.mPageTransformer != null) {
      int i = getScrollX();
      paramInt2 = getChildCount();
      for (paramInt1 = 0; paramInt1 < paramInt2; paramInt1++) {
        View view = getChildAt(paramInt1);
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        if (!layoutParams.isDecor) {
          paramFloat = (view.getLeft() - i) / getPaddedWidth();
          this.mPageTransformer.transformPage(view, paramFloat);
        } 
      } 
    } 
    this.mCalledSuper = true;
  }
  
  private void completeScroll(boolean paramBoolean) {
    boolean bool;
    if (this.mScrollState == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool) {
      setScrollingCacheEnabled(false);
      this.mScroller.abortAnimation();
      int i = getScrollX();
      int j = getScrollY();
      int k = this.mScroller.getCurrX();
      int m = this.mScroller.getCurrY();
      if (i != k || j != m)
        scrollTo(k, m); 
    } 
    this.mPopulatePending = false;
    for (byte b = 0; b < this.mItems.size(); b++) {
      ItemInfo itemInfo = this.mItems.get(b);
      if (itemInfo.scrolling) {
        bool = true;
        itemInfo.scrolling = false;
      } 
    } 
    if (bool)
      if (paramBoolean) {
        postOnAnimation(this.mEndScrollRunnable);
      } else {
        this.mEndScrollRunnable.run();
      }  
  }
  
  private boolean isGutterDrag(float paramFloat1, float paramFloat2) {
    boolean bool;
    if ((paramFloat1 < this.mGutterSize && paramFloat2 > 0.0F) || (paramFloat1 > (getWidth() - this.mGutterSize) && paramFloat2 < 0.0F)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void enableLayers(boolean paramBoolean) {
    int i = getChildCount();
    for (byte b = 0; b < i; b++) {
      boolean bool;
      if (paramBoolean) {
        bool = true;
      } else {
        bool = false;
      } 
      getChildAt(b).setLayerType(bool, null);
    } 
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent) {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual getAction : ()I
    //   4: sipush #255
    //   7: iand
    //   8: istore_2
    //   9: iload_2
    //   10: iconst_3
    //   11: if_icmpeq -> 490
    //   14: iload_2
    //   15: iconst_1
    //   16: if_icmpne -> 22
    //   19: goto -> 490
    //   22: iload_2
    //   23: ifeq -> 44
    //   26: aload_0
    //   27: getfield mIsBeingDragged : Z
    //   30: ifeq -> 35
    //   33: iconst_1
    //   34: ireturn
    //   35: aload_0
    //   36: getfield mIsUnableToDrag : Z
    //   39: ifeq -> 44
    //   42: iconst_0
    //   43: ireturn
    //   44: iload_2
    //   45: ifeq -> 324
    //   48: iload_2
    //   49: iconst_2
    //   50: if_icmpeq -> 70
    //   53: iload_2
    //   54: bipush #6
    //   56: if_icmpeq -> 62
    //   59: goto -> 463
    //   62: aload_0
    //   63: aload_1
    //   64: invokespecial onSecondaryPointerUp : (Landroid/view/MotionEvent;)V
    //   67: goto -> 463
    //   70: aload_0
    //   71: getfield mActivePointerId : I
    //   74: istore_2
    //   75: iload_2
    //   76: iconst_m1
    //   77: if_icmpne -> 83
    //   80: goto -> 463
    //   83: aload_1
    //   84: iload_2
    //   85: invokevirtual findPointerIndex : (I)I
    //   88: istore_2
    //   89: aload_1
    //   90: iload_2
    //   91: invokevirtual getX : (I)F
    //   94: fstore_3
    //   95: fload_3
    //   96: aload_0
    //   97: getfield mLastMotionX : F
    //   100: fsub
    //   101: fstore #4
    //   103: fload #4
    //   105: invokestatic abs : (F)F
    //   108: fstore #5
    //   110: aload_1
    //   111: iload_2
    //   112: invokevirtual getY : (I)F
    //   115: fstore #6
    //   117: fload #6
    //   119: aload_0
    //   120: getfield mInitialMotionY : F
    //   123: fsub
    //   124: invokestatic abs : (F)F
    //   127: fstore #7
    //   129: fload #4
    //   131: fconst_0
    //   132: fcmpl
    //   133: ifeq -> 194
    //   136: aload_0
    //   137: aload_0
    //   138: getfield mLastMotionX : F
    //   141: fload #4
    //   143: invokespecial isGutterDrag : (FF)Z
    //   146: ifne -> 194
    //   149: fload #4
    //   151: f2i
    //   152: istore_2
    //   153: fload_3
    //   154: f2i
    //   155: istore #8
    //   157: fload #6
    //   159: f2i
    //   160: istore #9
    //   162: aload_0
    //   163: aload_0
    //   164: iconst_0
    //   165: iload_2
    //   166: iload #8
    //   168: iload #9
    //   170: invokevirtual canScroll : (Landroid/view/View;ZIII)Z
    //   173: ifeq -> 194
    //   176: aload_0
    //   177: fload_3
    //   178: putfield mLastMotionX : F
    //   181: aload_0
    //   182: fload #6
    //   184: putfield mLastMotionY : F
    //   187: aload_0
    //   188: iconst_1
    //   189: putfield mIsUnableToDrag : Z
    //   192: iconst_0
    //   193: ireturn
    //   194: fload #5
    //   196: aload_0
    //   197: getfield mTouchSlop : I
    //   200: i2f
    //   201: fcmpl
    //   202: ifle -> 286
    //   205: ldc_w 0.5
    //   208: fload #5
    //   210: fmul
    //   211: fload #7
    //   213: fcmpl
    //   214: ifle -> 286
    //   217: aload_0
    //   218: iconst_1
    //   219: putfield mIsBeingDragged : Z
    //   222: aload_0
    //   223: iconst_1
    //   224: invokespecial requestParentDisallowInterceptTouchEvent : (Z)V
    //   227: aload_0
    //   228: iconst_1
    //   229: invokespecial setScrollState : (I)V
    //   232: fload #4
    //   234: fconst_0
    //   235: fcmpl
    //   236: ifle -> 254
    //   239: aload_0
    //   240: getfield mInitialMotionX : F
    //   243: aload_0
    //   244: getfield mTouchSlop : I
    //   247: i2f
    //   248: fadd
    //   249: fstore #5
    //   251: goto -> 266
    //   254: aload_0
    //   255: getfield mInitialMotionX : F
    //   258: aload_0
    //   259: getfield mTouchSlop : I
    //   262: i2f
    //   263: fsub
    //   264: fstore #5
    //   266: aload_0
    //   267: fload #5
    //   269: putfield mLastMotionX : F
    //   272: aload_0
    //   273: fload #6
    //   275: putfield mLastMotionY : F
    //   278: aload_0
    //   279: iconst_1
    //   280: invokespecial setScrollingCacheEnabled : (Z)V
    //   283: goto -> 302
    //   286: fload #7
    //   288: aload_0
    //   289: getfield mTouchSlop : I
    //   292: i2f
    //   293: fcmpl
    //   294: ifle -> 302
    //   297: aload_0
    //   298: iconst_1
    //   299: putfield mIsUnableToDrag : Z
    //   302: aload_0
    //   303: getfield mIsBeingDragged : Z
    //   306: ifeq -> 463
    //   309: aload_0
    //   310: fload_3
    //   311: invokespecial performDrag : (F)Z
    //   314: ifeq -> 463
    //   317: aload_0
    //   318: invokevirtual postInvalidateOnAnimation : ()V
    //   321: goto -> 463
    //   324: aload_1
    //   325: invokevirtual getX : ()F
    //   328: fstore #5
    //   330: aload_0
    //   331: fload #5
    //   333: putfield mInitialMotionX : F
    //   336: aload_0
    //   337: fload #5
    //   339: putfield mLastMotionX : F
    //   342: aload_1
    //   343: invokevirtual getY : ()F
    //   346: fstore #5
    //   348: aload_0
    //   349: fload #5
    //   351: putfield mInitialMotionY : F
    //   354: aload_0
    //   355: fload #5
    //   357: putfield mLastMotionY : F
    //   360: aload_0
    //   361: aload_1
    //   362: iconst_0
    //   363: invokevirtual getPointerId : (I)I
    //   366: putfield mActivePointerId : I
    //   369: aload_0
    //   370: iconst_0
    //   371: putfield mIsUnableToDrag : Z
    //   374: aload_0
    //   375: getfield mScroller : Landroid/widget/Scroller;
    //   378: invokevirtual computeScrollOffset : ()Z
    //   381: pop
    //   382: aload_0
    //   383: getfield mScrollState : I
    //   386: iconst_2
    //   387: if_icmpne -> 453
    //   390: aload_0
    //   391: getfield mScroller : Landroid/widget/Scroller;
    //   394: astore #10
    //   396: aload #10
    //   398: invokevirtual getFinalX : ()I
    //   401: aload_0
    //   402: getfield mScroller : Landroid/widget/Scroller;
    //   405: invokevirtual getCurrX : ()I
    //   408: isub
    //   409: invokestatic abs : (I)I
    //   412: aload_0
    //   413: getfield mCloseEnough : I
    //   416: if_icmple -> 453
    //   419: aload_0
    //   420: getfield mScroller : Landroid/widget/Scroller;
    //   423: invokevirtual abortAnimation : ()V
    //   426: aload_0
    //   427: iconst_0
    //   428: putfield mPopulatePending : Z
    //   431: aload_0
    //   432: invokevirtual populate : ()V
    //   435: aload_0
    //   436: iconst_1
    //   437: putfield mIsBeingDragged : Z
    //   440: aload_0
    //   441: iconst_1
    //   442: invokespecial requestParentDisallowInterceptTouchEvent : (Z)V
    //   445: aload_0
    //   446: iconst_1
    //   447: invokespecial setScrollState : (I)V
    //   450: goto -> 463
    //   453: aload_0
    //   454: iconst_0
    //   455: invokespecial completeScroll : (Z)V
    //   458: aload_0
    //   459: iconst_0
    //   460: putfield mIsBeingDragged : Z
    //   463: aload_0
    //   464: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   467: ifnonnull -> 477
    //   470: aload_0
    //   471: invokestatic obtain : ()Landroid/view/VelocityTracker;
    //   474: putfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   477: aload_0
    //   478: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   481: aload_1
    //   482: invokevirtual addMovement : (Landroid/view/MotionEvent;)V
    //   485: aload_0
    //   486: getfield mIsBeingDragged : Z
    //   489: ireturn
    //   490: aload_0
    //   491: iconst_0
    //   492: putfield mIsBeingDragged : Z
    //   495: aload_0
    //   496: iconst_0
    //   497: putfield mIsUnableToDrag : Z
    //   500: aload_0
    //   501: iconst_m1
    //   502: putfield mActivePointerId : I
    //   505: aload_0
    //   506: getfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   509: astore_1
    //   510: aload_1
    //   511: ifnull -> 523
    //   514: aload_1
    //   515: invokevirtual recycle : ()V
    //   518: aload_0
    //   519: aconst_null
    //   520: putfield mVelocityTracker : Landroid/view/VelocityTracker;
    //   523: iconst_0
    //   524: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1813	-> 0
    //   #1816	-> 9
    //   #1831	-> 22
    //   #1832	-> 26
    //   #1834	-> 33
    //   #1836	-> 35
    //   #1838	-> 42
    //   #1842	-> 44
    //   #1933	-> 62
    //   #1853	-> 70
    //   #1854	-> 75
    //   #1856	-> 80
    //   #1859	-> 83
    //   #1860	-> 89
    //   #1861	-> 95
    //   #1862	-> 103
    //   #1863	-> 110
    //   #1864	-> 117
    //   #1867	-> 129
    //   #1868	-> 162
    //   #1870	-> 176
    //   #1871	-> 181
    //   #1872	-> 187
    //   #1873	-> 192
    //   #1867	-> 194
    //   #1875	-> 194
    //   #1877	-> 217
    //   #1878	-> 222
    //   #1879	-> 227
    //   #1880	-> 232
    //   #1881	-> 254
    //   #1882	-> 272
    //   #1883	-> 278
    //   #1884	-> 286
    //   #1890	-> 297
    //   #1892	-> 302
    //   #1894	-> 309
    //   #1895	-> 317
    //   #1906	-> 324
    //   #1907	-> 342
    //   #1908	-> 360
    //   #1909	-> 369
    //   #1911	-> 374
    //   #1912	-> 382
    //   #1913	-> 396
    //   #1915	-> 419
    //   #1916	-> 426
    //   #1917	-> 431
    //   #1918	-> 435
    //   #1919	-> 440
    //   #1920	-> 445
    //   #1922	-> 453
    //   #1923	-> 458
    //   #1929	-> 463
    //   #1937	-> 463
    //   #1938	-> 470
    //   #1940	-> 477
    //   #1946	-> 485
    //   #1819	-> 490
    //   #1820	-> 495
    //   #1821	-> 500
    //   #1822	-> 505
    //   #1823	-> 514
    //   #1824	-> 518
    //   #1826	-> 523
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    if (paramMotionEvent.getAction() == 0 && paramMotionEvent.getEdgeFlags() != 0)
      return false; 
    PagerAdapter pagerAdapter = this.mAdapter;
    if (pagerAdapter == null || pagerAdapter.getCount() == 0)
      return false; 
    if (this.mVelocityTracker == null)
      this.mVelocityTracker = VelocityTracker.obtain(); 
    this.mVelocityTracker.addMovement(paramMotionEvent);
    int i = paramMotionEvent.getAction();
    int j = 0;
    i &= 0xFF;
    if (i != 0) {
      if (i != 1) {
        if (i != 2) {
          if (i != 3) {
            if (i != 5) {
              if (i == 6) {
                onSecondaryPointerUp(paramMotionEvent);
                this.mLastMotionX = paramMotionEvent.getX(paramMotionEvent.findPointerIndex(this.mActivePointerId));
              } 
            } else {
              i = paramMotionEvent.getActionIndex();
              float f = paramMotionEvent.getX(i);
              this.mLastMotionX = f;
              this.mActivePointerId = paramMotionEvent.getPointerId(i);
            } 
          } else if (this.mIsBeingDragged) {
            scrollToItem(this.mCurItem, true, 0, false);
            this.mActivePointerId = -1;
            endDrag();
            this.mLeftEdge.onRelease();
            this.mRightEdge.onRelease();
            j = 1;
          } 
        } else {
          if (!this.mIsBeingDragged) {
            i = paramMotionEvent.findPointerIndex(this.mActivePointerId);
            float f1 = paramMotionEvent.getX(i);
            float f2 = Math.abs(f1 - this.mLastMotionX);
            float f3 = paramMotionEvent.getY(i);
            float f4 = Math.abs(f3 - this.mLastMotionY);
            if (f2 > this.mTouchSlop && f2 > f4) {
              this.mIsBeingDragged = true;
              requestParentDisallowInterceptTouchEvent(true);
              f2 = this.mInitialMotionX;
              if (f1 - f2 > 0.0F) {
                f1 = f2 + this.mTouchSlop;
              } else {
                f1 = f2 - this.mTouchSlop;
              } 
              this.mLastMotionX = f1;
              this.mLastMotionY = f3;
              setScrollState(1);
              setScrollingCacheEnabled(true);
              ViewParent viewParent = getParent();
              if (viewParent != null)
                viewParent.requestDisallowInterceptTouchEvent(true); 
            } 
          } 
          if (this.mIsBeingDragged) {
            j = paramMotionEvent.findPointerIndex(this.mActivePointerId);
            float f = paramMotionEvent.getX(j);
            j = false | performDrag(f);
          } 
        } 
      } else if (this.mIsBeingDragged) {
        VelocityTracker velocityTracker = this.mVelocityTracker;
        velocityTracker.computeCurrentVelocity(1000, this.mMaximumVelocity);
        j = (int)velocityTracker.getXVelocity(this.mActivePointerId);
        this.mPopulatePending = true;
        float f1 = getScrollStart();
        f1 /= getPaddedWidth();
        ItemInfo itemInfo = infoForFirstVisiblePage();
        i = itemInfo.position;
        if (isLayoutRtl()) {
          f1 = (itemInfo.offset - f1) / itemInfo.widthFactor;
        } else {
          f1 = (f1 - itemInfo.offset) / itemInfo.widthFactor;
        } 
        int k = paramMotionEvent.findPointerIndex(this.mActivePointerId);
        float f2 = paramMotionEvent.getX(k);
        k = (int)(f2 - this.mInitialMotionX);
        i = determineTargetPage(i, f1, j, k);
        setCurrentItemInternal(i, true, true, j);
        this.mActivePointerId = -1;
        endDrag();
        this.mLeftEdge.onRelease();
        this.mRightEdge.onRelease();
        j = 1;
      } 
    } else {
      this.mScroller.abortAnimation();
      this.mPopulatePending = false;
      populate();
      float f = paramMotionEvent.getX();
      this.mLastMotionX = f;
      this.mInitialMotionY = f = paramMotionEvent.getY();
      this.mLastMotionY = f;
      this.mActivePointerId = paramMotionEvent.getPointerId(0);
    } 
    if (j != 0)
      postInvalidateOnAnimation(); 
    return true;
  }
  
  private void requestParentDisallowInterceptTouchEvent(boolean paramBoolean) {
    ViewParent viewParent = getParent();
    if (viewParent != null)
      viewParent.requestDisallowInterceptTouchEvent(paramBoolean); 
  }
  
  private boolean performDrag(float paramFloat) {
    EdgeEffect edgeEffect1, edgeEffect2;
    boolean bool4, bool5;
    float f2;
    boolean bool1 = false, bool2 = false, bool3 = false;
    int i = getPaddedWidth();
    float f1 = this.mLastMotionX;
    this.mLastMotionX = paramFloat;
    if (isLayoutRtl()) {
      edgeEffect1 = this.mRightEdge;
      edgeEffect2 = this.mLeftEdge;
    } else {
      edgeEffect1 = this.mLeftEdge;
      edgeEffect2 = this.mRightEdge;
    } 
    paramFloat = getScrollX() + f1 - paramFloat;
    if (isLayoutRtl())
      paramFloat = 1.6777216E7F - paramFloat; 
    ItemInfo itemInfo2 = this.mItems.get(0);
    if (itemInfo2.position == 0) {
      bool4 = true;
    } else {
      bool4 = false;
    } 
    if (bool4) {
      f1 = itemInfo2.offset * i;
    } else {
      f1 = i * this.mFirstOffset;
    } 
    ArrayList<ItemInfo> arrayList = this.mItems;
    ItemInfo itemInfo1 = arrayList.get(arrayList.size() - 1);
    if (itemInfo1.position == this.mAdapter.getCount() - 1) {
      bool5 = true;
    } else {
      bool5 = false;
    } 
    if (bool5) {
      f2 = itemInfo1.offset * i;
    } else {
      f2 = i * this.mLastOffset;
    } 
    if (paramFloat < f1) {
      if (bool4) {
        edgeEffect1.onPull(Math.abs(f1 - paramFloat) / i);
        bool3 = true;
      } 
      paramFloat = f1;
    } else if (paramFloat > f2) {
      bool3 = bool1;
      if (bool5) {
        edgeEffect2.onPull(Math.abs(paramFloat - f2) / i);
        bool3 = true;
      } 
      paramFloat = f2;
    } else {
      bool3 = bool2;
    } 
    if (isLayoutRtl())
      paramFloat = 1.6777216E7F - paramFloat; 
    this.mLastMotionX += paramFloat - (int)paramFloat;
    scrollTo((int)paramFloat, getScrollY());
    pageScrolled((int)paramFloat);
    return bool3;
  }
  
  private ItemInfo infoForFirstVisiblePage() {
    float f2;
    int i = getScrollStart();
    int j = getPaddedWidth();
    float f1 = 0.0F;
    if (j > 0) {
      f2 = i / j;
    } else {
      f2 = 0.0F;
    } 
    if (j > 0)
      f1 = this.mPageMargin / j; 
    int k = -1;
    float f3 = 0.0F;
    float f4 = 0.0F;
    boolean bool = true;
    ItemInfo itemInfo = null;
    int m = this.mItems.size();
    for (j = 0; j < m; ) {
      ItemInfo itemInfo1 = this.mItems.get(j);
      int n = j;
      ItemInfo itemInfo2 = itemInfo1;
      if (!bool) {
        n = j;
        itemInfo2 = itemInfo1;
        if (itemInfo1.position != k + 1) {
          itemInfo2 = this.mTempItem;
          itemInfo2.offset = f3 + f4 + f1;
          itemInfo2.position = k + 1;
          itemInfo2.widthFactor = this.mAdapter.getPageWidth(itemInfo2.position);
          n = j - 1;
        } 
      } 
      f3 = itemInfo2.offset;
      if (bool || f2 >= f3) {
        f4 = itemInfo2.widthFactor;
        if (f2 < f4 + f3 + f1 || n == this.mItems.size() - 1)
          return itemInfo2; 
        bool = false;
        k = itemInfo2.position;
        f4 = itemInfo2.widthFactor;
        j = n + 1;
        itemInfo = itemInfo2;
      } 
      return itemInfo;
    } 
    return itemInfo;
  }
  
  private int getScrollStart() {
    if (isLayoutRtl())
      return 16777216 - getScrollX(); 
    return getScrollX();
  }
  
  private int determineTargetPage(int paramInt1, float paramFloat, int paramInt2, int paramInt3) {
    if (Math.abs(paramInt3) > this.mFlingDistance && Math.abs(paramInt2) > this.mMinimumVelocity) {
      if (paramInt2 < 0) {
        paramInt2 = this.mLeftIncr;
      } else {
        paramInt2 = 0;
      } 
      paramInt1 -= paramInt2;
    } else {
      float f;
      if (paramInt1 >= this.mCurItem) {
        f = 0.4F;
      } else {
        f = 0.6F;
      } 
      paramInt1 = (int)(paramInt1 - this.mLeftIncr * (paramFloat + f));
    } 
    paramInt2 = paramInt1;
    if (this.mItems.size() > 0) {
      ItemInfo itemInfo1 = this.mItems.get(0);
      ArrayList<ItemInfo> arrayList = this.mItems;
      ItemInfo itemInfo2 = arrayList.get(arrayList.size() - 1);
      paramInt2 = MathUtils.constrain(paramInt1, itemInfo1.position, itemInfo2.position);
    } 
    return paramInt2;
  }
  
  public void draw(Canvas paramCanvas) {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: invokespecial draw : (Landroid/graphics/Canvas;)V
    //   5: iconst_0
    //   6: istore_2
    //   7: iconst_0
    //   8: istore_3
    //   9: aload_0
    //   10: invokevirtual getOverScrollMode : ()I
    //   13: istore #4
    //   15: iload #4
    //   17: ifeq -> 66
    //   20: iload #4
    //   22: iconst_1
    //   23: if_icmpne -> 49
    //   26: aload_0
    //   27: getfield mAdapter : Lcom/android/internal/widget/PagerAdapter;
    //   30: astore #5
    //   32: aload #5
    //   34: ifnull -> 49
    //   37: aload #5
    //   39: invokevirtual getCount : ()I
    //   42: iconst_1
    //   43: if_icmple -> 49
    //   46: goto -> 66
    //   49: aload_0
    //   50: getfield mLeftEdge : Landroid/widget/EdgeEffect;
    //   53: invokevirtual finish : ()V
    //   56: aload_0
    //   57: getfield mRightEdge : Landroid/widget/EdgeEffect;
    //   60: invokevirtual finish : ()V
    //   63: goto -> 257
    //   66: aload_0
    //   67: getfield mLeftEdge : Landroid/widget/EdgeEffect;
    //   70: invokevirtual isFinished : ()Z
    //   73: ifne -> 155
    //   76: aload_1
    //   77: invokevirtual save : ()I
    //   80: istore_2
    //   81: aload_0
    //   82: invokevirtual getHeight : ()I
    //   85: aload_0
    //   86: invokevirtual getPaddingTop : ()I
    //   89: isub
    //   90: aload_0
    //   91: invokevirtual getPaddingBottom : ()I
    //   94: isub
    //   95: istore #4
    //   97: aload_0
    //   98: invokevirtual getWidth : ()I
    //   101: istore_3
    //   102: aload_1
    //   103: ldc_w 270.0
    //   106: invokevirtual rotate : (F)V
    //   109: aload_1
    //   110: iload #4
    //   112: ineg
    //   113: aload_0
    //   114: invokevirtual getPaddingTop : ()I
    //   117: iadd
    //   118: i2f
    //   119: aload_0
    //   120: getfield mFirstOffset : F
    //   123: iload_3
    //   124: i2f
    //   125: fmul
    //   126: invokevirtual translate : (FF)V
    //   129: aload_0
    //   130: getfield mLeftEdge : Landroid/widget/EdgeEffect;
    //   133: iload #4
    //   135: iload_3
    //   136: invokevirtual setSize : (II)V
    //   139: iconst_0
    //   140: aload_0
    //   141: getfield mLeftEdge : Landroid/widget/EdgeEffect;
    //   144: aload_1
    //   145: invokevirtual draw : (Landroid/graphics/Canvas;)Z
    //   148: ior
    //   149: istore_3
    //   150: aload_1
    //   151: iload_2
    //   152: invokevirtual restoreToCount : (I)V
    //   155: iload_3
    //   156: istore_2
    //   157: aload_0
    //   158: getfield mRightEdge : Landroid/widget/EdgeEffect;
    //   161: invokevirtual isFinished : ()Z
    //   164: ifne -> 257
    //   167: aload_1
    //   168: invokevirtual save : ()I
    //   171: istore #4
    //   173: aload_0
    //   174: invokevirtual getWidth : ()I
    //   177: istore #6
    //   179: aload_0
    //   180: invokevirtual getHeight : ()I
    //   183: istore_2
    //   184: aload_0
    //   185: invokevirtual getPaddingTop : ()I
    //   188: istore #7
    //   190: aload_0
    //   191: invokevirtual getPaddingBottom : ()I
    //   194: istore #8
    //   196: aload_1
    //   197: ldc_w 90.0
    //   200: invokevirtual rotate : (F)V
    //   203: aload_1
    //   204: aload_0
    //   205: invokevirtual getPaddingTop : ()I
    //   208: ineg
    //   209: i2f
    //   210: aload_0
    //   211: getfield mLastOffset : F
    //   214: fconst_1
    //   215: fadd
    //   216: fneg
    //   217: iload #6
    //   219: i2f
    //   220: fmul
    //   221: invokevirtual translate : (FF)V
    //   224: aload_0
    //   225: getfield mRightEdge : Landroid/widget/EdgeEffect;
    //   228: iload_2
    //   229: iload #7
    //   231: isub
    //   232: iload #8
    //   234: isub
    //   235: iload #6
    //   237: invokevirtual setSize : (II)V
    //   240: iload_3
    //   241: aload_0
    //   242: getfield mRightEdge : Landroid/widget/EdgeEffect;
    //   245: aload_1
    //   246: invokevirtual draw : (Landroid/graphics/Canvas;)Z
    //   249: ior
    //   250: istore_2
    //   251: aload_1
    //   252: iload #4
    //   254: invokevirtual restoreToCount : (I)V
    //   257: iload_2
    //   258: ifeq -> 265
    //   261: aload_0
    //   262: invokevirtual postInvalidateOnAnimation : ()V
    //   265: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2251	-> 0
    //   #2252	-> 5
    //   #2254	-> 9
    //   #2255	-> 15
    //   #2257	-> 37
    //   #2281	-> 49
    //   #2282	-> 56
    //   #2258	-> 66
    //   #2259	-> 76
    //   #2260	-> 81
    //   #2261	-> 97
    //   #2263	-> 102
    //   #2264	-> 109
    //   #2265	-> 129
    //   #2266	-> 139
    //   #2267	-> 150
    //   #2269	-> 155
    //   #2270	-> 167
    //   #2271	-> 173
    //   #2272	-> 179
    //   #2274	-> 196
    //   #2275	-> 203
    //   #2276	-> 224
    //   #2277	-> 240
    //   #2278	-> 251
    //   #2285	-> 257
    //   #2287	-> 261
    //   #2289	-> 265
  }
  
  protected void onDraw(Canvas paramCanvas) {
    super.onDraw(paramCanvas);
    if (this.mPageMargin > 0 && this.mMarginDrawable != null && this.mItems.size() > 0 && this.mAdapter != null) {
      int i = getScrollX();
      int j = getWidth();
      float f1 = this.mPageMargin / j;
      byte b = 0;
      ItemInfo itemInfo = this.mItems.get(0);
      float f2 = itemInfo.offset;
      int k = this.mItems.size();
      int m = itemInfo.position;
      int n = ((ItemInfo)this.mItems.get(k - 1)).position;
      for (; m < n; m++) {
        ItemInfo itemInfo1;
        float f3;
        while (m > itemInfo.position && b < k) {
          ArrayList<ItemInfo> arrayList = this.mItems;
          itemInfo1 = arrayList.get(++b);
        } 
        if (m == itemInfo1.position) {
          f3 = itemInfo1.offset;
          f2 = itemInfo1.widthFactor;
        } else {
          f3 = f2;
          f2 = this.mAdapter.getPageWidth(m);
        } 
        float f4 = j * f3;
        if (isLayoutRtl()) {
          f4 = 1.6777216E7F - f4;
        } else {
          f4 = j * f2 + f4;
        } 
        f2 = f3 + f2 + f1;
        int i1 = this.mPageMargin;
        if (i1 + f4 > i) {
          this.mMarginDrawable.setBounds((int)f4, this.mTopPageBounds, (int)(i1 + f4 + 0.5F), this.mBottomPageBounds);
          this.mMarginDrawable.draw(paramCanvas);
        } 
        if (f4 > (i + j))
          break; 
      } 
    } 
  }
  
  private void onSecondaryPointerUp(MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.getActionIndex();
    int j = paramMotionEvent.getPointerId(i);
    if (j == this.mActivePointerId) {
      if (i == 0) {
        j = 1;
      } else {
        j = 0;
      } 
      this.mLastMotionX = paramMotionEvent.getX(j);
      this.mActivePointerId = paramMotionEvent.getPointerId(j);
      VelocityTracker velocityTracker = this.mVelocityTracker;
      if (velocityTracker != null)
        velocityTracker.clear(); 
    } 
  }
  
  private void endDrag() {
    this.mIsBeingDragged = false;
    this.mIsUnableToDrag = false;
    VelocityTracker velocityTracker = this.mVelocityTracker;
    if (velocityTracker != null) {
      velocityTracker.recycle();
      this.mVelocityTracker = null;
    } 
  }
  
  private void setScrollingCacheEnabled(boolean paramBoolean) {
    if (this.mScrollingCacheEnabled != paramBoolean)
      this.mScrollingCacheEnabled = paramBoolean; 
  }
  
  public boolean canScrollHorizontally(int paramInt) {
    PagerAdapter pagerAdapter = this.mAdapter;
    boolean bool1 = false, bool2 = false;
    if (pagerAdapter == null)
      return false; 
    int i = getPaddedWidth();
    int j = getScrollX();
    if (paramInt < 0) {
      bool1 = bool2;
      if (j > (int)(i * this.mFirstOffset))
        bool1 = true; 
      return bool1;
    } 
    if (paramInt > 0) {
      if (j < (int)(i * this.mLastOffset))
        bool1 = true; 
      return bool1;
    } 
    return false;
  }
  
  protected boolean canScroll(View paramView, boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3) {
    boolean bool = paramView instanceof ViewGroup;
    boolean bool1 = true;
    if (bool) {
      ViewGroup viewGroup = (ViewGroup)paramView;
      int i = paramView.getScrollX();
      int j = paramView.getScrollY();
      int k = viewGroup.getChildCount();
      for (; --k >= 0; k--) {
        View view = viewGroup.getChildAt(k);
        if (paramInt2 + i >= view.getLeft() && paramInt2 + i < view.getRight() && 
          paramInt3 + j >= view.getTop() && paramInt3 + j < view.getBottom()) {
          int m = view.getLeft();
          int n = view.getTop();
          if (canScroll(view, true, paramInt1, paramInt2 + i - m, paramInt3 + j - n))
            return true; 
        } 
      } 
    } 
    if (paramBoolean && paramView.canScrollHorizontally(-paramInt1)) {
      paramBoolean = bool1;
    } else {
      paramBoolean = false;
    } 
    return paramBoolean;
  }
  
  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent) {
    return (super.dispatchKeyEvent(paramKeyEvent) || executeKeyEvent(paramKeyEvent));
  }
  
  public boolean executeKeyEvent(KeyEvent paramKeyEvent) {
    boolean bool1 = false;
    boolean bool2 = bool1;
    if (paramKeyEvent.getAction() == 0) {
      int i = paramKeyEvent.getKeyCode();
      if (i != 21) {
        if (i != 22) {
          if (i != 61) {
            bool2 = bool1;
          } else if (paramKeyEvent.hasNoModifiers()) {
            bool2 = arrowScroll(2);
          } else {
            bool2 = bool1;
            if (paramKeyEvent.hasModifiers(1))
              bool2 = arrowScroll(1); 
          } 
        } else {
          bool2 = arrowScroll(66);
        } 
      } else {
        bool2 = arrowScroll(17);
      } 
    } 
    return bool2;
  }
  
  public boolean arrowScroll(int paramInt) {
    String str;
    View view = findFocus();
    if (view == this) {
      str = null;
    } else {
      View view1 = view;
      if (view != null) {
        boolean bool3, bool2 = false;
        ViewParent viewParent = view.getParent();
        while (true) {
          bool3 = bool2;
          if (viewParent instanceof ViewGroup) {
            if (viewParent == this) {
              bool3 = true;
              break;
            } 
            viewParent = viewParent.getParent();
            continue;
          } 
          break;
        } 
        View view2 = view;
        if (!bool3) {
          StringBuilder stringBuilder2 = new StringBuilder();
          stringBuilder2.append(view.getClass().getSimpleName());
          for (ViewParent viewParent1 = view.getParent(); viewParent1 instanceof ViewGroup; 
            viewParent1 = viewParent1.getParent()) {
            stringBuilder2.append(" => ");
            stringBuilder2.append(viewParent1.getClass().getSimpleName());
          } 
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("arrowScroll tried to find focus based on non-child current focused view ");
          stringBuilder1.append(stringBuilder2.toString());
          str = stringBuilder1.toString();
          Log.e("ViewPager", str);
          str = null;
        } 
      } 
    } 
    boolean bool = false;
    boolean bool1 = false;
    view = FocusFinder.getInstance().findNextFocus(this, (View)str, paramInt);
    if (view != null && view != str) {
      if (paramInt == 17) {
        int i = (getChildRectInPagerCoordinates(this.mTempRect, view)).left;
        int j = (getChildRectInPagerCoordinates(this.mTempRect, (View)str)).left;
        if (str != null && i >= j) {
          bool = pageLeft();
        } else {
          bool = view.requestFocus();
        } 
      } else {
        bool = bool1;
        if (paramInt == 66) {
          int i = (getChildRectInPagerCoordinates(this.mTempRect, view)).left;
          int j = (getChildRectInPagerCoordinates(this.mTempRect, (View)str)).left;
          if (str != null && i <= j) {
            bool = pageRight();
          } else {
            bool = view.requestFocus();
          } 
        } 
      } 
    } else if (paramInt == 17 || paramInt == 1) {
      bool = pageLeft();
    } else if (paramInt == 66 || paramInt == 2) {
      bool = pageRight();
    } 
    if (bool)
      playSoundEffect(SoundEffectConstants.getContantForFocusDirection(paramInt)); 
    return bool;
  }
  
  private Rect getChildRectInPagerCoordinates(Rect paramRect, View paramView) {
    Rect rect = paramRect;
    if (paramRect == null)
      rect = new Rect(); 
    if (paramView == null) {
      rect.set(0, 0, 0, 0);
      return rect;
    } 
    rect.left = paramView.getLeft();
    rect.right = paramView.getRight();
    rect.top = paramView.getTop();
    rect.bottom = paramView.getBottom();
    ViewParent viewParent = paramView.getParent();
    while (viewParent instanceof ViewGroup && viewParent != this) {
      ViewGroup viewGroup = (ViewGroup)viewParent;
      rect.left += viewGroup.getLeft();
      rect.right += viewGroup.getRight();
      rect.top += viewGroup.getTop();
      rect.bottom += viewGroup.getBottom();
      ViewParent viewParent1 = viewGroup.getParent();
    } 
    return rect;
  }
  
  boolean pageLeft() {
    return setCurrentItemInternal(this.mCurItem + this.mLeftIncr, true, false);
  }
  
  boolean pageRight() {
    return setCurrentItemInternal(this.mCurItem - this.mLeftIncr, true, false);
  }
  
  public void onRtlPropertiesChanged(int paramInt) {
    super.onRtlPropertiesChanged(paramInt);
    if (paramInt == 0) {
      this.mLeftIncr = -1;
    } else {
      this.mLeftIncr = 1;
    } 
  }
  
  public void addFocusables(ArrayList<View> paramArrayList, int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual size : ()I
    //   4: istore #4
    //   6: aload_0
    //   7: invokevirtual getDescendantFocusability : ()I
    //   10: istore #5
    //   12: iload #5
    //   14: ldc_w 393216
    //   17: if_icmpeq -> 87
    //   20: iconst_0
    //   21: istore #6
    //   23: iload #6
    //   25: aload_0
    //   26: invokevirtual getChildCount : ()I
    //   29: if_icmpge -> 87
    //   32: aload_0
    //   33: iload #6
    //   35: invokevirtual getChildAt : (I)Landroid/view/View;
    //   38: astore #7
    //   40: aload #7
    //   42: invokevirtual getVisibility : ()I
    //   45: ifne -> 81
    //   48: aload_0
    //   49: aload #7
    //   51: invokevirtual infoForChild : (Landroid/view/View;)Lcom/android/internal/widget/ViewPager$ItemInfo;
    //   54: astore #8
    //   56: aload #8
    //   58: ifnull -> 81
    //   61: aload #8
    //   63: getfield position : I
    //   66: aload_0
    //   67: getfield mCurItem : I
    //   70: if_icmpne -> 81
    //   73: aload #7
    //   75: aload_1
    //   76: iload_2
    //   77: iload_3
    //   78: invokevirtual addFocusables : (Ljava/util/ArrayList;II)V
    //   81: iinc #6, 1
    //   84: goto -> 23
    //   87: iload #5
    //   89: ldc 262144
    //   91: if_icmpne -> 103
    //   94: iload #4
    //   96: aload_1
    //   97: invokevirtual size : ()I
    //   100: if_icmpne -> 143
    //   103: aload_0
    //   104: invokevirtual isFocusable : ()Z
    //   107: ifne -> 111
    //   110: return
    //   111: iload_3
    //   112: iconst_1
    //   113: iand
    //   114: iconst_1
    //   115: if_icmpne -> 133
    //   118: aload_0
    //   119: invokevirtual isInTouchMode : ()Z
    //   122: ifeq -> 133
    //   125: aload_0
    //   126: invokevirtual isFocusableInTouchMode : ()Z
    //   129: ifne -> 133
    //   132: return
    //   133: aload_1
    //   134: ifnull -> 143
    //   137: aload_1
    //   138: aload_0
    //   139: invokevirtual add : (Ljava/lang/Object;)Z
    //   142: pop
    //   143: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2587	-> 0
    //   #2589	-> 6
    //   #2591	-> 12
    //   #2592	-> 20
    //   #2593	-> 32
    //   #2594	-> 40
    //   #2595	-> 48
    //   #2596	-> 56
    //   #2597	-> 73
    //   #2592	-> 81
    //   #2607	-> 87
    //   #2610	-> 94
    //   #2613	-> 103
    //   #2614	-> 110
    //   #2616	-> 111
    //   #2617	-> 118
    //   #2618	-> 132
    //   #2620	-> 133
    //   #2621	-> 137
    //   #2624	-> 143
  }
  
  public void addTouchables(ArrayList<View> paramArrayList) {
    for (byte b = 0; b < getChildCount(); b++) {
      View view = getChildAt(b);
      if (view.getVisibility() == 0) {
        ItemInfo itemInfo = infoForChild(view);
        if (itemInfo != null && itemInfo.position == this.mCurItem)
          view.addTouchables(paramArrayList); 
      } 
    } 
  }
  
  protected boolean onRequestFocusInDescendants(int paramInt, Rect paramRect) {
    int j;
    byte b;
    int i = getChildCount();
    if ((paramInt & 0x2) != 0) {
      j = 0;
      b = 1;
    } else {
      j = i - 1;
      b = -1;
      i = -1;
    } 
    for (; j != i; j += b) {
      View view = getChildAt(j);
      if (view.getVisibility() == 0) {
        ItemInfo itemInfo = infoForChild(view);
        if (itemInfo != null && itemInfo.position == this.mCurItem && 
          view.requestFocus(paramInt, paramRect))
          return true; 
      } 
    } 
    return false;
  }
  
  protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
    return new LayoutParams();
  }
  
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    return generateDefaultLayoutParams();
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    boolean bool;
    if (paramLayoutParams instanceof LayoutParams && super.checkLayoutParams(paramLayoutParams)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public ViewGroup.LayoutParams generateLayoutParams(AttributeSet paramAttributeSet) {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent) {
    super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
    paramAccessibilityEvent.setClassName(ViewPager.class.getName());
    paramAccessibilityEvent.setScrollable(canScroll());
    if (paramAccessibilityEvent.getEventType() == 4096) {
      PagerAdapter pagerAdapter = this.mAdapter;
      if (pagerAdapter != null) {
        paramAccessibilityEvent.setItemCount(pagerAdapter.getCount());
        paramAccessibilityEvent.setFromIndex(this.mCurItem);
        paramAccessibilityEvent.setToIndex(this.mCurItem);
      } 
    } 
  }
  
  public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
    paramAccessibilityNodeInfo.setClassName(ViewPager.class.getName());
    paramAccessibilityNodeInfo.setScrollable(canScroll());
    if (canScrollHorizontally(1)) {
      paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_FORWARD);
      paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_RIGHT);
    } 
    if (canScrollHorizontally(-1)) {
      paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_BACKWARD);
      paramAccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_LEFT);
    } 
  }
  
  public boolean performAccessibilityAction(int paramInt, Bundle paramBundle) {
    if (super.performAccessibilityAction(paramInt, paramBundle))
      return true; 
    if (paramInt != 4096)
      if (paramInt != 8192 && paramInt != 16908345) {
        if (paramInt != 16908347)
          return false; 
      } else {
        if (canScrollHorizontally(-1)) {
          setCurrentItem(this.mCurItem - 1);
          return true;
        } 
        return false;
      }  
    if (canScrollHorizontally(1)) {
      setCurrentItem(this.mCurItem + 1);
      return true;
    } 
    return false;
  }
  
  private boolean canScroll() {
    PagerAdapter pagerAdapter = this.mAdapter;
    boolean bool = true;
    if (pagerAdapter == null || pagerAdapter.getCount() <= 1)
      bool = false; 
    return bool;
  }
  
  class PagerObserver extends DataSetObserver {
    final ViewPager this$0;
    
    private PagerObserver() {}
    
    public void onChanged() {
      ViewPager.this.dataSetChanged();
    }
    
    public void onInvalidated() {
      ViewPager.this.dataSetChanged();
    }
  }
  
  class LayoutParams extends ViewGroup.LayoutParams {
    int childIndex;
    
    public int gravity;
    
    public boolean isDecor;
    
    boolean needsMeasure;
    
    int position;
    
    float widthFactor = 0.0F;
    
    public LayoutParams() {
      super(-1, -1);
    }
    
    public LayoutParams(ViewPager this$0, AttributeSet param1AttributeSet) {
      super((Context)this$0, param1AttributeSet);
      TypedArray typedArray = this$0.obtainStyledAttributes(param1AttributeSet, ViewPager.LAYOUT_ATTRS);
      this.gravity = typedArray.getInteger(0, 48);
      typedArray.recycle();
    }
  }
  
  class ViewPositionComparator implements Comparator<View> {
    public int compare(View param1View1, View param1View2) {
      ViewPager.LayoutParams layoutParams1 = (ViewPager.LayoutParams)param1View1.getLayoutParams();
      ViewPager.LayoutParams layoutParams2 = (ViewPager.LayoutParams)param1View2.getLayoutParams();
      if (layoutParams1.isDecor != layoutParams2.isDecor) {
        byte b;
        if (layoutParams1.isDecor) {
          b = 1;
        } else {
          b = -1;
        } 
        return b;
      } 
      return layoutParams1.position - layoutParams2.position;
    }
  }
  
  class Decor {}
  
  class OnAdapterChangeListener {
    public abstract void onAdapterChanged(PagerAdapter param1PagerAdapter1, PagerAdapter param1PagerAdapter2);
  }
  
  class OnPageChangeListener {
    public abstract void onPageScrollStateChanged(int param1Int);
    
    public abstract void onPageScrolled(int param1Int1, float param1Float, int param1Int2);
    
    public abstract void onPageSelected(int param1Int);
  }
  
  class PageTransformer {
    public abstract void transformPage(View param1View, float param1Float);
  }
}
