package com.android.internal.widget;

import android.os.Parcel;
import android.os.Parcelable;
import android.service.gatekeeper.GateKeeperResponse;
import android.util.Slog;

public final class VerifyCredentialResponse implements Parcelable {
  public static final Parcelable.Creator<VerifyCredentialResponse> CREATOR;
  
  public static final VerifyCredentialResponse ERROR;
  
  public static final VerifyCredentialResponse OK = new VerifyCredentialResponse();
  
  public static final int RESPONSE_ERROR = -1;
  
  public static final int RESPONSE_OK = 0;
  
  public static final int RESPONSE_RETRY = 1;
  
  private static final String TAG = "VerifyCredentialResponse";
  
  private byte[] mPayload;
  
  private int mResponseCode;
  
  private int mTimeout;
  
  static {
    ERROR = new VerifyCredentialResponse(-1, 0, null);
    CREATOR = new Parcelable.Creator<VerifyCredentialResponse>() {
        public VerifyCredentialResponse createFromParcel(Parcel param1Parcel) {
          int i = param1Parcel.readInt();
          VerifyCredentialResponse verifyCredentialResponse = new VerifyCredentialResponse(i, 0, null);
          if (i == 1) {
            verifyCredentialResponse.setTimeout(param1Parcel.readInt());
          } else if (i == 0) {
            i = param1Parcel.readInt();
            if (i > 0) {
              byte[] arrayOfByte = new byte[i];
              param1Parcel.readByteArray(arrayOfByte);
              verifyCredentialResponse.setPayload(arrayOfByte);
            } 
          } 
          return verifyCredentialResponse;
        }
        
        public VerifyCredentialResponse[] newArray(int param1Int) {
          return new VerifyCredentialResponse[param1Int];
        }
      };
  }
  
  public VerifyCredentialResponse() {
    this.mResponseCode = 0;
    this.mPayload = null;
  }
  
  public VerifyCredentialResponse(byte[] paramArrayOfbyte) {
    this.mPayload = paramArrayOfbyte;
    this.mResponseCode = 0;
  }
  
  public VerifyCredentialResponse(int paramInt) {
    this.mTimeout = paramInt;
    this.mResponseCode = 1;
    this.mPayload = null;
  }
  
  private VerifyCredentialResponse(int paramInt1, int paramInt2, byte[] paramArrayOfbyte) {
    this.mResponseCode = paramInt1;
    this.mTimeout = paramInt2;
    this.mPayload = paramArrayOfbyte;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mResponseCode);
    paramInt = this.mResponseCode;
    if (paramInt == 1) {
      paramParcel.writeInt(this.mTimeout);
    } else if (paramInt == 0) {
      byte[] arrayOfByte = this.mPayload;
      if (arrayOfByte != null) {
        paramParcel.writeInt(arrayOfByte.length);
        paramParcel.writeByteArray(this.mPayload);
      } else {
        paramParcel.writeInt(0);
      } 
    } 
  }
  
  public int describeContents() {
    return 0;
  }
  
  public byte[] getPayload() {
    return this.mPayload;
  }
  
  public int getTimeout() {
    return this.mTimeout;
  }
  
  public int getResponseCode() {
    return this.mResponseCode;
  }
  
  private void setTimeout(int paramInt) {
    this.mTimeout = paramInt;
  }
  
  private void setPayload(byte[] paramArrayOfbyte) {
    this.mPayload = paramArrayOfbyte;
  }
  
  public VerifyCredentialResponse stripPayload() {
    return new VerifyCredentialResponse(this.mResponseCode, this.mTimeout, new byte[0]);
  }
  
  public static VerifyCredentialResponse fromGateKeeperResponse(GateKeeperResponse paramGateKeeperResponse) {
    VerifyCredentialResponse verifyCredentialResponse;
    int i = paramGateKeeperResponse.getResponseCode();
    if (i == 1) {
      verifyCredentialResponse = new VerifyCredentialResponse(paramGateKeeperResponse.getTimeout());
    } else if (i == 0) {
      VerifyCredentialResponse verifyCredentialResponse1;
      byte[] arrayOfByte = verifyCredentialResponse.getPayload();
      if (arrayOfByte == null) {
        Slog.e("VerifyCredentialResponse", "verifyChallenge response had no associated payload");
        verifyCredentialResponse1 = ERROR;
      } else {
        verifyCredentialResponse1 = new VerifyCredentialResponse((byte[])verifyCredentialResponse1);
      } 
    } else {
      verifyCredentialResponse = ERROR;
    } 
    return verifyCredentialResponse;
  }
}
