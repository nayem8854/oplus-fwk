package com.android.internal.widget;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.CanvasProperty;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RecordingCanvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.IntArray;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.RenderNodeAnimator;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import com.android.internal.R;
import java.util.ArrayList;
import java.util.List;

public class LockPatternView extends View {
  private boolean mDrawingProfilingStarted = false;
  
  private final Paint mPaint = new Paint();
  
  private final Paint mPathPaint = new Paint();
  
  private final ArrayList<Cell> mPattern = new ArrayList<>(9);
  
  private final boolean[][] mPatternDrawLookup = new boolean[3][3];
  
  private float mInProgressX = -1.0F;
  
  private float mInProgressY = -1.0F;
  
  private long[] mLineFadeStart = new long[9];
  
  private DisplayMode mPatternDisplayMode = DisplayMode.Correct;
  
  private boolean mInputEnabled = true;
  
  private boolean mInStealthMode = false;
  
  private boolean mEnableHapticFeedback = true;
  
  private boolean mPatternInProgress = false;
  
  private boolean mFadePattern = true;
  
  private float mHitFactor = 0.6F;
  
  private final Path mCurrentPath = new Path();
  
  private final Rect mInvalidate = new Rect();
  
  private final Rect mTmpInvalidateRect = new Rect();
  
  private static final int ASPECT_LOCK_HEIGHT = 2;
  
  private static final int ASPECT_LOCK_WIDTH = 1;
  
  private static final int ASPECT_SQUARE = 0;
  
  public static final boolean DEBUG_A11Y = false;
  
  private static final float DRAG_THRESHHOLD = 0.0F;
  
  private static final float LINE_FADE_ALPHA_MULTIPLIER = 1.5F;
  
  private static final int MILLIS_PER_CIRCLE_ANIMATING = 700;
  
  private static final boolean PROFILE_DRAWING = false;
  
  private static final String TAG = "LockPatternView";
  
  public static final int VIRTUAL_BASE_VIEW_ID = 1;
  
  private long mAnimatingPeriodStart;
  
  private int mAspect;
  
  private AudioManager mAudioManager;
  
  private final CellState[][] mCellStates;
  
  private final int mDotSize;
  
  private final int mDotSizeActivated;
  
  private int mErrorColor;
  
  private PatternExploreByTouchHelper mExploreByTouchHelper;
  
  private final Interpolator mFastOutSlowInInterpolator;
  
  private final Interpolator mLinearOutSlowInInterpolator;
  
  private Drawable mNotSelectedDrawable;
  
  private OnPatternListener mOnPatternListener;
  
  private final int mPathWidth;
  
  private int mRegularColor;
  
  private Drawable mSelectedDrawable;
  
  private float mSquareHeight;
  
  private float mSquareWidth;
  
  private int mSuccessColor;
  
  private boolean mUseLockPatternDrawable;
  
  class Cell {
    private static final Cell[][] sCells = createCells();
    
    final int column;
    
    final int row;
    
    private static Cell[][] createCells() {
      Cell[][] arrayOfCell = new Cell[3][3];
      for (byte b = 0; b < 3; b++) {
        for (byte b1 = 0; b1 < 3; b1++)
          arrayOfCell[b][b1] = new Cell(b, b1); 
      } 
      return arrayOfCell;
    }
    
    private Cell(LockPatternView this$0, int param1Int1) {
      checkRange(this$0, param1Int1);
      this.row = this$0;
      this.column = param1Int1;
    }
    
    public int getRow() {
      return this.row;
    }
    
    public int getColumn() {
      return this.column;
    }
    
    public static Cell of(int param1Int1, int param1Int2) {
      checkRange(param1Int1, param1Int2);
      return sCells[param1Int1][param1Int2];
    }
    
    private static void checkRange(int param1Int1, int param1Int2) {
      if (param1Int1 >= 0 && param1Int1 <= 2) {
        if (param1Int2 >= 0 && param1Int2 <= 2)
          return; 
        throw new IllegalArgumentException("column must be in range 0-2");
      } 
      throw new IllegalArgumentException("row must be in range 0-2");
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("(row=");
      stringBuilder.append(this.row);
      stringBuilder.append(",clmn=");
      stringBuilder.append(this.column);
      stringBuilder.append(")");
      return stringBuilder.toString();
    }
  }
  
  class CellState {
    float alpha;
    
    int col;
    
    boolean hwAnimating;
    
    CanvasProperty<Float> hwCenterX;
    
    CanvasProperty<Float> hwCenterY;
    
    CanvasProperty<Paint> hwPaint;
    
    CanvasProperty<Float> hwRadius;
    
    public ValueAnimator lineAnimator;
    
    public float lineEndX;
    
    public float lineEndY;
    
    float radius;
    
    int row;
    
    float translationY;
    
    public CellState() {
      this.alpha = 1.0F;
      this.lineEndX = Float.MIN_VALUE;
      this.lineEndY = Float.MIN_VALUE;
    }
  }
  
  class DisplayMode extends Enum<DisplayMode> {
    private static final DisplayMode[] $VALUES;
    
    public static final DisplayMode Animate = new DisplayMode("Animate", 1);
    
    public static DisplayMode[] values() {
      return (DisplayMode[])$VALUES.clone();
    }
    
    public static DisplayMode valueOf(String param1String) {
      return Enum.<DisplayMode>valueOf(DisplayMode.class, param1String);
    }
    
    private DisplayMode(LockPatternView this$0, int param1Int) {
      super((String)this$0, param1Int);
    }
    
    public static final DisplayMode Correct = new DisplayMode("Correct", 0);
    
    public static final DisplayMode Wrong;
    
    static {
      DisplayMode displayMode = new DisplayMode("Wrong", 2);
      $VALUES = new DisplayMode[] { Correct, Animate, displayMode };
    }
  }
  
  public LockPatternView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public LockPatternView(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.LockPatternView, 17956979, 16975050);
    String str = typedArray.getString(0);
    if ("square".equals(str)) {
      this.mAspect = 0;
    } else if ("lock_width".equals(str)) {
      this.mAspect = 1;
    } else if ("lock_height".equals(str)) {
      this.mAspect = 2;
    } else {
      this.mAspect = 0;
    } 
    setClickable(true);
    this.mPathPaint.setAntiAlias(true);
    this.mPathPaint.setDither(true);
    this.mRegularColor = typedArray.getColor(3, 0);
    this.mErrorColor = typedArray.getColor(1, 0);
    this.mSuccessColor = typedArray.getColor(4, 0);
    int i = typedArray.getColor(2, this.mRegularColor);
    this.mPathPaint.setColor(i);
    this.mPathPaint.setStyle(Paint.Style.STROKE);
    this.mPathPaint.setStrokeJoin(Paint.Join.ROUND);
    this.mPathPaint.setStrokeCap(Paint.Cap.ROUND);
    this.mPathWidth = i = getResources().getDimensionPixelSize(17105306);
    this.mPathPaint.setStrokeWidth(i);
    this.mDotSize = getResources().getDimensionPixelSize(17105307);
    this.mDotSizeActivated = getResources().getDimensionPixelSize(17105308);
    boolean bool = getResources().getBoolean(17891618);
    if (bool) {
      this.mSelectedDrawable = getResources().getDrawable(17303040);
      this.mNotSelectedDrawable = getResources().getDrawable(17303038);
    } 
    this.mPaint.setAntiAlias(true);
    this.mPaint.setDither(true);
    this.mCellStates = new CellState[3][3];
    for (i = 0; i < 3; i++) {
      for (byte b = 0; b < 3; b++) {
        this.mCellStates[i][b] = new CellState();
        (this.mCellStates[i][b]).radius = (this.mDotSize / 2);
        (this.mCellStates[i][b]).row = i;
        (this.mCellStates[i][b]).col = b;
      } 
    } 
    this.mFastOutSlowInInterpolator = AnimationUtils.loadInterpolator(paramContext, 17563661);
    this.mLinearOutSlowInInterpolator = AnimationUtils.loadInterpolator(paramContext, 17563662);
    PatternExploreByTouchHelper patternExploreByTouchHelper = new PatternExploreByTouchHelper(this);
    setAccessibilityDelegate(patternExploreByTouchHelper);
    this.mAudioManager = (AudioManager)this.mContext.getSystemService("audio");
    typedArray.recycle();
  }
  
  public CellState[][] getCellStates() {
    return this.mCellStates;
  }
  
  public boolean isInStealthMode() {
    return this.mInStealthMode;
  }
  
  public boolean isTactileFeedbackEnabled() {
    return this.mEnableHapticFeedback;
  }
  
  public void setInStealthMode(boolean paramBoolean) {
    this.mInStealthMode = paramBoolean;
  }
  
  public void setFadePattern(boolean paramBoolean) {
    this.mFadePattern = paramBoolean;
  }
  
  public void setTactileFeedbackEnabled(boolean paramBoolean) {
    this.mEnableHapticFeedback = paramBoolean;
  }
  
  public void setOnPatternListener(OnPatternListener paramOnPatternListener) {
    this.mOnPatternListener = paramOnPatternListener;
  }
  
  public void setPattern(DisplayMode paramDisplayMode, List<Cell> paramList) {
    this.mPattern.clear();
    this.mPattern.addAll(paramList);
    clearPatternDrawLookup();
    for (Cell cell : paramList)
      this.mPatternDrawLookup[cell.getRow()][cell.getColumn()] = true; 
    setDisplayMode(paramDisplayMode);
  }
  
  public void setDisplayMode(DisplayMode paramDisplayMode) {
    this.mPatternDisplayMode = paramDisplayMode;
    if (paramDisplayMode == DisplayMode.Animate)
      if (this.mPattern.size() != 0) {
        this.mAnimatingPeriodStart = SystemClock.elapsedRealtime();
        Cell cell = this.mPattern.get(0);
        this.mInProgressX = getCenterXForColumn(cell.getColumn());
        this.mInProgressY = getCenterYForRow(cell.getRow());
        clearPatternDrawLookup();
      } else {
        throw new IllegalStateException("you must have a pattern to animate if you want to set the display mode to animate");
      }  
    invalidate();
  }
  
  public void startCellStateAnimation(CellState paramCellState, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, long paramLong1, long paramLong2, Interpolator paramInterpolator, Runnable paramRunnable) {
    if (isHardwareAccelerated()) {
      startCellStateAnimationHw(paramCellState, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5, paramFloat6, paramLong1, paramLong2, paramInterpolator, paramRunnable);
    } else {
      startCellStateAnimationSw(paramCellState, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5, paramFloat6, paramLong1, paramLong2, paramInterpolator, paramRunnable);
    } 
  }
  
  private void startCellStateAnimationSw(CellState paramCellState, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, long paramLong1, long paramLong2, Interpolator paramInterpolator, Runnable paramRunnable) {
    paramCellState.alpha = paramFloat1;
    paramCellState.translationY = paramFloat3;
    paramCellState.radius = (this.mDotSize / 2) * paramFloat5;
    ValueAnimator valueAnimator = ValueAnimator.ofFloat(new float[] { 0.0F, 1.0F });
    valueAnimator.setDuration(paramLong2);
    valueAnimator.setStartDelay(paramLong1);
    valueAnimator.setInterpolator((TimeInterpolator)paramInterpolator);
    valueAnimator.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new Object(this, paramCellState, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5, paramFloat6));
    valueAnimator.addListener((Animator.AnimatorListener)new Object(this, paramRunnable));
    valueAnimator.start();
  }
  
  private void startCellStateAnimationHw(CellState paramCellState, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, long paramLong1, long paramLong2, Interpolator paramInterpolator, Runnable paramRunnable) {
    paramCellState.alpha = paramFloat2;
    paramCellState.translationY = paramFloat4;
    paramCellState.radius = (this.mDotSize / 2) * paramFloat6;
    paramCellState.hwAnimating = true;
    int i = paramCellState.row;
    float f = getCenterYForRow(i);
    paramCellState.hwCenterY = CanvasProperty.createFloat(f + paramFloat3);
    paramCellState.hwCenterX = CanvasProperty.createFloat(getCenterXForColumn(paramCellState.col));
    paramCellState.hwRadius = CanvasProperty.createFloat((this.mDotSize / 2) * paramFloat5);
    this.mPaint.setColor(getCurrentColor(false));
    this.mPaint.setAlpha((int)(255.0F * paramFloat1));
    paramCellState.hwPaint = CanvasProperty.createPaint(new Paint(this.mPaint));
    CanvasProperty<Float> canvasProperty = paramCellState.hwCenterY;
    i = paramCellState.row;
    paramFloat1 = getCenterYForRow(i);
    startRtFloatAnimation(canvasProperty, paramFloat1 + paramFloat4, paramLong1, paramLong2, paramInterpolator);
    startRtFloatAnimation(paramCellState.hwRadius, (this.mDotSize / 2) * paramFloat6, paramLong1, paramLong2, paramInterpolator);
    startRtAlphaAnimation(paramCellState, paramFloat2, paramLong1, paramLong2, paramInterpolator, (Animator.AnimatorListener)new Object(this, paramCellState, paramRunnable));
    invalidate();
  }
  
  private void startRtAlphaAnimation(CellState paramCellState, float paramFloat, long paramLong1, long paramLong2, Interpolator paramInterpolator, Animator.AnimatorListener paramAnimatorListener) {
    RenderNodeAnimator renderNodeAnimator = new RenderNodeAnimator(paramCellState.hwPaint, 1, (int)(255.0F * paramFloat));
    renderNodeAnimator.setDuration(paramLong2);
    renderNodeAnimator.setStartDelay(paramLong1);
    renderNodeAnimator.setInterpolator((TimeInterpolator)paramInterpolator);
    renderNodeAnimator.setTarget(this);
    renderNodeAnimator.addListener(paramAnimatorListener);
    renderNodeAnimator.start();
  }
  
  private void startRtFloatAnimation(CanvasProperty<Float> paramCanvasProperty, float paramFloat, long paramLong1, long paramLong2, Interpolator paramInterpolator) {
    RenderNodeAnimator renderNodeAnimator = new RenderNodeAnimator(paramCanvasProperty, paramFloat);
    renderNodeAnimator.setDuration(paramLong2);
    renderNodeAnimator.setStartDelay(paramLong1);
    renderNodeAnimator.setInterpolator((TimeInterpolator)paramInterpolator);
    renderNodeAnimator.setTarget(this);
    renderNodeAnimator.start();
  }
  
  private void notifyCellAdded() {
    OnPatternListener onPatternListener = this.mOnPatternListener;
    if (onPatternListener != null)
      onPatternListener.onPatternCellAdded(this.mPattern); 
    this.mExploreByTouchHelper.invalidateRoot();
  }
  
  private void notifyPatternStarted() {
    sendAccessEvent(17040468);
    OnPatternListener onPatternListener = this.mOnPatternListener;
    if (onPatternListener != null)
      onPatternListener.onPatternStart(); 
  }
  
  private void notifyPatternDetected() {
    sendAccessEvent(17040467);
    OnPatternListener onPatternListener = this.mOnPatternListener;
    if (onPatternListener != null)
      onPatternListener.onPatternDetected(this.mPattern); 
  }
  
  private void notifyPatternCleared() {
    sendAccessEvent(17040466);
    OnPatternListener onPatternListener = this.mOnPatternListener;
    if (onPatternListener != null)
      onPatternListener.onPatternCleared(); 
  }
  
  public void clearPattern() {
    resetPattern();
  }
  
  protected boolean dispatchHoverEvent(MotionEvent paramMotionEvent) {
    boolean bool1 = super.dispatchHoverEvent(paramMotionEvent);
    boolean bool2 = this.mExploreByTouchHelper.dispatchHoverEvent(paramMotionEvent);
    return bool1 | bool2;
  }
  
  private void resetPattern() {
    this.mPattern.clear();
    clearPatternDrawLookup();
    this.mPatternDisplayMode = DisplayMode.Correct;
    invalidate();
  }
  
  public boolean isEmpty() {
    return this.mPattern.isEmpty();
  }
  
  private void clearPatternDrawLookup() {
    for (byte b = 0; b < 3; b++) {
      for (byte b1 = 0; b1 < 3; b1++) {
        this.mPatternDrawLookup[b][b1] = false;
        this.mLineFadeStart[b1 * 3 + b] = 0L;
      } 
    } 
  }
  
  public void disableInput() {
    this.mInputEnabled = false;
  }
  
  public void enableInput() {
    this.mInputEnabled = true;
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    paramInt1 = paramInt1 - this.mPaddingLeft - this.mPaddingRight;
    this.mSquareWidth = paramInt1 / 3.0F;
    paramInt2 = paramInt2 - this.mPaddingTop - this.mPaddingBottom;
    this.mSquareHeight = paramInt2 / 3.0F;
    this.mExploreByTouchHelper.invalidateRoot();
    if (this.mUseLockPatternDrawable) {
      this.mNotSelectedDrawable.setBounds(this.mPaddingLeft, this.mPaddingTop, paramInt1, paramInt2);
      this.mSelectedDrawable.setBounds(this.mPaddingLeft, this.mPaddingTop, paramInt1, paramInt2);
    } 
  }
  
  private int resolveMeasured(int paramInt1, int paramInt2) {
    int i = View.MeasureSpec.getSize(paramInt1);
    paramInt1 = View.MeasureSpec.getMode(paramInt1);
    if (paramInt1 != Integer.MIN_VALUE) {
      if (paramInt1 != 0) {
        paramInt1 = i;
      } else {
        paramInt1 = paramInt2;
      } 
    } else {
      paramInt1 = Math.max(i, paramInt2);
    } 
    return paramInt1;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    int i = getSuggestedMinimumWidth();
    int j = getSuggestedMinimumHeight();
    paramInt1 = resolveMeasured(paramInt1, i);
    paramInt2 = resolveMeasured(paramInt2, j);
    j = this.mAspect;
    if (j != 0) {
      if (j != 1) {
        if (j == 2)
          paramInt1 = Math.min(paramInt1, paramInt2); 
      } else {
        paramInt2 = Math.min(paramInt1, paramInt2);
      } 
    } else {
      paramInt2 = paramInt1 = Math.min(paramInt1, paramInt2);
    } 
    setMeasuredDimension(paramInt1, paramInt2);
  }
  
  private Cell detectAndAddHit(float paramFloat1, float paramFloat2) {
    Cell cell = checkForNewHit(paramFloat1, paramFloat2);
    if (cell != null) {
      Cell cell1 = null;
      ArrayList<Cell> arrayList = this.mPattern;
      if (!arrayList.isEmpty()) {
        cell1 = arrayList.get(arrayList.size() - 1);
        int i = cell.row - cell1.row;
        int j = cell.column - cell1.column;
        int k = cell1.row;
        int m = cell1.column;
        int n = Math.abs(i);
        byte b = -1;
        int i1 = k;
        if (n == 2) {
          i1 = k;
          if (Math.abs(j) != 1) {
            k = cell1.row;
            if (i > 0) {
              i1 = 1;
            } else {
              i1 = -1;
            } 
            i1 = k + i1;
          } 
        } 
        k = m;
        if (Math.abs(j) == 2) {
          k = m;
          if (Math.abs(i) != 1) {
            m = cell1.column;
            k = b;
            if (j > 0)
              k = 1; 
            k = m + k;
          } 
        } 
        cell1 = Cell.of(i1, k);
      } 
      if (cell1 != null && !this.mPatternDrawLookup[cell1.row][cell1.column])
        addCellToPattern(cell1); 
      addCellToPattern(cell);
      if (this.mEnableHapticFeedback)
        performHapticFeedback(1, 3); 
      return cell;
    } 
    return null;
  }
  
  private void addCellToPattern(Cell paramCell) {
    this.mPatternDrawLookup[paramCell.getRow()][paramCell.getColumn()] = true;
    this.mPattern.add(paramCell);
    if (!this.mInStealthMode)
      startCellActivatedAnimation(paramCell); 
    notifyCellAdded();
  }
  
  private void startCellActivatedAnimation(Cell paramCell) {
    CellState cellState = this.mCellStates[paramCell.row][paramCell.column];
    startRadiusAnimation((this.mDotSize / 2), (this.mDotSizeActivated / 2), 96L, this.mLinearOutSlowInInterpolator, cellState, (Runnable)new Object(this, cellState));
    float f1 = this.mInProgressX, f2 = this.mInProgressY;
    int i = paramCell.column;
    float f3 = getCenterXForColumn(i), f4 = getCenterYForRow(paramCell.row);
    startLineEndAnimation(cellState, f1, f2, f3, f4);
  }
  
  private void startLineEndAnimation(CellState paramCellState, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    ValueAnimator valueAnimator = ValueAnimator.ofFloat(new float[] { 0.0F, 1.0F });
    valueAnimator.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new Object(this, paramCellState, paramFloat1, paramFloat3, paramFloat2, paramFloat4));
    valueAnimator.addListener((Animator.AnimatorListener)new Object(this, paramCellState));
    valueAnimator.setInterpolator((TimeInterpolator)this.mFastOutSlowInInterpolator);
    valueAnimator.setDuration(100L);
    valueAnimator.start();
    paramCellState.lineAnimator = valueAnimator;
  }
  
  private void startRadiusAnimation(float paramFloat1, float paramFloat2, long paramLong, Interpolator paramInterpolator, CellState paramCellState, Runnable paramRunnable) {
    ValueAnimator valueAnimator = ValueAnimator.ofFloat(new float[] { paramFloat1, paramFloat2 });
    valueAnimator.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new Object(this, paramCellState));
    if (paramRunnable != null)
      valueAnimator.addListener((Animator.AnimatorListener)new Object(this, paramRunnable)); 
    valueAnimator.setInterpolator((TimeInterpolator)paramInterpolator);
    valueAnimator.setDuration(paramLong);
    valueAnimator.start();
  }
  
  private Cell checkForNewHit(float paramFloat1, float paramFloat2) {
    int i = getRowHit(paramFloat2);
    if (i < 0)
      return null; 
    int j = getColumnHit(paramFloat1);
    if (j < 0)
      return null; 
    if (this.mPatternDrawLookup[i][j])
      return null; 
    return Cell.of(i, j);
  }
  
  private int getRowHit(float paramFloat) {
    float f1 = this.mSquareHeight;
    float f2 = this.mHitFactor * f1;
    float f3 = this.mPaddingTop, f4 = (f1 - f2) / 2.0F;
    for (byte b = 0; b < 3; b++) {
      float f = b * f1 + f3 + f4;
      if (paramFloat >= f && paramFloat <= f + f2)
        return b; 
    } 
    return -1;
  }
  
  private int getColumnHit(float paramFloat) {
    float f1 = this.mSquareWidth;
    float f2 = this.mHitFactor * f1;
    float f3 = this.mPaddingLeft, f4 = (f1 - f2) / 2.0F;
    for (byte b = 0; b < 3; b++) {
      float f = b * f1 + f3 + f4;
      if (paramFloat >= f && paramFloat <= f + f2)
        return b; 
    } 
    return -1;
  }
  
  public boolean onHoverEvent(MotionEvent paramMotionEvent) {
    if (AccessibilityManager.getInstance(this.mContext).isTouchExplorationEnabled()) {
      int i = paramMotionEvent.getAction();
      if (i != 7) {
        if (i != 9) {
          if (i == 10)
            paramMotionEvent.setAction(1); 
        } else {
          paramMotionEvent.setAction(0);
        } 
      } else {
        paramMotionEvent.setAction(2);
      } 
      onTouchEvent(paramMotionEvent);
      paramMotionEvent.setAction(i);
    } 
    return super.onHoverEvent(paramMotionEvent);
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    if (!this.mInputEnabled || !isEnabled())
      return false; 
    int i = paramMotionEvent.getAction();
    if (i != 0) {
      if (i != 1) {
        if (i != 2) {
          if (i != 3)
            return false; 
          if (this.mPatternInProgress) {
            setPatternInProgress(false);
            resetPattern();
            notifyPatternCleared();
          } 
          return true;
        } 
        handleActionMove(paramMotionEvent);
        return true;
      } 
      handleActionUp();
      return true;
    } 
    handleActionDown(paramMotionEvent);
    return true;
  }
  
  private void setPatternInProgress(boolean paramBoolean) {
    this.mPatternInProgress = paramBoolean;
    this.mExploreByTouchHelper.invalidateRoot();
  }
  
  private void handleActionMove(MotionEvent paramMotionEvent) {
    float f = this.mPathWidth;
    int i = paramMotionEvent.getHistorySize();
    this.mTmpInvalidateRect.setEmpty();
    boolean bool = false;
    for (byte b = 0; b < i + 1; b++) {
      float f1, f2;
      if (b < i) {
        f1 = paramMotionEvent.getHistoricalX(b);
      } else {
        f1 = paramMotionEvent.getX();
      } 
      if (b < i) {
        f2 = paramMotionEvent.getHistoricalY(b);
      } else {
        f2 = paramMotionEvent.getY();
      } 
      Cell cell = detectAndAddHit(f1, f2);
      int j = this.mPattern.size();
      if (cell != null && j == 1) {
        setPatternInProgress(true);
        notifyPatternStarted();
      } 
      float f3 = Math.abs(f1 - this.mInProgressX);
      float f4 = Math.abs(f2 - this.mInProgressY);
      if (f3 > 0.0F || f4 > 0.0F)
        bool = true; 
      if (this.mPatternInProgress && j > 0) {
        ArrayList<Cell> arrayList = this.mPattern;
        Cell cell1 = arrayList.get(j - 1);
        float f5 = getCenterXForColumn(cell1.column);
        f3 = getCenterYForRow(cell1.row);
        f4 = Math.min(f5, f1) - f;
        f5 = Math.max(f5, f1) + f;
        f1 = Math.min(f3, f2) - f;
        f3 = Math.max(f3, f2) + f;
        if (cell != null) {
          float f6 = this.mSquareWidth * 0.5F;
          float f7 = this.mSquareHeight * 0.5F;
          f2 = getCenterXForColumn(cell.column);
          float f8 = getCenterYForRow(cell.row);
          f4 = Math.min(f2 - f6, f4);
          f2 = Math.max(f2 + f6, f5);
          f1 = Math.min(f8 - f7, f1);
          f3 = Math.max(f8 + f7, f3);
        } else {
          f2 = f5;
        } 
        Rect rect = this.mTmpInvalidateRect;
        int k = Math.round(f4), m = Math.round(f1);
        j = Math.round(f2);
        int n = Math.round(f3);
        rect.union(k, m, j, n);
      } 
    } 
    this.mInProgressX = paramMotionEvent.getX();
    this.mInProgressY = paramMotionEvent.getY();
    if (bool) {
      this.mInvalidate.union(this.mTmpInvalidateRect);
      invalidate(this.mInvalidate);
      this.mInvalidate.set(this.mTmpInvalidateRect);
    } 
  }
  
  private void sendAccessEvent(int paramInt) {
    announceForAccessibility(this.mContext.getString(paramInt));
  }
  
  private void handleActionUp() {
    if (!this.mPattern.isEmpty()) {
      setPatternInProgress(false);
      cancelLineAnimations();
      notifyPatternDetected();
      if (this.mFadePattern) {
        clearPatternDrawLookup();
        this.mPatternDisplayMode = DisplayMode.Correct;
      } 
      invalidate();
    } 
  }
  
  private void cancelLineAnimations() {
    for (byte b = 0; b < 3; b++) {
      for (byte b1 = 0; b1 < 3; b1++) {
        CellState cellState = this.mCellStates[b][b1];
        if (cellState.lineAnimator != null) {
          cellState.lineAnimator.cancel();
          cellState.lineEndX = Float.MIN_VALUE;
          cellState.lineEndY = Float.MIN_VALUE;
        } 
      } 
    } 
  }
  
  private void handleActionDown(MotionEvent paramMotionEvent) {
    resetPattern();
    float f1 = paramMotionEvent.getX();
    float f2 = paramMotionEvent.getY();
    Cell cell = detectAndAddHit(f1, f2);
    if (cell != null) {
      setPatternInProgress(true);
      this.mPatternDisplayMode = DisplayMode.Correct;
      notifyPatternStarted();
    } else if (this.mPatternInProgress) {
      setPatternInProgress(false);
      notifyPatternCleared();
    } 
    if (cell != null) {
      float f3 = getCenterXForColumn(cell.column);
      float f4 = getCenterYForRow(cell.row);
      float f5 = this.mSquareWidth / 2.0F;
      float f6 = this.mSquareHeight / 2.0F;
      invalidate((int)(f3 - f5), (int)(f4 - f6), (int)(f3 + f5), (int)(f4 + f6));
    } 
    this.mInProgressX = f1;
    this.mInProgressY = f2;
  }
  
  private float getCenterXForColumn(int paramInt) {
    float f1 = this.mPaddingLeft, f2 = paramInt, f3 = this.mSquareWidth;
    return f1 + f2 * f3 + f3 / 2.0F;
  }
  
  private float getCenterYForRow(int paramInt) {
    float f1 = this.mPaddingTop, f2 = paramInt, f3 = this.mSquareHeight;
    return f1 + f2 * f3 + f3 / 2.0F;
  }
  
  protected void onDraw(Canvas paramCanvas) {
    ArrayList<Cell> arrayList = this.mPattern;
    int i = arrayList.size();
    boolean[][] arrayOfBoolean = this.mPatternDrawLookup;
    if (this.mPatternDisplayMode == DisplayMode.Animate) {
      int k = (int)(SystemClock.elapsedRealtime() - this.mAnimatingPeriodStart) % (i + 1) * 700;
      int m = k / 700;
      clearPatternDrawLookup();
      int n;
      for (n = 0; n < m; n++) {
        Cell cell = arrayList.get(n);
        arrayOfBoolean[cell.getRow()][cell.getColumn()] = true;
      } 
      if (m > 0 && m < i) {
        n = 1;
      } else {
        n = 0;
      } 
      if (n != 0) {
        float f1 = (k % 700) / 700.0F;
        Cell cell = arrayList.get(m - 1);
        float f2 = getCenterXForColumn(cell.column);
        float f3 = getCenterYForRow(cell.row);
        cell = arrayList.get(m);
        n = cell.column;
        float f4 = getCenterXForColumn(n);
        n = cell.row;
        float f5 = getCenterYForRow(n);
        this.mInProgressX = f2 + (f4 - f2) * f1;
        this.mInProgressY = f3 + (f5 - f3) * f1;
      } 
      invalidate();
    } 
    Path path = this.mCurrentPath;
    path.rewind();
    int j;
    for (j = 0; j < 3; j++) {
      float f = getCenterYForRow(j);
      for (byte b = 0; b < 3; b++) {
        CellState cellState = this.mCellStates[j][b];
        float f2 = getCenterXForColumn(b);
        float f1 = cellState.translationY;
        if (this.mUseLockPatternDrawable) {
          drawCellDrawable(paramCanvas, j, b, cellState.radius, arrayOfBoolean[j][b]);
        } else if (isHardwareAccelerated() && cellState.hwAnimating) {
          RecordingCanvas recordingCanvas = (RecordingCanvas)paramCanvas;
          recordingCanvas.drawCircle(cellState.hwCenterX, cellState.hwCenterY, cellState.hwRadius, cellState.hwPaint);
        } else {
          drawCircle(paramCanvas, (int)f2, (int)f + f1, cellState.radius, arrayOfBoolean[j][b], cellState.alpha);
        } 
      } 
    } 
    j = this.mInStealthMode ^ true;
    if (j != 0) {
      this.mPathPaint.setColor(getCurrentColor(true));
      int k = 0;
      float f1 = 0.0F;
      float f2 = 0.0F;
      long l = SystemClock.elapsedRealtime();
      for (byte b = 0; b < i; b++, k = 1) {
        Cell cell = arrayList.get(b);
        if (!arrayOfBoolean[cell.row][cell.column])
          break; 
        long[] arrayOfLong = this.mLineFadeStart;
        if (arrayOfLong[b] == 0L)
          arrayOfLong[b] = SystemClock.elapsedRealtime(); 
        float f3 = getCenterXForColumn(cell.column);
        float f4 = getCenterYForRow(cell.row);
        if (b != 0) {
          k = (int)Math.min((float)(l - this.mLineFadeStart[b]) * 1.5F, 255.0F);
          CellState cellState = this.mCellStates[cell.row][cell.column];
          path.rewind();
          path.moveTo(f1, f2);
          if (cellState.lineEndX != Float.MIN_VALUE && cellState.lineEndY != Float.MIN_VALUE) {
            path.lineTo(cellState.lineEndX, cellState.lineEndY);
            if (this.mFadePattern) {
              this.mPathPaint.setAlpha(255 - k);
            } else {
              this.mPathPaint.setAlpha(255);
            } 
          } else {
            path.lineTo(f3, f4);
            if (this.mFadePattern) {
              this.mPathPaint.setAlpha(255 - k);
            } else {
              this.mPathPaint.setAlpha(255);
            } 
          } 
          paramCanvas.drawPath(path, this.mPathPaint);
        } 
        f1 = f3;
        f2 = f4;
      } 
      if ((this.mPatternInProgress || this.mPatternDisplayMode == DisplayMode.Animate) && k != 0) {
        path.rewind();
        path.moveTo(f1, f2);
        path.lineTo(this.mInProgressX, this.mInProgressY);
        this.mPathPaint.setAlpha((int)(calculateLastSegmentAlpha(this.mInProgressX, this.mInProgressY, f1, f2) * 255.0F));
        paramCanvas.drawPath(path, this.mPathPaint);
      } 
    } 
  }
  
  private float calculateLastSegmentAlpha(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    paramFloat1 -= paramFloat3;
    paramFloat2 -= paramFloat4;
    paramFloat1 = (float)Math.sqrt((paramFloat1 * paramFloat1 + paramFloat2 * paramFloat2));
    paramFloat1 /= this.mSquareWidth;
    return Math.min(1.0F, Math.max(0.0F, (paramFloat1 - 0.3F) * 4.0F));
  }
  
  private int getCurrentColor(boolean paramBoolean) {
    if (!paramBoolean || this.mInStealthMode || this.mPatternInProgress)
      return this.mRegularColor; 
    if (this.mPatternDisplayMode == DisplayMode.Wrong)
      return this.mErrorColor; 
    if (this.mPatternDisplayMode == DisplayMode.Correct || this.mPatternDisplayMode == DisplayMode.Animate)
      return this.mSuccessColor; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("unknown display mode ");
    stringBuilder.append(this.mPatternDisplayMode);
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  private void drawCircle(Canvas paramCanvas, float paramFloat1, float paramFloat2, float paramFloat3, boolean paramBoolean, float paramFloat4) {
    this.mPaint.setColor(getCurrentColor(paramBoolean));
    this.mPaint.setAlpha((int)(255.0F * paramFloat4));
    paramCanvas.drawCircle(paramFloat1, paramFloat2, paramFloat3, this.mPaint);
  }
  
  private void drawCellDrawable(Canvas paramCanvas, int paramInt1, int paramInt2, float paramFloat, boolean paramBoolean) {
    Rect rect = new Rect((int)(this.mPaddingLeft + paramInt2 * this.mSquareWidth), (int)(this.mPaddingTop + paramInt1 * this.mSquareHeight), (int)(this.mPaddingLeft + (paramInt2 + 1) * this.mSquareWidth), (int)(this.mPaddingTop + (paramInt1 + 1) * this.mSquareHeight));
    paramFloat /= (this.mDotSize / 2);
    paramCanvas.save();
    paramCanvas.clipRect(rect);
    paramCanvas.scale(paramFloat, paramFloat, rect.centerX(), rect.centerY());
    if (!paramBoolean || paramFloat > 1.0F) {
      this.mNotSelectedDrawable.draw(paramCanvas);
    } else {
      this.mSelectedDrawable.draw(paramCanvas);
    } 
    paramCanvas.restore();
  }
  
  protected Parcelable onSaveInstanceState() {
    Parcelable parcelable = super.onSaveInstanceState();
    byte[] arrayOfByte = LockPatternUtils.patternToByteArray(this.mPattern);
    if (arrayOfByte != null) {
      String str = new String(arrayOfByte);
    } else {
      arrayOfByte = null;
    } 
    DisplayMode displayMode = this.mPatternDisplayMode;
    return 
      
      (Parcelable)new SavedState((String)arrayOfByte, displayMode.ordinal(), this.mInputEnabled, this.mInStealthMode, this.mEnableHapticFeedback);
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable) {
    SavedState savedState = (SavedState)paramParcelable;
    super.onRestoreInstanceState(savedState.getSuperState());
    DisplayMode displayMode = DisplayMode.Correct;
    List<Cell> list = LockPatternUtils.byteArrayToPattern(savedState.getSerializedPattern().getBytes());
    setPattern(displayMode, list);
    this.mPatternDisplayMode = DisplayMode.values()[savedState.getDisplayMode()];
    this.mInputEnabled = savedState.isInputEnabled();
    this.mInStealthMode = savedState.isInStealthMode();
    this.mEnableHapticFeedback = savedState.isTactileFeedbackEnabled();
  }
  
  class SavedState extends View.BaseSavedState {
    private SavedState(String param1String, int param1Int, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3) {
      super((Parcelable)this$0);
      this.mSerializedPattern = param1String;
      this.mDisplayMode = param1Int;
      this.mInputEnabled = param1Boolean1;
      this.mInStealthMode = param1Boolean2;
      this.mTactileFeedbackEnabled = param1Boolean3;
    }
    
    private SavedState(LockPatternView this$0) {
      super((Parcel)this$0);
      this.mSerializedPattern = this$0.readString();
      this.mDisplayMode = this$0.readInt();
      this.mInputEnabled = ((Boolean)this$0.readValue(null)).booleanValue();
      this.mInStealthMode = ((Boolean)this$0.readValue(null)).booleanValue();
      this.mTactileFeedbackEnabled = ((Boolean)this$0.readValue(null)).booleanValue();
    }
    
    public String getSerializedPattern() {
      return this.mSerializedPattern;
    }
    
    public int getDisplayMode() {
      return this.mDisplayMode;
    }
    
    public boolean isInputEnabled() {
      return this.mInputEnabled;
    }
    
    public boolean isInStealthMode() {
      return this.mInStealthMode;
    }
    
    public boolean isTactileFeedbackEnabled() {
      return this.mTactileFeedbackEnabled;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeString(this.mSerializedPattern);
      param1Parcel.writeInt(this.mDisplayMode);
      param1Parcel.writeValue(Boolean.valueOf(this.mInputEnabled));
      param1Parcel.writeValue(Boolean.valueOf(this.mInStealthMode));
      param1Parcel.writeValue(Boolean.valueOf(this.mTactileFeedbackEnabled));
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = (Parcelable.Creator<SavedState>)new Object();
    
    private final int mDisplayMode;
    
    private final boolean mInStealthMode;
    
    private final boolean mInputEnabled;
    
    private final String mSerializedPattern;
    
    private final boolean mTactileFeedbackEnabled;
  }
  
  class PatternExploreByTouchHelper extends ExploreByTouchHelper {
    final LockPatternView this$0;
    
    private Rect mTempRect = new Rect();
    
    private final SparseArray<VirtualViewContainer> mItems = new SparseArray();
    
    class VirtualViewContainer {
      CharSequence description;
      
      final LockPatternView.PatternExploreByTouchHelper this$1;
      
      public VirtualViewContainer(CharSequence param2CharSequence) {
        this.description = param2CharSequence;
      }
    }
    
    public PatternExploreByTouchHelper(View param1View) {
      super(param1View);
      for (byte b = 1; b < 10; b++)
        this.mItems.put(b, new VirtualViewContainer(getTextForVirtualView(b))); 
    }
    
    protected int getVirtualViewAt(float param1Float1, float param1Float2) {
      return getVirtualViewIdForHit(param1Float1, param1Float2);
    }
    
    protected void getVisibleVirtualViews(IntArray param1IntArray) {
      if (!LockPatternView.this.mPatternInProgress)
        return; 
      for (byte b = 1; b < 10; b++)
        param1IntArray.add(b); 
    }
    
    protected void onPopulateEventForVirtualView(int param1Int, AccessibilityEvent param1AccessibilityEvent) {
      VirtualViewContainer virtualViewContainer = (VirtualViewContainer)this.mItems.get(param1Int);
      if (virtualViewContainer != null)
        param1AccessibilityEvent.getText().add(virtualViewContainer.description); 
    }
    
    public void onPopulateAccessibilityEvent(View param1View, AccessibilityEvent param1AccessibilityEvent) {
      super.onPopulateAccessibilityEvent(param1View, param1AccessibilityEvent);
      if (!LockPatternView.this.mPatternInProgress) {
        CharSequence charSequence = LockPatternView.this.getContext().getText(17040463);
        param1AccessibilityEvent.setContentDescription(charSequence);
      } 
    }
    
    protected void onPopulateNodeForVirtualView(int param1Int, AccessibilityNodeInfo param1AccessibilityNodeInfo) {
      param1AccessibilityNodeInfo.setText(getTextForVirtualView(param1Int));
      param1AccessibilityNodeInfo.setContentDescription(getTextForVirtualView(param1Int));
      if (LockPatternView.this.mPatternInProgress) {
        param1AccessibilityNodeInfo.setFocusable(true);
        if (isClickable(param1Int)) {
          param1AccessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_CLICK);
          param1AccessibilityNodeInfo.setClickable(isClickable(param1Int));
        } 
      } 
      Rect rect = getBoundsForVirtualView(param1Int);
      param1AccessibilityNodeInfo.setBoundsInParent(rect);
    }
    
    private boolean isClickable(int param1Int) {
      if (param1Int != Integer.MIN_VALUE) {
        int i = (param1Int - 1) / 3;
        return LockPatternView.this.mPatternDrawLookup[i][(param1Int - 1) % 3] ^ true;
      } 
      return false;
    }
    
    protected boolean onPerformActionForVirtualView(int param1Int1, int param1Int2, Bundle param1Bundle) {
      if (param1Int2 != 16)
        return false; 
      return onItemClicked(param1Int1);
    }
    
    boolean onItemClicked(int param1Int) {
      invalidateVirtualView(param1Int);
      sendEventForVirtualView(param1Int, 1);
      return true;
    }
    
    private Rect getBoundsForVirtualView(int param1Int) {
      int i = param1Int - 1;
      Rect rect = this.mTempRect;
      param1Int = i / 3;
      i %= 3;
      LockPatternView.CellState cellState = LockPatternView.this.mCellStates[param1Int][i];
      float f1 = LockPatternView.this.getCenterXForColumn(i);
      float f2 = LockPatternView.this.getCenterYForRow(param1Int);
      float f3 = LockPatternView.this.mSquareHeight * LockPatternView.this.mHitFactor * 0.5F;
      float f4 = LockPatternView.this.mSquareWidth * LockPatternView.this.mHitFactor * 0.5F;
      rect.left = (int)(f1 - f4);
      rect.right = (int)(f1 + f4);
      rect.top = (int)(f2 - f3);
      rect.bottom = (int)(f2 + f3);
      return rect;
    }
    
    private CharSequence getTextForVirtualView(int param1Int) {
      Resources resources = LockPatternView.this.getResources();
      return resources.getString(17040465, new Object[] { Integer.valueOf(param1Int) });
    }
    
    private int getVirtualViewIdForHit(float param1Float1, float param1Float2) {
      int i = LockPatternView.this.getRowHit(param1Float2);
      int j = Integer.MIN_VALUE;
      if (i < 0)
        return Integer.MIN_VALUE; 
      int k = LockPatternView.this.getColumnHit(param1Float1);
      if (k < 0)
        return Integer.MIN_VALUE; 
      boolean bool = LockPatternView.this.mPatternDrawLookup[i][k];
      if (bool)
        j = i * 3 + k + 1; 
      return j;
    }
  }
  
  class OnPatternListener {
    public abstract void onPatternCellAdded(List<LockPatternView.Cell> param1List);
    
    public abstract void onPatternCleared();
    
    public abstract void onPatternDetected(List<LockPatternView.Cell> param1List);
    
    public abstract void onPatternStart();
  }
}
