package com.android.internal.widget;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityNodeInfo;
import java.util.Arrays;

public class GridLayoutManager extends LinearLayoutManager {
  boolean mPendingSpanCountChange = false;
  
  int mSpanCount = -1;
  
  final SparseIntArray mPreLayoutSpanSizeCache = new SparseIntArray();
  
  final SparseIntArray mPreLayoutSpanIndexCache = new SparseIntArray();
  
  SpanSizeLookup mSpanSizeLookup = new DefaultSpanSizeLookup();
  
  final Rect mDecorInsets = new Rect();
  
  private static final boolean DEBUG = false;
  
  public static final int DEFAULT_SPAN_COUNT = -1;
  
  private static final String TAG = "GridLayoutManager";
  
  int[] mCachedBorders;
  
  View[] mSet;
  
  public GridLayoutManager(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    RecyclerView.LayoutManager.Properties properties = getProperties(paramContext, paramAttributeSet, paramInt1, paramInt2);
    setSpanCount(properties.spanCount);
  }
  
  public GridLayoutManager(Context paramContext, int paramInt) {
    super(paramContext);
    setSpanCount(paramInt);
  }
  
  public GridLayoutManager(Context paramContext, int paramInt1, int paramInt2, boolean paramBoolean) {
    super(paramContext, paramInt2, paramBoolean);
    setSpanCount(paramInt1);
  }
  
  public void setStackFromEnd(boolean paramBoolean) {
    if (!paramBoolean) {
      super.setStackFromEnd(false);
      return;
    } 
    throw new UnsupportedOperationException("GridLayoutManager does not support stack from end. Consider using reverse layout");
  }
  
  public int getRowCountForAccessibility(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState) {
    if (this.mOrientation == 0)
      return this.mSpanCount; 
    if (paramState.getItemCount() < 1)
      return 0; 
    return getSpanGroupIndex(paramRecycler, paramState, paramState.getItemCount() - 1) + 1;
  }
  
  public int getColumnCountForAccessibility(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState) {
    if (this.mOrientation == 1)
      return this.mSpanCount; 
    if (paramState.getItemCount() < 1)
      return 0; 
    return getSpanGroupIndex(paramRecycler, paramState, paramState.getItemCount() - 1) + 1;
  }
  
  public void onInitializeAccessibilityNodeInfoForItem(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, View paramView, AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    ViewGroup.LayoutParams layoutParams1 = paramView.getLayoutParams();
    if (!(layoutParams1 instanceof LayoutParams)) {
      onInitializeAccessibilityNodeInfoForItem(paramView, paramAccessibilityNodeInfo);
      return;
    } 
    LayoutParams layoutParams = (LayoutParams)layoutParams1;
    int i = getSpanGroupIndex(paramRecycler, paramState, layoutParams.getViewLayoutPosition());
    if (this.mOrientation == 0) {
      int j = layoutParams.getSpanIndex(), k = layoutParams.getSpanSize();
      paramAccessibilityNodeInfo.setCollectionItemInfo(AccessibilityNodeInfo.CollectionItemInfo.obtain(j, k, i, 1, false, false));
    } else {
      int j = layoutParams.getSpanIndex(), k = layoutParams.getSpanSize();
      paramAccessibilityNodeInfo.setCollectionItemInfo(AccessibilityNodeInfo.CollectionItemInfo.obtain(i, 1, j, k, false, false));
    } 
  }
  
  public void onLayoutChildren(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState) {
    if (paramState.isPreLayout())
      cachePreLayoutSpanMapping(); 
    super.onLayoutChildren(paramRecycler, paramState);
    clearPreLayoutSpanMappingCache();
  }
  
  public void onLayoutCompleted(RecyclerView.State paramState) {
    super.onLayoutCompleted(paramState);
    this.mPendingSpanCountChange = false;
  }
  
  private void clearPreLayoutSpanMappingCache() {
    this.mPreLayoutSpanSizeCache.clear();
    this.mPreLayoutSpanIndexCache.clear();
  }
  
  private void cachePreLayoutSpanMapping() {
    int i = getChildCount();
    for (byte b = 0; b < i; b++) {
      LayoutParams layoutParams = (LayoutParams)getChildAt(b).getLayoutParams();
      int j = layoutParams.getViewLayoutPosition();
      this.mPreLayoutSpanSizeCache.put(j, layoutParams.getSpanSize());
      this.mPreLayoutSpanIndexCache.put(j, layoutParams.getSpanIndex());
    } 
  }
  
  public void onItemsAdded(RecyclerView paramRecyclerView, int paramInt1, int paramInt2) {
    this.mSpanSizeLookup.invalidateSpanIndexCache();
  }
  
  public void onItemsChanged(RecyclerView paramRecyclerView) {
    this.mSpanSizeLookup.invalidateSpanIndexCache();
  }
  
  public void onItemsRemoved(RecyclerView paramRecyclerView, int paramInt1, int paramInt2) {
    this.mSpanSizeLookup.invalidateSpanIndexCache();
  }
  
  public void onItemsUpdated(RecyclerView paramRecyclerView, int paramInt1, int paramInt2, Object paramObject) {
    this.mSpanSizeLookup.invalidateSpanIndexCache();
  }
  
  public void onItemsMoved(RecyclerView paramRecyclerView, int paramInt1, int paramInt2, int paramInt3) {
    this.mSpanSizeLookup.invalidateSpanIndexCache();
  }
  
  public RecyclerView.LayoutParams generateDefaultLayoutParams() {
    if (this.mOrientation == 0)
      return new LayoutParams(-2, -1); 
    return new LayoutParams(-1, -2);
  }
  
  public RecyclerView.LayoutParams generateLayoutParams(Context paramContext, AttributeSet paramAttributeSet) {
    return new LayoutParams(paramContext, paramAttributeSet);
  }
  
  public RecyclerView.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    if (paramLayoutParams instanceof ViewGroup.MarginLayoutParams)
      return new LayoutParams((ViewGroup.MarginLayoutParams)paramLayoutParams); 
    return new LayoutParams(paramLayoutParams);
  }
  
  public boolean checkLayoutParams(RecyclerView.LayoutParams paramLayoutParams) {
    return paramLayoutParams instanceof LayoutParams;
  }
  
  public void setSpanSizeLookup(SpanSizeLookup paramSpanSizeLookup) {
    this.mSpanSizeLookup = paramSpanSizeLookup;
  }
  
  public SpanSizeLookup getSpanSizeLookup() {
    return this.mSpanSizeLookup;
  }
  
  private void updateMeasurements() {
    int i;
    if (getOrientation() == 1) {
      i = getWidth() - getPaddingRight() - getPaddingLeft();
    } else {
      i = getHeight() - getPaddingBottom() - getPaddingTop();
    } 
    calculateItemBorders(i);
  }
  
  public void setMeasuredDimension(Rect paramRect, int paramInt1, int paramInt2) {
    int[] arrayOfInt;
    if (this.mCachedBorders == null)
      super.setMeasuredDimension(paramRect, paramInt1, paramInt2); 
    int i = getPaddingLeft() + getPaddingRight();
    int j = getPaddingTop() + getPaddingBottom();
    if (this.mOrientation == 1) {
      int k = paramRect.height();
      paramInt2 = chooseSize(paramInt2, k + j, getMinimumHeight());
      arrayOfInt = this.mCachedBorders;
      j = arrayOfInt[arrayOfInt.length - 1];
      k = getMinimumWidth();
      paramInt1 = chooseSize(paramInt1, j + i, k);
    } else {
      int k = arrayOfInt.width();
      paramInt1 = chooseSize(paramInt1, k + i, getMinimumWidth());
      arrayOfInt = this.mCachedBorders;
      k = arrayOfInt[arrayOfInt.length - 1];
      i = getMinimumHeight();
      paramInt2 = chooseSize(paramInt2, k + j, i);
    } 
    setMeasuredDimension(paramInt1, paramInt2);
  }
  
  private void calculateItemBorders(int paramInt) {
    this.mCachedBorders = calculateItemBorders(this.mCachedBorders, this.mSpanCount, paramInt);
  }
  
  static int[] calculateItemBorders(int[] paramArrayOfint, int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: ifnull -> 24
    //   4: aload_0
    //   5: arraylength
    //   6: iload_1
    //   7: iconst_1
    //   8: iadd
    //   9: if_icmpne -> 24
    //   12: aload_0
    //   13: astore_3
    //   14: aload_0
    //   15: aload_0
    //   16: arraylength
    //   17: iconst_1
    //   18: isub
    //   19: iaload
    //   20: iload_2
    //   21: if_icmpeq -> 30
    //   24: iload_1
    //   25: iconst_1
    //   26: iadd
    //   27: newarray int
    //   29: astore_3
    //   30: aload_3
    //   31: iconst_0
    //   32: iconst_0
    //   33: iastore
    //   34: iload_2
    //   35: iload_1
    //   36: idiv
    //   37: istore #4
    //   39: iload_2
    //   40: iload_1
    //   41: irem
    //   42: istore #5
    //   44: iconst_0
    //   45: istore #6
    //   47: iconst_0
    //   48: istore_2
    //   49: iconst_1
    //   50: istore #7
    //   52: iload #7
    //   54: iload_1
    //   55: if_icmpgt -> 126
    //   58: iload #4
    //   60: istore #8
    //   62: iload_2
    //   63: iload #5
    //   65: iadd
    //   66: istore #9
    //   68: iload #9
    //   70: istore_2
    //   71: iload #8
    //   73: istore #10
    //   75: iload #9
    //   77: ifle -> 107
    //   80: iload #9
    //   82: istore_2
    //   83: iload #8
    //   85: istore #10
    //   87: iload_1
    //   88: iload #9
    //   90: isub
    //   91: iload #5
    //   93: if_icmpge -> 107
    //   96: iload #8
    //   98: iconst_1
    //   99: iadd
    //   100: istore #10
    //   102: iload #9
    //   104: iload_1
    //   105: isub
    //   106: istore_2
    //   107: iload #6
    //   109: iload #10
    //   111: iadd
    //   112: istore #6
    //   114: aload_3
    //   115: iload #7
    //   117: iload #6
    //   119: iastore
    //   120: iinc #7, 1
    //   123: goto -> 52
    //   126: aload_3
    //   127: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #319	-> 0
    //   #321	-> 24
    //   #323	-> 30
    //   #324	-> 34
    //   #325	-> 39
    //   #326	-> 44
    //   #327	-> 47
    //   #328	-> 49
    //   #329	-> 58
    //   #330	-> 62
    //   #331	-> 68
    //   #332	-> 96
    //   #333	-> 102
    //   #335	-> 107
    //   #336	-> 114
    //   #328	-> 120
    //   #338	-> 126
  }
  
  int getSpaceForSpanRange(int paramInt1, int paramInt2) {
    if (this.mOrientation == 1 && isLayoutRTL()) {
      int arrayOfInt1[] = this.mCachedBorders, i = this.mSpanCount;
      return arrayOfInt1[i - paramInt1] - arrayOfInt1[i - paramInt1 - paramInt2];
    } 
    int[] arrayOfInt = this.mCachedBorders;
    return arrayOfInt[paramInt1 + paramInt2] - arrayOfInt[paramInt1];
  }
  
  void onAnchorReady(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, LinearLayoutManager.AnchorInfo paramAnchorInfo, int paramInt) {
    super.onAnchorReady(paramRecycler, paramState, paramAnchorInfo, paramInt);
    updateMeasurements();
    if (paramState.getItemCount() > 0 && !paramState.isPreLayout())
      ensureAnchorIsInCorrectSpan(paramRecycler, paramState, paramAnchorInfo, paramInt); 
    ensureViewSet();
  }
  
  private void ensureViewSet() {
    View[] arrayOfView = this.mSet;
    if (arrayOfView == null || arrayOfView.length != this.mSpanCount)
      this.mSet = new View[this.mSpanCount]; 
  }
  
  public int scrollHorizontallyBy(int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState) {
    updateMeasurements();
    ensureViewSet();
    return super.scrollHorizontallyBy(paramInt, paramRecycler, paramState);
  }
  
  public int scrollVerticallyBy(int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState) {
    updateMeasurements();
    ensureViewSet();
    return super.scrollVerticallyBy(paramInt, paramRecycler, paramState);
  }
  
  private void ensureAnchorIsInCorrectSpan(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, LinearLayoutManager.AnchorInfo paramAnchorInfo, int paramInt) {
    int i;
    if (paramInt == 1) {
      i = 1;
    } else {
      i = 0;
    } 
    paramInt = getSpanIndex(paramRecycler, paramState, paramAnchorInfo.mPosition);
    if (i) {
      while (paramInt > 0 && paramAnchorInfo.mPosition > 0) {
        paramAnchorInfo.mPosition--;
        paramInt = getSpanIndex(paramRecycler, paramState, paramAnchorInfo.mPosition);
      } 
    } else {
      int j = paramState.getItemCount();
      i = paramAnchorInfo.mPosition;
      while (i < j - 1) {
        int k = getSpanIndex(paramRecycler, paramState, i + 1);
        if (k > paramInt) {
          i++;
          paramInt = k;
        } 
      } 
      paramAnchorInfo.mPosition = i;
    } 
  }
  
  View findReferenceChild(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, int paramInt1, int paramInt2, int paramInt3) {
    View view1;
    byte b;
    ensureLayoutState();
    View view2 = null;
    OrientationHelper orientationHelper = null;
    int i = this.mOrientationHelper.getStartAfterPadding();
    int j = this.mOrientationHelper.getEndAfterPadding();
    if (paramInt2 > paramInt1) {
      b = 1;
    } else {
      b = -1;
    } 
    View view3;
    for (; paramInt1 != paramInt2; paramInt1 += b, view2 = view5, view3 = view6) {
      View view6, view4 = getChildAt(paramInt1);
      int k = getPosition(view4);
      View view5 = view2;
      OrientationHelper orientationHelper1 = orientationHelper;
      if (k >= 0) {
        view5 = view2;
        orientationHelper1 = orientationHelper;
        if (k < paramInt3) {
          k = getSpanIndex(paramRecycler, paramState, k);
          if (k != 0) {
            view5 = view2;
            orientationHelper1 = orientationHelper;
          } else if (((RecyclerView.LayoutParams)view4.getLayoutParams()).isItemRemoved()) {
            view5 = view2;
            orientationHelper1 = orientationHelper;
            if (view2 == null) {
              view5 = view4;
              orientationHelper1 = orientationHelper;
            } 
          } else {
            if (this.mOrientationHelper.getDecoratedStart(view4) < j) {
              orientationHelper1 = this.mOrientationHelper;
              if (orientationHelper1.getDecoratedEnd(view4) >= i)
                return view4; 
            } 
            view5 = view2;
            orientationHelper1 = orientationHelper;
            if (orientationHelper == null) {
              view6 = view4;
              view5 = view2;
            } 
          } 
        } 
      } 
    } 
    if (view3 != null) {
      view1 = view3;
    } else {
      view1 = view2;
    } 
    return view1;
  }
  
  private int getSpanGroupIndex(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, int paramInt) {
    if (!paramState.isPreLayout())
      return this.mSpanSizeLookup.getSpanGroupIndex(paramInt, this.mSpanCount); 
    int i = paramRecycler.convertPreLayoutPositionToPostLayout(paramInt);
    if (i == -1) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Cannot find span size for pre layout position. ");
      stringBuilder.append(paramInt);
      Log.w("GridLayoutManager", stringBuilder.toString());
      return 0;
    } 
    return this.mSpanSizeLookup.getSpanGroupIndex(i, this.mSpanCount);
  }
  
  private int getSpanIndex(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, int paramInt) {
    if (!paramState.isPreLayout())
      return this.mSpanSizeLookup.getCachedSpanIndex(paramInt, this.mSpanCount); 
    int i = this.mPreLayoutSpanIndexCache.get(paramInt, -1);
    if (i != -1)
      return i; 
    i = paramRecycler.convertPreLayoutPositionToPostLayout(paramInt);
    if (i == -1) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:");
      stringBuilder.append(paramInt);
      Log.w("GridLayoutManager", stringBuilder.toString());
      return 0;
    } 
    return this.mSpanSizeLookup.getCachedSpanIndex(i, this.mSpanCount);
  }
  
  private int getSpanSize(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, int paramInt) {
    if (!paramState.isPreLayout())
      return this.mSpanSizeLookup.getSpanSize(paramInt); 
    int i = this.mPreLayoutSpanSizeCache.get(paramInt, -1);
    if (i != -1)
      return i; 
    i = paramRecycler.convertPreLayoutPositionToPostLayout(paramInt);
    if (i == -1) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:");
      stringBuilder.append(paramInt);
      Log.w("GridLayoutManager", stringBuilder.toString());
      return 1;
    } 
    return this.mSpanSizeLookup.getSpanSize(i);
  }
  
  void collectPrefetchPositionsForLayoutState(RecyclerView.State paramState, LinearLayoutManager.LayoutState paramLayoutState, RecyclerView.LayoutManager.LayoutPrefetchRegistry paramLayoutPrefetchRegistry) {
    int i = this.mSpanCount;
    byte b = 0;
    while (b < this.mSpanCount && paramLayoutState.hasMore(paramState) && i > 0) {
      int j = paramLayoutState.mCurrentPosition;
      paramLayoutPrefetchRegistry.addPosition(j, Math.max(0, paramLayoutState.mScrollingOffset));
      j = this.mSpanSizeLookup.getSpanSize(j);
      i -= j;
      paramLayoutState.mCurrentPosition += paramLayoutState.mItemDirection;
      b++;
    } 
  }
  
  void layoutChunk(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, LinearLayoutManager.LayoutState paramLayoutState, LinearLayoutManager.LayoutChunkResult paramLayoutChunkResult) {
    StringBuilder stringBuilder;
    int k;
    boolean bool;
    byte b;
    int i = this.mOrientationHelper.getModeInOther();
    if (i != 1073741824) {
      j = 1;
    } else {
      j = 0;
    } 
    if (getChildCount() > 0) {
      k = this.mCachedBorders[this.mSpanCount];
    } else {
      k = 0;
    } 
    if (j)
      updateMeasurements(); 
    if (paramLayoutState.mItemDirection == 1) {
      bool = true;
    } else {
      bool = false;
    } 
    int m = this.mSpanCount;
    if (!bool) {
      m = getSpanIndex(paramRecycler, paramState, paramLayoutState.mCurrentPosition);
      n = getSpanSize(paramRecycler, paramState, paramLayoutState.mCurrentPosition);
      m += n;
      b = 0;
      n = 0;
    } else {
      b = 0;
      n = 0;
    } 
    while (true) {
      int i3 = m;
      if (b < this.mSpanCount) {
        i3 = m;
        if (paramLayoutState.hasMore(paramState)) {
          i3 = m;
          if (m > 0) {
            int i4 = paramLayoutState.mCurrentPosition;
            i3 = getSpanSize(paramRecycler, paramState, i4);
            if (i3 <= this.mSpanCount) {
              m -= i3;
              if (m < 0) {
                i3 = m;
                break;
              } 
              View view = paramLayoutState.next(paramRecycler);
              if (view == null) {
                i3 = m;
                break;
              } 
              n += i3;
              this.mSet[b] = view;
              b++;
              continue;
            } 
            stringBuilder = new StringBuilder();
            stringBuilder.append("Item at position ");
            stringBuilder.append(i4);
            stringBuilder.append(" requires ");
            stringBuilder.append(i3);
            stringBuilder.append(" spans but GridLayoutManager has only ");
            stringBuilder.append(this.mSpanCount);
            stringBuilder.append(" spans.");
            throw new IllegalArgumentException(stringBuilder.toString());
          } 
        } 
      } 
      break;
    } 
    if (b == 0) {
      paramLayoutChunkResult.mFinished = true;
      return;
    } 
    assignSpans((RecyclerView.Recycler)stringBuilder, paramState, b, n, bool);
    int i2;
    float f;
    for (i2 = 0, m = 0, f = 0.0F; i2 < b; i2++, m = n, f = f2) {
      View view = this.mSet[i2];
      if (paramLayoutState.mScrapList == null) {
        if (bool) {
          addView(view);
        } else {
          addView(view, 0);
        } 
      } else if (bool) {
        addDisappearingView(view);
      } else {
        addDisappearingView(view, 0);
      } 
      calculateItemDecorationsForChild(view, this.mDecorInsets);
      measureChild(view, i, false);
      int i3 = this.mOrientationHelper.getDecoratedMeasurement(view);
      n = m;
      if (i3 > m)
        n = i3; 
      LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
      float f1 = this.mOrientationHelper.getDecoratedMeasurementInOther(view) * 1.0F / layoutParams.mSpanSize;
      float f2 = f;
      if (f1 > f)
        f2 = f1; 
    } 
    if (j) {
      guessMeasurement(f, k);
      m = 0;
      for (n = 0; n < b; n++, m = j) {
        View view = this.mSet[n];
        measureChild(view, 1073741824, true);
        k = this.mOrientationHelper.getDecoratedMeasurement(view);
        j = m;
        if (k > m)
          j = k; 
      } 
      k = m;
    } else {
      k = m;
    } 
    int n;
    for (n = 0, m = i; n < b; n++) {
      View view = this.mSet[n];
      if (this.mOrientationHelper.getDecoratedMeasurement(view) != k) {
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        Rect rect = layoutParams.mDecorInsets;
        i2 = rect.top + rect.bottom + layoutParams.topMargin + layoutParams.bottomMargin;
        j = rect.left + rect.right + layoutParams.leftMargin + layoutParams.rightMargin;
        i = getSpaceForSpanRange(layoutParams.mSpanIndex, layoutParams.mSpanSize);
        if (this.mOrientation == 1) {
          j = getChildMeasureSpec(i, 1073741824, j, layoutParams.width, false);
          i2 = View.MeasureSpec.makeMeasureSpec(k - i2, 1073741824);
        } else {
          j = View.MeasureSpec.makeMeasureSpec(k - j, 1073741824);
          i2 = getChildMeasureSpec(i, 1073741824, i2, layoutParams.height, false);
        } 
        measureChildWithDecorationsAndMargin(view, j, i2, true);
      } 
    } 
    paramLayoutChunkResult.mConsumed = k;
    n = 0;
    int j = 0, i1 = 0;
    m = 0;
    if (this.mOrientation == 1) {
      if (paramLayoutState.mLayoutDirection == -1) {
        m = paramLayoutState.mOffset;
        i1 = m - k;
      } else {
        i1 = paramLayoutState.mOffset;
        m = i1 + k;
      } 
    } else if (paramLayoutState.mLayoutDirection == -1) {
      j = paramLayoutState.mOffset;
      n = j - k;
    } else {
      n = paramLayoutState.mOffset;
      j = n + k;
    } 
    for (i2 = 0; i2 < b; i = i2 + 1, i2 = n, n = i1, i1 = i2, i2 = i) {
      View view = this.mSet[i2];
      LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
      if (this.mOrientation == 1) {
        if (isLayoutRTL()) {
          j = getPaddingLeft() + this.mCachedBorders[this.mSpanCount - layoutParams.mSpanIndex];
          i = j - this.mOrientationHelper.getDecoratedMeasurementInOther(view);
          n = i1;
          i1 = i;
        } else {
          n = getPaddingLeft() + this.mCachedBorders[layoutParams.mSpanIndex];
          i = this.mOrientationHelper.getDecoratedMeasurementInOther(view);
          j = n;
          i += n;
          n = i1;
          i1 = j;
          j = i;
        } 
      } else {
        m = getPaddingTop() + this.mCachedBorders[layoutParams.mSpanIndex];
        i1 = this.mOrientationHelper.getDecoratedMeasurementInOther(view);
        i = m;
        m = i1 + m;
        i1 = n;
        n = i;
      } 
      layoutDecoratedWithMargins(view, i1, n, j, m);
      if (layoutParams.isItemRemoved() || layoutParams.isItemChanged())
        paramLayoutChunkResult.mIgnoreConsumed = true; 
      paramLayoutChunkResult.mFocusable |= view.hasFocusable();
    } 
    Arrays.fill((Object[])this.mSet, (Object)null);
  }
  
  private void measureChild(View paramView, int paramInt, boolean paramBoolean) {
    LayoutParams layoutParams = (LayoutParams)paramView.getLayoutParams();
    Rect rect = layoutParams.mDecorInsets;
    int i = rect.top + rect.bottom + layoutParams.topMargin + layoutParams.bottomMargin;
    int j = rect.left + rect.right + layoutParams.leftMargin + layoutParams.rightMargin;
    int k = getSpaceForSpanRange(layoutParams.mSpanIndex, layoutParams.mSpanSize);
    if (this.mOrientation == 1) {
      j = getChildMeasureSpec(k, paramInt, j, layoutParams.width, false);
      paramInt = getChildMeasureSpec(this.mOrientationHelper.getTotalSpace(), getHeightMode(), i, layoutParams.height, true);
    } else {
      paramInt = getChildMeasureSpec(k, paramInt, i, layoutParams.height, false);
      j = getChildMeasureSpec(this.mOrientationHelper.getTotalSpace(), getWidthMode(), j, layoutParams.width, true);
    } 
    measureChildWithDecorationsAndMargin(paramView, j, paramInt, paramBoolean);
  }
  
  private void guessMeasurement(float paramFloat, int paramInt) {
    int i = Math.round(this.mSpanCount * paramFloat);
    calculateItemBorders(Math.max(i, paramInt));
  }
  
  private void measureChildWithDecorationsAndMargin(View paramView, int paramInt1, int paramInt2, boolean paramBoolean) {
    RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams)paramView.getLayoutParams();
    if (paramBoolean) {
      paramBoolean = shouldReMeasureChild(paramView, paramInt1, paramInt2, layoutParams);
    } else {
      paramBoolean = shouldMeasureChild(paramView, paramInt1, paramInt2, layoutParams);
    } 
    if (paramBoolean)
      paramView.measure(paramInt1, paramInt2); 
  }
  
  private void assignSpans(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, int paramInt1, int paramInt2, boolean paramBoolean) {
    byte b;
    if (paramBoolean) {
      boolean bool = false;
      b = paramInt1;
      paramInt2 = 1;
      paramInt1 = bool;
    } else {
      paramInt1--;
      b = -1;
      paramInt2 = -1;
    } 
    int i = 0;
    for (; paramInt1 != b; paramInt1 += paramInt2) {
      View view = this.mSet[paramInt1];
      LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
      layoutParams.mSpanSize = getSpanSize(paramRecycler, paramState, getPosition(view));
      layoutParams.mSpanIndex = i;
      i += layoutParams.mSpanSize;
    } 
  }
  
  public int getSpanCount() {
    return this.mSpanCount;
  }
  
  public void setSpanCount(int paramInt) {
    if (paramInt == this.mSpanCount)
      return; 
    this.mPendingSpanCountChange = true;
    if (paramInt >= 1) {
      this.mSpanCount = paramInt;
      this.mSpanSizeLookup.invalidateSpanIndexCache();
      requestLayout();
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Span count should be at least 1. Provided ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  class SpanSizeLookup {
    final SparseIntArray mSpanIndexCache = new SparseIntArray();
    
    private boolean mCacheSpanIndices = false;
    
    public void setSpanIndexCacheEnabled(boolean param1Boolean) {
      this.mCacheSpanIndices = param1Boolean;
    }
    
    public void invalidateSpanIndexCache() {
      this.mSpanIndexCache.clear();
    }
    
    public boolean isSpanIndexCacheEnabled() {
      return this.mCacheSpanIndices;
    }
    
    int getCachedSpanIndex(int param1Int1, int param1Int2) {
      if (!this.mCacheSpanIndices)
        return getSpanIndex(param1Int1, param1Int2); 
      int i = this.mSpanIndexCache.get(param1Int1, -1);
      if (i != -1)
        return i; 
      param1Int2 = getSpanIndex(param1Int1, param1Int2);
      this.mSpanIndexCache.put(param1Int1, param1Int2);
      return param1Int2;
    }
    
    public int getSpanIndex(int param1Int1, int param1Int2) {
      int i = getSpanSize(param1Int1);
      if (i == param1Int2)
        return 0; 
      int j = 0;
      int k = 0;
      int m = j, n = k;
      if (this.mCacheSpanIndices) {
        m = j;
        n = k;
        if (this.mSpanIndexCache.size() > 0) {
          int i1 = findReferenceIndexFromCache(param1Int1);
          m = j;
          n = k;
          if (i1 >= 0) {
            m = this.mSpanIndexCache.get(i1) + getSpanSize(i1);
            n = i1 + 1;
          } 
        } 
      } 
      for (; n < param1Int1; n++) {
        j = getSpanSize(n);
        k = m + j;
        if (k == param1Int2) {
          m = 0;
        } else {
          m = k;
          if (k > param1Int2)
            m = j; 
        } 
      } 
      if (m + i <= param1Int2)
        return m; 
      return 0;
    }
    
    int findReferenceIndexFromCache(int param1Int) {
      int i = 0;
      int j = this.mSpanIndexCache.size() - 1;
      while (i <= j) {
        int k = i + j >>> 1;
        int m = this.mSpanIndexCache.keyAt(k);
        if (m < param1Int) {
          i = k + 1;
          continue;
        } 
        j = k - 1;
      } 
      param1Int = i - 1;
      if (param1Int >= 0 && param1Int < this.mSpanIndexCache.size())
        return this.mSpanIndexCache.keyAt(param1Int); 
      return -1;
    }
    
    public int getSpanGroupIndex(int param1Int1, int param1Int2) {
      int i = 0;
      int j = 0;
      int k = getSpanSize(param1Int1);
      for (byte b = 0; b < param1Int1; b++, j = i1) {
        int i1, m = getSpanSize(b);
        int n = i + m;
        if (n == param1Int2) {
          i = 0;
          i1 = j + 1;
        } else {
          i = n;
          i1 = j;
          if (n > param1Int2) {
            i = m;
            i1 = j + 1;
          } 
        } 
      } 
      param1Int1 = j;
      if (i + k > param1Int2)
        param1Int1 = j + 1; 
      return param1Int1;
    }
    
    public abstract int getSpanSize(int param1Int);
  }
  
  public boolean supportsPredictiveItemAnimations() {
    boolean bool;
    if (this.mPendingSavedState == null && !this.mPendingSpanCountChange) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  class DefaultSpanSizeLookup extends SpanSizeLookup {
    public int getSpanSize(int param1Int) {
      return 1;
    }
    
    public int getSpanIndex(int param1Int1, int param1Int2) {
      return param1Int1 % param1Int2;
    }
  }
  
  class LayoutParams extends RecyclerView.LayoutParams {
    public static final int INVALID_SPAN_ID = -1;
    
    int mSpanIndex = -1;
    
    int mSpanSize = 0;
    
    public LayoutParams(GridLayoutManager this$0, AttributeSet param1AttributeSet) {
      super((Context)this$0, param1AttributeSet);
    }
    
    public LayoutParams(GridLayoutManager this$0, int param1Int1) {
      super(this$0, param1Int1);
    }
    
    public LayoutParams(GridLayoutManager this$0) {
      super((ViewGroup.MarginLayoutParams)this$0);
    }
    
    public LayoutParams(GridLayoutManager this$0) {
      super((ViewGroup.LayoutParams)this$0);
    }
    
    public LayoutParams(GridLayoutManager this$0) {
      super((RecyclerView.LayoutParams)this$0);
    }
    
    public int getSpanIndex() {
      return this.mSpanIndex;
    }
    
    public int getSpanSize() {
      return this.mSpanSize;
    }
  }
}
