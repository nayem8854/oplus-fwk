package com.android.internal.widget;

import android.content.Context;
import android.graphics.Rect;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.view.Window;
import com.android.internal.policy.DecorView;
import com.android.internal.policy.PhoneWindow;
import java.util.ArrayList;

public class DecorCaptionView extends ViewGroup implements View.OnTouchListener, GestureDetector.OnGestureListener {
  private PhoneWindow mOwner = null;
  
  private boolean mShow = false;
  
  private boolean mDragging = false;
  
  private boolean mOverlayWithAppContent = false;
  
  private ArrayList<View> mTouchDispatchList = new ArrayList<>(2);
  
  private final Rect mCloseRect = new Rect();
  
  private final Rect mMaximizeRect = new Rect();
  
  private static final String TAG = "DecorCaptionView";
  
  private View mCaption;
  
  private boolean mCheckForDragging;
  
  private View mClickTarget;
  
  private View mClose;
  
  private View mContent;
  
  private int mDragSlop;
  
  private GestureDetector mGestureDetector;
  
  private View mMaximize;
  
  private int mRootScrollY;
  
  private int mTouchDownX;
  
  private int mTouchDownY;
  
  public DecorCaptionView(Context paramContext) {
    super(paramContext);
    init(paramContext);
  }
  
  public DecorCaptionView(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    init(paramContext);
  }
  
  public DecorCaptionView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramContext);
  }
  
  private void init(Context paramContext) {
    this.mDragSlop = ViewConfiguration.get(paramContext).getScaledTouchSlop();
    this.mGestureDetector = new GestureDetector(paramContext, this);
    CharSequence charSequence = paramContext.getPackageManager().getApplicationLabel(paramContext.getApplicationInfo());
    setContentDescription(paramContext.getString(17039551, new Object[] { charSequence }));
  }
  
  protected void onFinishInflate() {
    super.onFinishInflate();
    this.mCaption = getChildAt(0);
  }
  
  public void setPhoneWindow(PhoneWindow paramPhoneWindow, boolean paramBoolean) {
    this.mOwner = paramPhoneWindow;
    this.mShow = paramBoolean;
    this.mOverlayWithAppContent = paramPhoneWindow.isOverlayWithDecorCaptionEnabled();
    updateCaptionVisibility();
    this.mOwner.getDecorView().setOutlineProvider(ViewOutlineProvider.BOUNDS);
    this.mMaximize = findViewById(16909149);
    this.mClose = findViewById(16908852);
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent) {
    boolean bool;
    if (paramMotionEvent.getAction() == 0) {
      int i = (int)paramMotionEvent.getX();
      int j = (int)paramMotionEvent.getY();
      if (this.mMaximizeRect.contains(i, j - this.mRootScrollY))
        this.mClickTarget = this.mMaximize; 
      if (this.mCloseRect.contains(i, j - this.mRootScrollY))
        this.mClickTarget = this.mClose; 
    } 
    if (this.mClickTarget != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    if (this.mClickTarget != null) {
      this.mGestureDetector.onTouchEvent(paramMotionEvent);
      int i = paramMotionEvent.getAction();
      if (i == 1 || i == 3)
        this.mClickTarget = null; 
      return true;
    } 
    return false;
  }
  
  public boolean onTouch(View paramView, MotionEvent paramMotionEvent) {
    // Byte code:
    //   0: aload_2
    //   1: invokevirtual getX : ()F
    //   4: f2i
    //   5: istore_3
    //   6: aload_2
    //   7: invokevirtual getY : ()F
    //   10: f2i
    //   11: istore #4
    //   13: aload_2
    //   14: aload_2
    //   15: invokevirtual getActionIndex : ()I
    //   18: invokevirtual getToolType : (I)I
    //   21: istore #5
    //   23: iconst_0
    //   24: istore #6
    //   26: iload #5
    //   28: iconst_3
    //   29: if_icmpne -> 38
    //   32: iconst_1
    //   33: istore #5
    //   35: goto -> 41
    //   38: iconst_0
    //   39: istore #5
    //   41: aload_2
    //   42: invokevirtual getButtonState : ()I
    //   45: iconst_1
    //   46: iand
    //   47: ifeq -> 56
    //   50: iconst_1
    //   51: istore #7
    //   53: goto -> 59
    //   56: iconst_0
    //   57: istore #7
    //   59: aload_2
    //   60: invokevirtual getActionMasked : ()I
    //   63: istore #8
    //   65: iload #8
    //   67: ifeq -> 178
    //   70: iload #8
    //   72: iconst_1
    //   73: if_icmpeq -> 146
    //   76: iload #8
    //   78: iconst_2
    //   79: if_icmpeq -> 91
    //   82: iload #8
    //   84: iconst_3
    //   85: if_icmpeq -> 146
    //   88: goto -> 213
    //   91: aload_0
    //   92: getfield mDragging : Z
    //   95: ifne -> 213
    //   98: aload_0
    //   99: getfield mCheckForDragging : Z
    //   102: ifeq -> 213
    //   105: iload #5
    //   107: ifne -> 120
    //   110: aload_0
    //   111: iload_3
    //   112: iload #4
    //   114: invokespecial passedSlop : (II)Z
    //   117: ifeq -> 213
    //   120: aload_0
    //   121: iconst_0
    //   122: putfield mCheckForDragging : Z
    //   125: aload_0
    //   126: iconst_1
    //   127: putfield mDragging : Z
    //   130: aload_0
    //   131: aload_2
    //   132: invokevirtual getRawX : ()F
    //   135: aload_2
    //   136: invokevirtual getRawY : ()F
    //   139: invokevirtual startMovingTask : (FF)Z
    //   142: pop
    //   143: goto -> 213
    //   146: aload_0
    //   147: getfield mDragging : Z
    //   150: ifne -> 156
    //   153: goto -> 213
    //   156: iload #8
    //   158: iconst_1
    //   159: if_icmpne -> 166
    //   162: aload_0
    //   163: invokevirtual finishMovingTask : ()V
    //   166: aload_0
    //   167: iconst_0
    //   168: putfield mDragging : Z
    //   171: aload_0
    //   172: getfield mCheckForDragging : Z
    //   175: iconst_1
    //   176: ixor
    //   177: ireturn
    //   178: aload_0
    //   179: getfield mShow : Z
    //   182: ifne -> 187
    //   185: iconst_0
    //   186: ireturn
    //   187: iload #5
    //   189: ifeq -> 197
    //   192: iload #7
    //   194: ifeq -> 213
    //   197: aload_0
    //   198: iconst_1
    //   199: putfield mCheckForDragging : Z
    //   202: aload_0
    //   203: iload_3
    //   204: putfield mTouchDownX : I
    //   207: aload_0
    //   208: iload #4
    //   210: putfield mTouchDownY : I
    //   213: aload_0
    //   214: getfield mDragging : Z
    //   217: ifne -> 227
    //   220: aload_0
    //   221: getfield mCheckForDragging : Z
    //   224: ifeq -> 230
    //   227: iconst_1
    //   228: istore #6
    //   230: iload #6
    //   232: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #181	-> 0
    //   #182	-> 6
    //   #183	-> 13
    //   #184	-> 41
    //   #185	-> 59
    //   #186	-> 65
    //   #202	-> 91
    //   #203	-> 120
    //   #204	-> 125
    //   #205	-> 130
    //   #214	-> 146
    //   #215	-> 153
    //   #218	-> 156
    //   #222	-> 162
    //   #224	-> 166
    //   #225	-> 171
    //   #188	-> 178
    //   #190	-> 185
    //   #194	-> 187
    //   #195	-> 197
    //   #196	-> 202
    //   #197	-> 207
    //   #227	-> 213
  }
  
  public boolean shouldDelayChildPressedState() {
    return false;
  }
  
  private boolean passedSlop(int paramInt1, int paramInt2) {
    return (Math.abs(paramInt1 - this.mTouchDownX) > this.mDragSlop || Math.abs(paramInt2 - this.mTouchDownY) > this.mDragSlop);
  }
  
  public void onConfigurationChanged(boolean paramBoolean) {
    this.mShow = paramBoolean;
    updateCaptionVisibility();
  }
  
  public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams) {
    if (paramLayoutParams instanceof ViewGroup.MarginLayoutParams) {
      if (paramInt < 2 && getChildCount() < 2) {
        super.addView(paramView, 0, paramLayoutParams);
        this.mContent = paramView;
        return;
      } 
      throw new IllegalStateException("DecorCaptionView can only handle 1 client view");
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("params ");
    stringBuilder.append(paramLayoutParams);
    stringBuilder.append(" must subclass MarginLayoutParams");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    boolean bool;
    if (this.mCaption.getVisibility() != 8) {
      measureChildWithMargins(this.mCaption, paramInt1, 0, paramInt2, 0);
      bool = this.mCaption.getMeasuredHeight();
    } else {
      bool = false;
    } 
    View view = this.mContent;
    if (view != null)
      if (this.mOverlayWithAppContent) {
        measureChildWithMargins(view, paramInt1, 0, paramInt2, 0);
      } else {
        measureChildWithMargins(view, paramInt1, 0, paramInt2, bool);
      }  
    paramInt1 = View.MeasureSpec.getSize(paramInt1);
    paramInt2 = View.MeasureSpec.getSize(paramInt2);
    setMeasuredDimension(paramInt1, paramInt2);
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (this.mCaption.getVisibility() != 8) {
      View view = this.mCaption;
      view.layout(0, 0, view.getMeasuredWidth(), this.mCaption.getMeasuredHeight());
      paramInt1 = this.mCaption.getBottom() - this.mCaption.getTop();
      this.mMaximize.getHitRect(this.mMaximizeRect);
      this.mClose.getHitRect(this.mCloseRect);
    } else {
      paramInt1 = 0;
      this.mMaximizeRect.setEmpty();
      this.mCloseRect.setEmpty();
    } 
    View view1 = this.mContent;
    if (view1 != null)
      if (this.mOverlayWithAppContent) {
        view1.layout(0, 0, view1.getMeasuredWidth(), this.mContent.getMeasuredHeight());
      } else {
        paramInt3 = view1.getMeasuredWidth();
        View view = this.mContent;
        paramInt2 = view.getMeasuredHeight();
        view1.layout(0, paramInt1, paramInt3, paramInt2 + paramInt1);
      }  
    ((DecorView)this.mOwner.getDecorView()).notifyCaptionHeightChanged();
    PhoneWindow phoneWindow = this.mOwner;
    paramInt3 = this.mMaximize.getLeft();
    paramInt1 = this.mMaximize.getTop();
    View view2 = this.mClose;
    paramInt2 = view2.getRight();
    paramInt4 = this.mClose.getBottom();
    phoneWindow.notifyRestrictedCaptionAreaCallback(paramInt3, paramInt1, paramInt2, paramInt4);
  }
  
  private void updateCaptionVisibility() {
    byte b;
    View view = this.mCaption;
    if (this.mShow) {
      b = 0;
    } else {
      b = 8;
    } 
    view.setVisibility(b);
    this.mCaption.setOnTouchListener(this);
  }
  
  private void toggleFreeformWindowingMode() {
    Window.WindowControllerCallback windowControllerCallback = this.mOwner.getWindowControllerCallback();
    if (windowControllerCallback != null)
      try {
        windowControllerCallback.toggleFreeformWindowingMode();
      } catch (RemoteException remoteException) {
        Log.e("DecorCaptionView", "Cannot change task workspace.");
      }  
  }
  
  public boolean isCaptionShowing() {
    return this.mShow;
  }
  
  public int getCaptionHeight() {
    boolean bool;
    View view = this.mCaption;
    if (view != null) {
      bool = view.getHeight();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void removeContentView() {
    View view = this.mContent;
    if (view != null) {
      removeView(view);
      this.mContent = null;
    } 
  }
  
  public View getCaption() {
    return this.mCaption;
  }
  
  public ViewGroup.LayoutParams generateLayoutParams(AttributeSet paramAttributeSet) {
    return (ViewGroup.LayoutParams)new ViewGroup.MarginLayoutParams(getContext(), paramAttributeSet);
  }
  
  protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
    return (ViewGroup.LayoutParams)new ViewGroup.MarginLayoutParams(-1, -1);
  }
  
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    return (ViewGroup.LayoutParams)new ViewGroup.MarginLayoutParams(paramLayoutParams);
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    return paramLayoutParams instanceof ViewGroup.MarginLayoutParams;
  }
  
  public boolean onDown(MotionEvent paramMotionEvent) {
    return false;
  }
  
  public void onShowPress(MotionEvent paramMotionEvent) {}
  
  public boolean onSingleTapUp(MotionEvent paramMotionEvent) {
    View view = this.mClickTarget;
    if (view == this.mMaximize) {
      toggleFreeformWindowingMode();
    } else if (view == this.mClose) {
      this.mOwner.dispatchOnWindowDismissed(true, false);
    } 
    return true;
  }
  
  public boolean onScroll(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2) {
    return false;
  }
  
  public void onLongPress(MotionEvent paramMotionEvent) {}
  
  public boolean onFling(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2) {
    return false;
  }
  
  public void onRootViewScrollYChanged(int paramInt) {
    View view = this.mCaption;
    if (view != null) {
      this.mRootScrollY = paramInt;
      view.setTranslationY(paramInt);
    } 
  }
}
