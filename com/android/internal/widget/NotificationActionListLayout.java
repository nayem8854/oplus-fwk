package com.android.internal.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.RippleDrawable;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.Gravity;
import android.view.RemotableViewMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RemoteViews.RemoteView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Comparator;

@RemoteView
public class NotificationActionListLayout extends OplusBaseNotificationActionListLayout {
  private int mTotalWidth = 0;
  
  private int mRegularHeight;
  
  private ArrayList<Pair<Integer, TextView>> mMeasureOrderTextViews = new ArrayList<>();
  
  private ArrayList<View> mMeasureOrderOther = new ArrayList<>();
  
  private final int mGravity;
  
  private boolean mEmphasizedMode;
  
  private int mEmphasizedHeight;
  
  private int mDefaultPaddingTop;
  
  private int mDefaultPaddingBottom;
  
  public NotificationActionListLayout(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public NotificationActionListLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public NotificationActionListLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, new int[] { 16842927 }, paramInt1, paramInt2);
    this.mGravity = typedArray.getInt(0, 0);
    typedArray.recycle();
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mEmphasizedMode : Z
    //   4: ifeq -> 14
    //   7: aload_0
    //   8: iload_1
    //   9: iload_2
    //   10: invokespecial onMeasure : (II)V
    //   13: return
    //   14: aload_0
    //   15: invokevirtual getChildCount : ()I
    //   18: istore_3
    //   19: iconst_0
    //   20: istore #4
    //   22: iconst_0
    //   23: istore #5
    //   25: iconst_0
    //   26: istore #6
    //   28: iconst_0
    //   29: istore #7
    //   31: iload #4
    //   33: iload_3
    //   34: if_icmpge -> 92
    //   37: aload_0
    //   38: iload #4
    //   40: invokevirtual getChildAt : (I)Landroid/view/View;
    //   43: astore #8
    //   45: aload #8
    //   47: instanceof android/widget/TextView
    //   50: ifeq -> 59
    //   53: iinc #5, 1
    //   56: goto -> 62
    //   59: iinc #6, 1
    //   62: iload #7
    //   64: istore #9
    //   66: aload #8
    //   68: invokevirtual getVisibility : ()I
    //   71: bipush #8
    //   73: if_icmpeq -> 82
    //   76: iload #7
    //   78: iconst_1
    //   79: iadd
    //   80: istore #9
    //   82: iinc #4, 1
    //   85: iload #9
    //   87: istore #7
    //   89: goto -> 31
    //   92: iconst_0
    //   93: istore #4
    //   95: iload #5
    //   97: aload_0
    //   98: getfield mMeasureOrderTextViews : Ljava/util/ArrayList;
    //   101: invokevirtual size : ()I
    //   104: if_icmpne -> 123
    //   107: aload_0
    //   108: getfield mMeasureOrderOther : Ljava/util/ArrayList;
    //   111: astore #8
    //   113: iload #6
    //   115: aload #8
    //   117: invokevirtual size : ()I
    //   120: if_icmpeq -> 126
    //   123: iconst_1
    //   124: istore #4
    //   126: iload #4
    //   128: ifne -> 206
    //   131: aload_0
    //   132: getfield mMeasureOrderTextViews : Ljava/util/ArrayList;
    //   135: invokevirtual size : ()I
    //   138: istore #10
    //   140: iconst_0
    //   141: istore #9
    //   143: iload #9
    //   145: iload #10
    //   147: if_icmpge -> 203
    //   150: aload_0
    //   151: getfield mMeasureOrderTextViews : Ljava/util/ArrayList;
    //   154: iload #9
    //   156: invokevirtual get : (I)Ljava/lang/Object;
    //   159: checkcast android/util/Pair
    //   162: astore #8
    //   164: aload #8
    //   166: getfield first : Ljava/lang/Object;
    //   169: checkcast java/lang/Integer
    //   172: invokevirtual intValue : ()I
    //   175: aload #8
    //   177: getfield second : Ljava/lang/Object;
    //   180: checkcast android/widget/TextView
    //   183: invokevirtual getText : ()Ljava/lang/CharSequence;
    //   186: invokeinterface length : ()I
    //   191: if_icmpeq -> 197
    //   194: iconst_1
    //   195: istore #4
    //   197: iinc #9, 1
    //   200: goto -> 143
    //   203: goto -> 206
    //   206: iload #4
    //   208: ifeq -> 219
    //   211: aload_0
    //   212: iload #5
    //   214: iload #6
    //   216: invokespecial rebuildMeasureOrder : (II)V
    //   219: iload_1
    //   220: invokestatic getMode : (I)I
    //   223: ifeq -> 232
    //   226: iconst_1
    //   227: istore #6
    //   229: goto -> 235
    //   232: iconst_0
    //   233: istore #6
    //   235: iload_1
    //   236: invokestatic getSize : (I)I
    //   239: aload_0
    //   240: getfield mPaddingLeft : I
    //   243: isub
    //   244: aload_0
    //   245: getfield mPaddingRight : I
    //   248: isub
    //   249: istore #11
    //   251: aload_0
    //   252: getfield mMeasureOrderOther : Ljava/util/ArrayList;
    //   255: invokevirtual size : ()I
    //   258: istore #12
    //   260: aload_0
    //   261: iconst_0
    //   262: putfield mMaxChildHeight : I
    //   265: iconst_0
    //   266: istore #4
    //   268: iconst_0
    //   269: istore #9
    //   271: iconst_0
    //   272: istore #5
    //   274: iload #9
    //   276: iload_3
    //   277: if_icmpge -> 430
    //   280: iload #9
    //   282: iload #12
    //   284: if_icmpge -> 304
    //   287: aload_0
    //   288: getfield mMeasureOrderOther : Ljava/util/ArrayList;
    //   291: iload #9
    //   293: invokevirtual get : (I)Ljava/lang/Object;
    //   296: checkcast android/view/View
    //   299: astore #8
    //   301: goto -> 327
    //   304: aload_0
    //   305: getfield mMeasureOrderTextViews : Ljava/util/ArrayList;
    //   308: iload #9
    //   310: iload #12
    //   312: isub
    //   313: invokevirtual get : (I)Ljava/lang/Object;
    //   316: checkcast android/util/Pair
    //   319: getfield second : Ljava/lang/Object;
    //   322: checkcast android/view/View
    //   325: astore #8
    //   327: aload #8
    //   329: invokevirtual getVisibility : ()I
    //   332: bipush #8
    //   334: if_icmpne -> 340
    //   337: goto -> 424
    //   340: aload #8
    //   342: invokevirtual getLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
    //   345: checkcast android/view/ViewGroup$MarginLayoutParams
    //   348: astore #13
    //   350: iload #6
    //   352: ifeq -> 378
    //   355: iload #11
    //   357: iload #4
    //   359: isub
    //   360: iload #7
    //   362: iload #5
    //   364: isub
    //   365: idiv
    //   366: istore #10
    //   368: iload #11
    //   370: iload #10
    //   372: isub
    //   373: istore #10
    //   375: goto -> 382
    //   378: iload #4
    //   380: istore #10
    //   382: aload_0
    //   383: aload #8
    //   385: iload_1
    //   386: iload #10
    //   388: iload_2
    //   389: iconst_0
    //   390: invokevirtual measureChildWithMargins : (Landroid/view/View;IIII)V
    //   393: iload #4
    //   395: aload #8
    //   397: invokevirtual getMeasuredWidth : ()I
    //   400: aload #13
    //   402: getfield rightMargin : I
    //   405: iadd
    //   406: aload #13
    //   408: getfield leftMargin : I
    //   411: iadd
    //   412: iadd
    //   413: istore #4
    //   415: aload_0
    //   416: aload #8
    //   418: invokevirtual resetCurrentMaxChildHeight : (Landroid/view/View;)V
    //   421: iinc #5, 1
    //   424: iinc #9, 1
    //   427: goto -> 274
    //   430: aload_0
    //   431: iload #4
    //   433: aload_0
    //   434: getfield mPaddingRight : I
    //   437: iadd
    //   438: aload_0
    //   439: getfield mPaddingLeft : I
    //   442: iadd
    //   443: putfield mTotalWidth : I
    //   446: aload_0
    //   447: iload_1
    //   448: iload_2
    //   449: invokevirtual setActionLayoutMeasuredDimension : (II)V
    //   452: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #77	-> 0
    //   #78	-> 7
    //   #79	-> 13
    //   #81	-> 14
    //   #82	-> 19
    //   #83	-> 19
    //   #84	-> 19
    //   #86	-> 19
    //   #87	-> 37
    //   #88	-> 45
    //   #89	-> 53
    //   #91	-> 59
    //   #93	-> 62
    //   #94	-> 76
    //   #86	-> 82
    //   #100	-> 92
    //   #101	-> 95
    //   #102	-> 113
    //   #103	-> 123
    //   #105	-> 126
    //   #106	-> 131
    //   #107	-> 140
    //   #108	-> 150
    //   #109	-> 164
    //   #110	-> 194
    //   #107	-> 197
    //   #105	-> 206
    //   #115	-> 206
    //   #116	-> 211
    //   #119	-> 219
    //   #120	-> 219
    //   #122	-> 235
    //   #123	-> 251
    //   #124	-> 260
    //   #126	-> 260
    //   #129	-> 260
    //   #131	-> 265
    //   #135	-> 280
    //   #136	-> 287
    //   #138	-> 304
    //   #140	-> 327
    //   #141	-> 337
    //   #143	-> 340
    //   #145	-> 350
    //   #146	-> 350
    //   #151	-> 355
    //   #152	-> 355
    //   #154	-> 368
    //   #146	-> 378
    //   #157	-> 382
    //   #160	-> 393
    //   #163	-> 415
    //   #165	-> 421
    //   #131	-> 424
    //   #167	-> 430
    //   #175	-> 446
    //   #177	-> 452
  }
  
  private void rebuildMeasureOrder(int paramInt1, int paramInt2) {
    clearMeasureOrder();
    this.mMeasureOrderTextViews.ensureCapacity(paramInt1);
    this.mMeasureOrderOther.ensureCapacity(paramInt2);
    paramInt2 = getChildCount();
    for (paramInt1 = 0; paramInt1 < paramInt2; paramInt1++) {
      View view = getChildAt(paramInt1);
      if (view instanceof TextView && ((TextView)view).getText().length() > 0) {
        this.mMeasureOrderTextViews.add(Pair.create(Integer.valueOf(((TextView)view).getText().length()), view));
      } else {
        this.mMeasureOrderOther.add(view);
      } 
    } 
    this.mMeasureOrderTextViews.sort(MEASURE_ORDER_COMPARATOR);
  }
  
  private void clearMeasureOrder() {
    this.mMeasureOrderOther.clear();
    this.mMeasureOrderTextViews.clear();
  }
  
  public void onViewAdded(View paramView) {
    super.onViewAdded(paramView);
    clearMeasureOrder();
    if (paramView.getBackground() instanceof RippleDrawable)
      ((RippleDrawable)paramView.getBackground()).setForceSoftware(true); 
  }
  
  public void onViewRemoved(View paramView) {
    super.onViewRemoved(paramView);
    clearMeasureOrder();
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (this.mEmphasizedMode) {
      super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
      return;
    } 
    paramBoolean = isLayoutRtl();
    int i = this.mPaddingTop;
    int j = this.mGravity, k = 1;
    if ((j & 0x1) == 0)
      k = 0; 
    if (k) {
      k = this.mPaddingLeft + paramInt1 + (paramInt3 - paramInt1) / 2 - this.mTotalWidth / 2;
    } else {
      j = this.mPaddingLeft;
      int i1 = Gravity.getAbsoluteGravity(8388611, getLayoutDirection());
      k = j;
      if (i1 == 5)
        k = j + paramInt3 - paramInt1 - this.mTotalWidth; 
    } 
    int m = this.mPaddingBottom;
    int n = getChildCount();
    j = 0;
    paramInt3 = 1;
    if (paramBoolean) {
      j = n - 1;
      paramInt3 = -1;
    } 
    for (byte b = 0; b < n; b++) {
      View view = getChildAt(paramInt3 * b + j);
      if (view.getVisibility() != 8) {
        int i1 = view.getMeasuredWidth();
        int i2 = view.getMeasuredHeight();
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams)view.getLayoutParams();
        int i3 = paramInt1 + (paramInt4 - paramInt2 - i - m - i2) / 2 + marginLayoutParams.topMargin - marginLayoutParams.bottomMargin;
        k += marginLayoutParams.leftMargin;
        view.layout(k, i3, k + i1, i3 + i2);
        k += marginLayoutParams.rightMargin + i1;
      } 
    } 
  }
  
  protected void onFinishInflate() {
    super.onFinishInflate();
    this.mDefaultPaddingBottom = getPaddingBottom();
    this.mDefaultPaddingTop = getPaddingTop();
    updateHeights();
  }
  
  private void updateHeights() {
    int i = getResources().getDimensionPixelSize(17105344);
    int j = getResources().getDimensionPixelSize(17105345);
    this.mEmphasizedHeight = j + i + getResources().getDimensionPixelSize(17105334);
    this.mRegularHeight = getResources().getDimensionPixelSize(17105335);
  }
  
  @RemotableViewMethod
  public void setEmphasizedMode(boolean paramBoolean) {
    int i;
    this.mEmphasizedMode = paramBoolean;
    if (paramBoolean) {
      int j = getResources().getDimensionPixelSize(17105344);
      int k = getResources().getDimensionPixelSize(17105345);
      i = this.mEmphasizedHeight;
      int m = getResources().getDimensionPixelSize(17104964);
      int n = getPaddingStart();
      int i1 = getPaddingEnd();
      setPaddingRelative(n, j - m, i1, k - m);
    } else {
      i = getPaddingStart();
      int m = this.mDefaultPaddingTop;
      int j = getPaddingEnd(), k = this.mDefaultPaddingBottom;
      setPaddingRelative(i, m, j, k);
      i = this.mRegularHeight;
    } 
    ViewGroup.LayoutParams layoutParams = getLayoutParams();
    layoutParams.height = i;
    setLayoutParams(layoutParams);
  }
  
  public int getExtraMeasureHeight() {
    if (this.mEmphasizedMode)
      return this.mEmphasizedHeight - this.mRegularHeight; 
    return 0;
  }
  
  public static final Comparator<Pair<Integer, TextView>> MEASURE_ORDER_COMPARATOR = (Comparator<Pair<Integer, TextView>>)_$$Lambda$NotificationActionListLayout$uFZFEmIEBpI3kn6c3tNvvgmMSv8.INSTANCE;
}
