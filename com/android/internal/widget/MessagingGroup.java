package com.android.internal.widget;

import android.app.Person;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Icon;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Pools;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RemoteViews.RemoteView;
import android.widget.TextView;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

@RemoteView
public class MessagingGroup extends LinearLayout implements MessagingLinearLayout.MessagingChild {
  private static Pools.SimplePool<MessagingGroup> sInstancePool = (Pools.SimplePool<MessagingGroup>)new Pools.SynchronizedPool(10);
  
  private String mAvatarSymbol = "";
  
  private CharSequence mAvatarName = "";
  
  private ArrayList<MessagingMessage> mAddedMessages = new ArrayList<>();
  
  private Point mDisplaySize = new Point();
  
  private boolean mShowingAvatar = true;
  
  private boolean mSingleLine = false;
  
  private int mRequestedMaxDisplayedLines = Integer.MAX_VALUE;
  
  private boolean mIsFirstGroupInLayout = true;
  
  private boolean mIsInConversation = true;
  
  public static final int IMAGE_DISPLAY_LOCATION_AT_END = 1;
  
  public static final int IMAGE_DISPLAY_LOCATION_EXTERNAL = 2;
  
  public static final int IMAGE_DISPLAY_LOCATION_INLINE = 0;
  
  private View mAvatarContainer;
  
  private Icon mAvatarIcon;
  
  private ImageView mAvatarView;
  
  private boolean mCanHideSenderIfFirst;
  
  private boolean mClippingDisabled;
  
  private LinearLayout mContentContainer;
  
  private int mConversationContentStart;
  
  private boolean mFirstLayout;
  
  private ViewGroup mImageContainer;
  
  private int mImageDisplayLocation;
  
  private boolean mIsHidingAnimated;
  
  private MessagingImageMessage mIsolatedMessage;
  
  private int mLayoutColor;
  
  private MessagingLinearLayout mMessageContainer;
  
  private List<MessagingMessage> mMessages;
  
  private ViewGroup mMessagingIconContainer;
  
  private boolean mNeedsGeneratedAvatar;
  
  private int mNonConversationMarginEnd;
  
  private int mNotificationTextMarginTop;
  
  private Person mSender;
  
  private CharSequence mSenderName;
  
  private int mSenderTextPaddingSingleLine;
  
  ImageFloatingTextView mSenderView;
  
  private ProgressBar mSendingSpinner;
  
  private View mSendingSpinnerContainer;
  
  private int mSendingTextColor;
  
  private int mTextColor;
  
  public MessagingGroup(Context paramContext) {
    super(paramContext);
  }
  
  public MessagingGroup(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  public MessagingGroup(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  public MessagingGroup(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  protected void onFinishInflate() {
    super.onFinishInflate();
    this.mMessageContainer = (MessagingLinearLayout)findViewById(16909025);
    this.mSenderView = (ImageFloatingTextView)findViewById(16909167);
    this.mAvatarView = (ImageView)findViewById(16909165);
    this.mImageContainer = (ViewGroup)findViewById(16909170);
    this.mSendingSpinner = (ProgressBar)findViewById(16909171);
    this.mMessagingIconContainer = (ViewGroup)findViewById(16909166);
    this.mContentContainer = (LinearLayout)findViewById(16909169);
    this.mSendingSpinnerContainer = findViewById(16909172);
    DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
    this.mDisplaySize.x = displayMetrics.widthPixels;
    this.mDisplaySize.y = displayMetrics.heightPixels;
    this.mSenderTextPaddingSingleLine = getResources().getDimensionPixelSize(17105316);
    this.mConversationContentStart = getResources().getDimensionPixelSize(17105106);
    this.mNonConversationMarginEnd = getResources().getDimensionPixelSize(17105321);
    this.mNotificationTextMarginTop = getResources().getDimensionPixelSize(17105383);
  }
  
  public void updateClipRect() {
    Rect rect;
    if (this.mSenderView.getVisibility() != 8 && !this.mClippingDisabled) {
      int i;
      if (this.mSingleLine) {
        i = 0;
      } else {
        i = getDistanceFromParent((View)this.mSenderView, (ViewGroup)this.mContentContainer);
        MessagingLinearLayout messagingLinearLayout = this.mMessageContainer;
        LinearLayout linearLayout = this.mContentContainer;
        int k = getDistanceFromParent((View)messagingLinearLayout, (ViewGroup)linearLayout);
        ImageFloatingTextView imageFloatingTextView = this.mSenderView;
        i = i - k + imageFloatingTextView.getHeight();
      } 
      int j = Math.max(this.mDisplaySize.x, this.mDisplaySize.y);
      rect = new Rect(-j, i, j, j);
    } else {
      rect = null;
    } 
    this.mMessageContainer.setClipBounds(rect);
  }
  
  private int getDistanceFromParent(View paramView, ViewGroup paramViewGroup) {
    int i = 0;
    while (paramView != paramViewGroup) {
      i = (int)(i + paramView.getTop() + paramView.getTranslationY());
      paramView = (View)paramView.getParent();
    } 
    return i;
  }
  
  public void setSender(Person paramPerson, CharSequence paramCharSequence) {
    this.mSender = paramPerson;
    CharSequence charSequence = paramCharSequence;
    if (paramCharSequence == null)
      charSequence = paramPerson.getName(); 
    this.mSenderName = charSequence;
    boolean bool = this.mSingleLine;
    boolean bool1 = true;
    paramCharSequence = charSequence;
    if (bool) {
      paramCharSequence = charSequence;
      if (!TextUtils.isEmpty(charSequence))
        paramCharSequence = this.mContext.getResources().getString(17040002, new Object[] { charSequence }); 
    } 
    this.mSenderView.setText(paramCharSequence);
    if (paramPerson.getIcon() != null)
      bool1 = false; 
    this.mNeedsGeneratedAvatar = bool1;
    if (!bool1)
      setAvatar(paramPerson.getIcon()); 
    updateSenderVisibility();
  }
  
  public void setShowingAvatar(boolean paramBoolean) {
    byte b;
    ImageView imageView = this.mAvatarView;
    if (paramBoolean) {
      b = 0;
    } else {
      b = 8;
    } 
    imageView.setVisibility(b);
    this.mShowingAvatar = paramBoolean;
  }
  
  public void setSending(boolean paramBoolean) {
    byte b;
    if (paramBoolean) {
      b = 0;
    } else {
      b = 8;
    } 
    if (this.mSendingSpinnerContainer.getVisibility() != b) {
      this.mSendingSpinnerContainer.setVisibility(b);
      updateMessageColor();
    } 
  }
  
  private int calculateSendingTextColor() {
    TypedValue typedValue = new TypedValue();
    this.mContext.getResources().getValue(17105381, typedValue, true);
    float f1 = typedValue.getFloat();
    null = this.mTextColor;
    float f2 = Color.red(null);
    null = this.mTextColor;
    float f3 = Color.green(null);
    null = this.mTextColor;
    float f4 = Color.blue(null);
    Color color = Color.valueOf(f2, f3, f4, f1);
    return color.toArgb();
  }
  
  public void setAvatar(Icon paramIcon) {
    this.mAvatarIcon = paramIcon;
    if (this.mShowingAvatar || paramIcon == null)
      this.mAvatarView.setImageIcon(paramIcon); 
    this.mAvatarSymbol = "";
    this.mAvatarName = "";
  }
  
  static MessagingGroup createGroup(MessagingLinearLayout paramMessagingLinearLayout) {
    MessagingGroup messagingGroup1 = (MessagingGroup)sInstancePool.acquire();
    MessagingGroup messagingGroup2 = messagingGroup1;
    if (messagingGroup1 == null) {
      messagingGroup2 = (MessagingGroup)LayoutInflater.from(paramMessagingLinearLayout.getContext()).inflate(17367217, paramMessagingLinearLayout, false);
      messagingGroup2.addOnLayoutChangeListener(MessagingLayout.MESSAGING_PROPERTY_ANIMATOR);
    } 
    paramMessagingLinearLayout.addView((View)messagingGroup2);
    return messagingGroup2;
  }
  
  public void removeMessage(MessagingMessage paramMessagingMessage) {
    View view = paramMessagingMessage.getView();
    boolean bool = view.isShown();
    ViewGroup viewGroup = (ViewGroup)view.getParent();
    if (viewGroup == null)
      return; 
    viewGroup.removeView(view);
    _$$Lambda$MessagingGroup$uEKViIlAuE6AYNmbbTgLGe5mU7I _$$Lambda$MessagingGroup$uEKViIlAuE6AYNmbbTgLGe5mU7I = new _$$Lambda$MessagingGroup$uEKViIlAuE6AYNmbbTgLGe5mU7I(viewGroup, view, paramMessagingMessage);
    if (bool && !MessagingLinearLayout.isGone(view)) {
      viewGroup.addTransientView(view, 0);
      performRemoveAnimation(view, _$$Lambda$MessagingGroup$uEKViIlAuE6AYNmbbTgLGe5mU7I);
    } else {
      _$$Lambda$MessagingGroup$uEKViIlAuE6AYNmbbTgLGe5mU7I.run();
    } 
  }
  
  public void recycle() {
    MessagingImageMessage messagingImageMessage = this.mIsolatedMessage;
    if (messagingImageMessage != null)
      this.mImageContainer.removeView((View)messagingImageMessage); 
    for (byte b = 0; b < this.mMessages.size(); b++) {
      MessagingMessage messagingMessage = this.mMessages.get(b);
      this.mMessageContainer.removeView(messagingMessage.getView());
      messagingMessage.recycle();
    } 
    setAvatar((Icon)null);
    this.mAvatarView.setAlpha(1.0F);
    this.mAvatarView.setTranslationY(0.0F);
    this.mSenderView.setAlpha(1.0F);
    this.mSenderView.setTranslationY(0.0F);
    setAlpha(1.0F);
    this.mIsolatedMessage = null;
    this.mMessages = null;
    this.mSenderName = null;
    this.mAddedMessages.clear();
    this.mFirstLayout = true;
    setCanHideSenderIfFirst(false);
    setIsFirstInLayout(true);
    setMaxDisplayedLines(2147483647);
    setSingleLine(false);
    setShowingAvatar(true);
    MessagingPropertyAnimator.recycle((View)this);
    sInstancePool.release(this);
  }
  
  public void removeGroupAnimated(Runnable paramRunnable) {
    performRemoveAnimation((View)this, new _$$Lambda$MessagingGroup$QKnXYzCylYJqF8wEQG98SXlcu2M(this, paramRunnable));
  }
  
  public void performRemoveAnimation(View paramView, Runnable paramRunnable) {
    performRemoveAnimation(paramView, -paramView.getHeight(), paramRunnable);
  }
  
  private void performRemoveAnimation(View paramView, int paramInt, Runnable paramRunnable) {
    MessagingPropertyAnimator.startLocalTranslationTo(paramView, paramInt, MessagingLayout.FAST_OUT_LINEAR_IN);
    MessagingPropertyAnimator.fadeOut(paramView, paramRunnable);
  }
  
  public CharSequence getSenderName() {
    return this.mSenderName;
  }
  
  public static void dropCache() {
    sInstancePool = (Pools.SimplePool<MessagingGroup>)new Pools.SynchronizedPool(10);
  }
  
  public int getMeasuredType() {
    if (this.mIsolatedMessage != null)
      return 1; 
    boolean bool = false;
    int i = this.mMessageContainer.getChildCount() - 1;
    while (true) {
      boolean bool1 = false;
      if (i >= 0) {
        boolean bool2;
        View view = this.mMessageContainer.getChildAt(i);
        if (view.getVisibility() == 8) {
          bool2 = bool;
        } else {
          bool2 = bool;
          if (view instanceof MessagingLinearLayout.MessagingChild) {
            int j = ((MessagingLinearLayout.MessagingChild)view).getMeasuredType();
            bool2 = bool1;
            if (j == 2)
              bool2 = true; 
            MessagingLinearLayout.LayoutParams layoutParams = (MessagingLinearLayout.LayoutParams)view.getLayoutParams();
            boolean bool3 = layoutParams.hide;
            if (bool2 | bool3) {
              if (bool)
                return 1; 
              return 2;
            } 
            if (j == 1)
              return 1; 
            bool2 = true;
          } 
        } 
        i--;
        bool = bool2;
        continue;
      } 
      break;
    } 
    return 0;
  }
  
  public int getConsumedLines() {
    int i = 0;
    for (byte b = 0; b < this.mMessageContainer.getChildCount(); b++, i = j) {
      View view = this.mMessageContainer.getChildAt(b);
      int j = i;
      if (view instanceof MessagingLinearLayout.MessagingChild)
        j = i + ((MessagingLinearLayout.MessagingChild)view).getConsumedLines(); 
    } 
    if (this.mIsolatedMessage != null)
      i = Math.max(i, 1); 
    return i + 1;
  }
  
  public void setMaxDisplayedLines(int paramInt) {
    this.mRequestedMaxDisplayedLines = paramInt;
    updateMaxDisplayedLines();
  }
  
  private void updateMaxDisplayedLines() {
    int i;
    MessagingLinearLayout messagingLinearLayout = this.mMessageContainer;
    if (this.mSingleLine) {
      i = 1;
    } else {
      i = this.mRequestedMaxDisplayedLines;
    } 
    messagingLinearLayout.setMaxDisplayedLines(i);
  }
  
  public void hideAnimated() {
    setIsHidingAnimated(true);
    removeGroupAnimated(new _$$Lambda$MessagingGroup$buM2CBWR7uz4neT0lee_MKMDx5M(this));
  }
  
  public boolean isHidingAnimated() {
    return this.mIsHidingAnimated;
  }
  
  public void setIsFirstInLayout(boolean paramBoolean) {
    if (paramBoolean != this.mIsFirstGroupInLayout) {
      this.mIsFirstGroupInLayout = paramBoolean;
      updateSenderVisibility();
    } 
  }
  
  public void setCanHideSenderIfFirst(boolean paramBoolean) {
    if (this.mCanHideSenderIfFirst != paramBoolean) {
      this.mCanHideSenderIfFirst = paramBoolean;
      updateSenderVisibility();
    } 
  }
  
  private void updateSenderVisibility() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mIsFirstGroupInLayout : Z
    //   4: istore_1
    //   5: iconst_0
    //   6: istore_2
    //   7: iload_1
    //   8: ifne -> 18
    //   11: aload_0
    //   12: getfield mSingleLine : Z
    //   15: ifeq -> 25
    //   18: aload_0
    //   19: getfield mCanHideSenderIfFirst : Z
    //   22: ifne -> 46
    //   25: aload_0
    //   26: getfield mSenderName : Ljava/lang/CharSequence;
    //   29: astore_3
    //   30: aload_3
    //   31: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   34: ifeq -> 40
    //   37: goto -> 46
    //   40: iconst_0
    //   41: istore #4
    //   43: goto -> 49
    //   46: iconst_1
    //   47: istore #4
    //   49: aload_0
    //   50: getfield mSenderView : Lcom/android/internal/widget/ImageFloatingTextView;
    //   53: astore_3
    //   54: iload #4
    //   56: ifeq -> 62
    //   59: bipush #8
    //   61: istore_2
    //   62: aload_3
    //   63: iload_2
    //   64: invokevirtual setVisibility : (I)V
    //   67: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #424	-> 0
    //   #425	-> 30
    //   #426	-> 49
    //   #427	-> 67
  }
  
  public boolean hasDifferentHeightWhenFirst() {
    boolean bool;
    if (this.mCanHideSenderIfFirst && !this.mSingleLine && !TextUtils.isEmpty(this.mSenderName)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void setIsHidingAnimated(boolean paramBoolean) {
    ViewParent viewParent = getParent();
    this.mIsHidingAnimated = paramBoolean;
    invalidate();
    if (viewParent instanceof ViewGroup)
      ((ViewGroup)viewParent).invalidate(); 
  }
  
  public boolean hasOverlappingRendering() {
    return false;
  }
  
  public Icon getAvatarSymbolIfMatching(CharSequence paramCharSequence, String paramString, int paramInt) {
    if (this.mAvatarName.equals(paramCharSequence) && this.mAvatarSymbol.equals(paramString) && paramInt == this.mLayoutColor)
      return this.mAvatarIcon; 
    return null;
  }
  
  public void setCreatedAvatar(Icon paramIcon, CharSequence paramCharSequence, String paramString, int paramInt) {
    if (!this.mAvatarName.equals(paramCharSequence) || !this.mAvatarSymbol.equals(paramString) || paramInt != this.mLayoutColor) {
      setAvatar(paramIcon);
      this.mAvatarSymbol = paramString;
      setLayoutColor(paramInt);
      this.mAvatarName = paramCharSequence;
    } 
  }
  
  public void setTextColors(int paramInt1, int paramInt2) {
    this.mTextColor = paramInt2;
    this.mSendingTextColor = calculateSendingTextColor();
    updateMessageColor();
    this.mSenderView.setTextColor(paramInt1);
  }
  
  public void setLayoutColor(int paramInt) {
    if (paramInt != this.mLayoutColor) {
      this.mLayoutColor = paramInt;
      this.mSendingSpinner.setIndeterminateTintList(ColorStateList.valueOf(paramInt));
    } 
  }
  
  private void updateMessageColor() {
    if (this.mMessages != null) {
      int i;
      if (this.mSendingSpinnerContainer.getVisibility() == 0) {
        i = this.mSendingTextColor;
      } else {
        i = this.mTextColor;
      } 
      for (MessagingMessage messagingMessage : this.mMessages) {
        int j;
        if (messagingMessage.getMessage().isRemoteInputHistory()) {
          j = i;
        } else {
          j = this.mTextColor;
        } 
        messagingMessage.setColor(j);
      } 
    } 
  }
  
  public void setMessages(List<MessagingMessage> paramList) {
    // Byte code:
    //   0: iconst_0
    //   1: istore_2
    //   2: aconst_null
    //   3: astore_3
    //   4: iconst_0
    //   5: istore #4
    //   7: iload #4
    //   9: aload_1
    //   10: invokeinterface size : ()I
    //   15: if_icmpge -> 240
    //   18: aload_1
    //   19: iload #4
    //   21: invokeinterface get : (I)Ljava/lang/Object;
    //   26: checkcast com/android/internal/widget/MessagingMessage
    //   29: astore #5
    //   31: aload #5
    //   33: invokeinterface getGroup : ()Lcom/android/internal/widget/MessagingGroup;
    //   38: aload_0
    //   39: if_acmpeq -> 60
    //   42: aload #5
    //   44: aload_0
    //   45: invokeinterface setMessagingGroup : (Lcom/android/internal/widget/MessagingGroup;)V
    //   50: aload_0
    //   51: getfield mAddedMessages : Ljava/util/ArrayList;
    //   54: aload #5
    //   56: invokevirtual add : (Ljava/lang/Object;)Z
    //   59: pop
    //   60: aload #5
    //   62: instanceof com/android/internal/widget/MessagingImageMessage
    //   65: istore #6
    //   67: aload_0
    //   68: getfield mImageDisplayLocation : I
    //   71: ifeq -> 88
    //   74: iload #6
    //   76: ifeq -> 88
    //   79: aload #5
    //   81: checkcast com/android/internal/widget/MessagingImageMessage
    //   84: astore_3
    //   85: goto -> 234
    //   88: aload_0
    //   89: aload #5
    //   91: aload_0
    //   92: getfield mMessageContainer : Lcom/android/internal/widget/MessagingLinearLayout;
    //   95: invokespecial removeFromParentIfDifferent : (Lcom/android/internal/widget/MessagingMessage;Landroid/view/ViewGroup;)Z
    //   98: ifeq -> 170
    //   101: aload #5
    //   103: invokeinterface getView : ()Landroid/view/View;
    //   108: invokevirtual getLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
    //   111: astore #7
    //   113: aload #7
    //   115: ifnull -> 155
    //   118: aload #7
    //   120: instanceof com/android/internal/widget/MessagingLinearLayout$LayoutParams
    //   123: ifne -> 155
    //   126: aload #5
    //   128: invokeinterface getView : ()Landroid/view/View;
    //   133: astore #7
    //   135: aload_0
    //   136: getfield mMessageContainer : Lcom/android/internal/widget/MessagingLinearLayout;
    //   139: astore #8
    //   141: aload #8
    //   143: invokevirtual generateDefaultLayoutParams : ()Lcom/android/internal/widget/MessagingLinearLayout$LayoutParams;
    //   146: astore #8
    //   148: aload #7
    //   150: aload #8
    //   152: invokevirtual setLayoutParams : (Landroid/view/ViewGroup$LayoutParams;)V
    //   155: aload_0
    //   156: getfield mMessageContainer : Lcom/android/internal/widget/MessagingLinearLayout;
    //   159: aload #5
    //   161: invokeinterface getView : ()Landroid/view/View;
    //   166: iload_2
    //   167: invokevirtual addView : (Landroid/view/View;I)V
    //   170: iload #6
    //   172: ifeq -> 184
    //   175: aload #5
    //   177: checkcast com/android/internal/widget/MessagingImageMessage
    //   180: iconst_0
    //   181: invokevirtual setIsolated : (Z)V
    //   184: iload_2
    //   185: aload_0
    //   186: getfield mMessageContainer : Lcom/android/internal/widget/MessagingLinearLayout;
    //   189: aload #5
    //   191: invokeinterface getView : ()Landroid/view/View;
    //   196: invokevirtual indexOfChild : (Landroid/view/View;)I
    //   199: if_icmpeq -> 231
    //   202: aload_0
    //   203: getfield mMessageContainer : Lcom/android/internal/widget/MessagingLinearLayout;
    //   206: aload #5
    //   208: invokeinterface getView : ()Landroid/view/View;
    //   213: invokevirtual removeView : (Landroid/view/View;)V
    //   216: aload_0
    //   217: getfield mMessageContainer : Lcom/android/internal/widget/MessagingLinearLayout;
    //   220: aload #5
    //   222: invokeinterface getView : ()Landroid/view/View;
    //   227: iload_2
    //   228: invokevirtual addView : (Landroid/view/View;I)V
    //   231: iinc #2, 1
    //   234: iinc #4, 1
    //   237: goto -> 7
    //   240: aload_3
    //   241: ifnull -> 312
    //   244: aload_0
    //   245: getfield mImageDisplayLocation : I
    //   248: iconst_1
    //   249: if_icmpne -> 289
    //   252: aload_0
    //   253: getfield mImageContainer : Landroid/view/ViewGroup;
    //   256: astore #5
    //   258: aload_0
    //   259: aload_3
    //   260: aload #5
    //   262: invokespecial removeFromParentIfDifferent : (Lcom/android/internal/widget/MessagingMessage;Landroid/view/ViewGroup;)Z
    //   265: ifeq -> 289
    //   268: aload_0
    //   269: getfield mImageContainer : Landroid/view/ViewGroup;
    //   272: invokevirtual removeAllViews : ()V
    //   275: aload_0
    //   276: getfield mImageContainer : Landroid/view/ViewGroup;
    //   279: aload_3
    //   280: invokevirtual getView : ()Landroid/view/View;
    //   283: invokevirtual addView : (Landroid/view/View;)V
    //   286: goto -> 304
    //   289: aload_0
    //   290: getfield mImageDisplayLocation : I
    //   293: iconst_2
    //   294: if_icmpne -> 304
    //   297: aload_0
    //   298: getfield mImageContainer : Landroid/view/ViewGroup;
    //   301: invokevirtual removeAllViews : ()V
    //   304: aload_3
    //   305: iconst_1
    //   306: invokevirtual setIsolated : (Z)V
    //   309: goto -> 326
    //   312: aload_0
    //   313: getfield mIsolatedMessage : Lcom/android/internal/widget/MessagingImageMessage;
    //   316: ifnull -> 326
    //   319: aload_0
    //   320: getfield mImageContainer : Landroid/view/ViewGroup;
    //   323: invokevirtual removeAllViews : ()V
    //   326: aload_0
    //   327: aload_3
    //   328: putfield mIsolatedMessage : Lcom/android/internal/widget/MessagingImageMessage;
    //   331: aload_0
    //   332: invokespecial updateImageContainerVisibility : ()V
    //   335: aload_0
    //   336: aload_1
    //   337: putfield mMessages : Ljava/util/List;
    //   340: aload_0
    //   341: invokespecial updateMessageColor : ()V
    //   344: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #494	-> 0
    //   #495	-> 2
    //   #496	-> 4
    //   #497	-> 18
    //   #498	-> 31
    //   #499	-> 42
    //   #500	-> 50
    //   #502	-> 60
    //   #503	-> 67
    //   #504	-> 79
    //   #506	-> 88
    //   #507	-> 101
    //   #508	-> 113
    //   #510	-> 126
    //   #511	-> 141
    //   #510	-> 148
    //   #513	-> 155
    //   #515	-> 170
    //   #516	-> 175
    //   #519	-> 184
    //   #520	-> 202
    //   #521	-> 216
    //   #523	-> 231
    //   #496	-> 234
    //   #526	-> 240
    //   #527	-> 244
    //   #528	-> 258
    //   #529	-> 268
    //   #530	-> 275
    //   #531	-> 289
    //   #532	-> 297
    //   #534	-> 304
    //   #535	-> 312
    //   #536	-> 319
    //   #538	-> 326
    //   #539	-> 331
    //   #540	-> 335
    //   #541	-> 340
    //   #542	-> 344
  }
  
  private void updateImageContainerVisibility() {
    byte b;
    ViewGroup viewGroup = this.mImageContainer;
    if (this.mIsolatedMessage != null && this.mImageDisplayLocation == 1) {
      b = 0;
    } else {
      b = 8;
    } 
    viewGroup.setVisibility(b);
  }
  
  private boolean removeFromParentIfDifferent(MessagingMessage paramMessagingMessage, ViewGroup paramViewGroup) {
    ViewParent viewParent = paramMessagingMessage.getView().getParent();
    if (viewParent != paramViewGroup) {
      if (viewParent instanceof ViewGroup)
        ((ViewGroup)viewParent).removeView(paramMessagingMessage.getView()); 
      return true;
    } 
    return false;
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    if (!this.mAddedMessages.isEmpty()) {
      paramBoolean = this.mFirstLayout;
      getViewTreeObserver().addOnPreDrawListener((ViewTreeObserver.OnPreDrawListener)new Object(this, paramBoolean));
    } 
    this.mFirstLayout = false;
    updateClipRect();
  }
  
  public int calculateGroupCompatibility(MessagingGroup paramMessagingGroup) {
    if (TextUtils.equals(getSenderName(), paramMessagingGroup.getSenderName())) {
      byte b1 = 1;
      for (byte b2 = 0; b2 < this.mMessages.size() && b2 < paramMessagingGroup.mMessages.size(); b2++) {
        List<MessagingMessage> list1 = this.mMessages;
        MessagingMessage messagingMessage1 = list1.get(list1.size() - 1 - b2);
        List<MessagingMessage> list2 = paramMessagingGroup.mMessages;
        int i = list2.size();
        MessagingMessage messagingMessage2 = list2.get(i - 1 - b2);
        if (!messagingMessage1.sameAs(messagingMessage2))
          return b1; 
        b1++;
      } 
      return b1;
    } 
    return 0;
  }
  
  public TextView getSenderView() {
    return this.mSenderView;
  }
  
  public View getAvatar() {
    return (View)this.mAvatarView;
  }
  
  public Icon getAvatarIcon() {
    return this.mAvatarIcon;
  }
  
  public MessagingLinearLayout getMessageContainer() {
    return this.mMessageContainer;
  }
  
  public MessagingImageMessage getIsolatedMessage() {
    return this.mIsolatedMessage;
  }
  
  public boolean needsGeneratedAvatar() {
    return this.mNeedsGeneratedAvatar;
  }
  
  public Person getSender() {
    return this.mSender;
  }
  
  public void setClippingDisabled(boolean paramBoolean) {
    this.mClippingDisabled = paramBoolean;
  }
  
  public void setImageDisplayLocation(int paramInt) {
    if (this.mImageDisplayLocation != paramInt) {
      this.mImageDisplayLocation = paramInt;
      updateImageContainerVisibility();
    } 
  }
  
  public List<MessagingMessage> getMessages() {
    return this.mMessages;
  }
  
  public void setSingleLine(boolean paramBoolean) {
    if (paramBoolean != this.mSingleLine) {
      this.mSingleLine = paramBoolean;
      ViewGroup.MarginLayoutParams marginLayoutParams2 = (ViewGroup.MarginLayoutParams)this.mMessageContainer.getLayoutParams();
      boolean bool = false;
      if (paramBoolean) {
        i = 0;
      } else {
        i = this.mNotificationTextMarginTop;
      } 
      marginLayoutParams2.topMargin = i;
      this.mMessageContainer.setLayoutParams((ViewGroup.LayoutParams)marginLayoutParams2);
      LinearLayout linearLayout = this.mContentContainer;
      linearLayout.setOrientation(paramBoolean ^ true);
      ViewGroup.MarginLayoutParams marginLayoutParams1 = (ViewGroup.MarginLayoutParams)this.mSenderView.getLayoutParams();
      int i = bool;
      if (paramBoolean)
        i = this.mSenderTextPaddingSingleLine; 
      marginLayoutParams1.setMarginEnd(i);
      this.mSenderView.setSingleLine(paramBoolean);
      updateMaxDisplayedLines();
      updateClipRect();
      updateSenderVisibility();
    } 
  }
  
  public boolean isSingleLine() {
    return this.mSingleLine;
  }
  
  public void setIsInConversation(boolean paramBoolean) {
    if (this.mIsInConversation != paramBoolean) {
      int i;
      this.mIsInConversation = paramBoolean;
      ViewGroup viewGroup = this.mMessagingIconContainer;
      ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams)viewGroup.getLayoutParams();
      if (this.mIsInConversation) {
        i = this.mConversationContentStart;
      } else {
        i = -2;
      } 
      marginLayoutParams.width = i;
      if (this.mIsInConversation) {
        i = 0;
      } else {
        i = this.mNonConversationMarginEnd;
      } 
      marginLayoutParams.setMarginEnd(i);
      this.mMessagingIconContainer.setLayoutParams((ViewGroup.LayoutParams)marginLayoutParams);
    } 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class ImageDisplayLocation implements Annotation {}
}
