package com.android.internal.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.RemotableViewMethod;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.RemoteViews.RemoteView;
import com.android.internal.R;

@RemoteView
public class MessagingLinearLayout extends ViewGroup {
  private int mMaxDisplayedLines = Integer.MAX_VALUE;
  
  private int mSpacing;
  
  public MessagingLinearLayout(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.MessagingLinearLayout, 0, 0);
    int i = typedArray.getIndexCount();
    for (byte b = 0; b < i; b++) {
      int j = typedArray.getIndex(b);
      if (j == 0)
        this.mSpacing = typedArray.getDimensionPixelSize(b, 0); 
    } 
    typedArray.recycle();
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: iload_2
    //   1: invokestatic getSize : (I)I
    //   4: istore_3
    //   5: iload_2
    //   6: invokestatic getMode : (I)I
    //   9: ifeq -> 15
    //   12: goto -> 18
    //   15: ldc 2147483647
    //   17: istore_3
    //   18: aload_0
    //   19: getfield mPaddingLeft : I
    //   22: istore #4
    //   24: aload_0
    //   25: getfield mPaddingRight : I
    //   28: istore #5
    //   30: aload_0
    //   31: invokevirtual getChildCount : ()I
    //   34: istore #6
    //   36: iconst_0
    //   37: istore #7
    //   39: iload #7
    //   41: iload #6
    //   43: if_icmpge -> 99
    //   46: aload_0
    //   47: iload #7
    //   49: invokevirtual getChildAt : (I)Landroid/view/View;
    //   52: astore #8
    //   54: aload #8
    //   56: invokevirtual getLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
    //   59: checkcast com/android/internal/widget/MessagingLinearLayout$LayoutParams
    //   62: astore #9
    //   64: aload #9
    //   66: iconst_1
    //   67: putfield hide : Z
    //   70: aload #8
    //   72: instanceof com/android/internal/widget/MessagingLinearLayout$MessagingChild
    //   75: ifeq -> 93
    //   78: aload #8
    //   80: checkcast com/android/internal/widget/MessagingLinearLayout$MessagingChild
    //   83: astore #8
    //   85: aload #8
    //   87: iconst_1
    //   88: invokeinterface setIsFirstInLayout : (Z)V
    //   93: iinc #7, 1
    //   96: goto -> 39
    //   99: aload_0
    //   100: getfield mPaddingTop : I
    //   103: istore #7
    //   105: aload_0
    //   106: getfield mPaddingBottom : I
    //   109: istore #10
    //   111: aload_0
    //   112: getfield mMaxDisplayedLines : I
    //   115: istore #11
    //   117: aconst_null
    //   118: astore #9
    //   120: aconst_null
    //   121: astore #8
    //   123: iconst_0
    //   124: istore #12
    //   126: iconst_0
    //   127: istore #13
    //   129: iconst_0
    //   130: istore #14
    //   132: iload #6
    //   134: iconst_1
    //   135: isub
    //   136: istore #15
    //   138: iload #4
    //   140: iload #5
    //   142: iadd
    //   143: istore #4
    //   145: iload #7
    //   147: iload #10
    //   149: iadd
    //   150: istore #7
    //   152: iconst_1
    //   153: istore #10
    //   155: iload #15
    //   157: iflt -> 704
    //   160: iload #7
    //   162: iload_3
    //   163: if_icmpge -> 704
    //   166: aload_0
    //   167: iload #15
    //   169: invokevirtual getChildAt : (I)Landroid/view/View;
    //   172: invokevirtual getVisibility : ()I
    //   175: bipush #8
    //   177: if_icmpne -> 183
    //   180: goto -> 639
    //   183: aload_0
    //   184: iload #15
    //   186: invokevirtual getChildAt : (I)Landroid/view/View;
    //   189: astore #16
    //   191: aload_0
    //   192: iload #15
    //   194: invokevirtual getChildAt : (I)Landroid/view/View;
    //   197: invokevirtual getLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
    //   200: checkcast com/android/internal/widget/MessagingLinearLayout$LayoutParams
    //   203: astore #17
    //   205: aconst_null
    //   206: astore #18
    //   208: aload_0
    //   209: getfield mSpacing : I
    //   212: istore #6
    //   214: iconst_0
    //   215: istore #5
    //   217: iconst_0
    //   218: istore #19
    //   220: aload #16
    //   222: instanceof com/android/internal/widget/MessagingLinearLayout$MessagingChild
    //   225: ifeq -> 332
    //   228: aload #9
    //   230: ifnull -> 297
    //   233: aload #9
    //   235: invokeinterface hasDifferentHeightWhenFirst : ()Z
    //   240: ifeq -> 297
    //   243: aload #9
    //   245: iconst_0
    //   246: invokeinterface setIsFirstInLayout : (Z)V
    //   251: aload_0
    //   252: aload #8
    //   254: iload_1
    //   255: iconst_0
    //   256: iload_2
    //   257: iload #13
    //   259: iload #12
    //   261: isub
    //   262: invokevirtual measureChildWithMargins : (Landroid/view/View;IIII)V
    //   265: aload #8
    //   267: invokevirtual getMeasuredHeight : ()I
    //   270: istore #5
    //   272: iload #11
    //   274: aload #9
    //   276: invokeinterface getConsumedLines : ()I
    //   281: iload #14
    //   283: isub
    //   284: isub
    //   285: istore #11
    //   287: iload #5
    //   289: iload #12
    //   291: isub
    //   292: istore #5
    //   294: goto -> 301
    //   297: iload #19
    //   299: istore #5
    //   301: aload #16
    //   303: checkcast com/android/internal/widget/MessagingLinearLayout$MessagingChild
    //   306: astore #18
    //   308: aload #18
    //   310: iload #11
    //   312: invokeinterface setMaxDisplayedLines : (I)V
    //   317: iload #6
    //   319: aload #18
    //   321: invokeinterface getExtraSpacing : ()I
    //   326: iadd
    //   327: istore #6
    //   329: goto -> 332
    //   332: iload #10
    //   334: ifeq -> 343
    //   337: iconst_0
    //   338: istore #6
    //   340: goto -> 343
    //   343: aload_0
    //   344: aload #16
    //   346: iload_1
    //   347: iconst_0
    //   348: iload_2
    //   349: iload #7
    //   351: aload_0
    //   352: getfield mPaddingTop : I
    //   355: isub
    //   356: aload_0
    //   357: getfield mPaddingBottom : I
    //   360: isub
    //   361: iload #6
    //   363: iadd
    //   364: invokevirtual measureChildWithMargins : (Landroid/view/View;IIII)V
    //   367: aload #16
    //   369: invokevirtual getMeasuredHeight : ()I
    //   372: istore #20
    //   374: iload #7
    //   376: iload #7
    //   378: iload #20
    //   380: iadd
    //   381: aload #17
    //   383: getfield topMargin : I
    //   386: iadd
    //   387: aload #17
    //   389: getfield bottomMargin : I
    //   392: iadd
    //   393: iload #6
    //   395: iadd
    //   396: iload #5
    //   398: iadd
    //   399: invokestatic max : (II)I
    //   402: istore #19
    //   404: aload #18
    //   406: ifnull -> 421
    //   409: aload #18
    //   411: invokeinterface getMeasuredType : ()I
    //   416: istore #6
    //   418: goto -> 424
    //   421: iconst_0
    //   422: istore #6
    //   424: iload #6
    //   426: iconst_2
    //   427: if_icmpne -> 441
    //   430: iload #10
    //   432: ifne -> 441
    //   435: iconst_1
    //   436: istore #5
    //   438: goto -> 444
    //   441: iconst_0
    //   442: istore #5
    //   444: iload #6
    //   446: iconst_1
    //   447: if_icmpeq -> 470
    //   450: iload #6
    //   452: iconst_2
    //   453: if_icmpne -> 464
    //   456: iload #10
    //   458: ifeq -> 464
    //   461: goto -> 470
    //   464: iconst_0
    //   465: istore #6
    //   467: goto -> 473
    //   470: iconst_1
    //   471: istore #6
    //   473: iload #19
    //   475: iload_3
    //   476: if_icmpgt -> 490
    //   479: iload #5
    //   481: ifne -> 490
    //   484: iconst_1
    //   485: istore #5
    //   487: goto -> 493
    //   490: iconst_0
    //   491: istore #5
    //   493: iload #5
    //   495: ifeq -> 645
    //   498: iload #11
    //   500: istore #5
    //   502: aload #18
    //   504: ifnull -> 539
    //   507: aload #18
    //   509: invokeinterface getConsumedLines : ()I
    //   514: istore #14
    //   516: iload #11
    //   518: iload #14
    //   520: isub
    //   521: istore #5
    //   523: aload #18
    //   525: astore #9
    //   527: aload #16
    //   529: astore #8
    //   531: iload #20
    //   533: istore #12
    //   535: iload #7
    //   537: istore #13
    //   539: iload #19
    //   541: istore #7
    //   543: aload #16
    //   545: invokevirtual getMeasuredWidth : ()I
    //   548: istore #11
    //   550: aload #17
    //   552: getfield leftMargin : I
    //   555: istore #20
    //   557: aload #17
    //   559: getfield rightMargin : I
    //   562: istore #19
    //   564: aload_0
    //   565: getfield mPaddingLeft : I
    //   568: istore #21
    //   570: aload_0
    //   571: getfield mPaddingRight : I
    //   574: istore #10
    //   576: iload #4
    //   578: iload #11
    //   580: iload #20
    //   582: iadd
    //   583: iload #19
    //   585: iadd
    //   586: iload #21
    //   588: iadd
    //   589: iload #10
    //   591: iadd
    //   592: invokestatic max : (II)I
    //   595: istore #4
    //   597: aload #17
    //   599: iconst_0
    //   600: putfield hide : Z
    //   603: iload #4
    //   605: istore #10
    //   607: iload #7
    //   609: istore #11
    //   611: iload #6
    //   613: ifne -> 712
    //   616: iload #5
    //   618: ifgt -> 632
    //   621: iload #4
    //   623: istore #10
    //   625: iload #7
    //   627: istore #11
    //   629: goto -> 712
    //   632: iconst_0
    //   633: istore #10
    //   635: iload #5
    //   637: istore #11
    //   639: iinc #15, -1
    //   642: goto -> 155
    //   645: aload #9
    //   647: ifnull -> 693
    //   650: aload #9
    //   652: invokeinterface hasDifferentHeightWhenFirst : ()Z
    //   657: ifeq -> 693
    //   660: aload #9
    //   662: iconst_1
    //   663: invokeinterface setIsFirstInLayout : (Z)V
    //   668: aload_0
    //   669: aload #8
    //   671: iload_1
    //   672: iconst_0
    //   673: iload_2
    //   674: iload #13
    //   676: iload #12
    //   678: isub
    //   679: invokevirtual measureChildWithMargins : (Landroid/view/View;IIII)V
    //   682: iload #4
    //   684: istore #10
    //   686: iload #7
    //   688: istore #11
    //   690: goto -> 712
    //   693: iload #4
    //   695: istore #10
    //   697: iload #7
    //   699: istore #11
    //   701: goto -> 712
    //   704: iload #7
    //   706: istore #11
    //   708: iload #4
    //   710: istore #10
    //   712: aload_0
    //   713: invokevirtual getSuggestedMinimumWidth : ()I
    //   716: iload #10
    //   718: invokestatic max : (II)I
    //   721: iload_1
    //   722: invokestatic resolveSize : (II)I
    //   725: istore_1
    //   726: aload_0
    //   727: invokevirtual getSuggestedMinimumHeight : ()I
    //   730: iload #11
    //   732: invokestatic max : (II)I
    //   735: istore_2
    //   736: aload_0
    //   737: iload_1
    //   738: iload_2
    //   739: invokevirtual setMeasuredDimension : (II)V
    //   742: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #71	-> 0
    //   #72	-> 5
    //   #74	-> 15
    //   #79	-> 18
    //   #80	-> 30
    //   #82	-> 36
    //   #83	-> 46
    //   #84	-> 54
    //   #85	-> 64
    //   #86	-> 70
    //   #87	-> 78
    //   #89	-> 85
    //   #82	-> 93
    //   #93	-> 99
    //   #94	-> 111
    //   #95	-> 111
    //   #98	-> 117
    //   #99	-> 117
    //   #100	-> 117
    //   #101	-> 117
    //   #102	-> 117
    //   #103	-> 117
    //   #104	-> 166
    //   #105	-> 180
    //   #107	-> 183
    //   #108	-> 191
    //   #109	-> 205
    //   #110	-> 208
    //   #111	-> 214
    //   #112	-> 220
    //   #114	-> 228
    //   #115	-> 243
    //   #116	-> 251
    //   #118	-> 265
    //   #119	-> 272
    //   #114	-> 297
    //   #121	-> 301
    //   #122	-> 308
    //   #123	-> 317
    //   #112	-> 332
    //   #125	-> 332
    //   #126	-> 343
    //   #129	-> 367
    //   #130	-> 374
    //   #132	-> 404
    //   #133	-> 404
    //   #134	-> 409
    //   #133	-> 421
    //   #138	-> 424
    //   #139	-> 444
    //   #141	-> 473
    //   #142	-> 493
    //   #143	-> 498
    //   #144	-> 507
    //   #145	-> 516
    //   #146	-> 523
    //   #147	-> 527
    //   #148	-> 531
    //   #149	-> 535
    //   #151	-> 539
    //   #152	-> 543
    //   #153	-> 543
    //   #152	-> 576
    //   #155	-> 597
    //   #156	-> 603
    //   #157	-> 621
    //   #172	-> 632
    //   #103	-> 639
    //   #162	-> 645
    //   #163	-> 660
    //   #165	-> 668
    //   #162	-> 693
    //   #103	-> 704
    //   #175	-> 712
    //   #176	-> 712
    //   #178	-> 726
    //   #175	-> 736
    //   #179	-> 742
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    paramInt4 = this.mPaddingLeft;
    int i = paramInt3 - paramInt1;
    int j = this.mPaddingRight;
    int k = getLayoutDirection();
    int m = getChildCount();
    paramInt1 = this.mPaddingTop;
    boolean bool = true;
    paramBoolean = isShown();
    for (paramInt3 = 0, paramInt2 = i; paramInt3 < m; paramInt3++) {
      View view = getChildAt(paramInt3);
      if (view.getVisibility() != 8) {
        int i2;
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        MessagingChild messagingChild = (MessagingChild)view;
        int n = view.getMeasuredWidth();
        int i1 = view.getMeasuredHeight();
        if (k == 1) {
          i2 = i - j - n - layoutParams.rightMargin;
        } else {
          i2 = paramInt4 + layoutParams.leftMargin;
        } 
        if (layoutParams.hide) {
          if (paramBoolean && layoutParams.visibleBefore) {
            view.layout(i2, paramInt1, i2 + n, layoutParams.lastVisibleHeight + paramInt1);
            messagingChild.hideAnimated();
          } 
          layoutParams.visibleBefore = false;
        } else {
          layoutParams.visibleBefore = true;
          layoutParams.lastVisibleHeight = i1;
          int i3 = paramInt1;
          if (!bool)
            i3 = paramInt1 + this.mSpacing; 
          paramInt1 = i3 + layoutParams.topMargin;
          view.layout(i2, paramInt1, i2 + n, paramInt1 + i1);
          paramInt1 += layoutParams.bottomMargin + i1;
          bool = false;
        } 
      } 
    } 
  }
  
  protected boolean drawChild(Canvas paramCanvas, View paramView, long paramLong) {
    LayoutParams layoutParams = (LayoutParams)paramView.getLayoutParams();
    if (layoutParams.hide) {
      MessagingChild messagingChild = (MessagingChild)paramView;
      if (!messagingChild.isHidingAnimated())
        return true; 
    } 
    return super.drawChild(paramCanvas, paramView, paramLong);
  }
  
  public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet) {
    return new LayoutParams(this.mContext, paramAttributeSet);
  }
  
  protected LayoutParams generateDefaultLayoutParams() {
    return new LayoutParams(-1, -2);
  }
  
  protected LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    LayoutParams layoutParams = new LayoutParams(paramLayoutParams.width, paramLayoutParams.height);
    if (paramLayoutParams instanceof ViewGroup.MarginLayoutParams)
      layoutParams.copyMarginsFrom((ViewGroup.MarginLayoutParams)paramLayoutParams); 
    return layoutParams;
  }
  
  public static boolean isGone(View paramView) {
    if (paramView.getVisibility() == 8)
      return true; 
    ViewGroup.LayoutParams layoutParams = paramView.getLayoutParams();
    if (layoutParams instanceof LayoutParams && ((LayoutParams)layoutParams).hide)
      return true; 
    return false;
  }
  
  @RemotableViewMethod
  public void setMaxDisplayedLines(int paramInt) {
    this.mMaxDisplayedLines = paramInt;
  }
  
  public IMessagingLayout getMessagingLayout() {
    MessagingLinearLayout messagingLinearLayout = this;
    while (true) {
      ViewParent viewParent = messagingLinearLayout.getParent();
      if (viewParent instanceof View) {
        View view = (View)viewParent;
        if (view instanceof IMessagingLayout)
          return (IMessagingLayout)view; 
        continue;
      } 
      break;
    } 
    return null;
  }
  
  public int getBaseline() {
    int i = getChildCount();
    while (--i >= 0) {
      View view = getChildAt(i);
      if (isGone(view)) {
        i--;
        continue;
      } 
      i = view.getBaseline();
      if (i == -1)
        return -1; 
      ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams)view.getLayoutParams();
      return marginLayoutParams.topMargin + i;
    } 
    return super.getBaseline();
  }
  
  class MessagingChild {
    public static final int MEASURED_NORMAL = 0;
    
    public static final int MEASURED_SHORTENED = 1;
    
    public static final int MEASURED_TOO_SMALL = 2;
    
    public abstract void setMaxDisplayedLines(int param1Int);
    
    public void setIsFirstInLayout(boolean param1Boolean) {}
    
    public abstract boolean isHidingAnimated();
    
    public abstract void hideAnimated();
    
    public boolean hasDifferentHeightWhenFirst() {
      return false;
    }
    
    public abstract int getMeasuredType();
    
    public int getExtraSpacing() {
      return 0;
    }
    
    public abstract int getConsumedLines();
  }
  
  class LayoutParams extends ViewGroup.MarginLayoutParams {
    public boolean hide = false;
    
    public int lastVisibleHeight;
    
    public boolean visibleBefore = false;
    
    public LayoutParams(MessagingLinearLayout this$0, AttributeSet param1AttributeSet) {
      super((Context)this$0, param1AttributeSet);
    }
    
    public LayoutParams(MessagingLinearLayout this$0, int param1Int1) {
      super(this$0, param1Int1);
    }
  }
}
