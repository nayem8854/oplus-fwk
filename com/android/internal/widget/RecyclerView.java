package com.android.internal.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.Observable;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.os.Trace;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.AbsSavedState;
import android.view.Display;
import android.view.FocusFinder;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.Interpolator;
import android.widget.EdgeEffect;
import android.widget.OverScroller;
import com.android.internal.R;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RecyclerView extends ViewGroup implements ScrollingView, NestedScrollingChild {
  static final boolean ALLOW_SIZE_IN_UNSPECIFIED_SPEC;
  
  private static final boolean ALLOW_THREAD_GAP_WORK;
  
  private static final int[] CLIP_TO_PADDING_ATTR;
  
  static final boolean DEBUG = false;
  
  static final boolean DISPATCH_TEMP_DETACH = false;
  
  private static final boolean FORCE_ABS_FOCUS_SEARCH_DIRECTION;
  
  static final boolean FORCE_INVALIDATE_DISPLAY_LIST;
  
  static final long FOREVER_NS = 9223372036854775807L;
  
  public static final int HORIZONTAL = 0;
  
  private static final boolean IGNORE_DETACHED_FOCUSED_CHILD;
  
  private static final int INVALID_POINTER = -1;
  
  public static final int INVALID_TYPE = -1;
  
  private static final Class<?>[] LAYOUT_MANAGER_CONSTRUCTOR_SIGNATURE;
  
  static final int MAX_SCROLL_DURATION = 2000;
  
  static {
    boolean bool;
  }
  
  private static final int[] NESTED_SCROLLING_ATTRS = new int[] { 16843830 };
  
  public static final long NO_ID = -1L;
  
  public static final int NO_POSITION = -1;
  
  static final boolean POST_UPDATES_ON_ANIMATION;
  
  public static final int SCROLL_STATE_DRAGGING = 1;
  
  public static final int SCROLL_STATE_IDLE = 0;
  
  public static final int SCROLL_STATE_SETTLING = 2;
  
  static final String TAG = "RecyclerView";
  
  public static final int TOUCH_SLOP_DEFAULT = 0;
  
  public static final int TOUCH_SLOP_PAGING = 1;
  
  static final String TRACE_BIND_VIEW_TAG = "RV OnBindView";
  
  static final String TRACE_CREATE_VIEW_TAG = "RV CreateView";
  
  private static final String TRACE_HANDLE_ADAPTER_UPDATES_TAG = "RV PartialInvalidate";
  
  static final String TRACE_NESTED_PREFETCH_TAG = "RV Nested Prefetch";
  
  private static final String TRACE_ON_DATA_SET_CHANGE_LAYOUT_TAG = "RV FullInvalidate";
  
  private static final String TRACE_ON_LAYOUT_TAG = "RV OnLayout";
  
  static final String TRACE_PREFETCH_TAG = "RV Prefetch";
  
  static final String TRACE_SCROLL_TAG = "RV Scroll";
  
  public static final int VERTICAL = 1;
  
  static final Interpolator sQuinticInterpolator;
  
  RecyclerViewAccessibilityDelegate mAccessibilityDelegate;
  
  private final AccessibilityManager mAccessibilityManager;
  
  private OnItemTouchListener mActiveOnItemTouchListener;
  
  Adapter mAdapter;
  
  AdapterHelper mAdapterHelper;
  
  boolean mAdapterUpdateDuringMeasure;
  
  private EdgeEffect mBottomGlow;
  
  private ChildDrawingOrderCallback mChildDrawingOrderCallback;
  
  ChildHelper mChildHelper;
  
  boolean mClipToPadding;
  
  boolean mDataSetHasChangedAfterLayout;
  
  private int mDispatchScrollCounter;
  
  private int mEatRequestLayout;
  
  private int mEatenAccessibilityChangeFlags;
  
  boolean mFirstLayoutComplete;
  
  GapWorker mGapWorker;
  
  boolean mHasFixedSize;
  
  private boolean mIgnoreMotionEventTillDown;
  
  private int mInitialTouchX;
  
  private int mInitialTouchY;
  
  boolean mIsAttached;
  
  ItemAnimator mItemAnimator;
  
  private ItemAnimator.ItemAnimatorListener mItemAnimatorListener;
  
  private Runnable mItemAnimatorRunner;
  
  final ArrayList<ItemDecoration> mItemDecorations;
  
  boolean mItemsAddedOrRemoved;
  
  boolean mItemsChanged;
  
  private int mLastTouchX;
  
  private int mLastTouchY;
  
  LayoutManager mLayout;
  
  boolean mLayoutFrozen;
  
  private int mLayoutOrScrollCounter;
  
  boolean mLayoutRequestEaten;
  
  private EdgeEffect mLeftGlow;
  
  private final int mMaxFlingVelocity;
  
  private final int mMinFlingVelocity;
  
  private final int[] mMinMaxLayoutPositions;
  
  private final int[] mNestedOffsets;
  
  private final RecyclerViewDataObserver mObserver;
  
  private List<OnChildAttachStateChangeListener> mOnChildAttachStateListeners;
  
  private OnFlingListener mOnFlingListener;
  
  private final ArrayList<OnItemTouchListener> mOnItemTouchListeners;
  
  final List<ViewHolder> mPendingAccessibilityImportanceChange;
  
  private SavedState mPendingSavedState;
  
  boolean mPostedAnimatorRunner;
  
  GapWorker.LayoutPrefetchRegistryImpl mPrefetchRegistry;
  
  private boolean mPreserveFocusAfterLayout;
  
  final Recycler mRecycler;
  
  RecyclerListener mRecyclerListener;
  
  private EdgeEffect mRightGlow;
  
  private final int[] mScrollConsumed;
  
  private float mScrollFactor;
  
  private OnScrollListener mScrollListener;
  
  private List<OnScrollListener> mScrollListeners;
  
  private final int[] mScrollOffset;
  
  private int mScrollPointerId;
  
  private int mScrollState;
  
  final State mState;
  
  final Rect mTempRect;
  
  private final Rect mTempRect2;
  
  final RectF mTempRectF;
  
  private EdgeEffect mTopGlow;
  
  private int mTouchSlop;
  
  final Runnable mUpdateChildViewsRunnable;
  
  private VelocityTracker mVelocityTracker;
  
  final ViewFlinger mViewFlinger;
  
  private final ViewInfoStore.ProcessCallback mViewInfoProcessCallback;
  
  final ViewInfoStore mViewInfoStore;
  
  static {
    CLIP_TO_PADDING_ATTR = new int[] { 16842987 };
    if (Build.VERSION.SDK_INT == 18 || Build.VERSION.SDK_INT == 19 || Build.VERSION.SDK_INT == 20) {
      bool = true;
    } else {
      bool = false;
    } 
    FORCE_INVALIDATE_DISPLAY_LIST = bool;
    if (Build.VERSION.SDK_INT >= 23) {
      bool = true;
    } else {
      bool = false;
    } 
    ALLOW_SIZE_IN_UNSPECIFIED_SPEC = bool;
    if (Build.VERSION.SDK_INT >= 16) {
      bool = true;
    } else {
      bool = false;
    } 
    POST_UPDATES_ON_ANIMATION = bool;
    if (Build.VERSION.SDK_INT >= 21) {
      bool = true;
    } else {
      bool = false;
    } 
    ALLOW_THREAD_GAP_WORK = bool;
    if (Build.VERSION.SDK_INT <= 15) {
      bool = true;
    } else {
      bool = false;
    } 
    FORCE_ABS_FOCUS_SEARCH_DIRECTION = bool;
    if (Build.VERSION.SDK_INT <= 15) {
      bool = true;
    } else {
      bool = false;
    } 
    IGNORE_DETACHED_FOCUSED_CHILD = bool;
    LAYOUT_MANAGER_CONSTRUCTOR_SIGNATURE = new Class[] { Context.class, AttributeSet.class, int.class, int.class };
    sQuinticInterpolator = (Interpolator)new Object();
  }
  
  public RecyclerView(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public RecyclerView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public RecyclerView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
    GapWorker.LayoutPrefetchRegistryImpl layoutPrefetchRegistryImpl;
    this.mObserver = new RecyclerViewDataObserver();
    this.mRecycler = new Recycler();
    this.mViewInfoStore = new ViewInfoStore();
    this.mUpdateChildViewsRunnable = (Runnable)new Object(this);
    this.mTempRect = new Rect();
    this.mTempRect2 = new Rect();
    this.mTempRectF = new RectF();
    this.mItemDecorations = new ArrayList<>();
    this.mOnItemTouchListeners = new ArrayList<>();
    this.mEatRequestLayout = 0;
    this.mDataSetHasChangedAfterLayout = false;
    this.mLayoutOrScrollCounter = 0;
    this.mDispatchScrollCounter = 0;
    this.mItemAnimator = new DefaultItemAnimator();
    this.mScrollState = 0;
    this.mScrollPointerId = -1;
    this.mScrollFactor = Float.MIN_VALUE;
    this.mPreserveFocusAfterLayout = true;
    this.mViewFlinger = new ViewFlinger();
    if (ALLOW_THREAD_GAP_WORK) {
      layoutPrefetchRegistryImpl = new GapWorker.LayoutPrefetchRegistryImpl();
    } else {
      layoutPrefetchRegistryImpl = null;
    } 
    this.mPrefetchRegistry = layoutPrefetchRegistryImpl;
    this.mState = new State();
    this.mItemsAddedOrRemoved = false;
    this.mItemsChanged = false;
    this.mItemAnimatorListener = new ItemAnimatorRestoreListener();
    this.mPostedAnimatorRunner = false;
    this.mMinMaxLayoutPositions = new int[2];
    this.mScrollOffset = new int[2];
    this.mScrollConsumed = new int[2];
    this.mNestedOffsets = new int[2];
    this.mPendingAccessibilityImportanceChange = new ArrayList<>();
    this.mItemAnimatorRunner = (Runnable)new Object(this);
    this.mViewInfoProcessCallback = (ViewInfoStore.ProcessCallback)new Object(this);
    if (paramAttributeSet != null) {
      TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, CLIP_TO_PADDING_ATTR, paramInt, 0);
      this.mClipToPadding = typedArray.getBoolean(0, true);
      typedArray.recycle();
    } else {
      this.mClipToPadding = true;
    } 
    setScrollContainer(true);
    setFocusableInTouchMode(true);
    ViewConfiguration viewConfiguration = ViewConfiguration.get(paramContext);
    this.mTouchSlop = viewConfiguration.getScaledTouchSlop();
    this.mMinFlingVelocity = viewConfiguration.getScaledMinimumFlingVelocity();
    this.mMaxFlingVelocity = viewConfiguration.getScaledMaximumFlingVelocity();
    if (getOverScrollMode() == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    setWillNotDraw(bool);
    this.mItemAnimator.setListener(this.mItemAnimatorListener);
    initAdapterManager();
    initChildrenHelper();
    if (getImportantForAccessibility() == 0)
      setImportantForAccessibility(1); 
    Context context = getContext();
    this.mAccessibilityManager = (AccessibilityManager)context.getSystemService("accessibility");
    setAccessibilityDelegateCompat(new RecyclerViewAccessibilityDelegate(this));
    boolean bool1 = true;
    boolean bool = true;
    if (paramAttributeSet != null) {
      TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.RecyclerView, paramInt, 0);
      String str = typedArray.getString(2);
      int i = typedArray.getInt(1, -1);
      if (i == -1)
        setDescendantFocusability(262144); 
      typedArray.recycle();
      createLayoutManager(paramContext, str, paramAttributeSet, paramInt, 0);
      if (Build.VERSION.SDK_INT >= 21) {
        TypedArray typedArray1 = paramContext.obtainStyledAttributes(paramAttributeSet, NESTED_SCROLLING_ATTRS, paramInt, 0);
        bool = typedArray1.getBoolean(0, true);
        typedArray1.recycle();
      } 
    } else {
      setDescendantFocusability(262144);
      bool = bool1;
    } 
    setNestedScrollingEnabled(bool);
  }
  
  public RecyclerViewAccessibilityDelegate getCompatAccessibilityDelegate() {
    return this.mAccessibilityDelegate;
  }
  
  public void setAccessibilityDelegateCompat(RecyclerViewAccessibilityDelegate paramRecyclerViewAccessibilityDelegate) {
    this.mAccessibilityDelegate = paramRecyclerViewAccessibilityDelegate;
    setAccessibilityDelegate(paramRecyclerViewAccessibilityDelegate);
  }
  
  private void createLayoutManager(Context paramContext, String paramString, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_2
    //   1: ifnull -> 442
    //   4: aload_2
    //   5: invokevirtual trim : ()Ljava/lang/String;
    //   8: astore_2
    //   9: aload_2
    //   10: invokevirtual length : ()I
    //   13: ifeq -> 442
    //   16: aload_0
    //   17: aload_1
    //   18: aload_2
    //   19: invokespecial getFullClassName : (Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    //   22: astore #6
    //   24: aload_0
    //   25: invokevirtual isInEditMode : ()Z
    //   28: ifeq -> 42
    //   31: aload_0
    //   32: invokevirtual getClass : ()Ljava/lang/Class;
    //   35: invokevirtual getClassLoader : ()Ljava/lang/ClassLoader;
    //   38: astore_2
    //   39: goto -> 47
    //   42: aload_1
    //   43: invokevirtual getClassLoader : ()Ljava/lang/ClassLoader;
    //   46: astore_2
    //   47: aload_2
    //   48: aload #6
    //   50: invokevirtual loadClass : (Ljava/lang/String;)Ljava/lang/Class;
    //   53: ldc com/android/internal/widget/RecyclerView$LayoutManager
    //   55: invokevirtual asSubclass : (Ljava/lang/Class;)Ljava/lang/Class;
    //   58: astore #7
    //   60: aconst_null
    //   61: astore #8
    //   63: getstatic com/android/internal/widget/RecyclerView.LAYOUT_MANAGER_CONSTRUCTOR_SIGNATURE : [Ljava/lang/Class;
    //   66: astore_2
    //   67: aload #7
    //   69: aload_2
    //   70: invokevirtual getConstructor : ([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
    //   73: astore_2
    //   74: iconst_4
    //   75: anewarray java/lang/Object
    //   78: dup
    //   79: iconst_0
    //   80: aload_1
    //   81: aastore
    //   82: dup
    //   83: iconst_1
    //   84: aload_3
    //   85: aastore
    //   86: dup
    //   87: iconst_2
    //   88: iload #4
    //   90: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   93: aastore
    //   94: dup
    //   95: iconst_3
    //   96: iload #5
    //   98: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   101: aastore
    //   102: astore_1
    //   103: goto -> 120
    //   106: astore_1
    //   107: aload #7
    //   109: iconst_0
    //   110: anewarray java/lang/Class
    //   113: invokevirtual getConstructor : ([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
    //   116: astore_2
    //   117: aload #8
    //   119: astore_1
    //   120: aload_2
    //   121: iconst_1
    //   122: invokevirtual setAccessible : (Z)V
    //   125: aload_0
    //   126: aload_2
    //   127: aload_1
    //   128: invokevirtual newInstance : ([Ljava/lang/Object;)Ljava/lang/Object;
    //   131: checkcast com/android/internal/widget/RecyclerView$LayoutManager
    //   134: invokevirtual setLayoutManager : (Lcom/android/internal/widget/RecyclerView$LayoutManager;)V
    //   137: goto -> 442
    //   140: astore_2
    //   141: aload_2
    //   142: aload_1
    //   143: invokevirtual initCause : (Ljava/lang/Throwable;)Ljava/lang/Throwable;
    //   146: pop
    //   147: new java/lang/IllegalStateException
    //   150: astore_1
    //   151: new java/lang/StringBuilder
    //   154: astore #8
    //   156: aload #8
    //   158: invokespecial <init> : ()V
    //   161: aload #8
    //   163: aload_3
    //   164: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   169: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   172: pop
    //   173: aload #8
    //   175: ldc_w ': Error creating LayoutManager '
    //   178: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   181: pop
    //   182: aload #8
    //   184: aload #6
    //   186: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   189: pop
    //   190: aload_1
    //   191: aload #8
    //   193: invokevirtual toString : ()Ljava/lang/String;
    //   196: aload_2
    //   197: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   200: aload_1
    //   201: athrow
    //   202: astore_2
    //   203: new java/lang/StringBuilder
    //   206: dup
    //   207: invokespecial <init> : ()V
    //   210: astore_1
    //   211: aload_1
    //   212: aload_3
    //   213: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   218: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   221: pop
    //   222: aload_1
    //   223: ldc_w ': Class is not a LayoutManager '
    //   226: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   229: pop
    //   230: aload_1
    //   231: aload #6
    //   233: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   236: pop
    //   237: new java/lang/IllegalStateException
    //   240: dup
    //   241: aload_1
    //   242: invokevirtual toString : ()Ljava/lang/String;
    //   245: aload_2
    //   246: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   249: athrow
    //   250: astore_2
    //   251: new java/lang/StringBuilder
    //   254: dup
    //   255: invokespecial <init> : ()V
    //   258: astore_1
    //   259: aload_1
    //   260: aload_3
    //   261: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   266: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   269: pop
    //   270: aload_1
    //   271: ldc_w ': Cannot access non-public constructor '
    //   274: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   277: pop
    //   278: aload_1
    //   279: aload #6
    //   281: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   284: pop
    //   285: new java/lang/IllegalStateException
    //   288: dup
    //   289: aload_1
    //   290: invokevirtual toString : ()Ljava/lang/String;
    //   293: aload_2
    //   294: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   297: athrow
    //   298: astore_1
    //   299: new java/lang/StringBuilder
    //   302: dup
    //   303: invokespecial <init> : ()V
    //   306: astore_2
    //   307: aload_2
    //   308: aload_3
    //   309: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   314: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   317: pop
    //   318: aload_2
    //   319: ldc_w ': Could not instantiate the LayoutManager: '
    //   322: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   325: pop
    //   326: aload_2
    //   327: aload #6
    //   329: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   332: pop
    //   333: new java/lang/IllegalStateException
    //   336: dup
    //   337: aload_2
    //   338: invokevirtual toString : ()Ljava/lang/String;
    //   341: aload_1
    //   342: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   345: athrow
    //   346: astore_2
    //   347: new java/lang/StringBuilder
    //   350: dup
    //   351: invokespecial <init> : ()V
    //   354: astore_1
    //   355: aload_1
    //   356: aload_3
    //   357: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   362: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   365: pop
    //   366: aload_1
    //   367: ldc_w ': Could not instantiate the LayoutManager: '
    //   370: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   373: pop
    //   374: aload_1
    //   375: aload #6
    //   377: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   380: pop
    //   381: new java/lang/IllegalStateException
    //   384: dup
    //   385: aload_1
    //   386: invokevirtual toString : ()Ljava/lang/String;
    //   389: aload_2
    //   390: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   393: athrow
    //   394: astore_1
    //   395: new java/lang/StringBuilder
    //   398: dup
    //   399: invokespecial <init> : ()V
    //   402: astore_2
    //   403: aload_2
    //   404: aload_3
    //   405: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   410: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   413: pop
    //   414: aload_2
    //   415: ldc_w ': Unable to find LayoutManager '
    //   418: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   421: pop
    //   422: aload_2
    //   423: aload #6
    //   425: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   428: pop
    //   429: new java/lang/IllegalStateException
    //   432: dup
    //   433: aload_2
    //   434: invokevirtual toString : ()Ljava/lang/String;
    //   437: aload_1
    //   438: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   441: athrow
    //   442: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #617	-> 0
    //   #618	-> 4
    //   #619	-> 9
    //   #620	-> 16
    //   #623	-> 24
    //   #625	-> 31
    //   #627	-> 42
    //   #629	-> 47
    //   #630	-> 47
    //   #632	-> 60
    //   #634	-> 63
    //   #635	-> 67
    //   #636	-> 74
    //   #645	-> 103
    //   #637	-> 106
    //   #639	-> 107
    //   #644	-> 117
    //   #646	-> 120
    //   #647	-> 125
    //   #663	-> 137
    //   #640	-> 140
    //   #641	-> 141
    //   #642	-> 147
    //   #660	-> 202
    //   #661	-> 203
    //   #657	-> 250
    //   #658	-> 251
    //   #654	-> 298
    //   #655	-> 299
    //   #651	-> 346
    //   #652	-> 347
    //   #648	-> 394
    //   #649	-> 395
    //   #666	-> 442
    // Exception table:
    //   from	to	target	type
    //   24	31	394	java/lang/ClassNotFoundException
    //   24	31	346	java/lang/reflect/InvocationTargetException
    //   24	31	298	java/lang/InstantiationException
    //   24	31	250	java/lang/IllegalAccessException
    //   24	31	202	java/lang/ClassCastException
    //   31	39	394	java/lang/ClassNotFoundException
    //   31	39	346	java/lang/reflect/InvocationTargetException
    //   31	39	298	java/lang/InstantiationException
    //   31	39	250	java/lang/IllegalAccessException
    //   31	39	202	java/lang/ClassCastException
    //   42	47	394	java/lang/ClassNotFoundException
    //   42	47	346	java/lang/reflect/InvocationTargetException
    //   42	47	298	java/lang/InstantiationException
    //   42	47	250	java/lang/IllegalAccessException
    //   42	47	202	java/lang/ClassCastException
    //   47	60	394	java/lang/ClassNotFoundException
    //   47	60	346	java/lang/reflect/InvocationTargetException
    //   47	60	298	java/lang/InstantiationException
    //   47	60	250	java/lang/IllegalAccessException
    //   47	60	202	java/lang/ClassCastException
    //   63	67	106	java/lang/NoSuchMethodException
    //   63	67	394	java/lang/ClassNotFoundException
    //   63	67	346	java/lang/reflect/InvocationTargetException
    //   63	67	298	java/lang/InstantiationException
    //   63	67	250	java/lang/IllegalAccessException
    //   63	67	202	java/lang/ClassCastException
    //   67	74	106	java/lang/NoSuchMethodException
    //   67	74	394	java/lang/ClassNotFoundException
    //   67	74	346	java/lang/reflect/InvocationTargetException
    //   67	74	298	java/lang/InstantiationException
    //   67	74	250	java/lang/IllegalAccessException
    //   67	74	202	java/lang/ClassCastException
    //   107	117	140	java/lang/NoSuchMethodException
    //   107	117	394	java/lang/ClassNotFoundException
    //   107	117	346	java/lang/reflect/InvocationTargetException
    //   107	117	298	java/lang/InstantiationException
    //   107	117	250	java/lang/IllegalAccessException
    //   107	117	202	java/lang/ClassCastException
    //   120	125	394	java/lang/ClassNotFoundException
    //   120	125	346	java/lang/reflect/InvocationTargetException
    //   120	125	298	java/lang/InstantiationException
    //   120	125	250	java/lang/IllegalAccessException
    //   120	125	202	java/lang/ClassCastException
    //   125	137	394	java/lang/ClassNotFoundException
    //   125	137	346	java/lang/reflect/InvocationTargetException
    //   125	137	298	java/lang/InstantiationException
    //   125	137	250	java/lang/IllegalAccessException
    //   125	137	202	java/lang/ClassCastException
    //   141	147	394	java/lang/ClassNotFoundException
    //   141	147	346	java/lang/reflect/InvocationTargetException
    //   141	147	298	java/lang/InstantiationException
    //   141	147	250	java/lang/IllegalAccessException
    //   141	147	202	java/lang/ClassCastException
    //   147	202	394	java/lang/ClassNotFoundException
    //   147	202	346	java/lang/reflect/InvocationTargetException
    //   147	202	298	java/lang/InstantiationException
    //   147	202	250	java/lang/IllegalAccessException
    //   147	202	202	java/lang/ClassCastException
  }
  
  private String getFullClassName(Context paramContext, String paramString) {
    if (paramString.charAt(0) == '.') {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramContext.getPackageName());
      stringBuilder1.append(paramString);
      return stringBuilder1.toString();
    } 
    if (paramString.contains("."))
      return paramString; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(RecyclerView.class.getPackage().getName());
    stringBuilder.append('.');
    stringBuilder.append(paramString);
    return stringBuilder.toString();
  }
  
  private void initChildrenHelper() {
    this.mChildHelper = new ChildHelper((ChildHelper.Callback)new Object(this));
  }
  
  void initAdapterManager() {
    this.mAdapterHelper = new AdapterHelper((AdapterHelper.Callback)new Object(this));
  }
  
  public void setHasFixedSize(boolean paramBoolean) {
    this.mHasFixedSize = paramBoolean;
  }
  
  public boolean hasFixedSize() {
    return this.mHasFixedSize;
  }
  
  public void setClipToPadding(boolean paramBoolean) {
    if (paramBoolean != this.mClipToPadding)
      invalidateGlows(); 
    this.mClipToPadding = paramBoolean;
    super.setClipToPadding(paramBoolean);
    if (this.mFirstLayoutComplete)
      requestLayout(); 
  }
  
  public boolean getClipToPadding() {
    return this.mClipToPadding;
  }
  
  public void setScrollingTouchSlop(int paramInt) {
    ViewConfiguration viewConfiguration = ViewConfiguration.get(getContext());
    if (paramInt != 0)
      if (paramInt != 1) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("setScrollingTouchSlop(): bad argument constant ");
        stringBuilder.append(paramInt);
        stringBuilder.append("; using default value");
        Log.w("RecyclerView", stringBuilder.toString());
      } else {
        this.mTouchSlop = viewConfiguration.getScaledPagingTouchSlop();
        return;
      }  
    this.mTouchSlop = viewConfiguration.getScaledTouchSlop();
  }
  
  public void swapAdapter(Adapter paramAdapter, boolean paramBoolean) {
    setLayoutFrozen(false);
    setAdapterInternal(paramAdapter, true, paramBoolean);
    setDataSetChangedAfterLayout();
    requestLayout();
  }
  
  public void setAdapter(Adapter paramAdapter) {
    setLayoutFrozen(false);
    setAdapterInternal(paramAdapter, false, true);
    requestLayout();
  }
  
  void removeAndRecycleViews() {
    ItemAnimator itemAnimator = this.mItemAnimator;
    if (itemAnimator != null)
      itemAnimator.endAnimations(); 
    LayoutManager layoutManager = this.mLayout;
    if (layoutManager != null) {
      layoutManager.removeAndRecycleAllViews(this.mRecycler);
      this.mLayout.removeAndRecycleScrapInt(this.mRecycler);
    } 
    this.mRecycler.clear();
  }
  
  private void setAdapterInternal(Adapter paramAdapter, boolean paramBoolean1, boolean paramBoolean2) {
    Adapter adapter = this.mAdapter;
    if (adapter != null) {
      adapter.unregisterAdapterDataObserver(this.mObserver);
      this.mAdapter.onDetachedFromRecyclerView(this);
    } 
    if (!paramBoolean1 || paramBoolean2)
      removeAndRecycleViews(); 
    this.mAdapterHelper.reset();
    adapter = this.mAdapter;
    this.mAdapter = paramAdapter;
    if (paramAdapter != null) {
      paramAdapter.registerAdapterDataObserver(this.mObserver);
      paramAdapter.onAttachedToRecyclerView(this);
    } 
    LayoutManager layoutManager = this.mLayout;
    if (layoutManager != null)
      layoutManager.onAdapterChanged(adapter, this.mAdapter); 
    this.mRecycler.onAdapterChanged(adapter, this.mAdapter, paramBoolean1);
    this.mState.mStructureChanged = true;
    markKnownViewsInvalid();
  }
  
  public Adapter getAdapter() {
    return this.mAdapter;
  }
  
  public void setRecyclerListener(RecyclerListener paramRecyclerListener) {
    this.mRecyclerListener = paramRecyclerListener;
  }
  
  public int getBaseline() {
    LayoutManager layoutManager = this.mLayout;
    if (layoutManager != null)
      return layoutManager.getBaseline(); 
    return super.getBaseline();
  }
  
  public void addOnChildAttachStateChangeListener(OnChildAttachStateChangeListener paramOnChildAttachStateChangeListener) {
    if (this.mOnChildAttachStateListeners == null)
      this.mOnChildAttachStateListeners = new ArrayList<>(); 
    this.mOnChildAttachStateListeners.add(paramOnChildAttachStateChangeListener);
  }
  
  public void removeOnChildAttachStateChangeListener(OnChildAttachStateChangeListener paramOnChildAttachStateChangeListener) {
    List<OnChildAttachStateChangeListener> list = this.mOnChildAttachStateListeners;
    if (list == null)
      return; 
    list.remove(paramOnChildAttachStateChangeListener);
  }
  
  public void clearOnChildAttachStateChangeListeners() {
    List<OnChildAttachStateChangeListener> list = this.mOnChildAttachStateListeners;
    if (list != null)
      list.clear(); 
  }
  
  public void setLayoutManager(LayoutManager paramLayoutManager) {
    if (paramLayoutManager == this.mLayout)
      return; 
    stopScroll();
    if (this.mLayout != null) {
      ItemAnimator itemAnimator = this.mItemAnimator;
      if (itemAnimator != null)
        itemAnimator.endAnimations(); 
      this.mLayout.removeAndRecycleAllViews(this.mRecycler);
      this.mLayout.removeAndRecycleScrapInt(this.mRecycler);
      this.mRecycler.clear();
      if (this.mIsAttached)
        this.mLayout.dispatchDetachedFromWindow(this, this.mRecycler); 
      this.mLayout.setRecyclerView(null);
      this.mLayout = null;
    } else {
      this.mRecycler.clear();
    } 
    this.mChildHelper.removeAllViewsUnfiltered();
    this.mLayout = paramLayoutManager;
    if (paramLayoutManager != null)
      if (paramLayoutManager.mRecyclerView == null) {
        this.mLayout.setRecyclerView(this);
        if (this.mIsAttached)
          this.mLayout.dispatchAttachedToWindow(this); 
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("LayoutManager ");
        stringBuilder.append(paramLayoutManager);
        stringBuilder.append(" is already attached to a RecyclerView: ");
        stringBuilder.append(paramLayoutManager.mRecyclerView);
        throw new IllegalArgumentException(stringBuilder.toString());
      }  
    this.mRecycler.updateViewCacheSize();
    requestLayout();
  }
  
  public void setOnFlingListener(OnFlingListener paramOnFlingListener) {
    this.mOnFlingListener = paramOnFlingListener;
  }
  
  public OnFlingListener getOnFlingListener() {
    return this.mOnFlingListener;
  }
  
  protected Parcelable onSaveInstanceState() {
    SavedState savedState1 = new SavedState(super.onSaveInstanceState());
    SavedState savedState2 = this.mPendingSavedState;
    if (savedState2 != null) {
      savedState1.copyFrom(savedState2);
    } else {
      LayoutManager layoutManager = this.mLayout;
      if (layoutManager != null) {
        savedState1.mLayoutState = layoutManager.onSaveInstanceState();
      } else {
        savedState1.mLayoutState = null;
      } 
    } 
    return (Parcelable)savedState1;
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable) {
    if (!(paramParcelable instanceof SavedState)) {
      super.onRestoreInstanceState(paramParcelable);
      return;
    } 
    SavedState savedState = (SavedState)paramParcelable;
    super.onRestoreInstanceState(savedState.getSuperState());
    if (this.mLayout != null && this.mPendingSavedState.mLayoutState != null)
      this.mLayout.onRestoreInstanceState(this.mPendingSavedState.mLayoutState); 
  }
  
  protected void dispatchSaveInstanceState(SparseArray<Parcelable> paramSparseArray) {
    dispatchFreezeSelfOnly(paramSparseArray);
  }
  
  protected void dispatchRestoreInstanceState(SparseArray<Parcelable> paramSparseArray) {
    dispatchThawSelfOnly(paramSparseArray);
  }
  
  private void addAnimatingView(ViewHolder paramViewHolder) {
    boolean bool;
    View view = paramViewHolder.itemView;
    if (view.getParent() == this) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mRecycler.unscrapView(getChildViewHolder(view));
    if (paramViewHolder.isTmpDetached()) {
      this.mChildHelper.attachViewToParent(view, -1, view.getLayoutParams(), true);
    } else if (!bool) {
      this.mChildHelper.addView(view, true);
    } else {
      this.mChildHelper.hide(view);
    } 
  }
  
  boolean removeAnimatingView(View paramView) {
    eatRequestLayout();
    boolean bool = this.mChildHelper.removeViewIfHidden(paramView);
    if (bool) {
      ViewHolder viewHolder = getChildViewHolderInt(paramView);
      this.mRecycler.unscrapView(viewHolder);
      this.mRecycler.recycleViewHolderInternal(viewHolder);
    } 
    resumeRequestLayout(bool ^ true);
    return bool;
  }
  
  public LayoutManager getLayoutManager() {
    return this.mLayout;
  }
  
  public RecycledViewPool getRecycledViewPool() {
    return this.mRecycler.getRecycledViewPool();
  }
  
  public void setRecycledViewPool(RecycledViewPool paramRecycledViewPool) {
    this.mRecycler.setRecycledViewPool(paramRecycledViewPool);
  }
  
  public void setViewCacheExtension(ViewCacheExtension paramViewCacheExtension) {
    this.mRecycler.setViewCacheExtension(paramViewCacheExtension);
  }
  
  public void setItemViewCacheSize(int paramInt) {
    this.mRecycler.setViewCacheSize(paramInt);
  }
  
  public int getScrollState() {
    return this.mScrollState;
  }
  
  void setScrollState(int paramInt) {
    if (paramInt == this.mScrollState)
      return; 
    this.mScrollState = paramInt;
    if (paramInt != 2)
      stopScrollersInternal(); 
    dispatchOnScrollStateChanged(paramInt);
  }
  
  public void addItemDecoration(ItemDecoration paramItemDecoration, int paramInt) {
    LayoutManager layoutManager = this.mLayout;
    if (layoutManager != null)
      layoutManager.assertNotInLayoutOrScroll("Cannot add item decoration during a scroll  or layout"); 
    if (this.mItemDecorations.isEmpty())
      setWillNotDraw(false); 
    if (paramInt < 0) {
      this.mItemDecorations.add(paramItemDecoration);
    } else {
      this.mItemDecorations.add(paramInt, paramItemDecoration);
    } 
    markItemDecorInsetsDirty();
    requestLayout();
  }
  
  public void addItemDecoration(ItemDecoration paramItemDecoration) {
    addItemDecoration(paramItemDecoration, -1);
  }
  
  public void removeItemDecoration(ItemDecoration paramItemDecoration) {
    LayoutManager layoutManager = this.mLayout;
    if (layoutManager != null)
      layoutManager.assertNotInLayoutOrScroll("Cannot remove item decoration during a scroll  or layout"); 
    this.mItemDecorations.remove(paramItemDecoration);
    if (this.mItemDecorations.isEmpty()) {
      boolean bool;
      if (getOverScrollMode() == 2) {
        bool = true;
      } else {
        bool = false;
      } 
      setWillNotDraw(bool);
    } 
    markItemDecorInsetsDirty();
    requestLayout();
  }
  
  public void setChildDrawingOrderCallback(ChildDrawingOrderCallback paramChildDrawingOrderCallback) {
    boolean bool;
    if (paramChildDrawingOrderCallback == this.mChildDrawingOrderCallback)
      return; 
    this.mChildDrawingOrderCallback = paramChildDrawingOrderCallback;
    if (paramChildDrawingOrderCallback != null) {
      bool = true;
    } else {
      bool = false;
    } 
    setChildrenDrawingOrderEnabled(bool);
  }
  
  @Deprecated
  public void setOnScrollListener(OnScrollListener paramOnScrollListener) {
    this.mScrollListener = paramOnScrollListener;
  }
  
  public void addOnScrollListener(OnScrollListener paramOnScrollListener) {
    if (this.mScrollListeners == null)
      this.mScrollListeners = new ArrayList<>(); 
    this.mScrollListeners.add(paramOnScrollListener);
  }
  
  public void removeOnScrollListener(OnScrollListener paramOnScrollListener) {
    List<OnScrollListener> list = this.mScrollListeners;
    if (list != null)
      list.remove(paramOnScrollListener); 
  }
  
  public void clearOnScrollListeners() {
    List<OnScrollListener> list = this.mScrollListeners;
    if (list != null)
      list.clear(); 
  }
  
  public void scrollToPosition(int paramInt) {
    if (this.mLayoutFrozen)
      return; 
    stopScroll();
    LayoutManager layoutManager = this.mLayout;
    if (layoutManager == null) {
      Log.e("RecyclerView", "Cannot scroll to position a LayoutManager set. Call setLayoutManager with a non-null argument.");
      return;
    } 
    layoutManager.scrollToPosition(paramInt);
    awakenScrollBars();
  }
  
  void jumpToPositionForSmoothScroller(int paramInt) {
    LayoutManager layoutManager = this.mLayout;
    if (layoutManager == null)
      return; 
    layoutManager.scrollToPosition(paramInt);
    awakenScrollBars();
  }
  
  public void smoothScrollToPosition(int paramInt) {
    if (this.mLayoutFrozen)
      return; 
    LayoutManager layoutManager = this.mLayout;
    if (layoutManager == null) {
      Log.e("RecyclerView", "Cannot smooth scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
      return;
    } 
    layoutManager.smoothScrollToPosition(this, this.mState, paramInt);
  }
  
  public void scrollTo(int paramInt1, int paramInt2) {
    Log.w("RecyclerView", "RecyclerView does not support scrolling to an absolute position. Use scrollToPosition instead");
  }
  
  public void scrollBy(int paramInt1, int paramInt2) {
    LayoutManager layoutManager = this.mLayout;
    if (layoutManager == null) {
      Log.e("RecyclerView", "Cannot scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
      return;
    } 
    if (this.mLayoutFrozen)
      return; 
    boolean bool1 = layoutManager.canScrollHorizontally();
    boolean bool2 = this.mLayout.canScrollVertically();
    if (bool1 || bool2) {
      int i = 0;
      if (!bool1)
        paramInt1 = 0; 
      if (bool2)
        i = paramInt2; 
      scrollByInternal(paramInt1, i, (MotionEvent)null);
    } 
  }
  
  void consumePendingUpdateOperations() {
    if (!this.mFirstLayoutComplete || this.mDataSetHasChangedAfterLayout) {
      Trace.beginSection("RV FullInvalidate");
      dispatchLayout();
      Trace.endSection();
      return;
    } 
    if (!this.mAdapterHelper.hasPendingUpdates())
      return; 
    if (this.mAdapterHelper.hasAnyUpdateTypes(4)) {
      AdapterHelper adapterHelper = this.mAdapterHelper;
      if (!adapterHelper.hasAnyUpdateTypes(11)) {
        Trace.beginSection("RV PartialInvalidate");
        eatRequestLayout();
        onEnterLayoutOrScroll();
        this.mAdapterHelper.preProcess();
        if (!this.mLayoutRequestEaten)
          if (hasUpdatedView()) {
            dispatchLayout();
          } else {
            this.mAdapterHelper.consumePostponedUpdates();
          }  
        resumeRequestLayout(true);
        onExitLayoutOrScroll();
        Trace.endSection();
        return;
      } 
    } 
    if (this.mAdapterHelper.hasPendingUpdates()) {
      Trace.beginSection("RV FullInvalidate");
      dispatchLayout();
      Trace.endSection();
    } 
  }
  
  private boolean hasUpdatedView() {
    int i = this.mChildHelper.getChildCount();
    for (byte b = 0; b < i; b++) {
      ViewHolder viewHolder = getChildViewHolderInt(this.mChildHelper.getChildAt(b));
      if (viewHolder != null && !viewHolder.shouldIgnore())
        if (viewHolder.isUpdated())
          return true;  
    } 
    return false;
  }
  
  boolean scrollByInternal(int paramInt1, int paramInt2, MotionEvent paramMotionEvent) {
    int arrayOfInt[], i = 0;
    byte b1 = 0;
    int j = 0;
    byte b2 = 0;
    int k = 0;
    byte b3 = 0;
    int m = 0;
    byte b4 = 0;
    consumePendingUpdateOperations();
    Adapter adapter = this.mAdapter;
    boolean bool = false;
    if (adapter != null) {
      eatRequestLayout();
      onEnterLayoutOrScroll();
      Trace.beginSection("RV Scroll");
      i = b1;
      k = b3;
      if (paramInt1 != 0) {
        k = this.mLayout.scrollHorizontallyBy(paramInt1, this.mRecycler, this.mState);
        i = paramInt1 - k;
      } 
      j = b2;
      m = b4;
      if (paramInt2 != 0) {
        m = this.mLayout.scrollVerticallyBy(paramInt2, this.mRecycler, this.mState);
        j = paramInt2 - m;
      } 
      Trace.endSection();
      repositionShadowingViews();
      onExitLayoutOrScroll();
      resumeRequestLayout(false);
    } 
    if (!this.mItemDecorations.isEmpty())
      invalidate(); 
    if (dispatchNestedScroll(k, m, i, j, this.mScrollOffset)) {
      paramInt1 = this.mLastTouchX;
      int[] arrayOfInt1 = this.mScrollOffset;
      this.mLastTouchX = paramInt1 - arrayOfInt1[0];
      this.mLastTouchY -= arrayOfInt1[1];
      if (paramMotionEvent != null)
        paramMotionEvent.offsetLocation(arrayOfInt1[0], arrayOfInt1[1]); 
      arrayOfInt = this.mNestedOffsets;
      paramInt1 = arrayOfInt[0];
      arrayOfInt1 = this.mScrollOffset;
      arrayOfInt[0] = paramInt1 + arrayOfInt1[0];
      arrayOfInt[1] = arrayOfInt[1] + arrayOfInt1[1];
    } else if (getOverScrollMode() != 2) {
      if (arrayOfInt != null)
        pullGlows(arrayOfInt.getX(), i, arrayOfInt.getY(), j); 
      considerReleasingGlowsOnScroll(paramInt1, paramInt2);
    } 
    if (k != 0 || m != 0)
      dispatchOnScrolled(k, m); 
    if (!awakenScrollBars())
      invalidate(); 
    if (k != 0 || m != 0)
      bool = true; 
    return bool;
  }
  
  public int computeHorizontalScrollOffset() {
    LayoutManager layoutManager = this.mLayout;
    int i = 0;
    if (layoutManager == null)
      return 0; 
    if (layoutManager.canScrollHorizontally())
      i = this.mLayout.computeHorizontalScrollOffset(this.mState); 
    return i;
  }
  
  public int computeHorizontalScrollExtent() {
    LayoutManager layoutManager = this.mLayout;
    int i = 0;
    if (layoutManager == null)
      return 0; 
    if (layoutManager.canScrollHorizontally())
      i = this.mLayout.computeHorizontalScrollExtent(this.mState); 
    return i;
  }
  
  public int computeHorizontalScrollRange() {
    LayoutManager layoutManager = this.mLayout;
    int i = 0;
    if (layoutManager == null)
      return 0; 
    if (layoutManager.canScrollHorizontally())
      i = this.mLayout.computeHorizontalScrollRange(this.mState); 
    return i;
  }
  
  public int computeVerticalScrollOffset() {
    LayoutManager layoutManager = this.mLayout;
    int i = 0;
    if (layoutManager == null)
      return 0; 
    if (layoutManager.canScrollVertically())
      i = this.mLayout.computeVerticalScrollOffset(this.mState); 
    return i;
  }
  
  public int computeVerticalScrollExtent() {
    LayoutManager layoutManager = this.mLayout;
    int i = 0;
    if (layoutManager == null)
      return 0; 
    if (layoutManager.canScrollVertically())
      i = this.mLayout.computeVerticalScrollExtent(this.mState); 
    return i;
  }
  
  public int computeVerticalScrollRange() {
    LayoutManager layoutManager = this.mLayout;
    int i = 0;
    if (layoutManager == null)
      return 0; 
    if (layoutManager.canScrollVertically())
      i = this.mLayout.computeVerticalScrollRange(this.mState); 
    return i;
  }
  
  void eatRequestLayout() {
    int i = this.mEatRequestLayout + 1;
    if (i == 1 && !this.mLayoutFrozen)
      this.mLayoutRequestEaten = false; 
  }
  
  void resumeRequestLayout(boolean paramBoolean) {
    if (this.mEatRequestLayout < 1)
      this.mEatRequestLayout = 1; 
    if (!paramBoolean)
      this.mLayoutRequestEaten = false; 
    if (this.mEatRequestLayout == 1) {
      if (paramBoolean && this.mLayoutRequestEaten && !this.mLayoutFrozen && this.mLayout != null && this.mAdapter != null)
        dispatchLayout(); 
      if (!this.mLayoutFrozen)
        this.mLayoutRequestEaten = false; 
    } 
    this.mEatRequestLayout--;
  }
  
  public void setLayoutFrozen(boolean paramBoolean) {
    if (paramBoolean != this.mLayoutFrozen) {
      assertNotInLayoutOrScroll("Do not setLayoutFrozen in layout or scroll");
      if (!paramBoolean) {
        this.mLayoutFrozen = false;
        if (this.mLayoutRequestEaten && this.mLayout != null && this.mAdapter != null)
          requestLayout(); 
        this.mLayoutRequestEaten = false;
      } else {
        long l = SystemClock.uptimeMillis();
        MotionEvent motionEvent = MotionEvent.obtain(l, l, 3, 0.0F, 0.0F, 0);
        onTouchEvent(motionEvent);
        this.mLayoutFrozen = true;
        this.mIgnoreMotionEventTillDown = true;
        stopScroll();
      } 
    } 
  }
  
  public boolean isLayoutFrozen() {
    return this.mLayoutFrozen;
  }
  
  public void smoothScrollBy(int paramInt1, int paramInt2) {
    smoothScrollBy(paramInt1, paramInt2, (Interpolator)null);
  }
  
  public void smoothScrollBy(int paramInt1, int paramInt2, Interpolator paramInterpolator) {
    LayoutManager layoutManager = this.mLayout;
    if (layoutManager == null) {
      Log.e("RecyclerView", "Cannot smooth scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
      return;
    } 
    if (this.mLayoutFrozen)
      return; 
    if (!layoutManager.canScrollHorizontally())
      paramInt1 = 0; 
    if (!this.mLayout.canScrollVertically())
      paramInt2 = 0; 
    if (paramInt1 != 0 || paramInt2 != 0)
      this.mViewFlinger.smoothScrollBy(paramInt1, paramInt2, paramInterpolator); 
  }
  
  public boolean fling(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mLayout : Lcom/android/internal/widget/RecyclerView$LayoutManager;
    //   4: astore_3
    //   5: aload_3
    //   6: ifnonnull -> 20
    //   9: ldc 'RecyclerView'
    //   11: ldc_w 'Cannot fling without a LayoutManager set. Call setLayoutManager with a non-null argument.'
    //   14: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   17: pop
    //   18: iconst_0
    //   19: ireturn
    //   20: aload_0
    //   21: getfield mLayoutFrozen : Z
    //   24: ifeq -> 29
    //   27: iconst_0
    //   28: ireturn
    //   29: aload_3
    //   30: invokevirtual canScrollHorizontally : ()Z
    //   33: istore #4
    //   35: aload_0
    //   36: getfield mLayout : Lcom/android/internal/widget/RecyclerView$LayoutManager;
    //   39: invokevirtual canScrollVertically : ()Z
    //   42: istore #5
    //   44: iload #4
    //   46: ifeq -> 63
    //   49: iload_1
    //   50: istore #6
    //   52: iload_1
    //   53: invokestatic abs : (I)I
    //   56: aload_0
    //   57: getfield mMinFlingVelocity : I
    //   60: if_icmpge -> 66
    //   63: iconst_0
    //   64: istore #6
    //   66: iload #5
    //   68: ifeq -> 84
    //   71: iload_2
    //   72: istore_1
    //   73: iload_2
    //   74: invokestatic abs : (I)I
    //   77: aload_0
    //   78: getfield mMinFlingVelocity : I
    //   81: if_icmpge -> 86
    //   84: iconst_0
    //   85: istore_1
    //   86: iload #6
    //   88: ifne -> 97
    //   91: iload_1
    //   92: ifne -> 97
    //   95: iconst_0
    //   96: ireturn
    //   97: aload_0
    //   98: iload #6
    //   100: i2f
    //   101: iload_1
    //   102: i2f
    //   103: invokevirtual dispatchNestedPreFling : (FF)Z
    //   106: ifne -> 331
    //   109: aload_0
    //   110: getfield mLayout : Lcom/android/internal/widget/RecyclerView$LayoutManager;
    //   113: iconst_0
    //   114: invokevirtual getChildAt : (I)Landroid/view/View;
    //   117: astore_3
    //   118: aload_0
    //   119: getfield mLayout : Lcom/android/internal/widget/RecyclerView$LayoutManager;
    //   122: astore #7
    //   124: aload #7
    //   126: aload #7
    //   128: invokevirtual getChildCount : ()I
    //   131: iconst_1
    //   132: isub
    //   133: invokevirtual getChildAt : (I)Landroid/view/View;
    //   136: astore #7
    //   138: iconst_0
    //   139: istore #8
    //   141: iload_1
    //   142: ifge -> 176
    //   145: aload_0
    //   146: aload_3
    //   147: invokevirtual getChildAdapterPosition : (Landroid/view/View;)I
    //   150: ifgt -> 173
    //   153: aload_3
    //   154: invokevirtual getTop : ()I
    //   157: aload_0
    //   158: invokevirtual getPaddingTop : ()I
    //   161: if_icmpge -> 167
    //   164: goto -> 173
    //   167: iconst_0
    //   168: istore #8
    //   170: goto -> 176
    //   173: iconst_1
    //   174: istore #8
    //   176: iload_1
    //   177: ifle -> 227
    //   180: aload_0
    //   181: aload #7
    //   183: invokevirtual getChildAdapterPosition : (Landroid/view/View;)I
    //   186: aload_0
    //   187: getfield mAdapter : Lcom/android/internal/widget/RecyclerView$Adapter;
    //   190: invokevirtual getItemCount : ()I
    //   193: iconst_1
    //   194: isub
    //   195: if_icmplt -> 224
    //   198: aload #7
    //   200: invokevirtual getBottom : ()I
    //   203: aload_0
    //   204: invokevirtual getHeight : ()I
    //   207: aload_0
    //   208: invokevirtual getPaddingBottom : ()I
    //   211: isub
    //   212: if_icmple -> 218
    //   215: goto -> 224
    //   218: iconst_0
    //   219: istore #8
    //   221: goto -> 227
    //   224: iconst_1
    //   225: istore #8
    //   227: aload_0
    //   228: iload #6
    //   230: i2f
    //   231: iload_1
    //   232: i2f
    //   233: iload #8
    //   235: invokevirtual dispatchNestedFling : (FFZ)Z
    //   238: pop
    //   239: aload_0
    //   240: getfield mOnFlingListener : Lcom/android/internal/widget/RecyclerView$OnFlingListener;
    //   243: astore_3
    //   244: aload_3
    //   245: ifnull -> 260
    //   248: aload_3
    //   249: iload #6
    //   251: iload_1
    //   252: invokevirtual onFling : (II)Z
    //   255: ifeq -> 260
    //   258: iconst_1
    //   259: ireturn
    //   260: iload #4
    //   262: ifne -> 278
    //   265: iload #5
    //   267: ifeq -> 273
    //   270: goto -> 278
    //   273: iconst_0
    //   274: istore_2
    //   275: goto -> 280
    //   278: iconst_1
    //   279: istore_2
    //   280: iload_2
    //   281: ifeq -> 331
    //   284: aload_0
    //   285: getfield mMaxFlingVelocity : I
    //   288: istore_2
    //   289: iload_2
    //   290: ineg
    //   291: iload #6
    //   293: iload_2
    //   294: invokestatic min : (II)I
    //   297: invokestatic max : (II)I
    //   300: istore_2
    //   301: aload_0
    //   302: getfield mMaxFlingVelocity : I
    //   305: istore #6
    //   307: iload #6
    //   309: ineg
    //   310: iload_1
    //   311: iload #6
    //   313: invokestatic min : (II)I
    //   316: invokestatic max : (II)I
    //   319: istore_1
    //   320: aload_0
    //   321: getfield mViewFlinger : Lcom/android/internal/widget/RecyclerView$ViewFlinger;
    //   324: iload_2
    //   325: iload_1
    //   326: invokevirtual fling : (II)V
    //   329: iconst_1
    //   330: ireturn
    //   331: iconst_0
    //   332: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1990	-> 0
    //   #1991	-> 9
    //   #1993	-> 18
    //   #1995	-> 20
    //   #1996	-> 27
    //   #1999	-> 29
    //   #2000	-> 35
    //   #2002	-> 44
    //   #2003	-> 63
    //   #2005	-> 66
    //   #2006	-> 84
    //   #2008	-> 86
    //   #2010	-> 95
    //   #2013	-> 97
    //   #2014	-> 109
    //   #2015	-> 118
    //   #2016	-> 138
    //   #2017	-> 141
    //   #2018	-> 145
    //   #2019	-> 153
    //   #2022	-> 176
    //   #2023	-> 180
    //   #2024	-> 198
    //   #2027	-> 227
    //   #2029	-> 239
    //   #2030	-> 258
    //   #2033	-> 260
    //   #2035	-> 280
    //   #2036	-> 284
    //   #2037	-> 301
    //   #2038	-> 320
    //   #2039	-> 329
    //   #2042	-> 331
  }
  
  public void stopScroll() {
    setScrollState(0);
    stopScrollersInternal();
  }
  
  private void stopScrollersInternal() {
    this.mViewFlinger.stop();
    LayoutManager layoutManager = this.mLayout;
    if (layoutManager != null)
      layoutManager.stopSmoothScroller(); 
  }
  
  public int getMinFlingVelocity() {
    return this.mMinFlingVelocity;
  }
  
  public int getMaxFlingVelocity() {
    return this.mMaxFlingVelocity;
  }
  
  private void pullGlows(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    boolean bool = false;
    if (paramFloat2 < 0.0F) {
      ensureLeftGlow();
      this.mLeftGlow.onPull(-paramFloat2 / getWidth(), 1.0F - paramFloat3 / getHeight());
      bool = true;
    } else if (paramFloat2 > 0.0F) {
      ensureRightGlow();
      this.mRightGlow.onPull(paramFloat2 / getWidth(), paramFloat3 / getHeight());
      bool = true;
    } 
    if (paramFloat4 < 0.0F) {
      ensureTopGlow();
      this.mTopGlow.onPull(-paramFloat4 / getHeight(), paramFloat1 / getWidth());
      bool = true;
    } else if (paramFloat4 > 0.0F) {
      ensureBottomGlow();
      this.mBottomGlow.onPull(paramFloat4 / getHeight(), 1.0F - paramFloat1 / getWidth());
      bool = true;
    } 
    if (bool || paramFloat2 != 0.0F || paramFloat4 != 0.0F)
      postInvalidateOnAnimation(); 
  }
  
  private void releaseGlows() {
    boolean bool = false;
    EdgeEffect edgeEffect = this.mLeftGlow;
    if (edgeEffect != null) {
      edgeEffect.onRelease();
      bool = true;
    } 
    edgeEffect = this.mTopGlow;
    if (edgeEffect != null) {
      edgeEffect.onRelease();
      bool = true;
    } 
    edgeEffect = this.mRightGlow;
    if (edgeEffect != null) {
      edgeEffect.onRelease();
      bool = true;
    } 
    edgeEffect = this.mBottomGlow;
    if (edgeEffect != null) {
      edgeEffect.onRelease();
      bool = true;
    } 
    if (bool)
      postInvalidateOnAnimation(); 
  }
  
  void considerReleasingGlowsOnScroll(int paramInt1, int paramInt2) {
    int i = 0;
    EdgeEffect edgeEffect = this.mLeftGlow;
    int j = i;
    if (edgeEffect != null) {
      j = i;
      if (!edgeEffect.isFinished()) {
        j = i;
        if (paramInt1 > 0) {
          this.mLeftGlow.onRelease();
          j = 1;
        } 
      } 
    } 
    edgeEffect = this.mRightGlow;
    i = j;
    if (edgeEffect != null) {
      i = j;
      if (!edgeEffect.isFinished()) {
        i = j;
        if (paramInt1 < 0) {
          this.mRightGlow.onRelease();
          i = 1;
        } 
      } 
    } 
    edgeEffect = this.mTopGlow;
    paramInt1 = i;
    if (edgeEffect != null) {
      paramInt1 = i;
      if (!edgeEffect.isFinished()) {
        paramInt1 = i;
        if (paramInt2 > 0) {
          this.mTopGlow.onRelease();
          paramInt1 = 1;
        } 
      } 
    } 
    edgeEffect = this.mBottomGlow;
    j = paramInt1;
    if (edgeEffect != null) {
      j = paramInt1;
      if (!edgeEffect.isFinished()) {
        j = paramInt1;
        if (paramInt2 < 0) {
          this.mBottomGlow.onRelease();
          j = 1;
        } 
      } 
    } 
    if (j != 0)
      postInvalidateOnAnimation(); 
  }
  
  void absorbGlows(int paramInt1, int paramInt2) {
    if (paramInt1 < 0) {
      ensureLeftGlow();
      this.mLeftGlow.onAbsorb(-paramInt1);
    } else if (paramInt1 > 0) {
      ensureRightGlow();
      this.mRightGlow.onAbsorb(paramInt1);
    } 
    if (paramInt2 < 0) {
      ensureTopGlow();
      this.mTopGlow.onAbsorb(-paramInt2);
    } else if (paramInt2 > 0) {
      ensureBottomGlow();
      this.mBottomGlow.onAbsorb(paramInt2);
    } 
    if (paramInt1 != 0 || paramInt2 != 0)
      postInvalidateOnAnimation(); 
  }
  
  void ensureLeftGlow() {
    if (this.mLeftGlow != null)
      return; 
    EdgeEffect edgeEffect = new EdgeEffect(getContext());
    if (this.mClipToPadding) {
      int i = getMeasuredHeight(), j = getPaddingTop(), k = getPaddingBottom();
      int m = getMeasuredWidth(), n = getPaddingLeft(), i1 = getPaddingRight();
      edgeEffect.setSize(i - j - k, m - n - i1);
    } else {
      edgeEffect.setSize(getMeasuredHeight(), getMeasuredWidth());
    } 
  }
  
  void ensureRightGlow() {
    if (this.mRightGlow != null)
      return; 
    EdgeEffect edgeEffect = new EdgeEffect(getContext());
    if (this.mClipToPadding) {
      int i = getMeasuredHeight(), j = getPaddingTop(), k = getPaddingBottom();
      int m = getMeasuredWidth(), n = getPaddingLeft(), i1 = getPaddingRight();
      edgeEffect.setSize(i - j - k, m - n - i1);
    } else {
      edgeEffect.setSize(getMeasuredHeight(), getMeasuredWidth());
    } 
  }
  
  void ensureTopGlow() {
    if (this.mTopGlow != null)
      return; 
    EdgeEffect edgeEffect = new EdgeEffect(getContext());
    if (this.mClipToPadding) {
      int i = getMeasuredWidth(), j = getPaddingLeft(), k = getPaddingRight();
      int m = getMeasuredHeight(), n = getPaddingTop(), i1 = getPaddingBottom();
      edgeEffect.setSize(i - j - k, m - n - i1);
    } else {
      edgeEffect.setSize(getMeasuredWidth(), getMeasuredHeight());
    } 
  }
  
  void ensureBottomGlow() {
    if (this.mBottomGlow != null)
      return; 
    EdgeEffect edgeEffect = new EdgeEffect(getContext());
    if (this.mClipToPadding) {
      int i = getMeasuredWidth(), j = getPaddingLeft(), k = getPaddingRight();
      int m = getMeasuredHeight(), n = getPaddingTop(), i1 = getPaddingBottom();
      edgeEffect.setSize(i - j - k, m - n - i1);
    } else {
      edgeEffect.setSize(getMeasuredWidth(), getMeasuredHeight());
    } 
  }
  
  void invalidateGlows() {
    this.mBottomGlow = null;
    this.mTopGlow = null;
    this.mRightGlow = null;
    this.mLeftGlow = null;
  }
  
  public View focusSearch(View paramView, int paramInt) {
    View view1;
    int i, j;
    View view2 = this.mLayout.onInterceptFocusSearch(paramView, paramInt);
    if (view2 != null)
      return view2; 
    Adapter adapter = this.mAdapter;
    boolean bool = true;
    if (adapter != null && this.mLayout != null && 
      !isComputingLayout() && !this.mLayoutFrozen) {
      i = 1;
    } else {
      i = 0;
    } 
    FocusFinder focusFinder = FocusFinder.getInstance();
    if (i && (paramInt == 2 || paramInt == 1)) {
      int k = 0;
      i = paramInt;
      if (this.mLayout.canScrollVertically()) {
        byte b;
        if (paramInt == 2) {
          b = 130;
        } else {
          b = 33;
        } 
        View view = focusFinder.findNextFocus(this, paramView, b);
        if (view == null) {
          i = 1;
        } else {
          i = 0;
        } 
        int n = i;
        k = n;
        i = paramInt;
        if (FORCE_ABS_FOCUS_SEARCH_DIRECTION) {
          i = b;
          k = n;
        } 
      } 
      int m = k;
      j = i;
      if (k == 0) {
        m = k;
        j = i;
        if (this.mLayout.canScrollHorizontally()) {
          if (this.mLayout.getLayoutDirection() == 1) {
            paramInt = 1;
          } else {
            paramInt = 0;
          } 
          if (i == 2) {
            j = 1;
          } else {
            j = 0;
          } 
          if ((j ^ paramInt) != 0) {
            paramInt = 66;
          } else {
            paramInt = 17;
          } 
          View view = focusFinder.findNextFocus(this, paramView, paramInt);
          if (view == null) {
            j = bool;
          } else {
            j = 0;
          } 
          k = j;
          m = k;
          j = i;
          if (FORCE_ABS_FOCUS_SEARCH_DIRECTION) {
            j = paramInt;
            m = k;
          } 
        } 
      } 
      if (m != 0) {
        consumePendingUpdateOperations();
        View view = findContainingItemView(paramView);
        if (view == null)
          return null; 
        eatRequestLayout();
        this.mLayout.onFocusSearchFailed(paramView, j, this.mRecycler, this.mState);
        resumeRequestLayout(false);
      } 
      view1 = focusFinder.findNextFocus(this, paramView, j);
    } else {
      View view = view1.findNextFocus(this, paramView, paramInt);
      view1 = view;
      j = paramInt;
      if (view == null) {
        view1 = view;
        j = paramInt;
        if (i != 0) {
          consumePendingUpdateOperations();
          view1 = findContainingItemView(paramView);
          if (view1 == null)
            return null; 
          eatRequestLayout();
          view1 = this.mLayout.onFocusSearchFailed(paramView, paramInt, this.mRecycler, this.mState);
          resumeRequestLayout(false);
          j = paramInt;
        } 
      } 
    } 
    if (isPreferredNextFocus(paramView, view1, j)) {
      paramView = view1;
    } else {
      paramView = super.focusSearch(paramView, j);
    } 
    return paramView;
  }
  
  private boolean isPreferredNextFocus(View paramView1, View paramView2, int paramInt) {
    byte b = 0;
    if (paramView2 == null || paramView2 == this)
      return false; 
    if (paramView1 == null)
      return true; 
    if (paramInt == 2 || paramInt == 1) {
      byte b1;
      if (this.mLayout.getLayoutDirection() == 1) {
        b1 = 1;
      } else {
        b1 = 0;
      } 
      if (paramInt == 2)
        b = 1; 
      if ((b ^ b1) != 0) {
        b1 = 66;
      } else {
        b1 = 17;
      } 
      if (isPreferredNextFocusAbsolute(paramView1, paramView2, b1))
        return true; 
      if (paramInt == 2)
        return isPreferredNextFocusAbsolute(paramView1, paramView2, 130); 
      return isPreferredNextFocusAbsolute(paramView1, paramView2, 33);
    } 
    return isPreferredNextFocusAbsolute(paramView1, paramView2, paramInt);
  }
  
  private boolean isPreferredNextFocusAbsolute(View paramView1, View paramView2, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mTempRect : Landroid/graphics/Rect;
    //   4: astore #4
    //   6: aload_1
    //   7: invokevirtual getWidth : ()I
    //   10: istore #5
    //   12: aload_1
    //   13: invokevirtual getHeight : ()I
    //   16: istore #6
    //   18: iconst_0
    //   19: istore #7
    //   21: iconst_0
    //   22: istore #8
    //   24: iconst_0
    //   25: istore #9
    //   27: iconst_0
    //   28: istore #10
    //   30: aload #4
    //   32: iconst_0
    //   33: iconst_0
    //   34: iload #5
    //   36: iload #6
    //   38: invokevirtual set : (IIII)V
    //   41: aload_0
    //   42: getfield mTempRect2 : Landroid/graphics/Rect;
    //   45: iconst_0
    //   46: iconst_0
    //   47: aload_2
    //   48: invokevirtual getWidth : ()I
    //   51: aload_2
    //   52: invokevirtual getHeight : ()I
    //   55: invokevirtual set : (IIII)V
    //   58: aload_0
    //   59: aload_1
    //   60: aload_0
    //   61: getfield mTempRect : Landroid/graphics/Rect;
    //   64: invokevirtual offsetDescendantRectToMyCoords : (Landroid/view/View;Landroid/graphics/Rect;)V
    //   67: aload_0
    //   68: aload_2
    //   69: aload_0
    //   70: getfield mTempRect2 : Landroid/graphics/Rect;
    //   73: invokevirtual offsetDescendantRectToMyCoords : (Landroid/view/View;Landroid/graphics/Rect;)V
    //   76: iload_3
    //   77: bipush #17
    //   79: if_icmpeq -> 330
    //   82: iload_3
    //   83: bipush #33
    //   85: if_icmpeq -> 265
    //   88: iload_3
    //   89: bipush #66
    //   91: if_icmpeq -> 200
    //   94: iload_3
    //   95: sipush #130
    //   98: if_icmpne -> 166
    //   101: aload_0
    //   102: getfield mTempRect : Landroid/graphics/Rect;
    //   105: getfield top : I
    //   108: aload_0
    //   109: getfield mTempRect2 : Landroid/graphics/Rect;
    //   112: getfield top : I
    //   115: if_icmplt -> 139
    //   118: iload #10
    //   120: istore #11
    //   122: aload_0
    //   123: getfield mTempRect : Landroid/graphics/Rect;
    //   126: getfield bottom : I
    //   129: aload_0
    //   130: getfield mTempRect2 : Landroid/graphics/Rect;
    //   133: getfield top : I
    //   136: if_icmpgt -> 163
    //   139: iload #10
    //   141: istore #11
    //   143: aload_0
    //   144: getfield mTempRect : Landroid/graphics/Rect;
    //   147: getfield bottom : I
    //   150: aload_0
    //   151: getfield mTempRect2 : Landroid/graphics/Rect;
    //   154: getfield bottom : I
    //   157: if_icmpge -> 163
    //   160: iconst_1
    //   161: istore #11
    //   163: iload #11
    //   165: ireturn
    //   166: new java/lang/StringBuilder
    //   169: dup
    //   170: invokespecial <init> : ()V
    //   173: astore_1
    //   174: aload_1
    //   175: ldc_w 'direction must be absolute. received:'
    //   178: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   181: pop
    //   182: aload_1
    //   183: iload_3
    //   184: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   187: pop
    //   188: new java/lang/IllegalArgumentException
    //   191: dup
    //   192: aload_1
    //   193: invokevirtual toString : ()Ljava/lang/String;
    //   196: invokespecial <init> : (Ljava/lang/String;)V
    //   199: athrow
    //   200: aload_0
    //   201: getfield mTempRect : Landroid/graphics/Rect;
    //   204: getfield left : I
    //   207: aload_0
    //   208: getfield mTempRect2 : Landroid/graphics/Rect;
    //   211: getfield left : I
    //   214: if_icmplt -> 238
    //   217: iload #7
    //   219: istore #11
    //   221: aload_0
    //   222: getfield mTempRect : Landroid/graphics/Rect;
    //   225: getfield right : I
    //   228: aload_0
    //   229: getfield mTempRect2 : Landroid/graphics/Rect;
    //   232: getfield left : I
    //   235: if_icmpgt -> 262
    //   238: iload #7
    //   240: istore #11
    //   242: aload_0
    //   243: getfield mTempRect : Landroid/graphics/Rect;
    //   246: getfield right : I
    //   249: aload_0
    //   250: getfield mTempRect2 : Landroid/graphics/Rect;
    //   253: getfield right : I
    //   256: if_icmpge -> 262
    //   259: iconst_1
    //   260: istore #11
    //   262: iload #11
    //   264: ireturn
    //   265: aload_0
    //   266: getfield mTempRect : Landroid/graphics/Rect;
    //   269: getfield bottom : I
    //   272: aload_0
    //   273: getfield mTempRect2 : Landroid/graphics/Rect;
    //   276: getfield bottom : I
    //   279: if_icmpgt -> 303
    //   282: iload #8
    //   284: istore #11
    //   286: aload_0
    //   287: getfield mTempRect : Landroid/graphics/Rect;
    //   290: getfield top : I
    //   293: aload_0
    //   294: getfield mTempRect2 : Landroid/graphics/Rect;
    //   297: getfield bottom : I
    //   300: if_icmplt -> 327
    //   303: iload #8
    //   305: istore #11
    //   307: aload_0
    //   308: getfield mTempRect : Landroid/graphics/Rect;
    //   311: getfield top : I
    //   314: aload_0
    //   315: getfield mTempRect2 : Landroid/graphics/Rect;
    //   318: getfield top : I
    //   321: if_icmple -> 327
    //   324: iconst_1
    //   325: istore #11
    //   327: iload #11
    //   329: ireturn
    //   330: aload_0
    //   331: getfield mTempRect : Landroid/graphics/Rect;
    //   334: getfield right : I
    //   337: aload_0
    //   338: getfield mTempRect2 : Landroid/graphics/Rect;
    //   341: getfield right : I
    //   344: if_icmpgt -> 368
    //   347: iload #9
    //   349: istore #11
    //   351: aload_0
    //   352: getfield mTempRect : Landroid/graphics/Rect;
    //   355: getfield left : I
    //   358: aload_0
    //   359: getfield mTempRect2 : Landroid/graphics/Rect;
    //   362: getfield right : I
    //   365: if_icmplt -> 392
    //   368: iload #9
    //   370: istore #11
    //   372: aload_0
    //   373: getfield mTempRect : Landroid/graphics/Rect;
    //   376: getfield left : I
    //   379: aload_0
    //   380: getfield mTempRect2 : Landroid/graphics/Rect;
    //   383: getfield left : I
    //   386: if_icmple -> 392
    //   389: iconst_1
    //   390: istore #11
    //   392: iload #11
    //   394: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2372	-> 0
    //   #2373	-> 41
    //   #2374	-> 58
    //   #2375	-> 67
    //   #2376	-> 76
    //   #2390	-> 101
    //   #2394	-> 166
    //   #2382	-> 200
    //   #2386	-> 265
    //   #2378	-> 330
  }
  
  public void requestChildFocus(View paramView1, View paramView2) {
    if (!this.mLayout.onRequestChildFocus(this, this.mState, paramView1, paramView2) && paramView2 != null) {
      this.mTempRect.set(0, 0, paramView2.getWidth(), paramView2.getHeight());
      ViewGroup.LayoutParams layoutParams = paramView2.getLayoutParams();
      if (layoutParams instanceof LayoutParams) {
        LayoutParams layoutParams1 = (LayoutParams)layoutParams;
        if (!layoutParams1.mInsetsDirty) {
          Rect rect1 = layoutParams1.mDecorInsets;
          Rect rect2 = this.mTempRect;
          rect2.left -= rect1.left;
          rect2 = this.mTempRect;
          rect2.right += rect1.right;
          rect2 = this.mTempRect;
          rect2.top -= rect1.top;
          rect2 = this.mTempRect;
          rect2.bottom += rect1.bottom;
        } 
      } 
      offsetDescendantRectToMyCoords(paramView2, this.mTempRect);
      offsetRectIntoDescendantCoords(paramView1, this.mTempRect);
      requestChildRectangleOnScreen(paramView1, this.mTempRect, this.mFirstLayoutComplete ^ true);
    } 
    super.requestChildFocus(paramView1, paramView2);
  }
  
  public boolean requestChildRectangleOnScreen(View paramView, Rect paramRect, boolean paramBoolean) {
    return this.mLayout.requestChildRectangleOnScreen(this, paramView, paramRect, paramBoolean);
  }
  
  public void addFocusables(ArrayList<View> paramArrayList, int paramInt1, int paramInt2) {
    LayoutManager layoutManager = this.mLayout;
    if (layoutManager == null || !layoutManager.onAddFocusables(this, paramArrayList, paramInt1, paramInt2))
      super.addFocusables(paramArrayList, paramInt1, paramInt2); 
  }
  
  protected boolean onRequestFocusInDescendants(int paramInt, Rect paramRect) {
    if (isComputingLayout())
      return false; 
    return super.onRequestFocusInDescendants(paramInt, paramRect);
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    this.mLayoutOrScrollCounter = 0;
    boolean bool = true;
    this.mIsAttached = true;
    if (!this.mFirstLayoutComplete || isLayoutRequested())
      bool = false; 
    this.mFirstLayoutComplete = bool;
    LayoutManager layoutManager = this.mLayout;
    if (layoutManager != null)
      layoutManager.dispatchAttachedToWindow(this); 
    this.mPostedAnimatorRunner = false;
    if (ALLOW_THREAD_GAP_WORK) {
      GapWorker gapWorker = GapWorker.sGapWorker.get();
      if (gapWorker == null) {
        this.mGapWorker = new GapWorker();
        Display display = getDisplay();
        float f1 = 60.0F;
        float f2 = f1;
        if (!isInEditMode()) {
          f2 = f1;
          if (display != null) {
            float f = display.getRefreshRate();
            f2 = f1;
            if (f >= 30.0F)
              f2 = f; 
          } 
        } 
        this.mGapWorker.mFrameIntervalNs = (long)(1.0E9F / f2);
        GapWorker.sGapWorker.set(this.mGapWorker);
      } 
      this.mGapWorker.add(this);
    } 
  }
  
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    ItemAnimator itemAnimator = this.mItemAnimator;
    if (itemAnimator != null)
      itemAnimator.endAnimations(); 
    stopScroll();
    this.mIsAttached = false;
    LayoutManager layoutManager = this.mLayout;
    if (layoutManager != null)
      layoutManager.dispatchDetachedFromWindow(this, this.mRecycler); 
    this.mPendingAccessibilityImportanceChange.clear();
    removeCallbacks(this.mItemAnimatorRunner);
    this.mViewInfoStore.onDetach();
    if (ALLOW_THREAD_GAP_WORK) {
      this.mGapWorker.remove(this);
      this.mGapWorker = null;
    } 
  }
  
  public boolean isAttachedToWindow() {
    return this.mIsAttached;
  }
  
  void assertInLayoutOrScroll(String paramString) {
    if (!isComputingLayout()) {
      if (paramString == null)
        throw new IllegalStateException("Cannot call this method unless RecyclerView is computing a layout or scrolling"); 
      throw new IllegalStateException(paramString);
    } 
  }
  
  void assertNotInLayoutOrScroll(String paramString) {
    if (isComputingLayout()) {
      if (paramString == null)
        throw new IllegalStateException("Cannot call this method while RecyclerView is computing a layout or scrolling"); 
      throw new IllegalStateException(paramString);
    } 
    if (this.mDispatchScrollCounter > 0)
      Log.w("RecyclerView", "Cannot call this method in a scroll callback. Scroll callbacks might be run during a measure & layout pass where you cannot change the RecyclerView data. Any method call that might change the structure of the RecyclerView or the adapter contents should be postponed to the next frame.", new IllegalStateException("")); 
  }
  
  public void addOnItemTouchListener(OnItemTouchListener paramOnItemTouchListener) {
    this.mOnItemTouchListeners.add(paramOnItemTouchListener);
  }
  
  public void removeOnItemTouchListener(OnItemTouchListener paramOnItemTouchListener) {
    this.mOnItemTouchListeners.remove(paramOnItemTouchListener);
    if (this.mActiveOnItemTouchListener == paramOnItemTouchListener)
      this.mActiveOnItemTouchListener = null; 
  }
  
  private boolean dispatchOnItemTouchIntercept(MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.getAction();
    if (i == 3 || i == 0)
      this.mActiveOnItemTouchListener = null; 
    int j = this.mOnItemTouchListeners.size();
    for (byte b = 0; b < j; b++) {
      OnItemTouchListener onItemTouchListener = this.mOnItemTouchListeners.get(b);
      if (onItemTouchListener.onInterceptTouchEvent(this, paramMotionEvent) && i != 3) {
        this.mActiveOnItemTouchListener = onItemTouchListener;
        return true;
      } 
    } 
    return false;
  }
  
  private boolean dispatchOnItemTouch(MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.getAction();
    OnItemTouchListener onItemTouchListener = this.mActiveOnItemTouchListener;
    if (onItemTouchListener != null)
      if (i == 0) {
        this.mActiveOnItemTouchListener = null;
      } else {
        onItemTouchListener.onTouchEvent(this, paramMotionEvent);
        if (i == 3 || i == 1)
          this.mActiveOnItemTouchListener = null; 
        return true;
      }  
    if (i != 0) {
      int j = this.mOnItemTouchListeners.size();
      for (i = 0; i < j; i++) {
        onItemTouchListener = this.mOnItemTouchListeners.get(i);
        if (onItemTouchListener.onInterceptTouchEvent(this, paramMotionEvent)) {
          this.mActiveOnItemTouchListener = onItemTouchListener;
          return true;
        } 
      } 
    } 
    return false;
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent) {
    StringBuilder stringBuilder;
    boolean bool1 = this.mLayoutFrozen;
    boolean bool = false;
    if (bool1)
      return false; 
    if (dispatchOnItemTouchIntercept(paramMotionEvent)) {
      cancelTouch();
      return true;
    } 
    LayoutManager layoutManager = this.mLayout;
    if (layoutManager == null)
      return false; 
    boolean bool2 = layoutManager.canScrollHorizontally();
    bool1 = this.mLayout.canScrollVertically();
    if (this.mVelocityTracker == null)
      this.mVelocityTracker = VelocityTracker.obtain(); 
    this.mVelocityTracker.addMovement(paramMotionEvent);
    int i = paramMotionEvent.getActionMasked();
    int j = paramMotionEvent.getActionIndex();
    if (i != 0) {
      if (i != 1) {
        if (i != 2) {
          if (i != 3) {
            if (i != 5) {
              if (i == 6)
                onPointerUp(paramMotionEvent); 
            } else {
              this.mScrollPointerId = paramMotionEvent.getPointerId(j);
              this.mLastTouchX = i = (int)(paramMotionEvent.getX(j) + 0.5F);
              this.mInitialTouchX = i;
              this.mLastTouchY = j = (int)(paramMotionEvent.getY(j) + 0.5F);
              this.mInitialTouchY = j;
            } 
          } else {
            cancelTouch();
          } 
        } else {
          j = paramMotionEvent.findPointerIndex(this.mScrollPointerId);
          if (j < 0) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Error processing scroll; pointer index for id ");
            stringBuilder.append(this.mScrollPointerId);
            stringBuilder.append(" not found. Did any MotionEvents get skipped?");
            Log.e("RecyclerView", stringBuilder.toString());
            return false;
          } 
          i = (int)(stringBuilder.getX(j) + 0.5F);
          j = (int)(stringBuilder.getY(j) + 0.5F);
          if (this.mScrollState != 1) {
            int k = i - this.mInitialTouchX;
            int m = j - this.mInitialTouchY;
            i = 0;
            j = i;
            if (bool2) {
              int n = Math.abs(k), i1 = this.mTouchSlop;
              j = i;
              if (n > i1) {
                i = this.mInitialTouchX;
                if (k < 0) {
                  j = -1;
                } else {
                  j = 1;
                } 
                this.mLastTouchX = i + i1 * j;
                j = 1;
              } 
            } 
            i = j;
            if (bool1) {
              k = Math.abs(m);
              int n = this.mTouchSlop;
              i = j;
              if (k > n) {
                i = this.mInitialTouchY;
                if (m < 0) {
                  j = -1;
                } else {
                  j = 1;
                } 
                this.mLastTouchY = i + n * j;
                i = 1;
              } 
            } 
            if (i != 0)
              setScrollState(1); 
          } 
        } 
      } else {
        this.mVelocityTracker.clear();
        stopNestedScroll();
      } 
    } else {
      if (this.mIgnoreMotionEventTillDown)
        this.mIgnoreMotionEventTillDown = false; 
      this.mScrollPointerId = stringBuilder.getPointerId(0);
      this.mLastTouchX = j = (int)(stringBuilder.getX() + 0.5F);
      this.mInitialTouchX = j;
      this.mLastTouchY = j = (int)(stringBuilder.getY() + 0.5F);
      this.mInitialTouchY = j;
      if (this.mScrollState == 2) {
        getParent().requestDisallowInterceptTouchEvent(true);
        setScrollState(1);
      } 
      int[] arrayOfInt = this.mNestedOffsets;
      arrayOfInt[1] = 0;
      arrayOfInt[0] = 0;
      j = 0;
      if (bool2)
        j = false | true; 
      i = j;
      if (bool1)
        i = j | 0x2; 
      startNestedScroll(i);
    } 
    if (this.mScrollState == 1)
      bool = true; 
    return bool;
  }
  
  public void requestDisallowInterceptTouchEvent(boolean paramBoolean) {
    int i = this.mOnItemTouchListeners.size();
    for (byte b = 0; b < i; b++) {
      OnItemTouchListener onItemTouchListener = this.mOnItemTouchListeners.get(b);
      onItemTouchListener.onRequestDisallowInterceptTouchEvent(paramBoolean);
    } 
    super.requestDisallowInterceptTouchEvent(paramBoolean);
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    int[] arrayOfInt1;
    boolean bool1 = this.mLayoutFrozen;
    boolean bool = false;
    if (bool1 || this.mIgnoreMotionEventTillDown)
      return false; 
    if (dispatchOnItemTouch(paramMotionEvent)) {
      cancelTouch();
      return true;
    } 
    LayoutManager layoutManager = this.mLayout;
    if (layoutManager == null)
      return false; 
    boolean bool2 = layoutManager.canScrollHorizontally();
    bool1 = this.mLayout.canScrollVertically();
    if (this.mVelocityTracker == null)
      this.mVelocityTracker = VelocityTracker.obtain(); 
    MotionEvent motionEvent = MotionEvent.obtain(paramMotionEvent);
    int i = paramMotionEvent.getActionMasked();
    int j = paramMotionEvent.getActionIndex();
    if (i == 0) {
      int[] arrayOfInt = this.mNestedOffsets;
      arrayOfInt[1] = 0;
      arrayOfInt[0] = 0;
    } 
    int[] arrayOfInt2 = this.mNestedOffsets;
    motionEvent.offsetLocation(arrayOfInt2[0], arrayOfInt2[1]);
    if (i != 0) {
      if (i != 1) {
        if (i != 2) {
          if (i != 3) {
            if (i != 5) {
              if (i == 6)
                onPointerUp(paramMotionEvent); 
            } else {
              this.mScrollPointerId = paramMotionEvent.getPointerId(j);
              this.mLastTouchX = i = (int)(paramMotionEvent.getX(j) + 0.5F);
              this.mInitialTouchX = i;
              this.mLastTouchY = j = (int)(paramMotionEvent.getY(j) + 0.5F);
              this.mInitialTouchY = j;
            } 
          } else {
            cancelTouch();
          } 
        } else {
          StringBuilder stringBuilder;
          j = paramMotionEvent.findPointerIndex(this.mScrollPointerId);
          if (j < 0) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Error processing scroll; pointer index for id ");
            stringBuilder.append(this.mScrollPointerId);
            stringBuilder.append(" not found. Did any MotionEvents get skipped?");
            Log.e("RecyclerView", stringBuilder.toString());
            return false;
          } 
          int k = (int)(stringBuilder.getX(j) + 0.5F);
          int m = (int)(stringBuilder.getY(j) + 0.5F);
          i = this.mLastTouchX - k;
          j = this.mLastTouchY - m;
          if (dispatchNestedPreScroll(i, j, this.mScrollConsumed, this.mScrollOffset)) {
            arrayOfInt1 = this.mScrollConsumed;
            i -= arrayOfInt1[0];
            j -= arrayOfInt1[1];
            arrayOfInt1 = this.mScrollOffset;
            motionEvent.offsetLocation(arrayOfInt1[0], arrayOfInt1[1]);
            arrayOfInt1 = this.mNestedOffsets;
            int i2 = arrayOfInt1[0];
            arrayOfInt2 = this.mScrollOffset;
            arrayOfInt1[0] = i2 + arrayOfInt2[0];
            arrayOfInt1[1] = arrayOfInt1[1] + arrayOfInt2[1];
          } 
          int n = i, i1 = j;
          if (this.mScrollState != 1) {
            int i2 = 0;
            i1 = i2;
            n = i;
            if (bool2) {
              int i3 = Math.abs(i), i4 = this.mTouchSlop;
              i1 = i2;
              n = i;
              if (i3 > i4) {
                if (i > 0) {
                  n = i - i4;
                } else {
                  n = i + i4;
                } 
                i1 = 1;
              } 
            } 
            i2 = i1;
            i = j;
            if (bool1) {
              int i4 = Math.abs(j), i3 = this.mTouchSlop;
              i2 = i1;
              i = j;
              if (i4 > i3) {
                if (j > 0) {
                  i = j - i3;
                } else {
                  i = j + i3;
                } 
                i2 = 1;
              } 
            } 
            if (i2 != 0) {
              setScrollState(1);
              i1 = i;
            } else {
              i1 = i;
            } 
          } 
          if (this.mScrollState == 1) {
            arrayOfInt1 = this.mScrollOffset;
            this.mLastTouchX = k - arrayOfInt1[0];
            this.mLastTouchY = m - arrayOfInt1[1];
            if (bool2) {
              j = n;
            } else {
              j = 0;
            } 
            i = bool;
            if (bool1)
              i = i1; 
            if (scrollByInternal(j, i, motionEvent))
              getParent().requestDisallowInterceptTouchEvent(true); 
            if (this.mGapWorker != null && (n != 0 || i1 != 0))
              this.mGapWorker.postFromTraversal(this, n, i1); 
          } 
        } 
      } else {
        float f1, f2;
        this.mVelocityTracker.addMovement(motionEvent);
        j = 1;
        this.mVelocityTracker.computeCurrentVelocity(1000, this.mMaxFlingVelocity);
        if (bool2) {
          f1 = -this.mVelocityTracker.getXVelocity(this.mScrollPointerId);
        } else {
          f1 = 0.0F;
        } 
        if (bool1) {
          f2 = -this.mVelocityTracker.getYVelocity(this.mScrollPointerId);
        } else {
          f2 = 0.0F;
        } 
        if ((f1 == 0.0F && f2 == 0.0F) || !fling((int)f1, (int)f2))
          setScrollState(0); 
        resetTouch();
        if (j == 0)
          this.mVelocityTracker.addMovement(motionEvent); 
      } 
    } else {
      this.mScrollPointerId = arrayOfInt1.getPointerId(0);
      this.mLastTouchX = j = (int)(arrayOfInt1.getX() + 0.5F);
      this.mInitialTouchX = j;
      this.mLastTouchY = j = (int)(arrayOfInt1.getY() + 0.5F);
      this.mInitialTouchY = j;
      j = 0;
      if (bool2)
        j = false | true; 
      i = j;
      if (bool1)
        i = j | 0x2; 
      startNestedScroll(i);
    } 
    j = 0;
    if (j == 0)
      this.mVelocityTracker.addMovement(motionEvent); 
  }
  
  private void resetTouch() {
    VelocityTracker velocityTracker = this.mVelocityTracker;
    if (velocityTracker != null)
      velocityTracker.clear(); 
    stopNestedScroll();
    releaseGlows();
  }
  
  private void cancelTouch() {
    resetTouch();
    setScrollState(0);
  }
  
  private void onPointerUp(MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.getActionIndex();
    if (paramMotionEvent.getPointerId(i) == this.mScrollPointerId) {
      if (i == 0) {
        i = 1;
      } else {
        i = 0;
      } 
      this.mScrollPointerId = paramMotionEvent.getPointerId(i);
      int j = (int)(paramMotionEvent.getX(i) + 0.5F);
      this.mInitialTouchX = j;
      this.mLastTouchY = i = (int)(paramMotionEvent.getY(i) + 0.5F);
      this.mInitialTouchY = i;
    } 
  }
  
  public boolean onGenericMotionEvent(MotionEvent paramMotionEvent) {
    if (this.mLayout == null)
      return false; 
    if (this.mLayoutFrozen)
      return false; 
    if ((paramMotionEvent.getSource() & 0x2) != 0 && 
      paramMotionEvent.getAction() == 8) {
      float f1;
      float f2;
      if (this.mLayout.canScrollVertically()) {
        f1 = -paramMotionEvent.getAxisValue(9);
      } else {
        f1 = 0.0F;
      } 
      if (this.mLayout.canScrollHorizontally()) {
        f2 = paramMotionEvent.getAxisValue(10);
      } else {
        f2 = 0.0F;
      } 
      if (f1 != 0.0F || f2 != 0.0F) {
        float f = getScrollFactor();
        scrollByInternal((int)(f2 * f), (int)(f1 * f), paramMotionEvent);
      } 
    } 
    return false;
  }
  
  private float getScrollFactor() {
    if (this.mScrollFactor == Float.MIN_VALUE) {
      TypedValue typedValue = new TypedValue();
      if (getContext().getTheme().resolveAttribute(16842829, typedValue, true)) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        this.mScrollFactor = typedValue.getDimension(displayMetrics);
      } else {
        return 0.0F;
      } 
    } 
    return this.mScrollFactor;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    LayoutManager layoutManager = this.mLayout;
    if (layoutManager == null) {
      defaultOnMeasure(paramInt1, paramInt2);
      return;
    } 
    boolean bool = layoutManager.mAutoMeasure;
    int i = 0;
    if (bool) {
      int j = View.MeasureSpec.getMode(paramInt1);
      int k = View.MeasureSpec.getMode(paramInt2);
      int m = i;
      if (j == 1073741824) {
        m = i;
        if (k == 1073741824)
          m = 1; 
      } 
      this.mLayout.onMeasure(this.mRecycler, this.mState, paramInt1, paramInt2);
      if (m || this.mAdapter == null)
        return; 
      if (this.mState.mLayoutStep == 1)
        dispatchLayoutStep1(); 
      this.mLayout.setMeasureSpecs(paramInt1, paramInt2);
      this.mState.mIsMeasuring = true;
      dispatchLayoutStep2();
      this.mLayout.setMeasuredDimensionFromChildren(paramInt1, paramInt2);
      if (this.mLayout.shouldMeasureTwice()) {
        layoutManager = this.mLayout;
        i = View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824);
        m = View.MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 1073741824);
        layoutManager.setMeasureSpecs(i, m);
        this.mState.mIsMeasuring = true;
        dispatchLayoutStep2();
        this.mLayout.setMeasuredDimensionFromChildren(paramInt1, paramInt2);
      } 
    } else {
      if (this.mHasFixedSize) {
        this.mLayout.onMeasure(this.mRecycler, this.mState, paramInt1, paramInt2);
        return;
      } 
      if (this.mAdapterUpdateDuringMeasure) {
        eatRequestLayout();
        onEnterLayoutOrScroll();
        processAdapterUpdatesAndSetAnimationFlags();
        onExitLayoutOrScroll();
        if (this.mState.mRunPredictiveAnimations) {
          this.mState.mInPreLayout = true;
        } else {
          this.mAdapterHelper.consumeUpdatesInOnePass();
          this.mState.mInPreLayout = false;
        } 
        this.mAdapterUpdateDuringMeasure = false;
        resumeRequestLayout(false);
      } 
      Adapter adapter = this.mAdapter;
      if (adapter != null) {
        this.mState.mItemCount = adapter.getItemCount();
      } else {
        this.mState.mItemCount = 0;
      } 
      eatRequestLayout();
      this.mLayout.onMeasure(this.mRecycler, this.mState, paramInt1, paramInt2);
      resumeRequestLayout(false);
      this.mState.mInPreLayout = false;
    } 
  }
  
  void defaultOnMeasure(int paramInt1, int paramInt2) {
    int i = getPaddingLeft(), j = getPaddingRight();
    int k = getMinimumWidth();
    paramInt1 = LayoutManager.chooseSize(paramInt1, i + j, k);
    j = getPaddingTop();
    k = getPaddingBottom();
    i = getMinimumHeight();
    paramInt2 = LayoutManager.chooseSize(paramInt2, j + k, i);
    setMeasuredDimension(paramInt1, paramInt2);
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    if (paramInt1 != paramInt3 || paramInt2 != paramInt4)
      invalidateGlows(); 
  }
  
  public void setItemAnimator(ItemAnimator paramItemAnimator) {
    ItemAnimator itemAnimator = this.mItemAnimator;
    if (itemAnimator != null) {
      itemAnimator.endAnimations();
      this.mItemAnimator.setListener(null);
    } 
    this.mItemAnimator = paramItemAnimator;
    if (paramItemAnimator != null)
      paramItemAnimator.setListener(this.mItemAnimatorListener); 
  }
  
  void onEnterLayoutOrScroll() {
    this.mLayoutOrScrollCounter++;
  }
  
  void onExitLayoutOrScroll() {
    int i = this.mLayoutOrScrollCounter - 1;
    if (i < 1) {
      this.mLayoutOrScrollCounter = 0;
      dispatchContentChangedIfNecessary();
      dispatchPendingImportantForAccessibilityChanges();
    } 
  }
  
  boolean isAccessibilityEnabled() {
    boolean bool;
    AccessibilityManager accessibilityManager = this.mAccessibilityManager;
    if (accessibilityManager != null && accessibilityManager.isEnabled()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void dispatchContentChangedIfNecessary() {
    int i = this.mEatenAccessibilityChangeFlags;
    this.mEatenAccessibilityChangeFlags = 0;
    if (i != 0 && isAccessibilityEnabled()) {
      AccessibilityEvent accessibilityEvent = AccessibilityEvent.obtain();
      accessibilityEvent.setEventType(2048);
      accessibilityEvent.setContentChangeTypes(i);
      sendAccessibilityEventUnchecked(accessibilityEvent);
    } 
  }
  
  public boolean isComputingLayout() {
    boolean bool;
    if (this.mLayoutOrScrollCounter > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  boolean shouldDeferAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent) {
    if (isComputingLayout()) {
      int i = 0;
      if (paramAccessibilityEvent != null)
        i = paramAccessibilityEvent.getContentChangeTypes(); 
      int j = i;
      if (i == 0)
        j = 0; 
      this.mEatenAccessibilityChangeFlags |= j;
      return true;
    } 
    return false;
  }
  
  public void sendAccessibilityEventUnchecked(AccessibilityEvent paramAccessibilityEvent) {
    if (shouldDeferAccessibilityEvent(paramAccessibilityEvent))
      return; 
    super.sendAccessibilityEventUnchecked(paramAccessibilityEvent);
  }
  
  public ItemAnimator getItemAnimator() {
    return this.mItemAnimator;
  }
  
  void postAnimationRunner() {
    if (!this.mPostedAnimatorRunner && this.mIsAttached) {
      postOnAnimation(this.mItemAnimatorRunner);
      this.mPostedAnimatorRunner = true;
    } 
  }
  
  private boolean predictiveItemAnimationsEnabled() {
    boolean bool;
    if (this.mItemAnimator != null && this.mLayout.supportsPredictiveItemAnimations()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void processAdapterUpdatesAndSetAnimationFlags() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mDataSetHasChangedAfterLayout : Z
    //   4: ifeq -> 22
    //   7: aload_0
    //   8: getfield mAdapterHelper : Lcom/android/internal/widget/AdapterHelper;
    //   11: invokevirtual reset : ()V
    //   14: aload_0
    //   15: getfield mLayout : Lcom/android/internal/widget/RecyclerView$LayoutManager;
    //   18: aload_0
    //   19: invokevirtual onItemsChanged : (Lcom/android/internal/widget/RecyclerView;)V
    //   22: aload_0
    //   23: invokespecial predictiveItemAnimationsEnabled : ()Z
    //   26: ifeq -> 39
    //   29: aload_0
    //   30: getfield mAdapterHelper : Lcom/android/internal/widget/AdapterHelper;
    //   33: invokevirtual preProcess : ()V
    //   36: goto -> 46
    //   39: aload_0
    //   40: getfield mAdapterHelper : Lcom/android/internal/widget/AdapterHelper;
    //   43: invokevirtual consumeUpdatesInOnePass : ()V
    //   46: aload_0
    //   47: getfield mItemsAddedOrRemoved : Z
    //   50: istore_1
    //   51: iconst_0
    //   52: istore_2
    //   53: iload_1
    //   54: ifne -> 72
    //   57: aload_0
    //   58: getfield mItemsChanged : Z
    //   61: ifeq -> 67
    //   64: goto -> 72
    //   67: iconst_0
    //   68: istore_3
    //   69: goto -> 74
    //   72: iconst_1
    //   73: istore_3
    //   74: aload_0
    //   75: getfield mState : Lcom/android/internal/widget/RecyclerView$State;
    //   78: astore #4
    //   80: aload_0
    //   81: getfield mFirstLayoutComplete : Z
    //   84: ifeq -> 141
    //   87: aload_0
    //   88: getfield mItemAnimator : Lcom/android/internal/widget/RecyclerView$ItemAnimator;
    //   91: ifnull -> 141
    //   94: aload_0
    //   95: getfield mDataSetHasChangedAfterLayout : Z
    //   98: ifne -> 115
    //   101: iload_3
    //   102: ifne -> 115
    //   105: aload_0
    //   106: getfield mLayout : Lcom/android/internal/widget/RecyclerView$LayoutManager;
    //   109: getfield mRequestedSimpleAnimations : Z
    //   112: ifeq -> 141
    //   115: aload_0
    //   116: getfield mDataSetHasChangedAfterLayout : Z
    //   119: ifeq -> 136
    //   122: aload_0
    //   123: getfield mAdapter : Lcom/android/internal/widget/RecyclerView$Adapter;
    //   126: astore #5
    //   128: aload #5
    //   130: invokevirtual hasStableIds : ()Z
    //   133: ifeq -> 141
    //   136: iconst_1
    //   137: istore_1
    //   138: goto -> 143
    //   141: iconst_0
    //   142: istore_1
    //   143: aload #4
    //   145: iload_1
    //   146: putfield mRunSimpleAnimations : Z
    //   149: aload_0
    //   150: getfield mState : Lcom/android/internal/widget/RecyclerView$State;
    //   153: astore #5
    //   155: aload #5
    //   157: getfield mRunSimpleAnimations : Z
    //   160: ifeq -> 186
    //   163: iload_3
    //   164: ifeq -> 186
    //   167: aload_0
    //   168: getfield mDataSetHasChangedAfterLayout : Z
    //   171: ifne -> 186
    //   174: aload_0
    //   175: invokespecial predictiveItemAnimationsEnabled : ()Z
    //   178: ifeq -> 186
    //   181: iconst_1
    //   182: istore_1
    //   183: goto -> 188
    //   186: iload_2
    //   187: istore_1
    //   188: aload #5
    //   190: iload_1
    //   191: putfield mRunPredictiveAnimations : Z
    //   194: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #3205	-> 0
    //   #3208	-> 7
    //   #3209	-> 14
    //   #3214	-> 22
    //   #3215	-> 29
    //   #3217	-> 39
    //   #3219	-> 46
    //   #3220	-> 74
    //   #3226	-> 128
    //   #3227	-> 149
    //   #3230	-> 174
    //   #3231	-> 194
  }
  
  void dispatchLayout() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mAdapter : Lcom/android/internal/widget/RecyclerView$Adapter;
    //   4: ifnonnull -> 17
    //   7: ldc 'RecyclerView'
    //   9: ldc_w 'No adapter attached; skipping layout'
    //   12: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   15: pop
    //   16: return
    //   17: aload_0
    //   18: getfield mLayout : Lcom/android/internal/widget/RecyclerView$LayoutManager;
    //   21: ifnonnull -> 34
    //   24: ldc 'RecyclerView'
    //   26: ldc_w 'No layout manager attached; skipping layout'
    //   29: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   32: pop
    //   33: return
    //   34: aload_0
    //   35: getfield mState : Lcom/android/internal/widget/RecyclerView$State;
    //   38: iconst_0
    //   39: putfield mIsMeasuring : Z
    //   42: aload_0
    //   43: getfield mState : Lcom/android/internal/widget/RecyclerView$State;
    //   46: getfield mLayoutStep : I
    //   49: iconst_1
    //   50: if_icmpne -> 72
    //   53: aload_0
    //   54: invokespecial dispatchLayoutStep1 : ()V
    //   57: aload_0
    //   58: getfield mLayout : Lcom/android/internal/widget/RecyclerView$LayoutManager;
    //   61: aload_0
    //   62: invokevirtual setExactMeasureSpecsFrom : (Lcom/android/internal/widget/RecyclerView;)V
    //   65: aload_0
    //   66: invokespecial dispatchLayoutStep2 : ()V
    //   69: goto -> 138
    //   72: aload_0
    //   73: getfield mAdapterHelper : Lcom/android/internal/widget/AdapterHelper;
    //   76: invokevirtual hasUpdates : ()Z
    //   79: ifne -> 126
    //   82: aload_0
    //   83: getfield mLayout : Lcom/android/internal/widget/RecyclerView$LayoutManager;
    //   86: invokevirtual getWidth : ()I
    //   89: aload_0
    //   90: invokevirtual getWidth : ()I
    //   93: if_icmpne -> 126
    //   96: aload_0
    //   97: getfield mLayout : Lcom/android/internal/widget/RecyclerView$LayoutManager;
    //   100: astore_1
    //   101: aload_1
    //   102: invokevirtual getHeight : ()I
    //   105: aload_0
    //   106: invokevirtual getHeight : ()I
    //   109: if_icmpeq -> 115
    //   112: goto -> 126
    //   115: aload_0
    //   116: getfield mLayout : Lcom/android/internal/widget/RecyclerView$LayoutManager;
    //   119: aload_0
    //   120: invokevirtual setExactMeasureSpecsFrom : (Lcom/android/internal/widget/RecyclerView;)V
    //   123: goto -> 138
    //   126: aload_0
    //   127: getfield mLayout : Lcom/android/internal/widget/RecyclerView$LayoutManager;
    //   130: aload_0
    //   131: invokevirtual setExactMeasureSpecsFrom : (Lcom/android/internal/widget/RecyclerView;)V
    //   134: aload_0
    //   135: invokespecial dispatchLayoutStep2 : ()V
    //   138: aload_0
    //   139: invokespecial dispatchLayoutStep3 : ()V
    //   142: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #3259	-> 0
    //   #3260	-> 7
    //   #3262	-> 16
    //   #3264	-> 17
    //   #3265	-> 24
    //   #3267	-> 33
    //   #3269	-> 34
    //   #3270	-> 42
    //   #3271	-> 53
    //   #3272	-> 57
    //   #3273	-> 65
    //   #3274	-> 72
    //   #3275	-> 101
    //   #3282	-> 115
    //   #3278	-> 126
    //   #3279	-> 134
    //   #3284	-> 138
    //   #3285	-> 142
  }
  
  private void saveFocusInfo() {
    ViewHolder viewHolder;
    View view1 = null;
    View view2 = view1;
    if (this.mPreserveFocusAfterLayout) {
      view2 = view1;
      if (hasFocus()) {
        view2 = view1;
        if (this.mAdapter != null)
          view2 = getFocusedChild(); 
      } 
    } 
    if (view2 == null) {
      view2 = null;
    } else {
      viewHolder = findContainingViewHolder(view2);
    } 
    if (viewHolder == null) {
      resetFocusInfo();
    } else {
      long l;
      int i;
      State state = this.mState;
      if (this.mAdapter.hasStableIds()) {
        l = viewHolder.getItemId();
      } else {
        l = -1L;
      } 
      state.mFocusedItemId = l;
      state = this.mState;
      if (this.mDataSetHasChangedAfterLayout) {
        i = -1;
      } else if (viewHolder.isRemoved()) {
        i = viewHolder.mOldPosition;
      } else {
        i = viewHolder.getAdapterPosition();
      } 
      state.mFocusedItemPosition = i;
      this.mState.mFocusedSubChildId = getDeepestFocusedViewWithId(viewHolder.itemView);
    } 
  }
  
  private void resetFocusInfo() {
    this.mState.mFocusedItemId = -1L;
    this.mState.mFocusedItemPosition = -1;
    this.mState.mFocusedSubChildId = -1;
  }
  
  private View findNextViewToFocus() {
    if (this.mState.mFocusedItemPosition != -1) {
      i = this.mState.mFocusedItemPosition;
    } else {
      i = 0;
    } 
    int j = this.mState.getItemCount();
    for (int k = i; k < j; k++) {
      ViewHolder viewHolder = findViewHolderForAdapterPosition(k);
      if (viewHolder == null)
        break; 
      if (viewHolder.itemView.hasFocusable())
        return viewHolder.itemView; 
    } 
    int i = Math.min(j, i);
    for (; --i >= 0; i--) {
      ViewHolder viewHolder = findViewHolderForAdapterPosition(i);
      if (viewHolder == null)
        return null; 
      if (viewHolder.itemView.hasFocusable())
        return viewHolder.itemView; 
    } 
    return null;
  }
  
  private void recoverFocusFromState() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mPreserveFocusAfterLayout : Z
    //   4: ifeq -> 269
    //   7: aload_0
    //   8: getfield mAdapter : Lcom/android/internal/widget/RecyclerView$Adapter;
    //   11: ifnull -> 269
    //   14: aload_0
    //   15: invokevirtual hasFocus : ()Z
    //   18: ifeq -> 269
    //   21: aload_0
    //   22: invokevirtual getDescendantFocusability : ()I
    //   25: ldc_w 393216
    //   28: if_icmpeq -> 269
    //   31: aload_0
    //   32: invokevirtual getDescendantFocusability : ()I
    //   35: ldc_w 131072
    //   38: if_icmpne -> 51
    //   41: aload_0
    //   42: invokevirtual isFocused : ()Z
    //   45: ifeq -> 51
    //   48: goto -> 269
    //   51: aload_0
    //   52: invokevirtual isFocused : ()Z
    //   55: ifne -> 111
    //   58: aload_0
    //   59: invokevirtual getFocusedChild : ()Landroid/view/View;
    //   62: astore_1
    //   63: getstatic com/android/internal/widget/RecyclerView.IGNORE_DETACHED_FOCUSED_CHILD : Z
    //   66: ifeq -> 99
    //   69: aload_1
    //   70: invokevirtual getParent : ()Landroid/view/ViewParent;
    //   73: ifnull -> 83
    //   76: aload_1
    //   77: invokevirtual hasFocus : ()Z
    //   80: ifne -> 99
    //   83: aload_0
    //   84: getfield mChildHelper : Lcom/android/internal/widget/ChildHelper;
    //   87: invokevirtual getChildCount : ()I
    //   90: ifne -> 111
    //   93: aload_0
    //   94: invokevirtual requestFocus : ()Z
    //   97: pop
    //   98: return
    //   99: aload_0
    //   100: getfield mChildHelper : Lcom/android/internal/widget/ChildHelper;
    //   103: aload_1
    //   104: invokevirtual isHidden : (Landroid/view/View;)Z
    //   107: ifne -> 111
    //   110: return
    //   111: aconst_null
    //   112: astore_2
    //   113: aload_2
    //   114: astore_1
    //   115: aload_0
    //   116: getfield mState : Lcom/android/internal/widget/RecyclerView$State;
    //   119: getfield mFocusedItemId : J
    //   122: ldc2_w -1
    //   125: lcmp
    //   126: ifeq -> 153
    //   129: aload_2
    //   130: astore_1
    //   131: aload_0
    //   132: getfield mAdapter : Lcom/android/internal/widget/RecyclerView$Adapter;
    //   135: invokevirtual hasStableIds : ()Z
    //   138: ifeq -> 153
    //   141: aload_0
    //   142: aload_0
    //   143: getfield mState : Lcom/android/internal/widget/RecyclerView$State;
    //   146: getfield mFocusedItemId : J
    //   149: invokevirtual findViewHolderForItemId : (J)Lcom/android/internal/widget/RecyclerView$ViewHolder;
    //   152: astore_1
    //   153: aconst_null
    //   154: astore_2
    //   155: aload_1
    //   156: ifnull -> 196
    //   159: aload_0
    //   160: getfield mChildHelper : Lcom/android/internal/widget/ChildHelper;
    //   163: aload_1
    //   164: getfield itemView : Landroid/view/View;
    //   167: invokevirtual isHidden : (Landroid/view/View;)Z
    //   170: ifne -> 196
    //   173: aload_1
    //   174: getfield itemView : Landroid/view/View;
    //   177: astore_3
    //   178: aload_3
    //   179: invokevirtual hasFocusable : ()Z
    //   182: ifne -> 188
    //   185: goto -> 196
    //   188: aload_1
    //   189: getfield itemView : Landroid/view/View;
    //   192: astore_1
    //   193: goto -> 213
    //   196: aload_2
    //   197: astore_1
    //   198: aload_0
    //   199: getfield mChildHelper : Lcom/android/internal/widget/ChildHelper;
    //   202: invokevirtual getChildCount : ()I
    //   205: ifle -> 213
    //   208: aload_0
    //   209: invokespecial findNextViewToFocus : ()Landroid/view/View;
    //   212: astore_1
    //   213: aload_1
    //   214: ifnull -> 268
    //   217: aload_1
    //   218: astore_2
    //   219: aload_0
    //   220: getfield mState : Lcom/android/internal/widget/RecyclerView$State;
    //   223: getfield mFocusedSubChildId : I
    //   226: i2l
    //   227: ldc2_w -1
    //   230: lcmp
    //   231: ifeq -> 263
    //   234: aload_1
    //   235: aload_0
    //   236: getfield mState : Lcom/android/internal/widget/RecyclerView$State;
    //   239: getfield mFocusedSubChildId : I
    //   242: invokevirtual findViewById : (I)Landroid/view/View;
    //   245: astore_3
    //   246: aload_1
    //   247: astore_2
    //   248: aload_3
    //   249: ifnull -> 263
    //   252: aload_1
    //   253: astore_2
    //   254: aload_3
    //   255: invokevirtual isFocusable : ()Z
    //   258: ifeq -> 263
    //   261: aload_3
    //   262: astore_2
    //   263: aload_2
    //   264: invokevirtual requestFocus : ()Z
    //   267: pop
    //   268: return
    //   269: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #3351	-> 0
    //   #3352	-> 21
    //   #3353	-> 31
    //   #3361	-> 51
    //   #3362	-> 58
    //   #3363	-> 63
    //   #3364	-> 69
    //   #3376	-> 83
    //   #3379	-> 93
    //   #3380	-> 98
    //   #3382	-> 99
    //   #3385	-> 110
    //   #3388	-> 111
    //   #3392	-> 113
    //   #3393	-> 141
    //   #3395	-> 153
    //   #3396	-> 155
    //   #3397	-> 178
    //   #3410	-> 188
    //   #3398	-> 196
    //   #3405	-> 208
    //   #3413	-> 213
    //   #3414	-> 217
    //   #3415	-> 234
    //   #3416	-> 246
    //   #3417	-> 261
    //   #3420	-> 263
    //   #3422	-> 268
    //   #3358	-> 269
  }
  
  private int getDeepestFocusedViewWithId(View paramView) {
    int i = paramView.getId();
    while (!paramView.isFocused() && paramView instanceof ViewGroup && paramView.hasFocus()) {
      paramView = ((ViewGroup)paramView).getFocusedChild();
      int j = paramView.getId();
      if (j != -1)
        i = paramView.getId(); 
    } 
    return i;
  }
  
  private void dispatchLayoutStep1() {
    State state = this.mState;
    boolean bool = true;
    state.assertLayoutStep(1);
    this.mState.mIsMeasuring = false;
    eatRequestLayout();
    this.mViewInfoStore.clear();
    onEnterLayoutOrScroll();
    processAdapterUpdatesAndSetAnimationFlags();
    saveFocusInfo();
    state = this.mState;
    if (!state.mRunSimpleAnimations || !this.mItemsChanged)
      bool = false; 
    state.mTrackOldChangeHolders = bool;
    this.mItemsChanged = false;
    this.mItemsAddedOrRemoved = false;
    state = this.mState;
    state.mInPreLayout = state.mRunPredictiveAnimations;
    this.mState.mItemCount = this.mAdapter.getItemCount();
    findMinMaxChildLayoutPositions(this.mMinMaxLayoutPositions);
    if (this.mState.mRunSimpleAnimations) {
      int i = this.mChildHelper.getChildCount();
      for (byte b = 0; b < i; b++) {
        ViewHolder viewHolder = getChildViewHolderInt(this.mChildHelper.getChildAt(b));
        if (!viewHolder.shouldIgnore() && (!viewHolder.isInvalid() || this.mAdapter.hasStableIds())) {
          ItemAnimator itemAnimator = this.mItemAnimator;
          State state1 = this.mState;
          int j = ItemAnimator.buildAdapterChangeFlagsForAnimations(viewHolder);
          List<Object> list = viewHolder.getUnmodifiedPayloads();
          ItemAnimator.ItemHolderInfo itemHolderInfo = itemAnimator.recordPreLayoutInformation(state1, viewHolder, j, list);
          this.mViewInfoStore.addToPreLayout(viewHolder, itemHolderInfo);
          if (this.mState.mTrackOldChangeHolders && viewHolder.isUpdated() && !viewHolder.isRemoved() && 
            !viewHolder.shouldIgnore() && !viewHolder.isInvalid()) {
            long l = getChangedHolderKey(viewHolder);
            this.mViewInfoStore.addToOldChangeHolders(l, viewHolder);
          } 
        } 
      } 
    } 
    if (this.mState.mRunPredictiveAnimations) {
      saveOldPositions();
      bool = this.mState.mStructureChanged;
      this.mState.mStructureChanged = false;
      this.mLayout.onLayoutChildren(this.mRecycler, this.mState);
      this.mState.mStructureChanged = bool;
      for (byte b = 0; b < this.mChildHelper.getChildCount(); b++) {
        View view = this.mChildHelper.getChildAt(b);
        ViewHolder viewHolder = getChildViewHolderInt(view);
        if (!viewHolder.shouldIgnore())
          if (!this.mViewInfoStore.isInPreLayout(viewHolder)) {
            int j = ItemAnimator.buildAdapterChangeFlagsForAnimations(viewHolder);
            bool = viewHolder.hasAnyOfTheFlags(8192);
            int i = j;
            if (!bool)
              i = j | 0x1000; 
            ItemAnimator itemAnimator = this.mItemAnimator;
            State state1 = this.mState;
            List<Object> list = viewHolder.getUnmodifiedPayloads();
            ItemAnimator.ItemHolderInfo itemHolderInfo = itemAnimator.recordPreLayoutInformation(state1, viewHolder, i, list);
            if (bool) {
              recordAnimationInfoIfBouncedHiddenView(viewHolder, itemHolderInfo);
            } else {
              this.mViewInfoStore.addToAppearedInPreLayoutHolders(viewHolder, itemHolderInfo);
            } 
          }  
      } 
      clearOldPositions();
    } else {
      clearOldPositions();
    } 
    onExitLayoutOrScroll();
    resumeRequestLayout(false);
    this.mState.mLayoutStep = 2;
  }
  
  private void dispatchLayoutStep2() {
    boolean bool;
    eatRequestLayout();
    onEnterLayoutOrScroll();
    this.mState.assertLayoutStep(6);
    this.mAdapterHelper.consumeUpdatesInOnePass();
    this.mState.mItemCount = this.mAdapter.getItemCount();
    this.mState.mDeletedInvisibleItemCountSincePreviousLayout = 0;
    this.mState.mInPreLayout = false;
    this.mLayout.onLayoutChildren(this.mRecycler, this.mState);
    this.mState.mStructureChanged = false;
    this.mPendingSavedState = null;
    State state = this.mState;
    if (state.mRunSimpleAnimations && this.mItemAnimator != null) {
      bool = true;
    } else {
      bool = false;
    } 
    state.mRunSimpleAnimations = bool;
    this.mState.mLayoutStep = 4;
    onExitLayoutOrScroll();
    resumeRequestLayout(false);
  }
  
  private void dispatchLayoutStep3() {
    this.mState.assertLayoutStep(4);
    eatRequestLayout();
    onEnterLayoutOrScroll();
    this.mState.mLayoutStep = 1;
    if (this.mState.mRunSimpleAnimations) {
      for (int i = this.mChildHelper.getChildCount() - 1; i >= 0; i--) {
        ViewHolder viewHolder = getChildViewHolderInt(this.mChildHelper.getChildAt(i));
        if (!viewHolder.shouldIgnore()) {
          long l = getChangedHolderKey(viewHolder);
          ItemAnimator itemAnimator = this.mItemAnimator;
          State state1 = this.mState;
          ItemAnimator.ItemHolderInfo itemHolderInfo = itemAnimator.recordPostLayoutInformation(state1, viewHolder);
          ViewHolder viewHolder1 = this.mViewInfoStore.getFromOldChangeHolders(l);
          if (viewHolder1 != null && !viewHolder1.shouldIgnore()) {
            boolean bool1 = this.mViewInfoStore.isDisappearing(viewHolder1);
            boolean bool2 = this.mViewInfoStore.isDisappearing(viewHolder);
            if (bool1 && viewHolder1 == viewHolder) {
              this.mViewInfoStore.addToPostLayout(viewHolder, itemHolderInfo);
            } else {
              ItemAnimator.ItemHolderInfo itemHolderInfo1 = this.mViewInfoStore.popFromPreLayout(viewHolder1);
              this.mViewInfoStore.addToPostLayout(viewHolder, itemHolderInfo);
              itemHolderInfo = this.mViewInfoStore.popFromPostLayout(viewHolder);
              if (itemHolderInfo1 == null) {
                handleMissingPreInfoForChangeError(l, viewHolder, viewHolder1);
              } else {
                animateChange(viewHolder1, viewHolder, itemHolderInfo1, itemHolderInfo, bool1, bool2);
              } 
            } 
          } else {
            this.mViewInfoStore.addToPostLayout(viewHolder, itemHolderInfo);
          } 
        } 
      } 
      this.mViewInfoStore.process(this.mViewInfoProcessCallback);
    } 
    this.mLayout.removeAndRecycleScrapInt(this.mRecycler);
    State state = this.mState;
    state.mPreviousLayoutItemCount = state.mItemCount;
    this.mDataSetHasChangedAfterLayout = false;
    this.mState.mRunSimpleAnimations = false;
    this.mState.mRunPredictiveAnimations = false;
    this.mLayout.mRequestedSimpleAnimations = false;
    if (this.mRecycler.mChangedScrap != null)
      this.mRecycler.mChangedScrap.clear(); 
    if (this.mLayout.mPrefetchMaxObservedInInitialPrefetch) {
      this.mLayout.mPrefetchMaxCountObserved = 0;
      this.mLayout.mPrefetchMaxObservedInInitialPrefetch = false;
      this.mRecycler.updateViewCacheSize();
    } 
    this.mLayout.onLayoutCompleted(this.mState);
    onExitLayoutOrScroll();
    resumeRequestLayout(false);
    this.mViewInfoStore.clear();
    int[] arrayOfInt = this.mMinMaxLayoutPositions;
    if (didChildRangeChange(arrayOfInt[0], arrayOfInt[1]))
      dispatchOnScrolled(0, 0); 
    recoverFocusFromState();
    resetFocusInfo();
  }
  
  private void handleMissingPreInfoForChangeError(long paramLong, ViewHolder paramViewHolder1, ViewHolder paramViewHolder2) {
    StringBuilder stringBuilder1;
    int i = this.mChildHelper.getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = this.mChildHelper.getChildAt(b);
      ViewHolder viewHolder = getChildViewHolderInt(view);
      if (viewHolder != paramViewHolder1) {
        long l = getChangedHolderKey(viewHolder);
        if (l == paramLong) {
          Adapter adapter = this.mAdapter;
          if (adapter != null && adapter.hasStableIds()) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Two different ViewHolders have the same stable ID. Stable IDs in your adapter MUST BE unique and SHOULD NOT change.\n ViewHolder 1:");
            stringBuilder.append(viewHolder);
            stringBuilder.append(" \n View Holder 2:");
            stringBuilder.append(paramViewHolder1);
            throw new IllegalStateException(stringBuilder.toString());
          } 
          stringBuilder1 = new StringBuilder();
          stringBuilder1.append("Two different ViewHolders have the same change ID. This might happen due to inconsistent Adapter update events or if the LayoutManager lays out the same View multiple times.\n ViewHolder 1:");
          stringBuilder1.append(viewHolder);
          stringBuilder1.append(" \n View Holder 2:");
          stringBuilder1.append(paramViewHolder1);
          throw new IllegalStateException(stringBuilder1.toString());
        } 
      } 
    } 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("Problem while matching changed view holders with the newones. The pre-layout information for the change holder ");
    stringBuilder2.append(stringBuilder1);
    stringBuilder2.append(" cannot be found but it is necessary for ");
    stringBuilder2.append(paramViewHolder1);
    Log.e("RecyclerView", stringBuilder2.toString());
  }
  
  void recordAnimationInfoIfBouncedHiddenView(ViewHolder paramViewHolder, ItemAnimator.ItemHolderInfo paramItemHolderInfo) {
    paramViewHolder.setFlags(0, 8192);
    if (this.mState.mTrackOldChangeHolders && paramViewHolder.isUpdated() && 
      !paramViewHolder.isRemoved() && !paramViewHolder.shouldIgnore()) {
      long l = getChangedHolderKey(paramViewHolder);
      this.mViewInfoStore.addToOldChangeHolders(l, paramViewHolder);
    } 
    this.mViewInfoStore.addToPreLayout(paramViewHolder, paramItemHolderInfo);
  }
  
  private void findMinMaxChildLayoutPositions(int[] paramArrayOfint) {
    int i = this.mChildHelper.getChildCount();
    if (i == 0) {
      paramArrayOfint[0] = -1;
      paramArrayOfint[1] = -1;
      return;
    } 
    int j = Integer.MAX_VALUE;
    int k = Integer.MIN_VALUE;
    for (byte b = 0; b < i; b++, k = m) {
      int m;
      ViewHolder viewHolder = getChildViewHolderInt(this.mChildHelper.getChildAt(b));
      if (viewHolder.shouldIgnore()) {
        m = k;
      } else {
        int n = viewHolder.getLayoutPosition();
        int i1 = j;
        if (n < j)
          i1 = n; 
        j = i1;
        m = k;
        if (n > k) {
          m = n;
          j = i1;
        } 
      } 
    } 
    paramArrayOfint[0] = j;
    paramArrayOfint[1] = k;
  }
  
  private boolean didChildRangeChange(int paramInt1, int paramInt2) {
    findMinMaxChildLayoutPositions(this.mMinMaxLayoutPositions);
    int[] arrayOfInt = this.mMinMaxLayoutPositions;
    boolean bool = false;
    if (arrayOfInt[0] != paramInt1 || arrayOfInt[1] != paramInt2)
      bool = true; 
    return bool;
  }
  
  protected void removeDetachedView(View paramView, boolean paramBoolean) {
    StringBuilder stringBuilder;
    ViewHolder viewHolder = getChildViewHolderInt(paramView);
    if (viewHolder != null)
      if (viewHolder.isTmpDetached()) {
        viewHolder.clearTmpDetachFlag();
      } else if (!viewHolder.shouldIgnore()) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Called removeDetachedView with a view which is not flagged as tmp detached.");
        stringBuilder.append(viewHolder);
        throw new IllegalArgumentException(stringBuilder.toString());
      }  
    dispatchChildDetached((View)stringBuilder);
    super.removeDetachedView((View)stringBuilder, paramBoolean);
  }
  
  long getChangedHolderKey(ViewHolder paramViewHolder) {
    long l;
    if (this.mAdapter.hasStableIds()) {
      l = paramViewHolder.getItemId();
    } else {
      l = paramViewHolder.mPosition;
    } 
    return l;
  }
  
  void animateAppearance(ViewHolder paramViewHolder, ItemAnimator.ItemHolderInfo paramItemHolderInfo1, ItemAnimator.ItemHolderInfo paramItemHolderInfo2) {
    paramViewHolder.setIsRecyclable(false);
    if (this.mItemAnimator.animateAppearance(paramViewHolder, paramItemHolderInfo1, paramItemHolderInfo2))
      postAnimationRunner(); 
  }
  
  void animateDisappearance(ViewHolder paramViewHolder, ItemAnimator.ItemHolderInfo paramItemHolderInfo1, ItemAnimator.ItemHolderInfo paramItemHolderInfo2) {
    addAnimatingView(paramViewHolder);
    paramViewHolder.setIsRecyclable(false);
    if (this.mItemAnimator.animateDisappearance(paramViewHolder, paramItemHolderInfo1, paramItemHolderInfo2))
      postAnimationRunner(); 
  }
  
  private void animateChange(ViewHolder paramViewHolder1, ViewHolder paramViewHolder2, ItemAnimator.ItemHolderInfo paramItemHolderInfo1, ItemAnimator.ItemHolderInfo paramItemHolderInfo2, boolean paramBoolean1, boolean paramBoolean2) {
    paramViewHolder1.setIsRecyclable(false);
    if (paramBoolean1)
      addAnimatingView(paramViewHolder1); 
    if (paramViewHolder1 != paramViewHolder2) {
      if (paramBoolean2)
        addAnimatingView(paramViewHolder2); 
      paramViewHolder1.mShadowedHolder = paramViewHolder2;
      addAnimatingView(paramViewHolder1);
      this.mRecycler.unscrapView(paramViewHolder1);
      paramViewHolder2.setIsRecyclable(false);
      paramViewHolder2.mShadowingHolder = paramViewHolder1;
    } 
    if (this.mItemAnimator.animateChange(paramViewHolder1, paramViewHolder2, paramItemHolderInfo1, paramItemHolderInfo2))
      postAnimationRunner(); 
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    Trace.beginSection("RV OnLayout");
    dispatchLayout();
    Trace.endSection();
    this.mFirstLayoutComplete = true;
  }
  
  public void requestLayout() {
    if (this.mEatRequestLayout == 0 && !this.mLayoutFrozen) {
      super.requestLayout();
    } else {
      this.mLayoutRequestEaten = true;
    } 
  }
  
  void markItemDecorInsetsDirty() {
    int i = this.mChildHelper.getUnfilteredChildCount();
    for (byte b = 0; b < i; b++) {
      View view = this.mChildHelper.getUnfilteredChildAt(b);
      ((LayoutParams)view.getLayoutParams()).mInsetsDirty = true;
    } 
    this.mRecycler.markItemDecorInsetsDirty();
  }
  
  public void draw(Canvas paramCanvas) {
    super.draw(paramCanvas);
    int i = this.mItemDecorations.size();
    int j;
    for (j = 0; j < i; j++)
      ((ItemDecoration)this.mItemDecorations.get(j)).onDrawOver(paramCanvas, this, this.mState); 
    j = 0;
    EdgeEffect edgeEffect = this.mLeftGlow;
    boolean bool = true;
    i = j;
    if (edgeEffect != null) {
      i = j;
      if (!edgeEffect.isFinished()) {
        i = paramCanvas.save();
        if (this.mClipToPadding) {
          j = getPaddingBottom();
        } else {
          j = 0;
        } 
        paramCanvas.rotate(270.0F);
        paramCanvas.translate((-getHeight() + j), 0.0F);
        edgeEffect = this.mLeftGlow;
        if (edgeEffect != null && edgeEffect.draw(paramCanvas)) {
          j = 1;
        } else {
          j = 0;
        } 
        paramCanvas.restoreToCount(i);
        i = j;
      } 
    } 
    edgeEffect = this.mTopGlow;
    j = i;
    if (edgeEffect != null) {
      j = i;
      if (!edgeEffect.isFinished()) {
        int k = paramCanvas.save();
        if (this.mClipToPadding)
          paramCanvas.translate(getPaddingLeft(), getPaddingTop()); 
        edgeEffect = this.mTopGlow;
        if (edgeEffect != null && edgeEffect.draw(paramCanvas)) {
          j = 1;
        } else {
          j = 0;
        } 
        j = i | j;
        paramCanvas.restoreToCount(k);
      } 
    } 
    edgeEffect = this.mRightGlow;
    i = j;
    if (edgeEffect != null) {
      i = j;
      if (!edgeEffect.isFinished()) {
        int k = paramCanvas.save();
        int m = getWidth();
        if (this.mClipToPadding) {
          i = getPaddingTop();
        } else {
          i = 0;
        } 
        paramCanvas.rotate(90.0F);
        paramCanvas.translate(-i, -m);
        edgeEffect = this.mRightGlow;
        if (edgeEffect != null && edgeEffect.draw(paramCanvas)) {
          i = 1;
        } else {
          i = 0;
        } 
        i = j | i;
        paramCanvas.restoreToCount(k);
      } 
    } 
    edgeEffect = this.mBottomGlow;
    j = i;
    if (edgeEffect != null) {
      j = i;
      if (!edgeEffect.isFinished()) {
        int k = paramCanvas.save();
        paramCanvas.rotate(180.0F);
        if (this.mClipToPadding) {
          paramCanvas.translate((-getWidth() + getPaddingRight()), (-getHeight() + getPaddingBottom()));
        } else {
          paramCanvas.translate(-getWidth(), -getHeight());
        } 
        edgeEffect = this.mBottomGlow;
        if (edgeEffect != null && edgeEffect.draw(paramCanvas)) {
          j = bool;
        } else {
          j = 0;
        } 
        j = i | j;
        paramCanvas.restoreToCount(k);
      } 
    } 
    i = j;
    if (j == 0) {
      i = j;
      if (this.mItemAnimator != null) {
        i = j;
        if (this.mItemDecorations.size() > 0) {
          ItemAnimator itemAnimator = this.mItemAnimator;
          i = j;
          if (itemAnimator.isRunning())
            i = 1; 
        } 
      } 
    } 
    if (i != 0)
      postInvalidateOnAnimation(); 
  }
  
  public void onDraw(Canvas paramCanvas) {
    super.onDraw(paramCanvas);
    int i = this.mItemDecorations.size();
    for (byte b = 0; b < i; b++)
      ((ItemDecoration)this.mItemDecorations.get(b)).onDraw(paramCanvas, this, this.mState); 
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    boolean bool;
    if (paramLayoutParams instanceof LayoutParams && this.mLayout.checkLayoutParams((LayoutParams)paramLayoutParams)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
    LayoutManager layoutManager = this.mLayout;
    if (layoutManager != null)
      return (ViewGroup.LayoutParams)layoutManager.generateDefaultLayoutParams(); 
    throw new IllegalStateException("RecyclerView has no LayoutManager");
  }
  
  public ViewGroup.LayoutParams generateLayoutParams(AttributeSet paramAttributeSet) {
    LayoutManager layoutManager = this.mLayout;
    if (layoutManager != null)
      return (ViewGroup.LayoutParams)layoutManager.generateLayoutParams(getContext(), paramAttributeSet); 
    throw new IllegalStateException("RecyclerView has no LayoutManager");
  }
  
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams) {
    LayoutManager layoutManager = this.mLayout;
    if (layoutManager != null)
      return (ViewGroup.LayoutParams)layoutManager.generateLayoutParams(paramLayoutParams); 
    throw new IllegalStateException("RecyclerView has no LayoutManager");
  }
  
  public boolean isAnimating() {
    boolean bool;
    ItemAnimator itemAnimator = this.mItemAnimator;
    if (itemAnimator != null && itemAnimator.isRunning()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  void saveOldPositions() {
    int i = this.mChildHelper.getUnfilteredChildCount();
    for (byte b = 0; b < i; b++) {
      ViewHolder viewHolder = getChildViewHolderInt(this.mChildHelper.getUnfilteredChildAt(b));
      if (!viewHolder.shouldIgnore())
        viewHolder.saveOldPosition(); 
    } 
  }
  
  void clearOldPositions() {
    int i = this.mChildHelper.getUnfilteredChildCount();
    for (byte b = 0; b < i; b++) {
      ViewHolder viewHolder = getChildViewHolderInt(this.mChildHelper.getUnfilteredChildAt(b));
      if (!viewHolder.shouldIgnore())
        viewHolder.clearOldPosition(); 
    } 
    this.mRecycler.clearOldPositions();
  }
  
  void offsetPositionRecordsForMove(int paramInt1, int paramInt2) {
    int j, k;
    boolean bool;
    int i = this.mChildHelper.getUnfilteredChildCount();
    if (paramInt1 < paramInt2) {
      j = paramInt1;
      k = paramInt2;
      bool = true;
    } else {
      j = paramInt2;
      k = paramInt1;
      bool = true;
    } 
    for (byte b = 0; b < i; b++) {
      ViewHolder viewHolder = getChildViewHolderInt(this.mChildHelper.getUnfilteredChildAt(b));
      if (viewHolder != null && viewHolder.mPosition >= j && viewHolder.mPosition <= k) {
        if (viewHolder.mPosition == paramInt1) {
          viewHolder.offsetPosition(paramInt2 - paramInt1, false);
        } else {
          viewHolder.offsetPosition(bool, false);
        } 
        this.mState.mStructureChanged = true;
      } 
    } 
    this.mRecycler.offsetPositionRecordsForMove(paramInt1, paramInt2);
    requestLayout();
  }
  
  void offsetPositionRecordsForInsert(int paramInt1, int paramInt2) {
    int i = this.mChildHelper.getUnfilteredChildCount();
    for (byte b = 0; b < i; b++) {
      ViewHolder viewHolder = getChildViewHolderInt(this.mChildHelper.getUnfilteredChildAt(b));
      if (viewHolder != null && !viewHolder.shouldIgnore() && viewHolder.mPosition >= paramInt1) {
        viewHolder.offsetPosition(paramInt2, false);
        this.mState.mStructureChanged = true;
      } 
    } 
    this.mRecycler.offsetPositionRecordsForInsert(paramInt1, paramInt2);
    requestLayout();
  }
  
  void offsetPositionRecordsForRemove(int paramInt1, int paramInt2, boolean paramBoolean) {
    int i = this.mChildHelper.getUnfilteredChildCount();
    for (byte b = 0; b < i; b++) {
      ViewHolder viewHolder = getChildViewHolderInt(this.mChildHelper.getUnfilteredChildAt(b));
      if (viewHolder != null && !viewHolder.shouldIgnore())
        if (viewHolder.mPosition >= paramInt1 + paramInt2) {
          viewHolder.offsetPosition(-paramInt2, paramBoolean);
          this.mState.mStructureChanged = true;
        } else if (viewHolder.mPosition >= paramInt1) {
          viewHolder.flagRemovedAndOffsetPosition(paramInt1 - 1, -paramInt2, paramBoolean);
          this.mState.mStructureChanged = true;
        }  
    } 
    this.mRecycler.offsetPositionRecordsForRemove(paramInt1, paramInt2, paramBoolean);
    requestLayout();
  }
  
  void viewRangeUpdate(int paramInt1, int paramInt2, Object paramObject) {
    int i = this.mChildHelper.getUnfilteredChildCount();
    for (byte b = 0; b < i; b++) {
      View view = this.mChildHelper.getUnfilteredChildAt(b);
      ViewHolder viewHolder = getChildViewHolderInt(view);
      if (viewHolder != null && !viewHolder.shouldIgnore())
        if (viewHolder.mPosition >= paramInt1 && viewHolder.mPosition < paramInt1 + paramInt2) {
          viewHolder.addFlags(2);
          viewHolder.addChangePayload(paramObject);
          ((LayoutParams)view.getLayoutParams()).mInsetsDirty = true;
        }  
    } 
    this.mRecycler.viewRangeUpdate(paramInt1, paramInt2);
  }
  
  boolean canReuseUpdatedViewHolder(ViewHolder paramViewHolder) {
    ItemAnimator itemAnimator = this.mItemAnimator;
    if (itemAnimator != null) {
      List<Object> list = paramViewHolder.getUnmodifiedPayloads();
      return itemAnimator.canReuseUpdatedViewHolder(paramViewHolder, list);
    } 
    return true;
  }
  
  void setDataSetChangedAfterLayout() {
    if (this.mDataSetHasChangedAfterLayout)
      return; 
    this.mDataSetHasChangedAfterLayout = true;
    int i = this.mChildHelper.getUnfilteredChildCount();
    for (byte b = 0; b < i; b++) {
      ViewHolder viewHolder = getChildViewHolderInt(this.mChildHelper.getUnfilteredChildAt(b));
      if (viewHolder != null && !viewHolder.shouldIgnore())
        viewHolder.addFlags(512); 
    } 
    this.mRecycler.setAdapterPositionsAsUnknown();
    markKnownViewsInvalid();
  }
  
  void markKnownViewsInvalid() {
    int i = this.mChildHelper.getUnfilteredChildCount();
    for (byte b = 0; b < i; b++) {
      ViewHolder viewHolder = getChildViewHolderInt(this.mChildHelper.getUnfilteredChildAt(b));
      if (viewHolder != null && !viewHolder.shouldIgnore())
        viewHolder.addFlags(6); 
    } 
    markItemDecorInsetsDirty();
    this.mRecycler.markKnownViewsInvalid();
  }
  
  public void invalidateItemDecorations() {
    if (this.mItemDecorations.size() == 0)
      return; 
    LayoutManager layoutManager = this.mLayout;
    if (layoutManager != null)
      layoutManager.assertNotInLayoutOrScroll("Cannot invalidate item decorations during a scroll or layout"); 
    markItemDecorInsetsDirty();
    requestLayout();
  }
  
  public boolean getPreserveFocusAfterLayout() {
    return this.mPreserveFocusAfterLayout;
  }
  
  public void setPreserveFocusAfterLayout(boolean paramBoolean) {
    this.mPreserveFocusAfterLayout = paramBoolean;
  }
  
  public ViewHolder getChildViewHolder(View paramView) {
    ViewParent viewParent = paramView.getParent();
    if (viewParent == null || viewParent == this)
      return getChildViewHolderInt(paramView); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("View ");
    stringBuilder.append(paramView);
    stringBuilder.append(" is not a direct child of ");
    stringBuilder.append(this);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public View findContainingItemView(View paramView) {
    ViewParent viewParent = paramView.getParent();
    while (viewParent != null && viewParent != this && viewParent instanceof View) {
      paramView = (View)viewParent;
      viewParent = paramView.getParent();
    } 
    if (viewParent != this)
      paramView = null; 
    return paramView;
  }
  
  public ViewHolder findContainingViewHolder(View paramView) {
    ViewHolder viewHolder;
    paramView = findContainingItemView(paramView);
    if (paramView == null) {
      paramView = null;
    } else {
      viewHolder = getChildViewHolder(paramView);
    } 
    return viewHolder;
  }
  
  static ViewHolder getChildViewHolderInt(View paramView) {
    if (paramView == null)
      return null; 
    return ((LayoutParams)paramView.getLayoutParams()).mViewHolder;
  }
  
  @Deprecated
  public int getChildPosition(View paramView) {
    return getChildAdapterPosition(paramView);
  }
  
  public int getChildAdapterPosition(View paramView) {
    byte b;
    ViewHolder viewHolder = getChildViewHolderInt(paramView);
    if (viewHolder != null) {
      b = viewHolder.getAdapterPosition();
    } else {
      b = -1;
    } 
    return b;
  }
  
  public int getChildLayoutPosition(View paramView) {
    byte b;
    ViewHolder viewHolder = getChildViewHolderInt(paramView);
    if (viewHolder != null) {
      b = viewHolder.getLayoutPosition();
    } else {
      b = -1;
    } 
    return b;
  }
  
  public long getChildItemId(View paramView) {
    Adapter adapter = this.mAdapter;
    long l = -1L;
    if (adapter == null || !adapter.hasStableIds())
      return -1L; 
    ViewHolder viewHolder = getChildViewHolderInt(paramView);
    if (viewHolder != null)
      l = viewHolder.getItemId(); 
    return l;
  }
  
  @Deprecated
  public ViewHolder findViewHolderForPosition(int paramInt) {
    return findViewHolderForPosition(paramInt, false);
  }
  
  public ViewHolder findViewHolderForLayoutPosition(int paramInt) {
    return findViewHolderForPosition(paramInt, false);
  }
  
  public ViewHolder findViewHolderForAdapterPosition(int paramInt) {
    if (this.mDataSetHasChangedAfterLayout)
      return null; 
    int i = this.mChildHelper.getUnfilteredChildCount();
    ViewHolder viewHolder = null;
    for (byte b = 0; b < i; b++, viewHolder = viewHolder2) {
      ViewHolder viewHolder1 = getChildViewHolderInt(this.mChildHelper.getUnfilteredChildAt(b));
      ViewHolder viewHolder2 = viewHolder;
      if (viewHolder1 != null) {
        viewHolder2 = viewHolder;
        if (!viewHolder1.isRemoved()) {
          viewHolder2 = viewHolder;
          if (getAdapterPositionFor(viewHolder1) == paramInt)
            if (this.mChildHelper.isHidden(viewHolder1.itemView)) {
              viewHolder2 = viewHolder1;
            } else {
              return viewHolder1;
            }  
        } 
      } 
    } 
    return viewHolder;
  }
  
  ViewHolder findViewHolderForPosition(int paramInt, boolean paramBoolean) {
    int i = this.mChildHelper.getUnfilteredChildCount();
    Object object = null;
    for (byte b = 0; b < i; b++, object = SYNTHETIC_LOCAL_VARIABLE_7) {
      ViewHolder viewHolder = getChildViewHolderInt(this.mChildHelper.getUnfilteredChildAt(b));
      Object object1 = object;
      if (viewHolder != null) {
        object1 = object;
        if (!viewHolder.isRemoved()) {
          if (paramBoolean) {
            if (viewHolder.mPosition != paramInt) {
              object1 = object;
              continue;
            } 
          } else if (viewHolder.getLayoutPosition() != paramInt) {
            object1 = object;
            continue;
          } 
          if (this.mChildHelper.isHidden(viewHolder.itemView)) {
            object1 = viewHolder;
          } else {
            return viewHolder;
          } 
        } 
      } 
      continue;
    } 
    return (ViewHolder)object;
  }
  
  public ViewHolder findViewHolderForItemId(long paramLong) {
    Adapter adapter1 = this.mAdapter;
    if (adapter1 == null || !adapter1.hasStableIds())
      return null; 
    int i = this.mChildHelper.getUnfilteredChildCount();
    Adapter adapter2 = null;
    ViewHolder viewHolder;
    for (byte b = 0; b < i; b++, viewHolder = viewHolder1) {
      ViewHolder viewHolder1, viewHolder2 = getChildViewHolderInt(this.mChildHelper.getUnfilteredChildAt(b));
      adapter1 = adapter2;
      if (viewHolder2 != null) {
        adapter1 = adapter2;
        if (!viewHolder2.isRemoved()) {
          adapter1 = adapter2;
          if (viewHolder2.getItemId() == paramLong)
            if (this.mChildHelper.isHidden(viewHolder2.itemView)) {
              viewHolder1 = viewHolder2;
            } else {
              return viewHolder2;
            }  
        } 
      } 
    } 
    return viewHolder;
  }
  
  public View findChildViewUnder(float paramFloat1, float paramFloat2) {
    int i = this.mChildHelper.getChildCount();
    for (; --i >= 0; i--) {
      View view = this.mChildHelper.getChildAt(i);
      float f1 = view.getTranslationX();
      float f2 = view.getTranslationY();
      if (paramFloat1 >= view.getLeft() + f1 && 
        paramFloat1 <= view.getRight() + f1 && 
        paramFloat2 >= view.getTop() + f2 && 
        paramFloat2 <= view.getBottom() + f2)
        return view; 
    } 
    return null;
  }
  
  public boolean drawChild(Canvas paramCanvas, View paramView, long paramLong) {
    return super.drawChild(paramCanvas, paramView, paramLong);
  }
  
  public void offsetChildrenVertical(int paramInt) {
    int i = this.mChildHelper.getChildCount();
    for (byte b = 0; b < i; b++)
      this.mChildHelper.getChildAt(b).offsetTopAndBottom(paramInt); 
  }
  
  public void onChildAttachedToWindow(View paramView) {}
  
  public void onChildDetachedFromWindow(View paramView) {}
  
  public void offsetChildrenHorizontal(int paramInt) {
    int i = this.mChildHelper.getChildCount();
    for (byte b = 0; b < i; b++)
      this.mChildHelper.getChildAt(b).offsetLeftAndRight(paramInt); 
  }
  
  public void getDecoratedBoundsWithMargins(View paramView, Rect paramRect) {
    getDecoratedBoundsWithMarginsInt(paramView, paramRect);
  }
  
  static void getDecoratedBoundsWithMarginsInt(View paramView, Rect paramRect) {
    LayoutParams layoutParams = (LayoutParams)paramView.getLayoutParams();
    Rect rect = layoutParams.mDecorInsets;
    int i = paramView.getLeft(), j = rect.left, k = layoutParams.leftMargin;
    int m = paramView.getTop(), n = rect.top, i1 = layoutParams.topMargin;
    int i2 = paramView.getRight(), i3 = rect.right, i4 = layoutParams.rightMargin;
    int i5 = paramView.getBottom(), i6 = rect.bottom, i7 = layoutParams.bottomMargin;
    paramRect.set(i - j - k, m - n - i1, i2 + i3 + i4, i5 + i6 + i7);
  }
  
  Rect getItemDecorInsetsForChild(View paramView) {
    LayoutParams layoutParams = (LayoutParams)paramView.getLayoutParams();
    if (!layoutParams.mInsetsDirty)
      return layoutParams.mDecorInsets; 
    if (this.mState.isPreLayout() && (layoutParams.isItemChanged() || layoutParams.isViewInvalid()))
      return layoutParams.mDecorInsets; 
    Rect rect = layoutParams.mDecorInsets;
    rect.set(0, 0, 0, 0);
    int i = this.mItemDecorations.size();
    for (byte b = 0; b < i; b++) {
      this.mTempRect.set(0, 0, 0, 0);
      ((ItemDecoration)this.mItemDecorations.get(b)).getItemOffsets(this.mTempRect, paramView, this, this.mState);
      rect.left += this.mTempRect.left;
      rect.top += this.mTempRect.top;
      rect.right += this.mTempRect.right;
      rect.bottom += this.mTempRect.bottom;
    } 
    layoutParams.mInsetsDirty = false;
    return rect;
  }
  
  public void onScrolled(int paramInt1, int paramInt2) {}
  
  void dispatchOnScrolled(int paramInt1, int paramInt2) {
    this.mDispatchScrollCounter++;
    int i = getScrollX();
    int j = getScrollY();
    onScrollChanged(i, j, i, j);
    onScrolled(paramInt1, paramInt2);
    OnScrollListener onScrollListener = this.mScrollListener;
    if (onScrollListener != null)
      onScrollListener.onScrolled(this, paramInt1, paramInt2); 
    List<OnScrollListener> list = this.mScrollListeners;
    if (list != null)
      for (i = list.size() - 1; i >= 0; i--)
        ((OnScrollListener)this.mScrollListeners.get(i)).onScrolled(this, paramInt1, paramInt2);  
    this.mDispatchScrollCounter--;
  }
  
  public void onScrollStateChanged(int paramInt) {}
  
  void dispatchOnScrollStateChanged(int paramInt) {
    LayoutManager layoutManager = this.mLayout;
    if (layoutManager != null)
      layoutManager.onScrollStateChanged(paramInt); 
    onScrollStateChanged(paramInt);
    OnScrollListener onScrollListener = this.mScrollListener;
    if (onScrollListener != null)
      onScrollListener.onScrollStateChanged(this, paramInt); 
    List<OnScrollListener> list = this.mScrollListeners;
    if (list != null)
      for (int i = list.size() - 1; i >= 0; i--)
        ((OnScrollListener)this.mScrollListeners.get(i)).onScrollStateChanged(this, paramInt);  
  }
  
  public boolean hasPendingAdapterUpdates() {
    if (this.mFirstLayoutComplete && !this.mDataSetHasChangedAfterLayout) {
      AdapterHelper adapterHelper = this.mAdapterHelper;
      return 
        adapterHelper.hasPendingUpdates();
    } 
    return true;
  }
  
  class ViewFlinger implements Runnable {
    Interpolator mInterpolator = RecyclerView.sQuinticInterpolator;
    
    private boolean mEatRunOnAnimationRequest = false;
    
    private boolean mReSchedulePostAnimationCallback = false;
    
    private int mLastFlingX;
    
    private int mLastFlingY;
    
    private OverScroller mScroller;
    
    final RecyclerView this$0;
    
    ViewFlinger() {
      this.mScroller = new OverScroller(RecyclerView.this.getContext(), RecyclerView.sQuinticInterpolator);
    }
    
    public void run() {
      // Byte code:
      //   0: aload_0
      //   1: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   4: getfield mLayout : Lcom/android/internal/widget/RecyclerView$LayoutManager;
      //   7: ifnonnull -> 15
      //   10: aload_0
      //   11: invokevirtual stop : ()V
      //   14: return
      //   15: aload_0
      //   16: invokespecial disableRunOnAnimationRequests : ()V
      //   19: aload_0
      //   20: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   23: invokevirtual consumePendingUpdateOperations : ()V
      //   26: aload_0
      //   27: getfield mScroller : Landroid/widget/OverScroller;
      //   30: astore_1
      //   31: aload_0
      //   32: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   35: getfield mLayout : Lcom/android/internal/widget/RecyclerView$LayoutManager;
      //   38: getfield mSmoothScroller : Lcom/android/internal/widget/RecyclerView$SmoothScroller;
      //   41: astore_2
      //   42: aload_1
      //   43: invokevirtual computeScrollOffset : ()Z
      //   46: ifeq -> 854
      //   49: aload_1
      //   50: invokevirtual getCurrX : ()I
      //   53: istore_3
      //   54: aload_1
      //   55: invokevirtual getCurrY : ()I
      //   58: istore #4
      //   60: iload_3
      //   61: aload_0
      //   62: getfield mLastFlingX : I
      //   65: isub
      //   66: istore #5
      //   68: iload #4
      //   70: aload_0
      //   71: getfield mLastFlingY : I
      //   74: isub
      //   75: istore #6
      //   77: iconst_0
      //   78: istore #7
      //   80: iconst_0
      //   81: istore #8
      //   83: iconst_0
      //   84: istore #9
      //   86: iconst_0
      //   87: istore #10
      //   89: aload_0
      //   90: iload_3
      //   91: putfield mLastFlingX : I
      //   94: aload_0
      //   95: iload #4
      //   97: putfield mLastFlingY : I
      //   100: iconst_0
      //   101: istore #11
      //   103: iconst_0
      //   104: istore #12
      //   106: iconst_0
      //   107: istore #13
      //   109: iconst_0
      //   110: istore #14
      //   112: aload_0
      //   113: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   116: getfield mAdapter : Lcom/android/internal/widget/RecyclerView$Adapter;
      //   119: ifnull -> 432
      //   122: aload_0
      //   123: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   126: invokevirtual eatRequestLayout : ()V
      //   129: aload_0
      //   130: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   133: invokevirtual onEnterLayoutOrScroll : ()V
      //   136: ldc 'RV Scroll'
      //   138: invokestatic beginSection : (Ljava/lang/String;)V
      //   141: iload #5
      //   143: ifeq -> 181
      //   146: aload_0
      //   147: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   150: getfield mLayout : Lcom/android/internal/widget/RecyclerView$LayoutManager;
      //   153: iload #5
      //   155: aload_0
      //   156: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   159: getfield mRecycler : Lcom/android/internal/widget/RecyclerView$Recycler;
      //   162: aload_0
      //   163: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   166: getfield mState : Lcom/android/internal/widget/RecyclerView$State;
      //   169: invokevirtual scrollHorizontallyBy : (ILcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;)I
      //   172: istore #8
      //   174: iload #5
      //   176: iload #8
      //   178: isub
      //   179: istore #12
      //   181: iload #6
      //   183: ifeq -> 221
      //   186: aload_0
      //   187: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   190: getfield mLayout : Lcom/android/internal/widget/RecyclerView$LayoutManager;
      //   193: iload #6
      //   195: aload_0
      //   196: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   199: getfield mRecycler : Lcom/android/internal/widget/RecyclerView$Recycler;
      //   202: aload_0
      //   203: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   206: getfield mState : Lcom/android/internal/widget/RecyclerView$State;
      //   209: invokevirtual scrollVerticallyBy : (ILcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;)I
      //   212: istore #10
      //   214: iload #6
      //   216: iload #10
      //   218: isub
      //   219: istore #14
      //   221: invokestatic endSection : ()V
      //   224: aload_0
      //   225: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   228: invokevirtual repositionShadowingViews : ()V
      //   231: aload_0
      //   232: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   235: invokevirtual onExitLayoutOrScroll : ()V
      //   238: aload_0
      //   239: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   242: iconst_0
      //   243: invokevirtual resumeRequestLayout : (Z)V
      //   246: iload #8
      //   248: istore #7
      //   250: iload #10
      //   252: istore #9
      //   254: iload #12
      //   256: istore #11
      //   258: iload #14
      //   260: istore #13
      //   262: aload_2
      //   263: ifnull -> 432
      //   266: iload #8
      //   268: istore #7
      //   270: iload #10
      //   272: istore #9
      //   274: iload #12
      //   276: istore #11
      //   278: iload #14
      //   280: istore #13
      //   282: aload_2
      //   283: invokevirtual isPendingInitialRun : ()Z
      //   286: ifne -> 432
      //   289: iload #8
      //   291: istore #7
      //   293: iload #10
      //   295: istore #9
      //   297: iload #12
      //   299: istore #11
      //   301: iload #14
      //   303: istore #13
      //   305: aload_2
      //   306: invokevirtual isRunning : ()Z
      //   309: ifeq -> 432
      //   312: aload_0
      //   313: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   316: getfield mState : Lcom/android/internal/widget/RecyclerView$State;
      //   319: invokevirtual getItemCount : ()I
      //   322: istore #7
      //   324: iload #7
      //   326: ifne -> 352
      //   329: aload_2
      //   330: invokevirtual stop : ()V
      //   333: iload #8
      //   335: istore #7
      //   337: iload #10
      //   339: istore #9
      //   341: iload #12
      //   343: istore #11
      //   345: iload #14
      //   347: istore #13
      //   349: goto -> 432
      //   352: aload_2
      //   353: invokevirtual getTargetPosition : ()I
      //   356: iload #7
      //   358: if_icmplt -> 402
      //   361: aload_2
      //   362: iload #7
      //   364: iconst_1
      //   365: isub
      //   366: invokevirtual setTargetPosition : (I)V
      //   369: aload_2
      //   370: iload #5
      //   372: iload #12
      //   374: isub
      //   375: iload #6
      //   377: iload #14
      //   379: isub
      //   380: invokestatic access$400 : (Lcom/android/internal/widget/RecyclerView$SmoothScroller;II)V
      //   383: iload #8
      //   385: istore #7
      //   387: iload #10
      //   389: istore #9
      //   391: iload #12
      //   393: istore #11
      //   395: iload #14
      //   397: istore #13
      //   399: goto -> 432
      //   402: aload_2
      //   403: iload #5
      //   405: iload #12
      //   407: isub
      //   408: iload #6
      //   410: iload #14
      //   412: isub
      //   413: invokestatic access$400 : (Lcom/android/internal/widget/RecyclerView$SmoothScroller;II)V
      //   416: iload #14
      //   418: istore #13
      //   420: iload #12
      //   422: istore #11
      //   424: iload #10
      //   426: istore #9
      //   428: iload #8
      //   430: istore #7
      //   432: aload_0
      //   433: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   436: getfield mItemDecorations : Ljava/util/ArrayList;
      //   439: invokevirtual isEmpty : ()Z
      //   442: ifne -> 452
      //   445: aload_0
      //   446: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   449: invokevirtual invalidate : ()V
      //   452: aload_0
      //   453: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   456: invokevirtual getOverScrollMode : ()I
      //   459: iconst_2
      //   460: if_icmpeq -> 474
      //   463: aload_0
      //   464: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   467: iload #5
      //   469: iload #6
      //   471: invokevirtual considerReleasingGlowsOnScroll : (II)V
      //   474: iload #11
      //   476: ifne -> 484
      //   479: iload #13
      //   481: ifeq -> 633
      //   484: aload_1
      //   485: invokevirtual getCurrVelocity : ()F
      //   488: f2i
      //   489: istore #12
      //   491: iconst_0
      //   492: istore #14
      //   494: iload #11
      //   496: iload_3
      //   497: if_icmpeq -> 532
      //   500: iload #11
      //   502: ifge -> 513
      //   505: iload #12
      //   507: ineg
      //   508: istore #8
      //   510: goto -> 528
      //   513: iload #11
      //   515: ifle -> 525
      //   518: iload #12
      //   520: istore #8
      //   522: goto -> 528
      //   525: iconst_0
      //   526: istore #8
      //   528: iload #8
      //   530: istore #14
      //   532: iconst_0
      //   533: istore #8
      //   535: iload #13
      //   537: iload #4
      //   539: if_icmpeq -> 570
      //   542: iload #13
      //   544: ifge -> 555
      //   547: iload #12
      //   549: ineg
      //   550: istore #8
      //   552: goto -> 570
      //   555: iload #13
      //   557: ifle -> 567
      //   560: iload #12
      //   562: istore #8
      //   564: goto -> 570
      //   567: iconst_0
      //   568: istore #8
      //   570: aload_0
      //   571: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   574: invokevirtual getOverScrollMode : ()I
      //   577: iconst_2
      //   578: if_icmpeq -> 592
      //   581: aload_0
      //   582: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   585: iload #14
      //   587: iload #8
      //   589: invokevirtual absorbGlows : (II)V
      //   592: iload #14
      //   594: ifne -> 610
      //   597: iload #11
      //   599: iload_3
      //   600: if_icmpeq -> 610
      //   603: aload_1
      //   604: invokevirtual getFinalX : ()I
      //   607: ifne -> 633
      //   610: iload #8
      //   612: ifne -> 629
      //   615: iload #13
      //   617: iload #4
      //   619: if_icmpeq -> 629
      //   622: aload_1
      //   623: invokevirtual getFinalY : ()I
      //   626: ifne -> 633
      //   629: aload_1
      //   630: invokevirtual abortAnimation : ()V
      //   633: iload #7
      //   635: ifne -> 643
      //   638: iload #9
      //   640: ifeq -> 654
      //   643: aload_0
      //   644: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   647: iload #7
      //   649: iload #9
      //   651: invokevirtual dispatchOnScrolled : (II)V
      //   654: aload_0
      //   655: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   658: invokestatic access$500 : (Lcom/android/internal/widget/RecyclerView;)Z
      //   661: ifne -> 671
      //   664: aload_0
      //   665: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   668: invokevirtual invalidate : ()V
      //   671: iconst_1
      //   672: istore #10
      //   674: iload #6
      //   676: ifeq -> 705
      //   679: aload_0
      //   680: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   683: getfield mLayout : Lcom/android/internal/widget/RecyclerView$LayoutManager;
      //   686: invokevirtual canScrollVertically : ()Z
      //   689: ifeq -> 705
      //   692: iload #9
      //   694: iload #6
      //   696: if_icmpne -> 705
      //   699: iconst_1
      //   700: istore #8
      //   702: goto -> 708
      //   705: iconst_0
      //   706: istore #8
      //   708: iload #5
      //   710: ifeq -> 739
      //   713: aload_0
      //   714: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   717: getfield mLayout : Lcom/android/internal/widget/RecyclerView$LayoutManager;
      //   720: invokevirtual canScrollHorizontally : ()Z
      //   723: ifeq -> 739
      //   726: iload #7
      //   728: iload #5
      //   730: if_icmpne -> 739
      //   733: iconst_1
      //   734: istore #12
      //   736: goto -> 742
      //   739: iconst_0
      //   740: istore #12
      //   742: iload #5
      //   744: ifne -> 756
      //   747: iload #10
      //   749: istore #14
      //   751: iload #6
      //   753: ifeq -> 780
      //   756: iload #10
      //   758: istore #14
      //   760: iload #12
      //   762: ifne -> 780
      //   765: iload #8
      //   767: ifeq -> 777
      //   770: iload #10
      //   772: istore #14
      //   774: goto -> 780
      //   777: iconst_0
      //   778: istore #14
      //   780: aload_1
      //   781: invokevirtual isFinished : ()Z
      //   784: ifne -> 830
      //   787: iload #14
      //   789: ifne -> 795
      //   792: goto -> 830
      //   795: aload_0
      //   796: invokevirtual postOnAnimation : ()V
      //   799: aload_0
      //   800: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   803: getfield mGapWorker : Lcom/android/internal/widget/GapWorker;
      //   806: ifnull -> 854
      //   809: aload_0
      //   810: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   813: getfield mGapWorker : Lcom/android/internal/widget/GapWorker;
      //   816: aload_0
      //   817: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   820: iload #5
      //   822: iload #6
      //   824: invokevirtual postFromTraversal : (Lcom/android/internal/widget/RecyclerView;II)V
      //   827: goto -> 854
      //   830: aload_0
      //   831: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   834: iconst_0
      //   835: invokevirtual setScrollState : (I)V
      //   838: invokestatic access$600 : ()Z
      //   841: ifeq -> 854
      //   844: aload_0
      //   845: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   848: getfield mPrefetchRegistry : Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;
      //   851: invokevirtual clearPrefetchPositions : ()V
      //   854: aload_2
      //   855: ifnull -> 882
      //   858: aload_2
      //   859: invokevirtual isPendingInitialRun : ()Z
      //   862: ifeq -> 871
      //   865: aload_2
      //   866: iconst_0
      //   867: iconst_0
      //   868: invokestatic access$400 : (Lcom/android/internal/widget/RecyclerView$SmoothScroller;II)V
      //   871: aload_0
      //   872: getfield mReSchedulePostAnimationCallback : Z
      //   875: ifne -> 882
      //   878: aload_2
      //   879: invokevirtual stop : ()V
      //   882: aload_0
      //   883: invokespecial enableRunOnAnimationRequests : ()V
      //   886: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #4659	-> 0
      //   #4660	-> 10
      //   #4661	-> 14
      //   #4663	-> 15
      //   #4664	-> 19
      //   #4667	-> 26
      //   #4668	-> 31
      //   #4669	-> 42
      //   #4670	-> 49
      //   #4671	-> 54
      //   #4672	-> 60
      //   #4673	-> 68
      //   #4674	-> 77
      //   #4675	-> 83
      //   #4676	-> 89
      //   #4677	-> 94
      //   #4678	-> 100
      //   #4679	-> 112
      //   #4680	-> 122
      //   #4681	-> 129
      //   #4682	-> 136
      //   #4683	-> 141
      //   #4684	-> 146
      //   #4685	-> 174
      //   #4687	-> 181
      //   #4688	-> 186
      //   #4689	-> 214
      //   #4691	-> 221
      //   #4692	-> 224
      //   #4694	-> 231
      //   #4695	-> 238
      //   #4697	-> 246
      //   #4698	-> 289
      //   #4699	-> 312
      //   #4700	-> 324
      //   #4701	-> 329
      //   #4702	-> 352
      //   #4703	-> 361
      //   #4704	-> 369
      //   #4706	-> 402
      //   #4710	-> 432
      //   #4711	-> 445
      //   #4713	-> 452
      //   #4714	-> 463
      //   #4716	-> 474
      //   #4717	-> 484
      //   #4719	-> 491
      //   #4720	-> 494
      //   #4721	-> 500
      //   #4724	-> 532
      //   #4725	-> 535
      //   #4726	-> 542
      //   #4729	-> 570
      //   #4730	-> 581
      //   #4732	-> 592
      //   #4733	-> 622
      //   #4734	-> 629
      //   #4737	-> 633
      //   #4738	-> 643
      //   #4741	-> 654
      //   #4742	-> 664
      //   #4745	-> 671
      //   #4747	-> 708
      //   #4749	-> 742
      //   #4752	-> 780
      //   #4758	-> 795
      //   #4759	-> 799
      //   #4760	-> 809
      //   #4753	-> 830
      //   #4754	-> 838
      //   #4755	-> 844
      //   #4765	-> 854
      //   #4766	-> 858
      //   #4767	-> 865
      //   #4769	-> 871
      //   #4770	-> 878
      //   #4773	-> 882
      //   #4774	-> 886
    }
    
    private void disableRunOnAnimationRequests() {
      this.mReSchedulePostAnimationCallback = false;
      this.mEatRunOnAnimationRequest = true;
    }
    
    private void enableRunOnAnimationRequests() {
      this.mEatRunOnAnimationRequest = false;
      if (this.mReSchedulePostAnimationCallback)
        postOnAnimation(); 
    }
    
    void postOnAnimation() {
      if (this.mEatRunOnAnimationRequest) {
        this.mReSchedulePostAnimationCallback = true;
      } else {
        RecyclerView.this.removeCallbacks(this);
        RecyclerView.this.postOnAnimation(this);
      } 
    }
    
    public void fling(int param1Int1, int param1Int2) {
      RecyclerView.this.setScrollState(2);
      this.mLastFlingY = 0;
      this.mLastFlingX = 0;
      this.mScroller.fling(0, 0, param1Int1, param1Int2, -2147483648, 2147483647, -2147483648, 2147483647);
      postOnAnimation();
    }
    
    public void smoothScrollBy(int param1Int1, int param1Int2) {
      smoothScrollBy(param1Int1, param1Int2, 0, 0);
    }
    
    public void smoothScrollBy(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      smoothScrollBy(param1Int1, param1Int2, computeScrollDuration(param1Int1, param1Int2, param1Int3, param1Int4));
    }
    
    private float distanceInfluenceForSnapDuration(float param1Float) {
      param1Float = (float)((param1Float - 0.5F) * 0.4712389167638204D);
      return (float)Math.sin(param1Float);
    }
    
    private int computeScrollDuration(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      boolean bool;
      int i = Math.abs(param1Int1);
      int j = Math.abs(param1Int2);
      if (i > j) {
        bool = true;
      } else {
        bool = false;
      } 
      param1Int3 = (int)Math.sqrt((param1Int3 * param1Int3 + param1Int4 * param1Int4));
      param1Int2 = (int)Math.sqrt((param1Int1 * param1Int1 + param1Int2 * param1Int2));
      RecyclerView recyclerView = RecyclerView.this;
      if (bool) {
        param1Int1 = recyclerView.getWidth();
      } else {
        param1Int1 = recyclerView.getHeight();
      } 
      param1Int4 = param1Int1 / 2;
      float f1 = Math.min(1.0F, param1Int2 * 1.0F / param1Int1);
      float f2 = param1Int4, f3 = param1Int4;
      f1 = distanceInfluenceForSnapDuration(f1);
      if (param1Int3 > 0) {
        param1Int1 = Math.round(Math.abs((f2 + f3 * f1) / param1Int3) * 1000.0F) * 4;
      } else {
        if (bool) {
          param1Int2 = i;
        } else {
          param1Int2 = j;
        } 
        f2 = param1Int2;
        param1Int1 = (int)((f2 / param1Int1 + 1.0F) * 300.0F);
      } 
      return Math.min(param1Int1, 2000);
    }
    
    public void smoothScrollBy(int param1Int1, int param1Int2, int param1Int3) {
      smoothScrollBy(param1Int1, param1Int2, param1Int3, RecyclerView.sQuinticInterpolator);
    }
    
    public void smoothScrollBy(int param1Int1, int param1Int2, Interpolator param1Interpolator) {
      int i = computeScrollDuration(param1Int1, param1Int2, 0, 0);
      if (param1Interpolator == null)
        param1Interpolator = RecyclerView.sQuinticInterpolator; 
      smoothScrollBy(param1Int1, param1Int2, i, param1Interpolator);
    }
    
    public void smoothScrollBy(int param1Int1, int param1Int2, int param1Int3, Interpolator param1Interpolator) {
      if (this.mInterpolator != param1Interpolator) {
        this.mInterpolator = param1Interpolator;
        this.mScroller = new OverScroller(RecyclerView.this.getContext(), param1Interpolator);
      } 
      RecyclerView.this.setScrollState(2);
      this.mLastFlingY = 0;
      this.mLastFlingX = 0;
      this.mScroller.startScroll(0, 0, param1Int1, param1Int2, param1Int3);
      postOnAnimation();
    }
    
    public void stop() {
      RecyclerView.this.removeCallbacks(this);
      this.mScroller.abortAnimation();
    }
  }
  
  void repositionShadowingViews() {
    int i = this.mChildHelper.getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = this.mChildHelper.getChildAt(b);
      ViewHolder viewHolder = getChildViewHolder(view);
      if (viewHolder != null && viewHolder.mShadowingHolder != null) {
        View view1 = viewHolder.mShadowingHolder.itemView;
        int j = view.getLeft();
        int k = view.getTop();
        if (j != view1.getLeft() || k != view1.getTop()) {
          int m = view1.getWidth();
          int n = view1.getHeight();
          view1.layout(j, k, m + j, n + k);
        } 
      } 
    } 
  }
  
  class RecyclerViewDataObserver extends AdapterDataObserver {
    final RecyclerView this$0;
    
    public void onChanged() {
      RecyclerView.this.assertNotInLayoutOrScroll((String)null);
      RecyclerView.this.mState.mStructureChanged = true;
      RecyclerView.this.setDataSetChangedAfterLayout();
      if (!RecyclerView.this.mAdapterHelper.hasPendingUpdates())
        RecyclerView.this.requestLayout(); 
    }
    
    public void onItemRangeChanged(int param1Int1, int param1Int2, Object param1Object) {
      RecyclerView.this.assertNotInLayoutOrScroll((String)null);
      if (RecyclerView.this.mAdapterHelper.onItemRangeChanged(param1Int1, param1Int2, param1Object))
        triggerUpdateProcessor(); 
    }
    
    public void onItemRangeInserted(int param1Int1, int param1Int2) {
      RecyclerView.this.assertNotInLayoutOrScroll((String)null);
      if (RecyclerView.this.mAdapterHelper.onItemRangeInserted(param1Int1, param1Int2))
        triggerUpdateProcessor(); 
    }
    
    public void onItemRangeRemoved(int param1Int1, int param1Int2) {
      RecyclerView.this.assertNotInLayoutOrScroll((String)null);
      if (RecyclerView.this.mAdapterHelper.onItemRangeRemoved(param1Int1, param1Int2))
        triggerUpdateProcessor(); 
    }
    
    public void onItemRangeMoved(int param1Int1, int param1Int2, int param1Int3) {
      RecyclerView.this.assertNotInLayoutOrScroll((String)null);
      if (RecyclerView.this.mAdapterHelper.onItemRangeMoved(param1Int1, param1Int2, param1Int3))
        triggerUpdateProcessor(); 
    }
    
    void triggerUpdateProcessor() {
      if (RecyclerView.POST_UPDATES_ON_ANIMATION && RecyclerView.this.mHasFixedSize && RecyclerView.this.mIsAttached) {
        RecyclerView recyclerView = RecyclerView.this;
        recyclerView.postOnAnimation(recyclerView.mUpdateChildViewsRunnable);
      } else {
        RecyclerView.this.mAdapterUpdateDuringMeasure = true;
        RecyclerView.this.requestLayout();
      } 
    }
  }
  
  class RecycledViewPool {
    static class ScrapData {
      ArrayList<RecyclerView.ViewHolder> mScrapHeap = new ArrayList<>();
      
      int mMaxScrap = 5;
      
      long mCreateRunningAverageNs = 0L;
      
      long mBindRunningAverageNs = 0L;
    }
    
    SparseArray<ScrapData> mScrap = new SparseArray();
    
    private int mAttachCount = 0;
    
    private static final int DEFAULT_MAX_SCRAP = 5;
    
    public void clear() {
      for (byte b = 0; b < this.mScrap.size(); b++) {
        ScrapData scrapData = (ScrapData)this.mScrap.valueAt(b);
        scrapData.mScrapHeap.clear();
      } 
    }
    
    public void setMaxRecycledViews(int param1Int1, int param1Int2) {
      ScrapData scrapData = getScrapDataForType(param1Int1);
      scrapData.mMaxScrap = param1Int2;
      ArrayList<RecyclerView.ViewHolder> arrayList = scrapData.mScrapHeap;
      if (arrayList != null)
        while (arrayList.size() > param1Int2)
          arrayList.remove(arrayList.size() - 1);  
    }
    
    public int getRecycledViewCount(int param1Int) {
      return (getScrapDataForType(param1Int)).mScrapHeap.size();
    }
    
    public RecyclerView.ViewHolder getRecycledView(int param1Int) {
      ScrapData scrapData = (ScrapData)this.mScrap.get(param1Int);
      if (scrapData != null && !scrapData.mScrapHeap.isEmpty()) {
        ArrayList<RecyclerView.ViewHolder> arrayList = scrapData.mScrapHeap;
        return arrayList.remove(arrayList.size() - 1);
      } 
      return null;
    }
    
    int size() {
      int i = 0;
      for (byte b = 0; b < this.mScrap.size(); b++, i = j) {
        ArrayList<RecyclerView.ViewHolder> arrayList = ((ScrapData)this.mScrap.valueAt(b)).mScrapHeap;
        int j = i;
        if (arrayList != null)
          j = i + arrayList.size(); 
      } 
      return i;
    }
    
    public void putRecycledView(RecyclerView.ViewHolder param1ViewHolder) {
      int i = param1ViewHolder.getItemViewType();
      ArrayList<RecyclerView.ViewHolder> arrayList = (getScrapDataForType(i)).mScrapHeap;
      if (((ScrapData)this.mScrap.get(i)).mMaxScrap <= arrayList.size())
        return; 
      param1ViewHolder.resetInternal();
      arrayList.add(param1ViewHolder);
    }
    
    long runningAverage(long param1Long1, long param1Long2) {
      if (param1Long1 == 0L)
        return param1Long2; 
      return param1Long1 / 4L * 3L + param1Long2 / 4L;
    }
    
    void factorInCreateTime(int param1Int, long param1Long) {
      ScrapData scrapData = getScrapDataForType(param1Int);
      scrapData.mCreateRunningAverageNs = runningAverage(scrapData.mCreateRunningAverageNs, param1Long);
    }
    
    void factorInBindTime(int param1Int, long param1Long) {
      ScrapData scrapData = getScrapDataForType(param1Int);
      scrapData.mBindRunningAverageNs = runningAverage(scrapData.mBindRunningAverageNs, param1Long);
    }
    
    boolean willCreateInTime(int param1Int, long param1Long1, long param1Long2) {
      long l = (getScrapDataForType(param1Int)).mCreateRunningAverageNs;
      return (l == 0L || param1Long1 + l < param1Long2);
    }
    
    boolean willBindInTime(int param1Int, long param1Long1, long param1Long2) {
      long l = (getScrapDataForType(param1Int)).mBindRunningAverageNs;
      return (l == 0L || param1Long1 + l < param1Long2);
    }
    
    void attach(RecyclerView.Adapter param1Adapter) {
      this.mAttachCount++;
    }
    
    void detach() {
      this.mAttachCount--;
    }
    
    void onAdapterChanged(RecyclerView.Adapter param1Adapter1, RecyclerView.Adapter param1Adapter2, boolean param1Boolean) {
      if (param1Adapter1 != null)
        detach(); 
      if (!param1Boolean && this.mAttachCount == 0)
        clear(); 
      if (param1Adapter2 != null)
        attach(param1Adapter2); 
    }
    
    private ScrapData getScrapDataForType(int param1Int) {
      ScrapData scrapData1 = (ScrapData)this.mScrap.get(param1Int);
      ScrapData scrapData2 = scrapData1;
      if (scrapData1 == null) {
        scrapData2 = new ScrapData();
        this.mScrap.put(param1Int, scrapData2);
      } 
      return scrapData2;
    }
  }
  
  static RecyclerView findNestedRecyclerView(View paramView) {
    if (!(paramView instanceof ViewGroup))
      return null; 
    if (paramView instanceof RecyclerView)
      return (RecyclerView)paramView; 
    ViewGroup viewGroup = (ViewGroup)paramView;
    int i = viewGroup.getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = viewGroup.getChildAt(b);
      RecyclerView recyclerView = findNestedRecyclerView(view);
      if (recyclerView != null)
        return recyclerView; 
    } 
    return null;
  }
  
  static void clearNestedRecyclerViewIfNotNested(ViewHolder paramViewHolder) {
    if (paramViewHolder.mNestedRecyclerView != null) {
      View view = (View)paramViewHolder.mNestedRecyclerView.get();
      while (view != null) {
        if (view == paramViewHolder.itemView)
          return; 
        ViewParent viewParent = view.getParent();
        if (viewParent instanceof View) {
          View view1 = (View)viewParent;
          continue;
        } 
        viewParent = null;
      } 
      paramViewHolder.mNestedRecyclerView = null;
    } 
  }
  
  long getNanoTime() {
    if (ALLOW_THREAD_GAP_WORK)
      return System.nanoTime(); 
    return 0L;
  }
  
  class Recycler {
    static final int DEFAULT_CACHE_SIZE = 2;
    
    final ArrayList<RecyclerView.ViewHolder> mAttachedScrap;
    
    final ArrayList<RecyclerView.ViewHolder> mCachedViews;
    
    ArrayList<RecyclerView.ViewHolder> mChangedScrap;
    
    RecyclerView.RecycledViewPool mRecyclerPool;
    
    private int mRequestedCacheMax;
    
    private final List<RecyclerView.ViewHolder> mUnmodifiableAttachedScrap;
    
    private RecyclerView.ViewCacheExtension mViewCacheExtension;
    
    int mViewCacheMax;
    
    final RecyclerView this$0;
    
    public Recycler() {
      this.mAttachedScrap = new ArrayList<>();
      this.mChangedScrap = null;
      this.mCachedViews = new ArrayList<>();
      ArrayList<RecyclerView.ViewHolder> arrayList = this.mAttachedScrap;
      this.mUnmodifiableAttachedScrap = Collections.unmodifiableList(arrayList);
      this.mRequestedCacheMax = 2;
      this.mViewCacheMax = 2;
    }
    
    public void clear() {
      this.mAttachedScrap.clear();
      recycleAndClearCachedViews();
    }
    
    public void setViewCacheSize(int param1Int) {
      this.mRequestedCacheMax = param1Int;
      updateViewCacheSize();
    }
    
    void updateViewCacheSize() {
      if (RecyclerView.this.mLayout != null) {
        i = RecyclerView.this.mLayout.mPrefetchMaxCountObserved;
      } else {
        i = 0;
      } 
      this.mViewCacheMax = this.mRequestedCacheMax + i;
      int i = this.mCachedViews.size() - 1;
      for (; i >= 0 && this.mCachedViews.size() > this.mViewCacheMax; i--)
        recycleCachedViewAt(i); 
    }
    
    public List<RecyclerView.ViewHolder> getScrapList() {
      return this.mUnmodifiableAttachedScrap;
    }
    
    boolean validateViewHolderForOffsetPosition(RecyclerView.ViewHolder param1ViewHolder) {
      if (param1ViewHolder.isRemoved())
        return RecyclerView.this.mState.isPreLayout(); 
      if (param1ViewHolder.mPosition >= 0 && param1ViewHolder.mPosition < RecyclerView.this.mAdapter.getItemCount()) {
        boolean bool = RecyclerView.this.mState.isPreLayout();
        boolean bool1 = false;
        if (!bool) {
          int i = RecyclerView.this.mAdapter.getItemViewType(param1ViewHolder.mPosition);
          if (i != param1ViewHolder.getItemViewType())
            return false; 
        } 
        if (RecyclerView.this.mAdapter.hasStableIds()) {
          if (param1ViewHolder.getItemId() == RecyclerView.this.mAdapter.getItemId(param1ViewHolder.mPosition))
            bool1 = true; 
          return bool1;
        } 
        return true;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Inconsistency detected. Invalid view holder adapter position");
      stringBuilder.append(param1ViewHolder);
      throw new IndexOutOfBoundsException(stringBuilder.toString());
    }
    
    private boolean tryBindViewHolderByDeadline(RecyclerView.ViewHolder param1ViewHolder, int param1Int1, int param1Int2, long param1Long) {
      param1ViewHolder.mOwnerRecyclerView = RecyclerView.this;
      int i = param1ViewHolder.getItemViewType();
      long l = RecyclerView.this.getNanoTime();
      if (param1Long != Long.MAX_VALUE) {
        RecyclerView.RecycledViewPool recycledViewPool = this.mRecyclerPool;
        if (!recycledViewPool.willBindInTime(i, l, param1Long))
          return false; 
      } 
      RecyclerView.this.mAdapter.bindViewHolder(param1ViewHolder, param1Int1);
      param1Long = RecyclerView.this.getNanoTime();
      this.mRecyclerPool.factorInBindTime(param1ViewHolder.getItemViewType(), param1Long - l);
      attachAccessibilityDelegate(param1ViewHolder.itemView);
      if (RecyclerView.this.mState.isPreLayout())
        param1ViewHolder.mPreLayoutPosition = param1Int2; 
      return true;
    }
    
    public void bindViewToPosition(View param1View, int param1Int) {
      RecyclerView.ViewHolder viewHolder = RecyclerView.getChildViewHolderInt(param1View);
      if (viewHolder != null) {
        int i = RecyclerView.this.mAdapterHelper.findPositionOffset(param1Int);
        if (i >= 0 && i < RecyclerView.this.mAdapter.getItemCount()) {
          RecyclerView.LayoutParams layoutParams;
          tryBindViewHolderByDeadline(viewHolder, i, param1Int, Long.MAX_VALUE);
          ViewGroup.LayoutParams layoutParams1 = viewHolder.itemView.getLayoutParams();
          if (layoutParams1 == null) {
            layoutParams = (RecyclerView.LayoutParams)RecyclerView.this.generateDefaultLayoutParams();
            viewHolder.itemView.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
          } else if (!RecyclerView.this.checkLayoutParams((ViewGroup.LayoutParams)layoutParams)) {
            layoutParams = (RecyclerView.LayoutParams)RecyclerView.this.generateLayoutParams((ViewGroup.LayoutParams)layoutParams);
            viewHolder.itemView.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
          } else {
            layoutParams = layoutParams;
          } 
          boolean bool = true;
          layoutParams.mInsetsDirty = true;
          layoutParams.mViewHolder = viewHolder;
          if (viewHolder.itemView.getParent() != null)
            bool = false; 
          layoutParams.mPendingInvalidate = bool;
          return;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Inconsistency detected. Invalid item position ");
        stringBuilder.append(param1Int);
        stringBuilder.append("(offset:");
        stringBuilder.append(i);
        stringBuilder.append(").state:");
        RecyclerView.State state = RecyclerView.this.mState;
        stringBuilder.append(state.getItemCount());
        throw new IndexOutOfBoundsException(stringBuilder.toString());
      } 
      throw new IllegalArgumentException("The view does not have a ViewHolder. You cannot pass arbitrary views to this method, they should be created by the Adapter");
    }
    
    public int convertPreLayoutPositionToPostLayout(int param1Int) {
      if (param1Int >= 0 && param1Int < RecyclerView.this.mState.getItemCount()) {
        if (!RecyclerView.this.mState.isPreLayout())
          return param1Int; 
        return RecyclerView.this.mAdapterHelper.findPositionOffset(param1Int);
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("invalid position ");
      stringBuilder.append(param1Int);
      stringBuilder.append(". State item count is ");
      RecyclerView.State state = RecyclerView.this.mState;
      stringBuilder.append(state.getItemCount());
      throw new IndexOutOfBoundsException(stringBuilder.toString());
    }
    
    public View getViewForPosition(int param1Int) {
      return getViewForPosition(param1Int, false);
    }
    
    View getViewForPosition(int param1Int, boolean param1Boolean) {
      return (tryGetViewHolderForPositionByDeadline(param1Int, param1Boolean, Long.MAX_VALUE)).itemView;
    }
    
    RecyclerView.ViewHolder tryGetViewHolderForPositionByDeadline(int param1Int, boolean param1Boolean, long param1Long) {
      if (param1Int >= 0 && param1Int < RecyclerView.this.mState.getItemCount()) {
        RecyclerView.LayoutParams layoutParams;
        RecyclerView.State state1;
        int i = 0;
        RecyclerView.ViewHolder viewHolder1 = null;
        boolean bool = RecyclerView.this.mState.isPreLayout();
        boolean bool1 = true;
        if (bool) {
          byte b;
          viewHolder1 = getChangedScrapViewForPosition(param1Int);
          if (viewHolder1 != null) {
            b = 1;
          } else {
            b = 0;
          } 
          i = b;
        } 
        int j = i;
        RecyclerView.ViewHolder viewHolder2 = viewHolder1;
        if (viewHolder1 == null) {
          viewHolder1 = getScrapOrHiddenOrCachedHolderForPosition(param1Int, param1Boolean);
          j = i;
          viewHolder2 = viewHolder1;
          if (viewHolder1 != null)
            if (!validateViewHolderForOffsetPosition(viewHolder1)) {
              if (!param1Boolean) {
                viewHolder1.addFlags(4);
                if (viewHolder1.isScrap()) {
                  RecyclerView.this.removeDetachedView(viewHolder1.itemView, false);
                  viewHolder1.unScrap();
                } else if (viewHolder1.wasReturnedFromScrap()) {
                  viewHolder1.clearReturnedFromScrapFlag();
                } 
                recycleViewHolderInternal(viewHolder1);
              } 
              viewHolder2 = null;
              j = i;
            } else {
              j = 1;
              viewHolder2 = viewHolder1;
            }  
        } 
        if (viewHolder2 == null) {
          int k = RecyclerView.this.mAdapterHelper.findPositionOffset(param1Int);
          if (k >= 0 && k < RecyclerView.this.mAdapter.getItemCount()) {
            RecyclerView recyclerView;
            int m = RecyclerView.this.mAdapter.getItemViewType(k);
            i = j;
            viewHolder1 = viewHolder2;
            if (RecyclerView.this.mAdapter.hasStableIds()) {
              viewHolder2 = getScrapOrCachedViewForId(RecyclerView.this.mAdapter.getItemId(k), m, param1Boolean);
              i = j;
              viewHolder1 = viewHolder2;
              if (viewHolder2 != null) {
                viewHolder2.mPosition = k;
                i = 1;
                viewHolder1 = viewHolder2;
              } 
            } 
            viewHolder2 = viewHolder1;
            if (viewHolder1 == null) {
              RecyclerView.ViewCacheExtension viewCacheExtension = this.mViewCacheExtension;
              viewHolder2 = viewHolder1;
              if (viewCacheExtension != null) {
                View view = viewCacheExtension.getViewForPositionAndType(this, param1Int, m);
                viewHolder2 = viewHolder1;
                if (view != null) {
                  viewHolder2 = RecyclerView.this.getChildViewHolder(view);
                  if (viewHolder2 != null) {
                    if (viewHolder2.shouldIgnore())
                      throw new IllegalArgumentException("getViewForPositionAndType returned a view that is ignored. You must call stopIgnoring before returning this view."); 
                  } else {
                    throw new IllegalArgumentException("getViewForPositionAndType returned a view which does not have a ViewHolder");
                  } 
                } 
              } 
            } 
            viewHolder1 = viewHolder2;
            if (viewHolder2 == null) {
              viewHolder2 = getRecycledViewPool().getRecycledView(m);
              viewHolder1 = viewHolder2;
              if (viewHolder2 != null) {
                viewHolder2.resetInternal();
                viewHolder1 = viewHolder2;
                if (RecyclerView.FORCE_INVALIDATE_DISPLAY_LIST) {
                  invalidateDisplayListInt(viewHolder2);
                  viewHolder1 = viewHolder2;
                } 
              } 
            } 
            if (viewHolder1 == null) {
              long l1 = RecyclerView.this.getNanoTime();
              if (param1Long != Long.MAX_VALUE) {
                RecyclerView.RecycledViewPool recycledViewPool = this.mRecyclerPool;
                if (!recycledViewPool.willCreateInTime(m, l1, param1Long))
                  return null; 
              } 
              viewHolder2 = RecyclerView.this.mAdapter.createViewHolder(RecyclerView.this, m);
              if (RecyclerView.ALLOW_THREAD_GAP_WORK) {
                recyclerView = RecyclerView.findNestedRecyclerView(viewHolder2.itemView);
                if (recyclerView != null)
                  viewHolder2.mNestedRecyclerView = new WeakReference<>(recyclerView); 
              } 
              long l2 = RecyclerView.this.getNanoTime();
              this.mRecyclerPool.factorInCreateTime(m, l2 - l1);
              j = i;
            } else {
              j = i;
              RecyclerView recyclerView1 = recyclerView;
            } 
          } else {
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.append("Inconsistency detected. Invalid item position ");
            stringBuilder1.append(param1Int);
            stringBuilder1.append("(offset:");
            stringBuilder1.append(k);
            stringBuilder1.append(").state:");
            state1 = RecyclerView.this.mState;
            stringBuilder1.append(state1.getItemCount());
            throw new IndexOutOfBoundsException(stringBuilder1.toString());
          } 
        } 
        if (j && !RecyclerView.this.mState.isPreLayout() && 
          state1.hasAnyOfTheFlags(8192)) {
          state1.setFlags(0, 8192);
          if (RecyclerView.this.mState.mRunSimpleAnimations) {
            i = RecyclerView.ItemAnimator.buildAdapterChangeFlagsForAnimations((RecyclerView.ViewHolder)state1);
            RecyclerView.ItemAnimator itemAnimator = RecyclerView.this.mItemAnimator;
            RecyclerView.State state2 = RecyclerView.this.mState;
            List<Object> list = state1.getUnmodifiedPayloads();
            RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo = itemAnimator.recordPreLayoutInformation(state2, (RecyclerView.ViewHolder)state1, i | 0x1000, list);
            RecyclerView.this.recordAnimationInfoIfBouncedHiddenView((RecyclerView.ViewHolder)state1, itemHolderInfo);
          } 
        } 
        param1Boolean = false;
        if (RecyclerView.this.mState.isPreLayout() && state1.isBound()) {
          ((RecyclerView.ViewHolder)state1).mPreLayoutPosition = param1Int;
        } else if (!state1.isBound() || state1.needsUpdate() || state1.isInvalid()) {
          i = RecyclerView.this.mAdapterHelper.findPositionOffset(param1Int);
          param1Boolean = tryBindViewHolderByDeadline((RecyclerView.ViewHolder)state1, i, param1Int, param1Long);
        } 
        ViewGroup.LayoutParams layoutParams1 = ((RecyclerView.ViewHolder)state1).itemView.getLayoutParams();
        if (layoutParams1 == null) {
          layoutParams = (RecyclerView.LayoutParams)RecyclerView.this.generateDefaultLayoutParams();
          ((RecyclerView.ViewHolder)state1).itemView.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
        } else if (!RecyclerView.this.checkLayoutParams((ViewGroup.LayoutParams)layoutParams)) {
          layoutParams = (RecyclerView.LayoutParams)RecyclerView.this.generateLayoutParams((ViewGroup.LayoutParams)layoutParams);
          ((RecyclerView.ViewHolder)state1).itemView.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
        } else {
          layoutParams = layoutParams;
        } 
        layoutParams.mViewHolder = (RecyclerView.ViewHolder)state1;
        if (j != 0 && param1Boolean) {
          param1Boolean = bool1;
        } else {
          param1Boolean = false;
        } 
        layoutParams.mPendingInvalidate = param1Boolean;
        return (RecyclerView.ViewHolder)state1;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid item position ");
      stringBuilder.append(param1Int);
      stringBuilder.append("(");
      stringBuilder.append(param1Int);
      stringBuilder.append("). Item count:");
      RecyclerView.State state = RecyclerView.this.mState;
      stringBuilder.append(state.getItemCount());
      throw new IndexOutOfBoundsException(stringBuilder.toString());
    }
    
    private void attachAccessibilityDelegate(View param1View) {
      if (RecyclerView.this.isAccessibilityEnabled()) {
        if (param1View.getImportantForAccessibility() == 0)
          param1View.setImportantForAccessibility(1); 
        if (param1View.getAccessibilityDelegate() == null)
          param1View.setAccessibilityDelegate(RecyclerView.this.mAccessibilityDelegate.getItemDelegate()); 
      } 
    }
    
    private void invalidateDisplayListInt(RecyclerView.ViewHolder param1ViewHolder) {
      if (param1ViewHolder.itemView instanceof ViewGroup)
        invalidateDisplayListInt((ViewGroup)param1ViewHolder.itemView, false); 
    }
    
    private void invalidateDisplayListInt(ViewGroup param1ViewGroup, boolean param1Boolean) {
      int i;
      for (i = param1ViewGroup.getChildCount() - 1; i >= 0; i--) {
        View view = param1ViewGroup.getChildAt(i);
        if (view instanceof ViewGroup)
          invalidateDisplayListInt((ViewGroup)view, true); 
      } 
      if (!param1Boolean)
        return; 
      if (param1ViewGroup.getVisibility() == 4) {
        param1ViewGroup.setVisibility(0);
        param1ViewGroup.setVisibility(4);
      } else {
        i = param1ViewGroup.getVisibility();
        param1ViewGroup.setVisibility(4);
        param1ViewGroup.setVisibility(i);
      } 
    }
    
    public void recycleView(View param1View) {
      RecyclerView.ViewHolder viewHolder = RecyclerView.getChildViewHolderInt(param1View);
      if (viewHolder.isTmpDetached())
        RecyclerView.this.removeDetachedView(param1View, false); 
      if (viewHolder.isScrap()) {
        viewHolder.unScrap();
      } else if (viewHolder.wasReturnedFromScrap()) {
        viewHolder.clearReturnedFromScrapFlag();
      } 
      recycleViewHolderInternal(viewHolder);
    }
    
    void recycleViewInternal(View param1View) {
      recycleViewHolderInternal(RecyclerView.getChildViewHolderInt(param1View));
    }
    
    void recycleAndClearCachedViews() {
      int i = this.mCachedViews.size();
      for (; --i >= 0; i--)
        recycleCachedViewAt(i); 
      this.mCachedViews.clear();
      if (RecyclerView.ALLOW_THREAD_GAP_WORK)
        RecyclerView.this.mPrefetchRegistry.clearPrefetchPositions(); 
    }
    
    void recycleCachedViewAt(int param1Int) {
      RecyclerView.ViewHolder viewHolder = this.mCachedViews.get(param1Int);
      addViewHolderToRecycledViewPool(viewHolder, true);
      this.mCachedViews.remove(param1Int);
    }
    
    void recycleViewHolderInternal(RecyclerView.ViewHolder param1ViewHolder) {
      // Byte code:
      //   0: aload_1
      //   1: invokevirtual isScrap : ()Z
      //   4: istore_2
      //   5: iconst_0
      //   6: istore_3
      //   7: iload_2
      //   8: ifne -> 402
      //   11: aload_1
      //   12: getfield itemView : Landroid/view/View;
      //   15: invokevirtual getParent : ()Landroid/view/ViewParent;
      //   18: ifnull -> 24
      //   21: goto -> 402
      //   24: aload_1
      //   25: invokevirtual isTmpDetached : ()Z
      //   28: ifne -> 364
      //   31: aload_1
      //   32: invokevirtual shouldIgnore : ()Z
      //   35: ifne -> 353
      //   38: aload_1
      //   39: invokestatic access$700 : (Lcom/android/internal/widget/RecyclerView$ViewHolder;)Z
      //   42: istore_3
      //   43: aload_0
      //   44: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   47: getfield mAdapter : Lcom/android/internal/widget/RecyclerView$Adapter;
      //   50: ifnull -> 81
      //   53: iload_3
      //   54: ifeq -> 81
      //   57: aload_0
      //   58: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   61: getfield mAdapter : Lcom/android/internal/widget/RecyclerView$Adapter;
      //   64: astore #4
      //   66: aload #4
      //   68: aload_1
      //   69: invokevirtual onFailedToRecycleView : (Lcom/android/internal/widget/RecyclerView$ViewHolder;)Z
      //   72: ifeq -> 81
      //   75: iconst_1
      //   76: istore #5
      //   78: goto -> 84
      //   81: iconst_0
      //   82: istore #5
      //   84: iconst_0
      //   85: istore #6
      //   87: iconst_0
      //   88: istore #7
      //   90: iconst_0
      //   91: istore #8
      //   93: iload #5
      //   95: ifne -> 109
      //   98: iload #8
      //   100: istore #9
      //   102: aload_1
      //   103: invokevirtual isRecyclable : ()Z
      //   106: ifeq -> 322
      //   109: iload #7
      //   111: istore #5
      //   113: aload_0
      //   114: getfield mViewCacheMax : I
      //   117: ifle -> 296
      //   120: iload #7
      //   122: istore #5
      //   124: aload_1
      //   125: sipush #526
      //   128: invokevirtual hasAnyOfTheFlags : (I)Z
      //   131: ifne -> 296
      //   134: aload_0
      //   135: getfield mCachedViews : Ljava/util/ArrayList;
      //   138: invokevirtual size : ()I
      //   141: istore #9
      //   143: iload #9
      //   145: istore #5
      //   147: iload #9
      //   149: aload_0
      //   150: getfield mViewCacheMax : I
      //   153: if_icmplt -> 176
      //   156: iload #9
      //   158: istore #5
      //   160: iload #9
      //   162: ifle -> 176
      //   165: aload_0
      //   166: iconst_0
      //   167: invokevirtual recycleCachedViewAt : (I)V
      //   170: iload #9
      //   172: iconst_1
      //   173: isub
      //   174: istore #5
      //   176: iload #5
      //   178: istore #6
      //   180: iload #6
      //   182: istore #9
      //   184: invokestatic access$600 : ()Z
      //   187: ifeq -> 283
      //   190: iload #6
      //   192: istore #9
      //   194: iload #5
      //   196: ifle -> 283
      //   199: aload_0
      //   200: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   203: getfield mPrefetchRegistry : Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;
      //   206: astore #4
      //   208: aload_1
      //   209: getfield mPosition : I
      //   212: istore #7
      //   214: iload #6
      //   216: istore #9
      //   218: aload #4
      //   220: iload #7
      //   222: invokevirtual lastPrefetchIncludedPosition : (I)Z
      //   225: ifne -> 283
      //   228: iinc #5, -1
      //   231: iload #5
      //   233: iflt -> 277
      //   236: aload_0
      //   237: getfield mCachedViews : Ljava/util/ArrayList;
      //   240: iload #5
      //   242: invokevirtual get : (I)Ljava/lang/Object;
      //   245: checkcast com/android/internal/widget/RecyclerView$ViewHolder
      //   248: getfield mPosition : I
      //   251: istore #9
      //   253: aload_0
      //   254: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   257: getfield mPrefetchRegistry : Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;
      //   260: iload #9
      //   262: invokevirtual lastPrefetchIncludedPosition : (I)Z
      //   265: ifne -> 271
      //   268: goto -> 277
      //   271: iinc #5, -1
      //   274: goto -> 231
      //   277: iload #5
      //   279: iconst_1
      //   280: iadd
      //   281: istore #9
      //   283: aload_0
      //   284: getfield mCachedViews : Ljava/util/ArrayList;
      //   287: iload #9
      //   289: aload_1
      //   290: invokevirtual add : (ILjava/lang/Object;)V
      //   293: iconst_1
      //   294: istore #5
      //   296: iload #5
      //   298: istore #6
      //   300: iload #8
      //   302: istore #9
      //   304: iload #5
      //   306: ifne -> 322
      //   309: aload_0
      //   310: aload_1
      //   311: iconst_1
      //   312: invokevirtual addViewHolderToRecycledViewPool : (Lcom/android/internal/widget/RecyclerView$ViewHolder;Z)V
      //   315: iconst_1
      //   316: istore #9
      //   318: iload #5
      //   320: istore #6
      //   322: aload_0
      //   323: getfield this$0 : Lcom/android/internal/widget/RecyclerView;
      //   326: getfield mViewInfoStore : Lcom/android/internal/widget/ViewInfoStore;
      //   329: aload_1
      //   330: invokevirtual removeViewHolder : (Lcom/android/internal/widget/RecyclerView$ViewHolder;)V
      //   333: iload #6
      //   335: ifne -> 352
      //   338: iload #9
      //   340: ifne -> 352
      //   343: iload_3
      //   344: ifeq -> 352
      //   347: aload_1
      //   348: aconst_null
      //   349: putfield mOwnerRecyclerView : Lcom/android/internal/widget/RecyclerView;
      //   352: return
      //   353: new java/lang/IllegalArgumentException
      //   356: dup
      //   357: ldc_w 'Trying to recycle an ignored view holder. You should first call stopIgnoringView(view) before calling recycle.'
      //   360: invokespecial <init> : (Ljava/lang/String;)V
      //   363: athrow
      //   364: new java/lang/StringBuilder
      //   367: dup
      //   368: invokespecial <init> : ()V
      //   371: astore #4
      //   373: aload #4
      //   375: ldc_w 'Tmp detached view should be removed from RecyclerView before it can be recycled: '
      //   378: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   381: pop
      //   382: aload #4
      //   384: aload_1
      //   385: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   388: pop
      //   389: new java/lang/IllegalArgumentException
      //   392: dup
      //   393: aload #4
      //   395: invokevirtual toString : ()Ljava/lang/String;
      //   398: invokespecial <init> : (Ljava/lang/String;)V
      //   401: athrow
      //   402: new java/lang/StringBuilder
      //   405: dup
      //   406: invokespecial <init> : ()V
      //   409: astore #4
      //   411: aload #4
      //   413: ldc_w 'Scrapped or attached views may not be recycled. isScrap:'
      //   416: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   419: pop
      //   420: aload #4
      //   422: aload_1
      //   423: invokevirtual isScrap : ()Z
      //   426: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   429: pop
      //   430: aload #4
      //   432: ldc_w ' isAttached:'
      //   435: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   438: pop
      //   439: aload_1
      //   440: getfield itemView : Landroid/view/View;
      //   443: astore_1
      //   444: aload_1
      //   445: invokevirtual getParent : ()Landroid/view/ViewParent;
      //   448: ifnull -> 453
      //   451: iconst_1
      //   452: istore_3
      //   453: aload #4
      //   455: iload_3
      //   456: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   459: pop
      //   460: new java/lang/IllegalArgumentException
      //   463: dup
      //   464: aload #4
      //   466: invokevirtual toString : ()Ljava/lang/String;
      //   469: invokespecial <init> : (Ljava/lang/String;)V
      //   472: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #5693	-> 0
      //   #5700	-> 24
      //   #5705	-> 31
      //   #5710	-> 38
      //   #5711	-> 38
      //   #5712	-> 43
      //   #5714	-> 66
      //   #5715	-> 84
      //   #5716	-> 90
      //   #5721	-> 93
      //   #5722	-> 109
      //   #5723	-> 120
      //   #5728	-> 134
      //   #5729	-> 143
      //   #5730	-> 165
      //   #5731	-> 170
      //   #5734	-> 176
      //   #5735	-> 180
      //   #5737	-> 214
      //   #5739	-> 228
      //   #5740	-> 231
      //   #5741	-> 236
      //   #5742	-> 253
      //   #5743	-> 268
      //   #5745	-> 271
      //   #5746	-> 274
      //   #5747	-> 277
      //   #5749	-> 283
      //   #5750	-> 293
      //   #5752	-> 296
      //   #5753	-> 309
      //   #5754	-> 315
      //   #5770	-> 322
      //   #5771	-> 333
      //   #5772	-> 347
      //   #5774	-> 352
      //   #5706	-> 353
      //   #5701	-> 364
      //   #5694	-> 402
      //   #5696	-> 420
      //   #5697	-> 444
    }
    
    void addViewHolderToRecycledViewPool(RecyclerView.ViewHolder param1ViewHolder, boolean param1Boolean) {
      RecyclerView.clearNestedRecyclerViewIfNotNested(param1ViewHolder);
      param1ViewHolder.itemView.setAccessibilityDelegate(null);
      if (param1Boolean)
        dispatchViewRecycled(param1ViewHolder); 
      param1ViewHolder.mOwnerRecyclerView = null;
      getRecycledViewPool().putRecycledView(param1ViewHolder);
    }
    
    void quickRecycleScrapView(View param1View) {
      RecyclerView.ViewHolder viewHolder = RecyclerView.getChildViewHolderInt(param1View);
      RecyclerView.ViewHolder.access$802(viewHolder, null);
      RecyclerView.ViewHolder.access$902(viewHolder, false);
      viewHolder.clearReturnedFromScrapFlag();
      recycleViewHolderInternal(viewHolder);
    }
    
    void scrapView(View param1View) {
      RecyclerView.ViewHolder viewHolder = RecyclerView.getChildViewHolderInt(param1View);
      if (viewHolder.hasAnyOfTheFlags(12) || 
        !viewHolder.isUpdated() || RecyclerView.this.canReuseUpdatedViewHolder(viewHolder)) {
        if (!viewHolder.isInvalid() || viewHolder.isRemoved() || RecyclerView.this.mAdapter.hasStableIds()) {
          viewHolder.setScrapContainer(this, false);
          this.mAttachedScrap.add(viewHolder);
          return;
        } 
        throw new IllegalArgumentException("Called scrap view with an invalid view. Invalid views cannot be reused from scrap, they should rebound from recycler pool.");
      } 
      if (this.mChangedScrap == null)
        this.mChangedScrap = new ArrayList<>(); 
      viewHolder.setScrapContainer(this, true);
      this.mChangedScrap.add(viewHolder);
    }
    
    void unscrapView(RecyclerView.ViewHolder param1ViewHolder) {
      if (param1ViewHolder.mInChangeScrap) {
        this.mChangedScrap.remove(param1ViewHolder);
      } else {
        this.mAttachedScrap.remove(param1ViewHolder);
      } 
      RecyclerView.ViewHolder.access$802(param1ViewHolder, null);
      RecyclerView.ViewHolder.access$902(param1ViewHolder, false);
      param1ViewHolder.clearReturnedFromScrapFlag();
    }
    
    int getScrapCount() {
      return this.mAttachedScrap.size();
    }
    
    View getScrapViewAt(int param1Int) {
      return ((RecyclerView.ViewHolder)this.mAttachedScrap.get(param1Int)).itemView;
    }
    
    void clearScrap() {
      this.mAttachedScrap.clear();
      ArrayList<RecyclerView.ViewHolder> arrayList = this.mChangedScrap;
      if (arrayList != null)
        arrayList.clear(); 
    }
    
    RecyclerView.ViewHolder getChangedScrapViewForPosition(int param1Int) {
      ArrayList<RecyclerView.ViewHolder> arrayList = this.mChangedScrap;
      if (arrayList != null) {
        int i = arrayList.size();
        if (i != 0) {
          for (byte b = 0; b < i; b++) {
            RecyclerView.ViewHolder viewHolder = this.mChangedScrap.get(b);
            if (!viewHolder.wasReturnedFromScrap() && viewHolder.getLayoutPosition() == param1Int) {
              viewHolder.addFlags(32);
              return viewHolder;
            } 
          } 
          if (RecyclerView.this.mAdapter.hasStableIds()) {
            param1Int = RecyclerView.this.mAdapterHelper.findPositionOffset(param1Int);
            if (param1Int > 0 && param1Int < RecyclerView.this.mAdapter.getItemCount()) {
              long l = RecyclerView.this.mAdapter.getItemId(param1Int);
              for (param1Int = 0; param1Int < i; param1Int++) {
                RecyclerView.ViewHolder viewHolder = this.mChangedScrap.get(param1Int);
                if (!viewHolder.wasReturnedFromScrap() && viewHolder.getItemId() == l) {
                  viewHolder.addFlags(32);
                  return viewHolder;
                } 
              } 
            } 
          } 
          return null;
        } 
      } 
      return null;
    }
    
    RecyclerView.ViewHolder getScrapOrHiddenOrCachedHolderForPosition(int param1Int, boolean param1Boolean) {
      int i = this.mAttachedScrap.size();
      byte b;
      for (b = 0; b < i; b++) {
        RecyclerView.ViewHolder viewHolder = this.mAttachedScrap.get(b);
        if (!viewHolder.wasReturnedFromScrap() && viewHolder.getLayoutPosition() == param1Int && 
          !viewHolder.isInvalid() && (RecyclerView.this.mState.mInPreLayout || !viewHolder.isRemoved())) {
          viewHolder.addFlags(32);
          return viewHolder;
        } 
      } 
      if (!param1Boolean) {
        View view = RecyclerView.this.mChildHelper.findHiddenNonRemovedView(param1Int);
        if (view != null) {
          RecyclerView.ViewHolder viewHolder = RecyclerView.getChildViewHolderInt(view);
          RecyclerView.this.mChildHelper.unhide(view);
          param1Int = RecyclerView.this.mChildHelper.indexOfChild(view);
          if (param1Int != -1) {
            RecyclerView.this.mChildHelper.detachViewFromParent(param1Int);
            scrapView(view);
            viewHolder.addFlags(8224);
            return viewHolder;
          } 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("layout index should not be -1 after unhiding a view:");
          stringBuilder.append(viewHolder);
          throw new IllegalStateException(stringBuilder.toString());
        } 
      } 
      i = this.mCachedViews.size();
      for (b = 0; b < i; b++) {
        RecyclerView.ViewHolder viewHolder = this.mCachedViews.get(b);
        if (!viewHolder.isInvalid() && viewHolder.getLayoutPosition() == param1Int) {
          if (!param1Boolean)
            this.mCachedViews.remove(b); 
          return viewHolder;
        } 
      } 
      return null;
    }
    
    RecyclerView.ViewHolder getScrapOrCachedViewForId(long param1Long, int param1Int, boolean param1Boolean) {
      int i = this.mAttachedScrap.size();
      for (; --i >= 0; i--) {
        RecyclerView.ViewHolder viewHolder = this.mAttachedScrap.get(i);
        if (viewHolder.getItemId() == param1Long && !viewHolder.wasReturnedFromScrap()) {
          if (param1Int == viewHolder.getItemViewType()) {
            viewHolder.addFlags(32);
            if (viewHolder.isRemoved())
              if (!RecyclerView.this.mState.isPreLayout())
                viewHolder.setFlags(2, 14);  
            return viewHolder;
          } 
          if (!param1Boolean) {
            this.mAttachedScrap.remove(i);
            RecyclerView.this.removeDetachedView(viewHolder.itemView, false);
            quickRecycleScrapView(viewHolder.itemView);
          } 
        } 
      } 
      i = this.mCachedViews.size();
      for (; --i >= 0; i--) {
        RecyclerView.ViewHolder viewHolder = this.mCachedViews.get(i);
        if (viewHolder.getItemId() == param1Long) {
          if (param1Int == viewHolder.getItemViewType()) {
            if (!param1Boolean)
              this.mCachedViews.remove(i); 
            return viewHolder;
          } 
          if (!param1Boolean) {
            recycleCachedViewAt(i);
            return null;
          } 
        } 
      } 
      return null;
    }
    
    void dispatchViewRecycled(RecyclerView.ViewHolder param1ViewHolder) {
      if (RecyclerView.this.mRecyclerListener != null)
        RecyclerView.this.mRecyclerListener.onViewRecycled(param1ViewHolder); 
      if (RecyclerView.this.mAdapter != null)
        RecyclerView.this.mAdapter.onViewRecycled(param1ViewHolder); 
      if (RecyclerView.this.mState != null)
        RecyclerView.this.mViewInfoStore.removeViewHolder(param1ViewHolder); 
    }
    
    void onAdapterChanged(RecyclerView.Adapter param1Adapter1, RecyclerView.Adapter param1Adapter2, boolean param1Boolean) {
      clear();
      getRecycledViewPool().onAdapterChanged(param1Adapter1, param1Adapter2, param1Boolean);
    }
    
    void offsetPositionRecordsForMove(int param1Int1, int param1Int2) {
      int i, j;
      boolean bool;
      if (param1Int1 < param1Int2) {
        i = param1Int1;
        j = param1Int2;
        bool = true;
      } else {
        i = param1Int2;
        j = param1Int1;
        bool = true;
      } 
      int k = this.mCachedViews.size();
      for (byte b = 0; b < k; b++) {
        RecyclerView.ViewHolder viewHolder = this.mCachedViews.get(b);
        if (viewHolder != null && viewHolder.mPosition >= i && viewHolder.mPosition <= j)
          if (viewHolder.mPosition == param1Int1) {
            viewHolder.offsetPosition(param1Int2 - param1Int1, false);
          } else {
            viewHolder.offsetPosition(bool, false);
          }  
      } 
    }
    
    void offsetPositionRecordsForInsert(int param1Int1, int param1Int2) {
      int i = this.mCachedViews.size();
      for (byte b = 0; b < i; b++) {
        RecyclerView.ViewHolder viewHolder = this.mCachedViews.get(b);
        if (viewHolder != null && viewHolder.mPosition >= param1Int1)
          viewHolder.offsetPosition(param1Int2, true); 
      } 
    }
    
    void offsetPositionRecordsForRemove(int param1Int1, int param1Int2, boolean param1Boolean) {
      int i = this.mCachedViews.size();
      for (; --i >= 0; i--) {
        RecyclerView.ViewHolder viewHolder = this.mCachedViews.get(i);
        if (viewHolder != null)
          if (viewHolder.mPosition >= param1Int1 + param1Int2) {
            viewHolder.offsetPosition(-param1Int2, param1Boolean);
          } else if (viewHolder.mPosition >= param1Int1) {
            viewHolder.addFlags(8);
            recycleCachedViewAt(i);
          }  
      } 
    }
    
    void setViewCacheExtension(RecyclerView.ViewCacheExtension param1ViewCacheExtension) {
      this.mViewCacheExtension = param1ViewCacheExtension;
    }
    
    void setRecycledViewPool(RecyclerView.RecycledViewPool param1RecycledViewPool) {
      RecyclerView.RecycledViewPool recycledViewPool = this.mRecyclerPool;
      if (recycledViewPool != null)
        recycledViewPool.detach(); 
      this.mRecyclerPool = param1RecycledViewPool;
      if (param1RecycledViewPool != null)
        param1RecycledViewPool.attach(RecyclerView.this.getAdapter()); 
    }
    
    RecyclerView.RecycledViewPool getRecycledViewPool() {
      if (this.mRecyclerPool == null)
        this.mRecyclerPool = new RecyclerView.RecycledViewPool(); 
      return this.mRecyclerPool;
    }
    
    void viewRangeUpdate(int param1Int1, int param1Int2) {
      int i = this.mCachedViews.size();
      for (; --i >= 0; i--) {
        RecyclerView.ViewHolder viewHolder = this.mCachedViews.get(i);
        if (viewHolder != null) {
          int j = viewHolder.getLayoutPosition();
          if (j >= param1Int1 && j < param1Int1 + param1Int2) {
            viewHolder.addFlags(2);
            recycleCachedViewAt(i);
          } 
        } 
      } 
    }
    
    void setAdapterPositionsAsUnknown() {
      int i = this.mCachedViews.size();
      for (byte b = 0; b < i; b++) {
        RecyclerView.ViewHolder viewHolder = this.mCachedViews.get(b);
        if (viewHolder != null)
          viewHolder.addFlags(512); 
      } 
    }
    
    void markKnownViewsInvalid() {
      if (RecyclerView.this.mAdapter != null && RecyclerView.this.mAdapter.hasStableIds()) {
        int i = this.mCachedViews.size();
        for (byte b = 0; b < i; b++) {
          RecyclerView.ViewHolder viewHolder = this.mCachedViews.get(b);
          if (viewHolder != null) {
            viewHolder.addFlags(6);
            viewHolder.addChangePayload(null);
          } 
        } 
      } else {
        recycleAndClearCachedViews();
      } 
    }
    
    void clearOldPositions() {
      int i = this.mCachedViews.size();
      byte b;
      for (b = 0; b < i; b++) {
        RecyclerView.ViewHolder viewHolder = this.mCachedViews.get(b);
        viewHolder.clearOldPosition();
      } 
      i = this.mAttachedScrap.size();
      for (b = 0; b < i; b++)
        ((RecyclerView.ViewHolder)this.mAttachedScrap.get(b)).clearOldPosition(); 
      ArrayList<RecyclerView.ViewHolder> arrayList = this.mChangedScrap;
      if (arrayList != null) {
        i = arrayList.size();
        for (b = 0; b < i; b++)
          ((RecyclerView.ViewHolder)this.mChangedScrap.get(b)).clearOldPosition(); 
      } 
    }
    
    void markItemDecorInsetsDirty() {
      int i = this.mCachedViews.size();
      for (byte b = 0; b < i; b++) {
        RecyclerView.ViewHolder viewHolder = this.mCachedViews.get(b);
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams)viewHolder.itemView.getLayoutParams();
        if (layoutParams != null)
          layoutParams.mInsetsDirty = true; 
      } 
    }
  }
  
  class ViewCacheExtension {
    public abstract View getViewForPositionAndType(RecyclerView.Recycler param1Recycler, int param1Int1, int param1Int2);
  }
  
  class Adapter<VH extends ViewHolder> {
    private final RecyclerView.AdapterDataObservable mObservable = new RecyclerView.AdapterDataObservable();
    
    private boolean mHasStableIds = false;
    
    public void onBindViewHolder(VH param1VH, int param1Int, List<Object> param1List) {
      onBindViewHolder(param1VH, param1Int);
    }
    
    public final VH createViewHolder(ViewGroup param1ViewGroup, int param1Int) {
      Trace.beginSection("RV CreateView");
      param1ViewGroup = (ViewGroup)onCreateViewHolder(param1ViewGroup, param1Int);
      ((RecyclerView.ViewHolder)param1ViewGroup).mItemViewType = param1Int;
      Trace.endSection();
      return (VH)param1ViewGroup;
    }
    
    public final void bindViewHolder(VH param1VH, int param1Int) {
      ((RecyclerView.ViewHolder)param1VH).mPosition = param1Int;
      if (hasStableIds())
        ((RecyclerView.ViewHolder)param1VH).mItemId = getItemId(param1Int); 
      param1VH.setFlags(1, 519);
      Trace.beginSection("RV OnBindView");
      onBindViewHolder(param1VH, param1Int, param1VH.getUnmodifiedPayloads());
      param1VH.clearPayload();
      ViewGroup.LayoutParams layoutParams = ((RecyclerView.ViewHolder)param1VH).itemView.getLayoutParams();
      if (layoutParams instanceof RecyclerView.LayoutParams)
        ((RecyclerView.LayoutParams)layoutParams).mInsetsDirty = true; 
      Trace.endSection();
    }
    
    public int getItemViewType(int param1Int) {
      return 0;
    }
    
    public void setHasStableIds(boolean param1Boolean) {
      if (!hasObservers()) {
        this.mHasStableIds = param1Boolean;
        return;
      } 
      throw new IllegalStateException("Cannot change whether this adapter has stable IDs while the adapter has registered observers.");
    }
    
    public long getItemId(int param1Int) {
      return -1L;
    }
    
    public final boolean hasStableIds() {
      return this.mHasStableIds;
    }
    
    public void onViewRecycled(VH param1VH) {}
    
    public boolean onFailedToRecycleView(VH param1VH) {
      return false;
    }
    
    public void onViewAttachedToWindow(VH param1VH) {}
    
    public void onViewDetachedFromWindow(VH param1VH) {}
    
    public final boolean hasObservers() {
      return this.mObservable.hasObservers();
    }
    
    public void registerAdapterDataObserver(RecyclerView.AdapterDataObserver param1AdapterDataObserver) {
      this.mObservable.registerObserver(param1AdapterDataObserver);
    }
    
    public void unregisterAdapterDataObserver(RecyclerView.AdapterDataObserver param1AdapterDataObserver) {
      this.mObservable.unregisterObserver(param1AdapterDataObserver);
    }
    
    public void onAttachedToRecyclerView(RecyclerView param1RecyclerView) {}
    
    public void onDetachedFromRecyclerView(RecyclerView param1RecyclerView) {}
    
    public final void notifyDataSetChanged() {
      this.mObservable.notifyChanged();
    }
    
    public final void notifyItemChanged(int param1Int) {
      this.mObservable.notifyItemRangeChanged(param1Int, 1);
    }
    
    public final void notifyItemChanged(int param1Int, Object param1Object) {
      this.mObservable.notifyItemRangeChanged(param1Int, 1, param1Object);
    }
    
    public final void notifyItemRangeChanged(int param1Int1, int param1Int2) {
      this.mObservable.notifyItemRangeChanged(param1Int1, param1Int2);
    }
    
    public final void notifyItemRangeChanged(int param1Int1, int param1Int2, Object param1Object) {
      this.mObservable.notifyItemRangeChanged(param1Int1, param1Int2, param1Object);
    }
    
    public final void notifyItemInserted(int param1Int) {
      this.mObservable.notifyItemRangeInserted(param1Int, 1);
    }
    
    public final void notifyItemMoved(int param1Int1, int param1Int2) {
      this.mObservable.notifyItemMoved(param1Int1, param1Int2);
    }
    
    public final void notifyItemRangeInserted(int param1Int1, int param1Int2) {
      this.mObservable.notifyItemRangeInserted(param1Int1, param1Int2);
    }
    
    public final void notifyItemRemoved(int param1Int) {
      this.mObservable.notifyItemRangeRemoved(param1Int, 1);
    }
    
    public final void notifyItemRangeRemoved(int param1Int1, int param1Int2) {
      this.mObservable.notifyItemRangeRemoved(param1Int1, param1Int2);
    }
    
    public abstract int getItemCount();
    
    public abstract void onBindViewHolder(VH param1VH, int param1Int);
    
    public abstract VH onCreateViewHolder(ViewGroup param1ViewGroup, int param1Int);
  }
  
  void dispatchChildDetached(View paramView) {
    ViewHolder viewHolder = getChildViewHolderInt(paramView);
    onChildDetachedFromWindow(paramView);
    Adapter<ViewHolder> adapter = this.mAdapter;
    if (adapter != null && viewHolder != null)
      adapter.onViewDetachedFromWindow(viewHolder); 
    List<OnChildAttachStateChangeListener> list = this.mOnChildAttachStateListeners;
    if (list != null) {
      int i = list.size();
      for (; --i >= 0; i--)
        ((OnChildAttachStateChangeListener)this.mOnChildAttachStateListeners.get(i)).onChildViewDetachedFromWindow(paramView); 
    } 
  }
  
  void dispatchChildAttached(View paramView) {
    ViewHolder viewHolder = getChildViewHolderInt(paramView);
    onChildAttachedToWindow(paramView);
    Adapter<ViewHolder> adapter = this.mAdapter;
    if (adapter != null && viewHolder != null)
      adapter.onViewAttachedToWindow(viewHolder); 
    List<OnChildAttachStateChangeListener> list = this.mOnChildAttachStateListeners;
    if (list != null) {
      int i = list.size();
      for (; --i >= 0; i--)
        ((OnChildAttachStateChangeListener)this.mOnChildAttachStateListeners.get(i)).onChildViewAttachedToWindow(paramView); 
    } 
  }
  
  class LayoutManager {
    boolean mRequestedSimpleAnimations = false;
    
    boolean mIsAttachedToWindow = false;
    
    boolean mAutoMeasure = false;
    
    private boolean mMeasurementCacheEnabled = true;
    
    private boolean mItemPrefetchEnabled = true;
    
    ChildHelper mChildHelper;
    
    private int mHeight;
    
    private int mHeightMode;
    
    int mPrefetchMaxCountObserved;
    
    boolean mPrefetchMaxObservedInInitialPrefetch;
    
    RecyclerView mRecyclerView;
    
    RecyclerView.SmoothScroller mSmoothScroller;
    
    private int mWidth;
    
    private int mWidthMode;
    
    void setRecyclerView(RecyclerView param1RecyclerView) {
      if (param1RecyclerView == null) {
        this.mRecyclerView = null;
        this.mChildHelper = null;
        this.mWidth = 0;
        this.mHeight = 0;
      } else {
        this.mRecyclerView = param1RecyclerView;
        this.mChildHelper = param1RecyclerView.mChildHelper;
        this.mWidth = param1RecyclerView.getWidth();
        this.mHeight = param1RecyclerView.getHeight();
      } 
      this.mWidthMode = 1073741824;
      this.mHeightMode = 1073741824;
    }
    
    void setMeasureSpecs(int param1Int1, int param1Int2) {
      this.mWidth = View.MeasureSpec.getSize(param1Int1);
      this.mWidthMode = param1Int1 = View.MeasureSpec.getMode(param1Int1);
      if (param1Int1 == 0 && !RecyclerView.ALLOW_SIZE_IN_UNSPECIFIED_SPEC)
        this.mWidth = 0; 
      this.mHeight = View.MeasureSpec.getSize(param1Int2);
      this.mHeightMode = param1Int1 = View.MeasureSpec.getMode(param1Int2);
      if (param1Int1 == 0 && !RecyclerView.ALLOW_SIZE_IN_UNSPECIFIED_SPEC)
        this.mHeight = 0; 
    }
    
    void setMeasuredDimensionFromChildren(int param1Int1, int param1Int2) {
      int i = getChildCount();
      if (i == 0) {
        this.mRecyclerView.defaultOnMeasure(param1Int1, param1Int2);
        return;
      } 
      int j = Integer.MAX_VALUE;
      int k = Integer.MAX_VALUE;
      int m = Integer.MIN_VALUE;
      int n = Integer.MIN_VALUE;
      for (byte b = 0; b < i; b++, j = i1, k = m, m = i2, n = i3) {
        View view = getChildAt(b);
        Rect rect = this.mRecyclerView.mTempRect;
        getDecoratedBoundsWithMargins(view, rect);
        int i1 = j;
        if (rect.left < j)
          i1 = rect.left; 
        int i2 = m;
        if (rect.right > m)
          i2 = rect.right; 
        m = k;
        if (rect.top < k)
          m = rect.top; 
        int i3 = n;
        if (rect.bottom > n)
          i3 = rect.bottom; 
      } 
      this.mRecyclerView.mTempRect.set(j, k, m, n);
      setMeasuredDimension(this.mRecyclerView.mTempRect, param1Int1, param1Int2);
    }
    
    public void setMeasuredDimension(Rect param1Rect, int param1Int1, int param1Int2) {
      int i = param1Rect.width(), j = getPaddingLeft(), k = getPaddingRight();
      int m = param1Rect.height(), n = getPaddingTop(), i1 = getPaddingBottom();
      param1Int1 = chooseSize(param1Int1, i + j + k, getMinimumWidth());
      param1Int2 = chooseSize(param1Int2, m + n + i1, getMinimumHeight());
      setMeasuredDimension(param1Int1, param1Int2);
    }
    
    public void requestLayout() {
      RecyclerView recyclerView = this.mRecyclerView;
      if (recyclerView != null)
        recyclerView.requestLayout(); 
    }
    
    public void assertInLayoutOrScroll(String param1String) {
      RecyclerView recyclerView = this.mRecyclerView;
      if (recyclerView != null)
        recyclerView.assertInLayoutOrScroll(param1String); 
    }
    
    public static int chooseSize(int param1Int1, int param1Int2, int param1Int3) {
      int i = View.MeasureSpec.getMode(param1Int1);
      param1Int1 = View.MeasureSpec.getSize(param1Int1);
      if (i != Integer.MIN_VALUE) {
        if (i != 1073741824)
          return Math.max(param1Int2, param1Int3); 
        return param1Int1;
      } 
      return Math.min(param1Int1, Math.max(param1Int2, param1Int3));
    }
    
    public void assertNotInLayoutOrScroll(String param1String) {
      RecyclerView recyclerView = this.mRecyclerView;
      if (recyclerView != null)
        recyclerView.assertNotInLayoutOrScroll(param1String); 
    }
    
    public void setAutoMeasureEnabled(boolean param1Boolean) {
      this.mAutoMeasure = param1Boolean;
    }
    
    public boolean isAutoMeasureEnabled() {
      return this.mAutoMeasure;
    }
    
    public boolean supportsPredictiveItemAnimations() {
      return false;
    }
    
    public final void setItemPrefetchEnabled(boolean param1Boolean) {
      if (param1Boolean != this.mItemPrefetchEnabled) {
        this.mItemPrefetchEnabled = param1Boolean;
        this.mPrefetchMaxCountObserved = 0;
        RecyclerView recyclerView = this.mRecyclerView;
        if (recyclerView != null)
          recyclerView.mRecycler.updateViewCacheSize(); 
      } 
    }
    
    public final boolean isItemPrefetchEnabled() {
      return this.mItemPrefetchEnabled;
    }
    
    public void collectAdjacentPrefetchPositions(int param1Int1, int param1Int2, RecyclerView.State param1State, LayoutPrefetchRegistry param1LayoutPrefetchRegistry) {}
    
    public void collectInitialPrefetchPositions(int param1Int, LayoutPrefetchRegistry param1LayoutPrefetchRegistry) {}
    
    void dispatchAttachedToWindow(RecyclerView param1RecyclerView) {
      this.mIsAttachedToWindow = true;
      onAttachedToWindow(param1RecyclerView);
    }
    
    void dispatchDetachedFromWindow(RecyclerView param1RecyclerView, RecyclerView.Recycler param1Recycler) {
      this.mIsAttachedToWindow = false;
      onDetachedFromWindow(param1RecyclerView, param1Recycler);
    }
    
    public boolean isAttachedToWindow() {
      return this.mIsAttachedToWindow;
    }
    
    public void postOnAnimation(Runnable param1Runnable) {
      RecyclerView recyclerView = this.mRecyclerView;
      if (recyclerView != null)
        recyclerView.postOnAnimation(param1Runnable); 
    }
    
    public boolean removeCallbacks(Runnable param1Runnable) {
      RecyclerView recyclerView = this.mRecyclerView;
      if (recyclerView != null)
        return recyclerView.removeCallbacks(param1Runnable); 
      return false;
    }
    
    public void onAttachedToWindow(RecyclerView param1RecyclerView) {}
    
    @Deprecated
    public void onDetachedFromWindow(RecyclerView param1RecyclerView) {}
    
    public void onDetachedFromWindow(RecyclerView param1RecyclerView, RecyclerView.Recycler param1Recycler) {
      onDetachedFromWindow(param1RecyclerView);
    }
    
    public boolean getClipToPadding() {
      boolean bool;
      RecyclerView recyclerView = this.mRecyclerView;
      if (recyclerView != null && recyclerView.mClipToPadding) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public void onLayoutChildren(RecyclerView.Recycler param1Recycler, RecyclerView.State param1State) {
      Log.e("RecyclerView", "You must override onLayoutChildren(Recycler recycler, State state) ");
    }
    
    public void onLayoutCompleted(RecyclerView.State param1State) {}
    
    public boolean checkLayoutParams(RecyclerView.LayoutParams param1LayoutParams) {
      boolean bool;
      if (param1LayoutParams != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public RecyclerView.LayoutParams generateLayoutParams(ViewGroup.LayoutParams param1LayoutParams) {
      if (param1LayoutParams instanceof RecyclerView.LayoutParams)
        return new RecyclerView.LayoutParams((RecyclerView.LayoutParams)param1LayoutParams); 
      if (param1LayoutParams instanceof ViewGroup.MarginLayoutParams)
        return new RecyclerView.LayoutParams((ViewGroup.MarginLayoutParams)param1LayoutParams); 
      return new RecyclerView.LayoutParams(param1LayoutParams);
    }
    
    public RecyclerView.LayoutParams generateLayoutParams(Context param1Context, AttributeSet param1AttributeSet) {
      return new RecyclerView.LayoutParams(param1Context, param1AttributeSet);
    }
    
    public int scrollHorizontallyBy(int param1Int, RecyclerView.Recycler param1Recycler, RecyclerView.State param1State) {
      return 0;
    }
    
    public int scrollVerticallyBy(int param1Int, RecyclerView.Recycler param1Recycler, RecyclerView.State param1State) {
      return 0;
    }
    
    public boolean canScrollHorizontally() {
      return false;
    }
    
    public boolean canScrollVertically() {
      return false;
    }
    
    public void scrollToPosition(int param1Int) {}
    
    public void smoothScrollToPosition(RecyclerView param1RecyclerView, RecyclerView.State param1State, int param1Int) {
      Log.e("RecyclerView", "You must override smoothScrollToPosition to support smooth scrolling");
    }
    
    public void startSmoothScroll(RecyclerView.SmoothScroller param1SmoothScroller) {
      RecyclerView.SmoothScroller smoothScroller = this.mSmoothScroller;
      if (smoothScroller != null && param1SmoothScroller != smoothScroller && 
        smoothScroller.isRunning())
        this.mSmoothScroller.stop(); 
      this.mSmoothScroller = param1SmoothScroller;
      param1SmoothScroller.start(this.mRecyclerView, this);
    }
    
    public boolean isSmoothScrolling() {
      boolean bool;
      RecyclerView.SmoothScroller smoothScroller = this.mSmoothScroller;
      if (smoothScroller != null && smoothScroller.isRunning()) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public int getLayoutDirection() {
      return this.mRecyclerView.getLayoutDirection();
    }
    
    public void endAnimation(View param1View) {
      if (this.mRecyclerView.mItemAnimator != null)
        this.mRecyclerView.mItemAnimator.endAnimation(RecyclerView.getChildViewHolderInt(param1View)); 
    }
    
    public void addDisappearingView(View param1View) {
      addDisappearingView(param1View, -1);
    }
    
    public void addDisappearingView(View param1View, int param1Int) {
      addViewInt(param1View, param1Int, true);
    }
    
    public void addView(View param1View) {
      addView(param1View, -1);
    }
    
    public void addView(View param1View, int param1Int) {
      addViewInt(param1View, param1Int, false);
    }
    
    private void addViewInt(View param1View, int param1Int, boolean param1Boolean) {
      RecyclerView recyclerView;
      StringBuilder stringBuilder;
      RecyclerView.ViewHolder viewHolder = RecyclerView.getChildViewHolderInt(param1View);
      if (param1Boolean || viewHolder.isRemoved()) {
        this.mRecyclerView.mViewInfoStore.addToDisappearedInLayout(viewHolder);
      } else {
        this.mRecyclerView.mViewInfoStore.removeFromDisappearedInLayout(viewHolder);
      } 
      RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams)param1View.getLayoutParams();
      if (viewHolder.wasReturnedFromScrap() || viewHolder.isScrap()) {
        if (viewHolder.isScrap()) {
          viewHolder.unScrap();
        } else {
          viewHolder.clearReturnedFromScrapFlag();
        } 
        this.mChildHelper.attachViewToParent(param1View, param1Int, param1View.getLayoutParams(), false);
      } else if (param1View.getParent() == this.mRecyclerView) {
        int i = this.mChildHelper.indexOfChild(param1View);
        int j = param1Int;
        if (param1Int == -1)
          j = this.mChildHelper.getChildCount(); 
        if (i != -1) {
          if (i != j)
            this.mRecyclerView.mLayout.moveView(i, j); 
        } else {
          stringBuilder = new StringBuilder();
          stringBuilder.append("Added View has RecyclerView as parent but view is not a real child. Unfiltered index:");
          recyclerView = this.mRecyclerView;
          stringBuilder.append(recyclerView.indexOfChild(param1View));
          throw new IllegalStateException(stringBuilder.toString());
        } 
      } else {
        this.mChildHelper.addView(param1View, param1Int, false);
        ((RecyclerView.LayoutParams)stringBuilder).mInsetsDirty = true;
        RecyclerView.SmoothScroller smoothScroller = this.mSmoothScroller;
        if (smoothScroller != null && smoothScroller.isRunning())
          this.mSmoothScroller.onChildAttachedToWindow(param1View); 
      } 
      if (((RecyclerView.LayoutParams)stringBuilder).mPendingInvalidate) {
        ((RecyclerView.ViewHolder)recyclerView).itemView.invalidate();
        ((RecyclerView.LayoutParams)stringBuilder).mPendingInvalidate = false;
      } 
    }
    
    public void removeView(View param1View) {
      this.mChildHelper.removeView(param1View);
    }
    
    public void removeViewAt(int param1Int) {
      View view = getChildAt(param1Int);
      if (view != null)
        this.mChildHelper.removeViewAt(param1Int); 
    }
    
    public void removeAllViews() {
      int i = getChildCount();
      for (; --i >= 0; i--)
        this.mChildHelper.removeViewAt(i); 
    }
    
    public int getBaseline() {
      return -1;
    }
    
    public int getPosition(View param1View) {
      return ((RecyclerView.LayoutParams)param1View.getLayoutParams()).getViewLayoutPosition();
    }
    
    public int getItemViewType(View param1View) {
      return RecyclerView.getChildViewHolderInt(param1View).getItemViewType();
    }
    
    public View findContainingItemView(View param1View) {
      RecyclerView recyclerView = this.mRecyclerView;
      if (recyclerView == null)
        return null; 
      param1View = recyclerView.findContainingItemView(param1View);
      if (param1View == null)
        return null; 
      if (this.mChildHelper.isHidden(param1View))
        return null; 
      return param1View;
    }
    
    public View findViewByPosition(int param1Int) {
      int i = getChildCount();
      for (byte b = 0; b < i; b++) {
        View view = getChildAt(b);
        RecyclerView.ViewHolder viewHolder = RecyclerView.getChildViewHolderInt(view);
        if (viewHolder != null)
          if (viewHolder.getLayoutPosition() == param1Int && !viewHolder.shouldIgnore()) {
            RecyclerView.State state = this.mRecyclerView.mState;
            if (state.isPreLayout() || !viewHolder.isRemoved())
              return view; 
          }  
      } 
      return null;
    }
    
    public void detachView(View param1View) {
      int i = this.mChildHelper.indexOfChild(param1View);
      if (i >= 0)
        detachViewInternal(i, param1View); 
    }
    
    public void detachViewAt(int param1Int) {
      detachViewInternal(param1Int, getChildAt(param1Int));
    }
    
    private void detachViewInternal(int param1Int, View param1View) {
      this.mChildHelper.detachViewFromParent(param1Int);
    }
    
    public void attachView(View param1View, int param1Int, RecyclerView.LayoutParams param1LayoutParams) {
      RecyclerView.ViewHolder viewHolder = RecyclerView.getChildViewHolderInt(param1View);
      if (viewHolder.isRemoved()) {
        this.mRecyclerView.mViewInfoStore.addToDisappearedInLayout(viewHolder);
      } else {
        this.mRecyclerView.mViewInfoStore.removeFromDisappearedInLayout(viewHolder);
      } 
      this.mChildHelper.attachViewToParent(param1View, param1Int, (ViewGroup.LayoutParams)param1LayoutParams, viewHolder.isRemoved());
    }
    
    public void attachView(View param1View, int param1Int) {
      attachView(param1View, param1Int, (RecyclerView.LayoutParams)param1View.getLayoutParams());
    }
    
    public void attachView(View param1View) {
      attachView(param1View, -1);
    }
    
    public void removeDetachedView(View param1View) {
      this.mRecyclerView.removeDetachedView(param1View, false);
    }
    
    public void moveView(int param1Int1, int param1Int2) {
      View view = getChildAt(param1Int1);
      if (view != null) {
        detachViewAt(param1Int1);
        attachView(view, param1Int2);
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Cannot move a child from non-existing index:");
      stringBuilder.append(param1Int1);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public void detachAndScrapView(View param1View, RecyclerView.Recycler param1Recycler) {
      int i = this.mChildHelper.indexOfChild(param1View);
      scrapOrRecycleView(param1Recycler, i, param1View);
    }
    
    public void detachAndScrapViewAt(int param1Int, RecyclerView.Recycler param1Recycler) {
      View view = getChildAt(param1Int);
      scrapOrRecycleView(param1Recycler, param1Int, view);
    }
    
    public void removeAndRecycleView(View param1View, RecyclerView.Recycler param1Recycler) {
      removeView(param1View);
      param1Recycler.recycleView(param1View);
    }
    
    public void removeAndRecycleViewAt(int param1Int, RecyclerView.Recycler param1Recycler) {
      View view = getChildAt(param1Int);
      removeViewAt(param1Int);
      param1Recycler.recycleView(view);
    }
    
    public int getChildCount() {
      boolean bool;
      ChildHelper childHelper = this.mChildHelper;
      if (childHelper != null) {
        bool = childHelper.getChildCount();
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public View getChildAt(int param1Int) {
      ChildHelper childHelper = this.mChildHelper;
      if (childHelper != null) {
        View view = childHelper.getChildAt(param1Int);
      } else {
        childHelper = null;
      } 
      return (View)childHelper;
    }
    
    public int getWidthMode() {
      return this.mWidthMode;
    }
    
    public int getHeightMode() {
      return this.mHeightMode;
    }
    
    public int getWidth() {
      return this.mWidth;
    }
    
    public int getHeight() {
      return this.mHeight;
    }
    
    public int getPaddingLeft() {
      boolean bool;
      RecyclerView recyclerView = this.mRecyclerView;
      if (recyclerView != null) {
        bool = recyclerView.getPaddingLeft();
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public int getPaddingTop() {
      boolean bool;
      RecyclerView recyclerView = this.mRecyclerView;
      if (recyclerView != null) {
        bool = recyclerView.getPaddingTop();
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public int getPaddingRight() {
      boolean bool;
      RecyclerView recyclerView = this.mRecyclerView;
      if (recyclerView != null) {
        bool = recyclerView.getPaddingRight();
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public int getPaddingBottom() {
      boolean bool;
      RecyclerView recyclerView = this.mRecyclerView;
      if (recyclerView != null) {
        bool = recyclerView.getPaddingBottom();
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public int getPaddingStart() {
      boolean bool;
      RecyclerView recyclerView = this.mRecyclerView;
      if (recyclerView != null) {
        bool = recyclerView.getPaddingStart();
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public int getPaddingEnd() {
      boolean bool;
      RecyclerView recyclerView = this.mRecyclerView;
      if (recyclerView != null) {
        bool = recyclerView.getPaddingEnd();
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean isFocused() {
      boolean bool;
      RecyclerView recyclerView = this.mRecyclerView;
      if (recyclerView != null && recyclerView.isFocused()) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean hasFocus() {
      boolean bool;
      RecyclerView recyclerView = this.mRecyclerView;
      if (recyclerView != null && recyclerView.hasFocus()) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public View getFocusedChild() {
      RecyclerView recyclerView = this.mRecyclerView;
      if (recyclerView == null)
        return null; 
      View view = recyclerView.getFocusedChild();
      if (view == null || this.mChildHelper.isHidden(view))
        return null; 
      return view;
    }
    
    public int getItemCount() {
      boolean bool;
      RecyclerView recyclerView = this.mRecyclerView;
      if (recyclerView != null) {
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
      } else {
        recyclerView = null;
      } 
      if (recyclerView != null) {
        bool = recyclerView.getItemCount();
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public void offsetChildrenHorizontal(int param1Int) {
      RecyclerView recyclerView = this.mRecyclerView;
      if (recyclerView != null)
        recyclerView.offsetChildrenHorizontal(param1Int); 
    }
    
    public void offsetChildrenVertical(int param1Int) {
      RecyclerView recyclerView = this.mRecyclerView;
      if (recyclerView != null)
        recyclerView.offsetChildrenVertical(param1Int); 
    }
    
    public void ignoreView(View param1View) {
      ViewParent viewParent = param1View.getParent();
      RecyclerView recyclerView = this.mRecyclerView;
      if (viewParent == recyclerView && recyclerView.indexOfChild(param1View) != -1) {
        RecyclerView.ViewHolder viewHolder = RecyclerView.getChildViewHolderInt(param1View);
        viewHolder.addFlags(128);
        this.mRecyclerView.mViewInfoStore.removeViewHolder(viewHolder);
        return;
      } 
      throw new IllegalArgumentException("View should be fully attached to be ignored");
    }
    
    public void stopIgnoringView(View param1View) {
      RecyclerView.ViewHolder viewHolder = RecyclerView.getChildViewHolderInt(param1View);
      viewHolder.stopIgnoring();
      viewHolder.resetInternal();
      viewHolder.addFlags(4);
    }
    
    public void detachAndScrapAttachedViews(RecyclerView.Recycler param1Recycler) {
      int i = getChildCount();
      for (; --i >= 0; i--) {
        View view = getChildAt(i);
        scrapOrRecycleView(param1Recycler, i, view);
      } 
    }
    
    private void scrapOrRecycleView(RecyclerView.Recycler param1Recycler, int param1Int, View param1View) {
      RecyclerView.ViewHolder viewHolder = RecyclerView.getChildViewHolderInt(param1View);
      if (viewHolder.shouldIgnore())
        return; 
      if (viewHolder.isInvalid() && !viewHolder.isRemoved()) {
        RecyclerView.Adapter adapter = this.mRecyclerView.mAdapter;
        if (!adapter.hasStableIds()) {
          removeViewAt(param1Int);
          param1Recycler.recycleViewHolderInternal(viewHolder);
          return;
        } 
      } 
      detachViewAt(param1Int);
      param1Recycler.scrapView(param1View);
      this.mRecyclerView.mViewInfoStore.onViewDetached(viewHolder);
    }
    
    void removeAndRecycleScrapInt(RecyclerView.Recycler param1Recycler) {
      int i = param1Recycler.getScrapCount();
      for (int j = i - 1; j >= 0; j--) {
        View view = param1Recycler.getScrapViewAt(j);
        RecyclerView.ViewHolder viewHolder = RecyclerView.getChildViewHolderInt(view);
        if (!viewHolder.shouldIgnore()) {
          viewHolder.setIsRecyclable(false);
          if (viewHolder.isTmpDetached())
            this.mRecyclerView.removeDetachedView(view, false); 
          if (this.mRecyclerView.mItemAnimator != null)
            this.mRecyclerView.mItemAnimator.endAnimation(viewHolder); 
          viewHolder.setIsRecyclable(true);
          param1Recycler.quickRecycleScrapView(view);
        } 
      } 
      param1Recycler.clearScrap();
      if (i > 0)
        this.mRecyclerView.invalidate(); 
    }
    
    public void measureChild(View param1View, int param1Int1, int param1Int2) {
      RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams)param1View.getLayoutParams();
      Rect rect = this.mRecyclerView.getItemDecorInsetsForChild(param1View);
      int i = rect.left, j = rect.right;
      int k = rect.top, m = rect.bottom;
      int n = getWidth(), i1 = getWidthMode();
      int i2 = getPaddingLeft(), i3 = getPaddingRight(), i4 = layoutParams.width;
      boolean bool = canScrollHorizontally();
      param1Int1 = getChildMeasureSpec(n, i1, i2 + i3 + param1Int1 + i + j, i4, bool);
      j = getHeight();
      i1 = getHeightMode();
      i4 = getPaddingTop();
      i3 = getPaddingBottom();
      i2 = layoutParams.height;
      bool = canScrollVertically();
      param1Int2 = getChildMeasureSpec(j, i1, i4 + i3 + param1Int2 + k + m, i2, bool);
      if (shouldMeasureChild(param1View, param1Int1, param1Int2, layoutParams))
        param1View.measure(param1Int1, param1Int2); 
    }
    
    boolean shouldReMeasureChild(View param1View, int param1Int1, int param1Int2, RecyclerView.LayoutParams param1LayoutParams) {
      return (!this.mMeasurementCacheEnabled || 
        !isMeasurementUpToDate(param1View.getMeasuredWidth(), param1Int1, param1LayoutParams.width) || 
        !isMeasurementUpToDate(param1View.getMeasuredHeight(), param1Int2, param1LayoutParams.height));
    }
    
    boolean shouldMeasureChild(View param1View, int param1Int1, int param1Int2, RecyclerView.LayoutParams param1LayoutParams) {
      return (!param1View.isLayoutRequested() && this.mMeasurementCacheEnabled) ? (
        
        (!isMeasurementUpToDate(param1View.getWidth(), param1Int1, param1LayoutParams.width) || 
        !isMeasurementUpToDate(param1View.getHeight(), param1Int2, param1LayoutParams.height))) : true;
    }
    
    public boolean isMeasurementCacheEnabled() {
      return this.mMeasurementCacheEnabled;
    }
    
    public void setMeasurementCacheEnabled(boolean param1Boolean) {
      this.mMeasurementCacheEnabled = param1Boolean;
    }
    
    private static boolean isMeasurementUpToDate(int param1Int1, int param1Int2, int param1Int3) {
      int i = View.MeasureSpec.getMode(param1Int2);
      param1Int2 = View.MeasureSpec.getSize(param1Int2);
      boolean bool1 = false, bool2 = false;
      if (param1Int3 > 0 && param1Int1 != param1Int3)
        return false; 
      if (i != Integer.MIN_VALUE) {
        if (i != 0) {
          if (i != 1073741824)
            return false; 
          bool1 = bool2;
          if (param1Int2 == param1Int1)
            bool1 = true; 
          return bool1;
        } 
        return true;
      } 
      if (param1Int2 >= param1Int1)
        bool1 = true; 
      return bool1;
    }
    
    public void measureChildWithMargins(View param1View, int param1Int1, int param1Int2) {
      RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams)param1View.getLayoutParams();
      Rect rect = this.mRecyclerView.getItemDecorInsetsForChild(param1View);
      int i = rect.left, j = rect.right;
      int k = rect.top, m = rect.bottom;
      int n = getWidth(), i1 = getWidthMode();
      int i2 = getPaddingLeft(), i3 = getPaddingRight(), i4 = layoutParams.leftMargin, i5 = layoutParams.rightMargin, i6 = layoutParams.width;
      boolean bool = canScrollHorizontally();
      param1Int1 = getChildMeasureSpec(n, i1, i2 + i3 + i4 + i5 + param1Int1 + i + j, i6, bool);
      i6 = getHeight();
      i4 = getHeightMode();
      i5 = getPaddingTop();
      j = getPaddingBottom();
      i2 = layoutParams.topMargin;
      i1 = layoutParams.bottomMargin;
      i3 = layoutParams.height;
      bool = canScrollVertically();
      param1Int2 = getChildMeasureSpec(i6, i4, i5 + j + i2 + i1 + param1Int2 + k + m, i3, bool);
      if (shouldMeasureChild(param1View, param1Int1, param1Int2, layoutParams))
        param1View.measure(param1Int1, param1Int2); 
    }
    
    @Deprecated
    public static int getChildMeasureSpec(int param1Int1, int param1Int2, int param1Int3, boolean param1Boolean) {
      int i = Math.max(0, param1Int1 - param1Int2);
      param1Int1 = 0;
      param1Int2 = 0;
      if (param1Boolean) {
        if (param1Int3 >= 0) {
          param1Int1 = param1Int3;
          param1Int2 = 1073741824;
        } else {
          param1Int1 = 0;
          param1Int2 = 0;
        } 
      } else if (param1Int3 >= 0) {
        param1Int1 = param1Int3;
        param1Int2 = 1073741824;
      } else if (param1Int3 == -1) {
        param1Int1 = i;
        param1Int2 = 1073741824;
      } else if (param1Int3 == -2) {
        param1Int1 = i;
        param1Int2 = Integer.MIN_VALUE;
      } 
      return View.MeasureSpec.makeMeasureSpec(param1Int1, param1Int2);
    }
    
    public static int getChildMeasureSpec(int param1Int1, int param1Int2, int param1Int3, int param1Int4, boolean param1Boolean) {
      int i = Math.max(0, param1Int1 - param1Int3);
      param1Int3 = 0;
      boolean bool1 = false;
      boolean bool2 = false;
      param1Int1 = 0;
      if (param1Boolean) {
        if (param1Int4 >= 0) {
          param1Int3 = param1Int4;
          param1Int1 = 1073741824;
        } else if (param1Int4 == -1) {
          if (param1Int2 != Integer.MIN_VALUE)
            if (param1Int2 != 0) {
              if (param1Int2 != 1073741824) {
                param1Int3 = bool1;
                return View.MeasureSpec.makeMeasureSpec(param1Int3, param1Int1);
              } 
            } else {
              param1Int3 = 0;
              param1Int1 = 0;
              return View.MeasureSpec.makeMeasureSpec(param1Int3, param1Int1);
            }  
          param1Int3 = i;
          param1Int1 = param1Int2;
        } else {
          param1Int1 = bool2;
          if (param1Int4 == -2) {
            param1Int3 = 0;
            param1Int1 = 0;
          } 
        } 
      } else if (param1Int4 >= 0) {
        param1Int3 = param1Int4;
        param1Int1 = 1073741824;
      } else if (param1Int4 == -1) {
        param1Int3 = i;
        param1Int1 = param1Int2;
      } else {
        param1Int1 = bool2;
        if (param1Int4 == -2) {
          param1Int3 = i;
          if (param1Int2 == Integer.MIN_VALUE || param1Int2 == 1073741824) {
            param1Int1 = Integer.MIN_VALUE;
            return View.MeasureSpec.makeMeasureSpec(param1Int3, param1Int1);
          } 
          param1Int1 = 0;
        } 
      } 
      return View.MeasureSpec.makeMeasureSpec(param1Int3, param1Int1);
    }
    
    public int getDecoratedMeasuredWidth(View param1View) {
      Rect rect = ((RecyclerView.LayoutParams)param1View.getLayoutParams()).mDecorInsets;
      return param1View.getMeasuredWidth() + rect.left + rect.right;
    }
    
    public int getDecoratedMeasuredHeight(View param1View) {
      Rect rect = ((RecyclerView.LayoutParams)param1View.getLayoutParams()).mDecorInsets;
      return param1View.getMeasuredHeight() + rect.top + rect.bottom;
    }
    
    public void layoutDecorated(View param1View, int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      Rect rect = ((RecyclerView.LayoutParams)param1View.getLayoutParams()).mDecorInsets;
      param1View.layout(rect.left + param1Int1, rect.top + param1Int2, param1Int3 - rect.right, param1Int4 - rect.bottom);
    }
    
    public void layoutDecoratedWithMargins(View param1View, int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams)param1View.getLayoutParams();
      Rect rect = layoutParams.mDecorInsets;
      param1View.layout(rect.left + param1Int1 + layoutParams.leftMargin, rect.top + param1Int2 + layoutParams.topMargin, param1Int3 - rect.right - layoutParams.rightMargin, param1Int4 - rect.bottom - layoutParams.bottomMargin);
    }
    
    public void getTransformedBoundingBox(View param1View, boolean param1Boolean, Rect param1Rect) {
      if (param1Boolean) {
        Rect rect = ((RecyclerView.LayoutParams)param1View.getLayoutParams()).mDecorInsets;
        int i = -rect.left, j = -rect.top;
        int k = param1View.getWidth(), m = rect.right, n = param1View.getHeight(), i1 = rect.bottom;
        param1Rect.set(i, j, k + m, n + i1);
      } else {
        param1Rect.set(0, 0, param1View.getWidth(), param1View.getHeight());
      } 
      if (this.mRecyclerView != null) {
        Matrix matrix = param1View.getMatrix();
        if (matrix != null && !matrix.isIdentity()) {
          RectF rectF = this.mRecyclerView.mTempRectF;
          rectF.set(param1Rect);
          matrix.mapRect(rectF);
          double d = rectF.left;
          int k = (int)Math.floor(d);
          d = rectF.top;
          int m = (int)Math.floor(d);
          d = rectF.right;
          int j = (int)Math.ceil(d);
          d = rectF.bottom;
          int i = (int)Math.ceil(d);
          param1Rect.set(k, m, j, i);
        } 
      } 
      param1Rect.offset(param1View.getLeft(), param1View.getTop());
    }
    
    public void getDecoratedBoundsWithMargins(View param1View, Rect param1Rect) {
      RecyclerView.getDecoratedBoundsWithMarginsInt(param1View, param1Rect);
    }
    
    public int getDecoratedLeft(View param1View) {
      return param1View.getLeft() - getLeftDecorationWidth(param1View);
    }
    
    public int getDecoratedTop(View param1View) {
      return param1View.getTop() - getTopDecorationHeight(param1View);
    }
    
    public int getDecoratedRight(View param1View) {
      return param1View.getRight() + getRightDecorationWidth(param1View);
    }
    
    public int getDecoratedBottom(View param1View) {
      return param1View.getBottom() + getBottomDecorationHeight(param1View);
    }
    
    public void calculateItemDecorationsForChild(View param1View, Rect param1Rect) {
      RecyclerView recyclerView = this.mRecyclerView;
      if (recyclerView == null) {
        param1Rect.set(0, 0, 0, 0);
        return;
      } 
      Rect rect = recyclerView.getItemDecorInsetsForChild(param1View);
      param1Rect.set(rect);
    }
    
    public int getTopDecorationHeight(View param1View) {
      return ((RecyclerView.LayoutParams)param1View.getLayoutParams()).mDecorInsets.top;
    }
    
    public int getBottomDecorationHeight(View param1View) {
      return ((RecyclerView.LayoutParams)param1View.getLayoutParams()).mDecorInsets.bottom;
    }
    
    public int getLeftDecorationWidth(View param1View) {
      return ((RecyclerView.LayoutParams)param1View.getLayoutParams()).mDecorInsets.left;
    }
    
    public int getRightDecorationWidth(View param1View) {
      return ((RecyclerView.LayoutParams)param1View.getLayoutParams()).mDecorInsets.right;
    }
    
    public View onFocusSearchFailed(View param1View, int param1Int, RecyclerView.Recycler param1Recycler, RecyclerView.State param1State) {
      return null;
    }
    
    public View onInterceptFocusSearch(View param1View, int param1Int) {
      return null;
    }
    
    public boolean requestChildRectangleOnScreen(RecyclerView param1RecyclerView, View param1View, Rect param1Rect, boolean param1Boolean) {
      int i = getPaddingLeft();
      int j = getPaddingTop();
      int k = getWidth() - getPaddingRight();
      int m = getHeight(), n = getPaddingBottom();
      int i1 = param1View.getLeft() + param1Rect.left - param1View.getScrollX();
      int i2 = param1View.getTop() + param1Rect.top - param1View.getScrollY();
      int i3 = param1Rect.width() + i1;
      int i4 = param1Rect.height();
      int i5 = Math.min(0, i1 - i);
      int i6 = Math.min(0, i2 - j);
      int i7 = Math.max(0, i3 - k);
      n = Math.max(0, i4 + i2 - m - n);
      if (getLayoutDirection() == 1) {
        if (i7 == 0)
          i7 = Math.max(i5, i3 - k); 
      } else if (i5 != 0) {
        i7 = i5;
      } else {
        i7 = Math.min(i1 - i, i7);
      } 
      if (i6 == 0)
        i6 = Math.min(i2 - j, n); 
      if (i7 != 0 || i6 != 0) {
        if (param1Boolean) {
          param1RecyclerView.scrollBy(i7, i6);
        } else {
          param1RecyclerView.smoothScrollBy(i7, i6);
        } 
        return true;
      } 
      return false;
    }
    
    @Deprecated
    public boolean onRequestChildFocus(RecyclerView param1RecyclerView, View param1View1, View param1View2) {
      return (isSmoothScrolling() || param1RecyclerView.isComputingLayout());
    }
    
    public boolean onRequestChildFocus(RecyclerView param1RecyclerView, RecyclerView.State param1State, View param1View1, View param1View2) {
      return onRequestChildFocus(param1RecyclerView, param1View1, param1View2);
    }
    
    public void onAdapterChanged(RecyclerView.Adapter param1Adapter1, RecyclerView.Adapter param1Adapter2) {}
    
    public boolean onAddFocusables(RecyclerView param1RecyclerView, ArrayList<View> param1ArrayList, int param1Int1, int param1Int2) {
      return false;
    }
    
    public void onItemsChanged(RecyclerView param1RecyclerView) {}
    
    public void onItemsAdded(RecyclerView param1RecyclerView, int param1Int1, int param1Int2) {}
    
    public void onItemsRemoved(RecyclerView param1RecyclerView, int param1Int1, int param1Int2) {}
    
    public void onItemsUpdated(RecyclerView param1RecyclerView, int param1Int1, int param1Int2) {}
    
    public void onItemsUpdated(RecyclerView param1RecyclerView, int param1Int1, int param1Int2, Object param1Object) {
      onItemsUpdated(param1RecyclerView, param1Int1, param1Int2);
    }
    
    public void onItemsMoved(RecyclerView param1RecyclerView, int param1Int1, int param1Int2, int param1Int3) {}
    
    public int computeHorizontalScrollExtent(RecyclerView.State param1State) {
      return 0;
    }
    
    public int computeHorizontalScrollOffset(RecyclerView.State param1State) {
      return 0;
    }
    
    public int computeHorizontalScrollRange(RecyclerView.State param1State) {
      return 0;
    }
    
    public int computeVerticalScrollExtent(RecyclerView.State param1State) {
      return 0;
    }
    
    public int computeVerticalScrollOffset(RecyclerView.State param1State) {
      return 0;
    }
    
    public int computeVerticalScrollRange(RecyclerView.State param1State) {
      return 0;
    }
    
    public void onMeasure(RecyclerView.Recycler param1Recycler, RecyclerView.State param1State, int param1Int1, int param1Int2) {
      this.mRecyclerView.defaultOnMeasure(param1Int1, param1Int2);
    }
    
    public void setMeasuredDimension(int param1Int1, int param1Int2) {
      this.mRecyclerView.setMeasuredDimension(param1Int1, param1Int2);
    }
    
    public int getMinimumWidth() {
      return this.mRecyclerView.getMinimumWidth();
    }
    
    public int getMinimumHeight() {
      return this.mRecyclerView.getMinimumHeight();
    }
    
    public Parcelable onSaveInstanceState() {
      return null;
    }
    
    public void onRestoreInstanceState(Parcelable param1Parcelable) {}
    
    void stopSmoothScroller() {
      RecyclerView.SmoothScroller smoothScroller = this.mSmoothScroller;
      if (smoothScroller != null)
        smoothScroller.stop(); 
    }
    
    private void onSmoothScrollerStopped(RecyclerView.SmoothScroller param1SmoothScroller) {
      if (this.mSmoothScroller == param1SmoothScroller)
        this.mSmoothScroller = null; 
    }
    
    public void onScrollStateChanged(int param1Int) {}
    
    public void removeAndRecycleAllViews(RecyclerView.Recycler param1Recycler) {
      for (int i = getChildCount() - 1; i >= 0; i--) {
        View view = getChildAt(i);
        if (!RecyclerView.getChildViewHolderInt(view).shouldIgnore())
          removeAndRecycleViewAt(i, param1Recycler); 
      } 
    }
    
    void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo param1AccessibilityNodeInfo) {
      onInitializeAccessibilityNodeInfo(this.mRecyclerView.mRecycler, this.mRecyclerView.mState, param1AccessibilityNodeInfo);
    }
    
    public void onInitializeAccessibilityNodeInfo(RecyclerView.Recycler param1Recycler, RecyclerView.State param1State, AccessibilityNodeInfo param1AccessibilityNodeInfo) {
      // Byte code:
      //   0: aload_0
      //   1: getfield mRecyclerView : Lcom/android/internal/widget/RecyclerView;
      //   4: iconst_m1
      //   5: invokevirtual canScrollVertically : (I)Z
      //   8: ifne -> 26
      //   11: aload_0
      //   12: getfield mRecyclerView : Lcom/android/internal/widget/RecyclerView;
      //   15: astore #4
      //   17: aload #4
      //   19: iconst_m1
      //   20: invokevirtual canScrollHorizontally : (I)Z
      //   23: ifeq -> 38
      //   26: aload_3
      //   27: sipush #8192
      //   30: invokevirtual addAction : (I)V
      //   33: aload_3
      //   34: iconst_1
      //   35: invokevirtual setScrollable : (Z)V
      //   38: aload_0
      //   39: getfield mRecyclerView : Lcom/android/internal/widget/RecyclerView;
      //   42: iconst_1
      //   43: invokevirtual canScrollVertically : (I)Z
      //   46: ifne -> 64
      //   49: aload_0
      //   50: getfield mRecyclerView : Lcom/android/internal/widget/RecyclerView;
      //   53: astore #4
      //   55: aload #4
      //   57: iconst_1
      //   58: invokevirtual canScrollHorizontally : (I)Z
      //   61: ifeq -> 76
      //   64: aload_3
      //   65: sipush #4096
      //   68: invokevirtual addAction : (I)V
      //   71: aload_3
      //   72: iconst_1
      //   73: invokevirtual setScrollable : (Z)V
      //   76: aload_0
      //   77: aload_1
      //   78: aload_2
      //   79: invokevirtual getRowCountForAccessibility : (Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;)I
      //   82: istore #5
      //   84: aload_0
      //   85: aload_1
      //   86: aload_2
      //   87: invokevirtual getColumnCountForAccessibility : (Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;)I
      //   90: istore #6
      //   92: aload_0
      //   93: aload_1
      //   94: aload_2
      //   95: invokevirtual isLayoutHierarchical : (Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;)Z
      //   98: istore #7
      //   100: aload_0
      //   101: aload_1
      //   102: aload_2
      //   103: invokevirtual getSelectionModeForAccessibility : (Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;)I
      //   106: istore #8
      //   108: iload #5
      //   110: iload #6
      //   112: iload #7
      //   114: iload #8
      //   116: invokestatic obtain : (IIZI)Landroid/view/accessibility/AccessibilityNodeInfo$CollectionInfo;
      //   119: astore_1
      //   120: aload_3
      //   121: aload_1
      //   122: invokevirtual setCollectionInfo : (Landroid/view/accessibility/AccessibilityNodeInfo$CollectionInfo;)V
      //   125: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #9363	-> 0
      //   #9364	-> 17
      //   #9365	-> 26
      //   #9366	-> 33
      //   #9368	-> 38
      //   #9369	-> 55
      //   #9370	-> 64
      //   #9371	-> 71
      //   #9373	-> 76
      //   #9375	-> 76
      //   #9376	-> 84
      //   #9377	-> 92
      //   #9378	-> 100
      //   #9375	-> 108
      //   #9379	-> 120
      //   #9380	-> 125
    }
    
    public void onInitializeAccessibilityEvent(AccessibilityEvent param1AccessibilityEvent) {
      onInitializeAccessibilityEvent(this.mRecyclerView.mRecycler, this.mRecyclerView.mState, param1AccessibilityEvent);
    }
    
    public void onInitializeAccessibilityEvent(RecyclerView.Recycler param1Recycler, RecyclerView.State param1State, AccessibilityEvent param1AccessibilityEvent) {
      RecyclerView recyclerView = this.mRecyclerView;
      if (recyclerView == null || param1AccessibilityEvent == null)
        return; 
      boolean bool = true;
      if (!recyclerView.canScrollVertically(1)) {
        recyclerView = this.mRecyclerView;
        if (!recyclerView.canScrollVertically(-1)) {
          recyclerView = this.mRecyclerView;
          if (!recyclerView.canScrollHorizontally(-1)) {
            recyclerView = this.mRecyclerView;
            if (!recyclerView.canScrollHorizontally(1))
              bool = false; 
          } 
        } 
      } 
      param1AccessibilityEvent.setScrollable(bool);
      if (this.mRecyclerView.mAdapter != null)
        param1AccessibilityEvent.setItemCount(this.mRecyclerView.mAdapter.getItemCount()); 
    }
    
    void onInitializeAccessibilityNodeInfoForItem(View param1View, AccessibilityNodeInfo param1AccessibilityNodeInfo) {
      RecyclerView.ViewHolder viewHolder = RecyclerView.getChildViewHolderInt(param1View);
      if (viewHolder != null && !viewHolder.isRemoved() && !this.mChildHelper.isHidden(viewHolder.itemView))
        onInitializeAccessibilityNodeInfoForItem(this.mRecyclerView.mRecycler, this.mRecyclerView.mState, param1View, param1AccessibilityNodeInfo); 
    }
    
    public void onInitializeAccessibilityNodeInfoForItem(RecyclerView.Recycler param1Recycler, RecyclerView.State param1State, View param1View, AccessibilityNodeInfo param1AccessibilityNodeInfo) {
      boolean bool1;
      boolean bool = canScrollVertically();
      int i = 0;
      if (bool) {
        bool1 = getPosition(param1View);
      } else {
        bool1 = false;
      } 
      if (canScrollHorizontally())
        i = getPosition(param1View); 
      AccessibilityNodeInfo.CollectionItemInfo collectionItemInfo = AccessibilityNodeInfo.CollectionItemInfo.obtain(bool1, 1, i, 1, false, false);
      param1AccessibilityNodeInfo.setCollectionItemInfo(collectionItemInfo);
    }
    
    public void requestSimpleAnimationsInNextLayout() {
      this.mRequestedSimpleAnimations = true;
    }
    
    public int getSelectionModeForAccessibility(RecyclerView.Recycler param1Recycler, RecyclerView.State param1State) {
      return 0;
    }
    
    public int getRowCountForAccessibility(RecyclerView.Recycler param1Recycler, RecyclerView.State param1State) {
      RecyclerView recyclerView = this.mRecyclerView;
      int i = 1;
      if (recyclerView == null || recyclerView.mAdapter == null)
        return 1; 
      if (canScrollVertically())
        i = this.mRecyclerView.mAdapter.getItemCount(); 
      return i;
    }
    
    public int getColumnCountForAccessibility(RecyclerView.Recycler param1Recycler, RecyclerView.State param1State) {
      RecyclerView recyclerView = this.mRecyclerView;
      int i = 1;
      if (recyclerView == null || recyclerView.mAdapter == null)
        return 1; 
      if (canScrollHorizontally())
        i = this.mRecyclerView.mAdapter.getItemCount(); 
      return i;
    }
    
    public boolean isLayoutHierarchical(RecyclerView.Recycler param1Recycler, RecyclerView.State param1State) {
      return false;
    }
    
    boolean performAccessibilityAction(int param1Int, Bundle param1Bundle) {
      return performAccessibilityAction(this.mRecyclerView.mRecycler, this.mRecyclerView.mState, param1Int, param1Bundle);
    }
    
    public boolean performAccessibilityAction(RecyclerView.Recycler param1Recycler, RecyclerView.State param1State, int param1Int, Bundle param1Bundle) {
      RecyclerView recyclerView = this.mRecyclerView;
      if (recyclerView == null)
        return false; 
      boolean bool = false;
      int i = 0;
      byte b = 0;
      int j = 0;
      if (param1Int != 4096) {
        if (param1Int != 8192) {
          param1Int = i;
        } else {
          i = b;
          if (recyclerView.canScrollVertically(-1))
            i = -(getHeight() - getPaddingTop() - getPaddingBottom()); 
          param1Int = i;
          if (this.mRecyclerView.canScrollHorizontally(-1)) {
            j = -(getWidth() - getPaddingLeft() - getPaddingRight());
            param1Int = i;
          } 
        } 
      } else {
        i = bool;
        if (recyclerView.canScrollVertically(1))
          i = getHeight() - getPaddingTop() - getPaddingBottom(); 
        param1Int = i;
        if (this.mRecyclerView.canScrollHorizontally(1)) {
          j = getWidth() - getPaddingLeft() - getPaddingRight();
          param1Int = i;
        } 
      } 
      if (param1Int == 0 && j == 0)
        return false; 
      this.mRecyclerView.smoothScrollBy(j, param1Int);
      return true;
    }
    
    boolean performAccessibilityActionForItem(View param1View, int param1Int, Bundle param1Bundle) {
      return performAccessibilityActionForItem(this.mRecyclerView.mRecycler, this.mRecyclerView.mState, param1View, param1Int, param1Bundle);
    }
    
    public boolean performAccessibilityActionForItem(RecyclerView.Recycler param1Recycler, RecyclerView.State param1State, View param1View, int param1Int, Bundle param1Bundle) {
      return false;
    }
    
    public static Properties getProperties(Context param1Context, AttributeSet param1AttributeSet, int param1Int1, int param1Int2) {
      Properties properties = new Properties();
      TypedArray typedArray = param1Context.obtainStyledAttributes(param1AttributeSet, R.styleable.RecyclerView, param1Int1, param1Int2);
      properties.orientation = typedArray.getInt(0, 1);
      properties.spanCount = typedArray.getInt(4, 1);
      properties.reverseLayout = typedArray.getBoolean(3, false);
      properties.stackFromEnd = typedArray.getBoolean(5, false);
      typedArray.recycle();
      return properties;
    }
    
    void setExactMeasureSpecsFrom(RecyclerView param1RecyclerView) {
      int i = View.MeasureSpec.makeMeasureSpec(param1RecyclerView.getWidth(), 1073741824);
      int j = View.MeasureSpec.makeMeasureSpec(param1RecyclerView.getHeight(), 1073741824);
      setMeasureSpecs(i, j);
    }
    
    boolean shouldMeasureTwice() {
      return false;
    }
    
    boolean hasFlexibleChildInBothOrientations() {
      int i = getChildCount();
      for (byte b = 0; b < i; b++) {
        View view = getChildAt(b);
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams.width < 0 && layoutParams.height < 0)
          return true; 
      } 
      return false;
    }
    
    public abstract RecyclerView.LayoutParams generateDefaultLayoutParams();
    
    public static interface LayoutPrefetchRegistry {
      void addPosition(int param2Int1, int param2Int2);
    }
    
    public static class Properties {
      public int orientation;
      
      public boolean reverseLayout;
      
      public int spanCount;
      
      public boolean stackFromEnd;
    }
  }
  
  class ItemDecoration {
    public void onDraw(Canvas param1Canvas, RecyclerView param1RecyclerView, RecyclerView.State param1State) {
      onDraw(param1Canvas, param1RecyclerView);
    }
    
    @Deprecated
    public void onDraw(Canvas param1Canvas, RecyclerView param1RecyclerView) {}
    
    public void onDrawOver(Canvas param1Canvas, RecyclerView param1RecyclerView, RecyclerView.State param1State) {
      onDrawOver(param1Canvas, param1RecyclerView);
    }
    
    @Deprecated
    public void onDrawOver(Canvas param1Canvas, RecyclerView param1RecyclerView) {}
    
    @Deprecated
    public void getItemOffsets(Rect param1Rect, int param1Int, RecyclerView param1RecyclerView) {
      param1Rect.set(0, 0, 0, 0);
    }
    
    public void getItemOffsets(Rect param1Rect, View param1View, RecyclerView param1RecyclerView, RecyclerView.State param1State) {
      getItemOffsets(param1Rect, ((RecyclerView.LayoutParams)param1View.getLayoutParams()).getViewLayoutPosition(), param1RecyclerView);
    }
  }
  
  class SimpleOnItemTouchListener implements OnItemTouchListener {
    public boolean onInterceptTouchEvent(RecyclerView param1RecyclerView, MotionEvent param1MotionEvent) {
      return false;
    }
    
    public void onTouchEvent(RecyclerView param1RecyclerView, MotionEvent param1MotionEvent) {}
    
    public void onRequestDisallowInterceptTouchEvent(boolean param1Boolean) {}
  }
  
  class OnScrollListener {
    public void onScrollStateChanged(RecyclerView param1RecyclerView, int param1Int) {}
    
    public void onScrolled(RecyclerView param1RecyclerView, int param1Int1, int param1Int2) {}
  }
  
  class ViewHolder {
    int mPosition = -1;
    
    int mOldPosition = -1;
    
    long mItemId = -1L;
    
    int mItemViewType = -1;
    
    int mPreLayoutPosition = -1;
    
    ViewHolder mShadowedHolder = null;
    
    ViewHolder mShadowingHolder = null;
    
    private static final List<Object> FULLUPDATE_PAYLOADS = Collections.EMPTY_LIST;
    
    List<Object> mPayloads = null;
    
    List<Object> mUnmodifiedPayloads = null;
    
    private int mIsRecyclableCount = 0;
    
    private RecyclerView.Recycler mScrapContainer = null;
    
    private boolean mInChangeScrap = false;
    
    private int mWasImportantForAccessibilityBeforeHidden = 0;
    
    int mPendingAccessibilityState = -1;
    
    static final int FLAG_ADAPTER_FULLUPDATE = 1024;
    
    static final int FLAG_ADAPTER_POSITION_UNKNOWN = 512;
    
    static final int FLAG_APPEARED_IN_PRE_LAYOUT = 4096;
    
    static final int FLAG_BOUNCED_FROM_HIDDEN_LIST = 8192;
    
    static final int FLAG_BOUND = 1;
    
    static final int FLAG_IGNORE = 128;
    
    static final int FLAG_INVALID = 4;
    
    static final int FLAG_MOVED = 2048;
    
    static final int FLAG_NOT_RECYCLABLE = 16;
    
    static final int FLAG_REMOVED = 8;
    
    static final int FLAG_RETURNED_FROM_SCRAP = 32;
    
    static final int FLAG_TMP_DETACHED = 256;
    
    static final int FLAG_UPDATE = 2;
    
    static final int PENDING_ACCESSIBILITY_STATE_NOT_SET = -1;
    
    public final View itemView;
    
    private int mFlags;
    
    WeakReference<RecyclerView> mNestedRecyclerView;
    
    RecyclerView mOwnerRecyclerView;
    
    public ViewHolder(RecyclerView this$0) {
      if (this$0 != null) {
        this.itemView = (View)this$0;
        return;
      } 
      throw new IllegalArgumentException("itemView may not be null");
    }
    
    void flagRemovedAndOffsetPosition(int param1Int1, int param1Int2, boolean param1Boolean) {
      addFlags(8);
      offsetPosition(param1Int2, param1Boolean);
      this.mPosition = param1Int1;
    }
    
    void offsetPosition(int param1Int, boolean param1Boolean) {
      if (this.mOldPosition == -1)
        this.mOldPosition = this.mPosition; 
      if (this.mPreLayoutPosition == -1)
        this.mPreLayoutPosition = this.mPosition; 
      if (param1Boolean)
        this.mPreLayoutPosition += param1Int; 
      this.mPosition += param1Int;
      if (this.itemView.getLayoutParams() != null)
        ((RecyclerView.LayoutParams)this.itemView.getLayoutParams()).mInsetsDirty = true; 
    }
    
    void clearOldPosition() {
      this.mOldPosition = -1;
      this.mPreLayoutPosition = -1;
    }
    
    void saveOldPosition() {
      if (this.mOldPosition == -1)
        this.mOldPosition = this.mPosition; 
    }
    
    boolean shouldIgnore() {
      boolean bool;
      if ((this.mFlags & 0x80) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    @Deprecated
    public final int getPosition() {
      int i = this.mPreLayoutPosition, j = i;
      if (i == -1)
        j = this.mPosition; 
      return j;
    }
    
    public final int getLayoutPosition() {
      int i = this.mPreLayoutPosition, j = i;
      if (i == -1)
        j = this.mPosition; 
      return j;
    }
    
    public final int getAdapterPosition() {
      RecyclerView recyclerView = this.mOwnerRecyclerView;
      if (recyclerView == null)
        return -1; 
      return recyclerView.getAdapterPositionFor(this);
    }
    
    public final int getOldPosition() {
      return this.mOldPosition;
    }
    
    public final long getItemId() {
      return this.mItemId;
    }
    
    public final int getItemViewType() {
      return this.mItemViewType;
    }
    
    boolean isScrap() {
      boolean bool;
      if (this.mScrapContainer != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    void unScrap() {
      this.mScrapContainer.unscrapView(this);
    }
    
    boolean wasReturnedFromScrap() {
      boolean bool;
      if ((this.mFlags & 0x20) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    void clearReturnedFromScrapFlag() {
      this.mFlags &= 0xFFFFFFDF;
    }
    
    void clearTmpDetachFlag() {
      this.mFlags &= 0xFFFFFEFF;
    }
    
    void stopIgnoring() {
      this.mFlags &= 0xFFFFFF7F;
    }
    
    void setScrapContainer(RecyclerView.Recycler param1Recycler, boolean param1Boolean) {
      this.mScrapContainer = param1Recycler;
      this.mInChangeScrap = param1Boolean;
    }
    
    boolean isInvalid() {
      boolean bool;
      if ((this.mFlags & 0x4) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    boolean needsUpdate() {
      boolean bool;
      if ((this.mFlags & 0x2) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    boolean isBound() {
      int i = this.mFlags;
      boolean bool = true;
      if ((i & 0x1) == 0)
        bool = false; 
      return bool;
    }
    
    boolean isRemoved() {
      boolean bool;
      if ((this.mFlags & 0x8) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    boolean hasAnyOfTheFlags(int param1Int) {
      boolean bool;
      if ((this.mFlags & param1Int) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    boolean isTmpDetached() {
      boolean bool;
      if ((this.mFlags & 0x100) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    boolean isAdapterPositionUnknown() {
      return ((this.mFlags & 0x200) != 0 || isInvalid());
    }
    
    void setFlags(int param1Int1, int param1Int2) {
      this.mFlags = this.mFlags & (param1Int2 ^ 0xFFFFFFFF) | param1Int1 & param1Int2;
    }
    
    void addFlags(int param1Int) {
      this.mFlags |= param1Int;
    }
    
    void addChangePayload(Object param1Object) {
      if (param1Object == null) {
        addFlags(1024);
      } else if ((0x400 & this.mFlags) == 0) {
        createPayloadsIfNeeded();
        this.mPayloads.add(param1Object);
      } 
    }
    
    private void createPayloadsIfNeeded() {
      if (this.mPayloads == null) {
        ArrayList<Object> arrayList = new ArrayList();
        this.mUnmodifiedPayloads = Collections.unmodifiableList(arrayList);
      } 
    }
    
    void clearPayload() {
      List<Object> list = this.mPayloads;
      if (list != null)
        list.clear(); 
      this.mFlags &= 0xFFFFFBFF;
    }
    
    List<Object> getUnmodifiedPayloads() {
      if ((this.mFlags & 0x400) == 0) {
        List<Object> list = this.mPayloads;
        if (list == null || list.size() == 0)
          return FULLUPDATE_PAYLOADS; 
        return this.mUnmodifiedPayloads;
      } 
      return FULLUPDATE_PAYLOADS;
    }
    
    void resetInternal() {
      this.mFlags = 0;
      this.mPosition = -1;
      this.mOldPosition = -1;
      this.mItemId = -1L;
      this.mPreLayoutPosition = -1;
      this.mIsRecyclableCount = 0;
      this.mShadowedHolder = null;
      this.mShadowingHolder = null;
      clearPayload();
      this.mWasImportantForAccessibilityBeforeHidden = 0;
      this.mPendingAccessibilityState = -1;
      RecyclerView.clearNestedRecyclerViewIfNotNested(this);
    }
    
    private void onEnteredHiddenState(RecyclerView param1RecyclerView) {
      View view = this.itemView;
      this.mWasImportantForAccessibilityBeforeHidden = view.getImportantForAccessibility();
      param1RecyclerView.setChildImportantForAccessibilityInternal(this, 4);
    }
    
    private void onLeftHiddenState(RecyclerView param1RecyclerView) {
      param1RecyclerView.setChildImportantForAccessibilityInternal(this, this.mWasImportantForAccessibilityBeforeHidden);
      this.mWasImportantForAccessibilityBeforeHidden = 0;
    }
    
    public String toString() {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("ViewHolder{");
      stringBuilder1.append(Integer.toHexString(hashCode()));
      stringBuilder1.append(" position=");
      stringBuilder1.append(this.mPosition);
      stringBuilder1.append(" id=");
      stringBuilder1.append(this.mItemId);
      stringBuilder1.append(", oldPos=");
      stringBuilder1.append(this.mOldPosition);
      stringBuilder1.append(", pLpos:");
      stringBuilder1.append(this.mPreLayoutPosition);
      StringBuilder stringBuilder2 = new StringBuilder(stringBuilder1.toString());
      if (isScrap()) {
        String str;
        stringBuilder2.append(" scrap ");
        if (this.mInChangeScrap) {
          str = "[changeScrap]";
        } else {
          str = "[attachedScrap]";
        } 
        stringBuilder2.append(str);
      } 
      if (isInvalid())
        stringBuilder2.append(" invalid"); 
      if (!isBound())
        stringBuilder2.append(" unbound"); 
      if (needsUpdate())
        stringBuilder2.append(" update"); 
      if (isRemoved())
        stringBuilder2.append(" removed"); 
      if (shouldIgnore())
        stringBuilder2.append(" ignored"); 
      if (isTmpDetached())
        stringBuilder2.append(" tmpDetached"); 
      if (!isRecyclable()) {
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append(" not recyclable(");
        stringBuilder1.append(this.mIsRecyclableCount);
        stringBuilder1.append(")");
        stringBuilder2.append(stringBuilder1.toString());
      } 
      if (isAdapterPositionUnknown())
        stringBuilder2.append(" undefined adapter position"); 
      if (this.itemView.getParent() == null)
        stringBuilder2.append(" no parent"); 
      stringBuilder2.append("}");
      return stringBuilder2.toString();
    }
    
    public final void setIsRecyclable(boolean param1Boolean) {
      int i = this.mIsRecyclableCount;
      if (param1Boolean) {
        i--;
      } else {
        i++;
      } 
      this.mIsRecyclableCount = i;
      if (i < 0) {
        this.mIsRecyclableCount = 0;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("isRecyclable decremented below 0: unmatched pair of setIsRecyable() calls for ");
        stringBuilder.append(this);
        Log.e("View", stringBuilder.toString());
      } else if (!param1Boolean && i == 1) {
        this.mFlags |= 0x10;
      } else if (param1Boolean && this.mIsRecyclableCount == 0) {
        this.mFlags &= 0xFFFFFFEF;
      } 
    }
    
    public final boolean isRecyclable() {
      if ((this.mFlags & 0x10) == 0) {
        View view = this.itemView;
        if (!view.hasTransientState())
          return true; 
      } 
      return false;
    }
    
    private boolean shouldBeKeptAsChild() {
      boolean bool;
      if ((this.mFlags & 0x10) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    private boolean doesTransientStatePreventRecycling() {
      boolean bool;
      if ((this.mFlags & 0x10) == 0 && this.itemView.hasTransientState()) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    boolean isUpdated() {
      boolean bool;
      if ((this.mFlags & 0x2) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
  }
  
  boolean setChildImportantForAccessibilityInternal(ViewHolder paramViewHolder, int paramInt) {
    if (isComputingLayout()) {
      paramViewHolder.mPendingAccessibilityState = paramInt;
      this.mPendingAccessibilityImportanceChange.add(paramViewHolder);
      return false;
    } 
    paramViewHolder.itemView.setImportantForAccessibility(paramInt);
    return true;
  }
  
  void dispatchPendingImportantForAccessibilityChanges() {
    for (int i = this.mPendingAccessibilityImportanceChange.size() - 1; i >= 0; i--) {
      ViewHolder viewHolder = this.mPendingAccessibilityImportanceChange.get(i);
      if (viewHolder.itemView.getParent() == this && !viewHolder.shouldIgnore()) {
        int j = viewHolder.mPendingAccessibilityState;
        if (j != -1) {
          viewHolder.itemView.setImportantForAccessibility(j);
          viewHolder.mPendingAccessibilityState = -1;
        } 
      } 
    } 
    this.mPendingAccessibilityImportanceChange.clear();
  }
  
  int getAdapterPositionFor(ViewHolder paramViewHolder) {
    if (!paramViewHolder.hasAnyOfTheFlags(524))
      if (paramViewHolder.isBound())
        return this.mAdapterHelper.applyPendingUpdatesToPosition(paramViewHolder.mPosition);  
    return -1;
  }
  
  class LayoutParams extends ViewGroup.MarginLayoutParams {
    final Rect mDecorInsets = new Rect();
    
    boolean mInsetsDirty = true;
    
    boolean mPendingInvalidate = false;
    
    RecyclerView.ViewHolder mViewHolder;
    
    public LayoutParams(RecyclerView this$0, AttributeSet param1AttributeSet) {
      super((Context)this$0, param1AttributeSet);
    }
    
    public LayoutParams(RecyclerView this$0, int param1Int1) {
      super(this$0, param1Int1);
    }
    
    public LayoutParams(RecyclerView this$0) {
      super((ViewGroup.MarginLayoutParams)this$0);
    }
    
    public LayoutParams(RecyclerView this$0) {
      super((ViewGroup.LayoutParams)this$0);
    }
    
    public LayoutParams(RecyclerView this$0) {
      super((ViewGroup.LayoutParams)this$0);
    }
    
    public boolean viewNeedsUpdate() {
      return this.mViewHolder.needsUpdate();
    }
    
    public boolean isViewInvalid() {
      return this.mViewHolder.isInvalid();
    }
    
    public boolean isItemRemoved() {
      return this.mViewHolder.isRemoved();
    }
    
    public boolean isItemChanged() {
      return this.mViewHolder.isUpdated();
    }
    
    @Deprecated
    public int getViewPosition() {
      return this.mViewHolder.getPosition();
    }
    
    public int getViewLayoutPosition() {
      return this.mViewHolder.getLayoutPosition();
    }
    
    public int getViewAdapterPosition() {
      return this.mViewHolder.getAdapterPosition();
    }
  }
  
  class AdapterDataObserver {
    public void onChanged() {}
    
    public void onItemRangeChanged(int param1Int1, int param1Int2) {}
    
    public void onItemRangeChanged(int param1Int1, int param1Int2, Object param1Object) {
      onItemRangeChanged(param1Int1, param1Int2);
    }
    
    public void onItemRangeInserted(int param1Int1, int param1Int2) {}
    
    public void onItemRangeRemoved(int param1Int1, int param1Int2) {}
    
    public void onItemRangeMoved(int param1Int1, int param1Int2, int param1Int3) {}
  }
  
  class SmoothScroller {
    private RecyclerView.LayoutManager mLayoutManager;
    
    private boolean mPendingInitialRun;
    
    private RecyclerView mRecyclerView;
    
    private final Action mRecyclingAction;
    
    private boolean mRunning;
    
    private int mTargetPosition = -1;
    
    private View mTargetView;
    
    public SmoothScroller() {
      this.mRecyclingAction = new Action(0, 0);
    }
    
    void start(RecyclerView param1RecyclerView, RecyclerView.LayoutManager param1LayoutManager) {
      this.mRecyclerView = param1RecyclerView;
      this.mLayoutManager = param1LayoutManager;
      if (this.mTargetPosition != -1) {
        RecyclerView.State.access$1102(param1RecyclerView.mState, this.mTargetPosition);
        this.mRunning = true;
        this.mPendingInitialRun = true;
        this.mTargetView = findViewByPosition(getTargetPosition());
        onStart();
        this.mRecyclerView.mViewFlinger.postOnAnimation();
        return;
      } 
      throw new IllegalArgumentException("Invalid target position");
    }
    
    public void setTargetPosition(int param1Int) {
      this.mTargetPosition = param1Int;
    }
    
    public RecyclerView.LayoutManager getLayoutManager() {
      return this.mLayoutManager;
    }
    
    protected final void stop() {
      if (!this.mRunning)
        return; 
      onStop();
      RecyclerView.State.access$1102(this.mRecyclerView.mState, -1);
      this.mTargetView = null;
      this.mTargetPosition = -1;
      this.mPendingInitialRun = false;
      this.mRunning = false;
      this.mLayoutManager.onSmoothScrollerStopped(this);
      this.mLayoutManager = null;
      this.mRecyclerView = null;
    }
    
    public boolean isPendingInitialRun() {
      return this.mPendingInitialRun;
    }
    
    public boolean isRunning() {
      return this.mRunning;
    }
    
    public int getTargetPosition() {
      return this.mTargetPosition;
    }
    
    private void onAnimation(int param1Int1, int param1Int2) {
      RecyclerView recyclerView = this.mRecyclerView;
      if (!this.mRunning || this.mTargetPosition == -1 || recyclerView == null)
        stop(); 
      this.mPendingInitialRun = false;
      View view = this.mTargetView;
      if (view != null)
        if (getChildPosition(view) == this.mTargetPosition) {
          onTargetFound(this.mTargetView, recyclerView.mState, this.mRecyclingAction);
          this.mRecyclingAction.runIfNecessary(recyclerView);
          stop();
        } else {
          Log.e("RecyclerView", "Passed over target position while smooth scrolling.");
          this.mTargetView = null;
        }  
      if (this.mRunning) {
        onSeekTargetStep(param1Int1, param1Int2, recyclerView.mState, this.mRecyclingAction);
        boolean bool = this.mRecyclingAction.hasJumpTarget();
        this.mRecyclingAction.runIfNecessary(recyclerView);
        if (bool)
          if (this.mRunning) {
            this.mPendingInitialRun = true;
            recyclerView.mViewFlinger.postOnAnimation();
          } else {
            stop();
          }  
      } 
    }
    
    public int getChildPosition(View param1View) {
      return this.mRecyclerView.getChildLayoutPosition(param1View);
    }
    
    public int getChildCount() {
      return this.mRecyclerView.mLayout.getChildCount();
    }
    
    public View findViewByPosition(int param1Int) {
      return this.mRecyclerView.mLayout.findViewByPosition(param1Int);
    }
    
    @Deprecated
    public void instantScrollToPosition(int param1Int) {
      this.mRecyclerView.scrollToPosition(param1Int);
    }
    
    protected void onChildAttachedToWindow(View param1View) {
      if (getChildPosition(param1View) == getTargetPosition())
        this.mTargetView = param1View; 
    }
    
    protected void normalize(PointF param1PointF) {
      double d = Math.sqrt((param1PointF.x * param1PointF.x + param1PointF.y * param1PointF.y));
      param1PointF.x = (float)(param1PointF.x / d);
      param1PointF.y = (float)(param1PointF.y / d);
    }
    
    protected abstract void onSeekTargetStep(int param1Int1, int param1Int2, RecyclerView.State param1State, Action param1Action);
    
    protected abstract void onStart();
    
    protected abstract void onStop();
    
    protected abstract void onTargetFound(View param1View, RecyclerView.State param1State, Action param1Action);
    
    public static class Action {
      private int mJumpToPosition = -1;
      
      private boolean mChanged = false;
      
      private int mConsecutiveUpdates = 0;
      
      public static final int UNDEFINED_DURATION = -2147483648;
      
      private int mDuration;
      
      private int mDx;
      
      private int mDy;
      
      private Interpolator mInterpolator;
      
      public Action(int param2Int1, int param2Int2) {
        this(param2Int1, param2Int2, -2147483648, null);
      }
      
      public Action(int param2Int1, int param2Int2, int param2Int3) {
        this(param2Int1, param2Int2, param2Int3, null);
      }
      
      public Action(int param2Int1, int param2Int2, int param2Int3, Interpolator param2Interpolator) {
        this.mDx = param2Int1;
        this.mDy = param2Int2;
        this.mDuration = param2Int3;
        this.mInterpolator = param2Interpolator;
      }
      
      public void jumpTo(int param2Int) {
        this.mJumpToPosition = param2Int;
      }
      
      boolean hasJumpTarget() {
        boolean bool;
        if (this.mJumpToPosition >= 0) {
          bool = true;
        } else {
          bool = false;
        } 
        return bool;
      }
      
      void runIfNecessary(RecyclerView param2RecyclerView) {
        if (this.mJumpToPosition >= 0) {
          int i = this.mJumpToPosition;
          this.mJumpToPosition = -1;
          param2RecyclerView.jumpToPositionForSmoothScroller(i);
          this.mChanged = false;
          return;
        } 
        if (this.mChanged) {
          validate();
          if (this.mInterpolator == null) {
            if (this.mDuration == Integer.MIN_VALUE) {
              param2RecyclerView.mViewFlinger.smoothScrollBy(this.mDx, this.mDy);
            } else {
              param2RecyclerView.mViewFlinger.smoothScrollBy(this.mDx, this.mDy, this.mDuration);
            } 
          } else {
            param2RecyclerView.mViewFlinger.smoothScrollBy(this.mDx, this.mDy, this.mDuration, this.mInterpolator);
          } 
          int i = this.mConsecutiveUpdates + 1;
          if (i > 10)
            Log.e("RecyclerView", "Smooth Scroll action is being updated too frequently. Make sure you are not changing it unless necessary"); 
          this.mChanged = false;
        } else {
          this.mConsecutiveUpdates = 0;
        } 
      }
      
      private void validate() {
        if (this.mInterpolator == null || this.mDuration >= 1) {
          if (this.mDuration >= 1)
            return; 
          throw new IllegalStateException("Scroll duration must be a positive number");
        } 
        throw new IllegalStateException("If you provide an interpolator, you must set a positive duration");
      }
      
      public int getDx() {
        return this.mDx;
      }
      
      public void setDx(int param2Int) {
        this.mChanged = true;
        this.mDx = param2Int;
      }
      
      public int getDy() {
        return this.mDy;
      }
      
      public void setDy(int param2Int) {
        this.mChanged = true;
        this.mDy = param2Int;
      }
      
      public int getDuration() {
        return this.mDuration;
      }
      
      public void setDuration(int param2Int) {
        this.mChanged = true;
        this.mDuration = param2Int;
      }
      
      public Interpolator getInterpolator() {
        return this.mInterpolator;
      }
      
      public void setInterpolator(Interpolator param2Interpolator) {
        this.mChanged = true;
        this.mInterpolator = param2Interpolator;
      }
      
      public void update(int param2Int1, int param2Int2, int param2Int3, Interpolator param2Interpolator) {
        this.mDx = param2Int1;
        this.mDy = param2Int2;
        this.mDuration = param2Int3;
        this.mInterpolator = param2Interpolator;
        this.mChanged = true;
      }
    }
    
    public static interface ScrollVectorProvider {
      PointF computeScrollVectorForPosition(int param2Int);
    }
  }
  
  class AdapterDataObservable extends Observable<AdapterDataObserver> {
    public boolean hasObservers() {
      return this.mObservers.isEmpty() ^ true;
    }
    
    public void notifyChanged() {
      for (int i = this.mObservers.size() - 1; i >= 0; i--)
        ((RecyclerView.AdapterDataObserver)this.mObservers.get(i)).onChanged(); 
    }
    
    public void notifyItemRangeChanged(int param1Int1, int param1Int2) {
      notifyItemRangeChanged(param1Int1, param1Int2, (Object)null);
    }
    
    public void notifyItemRangeChanged(int param1Int1, int param1Int2, Object param1Object) {
      for (int i = this.mObservers.size() - 1; i >= 0; i--)
        ((RecyclerView.AdapterDataObserver)this.mObservers.get(i)).onItemRangeChanged(param1Int1, param1Int2, param1Object); 
    }
    
    public void notifyItemRangeInserted(int param1Int1, int param1Int2) {
      for (int i = this.mObservers.size() - 1; i >= 0; i--)
        ((RecyclerView.AdapterDataObserver)this.mObservers.get(i)).onItemRangeInserted(param1Int1, param1Int2); 
    }
    
    public void notifyItemRangeRemoved(int param1Int1, int param1Int2) {
      for (int i = this.mObservers.size() - 1; i >= 0; i--)
        ((RecyclerView.AdapterDataObserver)this.mObservers.get(i)).onItemRangeRemoved(param1Int1, param1Int2); 
    }
    
    public void notifyItemMoved(int param1Int1, int param1Int2) {
      for (int i = this.mObservers.size() - 1; i >= 0; i--)
        ((RecyclerView.AdapterDataObserver)this.mObservers.get(i)).onItemRangeMoved(param1Int1, param1Int2, 1); 
    }
  }
  
  class SavedState extends AbsSavedState {
    SavedState(RecyclerView this$0) {
      super((Parcel)this$0);
      this.mLayoutState = this$0.readParcelable(RecyclerView.LayoutManager.class.getClassLoader());
    }
    
    SavedState(RecyclerView this$0) {
      super((Parcelable)this$0);
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeParcelable(this.mLayoutState, 0);
    }
    
    void copyFrom(SavedState param1SavedState) {
      this.mLayoutState = param1SavedState.mLayoutState;
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = (Parcelable.Creator<SavedState>)new Object();
    
    Parcelable mLayoutState;
  }
  
  class State {
    static final int STEP_ANIMATIONS = 4;
    
    static final int STEP_LAYOUT = 2;
    
    static final int STEP_START = 1;
    
    private SparseArray<Object> mData;
    
    int mDeletedInvisibleItemCountSincePreviousLayout;
    
    long mFocusedItemId;
    
    int mFocusedItemPosition;
    
    int mFocusedSubChildId;
    
    boolean mInPreLayout;
    
    boolean mIsMeasuring;
    
    int mItemCount;
    
    int mLayoutStep;
    
    int mPreviousLayoutItemCount;
    
    boolean mRunPredictiveAnimations;
    
    boolean mRunSimpleAnimations;
    
    boolean mStructureChanged;
    
    private int mTargetPosition;
    
    boolean mTrackOldChangeHolders;
    
    public State() {
      this.mTargetPosition = -1;
      this.mPreviousLayoutItemCount = 0;
      this.mDeletedInvisibleItemCountSincePreviousLayout = 0;
      this.mLayoutStep = 1;
      this.mItemCount = 0;
      this.mStructureChanged = false;
      this.mInPreLayout = false;
      this.mTrackOldChangeHolders = false;
      this.mIsMeasuring = false;
      this.mRunSimpleAnimations = false;
      this.mRunPredictiveAnimations = false;
    }
    
    void assertLayoutStep(int param1Int) {
      if ((this.mLayoutStep & param1Int) != 0)
        return; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Layout state should be one of ");
      stringBuilder.append(Integer.toBinaryString(param1Int));
      stringBuilder.append(" but it is ");
      param1Int = this.mLayoutStep;
      stringBuilder.append(Integer.toBinaryString(param1Int));
      throw new IllegalStateException(stringBuilder.toString());
    }
    
    State reset() {
      this.mTargetPosition = -1;
      SparseArray<Object> sparseArray = this.mData;
      if (sparseArray != null)
        sparseArray.clear(); 
      this.mItemCount = 0;
      this.mStructureChanged = false;
      this.mIsMeasuring = false;
      return this;
    }
    
    void prepareForNestedPrefetch(RecyclerView.Adapter param1Adapter) {
      this.mLayoutStep = 1;
      this.mItemCount = param1Adapter.getItemCount();
      this.mStructureChanged = false;
      this.mInPreLayout = false;
      this.mTrackOldChangeHolders = false;
      this.mIsMeasuring = false;
    }
    
    public boolean isMeasuring() {
      return this.mIsMeasuring;
    }
    
    public boolean isPreLayout() {
      return this.mInPreLayout;
    }
    
    public boolean willRunPredictiveAnimations() {
      return this.mRunPredictiveAnimations;
    }
    
    public boolean willRunSimpleAnimations() {
      return this.mRunSimpleAnimations;
    }
    
    public void remove(int param1Int) {
      SparseArray<Object> sparseArray = this.mData;
      if (sparseArray == null)
        return; 
      sparseArray.remove(param1Int);
    }
    
    public <T> T get(int param1Int) {
      SparseArray<Object> sparseArray = this.mData;
      if (sparseArray == null)
        return null; 
      return (T)sparseArray.get(param1Int);
    }
    
    public void put(int param1Int, Object param1Object) {
      if (this.mData == null)
        this.mData = new SparseArray(); 
      this.mData.put(param1Int, param1Object);
    }
    
    public int getTargetScrollPosition() {
      return this.mTargetPosition;
    }
    
    public boolean hasTargetScrollPosition() {
      boolean bool;
      if (this.mTargetPosition != -1) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean didStructureChange() {
      return this.mStructureChanged;
    }
    
    public int getItemCount() {
      int i;
      if (this.mInPreLayout) {
        i = this.mPreviousLayoutItemCount - this.mDeletedInvisibleItemCountSincePreviousLayout;
      } else {
        i = this.mItemCount;
      } 
      return i;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("State{mTargetPosition=");
      stringBuilder.append(this.mTargetPosition);
      stringBuilder.append(", mData=");
      stringBuilder.append(this.mData);
      stringBuilder.append(", mItemCount=");
      stringBuilder.append(this.mItemCount);
      stringBuilder.append(", mPreviousLayoutItemCount=");
      stringBuilder.append(this.mPreviousLayoutItemCount);
      stringBuilder.append(", mDeletedInvisibleItemCountSincePreviousLayout=");
      stringBuilder.append(this.mDeletedInvisibleItemCountSincePreviousLayout);
      stringBuilder.append(", mStructureChanged=");
      stringBuilder.append(this.mStructureChanged);
      stringBuilder.append(", mInPreLayout=");
      stringBuilder.append(this.mInPreLayout);
      stringBuilder.append(", mRunSimpleAnimations=");
      stringBuilder.append(this.mRunSimpleAnimations);
      stringBuilder.append(", mRunPredictiveAnimations=");
      stringBuilder.append(this.mRunPredictiveAnimations);
      stringBuilder.append('}');
      return stringBuilder.toString();
    }
    
    @Retention(RetentionPolicy.SOURCE)
    static @interface LayoutState {}
  }
  
  class OnFlingListener {
    public abstract boolean onFling(int param1Int1, int param1Int2);
  }
  
  class ItemAnimatorRestoreListener implements ItemAnimator.ItemAnimatorListener {
    final RecyclerView this$0;
    
    public void onAnimationFinished(RecyclerView.ViewHolder param1ViewHolder) {
      param1ViewHolder.setIsRecyclable(true);
      if (param1ViewHolder.mShadowedHolder != null && param1ViewHolder.mShadowingHolder == null)
        param1ViewHolder.mShadowedHolder = null; 
      param1ViewHolder.mShadowingHolder = null;
      if (!param1ViewHolder.shouldBeKeptAsChild() && 
        !RecyclerView.this.removeAnimatingView(param1ViewHolder.itemView) && param1ViewHolder.isTmpDetached())
        RecyclerView.this.removeDetachedView(param1ViewHolder.itemView, false); 
    }
  }
  
  class ItemAnimator {
    private ItemAnimatorListener mListener = null;
    
    private ArrayList<ItemAnimatorFinishedListener> mFinishedListeners = new ArrayList<>();
    
    private long mAddDuration = 120L;
    
    private long mRemoveDuration = 120L;
    
    private long mMoveDuration = 250L;
    
    private long mChangeDuration = 250L;
    
    public static final int FLAG_APPEARED_IN_PRE_LAYOUT = 4096;
    
    public static final int FLAG_CHANGED = 2;
    
    public static final int FLAG_INVALIDATED = 4;
    
    public static final int FLAG_MOVED = 2048;
    
    public static final int FLAG_REMOVED = 8;
    
    public long getMoveDuration() {
      return this.mMoveDuration;
    }
    
    public void setMoveDuration(long param1Long) {
      this.mMoveDuration = param1Long;
    }
    
    public long getAddDuration() {
      return this.mAddDuration;
    }
    
    public void setAddDuration(long param1Long) {
      this.mAddDuration = param1Long;
    }
    
    public long getRemoveDuration() {
      return this.mRemoveDuration;
    }
    
    public void setRemoveDuration(long param1Long) {
      this.mRemoveDuration = param1Long;
    }
    
    public long getChangeDuration() {
      return this.mChangeDuration;
    }
    
    public void setChangeDuration(long param1Long) {
      this.mChangeDuration = param1Long;
    }
    
    void setListener(ItemAnimatorListener param1ItemAnimatorListener) {
      this.mListener = param1ItemAnimatorListener;
    }
    
    public ItemHolderInfo recordPreLayoutInformation(RecyclerView.State param1State, RecyclerView.ViewHolder param1ViewHolder, int param1Int, List<Object> param1List) {
      return obtainHolderInfo().setFrom(param1ViewHolder);
    }
    
    public ItemHolderInfo recordPostLayoutInformation(RecyclerView.State param1State, RecyclerView.ViewHolder param1ViewHolder) {
      return obtainHolderInfo().setFrom(param1ViewHolder);
    }
    
    static int buildAdapterChangeFlagsForAnimations(RecyclerView.ViewHolder param1ViewHolder) {
      int i = param1ViewHolder.mFlags & 0xE;
      if (param1ViewHolder.isInvalid())
        return 4; 
      int j = i;
      if ((i & 0x4) == 0) {
        int k = param1ViewHolder.getOldPosition();
        int m = param1ViewHolder.getAdapterPosition();
        j = i;
        if (k != -1) {
          j = i;
          if (m != -1) {
            j = i;
            if (k != m)
              j = i | 0x800; 
          } 
        } 
      } 
      return j;
    }
    
    public final void dispatchAnimationFinished(RecyclerView.ViewHolder param1ViewHolder) {
      onAnimationFinished(param1ViewHolder);
      ItemAnimatorListener itemAnimatorListener = this.mListener;
      if (itemAnimatorListener != null)
        itemAnimatorListener.onAnimationFinished(param1ViewHolder); 
    }
    
    public void onAnimationFinished(RecyclerView.ViewHolder param1ViewHolder) {}
    
    public final void dispatchAnimationStarted(RecyclerView.ViewHolder param1ViewHolder) {
      onAnimationStarted(param1ViewHolder);
    }
    
    public void onAnimationStarted(RecyclerView.ViewHolder param1ViewHolder) {}
    
    public final boolean isRunning(ItemAnimatorFinishedListener param1ItemAnimatorFinishedListener) {
      boolean bool = isRunning();
      if (param1ItemAnimatorFinishedListener != null)
        if (!bool) {
          param1ItemAnimatorFinishedListener.onAnimationsFinished();
        } else {
          this.mFinishedListeners.add(param1ItemAnimatorFinishedListener);
        }  
      return bool;
    }
    
    public boolean canReuseUpdatedViewHolder(RecyclerView.ViewHolder param1ViewHolder) {
      return true;
    }
    
    public boolean canReuseUpdatedViewHolder(RecyclerView.ViewHolder param1ViewHolder, List<Object> param1List) {
      return canReuseUpdatedViewHolder(param1ViewHolder);
    }
    
    public final void dispatchAnimationsFinished() {
      int i = this.mFinishedListeners.size();
      for (byte b = 0; b < i; b++)
        ((ItemAnimatorFinishedListener)this.mFinishedListeners.get(b)).onAnimationsFinished(); 
      this.mFinishedListeners.clear();
    }
    
    public ItemHolderInfo obtainHolderInfo() {
      return new ItemHolderInfo();
    }
    
    public abstract boolean animateAppearance(RecyclerView.ViewHolder param1ViewHolder, ItemHolderInfo param1ItemHolderInfo1, ItemHolderInfo param1ItemHolderInfo2);
    
    public abstract boolean animateChange(RecyclerView.ViewHolder param1ViewHolder1, RecyclerView.ViewHolder param1ViewHolder2, ItemHolderInfo param1ItemHolderInfo1, ItemHolderInfo param1ItemHolderInfo2);
    
    public abstract boolean animateDisappearance(RecyclerView.ViewHolder param1ViewHolder, ItemHolderInfo param1ItemHolderInfo1, ItemHolderInfo param1ItemHolderInfo2);
    
    public abstract boolean animatePersistence(RecyclerView.ViewHolder param1ViewHolder, ItemHolderInfo param1ItemHolderInfo1, ItemHolderInfo param1ItemHolderInfo2);
    
    public abstract void endAnimation(RecyclerView.ViewHolder param1ViewHolder);
    
    public abstract void endAnimations();
    
    public abstract boolean isRunning();
    
    public abstract void runPendingAnimations();
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface AdapterChanges {}
    
    public static interface ItemAnimatorFinishedListener {
      void onAnimationsFinished();
    }
    
    static interface ItemAnimatorListener {
      void onAnimationFinished(RecyclerView.ViewHolder param2ViewHolder);
    }
    
    public static class ItemHolderInfo {
      public int bottom;
      
      public int changeFlags;
      
      public int left;
      
      public int right;
      
      public int top;
      
      public ItemHolderInfo setFrom(RecyclerView.ViewHolder param2ViewHolder) {
        return setFrom(param2ViewHolder, 0);
      }
      
      public ItemHolderInfo setFrom(RecyclerView.ViewHolder param2ViewHolder, int param2Int) {
        View view = param2ViewHolder.itemView;
        this.left = view.getLeft();
        this.top = view.getTop();
        this.right = view.getRight();
        this.bottom = view.getBottom();
        return this;
      }
    }
  }
  
  protected int getChildDrawingOrder(int paramInt1, int paramInt2) {
    ChildDrawingOrderCallback childDrawingOrderCallback = this.mChildDrawingOrderCallback;
    if (childDrawingOrderCallback == null)
      return super.getChildDrawingOrder(paramInt1, paramInt2); 
    return childDrawingOrderCallback.onGetChildDrawingOrder(paramInt1, paramInt2);
  }
  
  class ChildDrawingOrderCallback {
    public abstract int onGetChildDrawingOrder(int param1Int1, int param1Int2);
  }
  
  class OnChildAttachStateChangeListener {
    public abstract void onChildViewAttachedToWindow(View param1View);
    
    public abstract void onChildViewDetachedFromWindow(View param1View);
  }
  
  class OnItemTouchListener {
    public abstract boolean onInterceptTouchEvent(RecyclerView param1RecyclerView, MotionEvent param1MotionEvent);
    
    public abstract void onRequestDisallowInterceptTouchEvent(boolean param1Boolean);
    
    public abstract void onTouchEvent(RecyclerView param1RecyclerView, MotionEvent param1MotionEvent);
  }
  
  class RecyclerListener {
    public abstract void onViewRecycled(RecyclerView.ViewHolder param1ViewHolder);
  }
}
