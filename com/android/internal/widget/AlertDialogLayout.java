package com.android.internal.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class AlertDialogLayout extends LinearLayout {
  public AlertDialogLayout(Context paramContext) {
    super(paramContext);
  }
  
  public AlertDialogLayout(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  public AlertDialogLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  public AlertDialogLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    if (!tryOnMeasure(paramInt1, paramInt2))
      super.onMeasure(paramInt1, paramInt2); 
  }
  
  private boolean tryOnMeasure(int paramInt1, int paramInt2) {
    View view1 = null;
    View view2 = null;
    View view3 = null;
    int i = getChildCount();
    int j;
    for (j = 0; j < i; j++) {
      View view = getChildAt(j);
      if (view.getVisibility() != 8) {
        int i8 = view.getId();
        switch (i8) {
          default:
            return false;
          case 16909558:
            view1 = view;
            break;
          case 16908864:
          case 16908901:
            if (view3 != null)
              return false; 
            view3 = view;
            break;
          case 16908809:
            view2 = view;
            break;
        } 
      } 
    } 
    int m = View.MeasureSpec.getMode(paramInt2);
    int n = View.MeasureSpec.getSize(paramInt2);
    int i1 = View.MeasureSpec.getMode(paramInt1);
    int i2 = 0;
    j = getPaddingTop() + getPaddingBottom();
    int i3 = j;
    if (view1 != null) {
      view1.measure(paramInt1, 0);
      i3 = j + view1.getMeasuredHeight();
      i2 = combineMeasuredStates(0, view1.getMeasuredState());
    } 
    j = 0;
    int i4 = 0;
    int k = i2, i5 = i3;
    if (view2 != null) {
      view2.measure(paramInt1, 0);
      j = resolveMinimumHeight(view2);
      i4 = view2.getMeasuredHeight() - j;
      i5 = i3 + j;
      k = combineMeasuredStates(i2, view2.getMeasuredState());
    } 
    int i6 = 0;
    if (view3 != null) {
      if (m == 0) {
        i3 = 0;
      } else {
        i3 = Math.max(0, n - i5);
        i3 = View.MeasureSpec.makeMeasureSpec(i3, m);
      } 
      view3.measure(paramInt1, i3);
      i6 = view3.getMeasuredHeight();
      i5 += i6;
      k = combineMeasuredStates(k, view3.getMeasuredState());
    } 
    int i7 = n - i5;
    n = i7;
    i2 = k;
    i3 = i5;
    if (view2 != null) {
      n = Math.min(i7, i4);
      i3 = i7;
      i2 = j;
      if (n > 0) {
        i3 = i7 - n;
        i2 = j + n;
      } 
      i2 = View.MeasureSpec.makeMeasureSpec(i2, 1073741824);
      view2.measure(paramInt1, i2);
      j = i5 - j + view2.getMeasuredHeight();
      i2 = combineMeasuredStates(k, view2.getMeasuredState());
      n = i3;
      i3 = j;
    } 
    i5 = n;
    k = i2;
    j = i3;
    if (view3 != null) {
      i5 = n;
      k = i2;
      j = i3;
      if (n > 0) {
        j = View.MeasureSpec.makeMeasureSpec(i6 + n, m);
        view3.measure(paramInt1, j);
        j = i3 - i6 + view3.getMeasuredHeight();
        k = combineMeasuredStates(i2, view3.getMeasuredState());
        i5 = n - n;
      } 
    } 
    i5 = 0;
    for (i2 = 0; i2 < i; i2++, i5 = i3) {
      View view = getChildAt(i2);
      i3 = i5;
      if (view.getVisibility() != 8)
        i3 = Math.max(i5, view.getMeasuredWidth()); 
    } 
    i2 = getPaddingLeft();
    i3 = getPaddingRight();
    paramInt1 = resolveSizeAndState(i5 + i2 + i3, paramInt1, k);
    j = resolveSizeAndState(j, paramInt2, 0);
    setMeasuredDimension(paramInt1, j);
    if (i1 != 1073741824)
      forceUniformWidth(i, paramInt2); 
    return true;
  }
  
  private void forceUniformWidth(int paramInt1, int paramInt2) {
    int i = getMeasuredWidth();
    int j = View.MeasureSpec.makeMeasureSpec(i, 1073741824);
    for (i = 0; i < paramInt1; i++) {
      View view = getChildAt(i);
      if (view.getVisibility() != 8) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)view.getLayoutParams();
        if (layoutParams.width == -1) {
          int k = layoutParams.height;
          layoutParams.height = view.getMeasuredHeight();
          measureChildWithMargins(view, j, 0, paramInt2, 0);
          layoutParams.height = k;
        } 
      } 
    } 
  }
  
  private int resolveMinimumHeight(View paramView) {
    int i = paramView.getMinimumHeight();
    if (i > 0)
      return i; 
    if (paramView instanceof ViewGroup) {
      ViewGroup viewGroup = (ViewGroup)paramView;
      if (viewGroup.getChildCount() == 1)
        return resolveMinimumHeight(viewGroup.getChildAt(0)); 
    } 
    return 0;
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    int i = this.mPaddingLeft;
    int j = paramInt3 - paramInt1;
    int k = this.mPaddingRight;
    int m = this.mPaddingRight;
    paramInt3 = getMeasuredHeight();
    int n = getChildCount();
    int i1 = getGravity();
    paramInt1 = i1 & 0x70;
    if (paramInt1 != 16) {
      if (paramInt1 != 80) {
        paramInt1 = this.mPaddingTop;
      } else {
        paramInt1 = this.mPaddingTop + paramInt4 - paramInt2 - paramInt3;
      } 
    } else {
      paramInt1 = this.mPaddingTop + (paramInt4 - paramInt2 - paramInt3) / 2;
    } 
    Drawable drawable = getDividerDrawable();
    if (drawable == null) {
      paramInt3 = 0;
    } else {
      paramInt3 = drawable.getIntrinsicHeight();
    } 
    for (paramInt4 = 0; paramInt4 < n; paramInt4++) {
      View view = getChildAt(paramInt4);
      if (view != null && view.getVisibility() != 8) {
        int i2 = view.getMeasuredWidth();
        int i3 = view.getMeasuredHeight();
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)view.getLayoutParams();
        paramInt2 = layoutParams.gravity;
        if (paramInt2 < 0)
          paramInt2 = i1 & 0x800007; 
        int i4 = getLayoutDirection();
        paramInt2 = Gravity.getAbsoluteGravity(paramInt2, i4);
        paramInt2 &= 0x7;
        if (paramInt2 != 1) {
          if (paramInt2 != 5) {
            paramInt2 = layoutParams.leftMargin + i;
          } else {
            paramInt2 = layoutParams.rightMargin;
            paramInt2 = j - k - i2 - paramInt2;
          } 
        } else {
          paramInt2 = (j - i - m - i2) / 2;
          i4 = layoutParams.leftMargin;
          int i5 = layoutParams.rightMargin;
          paramInt2 = paramInt2 + i + i4 - i5;
        } 
        i4 = paramInt1;
        if (hasDividerBeforeChildAt(paramInt4))
          i4 = paramInt1 + paramInt3; 
        paramInt1 = i4 + layoutParams.topMargin;
        setChildFrame(view, paramInt2, paramInt1, i2, i3);
        paramInt1 += i3 + layoutParams.bottomMargin;
      } 
    } 
  }
  
  private void setChildFrame(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    paramView.layout(paramInt1, paramInt2, paramInt1 + paramInt3, paramInt2 + paramInt4);
  }
}
