package com.android.internal.widget;

public class PasswordValidationError {
  public static final int CONTAINS_INVALID_CHARACTERS = 2;
  
  public static final int CONTAINS_SEQUENCE = 5;
  
  public static final int NOT_ENOUGH_DIGITS = 9;
  
  public static final int NOT_ENOUGH_LETTERS = 6;
  
  public static final int NOT_ENOUGH_LOWER_CASE = 8;
  
  public static final int NOT_ENOUGH_NON_DIGITS = 12;
  
  public static final int NOT_ENOUGH_NON_LETTER = 11;
  
  public static final int NOT_ENOUGH_SYMBOLS = 10;
  
  public static final int NOT_ENOUGH_UPPER_CASE = 7;
  
  public static final int RECENTLY_USED = 13;
  
  public static final int TOO_LONG = 4;
  
  public static final int TOO_SHORT = 3;
  
  public static final int WEAK_CREDENTIAL_TYPE = 1;
  
  public final int errorCode;
  
  public final int requirement;
  
  public PasswordValidationError(int paramInt) {
    this(paramInt, 0);
  }
  
  public PasswordValidationError(int paramInt1, int paramInt2) {
    this.errorCode = paramInt1;
    this.requirement = paramInt2;
  }
  
  public String toString() {
    String str;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(errorCodeToString(this.errorCode));
    if (this.requirement > 0) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("; required: ");
      stringBuilder1.append(this.requirement);
      str = stringBuilder1.toString();
    } else {
      str = "";
    } 
    stringBuilder.append(str);
    return stringBuilder.toString();
  }
  
  private static String errorCodeToString(int paramInt) {
    StringBuilder stringBuilder;
    switch (paramInt) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown error ");
        stringBuilder.append(paramInt);
        return stringBuilder.toString();
      case 13:
        return "Pin or password was recently used";
      case 12:
        return "Too few non-numeric characters";
      case 11:
        return "Too few non-letter characters";
      case 10:
        return "Too few symbols";
      case 9:
        return "Too few numeric characters";
      case 8:
        return "Too few lower case letters";
      case 7:
        return "Too few upper case letters";
      case 6:
        return "Too few letters";
      case 5:
        return "Sequence too long";
      case 4:
        return "Password too long";
      case 3:
        return "Password too short";
      case 2:
        return "Contains an invalid character";
      case 1:
        break;
    } 
    return "Weak credential type";
  }
}
