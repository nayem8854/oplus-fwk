package com.android.internal.backup;

import android.app.backup.IBackupManager;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;

public interface IObbBackupService extends IInterface {
  void backupObbs(String paramString, ParcelFileDescriptor paramParcelFileDescriptor, int paramInt, IBackupManager paramIBackupManager) throws RemoteException;
  
  void restoreObbFile(String paramString1, ParcelFileDescriptor paramParcelFileDescriptor, long paramLong1, int paramInt1, String paramString2, long paramLong2, long paramLong3, int paramInt2, IBackupManager paramIBackupManager) throws RemoteException;
  
  class Default implements IObbBackupService {
    public void backupObbs(String param1String, ParcelFileDescriptor param1ParcelFileDescriptor, int param1Int, IBackupManager param1IBackupManager) throws RemoteException {}
    
    public void restoreObbFile(String param1String1, ParcelFileDescriptor param1ParcelFileDescriptor, long param1Long1, int param1Int1, String param1String2, long param1Long2, long param1Long3, int param1Int2, IBackupManager param1IBackupManager) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IObbBackupService {
    private static final String DESCRIPTOR = "com.android.internal.backup.IObbBackupService";
    
    static final int TRANSACTION_backupObbs = 1;
    
    static final int TRANSACTION_restoreObbFile = 2;
    
    public Stub() {
      attachInterface(this, "com.android.internal.backup.IObbBackupService");
    }
    
    public static IObbBackupService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.backup.IObbBackupService");
      if (iInterface != null && iInterface instanceof IObbBackupService)
        return (IObbBackupService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "restoreObbFile";
      } 
      return "backupObbs";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("com.android.internal.backup.IObbBackupService");
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.internal.backup.IObbBackupService");
        String str1 = param1Parcel1.readString();
        if (param1Parcel1.readInt() != 0) {
          ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel2 = null;
        } 
        long l1 = param1Parcel1.readLong();
        param1Int1 = param1Parcel1.readInt();
        String str2 = param1Parcel1.readString();
        long l2 = param1Parcel1.readLong();
        long l3 = param1Parcel1.readLong();
        param1Int2 = param1Parcel1.readInt();
        iBackupManager = IBackupManager.Stub.asInterface(param1Parcel1.readStrongBinder());
        restoreObbFile(str1, (ParcelFileDescriptor)param1Parcel2, l1, param1Int1, str2, l2, l3, param1Int2, iBackupManager);
        return true;
      } 
      iBackupManager.enforceInterface("com.android.internal.backup.IObbBackupService");
      String str = iBackupManager.readString();
      if (iBackupManager.readInt() != 0) {
        ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel((Parcel)iBackupManager);
      } else {
        param1Parcel2 = null;
      } 
      param1Int1 = iBackupManager.readInt();
      IBackupManager iBackupManager = IBackupManager.Stub.asInterface(iBackupManager.readStrongBinder());
      backupObbs(str, (ParcelFileDescriptor)param1Parcel2, param1Int1, iBackupManager);
      return true;
    }
    
    private static class Proxy implements IObbBackupService {
      public static IObbBackupService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.backup.IObbBackupService";
      }
      
      public void backupObbs(String param2String, ParcelFileDescriptor param2ParcelFileDescriptor, int param2Int, IBackupManager param2IBackupManager) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.backup.IObbBackupService");
          parcel.writeString(param2String);
          if (param2ParcelFileDescriptor != null) {
            parcel.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          if (param2IBackupManager != null) {
            iBinder = param2IBackupManager.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IObbBackupService.Stub.getDefaultImpl() != null) {
            IObbBackupService.Stub.getDefaultImpl().backupObbs(param2String, param2ParcelFileDescriptor, param2Int, param2IBackupManager);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void restoreObbFile(String param2String1, ParcelFileDescriptor param2ParcelFileDescriptor, long param2Long1, int param2Int1, String param2String2, long param2Long2, long param2Long3, int param2Int2, IBackupManager param2IBackupManager) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.backup.IObbBackupService");
          parcel.writeString(param2String1);
          if (param2ParcelFileDescriptor != null) {
            parcel.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeLong(param2Long1);
          parcel.writeInt(param2Int1);
          parcel.writeString(param2String2);
          parcel.writeLong(param2Long2);
          parcel.writeLong(param2Long3);
          parcel.writeInt(param2Int2);
          if (param2IBackupManager != null) {
            iBinder = param2IBackupManager.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IObbBackupService.Stub.getDefaultImpl() != null) {
            IObbBackupService.Stub.getDefaultImpl().restoreObbFile(param2String1, param2ParcelFileDescriptor, param2Long1, param2Int1, param2String2, param2Long2, param2Long3, param2Int2, param2IBackupManager);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IObbBackupService param1IObbBackupService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IObbBackupService != null) {
          Proxy.sDefaultImpl = param1IObbBackupService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IObbBackupService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
