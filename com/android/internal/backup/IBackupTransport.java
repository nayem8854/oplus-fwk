package com.android.internal.backup;

import android.app.backup.RestoreDescription;
import android.app.backup.RestoreSet;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.os.RemoteException;
import android.text.TextUtils;

public interface IBackupTransport extends IInterface {
  int abortFullRestore() throws RemoteException;
  
  void cancelFullBackup() throws RemoteException;
  
  int checkFullBackupSize(long paramLong) throws RemoteException;
  
  int clearBackupData(PackageInfo paramPackageInfo) throws RemoteException;
  
  Intent configurationIntent() throws RemoteException;
  
  String currentDestinationString() throws RemoteException;
  
  Intent dataManagementIntent() throws RemoteException;
  
  CharSequence dataManagementIntentLabel() throws RemoteException;
  
  int finishBackup() throws RemoteException;
  
  void finishRestore() throws RemoteException;
  
  RestoreSet[] getAvailableRestoreSets() throws RemoteException;
  
  long getBackupQuota(String paramString, boolean paramBoolean) throws RemoteException;
  
  long getCurrentRestoreSet() throws RemoteException;
  
  int getNextFullRestoreDataChunk(ParcelFileDescriptor paramParcelFileDescriptor) throws RemoteException;
  
  int getRestoreData(ParcelFileDescriptor paramParcelFileDescriptor) throws RemoteException;
  
  int getTransportFlags() throws RemoteException;
  
  int initializeDevice() throws RemoteException;
  
  boolean isAppEligibleForBackup(PackageInfo paramPackageInfo, boolean paramBoolean) throws RemoteException;
  
  String name() throws RemoteException;
  
  RestoreDescription nextRestorePackage() throws RemoteException;
  
  int performBackup(PackageInfo paramPackageInfo, ParcelFileDescriptor paramParcelFileDescriptor, int paramInt) throws RemoteException;
  
  int performFullBackup(PackageInfo paramPackageInfo, ParcelFileDescriptor paramParcelFileDescriptor, int paramInt) throws RemoteException;
  
  long requestBackupTime() throws RemoteException;
  
  long requestFullBackupTime() throws RemoteException;
  
  int sendBackupData(int paramInt) throws RemoteException;
  
  int startRestore(long paramLong, PackageInfo[] paramArrayOfPackageInfo) throws RemoteException;
  
  String transportDirName() throws RemoteException;
  
  class Default implements IBackupTransport {
    public String name() throws RemoteException {
      return null;
    }
    
    public Intent configurationIntent() throws RemoteException {
      return null;
    }
    
    public String currentDestinationString() throws RemoteException {
      return null;
    }
    
    public Intent dataManagementIntent() throws RemoteException {
      return null;
    }
    
    public CharSequence dataManagementIntentLabel() throws RemoteException {
      return null;
    }
    
    public String transportDirName() throws RemoteException {
      return null;
    }
    
    public long requestBackupTime() throws RemoteException {
      return 0L;
    }
    
    public int initializeDevice() throws RemoteException {
      return 0;
    }
    
    public int performBackup(PackageInfo param1PackageInfo, ParcelFileDescriptor param1ParcelFileDescriptor, int param1Int) throws RemoteException {
      return 0;
    }
    
    public int clearBackupData(PackageInfo param1PackageInfo) throws RemoteException {
      return 0;
    }
    
    public int finishBackup() throws RemoteException {
      return 0;
    }
    
    public RestoreSet[] getAvailableRestoreSets() throws RemoteException {
      return null;
    }
    
    public long getCurrentRestoreSet() throws RemoteException {
      return 0L;
    }
    
    public int startRestore(long param1Long, PackageInfo[] param1ArrayOfPackageInfo) throws RemoteException {
      return 0;
    }
    
    public RestoreDescription nextRestorePackage() throws RemoteException {
      return null;
    }
    
    public int getRestoreData(ParcelFileDescriptor param1ParcelFileDescriptor) throws RemoteException {
      return 0;
    }
    
    public void finishRestore() throws RemoteException {}
    
    public long requestFullBackupTime() throws RemoteException {
      return 0L;
    }
    
    public int performFullBackup(PackageInfo param1PackageInfo, ParcelFileDescriptor param1ParcelFileDescriptor, int param1Int) throws RemoteException {
      return 0;
    }
    
    public int checkFullBackupSize(long param1Long) throws RemoteException {
      return 0;
    }
    
    public int sendBackupData(int param1Int) throws RemoteException {
      return 0;
    }
    
    public void cancelFullBackup() throws RemoteException {}
    
    public boolean isAppEligibleForBackup(PackageInfo param1PackageInfo, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public long getBackupQuota(String param1String, boolean param1Boolean) throws RemoteException {
      return 0L;
    }
    
    public int getNextFullRestoreDataChunk(ParcelFileDescriptor param1ParcelFileDescriptor) throws RemoteException {
      return 0;
    }
    
    public int abortFullRestore() throws RemoteException {
      return 0;
    }
    
    public int getTransportFlags() throws RemoteException {
      return 0;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IBackupTransport {
    private static final String DESCRIPTOR = "com.android.internal.backup.IBackupTransport";
    
    static final int TRANSACTION_abortFullRestore = 26;
    
    static final int TRANSACTION_cancelFullBackup = 22;
    
    static final int TRANSACTION_checkFullBackupSize = 20;
    
    static final int TRANSACTION_clearBackupData = 10;
    
    static final int TRANSACTION_configurationIntent = 2;
    
    static final int TRANSACTION_currentDestinationString = 3;
    
    static final int TRANSACTION_dataManagementIntent = 4;
    
    static final int TRANSACTION_dataManagementIntentLabel = 5;
    
    static final int TRANSACTION_finishBackup = 11;
    
    static final int TRANSACTION_finishRestore = 17;
    
    static final int TRANSACTION_getAvailableRestoreSets = 12;
    
    static final int TRANSACTION_getBackupQuota = 24;
    
    static final int TRANSACTION_getCurrentRestoreSet = 13;
    
    static final int TRANSACTION_getNextFullRestoreDataChunk = 25;
    
    static final int TRANSACTION_getRestoreData = 16;
    
    static final int TRANSACTION_getTransportFlags = 27;
    
    static final int TRANSACTION_initializeDevice = 8;
    
    static final int TRANSACTION_isAppEligibleForBackup = 23;
    
    static final int TRANSACTION_name = 1;
    
    static final int TRANSACTION_nextRestorePackage = 15;
    
    static final int TRANSACTION_performBackup = 9;
    
    static final int TRANSACTION_performFullBackup = 19;
    
    static final int TRANSACTION_requestBackupTime = 7;
    
    static final int TRANSACTION_requestFullBackupTime = 18;
    
    static final int TRANSACTION_sendBackupData = 21;
    
    static final int TRANSACTION_startRestore = 14;
    
    static final int TRANSACTION_transportDirName = 6;
    
    public Stub() {
      attachInterface(this, "com.android.internal.backup.IBackupTransport");
    }
    
    public static IBackupTransport asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.backup.IBackupTransport");
      if (iInterface != null && iInterface instanceof IBackupTransport)
        return (IBackupTransport)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 27:
          return "getTransportFlags";
        case 26:
          return "abortFullRestore";
        case 25:
          return "getNextFullRestoreDataChunk";
        case 24:
          return "getBackupQuota";
        case 23:
          return "isAppEligibleForBackup";
        case 22:
          return "cancelFullBackup";
        case 21:
          return "sendBackupData";
        case 20:
          return "checkFullBackupSize";
        case 19:
          return "performFullBackup";
        case 18:
          return "requestFullBackupTime";
        case 17:
          return "finishRestore";
        case 16:
          return "getRestoreData";
        case 15:
          return "nextRestorePackage";
        case 14:
          return "startRestore";
        case 13:
          return "getCurrentRestoreSet";
        case 12:
          return "getAvailableRestoreSets";
        case 11:
          return "finishBackup";
        case 10:
          return "clearBackupData";
        case 9:
          return "performBackup";
        case 8:
          return "initializeDevice";
        case 7:
          return "requestBackupTime";
        case 6:
          return "transportDirName";
        case 5:
          return "dataManagementIntentLabel";
        case 4:
          return "dataManagementIntent";
        case 3:
          return "currentDestinationString";
        case 2:
          return "configurationIntent";
        case 1:
          break;
      } 
      return "name";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        int i;
        RestoreDescription restoreDescription;
        PackageInfo[] arrayOfPackageInfo;
        RestoreSet[] arrayOfRestoreSet;
        String str3;
        CharSequence charSequence;
        Intent intent2;
        String str2;
        Intent intent1;
        String str4;
        long l;
        ParcelFileDescriptor parcelFileDescriptor;
        boolean bool1 = false, bool2 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 27:
            param1Parcel1.enforceInterface("com.android.internal.backup.IBackupTransport");
            param1Int1 = getTransportFlags();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 26:
            param1Parcel1.enforceInterface("com.android.internal.backup.IBackupTransport");
            param1Int1 = abortFullRestore();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 25:
            param1Parcel1.enforceInterface("com.android.internal.backup.IBackupTransport");
            if (param1Parcel1.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor1 = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            param1Int1 = getNextFullRestoreDataChunk((ParcelFileDescriptor)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 24:
            param1Parcel1.enforceInterface("com.android.internal.backup.IBackupTransport");
            str4 = param1Parcel1.readString();
            bool1 = bool2;
            if (param1Parcel1.readInt() != 0)
              bool1 = true; 
            l = getBackupQuota(str4, bool1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 23:
            param1Parcel1.enforceInterface("com.android.internal.backup.IBackupTransport");
            if (param1Parcel1.readInt() != 0) {
              PackageInfo packageInfo = (PackageInfo)PackageInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str4 = null;
            } 
            if (param1Parcel1.readInt() != 0)
              bool1 = true; 
            bool = isAppEligibleForBackup((PackageInfo)str4, bool1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 22:
            param1Parcel1.enforceInterface("com.android.internal.backup.IBackupTransport");
            cancelFullBackup();
            param1Parcel2.writeNoException();
            return true;
          case 21:
            param1Parcel1.enforceInterface("com.android.internal.backup.IBackupTransport");
            i = param1Parcel1.readInt();
            i = sendBackupData(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 20:
            param1Parcel1.enforceInterface("com.android.internal.backup.IBackupTransport");
            l = param1Parcel1.readLong();
            i = checkFullBackupSize(l);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 19:
            param1Parcel1.enforceInterface("com.android.internal.backup.IBackupTransport");
            if (param1Parcel1.readInt() != 0) {
              PackageInfo packageInfo = (PackageInfo)PackageInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str4 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(param1Parcel1);
            } else {
              parcelFileDescriptor = null;
            } 
            i = param1Parcel1.readInt();
            i = performFullBackup((PackageInfo)str4, parcelFileDescriptor, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 18:
            param1Parcel1.enforceInterface("com.android.internal.backup.IBackupTransport");
            l = requestFullBackupTime();
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 17:
            param1Parcel1.enforceInterface("com.android.internal.backup.IBackupTransport");
            finishRestore();
            param1Parcel2.writeNoException();
            return true;
          case 16:
            param1Parcel1.enforceInterface("com.android.internal.backup.IBackupTransport");
            if (param1Parcel1.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor1 = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            i = getRestoreData((ParcelFileDescriptor)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 15:
            param1Parcel1.enforceInterface("com.android.internal.backup.IBackupTransport");
            restoreDescription = nextRestorePackage();
            param1Parcel2.writeNoException();
            if (restoreDescription != null) {
              param1Parcel2.writeInt(1);
              restoreDescription.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 14:
            restoreDescription.enforceInterface("com.android.internal.backup.IBackupTransport");
            l = restoreDescription.readLong();
            arrayOfPackageInfo = (PackageInfo[])restoreDescription.createTypedArray(PackageInfo.CREATOR);
            i = startRestore(l, arrayOfPackageInfo);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 13:
            arrayOfPackageInfo.enforceInterface("com.android.internal.backup.IBackupTransport");
            l = getCurrentRestoreSet();
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 12:
            arrayOfPackageInfo.enforceInterface("com.android.internal.backup.IBackupTransport");
            arrayOfRestoreSet = getAvailableRestoreSets();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray((Parcelable[])arrayOfRestoreSet, 1);
            return true;
          case 11:
            arrayOfRestoreSet.enforceInterface("com.android.internal.backup.IBackupTransport");
            i = finishBackup();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 10:
            arrayOfRestoreSet.enforceInterface("com.android.internal.backup.IBackupTransport");
            if (arrayOfRestoreSet.readInt() != 0) {
              PackageInfo packageInfo = (PackageInfo)PackageInfo.CREATOR.createFromParcel((Parcel)arrayOfRestoreSet);
            } else {
              arrayOfRestoreSet = null;
            } 
            i = clearBackupData((PackageInfo)arrayOfRestoreSet);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 9:
            arrayOfRestoreSet.enforceInterface("com.android.internal.backup.IBackupTransport");
            if (arrayOfRestoreSet.readInt() != 0) {
              PackageInfo packageInfo = (PackageInfo)PackageInfo.CREATOR.createFromParcel((Parcel)arrayOfRestoreSet);
            } else {
              str4 = null;
            } 
            if (arrayOfRestoreSet.readInt() != 0) {
              parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel((Parcel)arrayOfRestoreSet);
            } else {
              parcelFileDescriptor = null;
            } 
            i = arrayOfRestoreSet.readInt();
            i = performBackup((PackageInfo)str4, parcelFileDescriptor, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 8:
            arrayOfRestoreSet.enforceInterface("com.android.internal.backup.IBackupTransport");
            i = initializeDevice();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 7:
            arrayOfRestoreSet.enforceInterface("com.android.internal.backup.IBackupTransport");
            l = requestBackupTime();
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 6:
            arrayOfRestoreSet.enforceInterface("com.android.internal.backup.IBackupTransport");
            str3 = transportDirName();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str3);
            return true;
          case 5:
            str3.enforceInterface("com.android.internal.backup.IBackupTransport");
            charSequence = dataManagementIntentLabel();
            param1Parcel2.writeNoException();
            if (charSequence != null) {
              param1Parcel2.writeInt(1);
              TextUtils.writeToParcel(charSequence, param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 4:
            charSequence.enforceInterface("com.android.internal.backup.IBackupTransport");
            intent2 = dataManagementIntent();
            param1Parcel2.writeNoException();
            if (intent2 != null) {
              param1Parcel2.writeInt(1);
              intent2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 3:
            intent2.enforceInterface("com.android.internal.backup.IBackupTransport");
            str2 = currentDestinationString();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str2);
            return true;
          case 2:
            str2.enforceInterface("com.android.internal.backup.IBackupTransport");
            intent1 = configurationIntent();
            param1Parcel2.writeNoException();
            if (intent1 != null) {
              param1Parcel2.writeInt(1);
              intent1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 1:
            break;
        } 
        intent1.enforceInterface("com.android.internal.backup.IBackupTransport");
        String str1 = name();
        param1Parcel2.writeNoException();
        param1Parcel2.writeString(str1);
        return true;
      } 
      param1Parcel2.writeString("com.android.internal.backup.IBackupTransport");
      return true;
    }
    
    private static class Proxy implements IBackupTransport {
      public static IBackupTransport sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.backup.IBackupTransport";
      }
      
      public String name() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IBackupTransport.Stub.getDefaultImpl() != null)
            return IBackupTransport.Stub.getDefaultImpl().name(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Intent configurationIntent() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Intent intent;
          parcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IBackupTransport.Stub.getDefaultImpl() != null) {
            intent = IBackupTransport.Stub.getDefaultImpl().configurationIntent();
            return intent;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            intent = (Intent)Intent.CREATOR.createFromParcel(parcel2);
          } else {
            intent = null;
          } 
          return intent;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String currentDestinationString() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IBackupTransport.Stub.getDefaultImpl() != null)
            return IBackupTransport.Stub.getDefaultImpl().currentDestinationString(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Intent dataManagementIntent() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Intent intent;
          parcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IBackupTransport.Stub.getDefaultImpl() != null) {
            intent = IBackupTransport.Stub.getDefaultImpl().dataManagementIntent();
            return intent;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            intent = (Intent)Intent.CREATOR.createFromParcel(parcel2);
          } else {
            intent = null;
          } 
          return intent;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public CharSequence dataManagementIntentLabel() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          CharSequence charSequence;
          parcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IBackupTransport.Stub.getDefaultImpl() != null) {
            charSequence = IBackupTransport.Stub.getDefaultImpl().dataManagementIntentLabel();
            return charSequence;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            charSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel2);
          } else {
            charSequence = null;
          } 
          return charSequence;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String transportDirName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IBackupTransport.Stub.getDefaultImpl() != null)
            return IBackupTransport.Stub.getDefaultImpl().transportDirName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long requestBackupTime() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IBackupTransport.Stub.getDefaultImpl() != null)
            return IBackupTransport.Stub.getDefaultImpl().requestBackupTime(); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int initializeDevice() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IBackupTransport.Stub.getDefaultImpl() != null)
            return IBackupTransport.Stub.getDefaultImpl().initializeDevice(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int performBackup(PackageInfo param2PackageInfo, ParcelFileDescriptor param2ParcelFileDescriptor, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
          if (param2PackageInfo != null) {
            parcel1.writeInt(1);
            param2PackageInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2ParcelFileDescriptor != null) {
            parcel1.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IBackupTransport.Stub.getDefaultImpl() != null) {
            param2Int = IBackupTransport.Stub.getDefaultImpl().performBackup(param2PackageInfo, param2ParcelFileDescriptor, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int clearBackupData(PackageInfo param2PackageInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
          if (param2PackageInfo != null) {
            parcel1.writeInt(1);
            param2PackageInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IBackupTransport.Stub.getDefaultImpl() != null)
            return IBackupTransport.Stub.getDefaultImpl().clearBackupData(param2PackageInfo); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int finishBackup() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IBackupTransport.Stub.getDefaultImpl() != null)
            return IBackupTransport.Stub.getDefaultImpl().finishBackup(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public RestoreSet[] getAvailableRestoreSets() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IBackupTransport.Stub.getDefaultImpl() != null)
            return IBackupTransport.Stub.getDefaultImpl().getAvailableRestoreSets(); 
          parcel2.readException();
          return (RestoreSet[])parcel2.createTypedArray(RestoreSet.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getCurrentRestoreSet() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IBackupTransport.Stub.getDefaultImpl() != null)
            return IBackupTransport.Stub.getDefaultImpl().getCurrentRestoreSet(); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int startRestore(long param2Long, PackageInfo[] param2ArrayOfPackageInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
          parcel1.writeLong(param2Long);
          parcel1.writeTypedArray((Parcelable[])param2ArrayOfPackageInfo, 0);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IBackupTransport.Stub.getDefaultImpl() != null)
            return IBackupTransport.Stub.getDefaultImpl().startRestore(param2Long, param2ArrayOfPackageInfo); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public RestoreDescription nextRestorePackage() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          RestoreDescription restoreDescription;
          parcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IBackupTransport.Stub.getDefaultImpl() != null) {
            restoreDescription = IBackupTransport.Stub.getDefaultImpl().nextRestorePackage();
            return restoreDescription;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            restoreDescription = (RestoreDescription)RestoreDescription.CREATOR.createFromParcel(parcel2);
          } else {
            restoreDescription = null;
          } 
          return restoreDescription;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getRestoreData(ParcelFileDescriptor param2ParcelFileDescriptor) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
          if (param2ParcelFileDescriptor != null) {
            parcel1.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IBackupTransport.Stub.getDefaultImpl() != null)
            return IBackupTransport.Stub.getDefaultImpl().getRestoreData(param2ParcelFileDescriptor); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void finishRestore() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IBackupTransport.Stub.getDefaultImpl() != null) {
            IBackupTransport.Stub.getDefaultImpl().finishRestore();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long requestFullBackupTime() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IBackupTransport.Stub.getDefaultImpl() != null)
            return IBackupTransport.Stub.getDefaultImpl().requestFullBackupTime(); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int performFullBackup(PackageInfo param2PackageInfo, ParcelFileDescriptor param2ParcelFileDescriptor, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
          if (param2PackageInfo != null) {
            parcel1.writeInt(1);
            param2PackageInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2ParcelFileDescriptor != null) {
            parcel1.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IBackupTransport.Stub.getDefaultImpl() != null) {
            param2Int = IBackupTransport.Stub.getDefaultImpl().performFullBackup(param2PackageInfo, param2ParcelFileDescriptor, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int checkFullBackupSize(long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IBackupTransport.Stub.getDefaultImpl() != null)
            return IBackupTransport.Stub.getDefaultImpl().checkFullBackupSize(param2Long); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int sendBackupData(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IBackupTransport.Stub.getDefaultImpl() != null) {
            param2Int = IBackupTransport.Stub.getDefaultImpl().sendBackupData(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cancelFullBackup() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IBackupTransport.Stub.getDefaultImpl() != null) {
            IBackupTransport.Stub.getDefaultImpl().cancelFullBackup();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isAppEligibleForBackup(PackageInfo param2PackageInfo, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
          boolean bool = true;
          if (param2PackageInfo != null) {
            parcel1.writeInt(1);
            param2PackageInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool1 && IBackupTransport.Stub.getDefaultImpl() != null) {
            param2Boolean = IBackupTransport.Stub.getDefaultImpl().isAppEligibleForBackup(param2PackageInfo, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getBackupQuota(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool1 && IBackupTransport.Stub.getDefaultImpl() != null)
            return IBackupTransport.Stub.getDefaultImpl().getBackupQuota(param2String, param2Boolean); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getNextFullRestoreDataChunk(ParcelFileDescriptor param2ParcelFileDescriptor) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
          if (param2ParcelFileDescriptor != null) {
            parcel1.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IBackupTransport.Stub.getDefaultImpl() != null)
            return IBackupTransport.Stub.getDefaultImpl().getNextFullRestoreDataChunk(param2ParcelFileDescriptor); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int abortFullRestore() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IBackupTransport.Stub.getDefaultImpl() != null)
            return IBackupTransport.Stub.getDefaultImpl().abortFullRestore(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getTransportFlags() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.backup.IBackupTransport");
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && IBackupTransport.Stub.getDefaultImpl() != null)
            return IBackupTransport.Stub.getDefaultImpl().getTransportFlags(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IBackupTransport param1IBackupTransport) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IBackupTransport != null) {
          Proxy.sDefaultImpl = param1IBackupTransport;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IBackupTransport getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
