package com.android.internal.appwidget;

import android.app.IApplicationThread;
import android.app.IServiceConnection;
import android.appwidget.AppWidgetProviderInfo;
import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.ParceledListSlice;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.widget.RemoteViews;

public interface IAppWidgetService extends IInterface {
  int allocateAppWidgetId(String paramString, int paramInt) throws RemoteException;
  
  boolean bindAppWidgetId(String paramString, int paramInt1, int paramInt2, ComponentName paramComponentName, Bundle paramBundle) throws RemoteException;
  
  boolean bindRemoteViewsService(String paramString, int paramInt1, Intent paramIntent, IApplicationThread paramIApplicationThread, IBinder paramIBinder, IServiceConnection paramIServiceConnection, int paramInt2) throws RemoteException;
  
  IntentSender createAppWidgetConfigIntentSender(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void deleteAllHosts() throws RemoteException;
  
  void deleteAppWidgetId(String paramString, int paramInt) throws RemoteException;
  
  void deleteHost(String paramString, int paramInt) throws RemoteException;
  
  int[] getAppWidgetIds(ComponentName paramComponentName) throws RemoteException;
  
  int[] getAppWidgetIdsForHost(String paramString, int paramInt) throws RemoteException;
  
  AppWidgetProviderInfo getAppWidgetInfo(String paramString, int paramInt) throws RemoteException;
  
  Bundle getAppWidgetOptions(String paramString, int paramInt) throws RemoteException;
  
  RemoteViews getAppWidgetViews(String paramString, int paramInt) throws RemoteException;
  
  ParceledListSlice getInstalledProvidersForProfile(int paramInt1, int paramInt2, String paramString) throws RemoteException;
  
  boolean hasBindAppWidgetPermission(String paramString, int paramInt) throws RemoteException;
  
  boolean isBoundWidgetPackage(String paramString, int paramInt) throws RemoteException;
  
  boolean isRequestPinAppWidgetSupported() throws RemoteException;
  
  void noteAppWidgetTapped(String paramString, int paramInt) throws RemoteException;
  
  void notifyAppWidgetViewDataChanged(String paramString, int[] paramArrayOfint, int paramInt) throws RemoteException;
  
  void partiallyUpdateAppWidgetIds(String paramString, int[] paramArrayOfint, RemoteViews paramRemoteViews) throws RemoteException;
  
  boolean requestPinAppWidget(String paramString, ComponentName paramComponentName, Bundle paramBundle, IntentSender paramIntentSender) throws RemoteException;
  
  void setBindAppWidgetPermission(String paramString, int paramInt, boolean paramBoolean) throws RemoteException;
  
  ParceledListSlice startListening(IAppWidgetHost paramIAppWidgetHost, String paramString, int paramInt, int[] paramArrayOfint) throws RemoteException;
  
  void stopListening(String paramString, int paramInt) throws RemoteException;
  
  void updateAppWidgetIds(String paramString, int[] paramArrayOfint, RemoteViews paramRemoteViews) throws RemoteException;
  
  void updateAppWidgetOptions(String paramString, int paramInt, Bundle paramBundle) throws RemoteException;
  
  void updateAppWidgetProvider(ComponentName paramComponentName, RemoteViews paramRemoteViews) throws RemoteException;
  
  void updateAppWidgetProviderInfo(ComponentName paramComponentName, String paramString) throws RemoteException;
  
  class Default implements IAppWidgetService {
    public ParceledListSlice startListening(IAppWidgetHost param1IAppWidgetHost, String param1String, int param1Int, int[] param1ArrayOfint) throws RemoteException {
      return null;
    }
    
    public void stopListening(String param1String, int param1Int) throws RemoteException {}
    
    public int allocateAppWidgetId(String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public void deleteAppWidgetId(String param1String, int param1Int) throws RemoteException {}
    
    public void deleteHost(String param1String, int param1Int) throws RemoteException {}
    
    public void deleteAllHosts() throws RemoteException {}
    
    public RemoteViews getAppWidgetViews(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public int[] getAppWidgetIdsForHost(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public IntentSender createAppWidgetConfigIntentSender(String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public void updateAppWidgetIds(String param1String, int[] param1ArrayOfint, RemoteViews param1RemoteViews) throws RemoteException {}
    
    public void updateAppWidgetOptions(String param1String, int param1Int, Bundle param1Bundle) throws RemoteException {}
    
    public Bundle getAppWidgetOptions(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public void partiallyUpdateAppWidgetIds(String param1String, int[] param1ArrayOfint, RemoteViews param1RemoteViews) throws RemoteException {}
    
    public void updateAppWidgetProvider(ComponentName param1ComponentName, RemoteViews param1RemoteViews) throws RemoteException {}
    
    public void updateAppWidgetProviderInfo(ComponentName param1ComponentName, String param1String) throws RemoteException {}
    
    public void notifyAppWidgetViewDataChanged(String param1String, int[] param1ArrayOfint, int param1Int) throws RemoteException {}
    
    public ParceledListSlice getInstalledProvidersForProfile(int param1Int1, int param1Int2, String param1String) throws RemoteException {
      return null;
    }
    
    public AppWidgetProviderInfo getAppWidgetInfo(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean hasBindAppWidgetPermission(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public void setBindAppWidgetPermission(String param1String, int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public boolean bindAppWidgetId(String param1String, int param1Int1, int param1Int2, ComponentName param1ComponentName, Bundle param1Bundle) throws RemoteException {
      return false;
    }
    
    public boolean bindRemoteViewsService(String param1String, int param1Int1, Intent param1Intent, IApplicationThread param1IApplicationThread, IBinder param1IBinder, IServiceConnection param1IServiceConnection, int param1Int2) throws RemoteException {
      return false;
    }
    
    public int[] getAppWidgetIds(ComponentName param1ComponentName) throws RemoteException {
      return null;
    }
    
    public boolean isBoundWidgetPackage(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean requestPinAppWidget(String param1String, ComponentName param1ComponentName, Bundle param1Bundle, IntentSender param1IntentSender) throws RemoteException {
      return false;
    }
    
    public boolean isRequestPinAppWidgetSupported() throws RemoteException {
      return false;
    }
    
    public void noteAppWidgetTapped(String param1String, int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAppWidgetService {
    private static final String DESCRIPTOR = "com.android.internal.appwidget.IAppWidgetService";
    
    static final int TRANSACTION_allocateAppWidgetId = 3;
    
    static final int TRANSACTION_bindAppWidgetId = 21;
    
    static final int TRANSACTION_bindRemoteViewsService = 22;
    
    static final int TRANSACTION_createAppWidgetConfigIntentSender = 9;
    
    static final int TRANSACTION_deleteAllHosts = 6;
    
    static final int TRANSACTION_deleteAppWidgetId = 4;
    
    static final int TRANSACTION_deleteHost = 5;
    
    static final int TRANSACTION_getAppWidgetIds = 23;
    
    static final int TRANSACTION_getAppWidgetIdsForHost = 8;
    
    static final int TRANSACTION_getAppWidgetInfo = 18;
    
    static final int TRANSACTION_getAppWidgetOptions = 12;
    
    static final int TRANSACTION_getAppWidgetViews = 7;
    
    static final int TRANSACTION_getInstalledProvidersForProfile = 17;
    
    static final int TRANSACTION_hasBindAppWidgetPermission = 19;
    
    static final int TRANSACTION_isBoundWidgetPackage = 24;
    
    static final int TRANSACTION_isRequestPinAppWidgetSupported = 26;
    
    static final int TRANSACTION_noteAppWidgetTapped = 27;
    
    static final int TRANSACTION_notifyAppWidgetViewDataChanged = 16;
    
    static final int TRANSACTION_partiallyUpdateAppWidgetIds = 13;
    
    static final int TRANSACTION_requestPinAppWidget = 25;
    
    static final int TRANSACTION_setBindAppWidgetPermission = 20;
    
    static final int TRANSACTION_startListening = 1;
    
    static final int TRANSACTION_stopListening = 2;
    
    static final int TRANSACTION_updateAppWidgetIds = 10;
    
    static final int TRANSACTION_updateAppWidgetOptions = 11;
    
    static final int TRANSACTION_updateAppWidgetProvider = 14;
    
    static final int TRANSACTION_updateAppWidgetProviderInfo = 15;
    
    public Stub() {
      attachInterface(this, "com.android.internal.appwidget.IAppWidgetService");
    }
    
    public static IAppWidgetService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.appwidget.IAppWidgetService");
      if (iInterface != null && iInterface instanceof IAppWidgetService)
        return (IAppWidgetService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 27:
          return "noteAppWidgetTapped";
        case 26:
          return "isRequestPinAppWidgetSupported";
        case 25:
          return "requestPinAppWidget";
        case 24:
          return "isBoundWidgetPackage";
        case 23:
          return "getAppWidgetIds";
        case 22:
          return "bindRemoteViewsService";
        case 21:
          return "bindAppWidgetId";
        case 20:
          return "setBindAppWidgetPermission";
        case 19:
          return "hasBindAppWidgetPermission";
        case 18:
          return "getAppWidgetInfo";
        case 17:
          return "getInstalledProvidersForProfile";
        case 16:
          return "notifyAppWidgetViewDataChanged";
        case 15:
          return "updateAppWidgetProviderInfo";
        case 14:
          return "updateAppWidgetProvider";
        case 13:
          return "partiallyUpdateAppWidgetIds";
        case 12:
          return "getAppWidgetOptions";
        case 11:
          return "updateAppWidgetOptions";
        case 10:
          return "updateAppWidgetIds";
        case 9:
          return "createAppWidgetConfigIntentSender";
        case 8:
          return "getAppWidgetIdsForHost";
        case 7:
          return "getAppWidgetViews";
        case 6:
          return "deleteAllHosts";
        case 5:
          return "deleteHost";
        case 4:
          return "deleteAppWidgetId";
        case 3:
          return "allocateAppWidgetId";
        case 2:
          return "stopListening";
        case 1:
          break;
      } 
      return "startListening";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        boolean bool5;
        int n;
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        int[] arrayOfInt3;
        AppWidgetProviderInfo appWidgetProviderInfo;
        String str2;
        ParceledListSlice parceledListSlice2;
        String str1;
        Bundle bundle1;
        IntentSender intentSender;
        int[] arrayOfInt2;
        RemoteViews remoteViews;
        String str3;
        IBinder iBinder;
        ComponentName componentName;
        String str5;
        int[] arrayOfInt4;
        String str4;
        Bundle bundle2;
        String str7;
        int[] arrayOfInt5;
        IApplicationThread iApplicationThread;
        IServiceConnection iServiceConnection;
        boolean bool = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 27:
            param1Parcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
            str = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            noteAppWidgetTapped(str, param1Int1);
            return true;
          case 26:
            param1Parcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
            bool5 = isRequestPinAppWidgetSupported();
            str.writeNoException();
            str.writeInt(bool5);
            return true;
          case 25:
            param1Parcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
            str3 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              componentName = (ComponentName)ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              componentName = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              bundle2 = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              bundle2 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              IntentSender intentSender1 = (IntentSender)IntentSender.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            bool5 = requestPinAppWidget(str3, componentName, bundle2, (IntentSender)param1Parcel1);
            str.writeNoException();
            str.writeInt(bool5);
            return true;
          case 24:
            param1Parcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
            str5 = param1Parcel1.readString();
            n = param1Parcel1.readInt();
            bool4 = isBoundWidgetPackage(str5, n);
            str.writeNoException();
            str.writeInt(bool4);
            return true;
          case 23:
            param1Parcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
            if (param1Parcel1.readInt() != 0) {
              ComponentName componentName1 = (ComponentName)ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            arrayOfInt3 = getAppWidgetIds((ComponentName)param1Parcel1);
            str.writeNoException();
            str.writeIntArray(arrayOfInt3);
            return true;
          case 22:
            arrayOfInt3.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
            str7 = arrayOfInt3.readString();
            param1Int2 = arrayOfInt3.readInt();
            if (arrayOfInt3.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)arrayOfInt3);
            } else {
              str5 = null;
            } 
            iApplicationThread = IApplicationThread.Stub.asInterface(arrayOfInt3.readStrongBinder());
            iBinder = arrayOfInt3.readStrongBinder();
            iServiceConnection = IServiceConnection.Stub.asInterface(arrayOfInt3.readStrongBinder());
            m = arrayOfInt3.readInt();
            bool3 = bindRemoteViewsService(str7, param1Int2, (Intent)str5, iApplicationThread, iBinder, iServiceConnection, m);
            str.writeNoException();
            str.writeInt(bool3);
            return true;
          case 21:
            arrayOfInt3.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
            str7 = arrayOfInt3.readString();
            param1Int2 = arrayOfInt3.readInt();
            k = arrayOfInt3.readInt();
            if (arrayOfInt3.readInt() != 0) {
              ComponentName componentName1 = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)arrayOfInt3);
            } else {
              str5 = null;
            } 
            if (arrayOfInt3.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)arrayOfInt3);
            } else {
              arrayOfInt3 = null;
            } 
            bool2 = bindAppWidgetId(str7, param1Int2, k, (ComponentName)str5, (Bundle)arrayOfInt3);
            str.writeNoException();
            str.writeInt(bool2);
            return true;
          case 20:
            arrayOfInt3.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
            str5 = arrayOfInt3.readString();
            j = arrayOfInt3.readInt();
            if (arrayOfInt3.readInt() != 0)
              bool = true; 
            setBindAppWidgetPermission(str5, j, bool);
            str.writeNoException();
            return true;
          case 19:
            arrayOfInt3.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
            str5 = arrayOfInt3.readString();
            j = arrayOfInt3.readInt();
            bool1 = hasBindAppWidgetPermission(str5, j);
            str.writeNoException();
            str.writeInt(bool1);
            return true;
          case 18:
            arrayOfInt3.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
            str5 = arrayOfInt3.readString();
            i = arrayOfInt3.readInt();
            appWidgetProviderInfo = getAppWidgetInfo(str5, i);
            str.writeNoException();
            if (appWidgetProviderInfo != null) {
              str.writeInt(1);
              appWidgetProviderInfo.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 17:
            appWidgetProviderInfo.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
            i = appWidgetProviderInfo.readInt();
            param1Int2 = appWidgetProviderInfo.readInt();
            str2 = appWidgetProviderInfo.readString();
            parceledListSlice2 = getInstalledProvidersForProfile(i, param1Int2, str2);
            str.writeNoException();
            if (parceledListSlice2 != null) {
              str.writeInt(1);
              parceledListSlice2.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 16:
            parceledListSlice2.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
            str5 = parceledListSlice2.readString();
            arrayOfInt5 = parceledListSlice2.createIntArray();
            i = parceledListSlice2.readInt();
            notifyAppWidgetViewDataChanged(str5, arrayOfInt5, i);
            str.writeNoException();
            return true;
          case 15:
            parceledListSlice2.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
            if (parceledListSlice2.readInt() != 0) {
              ComponentName componentName1 = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)parceledListSlice2);
            } else {
              str5 = null;
            } 
            str1 = parceledListSlice2.readString();
            updateAppWidgetProviderInfo((ComponentName)str5, str1);
            str.writeNoException();
            return true;
          case 14:
            str1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
            if (str1.readInt() != 0) {
              ComponentName componentName1 = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str5 = null;
            } 
            if (str1.readInt() != 0) {
              RemoteViews remoteViews1 = (RemoteViews)RemoteViews.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            updateAppWidgetProvider((ComponentName)str5, (RemoteViews)str1);
            str.writeNoException();
            return true;
          case 13:
            str1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
            str5 = str1.readString();
            arrayOfInt5 = str1.createIntArray();
            if (str1.readInt() != 0) {
              RemoteViews remoteViews1 = (RemoteViews)RemoteViews.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            partiallyUpdateAppWidgetIds(str5, arrayOfInt5, (RemoteViews)str1);
            str.writeNoException();
            return true;
          case 12:
            str1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
            str5 = str1.readString();
            i = str1.readInt();
            bundle1 = getAppWidgetOptions(str5, i);
            str.writeNoException();
            if (bundle1 != null) {
              str.writeInt(1);
              bundle1.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 11:
            bundle1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
            str5 = bundle1.readString();
            i = bundle1.readInt();
            if (bundle1.readInt() != 0) {
              bundle1 = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)bundle1);
            } else {
              bundle1 = null;
            } 
            updateAppWidgetOptions(str5, i, bundle1);
            str.writeNoException();
            return true;
          case 10:
            bundle1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
            str6 = bundle1.readString();
            arrayOfInt4 = bundle1.createIntArray();
            if (bundle1.readInt() != 0) {
              RemoteViews remoteViews1 = (RemoteViews)RemoteViews.CREATOR.createFromParcel((Parcel)bundle1);
            } else {
              bundle1 = null;
            } 
            updateAppWidgetIds(str6, arrayOfInt4, (RemoteViews)bundle1);
            str.writeNoException();
            return true;
          case 9:
            bundle1.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
            str4 = bundle1.readString();
            param1Int2 = bundle1.readInt();
            i = bundle1.readInt();
            intentSender = createAppWidgetConfigIntentSender(str4, param1Int2, i);
            str.writeNoException();
            if (intentSender != null) {
              str.writeInt(1);
              intentSender.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 8:
            intentSender.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
            str4 = intentSender.readString();
            i = intentSender.readInt();
            arrayOfInt2 = getAppWidgetIdsForHost(str4, i);
            str.writeNoException();
            str.writeIntArray(arrayOfInt2);
            return true;
          case 7:
            arrayOfInt2.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
            str4 = arrayOfInt2.readString();
            i = arrayOfInt2.readInt();
            remoteViews = getAppWidgetViews(str4, i);
            str.writeNoException();
            if (remoteViews != null) {
              str.writeInt(1);
              remoteViews.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 6:
            remoteViews.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
            deleteAllHosts();
            str.writeNoException();
            return true;
          case 5:
            remoteViews.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
            str4 = remoteViews.readString();
            i = remoteViews.readInt();
            deleteHost(str4, i);
            str.writeNoException();
            return true;
          case 4:
            remoteViews.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
            str4 = remoteViews.readString();
            i = remoteViews.readInt();
            deleteAppWidgetId(str4, i);
            str.writeNoException();
            return true;
          case 3:
            remoteViews.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
            str4 = remoteViews.readString();
            i = remoteViews.readInt();
            i = allocateAppWidgetId(str4, i);
            str.writeNoException();
            str.writeInt(i);
            return true;
          case 2:
            remoteViews.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
            str4 = remoteViews.readString();
            i = remoteViews.readInt();
            stopListening(str4, i);
            str.writeNoException();
            return true;
          case 1:
            break;
        } 
        remoteViews.enforceInterface("com.android.internal.appwidget.IAppWidgetService");
        IAppWidgetHost iAppWidgetHost = IAppWidgetHost.Stub.asInterface(remoteViews.readStrongBinder());
        String str6 = remoteViews.readString();
        int i = remoteViews.readInt();
        int[] arrayOfInt1 = remoteViews.createIntArray();
        ParceledListSlice parceledListSlice1 = startListening(iAppWidgetHost, str6, i, arrayOfInt1);
        str.writeNoException();
        if (parceledListSlice1 != null) {
          str.writeInt(1);
          parceledListSlice1.writeToParcel((Parcel)str, 1);
        } else {
          str.writeInt(0);
        } 
        return true;
      } 
      str.writeString("com.android.internal.appwidget.IAppWidgetService");
      return true;
    }
    
    private static class Proxy implements IAppWidgetService {
      public static IAppWidgetService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.appwidget.IAppWidgetService";
      }
      
      public ParceledListSlice startListening(IAppWidgetHost param2IAppWidgetHost, String param2String, int param2Int, int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
          if (param2IAppWidgetHost != null) {
            iBinder = param2IAppWidgetHost.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IAppWidgetService.Stub.getDefaultImpl() != null)
            return IAppWidgetService.Stub.getDefaultImpl().startListening(param2IAppWidgetHost, param2String, param2Int, param2ArrayOfint); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2IAppWidgetHost = null;
          } 
          return (ParceledListSlice)param2IAppWidgetHost;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopListening(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IAppWidgetService.Stub.getDefaultImpl() != null) {
            IAppWidgetService.Stub.getDefaultImpl().stopListening(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int allocateAppWidgetId(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IAppWidgetService.Stub.getDefaultImpl() != null) {
            param2Int = IAppWidgetService.Stub.getDefaultImpl().allocateAppWidgetId(param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deleteAppWidgetId(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IAppWidgetService.Stub.getDefaultImpl() != null) {
            IAppWidgetService.Stub.getDefaultImpl().deleteAppWidgetId(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deleteHost(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IAppWidgetService.Stub.getDefaultImpl() != null) {
            IAppWidgetService.Stub.getDefaultImpl().deleteHost(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deleteAllHosts() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IAppWidgetService.Stub.getDefaultImpl() != null) {
            IAppWidgetService.Stub.getDefaultImpl().deleteAllHosts();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public RemoteViews getAppWidgetViews(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IAppWidgetService.Stub.getDefaultImpl() != null)
            return IAppWidgetService.Stub.getDefaultImpl().getAppWidgetViews(param2String, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            RemoteViews remoteViews = (RemoteViews)RemoteViews.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (RemoteViews)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] getAppWidgetIdsForHost(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IAppWidgetService.Stub.getDefaultImpl() != null)
            return IAppWidgetService.Stub.getDefaultImpl().getAppWidgetIdsForHost(param2String, param2Int); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IntentSender createAppWidgetConfigIntentSender(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IAppWidgetService.Stub.getDefaultImpl() != null)
            return IAppWidgetService.Stub.getDefaultImpl().createAppWidgetConfigIntentSender(param2String, param2Int1, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            IntentSender intentSender = (IntentSender)IntentSender.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (IntentSender)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateAppWidgetIds(String param2String, int[] param2ArrayOfint, RemoteViews param2RemoteViews) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
          parcel1.writeString(param2String);
          parcel1.writeIntArray(param2ArrayOfint);
          if (param2RemoteViews != null) {
            parcel1.writeInt(1);
            param2RemoteViews.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IAppWidgetService.Stub.getDefaultImpl() != null) {
            IAppWidgetService.Stub.getDefaultImpl().updateAppWidgetIds(param2String, param2ArrayOfint, param2RemoteViews);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateAppWidgetOptions(String param2String, int param2Int, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IAppWidgetService.Stub.getDefaultImpl() != null) {
            IAppWidgetService.Stub.getDefaultImpl().updateAppWidgetOptions(param2String, param2Int, param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bundle getAppWidgetOptions(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IAppWidgetService.Stub.getDefaultImpl() != null)
            return IAppWidgetService.Stub.getDefaultImpl().getAppWidgetOptions(param2String, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (Bundle)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void partiallyUpdateAppWidgetIds(String param2String, int[] param2ArrayOfint, RemoteViews param2RemoteViews) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
          parcel1.writeString(param2String);
          parcel1.writeIntArray(param2ArrayOfint);
          if (param2RemoteViews != null) {
            parcel1.writeInt(1);
            param2RemoteViews.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IAppWidgetService.Stub.getDefaultImpl() != null) {
            IAppWidgetService.Stub.getDefaultImpl().partiallyUpdateAppWidgetIds(param2String, param2ArrayOfint, param2RemoteViews);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateAppWidgetProvider(ComponentName param2ComponentName, RemoteViews param2RemoteViews) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2RemoteViews != null) {
            parcel1.writeInt(1);
            param2RemoteViews.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IAppWidgetService.Stub.getDefaultImpl() != null) {
            IAppWidgetService.Stub.getDefaultImpl().updateAppWidgetProvider(param2ComponentName, param2RemoteViews);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateAppWidgetProviderInfo(ComponentName param2ComponentName, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IAppWidgetService.Stub.getDefaultImpl() != null) {
            IAppWidgetService.Stub.getDefaultImpl().updateAppWidgetProviderInfo(param2ComponentName, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyAppWidgetViewDataChanged(String param2String, int[] param2ArrayOfint, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
          parcel1.writeString(param2String);
          parcel1.writeIntArray(param2ArrayOfint);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IAppWidgetService.Stub.getDefaultImpl() != null) {
            IAppWidgetService.Stub.getDefaultImpl().notifyAppWidgetViewDataChanged(param2String, param2ArrayOfint, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getInstalledProvidersForProfile(int param2Int1, int param2Int2, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IAppWidgetService.Stub.getDefaultImpl() != null)
            return IAppWidgetService.Stub.getDefaultImpl().getInstalledProvidersForProfile(param2Int1, param2Int2, param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (ParceledListSlice)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public AppWidgetProviderInfo getAppWidgetInfo(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IAppWidgetService.Stub.getDefaultImpl() != null)
            return IAppWidgetService.Stub.getDefaultImpl().getAppWidgetInfo(param2String, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            AppWidgetProviderInfo appWidgetProviderInfo = (AppWidgetProviderInfo)AppWidgetProviderInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (AppWidgetProviderInfo)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasBindAppWidgetPermission(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(19, parcel1, parcel2, 0);
          if (!bool2 && IAppWidgetService.Stub.getDefaultImpl() != null) {
            bool1 = IAppWidgetService.Stub.getDefaultImpl().hasBindAppWidgetPermission(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setBindAppWidgetPermission(String param2String, int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool1 && IAppWidgetService.Stub.getDefaultImpl() != null) {
            IAppWidgetService.Stub.getDefaultImpl().setBindAppWidgetPermission(param2String, param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean bindAppWidgetId(String param2String, int param2Int1, int param2Int2, ComponentName param2ComponentName, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
          try {
            parcel1.writeString(param2String);
            try {
              parcel1.writeInt(param2Int1);
              try {
                parcel1.writeInt(param2Int2);
                boolean bool = true;
                if (param2ComponentName != null) {
                  parcel1.writeInt(1);
                  param2ComponentName.writeToParcel(parcel1, 0);
                } else {
                  parcel1.writeInt(0);
                } 
                if (param2Bundle != null) {
                  parcel1.writeInt(1);
                  param2Bundle.writeToParcel(parcel1, 0);
                } else {
                  parcel1.writeInt(0);
                } 
                try {
                  boolean bool1 = this.mRemote.transact(21, parcel1, parcel2, 0);
                  if (!bool1 && IAppWidgetService.Stub.getDefaultImpl() != null) {
                    bool = IAppWidgetService.Stub.getDefaultImpl().bindAppWidgetId(param2String, param2Int1, param2Int2, param2ComponentName, param2Bundle);
                    parcel2.recycle();
                    parcel1.recycle();
                    return bool;
                  } 
                  parcel2.readException();
                  param2Int1 = parcel2.readInt();
                  if (param2Int1 == 0)
                    bool = false; 
                  parcel2.recycle();
                  parcel1.recycle();
                  return bool;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public boolean bindRemoteViewsService(String param2String, int param2Int1, Intent param2Intent, IApplicationThread param2IApplicationThread, IBinder param2IBinder, IServiceConnection param2IServiceConnection, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
          try {
            parcel1.writeString(param2String);
            try {
              IBinder iBinder2;
              parcel1.writeInt(param2Int1);
              boolean bool = true;
              if (param2Intent != null) {
                parcel1.writeInt(1);
                param2Intent.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              IBinder iBinder1 = null;
              if (param2IApplicationThread != null) {
                iBinder2 = param2IApplicationThread.asBinder();
              } else {
                iBinder2 = null;
              } 
              parcel1.writeStrongBinder(iBinder2);
              try {
                parcel1.writeStrongBinder(param2IBinder);
                iBinder2 = iBinder1;
                if (param2IServiceConnection != null)
                  iBinder2 = param2IServiceConnection.asBinder(); 
                parcel1.writeStrongBinder(iBinder2);
                try {
                  parcel1.writeInt(param2Int2);
                  boolean bool1 = this.mRemote.transact(22, parcel1, parcel2, 0);
                  if (!bool1 && IAppWidgetService.Stub.getDefaultImpl() != null) {
                    bool = IAppWidgetService.Stub.getDefaultImpl().bindRemoteViewsService(param2String, param2Int1, param2Intent, param2IApplicationThread, param2IBinder, param2IServiceConnection, param2Int2);
                    parcel2.recycle();
                    parcel1.recycle();
                    return bool;
                  } 
                  parcel2.readException();
                  param2Int1 = parcel2.readInt();
                  if (param2Int1 == 0)
                    bool = false; 
                  parcel2.recycle();
                  parcel1.recycle();
                  return bool;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public int[] getAppWidgetIds(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IAppWidgetService.Stub.getDefaultImpl() != null)
            return IAppWidgetService.Stub.getDefaultImpl().getAppWidgetIds(param2ComponentName); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isBoundWidgetPackage(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(24, parcel1, parcel2, 0);
          if (!bool2 && IAppWidgetService.Stub.getDefaultImpl() != null) {
            bool1 = IAppWidgetService.Stub.getDefaultImpl().isBoundWidgetPackage(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean requestPinAppWidget(String param2String, ComponentName param2ComponentName, Bundle param2Bundle, IntentSender param2IntentSender) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
          parcel1.writeString(param2String);
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2IntentSender != null) {
            parcel1.writeInt(1);
            param2IntentSender.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool2 && IAppWidgetService.Stub.getDefaultImpl() != null) {
            bool1 = IAppWidgetService.Stub.getDefaultImpl().requestPinAppWidget(param2String, param2ComponentName, param2Bundle, param2IntentSender);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isRequestPinAppWidgetSupported() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(26, parcel1, parcel2, 0);
          if (!bool2 && IAppWidgetService.Stub.getDefaultImpl() != null) {
            bool1 = IAppWidgetService.Stub.getDefaultImpl().isRequestPinAppWidgetSupported();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteAppWidgetTapped(String param2String, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetService");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(27, parcel, null, 1);
          if (!bool && IAppWidgetService.Stub.getDefaultImpl() != null) {
            IAppWidgetService.Stub.getDefaultImpl().noteAppWidgetTapped(param2String, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAppWidgetService param1IAppWidgetService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAppWidgetService != null) {
          Proxy.sDefaultImpl = param1IAppWidgetService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAppWidgetService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
