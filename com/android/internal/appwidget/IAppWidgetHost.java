package com.android.internal.appwidget;

import android.appwidget.AppWidgetProviderInfo;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.widget.RemoteViews;

public interface IAppWidgetHost extends IInterface {
  void appWidgetRemoved(int paramInt) throws RemoteException;
  
  void providerChanged(int paramInt, AppWidgetProviderInfo paramAppWidgetProviderInfo) throws RemoteException;
  
  void providersChanged() throws RemoteException;
  
  void updateAppWidget(int paramInt, RemoteViews paramRemoteViews) throws RemoteException;
  
  void viewDataChanged(int paramInt1, int paramInt2) throws RemoteException;
  
  class Default implements IAppWidgetHost {
    public void updateAppWidget(int param1Int, RemoteViews param1RemoteViews) throws RemoteException {}
    
    public void providerChanged(int param1Int, AppWidgetProviderInfo param1AppWidgetProviderInfo) throws RemoteException {}
    
    public void providersChanged() throws RemoteException {}
    
    public void viewDataChanged(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void appWidgetRemoved(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAppWidgetHost {
    private static final String DESCRIPTOR = "com.android.internal.appwidget.IAppWidgetHost";
    
    static final int TRANSACTION_appWidgetRemoved = 5;
    
    static final int TRANSACTION_providerChanged = 2;
    
    static final int TRANSACTION_providersChanged = 3;
    
    static final int TRANSACTION_updateAppWidget = 1;
    
    static final int TRANSACTION_viewDataChanged = 4;
    
    public Stub() {
      attachInterface(this, "com.android.internal.appwidget.IAppWidgetHost");
    }
    
    public static IAppWidgetHost asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.appwidget.IAppWidgetHost");
      if (iInterface != null && iInterface instanceof IAppWidgetHost)
        return (IAppWidgetHost)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "appWidgetRemoved";
            } 
            return "viewDataChanged";
          } 
          return "providersChanged";
        } 
        return "providerChanged";
      } 
      return "updateAppWidget";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("com.android.internal.appwidget.IAppWidgetHost");
                return true;
              } 
              param1Parcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetHost");
              param1Int1 = param1Parcel1.readInt();
              appWidgetRemoved(param1Int1);
              return true;
            } 
            param1Parcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetHost");
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            viewDataChanged(param1Int1, param1Int2);
            return true;
          } 
          param1Parcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetHost");
          providersChanged();
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetHost");
        param1Int1 = param1Parcel1.readInt();
        if (param1Parcel1.readInt() != 0) {
          AppWidgetProviderInfo appWidgetProviderInfo = (AppWidgetProviderInfo)AppWidgetProviderInfo.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        providerChanged(param1Int1, (AppWidgetProviderInfo)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.appwidget.IAppWidgetHost");
      param1Int1 = param1Parcel1.readInt();
      if (param1Parcel1.readInt() != 0) {
        RemoteViews remoteViews = (RemoteViews)RemoteViews.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      updateAppWidget(param1Int1, (RemoteViews)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IAppWidgetHost {
      public static IAppWidgetHost sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.appwidget.IAppWidgetHost";
      }
      
      public void updateAppWidget(int param2Int, RemoteViews param2RemoteViews) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetHost");
          parcel.writeInt(param2Int);
          if (param2RemoteViews != null) {
            parcel.writeInt(1);
            param2RemoteViews.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IAppWidgetHost.Stub.getDefaultImpl() != null) {
            IAppWidgetHost.Stub.getDefaultImpl().updateAppWidget(param2Int, param2RemoteViews);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void providerChanged(int param2Int, AppWidgetProviderInfo param2AppWidgetProviderInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetHost");
          parcel.writeInt(param2Int);
          if (param2AppWidgetProviderInfo != null) {
            parcel.writeInt(1);
            param2AppWidgetProviderInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IAppWidgetHost.Stub.getDefaultImpl() != null) {
            IAppWidgetHost.Stub.getDefaultImpl().providerChanged(param2Int, param2AppWidgetProviderInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void providersChanged() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetHost");
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IAppWidgetHost.Stub.getDefaultImpl() != null) {
            IAppWidgetHost.Stub.getDefaultImpl().providersChanged();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void viewDataChanged(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetHost");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IAppWidgetHost.Stub.getDefaultImpl() != null) {
            IAppWidgetHost.Stub.getDefaultImpl().viewDataChanged(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void appWidgetRemoved(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.appwidget.IAppWidgetHost");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IAppWidgetHost.Stub.getDefaultImpl() != null) {
            IAppWidgetHost.Stub.getDefaultImpl().appWidgetRemoved(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAppWidgetHost param1IAppWidgetHost) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAppWidgetHost != null) {
          Proxy.sDefaultImpl = param1IAppWidgetHost;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAppWidgetHost getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
