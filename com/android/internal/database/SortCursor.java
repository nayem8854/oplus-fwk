package com.android.internal.database;

import android.database.AbstractCursor;
import android.database.Cursor;
import android.database.DataSetObserver;

public class SortCursor extends AbstractCursor {
  private static final String TAG = "SortCursor";
  
  private final int ROWCACHESIZE;
  
  private int[][] mCurRowNumCache;
  
  private Cursor mCursor;
  
  private int[] mCursorCache;
  
  private Cursor[] mCursors;
  
  private int mLastCacheHit;
  
  private DataSetObserver mObserver;
  
  private int[] mRowNumCache;
  
  private int[] mSortColumns;
  
  public SortCursor(Cursor[] paramArrayOfCursor, String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial <init> : ()V
    //   4: aload_0
    //   5: bipush #64
    //   7: putfield ROWCACHESIZE : I
    //   10: aload_0
    //   11: bipush #64
    //   13: newarray int
    //   15: putfield mRowNumCache : [I
    //   18: aload_0
    //   19: bipush #64
    //   21: newarray int
    //   23: putfield mCursorCache : [I
    //   26: aload_0
    //   27: iconst_m1
    //   28: putfield mLastCacheHit : I
    //   31: aload_0
    //   32: new com/android/internal/database/SortCursor$1
    //   35: dup
    //   36: aload_0
    //   37: invokespecial <init> : (Lcom/android/internal/database/SortCursor;)V
    //   40: putfield mObserver : Landroid/database/DataSetObserver;
    //   43: aload_0
    //   44: aload_1
    //   45: putfield mCursors : [Landroid/database/Cursor;
    //   48: aload_1
    //   49: arraylength
    //   50: istore_3
    //   51: aload_0
    //   52: iload_3
    //   53: newarray int
    //   55: putfield mSortColumns : [I
    //   58: iconst_0
    //   59: istore #4
    //   61: iload #4
    //   63: iload_3
    //   64: if_icmpge -> 134
    //   67: aload_0
    //   68: getfield mCursors : [Landroid/database/Cursor;
    //   71: astore_1
    //   72: aload_1
    //   73: iload #4
    //   75: aaload
    //   76: ifnonnull -> 82
    //   79: goto -> 128
    //   82: aload_1
    //   83: iload #4
    //   85: aaload
    //   86: aload_0
    //   87: getfield mObserver : Landroid/database/DataSetObserver;
    //   90: invokeinterface registerDataSetObserver : (Landroid/database/DataSetObserver;)V
    //   95: aload_0
    //   96: getfield mCursors : [Landroid/database/Cursor;
    //   99: iload #4
    //   101: aaload
    //   102: invokeinterface moveToFirst : ()Z
    //   107: pop
    //   108: aload_0
    //   109: getfield mSortColumns : [I
    //   112: iload #4
    //   114: aload_0
    //   115: getfield mCursors : [Landroid/database/Cursor;
    //   118: iload #4
    //   120: aaload
    //   121: aload_2
    //   122: invokeinterface getColumnIndexOrThrow : (Ljava/lang/String;)I
    //   127: iastore
    //   128: iinc #4, 1
    //   131: goto -> 61
    //   134: aload_0
    //   135: aconst_null
    //   136: putfield mCursor : Landroid/database/Cursor;
    //   139: ldc ''
    //   141: astore_1
    //   142: iconst_0
    //   143: istore #4
    //   145: iload #4
    //   147: iload_3
    //   148: if_icmpge -> 246
    //   151: aload_0
    //   152: getfield mCursors : [Landroid/database/Cursor;
    //   155: astore #5
    //   157: aload_1
    //   158: astore_2
    //   159: aload #5
    //   161: iload #4
    //   163: aaload
    //   164: ifnull -> 238
    //   167: aload #5
    //   169: iload #4
    //   171: aaload
    //   172: invokeinterface isAfterLast : ()Z
    //   177: ifeq -> 185
    //   180: aload_1
    //   181: astore_2
    //   182: goto -> 238
    //   185: aload_0
    //   186: getfield mCursors : [Landroid/database/Cursor;
    //   189: iload #4
    //   191: aaload
    //   192: aload_0
    //   193: getfield mSortColumns : [I
    //   196: iload #4
    //   198: iaload
    //   199: invokeinterface getString : (I)Ljava/lang/String;
    //   204: astore #5
    //   206: aload_0
    //   207: getfield mCursor : Landroid/database/Cursor;
    //   210: ifnull -> 224
    //   213: aload_1
    //   214: astore_2
    //   215: aload #5
    //   217: aload_1
    //   218: invokevirtual compareToIgnoreCase : (Ljava/lang/String;)I
    //   221: ifge -> 238
    //   224: aload #5
    //   226: astore_2
    //   227: aload_0
    //   228: aload_0
    //   229: getfield mCursors : [Landroid/database/Cursor;
    //   232: iload #4
    //   234: aaload
    //   235: putfield mCursor : Landroid/database/Cursor;
    //   238: iinc #4, 1
    //   241: aload_2
    //   242: astore_1
    //   243: goto -> 145
    //   246: aload_0
    //   247: getfield mRowNumCache : [I
    //   250: arraylength
    //   251: iconst_1
    //   252: isub
    //   253: istore #4
    //   255: iload #4
    //   257: iflt -> 275
    //   260: aload_0
    //   261: getfield mRowNumCache : [I
    //   264: iload #4
    //   266: bipush #-2
    //   268: iastore
    //   269: iinc #4, -1
    //   272: goto -> 255
    //   275: aload_0
    //   276: bipush #64
    //   278: iload_3
    //   279: multianewarray[[I 2
    //   283: putfield mCurRowNumCache : [[I
    //   286: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #60	-> 0
    //   #37	-> 4
    //   #38	-> 10
    //   #39	-> 18
    //   #41	-> 26
    //   #43	-> 31
    //   #61	-> 43
    //   #63	-> 48
    //   #64	-> 51
    //   #65	-> 58
    //   #66	-> 67
    //   #69	-> 82
    //   #71	-> 95
    //   #74	-> 108
    //   #65	-> 128
    //   #76	-> 134
    //   #77	-> 139
    //   #78	-> 142
    //   #79	-> 151
    //   #80	-> 180
    //   #81	-> 185
    //   #82	-> 206
    //   #83	-> 224
    //   #84	-> 227
    //   #78	-> 238
    //   #88	-> 246
    //   #89	-> 260
    //   #88	-> 269
    //   #91	-> 275
    //   #92	-> 286
  }
  
  public int getCount() {
    int i = 0;
    int j = this.mCursors.length;
    for (byte b = 0; b < j; b++, i = k) {
      Cursor[] arrayOfCursor = this.mCursors;
      int k = i;
      if (arrayOfCursor[b] != null)
        k = i + arrayOfCursor[b].getCount(); 
    } 
    return i;
  }
  
  public boolean onMove(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: iload_1
    //   1: iload_2
    //   2: if_icmpne -> 7
    //   5: iconst_1
    //   6: ireturn
    //   7: iload_2
    //   8: bipush #64
    //   10: irem
    //   11: istore_3
    //   12: aload_0
    //   13: getfield mRowNumCache : [I
    //   16: iload_3
    //   17: iaload
    //   18: iload_2
    //   19: if_icmpne -> 81
    //   22: aload_0
    //   23: getfield mCursorCache : [I
    //   26: iload_3
    //   27: iaload
    //   28: istore_1
    //   29: aload_0
    //   30: getfield mCursors : [Landroid/database/Cursor;
    //   33: iload_1
    //   34: aaload
    //   35: astore #4
    //   37: aload_0
    //   38: aload #4
    //   40: putfield mCursor : Landroid/database/Cursor;
    //   43: aload #4
    //   45: ifnonnull -> 58
    //   48: ldc 'SortCursor'
    //   50: ldc 'onMove: cache results in a null cursor.'
    //   52: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   55: pop
    //   56: iconst_0
    //   57: ireturn
    //   58: aload #4
    //   60: aload_0
    //   61: getfield mCurRowNumCache : [[I
    //   64: iload_3
    //   65: aaload
    //   66: iload_1
    //   67: iaload
    //   68: invokeinterface moveToPosition : (I)Z
    //   73: pop
    //   74: aload_0
    //   75: iload_3
    //   76: putfield mLastCacheHit : I
    //   79: iconst_1
    //   80: ireturn
    //   81: aload_0
    //   82: aconst_null
    //   83: putfield mCursor : Landroid/database/Cursor;
    //   86: aload_0
    //   87: getfield mCursors : [Landroid/database/Cursor;
    //   90: arraylength
    //   91: istore #5
    //   93: aload_0
    //   94: getfield mLastCacheHit : I
    //   97: iflt -> 156
    //   100: iconst_0
    //   101: istore #6
    //   103: iload #6
    //   105: iload #5
    //   107: if_icmpge -> 156
    //   110: aload_0
    //   111: getfield mCursors : [Landroid/database/Cursor;
    //   114: astore #4
    //   116: aload #4
    //   118: iload #6
    //   120: aaload
    //   121: ifnonnull -> 127
    //   124: goto -> 150
    //   127: aload #4
    //   129: iload #6
    //   131: aaload
    //   132: aload_0
    //   133: getfield mCurRowNumCache : [[I
    //   136: aload_0
    //   137: getfield mLastCacheHit : I
    //   140: aaload
    //   141: iload #6
    //   143: iaload
    //   144: invokeinterface moveToPosition : (I)Z
    //   149: pop
    //   150: iinc #6, 1
    //   153: goto -> 103
    //   156: iload_2
    //   157: iload_1
    //   158: if_icmplt -> 169
    //   161: iload_1
    //   162: istore #6
    //   164: iload_1
    //   165: iconst_m1
    //   166: if_icmpne -> 212
    //   169: iconst_0
    //   170: istore_1
    //   171: iload_1
    //   172: iload #5
    //   174: if_icmpge -> 209
    //   177: aload_0
    //   178: getfield mCursors : [Landroid/database/Cursor;
    //   181: astore #4
    //   183: aload #4
    //   185: iload_1
    //   186: aaload
    //   187: ifnonnull -> 193
    //   190: goto -> 203
    //   193: aload #4
    //   195: iload_1
    //   196: aaload
    //   197: invokeinterface moveToFirst : ()Z
    //   202: pop
    //   203: iinc #1, 1
    //   206: goto -> 171
    //   209: iconst_0
    //   210: istore #6
    //   212: iload #6
    //   214: istore_1
    //   215: iload #6
    //   217: ifge -> 222
    //   220: iconst_0
    //   221: istore_1
    //   222: iconst_m1
    //   223: istore #6
    //   225: iload_1
    //   226: istore #7
    //   228: iload #6
    //   230: istore_1
    //   231: iload #7
    //   233: iload_2
    //   234: if_icmpgt -> 398
    //   237: ldc ''
    //   239: astore #4
    //   241: iconst_m1
    //   242: istore_1
    //   243: iconst_0
    //   244: istore #6
    //   246: iload #6
    //   248: iload #5
    //   250: if_icmpge -> 360
    //   253: aload_0
    //   254: getfield mCursors : [Landroid/database/Cursor;
    //   257: astore #8
    //   259: iload_1
    //   260: istore #9
    //   262: aload #4
    //   264: astore #10
    //   266: aload #8
    //   268: iload #6
    //   270: aaload
    //   271: ifnull -> 347
    //   274: aload #8
    //   276: iload #6
    //   278: aaload
    //   279: invokeinterface isAfterLast : ()Z
    //   284: ifeq -> 297
    //   287: iload_1
    //   288: istore #9
    //   290: aload #4
    //   292: astore #10
    //   294: goto -> 347
    //   297: aload_0
    //   298: getfield mCursors : [Landroid/database/Cursor;
    //   301: iload #6
    //   303: aaload
    //   304: aload_0
    //   305: getfield mSortColumns : [I
    //   308: iload #6
    //   310: iaload
    //   311: invokeinterface getString : (I)Ljava/lang/String;
    //   316: astore #8
    //   318: iload_1
    //   319: iflt -> 339
    //   322: iload_1
    //   323: istore #9
    //   325: aload #4
    //   327: astore #10
    //   329: aload #8
    //   331: aload #4
    //   333: invokevirtual compareToIgnoreCase : (Ljava/lang/String;)I
    //   336: ifge -> 347
    //   339: aload #8
    //   341: astore #10
    //   343: iload #6
    //   345: istore #9
    //   347: iinc #6, 1
    //   350: iload #9
    //   352: istore_1
    //   353: aload #10
    //   355: astore #4
    //   357: goto -> 246
    //   360: iload #7
    //   362: iload_2
    //   363: if_icmpne -> 369
    //   366: goto -> 398
    //   369: aload_0
    //   370: getfield mCursors : [Landroid/database/Cursor;
    //   373: astore #4
    //   375: aload #4
    //   377: iload_1
    //   378: aaload
    //   379: ifnull -> 392
    //   382: aload #4
    //   384: iload_1
    //   385: aaload
    //   386: invokeinterface moveToNext : ()Z
    //   391: pop
    //   392: iinc #7, 1
    //   395: goto -> 231
    //   398: aload_0
    //   399: aload_0
    //   400: getfield mCursors : [Landroid/database/Cursor;
    //   403: iload_1
    //   404: aaload
    //   405: putfield mCursor : Landroid/database/Cursor;
    //   408: aload_0
    //   409: getfield mRowNumCache : [I
    //   412: iload_3
    //   413: iload_2
    //   414: iastore
    //   415: aload_0
    //   416: getfield mCursorCache : [I
    //   419: iload_3
    //   420: iload_1
    //   421: iastore
    //   422: iconst_0
    //   423: istore_1
    //   424: iload_1
    //   425: iload #5
    //   427: if_icmpge -> 466
    //   430: aload_0
    //   431: getfield mCursors : [Landroid/database/Cursor;
    //   434: astore #4
    //   436: aload #4
    //   438: iload_1
    //   439: aaload
    //   440: ifnull -> 460
    //   443: aload_0
    //   444: getfield mCurRowNumCache : [[I
    //   447: iload_3
    //   448: aaload
    //   449: iload_1
    //   450: aload #4
    //   452: iload_1
    //   453: aaload
    //   454: invokeinterface getPosition : ()I
    //   459: iastore
    //   460: iinc #1, 1
    //   463: goto -> 424
    //   466: aload_0
    //   467: iconst_m1
    //   468: putfield mLastCacheHit : I
    //   471: iconst_1
    //   472: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #110	-> 0
    //   #111	-> 5
    //   #121	-> 7
    //   #123	-> 12
    //   #124	-> 22
    //   #125	-> 29
    //   #126	-> 43
    //   #127	-> 48
    //   #128	-> 56
    //   #130	-> 58
    //   #131	-> 74
    //   #132	-> 79
    //   #135	-> 81
    //   #136	-> 86
    //   #138	-> 93
    //   #139	-> 100
    //   #140	-> 110
    //   #141	-> 127
    //   #139	-> 150
    //   #145	-> 156
    //   #146	-> 169
    //   #147	-> 177
    //   #148	-> 193
    //   #146	-> 203
    //   #150	-> 209
    //   #152	-> 212
    //   #153	-> 220
    //   #157	-> 222
    //   #158	-> 225
    //   #159	-> 237
    //   #160	-> 241
    //   #161	-> 243
    //   #162	-> 253
    //   #163	-> 287
    //   #165	-> 297
    //   #166	-> 318
    //   #167	-> 339
    //   #168	-> 343
    //   #161	-> 347
    //   #171	-> 360
    //   #172	-> 369
    //   #173	-> 382
    //   #158	-> 392
    //   #176	-> 398
    //   #177	-> 408
    //   #178	-> 415
    //   #179	-> 422
    //   #180	-> 430
    //   #181	-> 443
    //   #179	-> 460
    //   #184	-> 466
    //   #185	-> 471
  }
  
  public String getString(int paramInt) {
    return this.mCursor.getString(paramInt);
  }
  
  public short getShort(int paramInt) {
    return this.mCursor.getShort(paramInt);
  }
  
  public int getInt(int paramInt) {
    return this.mCursor.getInt(paramInt);
  }
  
  public long getLong(int paramInt) {
    return this.mCursor.getLong(paramInt);
  }
  
  public float getFloat(int paramInt) {
    return this.mCursor.getFloat(paramInt);
  }
  
  public double getDouble(int paramInt) {
    return this.mCursor.getDouble(paramInt);
  }
  
  public int getType(int paramInt) {
    return this.mCursor.getType(paramInt);
  }
  
  public boolean isNull(int paramInt) {
    return this.mCursor.isNull(paramInt);
  }
  
  public byte[] getBlob(int paramInt) {
    return this.mCursor.getBlob(paramInt);
  }
  
  public String[] getColumnNames() {
    Cursor cursor = this.mCursor;
    if (cursor != null)
      return cursor.getColumnNames(); 
    int i = this.mCursors.length;
    for (byte b = 0; b < i; b++) {
      Cursor[] arrayOfCursor = this.mCursors;
      if (arrayOfCursor[b] != null)
        return arrayOfCursor[b].getColumnNames(); 
    } 
    throw new IllegalStateException("No cursor that can return names");
  }
  
  public void deactivate() {
    int i = this.mCursors.length;
    for (byte b = 0; b < i; b++) {
      Cursor[] arrayOfCursor = this.mCursors;
      if (arrayOfCursor[b] != null)
        arrayOfCursor[b].deactivate(); 
    } 
  }
  
  public void close() {
    int i = this.mCursors.length;
    for (byte b = 0; b < i; b++) {
      Cursor[] arrayOfCursor = this.mCursors;
      if (arrayOfCursor[b] != null)
        arrayOfCursor[b].close(); 
    } 
  }
  
  public void registerDataSetObserver(DataSetObserver paramDataSetObserver) {
    int i = this.mCursors.length;
    for (byte b = 0; b < i; b++) {
      Cursor[] arrayOfCursor = this.mCursors;
      if (arrayOfCursor[b] != null)
        arrayOfCursor[b].registerDataSetObserver(paramDataSetObserver); 
    } 
  }
  
  public void unregisterDataSetObserver(DataSetObserver paramDataSetObserver) {
    int i = this.mCursors.length;
    for (byte b = 0; b < i; b++) {
      Cursor[] arrayOfCursor = this.mCursors;
      if (arrayOfCursor[b] != null)
        arrayOfCursor[b].unregisterDataSetObserver(paramDataSetObserver); 
    } 
  }
  
  public boolean requery() {
    int i = this.mCursors.length;
    for (byte b = 0; b < i; b++) {
      Cursor[] arrayOfCursor = this.mCursors;
      if (arrayOfCursor[b] != null)
        if (!arrayOfCursor[b].requery())
          return false;  
    } 
    return true;
  }
}
