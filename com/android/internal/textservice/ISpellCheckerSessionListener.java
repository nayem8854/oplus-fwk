package com.android.internal.textservice;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.view.textservice.SentenceSuggestionsInfo;
import android.view.textservice.SuggestionsInfo;

public interface ISpellCheckerSessionListener extends IInterface {
  void onGetSentenceSuggestions(SentenceSuggestionsInfo[] paramArrayOfSentenceSuggestionsInfo) throws RemoteException;
  
  void onGetSuggestions(SuggestionsInfo[] paramArrayOfSuggestionsInfo) throws RemoteException;
  
  class Default implements ISpellCheckerSessionListener {
    public void onGetSuggestions(SuggestionsInfo[] param1ArrayOfSuggestionsInfo) throws RemoteException {}
    
    public void onGetSentenceSuggestions(SentenceSuggestionsInfo[] param1ArrayOfSentenceSuggestionsInfo) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISpellCheckerSessionListener {
    private static final String DESCRIPTOR = "com.android.internal.textservice.ISpellCheckerSessionListener";
    
    static final int TRANSACTION_onGetSentenceSuggestions = 2;
    
    static final int TRANSACTION_onGetSuggestions = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.textservice.ISpellCheckerSessionListener");
    }
    
    public static ISpellCheckerSessionListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.textservice.ISpellCheckerSessionListener");
      if (iInterface != null && iInterface instanceof ISpellCheckerSessionListener)
        return (ISpellCheckerSessionListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onGetSentenceSuggestions";
      } 
      return "onGetSuggestions";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      SentenceSuggestionsInfo[] arrayOfSentenceSuggestionsInfo;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("com.android.internal.textservice.ISpellCheckerSessionListener");
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.internal.textservice.ISpellCheckerSessionListener");
        arrayOfSentenceSuggestionsInfo = (SentenceSuggestionsInfo[])param1Parcel1.createTypedArray(SentenceSuggestionsInfo.CREATOR);
        onGetSentenceSuggestions(arrayOfSentenceSuggestionsInfo);
        return true;
      } 
      arrayOfSentenceSuggestionsInfo.enforceInterface("com.android.internal.textservice.ISpellCheckerSessionListener");
      SuggestionsInfo[] arrayOfSuggestionsInfo = (SuggestionsInfo[])arrayOfSentenceSuggestionsInfo.createTypedArray(SuggestionsInfo.CREATOR);
      onGetSuggestions(arrayOfSuggestionsInfo);
      return true;
    }
    
    private static class Proxy implements ISpellCheckerSessionListener {
      public static ISpellCheckerSessionListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.textservice.ISpellCheckerSessionListener";
      }
      
      public void onGetSuggestions(SuggestionsInfo[] param2ArrayOfSuggestionsInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.textservice.ISpellCheckerSessionListener");
          parcel.writeTypedArray((Parcelable[])param2ArrayOfSuggestionsInfo, 0);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && ISpellCheckerSessionListener.Stub.getDefaultImpl() != null) {
            ISpellCheckerSessionListener.Stub.getDefaultImpl().onGetSuggestions(param2ArrayOfSuggestionsInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onGetSentenceSuggestions(SentenceSuggestionsInfo[] param2ArrayOfSentenceSuggestionsInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.textservice.ISpellCheckerSessionListener");
          parcel.writeTypedArray((Parcelable[])param2ArrayOfSentenceSuggestionsInfo, 0);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && ISpellCheckerSessionListener.Stub.getDefaultImpl() != null) {
            ISpellCheckerSessionListener.Stub.getDefaultImpl().onGetSentenceSuggestions(param2ArrayOfSentenceSuggestionsInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISpellCheckerSessionListener param1ISpellCheckerSessionListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISpellCheckerSessionListener != null) {
          Proxy.sDefaultImpl = param1ISpellCheckerSessionListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISpellCheckerSessionListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
