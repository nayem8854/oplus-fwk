package com.android.internal.textservice;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.view.textservice.TextInfo;

public interface ISpellCheckerSession extends IInterface {
  void onCancel() throws RemoteException;
  
  void onClose() throws RemoteException;
  
  void onGetSentenceSuggestionsMultiple(TextInfo[] paramArrayOfTextInfo, int paramInt) throws RemoteException;
  
  void onGetSuggestionsMultiple(TextInfo[] paramArrayOfTextInfo, int paramInt, boolean paramBoolean) throws RemoteException;
  
  class Default implements ISpellCheckerSession {
    public void onGetSuggestionsMultiple(TextInfo[] param1ArrayOfTextInfo, int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void onGetSentenceSuggestionsMultiple(TextInfo[] param1ArrayOfTextInfo, int param1Int) throws RemoteException {}
    
    public void onCancel() throws RemoteException {}
    
    public void onClose() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISpellCheckerSession {
    private static final String DESCRIPTOR = "com.android.internal.textservice.ISpellCheckerSession";
    
    static final int TRANSACTION_onCancel = 3;
    
    static final int TRANSACTION_onClose = 4;
    
    static final int TRANSACTION_onGetSentenceSuggestionsMultiple = 2;
    
    static final int TRANSACTION_onGetSuggestionsMultiple = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.textservice.ISpellCheckerSession");
    }
    
    public static ISpellCheckerSession asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.textservice.ISpellCheckerSession");
      if (iInterface != null && iInterface instanceof ISpellCheckerSession)
        return (ISpellCheckerSession)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "onClose";
          } 
          return "onCancel";
        } 
        return "onGetSentenceSuggestionsMultiple";
      } 
      return "onGetSuggestionsMultiple";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("com.android.internal.textservice.ISpellCheckerSession");
              return true;
            } 
            param1Parcel1.enforceInterface("com.android.internal.textservice.ISpellCheckerSession");
            onClose();
            return true;
          } 
          param1Parcel1.enforceInterface("com.android.internal.textservice.ISpellCheckerSession");
          onCancel();
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.internal.textservice.ISpellCheckerSession");
        TextInfo[] arrayOfTextInfo1 = (TextInfo[])param1Parcel1.createTypedArray(TextInfo.CREATOR);
        param1Int1 = param1Parcel1.readInt();
        onGetSentenceSuggestionsMultiple(arrayOfTextInfo1, param1Int1);
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.textservice.ISpellCheckerSession");
      TextInfo[] arrayOfTextInfo = (TextInfo[])param1Parcel1.createTypedArray(TextInfo.CREATOR);
      param1Int1 = param1Parcel1.readInt();
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      onGetSuggestionsMultiple(arrayOfTextInfo, param1Int1, bool);
      return true;
    }
    
    private static class Proxy implements ISpellCheckerSession {
      public static ISpellCheckerSession sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.textservice.ISpellCheckerSession";
      }
      
      public void onGetSuggestionsMultiple(TextInfo[] param2ArrayOfTextInfo, int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.textservice.ISpellCheckerSession");
          boolean bool = false;
          parcel.writeTypedArray((Parcelable[])param2ArrayOfTextInfo, 0);
          parcel.writeInt(param2Int);
          if (param2Boolean)
            bool = true; 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, null, 1);
          if (!bool1 && ISpellCheckerSession.Stub.getDefaultImpl() != null) {
            ISpellCheckerSession.Stub.getDefaultImpl().onGetSuggestionsMultiple(param2ArrayOfTextInfo, param2Int, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onGetSentenceSuggestionsMultiple(TextInfo[] param2ArrayOfTextInfo, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.textservice.ISpellCheckerSession");
          parcel.writeTypedArray((Parcelable[])param2ArrayOfTextInfo, 0);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && ISpellCheckerSession.Stub.getDefaultImpl() != null) {
            ISpellCheckerSession.Stub.getDefaultImpl().onGetSentenceSuggestionsMultiple(param2ArrayOfTextInfo, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onCancel() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.textservice.ISpellCheckerSession");
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && ISpellCheckerSession.Stub.getDefaultImpl() != null) {
            ISpellCheckerSession.Stub.getDefaultImpl().onCancel();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onClose() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.textservice.ISpellCheckerSession");
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && ISpellCheckerSession.Stub.getDefaultImpl() != null) {
            ISpellCheckerSession.Stub.getDefaultImpl().onClose();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISpellCheckerSession param1ISpellCheckerSession) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISpellCheckerSession != null) {
          Proxy.sDefaultImpl = param1ISpellCheckerSession;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISpellCheckerSession getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
