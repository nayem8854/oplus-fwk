package com.android.internal.textservice;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ISpellCheckerServiceCallback extends IInterface {
  void onSessionCreated(ISpellCheckerSession paramISpellCheckerSession) throws RemoteException;
  
  class Default implements ISpellCheckerServiceCallback {
    public void onSessionCreated(ISpellCheckerSession param1ISpellCheckerSession) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISpellCheckerServiceCallback {
    private static final String DESCRIPTOR = "com.android.internal.textservice.ISpellCheckerServiceCallback";
    
    static final int TRANSACTION_onSessionCreated = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.textservice.ISpellCheckerServiceCallback");
    }
    
    public static ISpellCheckerServiceCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.textservice.ISpellCheckerServiceCallback");
      if (iInterface != null && iInterface instanceof ISpellCheckerServiceCallback)
        return (ISpellCheckerServiceCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onSessionCreated";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.textservice.ISpellCheckerServiceCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.textservice.ISpellCheckerServiceCallback");
      ISpellCheckerSession iSpellCheckerSession = ISpellCheckerSession.Stub.asInterface(param1Parcel1.readStrongBinder());
      onSessionCreated(iSpellCheckerSession);
      return true;
    }
    
    private static class Proxy implements ISpellCheckerServiceCallback {
      public static ISpellCheckerServiceCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.textservice.ISpellCheckerServiceCallback";
      }
      
      public void onSessionCreated(ISpellCheckerSession param2ISpellCheckerSession) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.textservice.ISpellCheckerServiceCallback");
          if (param2ISpellCheckerSession != null) {
            iBinder = param2ISpellCheckerSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && ISpellCheckerServiceCallback.Stub.getDefaultImpl() != null) {
            ISpellCheckerServiceCallback.Stub.getDefaultImpl().onSessionCreated(param2ISpellCheckerSession);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISpellCheckerServiceCallback param1ISpellCheckerServiceCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISpellCheckerServiceCallback != null) {
          Proxy.sDefaultImpl = param1ISpellCheckerServiceCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISpellCheckerServiceCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
