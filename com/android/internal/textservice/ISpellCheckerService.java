package com.android.internal.textservice;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ISpellCheckerService extends IInterface {
  void getISpellCheckerSession(String paramString, ISpellCheckerSessionListener paramISpellCheckerSessionListener, Bundle paramBundle, ISpellCheckerServiceCallback paramISpellCheckerServiceCallback) throws RemoteException;
  
  class Default implements ISpellCheckerService {
    public void getISpellCheckerSession(String param1String, ISpellCheckerSessionListener param1ISpellCheckerSessionListener, Bundle param1Bundle, ISpellCheckerServiceCallback param1ISpellCheckerServiceCallback) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISpellCheckerService {
    private static final String DESCRIPTOR = "com.android.internal.textservice.ISpellCheckerService";
    
    static final int TRANSACTION_getISpellCheckerSession = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.textservice.ISpellCheckerService");
    }
    
    public static ISpellCheckerService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.textservice.ISpellCheckerService");
      if (iInterface != null && iInterface instanceof ISpellCheckerService)
        return (ISpellCheckerService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "getISpellCheckerSession";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.textservice.ISpellCheckerService");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.textservice.ISpellCheckerService");
      String str = param1Parcel1.readString();
      ISpellCheckerSessionListener iSpellCheckerSessionListener = ISpellCheckerSessionListener.Stub.asInterface(param1Parcel1.readStrongBinder());
      if (param1Parcel1.readInt() != 0) {
        Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      ISpellCheckerServiceCallback iSpellCheckerServiceCallback = ISpellCheckerServiceCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
      getISpellCheckerSession(str, iSpellCheckerSessionListener, (Bundle)param1Parcel2, iSpellCheckerServiceCallback);
      return true;
    }
    
    private static class Proxy implements ISpellCheckerService {
      public static ISpellCheckerService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.textservice.ISpellCheckerService";
      }
      
      public void getISpellCheckerSession(String param2String, ISpellCheckerSessionListener param2ISpellCheckerSessionListener, Bundle param2Bundle, ISpellCheckerServiceCallback param2ISpellCheckerServiceCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.textservice.ISpellCheckerService");
          parcel.writeString(param2String);
          if (param2ISpellCheckerSessionListener != null) {
            iBinder = param2ISpellCheckerSessionListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ISpellCheckerServiceCallback != null) {
            iBinder = param2ISpellCheckerServiceCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && ISpellCheckerService.Stub.getDefaultImpl() != null) {
            ISpellCheckerService.Stub.getDefaultImpl().getISpellCheckerSession(param2String, param2ISpellCheckerSessionListener, param2Bundle, param2ISpellCheckerServiceCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISpellCheckerService param1ISpellCheckerService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISpellCheckerService != null) {
          Proxy.sDefaultImpl = param1ISpellCheckerService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISpellCheckerService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
