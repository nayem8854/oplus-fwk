package com.android.internal.textservice;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.view.textservice.SpellCheckerInfo;
import android.view.textservice.SpellCheckerSubtype;

public interface ITextServicesManager extends IInterface {
  void finishSpellCheckerService(int paramInt, ISpellCheckerSessionListener paramISpellCheckerSessionListener) throws RemoteException;
  
  SpellCheckerInfo getCurrentSpellChecker(int paramInt, String paramString) throws RemoteException;
  
  SpellCheckerSubtype getCurrentSpellCheckerSubtype(int paramInt, boolean paramBoolean) throws RemoteException;
  
  SpellCheckerInfo[] getEnabledSpellCheckers(int paramInt) throws RemoteException;
  
  void getSpellCheckerService(int paramInt, String paramString1, String paramString2, ITextServicesSessionListener paramITextServicesSessionListener, ISpellCheckerSessionListener paramISpellCheckerSessionListener, Bundle paramBundle) throws RemoteException;
  
  boolean isSpellCheckerEnabled(int paramInt) throws RemoteException;
  
  class Default implements ITextServicesManager {
    public SpellCheckerInfo getCurrentSpellChecker(int param1Int, String param1String) throws RemoteException {
      return null;
    }
    
    public SpellCheckerSubtype getCurrentSpellCheckerSubtype(int param1Int, boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public void getSpellCheckerService(int param1Int, String param1String1, String param1String2, ITextServicesSessionListener param1ITextServicesSessionListener, ISpellCheckerSessionListener param1ISpellCheckerSessionListener, Bundle param1Bundle) throws RemoteException {}
    
    public void finishSpellCheckerService(int param1Int, ISpellCheckerSessionListener param1ISpellCheckerSessionListener) throws RemoteException {}
    
    public boolean isSpellCheckerEnabled(int param1Int) throws RemoteException {
      return false;
    }
    
    public SpellCheckerInfo[] getEnabledSpellCheckers(int param1Int) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITextServicesManager {
    private static final String DESCRIPTOR = "com.android.internal.textservice.ITextServicesManager";
    
    static final int TRANSACTION_finishSpellCheckerService = 4;
    
    static final int TRANSACTION_getCurrentSpellChecker = 1;
    
    static final int TRANSACTION_getCurrentSpellCheckerSubtype = 2;
    
    static final int TRANSACTION_getEnabledSpellCheckers = 6;
    
    static final int TRANSACTION_getSpellCheckerService = 3;
    
    static final int TRANSACTION_isSpellCheckerEnabled = 5;
    
    public Stub() {
      attachInterface(this, "com.android.internal.textservice.ITextServicesManager");
    }
    
    public static ITextServicesManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.textservice.ITextServicesManager");
      if (iInterface != null && iInterface instanceof ITextServicesManager)
        return (ITextServicesManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 6:
          return "getEnabledSpellCheckers";
        case 5:
          return "isSpellCheckerEnabled";
        case 4:
          return "finishSpellCheckerService";
        case 3:
          return "getSpellCheckerService";
        case 2:
          return "getCurrentSpellCheckerSubtype";
        case 1:
          break;
      } 
      return "getCurrentSpellChecker";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      ITextServicesSessionListener iTextServicesSessionListener;
      if (param1Int1 != 1598968902) {
        boolean bool;
        SpellCheckerInfo[] arrayOfSpellCheckerInfo;
        ISpellCheckerSessionListener iSpellCheckerSessionListener1;
        SpellCheckerSubtype spellCheckerSubtype;
        String str2, str3;
        ISpellCheckerSessionListener iSpellCheckerSessionListener2;
        boolean bool1;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 6:
            param1Parcel1.enforceInterface("com.android.internal.textservice.ITextServicesManager");
            param1Int1 = param1Parcel1.readInt();
            arrayOfSpellCheckerInfo = getEnabledSpellCheckers(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray((Parcelable[])arrayOfSpellCheckerInfo, 1);
            return true;
          case 5:
            arrayOfSpellCheckerInfo.enforceInterface("com.android.internal.textservice.ITextServicesManager");
            param1Int1 = arrayOfSpellCheckerInfo.readInt();
            bool = isSpellCheckerEnabled(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 4:
            arrayOfSpellCheckerInfo.enforceInterface("com.android.internal.textservice.ITextServicesManager");
            i = arrayOfSpellCheckerInfo.readInt();
            iSpellCheckerSessionListener1 = ISpellCheckerSessionListener.Stub.asInterface(arrayOfSpellCheckerInfo.readStrongBinder());
            finishSpellCheckerService(i, iSpellCheckerSessionListener1);
            return true;
          case 3:
            iSpellCheckerSessionListener1.enforceInterface("com.android.internal.textservice.ITextServicesManager");
            i = iSpellCheckerSessionListener1.readInt();
            str2 = iSpellCheckerSessionListener1.readString();
            str3 = iSpellCheckerSessionListener1.readString();
            iTextServicesSessionListener = ITextServicesSessionListener.Stub.asInterface(iSpellCheckerSessionListener1.readStrongBinder());
            iSpellCheckerSessionListener2 = ISpellCheckerSessionListener.Stub.asInterface(iSpellCheckerSessionListener1.readStrongBinder());
            if (iSpellCheckerSessionListener1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iSpellCheckerSessionListener1);
            } else {
              iSpellCheckerSessionListener1 = null;
            } 
            getSpellCheckerService(i, str2, str3, iTextServicesSessionListener, iSpellCheckerSessionListener2, (Bundle)iSpellCheckerSessionListener1);
            return true;
          case 2:
            iSpellCheckerSessionListener1.enforceInterface("com.android.internal.textservice.ITextServicesManager");
            i = iSpellCheckerSessionListener1.readInt();
            if (iSpellCheckerSessionListener1.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            spellCheckerSubtype = getCurrentSpellCheckerSubtype(i, bool1);
            iTextServicesSessionListener.writeNoException();
            if (spellCheckerSubtype != null) {
              iTextServicesSessionListener.writeInt(1);
              spellCheckerSubtype.writeToParcel((Parcel)iTextServicesSessionListener, 1);
            } else {
              iTextServicesSessionListener.writeInt(0);
            } 
            return true;
          case 1:
            break;
        } 
        spellCheckerSubtype.enforceInterface("com.android.internal.textservice.ITextServicesManager");
        int i = spellCheckerSubtype.readInt();
        String str1 = spellCheckerSubtype.readString();
        SpellCheckerInfo spellCheckerInfo = getCurrentSpellChecker(i, str1);
        iTextServicesSessionListener.writeNoException();
        if (spellCheckerInfo != null) {
          iTextServicesSessionListener.writeInt(1);
          spellCheckerInfo.writeToParcel((Parcel)iTextServicesSessionListener, 1);
        } else {
          iTextServicesSessionListener.writeInt(0);
        } 
        return true;
      } 
      iTextServicesSessionListener.writeString("com.android.internal.textservice.ITextServicesManager");
      return true;
    }
    
    private static class Proxy implements ITextServicesManager {
      public static ITextServicesManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.textservice.ITextServicesManager";
      }
      
      public SpellCheckerInfo getCurrentSpellChecker(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.textservice.ITextServicesManager");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ITextServicesManager.Stub.getDefaultImpl() != null)
            return ITextServicesManager.Stub.getDefaultImpl().getCurrentSpellChecker(param2Int, param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            SpellCheckerInfo spellCheckerInfo = (SpellCheckerInfo)SpellCheckerInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (SpellCheckerInfo)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public SpellCheckerSubtype getCurrentSpellCheckerSubtype(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          SpellCheckerSubtype spellCheckerSubtype;
          parcel1.writeInterfaceToken("com.android.internal.textservice.ITextServicesManager");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool1 && ITextServicesManager.Stub.getDefaultImpl() != null) {
            spellCheckerSubtype = ITextServicesManager.Stub.getDefaultImpl().getCurrentSpellCheckerSubtype(param2Int, param2Boolean);
            return spellCheckerSubtype;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            spellCheckerSubtype = (SpellCheckerSubtype)SpellCheckerSubtype.CREATOR.createFromParcel(parcel2);
          } else {
            spellCheckerSubtype = null;
          } 
          return spellCheckerSubtype;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void getSpellCheckerService(int param2Int, String param2String1, String param2String2, ITextServicesSessionListener param2ITextServicesSessionListener, ISpellCheckerSessionListener param2ISpellCheckerSessionListener, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.textservice.ITextServicesManager");
          try {
            parcel.writeInt(param2Int);
            try {
              parcel.writeString(param2String1);
              try {
                IBinder iBinder;
                parcel.writeString(param2String2);
                if (param2ITextServicesSessionListener != null) {
                  iBinder = param2ITextServicesSessionListener.asBinder();
                } else {
                  iBinder = null;
                } 
                parcel.writeStrongBinder(iBinder);
                if (param2ISpellCheckerSessionListener != null) {
                  iBinder = param2ISpellCheckerSessionListener.asBinder();
                } else {
                  iBinder = null;
                } 
                parcel.writeStrongBinder(iBinder);
                if (param2Bundle != null) {
                  parcel.writeInt(1);
                  param2Bundle.writeToParcel(parcel, 0);
                } else {
                  parcel.writeInt(0);
                } 
                try {
                  boolean bool = this.mRemote.transact(3, parcel, null, 1);
                  if (!bool && ITextServicesManager.Stub.getDefaultImpl() != null) {
                    ITextServicesManager.Stub.getDefaultImpl().getSpellCheckerService(param2Int, param2String1, param2String2, param2ITextServicesSessionListener, param2ISpellCheckerSessionListener, param2Bundle);
                    parcel.recycle();
                    return;
                  } 
                  parcel.recycle();
                  return;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2String1;
      }
      
      public void finishSpellCheckerService(int param2Int, ISpellCheckerSessionListener param2ISpellCheckerSessionListener) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.textservice.ITextServicesManager");
          parcel.writeInt(param2Int);
          if (param2ISpellCheckerSessionListener != null) {
            iBinder = param2ISpellCheckerSessionListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && ITextServicesManager.Stub.getDefaultImpl() != null) {
            ITextServicesManager.Stub.getDefaultImpl().finishSpellCheckerService(param2Int, param2ISpellCheckerSessionListener);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public boolean isSpellCheckerEnabled(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.textservice.ITextServicesManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && ITextServicesManager.Stub.getDefaultImpl() != null) {
            bool1 = ITextServicesManager.Stub.getDefaultImpl().isSpellCheckerEnabled(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public SpellCheckerInfo[] getEnabledSpellCheckers(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.textservice.ITextServicesManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && ITextServicesManager.Stub.getDefaultImpl() != null)
            return ITextServicesManager.Stub.getDefaultImpl().getEnabledSpellCheckers(param2Int); 
          parcel2.readException();
          return (SpellCheckerInfo[])parcel2.createTypedArray(SpellCheckerInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITextServicesManager param1ITextServicesManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITextServicesManager != null) {
          Proxy.sDefaultImpl = param1ITextServicesManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITextServicesManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
