package com.android.internal.inputmethod;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.inputmethod.ExtractedText;

public interface IExtractedTextResultCallback extends IInterface {
  void onResult(ExtractedText paramExtractedText) throws RemoteException;
  
  class Default implements IExtractedTextResultCallback {
    public void onResult(ExtractedText param1ExtractedText) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IExtractedTextResultCallback {
    private static final String DESCRIPTOR = "com.android.internal.inputmethod.IExtractedTextResultCallback";
    
    static final int TRANSACTION_onResult = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.inputmethod.IExtractedTextResultCallback");
    }
    
    public static IExtractedTextResultCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.inputmethod.IExtractedTextResultCallback");
      if (iInterface != null && iInterface instanceof IExtractedTextResultCallback)
        return (IExtractedTextResultCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onResult";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.inputmethod.IExtractedTextResultCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.inputmethod.IExtractedTextResultCallback");
      if (param1Parcel1.readInt() != 0) {
        ExtractedText extractedText = (ExtractedText)ExtractedText.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onResult((ExtractedText)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IExtractedTextResultCallback {
      public static IExtractedTextResultCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.inputmethod.IExtractedTextResultCallback";
      }
      
      public void onResult(ExtractedText param2ExtractedText) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.inputmethod.IExtractedTextResultCallback");
          if (param2ExtractedText != null) {
            parcel.writeInt(1);
            param2ExtractedText.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IExtractedTextResultCallback.Stub.getDefaultImpl() != null) {
            IExtractedTextResultCallback.Stub.getDefaultImpl().onResult(param2ExtractedText);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IExtractedTextResultCallback param1IExtractedTextResultCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IExtractedTextResultCallback != null) {
          Proxy.sDefaultImpl = param1IExtractedTextResultCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IExtractedTextResultCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
