package com.android.internal.inputmethod;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IMultiClientInputMethod extends IInterface {
  void addClient(int paramInt1, int paramInt2, int paramInt3, int paramInt4) throws RemoteException;
  
  void initialize(IMultiClientInputMethodPrivilegedOperations paramIMultiClientInputMethodPrivilegedOperations) throws RemoteException;
  
  void removeClient(int paramInt) throws RemoteException;
  
  class Default implements IMultiClientInputMethod {
    public void initialize(IMultiClientInputMethodPrivilegedOperations param1IMultiClientInputMethodPrivilegedOperations) throws RemoteException {}
    
    public void addClient(int param1Int1, int param1Int2, int param1Int3, int param1Int4) throws RemoteException {}
    
    public void removeClient(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMultiClientInputMethod {
    private static final String DESCRIPTOR = "com.android.internal.inputmethod.IMultiClientInputMethod";
    
    static final int TRANSACTION_addClient = 2;
    
    static final int TRANSACTION_initialize = 1;
    
    static final int TRANSACTION_removeClient = 3;
    
    public Stub() {
      attachInterface(this, "com.android.internal.inputmethod.IMultiClientInputMethod");
    }
    
    public static IMultiClientInputMethod asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.inputmethod.IMultiClientInputMethod");
      if (iInterface != null && iInterface instanceof IMultiClientInputMethod)
        return (IMultiClientInputMethod)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "removeClient";
        } 
        return "addClient";
      } 
      return "initialize";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("com.android.internal.inputmethod.IMultiClientInputMethod");
            return true;
          } 
          param1Parcel1.enforceInterface("com.android.internal.inputmethod.IMultiClientInputMethod");
          param1Int1 = param1Parcel1.readInt();
          removeClient(param1Int1);
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.internal.inputmethod.IMultiClientInputMethod");
        int i = param1Parcel1.readInt();
        param1Int1 = param1Parcel1.readInt();
        int j = param1Parcel1.readInt();
        param1Int2 = param1Parcel1.readInt();
        addClient(i, param1Int1, j, param1Int2);
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.inputmethod.IMultiClientInputMethod");
      IMultiClientInputMethodPrivilegedOperations iMultiClientInputMethodPrivilegedOperations = IMultiClientInputMethodPrivilegedOperations.Stub.asInterface(param1Parcel1.readStrongBinder());
      initialize(iMultiClientInputMethodPrivilegedOperations);
      return true;
    }
    
    private static class Proxy implements IMultiClientInputMethod {
      public static IMultiClientInputMethod sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.inputmethod.IMultiClientInputMethod";
      }
      
      public void initialize(IMultiClientInputMethodPrivilegedOperations param2IMultiClientInputMethodPrivilegedOperations) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.inputmethod.IMultiClientInputMethod");
          if (param2IMultiClientInputMethodPrivilegedOperations != null) {
            iBinder = param2IMultiClientInputMethodPrivilegedOperations.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IMultiClientInputMethod.Stub.getDefaultImpl() != null) {
            IMultiClientInputMethod.Stub.getDefaultImpl().initialize(param2IMultiClientInputMethodPrivilegedOperations);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void addClient(int param2Int1, int param2Int2, int param2Int3, int param2Int4) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.inputmethod.IMultiClientInputMethod");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          parcel.writeInt(param2Int4);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IMultiClientInputMethod.Stub.getDefaultImpl() != null) {
            IMultiClientInputMethod.Stub.getDefaultImpl().addClient(param2Int1, param2Int2, param2Int3, param2Int4);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void removeClient(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.inputmethod.IMultiClientInputMethod");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IMultiClientInputMethod.Stub.getDefaultImpl() != null) {
            IMultiClientInputMethod.Stub.getDefaultImpl().removeClient(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMultiClientInputMethod param1IMultiClientInputMethod) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMultiClientInputMethod != null) {
          Proxy.sDefaultImpl = param1IMultiClientInputMethod;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMultiClientInputMethod getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
