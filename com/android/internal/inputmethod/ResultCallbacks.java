package com.android.internal.inputmethod;

import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicReference;

public final class ResultCallbacks {
  private static <T> T unwrap(AtomicReference<WeakReference<T>> paramAtomicReference) {
    WeakReference<Object> weakReference = (WeakReference)paramAtomicReference.getAndSet(null);
    if (weakReference == null)
      return null; 
    T t = (T)weakReference.get();
    weakReference.clear();
    return t;
  }
  
  public static IIntResultCallback.Stub of(CancellationGroup.Completable.Int paramInt) {
    AtomicReference atomicReference = new AtomicReference(new WeakReference<>(paramInt));
    return (IIntResultCallback.Stub)new Object(atomicReference);
  }
  
  public static ICharSequenceResultCallback.Stub of(CancellationGroup.Completable.CharSequence paramCharSequence) {
    AtomicReference atomicReference = new AtomicReference(new WeakReference<>(paramCharSequence));
    return (ICharSequenceResultCallback.Stub)new Object(atomicReference);
  }
  
  public static IExtractedTextResultCallback.Stub of(CancellationGroup.Completable.ExtractedText paramExtractedText) {
    AtomicReference atomicReference = new AtomicReference(new WeakReference<>(paramExtractedText));
    return (IExtractedTextResultCallback.Stub)new Object(atomicReference);
  }
}
