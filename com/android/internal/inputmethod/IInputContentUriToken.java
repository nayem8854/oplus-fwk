package com.android.internal.inputmethod;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IInputContentUriToken extends IInterface {
  void release() throws RemoteException;
  
  void take() throws RemoteException;
  
  class Default implements IInputContentUriToken {
    public void take() throws RemoteException {}
    
    public void release() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IInputContentUriToken {
    private static final String DESCRIPTOR = "com.android.internal.inputmethod.IInputContentUriToken";
    
    static final int TRANSACTION_release = 2;
    
    static final int TRANSACTION_take = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.inputmethod.IInputContentUriToken");
    }
    
    public static IInputContentUriToken asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.inputmethod.IInputContentUriToken");
      if (iInterface != null && iInterface instanceof IInputContentUriToken)
        return (IInputContentUriToken)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "release";
      } 
      return "take";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("com.android.internal.inputmethod.IInputContentUriToken");
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.internal.inputmethod.IInputContentUriToken");
        release();
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.inputmethod.IInputContentUriToken");
      take();
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IInputContentUriToken {
      public static IInputContentUriToken sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.inputmethod.IInputContentUriToken";
      }
      
      public void take() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.inputmethod.IInputContentUriToken");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IInputContentUriToken.Stub.getDefaultImpl() != null) {
            IInputContentUriToken.Stub.getDefaultImpl().take();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void release() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.inputmethod.IInputContentUriToken");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IInputContentUriToken.Stub.getDefaultImpl() != null) {
            IInputContentUriToken.Stub.getDefaultImpl().release();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IInputContentUriToken param1IInputContentUriToken) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IInputContentUriToken != null) {
          Proxy.sDefaultImpl = param1IInputContentUriToken;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IInputContentUriToken getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
