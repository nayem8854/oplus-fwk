package com.android.internal.inputmethod;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.text.TextUtils;

public interface ICharSequenceResultCallback extends IInterface {
  void onResult(CharSequence paramCharSequence) throws RemoteException;
  
  class Default implements ICharSequenceResultCallback {
    public void onResult(CharSequence param1CharSequence) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICharSequenceResultCallback {
    private static final String DESCRIPTOR = "com.android.internal.inputmethod.ICharSequenceResultCallback";
    
    static final int TRANSACTION_onResult = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.inputmethod.ICharSequenceResultCallback");
    }
    
    public static ICharSequenceResultCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.inputmethod.ICharSequenceResultCallback");
      if (iInterface != null && iInterface instanceof ICharSequenceResultCallback)
        return (ICharSequenceResultCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onResult";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.inputmethod.ICharSequenceResultCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.inputmethod.ICharSequenceResultCallback");
      if (param1Parcel1.readInt() != 0) {
        CharSequence charSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onResult((CharSequence)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements ICharSequenceResultCallback {
      public static ICharSequenceResultCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.inputmethod.ICharSequenceResultCallback";
      }
      
      public void onResult(CharSequence param2CharSequence) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.inputmethod.ICharSequenceResultCallback");
          if (param2CharSequence != null) {
            parcel.writeInt(1);
            TextUtils.writeToParcel(param2CharSequence, parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && ICharSequenceResultCallback.Stub.getDefaultImpl() != null) {
            ICharSequenceResultCallback.Stub.getDefaultImpl().onResult(param2CharSequence);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICharSequenceResultCallback param1ICharSequenceResultCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICharSequenceResultCallback != null) {
          Proxy.sDefaultImpl = param1ICharSequenceResultCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICharSequenceResultCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
