package com.android.internal.inputmethod;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.view.inputmethod.EditorInfo;
import com.android.internal.view.IInputContext;

public interface IMultiClientInputMethodSession extends IInterface {
  void hideSoftInput(int paramInt, ResultReceiver paramResultReceiver) throws RemoteException;
  
  void showSoftInput(int paramInt, ResultReceiver paramResultReceiver) throws RemoteException;
  
  void startInputOrWindowGainedFocus(IInputContext paramIInputContext, int paramInt1, EditorInfo paramEditorInfo, int paramInt2, int paramInt3, int paramInt4) throws RemoteException;
  
  class Default implements IMultiClientInputMethodSession {
    public void startInputOrWindowGainedFocus(IInputContext param1IInputContext, int param1Int1, EditorInfo param1EditorInfo, int param1Int2, int param1Int3, int param1Int4) throws RemoteException {}
    
    public void showSoftInput(int param1Int, ResultReceiver param1ResultReceiver) throws RemoteException {}
    
    public void hideSoftInput(int param1Int, ResultReceiver param1ResultReceiver) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMultiClientInputMethodSession {
    private static final String DESCRIPTOR = "com.android.internal.inputmethod.IMultiClientInputMethodSession";
    
    static final int TRANSACTION_hideSoftInput = 3;
    
    static final int TRANSACTION_showSoftInput = 2;
    
    static final int TRANSACTION_startInputOrWindowGainedFocus = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.inputmethod.IMultiClientInputMethodSession");
    }
    
    public static IMultiClientInputMethodSession asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.inputmethod.IMultiClientInputMethodSession");
      if (iInterface != null && iInterface instanceof IMultiClientInputMethodSession)
        return (IMultiClientInputMethodSession)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "hideSoftInput";
        } 
        return "showSoftInput";
      } 
      return "startInputOrWindowGainedFocus";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("com.android.internal.inputmethod.IMultiClientInputMethodSession");
            return true;
          } 
          param1Parcel1.enforceInterface("com.android.internal.inputmethod.IMultiClientInputMethodSession");
          param1Int1 = param1Parcel1.readInt();
          if (param1Parcel1.readInt() != 0) {
            ResultReceiver resultReceiver = (ResultReceiver)ResultReceiver.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel1 = null;
          } 
          hideSoftInput(param1Int1, (ResultReceiver)param1Parcel1);
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.internal.inputmethod.IMultiClientInputMethodSession");
        param1Int1 = param1Parcel1.readInt();
        if (param1Parcel1.readInt() != 0) {
          ResultReceiver resultReceiver = (ResultReceiver)ResultReceiver.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        showSoftInput(param1Int1, (ResultReceiver)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.inputmethod.IMultiClientInputMethodSession");
      IInputContext iInputContext = IInputContext.Stub.asInterface(param1Parcel1.readStrongBinder());
      param1Int2 = param1Parcel1.readInt();
      if (param1Parcel1.readInt() != 0) {
        EditorInfo editorInfo = (EditorInfo)EditorInfo.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      param1Int1 = param1Parcel1.readInt();
      int i = param1Parcel1.readInt();
      int j = param1Parcel1.readInt();
      startInputOrWindowGainedFocus(iInputContext, param1Int2, (EditorInfo)param1Parcel2, param1Int1, i, j);
      return true;
    }
    
    private static class Proxy implements IMultiClientInputMethodSession {
      public static IMultiClientInputMethodSession sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.inputmethod.IMultiClientInputMethodSession";
      }
      
      public void startInputOrWindowGainedFocus(IInputContext param2IInputContext, int param2Int1, EditorInfo param2EditorInfo, int param2Int2, int param2Int3, int param2Int4) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.inputmethod.IMultiClientInputMethodSession");
          if (param2IInputContext != null) {
            iBinder = param2IInputContext.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          try {
            parcel.writeInt(param2Int1);
            if (param2EditorInfo != null) {
              parcel.writeInt(1);
              param2EditorInfo.writeToParcel(parcel, 0);
            } else {
              parcel.writeInt(0);
            } 
            try {
              parcel.writeInt(param2Int2);
              try {
                parcel.writeInt(param2Int3);
                try {
                  parcel.writeInt(param2Int4);
                  try {
                    boolean bool = this.mRemote.transact(1, parcel, null, 1);
                    if (!bool && IMultiClientInputMethodSession.Stub.getDefaultImpl() != null) {
                      IMultiClientInputMethodSession.Stub.getDefaultImpl().startInputOrWindowGainedFocus(param2IInputContext, param2Int1, param2EditorInfo, param2Int2, param2Int3, param2Int4);
                      parcel.recycle();
                      return;
                    } 
                    parcel.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2IInputContext;
      }
      
      public void showSoftInput(int param2Int, ResultReceiver param2ResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.inputmethod.IMultiClientInputMethodSession");
          parcel.writeInt(param2Int);
          if (param2ResultReceiver != null) {
            parcel.writeInt(1);
            param2ResultReceiver.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IMultiClientInputMethodSession.Stub.getDefaultImpl() != null) {
            IMultiClientInputMethodSession.Stub.getDefaultImpl().showSoftInput(param2Int, param2ResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void hideSoftInput(int param2Int, ResultReceiver param2ResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.inputmethod.IMultiClientInputMethodSession");
          parcel.writeInt(param2Int);
          if (param2ResultReceiver != null) {
            parcel.writeInt(1);
            param2ResultReceiver.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IMultiClientInputMethodSession.Stub.getDefaultImpl() != null) {
            IMultiClientInputMethodSession.Stub.getDefaultImpl().hideSoftInput(param2Int, param2ResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMultiClientInputMethodSession param1IMultiClientInputMethodSession) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMultiClientInputMethodSession != null) {
          Proxy.sDefaultImpl = param1IMultiClientInputMethodSession;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMultiClientInputMethodSession getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
