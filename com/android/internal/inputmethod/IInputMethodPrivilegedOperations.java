package com.android.internal.inputmethod;

import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodSubtype;

public interface IInputMethodPrivilegedOperations extends IInterface {
  void applyImeVisibility(IBinder paramIBinder, boolean paramBoolean) throws RemoteException;
  
  IInputContentUriToken createInputContentUriToken(Uri paramUri, String paramString) throws RemoteException;
  
  void hideMySoftInput(int paramInt) throws RemoteException;
  
  void notifyUserAction() throws RemoteException;
  
  void reportFullscreenMode(boolean paramBoolean) throws RemoteException;
  
  void reportPreRendered(EditorInfo paramEditorInfo) throws RemoteException;
  
  void reportStartInput(IBinder paramIBinder) throws RemoteException;
  
  void setImeWindowStatus(int paramInt1, int paramInt2) throws RemoteException;
  
  void setInputMethod(String paramString) throws RemoteException;
  
  void setInputMethodAndSubtype(String paramString, InputMethodSubtype paramInputMethodSubtype) throws RemoteException;
  
  boolean shouldOfferSwitchingToNextInputMethod() throws RemoteException;
  
  void showMySoftInput(int paramInt) throws RemoteException;
  
  boolean switchToNextInputMethod(boolean paramBoolean) throws RemoteException;
  
  boolean switchToPreviousInputMethod() throws RemoteException;
  
  void updateStatusIcon(String paramString, int paramInt) throws RemoteException;
  
  class Default implements IInputMethodPrivilegedOperations {
    public void setImeWindowStatus(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void reportStartInput(IBinder param1IBinder) throws RemoteException {}
    
    public IInputContentUriToken createInputContentUriToken(Uri param1Uri, String param1String) throws RemoteException {
      return null;
    }
    
    public void reportFullscreenMode(boolean param1Boolean) throws RemoteException {}
    
    public void setInputMethod(String param1String) throws RemoteException {}
    
    public void setInputMethodAndSubtype(String param1String, InputMethodSubtype param1InputMethodSubtype) throws RemoteException {}
    
    public void hideMySoftInput(int param1Int) throws RemoteException {}
    
    public void showMySoftInput(int param1Int) throws RemoteException {}
    
    public void updateStatusIcon(String param1String, int param1Int) throws RemoteException {}
    
    public boolean switchToPreviousInputMethod() throws RemoteException {
      return false;
    }
    
    public boolean switchToNextInputMethod(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean shouldOfferSwitchingToNextInputMethod() throws RemoteException {
      return false;
    }
    
    public void notifyUserAction() throws RemoteException {}
    
    public void reportPreRendered(EditorInfo param1EditorInfo) throws RemoteException {}
    
    public void applyImeVisibility(IBinder param1IBinder, boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IInputMethodPrivilegedOperations {
    private static final String DESCRIPTOR = "com.android.internal.inputmethod.IInputMethodPrivilegedOperations";
    
    static final int TRANSACTION_applyImeVisibility = 15;
    
    static final int TRANSACTION_createInputContentUriToken = 3;
    
    static final int TRANSACTION_hideMySoftInput = 7;
    
    static final int TRANSACTION_notifyUserAction = 13;
    
    static final int TRANSACTION_reportFullscreenMode = 4;
    
    static final int TRANSACTION_reportPreRendered = 14;
    
    static final int TRANSACTION_reportStartInput = 2;
    
    static final int TRANSACTION_setImeWindowStatus = 1;
    
    static final int TRANSACTION_setInputMethod = 5;
    
    static final int TRANSACTION_setInputMethodAndSubtype = 6;
    
    static final int TRANSACTION_shouldOfferSwitchingToNextInputMethod = 12;
    
    static final int TRANSACTION_showMySoftInput = 8;
    
    static final int TRANSACTION_switchToNextInputMethod = 11;
    
    static final int TRANSACTION_switchToPreviousInputMethod = 10;
    
    static final int TRANSACTION_updateStatusIcon = 9;
    
    public Stub() {
      attachInterface(this, "com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
    }
    
    public static IInputMethodPrivilegedOperations asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
      if (iInterface != null && iInterface instanceof IInputMethodPrivilegedOperations)
        return (IInputMethodPrivilegedOperations)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 15:
          return "applyImeVisibility";
        case 14:
          return "reportPreRendered";
        case 13:
          return "notifyUserAction";
        case 12:
          return "shouldOfferSwitchingToNextInputMethod";
        case 11:
          return "switchToNextInputMethod";
        case 10:
          return "switchToPreviousInputMethod";
        case 9:
          return "updateStatusIcon";
        case 8:
          return "showMySoftInput";
        case 7:
          return "hideMySoftInput";
        case 6:
          return "setInputMethodAndSubtype";
        case 5:
          return "setInputMethod";
        case 4:
          return "reportFullscreenMode";
        case 3:
          return "createInputContentUriToken";
        case 2:
          return "reportStartInput";
        case 1:
          break;
      } 
      return "setImeWindowStatus";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        String str1;
        IInputContentUriToken iInputContentUriToken;
        IBinder iBinder1, iBinder2;
        String str2;
        boolean bool1 = false, bool2 = false, bool3 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 15:
            param1Parcel1.enforceInterface("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
            iBinder2 = param1Parcel1.readStrongBinder();
            bool1 = bool3;
            if (param1Parcel1.readInt() != 0)
              bool1 = true; 
            applyImeVisibility(iBinder2, bool1);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            param1Parcel1.enforceInterface("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
            if (param1Parcel1.readInt() != 0) {
              EditorInfo editorInfo = (EditorInfo)EditorInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            reportPreRendered((EditorInfo)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 13:
            param1Parcel1.enforceInterface("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
            notifyUserAction();
            param1Parcel2.writeNoException();
            return true;
          case 12:
            param1Parcel1.enforceInterface("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
            bool = shouldOfferSwitchingToNextInputMethod();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 11:
            param1Parcel1.enforceInterface("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
            if (param1Parcel1.readInt() != 0)
              bool1 = true; 
            bool = switchToNextInputMethod(bool1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 10:
            param1Parcel1.enforceInterface("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
            bool = switchToPreviousInputMethod();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 9:
            param1Parcel1.enforceInterface("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
            str2 = param1Parcel1.readString();
            i = param1Parcel1.readInt();
            updateStatusIcon(str2, i);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            param1Parcel1.enforceInterface("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
            i = param1Parcel1.readInt();
            showMySoftInput(i);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            param1Parcel1.enforceInterface("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
            i = param1Parcel1.readInt();
            hideMySoftInput(i);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            param1Parcel1.enforceInterface("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
            str2 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              InputMethodSubtype inputMethodSubtype = (InputMethodSubtype)InputMethodSubtype.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setInputMethodAndSubtype(str2, (InputMethodSubtype)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            param1Parcel1.enforceInterface("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
            str1 = param1Parcel1.readString();
            setInputMethod(str1);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            str1.enforceInterface("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
            bool1 = bool2;
            if (str1.readInt() != 0)
              bool1 = true; 
            reportFullscreenMode(bool1);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            str1.enforceInterface("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
            if (str1.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str2 = null;
            } 
            str1 = str1.readString();
            iInputContentUriToken = createInputContentUriToken((Uri)str2, str1);
            param1Parcel2.writeNoException();
            if (iInputContentUriToken != null) {
              IBinder iBinder = iInputContentUriToken.asBinder();
            } else {
              iInputContentUriToken = null;
            } 
            param1Parcel2.writeStrongBinder((IBinder)iInputContentUriToken);
            return true;
          case 2:
            iInputContentUriToken.enforceInterface("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
            iBinder1 = iInputContentUriToken.readStrongBinder();
            reportStartInput(iBinder1);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        iBinder1.enforceInterface("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
        param1Int2 = iBinder1.readInt();
        int i = iBinder1.readInt();
        setImeWindowStatus(param1Int2, i);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
      return true;
    }
    
    private static class Proxy implements IInputMethodPrivilegedOperations {
      public static IInputMethodPrivilegedOperations sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.inputmethod.IInputMethodPrivilegedOperations";
      }
      
      public void setImeWindowStatus(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IInputMethodPrivilegedOperations.Stub.getDefaultImpl() != null) {
            IInputMethodPrivilegedOperations.Stub.getDefaultImpl().setImeWindowStatus(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportStartInput(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IInputMethodPrivilegedOperations.Stub.getDefaultImpl() != null) {
            IInputMethodPrivilegedOperations.Stub.getDefaultImpl().reportStartInput(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IInputContentUriToken createInputContentUriToken(Uri param2Uri, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IInputMethodPrivilegedOperations.Stub.getDefaultImpl() != null)
            return IInputMethodPrivilegedOperations.Stub.getDefaultImpl().createInputContentUriToken(param2Uri, param2String); 
          parcel2.readException();
          return IInputContentUriToken.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportFullscreenMode(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool1 && IInputMethodPrivilegedOperations.Stub.getDefaultImpl() != null) {
            IInputMethodPrivilegedOperations.Stub.getDefaultImpl().reportFullscreenMode(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setInputMethod(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IInputMethodPrivilegedOperations.Stub.getDefaultImpl() != null) {
            IInputMethodPrivilegedOperations.Stub.getDefaultImpl().setInputMethod(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setInputMethodAndSubtype(String param2String, InputMethodSubtype param2InputMethodSubtype) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
          parcel1.writeString(param2String);
          if (param2InputMethodSubtype != null) {
            parcel1.writeInt(1);
            param2InputMethodSubtype.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IInputMethodPrivilegedOperations.Stub.getDefaultImpl() != null) {
            IInputMethodPrivilegedOperations.Stub.getDefaultImpl().setInputMethodAndSubtype(param2String, param2InputMethodSubtype);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void hideMySoftInput(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IInputMethodPrivilegedOperations.Stub.getDefaultImpl() != null) {
            IInputMethodPrivilegedOperations.Stub.getDefaultImpl().hideMySoftInput(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void showMySoftInput(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IInputMethodPrivilegedOperations.Stub.getDefaultImpl() != null) {
            IInputMethodPrivilegedOperations.Stub.getDefaultImpl().showMySoftInput(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateStatusIcon(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IInputMethodPrivilegedOperations.Stub.getDefaultImpl() != null) {
            IInputMethodPrivilegedOperations.Stub.getDefaultImpl().updateStatusIcon(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean switchToPreviousInputMethod() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(10, parcel1, parcel2, 0);
          if (!bool2 && IInputMethodPrivilegedOperations.Stub.getDefaultImpl() != null) {
            bool1 = IInputMethodPrivilegedOperations.Stub.getDefaultImpl().switchToPreviousInputMethod();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean switchToNextInputMethod(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool1 && IInputMethodPrivilegedOperations.Stub.getDefaultImpl() != null) {
            param2Boolean = IInputMethodPrivilegedOperations.Stub.getDefaultImpl().switchToNextInputMethod(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean shouldOfferSwitchingToNextInputMethod() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(12, parcel1, parcel2, 0);
          if (!bool2 && IInputMethodPrivilegedOperations.Stub.getDefaultImpl() != null) {
            bool1 = IInputMethodPrivilegedOperations.Stub.getDefaultImpl().shouldOfferSwitchingToNextInputMethod();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyUserAction() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IInputMethodPrivilegedOperations.Stub.getDefaultImpl() != null) {
            IInputMethodPrivilegedOperations.Stub.getDefaultImpl().notifyUserAction();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportPreRendered(EditorInfo param2EditorInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
          if (param2EditorInfo != null) {
            parcel1.writeInt(1);
            param2EditorInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IInputMethodPrivilegedOperations.Stub.getDefaultImpl() != null) {
            IInputMethodPrivilegedOperations.Stub.getDefaultImpl().reportPreRendered(param2EditorInfo);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void applyImeVisibility(IBinder param2IBinder, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.inputmethod.IInputMethodPrivilegedOperations");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool1 && IInputMethodPrivilegedOperations.Stub.getDefaultImpl() != null) {
            IInputMethodPrivilegedOperations.Stub.getDefaultImpl().applyImeVisibility(param2IBinder, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IInputMethodPrivilegedOperations param1IInputMethodPrivilegedOperations) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IInputMethodPrivilegedOperations != null) {
          Proxy.sDefaultImpl = param1IInputMethodPrivilegedOperations;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IInputMethodPrivilegedOperations getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
