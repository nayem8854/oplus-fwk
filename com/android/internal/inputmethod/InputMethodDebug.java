package com.android.internal.inputmethod;

import java.util.StringJoiner;

public final class InputMethodDebug {
  public static String startInputReasonToString(int paramInt) {
    StringBuilder stringBuilder;
    switch (paramInt) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown=");
        stringBuilder.append(paramInt);
        return stringBuilder.toString();
      case 10:
        return "SESSION_CREATED_BY_IME";
      case 9:
        return "DEACTIVATED_BY_IMMS";
      case 8:
        return "ACTIVATED_BY_IMMS";
      case 7:
        return "UNBOUND_FROM_IMMS";
      case 6:
        return "BOUND_TO_IMMS";
      case 5:
        return "CHECK_FOCUS";
      case 4:
        return "APP_CALLED_RESTART_INPUT_API";
      case 3:
        return "WINDOW_FOCUS_GAIN_REPORT_WITHOUT_CONNECTION";
      case 2:
        return "WINDOW_FOCUS_GAIN_REPORT_WITH_CONNECTION";
      case 1:
        return "WINDOW_FOCUS_GAIN";
      case 0:
        break;
    } 
    return "UNSPECIFIED";
  }
  
  public static String unbindReasonToString(int paramInt) {
    StringBuilder stringBuilder;
    switch (paramInt) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown=");
        stringBuilder.append(paramInt);
        return stringBuilder.toString();
      case 6:
        return "SWITCH_USER";
      case 5:
        return "SWITCH_IME_FAILED";
      case 4:
        return "NO_IME";
      case 3:
        return "DISCONNECT_IME";
      case 2:
        return "SWITCH_IME";
      case 1:
        return "SWITCH_CLIENT";
      case 0:
        break;
    } 
    return "UNSPECIFIED";
  }
  
  public static String softInputModeToString(int paramInt) {
    StringJoiner stringJoiner = new StringJoiner("|");
    int i = paramInt & 0xF;
    int j = paramInt & 0xF0;
    if ((paramInt & 0x100) != 0) {
      paramInt = 1;
    } else {
      paramInt = 0;
    } 
    if (i != 0) {
      if (i != 1) {
        if (i != 2) {
          if (i != 3) {
            if (i != 4) {
              if (i != 5) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("STATE_UNKNOWN(");
                stringBuilder.append(i);
                stringBuilder.append(")");
                stringJoiner.add(stringBuilder.toString());
              } else {
                stringJoiner.add("STATE_ALWAYS_VISIBLE");
              } 
            } else {
              stringJoiner.add("STATE_VISIBLE");
            } 
          } else {
            stringJoiner.add("STATE_ALWAYS_HIDDEN");
          } 
        } else {
          stringJoiner.add("STATE_HIDDEN");
        } 
      } else {
        stringJoiner.add("STATE_UNCHANGED");
      } 
    } else {
      stringJoiner.add("STATE_UNSPECIFIED");
    } 
    if (j != 0) {
      if (j != 16) {
        if (j != 32) {
          if (j != 48) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("ADJUST_UNKNOWN(");
            stringBuilder.append(j);
            stringBuilder.append(")");
            stringJoiner.add(stringBuilder.toString());
          } else {
            stringJoiner.add("ADJUST_NOTHING");
          } 
        } else {
          stringJoiner.add("ADJUST_PAN");
        } 
      } else {
        stringJoiner.add("ADJUST_RESIZE");
      } 
    } else {
      stringJoiner.add("ADJUST_UNSPECIFIED");
    } 
    if (paramInt != 0)
      stringJoiner.add("IS_FORWARD_NAVIGATION"); 
    return stringJoiner.setEmptyValue("(none)").toString();
  }
  
  public static String startInputFlagsToString(int paramInt) {
    StringJoiner stringJoiner = new StringJoiner("|");
    if ((paramInt & 0x1) != 0)
      stringJoiner.add("VIEW_HAS_FOCUS"); 
    if ((paramInt & 0x2) != 0)
      stringJoiner.add("IS_TEXT_EDITOR"); 
    if ((paramInt & 0x4) != 0)
      stringJoiner.add("INITIAL_CONNECTION"); 
    return stringJoiner.setEmptyValue("(none)").toString();
  }
  
  public static String softInputDisplayReasonToString(int paramInt) {
    StringBuilder stringBuilder;
    switch (paramInt) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown=");
        stringBuilder.append(paramInt);
        return stringBuilder.toString();
      case 18:
        return "HIDE_RECENTS_ANIMATION";
      case 17:
        return "HIDE_DOCKED_STACK_ATTACHED";
      case 16:
        return "HIDE_POWER_BUTTON_GO_HOME";
      case 15:
        return "HIDE_SETTINGS_ON_CHANGE";
      case 14:
        return "HIDE_RESET_SHELL_COMMAND";
      case 13:
        return "HIDE_ALWAYS_HIDDEN_STATE";
      case 12:
        return "HIDE_STATE_HIDDEN_FORWARD_NAV";
      case 11:
        return "HIDE_UNSPECIFIED_WINDOW";
      case 10:
        return "HIDE_INVALID_USER";
      case 9:
        return "HIDE_SWITCH_USER";
      case 8:
        return "SHOW_SETTINGS_ON_CHANGE";
      case 7:
        return "SHOW_STATE_ALWAYS_VISIBLE";
      case 6:
        return "SHOW_STATE_VISIBLE_FORWARD_NAV";
      case 5:
        return "SHOW_AUTO_EDITOR_FORWARD_NAV";
      case 4:
        return "HIDE_MY_SOFT_INPUT";
      case 3:
        return "HIDE_SOFT_INPUT";
      case 2:
        return "SHOW_MY_SOFT_INPUT";
      case 1:
        return "ATTACH_NEW_INPUT";
      case 0:
        break;
    } 
    return "SHOW_SOFT_INPUT";
  }
  
  public static String objToString(Object paramObject) {
    if (paramObject == null)
      return "null"; 
    StringBuilder stringBuilder = new StringBuilder(64);
    stringBuilder.setLength(0);
    stringBuilder.append(paramObject.getClass().getName());
    stringBuilder.append("@");
    stringBuilder.append(Integer.toHexString(paramObject.hashCode()));
    return stringBuilder.toString();
  }
}
