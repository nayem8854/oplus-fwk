package com.android.internal.inputmethod;

import android.net.Uri;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodSubtype;

public final class InputMethodPrivilegedOperations {
  private static final String TAG = "InputMethodPrivilegedOperations";
  
  private static final class OpsHolder {
    private IInputMethodPrivilegedOperations mPrivOps;
    
    private OpsHolder() {}
    
    public void set(IInputMethodPrivilegedOperations param1IInputMethodPrivilegedOperations) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mPrivOps : Lcom/android/internal/inputmethod/IInputMethodPrivilegedOperations;
      //   6: ifnonnull -> 17
      //   9: aload_0
      //   10: aload_1
      //   11: putfield mPrivOps : Lcom/android/internal/inputmethod/IInputMethodPrivilegedOperations;
      //   14: aload_0
      //   15: monitorexit
      //   16: return
      //   17: new java/lang/IllegalStateException
      //   20: astore_2
      //   21: new java/lang/StringBuilder
      //   24: astore_3
      //   25: aload_3
      //   26: invokespecial <init> : ()V
      //   29: aload_3
      //   30: ldc 'IInputMethodPrivilegedOperations must be set at most once. privOps='
      //   32: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   35: pop
      //   36: aload_3
      //   37: aload_1
      //   38: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   41: pop
      //   42: aload_2
      //   43: aload_3
      //   44: invokevirtual toString : ()Ljava/lang/String;
      //   47: invokespecial <init> : (Ljava/lang/String;)V
      //   50: aload_2
      //   51: athrow
      //   52: astore_1
      //   53: aload_0
      //   54: monitorexit
      //   55: aload_1
      //   56: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #52	-> 2
      //   #57	-> 9
      //   #58	-> 14
      //   #53	-> 17
      //   #51	-> 52
      // Exception table:
      //   from	to	target	type
      //   2	9	52	finally
      //   9	14	52	finally
      //   17	52	52	finally
    }
    
    private static String getCallerMethodName() {
      StackTraceElement[] arrayOfStackTraceElement = Thread.currentThread().getStackTrace();
      if (arrayOfStackTraceElement.length <= 4)
        return "<bottom of call stack>"; 
      return arrayOfStackTraceElement[4].getMethodName();
    }
    
    public IInputMethodPrivilegedOperations getAndWarnIfNull() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mPrivOps : Lcom/android/internal/inputmethod/IInputMethodPrivilegedOperations;
      //   6: ifnonnull -> 42
      //   9: new java/lang/StringBuilder
      //   12: astore_1
      //   13: aload_1
      //   14: invokespecial <init> : ()V
      //   17: aload_1
      //   18: invokestatic getCallerMethodName : ()Ljava/lang/String;
      //   21: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   24: pop
      //   25: aload_1
      //   26: ldc ' is ignored. Call it within attachToken() and InputMethodService.onDestroy()'
      //   28: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   31: pop
      //   32: ldc 'InputMethodPrivilegedOperations'
      //   34: aload_1
      //   35: invokevirtual toString : ()Ljava/lang/String;
      //   38: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
      //   41: pop
      //   42: aload_0
      //   43: getfield mPrivOps : Lcom/android/internal/inputmethod/IInputMethodPrivilegedOperations;
      //   46: astore_1
      //   47: aload_0
      //   48: monitorexit
      //   49: aload_1
      //   50: areturn
      //   51: astore_1
      //   52: aload_0
      //   53: monitorexit
      //   54: aload_1
      //   55: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #77	-> 2
      //   #78	-> 9
      //   #81	-> 42
      //   #76	-> 51
      // Exception table:
      //   from	to	target	type
      //   2	9	51	finally
      //   9	42	51	finally
      //   42	47	51	finally
    }
  }
  
  private final OpsHolder mOps = new OpsHolder();
  
  public void set(IInputMethodPrivilegedOperations paramIInputMethodPrivilegedOperations) {
    this.mOps.set(paramIInputMethodPrivilegedOperations);
  }
  
  public void setImeWindowStatus(int paramInt1, int paramInt2) {
    IInputMethodPrivilegedOperations iInputMethodPrivilegedOperations = this.mOps.getAndWarnIfNull();
    if (iInputMethodPrivilegedOperations == null)
      return; 
    try {
      iInputMethodPrivilegedOperations.setImeWindowStatus(paramInt1, paramInt2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void reportStartInput(IBinder paramIBinder) {
    IInputMethodPrivilegedOperations iInputMethodPrivilegedOperations = this.mOps.getAndWarnIfNull();
    if (iInputMethodPrivilegedOperations == null)
      return; 
    try {
      iInputMethodPrivilegedOperations.reportStartInput(paramIBinder);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public IInputContentUriToken createInputContentUriToken(Uri paramUri, String paramString) {
    IInputMethodPrivilegedOperations iInputMethodPrivilegedOperations = this.mOps.getAndWarnIfNull();
    if (iInputMethodPrivilegedOperations == null)
      return null; 
    try {
      return iInputMethodPrivilegedOperations.createInputContentUriToken(paramUri, paramString);
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public void reportFullscreenMode(boolean paramBoolean) {
    IInputMethodPrivilegedOperations iInputMethodPrivilegedOperations = this.mOps.getAndWarnIfNull();
    if (iInputMethodPrivilegedOperations == null)
      return; 
    try {
      iInputMethodPrivilegedOperations.reportFullscreenMode(paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void updateStatusIcon(String paramString, int paramInt) {
    IInputMethodPrivilegedOperations iInputMethodPrivilegedOperations = this.mOps.getAndWarnIfNull();
    if (iInputMethodPrivilegedOperations == null)
      return; 
    try {
      iInputMethodPrivilegedOperations.updateStatusIcon(paramString, paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setInputMethod(String paramString) {
    IInputMethodPrivilegedOperations iInputMethodPrivilegedOperations = this.mOps.getAndWarnIfNull();
    if (iInputMethodPrivilegedOperations == null)
      return; 
    try {
      iInputMethodPrivilegedOperations.setInputMethod(paramString);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setInputMethodAndSubtype(String paramString, InputMethodSubtype paramInputMethodSubtype) {
    IInputMethodPrivilegedOperations iInputMethodPrivilegedOperations = this.mOps.getAndWarnIfNull();
    if (iInputMethodPrivilegedOperations == null)
      return; 
    try {
      iInputMethodPrivilegedOperations.setInputMethodAndSubtype(paramString, paramInputMethodSubtype);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void hideMySoftInput(int paramInt) {
    IInputMethodPrivilegedOperations iInputMethodPrivilegedOperations = this.mOps.getAndWarnIfNull();
    if (iInputMethodPrivilegedOperations == null)
      return; 
    try {
      iInputMethodPrivilegedOperations.hideMySoftInput(paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void showMySoftInput(int paramInt) {
    IInputMethodPrivilegedOperations iInputMethodPrivilegedOperations = this.mOps.getAndWarnIfNull();
    if (iInputMethodPrivilegedOperations == null)
      return; 
    try {
      iInputMethodPrivilegedOperations.showMySoftInput(paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean switchToPreviousInputMethod() {
    IInputMethodPrivilegedOperations iInputMethodPrivilegedOperations = this.mOps.getAndWarnIfNull();
    if (iInputMethodPrivilegedOperations == null)
      return false; 
    try {
      return iInputMethodPrivilegedOperations.switchToPreviousInputMethod();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean switchToNextInputMethod(boolean paramBoolean) {
    IInputMethodPrivilegedOperations iInputMethodPrivilegedOperations = this.mOps.getAndWarnIfNull();
    if (iInputMethodPrivilegedOperations == null)
      return false; 
    try {
      return iInputMethodPrivilegedOperations.switchToNextInputMethod(paramBoolean);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean shouldOfferSwitchingToNextInputMethod() {
    IInputMethodPrivilegedOperations iInputMethodPrivilegedOperations = this.mOps.getAndWarnIfNull();
    if (iInputMethodPrivilegedOperations == null)
      return false; 
    try {
      return iInputMethodPrivilegedOperations.shouldOfferSwitchingToNextInputMethod();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void notifyUserAction() {
    IInputMethodPrivilegedOperations iInputMethodPrivilegedOperations = this.mOps.getAndWarnIfNull();
    if (iInputMethodPrivilegedOperations == null)
      return; 
    try {
      iInputMethodPrivilegedOperations.notifyUserAction();
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void reportPreRendered(EditorInfo paramEditorInfo) {
    IInputMethodPrivilegedOperations iInputMethodPrivilegedOperations = this.mOps.getAndWarnIfNull();
    if (iInputMethodPrivilegedOperations == null)
      return; 
    try {
      iInputMethodPrivilegedOperations.reportPreRendered(paramEditorInfo);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void applyImeVisibility(IBinder paramIBinder, boolean paramBoolean) {
    IInputMethodPrivilegedOperations iInputMethodPrivilegedOperations = this.mOps.getAndWarnIfNull();
    if (iInputMethodPrivilegedOperations == null)
      return; 
    try {
      iInputMethodPrivilegedOperations.applyImeVisibility(paramIBinder, paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
}
