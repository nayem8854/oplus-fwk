package com.android.internal.inputmethod;

import android.os.IBinder;
import android.os.RemoteException;
import android.view.InputChannel;
import com.android.internal.view.IInputMethodSession;

public class MultiClientInputMethodPrivilegedOperations {
  private static final String TAG = "MultiClientInputMethodPrivilegedOperations";
  
  private static final class OpsHolder {
    private IMultiClientInputMethodPrivilegedOperations mPrivOps;
    
    private OpsHolder() {}
    
    public void set(IMultiClientInputMethodPrivilegedOperations param1IMultiClientInputMethodPrivilegedOperations) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mPrivOps : Lcom/android/internal/inputmethod/IMultiClientInputMethodPrivilegedOperations;
      //   6: ifnonnull -> 17
      //   9: aload_0
      //   10: aload_1
      //   11: putfield mPrivOps : Lcom/android/internal/inputmethod/IMultiClientInputMethodPrivilegedOperations;
      //   14: aload_0
      //   15: monitorexit
      //   16: return
      //   17: new java/lang/IllegalStateException
      //   20: astore_2
      //   21: new java/lang/StringBuilder
      //   24: astore_3
      //   25: aload_3
      //   26: invokespecial <init> : ()V
      //   29: aload_3
      //   30: ldc 'IMultiClientInputMethodPrivilegedOperations must be set at most once. privOps='
      //   32: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   35: pop
      //   36: aload_3
      //   37: aload_1
      //   38: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   41: pop
      //   42: aload_2
      //   43: aload_3
      //   44: invokevirtual toString : ()Ljava/lang/String;
      //   47: invokespecial <init> : (Ljava/lang/String;)V
      //   50: aload_2
      //   51: athrow
      //   52: astore_1
      //   53: aload_0
      //   54: monitorexit
      //   55: aload_1
      //   56: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #51	-> 2
      //   #56	-> 9
      //   #57	-> 14
      //   #52	-> 17
      //   #50	-> 52
      // Exception table:
      //   from	to	target	type
      //   2	9	52	finally
      //   9	14	52	finally
      //   17	52	52	finally
    }
    
    private static String getCallerMethodName() {
      StackTraceElement[] arrayOfStackTraceElement = Thread.currentThread().getStackTrace();
      if (arrayOfStackTraceElement.length <= 4)
        return "<bottom of call stack>"; 
      return arrayOfStackTraceElement[4].getMethodName();
    }
    
    public void dispose() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: aconst_null
      //   4: putfield mPrivOps : Lcom/android/internal/inputmethod/IMultiClientInputMethodPrivilegedOperations;
      //   7: aload_0
      //   8: monitorexit
      //   9: return
      //   10: astore_1
      //   11: aload_0
      //   12: monitorexit
      //   13: aload_1
      //   14: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #75	-> 2
      //   #76	-> 7
      //   #74	-> 10
      // Exception table:
      //   from	to	target	type
      //   2	7	10	finally
    }
    
    public IMultiClientInputMethodPrivilegedOperations getAndWarnIfNull() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mPrivOps : Lcom/android/internal/inputmethod/IMultiClientInputMethodPrivilegedOperations;
      //   6: ifnonnull -> 42
      //   9: new java/lang/StringBuilder
      //   12: astore_1
      //   13: aload_1
      //   14: invokespecial <init> : ()V
      //   17: aload_1
      //   18: invokestatic getCallerMethodName : ()Ljava/lang/String;
      //   21: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   24: pop
      //   25: aload_1
      //   26: ldc ' is ignored. Call it within attachToken() and InputMethodService.onDestroy()'
      //   28: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   31: pop
      //   32: ldc 'MultiClientInputMethodPrivilegedOperations'
      //   34: aload_1
      //   35: invokevirtual toString : ()Ljava/lang/String;
      //   38: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
      //   41: pop
      //   42: aload_0
      //   43: getfield mPrivOps : Lcom/android/internal/inputmethod/IMultiClientInputMethodPrivilegedOperations;
      //   46: astore_1
      //   47: aload_0
      //   48: monitorexit
      //   49: aload_1
      //   50: areturn
      //   51: astore_1
      //   52: aload_0
      //   53: monitorexit
      //   54: aload_1
      //   55: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #81	-> 2
      //   #82	-> 9
      //   #85	-> 42
      //   #80	-> 51
      // Exception table:
      //   from	to	target	type
      //   2	9	51	finally
      //   9	42	51	finally
      //   42	47	51	finally
    }
  }
  
  private final OpsHolder mOps = new OpsHolder();
  
  public void set(IMultiClientInputMethodPrivilegedOperations paramIMultiClientInputMethodPrivilegedOperations) {
    this.mOps.set(paramIMultiClientInputMethodPrivilegedOperations);
  }
  
  public void dispose() {
    this.mOps.dispose();
  }
  
  public IBinder createInputMethodWindowToken(int paramInt) {
    IMultiClientInputMethodPrivilegedOperations iMultiClientInputMethodPrivilegedOperations = this.mOps.getAndWarnIfNull();
    if (iMultiClientInputMethodPrivilegedOperations == null)
      return null; 
    try {
      return iMultiClientInputMethodPrivilegedOperations.createInputMethodWindowToken(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void deleteInputMethodWindowToken(IBinder paramIBinder) {
    IMultiClientInputMethodPrivilegedOperations iMultiClientInputMethodPrivilegedOperations = this.mOps.getAndWarnIfNull();
    if (iMultiClientInputMethodPrivilegedOperations == null)
      return; 
    try {
      iMultiClientInputMethodPrivilegedOperations.deleteInputMethodWindowToken(paramIBinder);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void acceptClient(int paramInt, IInputMethodSession paramIInputMethodSession, IMultiClientInputMethodSession paramIMultiClientInputMethodSession, InputChannel paramInputChannel) {
    IMultiClientInputMethodPrivilegedOperations iMultiClientInputMethodPrivilegedOperations = this.mOps.getAndWarnIfNull();
    if (iMultiClientInputMethodPrivilegedOperations == null)
      return; 
    try {
      iMultiClientInputMethodPrivilegedOperations.acceptClient(paramInt, paramIInputMethodSession, paramIMultiClientInputMethodSession, paramInputChannel);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void reportImeWindowTarget(int paramInt1, int paramInt2, IBinder paramIBinder) {
    IMultiClientInputMethodPrivilegedOperations iMultiClientInputMethodPrivilegedOperations = this.mOps.getAndWarnIfNull();
    if (iMultiClientInputMethodPrivilegedOperations == null)
      return; 
    try {
      iMultiClientInputMethodPrivilegedOperations.reportImeWindowTarget(paramInt1, paramInt2, paramIBinder);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isUidAllowedOnDisplay(int paramInt1, int paramInt2) {
    IMultiClientInputMethodPrivilegedOperations iMultiClientInputMethodPrivilegedOperations = this.mOps.getAndWarnIfNull();
    if (iMultiClientInputMethodPrivilegedOperations == null)
      return false; 
    try {
      return iMultiClientInputMethodPrivilegedOperations.isUidAllowedOnDisplay(paramInt1, paramInt2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setActive(int paramInt, boolean paramBoolean) {
    IMultiClientInputMethodPrivilegedOperations iMultiClientInputMethodPrivilegedOperations = this.mOps.getAndWarnIfNull();
    if (iMultiClientInputMethodPrivilegedOperations == null)
      return; 
    try {
      iMultiClientInputMethodPrivilegedOperations.setActive(paramInt, paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
}
