package com.android.internal.inputmethod;

import android.text.TextUtils;
import java.util.Locale;

public class SubtypeLocaleUtils {
  public static Locale constructLocaleFromString(String paramString) {
    if (TextUtils.isEmpty(paramString))
      return null; 
    String[] arrayOfString = paramString.split("_", 3);
    if (arrayOfString.length >= 1 && "tl".equals(arrayOfString[0]))
      arrayOfString[0] = "fil"; 
    if (arrayOfString.length == 1)
      return new Locale(arrayOfString[0]); 
    if (arrayOfString.length == 2)
      return new Locale(arrayOfString[0], arrayOfString[1]); 
    if (arrayOfString.length == 3)
      return new Locale(arrayOfString[0], arrayOfString[1], arrayOfString[2]); 
    return null;
  }
}
