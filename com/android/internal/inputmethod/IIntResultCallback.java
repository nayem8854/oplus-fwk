package com.android.internal.inputmethod;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IIntResultCallback extends IInterface {
  void onResult(int paramInt) throws RemoteException;
  
  class Default implements IIntResultCallback {
    public void onResult(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IIntResultCallback {
    private static final String DESCRIPTOR = "com.android.internal.inputmethod.IIntResultCallback";
    
    static final int TRANSACTION_onResult = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.inputmethod.IIntResultCallback");
    }
    
    public static IIntResultCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.inputmethod.IIntResultCallback");
      if (iInterface != null && iInterface instanceof IIntResultCallback)
        return (IIntResultCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onResult";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.inputmethod.IIntResultCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.inputmethod.IIntResultCallback");
      param1Int1 = param1Parcel1.readInt();
      onResult(param1Int1);
      return true;
    }
    
    class Proxy implements IIntResultCallback {
      public static IIntResultCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IIntResultCallback.Stub this$0) {
        this.mRemote = (IBinder)this$0;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.inputmethod.IIntResultCallback";
      }
      
      public void onResult(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.inputmethod.IIntResultCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IIntResultCallback.Stub.getDefaultImpl() != null) {
            IIntResultCallback.Stub.getDefaultImpl().onResult(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IIntResultCallback param1IIntResultCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IIntResultCallback != null) {
          Proxy.sDefaultImpl = param1IIntResultCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IIntResultCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
