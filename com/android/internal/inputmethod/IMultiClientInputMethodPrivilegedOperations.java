package com.android.internal.inputmethod;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.InputChannel;
import com.android.internal.view.IInputMethodSession;

public interface IMultiClientInputMethodPrivilegedOperations extends IInterface {
  void acceptClient(int paramInt, IInputMethodSession paramIInputMethodSession, IMultiClientInputMethodSession paramIMultiClientInputMethodSession, InputChannel paramInputChannel) throws RemoteException;
  
  IBinder createInputMethodWindowToken(int paramInt) throws RemoteException;
  
  void deleteInputMethodWindowToken(IBinder paramIBinder) throws RemoteException;
  
  boolean isUidAllowedOnDisplay(int paramInt1, int paramInt2) throws RemoteException;
  
  void reportImeWindowTarget(int paramInt1, int paramInt2, IBinder paramIBinder) throws RemoteException;
  
  void setActive(int paramInt, boolean paramBoolean) throws RemoteException;
  
  class Default implements IMultiClientInputMethodPrivilegedOperations {
    public IBinder createInputMethodWindowToken(int param1Int) throws RemoteException {
      return null;
    }
    
    public void deleteInputMethodWindowToken(IBinder param1IBinder) throws RemoteException {}
    
    public void acceptClient(int param1Int, IInputMethodSession param1IInputMethodSession, IMultiClientInputMethodSession param1IMultiClientInputMethodSession, InputChannel param1InputChannel) throws RemoteException {}
    
    public void reportImeWindowTarget(int param1Int1, int param1Int2, IBinder param1IBinder) throws RemoteException {}
    
    public boolean isUidAllowedOnDisplay(int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public void setActive(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMultiClientInputMethodPrivilegedOperations {
    private static final String DESCRIPTOR = "com.android.internal.inputmethod.IMultiClientInputMethodPrivilegedOperations";
    
    static final int TRANSACTION_acceptClient = 3;
    
    static final int TRANSACTION_createInputMethodWindowToken = 1;
    
    static final int TRANSACTION_deleteInputMethodWindowToken = 2;
    
    static final int TRANSACTION_isUidAllowedOnDisplay = 5;
    
    static final int TRANSACTION_reportImeWindowTarget = 4;
    
    static final int TRANSACTION_setActive = 6;
    
    public Stub() {
      attachInterface(this, "com.android.internal.inputmethod.IMultiClientInputMethodPrivilegedOperations");
    }
    
    public static IMultiClientInputMethodPrivilegedOperations asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.inputmethod.IMultiClientInputMethodPrivilegedOperations");
      if (iInterface != null && iInterface instanceof IMultiClientInputMethodPrivilegedOperations)
        return (IMultiClientInputMethodPrivilegedOperations)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 6:
          return "setActive";
        case 5:
          return "isUidAllowedOnDisplay";
        case 4:
          return "reportImeWindowTarget";
        case 3:
          return "acceptClient";
        case 2:
          return "deleteInputMethodWindowToken";
        case 1:
          break;
      } 
      return "createInputMethodWindowToken";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        boolean bool1;
        IInputMethodSession iInputMethodSession;
        IMultiClientInputMethodSession iMultiClientInputMethodSession;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 6:
            param1Parcel1.enforceInterface("com.android.internal.inputmethod.IMultiClientInputMethodPrivilegedOperations");
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            setActive(param1Int1, bool1);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            param1Parcel1.enforceInterface("com.android.internal.inputmethod.IMultiClientInputMethodPrivilegedOperations");
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            bool = isUidAllowedOnDisplay(param1Int2, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 4:
            param1Parcel1.enforceInterface("com.android.internal.inputmethod.IMultiClientInputMethodPrivilegedOperations");
            param1Int2 = param1Parcel1.readInt();
            i = param1Parcel1.readInt();
            iBinder = param1Parcel1.readStrongBinder();
            reportImeWindowTarget(param1Int2, i, iBinder);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            iBinder.enforceInterface("com.android.internal.inputmethod.IMultiClientInputMethodPrivilegedOperations");
            i = iBinder.readInt();
            iInputMethodSession = IInputMethodSession.Stub.asInterface(iBinder.readStrongBinder());
            iMultiClientInputMethodSession = IMultiClientInputMethodSession.Stub.asInterface(iBinder.readStrongBinder());
            if (iBinder.readInt() != 0) {
              InputChannel inputChannel = (InputChannel)InputChannel.CREATOR.createFromParcel((Parcel)iBinder);
            } else {
              iBinder = null;
            } 
            acceptClient(i, iInputMethodSession, iMultiClientInputMethodSession, (InputChannel)iBinder);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            iBinder.enforceInterface("com.android.internal.inputmethod.IMultiClientInputMethodPrivilegedOperations");
            iBinder = iBinder.readStrongBinder();
            deleteInputMethodWindowToken(iBinder);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        iBinder.enforceInterface("com.android.internal.inputmethod.IMultiClientInputMethodPrivilegedOperations");
        int i = iBinder.readInt();
        IBinder iBinder = createInputMethodWindowToken(i);
        param1Parcel2.writeNoException();
        param1Parcel2.writeStrongBinder(iBinder);
        return true;
      } 
      param1Parcel2.writeString("com.android.internal.inputmethod.IMultiClientInputMethodPrivilegedOperations");
      return true;
    }
    
    private static class Proxy implements IMultiClientInputMethodPrivilegedOperations {
      public static IMultiClientInputMethodPrivilegedOperations sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.inputmethod.IMultiClientInputMethodPrivilegedOperations";
      }
      
      public IBinder createInputMethodWindowToken(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.inputmethod.IMultiClientInputMethodPrivilegedOperations");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IMultiClientInputMethodPrivilegedOperations.Stub.getDefaultImpl() != null)
            return IMultiClientInputMethodPrivilegedOperations.Stub.getDefaultImpl().createInputMethodWindowToken(param2Int); 
          parcel2.readException();
          return parcel2.readStrongBinder();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deleteInputMethodWindowToken(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.inputmethod.IMultiClientInputMethodPrivilegedOperations");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IMultiClientInputMethodPrivilegedOperations.Stub.getDefaultImpl() != null) {
            IMultiClientInputMethodPrivilegedOperations.Stub.getDefaultImpl().deleteInputMethodWindowToken(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void acceptClient(int param2Int, IInputMethodSession param2IInputMethodSession, IMultiClientInputMethodSession param2IMultiClientInputMethodSession, InputChannel param2InputChannel) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.inputmethod.IMultiClientInputMethodPrivilegedOperations");
          parcel1.writeInt(param2Int);
          IBinder iBinder1 = null;
          if (param2IInputMethodSession != null) {
            iBinder2 = param2IInputMethodSession.asBinder();
          } else {
            iBinder2 = null;
          } 
          parcel1.writeStrongBinder(iBinder2);
          IBinder iBinder2 = iBinder1;
          if (param2IMultiClientInputMethodSession != null)
            iBinder2 = param2IMultiClientInputMethodSession.asBinder(); 
          parcel1.writeStrongBinder(iBinder2);
          if (param2InputChannel != null) {
            parcel1.writeInt(1);
            param2InputChannel.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IMultiClientInputMethodPrivilegedOperations.Stub.getDefaultImpl() != null) {
            IMultiClientInputMethodPrivilegedOperations.Stub.getDefaultImpl().acceptClient(param2Int, param2IInputMethodSession, param2IMultiClientInputMethodSession, param2InputChannel);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportImeWindowTarget(int param2Int1, int param2Int2, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.inputmethod.IMultiClientInputMethodPrivilegedOperations");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IMultiClientInputMethodPrivilegedOperations.Stub.getDefaultImpl() != null) {
            IMultiClientInputMethodPrivilegedOperations.Stub.getDefaultImpl().reportImeWindowTarget(param2Int1, param2Int2, param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isUidAllowedOnDisplay(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.inputmethod.IMultiClientInputMethodPrivilegedOperations");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IMultiClientInputMethodPrivilegedOperations.Stub.getDefaultImpl() != null) {
            bool1 = IMultiClientInputMethodPrivilegedOperations.Stub.getDefaultImpl().isUidAllowedOnDisplay(param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setActive(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.inputmethod.IMultiClientInputMethodPrivilegedOperations");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool1 && IMultiClientInputMethodPrivilegedOperations.Stub.getDefaultImpl() != null) {
            IMultiClientInputMethodPrivilegedOperations.Stub.getDefaultImpl().setActive(param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMultiClientInputMethodPrivilegedOperations param1IMultiClientInputMethodPrivilegedOperations) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMultiClientInputMethodPrivilegedOperations != null) {
          Proxy.sDefaultImpl = param1IMultiClientInputMethodPrivilegedOperations;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMultiClientInputMethodPrivilegedOperations getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
