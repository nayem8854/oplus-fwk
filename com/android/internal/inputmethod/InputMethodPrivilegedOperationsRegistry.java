package com.android.internal.inputmethod;

import android.os.IBinder;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;

public final class InputMethodPrivilegedOperationsRegistry {
  private static final Object sLock = new Object();
  
  private static InputMethodPrivilegedOperations sNop;
  
  private static WeakHashMap<IBinder, WeakReference<InputMethodPrivilegedOperations>> sRegistry;
  
  private static InputMethodPrivilegedOperations getNopOps() {
    if (sNop == null)
      sNop = new InputMethodPrivilegedOperations(); 
    return sNop;
  }
  
  public static void put(IBinder paramIBinder, InputMethodPrivilegedOperations paramInputMethodPrivilegedOperations) {
    synchronized (sLock) {
      if (sRegistry == null) {
        WeakHashMap<Object, Object> weakHashMap1 = new WeakHashMap<>();
        this();
        sRegistry = (WeakHashMap)weakHashMap1;
      } 
      WeakHashMap<IBinder, WeakReference<InputMethodPrivilegedOperations>> weakHashMap = sRegistry;
      WeakReference<InputMethodPrivilegedOperations> weakReference = new WeakReference();
      this((T)paramInputMethodPrivilegedOperations);
      WeakReference weakReference1 = weakHashMap.put(paramIBinder, weakReference);
      if (weakReference1 == null)
        return; 
      IllegalStateException illegalStateException = new IllegalStateException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append(weakReference1.get());
      stringBuilder.append(" is already registered for  this token=");
      stringBuilder.append(paramIBinder);
      stringBuilder.append(" newOps=");
      stringBuilder.append(paramInputMethodPrivilegedOperations);
      this(stringBuilder.toString());
      throw illegalStateException;
    } 
  }
  
  public static InputMethodPrivilegedOperations get(IBinder paramIBinder) {
    synchronized (sLock) {
      InputMethodPrivilegedOperations inputMethodPrivilegedOperations2;
      if (sRegistry == null) {
        inputMethodPrivilegedOperations2 = getNopOps();
        return inputMethodPrivilegedOperations2;
      } 
      WeakReference weakReference = sRegistry.get(inputMethodPrivilegedOperations2);
      if (weakReference == null) {
        inputMethodPrivilegedOperations1 = getNopOps();
        return inputMethodPrivilegedOperations1;
      } 
      InputMethodPrivilegedOperations inputMethodPrivilegedOperations1 = inputMethodPrivilegedOperations1.get();
      if (inputMethodPrivilegedOperations1 == null) {
        inputMethodPrivilegedOperations1 = getNopOps();
        return inputMethodPrivilegedOperations1;
      } 
      return inputMethodPrivilegedOperations1;
    } 
  }
  
  public static void remove(IBinder paramIBinder) {
    synchronized (sLock) {
      if (sRegistry == null)
        return; 
      sRegistry.remove(paramIBinder);
      if (sRegistry.isEmpty())
        sRegistry = null; 
      return;
    } 
  }
  
  public static boolean isRegistered(IBinder paramIBinder) {
    synchronized (sLock) {
      if (sRegistry == null)
        return false; 
      return sRegistry.containsKey(paramIBinder);
    } 
  }
}
