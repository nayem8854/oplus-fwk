package com.android.internal.inputmethod;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public final class CancellationGroup {
  private final Object mLock = new Object();
  
  private ArrayList<CountDownLatch> mLatchList = null;
  
  private boolean mCanceled = false;
  
  public static final class Completable {
    protected static class ValueBase {
      private final CountDownLatch mLatch = new CountDownLatch(1);
      
      protected final Object mValueLock = new Object();
      
      protected boolean mHasValue = false;
      
      private final CancellationGroup mParentGroup;
      
      protected ValueBase(CancellationGroup param2CancellationGroup) {
        this.mParentGroup = param2CancellationGroup;
      }
      
      public boolean hasValue() {
        synchronized (this.mValueLock) {
          return this.mHasValue;
        } 
      }
      
      protected void onComplete() {
        this.mLatch.countDown();
      }
      
      public boolean await(int param2Int, TimeUnit param2TimeUnit) {
        if (!this.mParentGroup.registerLatch(this.mLatch))
          return false; 
        try {
          return this.mLatch.await(param2Int, param2TimeUnit);
        } catch (InterruptedException interruptedException) {
          return true;
        } finally {
          this.mParentGroup.unregisterLatch(this.mLatch);
        } 
      }
    }
    
    class Int extends ValueBase {
      private int mValue = 0;
      
      void onComplete(int param2Int) {
        synchronized (this.mValueLock) {
          if (!this.mHasValue) {
            this.mValue = param2Int;
            this.mHasValue = true;
            onComplete();
            return;
          } 
          UnsupportedOperationException unsupportedOperationException = new UnsupportedOperationException();
          this("onComplete() cannot be called multiple times");
          throw unsupportedOperationException;
        } 
      }
      
      public int getValue() {
        synchronized (this.mValueLock) {
          if (this.mHasValue)
            return this.mValue; 
          UnsupportedOperationException unsupportedOperationException = new UnsupportedOperationException();
          this("getValue() is allowed only if hasValue() returns true");
          throw unsupportedOperationException;
        } 
      }
      
      private Int(CancellationGroup.Completable this$0) {}
    }
    
    class Values<T> extends ValueBase {
      private T mValue = null;
      
      protected Values(CancellationGroup.Completable this$0) {
        super((CancellationGroup)this$0);
      }
      
      void onComplete(T param2T) {
        synchronized (this.mValueLock) {
          if (!this.mHasValue) {
            this.mValue = param2T;
            this.mHasValue = true;
            onComplete();
            return;
          } 
          UnsupportedOperationException unsupportedOperationException = new UnsupportedOperationException();
          this("onComplete() cannot be called multiple times");
          throw unsupportedOperationException;
        } 
      }
      
      public T getValue() {
        synchronized (this.mValueLock) {
          if (this.mHasValue)
            return this.mValue; 
          UnsupportedOperationException unsupportedOperationException = new UnsupportedOperationException();
          this("getValue() is allowed only if hasValue() returns true");
          throw unsupportedOperationException;
        } 
      }
    }
    
    class CharSequence extends Values<CharSequence> {
      private CharSequence(CancellationGroup.Completable this$0) {}
    }
    
    class ExtractedText extends Values<android.view.inputmethod.ExtractedText> {
      private ExtractedText(CancellationGroup.Completable this$0) {}
    }
  }
  
  protected static class ValueBase {
    private final CountDownLatch mLatch = new CountDownLatch(1);
    
    protected final Object mValueLock = new Object();
    
    protected boolean mHasValue = false;
    
    private final CancellationGroup mParentGroup;
    
    protected ValueBase(CancellationGroup param1CancellationGroup) {
      this.mParentGroup = param1CancellationGroup;
    }
    
    public boolean hasValue() {
      synchronized (this.mValueLock) {
        return this.mHasValue;
      } 
    }
    
    protected void onComplete() {
      this.mLatch.countDown();
    }
    
    public boolean await(int param1Int, TimeUnit param1TimeUnit) {
      if (!this.mParentGroup.registerLatch(this.mLatch))
        return false; 
      try {
        return this.mLatch.await(param1Int, param1TimeUnit);
      } catch (InterruptedException interruptedException) {
        return true;
      } finally {
        this.mParentGroup.unregisterLatch(this.mLatch);
      } 
    }
  }
  
  class Int extends Completable.ValueBase {
    private int mValue = 0;
    
    void onComplete(int param1Int) {
      synchronized (this.mValueLock) {
        if (!this.mHasValue) {
          this.mValue = param1Int;
          this.mHasValue = true;
          onComplete();
          return;
        } 
        UnsupportedOperationException unsupportedOperationException = new UnsupportedOperationException();
        this("onComplete() cannot be called multiple times");
        throw unsupportedOperationException;
      } 
    }
    
    public int getValue() {
      synchronized (this.mValueLock) {
        if (this.mHasValue)
          return this.mValue; 
        UnsupportedOperationException unsupportedOperationException = new UnsupportedOperationException();
        this("getValue() is allowed only if hasValue() returns true");
        throw unsupportedOperationException;
      } 
    }
    
    private Int(CancellationGroup this$0) {
      super(CancellationGroup.this);
    }
  }
  
  class Values<T> extends Completable.ValueBase {
    private T mValue = null;
    
    protected Values(CancellationGroup this$0) {
      super(this$0);
    }
    
    void onComplete(T param1T) {
      synchronized (this.mValueLock) {
        if (!this.mHasValue) {
          this.mValue = param1T;
          this.mHasValue = true;
          onComplete();
          return;
        } 
        UnsupportedOperationException unsupportedOperationException = new UnsupportedOperationException();
        this("onComplete() cannot be called multiple times");
        throw unsupportedOperationException;
      } 
    }
    
    public T getValue() {
      synchronized (this.mValueLock) {
        if (this.mHasValue)
          return this.mValue; 
        UnsupportedOperationException unsupportedOperationException = new UnsupportedOperationException();
        this("getValue() is allowed only if hasValue() returns true");
        throw unsupportedOperationException;
      } 
    }
  }
  
  class CharSequence extends Completable.Values<CharSequence> {
    private CharSequence(CancellationGroup this$0) {
      super(CancellationGroup.this);
    }
  }
  
  class ExtractedText extends Completable.Values<android.view.inputmethod.ExtractedText> {
    private ExtractedText(CancellationGroup this$0) {
      super(CancellationGroup.this);
    }
  }
  
  public Completable.Int createCompletableInt() {
    return new Completable.Int();
  }
  
  public Completable.CharSequence createCompletableCharSequence() {
    return new Completable.CharSequence();
  }
  
  public Completable.ExtractedText createCompletableExtractedText() {
    return new Completable.ExtractedText();
  }
  
  private boolean registerLatch(CountDownLatch paramCountDownLatch) {
    synchronized (this.mLock) {
      if (this.mCanceled)
        return false; 
      if (this.mLatchList == null) {
        ArrayList<CountDownLatch> arrayList = new ArrayList();
        this(1);
        this.mLatchList = arrayList;
      } 
      this.mLatchList.add(paramCountDownLatch);
      return true;
    } 
  }
  
  private void unregisterLatch(CountDownLatch paramCountDownLatch) {
    synchronized (this.mLock) {
      if (this.mLatchList != null)
        this.mLatchList.remove(paramCountDownLatch); 
      return;
    } 
  }
  
  public void cancelAll() {
    synchronized (this.mLock) {
      if (!this.mCanceled) {
        this.mCanceled = true;
        if (this.mLatchList != null) {
          this.mLatchList.forEach((Consumer<? super CountDownLatch>)_$$Lambda$wMDtnoultRQpGB37stq5CwJoQnU.INSTANCE);
          this.mLatchList.clear();
          this.mLatchList = null;
        } 
      } 
      return;
    } 
  }
  
  public boolean isCanceled() {
    synchronized (this.mLock) {
      return this.mCanceled;
    } 
  }
}
