package com.android.internal.logging;

import com.android.internal.util.FrameworkStatsLog;

public class UiEventLoggerImpl implements UiEventLogger {
  public void log(UiEventLogger.UiEventEnum paramUiEventEnum) {
    log(paramUiEventEnum, 0, null);
  }
  
  public void log(UiEventLogger.UiEventEnum paramUiEventEnum, int paramInt, String paramString) {
    int i = paramUiEventEnum.getId();
    if (i > 0)
      FrameworkStatsLog.write(90, i, paramInt, paramString); 
  }
  
  public void logWithInstanceId(UiEventLogger.UiEventEnum paramUiEventEnum, int paramInt, String paramString, InstanceId paramInstanceId) {
    int i = paramUiEventEnum.getId();
    if (i > 0 && paramInstanceId != null) {
      int j = paramInstanceId.getId();
      FrameworkStatsLog.write(90, i, paramInt, paramString, j);
    } else {
      log(paramUiEventEnum, paramInt, paramString);
    } 
  }
  
  public void logWithPosition(UiEventLogger.UiEventEnum paramUiEventEnum, int paramInt1, String paramString, int paramInt2) {
    paramInt1 = paramUiEventEnum.getId();
    if (paramInt1 > 0)
      FrameworkStatsLog.write(260, paramInt1, paramString, 0, paramInt2); 
  }
  
  public void logWithInstanceIdAndPosition(UiEventLogger.UiEventEnum paramUiEventEnum, int paramInt1, String paramString, InstanceId paramInstanceId, int paramInt2) {
    int i = paramUiEventEnum.getId();
    if (i > 0 && paramInstanceId != null) {
      paramInt1 = paramInstanceId.getId();
      FrameworkStatsLog.write(260, i, paramString, paramInt1, paramInt2);
    } else {
      logWithPosition(paramUiEventEnum, paramInt1, paramString, paramInt2);
    } 
  }
}
