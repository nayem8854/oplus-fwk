package com.android.internal.logging;

public interface UiEventLogger {
  void log(UiEventEnum paramUiEventEnum);
  
  void log(UiEventEnum paramUiEventEnum, int paramInt, String paramString);
  
  void logWithInstanceId(UiEventEnum paramUiEventEnum, int paramInt, String paramString, InstanceId paramInstanceId);
  
  void logWithInstanceIdAndPosition(UiEventEnum paramUiEventEnum, int paramInt1, String paramString, InstanceId paramInstanceId, int paramInt2);
  
  void logWithPosition(UiEventEnum paramUiEventEnum, int paramInt1, String paramString, int paramInt2);
  
  public static interface UiEventEnum {
    int getId();
  }
}
