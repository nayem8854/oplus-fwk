package com.android.internal.logging;

import android.util.Log;
import com.android.internal.util.FastPrintWriter;
import dalvik.system.DalvikLogHandler;
import dalvik.system.DalvikLogging;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class AndroidHandler extends Handler implements DalvikLogHandler {
  private static final Formatter THE_FORMATTER = new Formatter() {
      public String format(LogRecord param1LogRecord) {
        Throwable throwable = param1LogRecord.getThrown();
        if (throwable != null) {
          StringWriter stringWriter = new StringWriter();
          FastPrintWriter fastPrintWriter = new FastPrintWriter(stringWriter, false, 256);
          stringWriter.write(param1LogRecord.getMessage());
          stringWriter.write("\n");
          throwable.printStackTrace((PrintWriter)fastPrintWriter);
          fastPrintWriter.flush();
          return stringWriter.toString();
        } 
        return param1LogRecord.getMessage();
      }
    };
  
  public AndroidHandler() {
    setFormatter(THE_FORMATTER);
  }
  
  public void close() {}
  
  public void flush() {}
  
  public void publish(LogRecord paramLogRecord) {
    int i = getAndroidLevel(paramLogRecord.getLevel());
    String str = DalvikLogging.loggerNameToTag(paramLogRecord.getLoggerName());
    if (!Log.isLoggable(str, i))
      return; 
    try {
      String str1 = getFormatter().format(paramLogRecord);
      Log.println(i, str, str1);
    } catch (RuntimeException runtimeException) {
      Log.e("AndroidHandler", "Error logging message.", runtimeException);
    } 
  }
  
  public void publish(Logger paramLogger, String paramString1, Level paramLevel, String paramString2) {
    int i = getAndroidLevel(paramLevel);
    if (!Log.isLoggable(paramString1, i))
      return; 
    try {
      Log.println(i, paramString1, paramString2);
    } catch (RuntimeException runtimeException) {
      Log.e("AndroidHandler", "Error logging message.", runtimeException);
    } 
  }
  
  static int getAndroidLevel(Level paramLevel) {
    int i = paramLevel.intValue();
    if (i >= 1000)
      return 6; 
    if (i >= 900)
      return 5; 
    if (i >= 800)
      return 4; 
    return 3;
  }
}
