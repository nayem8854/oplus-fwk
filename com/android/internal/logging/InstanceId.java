package com.android.internal.logging;

import android.os.Parcel;
import android.os.Parcelable;

public final class InstanceId implements Parcelable {
  InstanceId(int paramInt) {
    this.mId = Math.min(Math.max(0, paramInt), 1048576);
  }
  
  private InstanceId(Parcel paramParcel) {
    this(paramParcel.readInt());
  }
  
  public int getId() {
    return this.mId;
  }
  
  public static InstanceId fakeInstanceId(int paramInt) {
    return new InstanceId(paramInt);
  }
  
  public int hashCode() {
    return this.mId;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof InstanceId;
    boolean bool1 = false;
    if (!bool)
      return false; 
    if (this.mId == ((InstanceId)paramObject).mId)
      bool1 = true; 
    return bool1;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mId);
  }
  
  public static final Parcelable.Creator<InstanceId> CREATOR = new Parcelable.Creator<InstanceId>() {
      public InstanceId createFromParcel(Parcel param1Parcel) {
        return new InstanceId(param1Parcel);
      }
      
      public InstanceId[] newArray(int param1Int) {
        return new InstanceId[param1Int];
      }
    };
  
  static final int INSTANCE_ID_MAX = 1048576;
  
  private final int mId;
}
