package com.android.internal.logging;

import java.security.SecureRandom;
import java.util.Random;

public class InstanceIdSequence {
  protected final int mInstanceIdMax;
  
  private final Random mRandom = new SecureRandom();
  
  public InstanceIdSequence(int paramInt) {
    this.mInstanceIdMax = Math.min(Math.max(1, paramInt), 1048576);
  }
  
  public InstanceId newInstanceId() {
    return newInstanceIdInternal(this.mRandom.nextInt(this.mInstanceIdMax) + 1);
  }
  
  protected InstanceId newInstanceIdInternal(int paramInt) {
    return new InstanceId(paramInt);
  }
}
