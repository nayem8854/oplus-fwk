package com.android.internal.logging.testing;

import com.android.internal.logging.InstanceId;
import com.android.internal.logging.UiEventLogger;
import java.util.LinkedList;
import java.util.List;

public class UiEventLoggerFake implements UiEventLogger {
  class FakeUiEvent {
    public final int eventId;
    
    public final InstanceId instanceId;
    
    public final String packageName;
    
    public final int position;
    
    public final int uid;
    
    FakeUiEvent(UiEventLoggerFake this$0, int param1Int1, String param1String) {
      this.eventId = this$0;
      this.uid = param1Int1;
      this.packageName = param1String;
      this.instanceId = null;
      this.position = 0;
    }
    
    FakeUiEvent(UiEventLoggerFake this$0, int param1Int1, String param1String, InstanceId param1InstanceId) {
      this.eventId = this$0;
      this.uid = param1Int1;
      this.packageName = param1String;
      this.instanceId = param1InstanceId;
      this.position = 0;
    }
    
    FakeUiEvent(UiEventLoggerFake this$0, int param1Int1, String param1String, InstanceId param1InstanceId, int param1Int2) {
      this.eventId = this$0;
      this.uid = param1Int1;
      this.packageName = param1String;
      this.instanceId = param1InstanceId;
      this.position = param1Int2;
    }
  }
  
  private List<FakeUiEvent> mLogs = new LinkedList<>();
  
  public List<FakeUiEvent> getLogs() {
    return this.mLogs;
  }
  
  public int numLogs() {
    return this.mLogs.size();
  }
  
  public FakeUiEvent get(int paramInt) {
    return this.mLogs.get(paramInt);
  }
  
  public int eventId(int paramInt) {
    return ((FakeUiEvent)this.mLogs.get(paramInt)).eventId;
  }
  
  public void log(UiEventLogger.UiEventEnum paramUiEventEnum) {
    log(paramUiEventEnum, 0, null);
  }
  
  public void log(UiEventLogger.UiEventEnum paramUiEventEnum, int paramInt, String paramString) {
    int i = paramUiEventEnum.getId();
    if (i > 0)
      this.mLogs.add(new FakeUiEvent(i, paramInt, paramString)); 
  }
  
  public void logWithInstanceId(UiEventLogger.UiEventEnum paramUiEventEnum, int paramInt, String paramString, InstanceId paramInstanceId) {
    int i = paramUiEventEnum.getId();
    if (i > 0)
      this.mLogs.add(new FakeUiEvent(i, paramInt, paramString, paramInstanceId)); 
  }
  
  public void logWithPosition(UiEventLogger.UiEventEnum paramUiEventEnum, int paramInt1, String paramString, int paramInt2) {
    int i = paramUiEventEnum.getId();
    if (i > 0)
      this.mLogs.add(new FakeUiEvent(i, paramInt1, paramString, null, paramInt2)); 
  }
  
  public void logWithInstanceIdAndPosition(UiEventLogger.UiEventEnum paramUiEventEnum, int paramInt1, String paramString, InstanceId paramInstanceId, int paramInt2) {
    int i = paramUiEventEnum.getId();
    if (i > 0)
      this.mLogs.add(new FakeUiEvent(i, paramInt1, paramString, paramInstanceId, paramInt2)); 
  }
}
