package com.android.internal.logging;

import android.content.Context;
import android.metrics.LogMaker;
import android.os.Build;
import android.util.SparseArray;
import com.android.internal.util.FrameworkStatsLog;

public class MetricsLogger {
  public static final int LOGTAG = 524292;
  
  public static final int VIEW_UNKNOWN = 0;
  
  private static MetricsLogger sMetricsLogger;
  
  private static MetricsLogger getLogger() {
    if (sMetricsLogger == null)
      sMetricsLogger = new MetricsLogger(); 
    return sMetricsLogger;
  }
  
  protected void saveLog(LogMaker paramLogMaker) {
    EventLogTags.writeSysuiMultiAction(paramLogMaker.serialize());
    SparseArray sparseArray = paramLogMaker.getEntries();
    FrameworkStatsLog.write(83, 0, sparseArray);
  }
  
  public void write(LogMaker paramLogMaker) {
    if (paramLogMaker.getType() == 0)
      paramLogMaker.setType(4); 
    saveLog(paramLogMaker);
  }
  
  public void count(String paramString, int paramInt) {
    LogMaker logMaker2 = new LogMaker(803);
    LogMaker logMaker1 = logMaker2.setCounterName(paramString);
    logMaker1 = logMaker1.setCounterValue(paramInt);
    saveLog(logMaker1);
  }
  
  public void histogram(String paramString, int paramInt) {
    LogMaker logMaker2 = new LogMaker(804);
    LogMaker logMaker1 = logMaker2.setCounterName(paramString);
    logMaker1 = logMaker1.setCounterBucket(paramInt);
    logMaker1 = logMaker1.setCounterValue(1);
    saveLog(logMaker1);
  }
  
  public void visible(int paramInt) throws IllegalArgumentException {
    if (!Build.IS_DEBUGGABLE || paramInt != 0) {
      saveLog((new LogMaker(paramInt)).setType(1));
      return;
    } 
    throw new IllegalArgumentException("Must define metric category");
  }
  
  public void hidden(int paramInt) throws IllegalArgumentException {
    if (!Build.IS_DEBUGGABLE || paramInt != 0) {
      saveLog((new LogMaker(paramInt)).setType(2));
      return;
    } 
    throw new IllegalArgumentException("Must define metric category");
  }
  
  public void visibility(int paramInt, boolean paramBoolean) throws IllegalArgumentException {
    if (paramBoolean) {
      visible(paramInt);
    } else {
      hidden(paramInt);
    } 
  }
  
  public void visibility(int paramInt1, int paramInt2) throws IllegalArgumentException {
    boolean bool;
    if (paramInt2 == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    visibility(paramInt1, bool);
  }
  
  public void action(int paramInt) {
    saveLog((new LogMaker(paramInt)).setType(4));
  }
  
  public void action(int paramInt1, int paramInt2) {
    saveLog((new LogMaker(paramInt1)).setType(4).setSubtype(paramInt2));
  }
  
  public void action(int paramInt, boolean paramBoolean) {
    saveLog((new LogMaker(paramInt)).setType(4).setSubtype(paramBoolean));
  }
  
  public void action(int paramInt, String paramString) {
    if (!Build.IS_DEBUGGABLE || paramInt != 0) {
      saveLog((new LogMaker(paramInt)).setType(4).setPackageName(paramString));
      return;
    } 
    throw new IllegalArgumentException("Must define metric category");
  }
  
  @Deprecated
  public static void visible(Context paramContext, int paramInt) throws IllegalArgumentException {
    getLogger().visible(paramInt);
  }
  
  @Deprecated
  public static void hidden(Context paramContext, int paramInt) throws IllegalArgumentException {
    getLogger().hidden(paramInt);
  }
  
  @Deprecated
  public static void visibility(Context paramContext, int paramInt, boolean paramBoolean) throws IllegalArgumentException {
    getLogger().visibility(paramInt, paramBoolean);
  }
  
  @Deprecated
  public static void visibility(Context paramContext, int paramInt1, int paramInt2) throws IllegalArgumentException {
    boolean bool;
    if (paramInt2 == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    visibility(paramContext, paramInt1, bool);
  }
  
  @Deprecated
  public static void action(Context paramContext, int paramInt) {
    getLogger().action(paramInt);
  }
  
  @Deprecated
  public static void action(Context paramContext, int paramInt1, int paramInt2) {
    getLogger().action(paramInt1, paramInt2);
  }
  
  @Deprecated
  public static void action(Context paramContext, int paramInt, boolean paramBoolean) {
    getLogger().action(paramInt, paramBoolean);
  }
  
  @Deprecated
  public static void action(LogMaker paramLogMaker) {
    getLogger().write(paramLogMaker);
  }
  
  @Deprecated
  public static void action(Context paramContext, int paramInt, String paramString) {
    getLogger().action(paramInt, paramString);
  }
  
  @Deprecated
  public static void count(Context paramContext, String paramString, int paramInt) {
    getLogger().count(paramString, paramInt);
  }
  
  @Deprecated
  public static void histogram(Context paramContext, String paramString, int paramInt) {
    getLogger().histogram(paramString, paramInt);
  }
}
