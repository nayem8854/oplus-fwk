package com.android.internal.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.DialogPreference;
import android.preference.Preference;
import android.util.AttributeSet;

public class YesNoPreference extends DialogPreference {
  private boolean mWasPositiveResult;
  
  public YesNoPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  public YesNoPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public YesNoPreference(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 16842896);
  }
  
  public YesNoPreference(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  protected void onDialogClosed(boolean paramBoolean) {
    super.onDialogClosed(paramBoolean);
    if (callChangeListener(Boolean.valueOf(paramBoolean)))
      setValue(paramBoolean); 
  }
  
  public void setValue(boolean paramBoolean) {
    this.mWasPositiveResult = paramBoolean;
    persistBoolean(paramBoolean);
    notifyDependencyChange(paramBoolean ^ true);
  }
  
  public boolean getValue() {
    return this.mWasPositiveResult;
  }
  
  protected Object onGetDefaultValue(TypedArray paramTypedArray, int paramInt) {
    return Boolean.valueOf(paramTypedArray.getBoolean(paramInt, false));
  }
  
  protected void onSetInitialValue(boolean paramBoolean, Object paramObject) {
    if (paramBoolean) {
      paramBoolean = getPersistedBoolean(this.mWasPositiveResult);
    } else {
      paramBoolean = ((Boolean)paramObject).booleanValue();
    } 
    setValue(paramBoolean);
  }
  
  public boolean shouldDisableDependents() {
    return (!this.mWasPositiveResult || super.shouldDisableDependents());
  }
  
  protected Parcelable onSaveInstanceState() {
    Parcelable parcelable = super.onSaveInstanceState();
    if (isPersistent())
      return parcelable; 
    SavedState savedState = new SavedState(parcelable);
    savedState.wasPositiveResult = getValue();
    return (Parcelable)savedState;
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable) {
    if (!paramParcelable.getClass().equals(SavedState.class)) {
      super.onRestoreInstanceState(paramParcelable);
      return;
    } 
    SavedState savedState = (SavedState)paramParcelable;
    super.onRestoreInstanceState(savedState.getSuperState());
    setValue(savedState.wasPositiveResult);
  }
  
  class SavedState extends Preference.BaseSavedState {
    public SavedState(YesNoPreference this$0) {
      super((Parcel)this$0);
      int i = this$0.readInt();
      boolean bool = true;
      if (i != 1)
        bool = false; 
      this.wasPositiveResult = bool;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeInt(this.wasPositiveResult);
    }
    
    public SavedState(YesNoPreference this$0) {
      super((Parcelable)this$0);
    }
    
    public static final Parcelable.Creator<SavedState> CREATOR = (Parcelable.Creator<SavedState>)new Object();
    
    boolean wasPositiveResult;
  }
}
