package com.android.internal.telephony;

import android.app.AppOpsManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.util.Log;

public final class OplusSmsApplication {
  private static final String LOG_TAG = "OplusSmsApplication";
  
  private static final String[] OEM_PACKAGE_ALLOW_WRITE_SMS;
  
  private static final String[] OEM_PACKAGE_MO_SMS_NOT_SHOW_IN_UI = new String[] { 
      "com.color.safecenter", "com.oppo.trafficmonitor", "com.oppo.activation", "com.nxp.wallet.oppo", "com.oppo.yellowpage", "com.oppo.systemhelper", "com.heytap.usercenter", "com.coloros.activation", "com.coloros.findmyphone", "com.oppo.oppopowermonitor", 
      "com.oppo.usercenter" };
  
  static {
    OEM_PACKAGE_ALLOW_WRITE_SMS = new String[] { "com.oppo.engineermode", "com.android.settings" };
  }
  
  public static boolean shouldWriteMessageForPackage(String paramString) {
    try {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("sw pkg=");
      stringBuilder.append(paramString);
      Log.d("OplusSmsApplication", stringBuilder.toString());
      if (OEM_PACKAGE_MO_SMS_NOT_SHOW_IN_UI != null)
        for (String str : OEM_PACKAGE_MO_SMS_NOT_SHOW_IN_UI) {
          if (str != null) {
            boolean bool = str.equals(paramString);
            if (bool)
              return false; 
          } 
        }  
    } catch (Exception exception) {}
    return true;
  }
  
  public static void oemAssignExclusiveSmsPermissionsToSystemApp(Context paramContext, PackageManager paramPackageManager, AppOpsManager paramAppOpsManager, Action paramAction) {
    try {
      if (OEM_PACKAGE_ALLOW_WRITE_SMS != null)
        for (byte b = 0; b < OEM_PACKAGE_ALLOW_WRITE_SMS.length; b++) {
          String str = OEM_PACKAGE_ALLOW_WRITE_SMS[b];
          if (!TextUtils.isEmpty(str))
            paramAction.doAction(str); 
        }  
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
  
  static interface Action {
    void doAction(String param1String);
  }
}
