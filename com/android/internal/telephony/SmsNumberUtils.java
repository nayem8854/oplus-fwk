package com.android.internal.telephony;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Binder;
import android.os.PersistableBundle;
import android.os.SystemProperties;
import android.telephony.CarrierConfigManager;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import com.android.internal.telephony.util.TelephonyUtils;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;

public class SmsNumberUtils {
  private static int[] ALL_COUNTRY_CODES;
  
  private static final int CDMA_HOME_NETWORK = 1;
  
  private static final int CDMA_ROAMING_NETWORK = 2;
  
  private static final boolean DBG;
  
  private static final int GSM_UMTS_NETWORK = 0;
  
  private static HashMap<String, ArrayList<String>> IDDS_MAPS;
  
  private static int MAX_COUNTRY_CODES_LENGTH = 0;
  
  private static final int MIN_COUNTRY_AREA_LOCAL_LENGTH = 10;
  
  private static final int NANP_CC = 1;
  
  private static final String NANP_IDD = "011";
  
  private static final int NANP_LONG_LENGTH = 11;
  
  private static final int NANP_MEDIUM_LENGTH = 10;
  
  private static final String NANP_NDD = "1";
  
  private static final int NANP_SHORT_LENGTH = 7;
  
  private static final int NP_CC_AREA_LOCAL = 104;
  
  private static final int NP_HOMEIDD_CC_AREA_LOCAL = 101;
  
  private static final int NP_INTERNATIONAL_BEGIN = 100;
  
  private static final int NP_LOCALIDD_CC_AREA_LOCAL = 103;
  
  private static final int NP_NANP_AREA_LOCAL = 2;
  
  private static final int NP_NANP_BEGIN = 1;
  
  private static final int NP_NANP_LOCAL = 1;
  
  private static final int NP_NANP_LOCALIDD_CC_AREA_LOCAL = 5;
  
  private static final int NP_NANP_NBPCD_CC_AREA_LOCAL = 4;
  
  private static final int NP_NANP_NBPCD_HOMEIDD_CC_AREA_LOCAL = 6;
  
  private static final int NP_NANP_NDD_AREA_LOCAL = 3;
  
  private static final int NP_NBPCD_CC_AREA_LOCAL = 102;
  
  private static final int NP_NBPCD_HOMEIDD_CC_AREA_LOCAL = 100;
  
  private static final int NP_NONE = 0;
  
  private static final String PLUS_SIGN = "+";
  
  private static final String TAG = "SmsNumberUtils";
  
  static {
    boolean bool = false;
    if (SystemProperties.getInt("ro.debuggable", 0) == 1)
      bool = true; 
    DBG = bool;
    ALL_COUNTRY_CODES = null;
    IDDS_MAPS = new HashMap<>();
  }
  
  private static class NumberEntry {
    public String IDD;
    
    public int countryCode;
    
    public String number;
    
    public NumberEntry(String param1String) {
      this.number = param1String;
    }
  }
  
  private static String formatNumber(Context paramContext, String paramString1, String paramString2, int paramInt) {
    // Byte code:
    //   0: aload_1
    //   1: ifnull -> 637
    //   4: aload_2
    //   5: ifnull -> 626
    //   8: aload_2
    //   9: invokevirtual trim : ()Ljava/lang/String;
    //   12: invokevirtual length : ()I
    //   15: ifeq -> 626
    //   18: aload_1
    //   19: invokestatic extractNetworkPortion : (Ljava/lang/String;)Ljava/lang/String;
    //   22: astore_1
    //   23: aload_1
    //   24: ifnull -> 615
    //   27: aload_1
    //   28: invokevirtual length : ()I
    //   31: ifeq -> 615
    //   34: new com/android/internal/telephony/SmsNumberUtils$NumberEntry
    //   37: dup
    //   38: aload_1
    //   39: invokespecial <init> : (Ljava/lang/String;)V
    //   42: astore #4
    //   44: aload_0
    //   45: aload_2
    //   46: invokestatic getAllIDDs : (Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;
    //   49: astore #5
    //   51: aload #4
    //   53: aload #5
    //   55: invokestatic checkNANP : (Lcom/android/internal/telephony/SmsNumberUtils$NumberEntry;Ljava/util/ArrayList;)I
    //   58: istore #6
    //   60: getstatic com/android/internal/telephony/SmsNumberUtils.DBG : Z
    //   63: ifeq -> 102
    //   66: new java/lang/StringBuilder
    //   69: dup
    //   70: invokespecial <init> : ()V
    //   73: astore_2
    //   74: aload_2
    //   75: ldc_w 'NANP type: '
    //   78: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   81: pop
    //   82: aload_2
    //   83: iload #6
    //   85: invokestatic getNumberPlanType : (I)Ljava/lang/String;
    //   88: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   91: pop
    //   92: ldc 'SmsNumberUtils'
    //   94: aload_2
    //   95: invokevirtual toString : ()Ljava/lang/String;
    //   98: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   101: pop
    //   102: iload #6
    //   104: iconst_1
    //   105: if_icmpeq -> 613
    //   108: iload #6
    //   110: iconst_2
    //   111: if_icmpeq -> 613
    //   114: iload #6
    //   116: iconst_3
    //   117: if_icmpne -> 123
    //   120: goto -> 613
    //   123: iload #6
    //   125: iconst_4
    //   126: if_icmpne -> 150
    //   129: iload_3
    //   130: iconst_1
    //   131: if_icmpeq -> 144
    //   134: iload_3
    //   135: iconst_2
    //   136: if_icmpne -> 142
    //   139: goto -> 144
    //   142: aload_1
    //   143: areturn
    //   144: aload_1
    //   145: iconst_1
    //   146: invokevirtual substring : (I)Ljava/lang/String;
    //   149: areturn
    //   150: iconst_0
    //   151: istore #7
    //   153: iconst_0
    //   154: istore #8
    //   156: iconst_0
    //   157: istore #9
    //   159: iload #6
    //   161: iconst_5
    //   162: if_icmpne -> 257
    //   165: iload_3
    //   166: iconst_1
    //   167: if_icmpne -> 172
    //   170: aload_1
    //   171: areturn
    //   172: iload_3
    //   173: ifne -> 226
    //   176: iload #9
    //   178: istore_3
    //   179: aload #4
    //   181: getfield IDD : Ljava/lang/String;
    //   184: ifnull -> 196
    //   187: aload #4
    //   189: getfield IDD : Ljava/lang/String;
    //   192: invokevirtual length : ()I
    //   195: istore_3
    //   196: new java/lang/StringBuilder
    //   199: dup
    //   200: invokespecial <init> : ()V
    //   203: astore_0
    //   204: aload_0
    //   205: ldc '+'
    //   207: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   210: pop
    //   211: aload_0
    //   212: aload_1
    //   213: iload_3
    //   214: invokevirtual substring : (I)Ljava/lang/String;
    //   217: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   220: pop
    //   221: aload_0
    //   222: invokevirtual toString : ()Ljava/lang/String;
    //   225: areturn
    //   226: iload_3
    //   227: iconst_2
    //   228: if_icmpne -> 257
    //   231: iload #7
    //   233: istore_3
    //   234: aload #4
    //   236: getfield IDD : Ljava/lang/String;
    //   239: ifnull -> 251
    //   242: aload #4
    //   244: getfield IDD : Ljava/lang/String;
    //   247: invokevirtual length : ()I
    //   250: istore_3
    //   251: aload_1
    //   252: iload_3
    //   253: invokevirtual substring : (I)Ljava/lang/String;
    //   256: areturn
    //   257: aload_0
    //   258: aload #4
    //   260: aload #5
    //   262: ldc '011'
    //   264: invokestatic checkInternationalNumberPlan : (Landroid/content/Context;Lcom/android/internal/telephony/SmsNumberUtils$NumberEntry;Ljava/util/ArrayList;Ljava/lang/String;)I
    //   267: istore #9
    //   269: getstatic com/android/internal/telephony/SmsNumberUtils.DBG : Z
    //   272: ifeq -> 311
    //   275: new java/lang/StringBuilder
    //   278: dup
    //   279: invokespecial <init> : ()V
    //   282: astore_0
    //   283: aload_0
    //   284: ldc_w 'International type: '
    //   287: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   290: pop
    //   291: aload_0
    //   292: iload #9
    //   294: invokestatic getNumberPlanType : (I)Ljava/lang/String;
    //   297: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   300: pop
    //   301: ldc 'SmsNumberUtils'
    //   303: aload_0
    //   304: invokevirtual toString : ()Ljava/lang/String;
    //   307: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   310: pop
    //   311: aconst_null
    //   312: astore_2
    //   313: iload #9
    //   315: tableswitch default -> 348, 100 -> 539, 101 -> 534, 102 -> 501, 103 -> 437, 104 -> 374
    //   348: aload_2
    //   349: astore_0
    //   350: aload_1
    //   351: ldc '+'
    //   353: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   356: ifeq -> 603
    //   359: iload_3
    //   360: iconst_1
    //   361: if_icmpeq -> 554
    //   364: aload_2
    //   365: astore_0
    //   366: iload_3
    //   367: iconst_2
    //   368: if_icmpne -> 603
    //   371: goto -> 554
    //   374: aload #4
    //   376: getfield countryCode : I
    //   379: istore_3
    //   380: aload_2
    //   381: astore_0
    //   382: aload #4
    //   384: invokestatic inExceptionListForNpCcAreaLocal : (Lcom/android/internal/telephony/SmsNumberUtils$NumberEntry;)Z
    //   387: ifne -> 603
    //   390: aload_2
    //   391: astore_0
    //   392: aload_1
    //   393: invokevirtual length : ()I
    //   396: bipush #11
    //   398: if_icmplt -> 603
    //   401: aload_2
    //   402: astore_0
    //   403: iload_3
    //   404: iconst_1
    //   405: if_icmpeq -> 603
    //   408: new java/lang/StringBuilder
    //   411: dup
    //   412: invokespecial <init> : ()V
    //   415: astore_0
    //   416: aload_0
    //   417: ldc '011'
    //   419: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   422: pop
    //   423: aload_0
    //   424: aload_1
    //   425: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   428: pop
    //   429: aload_0
    //   430: invokevirtual toString : ()Ljava/lang/String;
    //   433: astore_0
    //   434: goto -> 603
    //   437: iload_3
    //   438: ifeq -> 448
    //   441: aload_2
    //   442: astore_0
    //   443: iload_3
    //   444: iconst_2
    //   445: if_icmpne -> 603
    //   448: iload #8
    //   450: istore_3
    //   451: aload #4
    //   453: getfield IDD : Ljava/lang/String;
    //   456: ifnull -> 468
    //   459: aload #4
    //   461: getfield IDD : Ljava/lang/String;
    //   464: invokevirtual length : ()I
    //   467: istore_3
    //   468: new java/lang/StringBuilder
    //   471: dup
    //   472: invokespecial <init> : ()V
    //   475: astore_0
    //   476: aload_0
    //   477: ldc '011'
    //   479: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   482: pop
    //   483: aload_0
    //   484: aload_1
    //   485: iload_3
    //   486: invokevirtual substring : (I)Ljava/lang/String;
    //   489: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   492: pop
    //   493: aload_0
    //   494: invokevirtual toString : ()Ljava/lang/String;
    //   497: astore_0
    //   498: goto -> 603
    //   501: new java/lang/StringBuilder
    //   504: dup
    //   505: invokespecial <init> : ()V
    //   508: astore_0
    //   509: aload_0
    //   510: ldc '011'
    //   512: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   515: pop
    //   516: aload_0
    //   517: aload_1
    //   518: iconst_1
    //   519: invokevirtual substring : (I)Ljava/lang/String;
    //   522: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   525: pop
    //   526: aload_0
    //   527: invokevirtual toString : ()Ljava/lang/String;
    //   530: astore_0
    //   531: goto -> 603
    //   534: aload_1
    //   535: astore_0
    //   536: goto -> 603
    //   539: aload_2
    //   540: astore_0
    //   541: iload_3
    //   542: ifne -> 603
    //   545: aload_1
    //   546: iconst_1
    //   547: invokevirtual substring : (I)Ljava/lang/String;
    //   550: astore_0
    //   551: goto -> 603
    //   554: aload_1
    //   555: ldc_w '+011'
    //   558: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   561: ifeq -> 573
    //   564: aload_1
    //   565: iconst_1
    //   566: invokevirtual substring : (I)Ljava/lang/String;
    //   569: astore_0
    //   570: goto -> 603
    //   573: new java/lang/StringBuilder
    //   576: dup
    //   577: invokespecial <init> : ()V
    //   580: astore_0
    //   581: aload_0
    //   582: ldc '011'
    //   584: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   587: pop
    //   588: aload_0
    //   589: aload_1
    //   590: iconst_1
    //   591: invokevirtual substring : (I)Ljava/lang/String;
    //   594: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   597: pop
    //   598: aload_0
    //   599: invokevirtual toString : ()Ljava/lang/String;
    //   602: astore_0
    //   603: aload_0
    //   604: astore_2
    //   605: aload_0
    //   606: ifnonnull -> 611
    //   609: aload_1
    //   610: astore_2
    //   611: aload_2
    //   612: areturn
    //   613: aload_1
    //   614: areturn
    //   615: new java/lang/IllegalArgumentException
    //   618: dup
    //   619: ldc_w 'Number is invalid!'
    //   622: invokespecial <init> : (Ljava/lang/String;)V
    //   625: athrow
    //   626: new java/lang/IllegalArgumentException
    //   629: dup
    //   630: ldc_w 'activeMcc is null or empty!'
    //   633: invokespecial <init> : (Ljava/lang/String;)V
    //   636: athrow
    //   637: new java/lang/IllegalArgumentException
    //   640: dup
    //   641: ldc_w 'number is null'
    //   644: invokespecial <init> : (Ljava/lang/String;)V
    //   647: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #132	-> 0
    //   #136	-> 4
    //   #140	-> 18
    //   #141	-> 23
    //   #145	-> 34
    //   #146	-> 44
    //   #149	-> 51
    //   #150	-> 60
    //   #152	-> 102
    //   #156	-> 123
    //   #157	-> 129
    //   #162	-> 142
    //   #160	-> 144
    //   #164	-> 150
    //   #165	-> 165
    //   #166	-> 170
    //   #167	-> 172
    //   #169	-> 176
    //   #170	-> 196
    //   #171	-> 226
    //   #173	-> 231
    //   #174	-> 251
    //   #178	-> 257
    //   #180	-> 269
    //   #181	-> 311
    //   #183	-> 313
    //   #221	-> 348
    //   #205	-> 374
    //   #207	-> 380
    //   #208	-> 390
    //   #210	-> 408
    //   #197	-> 437
    //   #198	-> 448
    //   #200	-> 468
    //   #201	-> 498
    //   #193	-> 501
    //   #194	-> 531
    //   #215	-> 534
    //   #216	-> 536
    //   #185	-> 539
    //   #187	-> 545
    //   #223	-> 554
    //   #225	-> 564
    //   #228	-> 573
    //   #233	-> 603
    //   #234	-> 609
    //   #236	-> 611
    //   #155	-> 613
    //   #142	-> 615
    //   #137	-> 626
    //   #133	-> 637
  }
  
  private static ArrayList<String> getAllIDDs(Context paramContext, String paramString) {
    // Byte code:
    //   0: getstatic com/android/internal/telephony/SmsNumberUtils.IDDS_MAPS : Ljava/util/HashMap;
    //   3: aload_1
    //   4: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   7: checkcast java/util/ArrayList
    //   10: astore_2
    //   11: aload_2
    //   12: ifnull -> 17
    //   15: aload_2
    //   16: areturn
    //   17: new java/util/ArrayList
    //   20: dup
    //   21: invokespecial <init> : ()V
    //   24: astore_3
    //   25: aconst_null
    //   26: astore #4
    //   28: aconst_null
    //   29: astore #5
    //   31: aload_1
    //   32: ifnull -> 50
    //   35: ldc_w 'MCC=?'
    //   38: astore #4
    //   40: iconst_1
    //   41: anewarray java/lang/String
    //   44: dup
    //   45: iconst_0
    //   46: aload_1
    //   47: aastore
    //   48: astore #5
    //   50: aconst_null
    //   51: astore_2
    //   52: aconst_null
    //   53: astore #6
    //   55: aload_0
    //   56: invokevirtual getContentResolver : ()Landroid/content/ContentResolver;
    //   59: getstatic com/android/internal/telephony/HbpcdLookup$MccIdd.CONTENT_URI : Landroid/net/Uri;
    //   62: iconst_2
    //   63: anewarray java/lang/String
    //   66: dup
    //   67: iconst_0
    //   68: ldc_w 'IDD'
    //   71: aastore
    //   72: dup
    //   73: iconst_1
    //   74: ldc_w 'MCC'
    //   77: aastore
    //   78: aload #4
    //   80: aload #5
    //   82: aconst_null
    //   83: invokevirtual query : (Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   86: astore_0
    //   87: aload_0
    //   88: astore #6
    //   90: aload_0
    //   91: astore_2
    //   92: aload_0
    //   93: invokeinterface getCount : ()I
    //   98: ifle -> 158
    //   101: aload_0
    //   102: astore #6
    //   104: aload_0
    //   105: astore_2
    //   106: aload_0
    //   107: invokeinterface moveToNext : ()Z
    //   112: ifeq -> 158
    //   115: aload_0
    //   116: astore #6
    //   118: aload_0
    //   119: astore_2
    //   120: aload_0
    //   121: iconst_0
    //   122: invokeinterface getString : (I)Ljava/lang/String;
    //   127: astore #4
    //   129: aload_0
    //   130: astore #6
    //   132: aload_0
    //   133: astore_2
    //   134: aload_3
    //   135: aload #4
    //   137: invokevirtual contains : (Ljava/lang/Object;)Z
    //   140: ifne -> 155
    //   143: aload_0
    //   144: astore #6
    //   146: aload_0
    //   147: astore_2
    //   148: aload_3
    //   149: aload #4
    //   151: invokevirtual add : (Ljava/lang/Object;)Z
    //   154: pop
    //   155: goto -> 101
    //   158: aload_0
    //   159: ifnull -> 198
    //   162: aload_0
    //   163: invokeinterface close : ()V
    //   168: goto -> 198
    //   171: astore_0
    //   172: goto -> 261
    //   175: astore_0
    //   176: aload_2
    //   177: astore #6
    //   179: ldc 'SmsNumberUtils'
    //   181: ldc_w 'Can't access HbpcdLookup database'
    //   184: aload_0
    //   185: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   188: pop
    //   189: aload_2
    //   190: ifnull -> 198
    //   193: aload_2
    //   194: astore_0
    //   195: goto -> 162
    //   198: getstatic com/android/internal/telephony/SmsNumberUtils.IDDS_MAPS : Ljava/util/HashMap;
    //   201: aload_1
    //   202: aload_3
    //   203: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   206: pop
    //   207: getstatic com/android/internal/telephony/SmsNumberUtils.DBG : Z
    //   210: ifeq -> 259
    //   213: new java/lang/StringBuilder
    //   216: dup
    //   217: invokespecial <init> : ()V
    //   220: astore_0
    //   221: aload_0
    //   222: ldc_w 'MCC = '
    //   225: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   228: pop
    //   229: aload_0
    //   230: aload_1
    //   231: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   234: pop
    //   235: aload_0
    //   236: ldc_w ', all IDDs = '
    //   239: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   242: pop
    //   243: aload_0
    //   244: aload_3
    //   245: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   248: pop
    //   249: ldc 'SmsNumberUtils'
    //   251: aload_0
    //   252: invokevirtual toString : ()Ljava/lang/String;
    //   255: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   258: pop
    //   259: aload_3
    //   260: areturn
    //   261: aload #6
    //   263: ifnull -> 273
    //   266: aload #6
    //   268: invokeinterface close : ()V
    //   273: aload_0
    //   274: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #248	-> 0
    //   #249	-> 11
    //   #250	-> 15
    //   #252	-> 17
    //   #255	-> 25
    //   #256	-> 25
    //   #260	-> 28
    //   #261	-> 31
    //   #262	-> 35
    //   #263	-> 40
    //   #266	-> 50
    //   #268	-> 55
    //   #270	-> 87
    //   #271	-> 101
    //   #272	-> 115
    //   #273	-> 129
    //   #274	-> 143
    //   #276	-> 155
    //   #281	-> 158
    //   #282	-> 162
    //   #281	-> 171
    //   #278	-> 175
    //   #279	-> 176
    //   #281	-> 189
    //   #282	-> 193
    //   #286	-> 198
    //   #288	-> 207
    //   #289	-> 259
    //   #281	-> 261
    //   #282	-> 266
    //   #284	-> 273
    // Exception table:
    //   from	to	target	type
    //   55	87	175	android/database/SQLException
    //   55	87	171	finally
    //   92	101	175	android/database/SQLException
    //   92	101	171	finally
    //   106	115	175	android/database/SQLException
    //   106	115	171	finally
    //   120	129	175	android/database/SQLException
    //   120	129	171	finally
    //   134	143	175	android/database/SQLException
    //   134	143	171	finally
    //   148	155	175	android/database/SQLException
    //   148	155	171	finally
    //   179	189	171	finally
  }
  
  private static int checkNANP(NumberEntry paramNumberEntry, ArrayList<String> paramArrayList) {
    char c = Character.MIN_VALUE;
    String str = paramNumberEntry.number;
    if (str.length() == 7) {
      char c1 = str.charAt(0);
      char c2 = c;
      if (c1 >= '2') {
        c2 = c;
        if (c1 <= '9') {
          c1 = '\001';
          c = '\001';
          while (true) {
            c2 = c1;
            if (c < '\007') {
              char c3 = str.charAt(c);
              if (!PhoneNumberUtils.isISODigit(c3)) {
                c2 = Character.MIN_VALUE;
                break;
              } 
              c++;
              continue;
            } 
            break;
          } 
        } 
      } 
      if (c2 != '\000')
        return 1; 
    } else if (str.length() == 10) {
      if (isNANP(str))
        return 2; 
    } else if (str.length() == 11) {
      if (isNANP(str))
        return 3; 
    } else {
      String str1;
      if (str.startsWith("+")) {
        str1 = str.substring(1);
        if (str1.length() == 11) {
          if (isNANP(str1))
            return 4; 
        } else if (str1.startsWith("011") && str1.length() == 14) {
          str1 = str1.substring(3);
          if (isNANP(str1))
            return 6; 
        } 
      } else {
        for (String str2 : paramArrayList) {
          if (str.startsWith(str2)) {
            String str3 = str.substring(str2.length());
            if (str3 != null && str3.startsWith(String.valueOf(1)) && 
              isNANP(str3)) {
              ((NumberEntry)str1).IDD = str2;
              return 5;
            } 
          } 
        } 
      } 
    } 
    return 0;
  }
  
  private static boolean isNANP(String paramString) {
    // Byte code:
    //   0: iconst_0
    //   1: istore_1
    //   2: aload_0
    //   3: invokevirtual length : ()I
    //   6: bipush #10
    //   8: if_icmpeq -> 33
    //   11: iload_1
    //   12: istore_2
    //   13: aload_0
    //   14: invokevirtual length : ()I
    //   17: bipush #11
    //   19: if_icmpne -> 117
    //   22: iload_1
    //   23: istore_2
    //   24: aload_0
    //   25: ldc '1'
    //   27: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   30: ifeq -> 117
    //   33: aload_0
    //   34: astore_3
    //   35: aload_0
    //   36: invokevirtual length : ()I
    //   39: bipush #11
    //   41: if_icmpne -> 50
    //   44: aload_0
    //   45: iconst_1
    //   46: invokevirtual substring : (I)Ljava/lang/String;
    //   49: astore_3
    //   50: iload_1
    //   51: istore_2
    //   52: aload_3
    //   53: iconst_0
    //   54: invokevirtual charAt : (I)C
    //   57: invokestatic isTwoToNine : (C)Z
    //   60: ifeq -> 117
    //   63: iload_1
    //   64: istore_2
    //   65: aload_3
    //   66: iconst_3
    //   67: invokevirtual charAt : (I)C
    //   70: invokestatic isTwoToNine : (C)Z
    //   73: ifeq -> 117
    //   76: iconst_1
    //   77: istore_1
    //   78: iconst_1
    //   79: istore #4
    //   81: iload_1
    //   82: istore_2
    //   83: iload #4
    //   85: bipush #10
    //   87: if_icmpge -> 117
    //   90: aload_3
    //   91: iload #4
    //   93: invokevirtual charAt : (I)C
    //   96: istore #5
    //   98: iload #5
    //   100: invokestatic isISODigit : (C)Z
    //   103: ifne -> 111
    //   106: iconst_0
    //   107: istore_2
    //   108: goto -> 117
    //   111: iinc #4, 1
    //   114: goto -> 81
    //   117: iload_2
    //   118: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #371	-> 0
    //   #373	-> 2
    //   #374	-> 11
    //   #376	-> 33
    //   #377	-> 44
    //   #380	-> 50
    //   #381	-> 63
    //   #382	-> 76
    //   #383	-> 78
    //   #384	-> 90
    //   #385	-> 98
    //   #386	-> 106
    //   #387	-> 108
    //   #383	-> 111
    //   #392	-> 117
  }
  
  private static boolean isTwoToNine(char paramChar) {
    if (paramChar >= '2' && paramChar <= '9')
      return true; 
    return false;
  }
  
  private static int checkInternationalNumberPlan(Context paramContext, NumberEntry paramNumberEntry, ArrayList<String> paramArrayList, String paramString) {
    String str = paramNumberEntry.number;
    if (str.startsWith("+")) {
      String str1 = str.substring(1);
      if (str1.startsWith(paramString)) {
        str1 = str1.substring(paramString.length());
        int i = getCountryCode(paramContext, str1);
        if (i > 0) {
          paramNumberEntry.countryCode = i;
          return 100;
        } 
      } else {
        int i = getCountryCode(paramContext, str1);
        if (i > 0) {
          paramNumberEntry.countryCode = i;
          return 102;
        } 
      } 
    } else {
      String str1;
      if (str.startsWith(paramString)) {
        str1 = str.substring(paramString.length());
        int i = getCountryCode(paramContext, str1);
        if (i > 0) {
          paramNumberEntry.countryCode = i;
          return 101;
        } 
      } else {
        for (String paramString : str1) {
          if (str.startsWith(paramString)) {
            String str2 = str.substring(paramString.length());
            int i = getCountryCode(paramContext, str2);
            if (i > 0) {
              paramNumberEntry.countryCode = i;
              paramNumberEntry.IDD = paramString;
              return 103;
            } 
          } 
        } 
        if (!str.startsWith("0")) {
          int i = getCountryCode(paramContext, str);
          if (i > 0) {
            paramNumberEntry.countryCode = i;
            return 104;
          } 
        } 
      } 
    } 
    return 0;
  }
  
  private static int getCountryCode(Context paramContext, String paramString) {
    if (paramString.length() >= 10) {
      int[] arrayOfInt1 = getAllCountryCodes(paramContext);
      if (arrayOfInt1 == null)
        return -1; 
      int[] arrayOfInt2 = new int[MAX_COUNTRY_CODES_LENGTH];
      byte b;
      for (b = 0; b < MAX_COUNTRY_CODES_LENGTH; b++)
        arrayOfInt2[b] = Integer.parseInt(paramString.substring(0, b + 1)); 
      for (b = 0; b < arrayOfInt1.length; b++) {
        int i = arrayOfInt1[b];
        for (byte b1 = 0; b1 < MAX_COUNTRY_CODES_LENGTH; b1++) {
          if (i == arrayOfInt2[b1]) {
            if (DBG) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Country code = ");
              stringBuilder.append(i);
              Log.d("SmsNumberUtils", stringBuilder.toString());
            } 
            return i;
          } 
        } 
      } 
    } 
    return -1;
  }
  
  private static int[] getAllCountryCodes(Context paramContext) {
    Cursor cursor1;
    int[] arrayOfInt = ALL_COUNTRY_CODES;
    if (arrayOfInt != null)
      return arrayOfInt; 
    arrayOfInt = null;
    Cursor cursor2 = null;
    try {
      Cursor cursor = paramContext.getContentResolver().query(HbpcdLookup.MccLookup.CONTENT_URI, new String[] { "Country_Code" }, null, null, null);
      cursor2 = cursor;
      cursor1 = cursor;
      if (cursor.getCount() > 0) {
        cursor2 = cursor;
        cursor1 = cursor;
        ALL_COUNTRY_CODES = new int[cursor.getCount()];
        byte b = 0;
        while (true) {
          cursor2 = cursor;
          cursor1 = cursor;
          if (cursor.moveToNext()) {
            cursor2 = cursor;
            cursor1 = cursor;
            int i = cursor.getInt(0);
            cursor2 = cursor;
            cursor1 = cursor;
            ALL_COUNTRY_CODES[b] = i;
            cursor2 = cursor;
            cursor1 = cursor;
            i = String.valueOf(i).trim().length();
            cursor2 = cursor;
            cursor1 = cursor;
            if (i > MAX_COUNTRY_CODES_LENGTH) {
              cursor2 = cursor;
              cursor1 = cursor;
              MAX_COUNTRY_CODES_LENGTH = i;
            } 
            b++;
            continue;
          } 
          break;
        } 
      } 
      if (cursor != null)
        cursor.close(); 
    } catch (SQLException sQLException) {
      Cursor cursor;
      cursor2 = cursor1;
      Log.e("SmsNumberUtils", "Can't access HbpcdLookup database", (Throwable)sQLException);
      if (cursor1 != null) {
        cursor = cursor1;
      } else {
        return ALL_COUNTRY_CODES;
      } 
      cursor.close();
    } finally {}
    return ALL_COUNTRY_CODES;
  }
  
  private static boolean inExceptionListForNpCcAreaLocal(NumberEntry paramNumberEntry) {
    boolean bool;
    int i = paramNumberEntry.countryCode;
    if (paramNumberEntry.number.length() == 12 && (i == 7 || i == 20 || i == 65 || i == 90)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static String getNumberPlanType(int paramInt) {
    String str;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Number Plan type (");
    stringBuilder.append(paramInt);
    stringBuilder.append("): ");
    stringBuilder.toString();
    if (paramInt == 1) {
      str = "NP_NANP_LOCAL";
    } else if (paramInt == 2) {
      str = "NP_NANP_AREA_LOCAL";
    } else if (paramInt == 3) {
      str = "NP_NANP_NDD_AREA_LOCAL";
    } else if (paramInt == 4) {
      str = "NP_NANP_NBPCD_CC_AREA_LOCAL";
    } else if (paramInt == 5) {
      str = "NP_NANP_LOCALIDD_CC_AREA_LOCAL";
    } else if (paramInt == 6) {
      str = "NP_NANP_NBPCD_HOMEIDD_CC_AREA_LOCAL";
    } else if (paramInt == 100) {
      str = "NP_NBPCD_HOMEIDD_CC_AREA_LOCAL";
    } else if (paramInt == 101) {
      str = "NP_HOMEIDD_CC_AREA_LOCAL";
    } else if (paramInt == 102) {
      str = "NP_NBPCD_CC_AREA_LOCAL";
    } else if (paramInt == 103) {
      str = "NP_LOCALIDD_CC_AREA_LOCAL";
    } else if (paramInt == 104) {
      str = "NP_CC_AREA_LOCAL";
    } else {
      str = "Unknown type";
    } 
    return str;
  }
  
  public static String filterDestAddr(Context paramContext, int paramInt, String paramString) {
    StringBuilder stringBuilder;
    if (DBG) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("enter filterDestAddr. destAddr=\"");
      stringBuilder1.append(pii("SmsNumberUtils", paramString));
      stringBuilder1.append("\"");
      Log.d("SmsNumberUtils", stringBuilder1.toString());
    } 
    if (paramString == null || !PhoneNumberUtils.isGlobalPhoneNumber(paramString)) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("destAddr");
      stringBuilder.append(pii("SmsNumberUtils", paramString));
      stringBuilder.append(" is not a global phone number! Nothing changed.");
      Log.w("SmsNumberUtils", stringBuilder.toString());
      return paramString;
    } 
    TelephonyManager telephonyManager = ((TelephonyManager)stringBuilder.getSystemService("phone")).createForSubscriptionId(paramInt);
    String str2 = telephonyManager.getNetworkOperator();
    String str3 = null;
    String str1 = str3;
    if (needToConvert((Context)stringBuilder, paramInt)) {
      paramInt = getNetworkType(telephonyManager);
      str1 = str3;
      if (paramInt != -1) {
        str1 = str3;
        if (!TextUtils.isEmpty(str2)) {
          String str = str2.substring(0, 3);
          str1 = str3;
          if (str != null) {
            str1 = str3;
            if (str.trim().length() > 0)
              str1 = formatNumber((Context)stringBuilder, paramString, str, paramInt); 
          } 
        } 
      } 
    } 
    if (DBG) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("destAddr is ");
      if (str1 != null) {
        str = "formatted.";
      } else {
        str = "not formatted.";
      } 
      stringBuilder1.append(str);
      Log.d("SmsNumberUtils", stringBuilder1.toString());
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("leave filterDestAddr, new destAddr=\"");
      if (str1 != null) {
        str = pii("SmsNumberUtils", str1);
      } else {
        str = pii("SmsNumberUtils", paramString);
      } 
      stringBuilder1.append(str);
      stringBuilder1.append("\"");
      String str = stringBuilder1.toString();
      Log.d("SmsNumberUtils", str);
    } 
    if (str1 != null)
      paramString = str1; 
    return paramString;
  }
  
  private static int getNetworkType(TelephonyManager paramTelephonyManager) {
    byte b2, b1 = -1;
    int i = paramTelephonyManager.getPhoneType();
    if (i == 1) {
      b2 = 0;
    } else if (i == 2) {
      if (isInternationalRoaming(paramTelephonyManager)) {
        b2 = 2;
      } else {
        b2 = 1;
      } 
    } else {
      b2 = b1;
      if (DBG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("warning! unknown mPhoneType value=");
        stringBuilder.append(i);
        Log.w("SmsNumberUtils", stringBuilder.toString());
        b2 = b1;
      } 
    } 
    return b2;
  }
  
  private static boolean isInternationalRoaming(TelephonyManager paramTelephonyManager) {
    byte b;
    String str2 = paramTelephonyManager.getNetworkCountryIso();
    String str1 = paramTelephonyManager.getSimCountryIso();
    if (!TextUtils.isEmpty(str2) && 
      !TextUtils.isEmpty(str1) && 
      !str1.equals(str2)) {
      b = 1;
    } else {
      b = 0;
    } 
    int i = b;
    if (b)
      if ("us".equals(str1)) {
        i = true ^ "vi".equals(str2);
      } else {
        i = b;
        if ("vi".equals(str1))
          i = true ^ "us".equals(str2); 
      }  
    return i;
  }
  
  private static boolean needToConvert(Context paramContext, int paramInt) {
    long l = Binder.clearCallingIdentity();
    try {
      CarrierConfigManager carrierConfigManager = (CarrierConfigManager)paramContext.getSystemService("carrier_config");
      if (carrierConfigManager != null) {
        PersistableBundle persistableBundle = carrierConfigManager.getConfigForSubId(paramInt);
        if (persistableBundle != null)
          return persistableBundle.getBoolean("sms_requires_destination_number_conversion_bool"); 
      } 
      return false;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  private static String pii(String paramString, Object paramObject) {
    String str = String.valueOf(paramObject);
    if (paramObject == null || TextUtils.isEmpty(str) || Log.isLoggable(paramString, 2))
      return str; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[");
    stringBuilder.append(secureHash(str.getBytes()));
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  private static String secureHash(byte[] paramArrayOfbyte) {
    if (TelephonyUtils.IS_USER)
      return "****"; 
    try {
      MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
      paramArrayOfbyte = messageDigest.digest(paramArrayOfbyte);
      return Base64.encodeToString(paramArrayOfbyte, 11);
    } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
      return "####";
    } 
  }
}
