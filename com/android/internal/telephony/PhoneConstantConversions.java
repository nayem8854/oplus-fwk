package com.android.internal.telephony;

public class PhoneConstantConversions {
  public static int convertCallState(PhoneConstants.State paramState) {
    int i = null.$SwitchMap$com$android$internal$telephony$PhoneConstants$State[paramState.ordinal()];
    if (i != 1) {
      if (i != 2)
        return 0; 
      return 2;
    } 
    return 1;
  }
  
  public static PhoneConstants.State convertCallState(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2)
        return PhoneConstants.State.IDLE; 
      return PhoneConstants.State.OFFHOOK;
    } 
    return PhoneConstants.State.RINGING;
  }
  
  public static int convertDataState(PhoneConstants.DataState paramDataState) {
    int i = null.$SwitchMap$com$android$internal$telephony$PhoneConstants$DataState[paramDataState.ordinal()];
    if (i != 1) {
      if (i != 2) {
        if (i != 3) {
          if (i != 4)
            return 0; 
          return 4;
        } 
        return 3;
      } 
      return 2;
    } 
    return 1;
  }
  
  public static PhoneConstants.DataState convertDataState(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 3) {
          if (paramInt != 4)
            return PhoneConstants.DataState.DISCONNECTED; 
          return PhoneConstants.DataState.DISCONNECTING;
        } 
        return PhoneConstants.DataState.SUSPENDED;
      } 
      return PhoneConstants.DataState.CONNECTED;
    } 
    return PhoneConstants.DataState.CONNECTING;
  }
}
