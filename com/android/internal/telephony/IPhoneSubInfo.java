package com.android.internal.telephony;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.ImsiEncryptionInfo;

public interface IPhoneSubInfo extends IInterface {
  ImsiEncryptionInfo getCarrierInfoForImsiEncryption(int paramInt1, int paramInt2, String paramString) throws RemoteException;
  
  String getDeviceId(String paramString) throws RemoteException;
  
  String getDeviceIdForPhone(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  String getDeviceIdWithFeature(String paramString1, String paramString2) throws RemoteException;
  
  String getDeviceSvn(String paramString1, String paramString2) throws RemoteException;
  
  String getDeviceSvnUsingSubId(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  String getGroupIdLevel1ForSubscriber(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  String getIccSerialNumber(String paramString) throws RemoteException;
  
  String getIccSerialNumberForSubscriber(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  String getIccSerialNumberWithFeature(String paramString1, String paramString2) throws RemoteException;
  
  String getIccSimChallengeResponse(int paramInt1, int paramInt2, int paramInt3, String paramString) throws RemoteException;
  
  String getImeiForSubscriber(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  String getIsimDomain(int paramInt) throws RemoteException;
  
  String getIsimImpi(int paramInt) throws RemoteException;
  
  String[] getIsimImpu(int paramInt) throws RemoteException;
  
  String getIsimIst(int paramInt) throws RemoteException;
  
  String[] getIsimPcscf(int paramInt) throws RemoteException;
  
  String getLine1AlphaTag(String paramString1, String paramString2) throws RemoteException;
  
  String getLine1AlphaTagForSubscriber(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  String getLine1Number(String paramString1, String paramString2) throws RemoteException;
  
  String getLine1NumberForSubscriber(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  String getMsisdn(String paramString1, String paramString2) throws RemoteException;
  
  String getMsisdnForSubscriber(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  String getNaiForSubscriber(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  String getSubscriberId(String paramString) throws RemoteException;
  
  String getSubscriberIdForSubscriber(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  String getSubscriberIdWithFeature(String paramString1, String paramString2) throws RemoteException;
  
  String getVoiceMailAlphaTag(String paramString1, String paramString2) throws RemoteException;
  
  String getVoiceMailAlphaTagForSubscriber(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  String getVoiceMailNumber(String paramString1, String paramString2) throws RemoteException;
  
  String getVoiceMailNumberForSubscriber(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  void resetCarrierKeysForImsiEncryption(int paramInt, String paramString) throws RemoteException;
  
  void setCarrierInfoForImsiEncryption(int paramInt, String paramString, ImsiEncryptionInfo paramImsiEncryptionInfo) throws RemoteException;
  
  class Default implements IPhoneSubInfo {
    public String getDeviceId(String param1String) throws RemoteException {
      return null;
    }
    
    public String getDeviceIdWithFeature(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getNaiForSubscriber(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getDeviceIdForPhone(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getImeiForSubscriber(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getDeviceSvn(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getDeviceSvnUsingSubId(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getSubscriberId(String param1String) throws RemoteException {
      return null;
    }
    
    public String getSubscriberIdWithFeature(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getSubscriberIdForSubscriber(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getGroupIdLevel1ForSubscriber(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getIccSerialNumber(String param1String) throws RemoteException {
      return null;
    }
    
    public String getIccSerialNumberWithFeature(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getIccSerialNumberForSubscriber(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getLine1Number(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getLine1NumberForSubscriber(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getLine1AlphaTag(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getLine1AlphaTagForSubscriber(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getMsisdn(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getMsisdnForSubscriber(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getVoiceMailNumber(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getVoiceMailNumberForSubscriber(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public ImsiEncryptionInfo getCarrierInfoForImsiEncryption(int param1Int1, int param1Int2, String param1String) throws RemoteException {
      return null;
    }
    
    public void setCarrierInfoForImsiEncryption(int param1Int, String param1String, ImsiEncryptionInfo param1ImsiEncryptionInfo) throws RemoteException {}
    
    public void resetCarrierKeysForImsiEncryption(int param1Int, String param1String) throws RemoteException {}
    
    public String getVoiceMailAlphaTag(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getVoiceMailAlphaTagForSubscriber(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getIsimImpi(int param1Int) throws RemoteException {
      return null;
    }
    
    public String getIsimDomain(int param1Int) throws RemoteException {
      return null;
    }
    
    public String[] getIsimImpu(int param1Int) throws RemoteException {
      return null;
    }
    
    public String getIsimIst(int param1Int) throws RemoteException {
      return null;
    }
    
    public String[] getIsimPcscf(int param1Int) throws RemoteException {
      return null;
    }
    
    public String getIccSimChallengeResponse(int param1Int1, int param1Int2, int param1Int3, String param1String) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPhoneSubInfo {
    private static final String DESCRIPTOR = "com.android.internal.telephony.IPhoneSubInfo";
    
    static final int TRANSACTION_getCarrierInfoForImsiEncryption = 23;
    
    static final int TRANSACTION_getDeviceId = 1;
    
    static final int TRANSACTION_getDeviceIdForPhone = 4;
    
    static final int TRANSACTION_getDeviceIdWithFeature = 2;
    
    static final int TRANSACTION_getDeviceSvn = 6;
    
    static final int TRANSACTION_getDeviceSvnUsingSubId = 7;
    
    static final int TRANSACTION_getGroupIdLevel1ForSubscriber = 11;
    
    static final int TRANSACTION_getIccSerialNumber = 12;
    
    static final int TRANSACTION_getIccSerialNumberForSubscriber = 14;
    
    static final int TRANSACTION_getIccSerialNumberWithFeature = 13;
    
    static final int TRANSACTION_getIccSimChallengeResponse = 33;
    
    static final int TRANSACTION_getImeiForSubscriber = 5;
    
    static final int TRANSACTION_getIsimDomain = 29;
    
    static final int TRANSACTION_getIsimImpi = 28;
    
    static final int TRANSACTION_getIsimImpu = 30;
    
    static final int TRANSACTION_getIsimIst = 31;
    
    static final int TRANSACTION_getIsimPcscf = 32;
    
    static final int TRANSACTION_getLine1AlphaTag = 17;
    
    static final int TRANSACTION_getLine1AlphaTagForSubscriber = 18;
    
    static final int TRANSACTION_getLine1Number = 15;
    
    static final int TRANSACTION_getLine1NumberForSubscriber = 16;
    
    static final int TRANSACTION_getMsisdn = 19;
    
    static final int TRANSACTION_getMsisdnForSubscriber = 20;
    
    static final int TRANSACTION_getNaiForSubscriber = 3;
    
    static final int TRANSACTION_getSubscriberId = 8;
    
    static final int TRANSACTION_getSubscriberIdForSubscriber = 10;
    
    static final int TRANSACTION_getSubscriberIdWithFeature = 9;
    
    static final int TRANSACTION_getVoiceMailAlphaTag = 26;
    
    static final int TRANSACTION_getVoiceMailAlphaTagForSubscriber = 27;
    
    static final int TRANSACTION_getVoiceMailNumber = 21;
    
    static final int TRANSACTION_getVoiceMailNumberForSubscriber = 22;
    
    static final int TRANSACTION_resetCarrierKeysForImsiEncryption = 25;
    
    static final int TRANSACTION_setCarrierInfoForImsiEncryption = 24;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.IPhoneSubInfo");
    }
    
    public static IPhoneSubInfo asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.IPhoneSubInfo");
      if (iInterface != null && iInterface instanceof IPhoneSubInfo)
        return (IPhoneSubInfo)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 33:
          return "getIccSimChallengeResponse";
        case 32:
          return "getIsimPcscf";
        case 31:
          return "getIsimIst";
        case 30:
          return "getIsimImpu";
        case 29:
          return "getIsimDomain";
        case 28:
          return "getIsimImpi";
        case 27:
          return "getVoiceMailAlphaTagForSubscriber";
        case 26:
          return "getVoiceMailAlphaTag";
        case 25:
          return "resetCarrierKeysForImsiEncryption";
        case 24:
          return "setCarrierInfoForImsiEncryption";
        case 23:
          return "getCarrierInfoForImsiEncryption";
        case 22:
          return "getVoiceMailNumberForSubscriber";
        case 21:
          return "getVoiceMailNumber";
        case 20:
          return "getMsisdnForSubscriber";
        case 19:
          return "getMsisdn";
        case 18:
          return "getLine1AlphaTagForSubscriber";
        case 17:
          return "getLine1AlphaTag";
        case 16:
          return "getLine1NumberForSubscriber";
        case 15:
          return "getLine1Number";
        case 14:
          return "getIccSerialNumberForSubscriber";
        case 13:
          return "getIccSerialNumberWithFeature";
        case 12:
          return "getIccSerialNumber";
        case 11:
          return "getGroupIdLevel1ForSubscriber";
        case 10:
          return "getSubscriberIdForSubscriber";
        case 9:
          return "getSubscriberIdWithFeature";
        case 8:
          return "getSubscriberId";
        case 7:
          return "getDeviceSvnUsingSubId";
        case 6:
          return "getDeviceSvn";
        case 5:
          return "getImeiForSubscriber";
        case 4:
          return "getDeviceIdForPhone";
        case 3:
          return "getNaiForSubscriber";
        case 2:
          return "getDeviceIdWithFeature";
        case 1:
          break;
      } 
      return "getDeviceId";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        String str4, arrayOfString2[], str3, arrayOfString1[], str2;
        ImsiEncryptionInfo imsiEncryptionInfo;
        int i;
        String str5;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 33:
            param1Parcel1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            i = param1Parcel1.readInt();
            str4 = param1Parcel1.readString();
            str4 = getIccSimChallengeResponse(param1Int2, param1Int1, i, str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str4);
            return true;
          case 32:
            str4.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            param1Int1 = str4.readInt();
            arrayOfString2 = getIsimPcscf(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString2);
            return true;
          case 31:
            arrayOfString2.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            param1Int1 = arrayOfString2.readInt();
            str3 = getIsimIst(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str3);
            return true;
          case 30:
            str3.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            param1Int1 = str3.readInt();
            arrayOfString1 = getIsimImpu(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString1);
            return true;
          case 29:
            arrayOfString1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            param1Int1 = arrayOfString1.readInt();
            str2 = getIsimDomain(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str2);
            return true;
          case 28:
            str2.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            param1Int1 = str2.readInt();
            str2 = getIsimImpi(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str2);
            return true;
          case 27:
            str2.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            param1Int1 = str2.readInt();
            str5 = str2.readString();
            str2 = str2.readString();
            str2 = getVoiceMailAlphaTagForSubscriber(param1Int1, str5, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str2);
            return true;
          case 26:
            str2.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            str5 = str2.readString();
            str2 = str2.readString();
            str2 = getVoiceMailAlphaTag(str5, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str2);
            return true;
          case 25:
            str2.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            param1Int1 = str2.readInt();
            str2 = str2.readString();
            resetCarrierKeysForImsiEncryption(param1Int1, str2);
            param1Parcel2.writeNoException();
            return true;
          case 24:
            str2.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            param1Int1 = str2.readInt();
            str5 = str2.readString();
            if (str2.readInt() != 0) {
              ImsiEncryptionInfo imsiEncryptionInfo1 = (ImsiEncryptionInfo)ImsiEncryptionInfo.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            setCarrierInfoForImsiEncryption(param1Int1, str5, (ImsiEncryptionInfo)str2);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            str2.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            param1Int1 = str2.readInt();
            param1Int2 = str2.readInt();
            str2 = str2.readString();
            imsiEncryptionInfo = getCarrierInfoForImsiEncryption(param1Int1, param1Int2, str2);
            param1Parcel2.writeNoException();
            if (imsiEncryptionInfo != null) {
              param1Parcel2.writeInt(1);
              imsiEncryptionInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 22:
            imsiEncryptionInfo.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            param1Int1 = imsiEncryptionInfo.readInt();
            str5 = imsiEncryptionInfo.readString();
            str1 = imsiEncryptionInfo.readString();
            str1 = getVoiceMailNumberForSubscriber(param1Int1, str5, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 21:
            str1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            str5 = str1.readString();
            str1 = str1.readString();
            str1 = getVoiceMailNumber(str5, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 20:
            str1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            param1Int1 = str1.readInt();
            str5 = str1.readString();
            str1 = str1.readString();
            str1 = getMsisdnForSubscriber(param1Int1, str5, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 19:
            str1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            str5 = str1.readString();
            str1 = str1.readString();
            str1 = getMsisdn(str5, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 18:
            str1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            param1Int1 = str1.readInt();
            str5 = str1.readString();
            str1 = str1.readString();
            str1 = getLine1AlphaTagForSubscriber(param1Int1, str5, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 17:
            str1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            str5 = str1.readString();
            str1 = str1.readString();
            str1 = getLine1AlphaTag(str5, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 16:
            str1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            param1Int1 = str1.readInt();
            str5 = str1.readString();
            str1 = str1.readString();
            str1 = getLine1NumberForSubscriber(param1Int1, str5, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 15:
            str1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            str5 = str1.readString();
            str1 = str1.readString();
            str1 = getLine1Number(str5, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 14:
            str1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            param1Int1 = str1.readInt();
            str5 = str1.readString();
            str1 = str1.readString();
            str1 = getIccSerialNumberForSubscriber(param1Int1, str5, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 13:
            str1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            str5 = str1.readString();
            str1 = str1.readString();
            str1 = getIccSerialNumberWithFeature(str5, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 12:
            str1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            str1 = str1.readString();
            str1 = getIccSerialNumber(str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 11:
            str1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            param1Int1 = str1.readInt();
            str5 = str1.readString();
            str1 = str1.readString();
            str1 = getGroupIdLevel1ForSubscriber(param1Int1, str5, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 10:
            str1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            param1Int1 = str1.readInt();
            str5 = str1.readString();
            str1 = str1.readString();
            str1 = getSubscriberIdForSubscriber(param1Int1, str5, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 9:
            str1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            str5 = str1.readString();
            str1 = str1.readString();
            str1 = getSubscriberIdWithFeature(str5, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 8:
            str1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            str1 = str1.readString();
            str1 = getSubscriberId(str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 7:
            str1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            param1Int1 = str1.readInt();
            str5 = str1.readString();
            str1 = str1.readString();
            str1 = getDeviceSvnUsingSubId(param1Int1, str5, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 6:
            str1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            str5 = str1.readString();
            str1 = str1.readString();
            str1 = getDeviceSvn(str5, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 5:
            str1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            param1Int1 = str1.readInt();
            str5 = str1.readString();
            str1 = str1.readString();
            str1 = getImeiForSubscriber(param1Int1, str5, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 4:
            str1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            param1Int1 = str1.readInt();
            str5 = str1.readString();
            str1 = str1.readString();
            str1 = getDeviceIdForPhone(param1Int1, str5, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 3:
            str1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            param1Int1 = str1.readInt();
            str5 = str1.readString();
            str1 = str1.readString();
            str1 = getNaiForSubscriber(param1Int1, str5, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 2:
            str1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
            str5 = str1.readString();
            str1 = str1.readString();
            str1 = getDeviceIdWithFeature(str5, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("com.android.internal.telephony.IPhoneSubInfo");
        String str1 = str1.readString();
        str1 = getDeviceId(str1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeString(str1);
        return true;
      } 
      param1Parcel2.writeString("com.android.internal.telephony.IPhoneSubInfo");
      return true;
    }
    
    private static class Proxy implements IPhoneSubInfo {
      public static IPhoneSubInfo sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.IPhoneSubInfo";
      }
      
      public String getDeviceId(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null) {
            param2String = IPhoneSubInfo.Stub.getDefaultImpl().getDeviceId(param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getDeviceIdWithFeature(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null) {
            param2String1 = IPhoneSubInfo.Stub.getDefaultImpl().getDeviceIdWithFeature(param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getNaiForSubscriber(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null) {
            param2String1 = IPhoneSubInfo.Stub.getDefaultImpl().getNaiForSubscriber(param2Int, param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getDeviceIdForPhone(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null) {
            param2String1 = IPhoneSubInfo.Stub.getDefaultImpl().getDeviceIdForPhone(param2Int, param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getImeiForSubscriber(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null) {
            param2String1 = IPhoneSubInfo.Stub.getDefaultImpl().getImeiForSubscriber(param2Int, param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getDeviceSvn(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null) {
            param2String1 = IPhoneSubInfo.Stub.getDefaultImpl().getDeviceSvn(param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getDeviceSvnUsingSubId(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null) {
            param2String1 = IPhoneSubInfo.Stub.getDefaultImpl().getDeviceSvnUsingSubId(param2Int, param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getSubscriberId(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null) {
            param2String = IPhoneSubInfo.Stub.getDefaultImpl().getSubscriberId(param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getSubscriberIdWithFeature(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null) {
            param2String1 = IPhoneSubInfo.Stub.getDefaultImpl().getSubscriberIdWithFeature(param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getSubscriberIdForSubscriber(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null) {
            param2String1 = IPhoneSubInfo.Stub.getDefaultImpl().getSubscriberIdForSubscriber(param2Int, param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getGroupIdLevel1ForSubscriber(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null) {
            param2String1 = IPhoneSubInfo.Stub.getDefaultImpl().getGroupIdLevel1ForSubscriber(param2Int, param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getIccSerialNumber(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null) {
            param2String = IPhoneSubInfo.Stub.getDefaultImpl().getIccSerialNumber(param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getIccSerialNumberWithFeature(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null) {
            param2String1 = IPhoneSubInfo.Stub.getDefaultImpl().getIccSerialNumberWithFeature(param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getIccSerialNumberForSubscriber(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null) {
            param2String1 = IPhoneSubInfo.Stub.getDefaultImpl().getIccSerialNumberForSubscriber(param2Int, param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getLine1Number(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null) {
            param2String1 = IPhoneSubInfo.Stub.getDefaultImpl().getLine1Number(param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getLine1NumberForSubscriber(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null) {
            param2String1 = IPhoneSubInfo.Stub.getDefaultImpl().getLine1NumberForSubscriber(param2Int, param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getLine1AlphaTag(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null) {
            param2String1 = IPhoneSubInfo.Stub.getDefaultImpl().getLine1AlphaTag(param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getLine1AlphaTagForSubscriber(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null) {
            param2String1 = IPhoneSubInfo.Stub.getDefaultImpl().getLine1AlphaTagForSubscriber(param2Int, param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getMsisdn(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null) {
            param2String1 = IPhoneSubInfo.Stub.getDefaultImpl().getMsisdn(param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getMsisdnForSubscriber(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null) {
            param2String1 = IPhoneSubInfo.Stub.getDefaultImpl().getMsisdnForSubscriber(param2Int, param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getVoiceMailNumber(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null) {
            param2String1 = IPhoneSubInfo.Stub.getDefaultImpl().getVoiceMailNumber(param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getVoiceMailNumberForSubscriber(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null) {
            param2String1 = IPhoneSubInfo.Stub.getDefaultImpl().getVoiceMailNumberForSubscriber(param2Int, param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ImsiEncryptionInfo getCarrierInfoForImsiEncryption(int param2Int1, int param2Int2, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null)
            return IPhoneSubInfo.Stub.getDefaultImpl().getCarrierInfoForImsiEncryption(param2Int1, param2Int2, param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ImsiEncryptionInfo imsiEncryptionInfo = (ImsiEncryptionInfo)ImsiEncryptionInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (ImsiEncryptionInfo)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setCarrierInfoForImsiEncryption(int param2Int, String param2String, ImsiEncryptionInfo param2ImsiEncryptionInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          if (param2ImsiEncryptionInfo != null) {
            parcel1.writeInt(1);
            param2ImsiEncryptionInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null) {
            IPhoneSubInfo.Stub.getDefaultImpl().setCarrierInfoForImsiEncryption(param2Int, param2String, param2ImsiEncryptionInfo);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resetCarrierKeysForImsiEncryption(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null) {
            IPhoneSubInfo.Stub.getDefaultImpl().resetCarrierKeysForImsiEncryption(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getVoiceMailAlphaTag(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null) {
            param2String1 = IPhoneSubInfo.Stub.getDefaultImpl().getVoiceMailAlphaTag(param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getVoiceMailAlphaTagForSubscriber(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null) {
            param2String1 = IPhoneSubInfo.Stub.getDefaultImpl().getVoiceMailAlphaTagForSubscriber(param2Int, param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getIsimImpi(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null)
            return IPhoneSubInfo.Stub.getDefaultImpl().getIsimImpi(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getIsimDomain(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null)
            return IPhoneSubInfo.Stub.getDefaultImpl().getIsimDomain(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getIsimImpu(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null)
            return IPhoneSubInfo.Stub.getDefaultImpl().getIsimImpu(param2Int); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getIsimIst(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null)
            return IPhoneSubInfo.Stub.getDefaultImpl().getIsimIst(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getIsimPcscf(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null)
            return IPhoneSubInfo.Stub.getDefaultImpl().getIsimPcscf(param2Int); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getIccSimChallengeResponse(int param2Int1, int param2Int2, int param2Int3, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IPhoneSubInfo");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && IPhoneSubInfo.Stub.getDefaultImpl() != null) {
            param2String = IPhoneSubInfo.Stub.getDefaultImpl().getIccSimChallengeResponse(param2Int1, param2Int2, param2Int3, param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPhoneSubInfo param1IPhoneSubInfo) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPhoneSubInfo != null) {
          Proxy.sDefaultImpl = param1IPhoneSubInfo;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPhoneSubInfo getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
