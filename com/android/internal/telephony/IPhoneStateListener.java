package com.android.internal.telephony;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.BarringInfo;
import android.telephony.CallAttributes;
import android.telephony.CellIdentity;
import android.telephony.CellInfo;
import android.telephony.DataConnectionRealTimeInfo;
import android.telephony.PhoneCapability;
import android.telephony.PreciseCallState;
import android.telephony.PreciseDataConnectionState;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.TelephonyDisplayInfo;
import android.telephony.emergency.EmergencyNumber;
import android.telephony.ims.ImsReasonInfo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface IPhoneStateListener extends IInterface {
  void onActiveDataSubIdChanged(int paramInt) throws RemoteException;
  
  void onBarringInfoChanged(BarringInfo paramBarringInfo) throws RemoteException;
  
  void onCallAttributesChanged(CallAttributes paramCallAttributes) throws RemoteException;
  
  void onCallDisconnectCauseChanged(int paramInt1, int paramInt2) throws RemoteException;
  
  void onCallForwardingIndicatorChanged(boolean paramBoolean) throws RemoteException;
  
  void onCallStateChanged(int paramInt, String paramString) throws RemoteException;
  
  void onCarrierNetworkChange(boolean paramBoolean) throws RemoteException;
  
  void onCellInfoChanged(List<CellInfo> paramList) throws RemoteException;
  
  void onCellLocationChanged(CellIdentity paramCellIdentity) throws RemoteException;
  
  void onDataActivationStateChanged(int paramInt) throws RemoteException;
  
  void onDataActivity(int paramInt) throws RemoteException;
  
  void onDataConnectionRealTimeInfoChanged(DataConnectionRealTimeInfo paramDataConnectionRealTimeInfo) throws RemoteException;
  
  void onDataConnectionStateChanged(int paramInt1, int paramInt2) throws RemoteException;
  
  void onDisplayInfoChanged(TelephonyDisplayInfo paramTelephonyDisplayInfo) throws RemoteException;
  
  void onEmergencyNumberListChanged(Map paramMap) throws RemoteException;
  
  void onImsCallDisconnectCauseChanged(ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void onMessageWaitingIndicatorChanged(boolean paramBoolean) throws RemoteException;
  
  void onOemHookRawEvent(byte[] paramArrayOfbyte) throws RemoteException;
  
  void onOutgoingEmergencyCall(EmergencyNumber paramEmergencyNumber) throws RemoteException;
  
  void onOutgoingEmergencySms(EmergencyNumber paramEmergencyNumber) throws RemoteException;
  
  void onPhoneCapabilityChanged(PhoneCapability paramPhoneCapability) throws RemoteException;
  
  void onPreciseCallStateChanged(PreciseCallState paramPreciseCallState) throws RemoteException;
  
  void onPreciseDataConnectionStateChanged(PreciseDataConnectionState paramPreciseDataConnectionState) throws RemoteException;
  
  void onRadioPowerStateChanged(int paramInt) throws RemoteException;
  
  void onRegistrationFailed(CellIdentity paramCellIdentity, String paramString, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void onServiceStateChanged(ServiceState paramServiceState) throws RemoteException;
  
  void onSignalStrengthChanged(int paramInt) throws RemoteException;
  
  void onSignalStrengthsChanged(SignalStrength paramSignalStrength) throws RemoteException;
  
  void onSrvccStateChanged(int paramInt) throws RemoteException;
  
  void onUserMobileDataStateChanged(boolean paramBoolean) throws RemoteException;
  
  void onVoiceActivationStateChanged(int paramInt) throws RemoteException;
  
  class Default implements IPhoneStateListener {
    public void onServiceStateChanged(ServiceState param1ServiceState) throws RemoteException {}
    
    public void onSignalStrengthChanged(int param1Int) throws RemoteException {}
    
    public void onMessageWaitingIndicatorChanged(boolean param1Boolean) throws RemoteException {}
    
    public void onCallForwardingIndicatorChanged(boolean param1Boolean) throws RemoteException {}
    
    public void onCellLocationChanged(CellIdentity param1CellIdentity) throws RemoteException {}
    
    public void onCallStateChanged(int param1Int, String param1String) throws RemoteException {}
    
    public void onDataConnectionStateChanged(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onDataActivity(int param1Int) throws RemoteException {}
    
    public void onSignalStrengthsChanged(SignalStrength param1SignalStrength) throws RemoteException {}
    
    public void onCellInfoChanged(List<CellInfo> param1List) throws RemoteException {}
    
    public void onPreciseCallStateChanged(PreciseCallState param1PreciseCallState) throws RemoteException {}
    
    public void onPreciseDataConnectionStateChanged(PreciseDataConnectionState param1PreciseDataConnectionState) throws RemoteException {}
    
    public void onDataConnectionRealTimeInfoChanged(DataConnectionRealTimeInfo param1DataConnectionRealTimeInfo) throws RemoteException {}
    
    public void onSrvccStateChanged(int param1Int) throws RemoteException {}
    
    public void onVoiceActivationStateChanged(int param1Int) throws RemoteException {}
    
    public void onDataActivationStateChanged(int param1Int) throws RemoteException {}
    
    public void onOemHookRawEvent(byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public void onCarrierNetworkChange(boolean param1Boolean) throws RemoteException {}
    
    public void onUserMobileDataStateChanged(boolean param1Boolean) throws RemoteException {}
    
    public void onDisplayInfoChanged(TelephonyDisplayInfo param1TelephonyDisplayInfo) throws RemoteException {}
    
    public void onPhoneCapabilityChanged(PhoneCapability param1PhoneCapability) throws RemoteException {}
    
    public void onActiveDataSubIdChanged(int param1Int) throws RemoteException {}
    
    public void onRadioPowerStateChanged(int param1Int) throws RemoteException {}
    
    public void onCallAttributesChanged(CallAttributes param1CallAttributes) throws RemoteException {}
    
    public void onEmergencyNumberListChanged(Map param1Map) throws RemoteException {}
    
    public void onOutgoingEmergencyCall(EmergencyNumber param1EmergencyNumber) throws RemoteException {}
    
    public void onOutgoingEmergencySms(EmergencyNumber param1EmergencyNumber) throws RemoteException {}
    
    public void onCallDisconnectCauseChanged(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onImsCallDisconnectCauseChanged(ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void onRegistrationFailed(CellIdentity param1CellIdentity, String param1String, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void onBarringInfoChanged(BarringInfo param1BarringInfo) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPhoneStateListener {
    private static final String DESCRIPTOR = "com.android.internal.telephony.IPhoneStateListener";
    
    static final int TRANSACTION_onActiveDataSubIdChanged = 22;
    
    static final int TRANSACTION_onBarringInfoChanged = 31;
    
    static final int TRANSACTION_onCallAttributesChanged = 24;
    
    static final int TRANSACTION_onCallDisconnectCauseChanged = 28;
    
    static final int TRANSACTION_onCallForwardingIndicatorChanged = 4;
    
    static final int TRANSACTION_onCallStateChanged = 6;
    
    static final int TRANSACTION_onCarrierNetworkChange = 18;
    
    static final int TRANSACTION_onCellInfoChanged = 10;
    
    static final int TRANSACTION_onCellLocationChanged = 5;
    
    static final int TRANSACTION_onDataActivationStateChanged = 16;
    
    static final int TRANSACTION_onDataActivity = 8;
    
    static final int TRANSACTION_onDataConnectionRealTimeInfoChanged = 13;
    
    static final int TRANSACTION_onDataConnectionStateChanged = 7;
    
    static final int TRANSACTION_onDisplayInfoChanged = 20;
    
    static final int TRANSACTION_onEmergencyNumberListChanged = 25;
    
    static final int TRANSACTION_onImsCallDisconnectCauseChanged = 29;
    
    static final int TRANSACTION_onMessageWaitingIndicatorChanged = 3;
    
    static final int TRANSACTION_onOemHookRawEvent = 17;
    
    static final int TRANSACTION_onOutgoingEmergencyCall = 26;
    
    static final int TRANSACTION_onOutgoingEmergencySms = 27;
    
    static final int TRANSACTION_onPhoneCapabilityChanged = 21;
    
    static final int TRANSACTION_onPreciseCallStateChanged = 11;
    
    static final int TRANSACTION_onPreciseDataConnectionStateChanged = 12;
    
    static final int TRANSACTION_onRadioPowerStateChanged = 23;
    
    static final int TRANSACTION_onRegistrationFailed = 30;
    
    static final int TRANSACTION_onServiceStateChanged = 1;
    
    static final int TRANSACTION_onSignalStrengthChanged = 2;
    
    static final int TRANSACTION_onSignalStrengthsChanged = 9;
    
    static final int TRANSACTION_onSrvccStateChanged = 14;
    
    static final int TRANSACTION_onUserMobileDataStateChanged = 19;
    
    static final int TRANSACTION_onVoiceActivationStateChanged = 15;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.IPhoneStateListener");
    }
    
    public static IPhoneStateListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.IPhoneStateListener");
      if (iInterface != null && iInterface instanceof IPhoneStateListener)
        return (IPhoneStateListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 31:
          return "onBarringInfoChanged";
        case 30:
          return "onRegistrationFailed";
        case 29:
          return "onImsCallDisconnectCauseChanged";
        case 28:
          return "onCallDisconnectCauseChanged";
        case 27:
          return "onOutgoingEmergencySms";
        case 26:
          return "onOutgoingEmergencyCall";
        case 25:
          return "onEmergencyNumberListChanged";
        case 24:
          return "onCallAttributesChanged";
        case 23:
          return "onRadioPowerStateChanged";
        case 22:
          return "onActiveDataSubIdChanged";
        case 21:
          return "onPhoneCapabilityChanged";
        case 20:
          return "onDisplayInfoChanged";
        case 19:
          return "onUserMobileDataStateChanged";
        case 18:
          return "onCarrierNetworkChange";
        case 17:
          return "onOemHookRawEvent";
        case 16:
          return "onDataActivationStateChanged";
        case 15:
          return "onVoiceActivationStateChanged";
        case 14:
          return "onSrvccStateChanged";
        case 13:
          return "onDataConnectionRealTimeInfoChanged";
        case 12:
          return "onPreciseDataConnectionStateChanged";
        case 11:
          return "onPreciseCallStateChanged";
        case 10:
          return "onCellInfoChanged";
        case 9:
          return "onSignalStrengthsChanged";
        case 8:
          return "onDataActivity";
        case 7:
          return "onDataConnectionStateChanged";
        case 6:
          return "onCallStateChanged";
        case 5:
          return "onCellLocationChanged";
        case 4:
          return "onCallForwardingIndicatorChanged";
        case 3:
          return "onMessageWaitingIndicatorChanged";
        case 2:
          return "onSignalStrengthChanged";
        case 1:
          break;
      } 
      return "onServiceStateChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      ClassLoader classLoader;
      if (param1Int1 != 1598968902) {
        HashMap hashMap;
        byte[] arrayOfByte;
        ArrayList<CellInfo> arrayList;
        String str1, str2;
        int i;
        boolean bool1 = false, bool2 = false, bool3 = false, bool4 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 31:
            param1Parcel1.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            if (param1Parcel1.readInt() != 0) {
              BarringInfo barringInfo = (BarringInfo)BarringInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onBarringInfoChanged((BarringInfo)param1Parcel1);
            return true;
          case 30:
            param1Parcel1.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            if (param1Parcel1.readInt() != 0) {
              CellIdentity cellIdentity = (CellIdentity)CellIdentity.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            str2 = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            i = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            onRegistrationFailed((CellIdentity)param1Parcel2, str2, param1Int1, i, param1Int2);
            return true;
          case 29:
            param1Parcel1.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            if (param1Parcel1.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onImsCallDisconnectCauseChanged((ImsReasonInfo)param1Parcel1);
            return true;
          case 28:
            param1Parcel1.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            onCallDisconnectCauseChanged(param1Int1, param1Int2);
            return true;
          case 27:
            param1Parcel1.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            if (param1Parcel1.readInt() != 0) {
              EmergencyNumber emergencyNumber = (EmergencyNumber)EmergencyNumber.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onOutgoingEmergencySms((EmergencyNumber)param1Parcel1);
            return true;
          case 26:
            param1Parcel1.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            if (param1Parcel1.readInt() != 0) {
              EmergencyNumber emergencyNumber = (EmergencyNumber)EmergencyNumber.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onOutgoingEmergencyCall((EmergencyNumber)param1Parcel1);
            return true;
          case 25:
            param1Parcel1.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            classLoader = getClass().getClassLoader();
            hashMap = param1Parcel1.readHashMap(classLoader);
            onEmergencyNumberListChanged(hashMap);
            return true;
          case 24:
            hashMap.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            if (hashMap.readInt() != 0) {
              CallAttributes callAttributes = (CallAttributes)CallAttributes.CREATOR.createFromParcel((Parcel)hashMap);
            } else {
              hashMap = null;
            } 
            onCallAttributesChanged((CallAttributes)hashMap);
            return true;
          case 23:
            hashMap.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            param1Int1 = hashMap.readInt();
            onRadioPowerStateChanged(param1Int1);
            return true;
          case 22:
            hashMap.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            param1Int1 = hashMap.readInt();
            onActiveDataSubIdChanged(param1Int1);
            return true;
          case 21:
            hashMap.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            if (hashMap.readInt() != 0) {
              PhoneCapability phoneCapability = (PhoneCapability)PhoneCapability.CREATOR.createFromParcel((Parcel)hashMap);
            } else {
              hashMap = null;
            } 
            onPhoneCapabilityChanged((PhoneCapability)hashMap);
            return true;
          case 20:
            hashMap.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            if (hashMap.readInt() != 0) {
              TelephonyDisplayInfo telephonyDisplayInfo = (TelephonyDisplayInfo)TelephonyDisplayInfo.CREATOR.createFromParcel((Parcel)hashMap);
            } else {
              hashMap = null;
            } 
            onDisplayInfoChanged((TelephonyDisplayInfo)hashMap);
            return true;
          case 19:
            hashMap.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            if (hashMap.readInt() != 0)
              bool4 = true; 
            onUserMobileDataStateChanged(bool4);
            return true;
          case 18:
            hashMap.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            bool4 = bool1;
            if (hashMap.readInt() != 0)
              bool4 = true; 
            onCarrierNetworkChange(bool4);
            return true;
          case 17:
            hashMap.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            arrayOfByte = hashMap.createByteArray();
            onOemHookRawEvent(arrayOfByte);
            return true;
          case 16:
            arrayOfByte.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            param1Int1 = arrayOfByte.readInt();
            onDataActivationStateChanged(param1Int1);
            return true;
          case 15:
            arrayOfByte.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            param1Int1 = arrayOfByte.readInt();
            onVoiceActivationStateChanged(param1Int1);
            return true;
          case 14:
            arrayOfByte.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            param1Int1 = arrayOfByte.readInt();
            onSrvccStateChanged(param1Int1);
            return true;
          case 13:
            arrayOfByte.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            if (arrayOfByte.readInt() != 0) {
              DataConnectionRealTimeInfo dataConnectionRealTimeInfo = (DataConnectionRealTimeInfo)DataConnectionRealTimeInfo.CREATOR.createFromParcel((Parcel)arrayOfByte);
            } else {
              arrayOfByte = null;
            } 
            onDataConnectionRealTimeInfoChanged((DataConnectionRealTimeInfo)arrayOfByte);
            return true;
          case 12:
            arrayOfByte.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            if (arrayOfByte.readInt() != 0) {
              PreciseDataConnectionState preciseDataConnectionState = (PreciseDataConnectionState)PreciseDataConnectionState.CREATOR.createFromParcel((Parcel)arrayOfByte);
            } else {
              arrayOfByte = null;
            } 
            onPreciseDataConnectionStateChanged((PreciseDataConnectionState)arrayOfByte);
            return true;
          case 11:
            arrayOfByte.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            if (arrayOfByte.readInt() != 0) {
              PreciseCallState preciseCallState = (PreciseCallState)PreciseCallState.CREATOR.createFromParcel((Parcel)arrayOfByte);
            } else {
              arrayOfByte = null;
            } 
            onPreciseCallStateChanged((PreciseCallState)arrayOfByte);
            return true;
          case 10:
            arrayOfByte.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            arrayList = arrayOfByte.createTypedArrayList(CellInfo.CREATOR);
            onCellInfoChanged(arrayList);
            return true;
          case 9:
            arrayList.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            if (arrayList.readInt() != 0) {
              SignalStrength signalStrength = (SignalStrength)SignalStrength.CREATOR.createFromParcel((Parcel)arrayList);
            } else {
              arrayList = null;
            } 
            onSignalStrengthsChanged((SignalStrength)arrayList);
            return true;
          case 8:
            arrayList.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            param1Int1 = arrayList.readInt();
            onDataActivity(param1Int1);
            return true;
          case 7:
            arrayList.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            param1Int1 = arrayList.readInt();
            param1Int2 = arrayList.readInt();
            onDataConnectionStateChanged(param1Int1, param1Int2);
            return true;
          case 6:
            arrayList.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            param1Int1 = arrayList.readInt();
            str1 = arrayList.readString();
            onCallStateChanged(param1Int1, str1);
            return true;
          case 5:
            str1.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            if (str1.readInt() != 0) {
              CellIdentity cellIdentity = (CellIdentity)CellIdentity.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            onCellLocationChanged((CellIdentity)str1);
            return true;
          case 4:
            str1.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            bool4 = bool2;
            if (str1.readInt() != 0)
              bool4 = true; 
            onCallForwardingIndicatorChanged(bool4);
            return true;
          case 3:
            str1.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            bool4 = bool3;
            if (str1.readInt() != 0)
              bool4 = true; 
            onMessageWaitingIndicatorChanged(bool4);
            return true;
          case 2:
            str1.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
            param1Int1 = str1.readInt();
            onSignalStrengthChanged(param1Int1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("com.android.internal.telephony.IPhoneStateListener");
        if (str1.readInt() != 0) {
          ServiceState serviceState = (ServiceState)ServiceState.CREATOR.createFromParcel((Parcel)str1);
        } else {
          str1 = null;
        } 
        onServiceStateChanged((ServiceState)str1);
        return true;
      } 
      classLoader.writeString("com.android.internal.telephony.IPhoneStateListener");
      return true;
    }
    
    private static class Proxy implements IPhoneStateListener {
      public static IPhoneStateListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.IPhoneStateListener";
      }
      
      public void onServiceStateChanged(ServiceState param2ServiceState) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          if (param2ServiceState != null) {
            parcel.writeInt(1);
            param2ServiceState.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onServiceStateChanged(param2ServiceState);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSignalStrengthChanged(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onSignalStrengthChanged(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onMessageWaitingIndicatorChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(3, parcel, null, 1);
          if (!bool1 && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onMessageWaitingIndicatorChanged(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onCallForwardingIndicatorChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(4, parcel, null, 1);
          if (!bool1 && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onCallForwardingIndicatorChanged(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onCellLocationChanged(CellIdentity param2CellIdentity) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          if (param2CellIdentity != null) {
            parcel.writeInt(1);
            param2CellIdentity.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onCellLocationChanged(param2CellIdentity);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onCallStateChanged(int param2Int, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onCallStateChanged(param2Int, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDataConnectionStateChanged(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onDataConnectionStateChanged(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDataActivity(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onDataActivity(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSignalStrengthsChanged(SignalStrength param2SignalStrength) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          if (param2SignalStrength != null) {
            parcel.writeInt(1);
            param2SignalStrength.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onSignalStrengthsChanged(param2SignalStrength);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onCellInfoChanged(List<CellInfo> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(10, parcel, null, 1);
          if (!bool && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onCellInfoChanged(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPreciseCallStateChanged(PreciseCallState param2PreciseCallState) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          if (param2PreciseCallState != null) {
            parcel.writeInt(1);
            param2PreciseCallState.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(11, parcel, null, 1);
          if (!bool && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onPreciseCallStateChanged(param2PreciseCallState);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPreciseDataConnectionStateChanged(PreciseDataConnectionState param2PreciseDataConnectionState) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          if (param2PreciseDataConnectionState != null) {
            parcel.writeInt(1);
            param2PreciseDataConnectionState.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(12, parcel, null, 1);
          if (!bool && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onPreciseDataConnectionStateChanged(param2PreciseDataConnectionState);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDataConnectionRealTimeInfoChanged(DataConnectionRealTimeInfo param2DataConnectionRealTimeInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          if (param2DataConnectionRealTimeInfo != null) {
            parcel.writeInt(1);
            param2DataConnectionRealTimeInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(13, parcel, null, 1);
          if (!bool && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onDataConnectionRealTimeInfoChanged(param2DataConnectionRealTimeInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSrvccStateChanged(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(14, parcel, null, 1);
          if (!bool && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onSrvccStateChanged(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onVoiceActivationStateChanged(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(15, parcel, null, 1);
          if (!bool && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onVoiceActivationStateChanged(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDataActivationStateChanged(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(16, parcel, null, 1);
          if (!bool && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onDataActivationStateChanged(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onOemHookRawEvent(byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          parcel.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(17, parcel, null, 1);
          if (!bool && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onOemHookRawEvent(param2ArrayOfbyte);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onCarrierNetworkChange(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(18, parcel, null, 1);
          if (!bool1 && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onCarrierNetworkChange(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onUserMobileDataStateChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(19, parcel, null, 1);
          if (!bool1 && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onUserMobileDataStateChanged(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDisplayInfoChanged(TelephonyDisplayInfo param2TelephonyDisplayInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          if (param2TelephonyDisplayInfo != null) {
            parcel.writeInt(1);
            param2TelephonyDisplayInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(20, parcel, null, 1);
          if (!bool && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onDisplayInfoChanged(param2TelephonyDisplayInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPhoneCapabilityChanged(PhoneCapability param2PhoneCapability) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          if (param2PhoneCapability != null) {
            parcel.writeInt(1);
            param2PhoneCapability.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(21, parcel, null, 1);
          if (!bool && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onPhoneCapabilityChanged(param2PhoneCapability);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onActiveDataSubIdChanged(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(22, parcel, null, 1);
          if (!bool && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onActiveDataSubIdChanged(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onRadioPowerStateChanged(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(23, parcel, null, 1);
          if (!bool && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onRadioPowerStateChanged(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onCallAttributesChanged(CallAttributes param2CallAttributes) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          if (param2CallAttributes != null) {
            parcel.writeInt(1);
            param2CallAttributes.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(24, parcel, null, 1);
          if (!bool && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onCallAttributesChanged(param2CallAttributes);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onEmergencyNumberListChanged(Map param2Map) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          parcel.writeMap(param2Map);
          boolean bool = this.mRemote.transact(25, parcel, null, 1);
          if (!bool && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onEmergencyNumberListChanged(param2Map);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onOutgoingEmergencyCall(EmergencyNumber param2EmergencyNumber) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          if (param2EmergencyNumber != null) {
            parcel.writeInt(1);
            param2EmergencyNumber.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(26, parcel, null, 1);
          if (!bool && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onOutgoingEmergencyCall(param2EmergencyNumber);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onOutgoingEmergencySms(EmergencyNumber param2EmergencyNumber) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          if (param2EmergencyNumber != null) {
            parcel.writeInt(1);
            param2EmergencyNumber.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(27, parcel, null, 1);
          if (!bool && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onOutgoingEmergencySms(param2EmergencyNumber);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onCallDisconnectCauseChanged(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(28, parcel, null, 1);
          if (!bool && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onCallDisconnectCauseChanged(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onImsCallDisconnectCauseChanged(ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          if (param2ImsReasonInfo != null) {
            parcel.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(29, parcel, null, 1);
          if (!bool && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onImsCallDisconnectCauseChanged(param2ImsReasonInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onRegistrationFailed(CellIdentity param2CellIdentity, String param2String, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          if (param2CellIdentity != null) {
            parcel.writeInt(1);
            param2CellIdentity.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeString(param2String);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(30, parcel, null, 1);
          if (!bool && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onRegistrationFailed(param2CellIdentity, param2String, param2Int1, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onBarringInfoChanged(BarringInfo param2BarringInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IPhoneStateListener");
          if (param2BarringInfo != null) {
            parcel.writeInt(1);
            param2BarringInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(31, parcel, null, 1);
          if (!bool && IPhoneStateListener.Stub.getDefaultImpl() != null) {
            IPhoneStateListener.Stub.getDefaultImpl().onBarringInfoChanged(param2BarringInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPhoneStateListener param1IPhoneStateListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPhoneStateListener != null) {
          Proxy.sDefaultImpl = param1IPhoneStateListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPhoneStateListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
