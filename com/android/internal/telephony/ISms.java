package com.android.internal.telephony;

import android.app.PendingIntent;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface ISms extends IInterface {
  int checkSmsShortCodeDestination(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4) throws RemoteException;
  
  boolean copyMessageToIccEfForSubscriber(int paramInt1, String paramString, int paramInt2, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) throws RemoteException;
  
  String createAppSpecificSmsToken(int paramInt, String paramString, PendingIntent paramPendingIntent) throws RemoteException;
  
  String createAppSpecificSmsTokenWithPackageInfo(int paramInt, String paramString1, String paramString2, PendingIntent paramPendingIntent) throws RemoteException;
  
  boolean disableCellBroadcastForSubscriber(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  boolean disableCellBroadcastRangeForSubscriber(int paramInt1, int paramInt2, int paramInt3, int paramInt4) throws RemoteException;
  
  boolean enableCellBroadcastForSubscriber(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  boolean enableCellBroadcastRangeForSubscriber(int paramInt1, int paramInt2, int paramInt3, int paramInt4) throws RemoteException;
  
  List<SmsRawData> getAllMessagesFromIccEfForSubscriber(int paramInt, String paramString) throws RemoteException;
  
  Bundle getCarrierConfigValuesForSubscriber(int paramInt) throws RemoteException;
  
  String getImsSmsFormatForSubscriber(int paramInt) throws RemoteException;
  
  int getPreferredSmsSubscription() throws RemoteException;
  
  int getPremiumSmsPermission(String paramString) throws RemoteException;
  
  int getPremiumSmsPermissionForSubscriber(int paramInt, String paramString) throws RemoteException;
  
  int getSmsCapacityOnIccForSubscriber(int paramInt) throws RemoteException;
  
  String getSmscAddressFromIccEfForSubscriber(int paramInt, String paramString) throws RemoteException;
  
  void injectSmsPduForSubscriber(int paramInt, byte[] paramArrayOfbyte, String paramString, PendingIntent paramPendingIntent) throws RemoteException;
  
  boolean isImsSmsSupportedForSubscriber(int paramInt) throws RemoteException;
  
  boolean isSMSPromptEnabled() throws RemoteException;
  
  boolean isSmsSimPickActivityNeeded(int paramInt) throws RemoteException;
  
  boolean resetAllCellBroadcastRanges(int paramInt) throws RemoteException;
  
  void sendDataForSubscriber(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, int paramInt2, byte[] paramArrayOfbyte, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2) throws RemoteException;
  
  void sendMultipartTextForSubscriber(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4, List<String> paramList, List<PendingIntent> paramList1, List<PendingIntent> paramList2, boolean paramBoolean, long paramLong) throws RemoteException;
  
  void sendMultipartTextForSubscriberWithOptions(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, List<String> paramList, List<PendingIntent> paramList1, List<PendingIntent> paramList2, boolean paramBoolean1, int paramInt2, boolean paramBoolean2, int paramInt3) throws RemoteException;
  
  void sendStoredMultipartText(int paramInt, String paramString1, String paramString2, Uri paramUri, String paramString3, List<PendingIntent> paramList1, List<PendingIntent> paramList2) throws RemoteException;
  
  void sendStoredText(int paramInt, String paramString1, String paramString2, Uri paramUri, String paramString3, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2) throws RemoteException;
  
  void sendTextForSubscriber(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, boolean paramBoolean, long paramLong) throws RemoteException;
  
  void sendTextForSubscriberWithOptions(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, boolean paramBoolean1, int paramInt2, boolean paramBoolean2, int paramInt3) throws RemoteException;
  
  void setPremiumSmsPermission(String paramString, int paramInt) throws RemoteException;
  
  void setPremiumSmsPermissionForSubscriber(int paramInt1, String paramString, int paramInt2) throws RemoteException;
  
  boolean setSmscAddressOnIccEfForSubscriber(String paramString1, int paramInt, String paramString2) throws RemoteException;
  
  boolean updateMessageOnIccEfForSubscriber(int paramInt1, String paramString, int paramInt2, int paramInt3, byte[] paramArrayOfbyte) throws RemoteException;
  
  class Default implements ISms {
    public List<SmsRawData> getAllMessagesFromIccEfForSubscriber(int param1Int, String param1String) throws RemoteException {
      return null;
    }
    
    public boolean updateMessageOnIccEfForSubscriber(int param1Int1, String param1String, int param1Int2, int param1Int3, byte[] param1ArrayOfbyte) throws RemoteException {
      return false;
    }
    
    public boolean copyMessageToIccEfForSubscriber(int param1Int1, String param1String, int param1Int2, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2) throws RemoteException {
      return false;
    }
    
    public void sendDataForSubscriber(int param1Int1, String param1String1, String param1String2, String param1String3, String param1String4, int param1Int2, byte[] param1ArrayOfbyte, PendingIntent param1PendingIntent1, PendingIntent param1PendingIntent2) throws RemoteException {}
    
    public void sendTextForSubscriber(int param1Int, String param1String1, String param1String2, String param1String3, String param1String4, String param1String5, PendingIntent param1PendingIntent1, PendingIntent param1PendingIntent2, boolean param1Boolean, long param1Long) throws RemoteException {}
    
    public void sendTextForSubscriberWithOptions(int param1Int1, String param1String1, String param1String2, String param1String3, String param1String4, String param1String5, PendingIntent param1PendingIntent1, PendingIntent param1PendingIntent2, boolean param1Boolean1, int param1Int2, boolean param1Boolean2, int param1Int3) throws RemoteException {}
    
    public void injectSmsPduForSubscriber(int param1Int, byte[] param1ArrayOfbyte, String param1String, PendingIntent param1PendingIntent) throws RemoteException {}
    
    public void sendMultipartTextForSubscriber(int param1Int, String param1String1, String param1String2, String param1String3, String param1String4, List<String> param1List, List<PendingIntent> param1List1, List<PendingIntent> param1List2, boolean param1Boolean, long param1Long) throws RemoteException {}
    
    public void sendMultipartTextForSubscriberWithOptions(int param1Int1, String param1String1, String param1String2, String param1String3, String param1String4, List<String> param1List, List<PendingIntent> param1List1, List<PendingIntent> param1List2, boolean param1Boolean1, int param1Int2, boolean param1Boolean2, int param1Int3) throws RemoteException {}
    
    public boolean enableCellBroadcastForSubscriber(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {
      return false;
    }
    
    public boolean disableCellBroadcastForSubscriber(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {
      return false;
    }
    
    public boolean enableCellBroadcastRangeForSubscriber(int param1Int1, int param1Int2, int param1Int3, int param1Int4) throws RemoteException {
      return false;
    }
    
    public boolean disableCellBroadcastRangeForSubscriber(int param1Int1, int param1Int2, int param1Int3, int param1Int4) throws RemoteException {
      return false;
    }
    
    public int getPremiumSmsPermission(String param1String) throws RemoteException {
      return 0;
    }
    
    public int getPremiumSmsPermissionForSubscriber(int param1Int, String param1String) throws RemoteException {
      return 0;
    }
    
    public void setPremiumSmsPermission(String param1String, int param1Int) throws RemoteException {}
    
    public void setPremiumSmsPermissionForSubscriber(int param1Int1, String param1String, int param1Int2) throws RemoteException {}
    
    public boolean isImsSmsSupportedForSubscriber(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isSmsSimPickActivityNeeded(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getPreferredSmsSubscription() throws RemoteException {
      return 0;
    }
    
    public String getImsSmsFormatForSubscriber(int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean isSMSPromptEnabled() throws RemoteException {
      return false;
    }
    
    public void sendStoredText(int param1Int, String param1String1, String param1String2, Uri param1Uri, String param1String3, PendingIntent param1PendingIntent1, PendingIntent param1PendingIntent2) throws RemoteException {}
    
    public void sendStoredMultipartText(int param1Int, String param1String1, String param1String2, Uri param1Uri, String param1String3, List<PendingIntent> param1List1, List<PendingIntent> param1List2) throws RemoteException {}
    
    public Bundle getCarrierConfigValuesForSubscriber(int param1Int) throws RemoteException {
      return null;
    }
    
    public String createAppSpecificSmsToken(int param1Int, String param1String, PendingIntent param1PendingIntent) throws RemoteException {
      return null;
    }
    
    public String createAppSpecificSmsTokenWithPackageInfo(int param1Int, String param1String1, String param1String2, PendingIntent param1PendingIntent) throws RemoteException {
      return null;
    }
    
    public int checkSmsShortCodeDestination(int param1Int, String param1String1, String param1String2, String param1String3, String param1String4) throws RemoteException {
      return 0;
    }
    
    public String getSmscAddressFromIccEfForSubscriber(int param1Int, String param1String) throws RemoteException {
      return null;
    }
    
    public boolean setSmscAddressOnIccEfForSubscriber(String param1String1, int param1Int, String param1String2) throws RemoteException {
      return false;
    }
    
    public int getSmsCapacityOnIccForSubscriber(int param1Int) throws RemoteException {
      return 0;
    }
    
    public boolean resetAllCellBroadcastRanges(int param1Int) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISms {
    private static final String DESCRIPTOR = "com.android.internal.telephony.ISms";
    
    static final int TRANSACTION_checkSmsShortCodeDestination = 28;
    
    static final int TRANSACTION_copyMessageToIccEfForSubscriber = 3;
    
    static final int TRANSACTION_createAppSpecificSmsToken = 26;
    
    static final int TRANSACTION_createAppSpecificSmsTokenWithPackageInfo = 27;
    
    static final int TRANSACTION_disableCellBroadcastForSubscriber = 11;
    
    static final int TRANSACTION_disableCellBroadcastRangeForSubscriber = 13;
    
    static final int TRANSACTION_enableCellBroadcastForSubscriber = 10;
    
    static final int TRANSACTION_enableCellBroadcastRangeForSubscriber = 12;
    
    static final int TRANSACTION_getAllMessagesFromIccEfForSubscriber = 1;
    
    static final int TRANSACTION_getCarrierConfigValuesForSubscriber = 25;
    
    static final int TRANSACTION_getImsSmsFormatForSubscriber = 21;
    
    static final int TRANSACTION_getPreferredSmsSubscription = 20;
    
    static final int TRANSACTION_getPremiumSmsPermission = 14;
    
    static final int TRANSACTION_getPremiumSmsPermissionForSubscriber = 15;
    
    static final int TRANSACTION_getSmsCapacityOnIccForSubscriber = 31;
    
    static final int TRANSACTION_getSmscAddressFromIccEfForSubscriber = 29;
    
    static final int TRANSACTION_injectSmsPduForSubscriber = 7;
    
    static final int TRANSACTION_isImsSmsSupportedForSubscriber = 18;
    
    static final int TRANSACTION_isSMSPromptEnabled = 22;
    
    static final int TRANSACTION_isSmsSimPickActivityNeeded = 19;
    
    static final int TRANSACTION_resetAllCellBroadcastRanges = 32;
    
    static final int TRANSACTION_sendDataForSubscriber = 4;
    
    static final int TRANSACTION_sendMultipartTextForSubscriber = 8;
    
    static final int TRANSACTION_sendMultipartTextForSubscriberWithOptions = 9;
    
    static final int TRANSACTION_sendStoredMultipartText = 24;
    
    static final int TRANSACTION_sendStoredText = 23;
    
    static final int TRANSACTION_sendTextForSubscriber = 5;
    
    static final int TRANSACTION_sendTextForSubscriberWithOptions = 6;
    
    static final int TRANSACTION_setPremiumSmsPermission = 16;
    
    static final int TRANSACTION_setPremiumSmsPermissionForSubscriber = 17;
    
    static final int TRANSACTION_setSmscAddressOnIccEfForSubscriber = 30;
    
    static final int TRANSACTION_updateMessageOnIccEfForSubscriber = 2;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.ISms");
    }
    
    public static ISms asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.ISms");
      if (iInterface != null && iInterface instanceof ISms)
        return (ISms)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 32:
          return "resetAllCellBroadcastRanges";
        case 31:
          return "getSmsCapacityOnIccForSubscriber";
        case 30:
          return "setSmscAddressOnIccEfForSubscriber";
        case 29:
          return "getSmscAddressFromIccEfForSubscriber";
        case 28:
          return "checkSmsShortCodeDestination";
        case 27:
          return "createAppSpecificSmsTokenWithPackageInfo";
        case 26:
          return "createAppSpecificSmsToken";
        case 25:
          return "getCarrierConfigValuesForSubscriber";
        case 24:
          return "sendStoredMultipartText";
        case 23:
          return "sendStoredText";
        case 22:
          return "isSMSPromptEnabled";
        case 21:
          return "getImsSmsFormatForSubscriber";
        case 20:
          return "getPreferredSmsSubscription";
        case 19:
          return "isSmsSimPickActivityNeeded";
        case 18:
          return "isImsSmsSupportedForSubscriber";
        case 17:
          return "setPremiumSmsPermissionForSubscriber";
        case 16:
          return "setPremiumSmsPermission";
        case 15:
          return "getPremiumSmsPermissionForSubscriber";
        case 14:
          return "getPremiumSmsPermission";
        case 13:
          return "disableCellBroadcastRangeForSubscriber";
        case 12:
          return "enableCellBroadcastRangeForSubscriber";
        case 11:
          return "disableCellBroadcastForSubscriber";
        case 10:
          return "enableCellBroadcastForSubscriber";
        case 9:
          return "sendMultipartTextForSubscriberWithOptions";
        case 8:
          return "sendMultipartTextForSubscriber";
        case 7:
          return "injectSmsPduForSubscriber";
        case 6:
          return "sendTextForSubscriberWithOptions";
        case 5:
          return "sendTextForSubscriber";
        case 4:
          return "sendDataForSubscriber";
        case 3:
          return "copyMessageToIccEfForSubscriber";
        case 2:
          return "updateMessageOnIccEfForSubscriber";
        case 1:
          break;
      } 
      return "getAllMessagesFromIccEfForSubscriber";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool11;
        int i6;
        boolean bool10;
        int i5;
        boolean bool9;
        int i4;
        boolean bool8;
        int i3;
        boolean bool7;
        int i2;
        boolean bool6;
        int i1;
        boolean bool5;
        int n;
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        String str3;
        Bundle bundle;
        ArrayList<PendingIntent> arrayList1;
        String str2;
        byte[] arrayOfByte1;
        String str5;
        byte[] arrayOfByte2;
        String str4, str7;
        ArrayList<PendingIntent> arrayList2;
        String str6, str9;
        ArrayList<String> arrayList3;
        byte[] arrayOfByte3;
        String str8, str11;
        ArrayList<String> arrayList4;
        String str10;
        ArrayList<PendingIntent> arrayList6;
        String str13;
        ArrayList<PendingIntent> arrayList5;
        String str12;
        int i7, i8;
        String str14;
        ArrayList<PendingIntent> arrayList7;
        String str15;
        boolean bool12, bool13;
        long l;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 32:
            param1Parcel1.enforceInterface("com.android.internal.telephony.ISms");
            param1Int1 = param1Parcel1.readInt();
            bool11 = resetAllCellBroadcastRanges(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool11);
            return true;
          case 31:
            param1Parcel1.enforceInterface("com.android.internal.telephony.ISms");
            i6 = param1Parcel1.readInt();
            i6 = getSmsCapacityOnIccForSubscriber(i6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i6);
            return true;
          case 30:
            param1Parcel1.enforceInterface("com.android.internal.telephony.ISms");
            str5 = param1Parcel1.readString();
            i6 = param1Parcel1.readInt();
            str3 = param1Parcel1.readString();
            bool10 = setSmscAddressOnIccEfForSubscriber(str5, i6, str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool10);
            return true;
          case 29:
            str3.enforceInterface("com.android.internal.telephony.ISms");
            i5 = str3.readInt();
            str3 = str3.readString();
            str3 = getSmscAddressFromIccEfForSubscriber(i5, str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str3);
            return true;
          case 28:
            str3.enforceInterface("com.android.internal.telephony.ISms");
            i5 = str3.readInt();
            str7 = str3.readString();
            str9 = str3.readString();
            str5 = str3.readString();
            str3 = str3.readString();
            i5 = checkSmsShortCodeDestination(i5, str7, str9, str5, str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i5);
            return true;
          case 27:
            str3.enforceInterface("com.android.internal.telephony.ISms");
            i5 = str3.readInt();
            str5 = str3.readString();
            str9 = str3.readString();
            if (str3.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)str3);
            } else {
              str3 = null;
            } 
            str3 = createAppSpecificSmsTokenWithPackageInfo(i5, str5, str9, (PendingIntent)str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str3);
            return true;
          case 26:
            str3.enforceInterface("com.android.internal.telephony.ISms");
            i5 = str3.readInt();
            str5 = str3.readString();
            if (str3.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)str3);
            } else {
              str3 = null;
            } 
            str3 = createAppSpecificSmsToken(i5, str5, (PendingIntent)str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str3);
            return true;
          case 25:
            str3.enforceInterface("com.android.internal.telephony.ISms");
            i5 = str3.readInt();
            bundle = getCarrierConfigValuesForSubscriber(i5);
            param1Parcel2.writeNoException();
            if (bundle != null) {
              param1Parcel2.writeInt(1);
              bundle.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 24:
            bundle.enforceInterface("com.android.internal.telephony.ISms");
            i5 = bundle.readInt();
            str7 = bundle.readString();
            str9 = bundle.readString();
            if (bundle.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              str5 = null;
            } 
            str11 = bundle.readString();
            arrayList6 = bundle.createTypedArrayList(PendingIntent.CREATOR);
            arrayList1 = bundle.createTypedArrayList(PendingIntent.CREATOR);
            sendStoredMultipartText(i5, str7, str9, (Uri)str5, str11, arrayList6, arrayList1);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            arrayList1.enforceInterface("com.android.internal.telephony.ISms");
            i5 = arrayList1.readInt();
            str11 = arrayList1.readString();
            str7 = arrayList1.readString();
            if (arrayList1.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel((Parcel)arrayList1);
            } else {
              str5 = null;
            } 
            str13 = arrayList1.readString();
            if (arrayList1.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)arrayList1);
            } else {
              str9 = null;
            } 
            if (arrayList1.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)arrayList1);
            } else {
              arrayList1 = null;
            } 
            sendStoredText(i5, str11, str7, (Uri)str5, str13, (PendingIntent)str9, (PendingIntent)arrayList1);
            param1Parcel2.writeNoException();
            return true;
          case 22:
            arrayList1.enforceInterface("com.android.internal.telephony.ISms");
            bool9 = isSMSPromptEnabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool9);
            return true;
          case 21:
            arrayList1.enforceInterface("com.android.internal.telephony.ISms");
            i4 = arrayList1.readInt();
            str2 = getImsSmsFormatForSubscriber(i4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str2);
            return true;
          case 20:
            str2.enforceInterface("com.android.internal.telephony.ISms");
            i4 = getPreferredSmsSubscription();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i4);
            return true;
          case 19:
            str2.enforceInterface("com.android.internal.telephony.ISms");
            i4 = str2.readInt();
            bool8 = isSmsSimPickActivityNeeded(i4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool8);
            return true;
          case 18:
            str2.enforceInterface("com.android.internal.telephony.ISms");
            i3 = str2.readInt();
            bool7 = isImsSmsSupportedForSubscriber(i3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 17:
            str2.enforceInterface("com.android.internal.telephony.ISms");
            param1Int2 = str2.readInt();
            str5 = str2.readString();
            i2 = str2.readInt();
            setPremiumSmsPermissionForSubscriber(param1Int2, str5, i2);
            param1Parcel2.writeNoException();
            return true;
          case 16:
            str2.enforceInterface("com.android.internal.telephony.ISms");
            str5 = str2.readString();
            i2 = str2.readInt();
            setPremiumSmsPermission(str5, i2);
            param1Parcel2.writeNoException();
            return true;
          case 15:
            str2.enforceInterface("com.android.internal.telephony.ISms");
            i2 = str2.readInt();
            str2 = str2.readString();
            i2 = getPremiumSmsPermissionForSubscriber(i2, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i2);
            return true;
          case 14:
            str2.enforceInterface("com.android.internal.telephony.ISms");
            str2 = str2.readString();
            i2 = getPremiumSmsPermission(str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i2);
            return true;
          case 13:
            str2.enforceInterface("com.android.internal.telephony.ISms");
            i2 = str2.readInt();
            i7 = str2.readInt();
            i8 = str2.readInt();
            param1Int2 = str2.readInt();
            bool6 = disableCellBroadcastRangeForSubscriber(i2, i7, i8, param1Int2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 12:
            str2.enforceInterface("com.android.internal.telephony.ISms");
            i8 = str2.readInt();
            i1 = str2.readInt();
            i7 = str2.readInt();
            param1Int2 = str2.readInt();
            bool5 = enableCellBroadcastRangeForSubscriber(i8, i1, i7, param1Int2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 11:
            str2.enforceInterface("com.android.internal.telephony.ISms");
            n = str2.readInt();
            i8 = str2.readInt();
            param1Int2 = str2.readInt();
            bool4 = disableCellBroadcastForSubscriber(n, i8, param1Int2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 10:
            str2.enforceInterface("com.android.internal.telephony.ISms");
            param1Int2 = str2.readInt();
            i8 = str2.readInt();
            m = str2.readInt();
            bool3 = enableCellBroadcastForSubscriber(param1Int2, i8, m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 9:
            str2.enforceInterface("com.android.internal.telephony.ISms");
            param1Int2 = str2.readInt();
            str7 = str2.readString();
            str14 = str2.readString();
            str9 = str2.readString();
            str5 = str2.readString();
            arrayList4 = str2.createStringArrayList();
            arrayList5 = str2.createTypedArrayList(PendingIntent.CREATOR);
            arrayList7 = str2.createTypedArrayList(PendingIntent.CREATOR);
            if (str2.readInt() != 0) {
              bool12 = true;
            } else {
              bool12 = false;
            } 
            i8 = str2.readInt();
            if (str2.readInt() != 0) {
              bool13 = true;
            } else {
              bool13 = false;
            } 
            k = str2.readInt();
            sendMultipartTextForSubscriberWithOptions(param1Int2, str7, str14, str9, str5, arrayList4, arrayList5, arrayList7, bool12, i8, bool13, k);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            str2.enforceInterface("com.android.internal.telephony.ISms");
            k = str2.readInt();
            str12 = str2.readString();
            str14 = str2.readString();
            str15 = str2.readString();
            str5 = str2.readString();
            arrayList3 = str2.createStringArrayList();
            arrayList4 = str2.createTypedArrayList(PendingIntent.CREATOR);
            arrayList2 = str2.createTypedArrayList(PendingIntent.CREATOR);
            if (str2.readInt() != 0) {
              bool12 = true;
            } else {
              bool12 = false;
            } 
            l = str2.readLong();
            sendMultipartTextForSubscriber(k, str12, str14, str15, str5, arrayList3, (List)arrayList4, arrayList2, bool12, l);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            str2.enforceInterface("com.android.internal.telephony.ISms");
            k = str2.readInt();
            arrayOfByte3 = str2.createByteArray();
            str5 = str2.readString();
            if (str2.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            injectSmsPduForSubscriber(k, arrayOfByte3, str5, (PendingIntent)str2);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            str2.enforceInterface("com.android.internal.telephony.ISms");
            i8 = str2.readInt();
            str14 = str2.readString();
            str6 = str2.readString();
            str12 = str2.readString();
            str10 = str2.readString();
            str15 = str2.readString();
            if (str2.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str5 = null;
            } 
            if (str2.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)str2);
            } else {
              arrayOfByte3 = null;
            } 
            if (str2.readInt() != 0) {
              bool12 = true;
            } else {
              bool12 = false;
            } 
            param1Int2 = str2.readInt();
            if (str2.readInt() != 0) {
              bool13 = true;
            } else {
              bool13 = false;
            } 
            k = str2.readInt();
            sendTextForSubscriberWithOptions(i8, str14, str6, str12, str10, str15, (PendingIntent)str5, (PendingIntent)arrayOfByte3, bool12, param1Int2, bool13, k);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            str2.enforceInterface("com.android.internal.telephony.ISms");
            k = str2.readInt();
            str12 = str2.readString();
            str15 = str2.readString();
            str10 = str2.readString();
            str14 = str2.readString();
            str6 = str2.readString();
            if (str2.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str5 = null;
            } 
            if (str2.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)str2);
            } else {
              arrayOfByte3 = null;
            } 
            if (str2.readInt() != 0) {
              bool12 = true;
            } else {
              bool12 = false;
            } 
            l = str2.readLong();
            sendTextForSubscriber(k, str12, str15, str10, str14, str6, (PendingIntent)str5, (PendingIntent)arrayOfByte3, bool12, l);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            str2.enforceInterface("com.android.internal.telephony.ISms");
            k = str2.readInt();
            str10 = str2.readString();
            str15 = str2.readString();
            str12 = str2.readString();
            str6 = str2.readString();
            param1Int2 = str2.readInt();
            arrayOfByte3 = str2.createByteArray();
            if (str2.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str5 = null;
            } 
            if (str2.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            sendDataForSubscriber(k, str10, str15, str12, str6, param1Int2, arrayOfByte3, (PendingIntent)str5, (PendingIntent)str2);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            str2.enforceInterface("com.android.internal.telephony.ISms");
            param1Int2 = str2.readInt();
            str8 = str2.readString();
            k = str2.readInt();
            arrayOfByte2 = str2.createByteArray();
            arrayOfByte1 = str2.createByteArray();
            bool2 = copyMessageToIccEfForSubscriber(param1Int2, str8, k, arrayOfByte2, arrayOfByte1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 2:
            arrayOfByte1.enforceInterface("com.android.internal.telephony.ISms");
            param1Int2 = arrayOfByte1.readInt();
            str4 = arrayOfByte1.readString();
            i8 = arrayOfByte1.readInt();
            j = arrayOfByte1.readInt();
            arrayOfByte1 = arrayOfByte1.createByteArray();
            bool1 = updateMessageOnIccEfForSubscriber(param1Int2, str4, i8, j, arrayOfByte1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 1:
            break;
        } 
        arrayOfByte1.enforceInterface("com.android.internal.telephony.ISms");
        int i = arrayOfByte1.readInt();
        String str1 = arrayOfByte1.readString();
        List<SmsRawData> list = getAllMessagesFromIccEfForSubscriber(i, str1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeTypedList(list);
        return true;
      } 
      param1Parcel2.writeString("com.android.internal.telephony.ISms");
      return true;
    }
    
    private static class Proxy implements ISms {
      public static ISms sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.ISms";
      }
      
      public List<SmsRawData> getAllMessagesFromIccEfForSubscriber(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ISms.Stub.getDefaultImpl() != null)
            return ISms.Stub.getDefaultImpl().getAllMessagesFromIccEfForSubscriber(param2Int, param2String); 
          parcel2.readException();
          return parcel2.createTypedArrayList(SmsRawData.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean updateMessageOnIccEfForSubscriber(int param2Int1, String param2String, int param2Int2, int param2Int3, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeString(param2String);
              try {
                parcel1.writeInt(param2Int2);
                try {
                  parcel1.writeInt(param2Int3);
                  try {
                    parcel1.writeByteArray(param2ArrayOfbyte);
                    try {
                      IBinder iBinder = this.mRemote;
                      boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
                      if (!bool2 && ISms.Stub.getDefaultImpl() != null) {
                        bool1 = ISms.Stub.getDefaultImpl().updateMessageOnIccEfForSubscriber(param2Int1, param2String, param2Int2, param2Int3, param2ArrayOfbyte);
                        parcel2.recycle();
                        parcel1.recycle();
                        return bool1;
                      } 
                      parcel2.readException();
                      param2Int1 = parcel2.readInt();
                      if (param2Int1 != 0)
                        bool1 = true; 
                      parcel2.recycle();
                      parcel1.recycle();
                      return bool1;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public boolean copyMessageToIccEfForSubscriber(int param2Int1, String param2String, int param2Int2, byte[] param2ArrayOfbyte1, byte[] param2ArrayOfbyte2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeString(param2String);
              try {
                parcel1.writeInt(param2Int2);
                try {
                  parcel1.writeByteArray(param2ArrayOfbyte1);
                  try {
                    parcel1.writeByteArray(param2ArrayOfbyte2);
                    try {
                      IBinder iBinder = this.mRemote;
                      boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
                      if (!bool2 && ISms.Stub.getDefaultImpl() != null) {
                        bool1 = ISms.Stub.getDefaultImpl().copyMessageToIccEfForSubscriber(param2Int1, param2String, param2Int2, param2ArrayOfbyte1, param2ArrayOfbyte2);
                        parcel2.recycle();
                        parcel1.recycle();
                        return bool1;
                      } 
                      parcel2.readException();
                      param2Int1 = parcel2.readInt();
                      if (param2Int1 != 0)
                        bool1 = true; 
                      parcel2.recycle();
                      parcel1.recycle();
                      return bool1;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public void sendDataForSubscriber(int param2Int1, String param2String1, String param2String2, String param2String3, String param2String4, int param2Int2, byte[] param2ArrayOfbyte, PendingIntent param2PendingIntent1, PendingIntent param2PendingIntent2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          try {
            parcel1.writeInt(param2Int1);
            parcel1.writeString(param2String1);
            parcel1.writeString(param2String2);
            parcel1.writeString(param2String3);
            parcel1.writeString(param2String4);
            parcel1.writeInt(param2Int2);
            parcel1.writeByteArray(param2ArrayOfbyte);
            if (param2PendingIntent1 != null) {
              parcel1.writeInt(1);
              param2PendingIntent1.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            if (param2PendingIntent2 != null) {
              parcel1.writeInt(1);
              param2PendingIntent2.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
            if (!bool && ISms.Stub.getDefaultImpl() != null) {
              ISms.Stub.getDefaultImpl().sendDataForSubscriber(param2Int1, param2String1, param2String2, param2String3, param2String4, param2Int2, param2ArrayOfbyte, param2PendingIntent1, param2PendingIntent2);
              parcel2.recycle();
              parcel1.recycle();
              return;
            } 
            parcel2.readException();
            parcel2.recycle();
            parcel1.recycle();
            return;
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void sendTextForSubscriber(int param2Int, String param2String1, String param2String2, String param2String3, String param2String4, String param2String5, PendingIntent param2PendingIntent1, PendingIntent param2PendingIntent2, boolean param2Boolean, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          parcel1.writeString(param2String4);
          parcel1.writeString(param2String5);
          boolean bool = true;
          if (param2PendingIntent1 != null) {
            try {
              parcel1.writeInt(1);
              param2PendingIntent1.writeToParcel(parcel1, 0);
            } finally {}
          } else {
            parcel1.writeInt(0);
          } 
          if (param2PendingIntent2 != null) {
            parcel1.writeInt(1);
            param2PendingIntent2.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          parcel1.writeLong(param2Long);
          boolean bool1 = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool1 && ISms.Stub.getDefaultImpl() != null) {
            ISms iSms = ISms.Stub.getDefaultImpl();
            try {
              iSms.sendTextForSubscriber(param2Int, param2String1, param2String2, param2String3, param2String4, param2String5, param2PendingIntent1, param2PendingIntent2, param2Boolean, param2Long);
              parcel2.recycle();
              parcel1.recycle();
              return;
            } finally {}
          } else {
            parcel2.readException();
            parcel2.recycle();
            parcel1.recycle();
            return;
          } 
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void sendTextForSubscriberWithOptions(int param2Int1, String param2String1, String param2String2, String param2String3, String param2String4, String param2String5, PendingIntent param2PendingIntent1, PendingIntent param2PendingIntent2, boolean param2Boolean1, int param2Int2, boolean param2Boolean2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          parcel1.writeString(param2String4);
          parcel1.writeString(param2String5);
          boolean bool1 = true;
          if (param2PendingIntent1 != null) {
            try {
              parcel1.writeInt(1);
              param2PendingIntent1.writeToParcel(parcel1, 0);
            } finally {}
          } else {
            parcel1.writeInt(0);
          } 
          if (param2PendingIntent2 != null) {
            parcel1.writeInt(1);
            param2PendingIntent2.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeInt(param2Int2);
          if (param2Boolean2) {
            bool2 = bool1;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && ISms.Stub.getDefaultImpl() != null) {
            ISms iSms = ISms.Stub.getDefaultImpl();
            try {
              iSms.sendTextForSubscriberWithOptions(param2Int1, param2String1, param2String2, param2String3, param2String4, param2String5, param2PendingIntent1, param2PendingIntent2, param2Boolean1, param2Int2, param2Boolean2, param2Int3);
              parcel2.recycle();
              parcel1.recycle();
              return;
            } finally {}
          } else {
            parcel2.readException();
            parcel2.recycle();
            parcel1.recycle();
            return;
          } 
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void injectSmsPduForSubscriber(int param2Int, byte[] param2ArrayOfbyte, String param2String, PendingIntent param2PendingIntent) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          parcel1.writeInt(param2Int);
          parcel1.writeByteArray(param2ArrayOfbyte);
          parcel1.writeString(param2String);
          if (param2PendingIntent != null) {
            parcel1.writeInt(1);
            param2PendingIntent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && ISms.Stub.getDefaultImpl() != null) {
            ISms.Stub.getDefaultImpl().injectSmsPduForSubscriber(param2Int, param2ArrayOfbyte, param2String, param2PendingIntent);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendMultipartTextForSubscriber(int param2Int, String param2String1, String param2String2, String param2String3, String param2String4, List<String> param2List, List<PendingIntent> param2List1, List<PendingIntent> param2List2, boolean param2Boolean, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          try {
            boolean bool;
            parcel1.writeInt(param2Int);
            parcel1.writeString(param2String1);
            parcel1.writeString(param2String2);
            parcel1.writeString(param2String3);
            parcel1.writeString(param2String4);
            parcel1.writeStringList(param2List);
            parcel1.writeTypedList(param2List1);
            parcel1.writeTypedList(param2List2);
            if (param2Boolean) {
              bool = true;
            } else {
              bool = false;
            } 
            parcel1.writeInt(bool);
            parcel1.writeLong(param2Long);
            boolean bool1 = this.mRemote.transact(8, parcel1, parcel2, 0);
            if (!bool1 && ISms.Stub.getDefaultImpl() != null) {
              ISms.Stub.getDefaultImpl().sendMultipartTextForSubscriber(param2Int, param2String1, param2String2, param2String3, param2String4, param2List, param2List1, param2List2, param2Boolean, param2Long);
              parcel2.recycle();
              parcel1.recycle();
              return;
            } 
            parcel2.readException();
            parcel2.recycle();
            parcel1.recycle();
            return;
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void sendMultipartTextForSubscriberWithOptions(int param2Int1, String param2String1, String param2String2, String param2String3, String param2String4, List<String> param2List, List<PendingIntent> param2List1, List<PendingIntent> param2List2, boolean param2Boolean1, int param2Int2, boolean param2Boolean2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          parcel1.writeString(param2String4);
          parcel1.writeStringList(param2List);
          parcel1.writeTypedList(param2List1);
          parcel1.writeTypedList(param2List2);
          boolean bool1 = true;
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeInt(param2Int2);
          if (param2Boolean2) {
            bool2 = bool1;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && ISms.Stub.getDefaultImpl() != null) {
            ISms.Stub.getDefaultImpl().sendMultipartTextForSubscriberWithOptions(param2Int1, param2String1, param2String2, param2String3, param2String4, param2List, param2List1, param2List2, param2Boolean1, param2Int2, param2Boolean2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean enableCellBroadcastForSubscriber(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(10, parcel1, parcel2, 0);
          if (!bool2 && ISms.Stub.getDefaultImpl() != null) {
            bool1 = ISms.Stub.getDefaultImpl().enableCellBroadcastForSubscriber(param2Int1, param2Int2, param2Int3);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean disableCellBroadcastForSubscriber(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(11, parcel1, parcel2, 0);
          if (!bool2 && ISms.Stub.getDefaultImpl() != null) {
            bool1 = ISms.Stub.getDefaultImpl().disableCellBroadcastForSubscriber(param2Int1, param2Int2, param2Int3);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean enableCellBroadcastRangeForSubscriber(int param2Int1, int param2Int2, int param2Int3, int param2Int4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          parcel1.writeInt(param2Int4);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(12, parcel1, parcel2, 0);
          if (!bool2 && ISms.Stub.getDefaultImpl() != null) {
            bool1 = ISms.Stub.getDefaultImpl().enableCellBroadcastRangeForSubscriber(param2Int1, param2Int2, param2Int3, param2Int4);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean disableCellBroadcastRangeForSubscriber(int param2Int1, int param2Int2, int param2Int3, int param2Int4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          parcel1.writeInt(param2Int4);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(13, parcel1, parcel2, 0);
          if (!bool2 && ISms.Stub.getDefaultImpl() != null) {
            bool1 = ISms.Stub.getDefaultImpl().disableCellBroadcastRangeForSubscriber(param2Int1, param2Int2, param2Int3, param2Int4);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getPremiumSmsPermission(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && ISms.Stub.getDefaultImpl() != null)
            return ISms.Stub.getDefaultImpl().getPremiumSmsPermission(param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getPremiumSmsPermissionForSubscriber(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && ISms.Stub.getDefaultImpl() != null) {
            param2Int = ISms.Stub.getDefaultImpl().getPremiumSmsPermissionForSubscriber(param2Int, param2String);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPremiumSmsPermission(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && ISms.Stub.getDefaultImpl() != null) {
            ISms.Stub.getDefaultImpl().setPremiumSmsPermission(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPremiumSmsPermissionForSubscriber(int param2Int1, String param2String, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && ISms.Stub.getDefaultImpl() != null) {
            ISms.Stub.getDefaultImpl().setPremiumSmsPermissionForSubscriber(param2Int1, param2String, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isImsSmsSupportedForSubscriber(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(18, parcel1, parcel2, 0);
          if (!bool2 && ISms.Stub.getDefaultImpl() != null) {
            bool1 = ISms.Stub.getDefaultImpl().isImsSmsSupportedForSubscriber(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isSmsSimPickActivityNeeded(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(19, parcel1, parcel2, 0);
          if (!bool2 && ISms.Stub.getDefaultImpl() != null) {
            bool1 = ISms.Stub.getDefaultImpl().isSmsSimPickActivityNeeded(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getPreferredSmsSubscription() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && ISms.Stub.getDefaultImpl() != null)
            return ISms.Stub.getDefaultImpl().getPreferredSmsSubscription(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getImsSmsFormatForSubscriber(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && ISms.Stub.getDefaultImpl() != null)
            return ISms.Stub.getDefaultImpl().getImsSmsFormatForSubscriber(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isSMSPromptEnabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(22, parcel1, parcel2, 0);
          if (!bool2 && ISms.Stub.getDefaultImpl() != null) {
            bool1 = ISms.Stub.getDefaultImpl().isSMSPromptEnabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendStoredText(int param2Int, String param2String1, String param2String2, Uri param2Uri, String param2String3, PendingIntent param2PendingIntent1, PendingIntent param2PendingIntent2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          try {
            parcel1.writeInt(param2Int);
            try {
              parcel1.writeString(param2String1);
              parcel1.writeString(param2String2);
              if (param2Uri != null) {
                parcel1.writeInt(1);
                param2Uri.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              parcel1.writeString(param2String3);
              if (param2PendingIntent1 != null) {
                parcel1.writeInt(1);
                param2PendingIntent1.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              if (param2PendingIntent2 != null) {
                parcel1.writeInt(1);
                param2PendingIntent2.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
              if (!bool && ISms.Stub.getDefaultImpl() != null) {
                ISms.Stub.getDefaultImpl().sendStoredText(param2Int, param2String1, param2String2, param2Uri, param2String3, param2PendingIntent1, param2PendingIntent2);
                parcel2.recycle();
                parcel1.recycle();
                return;
              } 
              parcel2.readException();
              parcel2.recycle();
              parcel1.recycle();
              return;
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void sendStoredMultipartText(int param2Int, String param2String1, String param2String2, Uri param2Uri, String param2String3, List<PendingIntent> param2List1, List<PendingIntent> param2List2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          try {
            parcel1.writeInt(param2Int);
            try {
              parcel1.writeString(param2String1);
              try {
                parcel1.writeString(param2String2);
                if (param2Uri != null) {
                  parcel1.writeInt(1);
                  param2Uri.writeToParcel(parcel1, 0);
                } else {
                  parcel1.writeInt(0);
                } 
                try {
                  parcel1.writeString(param2String3);
                  parcel1.writeTypedList(param2List1);
                  parcel1.writeTypedList(param2List2);
                  boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
                  if (!bool && ISms.Stub.getDefaultImpl() != null) {
                    ISms.Stub.getDefaultImpl().sendStoredMultipartText(param2Int, param2String1, param2String2, param2Uri, param2String3, param2List1, param2List2);
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } 
                  parcel2.readException();
                  parcel2.recycle();
                  parcel1.recycle();
                  return;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public Bundle getCarrierConfigValuesForSubscriber(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Bundle bundle;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && ISms.Stub.getDefaultImpl() != null) {
            bundle = ISms.Stub.getDefaultImpl().getCarrierConfigValuesForSubscriber(param2Int);
            return bundle;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            bundle = (Bundle)Bundle.CREATOR.createFromParcel(parcel2);
          } else {
            bundle = null;
          } 
          return bundle;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String createAppSpecificSmsToken(int param2Int, String param2String, PendingIntent param2PendingIntent) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          if (param2PendingIntent != null) {
            parcel1.writeInt(1);
            param2PendingIntent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && ISms.Stub.getDefaultImpl() != null) {
            param2String = ISms.Stub.getDefaultImpl().createAppSpecificSmsToken(param2Int, param2String, param2PendingIntent);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String createAppSpecificSmsTokenWithPackageInfo(int param2Int, String param2String1, String param2String2, PendingIntent param2PendingIntent) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2PendingIntent != null) {
            parcel1.writeInt(1);
            param2PendingIntent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && ISms.Stub.getDefaultImpl() != null) {
            param2String1 = ISms.Stub.getDefaultImpl().createAppSpecificSmsTokenWithPackageInfo(param2Int, param2String1, param2String2, param2PendingIntent);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int checkSmsShortCodeDestination(int param2Int, String param2String1, String param2String2, String param2String3, String param2String4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          parcel1.writeString(param2String4);
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && ISms.Stub.getDefaultImpl() != null) {
            param2Int = ISms.Stub.getDefaultImpl().checkSmsShortCodeDestination(param2Int, param2String1, param2String2, param2String3, param2String4);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getSmscAddressFromIccEfForSubscriber(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && ISms.Stub.getDefaultImpl() != null) {
            param2String = ISms.Stub.getDefaultImpl().getSmscAddressFromIccEfForSubscriber(param2Int, param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setSmscAddressOnIccEfForSubscriber(String param2String1, int param2Int, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(30, parcel1, parcel2, 0);
          if (!bool2 && ISms.Stub.getDefaultImpl() != null) {
            bool1 = ISms.Stub.getDefaultImpl().setSmscAddressOnIccEfForSubscriber(param2String1, param2Int, param2String2);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getSmsCapacityOnIccForSubscriber(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && ISms.Stub.getDefaultImpl() != null) {
            param2Int = ISms.Stub.getDefaultImpl().getSmsCapacityOnIccForSubscriber(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean resetAllCellBroadcastRanges(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISms");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(32, parcel1, parcel2, 0);
          if (!bool2 && ISms.Stub.getDefaultImpl() != null) {
            bool1 = ISms.Stub.getDefaultImpl().resetAllCellBroadcastRanges(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISms param1ISms) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISms != null) {
          Proxy.sDefaultImpl = param1ISms;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISms getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
