package com.android.internal.telephony;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ISmsSecurityService extends IInterface {
  boolean register(ISmsSecurityAgent paramISmsSecurityAgent) throws RemoteException;
  
  boolean sendResponse(SmsAuthorizationRequest paramSmsAuthorizationRequest, boolean paramBoolean) throws RemoteException;
  
  boolean unregister(ISmsSecurityAgent paramISmsSecurityAgent) throws RemoteException;
  
  class Default implements ISmsSecurityService {
    public boolean register(ISmsSecurityAgent param1ISmsSecurityAgent) throws RemoteException {
      return false;
    }
    
    public boolean unregister(ISmsSecurityAgent param1ISmsSecurityAgent) throws RemoteException {
      return false;
    }
    
    public boolean sendResponse(SmsAuthorizationRequest param1SmsAuthorizationRequest, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISmsSecurityService {
    private static final String DESCRIPTOR = "com.android.internal.telephony.ISmsSecurityService";
    
    static final int TRANSACTION_register = 1;
    
    static final int TRANSACTION_sendResponse = 3;
    
    static final int TRANSACTION_unregister = 2;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.ISmsSecurityService");
    }
    
    public static ISmsSecurityService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.ISmsSecurityService");
      if (iInterface != null && iInterface instanceof ISmsSecurityService)
        return (ISmsSecurityService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "sendResponse";
        } 
        return "unregister";
      } 
      return "register";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          SmsAuthorizationRequest smsAuthorizationRequest;
          boolean bool3;
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("com.android.internal.telephony.ISmsSecurityService");
            return true;
          } 
          param1Parcel1.enforceInterface("com.android.internal.telephony.ISmsSecurityService");
          if (param1Parcel1.readInt() != 0) {
            smsAuthorizationRequest = (SmsAuthorizationRequest)SmsAuthorizationRequest.CREATOR.createFromParcel(param1Parcel1);
          } else {
            smsAuthorizationRequest = null;
          } 
          if (param1Parcel1.readInt() != 0) {
            bool3 = true;
          } else {
            bool3 = false;
          } 
          boolean bool2 = sendResponse(smsAuthorizationRequest, bool3);
          param1Parcel2.writeNoException();
          param1Parcel2.writeInt(bool2);
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.internal.telephony.ISmsSecurityService");
        iSmsSecurityAgent = ISmsSecurityAgent.Stub.asInterface(param1Parcel1.readStrongBinder());
        boolean bool1 = unregister(iSmsSecurityAgent);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      iSmsSecurityAgent.enforceInterface("com.android.internal.telephony.ISmsSecurityService");
      ISmsSecurityAgent iSmsSecurityAgent = ISmsSecurityAgent.Stub.asInterface(iSmsSecurityAgent.readStrongBinder());
      boolean bool = register(iSmsSecurityAgent);
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(bool);
      return true;
    }
    
    private static class Proxy implements ISmsSecurityService {
      public static ISmsSecurityService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.ISmsSecurityService";
      }
      
      public boolean register(ISmsSecurityAgent param2ISmsSecurityAgent) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISmsSecurityService");
          if (param2ISmsSecurityAgent != null) {
            iBinder = param2ISmsSecurityAgent.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && ISmsSecurityService.Stub.getDefaultImpl() != null) {
            bool1 = ISmsSecurityService.Stub.getDefaultImpl().register(param2ISmsSecurityAgent);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean unregister(ISmsSecurityAgent param2ISmsSecurityAgent) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISmsSecurityService");
          if (param2ISmsSecurityAgent != null) {
            iBinder = param2ISmsSecurityAgent.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && ISmsSecurityService.Stub.getDefaultImpl() != null) {
            bool1 = ISmsSecurityService.Stub.getDefaultImpl().unregister(param2ISmsSecurityAgent);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean sendResponse(SmsAuthorizationRequest param2SmsAuthorizationRequest, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISmsSecurityService");
          boolean bool = true;
          if (param2SmsAuthorizationRequest != null) {
            parcel1.writeInt(1);
            param2SmsAuthorizationRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool1 && ISmsSecurityService.Stub.getDefaultImpl() != null) {
            param2Boolean = ISmsSecurityService.Stub.getDefaultImpl().sendResponse(param2SmsAuthorizationRequest, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISmsSecurityService param1ISmsSecurityService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISmsSecurityService != null) {
          Proxy.sDefaultImpl = param1ISmsSecurityService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISmsSecurityService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
