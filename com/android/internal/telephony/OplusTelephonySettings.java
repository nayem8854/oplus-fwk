package com.android.internal.telephony;

public class OplusTelephonySettings {
  public static final String KEY_LAB_ANTPOS = "oplus.radio.lab_antpos";
  
  public static final String KEY_LIGHT_SMART5G_CFG = "oplus.radio.light_smart5g_cfg";
  
  public static final String KEY_LIGHT_SMART5G_SWITCH = "oplus.radio.light_smart5g_switch";
  
  public static final String KEY_LTE_WIFI_COEXIST = "oplus.radio.lte_wifi_coexist";
  
  public static final String KEY_PDP_RECOVERY = "oplus.radio.pdp_recovery";
  
  public static final String KEY_RECOVERY_CONFIG_VER = "oplus.radio.recovery.config.ver";
  
  public static final String KEY_RECOVERY_MAIN_DEBUG = "oplus.radio.recovery.main.debug";
  
  public static final String KEY_RECOVERY_PDP_TOAST = "oplus.radio.recovery.pdp.toast";
  
  public static final String KEY_RECOVERY_SLOW_CONFIG = "oplus.radio.recovery.config.slow";
  
  public static final String KEY_RECOVERY_TAC_LIST = "oplus.radio.recovery_tac_list";
  
  public static final String KEY_SERVICE_STATE_CFG = "oplus.radio.service_state_cfg";
  
  public static final String KEY_SETTINGS_PIN_RETRY_NUM_SLOT1 = "oplus.radio.sim.retry.pin1";
  
  public static final String KEY_SETTINGS_PIN_RETRY_NUM_SLOT2 = "oplus.radio.sim.retry.pin1.2";
  
  public static final String KEY_SETTINGS_PUK_RETRY_NUM_SLOT1 = "oplus.radio.sim.retry.puk1";
  
  public static final String KEY_SETTINGS_PUK_RETRY_NUM_SLOT2 = "oplus.radio.sim.retry.puk1.2";
  
  public static final String KEY_SETTINGS_VOLTE_CALL_INTERNET_MTK = "oplus.radio.data_service_enabled";
  
  public static final String KEY_SMART5G_APK_CFG = "oplus.radio.smart5g_apk_cfg";
  
  public static final String KEY_SMART5G_BASIC_CFG = "oplus.radio.smart5g_basic_cfg";
  
  public static final String KEY_SMART5G_CMCC_EVAL = "oplus.radio.smart5g_cmcc_eval_enable";
  
  public static final String KEY_SMART5G_NO_OPERATOR_ENABLE = "oplus.radio.smart5g_nooperator_exp_enable";
  
  public static final String KEY_SMART5G_SCENES_CFG = "oplus.radio.smart5g_scenes_cfg";
  
  public static final String KEY_SMART5G_SWITCH = "oplus.radio.smart5g_switch";
  
  public static final String KEY_SMART5G_THERMAL_CFG = "oplus.radio.smart5g_thermal_cfg";
  
  public static final String KEY_USENTP_INTERVAL = "oplus.radio.use_ntp_interval";
  
  public static final String KEY_USENTP_TYPE = "oplus.radio.use_ntp_type";
}
