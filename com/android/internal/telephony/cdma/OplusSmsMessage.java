package com.android.internal.telephony.cdma;

import android.content.res.Resources;
import android.os.SystemProperties;
import android.telephony.PhoneNumberUtils;
import android.telephony.Rlog;
import android.text.TextUtils;
import android.util.Log;
import com.android.internal.telephony.GsmAlphabet;
import com.android.internal.telephony.Sms7BitEncodingTranslator;
import com.android.internal.telephony.SmsHeader;
import com.android.internal.telephony.cdma.sms.BearerData;
import com.android.internal.telephony.cdma.sms.CdmaSmsAddress;
import com.android.internal.telephony.cdma.sms.OplusBearerData;
import com.android.internal.telephony.cdma.sms.SmsEnvelope;
import com.android.internal.telephony.cdma.sms.UserData;
import com.android.internal.telephony.util.ReflectionHelper;
import com.android.internal.util.HexDump;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class OplusSmsMessage {
  private static final String LOGGABLE_TAG = "cdma-OplusSmsMessage";
  
  private static final String LOG_TAG = "cdma-OplusSmsMessage";
  
  private static final int NUMBER_244 = 244;
  
  private static final int NUMBER_255 = 255;
  
  public static GsmAlphabet.TextEncodingDetails calculateLengthOem(CharSequence paramCharSequence, boolean paramBoolean1, boolean paramBoolean2, int paramInt) {
    String str = null;
    Resources resources = Resources.getSystem();
    if (resources.getBoolean(17891537)) {
      str = Sms7BitEncodingTranslator.translate(paramCharSequence, true);
      Rlog.d("cdma-OplusSmsMessage", "search calculateLengthCDMA for help!");
    } 
    CharSequence charSequence = str;
    if (TextUtils.isEmpty(str))
      charSequence = paramCharSequence; 
    return OplusBearerData.calcTextEncodingDetailsOem(charSequence, paramBoolean1, paramBoolean2, paramInt);
  }
  
  public static SmsMessage.SubmitPdu getSubmitPduOem(String paramString1, String paramString2, String paramString3, boolean paramBoolean1, SmsHeader paramSmsHeader, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean2) {
    int i = paramInt2;
    if (paramString3 == null || paramString2 == null) {
      Log.e("cdma-OplusSmsMessage", "getSubmitPduOem, null sms text or destination address. do nothing.");
      return null;
    } 
    if (paramString2.isEmpty()) {
      Log.e("cdma-OplusSmsMessage", "getSubmitPduOem, destination address is empty. do nothing.");
      return null;
    } 
    if (paramString3.isEmpty()) {
      Log.e("cdma-OplusSmsMessage", "getSubmitPduOem, message text is empty. do nothing.");
      return null;
    } 
    paramInt2 = i;
    if (i > 244) {
      paramInt2 = i;
      if (i <= 255)
        paramInt2 = 244; 
    } 
    UserData userData = new UserData();
    userData.payloadStr = paramString3;
    userData.userDataHeader = paramSmsHeader;
    i = 2;
    if (paramInt3 == 1) {
      if (paramBoolean2) {
        paramInt3 = i;
      } else {
        paramInt3 = 9;
      } 
      userData.msgEncoding = paramInt3;
    } else if (paramInt3 == 2) {
      userData.msgEncoding = 0;
    } else {
      userData.msgEncoding = 4;
    } 
    userData.msgEncodingSet = true;
    return privateGetSubmitPduOem(paramString2, paramBoolean1, userData, 0L, paramInt2, paramInt1);
  }
  
  private static SmsMessage.SubmitPdu privateGetSubmitPduOem(String paramString, boolean paramBoolean, UserData paramUserData, long paramLong, int paramInt1, int paramInt2) {
    byte b;
    boolean bool;
    paramString = PhoneNumberUtils.cdmaCheckAndProcessPlusCodeForSms(paramString);
    CdmaSmsAddress cdmaSmsAddress = CdmaSmsAddress.parse(paramString);
    if (cdmaSmsAddress == null)
      return null; 
    if (cdmaSmsAddress.numberOfDigits > 36) {
      Rlog.d("cdma-OplusSmsMessage", "number of digit exceeds the SMS_ADDRESS_MAX");
      return null;
    } 
    BearerData bearerData = new BearerData();
    bearerData.messageType = 2;
    bearerData.messageId = SmsMessage.getNextMessageId();
    bearerData.deliveryAckReq = paramBoolean;
    bearerData.userAckReq = false;
    bearerData.readAckReq = false;
    bearerData.reportReq = false;
    if (paramInt1 >= 0) {
      bearerData.validityPeriodRelativeSet = true;
      bearerData.validityPeriodRelative = paramInt1;
    } else {
      bearerData.validityPeriodRelativeSet = false;
    } 
    Object object = ReflectionHelper.getDeclaredField(SmsMessage.class, "com.android.internal.telephony.cdma.SmsMessage", "PRIORITY_NORMAL");
    if (object != null) {
      paramInt1 = ((Integer)object).intValue();
    } else {
      paramInt1 = 0;
    } 
    object = ReflectionHelper.getDeclaredField(SmsMessage.class, "com.android.internal.telephony.cdma.SmsMessage", "PRIORITY_EMERGENCY");
    if (object != null) {
      b = ((Integer)object).intValue();
    } else {
      b = 3;
    } 
    object = ReflectionHelper.getDeclaredField(SmsMessage.class, "com.android.internal.telephony.cdma.SmsMessage", "RETURN_ACK");
    if (object != null) {
      bool = ((Integer)object).intValue();
    } else {
      bool = true;
    } 
    if (paramInt2 >= paramInt1 && paramInt2 <= b) {
      bearerData.priorityIndicatorSet = true;
      bearerData.priority = paramInt2;
    } 
    bearerData.userData = paramUserData;
    object = BearerData.encode(bearerData);
    if (object == null)
      return null; 
    if (Rlog.isLoggable("cdma-OplusSmsMessage", 2)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("MO (encoded) BearerData = ");
      stringBuilder.append(bearerData);
      Rlog.d("cdma-OplusSmsMessage", stringBuilder.toString());
      stringBuilder = new StringBuilder();
      stringBuilder.append("MO raw BearerData = '");
      stringBuilder.append(HexDump.toHexString((byte[])object));
      stringBuilder.append("'");
      Rlog.d("cdma-OplusSmsMessage", stringBuilder.toString());
    } 
    if (bearerData.hasUserDataHeader && paramUserData.msgEncoding != 2) {
      paramInt1 = 4101;
    } else {
      paramInt1 = 4098;
    } 
    SmsEnvelope smsEnvelope = new SmsEnvelope();
    smsEnvelope.messageType = 0;
    smsEnvelope.teleService = paramInt1;
    smsEnvelope.destAddress = cdmaSmsAddress;
    smsEnvelope.bearerReply = bool;
    smsEnvelope.bearerData = (byte[])object;
    try {
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      this(100);
      DataOutputStream dataOutputStream = new DataOutputStream();
      this(byteArrayOutputStream);
      dataOutputStream.writeInt(smsEnvelope.teleService);
      dataOutputStream.writeInt(0);
      dataOutputStream.writeInt(0);
      dataOutputStream.write(cdmaSmsAddress.digitMode);
      dataOutputStream.write(cdmaSmsAddress.numberMode);
      dataOutputStream.write(cdmaSmsAddress.ton);
      dataOutputStream.write(cdmaSmsAddress.numberPlan);
      dataOutputStream.write(cdmaSmsAddress.numberOfDigits);
      dataOutputStream.write(cdmaSmsAddress.origBytes, 0, cdmaSmsAddress.origBytes.length);
      dataOutputStream.write(0);
      dataOutputStream.write(0);
      dataOutputStream.write(0);
      dataOutputStream.write(object.length);
      dataOutputStream.write((byte[])object, 0, object.length);
      dataOutputStream.close();
      SmsMessage.SubmitPdu submitPdu = new SmsMessage.SubmitPdu();
      this();
      submitPdu.encodedMessage = byteArrayOutputStream.toByteArray();
      submitPdu.encodedScAddress = null;
      return submitPdu;
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("creating SubmitPdu failed: ");
      stringBuilder.append(iOException);
      Rlog.e("cdma-OplusSmsMessage", stringBuilder.toString());
      return null;
    } 
  }
  
  public static boolean oemGetSubmitPdu() {
    try {
      boolean bool = SystemProperties.get("persist.sys.oplus.radio.ct_auto_ims", "0").equals("1");
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("isCtIms=");
      stringBuilder.append(bool);
      Rlog.d("cdma-OplusSmsMessage", stringBuilder.toString());
      if (bool) {
        SystemProperties.set("persist.sys.oplus.radio.ct_auto_ims", "0");
        return true;
      } 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
    return false;
  }
}
