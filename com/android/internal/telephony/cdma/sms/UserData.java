package com.android.internal.telephony.cdma.sms;

import android.util.SparseIntArray;
import com.android.internal.telephony.SmsHeader;
import com.android.internal.util.HexDump;

public class UserData {
  public static final int ASCII_CR_INDEX = 13;
  
  public static final char[] ASCII_MAP = new char[] { 
      ' ', '!', '"', '#', '$', '%', '&', '\'', '(', ')', 
      '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', 
      '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', 
      '>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 
      'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 
      'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', 
      '\\', ']', '^', '_', '`', 'a', 'b', 'c', 'd', 'e', 
      'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 
      'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 
      'z', '{', '|', '}', '~' };
  
  public static final int ASCII_MAP_BASE_INDEX = 32;
  
  public static final int ASCII_MAP_MAX_INDEX;
  
  public static final int ASCII_NL_INDEX = 10;
  
  public static final int ENCODING_7BIT_ASCII = 2;
  
  public static final int ENCODING_GSM_7BIT_ALPHABET = 9;
  
  public static final int ENCODING_GSM_DCS = 10;
  
  public static final int ENCODING_GSM_DCS_16BIT = 2;
  
  public static final int ENCODING_GSM_DCS_7BIT = 0;
  
  public static final int ENCODING_GSM_DCS_8BIT = 1;
  
  public static final int ENCODING_IA5 = 3;
  
  public static final int ENCODING_IS91_EXTENDED_PROTOCOL = 1;
  
  public static final int ENCODING_KOREAN = 6;
  
  public static final int ENCODING_LATIN = 8;
  
  public static final int ENCODING_LATIN_HEBREW = 7;
  
  public static final int ENCODING_OCTET = 0;
  
  public static final int ENCODING_SHIFT_JIS = 5;
  
  public static final int ENCODING_UNICODE_16 = 4;
  
  public static final int IS91_MSG_TYPE_CLI = 132;
  
  public static final int IS91_MSG_TYPE_SHORT_MESSAGE = 133;
  
  public static final int IS91_MSG_TYPE_SHORT_MESSAGE_FULL = 131;
  
  public static final int IS91_MSG_TYPE_VOICEMAIL_STATUS = 130;
  
  public static final int PRINTABLE_ASCII_MIN_INDEX = 32;
  
  static final byte UNENCODABLE_7_BIT_CHAR = 32;
  
  public static final SparseIntArray charToAscii = new SparseIntArray();
  
  public int msgEncoding;
  
  static {
    byte b = 0;
    while (true) {
      char[] arrayOfChar = ASCII_MAP;
      if (b < arrayOfChar.length) {
        charToAscii.put(arrayOfChar[b], b + 32);
        b++;
        continue;
      } 
      break;
    } 
    charToAscii.put(10, 10);
    charToAscii.put(13, 13);
    ASCII_MAP_MAX_INDEX = ASCII_MAP.length + 32 - 1;
  }
  
  public static byte[] stringToAscii(String paramString) {
    int i = paramString.length();
    byte[] arrayOfByte = new byte[i];
    for (byte b = 0; b < i; b++) {
      int j = charToAscii.get(paramString.charAt(b), -1);
      if (j == -1)
        return null; 
      arrayOfByte[b] = (byte)j;
    } 
    return arrayOfByte;
  }
  
  public boolean msgEncodingSet = false;
  
  public int msgType;
  
  public int numFields;
  
  public int paddingBits;
  
  public byte[] payload;
  
  public String payloadStr;
  
  public SmsHeader userDataHeader;
  
  public String toString() {
    String str;
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("UserData ");
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("{ msgEncoding=");
    if (this.msgEncodingSet) {
      Integer integer = Integer.valueOf(this.msgEncoding);
    } else {
      str = "unset";
    } 
    stringBuilder2.append(str);
    stringBuilder1.append(stringBuilder2.toString());
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append(", msgType=");
    stringBuilder3.append(this.msgType);
    stringBuilder1.append(stringBuilder3.toString());
    stringBuilder3 = new StringBuilder();
    stringBuilder3.append(", paddingBits=");
    stringBuilder3.append(this.paddingBits);
    stringBuilder1.append(stringBuilder3.toString());
    stringBuilder3 = new StringBuilder();
    stringBuilder3.append(", numFields=");
    stringBuilder3.append(this.numFields);
    stringBuilder1.append(stringBuilder3.toString());
    stringBuilder3 = new StringBuilder();
    stringBuilder3.append(", userDataHeader=");
    stringBuilder3.append(this.userDataHeader);
    stringBuilder1.append(stringBuilder3.toString());
    stringBuilder3 = new StringBuilder();
    stringBuilder3.append(", payload='");
    stringBuilder3.append(HexDump.toHexString(this.payload));
    stringBuilder3.append("'");
    stringBuilder1.append(stringBuilder3.toString());
    stringBuilder3 = new StringBuilder();
    stringBuilder3.append(", payloadStr='");
    stringBuilder3.append(this.payloadStr);
    stringBuilder3.append("'");
    stringBuilder1.append(stringBuilder3.toString());
    stringBuilder1.append(" }");
    return stringBuilder1.toString();
  }
}
