package com.android.internal.telephony.cdma.sms;

import android.telephony.Rlog;
import com.android.internal.telephony.GsmAlphabet;

public final class OplusBearerData {
  private static final String LOG_TAG = "OplusBearerData";
  
  public static GsmAlphabet.TextEncodingDetails calcTextEncodingDetailsOem(CharSequence paramCharSequence, boolean paramBoolean1, boolean paramBoolean2, int paramInt) {
    GsmAlphabet.TextEncodingDetails textEncodingDetails;
    int i = countAsciiSeptets(paramCharSequence, paramBoolean1);
    if (paramInt == 3) {
      Rlog.d("OplusBearerData", "16bit in cdma");
      i = -1;
    } 
    if (i != -1 && i <= 160) {
      textEncodingDetails = new GsmAlphabet.TextEncodingDetails();
      textEncodingDetails.msgCount = 1;
      textEncodingDetails.codeUnitCount = i;
      textEncodingDetails.codeUnitsRemaining = 160 - i;
      textEncodingDetails.codeUnitSize = 1;
    } else {
      Rlog.d("OplusBearerData", "gsm can understand the control character, but cdma ignore it(<0x20)");
      GsmAlphabet.TextEncodingDetails textEncodingDetails1 = BearerData.calcTextEncodingDetails(paramCharSequence, paramBoolean1, true);
      textEncodingDetails = textEncodingDetails1;
      if (textEncodingDetails1.msgCount == 1) {
        textEncodingDetails = textEncodingDetails1;
        if (textEncodingDetails1.codeUnitSize == 1) {
          textEncodingDetails1.codeUnitCount = paramCharSequence.length();
          paramInt = textEncodingDetails1.codeUnitCount * 2;
          if (paramInt > 140) {
            textEncodingDetails1.msgCount = (paramInt + 133) / 134;
            textEncodingDetails1.codeUnitsRemaining = (textEncodingDetails1.msgCount * 134 - paramInt) / 2;
          } else {
            textEncodingDetails1.msgCount = 1;
            textEncodingDetails1.codeUnitsRemaining = (140 - paramInt) / 2;
          } 
          textEncodingDetails1.codeUnitSize = 3;
          textEncodingDetails = textEncodingDetails1;
        } 
      } 
    } 
    return textEncodingDetails;
  }
  
  private static int countAsciiSeptets(CharSequence paramCharSequence, boolean paramBoolean) {
    int i = paramCharSequence.length();
    if (paramBoolean)
      return i; 
    for (byte b = 0; b < i; b++) {
      if (UserData.charToAscii.get(paramCharSequence.charAt(b), -1) == -1)
        return -1; 
    } 
    return i;
  }
}
