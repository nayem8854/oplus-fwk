package com.android.internal.telephony.cdma.sms;

import android.content.res.Resources;
import android.telephony.SmsCbCmasInfo;
import android.telephony.cdma.CdmaSmsCbProgramData;
import android.telephony.cdma.CdmaSmsCbProgramResults;
import com.android.internal.telephony.EncodeException;
import com.android.internal.telephony.GsmAlphabet;
import com.android.internal.telephony.SmsHeader;
import com.android.internal.telephony.SmsMessageBase;
import com.android.internal.telephony.gsm.SmsMessage;
import com.android.internal.telephony.uicc.IccUtils;
import com.android.internal.util.BitwiseInputStream;
import com.android.internal.util.BitwiseOutputStream;
import com.android.telephony.Rlog;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;

public final class BearerData {
  public boolean priorityIndicatorSet = false;
  
  public int priority = 0;
  
  public boolean privacyIndicatorSet = false;
  
  public int privacy = 0;
  
  public boolean alertIndicatorSet = false;
  
  public int alert = 0;
  
  public boolean displayModeSet = false;
  
  public int displayMode = 1;
  
  public boolean languageIndicatorSet = false;
  
  public int language = 0;
  
  public boolean messageStatusSet = false;
  
  public int errorClass = 255;
  
  public int messageStatus = 255;
  
  public boolean userResponseCodeSet = false;
  
  public static final int ALERT_DEFAULT = 0;
  
  public static final int ALERT_HIGH_PRIO = 3;
  
  public static final int ALERT_LOW_PRIO = 1;
  
  public static final int ALERT_MEDIUM_PRIO = 2;
  
  public static final int DISPLAY_MODE_DEFAULT = 1;
  
  public static final int DISPLAY_MODE_IMMEDIATE = 0;
  
  public static final int DISPLAY_MODE_USER = 2;
  
  public static final int ERROR_NONE = 0;
  
  public static final int ERROR_PERMANENT = 3;
  
  public static final int ERROR_TEMPORARY = 2;
  
  public static final int ERROR_UNDEFINED = 255;
  
  public static final int LANGUAGE_CHINESE = 6;
  
  public static final int LANGUAGE_ENGLISH = 1;
  
  public static final int LANGUAGE_FRENCH = 2;
  
  public static final int LANGUAGE_HEBREW = 7;
  
  public static final int LANGUAGE_JAPANESE = 4;
  
  public static final int LANGUAGE_KOREAN = 5;
  
  public static final int LANGUAGE_SPANISH = 3;
  
  public static final int LANGUAGE_UNKNOWN = 0;
  
  private static final String LOG_TAG = "BearerData";
  
  public static final int MESSAGE_TYPE_CANCELLATION = 3;
  
  public static final int MESSAGE_TYPE_DELIVER = 1;
  
  public static final int MESSAGE_TYPE_DELIVERY_ACK = 4;
  
  public static final int MESSAGE_TYPE_DELIVER_REPORT = 7;
  
  public static final int MESSAGE_TYPE_READ_ACK = 6;
  
  public static final int MESSAGE_TYPE_SUBMIT = 2;
  
  public static final int MESSAGE_TYPE_SUBMIT_REPORT = 8;
  
  public static final int MESSAGE_TYPE_USER_ACK = 5;
  
  public static final int PRIORITY_EMERGENCY = 3;
  
  public static final int PRIORITY_INTERACTIVE = 1;
  
  public static final int PRIORITY_NORMAL = 0;
  
  public static final int PRIORITY_URGENT = 2;
  
  public static final int PRIVACY_CONFIDENTIAL = 2;
  
  public static final int PRIVACY_NOT_RESTRICTED = 0;
  
  public static final int PRIVACY_RESTRICTED = 1;
  
  public static final int PRIVACY_SECRET = 3;
  
  public static final int RELATIVE_TIME_DAYS_LIMIT = 196;
  
  public static final int RELATIVE_TIME_HOURS_LIMIT = 167;
  
  public static final int RELATIVE_TIME_INDEFINITE = 245;
  
  public static final int RELATIVE_TIME_MINS_LIMIT = 143;
  
  public static final int RELATIVE_TIME_MOBILE_INACTIVE = 247;
  
  public static final int RELATIVE_TIME_NOW = 246;
  
  public static final int RELATIVE_TIME_RESERVED = 248;
  
  public static final int RELATIVE_TIME_WEEKS_LIMIT = 244;
  
  public static final int STATUS_ACCEPTED = 0;
  
  public static final int STATUS_BLOCKED_DESTINATION = 7;
  
  public static final int STATUS_CANCELLED = 3;
  
  public static final int STATUS_CANCEL_FAILED = 6;
  
  public static final int STATUS_DELIVERED = 2;
  
  public static final int STATUS_DEPOSITED_TO_INTERNET = 1;
  
  public static final int STATUS_DUPLICATE_MESSAGE = 9;
  
  public static final int STATUS_INVALID_DESTINATION = 10;
  
  public static final int STATUS_MESSAGE_EXPIRED = 13;
  
  public static final int STATUS_NETWORK_CONGESTION = 4;
  
  public static final int STATUS_NETWORK_ERROR = 5;
  
  public static final int STATUS_TEXT_TOO_LONG = 8;
  
  public static final int STATUS_UNDEFINED = 255;
  
  public static final int STATUS_UNKNOWN_ERROR = 31;
  
  private static final byte SUBPARAM_ALERT_ON_MESSAGE_DELIVERY = 12;
  
  private static final byte SUBPARAM_CALLBACK_NUMBER = 14;
  
  private static final byte SUBPARAM_DEFERRED_DELIVERY_TIME_ABSOLUTE = 6;
  
  private static final byte SUBPARAM_DEFERRED_DELIVERY_TIME_RELATIVE = 7;
  
  private static final byte SUBPARAM_ID_LAST_DEFINED = 23;
  
  private static final byte SUBPARAM_LANGUAGE_INDICATOR = 13;
  
  private static final byte SUBPARAM_MESSAGE_CENTER_TIME_STAMP = 3;
  
  private static final byte SUBPARAM_MESSAGE_DEPOSIT_INDEX = 17;
  
  private static final byte SUBPARAM_MESSAGE_DISPLAY_MODE = 15;
  
  private static final byte SUBPARAM_MESSAGE_IDENTIFIER = 0;
  
  private static final byte SUBPARAM_MESSAGE_STATUS = 20;
  
  private static final byte SUBPARAM_NUMBER_OF_MESSAGES = 11;
  
  private static final byte SUBPARAM_PRIORITY_INDICATOR = 8;
  
  private static final byte SUBPARAM_PRIVACY_INDICATOR = 9;
  
  private static final byte SUBPARAM_REPLY_OPTION = 10;
  
  private static final byte SUBPARAM_SERVICE_CATEGORY_PROGRAM_DATA = 18;
  
  private static final byte SUBPARAM_SERVICE_CATEGORY_PROGRAM_RESULTS = 19;
  
  private static final byte SUBPARAM_USER_DATA = 1;
  
  private static final byte SUBPARAM_USER_RESPONSE_CODE = 2;
  
  private static final byte SUBPARAM_VALIDITY_PERIOD_ABSOLUTE = 4;
  
  private static final byte SUBPARAM_VALIDITY_PERIOD_RELATIVE = 5;
  
  public CdmaSmsAddress callbackNumber;
  
  public SmsCbCmasInfo cmasWarningInfo;
  
  public TimeStamp deferredDeliveryTimeAbsolute;
  
  public int deferredDeliveryTimeRelative;
  
  public boolean deferredDeliveryTimeRelativeSet;
  
  public boolean deliveryAckReq;
  
  public int depositIndex;
  
  public boolean hasUserDataHeader;
  
  public int messageId;
  
  public int messageType;
  
  public TimeStamp msgCenterTimeStamp;
  
  public int numberOfMessages;
  
  public boolean readAckReq;
  
  public boolean reportReq;
  
  public ArrayList<CdmaSmsCbProgramData> serviceCategoryProgramData;
  
  public ArrayList<CdmaSmsCbProgramResults> serviceCategoryProgramResults;
  
  public boolean userAckReq;
  
  public UserData userData;
  
  public int userResponseCode;
  
  public TimeStamp validityPeriodAbsolute;
  
  public int validityPeriodRelative;
  
  public boolean validityPeriodRelativeSet;
  
  public static class TimeStamp {
    public int hour;
    
    private ZoneId mZoneId = ZoneId.systemDefault();
    
    public int minute;
    
    public int monthDay;
    
    public int monthOrdinal;
    
    public int second;
    
    public int year;
    
    public static TimeStamp fromByteArray(byte[] param1ArrayOfbyte) {
      TimeStamp timeStamp = new TimeStamp();
      int i = IccUtils.cdmaBcdByteToInt(param1ArrayOfbyte[0]);
      if (i > 99 || i < 0)
        return null; 
      if (i >= 96) {
        i += 1900;
      } else {
        i += 2000;
      } 
      timeStamp.year = i;
      i = IccUtils.cdmaBcdByteToInt(param1ArrayOfbyte[1]);
      if (i < 1 || i > 12)
        return null; 
      timeStamp.monthOrdinal = i;
      i = IccUtils.cdmaBcdByteToInt(param1ArrayOfbyte[2]);
      if (i < 1 || i > 31)
        return null; 
      timeStamp.monthDay = i;
      i = IccUtils.cdmaBcdByteToInt(param1ArrayOfbyte[3]);
      if (i < 0 || i > 23)
        return null; 
      timeStamp.hour = i;
      i = IccUtils.cdmaBcdByteToInt(param1ArrayOfbyte[4]);
      if (i < 0 || i > 59)
        return null; 
      timeStamp.minute = i;
      i = IccUtils.cdmaBcdByteToInt(param1ArrayOfbyte[5]);
      if (i < 0 || i > 59)
        return null; 
      timeStamp.second = i;
      return timeStamp;
    }
    
    public static TimeStamp fromMillis(long param1Long) {
      TimeStamp timeStamp = new TimeStamp();
      LocalDateTime localDateTime = Instant.ofEpochMilli(param1Long).atZone(timeStamp.mZoneId).toLocalDateTime();
      int i = localDateTime.getYear();
      if (i < 1996 || i > 2095)
        return null; 
      timeStamp.year = i;
      timeStamp.monthOrdinal = localDateTime.getMonthValue();
      timeStamp.monthDay = localDateTime.getDayOfMonth();
      timeStamp.hour = localDateTime.getHour();
      timeStamp.minute = localDateTime.getMinute();
      timeStamp.second = localDateTime.getSecond();
      return timeStamp;
    }
    
    public byte[] toByteArray() {
      int i = this.year % 100;
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(6);
      byteArrayOutputStream.write((i / 10 & 0xF) << 4 | i % 10 & 0xF);
      i = this.monthOrdinal;
      byteArrayOutputStream.write(i % 10 & 0xF | i / 10 << 4 & 0xF0);
      i = this.monthDay;
      byteArrayOutputStream.write(i % 10 & 0xF | i / 10 << 4 & 0xF0);
      i = this.hour;
      byteArrayOutputStream.write(i % 10 & 0xF | i / 10 << 4 & 0xF0);
      i = this.minute;
      byteArrayOutputStream.write(i % 10 & 0xF | i / 10 << 4 & 0xF0);
      i = this.second;
      byteArrayOutputStream.write(i % 10 & 0xF | i / 10 << 4 & 0xF0);
      return byteArrayOutputStream.toByteArray();
    }
    
    public long toMillis() {
      int i = this.year, j = this.monthOrdinal, k = this.monthDay, m = this.hour, n = this.minute, i1 = this.second;
      LocalDateTime localDateTime = LocalDateTime.of(i, j, k, m, n, i1);
      Instant instant = localDateTime.toInstant(this.mZoneId.getRules().getOffset(localDateTime));
      return instant.toEpochMilli();
    }
    
    public String toString() {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("TimeStamp ");
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("{ year=");
      stringBuilder2.append(this.year);
      stringBuilder1.append(stringBuilder2.toString());
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append(", month=");
      stringBuilder2.append(this.monthOrdinal);
      stringBuilder1.append(stringBuilder2.toString());
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append(", day=");
      stringBuilder2.append(this.monthDay);
      stringBuilder1.append(stringBuilder2.toString());
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append(", hour=");
      stringBuilder2.append(this.hour);
      stringBuilder1.append(stringBuilder2.toString());
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append(", minute=");
      stringBuilder2.append(this.minute);
      stringBuilder1.append(stringBuilder2.toString());
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append(", second=");
      stringBuilder2.append(this.second);
      stringBuilder1.append(stringBuilder2.toString());
      stringBuilder1.append(" }");
      return stringBuilder1.toString();
    }
  }
  
  private static class CodingException extends Exception {
    public CodingException(String param1String) {
      super(param1String);
    }
  }
  
  public String getLanguage() {
    return getLanguageCodeForValue(this.language);
  }
  
  private static String getLanguageCodeForValue(int paramInt) {
    switch (paramInt) {
      default:
        return null;
      case 7:
        return "he";
      case 6:
        return "zh";
      case 5:
        return "ko";
      case 4:
        return "ja";
      case 3:
        return "es";
      case 2:
        return "fr";
      case 1:
        break;
    } 
    return "en";
  }
  
  public String toString() {
    String str5;
    Integer integer;
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("BearerData ");
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append("{ messageType=");
    stringBuilder3.append(this.messageType);
    stringBuilder1.append(stringBuilder3.toString());
    stringBuilder3 = new StringBuilder();
    stringBuilder3.append(", messageId=");
    stringBuilder3.append(this.messageId);
    stringBuilder1.append(stringBuilder3.toString());
    StringBuilder stringBuilder4 = new StringBuilder();
    stringBuilder4.append(", priority=");
    boolean bool = this.priorityIndicatorSet;
    String str6 = "unset";
    if (bool) {
      integer = Integer.valueOf(this.priority);
    } else {
      str5 = "unset";
    } 
    stringBuilder4.append(str5);
    stringBuilder1.append(stringBuilder4.toString());
    stringBuilder4 = new StringBuilder();
    stringBuilder4.append(", privacy=");
    if (this.privacyIndicatorSet) {
      integer = Integer.valueOf(this.privacy);
    } else {
      str5 = "unset";
    } 
    stringBuilder4.append(str5);
    stringBuilder1.append(stringBuilder4.toString());
    stringBuilder4 = new StringBuilder();
    stringBuilder4.append(", alert=");
    if (this.alertIndicatorSet) {
      integer = Integer.valueOf(this.alert);
    } else {
      str5 = "unset";
    } 
    stringBuilder4.append(str5);
    stringBuilder1.append(stringBuilder4.toString());
    stringBuilder4 = new StringBuilder();
    stringBuilder4.append(", displayMode=");
    if (this.displayModeSet) {
      integer = Integer.valueOf(this.displayMode);
    } else {
      str5 = "unset";
    } 
    stringBuilder4.append(str5);
    stringBuilder1.append(stringBuilder4.toString());
    stringBuilder4 = new StringBuilder();
    stringBuilder4.append(", language=");
    if (this.languageIndicatorSet) {
      integer = Integer.valueOf(this.language);
    } else {
      str5 = "unset";
    } 
    stringBuilder4.append(str5);
    stringBuilder1.append(stringBuilder4.toString());
    stringBuilder4 = new StringBuilder();
    stringBuilder4.append(", errorClass=");
    if (this.messageStatusSet) {
      integer = Integer.valueOf(this.errorClass);
    } else {
      str5 = "unset";
    } 
    stringBuilder4.append(str5);
    stringBuilder1.append(stringBuilder4.toString());
    stringBuilder4 = new StringBuilder();
    stringBuilder4.append(", msgStatus=");
    if (this.messageStatusSet) {
      integer = Integer.valueOf(this.messageStatus);
    } else {
      str5 = "unset";
    } 
    stringBuilder4.append(str5);
    stringBuilder1.append(stringBuilder4.toString());
    stringBuilder4 = new StringBuilder();
    stringBuilder4.append(", msgCenterTimeStamp=");
    TimeStamp timeStamp3 = this.msgCenterTimeStamp;
    if (timeStamp3 == null)
      str4 = "unset"; 
    stringBuilder4.append(str4);
    String str4 = stringBuilder4.toString();
    stringBuilder1.append(str4);
    stringBuilder4 = new StringBuilder();
    stringBuilder4.append(", validityPeriodAbsolute=");
    TimeStamp timeStamp2 = this.validityPeriodAbsolute;
    if (timeStamp2 == null)
      str3 = "unset"; 
    stringBuilder4.append(str3);
    String str3 = stringBuilder4.toString();
    stringBuilder1.append(str3);
    stringBuilder4 = new StringBuilder();
    stringBuilder4.append(", validityPeriodRelative=");
    if (this.validityPeriodRelativeSet) {
      integer = Integer.valueOf(this.validityPeriodRelative);
    } else {
      str3 = "unset";
    } 
    stringBuilder4.append(str3);
    str3 = stringBuilder4.toString();
    stringBuilder1.append(str3);
    stringBuilder4 = new StringBuilder();
    stringBuilder4.append(", deferredDeliveryTimeAbsolute=");
    TimeStamp timeStamp1 = this.deferredDeliveryTimeAbsolute;
    if (timeStamp1 == null)
      str2 = "unset"; 
    stringBuilder4.append(str2);
    String str2 = stringBuilder4.toString();
    stringBuilder1.append(str2);
    stringBuilder4 = new StringBuilder();
    stringBuilder4.append(", deferredDeliveryTimeRelative=");
    str2 = str6;
    if (this.deferredDeliveryTimeRelativeSet)
      integer = Integer.valueOf(this.deferredDeliveryTimeRelative); 
    stringBuilder4.append(integer);
    String str1 = stringBuilder4.toString();
    stringBuilder1.append(str1);
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", userAckReq=");
    stringBuilder2.append(this.userAckReq);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", deliveryAckReq=");
    stringBuilder2.append(this.deliveryAckReq);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", readAckReq=");
    stringBuilder2.append(this.readAckReq);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", reportReq=");
    stringBuilder2.append(this.reportReq);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", numberOfMessages=");
    stringBuilder2.append(this.numberOfMessages);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", callbackNumber=");
    stringBuilder2.append(Rlog.pii("BearerData", this.callbackNumber));
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", depositIndex=");
    stringBuilder2.append(this.depositIndex);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", hasUserDataHeader=");
    stringBuilder2.append(this.hasUserDataHeader);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", userData=");
    stringBuilder2.append(this.userData);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder1.append(" }");
    return stringBuilder1.toString();
  }
  
  private static void encodeMessageId(BearerData paramBearerData, BitwiseOutputStream paramBitwiseOutputStream) throws BitwiseOutputStream.AccessException {
    paramBitwiseOutputStream.write(8, 3);
    paramBitwiseOutputStream.write(4, paramBearerData.messageType);
    paramBitwiseOutputStream.write(8, paramBearerData.messageId >> 8);
    paramBitwiseOutputStream.write(8, paramBearerData.messageId);
    paramBitwiseOutputStream.write(1, paramBearerData.hasUserDataHeader);
    paramBitwiseOutputStream.skip(3);
  }
  
  private static int countAsciiSeptets(CharSequence paramCharSequence, boolean paramBoolean) {
    int i = paramCharSequence.length();
    if (paramBoolean)
      return i; 
    for (byte b = 0; b < i; b++) {
      if (UserData.charToAscii.get(paramCharSequence.charAt(b), -1) == -1)
        return -1; 
    } 
    return i;
  }
  
  public static GsmAlphabet.TextEncodingDetails calcTextEncodingDetails(CharSequence paramCharSequence, boolean paramBoolean1, boolean paramBoolean2) {
    GsmAlphabet.TextEncodingDetails textEncodingDetails;
    int i = countAsciiSeptets(paramCharSequence, paramBoolean1);
    if (i != -1 && i <= 160) {
      textEncodingDetails = new GsmAlphabet.TextEncodingDetails();
      textEncodingDetails.msgCount = 1;
      textEncodingDetails.codeUnitCount = i;
      textEncodingDetails.codeUnitsRemaining = 160 - i;
      textEncodingDetails.codeUnitSize = 1;
    } else {
      GsmAlphabet.TextEncodingDetails textEncodingDetails1 = SmsMessage.calculateLength(paramCharSequence, paramBoolean1);
      textEncodingDetails = textEncodingDetails1;
      if (textEncodingDetails1.msgCount == 1) {
        textEncodingDetails = textEncodingDetails1;
        if (textEncodingDetails1.codeUnitSize == 1) {
          textEncodingDetails = textEncodingDetails1;
          if (paramBoolean2)
            return SmsMessageBase.calcUnicodeEncodingDetails(paramCharSequence); 
        } 
      } 
    } 
    return textEncodingDetails;
  }
  
  private static byte[] encode7bitAscii(String paramString, boolean paramBoolean) throws CodingException {
    try {
      StringBuilder stringBuilder;
      BitwiseOutputStream bitwiseOutputStream = new BitwiseOutputStream();
      this(paramString.length());
      int i = paramString.length();
      for (byte b = 0; b < i; b++) {
        int j = UserData.charToAscii.get(paramString.charAt(b), -1);
        if (j == -1) {
          if (paramBoolean) {
            bitwiseOutputStream.write(7, 32);
          } else {
            CodingException codingException = new CodingException();
            stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("cannot ASCII encode (");
            stringBuilder.append(paramString.charAt(b));
            stringBuilder.append(")");
            this(stringBuilder.toString());
            throw codingException;
          } 
        } else {
          stringBuilder.write(7, j);
        } 
      } 
      return stringBuilder.toByteArray();
    } catch (com.android.internal.util.BitwiseOutputStream.AccessException accessException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("7bit ASCII encode failed: ");
      stringBuilder.append(accessException);
      throw new CodingException(stringBuilder.toString());
    } 
  }
  
  private static byte[] encodeUtf16(String paramString) throws CodingException {
    try {
      return paramString.getBytes("utf-16be");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("UTF-16 encode failed: ");
      stringBuilder.append(unsupportedEncodingException);
      throw new CodingException(stringBuilder.toString());
    } 
  }
  
  private static class Gsm7bitCodingResult {
    byte[] data;
    
    int septets;
    
    private Gsm7bitCodingResult() {}
  }
  
  private static Gsm7bitCodingResult encode7bitGsm(String paramString, int paramInt, boolean paramBoolean) throws CodingException {
    if (!paramBoolean) {
      paramBoolean = true;
    } else {
      paramBoolean = false;
    } 
    try {
      byte[] arrayOfByte = GsmAlphabet.stringToGsm7BitPacked(paramString, paramInt, paramBoolean, 0, 0);
      Gsm7bitCodingResult gsm7bitCodingResult = new Gsm7bitCodingResult();
      this();
      gsm7bitCodingResult.data = new byte[arrayOfByte.length - 1];
      System.arraycopy(arrayOfByte, 1, gsm7bitCodingResult.data, 0, arrayOfByte.length - 1);
      gsm7bitCodingResult.septets = arrayOfByte[0] & 0xFF;
      return gsm7bitCodingResult;
    } catch (EncodeException encodeException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("7bit GSM encode failed: ");
      stringBuilder.append(encodeException);
      throw new CodingException(stringBuilder.toString());
    } 
  }
  
  private static void encode7bitEms(UserData paramUserData, byte[] paramArrayOfbyte, boolean paramBoolean) throws CodingException {
    int i = paramArrayOfbyte.length;
    i = ((i + 1) * 8 + 6) / 7;
    Gsm7bitCodingResult gsm7bitCodingResult = encode7bitGsm(paramUserData.payloadStr, i, paramBoolean);
    paramUserData.msgEncoding = 9;
    paramUserData.msgEncodingSet = true;
    paramUserData.numFields = gsm7bitCodingResult.septets;
    paramUserData.payload = gsm7bitCodingResult.data;
    paramUserData.payload[0] = (byte)paramArrayOfbyte.length;
    System.arraycopy(paramArrayOfbyte, 0, paramUserData.payload, 1, paramArrayOfbyte.length);
  }
  
  private static void encode16bitEms(UserData paramUserData, byte[] paramArrayOfbyte) throws CodingException {
    byte[] arrayOfByte = encodeUtf16(paramUserData.payloadStr);
    int i = paramArrayOfbyte.length + 1;
    int j = (i + 1) / 2;
    int k = arrayOfByte.length / 2;
    paramUserData.msgEncoding = 4;
    paramUserData.msgEncodingSet = true;
    paramUserData.numFields = j + k;
    paramUserData.payload = new byte[paramUserData.numFields * 2];
    paramUserData.payload[0] = (byte)paramArrayOfbyte.length;
    System.arraycopy(paramArrayOfbyte, 0, paramUserData.payload, 1, paramArrayOfbyte.length);
    System.arraycopy(arrayOfByte, 0, paramUserData.payload, i, arrayOfByte.length);
  }
  
  private static void encode7bitAsciiEms(UserData paramUserData, byte[] paramArrayOfbyte, boolean paramBoolean) throws CodingException {
    try {
      CodingException codingException;
      StringBuilder stringBuilder;
      Rlog.d("BearerData", "encode7bitAsciiEms");
      int i = paramArrayOfbyte.length + 1;
      int j = (i * 8 + 6) / 7;
      int k = j * 7 - i * 8;
      String str = paramUserData.payloadStr;
      int m = str.length();
      BitwiseOutputStream bitwiseOutputStream = new BitwiseOutputStream();
      if (k > 0) {
        b = 1;
      } else {
        b = 0;
      } 
      this(b + m);
      bitwiseOutputStream.write(k, 0);
      for (byte b = 0; b < m; b++) {
        k = UserData.charToAscii.get(str.charAt(b), -1);
        if (k == -1) {
          if (paramBoolean) {
            bitwiseOutputStream.write(7, 32);
          } else {
            codingException = new CodingException();
            stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("cannot ASCII encode (");
            stringBuilder.append(str.charAt(b));
            stringBuilder.append(")");
            this(stringBuilder.toString());
            throw codingException;
          } 
        } else {
          bitwiseOutputStream.write(7, k);
        } 
      } 
      byte[] arrayOfByte = bitwiseOutputStream.toByteArray();
      ((UserData)codingException).msgEncoding = 2;
      ((UserData)codingException).msgEncodingSet = true;
      ((UserData)codingException).numFields = ((UserData)codingException).payloadStr.length() + j;
      ((UserData)codingException).payload = new byte[arrayOfByte.length + i];
      ((UserData)codingException).payload[0] = (byte)stringBuilder.length;
      System.arraycopy(stringBuilder, 0, ((UserData)codingException).payload, 1, stringBuilder.length);
      System.arraycopy(arrayOfByte, 0, ((UserData)codingException).payload, i, arrayOfByte.length);
      return;
    } catch (com.android.internal.util.BitwiseOutputStream.AccessException accessException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("7bit ASCII encode failed: ");
      stringBuilder.append(accessException);
      throw new CodingException(stringBuilder.toString());
    } 
  }
  
  private static void encodeEmsUserDataPayload(UserData paramUserData) throws CodingException {
    byte[] arrayOfByte = SmsHeader.toByteArray(paramUserData.userDataHeader);
    if (paramUserData.msgEncodingSet) {
      if (paramUserData.msgEncoding == 9) {
        encode7bitEms(paramUserData, arrayOfByte, true);
      } else if (paramUserData.msgEncoding == 4) {
        encode16bitEms(paramUserData, arrayOfByte);
      } else if (paramUserData.msgEncoding == 2) {
        encode7bitAsciiEms(paramUserData, arrayOfByte, true);
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("unsupported EMS user data encoding (");
        stringBuilder.append(paramUserData.msgEncoding);
        stringBuilder.append(")");
        throw new CodingException(stringBuilder.toString());
      } 
    } else {
      try {
        encode7bitEms(paramUserData, arrayOfByte, false);
      } catch (CodingException codingException) {
        encode16bitEms(paramUserData, arrayOfByte);
      } 
    } 
  }
  
  private static byte[] encodeShiftJis(String paramString) throws CodingException {
    try {
      return paramString.getBytes("Shift_JIS");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Shift-JIS encode failed: ");
      stringBuilder.append(unsupportedEncodingException);
      throw new CodingException(stringBuilder.toString());
    } 
  }
  
  private static void encodeUserDataPayload(UserData paramUserData) throws CodingException {
    if (paramUserData.payloadStr == null && paramUserData.msgEncoding != 0) {
      Rlog.e("BearerData", "user data with null payloadStr");
      paramUserData.payloadStr = "";
    } 
    if (paramUserData.userDataHeader != null) {
      encodeEmsUserDataPayload(paramUserData);
      return;
    } 
    if (paramUserData.msgEncodingSet) {
      if (paramUserData.msgEncoding == 0) {
        if (paramUserData.payload == null) {
          Rlog.e("BearerData", "user data with octet encoding but null payload");
          paramUserData.payload = new byte[0];
          paramUserData.numFields = 0;
        } else {
          paramUserData.numFields = paramUserData.payload.length;
        } 
      } else {
        if (paramUserData.payloadStr == null) {
          Rlog.e("BearerData", "non-octet user data with null payloadStr");
          paramUserData.payloadStr = "";
        } 
        if (paramUserData.msgEncoding == 9) {
          Gsm7bitCodingResult gsm7bitCodingResult = encode7bitGsm(paramUserData.payloadStr, 0, true);
          paramUserData.payload = gsm7bitCodingResult.data;
          paramUserData.numFields = gsm7bitCodingResult.septets;
        } else if (paramUserData.msgEncoding == 2) {
          paramUserData.payload = encode7bitAscii(paramUserData.payloadStr, true);
          paramUserData.numFields = paramUserData.payloadStr.length();
        } else if (paramUserData.msgEncoding == 4) {
          paramUserData.payload = encodeUtf16(paramUserData.payloadStr);
          paramUserData.numFields = paramUserData.payloadStr.length();
        } else if (paramUserData.msgEncoding == 5) {
          paramUserData.payload = encodeShiftJis(paramUserData.payloadStr);
          paramUserData.numFields = paramUserData.payload.length;
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("unsupported user data encoding (");
          stringBuilder.append(paramUserData.msgEncoding);
          stringBuilder.append(")");
          throw new CodingException(stringBuilder.toString());
        } 
      } 
    } else {
      try {
        paramUserData.payload = encode7bitAscii(paramUserData.payloadStr, false);
        paramUserData.msgEncoding = 2;
      } catch (CodingException codingException) {
        paramUserData.payload = encodeUtf16(paramUserData.payloadStr);
        paramUserData.msgEncoding = 4;
      } 
      paramUserData.numFields = paramUserData.payloadStr.length();
      paramUserData.msgEncodingSet = true;
    } 
  }
  
  private static void encodeUserData(BearerData paramBearerData, BitwiseOutputStream paramBitwiseOutputStream) throws BitwiseOutputStream.AccessException, CodingException {
    // Byte code:
    //   0: aload_0
    //   1: getfield userData : Lcom/android/internal/telephony/cdma/sms/UserData;
    //   4: invokestatic encodeUserDataPayload : (Lcom/android/internal/telephony/cdma/sms/UserData;)V
    //   7: aload_0
    //   8: getfield userData : Lcom/android/internal/telephony/cdma/sms/UserData;
    //   11: getfield userDataHeader : Lcom/android/internal/telephony/SmsHeader;
    //   14: ifnull -> 22
    //   17: iconst_1
    //   18: istore_2
    //   19: goto -> 24
    //   22: iconst_0
    //   23: istore_2
    //   24: aload_0
    //   25: iload_2
    //   26: putfield hasUserDataHeader : Z
    //   29: aload_0
    //   30: getfield userData : Lcom/android/internal/telephony/cdma/sms/UserData;
    //   33: getfield payload : [B
    //   36: arraylength
    //   37: sipush #140
    //   40: if_icmpgt -> 291
    //   43: aload_0
    //   44: getfield userData : Lcom/android/internal/telephony/cdma/sms/UserData;
    //   47: getfield msgEncoding : I
    //   50: iconst_2
    //   51: if_icmpne -> 85
    //   54: aload_0
    //   55: getfield userData : Lcom/android/internal/telephony/cdma/sms/UserData;
    //   58: astore_3
    //   59: aload_3
    //   60: aload_3
    //   61: getfield payload : [B
    //   64: arraylength
    //   65: bipush #8
    //   67: imul
    //   68: aload_0
    //   69: getfield userData : Lcom/android/internal/telephony/cdma/sms/UserData;
    //   72: getfield numFields : I
    //   75: bipush #7
    //   77: imul
    //   78: isub
    //   79: putfield paddingBits : I
    //   82: goto -> 93
    //   85: aload_0
    //   86: getfield userData : Lcom/android/internal/telephony/cdma/sms/UserData;
    //   89: iconst_0
    //   90: putfield paddingBits : I
    //   93: aload_0
    //   94: getfield userData : Lcom/android/internal/telephony/cdma/sms/UserData;
    //   97: getfield payload : [B
    //   100: arraylength
    //   101: bipush #8
    //   103: imul
    //   104: aload_0
    //   105: getfield userData : Lcom/android/internal/telephony/cdma/sms/UserData;
    //   108: getfield paddingBits : I
    //   111: isub
    //   112: istore #4
    //   114: iload #4
    //   116: bipush #13
    //   118: iadd
    //   119: istore #5
    //   121: aload_0
    //   122: getfield userData : Lcom/android/internal/telephony/cdma/sms/UserData;
    //   125: getfield msgEncoding : I
    //   128: iconst_1
    //   129: if_icmpeq -> 148
    //   132: iload #5
    //   134: istore #6
    //   136: aload_0
    //   137: getfield userData : Lcom/android/internal/telephony/cdma/sms/UserData;
    //   140: getfield msgEncoding : I
    //   143: bipush #10
    //   145: if_icmpne -> 155
    //   148: iload #5
    //   150: bipush #8
    //   152: iadd
    //   153: istore #6
    //   155: iload #6
    //   157: bipush #8
    //   159: idiv
    //   160: istore #7
    //   162: iload #6
    //   164: bipush #8
    //   166: irem
    //   167: ifle -> 176
    //   170: iconst_1
    //   171: istore #5
    //   173: goto -> 179
    //   176: iconst_0
    //   177: istore #5
    //   179: iload #7
    //   181: iload #5
    //   183: iadd
    //   184: istore #5
    //   186: iload #5
    //   188: bipush #8
    //   190: imul
    //   191: iload #6
    //   193: isub
    //   194: istore #6
    //   196: aload_1
    //   197: bipush #8
    //   199: iload #5
    //   201: invokevirtual write : (II)V
    //   204: aload_1
    //   205: iconst_5
    //   206: aload_0
    //   207: getfield userData : Lcom/android/internal/telephony/cdma/sms/UserData;
    //   210: getfield msgEncoding : I
    //   213: invokevirtual write : (II)V
    //   216: aload_0
    //   217: getfield userData : Lcom/android/internal/telephony/cdma/sms/UserData;
    //   220: getfield msgEncoding : I
    //   223: iconst_1
    //   224: if_icmpeq -> 239
    //   227: aload_0
    //   228: getfield userData : Lcom/android/internal/telephony/cdma/sms/UserData;
    //   231: getfield msgEncoding : I
    //   234: bipush #10
    //   236: if_icmpne -> 252
    //   239: aload_1
    //   240: bipush #8
    //   242: aload_0
    //   243: getfield userData : Lcom/android/internal/telephony/cdma/sms/UserData;
    //   246: getfield msgType : I
    //   249: invokevirtual write : (II)V
    //   252: aload_1
    //   253: bipush #8
    //   255: aload_0
    //   256: getfield userData : Lcom/android/internal/telephony/cdma/sms/UserData;
    //   259: getfield numFields : I
    //   262: invokevirtual write : (II)V
    //   265: aload_1
    //   266: iload #4
    //   268: aload_0
    //   269: getfield userData : Lcom/android/internal/telephony/cdma/sms/UserData;
    //   272: getfield payload : [B
    //   275: invokevirtual writeByteArray : (I[B)V
    //   278: iload #6
    //   280: ifle -> 290
    //   283: aload_1
    //   284: iload #6
    //   286: iconst_0
    //   287: invokevirtual write : (II)V
    //   290: return
    //   291: new java/lang/StringBuilder
    //   294: dup
    //   295: invokespecial <init> : ()V
    //   298: astore_1
    //   299: aload_1
    //   300: ldc_w 'encoded user data too large ('
    //   303: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   306: pop
    //   307: aload_1
    //   308: aload_0
    //   309: getfield userData : Lcom/android/internal/telephony/cdma/sms/UserData;
    //   312: getfield payload : [B
    //   315: arraylength
    //   316: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   319: pop
    //   320: aload_1
    //   321: ldc_w ' > '
    //   324: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   327: pop
    //   328: aload_1
    //   329: sipush #140
    //   332: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   335: pop
    //   336: aload_1
    //   337: ldc_w ' bytes)'
    //   340: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   343: pop
    //   344: new com/android/internal/telephony/cdma/sms/BearerData$CodingException
    //   347: dup
    //   348: aload_1
    //   349: invokevirtual toString : ()Ljava/lang/String;
    //   352: invokespecial <init> : (Ljava/lang/String;)V
    //   355: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #802	-> 0
    //   #803	-> 7
    //   #805	-> 29
    //   #811	-> 43
    //   #812	-> 54
    //   #815	-> 85
    //   #818	-> 93
    //   #819	-> 114
    //   #820	-> 121
    //   #822	-> 148
    //   #824	-> 155
    //   #825	-> 186
    //   #826	-> 196
    //   #827	-> 204
    //   #828	-> 216
    //   #830	-> 239
    //   #832	-> 252
    //   #833	-> 265
    //   #834	-> 278
    //   #835	-> 290
    //   #806	-> 291
  }
  
  private static void encodeReplyOption(BearerData paramBearerData, BitwiseOutputStream paramBitwiseOutputStream) throws BitwiseOutputStream.AccessException {
    paramBitwiseOutputStream.write(8, 1);
    paramBitwiseOutputStream.write(1, paramBearerData.userAckReq);
    paramBitwiseOutputStream.write(1, paramBearerData.deliveryAckReq);
    paramBitwiseOutputStream.write(1, paramBearerData.readAckReq);
    paramBitwiseOutputStream.write(1, paramBearerData.reportReq);
    paramBitwiseOutputStream.write(4, 0);
  }
  
  private static byte[] encodeDtmfSmsAddress(String paramString) {
    int i = paramString.length();
    int j = i * 4;
    int k = j / 8;
    if (j % 8 > 0) {
      j = 1;
    } else {
      j = 0;
    } 
    byte[] arrayOfByte = new byte[k + j];
    for (k = 0; k < i; k++) {
      j = paramString.charAt(k);
      if (j >= 49 && j <= 57) {
        j -= 48;
      } else if (j == 48) {
        j = 10;
      } else if (j == 42) {
        j = 11;
      } else if (j == 35) {
        j = 12;
      } else {
        return null;
      } 
      int m = k / 2;
      arrayOfByte[m] = (byte)(arrayOfByte[m] | j << 4 - k % 2 * 4);
    } 
    return arrayOfByte;
  }
  
  private static void encodeCdmaSmsAddress(CdmaSmsAddress paramCdmaSmsAddress) throws CodingException {
    if (paramCdmaSmsAddress.digitMode == 1) {
      try {
        paramCdmaSmsAddress.origBytes = paramCdmaSmsAddress.address.getBytes("US-ASCII");
      } catch (UnsupportedEncodingException unsupportedEncodingException) {
        throw new CodingException("invalid SMS address, cannot convert to ASCII");
      } 
    } else {
      ((CdmaSmsAddress)unsupportedEncodingException).origBytes = encodeDtmfSmsAddress(((CdmaSmsAddress)unsupportedEncodingException).address);
    } 
  }
  
  private static void encodeCallbackNumber(BearerData paramBearerData, BitwiseOutputStream paramBitwiseOutputStream) throws BitwiseOutputStream.AccessException, CodingException {
    int j;
    CdmaSmsAddress cdmaSmsAddress = paramBearerData.callbackNumber;
    encodeCdmaSmsAddress(cdmaSmsAddress);
    int i = 9;
    if (cdmaSmsAddress.digitMode == 1) {
      i = 9 + 7;
      j = cdmaSmsAddress.numberOfDigits * 8;
    } else {
      j = cdmaSmsAddress.numberOfDigits * 4;
    } 
    int k = i + j;
    int m = k / 8;
    if (k % 8 > 0) {
      i = 1;
    } else {
      i = 0;
    } 
    i = m + i;
    k = i * 8 - k;
    paramBitwiseOutputStream.write(8, i);
    paramBitwiseOutputStream.write(1, cdmaSmsAddress.digitMode);
    if (cdmaSmsAddress.digitMode == 1) {
      paramBitwiseOutputStream.write(3, cdmaSmsAddress.ton);
      paramBitwiseOutputStream.write(4, cdmaSmsAddress.numberPlan);
    } 
    paramBitwiseOutputStream.write(8, cdmaSmsAddress.numberOfDigits);
    paramBitwiseOutputStream.writeByteArray(j, cdmaSmsAddress.origBytes);
    if (k > 0)
      paramBitwiseOutputStream.write(k, 0); 
  }
  
  private static void encodeMsgStatus(BearerData paramBearerData, BitwiseOutputStream paramBitwiseOutputStream) throws BitwiseOutputStream.AccessException {
    paramBitwiseOutputStream.write(8, 1);
    paramBitwiseOutputStream.write(2, paramBearerData.errorClass);
    paramBitwiseOutputStream.write(6, paramBearerData.messageStatus);
  }
  
  private static void encodeMsgCount(BearerData paramBearerData, BitwiseOutputStream paramBitwiseOutputStream) throws BitwiseOutputStream.AccessException {
    paramBitwiseOutputStream.write(8, 1);
    paramBitwiseOutputStream.write(8, paramBearerData.numberOfMessages);
  }
  
  private static void encodeValidityPeriodRel(BearerData paramBearerData, BitwiseOutputStream paramBitwiseOutputStream) throws BitwiseOutputStream.AccessException {
    paramBitwiseOutputStream.write(8, 1);
    paramBitwiseOutputStream.write(8, paramBearerData.validityPeriodRelative);
  }
  
  private static void encodePrivacyIndicator(BearerData paramBearerData, BitwiseOutputStream paramBitwiseOutputStream) throws BitwiseOutputStream.AccessException {
    paramBitwiseOutputStream.write(8, 1);
    paramBitwiseOutputStream.write(2, paramBearerData.privacy);
    paramBitwiseOutputStream.skip(6);
  }
  
  private static void encodeLanguageIndicator(BearerData paramBearerData, BitwiseOutputStream paramBitwiseOutputStream) throws BitwiseOutputStream.AccessException {
    paramBitwiseOutputStream.write(8, 1);
    paramBitwiseOutputStream.write(8, paramBearerData.language);
  }
  
  private static void encodeDisplayMode(BearerData paramBearerData, BitwiseOutputStream paramBitwiseOutputStream) throws BitwiseOutputStream.AccessException {
    paramBitwiseOutputStream.write(8, 1);
    paramBitwiseOutputStream.write(2, paramBearerData.displayMode);
    paramBitwiseOutputStream.skip(6);
  }
  
  private static void encodePriorityIndicator(BearerData paramBearerData, BitwiseOutputStream paramBitwiseOutputStream) throws BitwiseOutputStream.AccessException {
    paramBitwiseOutputStream.write(8, 1);
    paramBitwiseOutputStream.write(2, paramBearerData.priority);
    paramBitwiseOutputStream.skip(6);
  }
  
  private static void encodeMsgDeliveryAlert(BearerData paramBearerData, BitwiseOutputStream paramBitwiseOutputStream) throws BitwiseOutputStream.AccessException {
    paramBitwiseOutputStream.write(8, 1);
    paramBitwiseOutputStream.write(2, paramBearerData.alert);
    paramBitwiseOutputStream.skip(6);
  }
  
  private static void encodeScpResults(BearerData paramBearerData, BitwiseOutputStream paramBitwiseOutputStream) throws BitwiseOutputStream.AccessException {
    ArrayList<CdmaSmsCbProgramResults> arrayList = paramBearerData.serviceCategoryProgramResults;
    paramBitwiseOutputStream.write(8, arrayList.size() * 4);
    for (CdmaSmsCbProgramResults cdmaSmsCbProgramResults : arrayList) {
      int i = cdmaSmsCbProgramResults.getCategory();
      paramBitwiseOutputStream.write(8, i >> 8);
      paramBitwiseOutputStream.write(8, i);
      paramBitwiseOutputStream.write(8, cdmaSmsCbProgramResults.getLanguage());
      paramBitwiseOutputStream.write(4, cdmaSmsCbProgramResults.getCategoryResult());
      paramBitwiseOutputStream.skip(4);
    } 
  }
  
  private static void encodeMsgCenterTimeStamp(BearerData paramBearerData, BitwiseOutputStream paramBitwiseOutputStream) throws BitwiseOutputStream.AccessException {
    paramBitwiseOutputStream.write(8, 6);
    paramBitwiseOutputStream.writeByteArray(48, paramBearerData.msgCenterTimeStamp.toByteArray());
  }
  
  public static byte[] encode(BearerData paramBearerData) {
    boolean bool;
    UserData userData = paramBearerData.userData;
    if (userData != null && userData.userDataHeader != null) {
      bool = true;
    } else {
      bool = false;
    } 
    paramBearerData.hasUserDataHeader = bool;
    try {
      BitwiseOutputStream bitwiseOutputStream = new BitwiseOutputStream();
      this(200);
      bitwiseOutputStream.write(8, 0);
      encodeMessageId(paramBearerData, bitwiseOutputStream);
      if (paramBearerData.userData != null) {
        bitwiseOutputStream.write(8, 1);
        encodeUserData(paramBearerData, bitwiseOutputStream);
      } 
      if (paramBearerData.callbackNumber != null) {
        bitwiseOutputStream.write(8, 14);
        encodeCallbackNumber(paramBearerData, bitwiseOutputStream);
      } 
      if (paramBearerData.userAckReq || paramBearerData.deliveryAckReq || paramBearerData.readAckReq || paramBearerData.reportReq) {
        bitwiseOutputStream.write(8, 10);
        encodeReplyOption(paramBearerData, bitwiseOutputStream);
      } 
      if (paramBearerData.numberOfMessages != 0) {
        bitwiseOutputStream.write(8, 11);
        encodeMsgCount(paramBearerData, bitwiseOutputStream);
      } 
      if (paramBearerData.validityPeriodRelativeSet) {
        bitwiseOutputStream.write(8, 5);
        encodeValidityPeriodRel(paramBearerData, bitwiseOutputStream);
      } 
      if (paramBearerData.privacyIndicatorSet) {
        bitwiseOutputStream.write(8, 9);
        encodePrivacyIndicator(paramBearerData, bitwiseOutputStream);
      } 
      if (paramBearerData.languageIndicatorSet) {
        bitwiseOutputStream.write(8, 13);
        encodeLanguageIndicator(paramBearerData, bitwiseOutputStream);
      } 
      if (paramBearerData.displayModeSet) {
        bitwiseOutputStream.write(8, 15);
        encodeDisplayMode(paramBearerData, bitwiseOutputStream);
      } 
      if (paramBearerData.priorityIndicatorSet) {
        bitwiseOutputStream.write(8, 8);
        encodePriorityIndicator(paramBearerData, bitwiseOutputStream);
      } 
      if (paramBearerData.alertIndicatorSet) {
        bitwiseOutputStream.write(8, 12);
        encodeMsgDeliveryAlert(paramBearerData, bitwiseOutputStream);
      } 
      if (paramBearerData.messageStatusSet) {
        bitwiseOutputStream.write(8, 20);
        encodeMsgStatus(paramBearerData, bitwiseOutputStream);
      } 
      if (paramBearerData.serviceCategoryProgramResults != null) {
        bitwiseOutputStream.write(8, 19);
        encodeScpResults(paramBearerData, bitwiseOutputStream);
      } 
      if (paramBearerData.msgCenterTimeStamp != null) {
        bitwiseOutputStream.write(8, 3);
        encodeMsgCenterTimeStamp(paramBearerData, bitwiseOutputStream);
      } 
      return bitwiseOutputStream.toByteArray();
    } catch (com.android.internal.util.BitwiseOutputStream.AccessException accessException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("BearerData encode failed: ");
      stringBuilder.append(accessException);
      Rlog.e("BearerData", stringBuilder.toString());
    } catch (CodingException codingException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("BearerData encode failed: ");
      stringBuilder.append(codingException);
      Rlog.e("BearerData", stringBuilder.toString());
    } 
    return null;
  }
  
  private static boolean decodeMessageId(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream) throws BitwiseInputStream.AccessException {
    boolean bool = false;
    int i = paramBitwiseInputStream.read(8) * 8;
    int j = i;
    if (i >= 24) {
      j = i - 24;
      boolean bool1 = true;
      paramBearerData.messageType = paramBitwiseInputStream.read(4);
      paramBearerData.messageId = i = paramBitwiseInputStream.read(8) << 8;
      paramBearerData.messageId = paramBitwiseInputStream.read(8) | i;
      bool = true;
      if (paramBitwiseInputStream.read(1) != 1)
        bool = false; 
      paramBearerData.hasUserDataHeader = bool;
      paramBitwiseInputStream.skip(3);
      bool = bool1;
    } 
    if (!bool || j > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("MESSAGE_IDENTIFIER decode ");
      if (bool) {
        str = "succeeded";
      } else {
        str = "failed";
      } 
      stringBuilder.append(str);
      stringBuilder.append(" (extra bits = ");
      stringBuilder.append(j);
      stringBuilder.append(")");
      String str = stringBuilder.toString();
      Rlog.d("BearerData", str);
    } 
    paramBitwiseInputStream.skip(j);
    return bool;
  }
  
  private static boolean decodeReserved(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream, int paramInt) throws BitwiseInputStream.AccessException, CodingException {
    boolean bool = false;
    int i = paramBitwiseInputStream.read(8);
    int j = i * 8;
    if (j <= paramBitwiseInputStream.available()) {
      bool = true;
      paramBitwiseInputStream.skip(j);
    } 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("RESERVED bearer data subparameter ");
    stringBuilder2.append(paramInt);
    stringBuilder2.append(" decode ");
    if (bool) {
      str = "succeeded";
    } else {
      str = "failed";
    } 
    stringBuilder2.append(str);
    stringBuilder2.append(" (param bits = ");
    stringBuilder2.append(j);
    stringBuilder2.append(")");
    String str = stringBuilder2.toString();
    Rlog.d("BearerData", str);
    if (bool)
      return bool; 
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("RESERVED bearer data subparameter ");
    stringBuilder1.append(paramInt);
    stringBuilder1.append(" had invalid SUBPARAM_LEN ");
    stringBuilder1.append(i);
    throw new CodingException(stringBuilder1.toString());
  }
  
  private static boolean decodeUserData(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream) throws BitwiseInputStream.AccessException {
    int i = paramBitwiseInputStream.read(8);
    UserData userData = new UserData();
    userData.msgEncoding = paramBitwiseInputStream.read(5);
    paramBearerData.userData.msgEncodingSet = true;
    paramBearerData.userData.msgType = 0;
    int j = 5;
    if (paramBearerData.userData.msgEncoding == 1 || paramBearerData.userData.msgEncoding == 10) {
      paramBearerData.userData.msgType = paramBitwiseInputStream.read(8);
      j = 5 + 8;
    } 
    paramBearerData.userData.numFields = paramBitwiseInputStream.read(8);
    paramBearerData.userData.payload = paramBitwiseInputStream.readByteArray(i * 8 - j + 8);
    return true;
  }
  
  private static String decodeUtf8(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws CodingException {
    return decodeCharset(paramArrayOfbyte, paramInt1, paramInt2, 1, "UTF-8");
  }
  
  private static String decodeUtf16(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws CodingException {
    int i = (paramInt1 + paramInt1 % 2) / 2;
    return decodeCharset(paramArrayOfbyte, paramInt1, paramInt2 - i, 2, "utf-16be");
  }
  
  private static String decodeCharset(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3, String paramString) throws CodingException {
    // Byte code:
    //   0: iload_2
    //   1: iflt -> 17
    //   4: iload_2
    //   5: istore #5
    //   7: iload_2
    //   8: iload_3
    //   9: imul
    //   10: iload_1
    //   11: iadd
    //   12: aload_0
    //   13: arraylength
    //   14: if_icmple -> 128
    //   17: aload_0
    //   18: arraylength
    //   19: iload_1
    //   20: isub
    //   21: iload_1
    //   22: iload_3
    //   23: irem
    //   24: isub
    //   25: iload_3
    //   26: idiv
    //   27: istore #5
    //   29: iload #5
    //   31: iflt -> 193
    //   34: new java/lang/StringBuilder
    //   37: dup
    //   38: invokespecial <init> : ()V
    //   41: astore #6
    //   43: aload #6
    //   45: aload #4
    //   47: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   50: pop
    //   51: aload #6
    //   53: ldc_w ' decode error: offset = '
    //   56: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   59: pop
    //   60: aload #6
    //   62: iload_1
    //   63: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   66: pop
    //   67: aload #6
    //   69: ldc_w ' numFields = '
    //   72: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   75: pop
    //   76: aload #6
    //   78: iload_2
    //   79: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   82: pop
    //   83: aload #6
    //   85: ldc_w ' data.length = '
    //   88: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   91: pop
    //   92: aload #6
    //   94: aload_0
    //   95: arraylength
    //   96: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   99: pop
    //   100: aload #6
    //   102: ldc_w ' maxNumFields = '
    //   105: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   108: pop
    //   109: aload #6
    //   111: iload #5
    //   113: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   116: pop
    //   117: ldc 'BearerData'
    //   119: aload #6
    //   121: invokevirtual toString : ()Ljava/lang/String;
    //   124: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   127: pop
    //   128: new java/lang/String
    //   131: dup
    //   132: aload_0
    //   133: iload_1
    //   134: iload #5
    //   136: iload_3
    //   137: imul
    //   138: aload #4
    //   140: invokespecial <init> : ([BIILjava/lang/String;)V
    //   143: astore_0
    //   144: aload_0
    //   145: areturn
    //   146: astore_0
    //   147: new java/lang/StringBuilder
    //   150: dup
    //   151: invokespecial <init> : ()V
    //   154: astore #6
    //   156: aload #6
    //   158: aload #4
    //   160: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   163: pop
    //   164: aload #6
    //   166: ldc_w ' decode failed: '
    //   169: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   172: pop
    //   173: aload #6
    //   175: aload_0
    //   176: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   179: pop
    //   180: new com/android/internal/telephony/cdma/sms/BearerData$CodingException
    //   183: dup
    //   184: aload #6
    //   186: invokevirtual toString : ()Ljava/lang/String;
    //   189: invokespecial <init> : (Ljava/lang/String;)V
    //   192: athrow
    //   193: new java/lang/StringBuilder
    //   196: dup
    //   197: invokespecial <init> : ()V
    //   200: astore_0
    //   201: aload_0
    //   202: aload #4
    //   204: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   207: pop
    //   208: aload_0
    //   209: ldc_w ' decode failed: offset out of range'
    //   212: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   215: pop
    //   216: new com/android/internal/telephony/cdma/sms/BearerData$CodingException
    //   219: dup
    //   220: aload_0
    //   221: invokevirtual toString : ()Ljava/lang/String;
    //   224: invokespecial <init> : (Ljava/lang/String;)V
    //   227: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1154	-> 0
    //   #1156	-> 17
    //   #1157	-> 17
    //   #1158	-> 29
    //   #1161	-> 34
    //   #1164	-> 128
    //   #1167	-> 128
    //   #1168	-> 146
    //   #1169	-> 147
    //   #1159	-> 193
    // Exception table:
    //   from	to	target	type
    //   128	144	146	java/io/UnsupportedEncodingException
  }
  
  private static String decode7bitAscii(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws CodingException {
    try {
      paramInt1 = (paramInt1 * 8 + 6) / 7;
      paramInt2 -= paramInt1;
      StringBuffer stringBuffer = new StringBuffer();
      this(paramInt2);
      BitwiseInputStream bitwiseInputStream = new BitwiseInputStream();
      this(paramArrayOfbyte);
      int i = paramInt1 * 7 + paramInt2 * 7;
      if (bitwiseInputStream.available() >= i) {
        bitwiseInputStream.skip(paramInt1 * 7);
        for (paramInt1 = 0; paramInt1 < paramInt2; paramInt1++) {
          i = bitwiseInputStream.read(7);
          if (i >= 32 && i <= UserData.ASCII_MAP_MAX_INDEX) {
            stringBuffer.append(UserData.ASCII_MAP[i - 32]);
          } else if (i == 10) {
            stringBuffer.append('\n');
          } else if (i == 13) {
            stringBuffer.append('\r');
          } else {
            stringBuffer.append(' ');
          } 
        } 
        return stringBuffer.toString();
      } 
      CodingException codingException = new CodingException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("insufficient data (wanted ");
      stringBuilder.append(i);
      stringBuilder.append(" bits, but only have ");
      stringBuilder.append(bitwiseInputStream.available());
      stringBuilder.append(")");
      this(stringBuilder.toString());
      throw codingException;
    } catch (com.android.internal.util.BitwiseInputStream.AccessException accessException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("7bit ASCII decode failed: ");
      stringBuilder.append(accessException);
      throw new CodingException(stringBuilder.toString());
    } 
  }
  
  private static String decode7bitGsm(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws CodingException {
    int i = paramInt1 * 8;
    int j = (i + 6) / 7;
    String str = GsmAlphabet.gsm7BitPackedToString(paramArrayOfbyte, paramInt1, paramInt2 - j, j * 7 - i, 0, 0);
    if (str != null)
      return str; 
    throw new CodingException("7bit GSM decoding failed");
  }
  
  private static String decodeLatin(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws CodingException {
    return decodeCharset(paramArrayOfbyte, paramInt1, paramInt2, 1, "ISO-8859-1");
  }
  
  private static String decodeShiftJis(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws CodingException {
    return decodeCharset(paramArrayOfbyte, paramInt1, paramInt2, 1, "Shift_JIS");
  }
  
  private static String decodeGsmDcs(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3) throws CodingException {
    if ((paramInt3 & 0xC0) == 0) {
      StringBuilder stringBuilder1;
      int i = paramInt3 >> 2 & 0x3;
      if (i != 0) {
        if (i != 1) {
          if (i == 2)
            return decodeUtf16(paramArrayOfbyte, paramInt1, paramInt2); 
          stringBuilder1 = new StringBuilder();
          stringBuilder1.append("unsupported user msgType encoding (");
          stringBuilder1.append(paramInt3);
          stringBuilder1.append(")");
          throw new CodingException(stringBuilder1.toString());
        } 
        return decodeUtf8((byte[])stringBuilder1, paramInt1, paramInt2);
      } 
      return decode7bitGsm((byte[])stringBuilder1, paramInt1, paramInt2);
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("unsupported coding group (");
    stringBuilder.append(paramInt3);
    stringBuilder.append(")");
    throw new CodingException(stringBuilder.toString());
  }
  
  private static void decodeUserDataPayload(UserData paramUserData, boolean paramBoolean) throws CodingException {
    int i = 0;
    if (paramBoolean) {
      int k = paramUserData.payload[0] & 0xFF;
      i = 0 + k + 1;
      byte[] arrayOfByte = new byte[k];
      System.arraycopy(paramUserData.payload, 1, arrayOfByte, 0, k);
      paramUserData.userDataHeader = SmsHeader.fromByteArray(arrayOfByte);
    } 
    int j = paramUserData.msgEncoding;
    if (j != 0) {
      if (j != 2 && j != 3) {
        if (j != 4) {
          if (j != 5) {
            StringBuilder stringBuilder;
            switch (j) {
              default:
                stringBuilder = new StringBuilder();
                stringBuilder.append("unsupported user data encoding (");
                stringBuilder.append(paramUserData.msgEncoding);
                stringBuilder.append(")");
                throw new CodingException(stringBuilder.toString());
              case 10:
                paramUserData.payloadStr = decodeGsmDcs(paramUserData.payload, i, paramUserData.numFields, paramUserData.msgType);
                return;
              case 9:
                paramUserData.payloadStr = decode7bitGsm(paramUserData.payload, i, paramUserData.numFields);
                return;
              case 8:
                break;
            } 
            paramUserData.payloadStr = decodeLatin(paramUserData.payload, i, paramUserData.numFields);
          } else {
            paramUserData.payloadStr = decodeShiftJis(paramUserData.payload, i, paramUserData.numFields);
          } 
        } else {
          paramUserData.payloadStr = decodeUtf16(paramUserData.payload, i, paramUserData.numFields);
        } 
      } else {
        paramUserData.payloadStr = decode7bitAscii(paramUserData.payload, i, paramUserData.numFields);
      } 
    } else {
      Resources resources = Resources.getSystem();
      paramBoolean = resources.getBoolean(17891538);
      byte[] arrayOfByte = new byte[paramUserData.numFields];
      if (paramUserData.numFields < paramUserData.payload.length) {
        j = paramUserData.numFields;
      } else {
        j = paramUserData.payload.length;
      } 
      System.arraycopy(paramUserData.payload, 0, arrayOfByte, 0, j);
      paramUserData.payload = arrayOfByte;
      if (!paramBoolean) {
        paramUserData.payloadStr = decodeLatin(paramUserData.payload, i, paramUserData.numFields);
      } else {
        paramUserData.payloadStr = decodeUtf8(paramUserData.payload, i, paramUserData.numFields);
      } 
    } 
  }
  
  private static void decodeIs91VoicemailStatus(BearerData paramBearerData) throws BitwiseInputStream.AccessException, CodingException {
    BitwiseInputStream bitwiseInputStream = new BitwiseInputStream(paramBearerData.userData.payload);
    int i = bitwiseInputStream.available() / 6;
    int j = paramBearerData.userData.numFields;
    if (i <= 14 && i >= 3 && i >= j)
      try {
        CodingException codingException;
        StringBuilder stringBuilder;
        StringBuffer stringBuffer = new StringBuffer();
        this(i);
        while (bitwiseInputStream.available() >= 6)
          stringBuffer.append(UserData.ASCII_MAP[bitwiseInputStream.read(6)]); 
        String str = stringBuffer.toString();
        paramBearerData.numberOfMessages = Integer.parseInt(str.substring(0, 2));
        char c = str.charAt(2);
        if (c == ' ') {
          paramBearerData.priority = 0;
        } else if (c == '!') {
          paramBearerData.priority = 2;
        } else {
          codingException = new CodingException();
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("IS-91 voicemail status decoding failed: illegal priority setting (");
          stringBuilder.append(c);
          stringBuilder.append(")");
          this(stringBuilder.toString());
          throw codingException;
        } 
        ((BearerData)codingException).priorityIndicatorSet = true;
        ((BearerData)codingException).userData.payloadStr = stringBuilder.substring(3, j - 3);
        return;
      } catch (NumberFormatException numberFormatException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("IS-91 voicemail status decoding failed: ");
        stringBuilder.append(numberFormatException);
        throw new CodingException(stringBuilder.toString());
      } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("IS-91 voicemail status decoding failed: ");
        stringBuilder.append(indexOutOfBoundsException);
        throw new CodingException(stringBuilder.toString());
      }  
    throw new CodingException("IS-91 voicemail status decoding failed");
  }
  
  private static void decodeIs91ShortMessage(BearerData paramBearerData) throws BitwiseInputStream.AccessException, CodingException {
    BitwiseInputStream bitwiseInputStream = new BitwiseInputStream(paramBearerData.userData.payload);
    int i = bitwiseInputStream.available() / 6;
    int j = paramBearerData.userData.numFields;
    if (j <= 14 && i >= j) {
      StringBuffer stringBuffer = new StringBuffer(i);
      for (i = 0; i < j; i++)
        stringBuffer.append(UserData.ASCII_MAP[bitwiseInputStream.read(6)]); 
      paramBearerData.userData.payloadStr = stringBuffer.toString();
      return;
    } 
    throw new CodingException("IS-91 short message decoding failed");
  }
  
  private static void decodeIs91Cli(BearerData paramBearerData) throws CodingException {
    BitwiseInputStream bitwiseInputStream = new BitwiseInputStream(paramBearerData.userData.payload);
    int i = bitwiseInputStream.available() / 4;
    int j = paramBearerData.userData.numFields;
    if (i <= 14 && i >= 3 && i >= j) {
      CdmaSmsAddress cdmaSmsAddress = new CdmaSmsAddress();
      cdmaSmsAddress.digitMode = 0;
      cdmaSmsAddress.origBytes = paramBearerData.userData.payload;
      cdmaSmsAddress.numberOfDigits = (byte)j;
      decodeSmsAddress(cdmaSmsAddress);
      paramBearerData.callbackNumber = cdmaSmsAddress;
      return;
    } 
    throw new CodingException("IS-91 voicemail status decoding failed");
  }
  
  private static void decodeIs91(BearerData paramBearerData) throws BitwiseInputStream.AccessException, CodingException {
    StringBuilder stringBuilder;
    switch (paramBearerData.userData.msgType) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("unsupported IS-91 message type (");
        stringBuilder.append(paramBearerData.userData.msgType);
        stringBuilder.append(")");
        throw new CodingException(stringBuilder.toString());
      case 132:
        decodeIs91Cli(paramBearerData);
        return;
      case 131:
      case 133:
        decodeIs91ShortMessage(paramBearerData);
        return;
      case 130:
        break;
    } 
    decodeIs91VoicemailStatus(paramBearerData);
  }
  
  private static boolean decodeReplyOption(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream) throws BitwiseInputStream.AccessException {
    boolean bool = false;
    int i = paramBitwiseInputStream.read(8) * 8;
    int j = i;
    if (i >= 8) {
      j = i - 8;
      boolean bool1 = true;
      boolean bool2 = true;
      if (paramBitwiseInputStream.read(1) == 1) {
        bool = true;
      } else {
        bool = false;
      } 
      paramBearerData.userAckReq = bool;
      if (paramBitwiseInputStream.read(1) == 1) {
        bool = true;
      } else {
        bool = false;
      } 
      paramBearerData.deliveryAckReq = bool;
      if (paramBitwiseInputStream.read(1) == 1) {
        bool = true;
      } else {
        bool = false;
      } 
      paramBearerData.readAckReq = bool;
      if (paramBitwiseInputStream.read(1) == 1) {
        bool = bool2;
      } else {
        bool = false;
      } 
      paramBearerData.reportReq = bool;
      paramBitwiseInputStream.skip(4);
      bool = bool1;
    } 
    if (!bool || j > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("REPLY_OPTION decode ");
      if (bool) {
        str = "succeeded";
      } else {
        str = "failed";
      } 
      stringBuilder.append(str);
      stringBuilder.append(" (extra bits = ");
      stringBuilder.append(j);
      stringBuilder.append(")");
      String str = stringBuilder.toString();
      Rlog.d("BearerData", str);
    } 
    paramBitwiseInputStream.skip(j);
    return bool;
  }
  
  private static boolean decodeMsgCount(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream) throws BitwiseInputStream.AccessException {
    boolean bool = false;
    int i = paramBitwiseInputStream.read(8) * 8;
    int j = i;
    if (i >= 8) {
      j = i - 8;
      bool = true;
      paramBearerData.numberOfMessages = IccUtils.cdmaBcdByteToInt((byte)paramBitwiseInputStream.read(8));
    } 
    if (!bool || j > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("NUMBER_OF_MESSAGES decode ");
      if (bool) {
        str = "succeeded";
      } else {
        str = "failed";
      } 
      stringBuilder.append(str);
      stringBuilder.append(" (extra bits = ");
      stringBuilder.append(j);
      stringBuilder.append(")");
      String str = stringBuilder.toString();
      Rlog.d("BearerData", str);
    } 
    paramBitwiseInputStream.skip(j);
    return bool;
  }
  
  private static boolean decodeDepositIndex(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream) throws BitwiseInputStream.AccessException {
    boolean bool = false;
    int i = paramBitwiseInputStream.read(8) * 8;
    int j = i;
    if (i >= 16) {
      j = i - 16;
      bool = true;
      i = paramBitwiseInputStream.read(8);
      paramBearerData.depositIndex = paramBitwiseInputStream.read(8) | i << 8;
    } 
    if (!bool || j > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("MESSAGE_DEPOSIT_INDEX decode ");
      if (bool) {
        str = "succeeded";
      } else {
        str = "failed";
      } 
      stringBuilder.append(str);
      stringBuilder.append(" (extra bits = ");
      stringBuilder.append(j);
      stringBuilder.append(")");
      String str = stringBuilder.toString();
      Rlog.d("BearerData", str);
    } 
    paramBitwiseInputStream.skip(j);
    return bool;
  }
  
  private static String decodeDtmfSmsAddress(byte[] paramArrayOfbyte, int paramInt) throws CodingException {
    StringBuffer stringBuffer = new StringBuffer(paramInt);
    for (byte b = 0; b < paramInt; b++) {
      int i = paramArrayOfbyte[b / 2] >>> 4 - b % 2 * 4 & 0xF;
      if (i >= 1 && i <= 9) {
        stringBuffer.append(Integer.toString(i, 10));
      } else if (i == 10) {
        stringBuffer.append('0');
      } else if (i == 11) {
        stringBuffer.append('*');
      } else if (i == 12) {
        stringBuffer.append('#');
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("invalid SMS address DTMF code (");
        stringBuilder.append(i);
        stringBuilder.append(")");
        throw new CodingException(stringBuilder.toString());
      } 
    } 
    return stringBuffer.toString();
  }
  
  private static void decodeSmsAddress(CdmaSmsAddress paramCdmaSmsAddress) throws CodingException {
    if (paramCdmaSmsAddress.digitMode == 1) {
      try {
        String str = new String();
        this(paramCdmaSmsAddress.origBytes, 0, paramCdmaSmsAddress.origBytes.length, "US-ASCII");
        paramCdmaSmsAddress.address = str;
      } catch (UnsupportedEncodingException unsupportedEncodingException) {
        throw new CodingException("invalid SMS address ASCII code");
      } 
    } else {
      ((CdmaSmsAddress)unsupportedEncodingException).address = decodeDtmfSmsAddress(((CdmaSmsAddress)unsupportedEncodingException).origBytes, ((CdmaSmsAddress)unsupportedEncodingException).numberOfDigits);
    } 
  }
  
  private static boolean decodeCallbackNumber(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream) throws BitwiseInputStream.AccessException, CodingException {
    int i = paramBitwiseInputStream.read(8) * 8;
    if (i < 8) {
      paramBitwiseInputStream.skip(i);
      return false;
    } 
    CdmaSmsAddress cdmaSmsAddress = new CdmaSmsAddress();
    cdmaSmsAddress.digitMode = paramBitwiseInputStream.read(1);
    int j = 4;
    int k = 1;
    if (cdmaSmsAddress.digitMode == 1) {
      cdmaSmsAddress.ton = paramBitwiseInputStream.read(3);
      cdmaSmsAddress.numberPlan = paramBitwiseInputStream.read(4);
      j = 8;
      k = (byte)(1 + 7);
    } 
    cdmaSmsAddress.numberOfDigits = paramBitwiseInputStream.read(8);
    k = (byte)(k + 8);
    k = i - k;
    j = cdmaSmsAddress.numberOfDigits * j;
    i = k - j;
    if (k >= j) {
      cdmaSmsAddress.origBytes = paramBitwiseInputStream.readByteArray(j);
      paramBitwiseInputStream.skip(i);
      decodeSmsAddress(cdmaSmsAddress);
      paramBearerData.callbackNumber = cdmaSmsAddress;
      return true;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CALLBACK_NUMBER subparam encoding size error (remainingBits + ");
    stringBuilder.append(k);
    stringBuilder.append(", dataBits + ");
    stringBuilder.append(j);
    stringBuilder.append(", paddingBits + ");
    stringBuilder.append(i);
    stringBuilder.append(")");
    throw new CodingException(stringBuilder.toString());
  }
  
  private static boolean decodeMsgStatus(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream) throws BitwiseInputStream.AccessException {
    boolean bool = false;
    int i = paramBitwiseInputStream.read(8) * 8;
    int j = i;
    if (i >= 8) {
      j = i - 8;
      bool = true;
      paramBearerData.errorClass = paramBitwiseInputStream.read(2);
      paramBearerData.messageStatus = paramBitwiseInputStream.read(6);
    } 
    if (!bool || j > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("MESSAGE_STATUS decode ");
      if (bool) {
        str = "succeeded";
      } else {
        str = "failed";
      } 
      stringBuilder.append(str);
      stringBuilder.append(" (extra bits = ");
      stringBuilder.append(j);
      stringBuilder.append(")");
      String str = stringBuilder.toString();
      Rlog.d("BearerData", str);
    } 
    paramBitwiseInputStream.skip(j);
    paramBearerData.messageStatusSet = bool;
    return bool;
  }
  
  private static boolean decodeMsgCenterTimeStamp(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream) throws BitwiseInputStream.AccessException {
    boolean bool = false;
    int i = paramBitwiseInputStream.read(8) * 8;
    int j = i;
    if (i >= 48) {
      j = i - 48;
      bool = true;
      paramBearerData.msgCenterTimeStamp = TimeStamp.fromByteArray(paramBitwiseInputStream.readByteArray(48));
    } 
    if (!bool || j > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("MESSAGE_CENTER_TIME_STAMP decode ");
      if (bool) {
        str = "succeeded";
      } else {
        str = "failed";
      } 
      stringBuilder.append(str);
      stringBuilder.append(" (extra bits = ");
      stringBuilder.append(j);
      stringBuilder.append(")");
      String str = stringBuilder.toString();
      Rlog.d("BearerData", str);
    } 
    paramBitwiseInputStream.skip(j);
    return bool;
  }
  
  private static boolean decodeValidityAbs(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream) throws BitwiseInputStream.AccessException {
    boolean bool = false;
    int i = paramBitwiseInputStream.read(8) * 8;
    int j = i;
    if (i >= 48) {
      j = i - 48;
      bool = true;
      paramBearerData.validityPeriodAbsolute = TimeStamp.fromByteArray(paramBitwiseInputStream.readByteArray(48));
    } 
    if (!bool || j > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("VALIDITY_PERIOD_ABSOLUTE decode ");
      if (bool) {
        str = "succeeded";
      } else {
        str = "failed";
      } 
      stringBuilder.append(str);
      stringBuilder.append(" (extra bits = ");
      stringBuilder.append(j);
      stringBuilder.append(")");
      String str = stringBuilder.toString();
      Rlog.d("BearerData", str);
    } 
    paramBitwiseInputStream.skip(j);
    return bool;
  }
  
  private static boolean decodeDeferredDeliveryAbs(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream) throws BitwiseInputStream.AccessException {
    boolean bool = false;
    int i = paramBitwiseInputStream.read(8) * 8;
    int j = i;
    if (i >= 48) {
      j = i - 48;
      bool = true;
      byte[] arrayOfByte = paramBitwiseInputStream.readByteArray(48);
      paramBearerData.deferredDeliveryTimeAbsolute = TimeStamp.fromByteArray(arrayOfByte);
    } 
    if (!bool || j > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("DEFERRED_DELIVERY_TIME_ABSOLUTE decode ");
      if (bool) {
        str = "succeeded";
      } else {
        str = "failed";
      } 
      stringBuilder.append(str);
      stringBuilder.append(" (extra bits = ");
      stringBuilder.append(j);
      stringBuilder.append(")");
      String str = stringBuilder.toString();
      Rlog.d("BearerData", str);
    } 
    paramBitwiseInputStream.skip(j);
    return bool;
  }
  
  private static boolean decodeValidityRel(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream) throws BitwiseInputStream.AccessException {
    boolean bool = false;
    int i = paramBitwiseInputStream.read(8) * 8;
    int j = i;
    if (i >= 8) {
      j = i - 8;
      bool = true;
      paramBearerData.deferredDeliveryTimeRelative = paramBitwiseInputStream.read(8);
    } 
    if (!bool || j > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("VALIDITY_PERIOD_RELATIVE decode ");
      if (bool) {
        str = "succeeded";
      } else {
        str = "failed";
      } 
      stringBuilder.append(str);
      stringBuilder.append(" (extra bits = ");
      stringBuilder.append(j);
      stringBuilder.append(")");
      String str = stringBuilder.toString();
      Rlog.d("BearerData", str);
    } 
    paramBitwiseInputStream.skip(j);
    paramBearerData.deferredDeliveryTimeRelativeSet = bool;
    return bool;
  }
  
  private static boolean decodeDeferredDeliveryRel(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream) throws BitwiseInputStream.AccessException {
    boolean bool = false;
    int i = paramBitwiseInputStream.read(8) * 8;
    int j = i;
    if (i >= 8) {
      j = i - 8;
      bool = true;
      paramBearerData.validityPeriodRelative = paramBitwiseInputStream.read(8);
    } 
    if (!bool || j > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("DEFERRED_DELIVERY_TIME_RELATIVE decode ");
      if (bool) {
        str = "succeeded";
      } else {
        str = "failed";
      } 
      stringBuilder.append(str);
      stringBuilder.append(" (extra bits = ");
      stringBuilder.append(j);
      stringBuilder.append(")");
      String str = stringBuilder.toString();
      Rlog.d("BearerData", str);
    } 
    paramBitwiseInputStream.skip(j);
    paramBearerData.validityPeriodRelativeSet = bool;
    return bool;
  }
  
  private static boolean decodePrivacyIndicator(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream) throws BitwiseInputStream.AccessException {
    boolean bool = false;
    int i = paramBitwiseInputStream.read(8) * 8;
    int j = i;
    if (i >= 8) {
      j = i - 8;
      bool = true;
      paramBearerData.privacy = paramBitwiseInputStream.read(2);
      paramBitwiseInputStream.skip(6);
    } 
    if (!bool || j > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("PRIVACY_INDICATOR decode ");
      if (bool) {
        str = "succeeded";
      } else {
        str = "failed";
      } 
      stringBuilder.append(str);
      stringBuilder.append(" (extra bits = ");
      stringBuilder.append(j);
      stringBuilder.append(")");
      String str = stringBuilder.toString();
      Rlog.d("BearerData", str);
    } 
    paramBitwiseInputStream.skip(j);
    paramBearerData.privacyIndicatorSet = bool;
    return bool;
  }
  
  private static boolean decodeLanguageIndicator(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream) throws BitwiseInputStream.AccessException {
    boolean bool = false;
    int i = paramBitwiseInputStream.read(8) * 8;
    int j = i;
    if (i >= 8) {
      j = i - 8;
      bool = true;
      paramBearerData.language = paramBitwiseInputStream.read(8);
    } 
    if (!bool || j > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("LANGUAGE_INDICATOR decode ");
      if (bool) {
        str = "succeeded";
      } else {
        str = "failed";
      } 
      stringBuilder.append(str);
      stringBuilder.append(" (extra bits = ");
      stringBuilder.append(j);
      stringBuilder.append(")");
      String str = stringBuilder.toString();
      Rlog.d("BearerData", str);
    } 
    paramBitwiseInputStream.skip(j);
    paramBearerData.languageIndicatorSet = bool;
    return bool;
  }
  
  private static boolean decodeDisplayMode(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream) throws BitwiseInputStream.AccessException {
    boolean bool = false;
    int i = paramBitwiseInputStream.read(8) * 8;
    int j = i;
    if (i >= 8) {
      j = i - 8;
      bool = true;
      paramBearerData.displayMode = paramBitwiseInputStream.read(2);
      paramBitwiseInputStream.skip(6);
    } 
    if (!bool || j > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("DISPLAY_MODE decode ");
      if (bool) {
        str = "succeeded";
      } else {
        str = "failed";
      } 
      stringBuilder.append(str);
      stringBuilder.append(" (extra bits = ");
      stringBuilder.append(j);
      stringBuilder.append(")");
      String str = stringBuilder.toString();
      Rlog.d("BearerData", str);
    } 
    paramBitwiseInputStream.skip(j);
    paramBearerData.displayModeSet = bool;
    return bool;
  }
  
  private static boolean decodePriorityIndicator(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream) throws BitwiseInputStream.AccessException {
    boolean bool = false;
    int i = paramBitwiseInputStream.read(8) * 8;
    int j = i;
    if (i >= 8) {
      j = i - 8;
      bool = true;
      paramBearerData.priority = paramBitwiseInputStream.read(2);
      paramBitwiseInputStream.skip(6);
    } 
    if (!bool || j > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("PRIORITY_INDICATOR decode ");
      if (bool) {
        str = "succeeded";
      } else {
        str = "failed";
      } 
      stringBuilder.append(str);
      stringBuilder.append(" (extra bits = ");
      stringBuilder.append(j);
      stringBuilder.append(")");
      String str = stringBuilder.toString();
      Rlog.d("BearerData", str);
    } 
    paramBitwiseInputStream.skip(j);
    paramBearerData.priorityIndicatorSet = bool;
    return bool;
  }
  
  private static boolean decodeMsgDeliveryAlert(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream) throws BitwiseInputStream.AccessException {
    boolean bool = false;
    int i = paramBitwiseInputStream.read(8) * 8;
    int j = i;
    if (i >= 8) {
      j = i - 8;
      bool = true;
      paramBearerData.alert = paramBitwiseInputStream.read(2);
      paramBitwiseInputStream.skip(6);
    } 
    if (!bool || j > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ALERT_ON_MESSAGE_DELIVERY decode ");
      if (bool) {
        str = "succeeded";
      } else {
        str = "failed";
      } 
      stringBuilder.append(str);
      stringBuilder.append(" (extra bits = ");
      stringBuilder.append(j);
      stringBuilder.append(")");
      String str = stringBuilder.toString();
      Rlog.d("BearerData", str);
    } 
    paramBitwiseInputStream.skip(j);
    paramBearerData.alertIndicatorSet = bool;
    return bool;
  }
  
  private static boolean decodeUserResponseCode(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream) throws BitwiseInputStream.AccessException {
    boolean bool = false;
    int i = paramBitwiseInputStream.read(8) * 8;
    int j = i;
    if (i >= 8) {
      j = i - 8;
      bool = true;
      paramBearerData.userResponseCode = paramBitwiseInputStream.read(8);
    } 
    if (!bool || j > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("USER_RESPONSE_CODE decode ");
      if (bool) {
        str = "succeeded";
      } else {
        str = "failed";
      } 
      stringBuilder.append(str);
      stringBuilder.append(" (extra bits = ");
      stringBuilder.append(j);
      stringBuilder.append(")");
      String str = stringBuilder.toString();
      Rlog.d("BearerData", str);
    } 
    paramBitwiseInputStream.skip(j);
    paramBearerData.userResponseCodeSet = bool;
    return bool;
  }
  
  private static boolean decodeServiceCategoryProgramData(BearerData paramBearerData, BitwiseInputStream paramBitwiseInputStream) throws BitwiseInputStream.AccessException, CodingException {
    if (paramBitwiseInputStream.available() >= 13) {
      int i = paramBitwiseInputStream.read(8);
      int j = paramBitwiseInputStream.read(5);
      i = i * 8 - 5;
      if (paramBitwiseInputStream.available() >= i) {
        StringBuilder stringBuilder2;
        ArrayList<CdmaSmsCbProgramData> arrayList = new ArrayList();
        boolean bool = false;
        while (i >= 48) {
          int k = paramBitwiseInputStream.read(4);
          int m = paramBitwiseInputStream.read(8), n = paramBitwiseInputStream.read(8);
          int i1 = paramBitwiseInputStream.read(8);
          int i2 = paramBitwiseInputStream.read(8);
          int i3 = paramBitwiseInputStream.read(4);
          int i4 = paramBitwiseInputStream.read(8);
          int i5 = i - 48;
          i = getBitsForNumFields(j, i4);
          if (i5 >= i) {
            UserData userData = new UserData();
            userData.msgEncoding = j;
            userData.msgEncodingSet = true;
            userData.numFields = i4;
            userData.payload = paramBitwiseInputStream.readByteArray(i);
            i = i5 - i;
            decodeUserDataPayload(userData, false);
            String str = userData.payloadStr;
            CdmaSmsCbProgramData cdmaSmsCbProgramData = new CdmaSmsCbProgramData(k, m << 8 | n, i1, i2, i3, str);
            arrayList.add(cdmaSmsCbProgramData);
            bool = true;
            continue;
          } 
          stringBuilder2 = new StringBuilder();
          stringBuilder2.append("category name is ");
          stringBuilder2.append(i);
          stringBuilder2.append(" bits in length, but there are only ");
          stringBuilder2.append(i5);
          stringBuilder2.append(" bits available");
          throw new CodingException(stringBuilder2.toString());
        } 
        if (!bool || i > 0) {
          StringBuilder stringBuilder3 = new StringBuilder();
          stringBuilder3.append("SERVICE_CATEGORY_PROGRAM_DATA decode ");
          if (bool) {
            str = "succeeded";
          } else {
            str = "failed";
          } 
          stringBuilder3.append(str);
          stringBuilder3.append(" (extra bits = ");
          stringBuilder3.append(i);
          stringBuilder3.append(')');
          String str = stringBuilder3.toString();
          Rlog.d("BearerData", str);
        } 
        paramBitwiseInputStream.skip(i);
        ((BearerData)stringBuilder2).serviceCategoryProgramData = arrayList;
        return bool;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("SERVICE_CATEGORY_PROGRAM_DATA decode failed: only ");
      stringBuilder1.append(paramBitwiseInputStream.available());
      stringBuilder1.append(" bits available (");
      stringBuilder1.append(i);
      stringBuilder1.append(" bits expected)");
      throw new CodingException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SERVICE_CATEGORY_PROGRAM_DATA decode failed: only ");
    stringBuilder.append(paramBitwiseInputStream.available());
    stringBuilder.append(" bits available");
    throw new CodingException(stringBuilder.toString());
  }
  
  private static int serviceCategoryToCmasMessageClass(int paramInt) {
    switch (paramInt) {
      default:
        return -1;
      case 4100:
        return 4;
      case 4099:
        return 3;
      case 4098:
        return 2;
      case 4097:
        return 1;
      case 4096:
        break;
    } 
    return 0;
  }
  
  private static int getBitsForNumFields(int paramInt1, int paramInt2) throws CodingException {
    if (paramInt1 != 0) {
      StringBuilder stringBuilder;
      switch (paramInt1) {
        default:
          stringBuilder = new StringBuilder();
          stringBuilder.append("unsupported message encoding (");
          stringBuilder.append(paramInt1);
          stringBuilder.append(')');
          throw new CodingException(stringBuilder.toString());
        case 4:
          return paramInt2 * 16;
        case 2:
        case 3:
        case 9:
          return paramInt2 * 7;
        case 5:
        case 6:
        case 7:
        case 8:
          break;
      } 
    } 
    return paramInt2 * 8;
  }
  
  private static void decodeCmasUserData(BearerData paramBearerData, int paramInt) throws BitwiseInputStream.AccessException, CodingException {
    // Byte code:
    //   0: new com/android/internal/util/BitwiseInputStream
    //   3: dup
    //   4: aload_0
    //   5: getfield userData : Lcom/android/internal/telephony/cdma/sms/UserData;
    //   8: getfield payload : [B
    //   11: invokespecial <init> : ([B)V
    //   14: astore_2
    //   15: aload_2
    //   16: invokevirtual available : ()I
    //   19: bipush #8
    //   21: if_icmplt -> 389
    //   24: aload_2
    //   25: bipush #8
    //   27: invokevirtual read : (I)I
    //   30: istore_3
    //   31: iload_3
    //   32: ifne -> 355
    //   35: iload_1
    //   36: invokestatic serviceCategoryToCmasMessageClass : (I)I
    //   39: istore #4
    //   41: iconst_m1
    //   42: istore #5
    //   44: iconst_m1
    //   45: istore #6
    //   47: iconst_m1
    //   48: istore #7
    //   50: iconst_m1
    //   51: istore #8
    //   53: iconst_m1
    //   54: istore_3
    //   55: aload_2
    //   56: invokevirtual available : ()I
    //   59: bipush #16
    //   61: if_icmplt -> 332
    //   64: aload_2
    //   65: bipush #8
    //   67: invokevirtual read : (I)I
    //   70: istore_1
    //   71: aload_2
    //   72: bipush #8
    //   74: invokevirtual read : (I)I
    //   77: istore #9
    //   79: iload_1
    //   80: ifeq -> 189
    //   83: iload_1
    //   84: iconst_1
    //   85: if_icmpeq -> 138
    //   88: new java/lang/StringBuilder
    //   91: dup
    //   92: invokespecial <init> : ()V
    //   95: astore #10
    //   97: aload #10
    //   99: ldc_w 'skipping unsupported CMAS record type '
    //   102: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   105: pop
    //   106: aload #10
    //   108: iload_1
    //   109: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   112: pop
    //   113: ldc 'BearerData'
    //   115: aload #10
    //   117: invokevirtual toString : ()Ljava/lang/String;
    //   120: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   123: pop
    //   124: aload_2
    //   125: iload #9
    //   127: bipush #8
    //   129: imul
    //   130: invokevirtual skip : (I)V
    //   133: iload_3
    //   134: istore_1
    //   135: goto -> 327
    //   138: aload_2
    //   139: bipush #8
    //   141: invokevirtual read : (I)I
    //   144: istore #5
    //   146: aload_2
    //   147: bipush #8
    //   149: invokevirtual read : (I)I
    //   152: istore #6
    //   154: aload_2
    //   155: iconst_4
    //   156: invokevirtual read : (I)I
    //   159: istore #7
    //   161: aload_2
    //   162: iconst_4
    //   163: invokevirtual read : (I)I
    //   166: istore #8
    //   168: aload_2
    //   169: iconst_4
    //   170: invokevirtual read : (I)I
    //   173: istore_1
    //   174: aload_2
    //   175: iload #9
    //   177: bipush #8
    //   179: imul
    //   180: bipush #28
    //   182: isub
    //   183: invokevirtual skip : (I)V
    //   186: goto -> 327
    //   189: new com/android/internal/telephony/cdma/sms/UserData
    //   192: dup
    //   193: invokespecial <init> : ()V
    //   196: astore #10
    //   198: aload #10
    //   200: aload_2
    //   201: iconst_5
    //   202: invokevirtual read : (I)I
    //   205: putfield msgEncoding : I
    //   208: aload #10
    //   210: iconst_1
    //   211: putfield msgEncodingSet : Z
    //   214: aload #10
    //   216: iconst_0
    //   217: putfield msgType : I
    //   220: aload #10
    //   222: getfield msgEncoding : I
    //   225: istore_1
    //   226: iload_1
    //   227: ifeq -> 286
    //   230: iload_1
    //   231: iconst_2
    //   232: if_icmpeq -> 272
    //   235: iload_1
    //   236: iconst_3
    //   237: if_icmpeq -> 272
    //   240: iload_1
    //   241: iconst_4
    //   242: if_icmpeq -> 262
    //   245: iload_1
    //   246: bipush #8
    //   248: if_icmpeq -> 286
    //   251: iload_1
    //   252: bipush #9
    //   254: if_icmpeq -> 272
    //   257: iconst_0
    //   258: istore_1
    //   259: goto -> 291
    //   262: iload #9
    //   264: iconst_1
    //   265: isub
    //   266: iconst_2
    //   267: idiv
    //   268: istore_1
    //   269: goto -> 291
    //   272: iload #9
    //   274: bipush #8
    //   276: imul
    //   277: iconst_5
    //   278: isub
    //   279: bipush #7
    //   281: idiv
    //   282: istore_1
    //   283: goto -> 291
    //   286: iload #9
    //   288: iconst_1
    //   289: isub
    //   290: istore_1
    //   291: aload #10
    //   293: iload_1
    //   294: putfield numFields : I
    //   297: aload #10
    //   299: aload_2
    //   300: iload #9
    //   302: bipush #8
    //   304: imul
    //   305: iconst_5
    //   306: isub
    //   307: invokevirtual readByteArray : (I)[B
    //   310: putfield payload : [B
    //   313: aload #10
    //   315: iconst_0
    //   316: invokestatic decodeUserDataPayload : (Lcom/android/internal/telephony/cdma/sms/UserData;Z)V
    //   319: aload_0
    //   320: aload #10
    //   322: putfield userData : Lcom/android/internal/telephony/cdma/sms/UserData;
    //   325: iload_3
    //   326: istore_1
    //   327: iload_1
    //   328: istore_3
    //   329: goto -> 55
    //   332: aload_0
    //   333: new android/telephony/SmsCbCmasInfo
    //   336: dup
    //   337: iload #4
    //   339: iload #5
    //   341: iload #6
    //   343: iload #7
    //   345: iload #8
    //   347: iload_3
    //   348: invokespecial <init> : (IIIIII)V
    //   351: putfield cmasWarningInfo : Landroid/telephony/SmsCbCmasInfo;
    //   354: return
    //   355: new java/lang/StringBuilder
    //   358: dup
    //   359: invokespecial <init> : ()V
    //   362: astore_0
    //   363: aload_0
    //   364: ldc_w 'unsupported CMAE_protocol_version '
    //   367: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   370: pop
    //   371: aload_0
    //   372: iload_3
    //   373: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   376: pop
    //   377: new com/android/internal/telephony/cdma/sms/BearerData$CodingException
    //   380: dup
    //   381: aload_0
    //   382: invokevirtual toString : ()Ljava/lang/String;
    //   385: invokespecial <init> : (Ljava/lang/String;)V
    //   388: athrow
    //   389: new com/android/internal/telephony/cdma/sms/BearerData$CodingException
    //   392: dup
    //   393: ldc_w 'emergency CB with no CMAE_protocol_version'
    //   396: invokespecial <init> : (Ljava/lang/String;)V
    //   399: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1936	-> 0
    //   #1937	-> 15
    //   #1940	-> 24
    //   #1941	-> 31
    //   #1945	-> 35
    //   #1946	-> 41
    //   #1947	-> 41
    //   #1948	-> 41
    //   #1949	-> 41
    //   #1950	-> 41
    //   #1952	-> 55
    //   #1953	-> 64
    //   #1954	-> 71
    //   #1955	-> 79
    //   #1999	-> 88
    //   #2000	-> 124
    //   #1990	-> 138
    //   #1991	-> 146
    //   #1992	-> 154
    //   #1993	-> 161
    //   #1994	-> 168
    //   #1995	-> 174
    //   #1996	-> 186
    //   #1957	-> 189
    //   #1958	-> 198
    //   #1959	-> 208
    //   #1960	-> 214
    //   #1963	-> 220
    //   #1980	-> 257
    //   #1976	-> 262
    //   #1977	-> 269
    //   #1972	-> 272
    //   #1973	-> 283
    //   #1966	-> 286
    //   #1967	-> 291
    //   #1983	-> 291
    //   #1984	-> 297
    //   #1985	-> 313
    //   #1986	-> 319
    //   #1987	-> 325
    //   #2003	-> 327
    //   #2005	-> 332
    //   #2007	-> 354
    //   #1942	-> 355
    //   #1938	-> 389
  }
  
  public static BearerData decode(byte[] paramArrayOfbyte) {
    return decode(paramArrayOfbyte, 0);
  }
  
  private static boolean isCmasAlertCategory(int paramInt) {
    boolean bool;
    if (paramInt >= 4096 && paramInt <= 4351) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static BearerData decode(byte[] paramArrayOfbyte, int paramInt) {
    try {
      BitwiseInputStream bitwiseInputStream = new BitwiseInputStream();
      this(paramArrayOfbyte);
      BearerData bearerData = new BearerData();
      this();
      int i = 0;
      while (true) {
        int j = bitwiseInputStream.available();
        if (j > 0) {
          int k = bitwiseInputStream.read(8);
          int m = 1 << k;
          if ((i & m) == 0 || k < 0 || k > 23) {
            boolean bool;
            switch (k) {
              default:
                bool = decodeReserved(bearerData, bitwiseInputStream, k);
                break;
              case 20:
                bool = decodeMsgStatus(bearerData, bitwiseInputStream);
                break;
              case 18:
                bool = decodeServiceCategoryProgramData(bearerData, bitwiseInputStream);
                break;
              case 17:
                bool = decodeDepositIndex(bearerData, bitwiseInputStream);
                break;
              case 15:
                bool = decodeDisplayMode(bearerData, bitwiseInputStream);
                break;
              case 14:
                bool = decodeCallbackNumber(bearerData, bitwiseInputStream);
                break;
              case 13:
                bool = decodeLanguageIndicator(bearerData, bitwiseInputStream);
                break;
              case 12:
                bool = decodeMsgDeliveryAlert(bearerData, bitwiseInputStream);
                break;
              case 11:
                bool = decodeMsgCount(bearerData, bitwiseInputStream);
                break;
              case 10:
                bool = decodeReplyOption(bearerData, bitwiseInputStream);
                break;
              case 9:
                bool = decodePrivacyIndicator(bearerData, bitwiseInputStream);
                break;
              case 8:
                bool = decodePriorityIndicator(bearerData, bitwiseInputStream);
                break;
              case 7:
                bool = decodeDeferredDeliveryRel(bearerData, bitwiseInputStream);
                break;
              case 6:
                bool = decodeDeferredDeliveryAbs(bearerData, bitwiseInputStream);
                break;
              case 5:
                bool = decodeValidityRel(bearerData, bitwiseInputStream);
                break;
              case 4:
                bool = decodeValidityAbs(bearerData, bitwiseInputStream);
                break;
              case 3:
                bool = decodeMsgCenterTimeStamp(bearerData, bitwiseInputStream);
                break;
              case 2:
                bool = decodeUserResponseCode(bearerData, bitwiseInputStream);
                break;
              case 1:
                bool = decodeUserData(bearerData, bitwiseInputStream);
                break;
              case 0:
                bool = decodeMessageId(bearerData, bitwiseInputStream);
                break;
            } 
            j = i;
            if (bool) {
              j = i;
              if (k >= 0) {
                j = i;
                if (k <= 23)
                  j = i | m; 
              } 
            } 
            i = j;
            continue;
          } 
          codingException = new CodingException();
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("illegal duplicate subparameter (");
          stringBuilder.append(k);
          stringBuilder.append(")");
          this(stringBuilder.toString());
          throw codingException;
        } 
        break;
      } 
      if ((i & 0x1) != 0) {
        if (((BearerData)codingException).userData != null)
          if (isCmasAlertCategory(paramInt)) {
            decodeCmasUserData((BearerData)codingException, paramInt);
          } else if (((BearerData)codingException).userData.msgEncoding == 1) {
            if ((i ^ 0x1 ^ 0x2) != 0) {
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("IS-91 must occur without extra subparams (");
              stringBuilder.append(i);
              stringBuilder.append(")");
              Rlog.e("BearerData", stringBuilder.toString());
            } 
            decodeIs91((BearerData)codingException);
          } else {
            decodeUserDataPayload(((BearerData)codingException).userData, ((BearerData)codingException).hasUserDataHeader);
          }  
        return (BearerData)codingException;
      } 
      CodingException codingException = new CodingException();
      this("missing MESSAGE_IDENTIFIER subparam");
      throw codingException;
    } catch (com.android.internal.util.BitwiseInputStream.AccessException accessException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("BearerData decode failed: ");
      stringBuilder.append(accessException);
      Rlog.e("BearerData", stringBuilder.toString());
    } catch (CodingException codingException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("BearerData decode failed: ");
      stringBuilder.append(codingException);
      Rlog.e("BearerData", stringBuilder.toString());
    } 
    return null;
  }
}
