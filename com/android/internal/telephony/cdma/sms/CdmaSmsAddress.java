package com.android.internal.telephony.cdma.sms;

import android.util.SparseBooleanArray;
import com.android.internal.telephony.SmsAddress;
import com.android.internal.util.HexDump;

public class CdmaSmsAddress extends SmsAddress {
  public static final int DIGIT_MODE_4BIT_DTMF = 0;
  
  public static final int DIGIT_MODE_8BIT_CHAR = 1;
  
  public static final int NUMBERING_PLAN_ISDN_TELEPHONY = 1;
  
  public static final int NUMBERING_PLAN_UNKNOWN = 0;
  
  public static final int NUMBER_MODE_DATA_NETWORK = 1;
  
  public static final int NUMBER_MODE_NOT_DATA_NETWORK = 0;
  
  public static final int SMS_ADDRESS_MAX = 36;
  
  public static final int SMS_SUBADDRESS_MAX = 36;
  
  public static final int TON_ABBREVIATED = 6;
  
  public static final int TON_ALPHANUMERIC = 5;
  
  public static final int TON_INTERNATIONAL_OR_IP = 1;
  
  public static final int TON_NATIONAL_OR_EMAIL = 2;
  
  public static final int TON_NETWORK = 3;
  
  public static final int TON_RESERVED = 7;
  
  public static final int TON_SUBSCRIBER = 4;
  
  public static final int TON_UNKNOWN = 0;
  
  private static final SparseBooleanArray numericCharDialableMap;
  
  private static final char[] numericCharsDialable;
  
  private static final char[] numericCharsSugar;
  
  public int digitMode;
  
  public int numberMode;
  
  public int numberOfDigits;
  
  public int numberPlan;
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("CdmaSmsAddress ");
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("{ digitMode=");
    stringBuilder2.append(this.digitMode);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", numberMode=");
    stringBuilder2.append(this.numberMode);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", numberPlan=");
    stringBuilder2.append(this.numberPlan);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", numberOfDigits=");
    stringBuilder2.append(this.numberOfDigits);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", ton=");
    stringBuilder2.append(this.ton);
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", address=\"");
    stringBuilder2.append(this.address);
    stringBuilder2.append("\"");
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", origBytes=");
    stringBuilder2.append(HexDump.toHexString(this.origBytes));
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder1.append(" }");
    return stringBuilder1.toString();
  }
  
  public static byte[] parseToDtmf(String paramString) {
    int i = paramString.length();
    byte[] arrayOfByte = new byte[i];
    for (byte b = 0; b < i; b++) {
      char c = paramString.charAt(b);
      if (c >= '1' && c <= '9') {
        c -= '0';
      } else if (c == '0') {
        c = '\n';
      } else if (c == '*') {
        c = '\013';
      } else if (c == '#') {
        c = '\f';
      } else {
        return null;
      } 
      arrayOfByte[b] = (byte)c;
    } 
    return arrayOfByte;
  }
  
  static {
    char[] arrayOfChar1 = new char[12];
    arrayOfChar1[0] = '0';
    arrayOfChar1[1] = '1';
    arrayOfChar1[2] = '2';
    arrayOfChar1[3] = '3';
    arrayOfChar1[4] = '4';
    arrayOfChar1[5] = '5';
    arrayOfChar1[6] = '6';
    arrayOfChar1[7] = '7';
    arrayOfChar1[8] = '8';
    arrayOfChar1[9] = '9';
    arrayOfChar1[10] = '*';
    arrayOfChar1[11] = '#';
    numericCharsDialable = arrayOfChar1;
    char[] arrayOfChar2 = new char[8];
    arrayOfChar2[0] = '(';
    arrayOfChar2[1] = ')';
    arrayOfChar2[2] = ' ';
    arrayOfChar2[3] = '-';
    arrayOfChar2[4] = '+';
    arrayOfChar2[5] = '.';
    arrayOfChar2[6] = '/';
    arrayOfChar2[7] = '\\';
    numericCharsSugar = arrayOfChar2;
    numericCharDialableMap = new SparseBooleanArray(arrayOfChar1.length + arrayOfChar2.length);
    byte b = 0;
    while (true) {
      arrayOfChar2 = numericCharsDialable;
      if (b < arrayOfChar2.length) {
        numericCharDialableMap.put(arrayOfChar2[b], true);
        b++;
        continue;
      } 
      break;
    } 
    b = 0;
    while (true) {
      arrayOfChar2 = numericCharsSugar;
      if (b < arrayOfChar2.length) {
        numericCharDialableMap.put(arrayOfChar2[b], false);
        b++;
        continue;
      } 
      break;
    } 
  }
  
  private static String filterNumericSugar(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    int i = paramString.length();
    for (byte b = 0; b < i; b++) {
      char c = paramString.charAt(b);
      int j = numericCharDialableMap.indexOfKey(c);
      if (j < 0)
        return null; 
      if (numericCharDialableMap.valueAt(j))
        stringBuilder.append(c); 
    } 
    return stringBuilder.toString();
  }
  
  private static String filterWhitespace(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    int i = paramString.length();
    for (byte b = 0; b < i; b++) {
      char c = paramString.charAt(b);
      if (c != ' ' && c != '\r' && c != '\n' && c != '\t')
        stringBuilder.append(c); 
    } 
    return stringBuilder.toString();
  }
  
  public static CdmaSmsAddress parse(String paramString) {
    byte[] arrayOfByte;
    CdmaSmsAddress cdmaSmsAddress = new CdmaSmsAddress();
    cdmaSmsAddress.address = paramString;
    cdmaSmsAddress.ton = 0;
    cdmaSmsAddress.digitMode = 0;
    cdmaSmsAddress.numberPlan = 0;
    cdmaSmsAddress.numberMode = 0;
    String str = filterNumericSugar(paramString);
    if (paramString.contains("+") || str == null) {
      cdmaSmsAddress.digitMode = 1;
      cdmaSmsAddress.numberMode = 1;
      String str1 = filterWhitespace(paramString);
      if (paramString.contains("@")) {
        cdmaSmsAddress.ton = 2;
        str = str1;
      } else {
        str = str1;
        if (paramString.contains("+")) {
          str = str1;
          if (filterNumericSugar(paramString) != null) {
            cdmaSmsAddress.ton = 1;
            cdmaSmsAddress.numberPlan = 1;
            cdmaSmsAddress.numberMode = 0;
            str = filterNumericSugar(paramString);
          } 
        } 
      } 
      arrayOfByte = UserData.stringToAscii(str);
    } else {
      arrayOfByte = parseToDtmf(str);
    } 
    if (arrayOfByte == null)
      return null; 
    cdmaSmsAddress.origBytes = arrayOfByte;
    cdmaSmsAddress.numberOfDigits = arrayOfByte.length;
    return cdmaSmsAddress;
  }
}
