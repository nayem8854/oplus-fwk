package com.android.internal.telephony.cdma;

import android.content.res.Resources;
import android.sysprop.TelephonyProperties;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsCbLocation;
import android.telephony.SmsCbMessage;
import android.telephony.cdma.CdmaSmsCbProgramData;
import android.text.TextUtils;
import android.util.Log;
import com.android.internal.telephony.GsmAlphabet;
import com.android.internal.telephony.Sms7BitEncodingTranslator;
import com.android.internal.telephony.SmsAddress;
import com.android.internal.telephony.SmsConstants;
import com.android.internal.telephony.SmsHeader;
import com.android.internal.telephony.SmsMessageBase;
import com.android.internal.telephony.cdma.sms.BearerData;
import com.android.internal.telephony.cdma.sms.CdmaSmsAddress;
import com.android.internal.telephony.cdma.sms.CdmaSmsSubaddress;
import com.android.internal.telephony.cdma.sms.SmsEnvelope;
import com.android.internal.telephony.cdma.sms.UserData;
import com.android.internal.util.BitwiseInputStream;
import com.android.internal.util.HexDump;
import com.android.telephony.Rlog;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class SmsMessage extends SmsMessageBase {
  private static final byte BEARER_DATA = 8;
  
  private static final byte BEARER_REPLY_OPTION = 6;
  
  private static final byte CAUSE_CODES = 7;
  
  private static final byte DESTINATION_ADDRESS = 4;
  
  private static final byte DESTINATION_SUB_ADDRESS = 5;
  
  private static final String LOGGABLE_TAG = "CDMA:SMS";
  
  static final String LOG_TAG = "SmsMessage";
  
  private static final byte ORIGINATING_ADDRESS = 2;
  
  private static final byte ORIGINATING_SUB_ADDRESS = 3;
  
  private static final int PRIORITY_EMERGENCY = 3;
  
  private static final int PRIORITY_INTERACTIVE = 1;
  
  private static final int PRIORITY_NORMAL = 0;
  
  private static final int PRIORITY_URGENT = 2;
  
  private static final int RETURN_ACK = 1;
  
  private static final int RETURN_NO_ACK = 0;
  
  private static final byte SERVICE_CATEGORY = 1;
  
  private static final byte TELESERVICE_IDENTIFIER = 0;
  
  private static final boolean VDBG = false;
  
  private BearerData mBearerData;
  
  private SmsEnvelope mEnvelope;
  
  private int status;
  
  public SmsMessage(SmsAddress paramSmsAddress, SmsEnvelope paramSmsEnvelope) {
    this.mOriginatingAddress = paramSmsAddress;
    this.mEnvelope = paramSmsEnvelope;
    createPdu();
  }
  
  public SmsMessage() {}
  
  public static class SubmitPdu extends SmsMessageBase.SubmitPduBase {}
  
  public static SmsMessage createFromPdu(byte[] paramArrayOfbyte) {
    SmsMessage smsMessage = new SmsMessage();
    try {
      smsMessage.parsePdu(paramArrayOfbyte);
      return smsMessage;
    } catch (RuntimeException runtimeException) {
      Rlog.e("SmsMessage", "SMS PDU parsing failed: ", runtimeException);
      return null;
    } catch (OutOfMemoryError outOfMemoryError) {
      Log.e("SmsMessage", "SMS PDU parsing failed with out of memory: ", outOfMemoryError);
      return null;
    } 
  }
  
  public static SmsMessage createFromEfRecord(int paramInt, byte[] paramArrayOfbyte) {
    try {
      SmsMessage smsMessage = new SmsMessage();
      this();
      smsMessage.mIndexOnIcc = paramInt;
      if ((paramArrayOfbyte[0] & 0x1) == 0) {
        Rlog.w("SmsMessage", "SMS parsing failed: Trying to parse a free record");
        return null;
      } 
      smsMessage.mStatusOnIcc = paramArrayOfbyte[0] & 0x7;
      paramInt = paramArrayOfbyte[1] & 0xFF;
      byte[] arrayOfByte = new byte[paramInt];
      System.arraycopy(paramArrayOfbyte, 2, arrayOfByte, 0, paramInt);
      smsMessage.parsePduFromEfRecord(arrayOfByte);
      return smsMessage;
    } catch (RuntimeException runtimeException) {
      Rlog.e("SmsMessage", "SMS PDU parsing failed: ", runtimeException);
      return null;
    } 
  }
  
  public static int getTPLayerLengthForPDU(String paramString) {
    Rlog.w("SmsMessage", "getTPLayerLengthForPDU: is not supported in CDMA mode.");
    return 0;
  }
  
  public static SubmitPdu getSubmitPdu(String paramString1, String paramString2, String paramString3, boolean paramBoolean, SmsHeader paramSmsHeader) {
    return getSubmitPdu(paramString1, paramString2, paramString3, paramBoolean, paramSmsHeader, -1);
  }
  
  public static SubmitPdu getSubmitPdu(String paramString1, String paramString2, String paramString3, boolean paramBoolean, SmsHeader paramSmsHeader, int paramInt) {
    if (paramString3 == null || paramString2 == null)
      return null; 
    UserData userData = new UserData();
    userData.payloadStr = paramString3;
    userData.userDataHeader = paramSmsHeader;
    return privateGetSubmitPdu(paramString2, paramBoolean, userData, paramInt);
  }
  
  public static SubmitPdu getSubmitPduOem(String paramString1, String paramString2, String paramString3, boolean paramBoolean1, SmsHeader paramSmsHeader, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean2) {
    return OplusSmsMessage.getSubmitPduOem(paramString1, paramString2, paramString3, paramBoolean1, paramSmsHeader, paramInt1, paramInt2, paramInt3, paramBoolean2);
  }
  
  public static SubmitPdu getSubmitPdu(String paramString1, String paramString2, int paramInt, byte[] paramArrayOfbyte, boolean paramBoolean) {
    SmsHeader.PortAddrs portAddrs = new SmsHeader.PortAddrs();
    portAddrs.destPort = paramInt;
    portAddrs.origPort = 0;
    portAddrs.areEightBits = false;
    SmsHeader smsHeader = new SmsHeader();
    smsHeader.portAddrs = portAddrs;
    UserData userData = new UserData();
    userData.userDataHeader = smsHeader;
    if (OplusSmsMessage.oemGetSubmitPdu())
      userData.userDataHeader = null; 
    userData.msgEncoding = 0;
    userData.msgEncodingSet = true;
    userData.payload = paramArrayOfbyte;
    return privateGetSubmitPdu(paramString2, paramBoolean, userData);
  }
  
  public static SubmitPdu getSubmitPdu(String paramString, UserData paramUserData, boolean paramBoolean) {
    return privateGetSubmitPdu(paramString, paramBoolean, paramUserData);
  }
  
  public static SubmitPdu getSubmitPdu(String paramString, UserData paramUserData, boolean paramBoolean, int paramInt) {
    return privateGetSubmitPdu(paramString, paramBoolean, paramUserData, paramInt);
  }
  
  public int getProtocolIdentifier() {
    Rlog.w("SmsMessage", "getProtocolIdentifier: is not supported in CDMA mode.");
    return 0;
  }
  
  public boolean isReplace() {
    Rlog.w("SmsMessage", "isReplace: is not supported in CDMA mode.");
    return false;
  }
  
  public boolean isCphsMwiMessage() {
    Rlog.w("SmsMessage", "isCphsMwiMessage: is not supported in CDMA mode.");
    return false;
  }
  
  public boolean isMWIClearMessage() {
    boolean bool;
    BearerData bearerData = this.mBearerData;
    if (bearerData != null && bearerData.numberOfMessages == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isMWISetMessage() {
    boolean bool;
    BearerData bearerData = this.mBearerData;
    if (bearerData != null && bearerData.numberOfMessages > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isMwiDontStore() {
    boolean bool;
    BearerData bearerData = this.mBearerData;
    if (bearerData != null && bearerData.numberOfMessages > 0 && this.mBearerData.userData == null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getStatus() {
    return this.status << 16;
  }
  
  public boolean isStatusReportMessage() {
    boolean bool;
    if (this.mBearerData.messageType == 4) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isReplyPathPresent() {
    Rlog.w("SmsMessage", "isReplyPathPresent: is not supported in CDMA mode.");
    return false;
  }
  
  public static GsmAlphabet.TextEncodingDetails calculateLength(CharSequence paramCharSequence, boolean paramBoolean1, boolean paramBoolean2) {
    String str = null;
    Resources resources = Resources.getSystem();
    if (resources.getBoolean(17891537))
      str = Sms7BitEncodingTranslator.translate(paramCharSequence, true); 
    CharSequence charSequence = str;
    if (TextUtils.isEmpty(str))
      charSequence = paramCharSequence; 
    return BearerData.calcTextEncodingDetails(charSequence, paramBoolean1, paramBoolean2);
  }
  
  public static GsmAlphabet.TextEncodingDetails calculateLengthOem(CharSequence paramCharSequence, boolean paramBoolean1, boolean paramBoolean2, int paramInt) {
    return OplusSmsMessage.calculateLengthOem(paramCharSequence, paramBoolean1, paramBoolean2, paramInt);
  }
  
  public int getTeleService() {
    return this.mEnvelope.teleService;
  }
  
  public int getMessageType() {
    if (this.mEnvelope.serviceCategory != 0)
      return 1; 
    return 0;
  }
  
  private void parsePdu(byte[] paramArrayOfbyte) {
    StringBuilder stringBuilder;
    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(paramArrayOfbyte);
    DataInputStream dataInputStream = new DataInputStream(byteArrayInputStream);
    SmsEnvelope smsEnvelope = new SmsEnvelope();
    CdmaSmsAddress cdmaSmsAddress = new CdmaSmsAddress();
    CdmaSmsSubaddress cdmaSmsSubaddress = new CdmaSmsSubaddress();
    try {
      smsEnvelope.messageType = dataInputStream.readInt();
      smsEnvelope.teleService = dataInputStream.readInt();
      smsEnvelope.serviceCategory = dataInputStream.readInt();
      cdmaSmsAddress.digitMode = dataInputStream.readByte();
      cdmaSmsAddress.numberMode = dataInputStream.readByte();
      cdmaSmsAddress.ton = dataInputStream.readByte();
      cdmaSmsAddress.numberPlan = dataInputStream.readByte();
      int i = dataInputStream.readUnsignedByte();
      cdmaSmsAddress.numberOfDigits = i;
      int j = paramArrayOfbyte.length;
      if (i <= j) {
        cdmaSmsAddress.origBytes = new byte[i];
        dataInputStream.read(cdmaSmsAddress.origBytes, 0, i);
        smsEnvelope.bearerReply = dataInputStream.readInt();
        smsEnvelope.replySeqNo = dataInputStream.readByte();
        smsEnvelope.errorClass = dataInputStream.readByte();
        smsEnvelope.causeCode = dataInputStream.readByte();
        i = dataInputStream.readInt();
        if (i <= paramArrayOfbyte.length) {
          smsEnvelope.bearerData = new byte[i];
          dataInputStream.read(smsEnvelope.bearerData, 0, i);
          dataInputStream.close();
        } else {
          RuntimeException runtimeException = new RuntimeException();
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("createFromPdu: Invalid pdu, bearerDataLength ");
          stringBuilder1.append(i);
          stringBuilder1.append(" > pdu len ");
          stringBuilder1.append(paramArrayOfbyte.length);
          this(stringBuilder1.toString());
          throw runtimeException;
        } 
      } else {
        RuntimeException runtimeException = new RuntimeException();
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append("createFromPdu: Invalid pdu, addr.numberOfDigits ");
        stringBuilder1.append(i);
        stringBuilder1.append(" > pdu len ");
        stringBuilder1.append(paramArrayOfbyte.length);
        this(stringBuilder1.toString());
        throw runtimeException;
      } 
      this.mOriginatingAddress = cdmaSmsAddress;
      smsEnvelope.origAddress = cdmaSmsAddress;
      smsEnvelope.origSubaddress = cdmaSmsSubaddress;
      this.mEnvelope = smsEnvelope;
      this.mPdu = paramArrayOfbyte;
      parseSms();
      return;
    } catch (IOException iOException) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("createFromPdu: conversion from byte array to object failed: ");
      stringBuilder.append(iOException);
      throw new RuntimeException(stringBuilder.toString(), iOException);
    } catch (Exception exception) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("createFromPdu: conversion from byte array to object failed: ");
      stringBuilder1.append(exception);
      Rlog.e("SmsMessage", stringBuilder1.toString());
      this.mOriginatingAddress = (SmsAddress)stringBuilder;
      smsEnvelope.origAddress = (CdmaSmsAddress)stringBuilder;
      smsEnvelope.origSubaddress = cdmaSmsSubaddress;
      this.mEnvelope = smsEnvelope;
      this.mPdu = (byte[])iOException;
      parseSms();
      return;
    } 
  }
  
  private void parsePduFromEfRecord(byte[] paramArrayOfbyte) {
    // Byte code:
    //   0: new java/io/ByteArrayInputStream
    //   3: dup
    //   4: aload_1
    //   5: invokespecial <init> : ([B)V
    //   8: astore_2
    //   9: new java/io/DataInputStream
    //   12: dup
    //   13: aload_2
    //   14: invokespecial <init> : (Ljava/io/InputStream;)V
    //   17: astore_3
    //   18: new com/android/internal/telephony/cdma/sms/SmsEnvelope
    //   21: dup
    //   22: invokespecial <init> : ()V
    //   25: astore #4
    //   27: new com/android/internal/telephony/cdma/sms/CdmaSmsAddress
    //   30: dup
    //   31: invokespecial <init> : ()V
    //   34: astore #5
    //   36: new com/android/internal/telephony/cdma/sms/CdmaSmsSubaddress
    //   39: dup
    //   40: invokespecial <init> : ()V
    //   43: astore #6
    //   45: aload #6
    //   47: astore #7
    //   49: aload #4
    //   51: aload_3
    //   52: invokevirtual readByte : ()B
    //   55: putfield messageType : I
    //   58: aload #6
    //   60: astore #7
    //   62: aload_3
    //   63: invokevirtual available : ()I
    //   66: ifle -> 975
    //   69: aload #6
    //   71: astore #7
    //   73: aload_3
    //   74: invokevirtual readByte : ()B
    //   77: istore #8
    //   79: aload #6
    //   81: astore #7
    //   83: aload_3
    //   84: invokevirtual readUnsignedByte : ()I
    //   87: istore #9
    //   89: aload #6
    //   91: astore #7
    //   93: iload #9
    //   95: newarray byte
    //   97: astore #10
    //   99: iload #8
    //   101: tableswitch default -> 152, 0 -> 873, 1 -> 861, 2 -> 432, 3 -> 294, 4 -> 432, 5 -> 294, 6 -> 257, 7 -> 180, 8 -> 160
    //   152: new java/lang/Exception
    //   155: astore #7
    //   157: goto -> 926
    //   160: aload_3
    //   161: aload #10
    //   163: iconst_0
    //   164: iload #9
    //   166: invokevirtual read : ([BII)I
    //   169: pop
    //   170: aload #4
    //   172: aload #10
    //   174: putfield bearerData : [B
    //   177: goto -> 923
    //   180: aload_3
    //   181: aload #10
    //   183: iconst_0
    //   184: iload #9
    //   186: invokevirtual read : ([BII)I
    //   189: pop
    //   190: new com/android/internal/util/BitwiseInputStream
    //   193: astore #7
    //   195: aload #7
    //   197: aload #10
    //   199: invokespecial <init> : ([B)V
    //   202: aload #4
    //   204: aload #7
    //   206: bipush #6
    //   208: invokevirtual readByteArray : (I)[B
    //   211: iconst_0
    //   212: baload
    //   213: putfield replySeqNo : B
    //   216: aload #4
    //   218: aload #7
    //   220: iconst_2
    //   221: invokevirtual readByteArray : (I)[B
    //   224: iconst_0
    //   225: baload
    //   226: putfield errorClass : B
    //   229: aload #4
    //   231: getfield errorClass : B
    //   234: ifeq -> 254
    //   237: aload #4
    //   239: aload #7
    //   241: bipush #8
    //   243: invokevirtual readByteArray : (I)[B
    //   246: iconst_0
    //   247: baload
    //   248: putfield causeCode : B
    //   251: goto -> 923
    //   254: goto -> 923
    //   257: aload_3
    //   258: aload #10
    //   260: iconst_0
    //   261: iload #9
    //   263: invokevirtual read : ([BII)I
    //   266: pop
    //   267: new com/android/internal/util/BitwiseInputStream
    //   270: astore #7
    //   272: aload #7
    //   274: aload #10
    //   276: invokespecial <init> : ([B)V
    //   279: aload #4
    //   281: aload #7
    //   283: bipush #6
    //   285: invokevirtual read : (I)I
    //   288: putfield bearerReply : I
    //   291: goto -> 923
    //   294: aload_3
    //   295: aload #10
    //   297: iconst_0
    //   298: iload #9
    //   300: invokevirtual read : ([BII)I
    //   303: pop
    //   304: new com/android/internal/util/BitwiseInputStream
    //   307: astore #7
    //   309: aload #7
    //   311: aload #10
    //   313: invokespecial <init> : ([B)V
    //   316: aload #6
    //   318: aload #7
    //   320: iconst_3
    //   321: invokevirtual read : (I)I
    //   324: putfield type : I
    //   327: aload #6
    //   329: aload #7
    //   331: iconst_1
    //   332: invokevirtual readByteArray : (I)[B
    //   335: iconst_0
    //   336: baload
    //   337: putfield odd : B
    //   340: aload #7
    //   342: bipush #8
    //   344: invokevirtual read : (I)I
    //   347: istore #11
    //   349: iload #11
    //   351: newarray byte
    //   353: astore #10
    //   355: iconst_0
    //   356: istore #9
    //   358: iload #9
    //   360: iload #11
    //   362: if_icmpge -> 394
    //   365: aload #7
    //   367: iconst_4
    //   368: invokevirtual read : (I)I
    //   371: sipush #255
    //   374: iand
    //   375: i2b
    //   376: istore #12
    //   378: aload #10
    //   380: iload #9
    //   382: iload #12
    //   384: invokestatic convertDtmfToAscii : (B)B
    //   387: bastore
    //   388: iinc #9, 1
    //   391: goto -> 358
    //   394: aload #6
    //   396: aload #10
    //   398: putfield origBytes : [B
    //   401: iload #8
    //   403: iconst_3
    //   404: if_icmpne -> 417
    //   407: aload #4
    //   409: aload #6
    //   411: putfield origSubaddress : Lcom/android/internal/telephony/cdma/sms/CdmaSmsSubaddress;
    //   414: goto -> 923
    //   417: aload #4
    //   419: aload #6
    //   421: putfield destSubaddress : Lcom/android/internal/telephony/cdma/sms/CdmaSmsSubaddress;
    //   424: goto -> 923
    //   427: astore #6
    //   429: goto -> 993
    //   432: aload #6
    //   434: astore #7
    //   436: aload_3
    //   437: aload #10
    //   439: iconst_0
    //   440: iload #9
    //   442: invokevirtual read : ([BII)I
    //   445: pop
    //   446: aload #6
    //   448: astore #7
    //   450: new com/android/internal/util/BitwiseInputStream
    //   453: astore #13
    //   455: aload #6
    //   457: astore #7
    //   459: aload #13
    //   461: aload #10
    //   463: invokespecial <init> : ([B)V
    //   466: aload #6
    //   468: astore #7
    //   470: aload #5
    //   472: aload #13
    //   474: iconst_1
    //   475: invokevirtual read : (I)I
    //   478: putfield digitMode : I
    //   481: aload #6
    //   483: astore #7
    //   485: aload #5
    //   487: aload #13
    //   489: iconst_1
    //   490: invokevirtual read : (I)I
    //   493: putfield numberMode : I
    //   496: iconst_0
    //   497: istore #11
    //   499: aload #6
    //   501: astore #7
    //   503: aload #5
    //   505: getfield digitMode : I
    //   508: istore #14
    //   510: iload #14
    //   512: iconst_1
    //   513: if_icmpne -> 558
    //   516: aload #13
    //   518: iconst_3
    //   519: invokevirtual read : (I)I
    //   522: istore #14
    //   524: aload #5
    //   526: iload #14
    //   528: putfield ton : I
    //   531: iload #14
    //   533: istore #11
    //   535: aload #5
    //   537: getfield numberMode : I
    //   540: ifne -> 558
    //   543: aload #5
    //   545: aload #13
    //   547: iconst_4
    //   548: invokevirtual read : (I)I
    //   551: putfield numberPlan : I
    //   554: iload #14
    //   556: istore #11
    //   558: aload #6
    //   560: astore #7
    //   562: aload #5
    //   564: aload #13
    //   566: bipush #8
    //   568: invokevirtual read : (I)I
    //   571: putfield numberOfDigits : I
    //   574: aload #6
    //   576: astore #7
    //   578: aload #5
    //   580: getfield numberOfDigits : I
    //   583: newarray byte
    //   585: astore #10
    //   587: aload #6
    //   589: astore #7
    //   591: aload #5
    //   593: getfield digitMode : I
    //   596: istore #14
    //   598: iload #14
    //   600: ifne -> 647
    //   603: iconst_0
    //   604: istore #11
    //   606: iload #11
    //   608: aload #5
    //   610: getfield numberOfDigits : I
    //   613: if_icmpge -> 644
    //   616: aload #13
    //   618: iconst_4
    //   619: invokevirtual read : (I)I
    //   622: bipush #15
    //   624: iand
    //   625: i2b
    //   626: istore #12
    //   628: aload #10
    //   630: iload #11
    //   632: iload #12
    //   634: invokestatic convertDtmfToAscii : (B)B
    //   637: bastore
    //   638: iinc #11, 1
    //   641: goto -> 606
    //   644: goto -> 775
    //   647: aload #5
    //   649: getfield digitMode : I
    //   652: iconst_1
    //   653: if_icmpne -> 766
    //   656: aload #5
    //   658: getfield numberMode : I
    //   661: ifne -> 715
    //   664: iconst_0
    //   665: istore #14
    //   667: iload #9
    //   669: istore #11
    //   671: iload #14
    //   673: istore #9
    //   675: iload #9
    //   677: aload #5
    //   679: getfield numberOfDigits : I
    //   682: if_icmpge -> 712
    //   685: aload #13
    //   687: bipush #8
    //   689: invokevirtual read : (I)I
    //   692: sipush #255
    //   695: iand
    //   696: i2b
    //   697: istore #12
    //   699: aload #10
    //   701: iload #9
    //   703: iload #12
    //   705: bastore
    //   706: iinc #9, 1
    //   709: goto -> 675
    //   712: goto -> 775
    //   715: aload #5
    //   717: getfield numberMode : I
    //   720: iconst_1
    //   721: if_icmpne -> 754
    //   724: iload #11
    //   726: iconst_2
    //   727: if_icmpne -> 742
    //   730: ldc 'SmsMessage'
    //   732: ldc_w 'TODO: Addr is email id'
    //   735: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   738: pop
    //   739: goto -> 775
    //   742: ldc 'SmsMessage'
    //   744: ldc_w 'TODO: Addr is data network address'
    //   747: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   750: pop
    //   751: goto -> 775
    //   754: ldc 'SmsMessage'
    //   756: ldc_w 'Addr is of incorrect type'
    //   759: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   762: pop
    //   763: goto -> 775
    //   766: ldc 'SmsMessage'
    //   768: ldc_w 'Incorrect Digit mode'
    //   771: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   774: pop
    //   775: aload #5
    //   777: aload #10
    //   779: putfield origBytes : [B
    //   782: new java/lang/StringBuilder
    //   785: astore #7
    //   787: aload #7
    //   789: invokespecial <init> : ()V
    //   792: aload #7
    //   794: ldc_w 'Addr='
    //   797: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   800: pop
    //   801: aload #7
    //   803: aload #5
    //   805: invokevirtual toString : ()Ljava/lang/String;
    //   808: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   811: pop
    //   812: ldc 'SmsMessage'
    //   814: aload #7
    //   816: invokevirtual toString : ()Ljava/lang/String;
    //   819: invokestatic pii : (Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;
    //   822: pop
    //   823: iload #8
    //   825: iconst_2
    //   826: if_icmpne -> 845
    //   829: aload #4
    //   831: aload #5
    //   833: putfield origAddress : Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;
    //   836: aload_0
    //   837: aload #5
    //   839: putfield mOriginatingAddress : Lcom/android/internal/telephony/SmsAddress;
    //   842: goto -> 923
    //   845: aload #4
    //   847: aload #5
    //   849: putfield destAddress : Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;
    //   852: aload_0
    //   853: aload #5
    //   855: putfield mRecipientAddress : Lcom/android/internal/telephony/SmsAddress;
    //   858: goto -> 923
    //   861: aload #4
    //   863: aload_3
    //   864: invokevirtual readUnsignedShort : ()I
    //   867: putfield serviceCategory : I
    //   870: goto -> 923
    //   873: aload #4
    //   875: aload_3
    //   876: invokevirtual readUnsignedShort : ()I
    //   879: putfield teleService : I
    //   882: new java/lang/StringBuilder
    //   885: astore #7
    //   887: aload #7
    //   889: invokespecial <init> : ()V
    //   892: aload #7
    //   894: ldc_w 'teleservice = '
    //   897: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   900: pop
    //   901: aload #7
    //   903: aload #4
    //   905: getfield teleService : I
    //   908: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   911: pop
    //   912: ldc 'SmsMessage'
    //   914: aload #7
    //   916: invokevirtual toString : ()Ljava/lang/String;
    //   919: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   922: pop
    //   923: goto -> 58
    //   926: new java/lang/StringBuilder
    //   929: astore #6
    //   931: aload #6
    //   933: invokespecial <init> : ()V
    //   936: aload #6
    //   938: ldc_w 'unsupported parameterId ('
    //   941: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   944: pop
    //   945: aload #6
    //   947: iload #8
    //   949: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   952: pop
    //   953: aload #6
    //   955: ldc_w ')'
    //   958: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   961: pop
    //   962: aload #7
    //   964: aload #6
    //   966: invokevirtual toString : ()Ljava/lang/String;
    //   969: invokespecial <init> : (Ljava/lang/String;)V
    //   972: aload #7
    //   974: athrow
    //   975: aload_2
    //   976: invokevirtual close : ()V
    //   979: aload_3
    //   980: invokevirtual close : ()V
    //   983: goto -> 1030
    //   986: astore #6
    //   988: goto -> 993
    //   991: astore #6
    //   993: new java/lang/StringBuilder
    //   996: dup
    //   997: invokespecial <init> : ()V
    //   1000: astore #7
    //   1002: aload #7
    //   1004: ldc_w 'parsePduFromEfRecord: conversion from pdu to SmsMessage failed'
    //   1007: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1010: pop
    //   1011: aload #7
    //   1013: aload #6
    //   1015: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1018: pop
    //   1019: ldc 'SmsMessage'
    //   1021: aload #7
    //   1023: invokevirtual toString : ()Ljava/lang/String;
    //   1026: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   1029: pop
    //   1030: aload_0
    //   1031: aload #4
    //   1033: putfield mEnvelope : Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;
    //   1036: aload_0
    //   1037: aload_1
    //   1038: putfield mPdu : [B
    //   1041: aload_0
    //   1042: invokevirtual parseSms : ()V
    //   1045: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #564	-> 0
    //   #565	-> 9
    //   #566	-> 18
    //   #567	-> 27
    //   #568	-> 36
    //   #571	-> 45
    //   #573	-> 58
    //   #574	-> 69
    //   #575	-> 79
    //   #576	-> 89
    //   #578	-> 99
    //   #690	-> 152
    //   #686	-> 160
    //   #687	-> 170
    //   #688	-> 177
    //   #678	-> 180
    //   #679	-> 190
    //   #680	-> 202
    //   #681	-> 216
    //   #682	-> 229
    //   #683	-> 237
    //   #682	-> 254
    //   #673	-> 257
    //   #674	-> 267
    //   #675	-> 279
    //   #676	-> 291
    //   #654	-> 294
    //   #655	-> 304
    //   #656	-> 316
    //   #657	-> 327
    //   #658	-> 340
    //   #659	-> 349
    //   #660	-> 355
    //   #661	-> 365
    //   #663	-> 378
    //   #660	-> 388
    //   #665	-> 394
    //   #666	-> 401
    //   #667	-> 407
    //   #669	-> 417
    //   #671	-> 424
    //   #695	-> 427
    //   #597	-> 432
    //   #598	-> 446
    //   #599	-> 466
    //   #600	-> 481
    //   #601	-> 496
    //   #602	-> 499
    //   #603	-> 516
    //   #604	-> 524
    //   #606	-> 531
    //   #607	-> 543
    //   #610	-> 558
    //   #612	-> 574
    //   #613	-> 587
    //   #615	-> 587
    //   #617	-> 603
    //   #618	-> 616
    //   #621	-> 628
    //   #617	-> 638
    //   #623	-> 647
    //   #624	-> 656
    //   #625	-> 664
    //   #626	-> 685
    //   #627	-> 699
    //   #625	-> 706
    //   #630	-> 715
    //   #631	-> 724
    //   #632	-> 730
    //   #634	-> 742
    //   #637	-> 754
    //   #640	-> 766
    //   #642	-> 775
    //   #643	-> 782
    //   #644	-> 823
    //   #645	-> 829
    //   #646	-> 836
    //   #648	-> 845
    //   #649	-> 852
    //   #651	-> 858
    //   #593	-> 861
    //   #594	-> 870
    //   #585	-> 873
    //   #586	-> 882
    //   #587	-> 923
    //   #692	-> 923
    //   #690	-> 926
    //   #693	-> 975
    //   #694	-> 979
    //   #697	-> 983
    //   #695	-> 986
    //   #696	-> 993
    //   #700	-> 1030
    //   #701	-> 1036
    //   #703	-> 1041
    //   #704	-> 1045
    // Exception table:
    //   from	to	target	type
    //   49	58	991	java/lang/Exception
    //   62	69	991	java/lang/Exception
    //   73	79	991	java/lang/Exception
    //   83	89	991	java/lang/Exception
    //   93	99	991	java/lang/Exception
    //   152	157	986	java/lang/Exception
    //   160	170	427	java/lang/Exception
    //   170	177	427	java/lang/Exception
    //   180	190	427	java/lang/Exception
    //   190	202	427	java/lang/Exception
    //   202	216	427	java/lang/Exception
    //   216	229	427	java/lang/Exception
    //   229	237	427	java/lang/Exception
    //   237	251	427	java/lang/Exception
    //   257	267	427	java/lang/Exception
    //   267	279	427	java/lang/Exception
    //   279	291	427	java/lang/Exception
    //   294	304	427	java/lang/Exception
    //   304	316	427	java/lang/Exception
    //   316	327	427	java/lang/Exception
    //   327	340	427	java/lang/Exception
    //   340	349	427	java/lang/Exception
    //   349	355	427	java/lang/Exception
    //   365	378	427	java/lang/Exception
    //   378	388	427	java/lang/Exception
    //   394	401	427	java/lang/Exception
    //   407	414	427	java/lang/Exception
    //   417	424	427	java/lang/Exception
    //   436	446	991	java/lang/Exception
    //   450	455	991	java/lang/Exception
    //   459	466	991	java/lang/Exception
    //   470	481	991	java/lang/Exception
    //   485	496	991	java/lang/Exception
    //   503	510	991	java/lang/Exception
    //   516	524	427	java/lang/Exception
    //   524	531	427	java/lang/Exception
    //   535	543	427	java/lang/Exception
    //   543	554	427	java/lang/Exception
    //   562	574	991	java/lang/Exception
    //   578	587	991	java/lang/Exception
    //   591	598	991	java/lang/Exception
    //   606	616	986	java/lang/Exception
    //   616	628	986	java/lang/Exception
    //   628	638	986	java/lang/Exception
    //   647	656	986	java/lang/Exception
    //   656	664	986	java/lang/Exception
    //   675	685	986	java/lang/Exception
    //   685	699	986	java/lang/Exception
    //   715	724	986	java/lang/Exception
    //   730	739	986	java/lang/Exception
    //   742	751	986	java/lang/Exception
    //   754	763	986	java/lang/Exception
    //   766	775	986	java/lang/Exception
    //   775	782	986	java/lang/Exception
    //   782	823	986	java/lang/Exception
    //   829	836	986	java/lang/Exception
    //   836	842	986	java/lang/Exception
    //   845	852	986	java/lang/Exception
    //   852	858	986	java/lang/Exception
    //   861	870	986	java/lang/Exception
    //   873	882	986	java/lang/Exception
    //   882	923	986	java/lang/Exception
    //   926	975	986	java/lang/Exception
    //   975	979	986	java/lang/Exception
    //   979	983	986	java/lang/Exception
  }
  
  public boolean preprocessCdmaFdeaWap() {
    try {
      boolean bool;
      BitwiseInputStream bitwiseInputStream = new BitwiseInputStream();
      this(this.mUserData);
      if (bitwiseInputStream.read(8) != 0) {
        Rlog.e("SmsMessage", "Invalid FDEA WDP Header Message Identifier SUBPARAMETER_ID");
        return false;
      } 
      if (bitwiseInputStream.read(8) != 3) {
        Rlog.e("SmsMessage", "Invalid FDEA WDP Header Message Identifier SUBPARAM_LEN");
        return false;
      } 
      this.mBearerData.messageType = bitwiseInputStream.read(4);
      int i = bitwiseInputStream.read(8);
      i = i << 8 | bitwiseInputStream.read(8);
      this.mBearerData.messageId = i;
      this.mMessageRef = i;
      BearerData bearerData = this.mBearerData;
      if (bitwiseInputStream.read(1) == 1) {
        bool = true;
      } else {
        bool = false;
      } 
      bearerData.hasUserDataHeader = bool;
      if (this.mBearerData.hasUserDataHeader) {
        Rlog.e("SmsMessage", "Invalid FDEA WDP Header Message Identifier HEADER_IND");
        return false;
      } 
      bitwiseInputStream.skip(3);
      if (bitwiseInputStream.read(8) != 1) {
        Rlog.e("SmsMessage", "Invalid FDEA WDP Header User Data SUBPARAMETER_ID");
        return false;
      } 
      i = bitwiseInputStream.read(8);
      this.mBearerData.userData.msgEncoding = bitwiseInputStream.read(5);
      if (this.mBearerData.userData.msgEncoding != 0) {
        Rlog.e("SmsMessage", "Invalid FDEA WDP Header User Data MSG_ENCODING");
        return false;
      } 
      this.mBearerData.userData.numFields = bitwiseInputStream.read(8);
      i = i * 8 - 5 + 8;
      int j = this.mBearerData.userData.numFields * 8;
      if (j < i)
        i = j; 
      this.mBearerData.userData.payload = bitwiseInputStream.readByteArray(i);
      this.mUserData = this.mBearerData.userData.payload;
      return true;
    } catch (com.android.internal.util.BitwiseInputStream.AccessException accessException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Fail to preprocess FDEA WAP: ");
      stringBuilder.append(accessException);
      Rlog.e("SmsMessage", stringBuilder.toString());
      return false;
    } 
  }
  
  public void parseSms() {
    if (this.mEnvelope.teleService == 262144) {
      this.mBearerData = new BearerData();
      if (this.mEnvelope.bearerData != null)
        this.mBearerData.numberOfMessages = this.mEnvelope.bearerData[0] & 0xFF; 
      return;
    } 
    this.mBearerData = BearerData.decode(this.mEnvelope.bearerData);
    if (Rlog.isLoggable("CDMA:SMS", 2)) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("MT raw BearerData = '");
      byte[] arrayOfByte = this.mEnvelope.bearerData;
      stringBuilder1.append(HexDump.toHexString(arrayOfByte));
      stringBuilder1.append("'");
      String str = stringBuilder1.toString();
      Rlog.d("SmsMessage", str);
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("MT (decoded) BearerData = ");
      stringBuilder2.append(this.mBearerData);
      Rlog.d("SmsMessage", stringBuilder2.toString());
    } 
    this.mMessageRef = this.mBearerData.messageId;
    if (this.mBearerData.userData != null) {
      this.mUserData = this.mBearerData.userData.payload;
      this.mUserDataHeader = this.mBearerData.userData.userDataHeader;
      this.mMessageBody = this.mBearerData.userData.payloadStr;
    } 
    if (this.mOriginatingAddress != null)
      decodeSmsDisplayAddress(this.mOriginatingAddress); 
    if (this.mRecipientAddress != null)
      decodeSmsDisplayAddress(this.mRecipientAddress); 
    if (this.mBearerData.msgCenterTimeStamp != null)
      this.mScTimeMillis = this.mBearerData.msgCenterTimeStamp.toMillis(); 
    if (this.mBearerData.messageType == 4) {
      if (!this.mBearerData.messageStatusSet) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("DELIVERY_ACK message without msgStatus (");
        if (this.mUserData == null) {
          str = "also missing";
        } else {
          str = "does have";
        } 
        stringBuilder.append(str);
        stringBuilder.append(" userData).");
        String str = stringBuilder.toString();
        Rlog.d("SmsMessage", str);
        this.status = 2;
      } else {
        int i = this.mBearerData.errorClass << 8;
        this.status = i | this.mBearerData.messageStatus;
      } 
    } else if (this.mBearerData.messageType != 1 && this.mBearerData.messageType != 2) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unsupported message type: ");
      stringBuilder.append(this.mBearerData.messageType);
      throw new RuntimeException(stringBuilder.toString());
    } 
    if (this.mMessageBody != null) {
      parseMessageBody();
    } else {
      byte[] arrayOfByte = this.mUserData;
    } 
  }
  
  private void decodeSmsDisplayAddress(SmsAddress paramSmsAddress) {
    String str = TelephonyProperties.operator_idp_string().orElse(null);
    paramSmsAddress.address = new String(paramSmsAddress.origBytes);
    if (!TextUtils.isEmpty(str) && paramSmsAddress.address.startsWith(str)) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("+");
      stringBuilder1.append(paramSmsAddress.address.substring(str.length()));
      paramSmsAddress.address = stringBuilder1.toString();
    } else if (paramSmsAddress.ton == 1 && 
      paramSmsAddress.address.charAt(0) != '+') {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("+");
      stringBuilder1.append(paramSmsAddress.address);
      paramSmsAddress.address = stringBuilder1.toString();
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(" decodeSmsDisplayAddress = ");
    stringBuilder.append(paramSmsAddress.address);
    Rlog.pii("SmsMessage", stringBuilder.toString());
  }
  
  public SmsCbMessage parseBroadcastSms(String paramString, int paramInt1, int paramInt2) {
    BearerData bearerData = BearerData.decode(this.mEnvelope.bearerData, this.mEnvelope.serviceCategory);
    if (bearerData == null) {
      Rlog.w("SmsMessage", "BearerData.decode() returned null");
      return null;
    } 
    if (Rlog.isLoggable("CDMA:SMS", 2)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("MT raw BearerData = ");
      stringBuilder.append(HexDump.toHexString(this.mEnvelope.bearerData));
      Rlog.d("SmsMessage", stringBuilder.toString());
    } 
    SmsCbLocation smsCbLocation = new SmsCbLocation(paramString);
    int i = bearerData.messageId, j = this.mEnvelope.serviceCategory;
    return 
      
      new SmsCbMessage(2, 1, i, smsCbLocation, j, bearerData.getLanguage(), bearerData.userData.payloadStr, bearerData.priority, null, bearerData.cmasWarningInfo, paramInt1, paramInt2);
  }
  
  public byte[] getEnvelopeBearerData() {
    return this.mEnvelope.bearerData;
  }
  
  public int getEnvelopeServiceCategory() {
    return this.mEnvelope.serviceCategory;
  }
  
  public SmsConstants.MessageClass getMessageClass() {
    if (this.mBearerData.displayMode == 0)
      return SmsConstants.MessageClass.CLASS_0; 
    return SmsConstants.MessageClass.UNKNOWN;
  }
  
  public static int getNextMessageId() {
    // Byte code:
    //   0: ldc com/android/internal/telephony/cdma/SmsMessage
    //   2: monitorenter
    //   3: invokestatic cdma_msg_id : ()Ljava/util/Optional;
    //   6: iconst_1
    //   7: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   10: invokevirtual orElse : (Ljava/lang/Object;)Ljava/lang/Object;
    //   13: checkcast java/lang/Integer
    //   16: invokevirtual intValue : ()I
    //   19: istore_0
    //   20: iload_0
    //   21: ldc_w 65535
    //   24: irem
    //   25: iconst_1
    //   26: iadd
    //   27: istore_1
    //   28: iload_1
    //   29: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   32: invokestatic cdma_msg_id : (Ljava/lang/Integer;)V
    //   35: ldc 'CDMA:SMS'
    //   37: iconst_2
    //   38: invokestatic isLoggable : (Ljava/lang/String;I)Z
    //   41: ifeq -> 117
    //   44: new java/lang/StringBuilder
    //   47: astore_2
    //   48: aload_2
    //   49: invokespecial <init> : ()V
    //   52: aload_2
    //   53: ldc_w 'next persist.radio.cdma.msgid = '
    //   56: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   59: pop
    //   60: aload_2
    //   61: iload_1
    //   62: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   65: pop
    //   66: ldc 'SmsMessage'
    //   68: aload_2
    //   69: invokevirtual toString : ()Ljava/lang/String;
    //   72: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   75: pop
    //   76: new java/lang/StringBuilder
    //   79: astore_2
    //   80: aload_2
    //   81: invokespecial <init> : ()V
    //   84: aload_2
    //   85: ldc_w 'readback gets '
    //   88: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   91: pop
    //   92: aload_2
    //   93: invokestatic cdma_msg_id : ()Ljava/util/Optional;
    //   96: iconst_1
    //   97: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   100: invokevirtual orElse : (Ljava/lang/Object;)Ljava/lang/Object;
    //   103: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   106: pop
    //   107: ldc 'SmsMessage'
    //   109: aload_2
    //   110: invokevirtual toString : ()Ljava/lang/String;
    //   113: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   116: pop
    //   117: goto -> 153
    //   120: astore_2
    //   121: new java/lang/StringBuilder
    //   124: astore_3
    //   125: aload_3
    //   126: invokespecial <init> : ()V
    //   129: aload_3
    //   130: ldc_w 'set nextMessage ID failed: '
    //   133: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   136: pop
    //   137: aload_3
    //   138: aload_2
    //   139: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   142: pop
    //   143: ldc 'SmsMessage'
    //   145: aload_3
    //   146: invokevirtual toString : ()Ljava/lang/String;
    //   149: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   152: pop
    //   153: ldc com/android/internal/telephony/cdma/SmsMessage
    //   155: monitorexit
    //   156: iload_0
    //   157: ireturn
    //   158: astore_2
    //   159: ldc com/android/internal/telephony/cdma/SmsMessage
    //   161: monitorexit
    //   162: aload_2
    //   163: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #951	-> 3
    //   #952	-> 20
    //   #954	-> 28
    //   #955	-> 35
    //   #956	-> 44
    //   #957	-> 76
    //   #961	-> 117
    //   #959	-> 120
    //   #960	-> 121
    //   #962	-> 153
    //   #950	-> 158
    // Exception table:
    //   from	to	target	type
    //   3	20	158	finally
    //   28	35	120	java/lang/RuntimeException
    //   28	35	158	finally
    //   35	44	120	java/lang/RuntimeException
    //   35	44	158	finally
    //   44	76	120	java/lang/RuntimeException
    //   44	76	158	finally
    //   76	117	120	java/lang/RuntimeException
    //   76	117	158	finally
    //   121	153	158	finally
  }
  
  private static SubmitPdu privateGetSubmitPdu(String paramString, boolean paramBoolean, UserData paramUserData) {
    return privateGetSubmitPdu(paramString, paramBoolean, paramUserData, -1);
  }
  
  private static SubmitPdu privateGetSubmitPdu(String paramString, boolean paramBoolean, UserData paramUserData, int paramInt) {
    paramString = PhoneNumberUtils.cdmaCheckAndProcessPlusCodeForSms(paramString);
    CdmaSmsAddress cdmaSmsAddress = CdmaSmsAddress.parse(paramString);
    if (cdmaSmsAddress == null)
      return null; 
    BearerData bearerData = new BearerData();
    bearerData.messageType = 2;
    bearerData.messageId = getNextMessageId();
    bearerData.deliveryAckReq = paramBoolean;
    bearerData.userAckReq = false;
    bearerData.readAckReq = false;
    bearerData.reportReq = false;
    if (paramInt >= 0 && paramInt <= 3) {
      bearerData.priorityIndicatorSet = true;
      bearerData.priority = paramInt;
    } 
    bearerData.userData = paramUserData;
    byte[] arrayOfByte = BearerData.encode(bearerData);
    if (arrayOfByte == null)
      return null; 
    if (Rlog.isLoggable("CDMA:SMS", 2)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("MO (encoded) BearerData = ");
      stringBuilder.append(bearerData);
      Rlog.d("SmsMessage", stringBuilder.toString());
      stringBuilder = new StringBuilder();
      stringBuilder.append("MO raw BearerData = '");
      stringBuilder.append(HexDump.toHexString(arrayOfByte));
      stringBuilder.append("'");
      Rlog.d("SmsMessage", stringBuilder.toString());
    } 
    if (bearerData.hasUserDataHeader && paramUserData.msgEncoding != 2) {
      paramInt = 4101;
    } else {
      paramInt = 4098;
    } 
    SmsEnvelope smsEnvelope = new SmsEnvelope();
    smsEnvelope.messageType = 0;
    smsEnvelope.teleService = paramInt;
    smsEnvelope.destAddress = cdmaSmsAddress;
    smsEnvelope.bearerReply = 1;
    smsEnvelope.bearerData = arrayOfByte;
    try {
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      this(100);
      DataOutputStream dataOutputStream = new DataOutputStream();
      this(byteArrayOutputStream);
      dataOutputStream.writeInt(smsEnvelope.teleService);
      dataOutputStream.writeInt(0);
      dataOutputStream.writeInt(0);
      dataOutputStream.write(cdmaSmsAddress.digitMode);
      dataOutputStream.write(cdmaSmsAddress.numberMode);
      dataOutputStream.write(cdmaSmsAddress.ton);
      dataOutputStream.write(cdmaSmsAddress.numberPlan);
      dataOutputStream.write(cdmaSmsAddress.numberOfDigits);
      dataOutputStream.write(cdmaSmsAddress.origBytes, 0, cdmaSmsAddress.origBytes.length);
      dataOutputStream.write(0);
      dataOutputStream.write(0);
      dataOutputStream.write(0);
      dataOutputStream.write(arrayOfByte.length);
      dataOutputStream.write(arrayOfByte, 0, arrayOfByte.length);
      dataOutputStream.close();
      SubmitPdu submitPdu = new SubmitPdu();
      this();
      submitPdu.encodedMessage = byteArrayOutputStream.toByteArray();
      submitPdu.encodedScAddress = null;
      return submitPdu;
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("creating SubmitPdu failed: ");
      stringBuilder.append(iOException);
      Rlog.e("SmsMessage", stringBuilder.toString());
      return null;
    } 
  }
  
  public static SubmitPdu getDeliverPdu(String paramString1, String paramString2, long paramLong) {
    if (paramString1 == null || paramString2 == null)
      return null; 
    CdmaSmsAddress cdmaSmsAddress = CdmaSmsAddress.parse(paramString1);
    if (cdmaSmsAddress == null)
      return null; 
    BearerData bearerData = new BearerData();
    bearerData.messageType = 1;
    bearerData.messageId = 0;
    bearerData.deliveryAckReq = false;
    bearerData.userAckReq = false;
    bearerData.readAckReq = false;
    bearerData.reportReq = false;
    bearerData.userData = new UserData();
    bearerData.userData.payloadStr = paramString2;
    bearerData.msgCenterTimeStamp = BearerData.TimeStamp.fromMillis(paramLong);
    byte[] arrayOfByte = BearerData.encode(bearerData);
    if (arrayOfByte == null)
      return null; 
    try {
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      this(100);
      DataOutputStream dataOutputStream = new DataOutputStream();
      this(byteArrayOutputStream);
      dataOutputStream.writeInt(4098);
      dataOutputStream.writeInt(0);
      dataOutputStream.writeInt(0);
      dataOutputStream.write(cdmaSmsAddress.digitMode);
      dataOutputStream.write(cdmaSmsAddress.numberMode);
      dataOutputStream.write(cdmaSmsAddress.ton);
      dataOutputStream.write(cdmaSmsAddress.numberPlan);
      dataOutputStream.write(cdmaSmsAddress.numberOfDigits);
      dataOutputStream.write(cdmaSmsAddress.origBytes, 0, cdmaSmsAddress.origBytes.length);
      dataOutputStream.write(0);
      dataOutputStream.write(0);
      dataOutputStream.write(0);
      dataOutputStream.write(arrayOfByte.length);
      dataOutputStream.write(arrayOfByte, 0, arrayOfByte.length);
      dataOutputStream.close();
      SubmitPdu submitPdu = new SubmitPdu();
      this();
      submitPdu.encodedMessage = byteArrayOutputStream.toByteArray();
      submitPdu.encodedScAddress = null;
      return submitPdu;
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("creating Deliver PDU failed: ");
      stringBuilder.append(iOException);
      Rlog.e("SmsMessage", stringBuilder.toString());
      return null;
    } 
  }
  
  public void createPdu() {
    SmsEnvelope smsEnvelope = this.mEnvelope;
    CdmaSmsAddress cdmaSmsAddress = smsEnvelope.origAddress;
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(100);
    DataOutputStream dataOutputStream = new DataOutputStream(new BufferedOutputStream(byteArrayOutputStream));
    try {
      dataOutputStream.writeInt(smsEnvelope.messageType);
      dataOutputStream.writeInt(smsEnvelope.teleService);
      dataOutputStream.writeInt(smsEnvelope.serviceCategory);
      dataOutputStream.writeByte(cdmaSmsAddress.digitMode);
      dataOutputStream.writeByte(cdmaSmsAddress.numberMode);
      dataOutputStream.writeByte(cdmaSmsAddress.ton);
      dataOutputStream.writeByte(cdmaSmsAddress.numberPlan);
      dataOutputStream.writeByte(cdmaSmsAddress.numberOfDigits);
      dataOutputStream.write(cdmaSmsAddress.origBytes, 0, cdmaSmsAddress.origBytes.length);
      dataOutputStream.writeInt(smsEnvelope.bearerReply);
      dataOutputStream.writeByte(smsEnvelope.replySeqNo);
      dataOutputStream.writeByte(smsEnvelope.errorClass);
      dataOutputStream.writeByte(smsEnvelope.causeCode);
      dataOutputStream.writeInt(smsEnvelope.bearerData.length);
      dataOutputStream.write(smsEnvelope.bearerData, 0, smsEnvelope.bearerData.length);
      dataOutputStream.close();
      this.mPdu = byteArrayOutputStream.toByteArray();
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("createPdu: conversion from object to byte array failed: ");
      stringBuilder.append(iOException);
      Rlog.e("SmsMessage", stringBuilder.toString());
    } 
  }
  
  public static byte convertDtmfToAscii(byte paramByte) {
    switch (paramByte) {
      default:
        b = 32;
        return b;
      case 15:
        b = 67;
        return b;
      case 14:
        b = 66;
        return b;
      case 13:
        b = 65;
        return b;
      case 12:
        b = 35;
        return b;
      case 11:
        b = 42;
        return b;
      case 10:
        b = 48;
        return b;
      case 9:
        b = 57;
        return b;
      case 8:
        b = 56;
        return b;
      case 7:
        b = 55;
        return b;
      case 6:
        b = 54;
        return b;
      case 5:
        b = 53;
        return b;
      case 4:
        b = 52;
        return b;
      case 3:
        b = 51;
        return b;
      case 2:
        b = 50;
        return b;
      case 1:
        b = 49;
        return b;
      case 0:
        break;
    } 
    byte b = 68;
    return b;
  }
  
  public int getNumOfVoicemails() {
    return this.mBearerData.numberOfMessages;
  }
  
  public byte[] getIncomingSmsFingerprint() {
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    byteArrayOutputStream.write(this.mEnvelope.serviceCategory);
    byteArrayOutputStream.write(this.mEnvelope.teleService);
    byteArrayOutputStream.write(this.mEnvelope.origAddress.origBytes, 0, this.mEnvelope.origAddress.origBytes.length);
    byteArrayOutputStream.write(this.mEnvelope.bearerData, 0, this.mEnvelope.bearerData.length);
    if (this.mEnvelope.origSubaddress != null && this.mEnvelope.origSubaddress.origBytes != null)
      byteArrayOutputStream.write(this.mEnvelope.origSubaddress.origBytes, 0, this.mEnvelope.origSubaddress.origBytes.length); 
    return byteArrayOutputStream.toByteArray();
  }
  
  public ArrayList<CdmaSmsCbProgramData> getSmsCbProgramData() {
    return this.mBearerData.serviceCategoryProgramData;
  }
}
