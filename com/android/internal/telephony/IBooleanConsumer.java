package com.android.internal.telephony;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IBooleanConsumer extends IInterface {
  void accept(boolean paramBoolean) throws RemoteException;
  
  class Default implements IBooleanConsumer {
    public void accept(boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IBooleanConsumer {
    private static final String DESCRIPTOR = "com.android.internal.telephony.IBooleanConsumer";
    
    static final int TRANSACTION_accept = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.IBooleanConsumer");
    }
    
    public static IBooleanConsumer asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.IBooleanConsumer");
      if (iInterface != null && iInterface instanceof IBooleanConsumer)
        return (IBooleanConsumer)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "accept";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.telephony.IBooleanConsumer");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.telephony.IBooleanConsumer");
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      accept(bool);
      return true;
    }
    
    private static class Proxy implements IBooleanConsumer {
      public static IBooleanConsumer sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.IBooleanConsumer";
      }
      
      public void accept(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.telephony.IBooleanConsumer");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, null, 1);
          if (!bool1 && IBooleanConsumer.Stub.getDefaultImpl() != null) {
            IBooleanConsumer.Stub.getDefaultImpl().accept(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IBooleanConsumer param1IBooleanConsumer) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IBooleanConsumer != null) {
          Proxy.sDefaultImpl = param1IBooleanConsumer;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IBooleanConsumer getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
