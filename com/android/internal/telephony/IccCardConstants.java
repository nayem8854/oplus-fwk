package com.android.internal.telephony;

public class IccCardConstants {
  public static final String INTENT_KEY_ICC_STATE = "ss";
  
  public static final String INTENT_KEY_LOCKED_REASON = "reason";
  
  public static final String INTENT_KEY_PBM_STATE = "pbstate";
  
  public static final String INTENT_VALUE_ABSENT_ON_PERM_DISABLED = "PERM_DISABLED";
  
  public static final String INTENT_VALUE_ICC_ABSENT = "ABSENT";
  
  public static final String INTENT_VALUE_ICC_CARD_IO_ERROR = "CARD_IO_ERROR";
  
  public static final String INTENT_VALUE_ICC_CARD_RESTRICTED = "CARD_RESTRICTED";
  
  public static final String INTENT_VALUE_ICC_IMSI = "IMSI";
  
  public static final String INTENT_VALUE_ICC_LOADED = "LOADED";
  
  public static final String INTENT_VALUE_ICC_LOCKED = "LOCKED";
  
  public static final String INTENT_VALUE_ICC_NOT_READY = "NOT_READY";
  
  public static final String INTENT_VALUE_ICC_PRESENT = "PRESENT";
  
  public static final String INTENT_VALUE_ICC_READY = "READY";
  
  public static final String INTENT_VALUE_ICC_UNKNOWN = "UNKNOWN";
  
  public static final String INTENT_VALUE_LOCKED_NETWORK = "NETWORK";
  
  public static final String INTENT_VALUE_LOCKED_ON_PIN = "PIN";
  
  public static final String INTENT_VALUE_LOCKED_ON_PUK = "PUK";
  
  public static final String INTENT_VALUE_PBM_READY = "PBREADY";
  
  public enum State {
    ABSENT, CARD_IO_ERROR, CARD_RESTRICTED, IMSI, LOADED, NETWORK_LOCKED, NOT_READY, PERM_DISABLED, PIN_REQUIRED, PUK_REQUIRED, READY, UNKNOWN;
    
    private static final State[] $VALUES;
    
    static {
      NETWORK_LOCKED = new State("NETWORK_LOCKED", 4);
      READY = new State("READY", 5);
      NOT_READY = new State("NOT_READY", 6);
      PERM_DISABLED = new State("PERM_DISABLED", 7);
      CARD_IO_ERROR = new State("CARD_IO_ERROR", 8);
      CARD_RESTRICTED = new State("CARD_RESTRICTED", 9);
      LOADED = new State("LOADED", 10);
      State state = new State("IMSI", 11);
      $VALUES = new State[] { 
          UNKNOWN, ABSENT, PIN_REQUIRED, PUK_REQUIRED, NETWORK_LOCKED, READY, NOT_READY, PERM_DISABLED, CARD_IO_ERROR, CARD_RESTRICTED, 
          LOADED, state };
    }
    
    public boolean isPinLocked() {
      return (this == PIN_REQUIRED || this == PUK_REQUIRED);
    }
    
    public boolean iccCardExist() {
      return (this == PIN_REQUIRED || this == PUK_REQUIRED || this == NETWORK_LOCKED || this == READY || this == NOT_READY || this == PERM_DISABLED || this == CARD_IO_ERROR || this == CARD_RESTRICTED || this == LOADED);
    }
    
    public static State intToState(int param1Int) throws IllegalArgumentException {
      switch (param1Int) {
        default:
          throw new IllegalArgumentException();
        case 10:
          return LOADED;
        case 9:
          return CARD_RESTRICTED;
        case 8:
          return CARD_IO_ERROR;
        case 7:
          return PERM_DISABLED;
        case 6:
          return NOT_READY;
        case 5:
          return READY;
        case 4:
          return NETWORK_LOCKED;
        case 3:
          return PUK_REQUIRED;
        case 2:
          return PIN_REQUIRED;
        case 1:
          return ABSENT;
        case 0:
          break;
      } 
      return UNKNOWN;
    }
  }
}
