package com.android.internal.telephony;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IIntegerConsumer extends IInterface {
  void accept(int paramInt) throws RemoteException;
  
  class Default implements IIntegerConsumer {
    public void accept(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IIntegerConsumer {
    private static final String DESCRIPTOR = "com.android.internal.telephony.IIntegerConsumer";
    
    static final int TRANSACTION_accept = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.IIntegerConsumer");
    }
    
    public static IIntegerConsumer asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.IIntegerConsumer");
      if (iInterface != null && iInterface instanceof IIntegerConsumer)
        return (IIntegerConsumer)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "accept";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.telephony.IIntegerConsumer");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.telephony.IIntegerConsumer");
      param1Int1 = param1Parcel1.readInt();
      accept(param1Int1);
      return true;
    }
    
    private static class Proxy implements IIntegerConsumer {
      public static IIntegerConsumer sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.IIntegerConsumer";
      }
      
      public void accept(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IIntegerConsumer");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IIntegerConsumer.Stub.getDefaultImpl() != null) {
            IIntegerConsumer.Stub.getDefaultImpl().accept(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IIntegerConsumer param1IIntegerConsumer) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IIntegerConsumer != null) {
          Proxy.sDefaultImpl = param1IIntegerConsumer;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IIntegerConsumer getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
