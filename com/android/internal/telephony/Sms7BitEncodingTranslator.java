package com.android.internal.telephony;

import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.util.SparseIntArray;
import com.android.internal.telephony.cdma.sms.UserData;
import com.android.internal.telephony.util.TelephonyUtils;
import com.android.internal.telephony.util.XmlUtils;
import com.android.telephony.Rlog;
import org.xmlpull.v1.XmlPullParser;

public class Sms7BitEncodingTranslator {
  private static final boolean DBG = TelephonyUtils.IS_DEBUGGABLE;
  
  private static final String TAG = "Sms7BitEncodingTranslator";
  
  private static final String XML_CHARACTOR_TAG = "Character";
  
  private static final String XML_FROM_TAG = "from";
  
  private static final String XML_START_TAG = "SmsEnforce7BitTranslationTable";
  
  private static final String XML_TO_TAG = "to";
  
  private static final String XML_TRANSLATION_TYPE_TAG = "TranslationType";
  
  private static boolean mIs7BitTranslationTableLoaded = false;
  
  private static SparseIntArray mTranslationTable = null;
  
  private static SparseIntArray mTranslationTableCDMA;
  
  private static SparseIntArray mTranslationTableCommon = null;
  
  private static SparseIntArray mTranslationTableGSM = null;
  
  static {
    mTranslationTableCDMA = null;
  }
  
  public static String translate(CharSequence paramCharSequence, boolean paramBoolean) {
    if (paramCharSequence == null) {
      Rlog.w("Sms7BitEncodingTranslator", "Null message can not be translated");
      return null;
    } 
    int i = paramCharSequence.length();
    if (i <= 0)
      return ""; 
    if (!mIs7BitTranslationTableLoaded) {
      mTranslationTableCommon = new SparseIntArray();
      mTranslationTableGSM = new SparseIntArray();
      mTranslationTableCDMA = new SparseIntArray();
      load7BitTranslationTableFromXml();
      mIs7BitTranslationTableLoaded = true;
    } 
    SparseIntArray sparseIntArray = mTranslationTableCommon;
    if (sparseIntArray == null || sparseIntArray.size() <= 0) {
      sparseIntArray = mTranslationTableGSM;
      if (sparseIntArray == null || 
        sparseIntArray.size() <= 0) {
        sparseIntArray = mTranslationTableCDMA;
        if (sparseIntArray == null || 
          sparseIntArray.size() <= 0)
          return null; 
      } 
    } 
    char[] arrayOfChar = new char[i];
    for (byte b = 0; b < i; b++)
      arrayOfChar[b] = translateIfNeeded(paramCharSequence.charAt(b), paramBoolean); 
    return String.valueOf(arrayOfChar);
  }
  
  private static char translateIfNeeded(char paramChar, boolean paramBoolean) {
    if (noTranslationNeeded(paramChar, paramBoolean)) {
      if (DBG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("No translation needed for ");
        stringBuilder.append(Integer.toHexString(paramChar));
        Rlog.v("Sms7BitEncodingTranslator", stringBuilder.toString());
      } 
      return paramChar;
    } 
    int i = -1;
    SparseIntArray sparseIntArray = mTranslationTableCommon;
    if (sparseIntArray != null)
      i = sparseIntArray.get(paramChar, -1); 
    int j = i;
    if (i == -1)
      if (paramBoolean) {
        sparseIntArray = mTranslationTableCDMA;
        j = i;
        if (sparseIntArray != null)
          j = sparseIntArray.get(paramChar, -1); 
      } else {
        sparseIntArray = mTranslationTableGSM;
        j = i;
        if (sparseIntArray != null)
          j = sparseIntArray.get(paramChar, -1); 
      }  
    if (j != -1) {
      if (DBG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(Integer.toHexString(paramChar));
        stringBuilder.append(" (");
        stringBuilder.append(paramChar);
        stringBuilder.append(") translated to ");
        stringBuilder.append(Integer.toHexString(j));
        stringBuilder.append(" (");
        stringBuilder.append((char)j);
        stringBuilder.append(")");
        String str = stringBuilder.toString();
        Rlog.v("Sms7BitEncodingTranslator", str);
      } 
      return (char)j;
    } 
    if (DBG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("No translation found for ");
      stringBuilder.append(Integer.toHexString(paramChar));
      stringBuilder.append("! Replacing for empty space");
      Rlog.w("Sms7BitEncodingTranslator", stringBuilder.toString());
    } 
    return ' ';
  }
  
  private static boolean noTranslationNeeded(char paramChar, boolean paramBoolean) {
    if (paramBoolean) {
      if (GsmAlphabet.isGsmSeptets(paramChar) && UserData.charToAscii.get(paramChar, -1) != -1) {
        paramBoolean = true;
      } else {
        paramBoolean = false;
      } 
      return paramBoolean;
    } 
    return GsmAlphabet.isGsmSeptets(paramChar);
  }
  
  private static void load7BitTranslationTableFromXml() {
    XmlResourceParser xmlResourceParser = null;
    Resources resources = Resources.getSystem();
    if (!false) {
      if (DBG)
        Rlog.d("Sms7BitEncodingTranslator", "load7BitTranslationTableFromXml: open normal file"); 
      xmlResourceParser = resources.getXml(18284564);
    } 
    try {
      XmlUtils.beginDocument((XmlPullParser)xmlResourceParser, "SmsEnforce7BitTranslationTable");
      while (true) {
        StringBuilder stringBuilder;
        XmlUtils.nextElement((XmlPullParser)xmlResourceParser);
        String str = xmlResourceParser.getName();
        if (DBG) {
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("tag: ");
          stringBuilder1.append(str);
          Rlog.d("Sms7BitEncodingTranslator", stringBuilder1.toString());
        } 
        if ("TranslationType".equals(str)) {
          String str1 = xmlResourceParser.getAttributeValue(null, "Type");
          if (DBG) {
            StringBuilder stringBuilder1 = new StringBuilder();
            this();
            stringBuilder1.append("type: ");
            stringBuilder1.append(str1);
            Rlog.d("Sms7BitEncodingTranslator", stringBuilder1.toString());
          } 
          if (str1.equals("common")) {
            mTranslationTable = mTranslationTableCommon;
            continue;
          } 
          if (str1.equals("gsm")) {
            mTranslationTable = mTranslationTableGSM;
            continue;
          } 
          if (str1.equals("cdma")) {
            mTranslationTable = mTranslationTableCDMA;
            continue;
          } 
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Error Parsing 7BitTranslationTable: found incorrect type");
          stringBuilder.append(str1);
          Rlog.e("Sms7BitEncodingTranslator", stringBuilder.toString());
          continue;
        } 
        if ("Character".equals(stringBuilder) && mTranslationTable != null) {
          int i = xmlResourceParser.getAttributeUnsignedIntValue(null, "from", -1);
          int j = xmlResourceParser.getAttributeUnsignedIntValue(null, "to", -1);
          if (i != -1 && j != -1) {
            if (DBG) {
              StringBuilder stringBuilder1 = new StringBuilder();
              this();
              stringBuilder1.append("Loading mapping ");
              String str2 = Integer.toHexString(i);
              stringBuilder1.append(str2.toUpperCase());
              stringBuilder1.append(" -> ");
              str2 = Integer.toHexString(j);
              stringBuilder1.append(str2.toUpperCase());
              String str1 = stringBuilder1.toString();
              Rlog.d("Sms7BitEncodingTranslator", str1);
            } 
            mTranslationTable.put(i, j);
            continue;
          } 
          Rlog.d("Sms7BitEncodingTranslator", "Invalid translation table file format");
          continue;
        } 
        break;
      } 
      if (DBG)
        Rlog.d("Sms7BitEncodingTranslator", "load7BitTranslationTableFromXml: parsing successful, file loaded"); 
      if (xmlResourceParser instanceof XmlResourceParser) {
        xmlResourceParser.close();
        return;
      } 
    } catch (Exception exception) {
      Rlog.e("Sms7BitEncodingTranslator", "Got exception while loading 7BitTranslationTable file.", exception);
      if (xmlResourceParser instanceof XmlResourceParser) {
        xmlResourceParser.close();
        return;
      } 
    } finally {}
  }
}
