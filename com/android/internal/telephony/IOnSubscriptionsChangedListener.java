package com.android.internal.telephony;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOnSubscriptionsChangedListener extends IInterface {
  void onSubscriptionsChanged() throws RemoteException;
  
  class Default implements IOnSubscriptionsChangedListener {
    public void onSubscriptionsChanged() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOnSubscriptionsChangedListener {
    private static final String DESCRIPTOR = "com.android.internal.telephony.IOnSubscriptionsChangedListener";
    
    static final int TRANSACTION_onSubscriptionsChanged = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.IOnSubscriptionsChangedListener");
    }
    
    public static IOnSubscriptionsChangedListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.IOnSubscriptionsChangedListener");
      if (iInterface != null && iInterface instanceof IOnSubscriptionsChangedListener)
        return (IOnSubscriptionsChangedListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onSubscriptionsChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.telephony.IOnSubscriptionsChangedListener");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.telephony.IOnSubscriptionsChangedListener");
      onSubscriptionsChanged();
      return true;
    }
    
    private static class Proxy implements IOnSubscriptionsChangedListener {
      public static IOnSubscriptionsChangedListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.IOnSubscriptionsChangedListener";
      }
      
      public void onSubscriptionsChanged() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IOnSubscriptionsChangedListener");
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOnSubscriptionsChangedListener.Stub.getDefaultImpl() != null) {
            IOnSubscriptionsChangedListener.Stub.getDefaultImpl().onSubscriptionsChanged();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOnSubscriptionsChangedListener param1IOnSubscriptionsChangedListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOnSubscriptionsChangedListener != null) {
          Proxy.sDefaultImpl = param1IOnSubscriptionsChangedListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOnSubscriptionsChangedListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
