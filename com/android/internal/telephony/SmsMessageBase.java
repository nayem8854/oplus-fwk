package com.android.internal.telephony;

import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.util.Patterns;
import java.text.BreakIterator;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class SmsMessageBase {
  public static final Pattern NAME_ADDR_EMAIL_PATTERN = Pattern.compile("\\s*(\"[^\"]*\"|[^<>\"]+)\\s*<([^<>]+)>\\s*");
  
  protected int mStatusOnIcc = -1;
  
  protected int mIndexOnIcc = -1;
  
  protected String mEmailBody;
  
  protected String mEmailFrom;
  
  protected boolean mIsEmail;
  
  protected boolean mIsMwi;
  
  protected String mMessageBody;
  
  public int mMessageRef;
  
  protected boolean mMwiDontStore;
  
  protected boolean mMwiSense;
  
  protected SmsAddress mOriginatingAddress;
  
  protected byte[] mPdu;
  
  protected String mPseudoSubject;
  
  protected SmsAddress mRecipientAddress;
  
  protected String mScAddress;
  
  protected long mScTimeMillis;
  
  protected byte[] mUserData;
  
  protected SmsHeader mUserDataHeader;
  
  public static abstract class SubmitPduBase {
    public byte[] encodedMessage;
    
    public byte[] encodedScAddress;
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("SubmitPdu: encodedScAddress = ");
      byte[] arrayOfByte = this.encodedScAddress;
      stringBuilder.append(Arrays.toString(arrayOfByte));
      stringBuilder.append(", encodedMessage = ");
      arrayOfByte = this.encodedMessage;
      stringBuilder.append(Arrays.toString(arrayOfByte));
      return stringBuilder.toString();
    }
  }
  
  public String getServiceCenterAddress() {
    return this.mScAddress;
  }
  
  public String getOriginatingAddress() {
    SmsAddress smsAddress = this.mOriginatingAddress;
    if (smsAddress == null)
      return null; 
    return smsAddress.getAddressString();
  }
  
  public String getDisplayOriginatingAddress() {
    if (this.mIsEmail)
      return this.mEmailFrom; 
    return getOriginatingAddress();
  }
  
  public String getMessageBody() {
    return this.mMessageBody;
  }
  
  public String getDisplayMessageBody() {
    if (this.mIsEmail)
      return this.mEmailBody; 
    return getMessageBody();
  }
  
  public String getPseudoSubject() {
    String str1 = this.mPseudoSubject, str2 = str1;
    if (str1 == null)
      str2 = ""; 
    return str2;
  }
  
  public long getTimestampMillis() {
    return this.mScTimeMillis;
  }
  
  public boolean isEmail() {
    return this.mIsEmail;
  }
  
  public String getEmailBody() {
    return this.mEmailBody;
  }
  
  public String getEmailFrom() {
    return this.mEmailFrom;
  }
  
  public byte[] getUserData() {
    return this.mUserData;
  }
  
  public SmsHeader getUserDataHeader() {
    return this.mUserDataHeader;
  }
  
  public byte[] getPdu() {
    return this.mPdu;
  }
  
  public int getStatusOnIcc() {
    return this.mStatusOnIcc;
  }
  
  public int getIndexOnIcc() {
    return this.mIndexOnIcc;
  }
  
  protected void parseMessageBody() {
    SmsAddress smsAddress = this.mOriginatingAddress;
    if (smsAddress != null && smsAddress.couldBeEmailGateway())
      extractEmailAddressFromMessageBody(); 
  }
  
  private static String extractAddrSpec(String paramString) {
    Matcher matcher = NAME_ADDR_EMAIL_PATTERN.matcher(paramString);
    if (matcher.matches())
      return matcher.group(2); 
    return paramString;
  }
  
  public static boolean isEmailAddress(String paramString) {
    if (TextUtils.isEmpty(paramString))
      return false; 
    paramString = extractAddrSpec(paramString);
    Matcher matcher = Patterns.EMAIL_ADDRESS.matcher(paramString);
    return matcher.matches();
  }
  
  protected void extractEmailAddressFromMessageBody() {
    String[] arrayOfString = this.mMessageBody.split("( /)|( )", 2);
    if (arrayOfString.length < 2)
      return; 
    String str = arrayOfString[0];
    this.mEmailBody = arrayOfString[1];
    this.mIsEmail = isEmailAddress(str);
  }
  
  public static int findNextUnicodePosition(int paramInt1, int paramInt2, CharSequence paramCharSequence) {
    int i = Math.min(paramInt2 / 2 + paramInt1, paramCharSequence.length());
    paramInt2 = i;
    if (i < paramCharSequence.length()) {
      BreakIterator breakIterator = BreakIterator.getCharacterInstance();
      breakIterator.setText(paramCharSequence.toString());
      paramInt2 = i;
      if (!breakIterator.isBoundary(i)) {
        paramInt2 = breakIterator.preceding(i);
        while (paramInt2 + 4 <= i) {
          int j = Character.codePointAt(paramCharSequence, paramInt2);
          if (isRegionalIndicatorSymbol(j)) {
            j = Character.codePointAt(paramCharSequence, paramInt2 + 2);
            if (isRegionalIndicatorSymbol(j))
              paramInt2 += 4; 
          } 
        } 
        if (paramInt2 <= paramInt1) {
          paramInt2 = i;
          if (Character.isHighSurrogate(paramCharSequence.charAt(i - 1)))
            paramInt2 = i - 1; 
        } 
      } 
    } 
    return paramInt2;
  }
  
  private static boolean isRegionalIndicatorSymbol(int paramInt) {
    boolean bool;
    if (127462 <= paramInt && paramInt <= 127487) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static GsmAlphabet.TextEncodingDetails calcUnicodeEncodingDetails(CharSequence paramCharSequence) {
    GsmAlphabet.TextEncodingDetails textEncodingDetails = new GsmAlphabet.TextEncodingDetails();
    int i = paramCharSequence.length() * 2;
    textEncodingDetails.codeUnitSize = 3;
    textEncodingDetails.codeUnitCount = paramCharSequence.length();
    if (i > 140) {
      char c = '';
      int j = c;
      if (!SmsMessage.hasEmsSupport()) {
        j = c;
        if (i <= (134 - 2) * 9)
          j = 134 - 2; 
      } 
      i = 0;
      c = Character.MIN_VALUE;
      while (i < paramCharSequence.length()) {
        int k = findNextUnicodePosition(i, j, paramCharSequence);
        if (k == paramCharSequence.length()) {
          int m = j / 2;
          textEncodingDetails.codeUnitsRemaining = m + i - paramCharSequence.length();
        } 
        i = k;
        c++;
      } 
      textEncodingDetails.msgCount = c;
    } else {
      textEncodingDetails.msgCount = 1;
      textEncodingDetails.codeUnitsRemaining = (140 - i) / 2;
    } 
    return textEncodingDetails;
  }
  
  public String getRecipientAddress() {
    SmsAddress smsAddress = this.mRecipientAddress;
    if (smsAddress == null)
      return null; 
    return smsAddress.getAddressString();
  }
  
  public int getEncodingType() {
    return 0;
  }
  
  public abstract SmsConstants.MessageClass getMessageClass();
  
  public abstract int getProtocolIdentifier();
  
  public abstract int getStatus();
  
  public abstract boolean isCphsMwiMessage();
  
  public abstract boolean isMWIClearMessage();
  
  public abstract boolean isMWISetMessage();
  
  public abstract boolean isMwiDontStore();
  
  public abstract boolean isReplace();
  
  public abstract boolean isReplyPathPresent();
  
  public abstract boolean isStatusReportMessage();
}
