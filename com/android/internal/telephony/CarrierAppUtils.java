package com.android.internal.telephony;

import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.CarrierAssociatedAppEntry;
import android.os.SystemConfigManager;
import android.os.UserHandle;
import android.telephony.TelephonyManager;
import android.util.ArrayMap;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class CarrierAppUtils {
  private static final boolean DEBUG = false;
  
  private static final String TAG = "CarrierAppUtils";
  
  public static void disableCarrierAppsUntilPrivileged(String paramString, TelephonyManager paramTelephonyManager, int paramInt, Context paramContext) {
    // Byte code:
    //   0: ldc com/android/internal/telephony/CarrierAppUtils
    //   2: monitorenter
    //   3: aload_3
    //   4: ldc android/os/SystemConfigManager
    //   6: invokevirtual getSystemService : (Ljava/lang/Class;)Ljava/lang/Object;
    //   9: checkcast android/os/SystemConfigManager
    //   12: astore #4
    //   14: aload #4
    //   16: invokevirtual getDisabledUntilUsedPreinstalledCarrierApps : ()Ljava/util/Set;
    //   19: astore #5
    //   21: aload #4
    //   23: invokevirtual getDisabledUntilUsedPreinstalledCarrierAssociatedAppEntries : ()Ljava/util/Map;
    //   26: astore #6
    //   28: aload_3
    //   29: iload_2
    //   30: invokestatic getContentResolverForUser : (Landroid/content/Context;I)Landroid/content/ContentResolver;
    //   33: astore #4
    //   35: aload_0
    //   36: aload_1
    //   37: aload #4
    //   39: iload_2
    //   40: aload #5
    //   42: aload #6
    //   44: aload_3
    //   45: invokestatic disableCarrierAppsUntilPrivileged : (Ljava/lang/String;Landroid/telephony/TelephonyManager;Landroid/content/ContentResolver;ILjava/util/Set;Ljava/util/Map;Landroid/content/Context;)V
    //   48: ldc com/android/internal/telephony/CarrierAppUtils
    //   50: monitorexit
    //   51: return
    //   52: astore_0
    //   53: ldc com/android/internal/telephony/CarrierAppUtils
    //   55: monitorexit
    //   56: aload_0
    //   57: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #81	-> 3
    //   #82	-> 14
    //   #83	-> 14
    //   #84	-> 21
    //   #85	-> 21
    //   #86	-> 28
    //   #87	-> 35
    //   #90	-> 48
    //   #80	-> 52
    // Exception table:
    //   from	to	target	type
    //   3	14	52	finally
    //   14	21	52	finally
    //   21	28	52	finally
    //   28	35	52	finally
    //   35	48	52	finally
  }
  
  public static void disableCarrierAppsUntilPrivileged(String paramString, int paramInt, Context paramContext) {
    // Byte code:
    //   0: ldc com/android/internal/telephony/CarrierAppUtils
    //   2: monitorenter
    //   3: aload_2
    //   4: ldc android/os/SystemConfigManager
    //   6: invokevirtual getSystemService : (Ljava/lang/Class;)Ljava/lang/Object;
    //   9: checkcast android/os/SystemConfigManager
    //   12: astore_3
    //   13: aload_3
    //   14: invokevirtual getDisabledUntilUsedPreinstalledCarrierApps : ()Ljava/util/Set;
    //   17: astore #4
    //   19: aload_3
    //   20: invokevirtual getDisabledUntilUsedPreinstalledCarrierAssociatedAppEntries : ()Ljava/util/Map;
    //   23: astore #5
    //   25: aload_2
    //   26: iload_1
    //   27: invokestatic getContentResolverForUser : (Landroid/content/Context;I)Landroid/content/ContentResolver;
    //   30: astore_3
    //   31: aload_0
    //   32: aconst_null
    //   33: aload_3
    //   34: iload_1
    //   35: aload #4
    //   37: aload #5
    //   39: aload_2
    //   40: invokestatic disableCarrierAppsUntilPrivileged : (Ljava/lang/String;Landroid/telephony/TelephonyManager;Landroid/content/ContentResolver;ILjava/util/Set;Ljava/util/Map;Landroid/content/Context;)V
    //   43: ldc com/android/internal/telephony/CarrierAppUtils
    //   45: monitorexit
    //   46: return
    //   47: astore_0
    //   48: ldc com/android/internal/telephony/CarrierAppUtils
    //   50: monitorexit
    //   51: aload_0
    //   52: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #108	-> 3
    //   #109	-> 13
    //   #110	-> 13
    //   #112	-> 19
    //   #113	-> 19
    //   #114	-> 25
    //   #115	-> 31
    //   #118	-> 43
    //   #107	-> 47
    // Exception table:
    //   from	to	target	type
    //   3	13	47	finally
    //   13	19	47	finally
    //   19	25	47	finally
    //   25	31	47	finally
    //   31	43	47	finally
  }
  
  private static ContentResolver getContentResolverForUser(Context paramContext, int paramInt) {
    paramContext = paramContext.createContextAsUser(UserHandle.getUserHandleForUid(paramInt), 0);
    return paramContext.getContentResolver();
  }
  
  private static boolean isUpdatedSystemApp(ApplicationInfo paramApplicationInfo) {
    if ((paramApplicationInfo.flags & 0x80) != 0)
      return true; 
    return false;
  }
  
  public static void disableCarrierAppsUntilPrivileged(String paramString, TelephonyManager paramTelephonyManager, ContentResolver paramContentResolver, int paramInt, Set<String> paramSet, Map<String, List<CarrierAssociatedAppEntry>> paramMap, Context paramContext) {
    // Byte code:
    //   0: aload #6
    //   2: invokevirtual getPackageManager : ()Landroid/content/pm/PackageManager;
    //   5: astore #7
    //   7: aload #6
    //   9: ldc 'permission'
    //   11: invokevirtual getSystemService : (Ljava/lang/String;)Ljava/lang/Object;
    //   14: checkcast android/permission/PermissionManager
    //   17: astore #8
    //   19: iload_3
    //   20: aload #4
    //   22: aload #6
    //   24: invokestatic getDefaultCarrierAppCandidatesHelper : (ILjava/util/Set;Landroid/content/Context;)Ljava/util/List;
    //   27: astore #9
    //   29: aload #9
    //   31: ifnull -> 1567
    //   34: aload #9
    //   36: invokeinterface isEmpty : ()Z
    //   41: ifeq -> 47
    //   44: goto -> 1567
    //   47: iload_3
    //   48: aload #5
    //   50: aload #6
    //   52: invokestatic getDefaultCarrierAssociatedAppsHelper : (ILjava/util/Map;Landroid/content/Context;)Ljava/util/Map;
    //   55: astore #5
    //   57: new java/util/ArrayList
    //   60: dup
    //   61: invokespecial <init> : ()V
    //   64: astore #10
    //   66: ldc 'carrier_apps_handled'
    //   68: astore #11
    //   70: aload_2
    //   71: ldc 'carrier_apps_handled'
    //   73: iconst_0
    //   74: invokestatic getInt : (Landroid/content/ContentResolver;Ljava/lang/String;I)I
    //   77: istore #12
    //   79: iload #12
    //   81: ifeq -> 90
    //   84: iconst_1
    //   85: istore #13
    //   87: goto -> 93
    //   90: iconst_0
    //   91: istore #13
    //   93: iload #12
    //   95: getstatic android/os/Build$VERSION.SDK_INT : I
    //   98: if_icmpne -> 107
    //   101: iconst_1
    //   102: istore #14
    //   104: goto -> 110
    //   107: iconst_0
    //   108: istore #14
    //   110: aload #7
    //   112: astore #15
    //   114: aload #8
    //   116: astore #15
    //   118: aload #9
    //   120: astore #15
    //   122: aload #5
    //   124: astore #15
    //   126: iload #12
    //   128: istore #16
    //   130: aload #9
    //   132: invokeinterface iterator : ()Ljava/util/Iterator;
    //   137: astore #17
    //   139: aload #8
    //   141: astore #4
    //   143: aload #11
    //   145: astore #8
    //   147: aload #7
    //   149: astore #15
    //   151: aload #4
    //   153: astore #15
    //   155: aload #9
    //   157: astore #15
    //   159: aload #5
    //   161: astore #15
    //   163: iload #12
    //   165: istore #16
    //   167: aload #17
    //   169: invokeinterface hasNext : ()Z
    //   174: istore #18
    //   176: iload #18
    //   178: ifeq -> 1454
    //   181: aload #17
    //   183: invokeinterface next : ()Ljava/lang/Object;
    //   188: checkcast android/content/pm/ApplicationInfo
    //   191: astore #19
    //   193: aload #19
    //   195: getfield packageName : Ljava/lang/String;
    //   198: astore #11
    //   200: aload_1
    //   201: ifnull -> 228
    //   204: aload_1
    //   205: aload #11
    //   207: invokevirtual checkCarrierPrivilegesForPackageAnyPhone : (Ljava/lang/String;)I
    //   210: istore #16
    //   212: iload #16
    //   214: iconst_1
    //   215: if_icmpne -> 228
    //   218: iconst_1
    //   219: istore #16
    //   221: goto -> 231
    //   224: astore_0
    //   225: goto -> 1557
    //   228: iconst_0
    //   229: istore #16
    //   231: aload #7
    //   233: aload #11
    //   235: iconst_0
    //   236: invokevirtual setSystemAppState : (Ljava/lang/String;I)V
    //   239: aload #5
    //   241: aload #11
    //   243: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   248: checkcast java/util/List
    //   251: astore #20
    //   253: aload #20
    //   255: ifnull -> 341
    //   258: aload #4
    //   260: astore #15
    //   262: aload #5
    //   264: astore #15
    //   266: aload #20
    //   268: invokeinterface iterator : ()Ljava/util/Iterator;
    //   273: astore #21
    //   275: aload #4
    //   277: astore #15
    //   279: aload #5
    //   281: astore #15
    //   283: aload #21
    //   285: invokeinterface hasNext : ()Z
    //   290: ifeq -> 334
    //   293: aload #4
    //   295: astore #15
    //   297: aload #5
    //   299: astore #15
    //   301: aload #21
    //   303: invokeinterface next : ()Ljava/lang/Object;
    //   308: checkcast com/android/internal/telephony/CarrierAppUtils$AssociatedAppInfo
    //   311: astore #15
    //   313: aload #7
    //   315: aload #15
    //   317: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   320: getfield packageName : Ljava/lang/String;
    //   323: iconst_0
    //   324: invokevirtual setSystemAppState : (Ljava/lang/String;I)V
    //   327: goto -> 275
    //   330: astore_0
    //   331: goto -> 1557
    //   334: goto -> 341
    //   337: astore_0
    //   338: goto -> 1557
    //   341: aload #6
    //   343: iload_3
    //   344: invokestatic of : (I)Landroid/os/UserHandle;
    //   347: iconst_0
    //   348: invokevirtual createContextAsUser : (Landroid/os/UserHandle;I)Landroid/content/Context;
    //   351: astore #15
    //   353: aload #15
    //   355: invokevirtual getPackageManager : ()Landroid/content/pm/PackageManager;
    //   358: aload #11
    //   360: invokevirtual getApplicationEnabledSetting : (Ljava/lang/String;)I
    //   363: istore #22
    //   365: iload #16
    //   367: ifeq -> 974
    //   370: iload #12
    //   372: istore #16
    //   374: aload #19
    //   376: invokestatic isUpdatedSystemApp : (Landroid/content/pm/ApplicationInfo;)Z
    //   379: istore #18
    //   381: ldc '): ENABLED for user '
    //   383: astore #15
    //   385: iload #18
    //   387: ifne -> 395
    //   390: iload #22
    //   392: ifeq -> 423
    //   395: iload #22
    //   397: iconst_4
    //   398: if_icmpeq -> 423
    //   401: aload #19
    //   403: getfield flags : I
    //   406: istore #16
    //   408: iload #16
    //   410: ldc 8388608
    //   412: iand
    //   413: ifne -> 583
    //   416: goto -> 423
    //   419: astore_0
    //   420: goto -> 1557
    //   423: iload #12
    //   425: istore #16
    //   427: new java/lang/StringBuilder
    //   430: astore #21
    //   432: iload #12
    //   434: istore #16
    //   436: aload #21
    //   438: invokespecial <init> : ()V
    //   441: iload #12
    //   443: istore #16
    //   445: aload #21
    //   447: ldc 'Update state ('
    //   449: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   452: pop
    //   453: iload #12
    //   455: istore #16
    //   457: aload #21
    //   459: aload #11
    //   461: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   464: pop
    //   465: iload #12
    //   467: istore #16
    //   469: aload #21
    //   471: ldc '): ENABLED for user '
    //   473: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   476: pop
    //   477: iload #12
    //   479: istore #16
    //   481: aload #21
    //   483: iload_3
    //   484: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   487: pop
    //   488: iload #12
    //   490: istore #16
    //   492: ldc 'CarrierAppUtils'
    //   494: aload #21
    //   496: invokevirtual toString : ()Ljava/lang/String;
    //   499: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   502: pop
    //   503: iload #12
    //   505: istore #16
    //   507: aload #6
    //   509: iload_3
    //   510: invokestatic of : (I)Landroid/os/UserHandle;
    //   513: iconst_0
    //   514: invokevirtual createContextAsUser : (Landroid/os/UserHandle;I)Landroid/content/Context;
    //   517: astore #21
    //   519: iload #12
    //   521: istore #16
    //   523: aload #21
    //   525: invokevirtual getPackageManager : ()Landroid/content/pm/PackageManager;
    //   528: astore #21
    //   530: iload #12
    //   532: istore #16
    //   534: aload #21
    //   536: aload #11
    //   538: iconst_2
    //   539: invokevirtual setSystemAppState : (Ljava/lang/String;I)V
    //   542: iload #12
    //   544: istore #16
    //   546: aload #6
    //   548: aload_0
    //   549: iconst_0
    //   550: iload_3
    //   551: invokestatic of : (I)Landroid/os/UserHandle;
    //   554: invokevirtual createPackageContextAsUser : (Ljava/lang/String;ILandroid/os/UserHandle;)Landroid/content/Context;
    //   557: astore #21
    //   559: iload #12
    //   561: istore #16
    //   563: aload #21
    //   565: invokevirtual getPackageManager : ()Landroid/content/pm/PackageManager;
    //   568: astore #21
    //   570: iload #12
    //   572: istore #16
    //   574: aload #21
    //   576: aload #11
    //   578: iconst_1
    //   579: iconst_1
    //   580: invokevirtual setApplicationEnabledSetting : (Ljava/lang/String;II)V
    //   583: aload #20
    //   585: ifnull -> 950
    //   588: iload #12
    //   590: istore #16
    //   592: aload #20
    //   594: invokeinterface iterator : ()Ljava/util/Iterator;
    //   599: astore #11
    //   601: iload #12
    //   603: istore #16
    //   605: aload #11
    //   607: invokeinterface hasNext : ()Z
    //   612: ifeq -> 947
    //   615: iload #12
    //   617: istore #16
    //   619: aload #11
    //   621: invokeinterface next : ()Ljava/lang/Object;
    //   626: checkcast com/android/internal/telephony/CarrierAppUtils$AssociatedAppInfo
    //   629: astore #20
    //   631: iload #12
    //   633: istore #16
    //   635: aload #6
    //   637: iload_3
    //   638: invokestatic of : (I)Landroid/os/UserHandle;
    //   641: iconst_0
    //   642: invokevirtual createContextAsUser : (Landroid/os/UserHandle;I)Landroid/content/Context;
    //   645: astore #21
    //   647: iload #12
    //   649: istore #16
    //   651: aload #21
    //   653: invokevirtual getPackageManager : ()Landroid/content/pm/PackageManager;
    //   656: astore #21
    //   658: iload #12
    //   660: istore #16
    //   662: aload #20
    //   664: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   667: getfield packageName : Ljava/lang/String;
    //   670: astore #23
    //   672: iload #12
    //   674: istore #16
    //   676: aload #21
    //   678: aload #23
    //   680: invokevirtual getApplicationEnabledSetting : (Ljava/lang/String;)I
    //   683: istore #22
    //   685: iload #12
    //   687: istore #16
    //   689: aload #20
    //   691: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   694: getfield flags : I
    //   697: istore #16
    //   699: iload #16
    //   701: ldc 8388608
    //   703: iand
    //   704: ifeq -> 713
    //   707: iconst_1
    //   708: istore #16
    //   710: goto -> 716
    //   713: iconst_0
    //   714: istore #16
    //   716: iload #22
    //   718: ifeq -> 738
    //   721: iload #22
    //   723: iconst_4
    //   724: if_icmpeq -> 738
    //   727: iload #16
    //   729: ifne -> 735
    //   732: goto -> 738
    //   735: goto -> 944
    //   738: iload #12
    //   740: istore #22
    //   742: iload #22
    //   744: istore #16
    //   746: new java/lang/StringBuilder
    //   749: astore #21
    //   751: iload #22
    //   753: istore #16
    //   755: aload #21
    //   757: invokespecial <init> : ()V
    //   760: iload #22
    //   762: istore #16
    //   764: aload #21
    //   766: ldc 'Update associated state ('
    //   768: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   771: pop
    //   772: iload #22
    //   774: istore #16
    //   776: aload #21
    //   778: aload #20
    //   780: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   783: getfield packageName : Ljava/lang/String;
    //   786: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   789: pop
    //   790: iload #22
    //   792: istore #16
    //   794: aload #21
    //   796: aload #15
    //   798: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   801: pop
    //   802: iload #22
    //   804: istore #16
    //   806: aload #21
    //   808: iload_3
    //   809: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   812: pop
    //   813: iload #22
    //   815: istore #16
    //   817: ldc 'CarrierAppUtils'
    //   819: aload #21
    //   821: invokevirtual toString : ()Ljava/lang/String;
    //   824: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   827: pop
    //   828: iload #22
    //   830: istore #16
    //   832: aload #6
    //   834: iload_3
    //   835: invokestatic of : (I)Landroid/os/UserHandle;
    //   838: iconst_0
    //   839: invokevirtual createContextAsUser : (Landroid/os/UserHandle;I)Landroid/content/Context;
    //   842: astore #21
    //   844: iload #22
    //   846: istore #16
    //   848: aload #21
    //   850: invokevirtual getPackageManager : ()Landroid/content/pm/PackageManager;
    //   853: astore #21
    //   855: iload #22
    //   857: istore #16
    //   859: aload #20
    //   861: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   864: getfield packageName : Ljava/lang/String;
    //   867: astore #23
    //   869: iload #22
    //   871: istore #16
    //   873: aload #21
    //   875: aload #23
    //   877: iconst_2
    //   878: invokevirtual setSystemAppState : (Ljava/lang/String;I)V
    //   881: iload #22
    //   883: istore #16
    //   885: iload_3
    //   886: invokestatic of : (I)Landroid/os/UserHandle;
    //   889: astore #21
    //   891: iload #22
    //   893: istore #16
    //   895: aload #6
    //   897: aload_0
    //   898: iconst_0
    //   899: aload #21
    //   901: invokevirtual createPackageContextAsUser : (Ljava/lang/String;ILandroid/os/UserHandle;)Landroid/content/Context;
    //   904: astore #21
    //   906: iload #22
    //   908: istore #16
    //   910: aload #21
    //   912: invokevirtual getPackageManager : ()Landroid/content/pm/PackageManager;
    //   915: astore #21
    //   917: iload #22
    //   919: istore #16
    //   921: aload #20
    //   923: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   926: getfield packageName : Ljava/lang/String;
    //   929: astore #20
    //   931: iload #22
    //   933: istore #16
    //   935: aload #21
    //   937: aload #20
    //   939: iconst_1
    //   940: iconst_1
    //   941: invokevirtual setApplicationEnabledSetting : (Ljava/lang/String;II)V
    //   944: goto -> 601
    //   947: goto -> 950
    //   950: iload #12
    //   952: istore #16
    //   954: aload #10
    //   956: aload #19
    //   958: getfield packageName : Ljava/lang/String;
    //   961: invokeinterface add : (Ljava/lang/Object;)Z
    //   966: pop
    //   967: goto -> 1435
    //   970: astore_0
    //   971: goto -> 1557
    //   974: iload #12
    //   976: istore #16
    //   978: aload #19
    //   980: invokestatic isUpdatedSystemApp : (Landroid/content/pm/ApplicationInfo;)Z
    //   983: istore #18
    //   985: iload #18
    //   987: ifne -> 1136
    //   990: iload #22
    //   992: ifne -> 1136
    //   995: iload #12
    //   997: istore #16
    //   999: aload #19
    //   1001: getfield flags : I
    //   1004: ldc 8388608
    //   1006: iand
    //   1007: ifeq -> 1136
    //   1010: iload #12
    //   1012: istore #16
    //   1014: new java/lang/StringBuilder
    //   1017: astore #15
    //   1019: iload #12
    //   1021: istore #16
    //   1023: aload #15
    //   1025: invokespecial <init> : ()V
    //   1028: iload #12
    //   1030: istore #16
    //   1032: aload #15
    //   1034: ldc 'Update state ('
    //   1036: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1039: pop
    //   1040: iload #12
    //   1042: istore #16
    //   1044: aload #15
    //   1046: aload #11
    //   1048: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1051: pop
    //   1052: iload #12
    //   1054: istore #16
    //   1056: aload #15
    //   1058: ldc '): DISABLED_UNTIL_USED for user '
    //   1060: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1063: pop
    //   1064: iload #12
    //   1066: istore #16
    //   1068: aload #15
    //   1070: iload_3
    //   1071: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1074: pop
    //   1075: iload #12
    //   1077: istore #16
    //   1079: ldc 'CarrierAppUtils'
    //   1081: aload #15
    //   1083: invokevirtual toString : ()Ljava/lang/String;
    //   1086: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   1089: pop
    //   1090: iload #12
    //   1092: istore #16
    //   1094: aload #6
    //   1096: iload_3
    //   1097: invokestatic of : (I)Landroid/os/UserHandle;
    //   1100: iconst_0
    //   1101: invokevirtual createContextAsUser : (Landroid/os/UserHandle;I)Landroid/content/Context;
    //   1104: astore #15
    //   1106: iload #12
    //   1108: istore #16
    //   1110: aload #15
    //   1112: invokevirtual getPackageManager : ()Landroid/content/pm/PackageManager;
    //   1115: astore #15
    //   1117: iload #12
    //   1119: istore #16
    //   1121: aload #15
    //   1123: aload #11
    //   1125: iconst_3
    //   1126: invokevirtual setSystemAppState : (Ljava/lang/String;I)V
    //   1129: goto -> 1136
    //   1132: astore_0
    //   1133: goto -> 1557
    //   1136: aload #20
    //   1138: ifnull -> 1435
    //   1141: iload #12
    //   1143: istore #16
    //   1145: aload #20
    //   1147: invokeinterface iterator : ()Ljava/util/Iterator;
    //   1152: astore #15
    //   1154: iload #12
    //   1156: istore #16
    //   1158: aload #15
    //   1160: invokeinterface hasNext : ()Z
    //   1165: ifeq -> 1432
    //   1168: iload #12
    //   1170: istore #16
    //   1172: aload #15
    //   1174: invokeinterface next : ()Ljava/lang/Object;
    //   1179: checkcast com/android/internal/telephony/CarrierAppUtils$AssociatedAppInfo
    //   1182: astore #11
    //   1184: iload #13
    //   1186: ifeq -> 1248
    //   1189: iload #14
    //   1191: ifne -> 1242
    //   1194: iload #12
    //   1196: istore #16
    //   1198: aload #11
    //   1200: getfield addedInSdk : I
    //   1203: iconst_m1
    //   1204: if_icmpeq -> 1242
    //   1207: iload #12
    //   1209: istore #16
    //   1211: aload #11
    //   1213: getfield addedInSdk : I
    //   1216: istore #16
    //   1218: iload #16
    //   1220: iload #12
    //   1222: if_icmple -> 1239
    //   1225: aload #11
    //   1227: getfield addedInSdk : I
    //   1230: getstatic android/os/Build$VERSION.SDK_INT : I
    //   1233: if_icmpgt -> 1242
    //   1236: goto -> 1248
    //   1239: goto -> 1242
    //   1242: iconst_0
    //   1243: istore #16
    //   1245: goto -> 1251
    //   1248: iconst_1
    //   1249: istore #16
    //   1251: aload #6
    //   1253: iload_3
    //   1254: invokestatic of : (I)Landroid/os/UserHandle;
    //   1257: iconst_0
    //   1258: invokevirtual createContextAsUser : (Landroid/os/UserHandle;I)Landroid/content/Context;
    //   1261: astore #19
    //   1263: aload #19
    //   1265: invokevirtual getPackageManager : ()Landroid/content/pm/PackageManager;
    //   1268: astore #19
    //   1270: aload #11
    //   1272: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   1275: getfield packageName : Ljava/lang/String;
    //   1278: astore #20
    //   1280: aload #19
    //   1282: aload #20
    //   1284: invokevirtual getApplicationEnabledSetting : (Ljava/lang/String;)I
    //   1287: istore #24
    //   1289: aload #11
    //   1291: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   1294: getfield flags : I
    //   1297: ldc 8388608
    //   1299: iand
    //   1300: ifeq -> 1309
    //   1303: iconst_1
    //   1304: istore #22
    //   1306: goto -> 1312
    //   1309: iconst_0
    //   1310: istore #22
    //   1312: iload #16
    //   1314: ifeq -> 1425
    //   1317: iload #24
    //   1319: ifne -> 1425
    //   1322: iload #22
    //   1324: ifeq -> 1425
    //   1327: new java/lang/StringBuilder
    //   1330: astore #19
    //   1332: aload #19
    //   1334: invokespecial <init> : ()V
    //   1337: aload #19
    //   1339: ldc 'Update associated state ('
    //   1341: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1344: pop
    //   1345: aload #19
    //   1347: aload #11
    //   1349: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   1352: getfield packageName : Ljava/lang/String;
    //   1355: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1358: pop
    //   1359: aload #19
    //   1361: ldc '): DISABLED_UNTIL_USED for user '
    //   1363: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1366: pop
    //   1367: aload #19
    //   1369: iload_3
    //   1370: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1373: pop
    //   1374: ldc 'CarrierAppUtils'
    //   1376: aload #19
    //   1378: invokevirtual toString : ()Ljava/lang/String;
    //   1381: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   1384: pop
    //   1385: aload #6
    //   1387: iload_3
    //   1388: invokestatic of : (I)Landroid/os/UserHandle;
    //   1391: iconst_0
    //   1392: invokevirtual createContextAsUser : (Landroid/os/UserHandle;I)Landroid/content/Context;
    //   1395: astore #19
    //   1397: aload #19
    //   1399: invokevirtual getPackageManager : ()Landroid/content/pm/PackageManager;
    //   1402: astore #19
    //   1404: aload #11
    //   1406: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   1409: getfield packageName : Ljava/lang/String;
    //   1412: astore #11
    //   1414: aload #19
    //   1416: aload #11
    //   1418: iconst_3
    //   1419: invokevirtual setSystemAppState : (Ljava/lang/String;I)V
    //   1422: goto -> 1425
    //   1425: goto -> 1154
    //   1428: astore_0
    //   1429: goto -> 1557
    //   1432: goto -> 1435
    //   1435: goto -> 147
    //   1438: astore_0
    //   1439: goto -> 1557
    //   1442: astore_0
    //   1443: goto -> 1557
    //   1446: astore_0
    //   1447: goto -> 1557
    //   1450: astore_0
    //   1451: goto -> 1557
    //   1454: iload #13
    //   1456: ifeq -> 1470
    //   1459: iload #14
    //   1461: ifne -> 1467
    //   1464: goto -> 1470
    //   1467: goto -> 1484
    //   1470: getstatic android/os/Build$VERSION.SDK_INT : I
    //   1473: istore #12
    //   1475: aload_2
    //   1476: aload #8
    //   1478: iload #12
    //   1480: invokestatic putInt : (Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    //   1483: pop
    //   1484: aload #10
    //   1486: invokeinterface isEmpty : ()Z
    //   1491: ifne -> 1545
    //   1494: aload #10
    //   1496: invokeinterface size : ()I
    //   1501: anewarray java/lang/String
    //   1504: astore_0
    //   1505: aload #10
    //   1507: aload_0
    //   1508: invokeinterface toArray : ([Ljava/lang/Object;)[Ljava/lang/Object;
    //   1513: pop
    //   1514: iload_3
    //   1515: invokestatic of : (I)Landroid/os/UserHandle;
    //   1518: astore_2
    //   1519: getstatic com/android/internal/telephony/_$$Lambda$_14QHG018Z6p13d3hzJuGTWnNeo.INSTANCE : Lcom/android/internal/telephony/-$$Lambda$_14QHG018Z6p13d3hzJuGTWnNeo;
    //   1522: astore_1
    //   1523: getstatic com/android/internal/telephony/_$$Lambda$CarrierAppUtils$oAca0vwfzY3MLxvgrejL5_ugnfc.INSTANCE : Lcom/android/internal/telephony/-$$Lambda$CarrierAppUtils$oAca0vwfzY3MLxvgrejL5_ugnfc;
    //   1526: astore #5
    //   1528: aload #4
    //   1530: aload_0
    //   1531: aload_2
    //   1532: aload_1
    //   1533: aload #5
    //   1535: invokevirtual grantDefaultPermissionsToEnabledCarrierApps : ([Ljava/lang/String;Landroid/os/UserHandle;Ljava/util/concurrent/Executor;Ljava/util/function/Consumer;)V
    //   1538: goto -> 1545
    //   1541: astore_0
    //   1542: goto -> 1557
    //   1545: goto -> 1566
    //   1548: astore_0
    //   1549: goto -> 1553
    //   1552: astore_0
    //   1553: goto -> 1557
    //   1556: astore_0
    //   1557: ldc 'CarrierAppUtils'
    //   1559: ldc 'Could not reach PackageManager'
    //   1561: aload_0
    //   1562: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   1565: pop
    //   1566: return
    //   1567: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #145	-> 0
    //   #146	-> 7
    //   #147	-> 7
    //   #148	-> 19
    //   #150	-> 29
    //   #154	-> 47
    //   #157	-> 57
    //   #158	-> 66
    //   #159	-> 66
    //   #163	-> 79
    //   #164	-> 93
    //   #167	-> 110
    //   #168	-> 193
    //   #169	-> 200
    //   #170	-> 204
    //   #323	-> 224
    //   #170	-> 228
    //   #174	-> 231
    //   #176	-> 239
    //   #177	-> 253
    //   #178	-> 258
    //   #179	-> 313
    //   #181	-> 327
    //   #323	-> 330
    //   #178	-> 334
    //   #323	-> 337
    //   #177	-> 341
    //   #184	-> 341
    //   #185	-> 353
    //   #186	-> 365
    //   #189	-> 370
    //   #323	-> 419
    //   #194	-> 423
    //   #196	-> 503
    //   #197	-> 519
    //   #198	-> 530
    //   #200	-> 542
    //   #201	-> 559
    //   #202	-> 570
    //   #209	-> 583
    //   #210	-> 588
    //   #211	-> 631
    //   #212	-> 631
    //   #213	-> 647
    //   #214	-> 672
    //   #216	-> 685
    //   #224	-> 716
    //   #229	-> 738
    //   #232	-> 828
    //   #233	-> 844
    //   #234	-> 869
    //   #236	-> 881
    //   #237	-> 881
    //   #236	-> 891
    //   #238	-> 906
    //   #239	-> 931
    //   #244	-> 944
    //   #210	-> 947
    //   #209	-> 950
    //   #248	-> 950
    //   #323	-> 970
    //   #252	-> 974
    //   #255	-> 1010
    //   #257	-> 1090
    //   #258	-> 1106
    //   #259	-> 1117
    //   #323	-> 1132
    //   #269	-> 1136
    //   #270	-> 1141
    //   #271	-> 1184
    //   #276	-> 1251
    //   #277	-> 1251
    //   #278	-> 1263
    //   #279	-> 1280
    //   #281	-> 1289
    //   #291	-> 1312
    //   #295	-> 1327
    //   #299	-> 1385
    //   #300	-> 1397
    //   #301	-> 1414
    //   #291	-> 1425
    //   #304	-> 1425
    //   #323	-> 1428
    //   #270	-> 1432
    //   #269	-> 1435
    //   #307	-> 1435
    //   #323	-> 1438
    //   #310	-> 1454
    //   #311	-> 1470
    //   #315	-> 1484
    //   #318	-> 1494
    //   #319	-> 1505
    //   #320	-> 1514
    //   #321	-> 1514
    //   #320	-> 1528
    //   #323	-> 1541
    //   #315	-> 1545
    //   #325	-> 1545
    //   #323	-> 1548
    //   #324	-> 1557
    //   #326	-> 1566
    //   #150	-> 1567
    //   #151	-> 1567
    // Exception table:
    //   from	to	target	type
    //   130	139	1556	android/content/pm/PackageManager$NameNotFoundException
    //   167	176	1556	android/content/pm/PackageManager$NameNotFoundException
    //   181	193	1450	android/content/pm/PackageManager$NameNotFoundException
    //   193	200	1446	android/content/pm/PackageManager$NameNotFoundException
    //   204	212	224	android/content/pm/PackageManager$NameNotFoundException
    //   231	239	1446	android/content/pm/PackageManager$NameNotFoundException
    //   239	253	1446	android/content/pm/PackageManager$NameNotFoundException
    //   266	275	337	android/content/pm/PackageManager$NameNotFoundException
    //   283	293	337	android/content/pm/PackageManager$NameNotFoundException
    //   301	313	337	android/content/pm/PackageManager$NameNotFoundException
    //   313	327	330	android/content/pm/PackageManager$NameNotFoundException
    //   341	353	1442	android/content/pm/PackageManager$NameNotFoundException
    //   353	365	1442	android/content/pm/PackageManager$NameNotFoundException
    //   374	381	970	android/content/pm/PackageManager$NameNotFoundException
    //   401	408	419	android/content/pm/PackageManager$NameNotFoundException
    //   427	432	970	android/content/pm/PackageManager$NameNotFoundException
    //   436	441	970	android/content/pm/PackageManager$NameNotFoundException
    //   445	453	970	android/content/pm/PackageManager$NameNotFoundException
    //   457	465	970	android/content/pm/PackageManager$NameNotFoundException
    //   469	477	970	android/content/pm/PackageManager$NameNotFoundException
    //   481	488	970	android/content/pm/PackageManager$NameNotFoundException
    //   492	503	970	android/content/pm/PackageManager$NameNotFoundException
    //   507	519	970	android/content/pm/PackageManager$NameNotFoundException
    //   523	530	970	android/content/pm/PackageManager$NameNotFoundException
    //   534	542	970	android/content/pm/PackageManager$NameNotFoundException
    //   546	559	970	android/content/pm/PackageManager$NameNotFoundException
    //   563	570	970	android/content/pm/PackageManager$NameNotFoundException
    //   574	583	970	android/content/pm/PackageManager$NameNotFoundException
    //   592	601	970	android/content/pm/PackageManager$NameNotFoundException
    //   605	615	970	android/content/pm/PackageManager$NameNotFoundException
    //   619	631	970	android/content/pm/PackageManager$NameNotFoundException
    //   635	647	970	android/content/pm/PackageManager$NameNotFoundException
    //   651	658	970	android/content/pm/PackageManager$NameNotFoundException
    //   662	672	970	android/content/pm/PackageManager$NameNotFoundException
    //   676	685	970	android/content/pm/PackageManager$NameNotFoundException
    //   689	699	970	android/content/pm/PackageManager$NameNotFoundException
    //   746	751	1132	android/content/pm/PackageManager$NameNotFoundException
    //   755	760	1132	android/content/pm/PackageManager$NameNotFoundException
    //   764	772	1132	android/content/pm/PackageManager$NameNotFoundException
    //   776	790	1132	android/content/pm/PackageManager$NameNotFoundException
    //   794	802	1132	android/content/pm/PackageManager$NameNotFoundException
    //   806	813	1132	android/content/pm/PackageManager$NameNotFoundException
    //   817	828	1132	android/content/pm/PackageManager$NameNotFoundException
    //   832	844	1132	android/content/pm/PackageManager$NameNotFoundException
    //   848	855	1132	android/content/pm/PackageManager$NameNotFoundException
    //   859	869	1132	android/content/pm/PackageManager$NameNotFoundException
    //   873	881	1132	android/content/pm/PackageManager$NameNotFoundException
    //   885	891	1132	android/content/pm/PackageManager$NameNotFoundException
    //   895	906	1132	android/content/pm/PackageManager$NameNotFoundException
    //   910	917	1132	android/content/pm/PackageManager$NameNotFoundException
    //   921	931	1132	android/content/pm/PackageManager$NameNotFoundException
    //   935	944	1132	android/content/pm/PackageManager$NameNotFoundException
    //   954	967	1132	android/content/pm/PackageManager$NameNotFoundException
    //   978	985	1438	android/content/pm/PackageManager$NameNotFoundException
    //   999	1010	1132	android/content/pm/PackageManager$NameNotFoundException
    //   1014	1019	1132	android/content/pm/PackageManager$NameNotFoundException
    //   1023	1028	1132	android/content/pm/PackageManager$NameNotFoundException
    //   1032	1040	1132	android/content/pm/PackageManager$NameNotFoundException
    //   1044	1052	1132	android/content/pm/PackageManager$NameNotFoundException
    //   1056	1064	1132	android/content/pm/PackageManager$NameNotFoundException
    //   1068	1075	1132	android/content/pm/PackageManager$NameNotFoundException
    //   1079	1090	1132	android/content/pm/PackageManager$NameNotFoundException
    //   1094	1106	1132	android/content/pm/PackageManager$NameNotFoundException
    //   1110	1117	1132	android/content/pm/PackageManager$NameNotFoundException
    //   1121	1129	1132	android/content/pm/PackageManager$NameNotFoundException
    //   1145	1154	1438	android/content/pm/PackageManager$NameNotFoundException
    //   1158	1168	1438	android/content/pm/PackageManager$NameNotFoundException
    //   1172	1184	1438	android/content/pm/PackageManager$NameNotFoundException
    //   1198	1207	1438	android/content/pm/PackageManager$NameNotFoundException
    //   1211	1218	1438	android/content/pm/PackageManager$NameNotFoundException
    //   1225	1236	1428	android/content/pm/PackageManager$NameNotFoundException
    //   1251	1263	1428	android/content/pm/PackageManager$NameNotFoundException
    //   1263	1280	1428	android/content/pm/PackageManager$NameNotFoundException
    //   1280	1289	1428	android/content/pm/PackageManager$NameNotFoundException
    //   1289	1303	1428	android/content/pm/PackageManager$NameNotFoundException
    //   1327	1385	1428	android/content/pm/PackageManager$NameNotFoundException
    //   1385	1397	1428	android/content/pm/PackageManager$NameNotFoundException
    //   1397	1414	1428	android/content/pm/PackageManager$NameNotFoundException
    //   1414	1422	1428	android/content/pm/PackageManager$NameNotFoundException
    //   1470	1475	1552	android/content/pm/PackageManager$NameNotFoundException
    //   1475	1484	1548	android/content/pm/PackageManager$NameNotFoundException
    //   1484	1494	1548	android/content/pm/PackageManager$NameNotFoundException
    //   1494	1505	1548	android/content/pm/PackageManager$NameNotFoundException
    //   1505	1514	1548	android/content/pm/PackageManager$NameNotFoundException
    //   1514	1528	1548	android/content/pm/PackageManager$NameNotFoundException
    //   1528	1538	1541	android/content/pm/PackageManager$NameNotFoundException
  }
  
  public static List<ApplicationInfo> getDefaultCarrierApps(TelephonyManager paramTelephonyManager, int paramInt, Context paramContext) {
    List<ApplicationInfo> list = getDefaultCarrierAppCandidates(paramInt, paramContext);
    if (list == null || list.isEmpty())
      return null; 
    for (paramInt = list.size() - 1; paramInt >= 0; paramInt--) {
      boolean bool;
      ApplicationInfo applicationInfo = list.get(paramInt);
      String str = applicationInfo.packageName;
      if (paramTelephonyManager.checkCarrierPrivilegesForPackageAnyPhone(str) == 1) {
        bool = true;
      } else {
        bool = false;
      } 
      if (!bool)
        list.remove(paramInt); 
    } 
    return list;
  }
  
  public static List<ApplicationInfo> getDefaultCarrierAppCandidates(int paramInt, Context paramContext) {
    SystemConfigManager systemConfigManager = (SystemConfigManager)paramContext.getSystemService(SystemConfigManager.class);
    Set<String> set = systemConfigManager.getDisabledUntilUsedPreinstalledCarrierApps();
    return getDefaultCarrierAppCandidatesHelper(paramInt, set, paramContext);
  }
  
  private static List<ApplicationInfo> getDefaultCarrierAppCandidatesHelper(int paramInt, Set<String> paramSet, Context paramContext) {
    if (paramSet == null || 
      paramSet.isEmpty())
      return null; 
    ArrayList<ApplicationInfo> arrayList = new ArrayList(paramSet.size());
    for (String str : paramSet) {
      ApplicationInfo applicationInfo = getApplicationInfoIfSystemApp(paramInt, str, paramContext);
      if (applicationInfo != null)
        arrayList.add(applicationInfo); 
    } 
    return arrayList;
  }
  
  private static Map<String, List<AssociatedAppInfo>> getDefaultCarrierAssociatedAppsHelper(int paramInt, Map<String, List<CarrierAssociatedAppEntry>> paramMap, Context paramContext) {
    int i = paramMap.size();
    ArrayMap<String, List> arrayMap = new ArrayMap(i);
    for (Map.Entry<String, List<CarrierAssociatedAppEntry>> entry : paramMap.entrySet()) {
      String str = (String)entry.getKey();
      List<CarrierAssociatedAppEntry> list = (List)entry.getValue();
      for (i = 0; i < list.size(); i++) {
        CarrierAssociatedAppEntry carrierAssociatedAppEntry = list.get(i);
        String str1 = carrierAssociatedAppEntry.packageName;
        ApplicationInfo applicationInfo = getApplicationInfoIfSystemApp(paramInt, str1, paramContext);
        if (applicationInfo != null && !isUpdatedSystemApp(applicationInfo)) {
          List<AssociatedAppInfo> list2 = (List)arrayMap.get(str);
          List<AssociatedAppInfo> list1 = list2;
          if (list2 == null) {
            list1 = new ArrayList();
            arrayMap.put(str, list1);
          } 
          list1.add(new AssociatedAppInfo(applicationInfo, carrierAssociatedAppEntry.addedInSdk));
        } 
      } 
    } 
    return (Map)arrayMap;
  }
  
  private static ApplicationInfo getApplicationInfoIfSystemApp(int paramInt, String paramString, Context paramContext) {
    try {
      paramContext = paramContext.createContextAsUser(UserHandle.of(paramInt), 0);
      PackageManager packageManager = paramContext.getPackageManager();
      ApplicationInfo applicationInfo = packageManager.getApplicationInfo(paramString, 537952256);
      if (applicationInfo != null)
        return applicationInfo; 
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      Log.w("CarrierAppUtils", "Could not reach PackageManager", (Throwable)nameNotFoundException);
    } 
    return null;
  }
  
  private static final class AssociatedAppInfo {
    public final int addedInSdk;
    
    public final ApplicationInfo appInfo;
    
    AssociatedAppInfo(ApplicationInfo param1ApplicationInfo, int param1Int) {
      this.appInfo = param1ApplicationInfo;
      this.addedInSdk = param1Int;
    }
  }
}
