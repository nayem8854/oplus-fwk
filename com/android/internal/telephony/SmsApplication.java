package com.android.internal.telephony;

import android.app.AppOpsManager;
import android.app.role.RoleManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.UserHandle;
import android.telephony.PackageChangeReceiver;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class SmsApplication {
  public static final String ACTION_DEFAULT_SMS_PACKAGE_CHANGED_INTERNAL = "android.provider.action.DEFAULT_SMS_PACKAGE_CHANGED_INTERNAL";
  
  public static final String BLUETOOTH_PACKAGE_NAME = "com.android.bluetooth";
  
  private static final boolean DEBUG = false;
  
  private static final boolean DEBUG_MULTIUSER = false;
  
  private static final String[] DEFAULT_APP_EXCLUSIVE_APPOPS = new String[] { "android:read_sms", "android:write_sms", "android:receive_sms", "android:receive_wap_push", "android:send_sms", "android:read_cell_broadcasts" };
  
  static final String LOG_TAG = "SmsApplication";
  
  private static final int MAIN_USER_ID = 0;
  
  public static final String MMS_SERVICE_PACKAGE_NAME = "com.android.mms.service";
  
  public static final String PERMISSION_MONITOR_DEFAULT_SMS_PACKAGE = "android.permission.MONITOR_DEFAULT_SMS_PACKAGE";
  
  public static final String PHONE_PACKAGE_NAME = "com.android.phone";
  
  private static final String SCHEME_MMS = "mms";
  
  private static final String SCHEME_MMSTO = "mmsto";
  
  private static final String SCHEME_SMS = "sms";
  
  private static final String SCHEME_SMSTO = "smsto";
  
  public static final String TELEPHONY_PROVIDER_PACKAGE_NAME = "com.android.providers.telephony";
  
  private static int mUserId = 0;
  
  private static SmsPackageMonitor sSmsPackageMonitor = null;
  
  public static class SmsApplicationData {
    private String mApplicationName;
    
    private String mMmsReceiverClass;
    
    public String mPackageName;
    
    private String mProviderChangedReceiverClass;
    
    private String mRespondViaMessageClass;
    
    private String mSendToClass;
    
    private String mSimFullReceiverClass;
    
    private String mSmsAppChangedReceiverClass;
    
    private String mSmsReceiverClass;
    
    private int mUid;
    
    public boolean isComplete() {
      boolean bool;
      if (this.mSmsReceiverClass != null && this.mMmsReceiverClass != null && this.mRespondViaMessageClass != null && this.mSendToClass != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public SmsApplicationData(String param1String, int param1Int) {
      this.mPackageName = param1String;
      this.mUid = param1Int;
    }
    
    public String getApplicationName(Context param1Context) {
      if (this.mApplicationName == null) {
        PackageManager packageManager = param1Context.getPackageManager();
        param1Context = null;
        try {
          String str = this.mPackageName;
          int i = this.mUid;
          UserHandle userHandle = UserHandle.getUserHandleForUid(i);
          ApplicationInfo applicationInfo = packageManager.getApplicationInfoAsUser(str, 0, userHandle);
          if (applicationInfo != null) {
            String str1;
            CharSequence charSequence = packageManager.getApplicationLabel(applicationInfo);
            if (charSequence != null)
              str1 = charSequence.toString(); 
            this.mApplicationName = str1;
          } 
        } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
          return null;
        } 
      } 
      return this.mApplicationName;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(" mPackageName: ");
      stringBuilder.append(this.mPackageName);
      stringBuilder.append(" mSmsReceiverClass: ");
      stringBuilder.append(this.mSmsReceiverClass);
      stringBuilder.append(" mMmsReceiverClass: ");
      stringBuilder.append(this.mMmsReceiverClass);
      stringBuilder.append(" mRespondViaMessageClass: ");
      stringBuilder.append(this.mRespondViaMessageClass);
      stringBuilder.append(" mSendToClass: ");
      stringBuilder.append(this.mSendToClass);
      stringBuilder.append(" mSmsAppChangedClass: ");
      stringBuilder.append(this.mSmsAppChangedReceiverClass);
      stringBuilder.append(" mProviderChangedReceiverClass: ");
      stringBuilder.append(this.mProviderChangedReceiverClass);
      stringBuilder.append(" mSimFullReceiverClass: ");
      stringBuilder.append(this.mSimFullReceiverClass);
      stringBuilder.append(" mUid: ");
      stringBuilder.append(this.mUid);
      return stringBuilder.toString();
    }
  }
  
  private static int getIncomingUserId(Context paramContext) {
    int i = UserHandle.myUserId();
    int j = Binder.getCallingUid();
    if (UserHandle.getAppId(j) < 10000)
      return i; 
    return UserHandle.getUserHandleForUid(j).getIdentifier();
  }
  
  public static Collection<SmsApplicationData> getApplicationCollection(Context paramContext) {
    return getApplicationCollectionAsUser(paramContext, getIncomingUserId(paramContext));
  }
  
  public static Collection<SmsApplicationData> getApplicationCollectionAsUser(Context paramContext, int paramInt) {
    long l = Binder.clearCallingIdentity();
    try {
      return getApplicationCollectionInternal(paramContext, paramInt);
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  private static Collection<SmsApplicationData> getApplicationCollectionInternal(Context paramContext, int paramInt) {
    PackageManager packageManager = paramContext.getPackageManager();
    UserHandle userHandle1 = UserHandle.of(paramInt);
    Intent intent1 = new Intent("android.provider.Telephony.SMS_DELIVER");
    List list2 = packageManager.queryBroadcastReceiversAsUser(intent1, 786432, userHandle1);
    HashMap<Object, Object> hashMap = new HashMap<>();
    for (ResolveInfo resolveInfo : list2) {
      ActivityInfo activityInfo = resolveInfo.activityInfo;
      if (activityInfo == null)
        continue; 
      if (!"android.permission.BROADCAST_SMS".equals(activityInfo.permission))
        continue; 
      String str = activityInfo.packageName;
      if (!hashMap.containsKey(str)) {
        SmsApplicationData smsApplicationData = new SmsApplicationData(str, activityInfo.applicationInfo.uid);
        SmsApplicationData.access$002(smsApplicationData, activityInfo.name);
        hashMap.put(str, smsApplicationData);
      } 
    } 
    Intent intent6 = new Intent("android.provider.Telephony.WAP_PUSH_DELIVER");
    intent6.setDataAndType(null, "application/vnd.wap.mms-message");
    List list7 = packageManager.queryBroadcastReceiversAsUser(intent6, 786432, userHandle1);
    for (ResolveInfo resolveInfo : list7) {
      ActivityInfo activityInfo = resolveInfo.activityInfo;
      if (activityInfo == null)
        continue; 
      if (!"android.permission.BROADCAST_WAP_PUSH".equals(activityInfo.permission))
        continue; 
      String str = activityInfo.packageName;
      SmsApplicationData smsApplicationData = (SmsApplicationData)hashMap.get(str);
      if (smsApplicationData != null)
        SmsApplicationData.access$102(smsApplicationData, activityInfo.name); 
    } 
    Intent intent7 = new Intent("android.intent.action.RESPOND_VIA_MESSAGE", Uri.fromParts("smsto", "", null));
    UserHandle userHandle2 = UserHandle.of(paramInt);
    List list6 = packageManager.queryIntentServicesAsUser(intent7, 786432, userHandle2);
    for (ResolveInfo resolveInfo : list6) {
      ServiceInfo serviceInfo = resolveInfo.serviceInfo;
      if (serviceInfo == null)
        continue; 
      if (!"android.permission.SEND_RESPOND_VIA_MESSAGE".equals(serviceInfo.permission))
        continue; 
      String str = serviceInfo.packageName;
      SmsApplicationData smsApplicationData = (SmsApplicationData)hashMap.get(str);
      if (smsApplicationData != null)
        SmsApplicationData.access$202(smsApplicationData, serviceInfo.name); 
    } 
    Intent intent5 = new Intent("android.intent.action.SENDTO", Uri.fromParts("smsto", "", null));
    List list5 = packageManager.queryIntentActivitiesAsUser(intent5, 786432, userHandle1);
    for (ResolveInfo resolveInfo : list5) {
      ActivityInfo activityInfo = resolveInfo.activityInfo;
      if (activityInfo == null)
        continue; 
      String str = activityInfo.packageName;
      SmsApplicationData smsApplicationData = (SmsApplicationData)hashMap.get(str);
      if (smsApplicationData != null)
        SmsApplicationData.access$302(smsApplicationData, activityInfo.name); 
    } 
    Intent intent4 = new Intent("android.provider.action.DEFAULT_SMS_PACKAGE_CHANGED");
    List list4 = packageManager.queryBroadcastReceiversAsUser(intent4, 786432, userHandle1);
    for (ResolveInfo resolveInfo : list4) {
      ActivityInfo activityInfo = resolveInfo.activityInfo;
      if (activityInfo == null)
        continue; 
      String str = activityInfo.packageName;
      SmsApplicationData smsApplicationData = (SmsApplicationData)hashMap.get(str);
      if (smsApplicationData != null)
        SmsApplicationData.access$402(smsApplicationData, activityInfo.name); 
    } 
    Intent intent3 = new Intent("android.provider.action.EXTERNAL_PROVIDER_CHANGE");
    List list3 = packageManager.queryBroadcastReceiversAsUser(intent3, 786432, userHandle1);
    for (ResolveInfo resolveInfo : list3) {
      ActivityInfo activityInfo = resolveInfo.activityInfo;
      if (activityInfo == null)
        continue; 
      String str = activityInfo.packageName;
      SmsApplicationData smsApplicationData = (SmsApplicationData)hashMap.get(str);
      if (smsApplicationData != null)
        SmsApplicationData.access$502(smsApplicationData, activityInfo.name); 
    } 
    Intent intent2 = new Intent("android.provider.Telephony.SIM_FULL");
    List list1 = packageManager.queryBroadcastReceiversAsUser(intent2, 786432, userHandle1);
    for (ResolveInfo resolveInfo : list1) {
      ActivityInfo activityInfo = resolveInfo.activityInfo;
      if (activityInfo == null)
        continue; 
      String str = activityInfo.packageName;
      SmsApplicationData smsApplicationData = (SmsApplicationData)hashMap.get(str);
      if (smsApplicationData != null)
        SmsApplicationData.access$602(smsApplicationData, activityInfo.name); 
    } 
    for (ResolveInfo resolveInfo : list2) {
      ActivityInfo activityInfo = resolveInfo.activityInfo;
      if (activityInfo == null)
        continue; 
      String str = activityInfo.packageName;
      SmsApplicationData smsApplicationData = (SmsApplicationData)hashMap.get(str);
      if (smsApplicationData != null && 
        !smsApplicationData.isComplete())
        hashMap.remove(str); 
    } 
    return hashMap.values();
  }
  
  public static SmsApplicationData getApplicationForPackage(Collection<SmsApplicationData> paramCollection, String paramString) {
    if (paramString == null)
      return null; 
    for (SmsApplicationData smsApplicationData : paramCollection) {
      if (smsApplicationData.mPackageName.contentEquals(paramString))
        return smsApplicationData; 
    } 
    return null;
  }
  
  private static SmsApplicationData getApplication(Context paramContext, boolean paramBoolean, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: ldc 'phone'
    //   3: invokevirtual getSystemService : (Ljava/lang/String;)Ljava/lang/Object;
    //   6: checkcast android/telephony/TelephonyManager
    //   9: astore_3
    //   10: aload_0
    //   11: ldc_w 'role'
    //   14: invokevirtual getSystemService : (Ljava/lang/String;)Ljava/lang/Object;
    //   17: checkcast android/app/role/RoleManager
    //   20: astore #4
    //   22: aload_3
    //   23: invokevirtual isSmsCapable : ()Z
    //   26: ifne -> 47
    //   29: aload #4
    //   31: ifnull -> 45
    //   34: aload #4
    //   36: ldc_w 'android.app.role.SMS'
    //   39: invokevirtual isRoleAvailable : (Ljava/lang/String;)Z
    //   42: ifne -> 47
    //   45: aconst_null
    //   46: areturn
    //   47: aload_0
    //   48: iload_2
    //   49: invokestatic getApplicationCollectionInternal : (Landroid/content/Context;I)Ljava/util/Collection;
    //   52: astore #5
    //   54: aload_0
    //   55: iload_2
    //   56: invokestatic getDefaultSmsPackage : (Landroid/content/Context;I)Ljava/lang/String;
    //   59: astore_3
    //   60: aconst_null
    //   61: astore #4
    //   63: aload_3
    //   64: ifnull -> 75
    //   67: aload #5
    //   69: aload_3
    //   70: invokestatic getApplicationForPackage : (Ljava/util/Collection;Ljava/lang/String;)Lcom/android/internal/telephony/SmsApplication$SmsApplicationData;
    //   73: astore #4
    //   75: aload #4
    //   77: astore #5
    //   79: aload #4
    //   81: ifnull -> 142
    //   84: iload_1
    //   85: ifne -> 102
    //   88: aload #4
    //   90: astore_3
    //   91: aload #4
    //   93: invokestatic access$700 : (Lcom/android/internal/telephony/SmsApplication$SmsApplicationData;)I
    //   96: invokestatic myUid : ()I
    //   99: if_icmpne -> 121
    //   102: aload_0
    //   103: aload #4
    //   105: iload_1
    //   106: invokestatic tryFixExclusiveSmsAppops : (Landroid/content/Context;Lcom/android/internal/telephony/SmsApplication$SmsApplicationData;Z)Z
    //   109: istore #6
    //   111: aload #4
    //   113: astore_3
    //   114: iload #6
    //   116: ifne -> 121
    //   119: aconst_null
    //   120: astore_3
    //   121: aload_3
    //   122: astore #5
    //   124: aload_3
    //   125: ifnull -> 142
    //   128: aload_3
    //   129: astore #5
    //   131: iload_1
    //   132: ifeq -> 142
    //   135: aload_0
    //   136: invokestatic defaultSmsAppChanged : (Landroid/content/Context;)V
    //   139: aload_3
    //   140: astore #5
    //   142: aload #5
    //   144: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #471	-> 0
    //   #472	-> 0
    //   #473	-> 10
    //   #476	-> 22
    //   #479	-> 45
    //   #482	-> 47
    //   #488	-> 54
    //   #493	-> 60
    //   #494	-> 63
    //   #495	-> 67
    //   #502	-> 75
    //   #507	-> 84
    //   #509	-> 102
    //   #510	-> 102
    //   #511	-> 111
    //   #513	-> 119
    //   #518	-> 121
    //   #523	-> 135
    //   #529	-> 142
  }
  
  private static String getDefaultSmsPackage(Context paramContext, int paramInt) {
    return ((RoleManager)paramContext.getSystemService(RoleManager.class)).getDefaultSmsPackage(paramInt);
  }
  
  private static void defaultSmsAppChanged(Context paramContext) {
    PackageManager packageManager = paramContext.getPackageManager();
    AppOpsManager appOpsManager = (AppOpsManager)paramContext.getSystemService(AppOpsManager.class);
    assignExclusiveSmsPermissionsToSystemApp(paramContext, packageManager, appOpsManager, "com.android.phone", true);
    assignExclusiveSmsPermissionsToSystemApp(paramContext, packageManager, appOpsManager, "com.android.bluetooth", true);
    assignExclusiveSmsPermissionsToSystemApp(paramContext, packageManager, appOpsManager, "com.android.mms.service", true);
    assignExclusiveSmsPermissionsToSystemApp(paramContext, packageManager, appOpsManager, "com.android.providers.telephony", true);
    str = CellBroadcastUtils.getDefaultCellBroadcastReceiverPackageName(paramContext);
    assignExclusiveSmsPermissionsToSystemApp(paramContext, packageManager, appOpsManager, str, false);
    OplusSmsApplication.oemAssignExclusiveSmsPermissionsToSystemApp(paramContext, packageManager, appOpsManager, new _$$Lambda$SmsApplication$7Qllt5_6eSpp3AUiCvanexI7r_s(paramContext, packageManager, appOpsManager));
    for (String str1 : packageManager.getPackagesForUid(1001)) {
      for (String str : DEFAULT_APP_EXCLUSIVE_APPOPS) {
        int i = appOpsManager.checkOp(str, 1001, str1);
        if (i != 0)
          appOpsManager.setUidMode(str, 1001, 0); 
      } 
    } 
  }
  
  private static boolean tryFixExclusiveSmsAppops(Context paramContext, SmsApplicationData paramSmsApplicationData, boolean paramBoolean) {
    AppOpsManager appOpsManager = (AppOpsManager)paramContext.getSystemService(AppOpsManager.class);
    for (String str : DEFAULT_APP_EXCLUSIVE_APPOPS) {
      int i = appOpsManager.unsafeCheckOp(str, paramSmsApplicationData.mUid, paramSmsApplicationData.mPackageName);
      if (i != 0) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(paramSmsApplicationData.mPackageName);
        stringBuilder.append(" lost ");
        stringBuilder.append(str);
        stringBuilder.append(": ");
        if (paramBoolean) {
          str1 = " (fixing)";
        } else {
          str1 = " (no permission to fix)";
        } 
        stringBuilder.append(str1);
        String str1 = stringBuilder.toString();
        Log.e("SmsApplication", str1);
        if (paramBoolean) {
          appOpsManager.setUidMode(str, paramSmsApplicationData.mUid, 0);
        } else {
          return false;
        } 
      } 
    } 
    return true;
  }
  
  public static void setDefaultApplication(String paramString, Context paramContext) {
    setDefaultApplicationAsUser(paramString, paramContext, getIncomingUserId(paramContext));
  }
  
  public static void setDefaultApplicationAsUser(String paramString, Context paramContext, int paramInt) {
    TelephonyManager telephonyManager = (TelephonyManager)paramContext.getSystemService("phone");
    RoleManager roleManager = (RoleManager)paramContext.getSystemService("role");
    if (!telephonyManager.isSmsCapable() && (roleManager == null || !roleManager.isRoleAvailable("android.app.role.SMS")))
      return; 
    long l = Binder.clearCallingIdentity();
    try {
      setDefaultApplicationInternal(paramString, paramContext, paramInt);
      return;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  private static void setDefaultApplicationInternal(String paramString, Context paramContext, int paramInt) {
    UserHandle userHandle = UserHandle.of(paramInt);
    String str = getDefaultSmsPackage(paramContext, paramInt);
    if (paramString != null && str != null && paramString.equals(str))
      return; 
    PackageManager packageManager = paramContext.createContextAsUser(userHandle, 0).getPackageManager();
    Collection<SmsApplicationData> collection = getApplicationCollectionInternal(paramContext, paramInt);
    if (str != null)
      getApplicationForPackage(collection, str); 
    SmsApplicationData smsApplicationData = getApplicationForPackage(collection, paramString);
    if (smsApplicationData != null) {
      AppOpsManager appOpsManager = (AppOpsManager)paramContext.getSystemService("appops");
      if (str != null && !"com.android.mms".equals(str))
        try {
          int i = (packageManager.getPackageInfo(str, 0)).applicationInfo.uid;
          setExclusiveAppops(str, appOpsManager, i, 3);
        } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Old SMS package not found: ");
          stringBuilder.append(str);
          Log.w("SmsApplication", stringBuilder.toString());
        }  
      CompletableFuture completableFuture = new CompletableFuture();
      _$$Lambda$SmsApplication$X5EjuywYuCFrDw0Uo6AfM80bZN8 _$$Lambda$SmsApplication$X5EjuywYuCFrDw0Uo6AfM80bZN8 = new _$$Lambda$SmsApplication$X5EjuywYuCFrDw0Uo6AfM80bZN8(completableFuture);
      RoleManager roleManager = (RoleManager)paramContext.getSystemService(RoleManager.class);
      String str1 = smsApplicationData.mPackageName;
      UserHandle userHandle1 = UserHandle.of(paramInt);
      Executor executor = AsyncTask.THREAD_POOL_EXECUTOR;
      roleManager.addRoleHolderAsUser("android.app.role.SMS", str1, 0, userHandle1, executor, _$$Lambda$SmsApplication$X5EjuywYuCFrDw0Uo6AfM80bZN8);
      try {
        completableFuture.get(5L, TimeUnit.SECONDS);
        defaultSmsAppChanged(paramContext);
      } catch (InterruptedException|java.util.concurrent.ExecutionException|java.util.concurrent.TimeoutException interruptedException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Exception while adding sms role holder ");
        stringBuilder.append(smsApplicationData);
        Log.e("SmsApplication", stringBuilder.toString(), interruptedException);
        return;
      } 
    } 
  }
  
  public static void broadcastSmsAppChange(Context paramContext, UserHandle paramUserHandle, String paramString1, String paramString2) {
    Collection<SmsApplicationData> collection = getApplicationCollection(paramContext);
    SmsApplicationData smsApplicationData1 = getApplicationForPackage(collection, paramString1);
    SmsApplicationData smsApplicationData2 = getApplicationForPackage(collection, paramString2);
    broadcastSmsAppChange(paramContext, paramUserHandle, smsApplicationData1, smsApplicationData2);
  }
  
  private static void broadcastSmsAppChange(Context paramContext, UserHandle paramUserHandle, SmsApplicationData paramSmsApplicationData1, SmsApplicationData paramSmsApplicationData2) {
    if (paramSmsApplicationData1 != null && paramSmsApplicationData1.mSmsAppChangedReceiverClass != null) {
      Intent intent1 = new Intent("android.provider.action.DEFAULT_SMS_PACKAGE_CHANGED");
      String str = paramSmsApplicationData1.mPackageName;
      ComponentName componentName = new ComponentName(str, paramSmsApplicationData1.mSmsAppChangedReceiverClass);
      intent1.setComponent(componentName);
      intent1.putExtra("android.provider.extra.IS_DEFAULT_SMS_APP", false);
      paramContext.sendBroadcastAsUser(intent1, paramUserHandle);
    } 
    if (paramSmsApplicationData2 != null && paramSmsApplicationData2.mSmsAppChangedReceiverClass != null) {
      Intent intent1 = new Intent("android.provider.action.DEFAULT_SMS_PACKAGE_CHANGED");
      String str = paramSmsApplicationData2.mPackageName;
      ComponentName componentName = new ComponentName(str, paramSmsApplicationData2.mSmsAppChangedReceiverClass);
      intent1.setComponent(componentName);
      intent1.putExtra("android.provider.extra.IS_DEFAULT_SMS_APP", true);
      paramContext.sendBroadcastAsUser(intent1, paramUserHandle);
    } 
    Intent intent = new Intent("android.provider.action.DEFAULT_SMS_PACKAGE_CHANGED_INTERNAL");
    paramContext.sendBroadcastAsUser(intent, paramUserHandle, "android.permission.MONITOR_DEFAULT_SMS_PACKAGE");
  }
  
  private static void assignExclusiveSmsPermissionsToSystemApp(Context paramContext, PackageManager paramPackageManager, AppOpsManager paramAppOpsManager, String paramString, boolean paramBoolean) {
    if (paramBoolean) {
      int i = paramPackageManager.checkSignatures(paramContext.getPackageName(), paramString);
      if (i != 0) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(paramString);
        stringBuilder.append(" does not have system signature");
        Log.e("SmsApplication", stringBuilder.toString());
        return;
      } 
    } 
    try {
      PackageInfo packageInfo = paramPackageManager.getPackageInfo(paramString, 0);
      int i = paramAppOpsManager.unsafeCheckOp("android:write_sms", packageInfo.applicationInfo.uid, paramString);
      if (i != 0) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append(paramString);
        stringBuilder.append(" does not have OP_WRITE_SMS:  (fixing)");
        Log.w("SmsApplication", stringBuilder.toString());
        setExclusiveAppops(paramString, paramAppOpsManager, packageInfo.applicationInfo.uid, 0);
      } 
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Package not found: ");
      stringBuilder.append(paramString);
      Log.e("SmsApplication", stringBuilder.toString());
    } 
  }
  
  private static void setExclusiveAppops(String paramString, AppOpsManager paramAppOpsManager, int paramInt1, int paramInt2) {
    for (String str : DEFAULT_APP_EXCLUSIVE_APPOPS)
      paramAppOpsManager.setUidMode(str, paramInt1, paramInt2); 
  }
  
  class SmsPackageMonitor extends PackageChangeReceiver {
    final Context mContext;
    
    public SmsPackageMonitor(SmsApplication this$0) {
      this.mContext = (Context)this$0;
    }
    
    public void onPackageDisappeared() {
      onPackageChanged();
    }
    
    public void onPackageAppeared() {
      onPackageChanged();
    }
    
    public void onPackageModified(String param1String) {
      onPackageChanged();
    }
    
    private void onPackageChanged() {
      int i;
      try {
        i = getSendingUser().getIdentifier();
      } catch (NullPointerException nullPointerException) {
        i = UserHandle.SYSTEM.getIdentifier();
      } 
      Context context2 = this.mContext;
      Context context1 = context2;
      if (i != UserHandle.SYSTEM.getIdentifier())
        try {
          context1 = this.mContext;
          String str = this.mContext.getPackageName();
          UserHandle userHandle = UserHandle.of(i);
          context1 = context1.createPackageContextAsUser(str, 0, userHandle);
        } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
          context1 = context2;
        }  
      PackageManager packageManager = context1.getPackageManager();
      ComponentName componentName = SmsApplication.getDefaultSendToApplication(context1, true);
      if (componentName != null)
        SmsApplication.configurePreferredActivity(packageManager, componentName); 
    }
  }
  
  public static void initSmsPackageMonitor(Context paramContext) {
    SmsPackageMonitor smsPackageMonitor = new SmsPackageMonitor(paramContext);
    smsPackageMonitor.register(paramContext, paramContext.getMainLooper(), UserHandle.ALL);
  }
  
  private static void configurePreferredActivity(PackageManager paramPackageManager, ComponentName paramComponentName) {
    replacePreferredActivity(paramPackageManager, paramComponentName, "sms");
    replacePreferredActivity(paramPackageManager, paramComponentName, "smsto");
    replacePreferredActivity(paramPackageManager, paramComponentName, "mms");
    replacePreferredActivity(paramPackageManager, paramComponentName, "mmsto");
  }
  
  private static void replacePreferredActivity(PackageManager paramPackageManager, ComponentName paramComponentName, String paramString) {
    Intent intent = new Intent("android.intent.action.SENDTO", Uri.fromParts(paramString, "", null));
    List list1 = paramPackageManager.queryIntentActivities(intent, 65600);
    Stream stream = list1.stream().map((Function)_$$Lambda$SmsApplication$_5EwKqcT34Co49HIhtJwmkt_SdQ.INSTANCE);
    List list2 = (List)stream.collect(Collectors.toList());
    IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction("android.intent.action.SENDTO");
    intentFilter.addCategory("android.intent.category.DEFAULT");
    intentFilter.addDataScheme(paramString);
    paramPackageManager.replacePreferredActivity(intentFilter, 2129920, list2, paramComponentName);
  }
  
  public static SmsApplicationData getSmsApplicationData(String paramString, Context paramContext) {
    Collection<SmsApplicationData> collection = getApplicationCollection(paramContext);
    return getApplicationForPackage(collection, paramString);
  }
  
  public static ComponentName getDefaultSmsApplication(Context paramContext, boolean paramBoolean) {
    int i = mUserId;
    if (i != 0)
      return getDefaultSmsApplicationAsUser(paramContext, paramBoolean, i); 
    return getDefaultSmsApplicationAsUser(paramContext, paramBoolean, getIncomingUserId(paramContext));
  }
  
  public static ComponentName getDefaultSmsApplicationAsUser(Context paramContext, boolean paramBoolean, int paramInt) {
    long l = Binder.clearCallingIdentity();
    Context context = null;
    try {
      ComponentName componentName;
      SmsApplicationData smsApplicationData = getApplication(paramContext, paramBoolean, paramInt);
      paramContext = context;
      if (smsApplicationData != null) {
        String str = smsApplicationData.mPackageName;
        componentName = new ComponentName(str, smsApplicationData.mSmsReceiverClass);
      } 
      return componentName;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public static ComponentName getDefaultMmsApplication(Context paramContext, boolean paramBoolean) {
    int i = getIncomingUserId(paramContext);
    long l = Binder.clearCallingIdentity();
    Context context = null;
    try {
      ComponentName componentName;
      SmsApplicationData smsApplicationData = getApplication(paramContext, paramBoolean, i);
      paramContext = context;
      if (smsApplicationData != null) {
        String str = smsApplicationData.mPackageName;
        componentName = new ComponentName(str, smsApplicationData.mMmsReceiverClass);
      } 
      return componentName;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public static ComponentName getDefaultRespondViaMessageApplication(Context paramContext, boolean paramBoolean) {
    int i = getIncomingUserId(paramContext);
    long l = Binder.clearCallingIdentity();
    Context context = null;
    try {
      ComponentName componentName;
      SmsApplicationData smsApplicationData = getApplication(paramContext, paramBoolean, i);
      paramContext = context;
      if (smsApplicationData != null) {
        String str = smsApplicationData.mPackageName;
        componentName = new ComponentName(str, smsApplicationData.mRespondViaMessageClass);
      } 
      return componentName;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public static ComponentName getDefaultSendToApplication(Context paramContext, boolean paramBoolean) {
    int i = getIncomingUserId(paramContext);
    long l = Binder.clearCallingIdentity();
    Context context = null;
    try {
      ComponentName componentName;
      SmsApplicationData smsApplicationData = getApplication(paramContext, paramBoolean, i);
      paramContext = context;
      if (smsApplicationData != null) {
        String str = smsApplicationData.mPackageName;
        componentName = new ComponentName(str, smsApplicationData.mSendToClass);
      } 
      return componentName;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public static ComponentName getDefaultExternalTelephonyProviderChangedApplication(Context paramContext, boolean paramBoolean) {
    int i = getIncomingUserId(paramContext);
    long l = Binder.clearCallingIdentity();
    Context context = null;
    try {
      ComponentName componentName;
      SmsApplicationData smsApplicationData = getApplication(paramContext, paramBoolean, i);
      paramContext = context;
      if (smsApplicationData != null) {
        paramContext = context;
        if (smsApplicationData.mProviderChangedReceiverClass != null) {
          String str = smsApplicationData.mPackageName;
          componentName = new ComponentName(str, smsApplicationData.mProviderChangedReceiverClass);
        } 
      } 
      return componentName;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public static ComponentName getDefaultSimFullApplication(Context paramContext, boolean paramBoolean) {
    int i = getIncomingUserId(paramContext);
    long l = Binder.clearCallingIdentity();
    Context context = null;
    try {
      ComponentName componentName;
      SmsApplicationData smsApplicationData = getApplication(paramContext, paramBoolean, i);
      paramContext = context;
      if (smsApplicationData != null) {
        paramContext = context;
        if (smsApplicationData.mSimFullReceiverClass != null) {
          String str = smsApplicationData.mPackageName;
          componentName = new ComponentName(str, smsApplicationData.mSimFullReceiverClass);
        } 
      } 
      return componentName;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public static boolean shouldWriteMessageForPackage(String paramString, Context paramContext) {
    if (!OplusSmsApplication.shouldWriteMessageForPackage(paramString))
      return false; 
    return isDefaultSmsApplication(paramContext, paramString) ^ true;
  }
  
  public static boolean isDefaultSmsApplication(Context paramContext, String paramString) {
    if (paramString == null)
      return false; 
    String str = getDefaultSmsApplicationPackageName(paramContext);
    if ((str != null && str.equals(paramString)) || 
      "com.android.bluetooth".equals(paramString))
      return true; 
    return false;
  }
  
  private static String getDefaultSmsApplicationPackageName(Context paramContext) {
    ComponentName componentName = getDefaultSmsApplication(paramContext, false);
    mUserId = 0;
    if (componentName != null)
      return componentName.getPackageName(); 
    return null;
  }
  
  public static void setUserId(int paramInt) {
    mUserId = paramInt;
  }
}
