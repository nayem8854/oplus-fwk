package com.android.internal.telephony;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.BarringInfo;
import android.telephony.CallQuality;
import android.telephony.CellIdentity;
import android.telephony.CellInfo;
import android.telephony.PhoneCapability;
import android.telephony.PreciseDataConnectionState;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.TelephonyDisplayInfo;
import android.telephony.emergency.EmergencyNumber;
import android.telephony.ims.ImsReasonInfo;
import java.util.ArrayList;
import java.util.List;

public interface ITelephonyRegistry extends IInterface {
  void addOnOpportunisticSubscriptionsChangedListener(String paramString1, String paramString2, IOnSubscriptionsChangedListener paramIOnSubscriptionsChangedListener) throws RemoteException;
  
  void addOnSubscriptionsChangedListener(String paramString1, String paramString2, IOnSubscriptionsChangedListener paramIOnSubscriptionsChangedListener) throws RemoteException;
  
  void listen(String paramString, IPhoneStateListener paramIPhoneStateListener, int paramInt, boolean paramBoolean) throws RemoteException;
  
  void listenForSubscriber(int paramInt1, String paramString1, String paramString2, IPhoneStateListener paramIPhoneStateListener, int paramInt2, boolean paramBoolean) throws RemoteException;
  
  void listenWithFeature(String paramString1, String paramString2, IPhoneStateListener paramIPhoneStateListener, int paramInt, boolean paramBoolean) throws RemoteException;
  
  void notifyActiveDataSubIdChanged(int paramInt) throws RemoteException;
  
  void notifyBarringInfoChanged(int paramInt1, int paramInt2, BarringInfo paramBarringInfo) throws RemoteException;
  
  void notifyCallForwardingChanged(boolean paramBoolean) throws RemoteException;
  
  void notifyCallForwardingChangedForSubscriber(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void notifyCallQualityChanged(CallQuality paramCallQuality, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void notifyCallState(int paramInt1, int paramInt2, int paramInt3, String paramString) throws RemoteException;
  
  void notifyCallStateForAllSubs(int paramInt, String paramString) throws RemoteException;
  
  void notifyCarrierNetworkChange(boolean paramBoolean) throws RemoteException;
  
  void notifyCellInfo(List<CellInfo> paramList) throws RemoteException;
  
  void notifyCellInfoForSubscriber(int paramInt, List<CellInfo> paramList) throws RemoteException;
  
  void notifyCellLocation(CellIdentity paramCellIdentity) throws RemoteException;
  
  void notifyCellLocationForSubscriber(int paramInt, CellIdentity paramCellIdentity) throws RemoteException;
  
  void notifyDataActivity(int paramInt) throws RemoteException;
  
  void notifyDataActivityForSubscriber(int paramInt1, int paramInt2) throws RemoteException;
  
  void notifyDataConnectionFailed(String paramString) throws RemoteException;
  
  void notifyDataConnectionForSubscriber(int paramInt1, int paramInt2, int paramInt3, PreciseDataConnectionState paramPreciseDataConnectionState) throws RemoteException;
  
  void notifyDisconnectCause(int paramInt1, int paramInt2, int paramInt3, int paramInt4) throws RemoteException;
  
  void notifyDisplayInfoChanged(int paramInt1, int paramInt2, TelephonyDisplayInfo paramTelephonyDisplayInfo) throws RemoteException;
  
  void notifyEmergencyNumberList(int paramInt1, int paramInt2) throws RemoteException;
  
  void notifyImsDisconnectCause(int paramInt, ImsReasonInfo paramImsReasonInfo) throws RemoteException;
  
  void notifyMessageWaitingChangedForPhoneId(int paramInt1, int paramInt2, boolean paramBoolean) throws RemoteException;
  
  void notifyOemHookRawEventForSubscriber(int paramInt1, int paramInt2, byte[] paramArrayOfbyte) throws RemoteException;
  
  void notifyOpportunisticSubscriptionInfoChanged() throws RemoteException;
  
  void notifyOutgoingEmergencyCall(int paramInt1, int paramInt2, EmergencyNumber paramEmergencyNumber) throws RemoteException;
  
  void notifyOutgoingEmergencySms(int paramInt1, int paramInt2, EmergencyNumber paramEmergencyNumber) throws RemoteException;
  
  void notifyPhoneCapabilityChanged(PhoneCapability paramPhoneCapability) throws RemoteException;
  
  void notifyPreciseCallState(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) throws RemoteException;
  
  void notifyPreciseDataConnectionFailed(int paramInt1, int paramInt2, int paramInt3, String paramString, int paramInt4) throws RemoteException;
  
  void notifyRadioPowerStateChanged(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void notifyRegistrationFailed(int paramInt1, int paramInt2, CellIdentity paramCellIdentity, String paramString, int paramInt3, int paramInt4, int paramInt5) throws RemoteException;
  
  void notifyServiceStateForPhoneId(int paramInt1, int paramInt2, ServiceState paramServiceState) throws RemoteException;
  
  void notifySignalStrengthForPhoneId(int paramInt1, int paramInt2, SignalStrength paramSignalStrength) throws RemoteException;
  
  void notifySimActivationStateChangedForPhoneId(int paramInt1, int paramInt2, int paramInt3, int paramInt4) throws RemoteException;
  
  void notifySrvccStateChanged(int paramInt1, int paramInt2) throws RemoteException;
  
  void notifySubscriptionInfoChanged() throws RemoteException;
  
  void notifyUserMobileDataStateChangedForPhoneId(int paramInt1, int paramInt2, boolean paramBoolean) throws RemoteException;
  
  void removeOnSubscriptionsChangedListener(String paramString, IOnSubscriptionsChangedListener paramIOnSubscriptionsChangedListener) throws RemoteException;
  
  class Default implements ITelephonyRegistry {
    public void addOnSubscriptionsChangedListener(String param1String1, String param1String2, IOnSubscriptionsChangedListener param1IOnSubscriptionsChangedListener) throws RemoteException {}
    
    public void addOnOpportunisticSubscriptionsChangedListener(String param1String1, String param1String2, IOnSubscriptionsChangedListener param1IOnSubscriptionsChangedListener) throws RemoteException {}
    
    public void removeOnSubscriptionsChangedListener(String param1String, IOnSubscriptionsChangedListener param1IOnSubscriptionsChangedListener) throws RemoteException {}
    
    public void listen(String param1String, IPhoneStateListener param1IPhoneStateListener, int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void listenWithFeature(String param1String1, String param1String2, IPhoneStateListener param1IPhoneStateListener, int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void listenForSubscriber(int param1Int1, String param1String1, String param1String2, IPhoneStateListener param1IPhoneStateListener, int param1Int2, boolean param1Boolean) throws RemoteException {}
    
    public void notifyCallStateForAllSubs(int param1Int, String param1String) throws RemoteException {}
    
    public void notifyCallState(int param1Int1, int param1Int2, int param1Int3, String param1String) throws RemoteException {}
    
    public void notifyServiceStateForPhoneId(int param1Int1, int param1Int2, ServiceState param1ServiceState) throws RemoteException {}
    
    public void notifySignalStrengthForPhoneId(int param1Int1, int param1Int2, SignalStrength param1SignalStrength) throws RemoteException {}
    
    public void notifyMessageWaitingChangedForPhoneId(int param1Int1, int param1Int2, boolean param1Boolean) throws RemoteException {}
    
    public void notifyCallForwardingChanged(boolean param1Boolean) throws RemoteException {}
    
    public void notifyCallForwardingChangedForSubscriber(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void notifyDataActivity(int param1Int) throws RemoteException {}
    
    public void notifyDataActivityForSubscriber(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void notifyDataConnectionForSubscriber(int param1Int1, int param1Int2, int param1Int3, PreciseDataConnectionState param1PreciseDataConnectionState) throws RemoteException {}
    
    public void notifyDataConnectionFailed(String param1String) throws RemoteException {}
    
    public void notifyCellLocation(CellIdentity param1CellIdentity) throws RemoteException {}
    
    public void notifyCellLocationForSubscriber(int param1Int, CellIdentity param1CellIdentity) throws RemoteException {}
    
    public void notifyCellInfo(List<CellInfo> param1List) throws RemoteException {}
    
    public void notifyPreciseCallState(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5) throws RemoteException {}
    
    public void notifyDisconnectCause(int param1Int1, int param1Int2, int param1Int3, int param1Int4) throws RemoteException {}
    
    public void notifyPreciseDataConnectionFailed(int param1Int1, int param1Int2, int param1Int3, String param1String, int param1Int4) throws RemoteException {}
    
    public void notifyCellInfoForSubscriber(int param1Int, List<CellInfo> param1List) throws RemoteException {}
    
    public void notifySrvccStateChanged(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void notifySimActivationStateChangedForPhoneId(int param1Int1, int param1Int2, int param1Int3, int param1Int4) throws RemoteException {}
    
    public void notifyOemHookRawEventForSubscriber(int param1Int1, int param1Int2, byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public void notifySubscriptionInfoChanged() throws RemoteException {}
    
    public void notifyOpportunisticSubscriptionInfoChanged() throws RemoteException {}
    
    public void notifyCarrierNetworkChange(boolean param1Boolean) throws RemoteException {}
    
    public void notifyUserMobileDataStateChangedForPhoneId(int param1Int1, int param1Int2, boolean param1Boolean) throws RemoteException {}
    
    public void notifyDisplayInfoChanged(int param1Int1, int param1Int2, TelephonyDisplayInfo param1TelephonyDisplayInfo) throws RemoteException {}
    
    public void notifyPhoneCapabilityChanged(PhoneCapability param1PhoneCapability) throws RemoteException {}
    
    public void notifyActiveDataSubIdChanged(int param1Int) throws RemoteException {}
    
    public void notifyRadioPowerStateChanged(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void notifyEmergencyNumberList(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void notifyOutgoingEmergencyCall(int param1Int1, int param1Int2, EmergencyNumber param1EmergencyNumber) throws RemoteException {}
    
    public void notifyOutgoingEmergencySms(int param1Int1, int param1Int2, EmergencyNumber param1EmergencyNumber) throws RemoteException {}
    
    public void notifyCallQualityChanged(CallQuality param1CallQuality, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void notifyImsDisconnectCause(int param1Int, ImsReasonInfo param1ImsReasonInfo) throws RemoteException {}
    
    public void notifyRegistrationFailed(int param1Int1, int param1Int2, CellIdentity param1CellIdentity, String param1String, int param1Int3, int param1Int4, int param1Int5) throws RemoteException {}
    
    public void notifyBarringInfoChanged(int param1Int1, int param1Int2, BarringInfo param1BarringInfo) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITelephonyRegistry {
    private static final String DESCRIPTOR = "com.android.internal.telephony.ITelephonyRegistry";
    
    static final int TRANSACTION_addOnOpportunisticSubscriptionsChangedListener = 2;
    
    static final int TRANSACTION_addOnSubscriptionsChangedListener = 1;
    
    static final int TRANSACTION_listen = 4;
    
    static final int TRANSACTION_listenForSubscriber = 6;
    
    static final int TRANSACTION_listenWithFeature = 5;
    
    static final int TRANSACTION_notifyActiveDataSubIdChanged = 34;
    
    static final int TRANSACTION_notifyBarringInfoChanged = 42;
    
    static final int TRANSACTION_notifyCallForwardingChanged = 12;
    
    static final int TRANSACTION_notifyCallForwardingChangedForSubscriber = 13;
    
    static final int TRANSACTION_notifyCallQualityChanged = 39;
    
    static final int TRANSACTION_notifyCallState = 8;
    
    static final int TRANSACTION_notifyCallStateForAllSubs = 7;
    
    static final int TRANSACTION_notifyCarrierNetworkChange = 30;
    
    static final int TRANSACTION_notifyCellInfo = 20;
    
    static final int TRANSACTION_notifyCellInfoForSubscriber = 24;
    
    static final int TRANSACTION_notifyCellLocation = 18;
    
    static final int TRANSACTION_notifyCellLocationForSubscriber = 19;
    
    static final int TRANSACTION_notifyDataActivity = 14;
    
    static final int TRANSACTION_notifyDataActivityForSubscriber = 15;
    
    static final int TRANSACTION_notifyDataConnectionFailed = 17;
    
    static final int TRANSACTION_notifyDataConnectionForSubscriber = 16;
    
    static final int TRANSACTION_notifyDisconnectCause = 22;
    
    static final int TRANSACTION_notifyDisplayInfoChanged = 32;
    
    static final int TRANSACTION_notifyEmergencyNumberList = 36;
    
    static final int TRANSACTION_notifyImsDisconnectCause = 40;
    
    static final int TRANSACTION_notifyMessageWaitingChangedForPhoneId = 11;
    
    static final int TRANSACTION_notifyOemHookRawEventForSubscriber = 27;
    
    static final int TRANSACTION_notifyOpportunisticSubscriptionInfoChanged = 29;
    
    static final int TRANSACTION_notifyOutgoingEmergencyCall = 37;
    
    static final int TRANSACTION_notifyOutgoingEmergencySms = 38;
    
    static final int TRANSACTION_notifyPhoneCapabilityChanged = 33;
    
    static final int TRANSACTION_notifyPreciseCallState = 21;
    
    static final int TRANSACTION_notifyPreciseDataConnectionFailed = 23;
    
    static final int TRANSACTION_notifyRadioPowerStateChanged = 35;
    
    static final int TRANSACTION_notifyRegistrationFailed = 41;
    
    static final int TRANSACTION_notifyServiceStateForPhoneId = 9;
    
    static final int TRANSACTION_notifySignalStrengthForPhoneId = 10;
    
    static final int TRANSACTION_notifySimActivationStateChangedForPhoneId = 26;
    
    static final int TRANSACTION_notifySrvccStateChanged = 25;
    
    static final int TRANSACTION_notifySubscriptionInfoChanged = 28;
    
    static final int TRANSACTION_notifyUserMobileDataStateChangedForPhoneId = 31;
    
    static final int TRANSACTION_removeOnSubscriptionsChangedListener = 3;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.ITelephonyRegistry");
    }
    
    public static ITelephonyRegistry asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.ITelephonyRegistry");
      if (iInterface != null && iInterface instanceof ITelephonyRegistry)
        return (ITelephonyRegistry)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 42:
          return "notifyBarringInfoChanged";
        case 41:
          return "notifyRegistrationFailed";
        case 40:
          return "notifyImsDisconnectCause";
        case 39:
          return "notifyCallQualityChanged";
        case 38:
          return "notifyOutgoingEmergencySms";
        case 37:
          return "notifyOutgoingEmergencyCall";
        case 36:
          return "notifyEmergencyNumberList";
        case 35:
          return "notifyRadioPowerStateChanged";
        case 34:
          return "notifyActiveDataSubIdChanged";
        case 33:
          return "notifyPhoneCapabilityChanged";
        case 32:
          return "notifyDisplayInfoChanged";
        case 31:
          return "notifyUserMobileDataStateChangedForPhoneId";
        case 30:
          return "notifyCarrierNetworkChange";
        case 29:
          return "notifyOpportunisticSubscriptionInfoChanged";
        case 28:
          return "notifySubscriptionInfoChanged";
        case 27:
          return "notifyOemHookRawEventForSubscriber";
        case 26:
          return "notifySimActivationStateChangedForPhoneId";
        case 25:
          return "notifySrvccStateChanged";
        case 24:
          return "notifyCellInfoForSubscriber";
        case 23:
          return "notifyPreciseDataConnectionFailed";
        case 22:
          return "notifyDisconnectCause";
        case 21:
          return "notifyPreciseCallState";
        case 20:
          return "notifyCellInfo";
        case 19:
          return "notifyCellLocationForSubscriber";
        case 18:
          return "notifyCellLocation";
        case 17:
          return "notifyDataConnectionFailed";
        case 16:
          return "notifyDataConnectionForSubscriber";
        case 15:
          return "notifyDataActivityForSubscriber";
        case 14:
          return "notifyDataActivity";
        case 13:
          return "notifyCallForwardingChangedForSubscriber";
        case 12:
          return "notifyCallForwardingChanged";
        case 11:
          return "notifyMessageWaitingChangedForPhoneId";
        case 10:
          return "notifySignalStrengthForPhoneId";
        case 9:
          return "notifyServiceStateForPhoneId";
        case 8:
          return "notifyCallState";
        case 7:
          return "notifyCallStateForAllSubs";
        case 6:
          return "listenForSubscriber";
        case 5:
          return "listenWithFeature";
        case 4:
          return "listen";
        case 3:
          return "removeOnSubscriptionsChangedListener";
        case 2:
          return "addOnOpportunisticSubscriptionsChangedListener";
        case 1:
          break;
      } 
      return "addOnSubscriptionsChangedListener";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        byte[] arrayOfByte;
        ArrayList<CellInfo> arrayList;
        String str1;
        int i;
        CellIdentity cellIdentity;
        String str4;
        IPhoneStateListener iPhoneStateListener1;
        int j, k;
        IPhoneStateListener iPhoneStateListener2;
        String str5;
        boolean bool1 = false, bool2 = false, bool3 = false, bool4 = false, bool5 = false, bool6 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 42:
            param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              BarringInfo barringInfo = (BarringInfo)BarringInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            notifyBarringInfoChanged(param1Int2, param1Int1, (BarringInfo)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 41:
            param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            i = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              cellIdentity = (CellIdentity)CellIdentity.CREATOR.createFromParcel(param1Parcel1);
            } else {
              cellIdentity = null;
            } 
            str4 = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            j = param1Parcel1.readInt();
            k = param1Parcel1.readInt();
            notifyRegistrationFailed(i, param1Int2, cellIdentity, str4, param1Int1, j, k);
            param1Parcel2.writeNoException();
            return true;
          case 40:
            param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              ImsReasonInfo imsReasonInfo = (ImsReasonInfo)ImsReasonInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            notifyImsDisconnectCause(param1Int1, (ImsReasonInfo)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 39:
            param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            if (param1Parcel1.readInt() != 0) {
              CallQuality callQuality = (CallQuality)CallQuality.CREATOR.createFromParcel(param1Parcel1);
            } else {
              cellIdentity = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            k = param1Parcel1.readInt();
            notifyCallQualityChanged((CallQuality)cellIdentity, param1Int1, param1Int2, k);
            param1Parcel2.writeNoException();
            return true;
          case 38:
            param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              EmergencyNumber emergencyNumber = (EmergencyNumber)EmergencyNumber.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            notifyOutgoingEmergencySms(param1Int2, param1Int1, (EmergencyNumber)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 37:
            param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              EmergencyNumber emergencyNumber = (EmergencyNumber)EmergencyNumber.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            notifyOutgoingEmergencyCall(param1Int2, param1Int1, (EmergencyNumber)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 36:
            param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            notifyEmergencyNumberList(param1Int2, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 35:
            param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            k = param1Parcel1.readInt();
            notifyRadioPowerStateChanged(param1Int2, param1Int1, k);
            param1Parcel2.writeNoException();
            return true;
          case 34:
            param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            param1Int1 = param1Parcel1.readInt();
            notifyActiveDataSubIdChanged(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 33:
            param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            if (param1Parcel1.readInt() != 0) {
              PhoneCapability phoneCapability = (PhoneCapability)PhoneCapability.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            notifyPhoneCapabilityChanged((PhoneCapability)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 32:
            param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              TelephonyDisplayInfo telephonyDisplayInfo = (TelephonyDisplayInfo)TelephonyDisplayInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            notifyDisplayInfoChanged(param1Int1, param1Int2, (TelephonyDisplayInfo)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 31:
            param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0)
              bool6 = true; 
            notifyUserMobileDataStateChangedForPhoneId(param1Int1, param1Int2, bool6);
            param1Parcel2.writeNoException();
            return true;
          case 30:
            param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            bool6 = bool1;
            if (param1Parcel1.readInt() != 0)
              bool6 = true; 
            notifyCarrierNetworkChange(bool6);
            param1Parcel2.writeNoException();
            return true;
          case 29:
            param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            notifyOpportunisticSubscriptionInfoChanged();
            param1Parcel2.writeNoException();
            return true;
          case 28:
            param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            notifySubscriptionInfoChanged();
            param1Parcel2.writeNoException();
            return true;
          case 27:
            param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            arrayOfByte = param1Parcel1.createByteArray();
            notifyOemHookRawEventForSubscriber(param1Int1, param1Int2, arrayOfByte);
            param1Parcel2.writeNoException();
            return true;
          case 26:
            arrayOfByte.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            param1Int2 = arrayOfByte.readInt();
            k = arrayOfByte.readInt();
            param1Int1 = arrayOfByte.readInt();
            j = arrayOfByte.readInt();
            notifySimActivationStateChangedForPhoneId(param1Int2, k, param1Int1, j);
            param1Parcel2.writeNoException();
            return true;
          case 25:
            arrayOfByte.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            param1Int2 = arrayOfByte.readInt();
            param1Int1 = arrayOfByte.readInt();
            notifySrvccStateChanged(param1Int2, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 24:
            arrayOfByte.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            param1Int1 = arrayOfByte.readInt();
            arrayList = arrayOfByte.createTypedArrayList(CellInfo.CREATOR);
            notifyCellInfoForSubscriber(param1Int1, arrayList);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            arrayList.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            param1Int1 = arrayList.readInt();
            j = arrayList.readInt();
            param1Int2 = arrayList.readInt();
            str2 = arrayList.readString();
            k = arrayList.readInt();
            notifyPreciseDataConnectionFailed(param1Int1, j, param1Int2, str2, k);
            param1Parcel2.writeNoException();
            return true;
          case 22:
            arrayList.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            j = arrayList.readInt();
            param1Int2 = arrayList.readInt();
            k = arrayList.readInt();
            param1Int1 = arrayList.readInt();
            notifyDisconnectCause(j, param1Int2, k, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 21:
            arrayList.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            param1Int2 = arrayList.readInt();
            i = arrayList.readInt();
            j = arrayList.readInt();
            param1Int1 = arrayList.readInt();
            k = arrayList.readInt();
            notifyPreciseCallState(param1Int2, i, j, param1Int1, k);
            param1Parcel2.writeNoException();
            return true;
          case 20:
            arrayList.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            arrayList = arrayList.createTypedArrayList(CellInfo.CREATOR);
            notifyCellInfo(arrayList);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            arrayList.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            param1Int1 = arrayList.readInt();
            if (arrayList.readInt() != 0) {
              CellIdentity cellIdentity1 = (CellIdentity)CellIdentity.CREATOR.createFromParcel((Parcel)arrayList);
            } else {
              arrayList = null;
            } 
            notifyCellLocationForSubscriber(param1Int1, (CellIdentity)arrayList);
            param1Parcel2.writeNoException();
            return true;
          case 18:
            arrayList.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            if (arrayList.readInt() != 0) {
              CellIdentity cellIdentity1 = (CellIdentity)CellIdentity.CREATOR.createFromParcel((Parcel)arrayList);
            } else {
              arrayList = null;
            } 
            notifyCellLocation((CellIdentity)arrayList);
            param1Parcel2.writeNoException();
            return true;
          case 17:
            arrayList.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            str1 = arrayList.readString();
            notifyDataConnectionFailed(str1);
            param1Parcel2.writeNoException();
            return true;
          case 16:
            str1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            param1Int1 = str1.readInt();
            param1Int2 = str1.readInt();
            k = str1.readInt();
            if (str1.readInt() != 0) {
              PreciseDataConnectionState preciseDataConnectionState = (PreciseDataConnectionState)PreciseDataConnectionState.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            notifyDataConnectionForSubscriber(param1Int1, param1Int2, k, (PreciseDataConnectionState)str1);
            param1Parcel2.writeNoException();
            return true;
          case 15:
            str1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            param1Int2 = str1.readInt();
            param1Int1 = str1.readInt();
            notifyDataActivityForSubscriber(param1Int2, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            str1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            param1Int1 = str1.readInt();
            notifyDataActivity(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 13:
            str1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            param1Int1 = str1.readInt();
            bool6 = bool2;
            if (str1.readInt() != 0)
              bool6 = true; 
            notifyCallForwardingChangedForSubscriber(param1Int1, bool6);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            str1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            bool6 = bool3;
            if (str1.readInt() != 0)
              bool6 = true; 
            notifyCallForwardingChanged(bool6);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            str1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            param1Int1 = str1.readInt();
            param1Int2 = str1.readInt();
            bool6 = bool4;
            if (str1.readInt() != 0)
              bool6 = true; 
            notifyMessageWaitingChangedForPhoneId(param1Int1, param1Int2, bool6);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            str1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            param1Int2 = str1.readInt();
            param1Int1 = str1.readInt();
            if (str1.readInt() != 0) {
              SignalStrength signalStrength = (SignalStrength)SignalStrength.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            notifySignalStrengthForPhoneId(param1Int2, param1Int1, (SignalStrength)str1);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            str1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            param1Int1 = str1.readInt();
            param1Int2 = str1.readInt();
            if (str1.readInt() != 0) {
              ServiceState serviceState = (ServiceState)ServiceState.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            notifyServiceStateForPhoneId(param1Int1, param1Int2, (ServiceState)str1);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            str1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            param1Int1 = str1.readInt();
            k = str1.readInt();
            param1Int2 = str1.readInt();
            str1 = str1.readString();
            notifyCallState(param1Int1, k, param1Int2, str1);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            str1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            param1Int1 = str1.readInt();
            str1 = str1.readString();
            notifyCallStateForAllSubs(param1Int1, str1);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            str1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            param1Int1 = str1.readInt();
            str4 = str1.readString();
            str2 = str1.readString();
            iPhoneStateListener2 = IPhoneStateListener.Stub.asInterface(str1.readStrongBinder());
            param1Int2 = str1.readInt();
            if (str1.readInt() != 0) {
              bool6 = true;
            } else {
              bool6 = false;
            } 
            listenForSubscriber(param1Int1, str4, str2, iPhoneStateListener2, param1Int2, bool6);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            str1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            str5 = str1.readString();
            str2 = str1.readString();
            iPhoneStateListener1 = IPhoneStateListener.Stub.asInterface(str1.readStrongBinder());
            param1Int1 = str1.readInt();
            if (str1.readInt() != 0) {
              bool6 = true;
            } else {
              bool6 = false;
            } 
            listenWithFeature(str5, str2, iPhoneStateListener1, param1Int1, bool6);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            str1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            str2 = str1.readString();
            iPhoneStateListener1 = IPhoneStateListener.Stub.asInterface(str1.readStrongBinder());
            param1Int1 = str1.readInt();
            bool6 = bool5;
            if (str1.readInt() != 0)
              bool6 = true; 
            listen(str2, iPhoneStateListener1, param1Int1, bool6);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            str1.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            str2 = str1.readString();
            iOnSubscriptionsChangedListener = IOnSubscriptionsChangedListener.Stub.asInterface(str1.readStrongBinder());
            removeOnSubscriptionsChangedListener(str2, iOnSubscriptionsChangedListener);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            iOnSubscriptionsChangedListener.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
            str3 = iOnSubscriptionsChangedListener.readString();
            str2 = iOnSubscriptionsChangedListener.readString();
            iOnSubscriptionsChangedListener = IOnSubscriptionsChangedListener.Stub.asInterface(iOnSubscriptionsChangedListener.readStrongBinder());
            addOnOpportunisticSubscriptionsChangedListener(str3, str2, iOnSubscriptionsChangedListener);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        iOnSubscriptionsChangedListener.enforceInterface("com.android.internal.telephony.ITelephonyRegistry");
        String str3 = iOnSubscriptionsChangedListener.readString();
        String str2 = iOnSubscriptionsChangedListener.readString();
        IOnSubscriptionsChangedListener iOnSubscriptionsChangedListener = IOnSubscriptionsChangedListener.Stub.asInterface(iOnSubscriptionsChangedListener.readStrongBinder());
        addOnSubscriptionsChangedListener(str3, str2, iOnSubscriptionsChangedListener);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("com.android.internal.telephony.ITelephonyRegistry");
      return true;
    }
    
    private static class Proxy implements ITelephonyRegistry {
      public static ITelephonyRegistry sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.ITelephonyRegistry";
      }
      
      public void addOnSubscriptionsChangedListener(String param2String1, String param2String2, IOnSubscriptionsChangedListener param2IOnSubscriptionsChangedListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2IOnSubscriptionsChangedListener != null) {
            iBinder = param2IOnSubscriptionsChangedListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().addOnSubscriptionsChangedListener(param2String1, param2String2, param2IOnSubscriptionsChangedListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addOnOpportunisticSubscriptionsChangedListener(String param2String1, String param2String2, IOnSubscriptionsChangedListener param2IOnSubscriptionsChangedListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2IOnSubscriptionsChangedListener != null) {
            iBinder = param2IOnSubscriptionsChangedListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().addOnOpportunisticSubscriptionsChangedListener(param2String1, param2String2, param2IOnSubscriptionsChangedListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeOnSubscriptionsChangedListener(String param2String, IOnSubscriptionsChangedListener param2IOnSubscriptionsChangedListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeString(param2String);
          if (param2IOnSubscriptionsChangedListener != null) {
            iBinder = param2IOnSubscriptionsChangedListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().removeOnSubscriptionsChangedListener(param2String, param2IOnSubscriptionsChangedListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void listen(String param2String, IPhoneStateListener param2IPhoneStateListener, int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeString(param2String);
          if (param2IPhoneStateListener != null) {
            iBinder = param2IPhoneStateListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool1 && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().listen(param2String, param2IPhoneStateListener, param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void listenWithFeature(String param2String1, String param2String2, IPhoneStateListener param2IPhoneStateListener, int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2IPhoneStateListener != null) {
            iBinder = param2IPhoneStateListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool1 && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().listenWithFeature(param2String1, param2String2, param2IPhoneStateListener, param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void listenForSubscriber(int param2Int1, String param2String1, String param2String2, IPhoneStateListener param2IPhoneStateListener, int param2Int2, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeString(param2String1);
              try {
                IBinder iBinder;
                parcel1.writeString(param2String2);
                if (param2IPhoneStateListener != null) {
                  iBinder = param2IPhoneStateListener.asBinder();
                } else {
                  iBinder = null;
                } 
                parcel1.writeStrongBinder(iBinder);
                try {
                  boolean bool;
                  parcel1.writeInt(param2Int2);
                  if (param2Boolean) {
                    bool = true;
                  } else {
                    bool = false;
                  } 
                  parcel1.writeInt(bool);
                  try {
                    boolean bool1 = this.mRemote.transact(6, parcel1, parcel2, 0);
                    if (!bool1 && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
                      ITelephonyRegistry.Stub.getDefaultImpl().listenForSubscriber(param2Int1, param2String1, param2String2, param2IPhoneStateListener, param2Int2, param2Boolean);
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } 
                    parcel2.readException();
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void notifyCallStateForAllSubs(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyCallStateForAllSubs(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyCallState(int param2Int1, int param2Int2, int param2Int3, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyCallState(param2Int1, param2Int2, param2Int3, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyServiceStateForPhoneId(int param2Int1, int param2Int2, ServiceState param2ServiceState) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2ServiceState != null) {
            parcel1.writeInt(1);
            param2ServiceState.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyServiceStateForPhoneId(param2Int1, param2Int2, param2ServiceState);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifySignalStrengthForPhoneId(int param2Int1, int param2Int2, SignalStrength param2SignalStrength) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2SignalStrength != null) {
            parcel1.writeInt(1);
            param2SignalStrength.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifySignalStrengthForPhoneId(param2Int1, param2Int2, param2SignalStrength);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyMessageWaitingChangedForPhoneId(int param2Int1, int param2Int2, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool1 && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyMessageWaitingChangedForPhoneId(param2Int1, param2Int2, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyCallForwardingChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool1 && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyCallForwardingChanged(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyCallForwardingChangedForSubscriber(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool1 && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyCallForwardingChangedForSubscriber(param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyDataActivity(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyDataActivity(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyDataActivityForSubscriber(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyDataActivityForSubscriber(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyDataConnectionForSubscriber(int param2Int1, int param2Int2, int param2Int3, PreciseDataConnectionState param2PreciseDataConnectionState) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          if (param2PreciseDataConnectionState != null) {
            parcel1.writeInt(1);
            param2PreciseDataConnectionState.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyDataConnectionForSubscriber(param2Int1, param2Int2, param2Int3, param2PreciseDataConnectionState);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyDataConnectionFailed(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyDataConnectionFailed(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyCellLocation(CellIdentity param2CellIdentity) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          if (param2CellIdentity != null) {
            parcel1.writeInt(1);
            param2CellIdentity.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyCellLocation(param2CellIdentity);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyCellLocationForSubscriber(int param2Int, CellIdentity param2CellIdentity) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeInt(param2Int);
          if (param2CellIdentity != null) {
            parcel1.writeInt(1);
            param2CellIdentity.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyCellLocationForSubscriber(param2Int, param2CellIdentity);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyCellInfo(List<CellInfo> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyCellInfo(param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyPreciseCallState(int param2Int1, int param2Int2, int param2Int3, int param2Int4, int param2Int5) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          parcel1.writeInt(param2Int4);
          parcel1.writeInt(param2Int5);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyPreciseCallState(param2Int1, param2Int2, param2Int3, param2Int4, param2Int5);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyDisconnectCause(int param2Int1, int param2Int2, int param2Int3, int param2Int4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          parcel1.writeInt(param2Int4);
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyDisconnectCause(param2Int1, param2Int2, param2Int3, param2Int4);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyPreciseDataConnectionFailed(int param2Int1, int param2Int2, int param2Int3, String param2String, int param2Int4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int4);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyPreciseDataConnectionFailed(param2Int1, param2Int2, param2Int3, param2String, param2Int4);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyCellInfoForSubscriber(int param2Int, List<CellInfo> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeInt(param2Int);
          parcel1.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyCellInfoForSubscriber(param2Int, param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifySrvccStateChanged(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifySrvccStateChanged(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifySimActivationStateChangedForPhoneId(int param2Int1, int param2Int2, int param2Int3, int param2Int4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          parcel1.writeInt(param2Int4);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifySimActivationStateChangedForPhoneId(param2Int1, param2Int2, param2Int3, param2Int4);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyOemHookRawEventForSubscriber(int param2Int1, int param2Int2, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyOemHookRawEventForSubscriber(param2Int1, param2Int2, param2ArrayOfbyte);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifySubscriptionInfoChanged() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifySubscriptionInfoChanged();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyOpportunisticSubscriptionInfoChanged() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyOpportunisticSubscriptionInfoChanged();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyCarrierNetworkChange(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool1 && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyCarrierNetworkChange(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyUserMobileDataStateChangedForPhoneId(int param2Int1, int param2Int2, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool1 && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyUserMobileDataStateChangedForPhoneId(param2Int1, param2Int2, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyDisplayInfoChanged(int param2Int1, int param2Int2, TelephonyDisplayInfo param2TelephonyDisplayInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2TelephonyDisplayInfo != null) {
            parcel1.writeInt(1);
            param2TelephonyDisplayInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyDisplayInfoChanged(param2Int1, param2Int2, param2TelephonyDisplayInfo);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyPhoneCapabilityChanged(PhoneCapability param2PhoneCapability) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          if (param2PhoneCapability != null) {
            parcel1.writeInt(1);
            param2PhoneCapability.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyPhoneCapabilityChanged(param2PhoneCapability);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyActiveDataSubIdChanged(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyActiveDataSubIdChanged(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyRadioPowerStateChanged(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyRadioPowerStateChanged(param2Int1, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyEmergencyNumberList(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyEmergencyNumberList(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyOutgoingEmergencyCall(int param2Int1, int param2Int2, EmergencyNumber param2EmergencyNumber) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2EmergencyNumber != null) {
            parcel1.writeInt(1);
            param2EmergencyNumber.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyOutgoingEmergencyCall(param2Int1, param2Int2, param2EmergencyNumber);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyOutgoingEmergencySms(int param2Int1, int param2Int2, EmergencyNumber param2EmergencyNumber) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2EmergencyNumber != null) {
            parcel1.writeInt(1);
            param2EmergencyNumber.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(38, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyOutgoingEmergencySms(param2Int1, param2Int2, param2EmergencyNumber);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyCallQualityChanged(CallQuality param2CallQuality, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          if (param2CallQuality != null) {
            parcel1.writeInt(1);
            param2CallQuality.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyCallQualityChanged(param2CallQuality, param2Int1, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyImsDisconnectCause(int param2Int, ImsReasonInfo param2ImsReasonInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeInt(param2Int);
          if (param2ImsReasonInfo != null) {
            parcel1.writeInt(1);
            param2ImsReasonInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(40, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyImsDisconnectCause(param2Int, param2ImsReasonInfo);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyRegistrationFailed(int param2Int1, int param2Int2, CellIdentity param2CellIdentity, String param2String, int param2Int3, int param2Int4, int param2Int5) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeInt(param2Int2);
              if (param2CellIdentity != null) {
                parcel1.writeInt(1);
                param2CellIdentity.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              try {
                parcel1.writeString(param2String);
                try {
                  parcel1.writeInt(param2Int3);
                  parcel1.writeInt(param2Int4);
                  parcel1.writeInt(param2Int5);
                  boolean bool = this.mRemote.transact(41, parcel1, parcel2, 0);
                  if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
                    ITelephonyRegistry.Stub.getDefaultImpl().notifyRegistrationFailed(param2Int1, param2Int2, param2CellIdentity, param2String, param2Int3, param2Int4, param2Int5);
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } 
                  parcel2.readException();
                  parcel2.recycle();
                  parcel1.recycle();
                  return;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2CellIdentity;
      }
      
      public void notifyBarringInfoChanged(int param2Int1, int param2Int2, BarringInfo param2BarringInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephonyRegistry");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2BarringInfo != null) {
            parcel1.writeInt(1);
            param2BarringInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(42, parcel1, parcel2, 0);
          if (!bool && ITelephonyRegistry.Stub.getDefaultImpl() != null) {
            ITelephonyRegistry.Stub.getDefaultImpl().notifyBarringInfoChanged(param2Int1, param2Int2, param2BarringInfo);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITelephonyRegistry param1ITelephonyRegistry) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITelephonyRegistry != null) {
          Proxy.sDefaultImpl = param1ITelephonyRegistry;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITelephonyRegistry getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
