package com.android.internal.telephony;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ISmsSecurityAgent extends IInterface {
  void onAuthorize(SmsAuthorizationRequest paramSmsAuthorizationRequest) throws RemoteException;
  
  class Default implements ISmsSecurityAgent {
    public void onAuthorize(SmsAuthorizationRequest param1SmsAuthorizationRequest) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISmsSecurityAgent {
    private static final String DESCRIPTOR = "com.android.internal.telephony.ISmsSecurityAgent";
    
    static final int TRANSACTION_onAuthorize = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.ISmsSecurityAgent");
    }
    
    public static ISmsSecurityAgent asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.ISmsSecurityAgent");
      if (iInterface != null && iInterface instanceof ISmsSecurityAgent)
        return (ISmsSecurityAgent)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onAuthorize";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.telephony.ISmsSecurityAgent");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.telephony.ISmsSecurityAgent");
      if (param1Parcel1.readInt() != 0) {
        SmsAuthorizationRequest smsAuthorizationRequest = (SmsAuthorizationRequest)SmsAuthorizationRequest.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onAuthorize((SmsAuthorizationRequest)param1Parcel1);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements ISmsSecurityAgent {
      public static ISmsSecurityAgent sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.ISmsSecurityAgent";
      }
      
      public void onAuthorize(SmsAuthorizationRequest param2SmsAuthorizationRequest) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISmsSecurityAgent");
          if (param2SmsAuthorizationRequest != null) {
            parcel1.writeInt(1);
            param2SmsAuthorizationRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ISmsSecurityAgent.Stub.getDefaultImpl() != null) {
            ISmsSecurityAgent.Stub.getDefaultImpl().onAuthorize(param2SmsAuthorizationRequest);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISmsSecurityAgent param1ISmsSecurityAgent) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISmsSecurityAgent != null) {
          Proxy.sDefaultImpl = param1ISmsSecurityAgent;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISmsSecurityAgent getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
