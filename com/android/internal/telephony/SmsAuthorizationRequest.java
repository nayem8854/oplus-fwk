package com.android.internal.telephony;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public class SmsAuthorizationRequest implements Parcelable {
  public SmsAuthorizationRequest(Parcel paramParcel) {
    this.service = ISmsSecurityService.Stub.asInterface(paramParcel.readStrongBinder());
    this.token = paramParcel.readStrongBinder();
    this.packageName = paramParcel.readString();
    this.destinationAddress = paramParcel.readString();
    this.message = paramParcel.readString();
  }
  
  public SmsAuthorizationRequest(ISmsSecurityService paramISmsSecurityService, IBinder paramIBinder, String paramString1, String paramString2, String paramString3) {
    this.service = paramISmsSecurityService;
    this.token = paramIBinder;
    this.packageName = paramString1;
    this.destinationAddress = paramString2;
    this.message = paramString3;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeStrongBinder(this.service.asBinder());
    paramParcel.writeStrongBinder(this.token);
    paramParcel.writeString(this.packageName);
    paramParcel.writeString(this.destinationAddress);
    paramParcel.writeString(this.message);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static Parcelable.Creator<SmsAuthorizationRequest> CREATOR = new Parcelable.Creator<SmsAuthorizationRequest>() {
      public SmsAuthorizationRequest[] newArray(int param1Int) {
        return new SmsAuthorizationRequest[param1Int];
      }
      
      public SmsAuthorizationRequest createFromParcel(Parcel param1Parcel) {
        return new SmsAuthorizationRequest(param1Parcel);
      }
    };
  
  public final String destinationAddress;
  
  public final String message;
  
  public final String packageName;
  
  private final ISmsSecurityService service;
  
  private final IBinder token;
  
  public void accept() throws RemoteException {
    this.service.sendResponse(this, true);
  }
  
  public void reject() throws RemoteException {
    this.service.sendResponse(this, false);
  }
  
  public IBinder getToken() {
    return this.token;
  }
  
  public String toString() {
    return String.format("[%s] (%s) # %s", new Object[] { this.packageName, this.destinationAddress, this.message });
  }
}
