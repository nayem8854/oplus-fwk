package com.android.internal.telephony;

import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IWapPushManager extends IInterface {
  boolean addPackage(String paramString1, String paramString2, String paramString3, String paramString4, int paramInt, boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  boolean deletePackage(String paramString1, String paramString2, String paramString3, String paramString4) throws RemoteException;
  
  int processMessage(String paramString1, String paramString2, Intent paramIntent) throws RemoteException;
  
  boolean updatePackage(String paramString1, String paramString2, String paramString3, String paramString4, int paramInt, boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  class Default implements IWapPushManager {
    public int processMessage(String param1String1, String param1String2, Intent param1Intent) throws RemoteException {
      return 0;
    }
    
    public boolean addPackage(String param1String1, String param1String2, String param1String3, String param1String4, int param1Int, boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {
      return false;
    }
    
    public boolean updatePackage(String param1String1, String param1String2, String param1String3, String param1String4, int param1Int, boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {
      return false;
    }
    
    public boolean deletePackage(String param1String1, String param1String2, String param1String3, String param1String4) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IWapPushManager {
    private static final String DESCRIPTOR = "com.android.internal.telephony.IWapPushManager";
    
    static final int TRANSACTION_addPackage = 2;
    
    static final int TRANSACTION_deletePackage = 4;
    
    static final int TRANSACTION_processMessage = 1;
    
    static final int TRANSACTION_updatePackage = 3;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.IWapPushManager");
    }
    
    public static IWapPushManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.IWapPushManager");
      if (iInterface != null && iInterface instanceof IWapPushManager)
        return (IWapPushManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "deletePackage";
          } 
          return "updatePackage";
        } 
        return "addPackage";
      } 
      return "processMessage";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str1;
      if (param1Int1 != 1) {
        boolean bool1, bool2;
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("com.android.internal.telephony.IWapPushManager");
              return true;
            } 
            param1Parcel1.enforceInterface("com.android.internal.telephony.IWapPushManager");
            String str12 = param1Parcel1.readString();
            String str13 = param1Parcel1.readString();
            String str14 = param1Parcel1.readString();
            str1 = param1Parcel1.readString();
            boolean bool4 = deletePackage(str12, str13, str14, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          } 
          str1.enforceInterface("com.android.internal.telephony.IWapPushManager");
          String str11 = str1.readString();
          String str8 = str1.readString();
          String str9 = str1.readString();
          String str10 = str1.readString();
          param1Int1 = str1.readInt();
          if (str1.readInt() != 0) {
            bool1 = true;
          } else {
            bool1 = false;
          } 
          if (str1.readInt() != 0) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          boolean bool3 = updatePackage(str11, str8, str9, str10, param1Int1, bool1, bool2);
          param1Parcel2.writeNoException();
          param1Parcel2.writeInt(bool3);
          return true;
        } 
        str1.enforceInterface("com.android.internal.telephony.IWapPushManager");
        String str6 = str1.readString();
        String str7 = str1.readString();
        String str5 = str1.readString();
        String str4 = str1.readString();
        param1Int1 = str1.readInt();
        if (str1.readInt() != 0) {
          bool1 = true;
        } else {
          bool1 = false;
        } 
        if (str1.readInt() != 0) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        boolean bool = addPackage(str6, str7, str5, str4, param1Int1, bool1, bool2);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool);
        return true;
      } 
      str1.enforceInterface("com.android.internal.telephony.IWapPushManager");
      String str3 = str1.readString();
      String str2 = str1.readString();
      if (str1.readInt() != 0) {
        Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)str1);
      } else {
        str1 = null;
      } 
      param1Int1 = processMessage(str3, str2, (Intent)str1);
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(param1Int1);
      return true;
    }
    
    private static class Proxy implements IWapPushManager {
      public static IWapPushManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.IWapPushManager";
      }
      
      public int processMessage(String param2String1, String param2String2, Intent param2Intent) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IWapPushManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IWapPushManager.Stub.getDefaultImpl() != null)
            return IWapPushManager.Stub.getDefaultImpl().processMessage(param2String1, param2String2, param2Intent); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean addPackage(String param2String1, String param2String2, String param2String3, String param2String4, int param2Int, boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IWapPushManager");
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeString(param2String2);
              try {
                parcel1.writeString(param2String3);
                try {
                  parcel1.writeString(param2String4);
                  try {
                    boolean bool2;
                    parcel1.writeInt(param2Int);
                    boolean bool1 = true;
                    if (param2Boolean1) {
                      bool2 = true;
                    } else {
                      bool2 = false;
                    } 
                    parcel1.writeInt(bool2);
                    if (param2Boolean2) {
                      bool2 = true;
                    } else {
                      bool2 = false;
                    } 
                    parcel1.writeInt(bool2);
                    boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
                    if (!bool && IWapPushManager.Stub.getDefaultImpl() != null) {
                      param2Boolean1 = IWapPushManager.Stub.getDefaultImpl().addPackage(param2String1, param2String2, param2String3, param2String4, param2Int, param2Boolean1, param2Boolean2);
                      parcel2.recycle();
                      parcel1.recycle();
                      return param2Boolean1;
                    } 
                    parcel2.readException();
                    param2Int = parcel2.readInt();
                    if (param2Int != 0) {
                      param2Boolean1 = bool1;
                    } else {
                      param2Boolean1 = false;
                    } 
                    parcel2.recycle();
                    parcel1.recycle();
                    return param2Boolean1;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public boolean updatePackage(String param2String1, String param2String2, String param2String3, String param2String4, int param2Int, boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IWapPushManager");
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeString(param2String2);
              try {
                parcel1.writeString(param2String3);
                try {
                  parcel1.writeString(param2String4);
                  try {
                    boolean bool2;
                    parcel1.writeInt(param2Int);
                    boolean bool1 = true;
                    if (param2Boolean1) {
                      bool2 = true;
                    } else {
                      bool2 = false;
                    } 
                    parcel1.writeInt(bool2);
                    if (param2Boolean2) {
                      bool2 = true;
                    } else {
                      bool2 = false;
                    } 
                    parcel1.writeInt(bool2);
                    boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
                    if (!bool && IWapPushManager.Stub.getDefaultImpl() != null) {
                      param2Boolean1 = IWapPushManager.Stub.getDefaultImpl().updatePackage(param2String1, param2String2, param2String3, param2String4, param2Int, param2Boolean1, param2Boolean2);
                      parcel2.recycle();
                      parcel1.recycle();
                      return param2Boolean1;
                    } 
                    parcel2.readException();
                    param2Int = parcel2.readInt();
                    if (param2Int != 0) {
                      param2Boolean1 = bool1;
                    } else {
                      param2Boolean1 = false;
                    } 
                    parcel2.recycle();
                    parcel1.recycle();
                    return param2Boolean1;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public boolean deletePackage(String param2String1, String param2String2, String param2String3, String param2String4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IWapPushManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          parcel1.writeString(param2String4);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IWapPushManager.Stub.getDefaultImpl() != null) {
            bool1 = IWapPushManager.Stub.getDefaultImpl().deletePackage(param2String1, param2String2, param2String3, param2String4);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IWapPushManager param1IWapPushManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IWapPushManager != null) {
          Proxy.sDefaultImpl = param1IWapPushManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IWapPushManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
