package com.android.internal.telephony;

import android.os.Parcel;
import android.os.Parcelable;

public class CallInfo implements Parcelable {
  public CallInfo(String paramString) {
    this.handle = paramString;
  }
  
  public String getHandle() {
    return this.handle;
  }
  
  public static final Parcelable.Creator<CallInfo> CREATOR = new Parcelable.Creator<CallInfo>() {
      public CallInfo createFromParcel(Parcel param1Parcel) {
        return new CallInfo(param1Parcel.readString());
      }
      
      public CallInfo[] newArray(int param1Int) {
        return new CallInfo[param1Int];
      }
    };
  
  private String handle;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.handle);
  }
}
