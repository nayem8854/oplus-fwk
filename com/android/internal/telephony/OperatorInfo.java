package com.android.internal.telephony;

import android.os.Parcel;
import android.os.Parcelable;

public class OperatorInfo implements Parcelable {
  class State extends Enum<State> {
    private static final State[] $VALUES;
    
    public static final State AVAILABLE = new State("AVAILABLE", 1);
    
    public static final State CURRENT = new State("CURRENT", 2);
    
    public static final State FORBIDDEN;
    
    public static State[] values() {
      return (State[])$VALUES.clone();
    }
    
    public static State valueOf(String param1String) {
      return Enum.<State>valueOf(State.class, param1String);
    }
    
    private State(OperatorInfo this$0, int param1Int) {
      super((String)this$0, param1Int);
    }
    
    public static final State UNKNOWN = new State("UNKNOWN", 0);
    
    static {
      State state = new State("FORBIDDEN", 3);
      $VALUES = new State[] { UNKNOWN, AVAILABLE, CURRENT, state };
    }
  }
  
  private State mState = State.UNKNOWN;
  
  private int mRan = 0;
  
  private String mOperatorNumeric;
  
  private String mOperatorAlphaShort;
  
  private String mOperatorAlphaLong;
  
  private String mNetworktype;
  
  public String getOperatorAlphaLong() {
    return this.mOperatorAlphaLong;
  }
  
  public String getOperatorAlphaShort() {
    return this.mOperatorAlphaShort;
  }
  
  public String getOperatorNumeric() {
    return this.mOperatorNumeric;
  }
  
  public State getState() {
    return this.mState;
  }
  
  public int getRan() {
    return this.mRan;
  }
  
  OperatorInfo(String paramString1, String paramString2, String paramString3, State paramState) {
    this.mOperatorAlphaLong = paramString1;
    this.mOperatorAlphaShort = paramString2;
    this.mOperatorNumeric = paramString3;
    this.mState = paramState;
  }
  
  OperatorInfo(String paramString1, String paramString2, String paramString3, State paramState, int paramInt) {
    this(paramString1, paramString2, paramString3, paramState);
    this.mRan = paramInt;
  }
  
  public OperatorInfo(String paramString1, String paramString2, String paramString3, String paramString4) {
    this(paramString1, paramString2, paramString3, state);
  }
  
  public OperatorInfo(String paramString1, String paramString2, String paramString3, int paramInt) {
    this(paramString1, paramString2, paramString3);
    this.mRan = paramInt;
  }
  
  public OperatorInfo(String paramString1, String paramString2, String paramString3) {
    this(paramString1, paramString2, paramString3, State.UNKNOWN);
  }
  
  private static State rilStateToState(String paramString) {
    if (paramString.equals("unknown"))
      return State.UNKNOWN; 
    if (paramString.equals("available"))
      return State.AVAILABLE; 
    if (paramString.equals("current"))
      return State.CURRENT; 
    if (paramString.equals("forbidden"))
      return State.FORBIDDEN; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("RIL impl error: Invalid network state '");
    stringBuilder.append(paramString);
    stringBuilder.append("'");
    throw new RuntimeException(stringBuilder.toString());
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("OperatorInfo ");
    stringBuilder.append(this.mOperatorAlphaLong);
    stringBuilder.append("/");
    stringBuilder.append(this.mOperatorAlphaShort);
    stringBuilder.append("/");
    stringBuilder.append(this.mOperatorNumeric);
    stringBuilder.append("/");
    stringBuilder.append(this.mState);
    stringBuilder.append("/");
    stringBuilder.append(this.mRan);
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mOperatorAlphaLong);
    paramParcel.writeString(this.mOperatorAlphaShort);
    paramParcel.writeString(this.mOperatorNumeric);
    paramParcel.writeSerializable(this.mState);
    paramParcel.writeString(this.mNetworktype);
    paramParcel.writeInt(this.mRan);
  }
  
  public static final Parcelable.Creator<OperatorInfo> CREATOR = new Parcelable.Creator<OperatorInfo>() {
      public OperatorInfo createFromParcel(Parcel param1Parcel) {
        String str1 = param1Parcel.readString();
        String str2 = param1Parcel.readString();
        String str3 = param1Parcel.readString();
        OperatorInfo.State state = (OperatorInfo.State)param1Parcel.readSerializable();
        return new OperatorInfo(str1, str2, str3, state, param1Parcel.readInt());
      }
      
      public OperatorInfo[] newArray(int param1Int) {
        return new OperatorInfo[param1Int];
      }
    };
}
