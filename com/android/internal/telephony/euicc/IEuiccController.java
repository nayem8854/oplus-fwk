package com.android.internal.telephony.euicc;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.euicc.DownloadableSubscription;
import android.telephony.euicc.EuiccInfo;
import java.util.List;

public interface IEuiccController extends IInterface {
  void continueOperation(int paramInt, Intent paramIntent, Bundle paramBundle) throws RemoteException;
  
  void deleteSubscription(int paramInt1, int paramInt2, String paramString, PendingIntent paramPendingIntent) throws RemoteException;
  
  void downloadSubscription(int paramInt, DownloadableSubscription paramDownloadableSubscription, boolean paramBoolean, String paramString, Bundle paramBundle, PendingIntent paramPendingIntent) throws RemoteException;
  
  void eraseSubscriptions(int paramInt, PendingIntent paramPendingIntent) throws RemoteException;
  
  void eraseSubscriptionsWithOptions(int paramInt1, int paramInt2, PendingIntent paramPendingIntent) throws RemoteException;
  
  void getDefaultDownloadableSubscriptionList(int paramInt, String paramString, PendingIntent paramPendingIntent) throws RemoteException;
  
  void getDownloadableSubscriptionMetadata(int paramInt, DownloadableSubscription paramDownloadableSubscription, String paramString, PendingIntent paramPendingIntent) throws RemoteException;
  
  String getEid(int paramInt, String paramString) throws RemoteException;
  
  EuiccInfo getEuiccInfo(int paramInt) throws RemoteException;
  
  int getOtaStatus(int paramInt) throws RemoteException;
  
  List<String> getSupportedCountries(boolean paramBoolean) throws RemoteException;
  
  boolean isSupportedCountry(String paramString) throws RemoteException;
  
  void retainSubscriptionsForFactoryReset(int paramInt, PendingIntent paramPendingIntent) throws RemoteException;
  
  void setSupportedCountries(boolean paramBoolean, List<String> paramList) throws RemoteException;
  
  void switchToSubscription(int paramInt1, int paramInt2, String paramString, PendingIntent paramPendingIntent) throws RemoteException;
  
  void updateSubscriptionNickname(int paramInt1, int paramInt2, String paramString1, String paramString2, PendingIntent paramPendingIntent) throws RemoteException;
  
  class Default implements IEuiccController {
    public void continueOperation(int param1Int, Intent param1Intent, Bundle param1Bundle) throws RemoteException {}
    
    public void getDownloadableSubscriptionMetadata(int param1Int, DownloadableSubscription param1DownloadableSubscription, String param1String, PendingIntent param1PendingIntent) throws RemoteException {}
    
    public void getDefaultDownloadableSubscriptionList(int param1Int, String param1String, PendingIntent param1PendingIntent) throws RemoteException {}
    
    public String getEid(int param1Int, String param1String) throws RemoteException {
      return null;
    }
    
    public int getOtaStatus(int param1Int) throws RemoteException {
      return 0;
    }
    
    public void downloadSubscription(int param1Int, DownloadableSubscription param1DownloadableSubscription, boolean param1Boolean, String param1String, Bundle param1Bundle, PendingIntent param1PendingIntent) throws RemoteException {}
    
    public EuiccInfo getEuiccInfo(int param1Int) throws RemoteException {
      return null;
    }
    
    public void deleteSubscription(int param1Int1, int param1Int2, String param1String, PendingIntent param1PendingIntent) throws RemoteException {}
    
    public void switchToSubscription(int param1Int1, int param1Int2, String param1String, PendingIntent param1PendingIntent) throws RemoteException {}
    
    public void updateSubscriptionNickname(int param1Int1, int param1Int2, String param1String1, String param1String2, PendingIntent param1PendingIntent) throws RemoteException {}
    
    public void eraseSubscriptions(int param1Int, PendingIntent param1PendingIntent) throws RemoteException {}
    
    public void eraseSubscriptionsWithOptions(int param1Int1, int param1Int2, PendingIntent param1PendingIntent) throws RemoteException {}
    
    public void retainSubscriptionsForFactoryReset(int param1Int, PendingIntent param1PendingIntent) throws RemoteException {}
    
    public void setSupportedCountries(boolean param1Boolean, List<String> param1List) throws RemoteException {}
    
    public List<String> getSupportedCountries(boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public boolean isSupportedCountry(String param1String) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IEuiccController {
    private static final String DESCRIPTOR = "com.android.internal.telephony.euicc.IEuiccController";
    
    static final int TRANSACTION_continueOperation = 1;
    
    static final int TRANSACTION_deleteSubscription = 8;
    
    static final int TRANSACTION_downloadSubscription = 6;
    
    static final int TRANSACTION_eraseSubscriptions = 11;
    
    static final int TRANSACTION_eraseSubscriptionsWithOptions = 12;
    
    static final int TRANSACTION_getDefaultDownloadableSubscriptionList = 3;
    
    static final int TRANSACTION_getDownloadableSubscriptionMetadata = 2;
    
    static final int TRANSACTION_getEid = 4;
    
    static final int TRANSACTION_getEuiccInfo = 7;
    
    static final int TRANSACTION_getOtaStatus = 5;
    
    static final int TRANSACTION_getSupportedCountries = 15;
    
    static final int TRANSACTION_isSupportedCountry = 16;
    
    static final int TRANSACTION_retainSubscriptionsForFactoryReset = 13;
    
    static final int TRANSACTION_setSupportedCountries = 14;
    
    static final int TRANSACTION_switchToSubscription = 9;
    
    static final int TRANSACTION_updateSubscriptionNickname = 10;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.euicc.IEuiccController");
    }
    
    public static IEuiccController asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.euicc.IEuiccController");
      if (iInterface != null && iInterface instanceof IEuiccController)
        return (IEuiccController)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 16:
          return "isSupportedCountry";
        case 15:
          return "getSupportedCountries";
        case 14:
          return "setSupportedCountries";
        case 13:
          return "retainSubscriptionsForFactoryReset";
        case 12:
          return "eraseSubscriptionsWithOptions";
        case 11:
          return "eraseSubscriptions";
        case 10:
          return "updateSubscriptionNickname";
        case 9:
          return "switchToSubscription";
        case 8:
          return "deleteSubscription";
        case 7:
          return "getEuiccInfo";
        case 6:
          return "downloadSubscription";
        case 5:
          return "getOtaStatus";
        case 4:
          return "getEid";
        case 3:
          return "getDefaultDownloadableSubscriptionList";
        case 2:
          return "getDownloadableSubscriptionMetadata";
        case 1:
          break;
      } 
      return "continueOperation";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        boolean bool;
        String str2;
        List<String> list;
        EuiccInfo euiccInfo;
        String str1, str3, str4;
        boolean bool1 = false, bool2 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 16:
            param1Parcel1.enforceInterface("com.android.internal.telephony.euicc.IEuiccController");
            str2 = param1Parcel1.readString();
            bool = isSupportedCountry(str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 15:
            str2.enforceInterface("com.android.internal.telephony.euicc.IEuiccController");
            bool1 = bool2;
            if (str2.readInt() != 0)
              bool1 = true; 
            list = getSupportedCountries(bool1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list);
            return true;
          case 14:
            list.enforceInterface("com.android.internal.telephony.euicc.IEuiccController");
            if (list.readInt() != 0)
              bool1 = true; 
            list = list.createStringArrayList();
            setSupportedCountries(bool1, list);
            param1Parcel2.writeNoException();
            return true;
          case 13:
            list.enforceInterface("com.android.internal.telephony.euicc.IEuiccController");
            i = list.readInt();
            if (list.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)list);
            } else {
              list = null;
            } 
            retainSubscriptionsForFactoryReset(i, (PendingIntent)list);
            return true;
          case 12:
            list.enforceInterface("com.android.internal.telephony.euicc.IEuiccController");
            param1Int2 = list.readInt();
            i = list.readInt();
            if (list.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)list);
            } else {
              list = null;
            } 
            eraseSubscriptionsWithOptions(param1Int2, i, (PendingIntent)list);
            return true;
          case 11:
            list.enforceInterface("com.android.internal.telephony.euicc.IEuiccController");
            i = list.readInt();
            if (list.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)list);
            } else {
              list = null;
            } 
            eraseSubscriptions(i, (PendingIntent)list);
            return true;
          case 10:
            list.enforceInterface("com.android.internal.telephony.euicc.IEuiccController");
            i = list.readInt();
            param1Int2 = list.readInt();
            str3 = list.readString();
            str = list.readString();
            if (list.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)list);
            } else {
              list = null;
            } 
            updateSubscriptionNickname(i, param1Int2, str3, str, (PendingIntent)list);
            return true;
          case 9:
            list.enforceInterface("com.android.internal.telephony.euicc.IEuiccController");
            param1Int2 = list.readInt();
            i = list.readInt();
            str = list.readString();
            if (list.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)list);
            } else {
              list = null;
            } 
            switchToSubscription(param1Int2, i, str, (PendingIntent)list);
            return true;
          case 8:
            list.enforceInterface("com.android.internal.telephony.euicc.IEuiccController");
            i = list.readInt();
            param1Int2 = list.readInt();
            str = list.readString();
            if (list.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)list);
            } else {
              list = null;
            } 
            deleteSubscription(i, param1Int2, str, (PendingIntent)list);
            return true;
          case 7:
            list.enforceInterface("com.android.internal.telephony.euicc.IEuiccController");
            i = list.readInt();
            euiccInfo = getEuiccInfo(i);
            str.writeNoException();
            if (euiccInfo != null) {
              str.writeInt(1);
              euiccInfo.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 6:
            euiccInfo.enforceInterface("com.android.internal.telephony.euicc.IEuiccController");
            i = euiccInfo.readInt();
            if (euiccInfo.readInt() != 0) {
              DownloadableSubscription downloadableSubscription = (DownloadableSubscription)DownloadableSubscription.CREATOR.createFromParcel((Parcel)euiccInfo);
            } else {
              str = null;
            } 
            if (euiccInfo.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            str4 = euiccInfo.readString();
            if (euiccInfo.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)euiccInfo);
            } else {
              str3 = null;
            } 
            if (euiccInfo.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)euiccInfo);
            } else {
              euiccInfo = null;
            } 
            downloadSubscription(i, (DownloadableSubscription)str, bool1, str4, (Bundle)str3, (PendingIntent)euiccInfo);
            return true;
          case 5:
            euiccInfo.enforceInterface("com.android.internal.telephony.euicc.IEuiccController");
            i = euiccInfo.readInt();
            i = getOtaStatus(i);
            str.writeNoException();
            str.writeInt(i);
            return true;
          case 4:
            euiccInfo.enforceInterface("com.android.internal.telephony.euicc.IEuiccController");
            i = euiccInfo.readInt();
            str1 = euiccInfo.readString();
            str1 = getEid(i, str1);
            str.writeNoException();
            str.writeString(str1);
            return true;
          case 3:
            str1.enforceInterface("com.android.internal.telephony.euicc.IEuiccController");
            i = str1.readInt();
            str = str1.readString();
            if (str1.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            getDefaultDownloadableSubscriptionList(i, str, (PendingIntent)str1);
            return true;
          case 2:
            str1.enforceInterface("com.android.internal.telephony.euicc.IEuiccController");
            i = str1.readInt();
            if (str1.readInt() != 0) {
              DownloadableSubscription downloadableSubscription = (DownloadableSubscription)DownloadableSubscription.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str = null;
            } 
            str3 = str1.readString();
            if (str1.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            getDownloadableSubscriptionMetadata(i, (DownloadableSubscription)str, str3, (PendingIntent)str1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("com.android.internal.telephony.euicc.IEuiccController");
        int i = str1.readInt();
        if (str1.readInt() != 0) {
          Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)str1);
        } else {
          str = null;
        } 
        if (str1.readInt() != 0) {
          Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)str1);
        } else {
          str1 = null;
        } 
        continueOperation(i, (Intent)str, (Bundle)str1);
        return true;
      } 
      str.writeString("com.android.internal.telephony.euicc.IEuiccController");
      return true;
    }
    
    private static class Proxy implements IEuiccController {
      public static IEuiccController sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.euicc.IEuiccController";
      }
      
      public void continueOperation(int param2Int, Intent param2Intent, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccController");
          parcel.writeInt(param2Int);
          if (param2Intent != null) {
            parcel.writeInt(1);
            param2Intent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IEuiccController.Stub.getDefaultImpl() != null) {
            IEuiccController.Stub.getDefaultImpl().continueOperation(param2Int, param2Intent, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getDownloadableSubscriptionMetadata(int param2Int, DownloadableSubscription param2DownloadableSubscription, String param2String, PendingIntent param2PendingIntent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccController");
          parcel.writeInt(param2Int);
          if (param2DownloadableSubscription != null) {
            parcel.writeInt(1);
            param2DownloadableSubscription.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeString(param2String);
          if (param2PendingIntent != null) {
            parcel.writeInt(1);
            param2PendingIntent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IEuiccController.Stub.getDefaultImpl() != null) {
            IEuiccController.Stub.getDefaultImpl().getDownloadableSubscriptionMetadata(param2Int, param2DownloadableSubscription, param2String, param2PendingIntent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getDefaultDownloadableSubscriptionList(int param2Int, String param2String, PendingIntent param2PendingIntent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccController");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          if (param2PendingIntent != null) {
            parcel.writeInt(1);
            param2PendingIntent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IEuiccController.Stub.getDefaultImpl() != null) {
            IEuiccController.Stub.getDefaultImpl().getDefaultDownloadableSubscriptionList(param2Int, param2String, param2PendingIntent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public String getEid(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccController");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IEuiccController.Stub.getDefaultImpl() != null) {
            param2String = IEuiccController.Stub.getDefaultImpl().getEid(param2Int, param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getOtaStatus(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccController");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IEuiccController.Stub.getDefaultImpl() != null) {
            param2Int = IEuiccController.Stub.getDefaultImpl().getOtaStatus(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void downloadSubscription(int param2Int, DownloadableSubscription param2DownloadableSubscription, boolean param2Boolean, String param2String, Bundle param2Bundle, PendingIntent param2PendingIntent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccController");
          try {
            boolean bool;
            parcel.writeInt(param2Int);
            if (param2DownloadableSubscription != null) {
              parcel.writeInt(1);
              param2DownloadableSubscription.writeToParcel(parcel, 0);
            } else {
              parcel.writeInt(0);
            } 
            if (param2Boolean) {
              bool = true;
            } else {
              bool = false;
            } 
            parcel.writeInt(bool);
            try {
              parcel.writeString(param2String);
              if (param2Bundle != null) {
                parcel.writeInt(1);
                param2Bundle.writeToParcel(parcel, 0);
              } else {
                parcel.writeInt(0);
              } 
              if (param2PendingIntent != null) {
                parcel.writeInt(1);
                param2PendingIntent.writeToParcel(parcel, 0);
              } else {
                parcel.writeInt(0);
              } 
              try {
                boolean bool1 = this.mRemote.transact(6, parcel, null, 1);
                if (!bool1 && IEuiccController.Stub.getDefaultImpl() != null) {
                  IEuiccController.Stub.getDefaultImpl().downloadSubscription(param2Int, param2DownloadableSubscription, param2Boolean, param2String, param2Bundle, param2PendingIntent);
                  parcel.recycle();
                  return;
                } 
                parcel.recycle();
                return;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2DownloadableSubscription;
      }
      
      public EuiccInfo getEuiccInfo(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          EuiccInfo euiccInfo;
          parcel1.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccController");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IEuiccController.Stub.getDefaultImpl() != null) {
            euiccInfo = IEuiccController.Stub.getDefaultImpl().getEuiccInfo(param2Int);
            return euiccInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            euiccInfo = (EuiccInfo)EuiccInfo.CREATOR.createFromParcel(parcel2);
          } else {
            euiccInfo = null;
          } 
          return euiccInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deleteSubscription(int param2Int1, int param2Int2, String param2String, PendingIntent param2PendingIntent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccController");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeString(param2String);
          if (param2PendingIntent != null) {
            parcel.writeInt(1);
            param2PendingIntent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IEuiccController.Stub.getDefaultImpl() != null) {
            IEuiccController.Stub.getDefaultImpl().deleteSubscription(param2Int1, param2Int2, param2String, param2PendingIntent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void switchToSubscription(int param2Int1, int param2Int2, String param2String, PendingIntent param2PendingIntent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccController");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeString(param2String);
          if (param2PendingIntent != null) {
            parcel.writeInt(1);
            param2PendingIntent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IEuiccController.Stub.getDefaultImpl() != null) {
            IEuiccController.Stub.getDefaultImpl().switchToSubscription(param2Int1, param2Int2, param2String, param2PendingIntent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void updateSubscriptionNickname(int param2Int1, int param2Int2, String param2String1, String param2String2, PendingIntent param2PendingIntent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccController");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          if (param2PendingIntent != null) {
            parcel.writeInt(1);
            param2PendingIntent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel, null, 1);
          if (!bool && IEuiccController.Stub.getDefaultImpl() != null) {
            IEuiccController.Stub.getDefaultImpl().updateSubscriptionNickname(param2Int1, param2Int2, param2String1, param2String2, param2PendingIntent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void eraseSubscriptions(int param2Int, PendingIntent param2PendingIntent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccController");
          parcel.writeInt(param2Int);
          if (param2PendingIntent != null) {
            parcel.writeInt(1);
            param2PendingIntent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(11, parcel, null, 1);
          if (!bool && IEuiccController.Stub.getDefaultImpl() != null) {
            IEuiccController.Stub.getDefaultImpl().eraseSubscriptions(param2Int, param2PendingIntent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void eraseSubscriptionsWithOptions(int param2Int1, int param2Int2, PendingIntent param2PendingIntent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccController");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          if (param2PendingIntent != null) {
            parcel.writeInt(1);
            param2PendingIntent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(12, parcel, null, 1);
          if (!bool && IEuiccController.Stub.getDefaultImpl() != null) {
            IEuiccController.Stub.getDefaultImpl().eraseSubscriptionsWithOptions(param2Int1, param2Int2, param2PendingIntent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void retainSubscriptionsForFactoryReset(int param2Int, PendingIntent param2PendingIntent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccController");
          parcel.writeInt(param2Int);
          if (param2PendingIntent != null) {
            parcel.writeInt(1);
            param2PendingIntent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(13, parcel, null, 1);
          if (!bool && IEuiccController.Stub.getDefaultImpl() != null) {
            IEuiccController.Stub.getDefaultImpl().retainSubscriptionsForFactoryReset(param2Int, param2PendingIntent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setSupportedCountries(boolean param2Boolean, List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccController");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeStringList(param2List);
          boolean bool1 = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool1 && IEuiccController.Stub.getDefaultImpl() != null) {
            IEuiccController.Stub.getDefaultImpl().setSupportedCountries(param2Boolean, param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getSupportedCountries(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccController");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool1 && IEuiccController.Stub.getDefaultImpl() != null)
            return IEuiccController.Stub.getDefaultImpl().getSupportedCountries(param2Boolean); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isSupportedCountry(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccController");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(16, parcel1, parcel2, 0);
          if (!bool2 && IEuiccController.Stub.getDefaultImpl() != null) {
            bool1 = IEuiccController.Stub.getDefaultImpl().isSupportedCountry(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IEuiccController param1IEuiccController) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IEuiccController != null) {
          Proxy.sDefaultImpl = param1IEuiccController;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IEuiccController getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
