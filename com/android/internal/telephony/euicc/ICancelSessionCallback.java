package com.android.internal.telephony.euicc;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ICancelSessionCallback extends IInterface {
  void onComplete(int paramInt, byte[] paramArrayOfbyte) throws RemoteException;
  
  class Default implements ICancelSessionCallback {
    public void onComplete(int param1Int, byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICancelSessionCallback {
    private static final String DESCRIPTOR = "com.android.internal.telephony.euicc.ICancelSessionCallback";
    
    static final int TRANSACTION_onComplete = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.euicc.ICancelSessionCallback");
    }
    
    public static ICancelSessionCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.euicc.ICancelSessionCallback");
      if (iInterface != null && iInterface instanceof ICancelSessionCallback)
        return (ICancelSessionCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onComplete";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.telephony.euicc.ICancelSessionCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.telephony.euicc.ICancelSessionCallback");
      param1Int1 = param1Parcel1.readInt();
      byte[] arrayOfByte = param1Parcel1.createByteArray();
      onComplete(param1Int1, arrayOfByte);
      return true;
    }
    
    private static class Proxy implements ICancelSessionCallback {
      public static ICancelSessionCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.euicc.ICancelSessionCallback";
      }
      
      public void onComplete(int param2Int, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.ICancelSessionCallback");
          parcel.writeInt(param2Int);
          parcel.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && ICancelSessionCallback.Stub.getDefaultImpl() != null) {
            ICancelSessionCallback.Stub.getDefaultImpl().onComplete(param2Int, param2ArrayOfbyte);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICancelSessionCallback param1ICancelSessionCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICancelSessionCallback != null) {
          Proxy.sDefaultImpl = param1ICancelSessionCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICancelSessionCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
