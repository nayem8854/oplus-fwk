package com.android.internal.telephony.euicc;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.euicc.EuiccNotification;

public interface IRetrieveNotificationCallback extends IInterface {
  void onComplete(int paramInt, EuiccNotification paramEuiccNotification) throws RemoteException;
  
  class Default implements IRetrieveNotificationCallback {
    public void onComplete(int param1Int, EuiccNotification param1EuiccNotification) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IRetrieveNotificationCallback {
    private static final String DESCRIPTOR = "com.android.internal.telephony.euicc.IRetrieveNotificationCallback";
    
    static final int TRANSACTION_onComplete = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.euicc.IRetrieveNotificationCallback");
    }
    
    public static IRetrieveNotificationCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.euicc.IRetrieveNotificationCallback");
      if (iInterface != null && iInterface instanceof IRetrieveNotificationCallback)
        return (IRetrieveNotificationCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onComplete";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.telephony.euicc.IRetrieveNotificationCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.telephony.euicc.IRetrieveNotificationCallback");
      param1Int1 = param1Parcel1.readInt();
      if (param1Parcel1.readInt() != 0) {
        EuiccNotification euiccNotification = (EuiccNotification)EuiccNotification.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onComplete(param1Int1, (EuiccNotification)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IRetrieveNotificationCallback {
      public static IRetrieveNotificationCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.euicc.IRetrieveNotificationCallback";
      }
      
      public void onComplete(int param2Int, EuiccNotification param2EuiccNotification) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IRetrieveNotificationCallback");
          parcel.writeInt(param2Int);
          if (param2EuiccNotification != null) {
            parcel.writeInt(1);
            param2EuiccNotification.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IRetrieveNotificationCallback.Stub.getDefaultImpl() != null) {
            IRetrieveNotificationCallback.Stub.getDefaultImpl().onComplete(param2Int, param2EuiccNotification);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IRetrieveNotificationCallback param1IRetrieveNotificationCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IRetrieveNotificationCallback != null) {
          Proxy.sDefaultImpl = param1IRetrieveNotificationCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IRetrieveNotificationCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
