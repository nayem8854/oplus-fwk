package com.android.internal.telephony.euicc;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IEuiccCardController extends IInterface {
  void authenticateServer(String paramString1, String paramString2, String paramString3, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, byte[] paramArrayOfbyte3, byte[] paramArrayOfbyte4, IAuthenticateServerCallback paramIAuthenticateServerCallback) throws RemoteException;
  
  void cancelSession(String paramString1, String paramString2, byte[] paramArrayOfbyte, int paramInt, ICancelSessionCallback paramICancelSessionCallback) throws RemoteException;
  
  void deleteProfile(String paramString1, String paramString2, String paramString3, IDeleteProfileCallback paramIDeleteProfileCallback) throws RemoteException;
  
  void disableProfile(String paramString1, String paramString2, String paramString3, boolean paramBoolean, IDisableProfileCallback paramIDisableProfileCallback) throws RemoteException;
  
  void getAllProfiles(String paramString1, String paramString2, IGetAllProfilesCallback paramIGetAllProfilesCallback) throws RemoteException;
  
  void getDefaultSmdpAddress(String paramString1, String paramString2, IGetDefaultSmdpAddressCallback paramIGetDefaultSmdpAddressCallback) throws RemoteException;
  
  void getEuiccChallenge(String paramString1, String paramString2, IGetEuiccChallengeCallback paramIGetEuiccChallengeCallback) throws RemoteException;
  
  void getEuiccInfo1(String paramString1, String paramString2, IGetEuiccInfo1Callback paramIGetEuiccInfo1Callback) throws RemoteException;
  
  void getEuiccInfo2(String paramString1, String paramString2, IGetEuiccInfo2Callback paramIGetEuiccInfo2Callback) throws RemoteException;
  
  void getProfile(String paramString1, String paramString2, String paramString3, IGetProfileCallback paramIGetProfileCallback) throws RemoteException;
  
  void getRulesAuthTable(String paramString1, String paramString2, IGetRulesAuthTableCallback paramIGetRulesAuthTableCallback) throws RemoteException;
  
  void getSmdsAddress(String paramString1, String paramString2, IGetSmdsAddressCallback paramIGetSmdsAddressCallback) throws RemoteException;
  
  void listNotifications(String paramString1, String paramString2, int paramInt, IListNotificationsCallback paramIListNotificationsCallback) throws RemoteException;
  
  void loadBoundProfilePackage(String paramString1, String paramString2, byte[] paramArrayOfbyte, ILoadBoundProfilePackageCallback paramILoadBoundProfilePackageCallback) throws RemoteException;
  
  void prepareDownload(String paramString1, String paramString2, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, byte[] paramArrayOfbyte3, byte[] paramArrayOfbyte4, IPrepareDownloadCallback paramIPrepareDownloadCallback) throws RemoteException;
  
  void removeNotificationFromList(String paramString1, String paramString2, int paramInt, IRemoveNotificationFromListCallback paramIRemoveNotificationFromListCallback) throws RemoteException;
  
  void resetMemory(String paramString1, String paramString2, int paramInt, IResetMemoryCallback paramIResetMemoryCallback) throws RemoteException;
  
  void retrieveNotification(String paramString1, String paramString2, int paramInt, IRetrieveNotificationCallback paramIRetrieveNotificationCallback) throws RemoteException;
  
  void retrieveNotificationList(String paramString1, String paramString2, int paramInt, IRetrieveNotificationListCallback paramIRetrieveNotificationListCallback) throws RemoteException;
  
  void setDefaultSmdpAddress(String paramString1, String paramString2, String paramString3, ISetDefaultSmdpAddressCallback paramISetDefaultSmdpAddressCallback) throws RemoteException;
  
  void setNickname(String paramString1, String paramString2, String paramString3, String paramString4, ISetNicknameCallback paramISetNicknameCallback) throws RemoteException;
  
  void switchToProfile(String paramString1, String paramString2, String paramString3, boolean paramBoolean, ISwitchToProfileCallback paramISwitchToProfileCallback) throws RemoteException;
  
  class Default implements IEuiccCardController {
    public void getAllProfiles(String param1String1, String param1String2, IGetAllProfilesCallback param1IGetAllProfilesCallback) throws RemoteException {}
    
    public void getProfile(String param1String1, String param1String2, String param1String3, IGetProfileCallback param1IGetProfileCallback) throws RemoteException {}
    
    public void disableProfile(String param1String1, String param1String2, String param1String3, boolean param1Boolean, IDisableProfileCallback param1IDisableProfileCallback) throws RemoteException {}
    
    public void switchToProfile(String param1String1, String param1String2, String param1String3, boolean param1Boolean, ISwitchToProfileCallback param1ISwitchToProfileCallback) throws RemoteException {}
    
    public void setNickname(String param1String1, String param1String2, String param1String3, String param1String4, ISetNicknameCallback param1ISetNicknameCallback) throws RemoteException {}
    
    public void deleteProfile(String param1String1, String param1String2, String param1String3, IDeleteProfileCallback param1IDeleteProfileCallback) throws RemoteException {}
    
    public void resetMemory(String param1String1, String param1String2, int param1Int, IResetMemoryCallback param1IResetMemoryCallback) throws RemoteException {}
    
    public void getDefaultSmdpAddress(String param1String1, String param1String2, IGetDefaultSmdpAddressCallback param1IGetDefaultSmdpAddressCallback) throws RemoteException {}
    
    public void getSmdsAddress(String param1String1, String param1String2, IGetSmdsAddressCallback param1IGetSmdsAddressCallback) throws RemoteException {}
    
    public void setDefaultSmdpAddress(String param1String1, String param1String2, String param1String3, ISetDefaultSmdpAddressCallback param1ISetDefaultSmdpAddressCallback) throws RemoteException {}
    
    public void getRulesAuthTable(String param1String1, String param1String2, IGetRulesAuthTableCallback param1IGetRulesAuthTableCallback) throws RemoteException {}
    
    public void getEuiccChallenge(String param1String1, String param1String2, IGetEuiccChallengeCallback param1IGetEuiccChallengeCallback) throws RemoteException {}
    
    public void getEuiccInfo1(String param1String1, String param1String2, IGetEuiccInfo1Callback param1IGetEuiccInfo1Callback) throws RemoteException {}
    
    public void getEuiccInfo2(String param1String1, String param1String2, IGetEuiccInfo2Callback param1IGetEuiccInfo2Callback) throws RemoteException {}
    
    public void authenticateServer(String param1String1, String param1String2, String param1String3, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2, byte[] param1ArrayOfbyte3, byte[] param1ArrayOfbyte4, IAuthenticateServerCallback param1IAuthenticateServerCallback) throws RemoteException {}
    
    public void prepareDownload(String param1String1, String param1String2, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2, byte[] param1ArrayOfbyte3, byte[] param1ArrayOfbyte4, IPrepareDownloadCallback param1IPrepareDownloadCallback) throws RemoteException {}
    
    public void loadBoundProfilePackage(String param1String1, String param1String2, byte[] param1ArrayOfbyte, ILoadBoundProfilePackageCallback param1ILoadBoundProfilePackageCallback) throws RemoteException {}
    
    public void cancelSession(String param1String1, String param1String2, byte[] param1ArrayOfbyte, int param1Int, ICancelSessionCallback param1ICancelSessionCallback) throws RemoteException {}
    
    public void listNotifications(String param1String1, String param1String2, int param1Int, IListNotificationsCallback param1IListNotificationsCallback) throws RemoteException {}
    
    public void retrieveNotificationList(String param1String1, String param1String2, int param1Int, IRetrieveNotificationListCallback param1IRetrieveNotificationListCallback) throws RemoteException {}
    
    public void retrieveNotification(String param1String1, String param1String2, int param1Int, IRetrieveNotificationCallback param1IRetrieveNotificationCallback) throws RemoteException {}
    
    public void removeNotificationFromList(String param1String1, String param1String2, int param1Int, IRemoveNotificationFromListCallback param1IRemoveNotificationFromListCallback) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IEuiccCardController {
    private static final String DESCRIPTOR = "com.android.internal.telephony.euicc.IEuiccCardController";
    
    static final int TRANSACTION_authenticateServer = 15;
    
    static final int TRANSACTION_cancelSession = 18;
    
    static final int TRANSACTION_deleteProfile = 6;
    
    static final int TRANSACTION_disableProfile = 3;
    
    static final int TRANSACTION_getAllProfiles = 1;
    
    static final int TRANSACTION_getDefaultSmdpAddress = 8;
    
    static final int TRANSACTION_getEuiccChallenge = 12;
    
    static final int TRANSACTION_getEuiccInfo1 = 13;
    
    static final int TRANSACTION_getEuiccInfo2 = 14;
    
    static final int TRANSACTION_getProfile = 2;
    
    static final int TRANSACTION_getRulesAuthTable = 11;
    
    static final int TRANSACTION_getSmdsAddress = 9;
    
    static final int TRANSACTION_listNotifications = 19;
    
    static final int TRANSACTION_loadBoundProfilePackage = 17;
    
    static final int TRANSACTION_prepareDownload = 16;
    
    static final int TRANSACTION_removeNotificationFromList = 22;
    
    static final int TRANSACTION_resetMemory = 7;
    
    static final int TRANSACTION_retrieveNotification = 21;
    
    static final int TRANSACTION_retrieveNotificationList = 20;
    
    static final int TRANSACTION_setDefaultSmdpAddress = 10;
    
    static final int TRANSACTION_setNickname = 5;
    
    static final int TRANSACTION_switchToProfile = 4;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.euicc.IEuiccCardController");
    }
    
    public static IEuiccCardController asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.euicc.IEuiccCardController");
      if (iInterface != null && iInterface instanceof IEuiccCardController)
        return (IEuiccCardController)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 22:
          return "removeNotificationFromList";
        case 21:
          return "retrieveNotification";
        case 20:
          return "retrieveNotificationList";
        case 19:
          return "listNotifications";
        case 18:
          return "cancelSession";
        case 17:
          return "loadBoundProfilePackage";
        case 16:
          return "prepareDownload";
        case 15:
          return "authenticateServer";
        case 14:
          return "getEuiccInfo2";
        case 13:
          return "getEuiccInfo1";
        case 12:
          return "getEuiccChallenge";
        case 11:
          return "getRulesAuthTable";
        case 10:
          return "setDefaultSmdpAddress";
        case 9:
          return "getSmdsAddress";
        case 8:
          return "getDefaultSmdpAddress";
        case 7:
          return "resetMemory";
        case 6:
          return "deleteProfile";
        case 5:
          return "setNickname";
        case 4:
          return "switchToProfile";
        case 3:
          return "disableProfile";
        case 2:
          return "getProfile";
        case 1:
          break;
      } 
      return "getAllProfiles";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        IRemoveNotificationFromListCallback iRemoveNotificationFromListCallback;
        IRetrieveNotificationCallback iRetrieveNotificationCallback;
        IRetrieveNotificationListCallback iRetrieveNotificationListCallback;
        IListNotificationsCallback iListNotificationsCallback;
        ICancelSessionCallback iCancelSessionCallback;
        ILoadBoundProfilePackageCallback iLoadBoundProfilePackageCallback;
        IPrepareDownloadCallback iPrepareDownloadCallback;
        IAuthenticateServerCallback iAuthenticateServerCallback;
        IGetEuiccInfo2Callback iGetEuiccInfo2Callback;
        IGetEuiccInfo1Callback iGetEuiccInfo1Callback;
        IGetEuiccChallengeCallback iGetEuiccChallengeCallback;
        IGetRulesAuthTableCallback iGetRulesAuthTableCallback;
        ISetDefaultSmdpAddressCallback iSetDefaultSmdpAddressCallback;
        IGetSmdsAddressCallback iGetSmdsAddressCallback;
        IGetDefaultSmdpAddressCallback iGetDefaultSmdpAddressCallback;
        IResetMemoryCallback iResetMemoryCallback;
        IDeleteProfileCallback iDeleteProfileCallback;
        ISetNicknameCallback iSetNicknameCallback;
        ISwitchToProfileCallback iSwitchToProfileCallback;
        IDisableProfileCallback iDisableProfileCallback;
        IGetProfileCallback iGetProfileCallback;
        String str1;
        byte[] arrayOfByte1;
        String str4;
        byte[] arrayOfByte3;
        String str3;
        byte[] arrayOfByte2;
        String str6;
        byte[] arrayOfByte4;
        String str5;
        byte[] arrayOfByte5;
        String str7;
        byte[] arrayOfByte6, arrayOfByte7;
        String str8;
        byte[] arrayOfByte8;
        boolean bool;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 22:
            param1Parcel1.enforceInterface("com.android.internal.telephony.euicc.IEuiccCardController");
            str1 = param1Parcel1.readString();
            str4 = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            iRemoveNotificationFromListCallback = IRemoveNotificationFromListCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            removeNotificationFromList(str1, str4, param1Int1, iRemoveNotificationFromListCallback);
            return true;
          case 21:
            iRemoveNotificationFromListCallback.enforceInterface("com.android.internal.telephony.euicc.IEuiccCardController");
            str4 = iRemoveNotificationFromListCallback.readString();
            str1 = iRemoveNotificationFromListCallback.readString();
            param1Int1 = iRemoveNotificationFromListCallback.readInt();
            iRetrieveNotificationCallback = IRetrieveNotificationCallback.Stub.asInterface(iRemoveNotificationFromListCallback.readStrongBinder());
            retrieveNotification(str4, str1, param1Int1, iRetrieveNotificationCallback);
            return true;
          case 20:
            iRetrieveNotificationCallback.enforceInterface("com.android.internal.telephony.euicc.IEuiccCardController");
            str4 = iRetrieveNotificationCallback.readString();
            str1 = iRetrieveNotificationCallback.readString();
            param1Int1 = iRetrieveNotificationCallback.readInt();
            iRetrieveNotificationListCallback = IRetrieveNotificationListCallback.Stub.asInterface(iRetrieveNotificationCallback.readStrongBinder());
            retrieveNotificationList(str4, str1, param1Int1, iRetrieveNotificationListCallback);
            return true;
          case 19:
            iRetrieveNotificationListCallback.enforceInterface("com.android.internal.telephony.euicc.IEuiccCardController");
            str4 = iRetrieveNotificationListCallback.readString();
            str1 = iRetrieveNotificationListCallback.readString();
            param1Int1 = iRetrieveNotificationListCallback.readInt();
            iListNotificationsCallback = IListNotificationsCallback.Stub.asInterface(iRetrieveNotificationListCallback.readStrongBinder());
            listNotifications(str4, str1, param1Int1, iListNotificationsCallback);
            return true;
          case 18:
            iListNotificationsCallback.enforceInterface("com.android.internal.telephony.euicc.IEuiccCardController");
            str6 = iListNotificationsCallback.readString();
            str1 = iListNotificationsCallback.readString();
            arrayOfByte3 = iListNotificationsCallback.createByteArray();
            param1Int1 = iListNotificationsCallback.readInt();
            iCancelSessionCallback = ICancelSessionCallback.Stub.asInterface(iListNotificationsCallback.readStrongBinder());
            cancelSession(str6, str1, arrayOfByte3, param1Int1, iCancelSessionCallback);
            return true;
          case 17:
            iCancelSessionCallback.enforceInterface("com.android.internal.telephony.euicc.IEuiccCardController");
            str3 = iCancelSessionCallback.readString();
            str1 = iCancelSessionCallback.readString();
            arrayOfByte4 = iCancelSessionCallback.createByteArray();
            iLoadBoundProfilePackageCallback = ILoadBoundProfilePackageCallback.Stub.asInterface(iCancelSessionCallback.readStrongBinder());
            loadBoundProfilePackage(str3, str1, arrayOfByte4, iLoadBoundProfilePackageCallback);
            return true;
          case 16:
            iLoadBoundProfilePackageCallback.enforceInterface("com.android.internal.telephony.euicc.IEuiccCardController");
            str3 = iLoadBoundProfilePackageCallback.readString();
            str5 = iLoadBoundProfilePackageCallback.readString();
            arrayOfByte5 = iLoadBoundProfilePackageCallback.createByteArray();
            arrayOfByte1 = iLoadBoundProfilePackageCallback.createByteArray();
            arrayOfByte6 = iLoadBoundProfilePackageCallback.createByteArray();
            arrayOfByte7 = iLoadBoundProfilePackageCallback.createByteArray();
            iPrepareDownloadCallback = IPrepareDownloadCallback.Stub.asInterface(iLoadBoundProfilePackageCallback.readStrongBinder());
            prepareDownload(str3, str5, arrayOfByte5, arrayOfByte1, arrayOfByte6, arrayOfByte7, iPrepareDownloadCallback);
            return true;
          case 15:
            iPrepareDownloadCallback.enforceInterface("com.android.internal.telephony.euicc.IEuiccCardController");
            str = iPrepareDownloadCallback.readString();
            str5 = iPrepareDownloadCallback.readString();
            str7 = iPrepareDownloadCallback.readString();
            arrayOfByte2 = iPrepareDownloadCallback.createByteArray();
            arrayOfByte6 = iPrepareDownloadCallback.createByteArray();
            arrayOfByte8 = iPrepareDownloadCallback.createByteArray();
            arrayOfByte7 = iPrepareDownloadCallback.createByteArray();
            iAuthenticateServerCallback = IAuthenticateServerCallback.Stub.asInterface(iPrepareDownloadCallback.readStrongBinder());
            authenticateServer(str, str5, str7, arrayOfByte2, arrayOfByte6, arrayOfByte8, arrayOfByte7, iAuthenticateServerCallback);
            return true;
          case 14:
            iAuthenticateServerCallback.enforceInterface("com.android.internal.telephony.euicc.IEuiccCardController");
            str2 = iAuthenticateServerCallback.readString();
            str = iAuthenticateServerCallback.readString();
            iGetEuiccInfo2Callback = IGetEuiccInfo2Callback.Stub.asInterface(iAuthenticateServerCallback.readStrongBinder());
            getEuiccInfo2(str2, str, iGetEuiccInfo2Callback);
            return true;
          case 13:
            iGetEuiccInfo2Callback.enforceInterface("com.android.internal.telephony.euicc.IEuiccCardController");
            str = iGetEuiccInfo2Callback.readString();
            str2 = iGetEuiccInfo2Callback.readString();
            iGetEuiccInfo1Callback = IGetEuiccInfo1Callback.Stub.asInterface(iGetEuiccInfo2Callback.readStrongBinder());
            getEuiccInfo1(str, str2, iGetEuiccInfo1Callback);
            return true;
          case 12:
            iGetEuiccInfo1Callback.enforceInterface("com.android.internal.telephony.euicc.IEuiccCardController");
            str = iGetEuiccInfo1Callback.readString();
            str2 = iGetEuiccInfo1Callback.readString();
            iGetEuiccChallengeCallback = IGetEuiccChallengeCallback.Stub.asInterface(iGetEuiccInfo1Callback.readStrongBinder());
            getEuiccChallenge(str, str2, iGetEuiccChallengeCallback);
            return true;
          case 11:
            iGetEuiccChallengeCallback.enforceInterface("com.android.internal.telephony.euicc.IEuiccCardController");
            str2 = iGetEuiccChallengeCallback.readString();
            str = iGetEuiccChallengeCallback.readString();
            iGetRulesAuthTableCallback = IGetRulesAuthTableCallback.Stub.asInterface(iGetEuiccChallengeCallback.readStrongBinder());
            getRulesAuthTable(str2, str, iGetRulesAuthTableCallback);
            return true;
          case 10:
            iGetRulesAuthTableCallback.enforceInterface("com.android.internal.telephony.euicc.IEuiccCardController");
            str = iGetRulesAuthTableCallback.readString();
            str2 = iGetRulesAuthTableCallback.readString();
            str5 = iGetRulesAuthTableCallback.readString();
            iSetDefaultSmdpAddressCallback = ISetDefaultSmdpAddressCallback.Stub.asInterface(iGetRulesAuthTableCallback.readStrongBinder());
            setDefaultSmdpAddress(str, str2, str5, iSetDefaultSmdpAddressCallback);
            return true;
          case 9:
            iSetDefaultSmdpAddressCallback.enforceInterface("com.android.internal.telephony.euicc.IEuiccCardController");
            str = iSetDefaultSmdpAddressCallback.readString();
            str2 = iSetDefaultSmdpAddressCallback.readString();
            iGetSmdsAddressCallback = IGetSmdsAddressCallback.Stub.asInterface(iSetDefaultSmdpAddressCallback.readStrongBinder());
            getSmdsAddress(str, str2, iGetSmdsAddressCallback);
            return true;
          case 8:
            iGetSmdsAddressCallback.enforceInterface("com.android.internal.telephony.euicc.IEuiccCardController");
            str2 = iGetSmdsAddressCallback.readString();
            str = iGetSmdsAddressCallback.readString();
            iGetDefaultSmdpAddressCallback = IGetDefaultSmdpAddressCallback.Stub.asInterface(iGetSmdsAddressCallback.readStrongBinder());
            getDefaultSmdpAddress(str2, str, iGetDefaultSmdpAddressCallback);
            return true;
          case 7:
            iGetDefaultSmdpAddressCallback.enforceInterface("com.android.internal.telephony.euicc.IEuiccCardController");
            str = iGetDefaultSmdpAddressCallback.readString();
            str2 = iGetDefaultSmdpAddressCallback.readString();
            param1Int1 = iGetDefaultSmdpAddressCallback.readInt();
            iResetMemoryCallback = IResetMemoryCallback.Stub.asInterface(iGetDefaultSmdpAddressCallback.readStrongBinder());
            resetMemory(str, str2, param1Int1, iResetMemoryCallback);
            return true;
          case 6:
            iResetMemoryCallback.enforceInterface("com.android.internal.telephony.euicc.IEuiccCardController");
            str2 = iResetMemoryCallback.readString();
            str5 = iResetMemoryCallback.readString();
            str = iResetMemoryCallback.readString();
            iDeleteProfileCallback = IDeleteProfileCallback.Stub.asInterface(iResetMemoryCallback.readStrongBinder());
            deleteProfile(str2, str5, str, iDeleteProfileCallback);
            return true;
          case 5:
            iDeleteProfileCallback.enforceInterface("com.android.internal.telephony.euicc.IEuiccCardController");
            str5 = iDeleteProfileCallback.readString();
            str8 = iDeleteProfileCallback.readString();
            str2 = iDeleteProfileCallback.readString();
            str = iDeleteProfileCallback.readString();
            iSetNicknameCallback = ISetNicknameCallback.Stub.asInterface(iDeleteProfileCallback.readStrongBinder());
            setNickname(str5, str8, str2, str, iSetNicknameCallback);
            return true;
          case 4:
            iSetNicknameCallback.enforceInterface("com.android.internal.telephony.euicc.IEuiccCardController");
            str5 = iSetNicknameCallback.readString();
            str = iSetNicknameCallback.readString();
            str2 = iSetNicknameCallback.readString();
            if (iSetNicknameCallback.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            iSwitchToProfileCallback = ISwitchToProfileCallback.Stub.asInterface(iSetNicknameCallback.readStrongBinder());
            switchToProfile(str5, str, str2, bool, iSwitchToProfileCallback);
            return true;
          case 3:
            iSwitchToProfileCallback.enforceInterface("com.android.internal.telephony.euicc.IEuiccCardController");
            str2 = iSwitchToProfileCallback.readString();
            str = iSwitchToProfileCallback.readString();
            str5 = iSwitchToProfileCallback.readString();
            if (iSwitchToProfileCallback.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            iDisableProfileCallback = IDisableProfileCallback.Stub.asInterface(iSwitchToProfileCallback.readStrongBinder());
            disableProfile(str2, str, str5, bool, iDisableProfileCallback);
            return true;
          case 2:
            iDisableProfileCallback.enforceInterface("com.android.internal.telephony.euicc.IEuiccCardController");
            str = iDisableProfileCallback.readString();
            str2 = iDisableProfileCallback.readString();
            str5 = iDisableProfileCallback.readString();
            iGetProfileCallback = IGetProfileCallback.Stub.asInterface(iDisableProfileCallback.readStrongBinder());
            getProfile(str, str2, str5, iGetProfileCallback);
            return true;
          case 1:
            break;
        } 
        iGetProfileCallback.enforceInterface("com.android.internal.telephony.euicc.IEuiccCardController");
        str = iGetProfileCallback.readString();
        String str2 = iGetProfileCallback.readString();
        IGetAllProfilesCallback iGetAllProfilesCallback = IGetAllProfilesCallback.Stub.asInterface(iGetProfileCallback.readStrongBinder());
        getAllProfiles(str, str2, iGetAllProfilesCallback);
        return true;
      } 
      str.writeString("com.android.internal.telephony.euicc.IEuiccCardController");
      return true;
    }
    
    private static class Proxy implements IEuiccCardController {
      public static IEuiccCardController sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.euicc.IEuiccCardController";
      }
      
      public void getAllProfiles(String param2String1, String param2String2, IGetAllProfilesCallback param2IGetAllProfilesCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccCardController");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          if (param2IGetAllProfilesCallback != null) {
            iBinder = param2IGetAllProfilesCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IEuiccCardController.Stub.getDefaultImpl() != null) {
            IEuiccCardController.Stub.getDefaultImpl().getAllProfiles(param2String1, param2String2, param2IGetAllProfilesCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getProfile(String param2String1, String param2String2, String param2String3, IGetProfileCallback param2IGetProfileCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccCardController");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          parcel.writeString(param2String3);
          if (param2IGetProfileCallback != null) {
            iBinder = param2IGetProfileCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IEuiccCardController.Stub.getDefaultImpl() != null) {
            IEuiccCardController.Stub.getDefaultImpl().getProfile(param2String1, param2String2, param2String3, param2IGetProfileCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void disableProfile(String param2String1, String param2String2, String param2String3, boolean param2Boolean, IDisableProfileCallback param2IDisableProfileCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccCardController");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          parcel.writeString(param2String3);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2IDisableProfileCallback != null) {
            iBinder = param2IDisableProfileCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool1 = this.mRemote.transact(3, parcel, null, 1);
          if (!bool1 && IEuiccCardController.Stub.getDefaultImpl() != null) {
            IEuiccCardController.Stub.getDefaultImpl().disableProfile(param2String1, param2String2, param2String3, param2Boolean, param2IDisableProfileCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void switchToProfile(String param2String1, String param2String2, String param2String3, boolean param2Boolean, ISwitchToProfileCallback param2ISwitchToProfileCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccCardController");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          parcel.writeString(param2String3);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2ISwitchToProfileCallback != null) {
            iBinder = param2ISwitchToProfileCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool1 = this.mRemote.transact(4, parcel, null, 1);
          if (!bool1 && IEuiccCardController.Stub.getDefaultImpl() != null) {
            IEuiccCardController.Stub.getDefaultImpl().switchToProfile(param2String1, param2String2, param2String3, param2Boolean, param2ISwitchToProfileCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setNickname(String param2String1, String param2String2, String param2String3, String param2String4, ISetNicknameCallback param2ISetNicknameCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccCardController");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          parcel.writeString(param2String3);
          parcel.writeString(param2String4);
          if (param2ISetNicknameCallback != null) {
            iBinder = param2ISetNicknameCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IEuiccCardController.Stub.getDefaultImpl() != null) {
            IEuiccCardController.Stub.getDefaultImpl().setNickname(param2String1, param2String2, param2String3, param2String4, param2ISetNicknameCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void deleteProfile(String param2String1, String param2String2, String param2String3, IDeleteProfileCallback param2IDeleteProfileCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccCardController");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          parcel.writeString(param2String3);
          if (param2IDeleteProfileCallback != null) {
            iBinder = param2IDeleteProfileCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IEuiccCardController.Stub.getDefaultImpl() != null) {
            IEuiccCardController.Stub.getDefaultImpl().deleteProfile(param2String1, param2String2, param2String3, param2IDeleteProfileCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void resetMemory(String param2String1, String param2String2, int param2Int, IResetMemoryCallback param2IResetMemoryCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccCardController");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          parcel.writeInt(param2Int);
          if (param2IResetMemoryCallback != null) {
            iBinder = param2IResetMemoryCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IEuiccCardController.Stub.getDefaultImpl() != null) {
            IEuiccCardController.Stub.getDefaultImpl().resetMemory(param2String1, param2String2, param2Int, param2IResetMemoryCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getDefaultSmdpAddress(String param2String1, String param2String2, IGetDefaultSmdpAddressCallback param2IGetDefaultSmdpAddressCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccCardController");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          if (param2IGetDefaultSmdpAddressCallback != null) {
            iBinder = param2IGetDefaultSmdpAddressCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IEuiccCardController.Stub.getDefaultImpl() != null) {
            IEuiccCardController.Stub.getDefaultImpl().getDefaultSmdpAddress(param2String1, param2String2, param2IGetDefaultSmdpAddressCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getSmdsAddress(String param2String1, String param2String2, IGetSmdsAddressCallback param2IGetSmdsAddressCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccCardController");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          if (param2IGetSmdsAddressCallback != null) {
            iBinder = param2IGetSmdsAddressCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IEuiccCardController.Stub.getDefaultImpl() != null) {
            IEuiccCardController.Stub.getDefaultImpl().getSmdsAddress(param2String1, param2String2, param2IGetSmdsAddressCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setDefaultSmdpAddress(String param2String1, String param2String2, String param2String3, ISetDefaultSmdpAddressCallback param2ISetDefaultSmdpAddressCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccCardController");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          parcel.writeString(param2String3);
          if (param2ISetDefaultSmdpAddressCallback != null) {
            iBinder = param2ISetDefaultSmdpAddressCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(10, parcel, null, 1);
          if (!bool && IEuiccCardController.Stub.getDefaultImpl() != null) {
            IEuiccCardController.Stub.getDefaultImpl().setDefaultSmdpAddress(param2String1, param2String2, param2String3, param2ISetDefaultSmdpAddressCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getRulesAuthTable(String param2String1, String param2String2, IGetRulesAuthTableCallback param2IGetRulesAuthTableCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccCardController");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          if (param2IGetRulesAuthTableCallback != null) {
            iBinder = param2IGetRulesAuthTableCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(11, parcel, null, 1);
          if (!bool && IEuiccCardController.Stub.getDefaultImpl() != null) {
            IEuiccCardController.Stub.getDefaultImpl().getRulesAuthTable(param2String1, param2String2, param2IGetRulesAuthTableCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getEuiccChallenge(String param2String1, String param2String2, IGetEuiccChallengeCallback param2IGetEuiccChallengeCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccCardController");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          if (param2IGetEuiccChallengeCallback != null) {
            iBinder = param2IGetEuiccChallengeCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(12, parcel, null, 1);
          if (!bool && IEuiccCardController.Stub.getDefaultImpl() != null) {
            IEuiccCardController.Stub.getDefaultImpl().getEuiccChallenge(param2String1, param2String2, param2IGetEuiccChallengeCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getEuiccInfo1(String param2String1, String param2String2, IGetEuiccInfo1Callback param2IGetEuiccInfo1Callback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccCardController");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          if (param2IGetEuiccInfo1Callback != null) {
            iBinder = param2IGetEuiccInfo1Callback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(13, parcel, null, 1);
          if (!bool && IEuiccCardController.Stub.getDefaultImpl() != null) {
            IEuiccCardController.Stub.getDefaultImpl().getEuiccInfo1(param2String1, param2String2, param2IGetEuiccInfo1Callback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getEuiccInfo2(String param2String1, String param2String2, IGetEuiccInfo2Callback param2IGetEuiccInfo2Callback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccCardController");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          if (param2IGetEuiccInfo2Callback != null) {
            iBinder = param2IGetEuiccInfo2Callback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(14, parcel, null, 1);
          if (!bool && IEuiccCardController.Stub.getDefaultImpl() != null) {
            IEuiccCardController.Stub.getDefaultImpl().getEuiccInfo2(param2String1, param2String2, param2IGetEuiccInfo2Callback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void authenticateServer(String param2String1, String param2String2, String param2String3, byte[] param2ArrayOfbyte1, byte[] param2ArrayOfbyte2, byte[] param2ArrayOfbyte3, byte[] param2ArrayOfbyte4, IAuthenticateServerCallback param2IAuthenticateServerCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccCardController");
          try {
            parcel.writeString(param2String1);
            try {
              parcel.writeString(param2String2);
              try {
                parcel.writeString(param2String3);
                try {
                  parcel.writeByteArray(param2ArrayOfbyte1);
                  try {
                    IBinder iBinder;
                    parcel.writeByteArray(param2ArrayOfbyte2);
                    parcel.writeByteArray(param2ArrayOfbyte3);
                    parcel.writeByteArray(param2ArrayOfbyte4);
                    if (param2IAuthenticateServerCallback != null) {
                      iBinder = param2IAuthenticateServerCallback.asBinder();
                    } else {
                      iBinder = null;
                    } 
                    parcel.writeStrongBinder(iBinder);
                    boolean bool = this.mRemote.transact(15, parcel, null, 1);
                    if (!bool && IEuiccCardController.Stub.getDefaultImpl() != null) {
                      IEuiccCardController.Stub.getDefaultImpl().authenticateServer(param2String1, param2String2, param2String3, param2ArrayOfbyte1, param2ArrayOfbyte2, param2ArrayOfbyte3, param2ArrayOfbyte4, param2IAuthenticateServerCallback);
                      parcel.recycle();
                      return;
                    } 
                    parcel.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2String1;
      }
      
      public void prepareDownload(String param2String1, String param2String2, byte[] param2ArrayOfbyte1, byte[] param2ArrayOfbyte2, byte[] param2ArrayOfbyte3, byte[] param2ArrayOfbyte4, IPrepareDownloadCallback param2IPrepareDownloadCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccCardController");
          try {
            parcel.writeString(param2String1);
            try {
              parcel.writeString(param2String2);
              try {
                parcel.writeByteArray(param2ArrayOfbyte1);
                try {
                  parcel.writeByteArray(param2ArrayOfbyte2);
                  try {
                    parcel.writeByteArray(param2ArrayOfbyte3);
                    try {
                      IBinder iBinder;
                      parcel.writeByteArray(param2ArrayOfbyte4);
                      if (param2IPrepareDownloadCallback != null) {
                        iBinder = param2IPrepareDownloadCallback.asBinder();
                      } else {
                        iBinder = null;
                      } 
                      parcel.writeStrongBinder(iBinder);
                      boolean bool = this.mRemote.transact(16, parcel, null, 1);
                      if (!bool && IEuiccCardController.Stub.getDefaultImpl() != null) {
                        IEuiccCardController.Stub.getDefaultImpl().prepareDownload(param2String1, param2String2, param2ArrayOfbyte1, param2ArrayOfbyte2, param2ArrayOfbyte3, param2ArrayOfbyte4, param2IPrepareDownloadCallback);
                        parcel.recycle();
                        return;
                      } 
                      parcel.recycle();
                      return;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2String1;
      }
      
      public void loadBoundProfilePackage(String param2String1, String param2String2, byte[] param2ArrayOfbyte, ILoadBoundProfilePackageCallback param2ILoadBoundProfilePackageCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccCardController");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          parcel.writeByteArray(param2ArrayOfbyte);
          if (param2ILoadBoundProfilePackageCallback != null) {
            iBinder = param2ILoadBoundProfilePackageCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(17, parcel, null, 1);
          if (!bool && IEuiccCardController.Stub.getDefaultImpl() != null) {
            IEuiccCardController.Stub.getDefaultImpl().loadBoundProfilePackage(param2String1, param2String2, param2ArrayOfbyte, param2ILoadBoundProfilePackageCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void cancelSession(String param2String1, String param2String2, byte[] param2ArrayOfbyte, int param2Int, ICancelSessionCallback param2ICancelSessionCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccCardController");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          parcel.writeByteArray(param2ArrayOfbyte);
          parcel.writeInt(param2Int);
          if (param2ICancelSessionCallback != null) {
            iBinder = param2ICancelSessionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(18, parcel, null, 1);
          if (!bool && IEuiccCardController.Stub.getDefaultImpl() != null) {
            IEuiccCardController.Stub.getDefaultImpl().cancelSession(param2String1, param2String2, param2ArrayOfbyte, param2Int, param2ICancelSessionCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void listNotifications(String param2String1, String param2String2, int param2Int, IListNotificationsCallback param2IListNotificationsCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccCardController");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          parcel.writeInt(param2Int);
          if (param2IListNotificationsCallback != null) {
            iBinder = param2IListNotificationsCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(19, parcel, null, 1);
          if (!bool && IEuiccCardController.Stub.getDefaultImpl() != null) {
            IEuiccCardController.Stub.getDefaultImpl().listNotifications(param2String1, param2String2, param2Int, param2IListNotificationsCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void retrieveNotificationList(String param2String1, String param2String2, int param2Int, IRetrieveNotificationListCallback param2IRetrieveNotificationListCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccCardController");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          parcel.writeInt(param2Int);
          if (param2IRetrieveNotificationListCallback != null) {
            iBinder = param2IRetrieveNotificationListCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(20, parcel, null, 1);
          if (!bool && IEuiccCardController.Stub.getDefaultImpl() != null) {
            IEuiccCardController.Stub.getDefaultImpl().retrieveNotificationList(param2String1, param2String2, param2Int, param2IRetrieveNotificationListCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void retrieveNotification(String param2String1, String param2String2, int param2Int, IRetrieveNotificationCallback param2IRetrieveNotificationCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccCardController");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          parcel.writeInt(param2Int);
          if (param2IRetrieveNotificationCallback != null) {
            iBinder = param2IRetrieveNotificationCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(21, parcel, null, 1);
          if (!bool && IEuiccCardController.Stub.getDefaultImpl() != null) {
            IEuiccCardController.Stub.getDefaultImpl().retrieveNotification(param2String1, param2String2, param2Int, param2IRetrieveNotificationCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void removeNotificationFromList(String param2String1, String param2String2, int param2Int, IRemoveNotificationFromListCallback param2IRemoveNotificationFromListCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IEuiccCardController");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          parcel.writeInt(param2Int);
          if (param2IRemoveNotificationFromListCallback != null) {
            iBinder = param2IRemoveNotificationFromListCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(22, parcel, null, 1);
          if (!bool && IEuiccCardController.Stub.getDefaultImpl() != null) {
            IEuiccCardController.Stub.getDefaultImpl().removeNotificationFromList(param2String1, param2String2, param2Int, param2IRemoveNotificationFromListCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IEuiccCardController param1IEuiccCardController) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IEuiccCardController != null) {
          Proxy.sDefaultImpl = param1IEuiccCardController;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IEuiccCardController getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
