package com.android.internal.telephony.euicc;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IPrepareDownloadCallback extends IInterface {
  void onComplete(int paramInt, byte[] paramArrayOfbyte) throws RemoteException;
  
  class Default implements IPrepareDownloadCallback {
    public void onComplete(int param1Int, byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPrepareDownloadCallback {
    private static final String DESCRIPTOR = "com.android.internal.telephony.euicc.IPrepareDownloadCallback";
    
    static final int TRANSACTION_onComplete = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.euicc.IPrepareDownloadCallback");
    }
    
    public static IPrepareDownloadCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.euicc.IPrepareDownloadCallback");
      if (iInterface != null && iInterface instanceof IPrepareDownloadCallback)
        return (IPrepareDownloadCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onComplete";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.telephony.euicc.IPrepareDownloadCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.telephony.euicc.IPrepareDownloadCallback");
      param1Int1 = param1Parcel1.readInt();
      byte[] arrayOfByte = param1Parcel1.createByteArray();
      onComplete(param1Int1, arrayOfByte);
      return true;
    }
    
    private static class Proxy implements IPrepareDownloadCallback {
      public static IPrepareDownloadCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.euicc.IPrepareDownloadCallback";
      }
      
      public void onComplete(int param2Int, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IPrepareDownloadCallback");
          parcel.writeInt(param2Int);
          parcel.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IPrepareDownloadCallback.Stub.getDefaultImpl() != null) {
            IPrepareDownloadCallback.Stub.getDefaultImpl().onComplete(param2Int, param2ArrayOfbyte);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPrepareDownloadCallback param1IPrepareDownloadCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPrepareDownloadCallback != null) {
          Proxy.sDefaultImpl = param1IPrepareDownloadCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPrepareDownloadCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
