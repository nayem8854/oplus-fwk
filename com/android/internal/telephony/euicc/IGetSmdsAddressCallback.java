package com.android.internal.telephony.euicc;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IGetSmdsAddressCallback extends IInterface {
  void onComplete(int paramInt, String paramString) throws RemoteException;
  
  class Default implements IGetSmdsAddressCallback {
    public void onComplete(int param1Int, String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IGetSmdsAddressCallback {
    private static final String DESCRIPTOR = "com.android.internal.telephony.euicc.IGetSmdsAddressCallback";
    
    static final int TRANSACTION_onComplete = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.euicc.IGetSmdsAddressCallback");
    }
    
    public static IGetSmdsAddressCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.euicc.IGetSmdsAddressCallback");
      if (iInterface != null && iInterface instanceof IGetSmdsAddressCallback)
        return (IGetSmdsAddressCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onComplete";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.telephony.euicc.IGetSmdsAddressCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.telephony.euicc.IGetSmdsAddressCallback");
      param1Int1 = param1Parcel1.readInt();
      String str = param1Parcel1.readString();
      onComplete(param1Int1, str);
      return true;
    }
    
    private static class Proxy implements IGetSmdsAddressCallback {
      public static IGetSmdsAddressCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.euicc.IGetSmdsAddressCallback";
      }
      
      public void onComplete(int param2Int, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IGetSmdsAddressCallback");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IGetSmdsAddressCallback.Stub.getDefaultImpl() != null) {
            IGetSmdsAddressCallback.Stub.getDefaultImpl().onComplete(param2Int, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IGetSmdsAddressCallback param1IGetSmdsAddressCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IGetSmdsAddressCallback != null) {
          Proxy.sDefaultImpl = param1IGetSmdsAddressCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IGetSmdsAddressCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
