package com.android.internal.telephony.euicc;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.service.euicc.EuiccProfileInfo;

public interface ISwitchToProfileCallback extends IInterface {
  void onComplete(int paramInt, EuiccProfileInfo paramEuiccProfileInfo) throws RemoteException;
  
  class Default implements ISwitchToProfileCallback {
    public void onComplete(int param1Int, EuiccProfileInfo param1EuiccProfileInfo) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISwitchToProfileCallback {
    private static final String DESCRIPTOR = "com.android.internal.telephony.euicc.ISwitchToProfileCallback";
    
    static final int TRANSACTION_onComplete = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.euicc.ISwitchToProfileCallback");
    }
    
    public static ISwitchToProfileCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.euicc.ISwitchToProfileCallback");
      if (iInterface != null && iInterface instanceof ISwitchToProfileCallback)
        return (ISwitchToProfileCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onComplete";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.telephony.euicc.ISwitchToProfileCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.telephony.euicc.ISwitchToProfileCallback");
      param1Int1 = param1Parcel1.readInt();
      if (param1Parcel1.readInt() != 0) {
        EuiccProfileInfo euiccProfileInfo = (EuiccProfileInfo)EuiccProfileInfo.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onComplete(param1Int1, (EuiccProfileInfo)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements ISwitchToProfileCallback {
      public static ISwitchToProfileCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.euicc.ISwitchToProfileCallback";
      }
      
      public void onComplete(int param2Int, EuiccProfileInfo param2EuiccProfileInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.ISwitchToProfileCallback");
          parcel.writeInt(param2Int);
          if (param2EuiccProfileInfo != null) {
            parcel.writeInt(1);
            param2EuiccProfileInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && ISwitchToProfileCallback.Stub.getDefaultImpl() != null) {
            ISwitchToProfileCallback.Stub.getDefaultImpl().onComplete(param2Int, param2EuiccProfileInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISwitchToProfileCallback param1ISwitchToProfileCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISwitchToProfileCallback != null) {
          Proxy.sDefaultImpl = param1ISwitchToProfileCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISwitchToProfileCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
