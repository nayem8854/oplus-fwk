package com.android.internal.telephony.euicc;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ISetDefaultSmdpAddressCallback extends IInterface {
  void onComplete(int paramInt) throws RemoteException;
  
  class Default implements ISetDefaultSmdpAddressCallback {
    public void onComplete(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISetDefaultSmdpAddressCallback {
    private static final String DESCRIPTOR = "com.android.internal.telephony.euicc.ISetDefaultSmdpAddressCallback";
    
    static final int TRANSACTION_onComplete = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.euicc.ISetDefaultSmdpAddressCallback");
    }
    
    public static ISetDefaultSmdpAddressCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.euicc.ISetDefaultSmdpAddressCallback");
      if (iInterface != null && iInterface instanceof ISetDefaultSmdpAddressCallback)
        return (ISetDefaultSmdpAddressCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onComplete";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.telephony.euicc.ISetDefaultSmdpAddressCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.telephony.euicc.ISetDefaultSmdpAddressCallback");
      param1Int1 = param1Parcel1.readInt();
      onComplete(param1Int1);
      return true;
    }
    
    private static class Proxy implements ISetDefaultSmdpAddressCallback {
      public static ISetDefaultSmdpAddressCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.euicc.ISetDefaultSmdpAddressCallback";
      }
      
      public void onComplete(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.ISetDefaultSmdpAddressCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && ISetDefaultSmdpAddressCallback.Stub.getDefaultImpl() != null) {
            ISetDefaultSmdpAddressCallback.Stub.getDefaultImpl().onComplete(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISetDefaultSmdpAddressCallback param1ISetDefaultSmdpAddressCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISetDefaultSmdpAddressCallback != null) {
          Proxy.sDefaultImpl = param1ISetDefaultSmdpAddressCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISetDefaultSmdpAddressCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
