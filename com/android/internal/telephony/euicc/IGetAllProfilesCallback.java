package com.android.internal.telephony.euicc;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.service.euicc.EuiccProfileInfo;

public interface IGetAllProfilesCallback extends IInterface {
  void onComplete(int paramInt, EuiccProfileInfo[] paramArrayOfEuiccProfileInfo) throws RemoteException;
  
  class Default implements IGetAllProfilesCallback {
    public void onComplete(int param1Int, EuiccProfileInfo[] param1ArrayOfEuiccProfileInfo) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IGetAllProfilesCallback {
    private static final String DESCRIPTOR = "com.android.internal.telephony.euicc.IGetAllProfilesCallback";
    
    static final int TRANSACTION_onComplete = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.euicc.IGetAllProfilesCallback");
    }
    
    public static IGetAllProfilesCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.euicc.IGetAllProfilesCallback");
      if (iInterface != null && iInterface instanceof IGetAllProfilesCallback)
        return (IGetAllProfilesCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onComplete";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.telephony.euicc.IGetAllProfilesCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.telephony.euicc.IGetAllProfilesCallback");
      param1Int1 = param1Parcel1.readInt();
      EuiccProfileInfo[] arrayOfEuiccProfileInfo = (EuiccProfileInfo[])param1Parcel1.createTypedArray(EuiccProfileInfo.CREATOR);
      onComplete(param1Int1, arrayOfEuiccProfileInfo);
      return true;
    }
    
    private static class Proxy implements IGetAllProfilesCallback {
      public static IGetAllProfilesCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.euicc.IGetAllProfilesCallback";
      }
      
      public void onComplete(int param2Int, EuiccProfileInfo[] param2ArrayOfEuiccProfileInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IGetAllProfilesCallback");
          parcel.writeInt(param2Int);
          parcel.writeTypedArray((Parcelable[])param2ArrayOfEuiccProfileInfo, 0);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IGetAllProfilesCallback.Stub.getDefaultImpl() != null) {
            IGetAllProfilesCallback.Stub.getDefaultImpl().onComplete(param2Int, param2ArrayOfEuiccProfileInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IGetAllProfilesCallback param1IGetAllProfilesCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IGetAllProfilesCallback != null) {
          Proxy.sDefaultImpl = param1IGetAllProfilesCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IGetAllProfilesCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
