package com.android.internal.telephony.euicc;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IAuthenticateServerCallback extends IInterface {
  void onComplete(int paramInt, byte[] paramArrayOfbyte) throws RemoteException;
  
  class Default implements IAuthenticateServerCallback {
    public void onComplete(int param1Int, byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAuthenticateServerCallback {
    private static final String DESCRIPTOR = "com.android.internal.telephony.euicc.IAuthenticateServerCallback";
    
    static final int TRANSACTION_onComplete = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.euicc.IAuthenticateServerCallback");
    }
    
    public static IAuthenticateServerCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.euicc.IAuthenticateServerCallback");
      if (iInterface != null && iInterface instanceof IAuthenticateServerCallback)
        return (IAuthenticateServerCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onComplete";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.telephony.euicc.IAuthenticateServerCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.telephony.euicc.IAuthenticateServerCallback");
      param1Int1 = param1Parcel1.readInt();
      byte[] arrayOfByte = param1Parcel1.createByteArray();
      onComplete(param1Int1, arrayOfByte);
      return true;
    }
    
    private static class Proxy implements IAuthenticateServerCallback {
      public static IAuthenticateServerCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.euicc.IAuthenticateServerCallback";
      }
      
      public void onComplete(int param2Int, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.euicc.IAuthenticateServerCallback");
          parcel.writeInt(param2Int);
          parcel.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IAuthenticateServerCallback.Stub.getDefaultImpl() != null) {
            IAuthenticateServerCallback.Stub.getDefaultImpl().onComplete(param2Int, param2ArrayOfbyte);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAuthenticateServerCallback param1IAuthenticateServerCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAuthenticateServerCallback != null) {
          Proxy.sDefaultImpl = param1IAuthenticateServerCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAuthenticateServerCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
