package com.android.internal.telephony.gsm;

import android.content.res.Resources;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import com.android.internal.telephony.EncodeException;
import com.android.internal.telephony.GsmAlphabet;
import com.android.internal.telephony.Sms7BitEncodingTranslator;
import com.android.internal.telephony.SmsAddress;
import com.android.internal.telephony.SmsConstants;
import com.android.internal.telephony.SmsHeader;
import com.android.internal.telephony.SmsMessageBase;
import com.android.internal.telephony.uicc.IccUtils;
import com.android.telephony.Rlog;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class SmsMessage extends SmsMessageBase {
  private boolean mReplyPathPresent = false;
  
  private boolean mIsStatusReportMessage = false;
  
  private int mVoiceMailCount = 0;
  
  private static final int INVALID_VALIDITY_PERIOD = -1;
  
  static final String LOG_TAG = "SmsMessage";
  
  private static final int VALIDITY_PERIOD_FORMAT_ABSOLUTE = 3;
  
  private static final int VALIDITY_PERIOD_FORMAT_ENHANCED = 1;
  
  private static final int VALIDITY_PERIOD_FORMAT_NONE = 0;
  
  private static final int VALIDITY_PERIOD_FORMAT_RELATIVE = 2;
  
  private static final int VALIDITY_PERIOD_MAX = 635040;
  
  private static final int VALIDITY_PERIOD_MIN = 5;
  
  private static final boolean VDBG = false;
  
  private int mDataCodingScheme;
  
  private int mEncodingType;
  
  private int mMti;
  
  private int mProtocolIdentifier;
  
  private int mStatus;
  
  private SmsConstants.MessageClass messageClass;
  
  public static class SubmitPdu extends SmsMessageBase.SubmitPduBase {}
  
  public static SmsMessage createFromPdu(byte[] paramArrayOfbyte) {
    try {
      SmsMessage smsMessage = new SmsMessage();
      this();
      smsMessage.parsePdu(paramArrayOfbyte);
      return smsMessage;
    } catch (RuntimeException runtimeException) {
      Rlog.e("SmsMessage", "SMS PDU parsing failed: ", runtimeException);
      return null;
    } catch (OutOfMemoryError outOfMemoryError) {
      Rlog.e("SmsMessage", "SMS PDU parsing failed with out of memory: ", outOfMemoryError);
      return null;
    } 
  }
  
  public boolean isTypeZero() {
    boolean bool;
    if (this.mProtocolIdentifier == 64) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static SmsMessage newFromCMT(byte[] paramArrayOfbyte) {
    try {
      SmsMessage smsMessage = new SmsMessage();
      this();
      smsMessage.parsePdu(paramArrayOfbyte);
      return smsMessage;
    } catch (RuntimeException runtimeException) {
      Rlog.e("SmsMessage", "SMS PDU parsing failed: ", runtimeException);
      return null;
    } 
  }
  
  public static SmsMessage newFromCDS(byte[] paramArrayOfbyte) {
    try {
      SmsMessage smsMessage = new SmsMessage();
      this();
      smsMessage.parsePdu(paramArrayOfbyte);
      return smsMessage;
    } catch (RuntimeException runtimeException) {
      Rlog.e("SmsMessage", "CDS SMS PDU parsing failed: ", runtimeException);
      return null;
    } 
  }
  
  public static SmsMessage createFromEfRecord(int paramInt, byte[] paramArrayOfbyte) {
    try {
      SmsMessage smsMessage = new SmsMessage();
      this();
      smsMessage.mIndexOnIcc = paramInt;
      if ((paramArrayOfbyte[0] & 0x1) == 0) {
        Rlog.w("SmsMessage", "SMS parsing failed: Trying to parse a free record");
        return null;
      } 
      smsMessage.mStatusOnIcc = paramArrayOfbyte[0] & 0x7;
      paramInt = paramArrayOfbyte.length - 1;
      byte[] arrayOfByte = new byte[paramInt];
      System.arraycopy(paramArrayOfbyte, 1, arrayOfByte, 0, paramInt);
      smsMessage.parsePdu(arrayOfByte);
      return smsMessage;
    } catch (RuntimeException runtimeException) {
      Rlog.e("SmsMessage", "SMS PDU parsing failed: ", runtimeException);
      return null;
    } 
  }
  
  public static int getTPLayerLengthForPDU(String paramString) {
    int i = paramString.length() / 2;
    int j = Integer.parseInt(paramString.substring(0, 2), 16);
    return i - j - 1;
  }
  
  public static int getRelativeValidityPeriod(int paramInt) {
    int i = -1;
    if (paramInt < 5 || paramInt > 635040) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid Validity Period");
      stringBuilder.append(paramInt);
      Rlog.e("SmsMessage", stringBuilder.toString());
      return -1;
    } 
    if (paramInt <= 720) {
      i = paramInt / 5 - 1;
    } else if (paramInt <= 1440) {
      i = (paramInt - 720) / 30 + 143;
    } else if (paramInt <= 43200) {
      i = paramInt / 1440 + 166;
    } else if (paramInt <= 635040) {
      i = paramInt / 10080 + 192;
    } 
    return i;
  }
  
  public static SubmitPdu getSubmitPdu(String paramString1, String paramString2, String paramString3, boolean paramBoolean, byte[] paramArrayOfbyte) {
    return getSubmitPdu(paramString1, paramString2, paramString3, paramBoolean, paramArrayOfbyte, 0, 0, 0);
  }
  
  public static SubmitPdu getSubmitPdu(String paramString1, String paramString2, String paramString3, boolean paramBoolean, byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3) {
    return getSubmitPdu(paramString1, paramString2, paramString3, paramBoolean, paramArrayOfbyte, paramInt1, paramInt2, paramInt3, -1);
  }
  
  public static SubmitPdu getSubmitPdu(String paramString1, String paramString2, String paramString3, boolean paramBoolean, byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    // Byte code:
    //   0: aload_2
    //   1: ifnull -> 608
    //   4: aload_1
    //   5: ifnonnull -> 11
    //   8: goto -> 608
    //   11: iload #5
    //   13: ifne -> 252
    //   16: aload_2
    //   17: iconst_0
    //   18: invokestatic calculateLength : (Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    //   21: astore #9
    //   23: aload #9
    //   25: getfield codeUnitSize : I
    //   28: istore #5
    //   30: aload #9
    //   32: getfield languageTable : I
    //   35: istore #7
    //   37: aload #9
    //   39: getfield languageShiftTable : I
    //   42: istore #6
    //   44: iload #5
    //   46: iconst_1
    //   47: if_icmpne -> 249
    //   50: iload #7
    //   52: ifne -> 60
    //   55: iload #6
    //   57: ifeq -> 249
    //   60: aload #4
    //   62: ifnull -> 216
    //   65: aload #4
    //   67: invokestatic fromByteArray : ([B)Lcom/android/internal/telephony/SmsHeader;
    //   70: astore #9
    //   72: aload #9
    //   74: getfield languageTable : I
    //   77: iload #7
    //   79: if_icmpne -> 98
    //   82: aload #9
    //   84: getfield languageShiftTable : I
    //   87: iload #6
    //   89: if_icmpeq -> 95
    //   92: goto -> 98
    //   95: goto -> 213
    //   98: new java/lang/StringBuilder
    //   101: dup
    //   102: invokespecial <init> : ()V
    //   105: astore #4
    //   107: aload #4
    //   109: ldc_w 'Updating language table in SMS header: '
    //   112: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   115: pop
    //   116: aload #4
    //   118: aload #9
    //   120: getfield languageTable : I
    //   123: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   126: pop
    //   127: aload #4
    //   129: ldc_w ' -> '
    //   132: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   135: pop
    //   136: aload #4
    //   138: iload #7
    //   140: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   143: pop
    //   144: aload #4
    //   146: ldc_w ', '
    //   149: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   152: pop
    //   153: aload #4
    //   155: aload #9
    //   157: getfield languageShiftTable : I
    //   160: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   163: pop
    //   164: aload #4
    //   166: ldc_w ' -> '
    //   169: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   172: pop
    //   173: aload #4
    //   175: iload #6
    //   177: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   180: pop
    //   181: ldc 'SmsMessage'
    //   183: aload #4
    //   185: invokevirtual toString : ()Ljava/lang/String;
    //   188: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   191: pop
    //   192: aload #9
    //   194: iload #7
    //   196: putfield languageTable : I
    //   199: aload #9
    //   201: iload #6
    //   203: putfield languageShiftTable : I
    //   206: aload #9
    //   208: invokestatic toByteArray : (Lcom/android/internal/telephony/SmsHeader;)[B
    //   211: astore #4
    //   213: goto -> 264
    //   216: new com/android/internal/telephony/SmsHeader
    //   219: dup
    //   220: invokespecial <init> : ()V
    //   223: astore #4
    //   225: aload #4
    //   227: iload #7
    //   229: putfield languageTable : I
    //   232: aload #4
    //   234: iload #6
    //   236: putfield languageShiftTable : I
    //   239: aload #4
    //   241: invokestatic toByteArray : (Lcom/android/internal/telephony/SmsHeader;)[B
    //   244: astore #4
    //   246: goto -> 264
    //   249: goto -> 264
    //   252: iload #6
    //   254: istore #10
    //   256: iload #7
    //   258: istore #6
    //   260: iload #10
    //   262: istore #7
    //   264: new com/android/internal/telephony/gsm/SmsMessage$SubmitPdu
    //   267: dup
    //   268: invokespecial <init> : ()V
    //   271: astore #9
    //   273: iload #8
    //   275: invokestatic getRelativeValidityPeriod : (I)I
    //   278: istore #11
    //   280: iload #11
    //   282: iflt -> 291
    //   285: iconst_2
    //   286: istore #8
    //   288: goto -> 294
    //   291: iconst_0
    //   292: istore #8
    //   294: aload #4
    //   296: ifnull -> 306
    //   299: bipush #64
    //   301: istore #10
    //   303: goto -> 309
    //   306: iconst_0
    //   307: istore #10
    //   309: iload #8
    //   311: iconst_3
    //   312: ishl
    //   313: iconst_1
    //   314: ior
    //   315: iload #10
    //   317: ior
    //   318: i2b
    //   319: istore #12
    //   321: aload_0
    //   322: aload_1
    //   323: iload #12
    //   325: iload_3
    //   326: aload #9
    //   328: invokestatic getSubmitPduHead : (Ljava/lang/String;Ljava/lang/String;BZLcom/android/internal/telephony/gsm/SmsMessage$SubmitPdu;)Ljava/io/ByteArrayOutputStream;
    //   331: astore_1
    //   332: aload_1
    //   333: ifnonnull -> 339
    //   336: aload #9
    //   338: areturn
    //   339: iload #5
    //   341: iconst_1
    //   342: if_icmpne -> 359
    //   345: aload_2
    //   346: aload #4
    //   348: iload #7
    //   350: iload #6
    //   352: invokestatic stringToGsm7BitPackedWithHeader : (Ljava/lang/String;[BII)[B
    //   355: astore_0
    //   356: goto -> 366
    //   359: aload_2
    //   360: aload #4
    //   362: invokestatic encodeUCS2 : (Ljava/lang/String;[B)[B
    //   365: astore_0
    //   366: goto -> 414
    //   369: astore_0
    //   370: goto -> 385
    //   373: astore_0
    //   374: ldc 'SmsMessage'
    //   376: ldc 'Implausible UnsupportedEncodingException '
    //   378: aload_0
    //   379: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   382: pop
    //   383: aconst_null
    //   384: areturn
    //   385: aload_0
    //   386: invokevirtual getError : ()I
    //   389: iconst_1
    //   390: if_icmpne -> 404
    //   393: ldc 'SmsMessage'
    //   395: ldc 'Exceed size limitation EncodeException'
    //   397: aload_0
    //   398: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   401: pop
    //   402: aconst_null
    //   403: areturn
    //   404: aload_2
    //   405: aload #4
    //   407: invokestatic encodeUCS2 : (Ljava/lang/String;[B)[B
    //   410: astore_0
    //   411: iconst_3
    //   412: istore #5
    //   414: iload #5
    //   416: iconst_1
    //   417: if_icmpne -> 487
    //   420: aload_0
    //   421: iconst_0
    //   422: baload
    //   423: sipush #255
    //   426: iand
    //   427: sipush #160
    //   430: if_icmple -> 479
    //   433: new java/lang/StringBuilder
    //   436: dup
    //   437: invokespecial <init> : ()V
    //   440: astore_1
    //   441: aload_1
    //   442: ldc 'Message too long ('
    //   444: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   447: pop
    //   448: aload_1
    //   449: aload_0
    //   450: iconst_0
    //   451: baload
    //   452: sipush #255
    //   455: iand
    //   456: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   459: pop
    //   460: aload_1
    //   461: ldc ' septets)'
    //   463: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   466: pop
    //   467: ldc 'SmsMessage'
    //   469: aload_1
    //   470: invokevirtual toString : ()Ljava/lang/String;
    //   473: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   476: pop
    //   477: aconst_null
    //   478: areturn
    //   479: aload_1
    //   480: iconst_0
    //   481: invokevirtual write : (I)V
    //   484: goto -> 552
    //   487: aload_0
    //   488: iconst_0
    //   489: baload
    //   490: sipush #255
    //   493: iand
    //   494: sipush #140
    //   497: if_icmple -> 546
    //   500: new java/lang/StringBuilder
    //   503: dup
    //   504: invokespecial <init> : ()V
    //   507: astore_1
    //   508: aload_1
    //   509: ldc 'Message too long ('
    //   511: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   514: pop
    //   515: aload_1
    //   516: aload_0
    //   517: iconst_0
    //   518: baload
    //   519: sipush #255
    //   522: iand
    //   523: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   526: pop
    //   527: aload_1
    //   528: ldc ' bytes)'
    //   530: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   533: pop
    //   534: ldc 'SmsMessage'
    //   536: aload_1
    //   537: invokevirtual toString : ()Ljava/lang/String;
    //   540: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   543: pop
    //   544: aconst_null
    //   545: areturn
    //   546: aload_1
    //   547: bipush #8
    //   549: invokevirtual write : (I)V
    //   552: iload #8
    //   554: iconst_2
    //   555: if_icmpne -> 564
    //   558: aload_1
    //   559: iload #11
    //   561: invokevirtual write : (I)V
    //   564: aload_1
    //   565: aload_0
    //   566: iconst_0
    //   567: aload_0
    //   568: arraylength
    //   569: invokevirtual write : ([BII)V
    //   572: aload #9
    //   574: aload_1
    //   575: invokevirtual toByteArray : ()[B
    //   578: putfield encodedMessage : [B
    //   581: aload #9
    //   583: areturn
    //   584: astore_0
    //   585: ldc 'SmsMessage'
    //   587: ldc 'Implausible UnsupportedEncodingException '
    //   589: aload_0
    //   590: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   593: pop
    //   594: aconst_null
    //   595: areturn
    //   596: astore_0
    //   597: ldc 'SmsMessage'
    //   599: ldc 'Exceed size limitation EncodeException'
    //   601: aload_0
    //   602: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   605: pop
    //   606: aconst_null
    //   607: areturn
    //   608: aconst_null
    //   609: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #333	-> 0
    //   #337	-> 11
    //   #339	-> 16
    //   #340	-> 23
    //   #341	-> 30
    //   #342	-> 37
    //   #344	-> 44
    //   #346	-> 60
    //   #347	-> 65
    //   #348	-> 72
    //   #350	-> 98
    //   #353	-> 192
    //   #354	-> 199
    //   #355	-> 206
    //   #357	-> 213
    //   #358	-> 216
    //   #359	-> 225
    //   #360	-> 232
    //   #361	-> 239
    //   #366	-> 249
    //   #337	-> 252
    //   #366	-> 264
    //   #368	-> 273
    //   #369	-> 273
    //   #373	-> 273
    //   #374	-> 285
    //   #373	-> 291
    //   #377	-> 294
    //   #378	-> 294
    //   #380	-> 321
    //   #386	-> 332
    //   #391	-> 339
    //   #392	-> 345
    //   #396	-> 359
    //   #402	-> 366
    //   #422	-> 366
    //   #404	-> 369
    //   #397	-> 373
    //   #398	-> 374
    //   #401	-> 383
    //   #404	-> 385
    //   #405	-> 385
    //   #406	-> 393
    //   #407	-> 402
    //   #412	-> 404
    //   #413	-> 411
    //   #420	-> 414
    //   #424	-> 414
    //   #425	-> 420
    //   #427	-> 433
    //   #428	-> 477
    //   #438	-> 479
    //   #440	-> 487
    //   #442	-> 500
    //   #443	-> 544
    //   #447	-> 546
    //   #450	-> 552
    //   #452	-> 558
    //   #455	-> 564
    //   #456	-> 572
    //   #457	-> 581
    //   #417	-> 584
    //   #418	-> 585
    //   #419	-> 594
    //   #414	-> 596
    //   #415	-> 597
    //   #416	-> 606
    //   #333	-> 608
    //   #334	-> 608
    // Exception table:
    //   from	to	target	type
    //   345	356	369	com/android/internal/telephony/EncodeException
    //   359	366	373	java/io/UnsupportedEncodingException
    //   359	366	369	com/android/internal/telephony/EncodeException
    //   374	383	369	com/android/internal/telephony/EncodeException
    //   404	411	596	com/android/internal/telephony/EncodeException
    //   404	411	584	java/io/UnsupportedEncodingException
  }
  
  private static byte[] encodeUCS2(String paramString, byte[] paramArrayOfbyte) throws UnsupportedEncodingException, EncodeException {
    byte[] arrayOfByte1, arrayOfByte2 = paramString.getBytes("utf-16be");
    if (paramArrayOfbyte != null) {
      arrayOfByte1 = new byte[paramArrayOfbyte.length + arrayOfByte2.length + 1];
      arrayOfByte1[0] = (byte)paramArrayOfbyte.length;
      System.arraycopy(paramArrayOfbyte, 0, arrayOfByte1, 1, paramArrayOfbyte.length);
      System.arraycopy(arrayOfByte2, 0, arrayOfByte1, paramArrayOfbyte.length + 1, arrayOfByte2.length);
    } else {
      arrayOfByte1 = arrayOfByte2;
    } 
    if (arrayOfByte1.length <= 255) {
      paramArrayOfbyte = new byte[arrayOfByte1.length + 1];
      paramArrayOfbyte[0] = (byte)(0xFF & arrayOfByte1.length);
      System.arraycopy(arrayOfByte1, 0, paramArrayOfbyte, 1, arrayOfByte1.length);
      return paramArrayOfbyte;
    } 
    throw new EncodeException("Payload cannot exceed 255 bytes", 1);
  }
  
  public static SubmitPdu getSubmitPdu(String paramString1, String paramString2, String paramString3, boolean paramBoolean) {
    return getSubmitPdu(paramString1, paramString2, paramString3, paramBoolean, (byte[])null);
  }
  
  public static SubmitPdu getSubmitPdu(String paramString1, String paramString2, String paramString3, boolean paramBoolean, int paramInt) {
    return getSubmitPdu(paramString1, paramString2, paramString3, paramBoolean, (byte[])null, 0, 0, 0, paramInt);
  }
  
  public static SubmitPdu getSubmitPdu(String paramString1, String paramString2, int paramInt, byte[] paramArrayOfbyte, boolean paramBoolean) {
    StringBuilder stringBuilder;
    SubmitPdu submitPdu1 = OplusSmsMessage.oemGetSubmitPdu(paramString1, paramString2, paramInt, paramArrayOfbyte, paramBoolean);
    if (submitPdu1 != null)
      return submitPdu1; 
    SmsHeader.PortAddrs portAddrs = new SmsHeader.PortAddrs();
    portAddrs.destPort = paramInt;
    portAddrs.origPort = 0;
    portAddrs.areEightBits = false;
    SmsHeader smsHeader = new SmsHeader();
    smsHeader.portAddrs = portAddrs;
    byte[] arrayOfByte = SmsHeader.toByteArray(smsHeader);
    if (paramArrayOfbyte.length + arrayOfByte.length + 1 > 140) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("SMS data message may only contain ");
      stringBuilder.append(140 - arrayOfByte.length - 1);
      stringBuilder.append(" bytes");
      Rlog.e("SmsMessage", stringBuilder.toString());
      return null;
    } 
    SubmitPdu submitPdu2 = new SubmitPdu();
    ByteArrayOutputStream byteArrayOutputStream = getSubmitPduHead((String)stringBuilder, paramString2, (byte)65, paramBoolean, submitPdu2);
    if (byteArrayOutputStream == null)
      return submitPdu2; 
    byteArrayOutputStream.write(4);
    byteArrayOutputStream.write(paramArrayOfbyte.length + arrayOfByte.length + 1);
    byteArrayOutputStream.write(arrayOfByte.length);
    byteArrayOutputStream.write(arrayOfByte, 0, arrayOfByte.length);
    byteArrayOutputStream.write(paramArrayOfbyte, 0, paramArrayOfbyte.length);
    submitPdu2.encodedMessage = byteArrayOutputStream.toByteArray();
    return submitPdu2;
  }
  
  private static ByteArrayOutputStream getSubmitPduHead(String paramString1, String paramString2, byte paramByte, boolean paramBoolean, SubmitPdu paramSubmitPdu) {
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(180);
    if (paramString1 == null) {
      paramSubmitPdu.encodedScAddress = null;
    } else {
      paramSubmitPdu.encodedScAddress = PhoneNumberUtils.networkPortionToCalledPartyBCDWithLength(paramString1);
    } 
    int i = paramByte;
    if (paramBoolean)
      i = (byte)(paramByte | 0x20); 
    byteArrayOutputStream.write(i);
    byteArrayOutputStream.write(0);
    byte[] arrayOfByte = PhoneNumberUtils.networkPortionToCalledPartyBCD(paramString2);
    if (arrayOfByte == null)
      return null; 
    i = arrayOfByte.length;
    paramByte = 1;
    if ((arrayOfByte[arrayOfByte.length - 1] & 0xF0) != 240)
      paramByte = 0; 
    byteArrayOutputStream.write((i - 1) * 2 - paramByte);
    byteArrayOutputStream.write(arrayOfByte, 0, arrayOfByte.length);
    byteArrayOutputStream.write(0);
    return byteArrayOutputStream;
  }
  
  public static SubmitPdu getDeliverPdu(String paramString1, String paramString2, String paramString3, long paramLong) {
    // Byte code:
    //   0: aload_1
    //   1: ifnull -> 793
    //   4: aload_2
    //   5: ifnonnull -> 11
    //   8: goto -> 793
    //   11: aload_2
    //   12: iconst_0
    //   13: invokestatic calculateLength : (Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    //   16: astore #5
    //   18: aload #5
    //   20: getfield codeUnitSize : I
    //   23: istore #6
    //   25: aload #5
    //   27: getfield languageTable : I
    //   30: istore #7
    //   32: aload #5
    //   34: getfield languageShiftTable : I
    //   37: istore #8
    //   39: iload #6
    //   41: iconst_1
    //   42: if_icmpne -> 88
    //   45: iload #7
    //   47: ifne -> 55
    //   50: iload #8
    //   52: ifeq -> 88
    //   55: new com/android/internal/telephony/SmsHeader
    //   58: dup
    //   59: invokespecial <init> : ()V
    //   62: astore #5
    //   64: aload #5
    //   66: iload #7
    //   68: putfield languageTable : I
    //   71: aload #5
    //   73: iload #8
    //   75: putfield languageShiftTable : I
    //   78: aload #5
    //   80: invokestatic toByteArray : (Lcom/android/internal/telephony/SmsHeader;)[B
    //   83: astore #5
    //   85: goto -> 91
    //   88: aconst_null
    //   89: astore #5
    //   91: new com/android/internal/telephony/gsm/SmsMessage$SubmitPdu
    //   94: dup
    //   95: invokespecial <init> : ()V
    //   98: astore #9
    //   100: new java/io/ByteArrayOutputStream
    //   103: dup
    //   104: sipush #180
    //   107: invokespecial <init> : (I)V
    //   110: astore #10
    //   112: aload_0
    //   113: ifnonnull -> 125
    //   116: aload #9
    //   118: aconst_null
    //   119: putfield encodedScAddress : [B
    //   122: goto -> 134
    //   125: aload #9
    //   127: aload_0
    //   128: invokestatic networkPortionToCalledPartyBCDWithLength : (Ljava/lang/String;)[B
    //   131: putfield encodedScAddress : [B
    //   134: aload #10
    //   136: iconst_0
    //   137: invokevirtual write : (I)V
    //   140: aload_1
    //   141: invokestatic networkPortionToCalledPartyBCD : (Ljava/lang/String;)[B
    //   144: astore_0
    //   145: aload_0
    //   146: ifnonnull -> 151
    //   149: aconst_null
    //   150: areturn
    //   151: aload_0
    //   152: arraylength
    //   153: istore #11
    //   155: aload_0
    //   156: aload_0
    //   157: arraylength
    //   158: iconst_1
    //   159: isub
    //   160: baload
    //   161: sipush #240
    //   164: iand
    //   165: sipush #240
    //   168: if_icmpne -> 177
    //   171: iconst_1
    //   172: istore #12
    //   174: goto -> 180
    //   177: iconst_0
    //   178: istore #12
    //   180: aload #10
    //   182: iload #11
    //   184: iconst_1
    //   185: isub
    //   186: iconst_2
    //   187: imul
    //   188: iload #12
    //   190: isub
    //   191: invokevirtual write : (I)V
    //   194: aload #10
    //   196: aload_0
    //   197: iconst_0
    //   198: aload_0
    //   199: arraylength
    //   200: invokevirtual write : ([BII)V
    //   203: aload #10
    //   205: iconst_0
    //   206: invokevirtual write : (I)V
    //   209: iload #6
    //   211: iconst_1
    //   212: if_icmpne -> 229
    //   215: aload_2
    //   216: aload #5
    //   218: iload #7
    //   220: iload #8
    //   222: invokestatic stringToGsm7BitPackedWithHeader : (Ljava/lang/String;[BII)[B
    //   225: astore_0
    //   226: goto -> 236
    //   229: aload_2
    //   230: aload #5
    //   232: invokestatic encodeUCS2 : (Ljava/lang/String;[B)[B
    //   235: astore_0
    //   236: iload #6
    //   238: istore #12
    //   240: goto -> 288
    //   243: astore_0
    //   244: goto -> 259
    //   247: astore_0
    //   248: ldc 'SmsMessage'
    //   250: ldc 'Implausible UnsupportedEncodingException '
    //   252: aload_0
    //   253: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   256: pop
    //   257: aconst_null
    //   258: areturn
    //   259: aload_0
    //   260: invokevirtual getError : ()I
    //   263: iconst_1
    //   264: if_icmpne -> 278
    //   267: ldc 'SmsMessage'
    //   269: ldc 'Exceed size limitation EncodeException'
    //   271: aload_0
    //   272: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   275: pop
    //   276: aconst_null
    //   277: areturn
    //   278: aload_2
    //   279: aload #5
    //   281: invokestatic encodeUCS2 : (Ljava/lang/String;[B)[B
    //   284: astore_0
    //   285: iconst_3
    //   286: istore #12
    //   288: iload #12
    //   290: iconst_1
    //   291: if_icmpne -> 362
    //   294: aload_0
    //   295: iconst_0
    //   296: baload
    //   297: sipush #255
    //   300: iand
    //   301: sipush #160
    //   304: if_icmple -> 353
    //   307: new java/lang/StringBuilder
    //   310: dup
    //   311: invokespecial <init> : ()V
    //   314: astore_1
    //   315: aload_1
    //   316: ldc 'Message too long ('
    //   318: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   321: pop
    //   322: aload_1
    //   323: aload_0
    //   324: iconst_0
    //   325: baload
    //   326: sipush #255
    //   329: iand
    //   330: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   333: pop
    //   334: aload_1
    //   335: ldc ' septets)'
    //   337: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   340: pop
    //   341: ldc 'SmsMessage'
    //   343: aload_1
    //   344: invokevirtual toString : ()Ljava/lang/String;
    //   347: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   350: pop
    //   351: aconst_null
    //   352: areturn
    //   353: aload #10
    //   355: iconst_0
    //   356: invokevirtual write : (I)V
    //   359: goto -> 428
    //   362: aload_0
    //   363: iconst_0
    //   364: baload
    //   365: sipush #255
    //   368: iand
    //   369: sipush #140
    //   372: if_icmple -> 421
    //   375: new java/lang/StringBuilder
    //   378: dup
    //   379: invokespecial <init> : ()V
    //   382: astore_1
    //   383: aload_1
    //   384: ldc 'Message too long ('
    //   386: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   389: pop
    //   390: aload_1
    //   391: aload_0
    //   392: iconst_0
    //   393: baload
    //   394: sipush #255
    //   397: iand
    //   398: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   401: pop
    //   402: aload_1
    //   403: ldc ' bytes)'
    //   405: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   408: pop
    //   409: ldc 'SmsMessage'
    //   411: aload_1
    //   412: invokevirtual toString : ()Ljava/lang/String;
    //   415: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   418: pop
    //   419: aconst_null
    //   420: areturn
    //   421: aload #10
    //   423: bipush #8
    //   425: invokevirtual write : (I)V
    //   428: bipush #7
    //   430: newarray byte
    //   432: astore_2
    //   433: lload_3
    //   434: invokestatic ofEpochMilli : (J)Ljava/time/Instant;
    //   437: invokestatic systemDefault : ()Ljava/time/ZoneId;
    //   440: invokevirtual atZone : (Ljava/time/ZoneId;)Ljava/time/ZonedDateTime;
    //   443: astore_1
    //   444: aload_1
    //   445: invokevirtual toLocalDateTime : ()Ljava/time/LocalDateTime;
    //   448: astore #5
    //   450: aload_1
    //   451: invokevirtual getOffset : ()Ljava/time/ZoneOffset;
    //   454: invokevirtual getTotalSeconds : ()I
    //   457: bipush #60
    //   459: idiv
    //   460: bipush #15
    //   462: idiv
    //   463: istore #11
    //   465: iload #11
    //   467: ifge -> 476
    //   470: iconst_1
    //   471: istore #12
    //   473: goto -> 479
    //   476: iconst_0
    //   477: istore #12
    //   479: iload #11
    //   481: istore #6
    //   483: iload #12
    //   485: ifeq -> 493
    //   488: iload #11
    //   490: ineg
    //   491: istore #6
    //   493: aload #5
    //   495: invokevirtual getYear : ()I
    //   498: istore #11
    //   500: aload #5
    //   502: invokevirtual getMonthValue : ()I
    //   505: istore #8
    //   507: aload #5
    //   509: invokevirtual getDayOfMonth : ()I
    //   512: istore #13
    //   514: aload #5
    //   516: invokevirtual getHour : ()I
    //   519: istore #7
    //   521: aload #5
    //   523: invokevirtual getMinute : ()I
    //   526: istore #14
    //   528: aload #5
    //   530: invokevirtual getSecond : ()I
    //   533: istore #15
    //   535: iload #11
    //   537: sipush #2000
    //   540: if_icmple -> 552
    //   543: wide iinc #11 -2000
    //   549: goto -> 558
    //   552: wide iinc #11 -1900
    //   558: aload_2
    //   559: iconst_0
    //   560: iload #11
    //   562: bipush #10
    //   564: irem
    //   565: bipush #15
    //   567: iand
    //   568: iconst_4
    //   569: ishl
    //   570: iload #11
    //   572: bipush #10
    //   574: idiv
    //   575: bipush #15
    //   577: iand
    //   578: ior
    //   579: i2b
    //   580: bastore
    //   581: aload_2
    //   582: iconst_1
    //   583: iload #8
    //   585: bipush #10
    //   587: irem
    //   588: bipush #15
    //   590: iand
    //   591: iconst_4
    //   592: ishl
    //   593: iload #8
    //   595: bipush #10
    //   597: idiv
    //   598: bipush #15
    //   600: iand
    //   601: ior
    //   602: i2b
    //   603: bastore
    //   604: aload_2
    //   605: iconst_2
    //   606: iload #13
    //   608: bipush #10
    //   610: irem
    //   611: bipush #15
    //   613: iand
    //   614: iconst_4
    //   615: ishl
    //   616: iload #13
    //   618: bipush #10
    //   620: idiv
    //   621: bipush #15
    //   623: iand
    //   624: ior
    //   625: i2b
    //   626: bastore
    //   627: aload_2
    //   628: iconst_3
    //   629: iload #7
    //   631: bipush #10
    //   633: irem
    //   634: bipush #15
    //   636: iand
    //   637: iconst_4
    //   638: ishl
    //   639: iload #7
    //   641: bipush #10
    //   643: idiv
    //   644: bipush #15
    //   646: iand
    //   647: ior
    //   648: i2b
    //   649: bastore
    //   650: aload_2
    //   651: iconst_4
    //   652: iload #14
    //   654: bipush #10
    //   656: irem
    //   657: bipush #15
    //   659: iand
    //   660: iconst_4
    //   661: ishl
    //   662: iload #14
    //   664: bipush #10
    //   666: idiv
    //   667: bipush #15
    //   669: iand
    //   670: ior
    //   671: i2b
    //   672: bastore
    //   673: aload_2
    //   674: iconst_5
    //   675: iload #15
    //   677: bipush #10
    //   679: irem
    //   680: bipush #15
    //   682: iand
    //   683: iconst_4
    //   684: ishl
    //   685: iload #15
    //   687: bipush #10
    //   689: idiv
    //   690: bipush #15
    //   692: iand
    //   693: ior
    //   694: i2b
    //   695: bastore
    //   696: aload_2
    //   697: bipush #6
    //   699: iload #6
    //   701: bipush #10
    //   703: irem
    //   704: bipush #15
    //   706: iand
    //   707: iconst_4
    //   708: ishl
    //   709: iload #6
    //   711: bipush #10
    //   713: idiv
    //   714: bipush #15
    //   716: iand
    //   717: ior
    //   718: i2b
    //   719: bastore
    //   720: iload #12
    //   722: ifeq -> 738
    //   725: aload_2
    //   726: iconst_0
    //   727: aload_2
    //   728: iconst_0
    //   729: baload
    //   730: bipush #8
    //   732: ior
    //   733: i2b
    //   734: bastore
    //   735: goto -> 738
    //   738: aload #10
    //   740: aload_2
    //   741: iconst_0
    //   742: aload_2
    //   743: arraylength
    //   744: invokevirtual write : ([BII)V
    //   747: aload #10
    //   749: aload_0
    //   750: iconst_0
    //   751: aload_0
    //   752: arraylength
    //   753: invokevirtual write : ([BII)V
    //   756: aload #9
    //   758: aload #10
    //   760: invokevirtual toByteArray : ()[B
    //   763: putfield encodedMessage : [B
    //   766: aload #9
    //   768: areturn
    //   769: astore_0
    //   770: ldc 'SmsMessage'
    //   772: ldc 'Implausible UnsupportedEncodingException '
    //   774: aload_0
    //   775: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   778: pop
    //   779: aconst_null
    //   780: areturn
    //   781: astore_0
    //   782: ldc 'SmsMessage'
    //   784: ldc 'Exceed size limitation EncodeException'
    //   786: aload_0
    //   787: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   790: pop
    //   791: aconst_null
    //   792: areturn
    //   793: aconst_null
    //   794: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #669	-> 0
    //   #674	-> 11
    //   #675	-> 18
    //   #676	-> 25
    //   #677	-> 32
    //   #678	-> 39
    //   #680	-> 39
    //   #681	-> 55
    //   #682	-> 64
    //   #683	-> 71
    //   #684	-> 78
    //   #687	-> 88
    //   #689	-> 100
    //   #692	-> 112
    //   #693	-> 116
    //   #695	-> 125
    //   #696	-> 125
    //   #700	-> 134
    //   #704	-> 140
    //   #707	-> 145
    //   #711	-> 151
    //   #714	-> 194
    //   #717	-> 203
    //   #722	-> 209
    //   #723	-> 215
    //   #727	-> 229
    //   #731	-> 236
    //   #751	-> 236
    //   #733	-> 243
    //   #728	-> 247
    //   #729	-> 248
    //   #730	-> 257
    //   #733	-> 259
    //   #734	-> 259
    //   #735	-> 267
    //   #736	-> 276
    //   #741	-> 278
    //   #742	-> 285
    //   #749	-> 288
    //   #753	-> 288
    //   #754	-> 294
    //   #756	-> 307
    //   #757	-> 351
    //   #761	-> 353
    //   #763	-> 362
    //   #765	-> 375
    //   #766	-> 419
    //   #770	-> 421
    //   #774	-> 428
    //   #776	-> 433
    //   #777	-> 444
    //   #781	-> 450
    //   #782	-> 465
    //   #783	-> 479
    //   #784	-> 488
    //   #786	-> 493
    //   #787	-> 500
    //   #788	-> 507
    //   #789	-> 514
    //   #790	-> 521
    //   #791	-> 528
    //   #793	-> 535
    //   #794	-> 558
    //   #795	-> 581
    //   #796	-> 604
    //   #797	-> 627
    //   #798	-> 650
    //   #799	-> 673
    //   #800	-> 696
    //   #801	-> 720
    //   #802	-> 725
    //   #801	-> 738
    //   #804	-> 738
    //   #806	-> 747
    //   #807	-> 756
    //   #808	-> 766
    //   #746	-> 769
    //   #747	-> 770
    //   #748	-> 779
    //   #743	-> 781
    //   #744	-> 782
    //   #745	-> 791
    //   #670	-> 793
    // Exception table:
    //   from	to	target	type
    //   215	226	243	com/android/internal/telephony/EncodeException
    //   229	236	247	java/io/UnsupportedEncodingException
    //   229	236	243	com/android/internal/telephony/EncodeException
    //   248	257	243	com/android/internal/telephony/EncodeException
    //   278	285	781	com/android/internal/telephony/EncodeException
    //   278	285	769	java/io/UnsupportedEncodingException
  }
  
  class PduParser {
    int mCur;
    
    byte[] mPdu;
    
    byte[] mUserData;
    
    SmsHeader mUserDataHeader;
    
    int mUserDataSeptetPadding;
    
    PduParser(SmsMessage this$0) {
      this.mPdu = (byte[])this$0;
      this.mCur = 0;
      this.mUserDataSeptetPadding = 0;
    }
    
    String getSCAddress() {
      int i = getByte();
      if (i == 0) {
        runtimeException = null;
      } else {
        try {
          String str = PhoneNumberUtils.calledPartyBCDToString(this.mPdu, this.mCur, i, 2);
        } catch (RuntimeException runtimeException) {
          Rlog.d("SmsMessage", "invalid SC address: ", runtimeException);
          runtimeException = null;
        } 
      } 
      this.mCur += i;
      return (String)runtimeException;
    }
    
    int getByte() {
      byte[] arrayOfByte = this.mPdu;
      int i = this.mCur;
      this.mCur = i + 1;
      return arrayOfByte[i] & 0xFF;
    }
    
    GsmSmsAddress getAddress() {
      byte[] arrayOfByte = this.mPdu;
      int i = this.mCur;
      byte b = arrayOfByte[i];
      int j = ((b & 0xFF) + 1) / 2 + 2;
      try {
        GsmSmsAddress gsmSmsAddress = new GsmSmsAddress(arrayOfByte, i, j);
        this.mCur += j;
        return gsmSmsAddress;
      } catch (ParseException parseException) {
        throw new RuntimeException(parseException.getMessage());
      } 
    }
    
    long getSCTimestampMillis() {
      byte[] arrayOfByte = this.mPdu;
      int i = this.mCur;
      this.mCur = i + 1;
      int j = IccUtils.gsmBcdByteToInt(arrayOfByte[i]);
      arrayOfByte = this.mPdu;
      i = this.mCur;
      this.mCur = i + 1;
      int k = IccUtils.gsmBcdByteToInt(arrayOfByte[i]);
      arrayOfByte = this.mPdu;
      i = this.mCur;
      this.mCur = i + 1;
      int m = IccUtils.gsmBcdByteToInt(arrayOfByte[i]);
      arrayOfByte = this.mPdu;
      i = this.mCur;
      this.mCur = i + 1;
      int n = IccUtils.gsmBcdByteToInt(arrayOfByte[i]);
      arrayOfByte = this.mPdu;
      i = this.mCur;
      this.mCur = i + 1;
      int i1 = IccUtils.gsmBcdByteToInt(arrayOfByte[i]);
      arrayOfByte = this.mPdu;
      i = this.mCur;
      this.mCur = i + 1;
      int i2 = IccUtils.gsmBcdByteToInt(arrayOfByte[i]);
      arrayOfByte = this.mPdu;
      i = this.mCur;
      this.mCur = i + 1;
      byte b = arrayOfByte[i];
      i = IccUtils.gsmBcdByteToInt((byte)(b & 0xFFFFFFF7));
      if ((b & 0x8) != 0)
        i = -i; 
      if (j >= 90) {
        j += 1900;
      } else {
        j += 2000;
      } 
      LocalDateTime localDateTime = LocalDateTime.of(j, k, m, n, i1, i2);
      long l1 = localDateTime.toEpochSecond(ZoneOffset.UTC), l2 = (i * 15 * 60);
      return 1000L * (l1 - l2);
    }
    
    int constructUserData(boolean param1Boolean1, boolean param1Boolean2) {
      int i = this.mCur;
      byte[] arrayOfByte1 = this.mPdu;
      int j = i + 1, k = arrayOfByte1[i] & 0xFF;
      int m = 0;
      i = 0;
      boolean bool = false;
      int n = j;
      if (param1Boolean1) {
        int i1 = j + 1;
        m = arrayOfByte1[j] & 0xFF;
        byte[] arrayOfByte = new byte[m];
        System.arraycopy(arrayOfByte1, i1, arrayOfByte, 0, m);
        this.mUserDataHeader = SmsHeader.fromByteArray(arrayOfByte);
        j = (m + 1) * 8;
        n = j / 7;
        if (j % 7 > 0) {
          i = 1;
        } else {
          i = 0;
        } 
        n += i;
        this.mUserDataSeptetPadding = n * 7 - j;
        j = i1 + m;
        i = m;
        m = n;
        n = j;
      } 
      if (param1Boolean2) {
        i = this.mPdu.length - n;
      } else {
        if (param1Boolean1) {
          i++;
        } else {
          i = 0;
        } 
        j = k - i;
        i = j;
        if (j < 0)
          i = 0; 
      } 
      byte[] arrayOfByte2 = new byte[i];
      System.arraycopy(this.mPdu, n, arrayOfByte2, 0, arrayOfByte2.length);
      this.mCur = n;
      if (param1Boolean2) {
        i = k - m;
        if (i < 0)
          i = bool; 
        return i;
      } 
      return this.mUserData.length;
    }
    
    byte[] getUserData() {
      return this.mUserData;
    }
    
    SmsHeader getUserDataHeader() {
      return this.mUserDataHeader;
    }
    
    String getUserDataGSM7Bit(int param1Int1, int param1Int2, int param1Int3) {
      String str = GsmAlphabet.gsm7BitPackedToString(this.mPdu, this.mCur, param1Int1, this.mUserDataSeptetPadding, param1Int2, param1Int3);
      this.mCur += param1Int1 * 7 / 8;
      return str;
    }
    
    String getUserDataGSM8bit(int param1Int) {
      String str = GsmAlphabet.gsm8BitUnpackedToString(this.mPdu, this.mCur, param1Int);
      this.mCur += param1Int;
      return str;
    }
    
    String getUserDataOem8bit(int param1Int) {
      String str = OplusSmsMessage.getUserDataOem8bit(this.mUserData, this.mPdu, this.mCur, param1Int);
      if (str != null) {
        this.mCur += param1Int;
        return str;
      } 
      return null;
    }
    
    String getUserDataUCS2(int param1Int) {
      String str;
      try {
        str = new String();
        this(this.mPdu, this.mCur, param1Int, "utf-16");
      } catch (UnsupportedEncodingException unsupportedEncodingException) {
        Rlog.e("SmsMessage", "implausible UnsupportedEncodingException", unsupportedEncodingException);
        str = "";
      } 
      this.mCur += param1Int;
      return str;
    }
    
    String getUserDataKSC5601(int param1Int) {
      String str;
      try {
        str = new String();
        this(this.mPdu, this.mCur, param1Int, "KSC5601");
      } catch (UnsupportedEncodingException unsupportedEncodingException) {
        Rlog.e("SmsMessage", "implausible UnsupportedEncodingException", unsupportedEncodingException);
        str = "";
      } 
      this.mCur += param1Int;
      return str;
    }
    
    boolean moreDataPresent() {
      boolean bool;
      if (this.mPdu.length > this.mCur) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
  }
  
  public static GsmAlphabet.TextEncodingDetails calculateLength(CharSequence paramCharSequence, boolean paramBoolean) {
    String str = null;
    Resources resources = Resources.getSystem();
    if (resources.getBoolean(17891537))
      str = Sms7BitEncodingTranslator.translate(paramCharSequence, false); 
    CharSequence charSequence = str;
    if (TextUtils.isEmpty(str))
      charSequence = paramCharSequence; 
    GsmAlphabet.TextEncodingDetails textEncodingDetails = GsmAlphabet.countGsmSeptets(charSequence, paramBoolean);
    if (textEncodingDetails == null)
      return SmsMessageBase.calcUnicodeEncodingDetails(charSequence); 
    return textEncodingDetails;
  }
  
  public static GsmAlphabet.TextEncodingDetails calculateLengthOem(CharSequence paramCharSequence, boolean paramBoolean, int paramInt) {
    return OplusSmsMessage.calculateLengthOem(paramCharSequence, paramBoolean, paramInt);
  }
  
  public int getProtocolIdentifier() {
    return this.mProtocolIdentifier;
  }
  
  int getDataCodingScheme() {
    return this.mDataCodingScheme;
  }
  
  public boolean isReplace() {
    boolean bool;
    int i = this.mProtocolIdentifier;
    if ((i & 0xC0) == 64 && (i & 0x3F) > 0 && (i & 0x3F) < 8) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isCphsMwiMessage() {
    if (!((GsmSmsAddress)this.mOriginatingAddress).isCphsVoiceMessageClear()) {
      GsmSmsAddress gsmSmsAddress = (GsmSmsAddress)this.mOriginatingAddress;
      return 
        gsmSmsAddress.isCphsVoiceMessageSet();
    } 
    return true;
  }
  
  public boolean isMWIClearMessage() {
    boolean bool = this.mIsMwi;
    null = true;
    if (bool && !this.mMwiSense)
      return true; 
    if (this.mOriginatingAddress != null) {
      GsmSmsAddress gsmSmsAddress = (GsmSmsAddress)this.mOriginatingAddress;
      if (gsmSmsAddress.isCphsVoiceMessageClear())
        return null; 
    } 
    return false;
  }
  
  public boolean isMWISetMessage() {
    boolean bool = this.mIsMwi;
    null = true;
    if (bool && this.mMwiSense)
      return true; 
    if (this.mOriginatingAddress != null) {
      GsmSmsAddress gsmSmsAddress = (GsmSmsAddress)this.mOriginatingAddress;
      if (gsmSmsAddress.isCphsVoiceMessageSet())
        return null; 
    } 
    return false;
  }
  
  public boolean isMwiDontStore() {
    if (this.mIsMwi && this.mMwiDontStore)
      return true; 
    if (isCphsMwiMessage())
      if (" ".equals(getMessageBody()))
        return true;  
    return false;
  }
  
  public int getStatus() {
    return this.mStatus;
  }
  
  public boolean isStatusReportMessage() {
    return this.mIsStatusReportMessage;
  }
  
  public boolean isReplyPathPresent() {
    return this.mReplyPathPresent;
  }
  
  private void parsePdu(byte[] paramArrayOfbyte) {
    this.mPdu = paramArrayOfbyte;
    PduParser pduParser = new PduParser(paramArrayOfbyte);
    this.mScAddress = pduParser.getSCAddress();
    String str = this.mScAddress;
    int i = pduParser.getByte();
    int j = i & 0x3;
    if (j != 0)
      if (j != 1) {
        if (j != 2) {
          if (j != 3)
            throw new RuntimeException("Unsupported message type"); 
        } else {
          parseSmsStatusReport(pduParser, i);
          return;
        } 
      } else {
        parseSmsSubmit(pduParser, i);
        return;
      }  
    parseSmsDeliver(pduParser, i);
  }
  
  private void parseSmsStatusReport(PduParser paramPduParser, int paramInt) {
    boolean bool = true;
    this.mIsStatusReportMessage = true;
    this.mMessageRef = paramPduParser.getByte();
    this.mRecipientAddress = paramPduParser.getAddress();
    this.mScTimeMillis = paramPduParser.getSCTimestampMillis();
    paramPduParser.getSCTimestampMillis();
    this.mStatus = paramPduParser.getByte();
    if (paramPduParser.moreDataPresent()) {
      int i = paramPduParser.getByte();
      int j = i;
      while ((j & 0x80) != 0)
        j = paramPduParser.getByte(); 
      if ((i & 0x78) == 0) {
        if ((i & 0x1) != 0)
          this.mProtocolIdentifier = paramPduParser.getByte(); 
        if ((i & 0x2) != 0)
          this.mDataCodingScheme = paramPduParser.getByte(); 
        if ((i & 0x4) != 0) {
          if ((paramInt & 0x40) != 64)
            bool = false; 
          parseUserData(paramPduParser, bool);
        } 
      } 
    } 
  }
  
  private void parseSmsDeliver(PduParser paramPduParser, int paramInt) {
    boolean bool2, bool1 = true;
    if ((paramInt & 0x80) == 128) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mReplyPathPresent = bool2;
    this.mOriginatingAddress = paramPduParser.getAddress();
    SmsAddress smsAddress = this.mOriginatingAddress;
    this.mProtocolIdentifier = paramPduParser.getByte();
    this.mDataCodingScheme = paramPduParser.getByte();
    this.mScTimeMillis = paramPduParser.getSCTimestampMillis();
    if ((paramInt & 0x40) == 64) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    parseUserData(paramPduParser, bool2);
  }
  
  private void parseSmsSubmit(PduParser paramPduParser, int paramInt) {
    boolean bool2, bool1 = true;
    if ((paramInt & 0x80) == 128) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mReplyPathPresent = bool2;
    this.mMessageRef = paramPduParser.getByte();
    this.mRecipientAddress = paramPduParser.getAddress();
    SmsAddress smsAddress = this.mRecipientAddress;
    this.mProtocolIdentifier = paramPduParser.getByte();
    this.mDataCodingScheme = paramPduParser.getByte();
    int i = paramInt >> 3 & 0x3;
    if (i == 0) {
      i = 0;
    } else if (2 == i) {
      i = 1;
    } else {
      i = 7;
    } 
    while (i > 0) {
      paramPduParser.getByte();
      i--;
    } 
    if ((paramInt & 0x40) == 64) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    parseUserData(paramPduParser, bool2);
  }
  
  private void parseUserData(PduParser paramPduParser, boolean paramBoolean) {
    // Byte code:
    //   0: iconst_0
    //   1: istore_3
    //   2: iconst_0
    //   3: istore #4
    //   5: iconst_0
    //   6: istore #5
    //   8: invokestatic getSystem : ()Landroid/content/res/Resources;
    //   11: astore #6
    //   13: aload_0
    //   14: getfield mDataCodingScheme : I
    //   17: istore #7
    //   19: iload #7
    //   21: sipush #128
    //   24: iand
    //   25: ifne -> 259
    //   28: iload #7
    //   30: bipush #32
    //   32: iand
    //   33: ifeq -> 42
    //   36: iconst_1
    //   37: istore #8
    //   39: goto -> 45
    //   42: iconst_0
    //   43: istore #8
    //   45: aload_0
    //   46: getfield mDataCodingScheme : I
    //   49: bipush #16
    //   51: iand
    //   52: ifeq -> 61
    //   55: iconst_1
    //   56: istore #7
    //   58: goto -> 64
    //   61: iconst_0
    //   62: istore #7
    //   64: iload #7
    //   66: istore_3
    //   67: iload #8
    //   69: ifeq -> 122
    //   72: new java/lang/StringBuilder
    //   75: dup
    //   76: invokespecial <init> : ()V
    //   79: astore #9
    //   81: aload #9
    //   83: ldc_w '4 - Unsupported SMS data coding scheme (compression) '
    //   86: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   89: pop
    //   90: aload #9
    //   92: aload_0
    //   93: getfield mDataCodingScheme : I
    //   96: sipush #255
    //   99: iand
    //   100: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   103: pop
    //   104: ldc 'SmsMessage'
    //   106: aload #9
    //   108: invokevirtual toString : ()Ljava/lang/String;
    //   111: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   114: pop
    //   115: iload #4
    //   117: istore #7
    //   119: goto -> 701
    //   122: aload_0
    //   123: getfield mDataCodingScheme : I
    //   126: iconst_2
    //   127: ishr
    //   128: iconst_3
    //   129: iand
    //   130: istore #7
    //   132: iload #7
    //   134: ifeq -> 253
    //   137: iload #7
    //   139: iconst_1
    //   140: if_icmpeq -> 168
    //   143: iload #7
    //   145: iconst_2
    //   146: if_icmpeq -> 162
    //   149: iload #7
    //   151: iconst_3
    //   152: if_icmpeq -> 197
    //   155: iload #5
    //   157: istore #7
    //   159: goto -> 256
    //   162: iconst_3
    //   163: istore #7
    //   165: goto -> 256
    //   168: invokestatic isEnable8BitMtSms : ()Z
    //   171: ifeq -> 180
    //   174: iconst_2
    //   175: istore #7
    //   177: goto -> 256
    //   180: aload #6
    //   182: ldc_w 17891536
    //   185: invokevirtual getBoolean : (I)Z
    //   188: ifeq -> 197
    //   191: iconst_2
    //   192: istore #7
    //   194: goto -> 256
    //   197: new java/lang/StringBuilder
    //   200: dup
    //   201: invokespecial <init> : ()V
    //   204: astore #9
    //   206: aload #9
    //   208: ldc_w '1 - Unsupported SMS data coding scheme '
    //   211: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   214: pop
    //   215: aload #9
    //   217: aload_0
    //   218: getfield mDataCodingScheme : I
    //   221: sipush #255
    //   224: iand
    //   225: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   228: pop
    //   229: ldc 'SmsMessage'
    //   231: aload #9
    //   233: invokevirtual toString : ()Ljava/lang/String;
    //   236: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   239: pop
    //   240: aload #6
    //   242: ldc_w 17694933
    //   245: invokevirtual getInteger : (I)I
    //   248: istore #7
    //   250: goto -> 256
    //   253: iconst_1
    //   254: istore #7
    //   256: goto -> 701
    //   259: iload #7
    //   261: sipush #240
    //   264: iand
    //   265: sipush #240
    //   268: if_icmpne -> 292
    //   271: iconst_1
    //   272: istore_3
    //   273: iload #7
    //   275: iconst_4
    //   276: iand
    //   277: ifne -> 286
    //   280: iconst_1
    //   281: istore #7
    //   283: goto -> 701
    //   286: iconst_2
    //   287: istore #7
    //   289: goto -> 701
    //   292: iload #7
    //   294: sipush #240
    //   297: iand
    //   298: sipush #192
    //   301: if_icmpeq -> 457
    //   304: iload #7
    //   306: sipush #240
    //   309: iand
    //   310: sipush #208
    //   313: if_icmpeq -> 457
    //   316: iload #7
    //   318: sipush #240
    //   321: iand
    //   322: sipush #224
    //   325: if_icmpne -> 331
    //   328: goto -> 457
    //   331: iload #7
    //   333: sipush #192
    //   336: iand
    //   337: sipush #128
    //   340: if_icmpne -> 407
    //   343: iload #7
    //   345: sipush #132
    //   348: if_icmpne -> 357
    //   351: iconst_4
    //   352: istore #7
    //   354: goto -> 701
    //   357: new java/lang/StringBuilder
    //   360: dup
    //   361: invokespecial <init> : ()V
    //   364: astore #9
    //   366: aload #9
    //   368: ldc_w '5 - Unsupported SMS data coding scheme '
    //   371: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   374: pop
    //   375: aload #9
    //   377: aload_0
    //   378: getfield mDataCodingScheme : I
    //   381: sipush #255
    //   384: iand
    //   385: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   388: pop
    //   389: ldc 'SmsMessage'
    //   391: aload #9
    //   393: invokevirtual toString : ()Ljava/lang/String;
    //   396: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   399: pop
    //   400: iload #4
    //   402: istore #7
    //   404: goto -> 701
    //   407: new java/lang/StringBuilder
    //   410: dup
    //   411: invokespecial <init> : ()V
    //   414: astore #9
    //   416: aload #9
    //   418: ldc_w '3 - Unsupported SMS data coding scheme '
    //   421: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   424: pop
    //   425: aload #9
    //   427: aload_0
    //   428: getfield mDataCodingScheme : I
    //   431: sipush #255
    //   434: iand
    //   435: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   438: pop
    //   439: ldc 'SmsMessage'
    //   441: aload #9
    //   443: invokevirtual toString : ()Ljava/lang/String;
    //   446: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   449: pop
    //   450: iload #4
    //   452: istore #7
    //   454: goto -> 701
    //   457: aload_0
    //   458: getfield mDataCodingScheme : I
    //   461: sipush #240
    //   464: iand
    //   465: sipush #224
    //   468: if_icmpne -> 477
    //   471: iconst_3
    //   472: istore #7
    //   474: goto -> 480
    //   477: iconst_1
    //   478: istore #7
    //   480: aload_0
    //   481: getfield mDataCodingScheme : I
    //   484: bipush #8
    //   486: iand
    //   487: bipush #8
    //   489: if_icmpne -> 498
    //   492: iconst_1
    //   493: istore #10
    //   495: goto -> 501
    //   498: iconst_0
    //   499: istore #10
    //   501: aload_0
    //   502: getfield mDataCodingScheme : I
    //   505: iconst_3
    //   506: iand
    //   507: ifne -> 653
    //   510: aload_0
    //   511: iconst_1
    //   512: putfield mIsMwi : Z
    //   515: aload_0
    //   516: iload #10
    //   518: putfield mMwiSense : Z
    //   521: aload_0
    //   522: getfield mDataCodingScheme : I
    //   525: sipush #240
    //   528: iand
    //   529: sipush #192
    //   532: if_icmpne -> 541
    //   535: iconst_1
    //   536: istore #11
    //   538: goto -> 544
    //   541: iconst_0
    //   542: istore #11
    //   544: aload_0
    //   545: iload #11
    //   547: putfield mMwiDontStore : Z
    //   550: iload #10
    //   552: iconst_1
    //   553: if_icmpne -> 564
    //   556: aload_0
    //   557: iconst_m1
    //   558: putfield mVoiceMailCount : I
    //   561: goto -> 569
    //   564: aload_0
    //   565: iconst_0
    //   566: putfield mVoiceMailCount : I
    //   569: new java/lang/StringBuilder
    //   572: dup
    //   573: invokespecial <init> : ()V
    //   576: astore #9
    //   578: aload #9
    //   580: ldc_w 'MWI in DCS for Vmail. DCS = '
    //   583: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   586: pop
    //   587: aload #9
    //   589: aload_0
    //   590: getfield mDataCodingScheme : I
    //   593: sipush #255
    //   596: iand
    //   597: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   600: pop
    //   601: aload #9
    //   603: ldc_w ' Dont store = '
    //   606: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   609: pop
    //   610: aload #9
    //   612: aload_0
    //   613: getfield mMwiDontStore : Z
    //   616: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   619: pop
    //   620: aload #9
    //   622: ldc_w ' vmail count = '
    //   625: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   628: pop
    //   629: aload #9
    //   631: aload_0
    //   632: getfield mVoiceMailCount : I
    //   635: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   638: pop
    //   639: ldc 'SmsMessage'
    //   641: aload #9
    //   643: invokevirtual toString : ()Ljava/lang/String;
    //   646: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   649: pop
    //   650: goto -> 701
    //   653: aload_0
    //   654: iconst_0
    //   655: putfield mIsMwi : Z
    //   658: new java/lang/StringBuilder
    //   661: dup
    //   662: invokespecial <init> : ()V
    //   665: astore #9
    //   667: aload #9
    //   669: ldc_w 'MWI in DCS for fax/email/other: '
    //   672: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   675: pop
    //   676: aload #9
    //   678: aload_0
    //   679: getfield mDataCodingScheme : I
    //   682: sipush #255
    //   685: iand
    //   686: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   689: pop
    //   690: ldc 'SmsMessage'
    //   692: aload #9
    //   694: invokevirtual toString : ()Ljava/lang/String;
    //   697: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   700: pop
    //   701: iload #7
    //   703: iconst_1
    //   704: if_icmpne -> 713
    //   707: iconst_1
    //   708: istore #10
    //   710: goto -> 716
    //   713: iconst_0
    //   714: istore #10
    //   716: aload_1
    //   717: iload_2
    //   718: iload #10
    //   720: invokevirtual constructUserData : (ZZ)I
    //   723: istore #5
    //   725: aload_0
    //   726: aload_1
    //   727: invokevirtual getUserData : ()[B
    //   730: putfield mUserData : [B
    //   733: aload_0
    //   734: aload_1
    //   735: invokevirtual getUserDataHeader : ()Lcom/android/internal/telephony/SmsHeader;
    //   738: putfield mUserDataHeader : Lcom/android/internal/telephony/SmsHeader;
    //   741: aload_0
    //   742: iload #7
    //   744: putfield mEncodingType : I
    //   747: iload_2
    //   748: ifeq -> 1056
    //   751: aload_0
    //   752: getfield mUserDataHeader : Lcom/android/internal/telephony/SmsHeader;
    //   755: getfield specialSmsMsgList : Ljava/util/ArrayList;
    //   758: invokevirtual size : ()I
    //   761: ifeq -> 1056
    //   764: aload_0
    //   765: getfield mUserDataHeader : Lcom/android/internal/telephony/SmsHeader;
    //   768: getfield specialSmsMsgList : Ljava/util/ArrayList;
    //   771: invokevirtual iterator : ()Ljava/util/Iterator;
    //   774: astore #9
    //   776: aload #9
    //   778: invokeinterface hasNext : ()Z
    //   783: ifeq -> 1056
    //   786: aload #9
    //   788: invokeinterface next : ()Ljava/lang/Object;
    //   793: checkcast com/android/internal/telephony/SmsHeader$SpecialSmsMsg
    //   796: astore #12
    //   798: aload #12
    //   800: getfield msgIndType : I
    //   803: sipush #255
    //   806: iand
    //   807: istore #8
    //   809: iload #8
    //   811: ifeq -> 865
    //   814: iload #8
    //   816: sipush #128
    //   819: if_icmpne -> 825
    //   822: goto -> 865
    //   825: new java/lang/StringBuilder
    //   828: dup
    //   829: invokespecial <init> : ()V
    //   832: astore #12
    //   834: aload #12
    //   836: ldc_w 'TP_UDH fax/email/extended msg/multisubscriber profile. Msg Ind = '
    //   839: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   842: pop
    //   843: aload #12
    //   845: iload #8
    //   847: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   850: pop
    //   851: ldc 'SmsMessage'
    //   853: aload #12
    //   855: invokevirtual toString : ()Ljava/lang/String;
    //   858: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   861: pop
    //   862: goto -> 1053
    //   865: aload_0
    //   866: iconst_1
    //   867: putfield mIsMwi : Z
    //   870: iload #8
    //   872: sipush #128
    //   875: if_icmpne -> 886
    //   878: aload_0
    //   879: iconst_0
    //   880: putfield mMwiDontStore : Z
    //   883: goto -> 943
    //   886: aload_0
    //   887: getfield mMwiDontStore : Z
    //   890: ifne -> 943
    //   893: aload_0
    //   894: getfield mDataCodingScheme : I
    //   897: istore #4
    //   899: iload #4
    //   901: sipush #240
    //   904: iand
    //   905: sipush #208
    //   908: if_icmpeq -> 926
    //   911: iload #4
    //   913: sipush #240
    //   916: iand
    //   917: sipush #224
    //   920: if_icmpne -> 935
    //   923: goto -> 926
    //   926: aload_0
    //   927: getfield mDataCodingScheme : I
    //   930: iconst_3
    //   931: iand
    //   932: ifeq -> 943
    //   935: aload_0
    //   936: iconst_1
    //   937: putfield mMwiDontStore : Z
    //   940: goto -> 943
    //   943: aload #12
    //   945: getfield msgCount : I
    //   948: sipush #255
    //   951: iand
    //   952: istore #4
    //   954: aload_0
    //   955: iload #4
    //   957: putfield mVoiceMailCount : I
    //   960: iload #4
    //   962: ifle -> 973
    //   965: aload_0
    //   966: iconst_1
    //   967: putfield mMwiSense : Z
    //   970: goto -> 978
    //   973: aload_0
    //   974: iconst_0
    //   975: putfield mMwiSense : Z
    //   978: new java/lang/StringBuilder
    //   981: dup
    //   982: invokespecial <init> : ()V
    //   985: astore #12
    //   987: aload #12
    //   989: ldc_w 'MWI in TP-UDH for Vmail. Msg Ind = '
    //   992: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   995: pop
    //   996: aload #12
    //   998: iload #8
    //   1000: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1003: pop
    //   1004: aload #12
    //   1006: ldc_w ' Dont store = '
    //   1009: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1012: pop
    //   1013: aload #12
    //   1015: aload_0
    //   1016: getfield mMwiDontStore : Z
    //   1019: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   1022: pop
    //   1023: aload #12
    //   1025: ldc_w ' Vmail count = '
    //   1028: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1031: pop
    //   1032: aload #12
    //   1034: aload_0
    //   1035: getfield mVoiceMailCount : I
    //   1038: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1041: pop
    //   1042: ldc 'SmsMessage'
    //   1044: aload #12
    //   1046: invokevirtual toString : ()Ljava/lang/String;
    //   1049: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   1052: pop
    //   1053: goto -> 776
    //   1056: iload #7
    //   1058: ifeq -> 1231
    //   1061: iload #7
    //   1063: iconst_1
    //   1064: if_icmpeq -> 1176
    //   1067: iload #7
    //   1069: iconst_2
    //   1070: if_icmpeq -> 1114
    //   1073: iload #7
    //   1075: iconst_3
    //   1076: if_icmpeq -> 1101
    //   1079: iload #7
    //   1081: iconst_4
    //   1082: if_icmpeq -> 1088
    //   1085: goto -> 1236
    //   1088: aload_0
    //   1089: aload_1
    //   1090: iload #5
    //   1092: invokevirtual getUserDataKSC5601 : (I)Ljava/lang/String;
    //   1095: putfield mMessageBody : Ljava/lang/String;
    //   1098: goto -> 1236
    //   1101: aload_0
    //   1102: aload_1
    //   1103: iload #5
    //   1105: invokevirtual getUserDataUCS2 : (I)Ljava/lang/String;
    //   1108: putfield mMessageBody : Ljava/lang/String;
    //   1111: goto -> 1236
    //   1114: invokestatic isEnable8BitMtSms : ()Z
    //   1117: ifeq -> 1144
    //   1120: aload_1
    //   1121: ifnull -> 1144
    //   1124: aload_0
    //   1125: aload_1
    //   1126: iload #5
    //   1128: invokevirtual getUserDataOem8bit : (I)Ljava/lang/String;
    //   1131: putfield mMessageBody : Ljava/lang/String;
    //   1134: aload_0
    //   1135: getfield mMessageBody : Ljava/lang/String;
    //   1138: ifnull -> 1144
    //   1141: goto -> 1236
    //   1144: aload #6
    //   1146: ldc_w 17891536
    //   1149: invokevirtual getBoolean : (I)Z
    //   1152: ifeq -> 1168
    //   1155: aload_0
    //   1156: aload_1
    //   1157: iload #5
    //   1159: invokevirtual getUserDataGSM8bit : (I)Ljava/lang/String;
    //   1162: putfield mMessageBody : Ljava/lang/String;
    //   1165: goto -> 1236
    //   1168: aload_0
    //   1169: aconst_null
    //   1170: putfield mMessageBody : Ljava/lang/String;
    //   1173: goto -> 1236
    //   1176: iload_2
    //   1177: ifeq -> 1192
    //   1180: aload_0
    //   1181: getfield mUserDataHeader : Lcom/android/internal/telephony/SmsHeader;
    //   1184: getfield languageTable : I
    //   1187: istore #7
    //   1189: goto -> 1195
    //   1192: iconst_0
    //   1193: istore #7
    //   1195: iload_2
    //   1196: ifeq -> 1211
    //   1199: aload_0
    //   1200: getfield mUserDataHeader : Lcom/android/internal/telephony/SmsHeader;
    //   1203: getfield languageShiftTable : I
    //   1206: istore #8
    //   1208: goto -> 1214
    //   1211: iconst_0
    //   1212: istore #8
    //   1214: aload_0
    //   1215: aload_1
    //   1216: iload #5
    //   1218: iload #7
    //   1220: iload #8
    //   1222: invokevirtual getUserDataGSM7Bit : (III)Ljava/lang/String;
    //   1225: putfield mMessageBody : Ljava/lang/String;
    //   1228: goto -> 1236
    //   1231: aload_0
    //   1232: aconst_null
    //   1233: putfield mMessageBody : Ljava/lang/String;
    //   1236: aload_0
    //   1237: getfield mMessageBody : Ljava/lang/String;
    //   1240: ifnull -> 1247
    //   1243: aload_0
    //   1244: invokevirtual parseMessageBody : ()V
    //   1247: iload_3
    //   1248: ifne -> 1261
    //   1251: aload_0
    //   1252: getstatic com/android/internal/telephony/SmsConstants$MessageClass.UNKNOWN : Lcom/android/internal/telephony/SmsConstants$MessageClass;
    //   1255: putfield messageClass : Lcom/android/internal/telephony/SmsConstants$MessageClass;
    //   1258: goto -> 1332
    //   1261: aload_0
    //   1262: getfield mDataCodingScheme : I
    //   1265: iconst_3
    //   1266: iand
    //   1267: istore #7
    //   1269: iload #7
    //   1271: ifeq -> 1325
    //   1274: iload #7
    //   1276: iconst_1
    //   1277: if_icmpeq -> 1315
    //   1280: iload #7
    //   1282: iconst_2
    //   1283: if_icmpeq -> 1305
    //   1286: iload #7
    //   1288: iconst_3
    //   1289: if_icmpeq -> 1295
    //   1292: goto -> 1332
    //   1295: aload_0
    //   1296: getstatic com/android/internal/telephony/SmsConstants$MessageClass.CLASS_3 : Lcom/android/internal/telephony/SmsConstants$MessageClass;
    //   1299: putfield messageClass : Lcom/android/internal/telephony/SmsConstants$MessageClass;
    //   1302: goto -> 1332
    //   1305: aload_0
    //   1306: getstatic com/android/internal/telephony/SmsConstants$MessageClass.CLASS_2 : Lcom/android/internal/telephony/SmsConstants$MessageClass;
    //   1309: putfield messageClass : Lcom/android/internal/telephony/SmsConstants$MessageClass;
    //   1312: goto -> 1332
    //   1315: aload_0
    //   1316: getstatic com/android/internal/telephony/SmsConstants$MessageClass.CLASS_1 : Lcom/android/internal/telephony/SmsConstants$MessageClass;
    //   1319: putfield messageClass : Lcom/android/internal/telephony/SmsConstants$MessageClass;
    //   1322: goto -> 1332
    //   1325: aload_0
    //   1326: getstatic com/android/internal/telephony/SmsConstants$MessageClass.CLASS_0 : Lcom/android/internal/telephony/SmsConstants$MessageClass;
    //   1329: putfield messageClass : Lcom/android/internal/telephony/SmsConstants$MessageClass;
    //   1332: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1433	-> 0
    //   #1434	-> 2
    //   #1436	-> 2
    //   #1438	-> 8
    //   #1440	-> 13
    //   #1441	-> 28
    //   #1442	-> 45
    //   #1444	-> 67
    //   #1445	-> 72
    //   #1448	-> 122
    //   #1454	-> 162
    //   #1455	-> 165
    //   #1460	-> 168
    //   #1461	-> 174
    //   #1462	-> 177
    //   #1468	-> 180
    //   #1470	-> 191
    //   #1471	-> 194
    //   #1475	-> 197
    //   #1477	-> 240
    //   #1450	-> 253
    //   #1451	-> 256
    //   #1479	-> 256
    //   #1482	-> 259
    //   #1483	-> 271
    //   #1484	-> 273
    //   #1486	-> 273
    //   #1488	-> 280
    //   #1491	-> 286
    //   #1493	-> 292
    //   #1534	-> 331
    //   #1537	-> 343
    //   #1539	-> 351
    //   #1541	-> 357
    //   #1545	-> 407
    //   #1502	-> 457
    //   #1503	-> 471
    //   #1505	-> 477
    //   #1508	-> 480
    //   #1509	-> 480
    //   #1513	-> 501
    //   #1514	-> 510
    //   #1515	-> 515
    //   #1516	-> 521
    //   #1519	-> 550
    //   #1520	-> 556
    //   #1522	-> 564
    //   #1525	-> 569
    //   #1530	-> 653
    //   #1531	-> 658
    //   #1534	-> 701
    //   #1550	-> 701
    //   #1552	-> 725
    //   #1553	-> 733
    //   #1557	-> 741
    //   #1568	-> 747
    //   #1569	-> 764
    //   #1570	-> 798
    //   #1577	-> 809
    //   #1623	-> 825
    //   #1578	-> 865
    //   #1579	-> 870
    //   #1581	-> 878
    //   #1582	-> 886
    //   #1590	-> 893
    //   #1596	-> 935
    //   #1582	-> 943
    //   #1600	-> 943
    //   #1608	-> 960
    //   #1609	-> 965
    //   #1611	-> 973
    //   #1613	-> 978
    //   #1626	-> 1053
    //   #1629	-> 1056
    //   #1664	-> 1088
    //   #1660	-> 1101
    //   #1661	-> 1111
    //   #1637	-> 1114
    //   #1638	-> 1124
    //   #1639	-> 1134
    //   #1645	-> 1144
    //   #1647	-> 1155
    //   #1649	-> 1168
    //   #1651	-> 1173
    //   #1654	-> 1176
    //   #1655	-> 1176
    //   #1656	-> 1195
    //   #1654	-> 1214
    //   #1657	-> 1228
    //   #1631	-> 1231
    //   #1632	-> 1236
    //   #1670	-> 1236
    //   #1671	-> 1243
    //   #1674	-> 1247
    //   #1675	-> 1251
    //   #1677	-> 1261
    //   #1688	-> 1295
    //   #1685	-> 1305
    //   #1686	-> 1312
    //   #1682	-> 1315
    //   #1683	-> 1322
    //   #1679	-> 1325
    //   #1680	-> 1332
    //   #1692	-> 1332
  }
  
  public SmsConstants.MessageClass getMessageClass() {
    return this.messageClass;
  }
  
  boolean isUsimDataDownload() {
    if (this.messageClass == SmsConstants.MessageClass.CLASS_2) {
      int i = this.mProtocolIdentifier;
      if (i == 127 || i == 124)
        return true; 
    } 
    return false;
  }
  
  public int getNumOfVoicemails() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mIsMwi : Z
    //   4: ifne -> 60
    //   7: aload_0
    //   8: invokevirtual isCphsMwiMessage : ()Z
    //   11: ifeq -> 60
    //   14: aload_0
    //   15: getfield mOriginatingAddress : Lcom/android/internal/telephony/SmsAddress;
    //   18: ifnull -> 46
    //   21: aload_0
    //   22: getfield mOriginatingAddress : Lcom/android/internal/telephony/SmsAddress;
    //   25: checkcast com/android/internal/telephony/gsm/GsmSmsAddress
    //   28: astore_1
    //   29: aload_1
    //   30: invokevirtual isCphsVoiceMessageSet : ()Z
    //   33: ifeq -> 46
    //   36: aload_0
    //   37: sipush #255
    //   40: putfield mVoiceMailCount : I
    //   43: goto -> 51
    //   46: aload_0
    //   47: iconst_0
    //   48: putfield mVoiceMailCount : I
    //   51: ldc 'SmsMessage'
    //   53: ldc_w 'CPHS voice mail message'
    //   56: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   59: pop
    //   60: aload_0
    //   61: getfield mVoiceMailCount : I
    //   64: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1724	-> 0
    //   #1725	-> 14
    //   #1726	-> 29
    //   #1727	-> 36
    //   #1729	-> 46
    //   #1731	-> 51
    //   #1733	-> 60
  }
  
  public SmsMessage() {
    this.mEncodingType = 0;
  }
  
  public int getEncodingType() {
    return this.mEncodingType;
  }
}
