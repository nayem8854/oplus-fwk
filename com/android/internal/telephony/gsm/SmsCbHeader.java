package com.android.internal.telephony.gsm;

import android.telephony.SmsCbCmasInfo;
import android.telephony.SmsCbEtwsInfo;
import java.util.Locale;

public class SmsCbHeader {
  public static final int FORMAT_ETWS_PRIMARY = 3;
  
  public static final int FORMAT_GSM = 1;
  
  public static final int FORMAT_UMTS = 2;
  
  private static final String[] LANGUAGE_CODES_GROUP_0;
  
  private static final String[] LANGUAGE_CODES_GROUP_2;
  
  private static final int MESSAGE_TYPE_CBS_MESSAGE = 1;
  
  public static final int PDU_HEADER_LENGTH = 6;
  
  private static final int PDU_LENGTH_ETWS = 56;
  
  private static final int PDU_LENGTH_GSM = 88;
  
  private final SmsCbCmasInfo mCmasInfo;
  
  private final int mDataCodingScheme;
  
  private DataCodingScheme mDataCodingSchemeStructedData;
  
  private final SmsCbEtwsInfo mEtwsInfo;
  
  private final int mFormat;
  
  private final int mGeographicalScope;
  
  private final int mMessageIdentifier;
  
  private final int mNrOfPages;
  
  private final int mPageIndex;
  
  private final int mSerialNumber;
  
  static {
    Locale locale2 = Locale.GERMAN;
    String str2 = locale2.getLanguage();
    Locale locale4 = Locale.ENGLISH;
    String str4 = locale4.getLanguage();
    Locale locale6 = Locale.ITALIAN;
    String str6 = locale6.getLanguage();
    Locale locale8 = Locale.FRENCH;
    String str8 = locale8.getLanguage();
    Locale locale10 = new Locale("es");
    String str9 = locale10.getLanguage();
    Locale locale11 = new Locale("nl");
    String str10 = locale11.getLanguage();
    Locale locale12 = new Locale("sv");
    String str11 = locale12.getLanguage();
    Locale locale13 = new Locale("da");
    String str12 = locale13.getLanguage();
    Locale locale14 = new Locale("pt");
    String str13 = locale14.getLanguage();
    Locale locale15 = new Locale("fi");
    String str14 = locale15.getLanguage();
    Locale locale16 = new Locale("nb");
    String str15 = locale16.getLanguage();
    Locale locale17 = new Locale("el");
    String str16 = locale17.getLanguage();
    Locale locale18 = new Locale("tr");
    String str17 = locale18.getLanguage();
    Locale locale19 = new Locale("hu");
    String str18 = locale19.getLanguage();
    locale19 = new Locale("pl");
    LANGUAGE_CODES_GROUP_0 = new String[] { 
        str2, str4, str6, str8, str9, str10, str11, str12, str13, str14, 
        str15, str16, str17, str18, locale19.getLanguage(), null };
    Locale locale1 = new Locale("cs");
    String str1 = locale1.getLanguage();
    Locale locale3 = new Locale("he");
    String str3 = locale3.getLanguage();
    Locale locale5 = new Locale("ar");
    String str5 = locale5.getLanguage();
    Locale locale7 = new Locale("ru");
    String str7 = locale7.getLanguage();
    Locale locale9 = new Locale("is");
    LANGUAGE_CODES_GROUP_2 = new String[] { 
        str1, str3, str5, str7, locale9.getLanguage(), null, null, null, null, null, 
        null, null, null, null, null, null };
  }
  
  public SmsCbHeader(byte[] paramArrayOfbyte) throws IllegalArgumentException {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial <init> : ()V
    //   4: aload_1
    //   5: ifnull -> 555
    //   8: aload_1
    //   9: arraylength
    //   10: bipush #6
    //   12: if_icmplt -> 555
    //   15: aload_1
    //   16: arraylength
    //   17: bipush #88
    //   19: if_icmpgt -> 282
    //   22: aload_0
    //   23: aload_1
    //   24: iconst_0
    //   25: baload
    //   26: sipush #192
    //   29: iand
    //   30: bipush #6
    //   32: iushr
    //   33: putfield mGeographicalScope : I
    //   36: aload_0
    //   37: aload_1
    //   38: iconst_0
    //   39: baload
    //   40: sipush #255
    //   43: iand
    //   44: bipush #8
    //   46: ishl
    //   47: aload_1
    //   48: iconst_1
    //   49: baload
    //   50: sipush #255
    //   53: iand
    //   54: ior
    //   55: putfield mSerialNumber : I
    //   58: aload_0
    //   59: aload_1
    //   60: iconst_2
    //   61: baload
    //   62: sipush #255
    //   65: iand
    //   66: bipush #8
    //   68: ishl
    //   69: aload_1
    //   70: iconst_3
    //   71: baload
    //   72: sipush #255
    //   75: iand
    //   76: ior
    //   77: putfield mMessageIdentifier : I
    //   80: aload_0
    //   81: invokespecial isEtwsMessage : ()Z
    //   84: ifeq -> 201
    //   87: aload_1
    //   88: arraylength
    //   89: bipush #56
    //   91: if_icmpgt -> 201
    //   94: aload_0
    //   95: iconst_3
    //   96: putfield mFormat : I
    //   99: aload_0
    //   100: iconst_m1
    //   101: putfield mDataCodingScheme : I
    //   104: aload_0
    //   105: iconst_m1
    //   106: putfield mPageIndex : I
    //   109: aload_0
    //   110: iconst_m1
    //   111: putfield mNrOfPages : I
    //   114: aload_1
    //   115: iconst_4
    //   116: baload
    //   117: iconst_1
    //   118: iand
    //   119: ifeq -> 127
    //   122: iconst_1
    //   123: istore_2
    //   124: goto -> 129
    //   127: iconst_0
    //   128: istore_2
    //   129: aload_1
    //   130: iconst_5
    //   131: baload
    //   132: sipush #128
    //   135: iand
    //   136: ifeq -> 144
    //   139: iconst_1
    //   140: istore_3
    //   141: goto -> 146
    //   144: iconst_0
    //   145: istore_3
    //   146: aload_1
    //   147: iconst_4
    //   148: baload
    //   149: istore #4
    //   151: aload_1
    //   152: arraylength
    //   153: bipush #6
    //   155: if_icmple -> 170
    //   158: aload_1
    //   159: bipush #6
    //   161: aload_1
    //   162: arraylength
    //   163: invokestatic copyOfRange : ([BII)[B
    //   166: astore_1
    //   167: goto -> 172
    //   170: aconst_null
    //   171: astore_1
    //   172: aload_0
    //   173: new android/telephony/SmsCbEtwsInfo
    //   176: dup
    //   177: iload #4
    //   179: sipush #254
    //   182: iand
    //   183: iconst_1
    //   184: iushr
    //   185: iload_2
    //   186: iload_3
    //   187: iconst_1
    //   188: aload_1
    //   189: invokespecial <init> : (IZZZ[B)V
    //   192: putfield mEtwsInfo : Landroid/telephony/SmsCbEtwsInfo;
    //   195: aload_0
    //   196: aconst_null
    //   197: putfield mCmasInfo : Landroid/telephony/SmsCbCmasInfo;
    //   200: return
    //   201: aload_0
    //   202: iconst_1
    //   203: putfield mFormat : I
    //   206: aload_0
    //   207: aload_1
    //   208: iconst_4
    //   209: baload
    //   210: sipush #255
    //   213: iand
    //   214: putfield mDataCodingScheme : I
    //   217: aload_1
    //   218: iconst_5
    //   219: baload
    //   220: sipush #240
    //   223: iand
    //   224: iconst_4
    //   225: iushr
    //   226: istore #5
    //   228: aload_1
    //   229: iconst_5
    //   230: baload
    //   231: bipush #15
    //   233: iand
    //   234: istore #6
    //   236: iload #5
    //   238: ifeq -> 261
    //   241: iload #6
    //   243: ifeq -> 261
    //   246: iload #5
    //   248: istore #7
    //   250: iload #6
    //   252: istore #4
    //   254: iload #5
    //   256: iload #6
    //   258: if_icmple -> 267
    //   261: iconst_1
    //   262: istore #7
    //   264: iconst_1
    //   265: istore #4
    //   267: aload_0
    //   268: iload #7
    //   270: putfield mPageIndex : I
    //   273: aload_0
    //   274: iload #4
    //   276: putfield mNrOfPages : I
    //   279: goto -> 377
    //   282: aload_0
    //   283: iconst_2
    //   284: putfield mFormat : I
    //   287: aload_1
    //   288: iconst_0
    //   289: baload
    //   290: istore #4
    //   292: iload #4
    //   294: iconst_1
    //   295: if_icmpne -> 521
    //   298: aload_0
    //   299: aload_1
    //   300: iconst_1
    //   301: baload
    //   302: sipush #255
    //   305: iand
    //   306: bipush #8
    //   308: ishl
    //   309: aload_1
    //   310: iconst_2
    //   311: baload
    //   312: sipush #255
    //   315: iand
    //   316: ior
    //   317: putfield mMessageIdentifier : I
    //   320: aload_0
    //   321: aload_1
    //   322: iconst_3
    //   323: baload
    //   324: sipush #192
    //   327: iand
    //   328: bipush #6
    //   330: iushr
    //   331: putfield mGeographicalScope : I
    //   334: aload_0
    //   335: aload_1
    //   336: iconst_3
    //   337: baload
    //   338: sipush #255
    //   341: iand
    //   342: bipush #8
    //   344: ishl
    //   345: aload_1
    //   346: iconst_4
    //   347: baload
    //   348: sipush #255
    //   351: iand
    //   352: ior
    //   353: putfield mSerialNumber : I
    //   356: aload_0
    //   357: aload_1
    //   358: iconst_5
    //   359: baload
    //   360: sipush #255
    //   363: iand
    //   364: putfield mDataCodingScheme : I
    //   367: aload_0
    //   368: iconst_1
    //   369: putfield mPageIndex : I
    //   372: aload_0
    //   373: iconst_1
    //   374: putfield mNrOfPages : I
    //   377: aload_0
    //   378: getfield mDataCodingScheme : I
    //   381: istore #4
    //   383: iload #4
    //   385: iconst_m1
    //   386: if_icmpeq -> 402
    //   389: aload_0
    //   390: new com/android/internal/telephony/gsm/SmsCbHeader$DataCodingScheme
    //   393: dup
    //   394: iload #4
    //   396: invokespecial <init> : (I)V
    //   399: putfield mDataCodingSchemeStructedData : Lcom/android/internal/telephony/gsm/SmsCbHeader$DataCodingScheme;
    //   402: aload_0
    //   403: invokespecial isEtwsMessage : ()Z
    //   406: ifeq -> 450
    //   409: aload_0
    //   410: invokespecial isEtwsEmergencyUserAlert : ()Z
    //   413: istore_2
    //   414: aload_0
    //   415: invokespecial isEtwsPopupAlert : ()Z
    //   418: istore_3
    //   419: aload_0
    //   420: invokespecial getEtwsWarningType : ()I
    //   423: istore #4
    //   425: aload_0
    //   426: new android/telephony/SmsCbEtwsInfo
    //   429: dup
    //   430: iload #4
    //   432: iload_2
    //   433: iload_3
    //   434: iconst_0
    //   435: aconst_null
    //   436: invokespecial <init> : (IZZZ[B)V
    //   439: putfield mEtwsInfo : Landroid/telephony/SmsCbEtwsInfo;
    //   442: aload_0
    //   443: aconst_null
    //   444: putfield mCmasInfo : Landroid/telephony/SmsCbCmasInfo;
    //   447: goto -> 520
    //   450: aload_0
    //   451: invokespecial isCmasMessage : ()Z
    //   454: ifeq -> 510
    //   457: aload_0
    //   458: invokespecial getCmasMessageClass : ()I
    //   461: istore #6
    //   463: aload_0
    //   464: invokespecial getCmasSeverity : ()I
    //   467: istore #4
    //   469: aload_0
    //   470: invokespecial getCmasUrgency : ()I
    //   473: istore #7
    //   475: aload_0
    //   476: invokespecial getCmasCertainty : ()I
    //   479: istore #5
    //   481: aload_0
    //   482: aconst_null
    //   483: putfield mEtwsInfo : Landroid/telephony/SmsCbEtwsInfo;
    //   486: aload_0
    //   487: new android/telephony/SmsCbCmasInfo
    //   490: dup
    //   491: iload #6
    //   493: iconst_m1
    //   494: iconst_m1
    //   495: iload #4
    //   497: iload #7
    //   499: iload #5
    //   501: invokespecial <init> : (IIIIII)V
    //   504: putfield mCmasInfo : Landroid/telephony/SmsCbCmasInfo;
    //   507: goto -> 520
    //   510: aload_0
    //   511: aconst_null
    //   512: putfield mEtwsInfo : Landroid/telephony/SmsCbEtwsInfo;
    //   515: aload_0
    //   516: aconst_null
    //   517: putfield mCmasInfo : Landroid/telephony/SmsCbCmasInfo;
    //   520: return
    //   521: new java/lang/StringBuilder
    //   524: dup
    //   525: invokespecial <init> : ()V
    //   528: astore_1
    //   529: aload_1
    //   530: ldc 'Unsupported message type '
    //   532: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   535: pop
    //   536: aload_1
    //   537: iload #4
    //   539: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   542: pop
    //   543: new java/lang/IllegalArgumentException
    //   546: dup
    //   547: aload_1
    //   548: invokevirtual toString : ()Ljava/lang/String;
    //   551: invokespecial <init> : (Ljava/lang/String;)V
    //   554: athrow
    //   555: new java/lang/IllegalArgumentException
    //   558: dup
    //   559: ldc 'Illegal PDU'
    //   561: invokespecial <init> : (Ljava/lang/String;)V
    //   564: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #134	-> 0
    //   #135	-> 4
    //   #139	-> 15
    //   #144	-> 22
    //   #145	-> 36
    //   #146	-> 58
    //   #147	-> 80
    //   #148	-> 94
    //   #149	-> 99
    //   #150	-> 104
    //   #151	-> 109
    //   #152	-> 114
    //   #153	-> 129
    //   #154	-> 146
    //   #157	-> 151
    //   #158	-> 158
    //   #160	-> 170
    //   #162	-> 172
    //   #164	-> 195
    //   #165	-> 200
    //   #168	-> 201
    //   #169	-> 206
    //   #172	-> 217
    //   #173	-> 228
    //   #175	-> 236
    //   #176	-> 261
    //   #177	-> 264
    //   #180	-> 267
    //   #181	-> 273
    //   #182	-> 279
    //   #186	-> 282
    //   #188	-> 287
    //   #190	-> 292
    //   #194	-> 298
    //   #195	-> 320
    //   #196	-> 334
    //   #197	-> 356
    //   #202	-> 367
    //   #203	-> 372
    //   #206	-> 377
    //   #207	-> 389
    //   #210	-> 402
    //   #211	-> 409
    //   #212	-> 414
    //   #213	-> 419
    //   #214	-> 425
    //   #216	-> 442
    //   #217	-> 447
    //   #218	-> 457
    //   #219	-> 463
    //   #220	-> 469
    //   #221	-> 475
    //   #222	-> 481
    //   #223	-> 486
    //   #225	-> 507
    //   #226	-> 510
    //   #227	-> 515
    //   #229	-> 520
    //   #191	-> 521
    //   #136	-> 555
  }
  
  public int getGeographicalScope() {
    return this.mGeographicalScope;
  }
  
  public int getSerialNumber() {
    return this.mSerialNumber;
  }
  
  public int getServiceCategory() {
    return this.mMessageIdentifier;
  }
  
  public int getDataCodingScheme() {
    return this.mDataCodingScheme;
  }
  
  public DataCodingScheme getDataCodingSchemeStructedData() {
    return this.mDataCodingSchemeStructedData;
  }
  
  public int getPageIndex() {
    return this.mPageIndex;
  }
  
  public int getNumberOfPages() {
    return this.mNrOfPages;
  }
  
  public SmsCbEtwsInfo getEtwsInfo() {
    return this.mEtwsInfo;
  }
  
  public SmsCbCmasInfo getCmasInfo() {
    return this.mCmasInfo;
  }
  
  public boolean isEmergencyMessage() {
    boolean bool;
    int i = this.mMessageIdentifier;
    if (i >= 4352 && i <= 6399) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isEtwsMessage() {
    boolean bool;
    if ((this.mMessageIdentifier & 0xFFF8) == 4352) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isEtwsPrimaryNotification() {
    boolean bool;
    if (this.mFormat == 3) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isUmtsFormat() {
    boolean bool;
    if (this.mFormat == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isCmasMessage() {
    boolean bool;
    int i = this.mMessageIdentifier;
    if (i >= 4370 && i <= 4400) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isEtwsPopupAlert() {
    boolean bool;
    if ((this.mSerialNumber & 0x1000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isEtwsEmergencyUserAlert() {
    boolean bool;
    if ((this.mSerialNumber & 0x2000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private int getEtwsWarningType() {
    return this.mMessageIdentifier - 4352;
  }
  
  private int getCmasMessageClass() {
    switch (this.mMessageIdentifier) {
      default:
        return -1;
      case 4382:
      case 4395:
        return 6;
      case 4381:
      case 4394:
        return 5;
      case 4380:
      case 4393:
        return 4;
      case 4379:
      case 4392:
        return 3;
      case 4373:
      case 4374:
      case 4375:
      case 4376:
      case 4377:
      case 4378:
      case 4386:
      case 4387:
      case 4388:
      case 4389:
      case 4390:
      case 4391:
        return 2;
      case 4371:
      case 4372:
      case 4384:
      case 4385:
        return 1;
      case 4370:
      case 4383:
        break;
    } 
    return 0;
  }
  
  private int getCmasSeverity() {
    int i = this.mMessageIdentifier;
    switch (i) {
      default:
        switch (i) {
          default:
            return -1;
          case 4388:
          case 4389:
          case 4390:
          case 4391:
            return 1;
          case 4384:
          case 4385:
          case 4386:
          case 4387:
            break;
        } 
        break;
      case 4375:
      case 4376:
      case 4377:
      case 4378:
      
      case 4371:
      case 4372:
      case 4373:
      case 4374:
        break;
    } 
    return 0;
  }
  
  private int getCmasUrgency() {
    int i = this.mMessageIdentifier;
    switch (i) {
      default:
        switch (i) {
          default:
            return -1;
          case 4386:
          case 4387:
          case 4390:
          case 4391:
            return 1;
          case 4384:
          case 4385:
          case 4388:
          case 4389:
            break;
        } 
        break;
      case 4373:
      case 4374:
      case 4377:
      case 4378:
      
      case 4371:
      case 4372:
      case 4375:
      case 4376:
        break;
    } 
    return 0;
  }
  
  private int getCmasCertainty() {
    int i = this.mMessageIdentifier;
    switch (i) {
      default:
        switch (i) {
          default:
            return -1;
          case 4385:
          case 4387:
          case 4389:
          case 4391:
            return 1;
          case 4384:
          case 4386:
          case 4388:
          case 4390:
            break;
        } 
        break;
      case 4372:
      case 4374:
      case 4376:
      case 4378:
      
      case 4371:
      case 4373:
      case 4375:
      case 4377:
        break;
    } 
    return 0;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SmsCbHeader{GS=");
    stringBuilder.append(this.mGeographicalScope);
    stringBuilder.append(", serialNumber=0x");
    int i = this.mSerialNumber;
    stringBuilder.append(Integer.toHexString(i));
    stringBuilder.append(", messageIdentifier=0x");
    i = this.mMessageIdentifier;
    stringBuilder.append(Integer.toHexString(i));
    stringBuilder.append(", format=");
    stringBuilder.append(this.mFormat);
    stringBuilder.append(", DCS=0x");
    i = this.mDataCodingScheme;
    stringBuilder.append(Integer.toHexString(i));
    stringBuilder.append(", page ");
    stringBuilder.append(this.mPageIndex);
    stringBuilder.append(" of ");
    stringBuilder.append(this.mNrOfPages);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public static final class DataCodingScheme {
    public final int encoding;
    
    public final boolean hasLanguageIndicator;
    
    public final String language;
    
    public DataCodingScheme(int param1Int) {
      String str = null;
      boolean bool = false;
      int i = (param1Int & 0xF0) >> 4;
      if (i != 9 && i != 14)
        if (i != 15) {
          switch (i) {
            default:
              param1Int = 1;
              this.encoding = param1Int;
              this.language = str;
              this.hasLanguageIndicator = bool;
            case 4:
            case 5:
              param1Int = (param1Int & 0xC) >> 2;
              if (param1Int != 1) {
                if (param1Int != 2) {
                  param1Int = 1;
                } else {
                  param1Int = 3;
                } 
              } else {
                param1Int = 2;
              } 
              this.encoding = param1Int;
              this.language = str;
              this.hasLanguageIndicator = bool;
            case 3:
              param1Int = 1;
              this.encoding = param1Int;
              this.language = str;
              this.hasLanguageIndicator = bool;
            case 2:
              i = 1;
              str = SmsCbHeader.LANGUAGE_CODES_GROUP_2[param1Int & 0xF];
              param1Int = i;
              this.encoding = param1Int;
              this.language = str;
              this.hasLanguageIndicator = bool;
            case 1:
              bool = true;
              if ((param1Int & 0xF) == 1) {
                param1Int = 3;
              } else {
                param1Int = 1;
              } 
              this.encoding = param1Int;
              this.language = str;
              this.hasLanguageIndicator = bool;
            case 0:
              i = 1;
              str = SmsCbHeader.LANGUAGE_CODES_GROUP_0[param1Int & 0xF];
              param1Int = i;
              this.encoding = param1Int;
              this.language = str;
              this.hasLanguageIndicator = bool;
            case 6:
            case 7:
              break;
          } 
        } else {
          if ((param1Int & 0x4) >> 2 == 1) {
            param1Int = 2;
          } else {
            param1Int = 1;
          } 
          this.encoding = param1Int;
          this.language = str;
          this.hasLanguageIndicator = bool;
        }  
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unsupported GSM dataCodingScheme ");
      stringBuilder.append(param1Int);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
  }
}
