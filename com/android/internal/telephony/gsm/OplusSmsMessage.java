package com.android.internal.telephony.gsm;

import android.content.res.Resources;
import android.os.SystemProperties;
import android.telephony.Rlog;
import android.text.TextUtils;
import com.android.internal.telephony.GsmAlphabet;
import com.android.internal.telephony.Sms7BitEncodingTranslator;
import com.android.internal.telephony.SmsMessageBase;
import com.android.internal.telephony.util.ReflectionHelper;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.StandardCharsets;

public class OplusSmsMessage {
  private static final String LOG_TAG = "gsm-OplusSmsMessage";
  
  private static final int NUMBER_0x04 = 4;
  
  public static GsmAlphabet.TextEncodingDetails calculateLengthOem(CharSequence paramCharSequence, boolean paramBoolean, int paramInt) {
    String str;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("use7bitOnly=");
    stringBuilder.append(paramBoolean);
    stringBuilder.append(",encodingType=");
    stringBuilder.append(paramInt);
    Rlog.d("gsm-OplusSmsMessage", stringBuilder.toString());
    stringBuilder = null;
    Resources resources = Resources.getSystem();
    if (paramBoolean || resources.getBoolean(17891537))
      str = Sms7BitEncodingTranslator.translate(paramCharSequence, false); 
    CharSequence charSequence = str;
    if (TextUtils.isEmpty(str))
      charSequence = paramCharSequence; 
    GsmAlphabet.TextEncodingDetails textEncodingDetails = GsmAlphabet.countGsmSeptets(charSequence, paramBoolean);
    if (paramInt == 3) {
      Rlog.d("gsm-OplusSmsMessage", "input mode is unicode");
      textEncodingDetails = null;
    } 
    if (textEncodingDetails == null) {
      Rlog.d("gsm-OplusSmsMessage", "7-bit encoding fail");
      return SmsMessageBase.calcUnicodeEncodingDetails(charSequence);
    } 
    return textEncodingDetails;
  }
  
  public static String getUserDataOem8bit(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, int paramInt1, int paramInt2) {
    CharsetDecoder charsetDecoder1 = null;
    CharsetDecoder charsetDecoder2 = charsetDecoder1;
    if (paramArrayOfbyte1 != null) {
      charsetDecoder2 = charsetDecoder1;
      if (paramArrayOfbyte2 != null)
        try {
          charsetDecoder2 = StandardCharsets.UTF_8.newDecoder();
          int i = paramArrayOfbyte1.length;
          byte[] arrayOfByte = new byte[i];
          System.arraycopy(paramArrayOfbyte1, 0, arrayOfByte, 0, i);
          ByteBuffer byteBuffer = ByteBuffer.wrap(arrayOfByte);
          String str = charsetDecoder2.decode(byteBuffer).toString();
        } catch (Exception exception) {
          Rlog.d("gsm-OplusSmsMessage", "UTF_8 parse error");
          try {
            String str = GsmAlphabet.gsm8BitUnpackedToString(paramArrayOfbyte2, paramInt1, paramInt2);
          } catch (Exception exception1) {
            Rlog.d("gsm-OplusSmsMessage", "GSM_8 parse error");
            charsetDecoder2 = charsetDecoder1;
          } 
        }  
    } 
    return (String)charsetDecoder2;
  }
  
  public static boolean isEnable8BitMtSms() {
    Rlog.d("gsm-OplusSmsMessage", "isEnable8BitMtSms false");
    return false;
  }
  
  public static SmsMessage.SubmitPdu oemGetSubmitPdu(String paramString1, String paramString2, int paramInt, byte[] paramArrayOfbyte, boolean paramBoolean) {
    ByteArrayOutputStream byteArrayOutputStream;
    boolean bool = false;
    try {
      boolean bool1 = SystemProperties.get("persist.sys.oplus.radio.ct_auto_ims", "0").equals("1");
      bool = bool1;
      StringBuilder stringBuilder = new StringBuilder();
      bool = bool1;
      this();
      bool = bool1;
      stringBuilder.append("isCtIms=");
      bool = bool1;
      stringBuilder.append(bool1);
      bool = bool1;
      Rlog.d("gsm-OplusSmsMessage", stringBuilder.toString());
      bool = bool1;
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
    String str = null;
    if (!bool)
      return null; 
    if (paramArrayOfbyte.length > 140) {
      Rlog.e("gsm-OplusSmsMessage", "SMS data message may only contain 140 bytes");
      return null;
    } 
    SmsMessage.SubmitPdu submitPdu = new SmsMessage.SubmitPdu();
    Class<byte> clazz = byte.class;
    Class<boolean> clazz1 = boolean.class;
    Object object = ReflectionHelper.callDeclaredMethodOrThrow(null, "com.android.internal.telephony.gsm.SmsMessage", "getSubmitPduHead", new Class[] { String.class, String.class, clazz, clazz1, SmsMessage.SubmitPdu.class }, new Object[] { paramString1, paramString2, Byte.valueOf((byte)1), Boolean.valueOf(paramBoolean), submitPdu });
    paramString1 = str;
    if (object != null)
      byteArrayOutputStream = (ByteArrayOutputStream)object; 
    if (byteArrayOutputStream == null)
      return submitPdu; 
    byteArrayOutputStream.write(4);
    byteArrayOutputStream.write(paramArrayOfbyte.length);
    byteArrayOutputStream.write(paramArrayOfbyte, 0, paramArrayOfbyte.length);
    submitPdu.encodedMessage = byteArrayOutputStream.toByteArray();
    try {
      SystemProperties.set("persist.sys.oplus.radio.ct_auto_ims", "0");
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
    return submitPdu;
  }
}
