package com.android.internal.telephony.gsm;

public final class SmsBroadcastConfigInfo {
  private int mFromCodeScheme;
  
  private int mFromServiceId;
  
  private boolean mSelected;
  
  private int mToCodeScheme;
  
  private int mToServiceId;
  
  public SmsBroadcastConfigInfo(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean) {
    this.mFromServiceId = paramInt1;
    this.mToServiceId = paramInt2;
    this.mFromCodeScheme = paramInt3;
    this.mToCodeScheme = paramInt4;
    this.mSelected = paramBoolean;
  }
  
  public void setFromServiceId(int paramInt) {
    this.mFromServiceId = paramInt;
  }
  
  public int getFromServiceId() {
    return this.mFromServiceId;
  }
  
  public void setToServiceId(int paramInt) {
    this.mToServiceId = paramInt;
  }
  
  public int getToServiceId() {
    return this.mToServiceId;
  }
  
  public void setFromCodeScheme(int paramInt) {
    this.mFromCodeScheme = paramInt;
  }
  
  public int getFromCodeScheme() {
    return this.mFromCodeScheme;
  }
  
  public void setToCodeScheme(int paramInt) {
    this.mToCodeScheme = paramInt;
  }
  
  public int getToCodeScheme() {
    return this.mToCodeScheme;
  }
  
  public void setSelected(boolean paramBoolean) {
    this.mSelected = paramBoolean;
  }
  
  public boolean isSelected() {
    return this.mSelected;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SmsBroadcastConfigInfo: Id [");
    stringBuilder.append(this.mFromServiceId);
    stringBuilder.append(',');
    stringBuilder.append(this.mToServiceId);
    stringBuilder.append("] Code [");
    stringBuilder.append(this.mFromCodeScheme);
    stringBuilder.append(',');
    stringBuilder.append(this.mToCodeScheme);
    stringBuilder.append("] ");
    if (this.mSelected) {
      null = "ENABLED";
    } else {
      null = "DISABLED";
    } 
    stringBuilder.append(null);
    return stringBuilder.toString();
  }
}
