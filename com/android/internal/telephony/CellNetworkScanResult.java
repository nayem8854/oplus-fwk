package com.android.internal.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

public class CellNetworkScanResult implements Parcelable {
  public CellNetworkScanResult(int paramInt, List<OperatorInfo> paramList) {
    this.mStatus = paramInt;
    this.mOperators = paramList;
  }
  
  private CellNetworkScanResult(Parcel paramParcel) {
    this.mStatus = paramParcel.readInt();
    int i = paramParcel.readInt();
    if (i > 0) {
      this.mOperators = new ArrayList<>();
      for (byte b = 0; b < i; b++)
        this.mOperators.add((OperatorInfo)OperatorInfo.CREATOR.createFromParcel(paramParcel)); 
    } else {
      this.mOperators = null;
    } 
  }
  
  public int getStatus() {
    return this.mStatus;
  }
  
  public List<OperatorInfo> getOperators() {
    return this.mOperators;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mStatus);
    List<OperatorInfo> list = this.mOperators;
    if (list != null && list.size() > 0) {
      paramParcel.writeInt(this.mOperators.size());
      for (OperatorInfo operatorInfo : this.mOperators)
        operatorInfo.writeToParcel(paramParcel, paramInt); 
    } else {
      paramParcel.writeInt(0);
    } 
  }
  
  public String toString() {
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append("CellNetworkScanResult: {");
    stringBuffer.append(" status:");
    stringBuffer.append(this.mStatus);
    List<OperatorInfo> list = this.mOperators;
    if (list != null)
      for (OperatorInfo operatorInfo : list) {
        stringBuffer.append(" network:");
        stringBuffer.append(operatorInfo);
      }  
    stringBuffer.append("}");
    return stringBuffer.toString();
  }
  
  public static final Parcelable.Creator<CellNetworkScanResult> CREATOR = new Parcelable.Creator<CellNetworkScanResult>() {
      public CellNetworkScanResult createFromParcel(Parcel param1Parcel) {
        return new CellNetworkScanResult(param1Parcel);
      }
      
      public CellNetworkScanResult[] newArray(int param1Int) {
        return new CellNetworkScanResult[param1Int];
      }
    };
  
  public static final int STATUS_RADIO_GENERIC_FAILURE = 3;
  
  public static final int STATUS_RADIO_NOT_AVAILABLE = 2;
  
  public static final int STATUS_SUCCESS = 1;
  
  public static final int STATUS_UNKNOWN_ERROR = 4;
  
  private final List<OperatorInfo> mOperators;
  
  private final int mStatus;
}
