package com.android.internal.telephony;

import android.os.Parcel;
import android.os.Parcelable;

public class SmsRawData implements Parcelable {
  public static final Parcelable.Creator<SmsRawData> CREATOR = new Parcelable.Creator<SmsRawData>() {
      public SmsRawData createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        byte[] arrayOfByte = new byte[i];
        param1Parcel.readByteArray(arrayOfByte);
        return new SmsRawData(arrayOfByte);
      }
      
      public SmsRawData[] newArray(int param1Int) {
        return new SmsRawData[param1Int];
      }
    };
  
  byte[] data;
  
  public SmsRawData(byte[] paramArrayOfbyte) {
    this.data = paramArrayOfbyte;
  }
  
  public byte[] getBytes() {
    return this.data;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.data.length);
    paramParcel.writeByteArray(this.data);
  }
}
