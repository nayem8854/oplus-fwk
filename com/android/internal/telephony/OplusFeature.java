package com.android.internal.telephony;

import android.app.ActivityThread;
import android.os.RemoteException;
import android.telephony.OplusTelephonyManager;
import android.telephony.Rlog;

public class OplusFeature {
  public static final boolean FEATURE_NW_NEC;
  
  public static final boolean FEATURE_NW_SIM_CMCC_AUTO_REG = true;
  
  public static final boolean FEATURE_NW_SIM_HOTSWAP = true;
  
  public static final boolean OPLUS_BUG_STABILITY = true;
  
  public static final boolean OPLUS_FEATURE_1X_INCALL_MMI_CODE = true;
  
  public static final boolean OPLUS_FEATURE_5G_ICON_JP_REQUIREMENT;
  
  public static final boolean OPLUS_FEATURE_5G_SUPPORT;
  
  public static final boolean OPLUS_FEATURE_ACCELERATE_THREAD;
  
  public static final boolean OPLUS_FEATURE_ALLOWSMSWRITE_NONSYSTEMAPP = true;
  
  public static final boolean OPLUS_FEATURE_ALLOWSMSWRITE_SYSTEMAPP = true;
  
  public static final boolean OPLUS_FEATURE_APTG_FORCE_ENABLE_ROAMING;
  
  public static final boolean OPLUS_FEATURE_AUTO_ANSWER = true;
  
  public static final boolean OPLUS_FEATURE_BUGFIX_GOOGLE = true;
  
  public static final boolean OPLUS_FEATURE_BUGFIX_SMSDELAY4S;
  
  public static final boolean OPLUS_FEATURE_CB;
  
  public static final boolean OPLUS_FEATURE_CBCHINA_CDMA = true;
  
  public static final boolean OPLUS_FEATURE_CBCHINA_GSM = true;
  
  public static final boolean OPLUS_FEATURE_CBEXP_CDMA = false;
  
  public static final boolean OPLUS_FEATURE_CBEXP_GSM = true;
  
  public static final boolean OPLUS_FEATURE_CBGSM_NONDDS;
  
  public static final boolean OPLUS_FEATURE_CBLTE_NONDDS;
  
  public static final boolean OPLUS_FEATURE_CB_RAKUTEN;
  
  public static final boolean OPLUS_FEATURE_CDMACW_FILTER = true;
  
  public static final boolean OPLUS_FEATURE_CMCC_DM_SWITCH;
  
  public static final boolean OPLUS_FEATURE_COMM_AOL;
  
  public static final boolean OPLUS_FEATURE_COMM_PROXIMITY = true;
  
  public static final boolean OPLUS_FEATURE_CTIMSSMS_AUTOREG = true;
  
  public static final boolean OPLUS_FEATURE_CTSMS_AUTOREG = true;
  
  public static final boolean OPLUS_FEATURE_CTSMS_DCCHANNLE = true;
  
  public static final boolean OPLUS_FEATURE_DATA_AVOID_DATA_ICON = true;
  
  public static final boolean OPLUS_FEATURE_DATA_CTA = true;
  
  public static final boolean OPLUS_FEATURE_DATA_DEFAULT_AS_DUN_APN = true;
  
  public static final boolean OPLUS_FEATURE_DATA_DYNAMIC_ENABLE = true;
  
  public static final boolean OPLUS_FEATURE_DATA_MTU = true;
  
  public static final boolean OPLUS_FEATURE_DATA_POWER_OPTIMIZE = true;
  
  public static final boolean OPLUS_FEATURE_DATA_VSIM;
  
  public static final boolean OPLUS_FEATURE_DDS_SWITCH;
  
  public static final boolean OPLUS_FEATURE_DIALING_SHOW86_ENABLED;
  
  public static final boolean OPLUS_FEATURE_DISABLE_AUTO_NETWORK_SELECT;
  
  public static final boolean OPLUS_FEATURE_DISABLE_NR5G_CAUSE_5G_DUMP;
  
  public static final boolean OPLUS_FEATURE_DISABLE_SMSCHECKPERIOD = true;
  
  public static final boolean OPLUS_FEATURE_DISPLAY_PLMN_SPN;
  
  public static final boolean OPLUS_FEATURE_DS_CK = false;
  
  public static final boolean OPLUS_FEATURE_ECC_CSFB_UPDATE_RAT;
  
  public static final boolean OPLUS_FEATURE_ECC_SOLUTION = true;
  
  public static final boolean OPLUS_FEATURE_FAST_DORMANCY;
  
  public static final boolean OPLUS_FEATURE_FAST_RECOVERY;
  
  public static final boolean OPLUS_FEATURE_FBE_BLACKLIST = true;
  
  public static final boolean OPLUS_FEATURE_FIND_PHONE;
  
  public static final boolean OPLUS_FEATURE_FIX_DUPLICATEDSMS;
  
  public static final boolean OPLUS_FEATURE_GAMESPACE_MT_OPTIMIZATION = true;
  
  public static final boolean OPLUS_FEATURE_GETDESTINATION_TOUI = true;
  
  public static final boolean OPLUS_FEATURE_GETRIGHT_PACKAGENAME = true;
  
  public static final boolean OPLUS_FEATURE_GOOGLE_MESSAGE_CONFIG = true;
  
  public static final boolean OPLUS_FEATURE_HIDESMS_SYSTEMAPP = true;
  
  public static final boolean OPLUS_FEATURE_HVOLTE_SUPPORT;
  
  public static final boolean OPLUS_FEATURE_IGNORE_GOOGLE_ORIGINAL_NOTIFICATION_PS_CS = true;
  
  public static final boolean OPLUS_FEATURE_IGNORE_RETRICTED;
  
  public static final boolean OPLUS_FEATURE_IGNORE_SIM_VM_NUMBER = true;
  
  public static final boolean OPLUS_FEATURE_IGNOTIFY_CSPS = true;
  
  public static final boolean OPLUS_FEATURE_IMSSMSERROR30_RETRYTOSGS = true;
  
  public static final boolean OPLUS_FEATURE_JP_SIGNAL_STRENGTH;
  
  public static final boolean OPLUS_FEATURE_LIGHT_SMART5G;
  
  public static final boolean OPLUS_FEATURE_LOC_CONTROL_SWITCH;
  
  public static final boolean OPLUS_FEATURE_LOG_GAME_ERR;
  
  public static final boolean OPLUS_FEATURE_LOG_GAME_PAGING;
  
  public static final boolean OPLUS_FEATURE_LTE_INSTEND_OF_CA;
  
  public static final boolean OPLUS_FEATURE_MAX_THREE_PDNS;
  
  public static final boolean OPLUS_FEATURE_MMS_CUSTOMIZE = true;
  
  public static final boolean OPLUS_FEATURE_MMS_LOGCONTROL = true;
  
  public static final boolean OPLUS_FEATURE_MMS_NOT_ALLOW_INTERNET;
  
  public static final boolean OPLUS_FEATURE_MOSMS_RECORD = true;
  
  public static final boolean OPLUS_FEATURE_MSGTYPE_TOUI = true;
  
  public static final boolean OPLUS_FEATURE_MTSMS_RECORD = true;
  
  public static final boolean OPLUS_FEATURE_NOTIFY_DATA_CONNECTION;
  
  public static final boolean OPLUS_FEATURE_NR_ALWAYS_SA_PRE;
  
  public static final boolean OPLUS_FEATURE_NR_ICON_OPTIMIZED;
  
  public static final boolean OPLUS_FEATURE_NTP_PREFERRED;
  
  public static final boolean OPLUS_FEATURE_NW_DATA_COMM_OPTIMI = true;
  
  public static final boolean OPLUS_FEATURE_NW_DATA_DISCONNECT_FOR_PEER_SIM_IMS_CALL = true;
  
  public static final boolean OPLUS_FEATURE_NW_DATA_DOMESTIC_ROAMING = true;
  
  public static final boolean OPLUS_FEATURE_NW_DATA_FAST_DORMANCY = true;
  
  public static final boolean OPLUS_FEATURE_NW_DATA_HONGBAO = true;
  
  public static final boolean OPLUS_FEATURE_NW_DATA_OPERATOR_NUMERIC = true;
  
  public static final boolean OPLUS_FEATURE_NW_DATA_ROAMING_ENABLED = true;
  
  public static final boolean OPLUS_FEATURE_NW_DATA_STALL_ALARM = true;
  
  public static final boolean OPLUS_FEATURE_NW_DATA_TELSTRA = true;
  
  public static final boolean OPLUS_FEATURE_NW_DATA_VSIM = true;
  
  public static final boolean OPLUS_FEATURE_NW_DATA_WLAN_ASSIST = true;
  
  public static final boolean OPLUS_FEATURE_POLICY_SALEMODE = true;
  
  public static final boolean OPLUS_FEATURE_POLIC_CONTROL = true;
  
  public static final boolean OPLUS_FEATURE_PREPAID_POSTPAID_DYNAMIC_APN;
  
  public static final boolean OPLUS_FEATURE_RF_SAR_AIRINTERFACE_DETECT_NOTSUPPORT;
  
  public static final boolean OPLUS_FEATURE_RF_SAR_EARPIECE_DETECT_SUPPORT;
  
  public static final boolean OPLUS_FEATURE_RF_SAR_FCC_DETECT_SUPPORT;
  
  public static final boolean OPLUS_FEATURE_RF_SET_WIFI_SAR_ENABLED;
  
  public static final boolean OPLUS_FEATURE_ROAMING_DYNAMIC_DUN_APN;
  
  public static final boolean OPLUS_FEATURE_RUS_TELEPHONY = true;
  
  public static final boolean OPLUS_FEATURE_SA_RAT_CONTROL;
  
  public static final boolean OPLUS_FEATURE_SERVICESTATE_SMOOTH;
  
  public static final boolean OPLUS_FEATURE_SHOW_HD_PLUS;
  
  public static final boolean OPLUS_FEATURE_SHOW_NR5G_MODE;
  
  public static final boolean OPLUS_FEATURE_SIGNAL_SMOOTH;
  
  public static final boolean OPLUS_FEATURE_SIM_LOCALE = true;
  
  public static final boolean OPLUS_FEATURE_SIM_REGION = true;
  
  public static final boolean OPLUS_FEATURE_SIP491_RESUME_FAILURE = true;
  
  public static final boolean OPLUS_FEATURE_SMART5G_EXP_CFG;
  
  public static final boolean OPLUS_FEATURE_SMART5G_SA;
  
  public static final boolean OPLUS_FEATURE_SMART5G_SP_REGION_DISABLE;
  
  public static final boolean OPLUS_FEATURE_SMSBLACKLIST = true;
  
  public static final boolean OPLUS_FEATURE_SMSGSM8IT_ENABLE = true;
  
  public static final boolean OPLUS_FEATURE_SMS_7BIT16BIT;
  
  public static final boolean OPLUS_FEATURE_SMS_RTC832389 = true;
  
  public static final boolean OPLUS_FEATURE_SMS_RUNTIMECHECK = true;
  
  public static final boolean OPLUS_FEATURE_SMS_SET_DEFAULT_APP;
  
  public static final boolean OPLUS_FEATURE_SOFT_SIM = true;
  
  public static final boolean OPLUS_FEATURE_SUPPORT_DUAL_NR;
  
  public static final boolean OPLUS_FEATURE_SUPPORT_ROMBLOCKSMS = true;
  
  public static final boolean OPLUS_FEATURE_SUPPORT_ROMOLDFUNC = true;
  
  public static final boolean OPLUS_FEATURE_SUPPORT_SIM_LOCK_STATE;
  
  public static final boolean OPLUS_FEATURE_WAPPUSH_ENABLE;
  
  public static final boolean OPLUS_FEATURE_WMS_MAXRETRY3;
  
  public static final boolean OPLUS_FEATURE_WMS_PERIOD120INTERVAL45;
  
  public static final boolean OPLUS_FEATURE_WMS_RETRYINTERVAL5AND10;
  
  public static final boolean OPLUS_FEATURE_WMS_RPERROR38;
  
  public static final boolean OPLUS_FEATURE_WMS_RPERROR41;
  
  static {
    int i = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.signal_smooth_disabled") ^ true;
    OPLUS_FEATURE_SERVICESTATE_SMOOTH = i;
    OPLUS_FEATURE_NR_ICON_OPTIMIZED = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.nr_icon_optimized_enabled");
    OPLUS_FEATURE_SUPPORT_DUAL_NR = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.support_dual_nr");
    OPLUS_FEATURE_LTE_INSTEND_OF_CA = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.lte_instend_of_ca");
    OPLUS_FEATURE_LIGHT_SMART5G = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.light_smart5g_enabled");
    OPLUS_FEATURE_SMART5G_SA = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.smart5g_sa_enabled");
    OPLUS_FEATURE_SMART5G_EXP_CFG = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.smart5g_exp_cfg");
    OPLUS_FEATURE_SMART5G_SP_REGION_DISABLE = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.smart5g_sp_region_disable");
    OPLUS_FEATURE_ECC_CSFB_UPDATE_RAT = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.ecc_csfb_update_rat");
    OPLUS_FEATURE_IGNORE_RETRICTED = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.ignore_dcnr_retricted_enable");
    OPLUS_FEATURE_DISABLE_AUTO_NETWORK_SELECT = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.disable_auto_network_select");
    OPLUS_FEATURE_DISPLAY_PLMN_SPN = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.display_plmn_spn");
    OPLUS_FEATURE_LOC_CONTROL_SWITCH = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.loc_control_switch");
    OPLUS_FEATURE_SHOW_NR5G_MODE = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.show_5g_nr_mode");
    OPLUS_FEATURE_DISABLE_NR5G_CAUSE_5G_DUMP = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.disable_5g_nr_5gdump");
    OPLUS_FEATURE_NR_ALWAYS_SA_PRE = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.nr_always_sa_pre");
    OPLUS_FEATURE_FAST_DORMANCY = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.fast_dormancy");
    OPLUS_FEATURE_JP_SIGNAL_STRENGTH = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.jp_signal_strength");
    OPLUS_FEATURE_5G_SUPPORT = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.support_5g");
    OPLUS_FEATURE_SA_RAT_CONTROL = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.sa_rat_control");
    OPLUS_FEATURE_ACCELERATE_THREAD = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.accelerate");
    OPLUS_FEATURE_5G_ICON_JP_REQUIREMENT = OplusFeatureHelper.getInstance().hasFeature("oplus.software.5g_icon_jp_requirement");
    OPLUS_FEATURE_HVOLTE_SUPPORT = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.hvolte_disabled") ^ true;
    OPLUS_FEATURE_DIALING_SHOW86_ENABLED = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.dialingshow86_enabled");
    OPLUS_FEATURE_SHOW_HD_PLUS = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.show_hd_plus");
    OPLUS_FEATURE_NTP_PREFERRED = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.ntp_preferred");
    OPLUS_FEATURE_FAST_RECOVERY = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.data_fast_recovery");
    OPLUS_FEATURE_APTG_FORCE_ENABLE_ROAMING = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.aptg_force_enable_roaming");
    OPLUS_FEATURE_DATA_VSIM = isVsimIgnoreUserDataSetting();
    OPLUS_FEATURE_MAX_THREE_PDNS = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.max_three_pdns");
    OPLUS_FEATURE_PREPAID_POSTPAID_DYNAMIC_APN = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.prepaid_postpaid_dynamic_apn");
    OPLUS_FEATURE_ROAMING_DYNAMIC_DUN_APN = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.roaming_dynamic_dun_apn");
    OPLUS_FEATURE_NOTIFY_DATA_CONNECTION = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.notify_data_connection");
    OPLUS_FEATURE_SMS_7BIT16BIT = OplusTelephonyManager.isQcomPlatform();
    OPLUS_FEATURE_SMS_SET_DEFAULT_APP = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.sms_set_default_app");
    OPLUS_FEATURE_WAPPUSH_ENABLE = OplusTelephonyManager.isQcomPlatform();
    OPLUS_FEATURE_FIX_DUPLICATEDSMS = OplusTelephonyManager.isQcomPlatform();
    OPLUS_FEATURE_CB = OplusTelephonyManager.isQcomPlatform();
    OPLUS_FEATURE_CBLTE_NONDDS = OplusTelephonyManager.isQcomPlatform();
    OPLUS_FEATURE_CBGSM_NONDDS = OplusTelephonyManager.isQcomPlatform();
    OPLUS_FEATURE_WMS_PERIOD120INTERVAL45 = OplusTelephonyManager.isQcomPlatform();
    OPLUS_FEATURE_WMS_RETRYINTERVAL5AND10 = OplusTelephonyManager.isQcomPlatform();
    OPLUS_FEATURE_WMS_RPERROR38 = OplusTelephonyManager.isQcomPlatform();
    OPLUS_FEATURE_WMS_RPERROR41 = OplusTelephonyManager.isQcomPlatform();
    OPLUS_FEATURE_WMS_MAXRETRY3 = OplusTelephonyManager.isQcomPlatform();
    OPLUS_FEATURE_BUGFIX_SMSDELAY4S = OplusTelephonyManager.isQcomPlatform();
    OPLUS_FEATURE_CB_RAKUTEN = OplusTelephonyManager.isQcomPlatform();
    OPLUS_FEATURE_DDS_SWITCH = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.dds_switch");
    OPLUS_FEATURE_LOG_GAME_ERR = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.game_err");
    OPLUS_FEATURE_LOG_GAME_PAGING = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.game_paging");
    OPLUS_FEATURE_FIND_PHONE = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.find_phone");
    OPLUS_FEATURE_MMS_NOT_ALLOW_INTERNET = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.mms_not_allow_internet");
    OPLUS_FEATURE_CMCC_DM_SWITCH = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.cmcc_dm");
    OPLUS_FEATURE_SUPPORT_SIM_LOCK_STATE = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.support_sim_lock_state");
    OPLUS_FEATURE_RF_SET_WIFI_SAR_ENABLED = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.set_wifi_sar_enabled");
    OPLUS_FEATURE_COMM_AOL = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.motor_dat_enabled");
    OPLUS_FEATURE_RF_SAR_EARPIECE_DETECT_SUPPORT = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.sar.earpiece.detect.support");
    OPLUS_FEATURE_RF_SAR_FCC_DETECT_SUPPORT = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.sar.fcc.detect.support");
    OPLUS_FEATURE_RF_SAR_AIRINTERFACE_DETECT_NOTSUPPORT = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.sar.airinterface.detect.notsupport");
    FEATURE_NW_NEC = OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.health_service_support");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("OPLUS_FEATURE_SIGNAL_SMOOTH = ");
    stringBuilder.append(OPLUS_FEATURE_SIGNAL_SMOOTH);
    stringBuilder.append(", hasFeature =");
    stringBuilder.append(OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.signal_smooth_disabled"));
    stringBuilder.append(", OPLUS_FEATURE_NR_ICON_OPTIMIZED = ");
    stringBuilder.append(OPLUS_FEATURE_NR_ICON_OPTIMIZED);
    stringBuilder.append(", OPLUS_FEATURE_FAST_RECOVERY = ");
    stringBuilder.append(OPLUS_FEATURE_FAST_RECOVERY);
    String str = stringBuilder.toString();
    Rlog.d("OplusFeature init: ", str);
  }
  
  public static boolean isDualNrEnabled() {
    if (OPLUS_FEATURE_SUPPORT_DUAL_NR)
      return OplusFeatureHelper.getInstance().hasFeature("oplus.software.radio.dual_nr_enabled"); 
    return false;
  }
  
  public static boolean isVsimIgnoreUserDataSetting() {
    try {
      return ActivityThread.getPackageManager().hasSystemFeature("oplus.softsim.ignore_data_setting", 0);
    } catch (RemoteException remoteException) {
      remoteException.printStackTrace();
      return false;
    } 
  }
}
