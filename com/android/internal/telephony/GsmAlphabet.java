package com.android.internal.telephony;

import android.content.res.Resources;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseIntArray;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;

public class GsmAlphabet {
  public static final byte GSM_EXTENDED_ESCAPE = 27;
  
  private static final String TAG = "GSM";
  
  public static final int UDH_SEPTET_COST_CONCATENATED_MESSAGE = 6;
  
  public static final int UDH_SEPTET_COST_LENGTH = 1;
  
  public static final int UDH_SEPTET_COST_ONE_SHIFT_TABLE = 4;
  
  public static final int UDH_SEPTET_COST_TWO_SHIFT_TABLES = 7;
  
  private static final SparseIntArray[] sCharsToGsmTables;
  
  private static final SparseIntArray[] sCharsToShiftTables;
  
  public static class TextEncodingDetails {
    public int codeUnitCount;
    
    public int codeUnitSize;
    
    public int codeUnitsRemaining;
    
    public int languageShiftTable;
    
    public int languageTable;
    
    public int msgCount;
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("TextEncodingDetails { msgCount=");
      stringBuilder.append(this.msgCount);
      stringBuilder.append(", codeUnitCount=");
      stringBuilder.append(this.codeUnitCount);
      stringBuilder.append(", codeUnitsRemaining=");
      stringBuilder.append(this.codeUnitsRemaining);
      stringBuilder.append(", codeUnitSize=");
      stringBuilder.append(this.codeUnitSize);
      stringBuilder.append(", languageTable=");
      stringBuilder.append(this.languageTable);
      stringBuilder.append(", languageShiftTable=");
      stringBuilder.append(this.languageShiftTable);
      stringBuilder.append(" }");
      return stringBuilder.toString();
    }
  }
  
  public static int charToGsm(char paramChar) {
    try {
      return charToGsm(paramChar, false);
    } catch (EncodeException encodeException) {
      return sCharsToGsmTables[0].get(32, 32);
    } 
  }
  
  public static int charToGsm(char paramChar, boolean paramBoolean) throws EncodeException {
    int i = sCharsToGsmTables[0].get(paramChar, -1);
    if (i == -1) {
      i = sCharsToShiftTables[0].get(paramChar, -1);
      if (i == -1) {
        if (!paramBoolean)
          return sCharsToGsmTables[0].get(32, 32); 
        throw new EncodeException(paramChar);
      } 
      return 27;
    } 
    return i;
  }
  
  public static int charToGsmExtended(char paramChar) {
    int i = sCharsToShiftTables[0].get(paramChar, -1);
    if (i == -1)
      return sCharsToGsmTables[0].get(32, 32); 
    return i;
  }
  
  public static char gsmToChar(int paramInt) {
    if (paramInt >= 0 && paramInt < 128)
      return sLanguageTables[0].charAt(paramInt); 
    return ' ';
  }
  
  public static char gsmExtendedToChar(int paramInt) {
    if (paramInt == 27)
      return ' '; 
    if (paramInt >= 0 && paramInt < 128) {
      char c = sLanguageShiftTables[0].charAt(paramInt);
      if (c == ' ')
        return sLanguageTables[0].charAt(paramInt); 
      return c;
    } 
    return ' ';
  }
  
  public static byte[] stringToGsm7BitPackedWithHeader(String paramString, byte[] paramArrayOfbyte) throws EncodeException {
    return stringToGsm7BitPackedWithHeader(paramString, paramArrayOfbyte, 0, 0);
  }
  
  public static byte[] stringToGsm7BitPackedWithHeader(String paramString, byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws EncodeException {
    if (paramArrayOfbyte == null || paramArrayOfbyte.length == 0)
      return stringToGsm7BitPacked(paramString, paramInt1, paramInt2); 
    int i = paramArrayOfbyte.length;
    i = ((i + 1) * 8 + 6) / 7;
    byte[] arrayOfByte = stringToGsm7BitPacked(paramString, i, true, paramInt1, paramInt2);
    arrayOfByte[1] = (byte)paramArrayOfbyte.length;
    System.arraycopy(paramArrayOfbyte, 0, arrayOfByte, 2, paramArrayOfbyte.length);
    return arrayOfByte;
  }
  
  public static byte[] stringToGsm7BitPacked(String paramString) throws EncodeException {
    return stringToGsm7BitPacked(paramString, 0, true, 0, 0);
  }
  
  public static byte[] stringToGsm7BitPacked(String paramString, int paramInt1, int paramInt2) throws EncodeException {
    return stringToGsm7BitPacked(paramString, 0, true, paramInt1, paramInt2);
  }
  
  public static byte[] stringToGsm7BitPacked(String paramString, int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3) throws EncodeException {
    int i = paramString.length();
    int j = countGsmSeptetsUsingTables(paramString, paramBoolean ^ true, paramInt2, paramInt3);
    if (j != -1) {
      int k = j + paramInt1;
      if (k <= 255) {
        j = (k * 7 + 7) / 8;
        byte[] arrayOfByte = new byte[j + 1];
        SparseIntArray sparseIntArray1 = sCharsToGsmTables[paramInt2];
        SparseIntArray sparseIntArray2 = sCharsToShiftTables[paramInt3];
        j = 0;
        paramInt3 = paramInt1;
        paramInt2 = paramInt1 * 7;
        paramInt1 = paramInt3;
        for (; j < i && paramInt1 < k; 
          j++, paramInt2 = i1 + 7) {
          char c = paramString.charAt(j);
          int m = sparseIntArray1.get(c, -1);
          int n = paramInt1, i1 = paramInt2;
          paramInt3 = m;
          if (m == -1) {
            paramInt3 = sparseIntArray2.get(c, -1);
            if (paramInt3 == -1) {
              if (!paramBoolean) {
                paramInt3 = sparseIntArray1.get(32, 32);
                n = paramInt1;
                i1 = paramInt2;
              } else {
                throw new EncodeException("stringToGsm7BitPacked(): unencodable char");
              } 
            } else {
              packSmsChar(arrayOfByte, paramInt2, 27);
              i1 = paramInt2 + 7;
              n = paramInt1 + 1;
            } 
          } 
          packSmsChar(arrayOfByte, i1, paramInt3);
          paramInt1 = n + 1;
        } 
        arrayOfByte[0] = (byte)k;
        return arrayOfByte;
      } 
      throw new EncodeException("Payload cannot exceed 255 septets", 1);
    } 
    throw new EncodeException("countGsmSeptetsUsingTables(): unencodable char");
  }
  
  private static void packSmsChar(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    int i = paramInt1 / 8;
    paramInt1 %= 8;
    paramArrayOfbyte[++i] = (byte)(paramArrayOfbyte[i] | paramInt2 << paramInt1);
    if (paramInt1 > 1)
      paramArrayOfbyte[i + 1] = (byte)(paramInt2 >> 8 - paramInt1); 
  }
  
  public static String gsm7BitPackedToString(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    return gsm7BitPackedToString(paramArrayOfbyte, paramInt1, paramInt2, 0, 0, 0);
  }
  
  public static String gsm7BitPackedToString(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    // Byte code:
    //   0: new java/lang/StringBuilder
    //   3: dup
    //   4: iload_2
    //   5: invokespecial <init> : (I)V
    //   8: astore #6
    //   10: iload #4
    //   12: iflt -> 30
    //   15: iload #4
    //   17: getstatic com/android/internal/telephony/GsmAlphabet.sLanguageTables : [Ljava/lang/String;
    //   20: arraylength
    //   21: if_icmple -> 27
    //   24: goto -> 30
    //   27: goto -> 79
    //   30: new java/lang/StringBuilder
    //   33: dup
    //   34: invokespecial <init> : ()V
    //   37: astore #7
    //   39: aload #7
    //   41: ldc_w 'unknown language table '
    //   44: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   47: pop
    //   48: aload #7
    //   50: iload #4
    //   52: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   55: pop
    //   56: aload #7
    //   58: ldc_w ', using default'
    //   61: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   64: pop
    //   65: ldc 'GSM'
    //   67: aload #7
    //   69: invokevirtual toString : ()Ljava/lang/String;
    //   72: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   75: pop
    //   76: iconst_0
    //   77: istore #4
    //   79: iload #5
    //   81: iflt -> 97
    //   84: iload #5
    //   86: istore #8
    //   88: iload #5
    //   90: getstatic com/android/internal/telephony/GsmAlphabet.sLanguageShiftTables : [Ljava/lang/String;
    //   93: arraylength
    //   94: if_icmple -> 146
    //   97: new java/lang/StringBuilder
    //   100: dup
    //   101: invokespecial <init> : ()V
    //   104: astore #7
    //   106: aload #7
    //   108: ldc_w 'unknown single shift table '
    //   111: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   114: pop
    //   115: aload #7
    //   117: iload #5
    //   119: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   122: pop
    //   123: aload #7
    //   125: ldc_w ', using default'
    //   128: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   131: pop
    //   132: ldc 'GSM'
    //   134: aload #7
    //   136: invokevirtual toString : ()Ljava/lang/String;
    //   139: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   142: pop
    //   143: iconst_0
    //   144: istore #8
    //   146: iconst_0
    //   147: istore #9
    //   149: getstatic com/android/internal/telephony/GsmAlphabet.sLanguageTables : [Ljava/lang/String;
    //   152: iload #4
    //   154: aaload
    //   155: astore #10
    //   157: getstatic com/android/internal/telephony/GsmAlphabet.sLanguageShiftTables : [Ljava/lang/String;
    //   160: iload #8
    //   162: aaload
    //   163: astore #11
    //   165: aload #10
    //   167: astore #7
    //   169: aload #10
    //   171: invokevirtual isEmpty : ()Z
    //   174: ifeq -> 231
    //   177: new java/lang/StringBuilder
    //   180: astore #7
    //   182: aload #7
    //   184: invokespecial <init> : ()V
    //   187: aload #7
    //   189: ldc_w 'no language table for code '
    //   192: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   195: pop
    //   196: aload #7
    //   198: iload #4
    //   200: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   203: pop
    //   204: aload #7
    //   206: ldc_w ', using default'
    //   209: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   212: pop
    //   213: ldc 'GSM'
    //   215: aload #7
    //   217: invokevirtual toString : ()Ljava/lang/String;
    //   220: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   223: pop
    //   224: getstatic com/android/internal/telephony/GsmAlphabet.sLanguageTables : [Ljava/lang/String;
    //   227: iconst_0
    //   228: aaload
    //   229: astore #7
    //   231: aload #11
    //   233: astore #10
    //   235: aload #11
    //   237: invokevirtual isEmpty : ()Z
    //   240: ifeq -> 297
    //   243: new java/lang/StringBuilder
    //   246: astore #10
    //   248: aload #10
    //   250: invokespecial <init> : ()V
    //   253: aload #10
    //   255: ldc_w 'no single shift table for code '
    //   258: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   261: pop
    //   262: aload #10
    //   264: iload #8
    //   266: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   269: pop
    //   270: aload #10
    //   272: ldc_w ', using default'
    //   275: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   278: pop
    //   279: ldc 'GSM'
    //   281: aload #10
    //   283: invokevirtual toString : ()Ljava/lang/String;
    //   286: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   289: pop
    //   290: getstatic com/android/internal/telephony/GsmAlphabet.sLanguageShiftTables : [Ljava/lang/String;
    //   293: iconst_0
    //   294: aaload
    //   295: astore #10
    //   297: iconst_0
    //   298: istore #5
    //   300: iload #9
    //   302: istore #4
    //   304: iload #5
    //   306: iload_2
    //   307: if_icmpge -> 488
    //   310: iload #5
    //   312: bipush #7
    //   314: imul
    //   315: iload_3
    //   316: iadd
    //   317: istore #8
    //   319: iload #8
    //   321: bipush #8
    //   323: idiv
    //   324: istore #12
    //   326: iload #8
    //   328: bipush #8
    //   330: irem
    //   331: istore #13
    //   333: aload_0
    //   334: iload_1
    //   335: iload #12
    //   337: iadd
    //   338: baload
    //   339: iload #13
    //   341: ishr
    //   342: bipush #127
    //   344: iand
    //   345: istore #9
    //   347: iload #9
    //   349: istore #8
    //   351: iload #13
    //   353: iconst_1
    //   354: if_icmple -> 387
    //   357: iload #9
    //   359: bipush #127
    //   361: iload #13
    //   363: iconst_1
    //   364: isub
    //   365: ishr
    //   366: iand
    //   367: bipush #127
    //   369: aload_0
    //   370: iload_1
    //   371: iload #12
    //   373: iadd
    //   374: iconst_1
    //   375: iadd
    //   376: baload
    //   377: bipush #8
    //   379: iload #13
    //   381: isub
    //   382: ishl
    //   383: iand
    //   384: ior
    //   385: istore #8
    //   387: iload #4
    //   389: ifeq -> 456
    //   392: iload #8
    //   394: bipush #27
    //   396: if_icmpne -> 410
    //   399: aload #6
    //   401: bipush #32
    //   403: invokevirtual append : (C)Ljava/lang/StringBuilder;
    //   406: pop
    //   407: goto -> 450
    //   410: aload #10
    //   412: iload #8
    //   414: invokevirtual charAt : (I)C
    //   417: istore #14
    //   419: iload #14
    //   421: bipush #32
    //   423: if_icmpne -> 442
    //   426: aload #6
    //   428: aload #7
    //   430: iload #8
    //   432: invokevirtual charAt : (I)C
    //   435: invokevirtual append : (C)Ljava/lang/StringBuilder;
    //   438: pop
    //   439: goto -> 450
    //   442: aload #6
    //   444: iload #14
    //   446: invokevirtual append : (C)Ljava/lang/StringBuilder;
    //   449: pop
    //   450: iconst_0
    //   451: istore #4
    //   453: goto -> 482
    //   456: iload #8
    //   458: bipush #27
    //   460: if_icmpne -> 469
    //   463: iconst_1
    //   464: istore #4
    //   466: goto -> 482
    //   469: aload #6
    //   471: aload #7
    //   473: iload #8
    //   475: invokevirtual charAt : (I)C
    //   478: invokevirtual append : (C)Ljava/lang/StringBuilder;
    //   481: pop
    //   482: iinc #5, 1
    //   485: goto -> 304
    //   488: aload #6
    //   490: invokevirtual toString : ()Ljava/lang/String;
    //   493: areturn
    //   494: astore_0
    //   495: ldc 'GSM'
    //   497: ldc_w 'Error GSM 7 bit packed: '
    //   500: aload_0
    //   501: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   504: pop
    //   505: aconst_null
    //   506: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #498	-> 0
    //   #500	-> 10
    //   #501	-> 30
    //   #502	-> 76
    //   #504	-> 79
    //   #505	-> 97
    //   #506	-> 143
    //   #510	-> 146
    //   #511	-> 149
    //   #512	-> 157
    //   #514	-> 165
    //   #515	-> 177
    //   #516	-> 224
    //   #518	-> 231
    //   #519	-> 243
    //   #520	-> 290
    //   #523	-> 297
    //   #524	-> 310
    //   #526	-> 319
    //   #527	-> 326
    //   #530	-> 333
    //   #533	-> 347
    //   #535	-> 357
    //   #537	-> 357
    //   #540	-> 387
    //   #541	-> 392
    //   #542	-> 399
    //   #544	-> 410
    //   #545	-> 419
    //   #546	-> 426
    //   #548	-> 442
    //   #551	-> 450
    //   #552	-> 456
    //   #553	-> 463
    //   #555	-> 469
    //   #523	-> 482
    //   #561	-> 488
    //   #563	-> 488
    //   #558	-> 494
    //   #559	-> 495
    //   #560	-> 505
    // Exception table:
    //   from	to	target	type
    //   149	157	494	java/lang/RuntimeException
    //   157	165	494	java/lang/RuntimeException
    //   169	177	494	java/lang/RuntimeException
    //   177	224	494	java/lang/RuntimeException
    //   224	231	494	java/lang/RuntimeException
    //   235	243	494	java/lang/RuntimeException
    //   243	290	494	java/lang/RuntimeException
    //   290	297	494	java/lang/RuntimeException
    //   319	326	494	java/lang/RuntimeException
    //   399	407	494	java/lang/RuntimeException
    //   410	419	494	java/lang/RuntimeException
    //   426	439	494	java/lang/RuntimeException
    //   442	450	494	java/lang/RuntimeException
    //   469	482	494	java/lang/RuntimeException
  }
  
  public static String gsm8BitUnpackedToString(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    return gsm8BitUnpackedToString(paramArrayOfbyte, paramInt1, paramInt2, "");
  }
  
  public static String gsm8BitUnpackedToString(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, String paramString) {
    int j;
    Charset charset2;
    ByteBuffer byteBuffer2;
    int i = 0;
    Charset charset1 = null;
    ByteBuffer byteBuffer1 = null;
    if (!TextUtils.isEmpty(paramString)) {
      j = i;
      charset2 = charset1;
      byteBuffer2 = byteBuffer1;
      if (!paramString.equalsIgnoreCase("us-ascii")) {
        j = i;
        charset2 = charset1;
        byteBuffer2 = byteBuffer1;
        if (Charset.isSupported(paramString)) {
          j = 1;
          charset2 = Charset.forName(paramString);
          byteBuffer2 = ByteBuffer.allocate(2);
        } 
      } 
    } else {
      byteBuffer2 = byteBuffer1;
      charset2 = charset1;
      j = i;
    } 
    String str1 = sLanguageTables[0];
    String str2 = sLanguageShiftTables[0];
    StringBuilder stringBuilder = new StringBuilder(paramInt2);
    boolean bool = false;
    for (i = paramInt1; i < paramInt1 + paramInt2; i++) {
      int k = paramArrayOfbyte[i] & 0xFF;
      if (k == 255)
        break; 
      if (k == 27) {
        if (bool) {
          stringBuilder.append(' ');
          bool = false;
        } else {
          bool = true;
        } 
      } else {
        if (bool) {
          byte b;
          if (k < str2.length()) {
            b = str2.charAt(k);
          } else {
            b = 32;
          } 
          if (b == 32) {
            if (k < str1.length()) {
              stringBuilder.append(str1.charAt(k));
            } else {
              stringBuilder.append(' ');
            } 
          } else {
            stringBuilder.append(b);
          } 
        } else if (j == 0 || k < 128 || i + 1 >= paramInt1 + paramInt2) {
          if (k < str1.length()) {
            stringBuilder.append(str1.charAt(k));
          } else {
            stringBuilder.append(' ');
          } 
        } else {
          byteBuffer2.clear();
          byteBuffer2.put(paramArrayOfbyte, i, 2);
          byteBuffer2.flip();
          stringBuilder.append(charset2.decode(byteBuffer2).toString());
          i++;
        } 
        bool = false;
      } 
    } 
    return stringBuilder.toString();
  }
  
  public static byte[] stringToGsm8BitPacked(String paramString) {
    int i = countGsmSeptetsUsingTables(paramString, true, 0, 0);
    byte[] arrayOfByte = new byte[i];
    stringToGsm8BitUnpackedField(paramString, arrayOfByte, 0, arrayOfByte.length);
    return arrayOfByte;
  }
  
  public static void stringToGsm8BitUnpackedField(String paramString, byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    int k, i = paramInt1;
    SparseIntArray sparseIntArray1 = sCharsToGsmTables[0];
    SparseIntArray sparseIntArray2 = sCharsToShiftTables[0];
    byte b = 0;
    int j = paramString.length();
    while (true) {
      k = i;
      if (b < j) {
        k = i;
        if (i - paramInt1 < paramInt2) {
          char c = paramString.charAt(b);
          int m = sparseIntArray1.get(c, -1);
          int n = i;
          k = m;
          if (m == -1) {
            k = sparseIntArray2.get(c, -1);
            if (k == -1) {
              k = sparseIntArray1.get(32, 32);
              n = i;
            } else {
              if (i + 1 - paramInt1 >= paramInt2) {
                k = i;
                break;
              } 
              paramArrayOfbyte[i] = 27;
              n = i + 1;
            } 
          } 
          paramArrayOfbyte[n] = (byte)k;
          b++;
          i = n + 1;
          continue;
        } 
      } 
      break;
    } 
    while (k - paramInt1 < paramInt2) {
      paramArrayOfbyte[k] = -1;
      k++;
    } 
  }
  
  public static int countGsmSeptets(char paramChar) {
    try {
      return countGsmSeptets(paramChar, false);
    } catch (EncodeException encodeException) {
      return 0;
    } 
  }
  
  public static int countGsmSeptets(char paramChar, boolean paramBoolean) throws EncodeException {
    if (sCharsToGsmTables[0].get(paramChar, -1) != -1)
      return 1; 
    if (sCharsToShiftTables[0].get(paramChar, -1) != -1)
      return 2; 
    if (!paramBoolean)
      return 1; 
    throw new EncodeException(paramChar);
  }
  
  public static boolean isGsmSeptets(char paramChar) {
    if (sCharsToGsmTables[0].get(paramChar, -1) != -1)
      return true; 
    if (sCharsToShiftTables[0].get(paramChar, -1) != -1)
      return true; 
    return false;
  }
  
  public static int countGsmSeptetsUsingTables(CharSequence paramCharSequence, boolean paramBoolean, int paramInt1, int paramInt2) {
    char c = Character.MIN_VALUE;
    int i = paramCharSequence.length();
    SparseIntArray sparseIntArray1 = sCharsToGsmTables[paramInt1];
    SparseIntArray sparseIntArray2 = sCharsToShiftTables[paramInt2];
    for (paramInt2 = 0, paramInt1 = c; paramInt2 < i; paramInt2++) {
      c = paramCharSequence.charAt(paramInt2);
      if (c == '\033') {
        Log.w("GSM", "countGsmSeptets() string contains Escape character, skipping.");
      } else if (sparseIntArray1.get(c, -1) != -1) {
        paramInt1++;
      } else if (sparseIntArray2.get(c, -1) != -1) {
        paramInt1 += 2;
      } else if (paramBoolean) {
        paramInt1++;
      } else {
        return -1;
      } 
    } 
    return paramInt1;
  }
  
  public static TextEncodingDetails countGsmSeptets(CharSequence paramCharSequence, boolean paramBoolean) {
    int n;
    if (!sDisableCountryEncodingCheck)
      enableCountrySpecificEncodings(); 
    int i = sEnabledSingleShiftTables.length, j = sEnabledLockingShiftTables.length, k = 0;
    if (i + j == 0) {
      TextEncodingDetails textEncodingDetails1 = new TextEncodingDetails();
      k = countGsmSeptetsUsingTables(paramCharSequence, paramBoolean, 0, 0);
      if (k == -1)
        return null; 
      textEncodingDetails1.codeUnitSize = 1;
      textEncodingDetails1.codeUnitCount = k;
      if (k > 160) {
        textEncodingDetails1.msgCount = (k + 152) / 153;
        textEncodingDetails1.codeUnitsRemaining = textEncodingDetails1.msgCount * 153 - k;
      } else {
        textEncodingDetails1.msgCount = 1;
        textEncodingDetails1.codeUnitsRemaining = 160 - k;
      } 
      return textEncodingDetails1;
    } 
    int m = sHighestEnabledSingleShiftCode;
    ArrayList<LanguagePairCount> arrayList = new ArrayList(sEnabledLockingShiftTables.length + 1);
    arrayList.add(new LanguagePairCount(0));
    for (int[] arrayOfInt = sEnabledLockingShiftTables; k < i; ) {
      j = arrayOfInt[k];
      if (j != 0 && !sLanguageTables[j].isEmpty())
        arrayList.add(new LanguagePairCount(j)); 
      k++;
    } 
    j = paramCharSequence.length();
    for (k = 0; k < j && !arrayList.isEmpty(); k++) {
      char c = paramCharSequence.charAt(k);
      if (c == '\033') {
        Log.w("GSM", "countGsmSeptets() string contains Escape character, ignoring!");
      } else {
        for (LanguagePairCount languagePairCount : arrayList) {
          i = sCharsToGsmTables[languagePairCount.languageCode].get(c, -1);
          if (i == -1) {
            for (i = 0; i <= m; i++) {
              if (languagePairCount.septetCounts[i] != -1) {
                n = sCharsToShiftTables[i].get(c, -1);
                if (n == -1) {
                  if (paramBoolean) {
                    int[] arrayOfInt1 = languagePairCount.septetCounts;
                    arrayOfInt1[i] = arrayOfInt1[i] + 1;
                    arrayOfInt1 = languagePairCount.unencodableCounts;
                    arrayOfInt1[i] = arrayOfInt1[i] + 1;
                  } else {
                    languagePairCount.septetCounts[i] = -1;
                  } 
                } else {
                  int[] arrayOfInt1 = languagePairCount.septetCounts;
                  arrayOfInt1[i] = arrayOfInt1[i] + 2;
                } 
              } 
            } 
            continue;
          } 
          for (i = 0; i <= m; i++) {
            if (languagePairCount.septetCounts[i] != -1) {
              int[] arrayOfInt1 = languagePairCount.septetCounts;
              arrayOfInt1[i] = arrayOfInt1[i] + 1;
            } 
          } 
        } 
      } 
    } 
    TextEncodingDetails textEncodingDetails = new TextEncodingDetails();
    textEncodingDetails.msgCount = Integer.MAX_VALUE;
    textEncodingDetails.codeUnitSize = 1;
    j = Integer.MAX_VALUE;
    for (LanguagePairCount languagePairCount : arrayList) {
      i = 0;
      while (true) {
        i++;
        j = n;
      } 
    } 
    if (textEncodingDetails.msgCount == Integer.MAX_VALUE)
      return null; 
    return textEncodingDetails;
  }
  
  public static int findGsmSeptetLimitIndex(String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    boolean bool = false;
    int i = paramString.length();
    SparseIntArray sparseIntArray1 = sCharsToGsmTables[paramInt3];
    SparseIntArray sparseIntArray2 = sCharsToShiftTables[paramInt4];
    for (paramInt3 = paramInt1, paramInt1 = bool; paramInt3 < i; paramInt3++) {
      paramInt4 = sparseIntArray1.get(paramString.charAt(paramInt3), -1);
      if (paramInt4 == -1) {
        paramInt4 = sparseIntArray2.get(paramString.charAt(paramInt3), -1);
        if (paramInt4 == -1) {
          paramInt1++;
        } else {
          paramInt1 += 2;
        } 
      } else {
        paramInt1++;
      } 
      if (paramInt1 > paramInt2)
        return paramInt3; 
    } 
    return i;
  }
  
  public static void setEnabledSingleShiftTables(int[] paramArrayOfint) {
    // Byte code:
    //   0: ldc com/android/internal/telephony/GsmAlphabet
    //   2: monitorenter
    //   3: aload_0
    //   4: putstatic com/android/internal/telephony/GsmAlphabet.sEnabledSingleShiftTables : [I
    //   7: iconst_1
    //   8: putstatic com/android/internal/telephony/GsmAlphabet.sDisableCountryEncodingCheck : Z
    //   11: aload_0
    //   12: arraylength
    //   13: ifle -> 28
    //   16: aload_0
    //   17: aload_0
    //   18: arraylength
    //   19: iconst_1
    //   20: isub
    //   21: iaload
    //   22: putstatic com/android/internal/telephony/GsmAlphabet.sHighestEnabledSingleShiftCode : I
    //   25: goto -> 32
    //   28: iconst_0
    //   29: putstatic com/android/internal/telephony/GsmAlphabet.sHighestEnabledSingleShiftCode : I
    //   32: ldc com/android/internal/telephony/GsmAlphabet
    //   34: monitorexit
    //   35: return
    //   36: astore_0
    //   37: ldc com/android/internal/telephony/GsmAlphabet
    //   39: monitorexit
    //   40: aload_0
    //   41: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1041	-> 3
    //   #1042	-> 7
    //   #1044	-> 11
    //   #1045	-> 16
    //   #1047	-> 28
    //   #1049	-> 32
    //   #1040	-> 36
    // Exception table:
    //   from	to	target	type
    //   3	7	36	finally
    //   7	11	36	finally
    //   11	16	36	finally
    //   16	25	36	finally
    //   28	32	36	finally
  }
  
  public static void setEnabledLockingShiftTables(int[] paramArrayOfint) {
    // Byte code:
    //   0: ldc com/android/internal/telephony/GsmAlphabet
    //   2: monitorenter
    //   3: aload_0
    //   4: putstatic com/android/internal/telephony/GsmAlphabet.sEnabledLockingShiftTables : [I
    //   7: iconst_1
    //   8: putstatic com/android/internal/telephony/GsmAlphabet.sDisableCountryEncodingCheck : Z
    //   11: ldc com/android/internal/telephony/GsmAlphabet
    //   13: monitorexit
    //   14: return
    //   15: astore_0
    //   16: ldc com/android/internal/telephony/GsmAlphabet
    //   18: monitorexit
    //   19: aload_0
    //   20: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1059	-> 3
    //   #1060	-> 7
    //   #1061	-> 11
    //   #1058	-> 15
    // Exception table:
    //   from	to	target	type
    //   3	7	15	finally
    //   7	11	15	finally
  }
  
  public static int[] getEnabledSingleShiftTables() {
    // Byte code:
    //   0: ldc com/android/internal/telephony/GsmAlphabet
    //   2: monitorenter
    //   3: getstatic com/android/internal/telephony/GsmAlphabet.sEnabledSingleShiftTables : [I
    //   6: astore_0
    //   7: ldc com/android/internal/telephony/GsmAlphabet
    //   9: monitorexit
    //   10: aload_0
    //   11: areturn
    //   12: astore_0
    //   13: ldc com/android/internal/telephony/GsmAlphabet
    //   15: monitorexit
    //   16: aload_0
    //   17: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1071	-> 3
    //   #1071	-> 12
    // Exception table:
    //   from	to	target	type
    //   3	7	12	finally
  }
  
  public static int[] getEnabledLockingShiftTables() {
    // Byte code:
    //   0: ldc com/android/internal/telephony/GsmAlphabet
    //   2: monitorenter
    //   3: getstatic com/android/internal/telephony/GsmAlphabet.sEnabledLockingShiftTables : [I
    //   6: astore_0
    //   7: ldc com/android/internal/telephony/GsmAlphabet
    //   9: monitorexit
    //   10: aload_0
    //   11: areturn
    //   12: astore_0
    //   13: ldc com/android/internal/telephony/GsmAlphabet
    //   15: monitorexit
    //   16: aload_0
    //   17: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1082	-> 3
    //   #1082	-> 12
    // Exception table:
    //   from	to	target	type
    //   3	7	12	finally
  }
  
  private static void enableCountrySpecificEncodings() {
    Resources resources = Resources.getSystem();
    sEnabledSingleShiftTables = resources.getIntArray(17236078);
    sEnabledLockingShiftTables = resources.getIntArray(17236077);
    int[] arrayOfInt = sEnabledSingleShiftTables;
    if (arrayOfInt.length > 0) {
      sHighestEnabledSingleShiftCode = arrayOfInt[arrayOfInt.length - 1];
    } else {
      sHighestEnabledSingleShiftCode = 0;
    } 
  }
  
  private static boolean sDisableCountryEncodingCheck = false;
  
  private static int[] sEnabledLockingShiftTables;
  
  private static int[] sEnabledSingleShiftTables;
  
  private static int sHighestEnabledSingleShiftCode;
  
  private static final String[] sLanguageShiftTables;
  
  private static class LanguagePairCount {
    final int languageCode;
    
    final int[] septetCounts;
    
    final int[] unencodableCounts;
    
    LanguagePairCount(int param1Int) {
      this.languageCode = param1Int;
      int i = GsmAlphabet.sHighestEnabledSingleShiftCode;
      this.septetCounts = new int[i + 1];
      this.unencodableCounts = new int[i + 1];
      for (byte b1 = 1, b2 = 0; b1 <= i; b1++) {
        if (GsmAlphabet.sEnabledSingleShiftTables[b2] == b1) {
          b2++;
        } else {
          this.septetCounts[b1] = -1;
        } 
      } 
      if (param1Int == 1 && i >= 1) {
        this.septetCounts[1] = -1;
      } else if (param1Int == 3 && i >= 2) {
        this.septetCounts[2] = -1;
      } 
    }
  }
  
  private static final String[] sLanguageTables = new String[] { 
      "@£$¥èéùìòÇ\nØø\rÅåΔ_ΦΓΛΩΠΨΣΘΞ￿ÆæßÉ !\"#¤%&'()*+,-./0123456789:;<=>?¡ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÑÜ§¿abcdefghijklmnopqrstuvwxyzäöñüà", "@£$¥€éùıòÇ\nĞğ\rÅåΔ_ΦΓΛΩΠΨΣΘΞ￿ŞşßÉ !\"#¤%&'()*+,-./0123456789:;<=>?İABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÑÜ§çabcdefghijklmnopqrstuvwxyzäöñüà", "", "@£$¥êéúíóç\nÔô\rÁáΔ_ªÇÀ∞^\\€Ó|￿ÂâÊÉ !\"#º%&'()*+,-./0123456789:;<=>?ÍABCDEFGHIJKLMNOPQRSTUVWXYZÃÕÚÜ§~abcdefghijklmnopqrstuvwxyzãõ`üà", "ঁংঃঅআইঈউঊঋ\nঌ \r এঐ  ওঔকখগঘঙচ￿ছজঝঞ !টঠডঢণত)(থদ,ধ.ন0123456789:; পফ?বভমযর ল   শষসহ়ঽািীুূৃৄ  েৈ  োৌ্ৎabcdefghijklmnopqrstuvwxyzৗড়ঢ়ৰৱ", "ઁંઃઅઆઇઈઉઊઋ\nઌઍ\r એઐઑ ઓઔકખગઘઙચ￿છજઝઞ !ટઠડઢણત)(થદ,ધ.ન0123456789:; પફ?બભમયર લળ વશષસહ઼ઽાિીુૂૃૄૅ ેૈૉ ોૌ્ૐabcdefghijklmnopqrstuvwxyzૠૡૢૣ૱", "ँंःअआइईउऊऋ\nऌऍ\rऎएऐऑऒओऔकखगघङच￿छजझञ !टठडढणत)(थद,ध.न0123456789:;ऩपफ?बभमयरऱलळऴवशषसह़ऽािीुूृॄॅॆेैॉॊोौ्ॐabcdefghijklmnopqrstuvwxyzॲॻॼॾॿ", " ಂಃಅಆಇಈಉಊಋ\nಌ \rಎಏಐ ಒಓಔಕಖಗಘಙಚ￿ಛಜಝಞ !ಟಠಡಢಣತ)(ಥದ,ಧ.ನ0123456789:; ಪಫ?ಬಭಮಯರಱಲಳ ವಶಷಸಹ಼ಽಾಿೀುೂೃೄ ೆೇೈ ೊೋೌ್ೕabcdefghijklmnopqrstuvwxyzೖೠೡೢೣ", " ംഃഅആഇഈഉഊഋ\nഌ \rഎഏഐ ഒഓഔകഖഗഘങച￿ഛജഝഞ !ടഠഡഢണത)(ഥദ,ധ.ന0123456789:; പഫ?ബഭമയരറലളഴവശഷസഹ ഽാിീുൂൃൄ െേൈ ൊോൌ്ൗabcdefghijklmnopqrstuvwxyzൠൡൢൣ൹", "ଁଂଃଅଆଇଈଉଊଋ\nଌ \r ଏଐ  ଓଔକଖଗଘଙଚ￿ଛଜଝଞ !ଟଠଡଢଣତ)(ଥଦ,ଧ.ନ0123456789:; ପଫ?ବଭମଯର ଲଳ ଵଶଷସହ଼ଽାିୀୁୂୃୄ  େୈ  ୋୌ୍ୖabcdefghijklmnopqrstuvwxyzୗୠୡୢୣ", 
      "ਁਂਃਅਆਇਈਉਊ \n  \r ਏਐ  ਓਔਕਖਗਘਙਚ￿ਛਜਝਞ !ਟਠਡਢਣਤ)(ਥਦ,ਧ.ਨ0123456789:; ਪਫ?ਬਭਮਯਰ ਲਲ਼ ਵਸ਼ ਸਹ਼ ਾਿੀੁੂ    ੇੈ  ੋੌ੍ੑabcdefghijklmnopqrstuvwxyzੰੱੲੳੴ", " ஂஃஅஆஇஈஉஊ \n  \rஎஏஐ ஒஓஔக   ஙச￿ ஜ ஞ !ட   ணத)(  , .ந0123456789:;னப ?  மயரறலளழவஶஷஸஹ  ாிீுூ   ெேை ொோௌ்ௐabcdefghijklmnopqrstuvwxyzௗ௰௱௲௹", "ఁంఃఅఆఇఈఉఊఋ\nఌ \rఎఏఐ ఒఓఔకఖగఘఙచ￿ఛజఝఞ !టఠడఢణత)(థద,ధ.న0123456789:; పఫ?బభమయరఱలళ వశషసహ ఽాిీుూృౄ ెేై ొోౌ్ౕabcdefghijklmnopqrstuvwxyzౖౠౡౢౣ", "اآبٻڀپڦتۂٿ\nٹٽ\rٺټثجځڄڃڅچڇحخد￿ڌڈډڊ !ڏڍذرڑړ)(ڙز,ږ.ژ0123456789:;ښسش?صضطظعفقکڪګگڳڱلمنںڻڼوۄەہھءیېےٍُِٗٔabcdefghijklmnopqrstuvwxyzّٰٕٖٓ" };
  
  static {
    sLanguageShiftTables = new String[] { 
        "          \f         ^                   {}     \\            [~] |                                    €                          ", "          \f         ^                   {}     \\            [~] |      Ğ İ         Ş               ç € ğ ı         ş            ", "         ç\f         ^                   {}     \\            [~] |Á       Í     Ó     Ú           á   €   í     ó     ú          ", "     ê   ç\fÔô Áá  ΦΓ^ΩΠΨΣΘ     Ê        {}     \\            [~] |À       Í     Ó     Ú     ÃÕ    Â   €   í     ó     ú     ãõ  â", "@£$¥¿\"¤%&'\f*+ -/<=>¡^¡_#*০১ ২৩৪৫৬৭৮৯য়ৠৡৢ{}ৣ৲৳৴৵\\৶৷৸৹৺       [~] |ABCDEFGHIJKLMNOPQRSTUVWXYZ          €                          ", "@£$¥¿\"¤%&'\f*+ -/<=>¡^¡_#*।॥ ૦૧૨૩૪૫૬૭૮૯  {}     \\            [~] |ABCDEFGHIJKLMNOPQRSTUVWXYZ          €                          ", "@£$¥¿\"¤%&'\f*+ -/<=>¡^¡_#*।॥ ०१२३४५६७८९॒॑{}॓॔क़ख़ग़\\ज़ड़ढ़फ़य़ॠॡॢॣ॰ॱ [~] |ABCDEFGHIJKLMNOPQRSTUVWXYZ          €                          ", "@£$¥¿\"¤%&'\f*+ -/<=>¡^¡_#*।॥ ೦೧೨೩೪೫೬೭೮೯ೞೱ{}ೲ    \\            [~] |ABCDEFGHIJKLMNOPQRSTUVWXYZ          €                          ", "@£$¥¿\"¤%&'\f*+ -/<=>¡^¡_#*।॥ ൦൧൨൩൪൫൬൭൮൯൰൱{}൲൳൴൵ൺ\\ൻർൽൾൿ       [~] |ABCDEFGHIJKLMNOPQRSTUVWXYZ          €                          ", "@£$¥¿\"¤%&'\f*+ -/<=>¡^¡_#*।॥ ୦୧୨୩୪୫୬୭୮୯ଡ଼ଢ଼{}ୟ୰ୱ  \\            [~] |ABCDEFGHIJKLMNOPQRSTUVWXYZ          €                          ", 
        "@£$¥¿\"¤%&'\f*+ -/<=>¡^¡_#*।॥ ੦੧੨੩੪੫੬੭੮੯ਖ਼ਗ਼{}ਜ਼ੜਫ਼ੵ \\            [~] |ABCDEFGHIJKLMNOPQRSTUVWXYZ          €                          ", "@£$¥¿\"¤%&'\f*+ -/<=>¡^¡_#*।॥ ௦௧௨௩௪௫௬௭௮௯௳௴{}௵௶௷௸௺\\            [~] |ABCDEFGHIJKLMNOPQRSTUVWXYZ          €                          ", "@£$¥¿\"¤%&'\f*+ -/<=>¡^¡_#*   ౦౧౨౩౪౫౬౭౮౯ౘౙ{}౸౹౺౻౼\\౽౾౿         [~] |ABCDEFGHIJKLMNOPQRSTUVWXYZ          €                          ", "@£$¥¿\"¤%&'\f*+ -/<=>¡^¡_#*؀؁ ۰۱۲۳۴۵۶۷۸۹،؍{}؎؏ؐؑؒ\\ؓؔ؛؟ـْ٘٫٬ٲٳۍ[~]۔|ABCDEFGHIJKLMNOPQRSTUVWXYZ          €                          " };
    enableCountrySpecificEncodings();
    int i = sLanguageTables.length;
    int j = sLanguageShiftTables.length;
    if (i != j) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error: language tables array length ");
      stringBuilder.append(i);
      stringBuilder.append(" != shift tables array length ");
      stringBuilder.append(j);
      Log.e("GSM", stringBuilder.toString());
    } 
    sCharsToGsmTables = new SparseIntArray[i];
    byte b;
    for (b = 0; b < i; b++) {
      String str = sLanguageTables[b];
      int k = str.length();
      if (k != 0 && k != 128) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Error: language tables index ");
        stringBuilder.append(b);
        stringBuilder.append(" length ");
        stringBuilder.append(k);
        stringBuilder.append(" (expected 128 or 0)");
        Log.e("GSM", stringBuilder.toString());
      } 
      SparseIntArray sparseIntArray = new SparseIntArray(k);
      sCharsToGsmTables[b] = sparseIntArray;
      for (byte b1 = 0; b1 < k; b1++) {
        char c = str.charAt(b1);
        sparseIntArray.put(c, b1);
      } 
    } 
    sCharsToShiftTables = new SparseIntArray[j];
    for (b = 0; b < j; b++) {
      String str = sLanguageShiftTables[b];
      i = str.length();
      if (i != 0 && i != 128) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Error: language shift tables index ");
        stringBuilder.append(b);
        stringBuilder.append(" length ");
        stringBuilder.append(i);
        stringBuilder.append(" (expected 128 or 0)");
        Log.e("GSM", stringBuilder.toString());
      } 
      SparseIntArray sparseIntArray = new SparseIntArray(i);
      sCharsToShiftTables[b] = sparseIntArray;
      for (byte b1 = 0; b1 < i; b1++) {
        char c = str.charAt(b1);
        if (c != ' ')
          sparseIntArray.put(c, b1); 
      } 
    } 
  }
}
