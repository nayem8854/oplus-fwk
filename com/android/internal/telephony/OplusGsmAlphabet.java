package com.android.internal.telephony;

import android.text.TextUtils;
import android.util.Log;
import java.io.UnsupportedEncodingException;

public class OplusGsmAlphabet {
  private static final String TAG = "GSM";
  
  private static boolean is0X80coding = false;
  
  private static boolean is0X81coding = false;
  
  private static boolean is0X82coding = false;
  
  private static int max;
  
  private static int min;
  
  public static byte[] stringToGsm8BitOrUCSPackedForADN(String paramString) {
    byte[] arrayOfByte;
    if (paramString == null)
      return null; 
    try {
      int i = countGsmSeptets(paramString, true, 1);
      byte[] arrayOfByte1 = new byte[i];
      GsmAlphabet.stringToGsm8BitUnpackedField(paramString, arrayOfByte1, 0, arrayOfByte1.length);
      arrayOfByte = arrayOfByte1;
    } catch (EncodeException encodeException) {
      try {
        byte[] arrayOfByte1 = arrayOfByte.getBytes("utf-16be");
        arrayOfByte = new byte[arrayOfByte1.length / 2];
        judge(arrayOfByte1, 0, arrayOfByte1.length);
        arrayOfByte = ucs2ToAlphaField(arrayOfByte1, 0, arrayOfByte1.length, 0, arrayOfByte);
        return arrayOfByte;
      } catch (UnsupportedEncodingException unsupportedEncodingException) {
        Log.e("GSM", "unsurport encoding.", unsupportedEncodingException);
        return null;
      } 
    } 
    return (byte[])unsupportedEncodingException;
  }
  
  public static void judge(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    min = 32767;
    max = 0;
    if (paramInt2 >= 2)
      for (byte b = 0; b < paramInt2; b += 2) {
        if (paramArrayOfbyte[paramInt1 + b] != 0) {
          int i = paramArrayOfbyte[paramInt1 + b] << 8 & 0xFF00 | paramArrayOfbyte[paramInt1 + b + 1] & 0xFF;
          if (min > i)
            min = i; 
          if (max < i)
            max = i; 
        } else if ((paramArrayOfbyte[paramInt1 + b + 1] & 0x80) != 0) {
          max = min + 130;
          break;
        } 
      }  
    paramInt2 = max;
    paramInt1 = min;
    if (paramInt2 - paramInt1 < 129) {
      if ((byte)(paramInt1 & 0x80) == (byte)(paramInt2 & 0x80)) {
        is0X81coding = true;
        is0X82coding = false;
        is0X80coding = false;
      } else {
        is0X82coding = true;
        is0X81coding = false;
        is0X80coding = false;
      } 
    } else {
      is0X80coding = true;
      is0X82coding = false;
      is0X81coding = false;
    } 
  }
  
  public static byte[] ucs2ToAlphaField(byte[] paramArrayOfbyte1, int paramInt1, int paramInt2, int paramInt3, byte[] paramArrayOfbyte2) {
    byte[] arrayOfByte;
    int i = 0;
    if (!is0X80coding) {
      if (is0X81coding) {
        paramArrayOfbyte2 = new byte[paramInt2 / 2 + 3];
        paramArrayOfbyte2[paramInt3 + 1] = (byte)(paramInt2 / 2);
        paramArrayOfbyte2[paramInt3] = -127;
        min = i = min & 0x7F80;
        paramArrayOfbyte2[paramInt3 + 2] = (byte)(i >> 7 & 0xFF);
        i = paramInt3 + 3;
      } else if (is0X82coding) {
        paramArrayOfbyte2 = new byte[paramInt2 / 2 + 4];
        paramArrayOfbyte2[paramInt3 + 1] = (byte)(paramInt2 / 2);
        paramArrayOfbyte2[paramInt3] = -126;
        i = min;
        paramArrayOfbyte2[paramInt3 + 2] = (byte)(i >> 8 & 0xFF);
        paramArrayOfbyte2[paramInt3 + 3] = (byte)(i & 0xFF);
        i = paramInt3 + 4;
      } 
      paramInt3 = 0;
      while (true) {
        arrayOfByte = paramArrayOfbyte2;
        if (paramInt3 < paramInt2) {
          if (paramArrayOfbyte1[paramInt1 + paramInt3] == 0) {
            paramArrayOfbyte2[i] = (byte)(paramArrayOfbyte1[paramInt1 + paramInt3 + 1] & Byte.MAX_VALUE);
          } else {
            byte b1 = paramArrayOfbyte1[paramInt1 + paramInt3], b2 = paramArrayOfbyte1[paramInt1 + paramInt3 + 1];
            int j = min;
            paramArrayOfbyte2[i] = (byte)((b1 << 8 & 0xFF00 | b2 & 0xFF) - j | 0x80);
          } 
          i++;
          paramInt3 += 2;
          continue;
        } 
        break;
      } 
    } else {
      arrayOfByte = new byte[paramInt2 + 1];
      arrayOfByte[paramInt3] = Byte.MIN_VALUE;
      System.arraycopy(paramArrayOfbyte1, 0, arrayOfByte, 1, paramInt2);
    } 
    return arrayOfByte;
  }
  
  public static boolean enableToEncode0X80() {
    return is0X80coding;
  }
  
  public static boolean enableToEncode0X81() {
    return is0X81coding;
  }
  
  public static boolean enableToEncode0X82() {
    return is0X82coding;
  }
  
  public static int countGsmSeptets(CharSequence paramCharSequence, boolean paramBoolean, int paramInt) throws EncodeException {
    byte b = 0;
    int i = paramCharSequence.length();
    paramInt = 0;
    while (b < i) {
      paramInt += GsmAlphabet.countGsmSeptets(paramCharSequence.charAt(b), paramBoolean);
      b++;
    } 
    return paramInt;
  }
  
  public static boolean isChinese(char paramChar) {
    boolean bool = false;
    Character.UnicodeBlock unicodeBlock = Character.UnicodeBlock.of(paramChar);
    if (unicodeBlock == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS || unicodeBlock == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS || unicodeBlock == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A || unicodeBlock == Character.UnicodeBlock.GENERAL_PUNCTUATION || unicodeBlock == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION || unicodeBlock == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS)
      bool = true; 
    return bool;
  }
  
  public static boolean containChinese(String paramString) {
    if (TextUtils.isEmpty(paramString))
      return false; 
    char[] arrayOfChar = paramString.toCharArray();
    int i = arrayOfChar.length;
    for (byte b = 0; b < i; b++) {
      char c = arrayOfChar[b];
      if (isChinese(c))
        return true; 
    } 
    return false;
  }
  
  public static boolean isEnglish(String paramString) {
    boolean bool2;
    int i = paramString.length();
    boolean bool1 = true;
    byte b = 0;
    while (true) {
      bool2 = bool1;
      if (b < i) {
        char c = paramString.charAt(b);
        if (c >= '!' && c <= '~') {
          b++;
          continue;
        } 
        bool2 = false;
      } 
      break;
    } 
    return bool2;
  }
  
  public static boolean isThai(String paramString) {
    char c2;
    boolean bool2;
    int i = paramString.length();
    char c1 = '\001';
    boolean bool1 = false;
    byte b = 0;
    while (true) {
      c2 = c1;
      if (b < i) {
        c2 = paramString.charAt(b);
        if (c2 < '!' || c2 > '~')
          if (c2 >= 'ก' && c2 <= '๙') {
            bool1 = true;
          } else {
            c2 = Character.MIN_VALUE;
            break;
          }  
        b++;
        continue;
      } 
      break;
    } 
    if (c2 != '\000' && bool1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    return bool2;
  }
  
  public static boolean isRussian(String paramString) {
    char c2;
    boolean bool2;
    int i = paramString.length();
    char c1 = '\001';
    boolean bool1 = false;
    byte b = 0;
    while (true) {
      c2 = c1;
      if (b < i) {
        c2 = paramString.charAt(b);
        if (c2 < '!' || c2 > '~')
          if (c2 >= 'Ѐ' && c2 <= 'ӿ') {
            bool1 = true;
          } else {
            c2 = Character.MIN_VALUE;
            break;
          }  
        b++;
        continue;
      } 
      break;
    } 
    if (c2 != '\000' && bool1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    return bool2;
  }
  
  public static boolean enableEncodeTo0x81(String paramString) {
    boolean bool = false;
    if (containChinese(paramString)) {
      bool = false;
    } else if (isThai(paramString)) {
      bool = true;
    } else if (isRussian(paramString)) {
      bool = true;
    } 
    return bool;
  }
  
  public static byte[] encodeTo0x81(String paramString) {
    StringBuilder stringBuilder;
    byte b = 0;
    int i = paramString.length();
    byte[] arrayOfByte = new byte[i + 3];
    arrayOfByte[0] = -127;
    arrayOfByte[1] = (byte)i;
    for (byte b1 = 0; b1 < i; ) {
      String str = paramString.substring(b1, b1 + 1);
      try {
        byte b2, arrayOfByte1[] = str.getBytes("utf-16be");
        if (!isEnglish(str)) {
          byte b3 = b;
          if (!b) {
            byte b5 = (byte)(arrayOfByte1[0] << 1);
            arrayOfByte[2] = b5;
            b3 = b5;
          } 
          byte b4 = arrayOfByte1[1];
          b = b3;
          b2 = b4;
          if ((b4 & 0x80) == 0) {
            b2 = (byte)(b4 | 0x80);
            b = b3;
          } 
        } else {
          b2 = arrayOfByte1[1];
        } 
        arrayOfByte[3 + b1] = b2;
        b1++;
      } catch (UnsupportedEncodingException unsupportedEncodingException) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("encodeTo0x81() : unsurport encoding of ");
        stringBuilder.append(str);
        Log.e("GSM", stringBuilder.toString(), unsupportedEncodingException);
        return null;
      } 
    } 
    return (byte[])stringBuilder;
  }
}
