package com.android.internal.telephony;

import android.app.PendingIntent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IOplusTelephonyExt extends IInterface {
  void eventDataActionAutoPlmn() throws RemoteException;
  
  void eventDataActionRetryPdn() throws RemoteException;
  
  void eventDataCheckDns() throws RemoteException;
  
  void eventDataCheckHttp() throws RemoteException;
  
  void eventDataCheckPdn() throws RemoteException;
  
  long getActionExecuteTime(int paramInt1, int paramInt2) throws RemoteException;
  
  int getCardType(int paramInt) throws RemoteException;
  
  int getLastAction(int paramInt) throws RemoteException;
  
  String[] getLteCdmaImsi(int paramInt) throws RemoteException;
  
  String getOperatorNumericForData(int paramInt) throws RemoteException;
  
  int getSoftSimCardSlotId() throws RemoteException;
  
  boolean ishVoLTESupport() throws RemoteException;
  
  void registerCallback(String paramString, IOplusTelephonyExtCallback paramIOplusTelephonyExtCallback) throws RemoteException;
  
  void reportGameEnterOrLeave(int paramInt, String paramString, boolean paramBoolean) throws RemoteException;
  
  void reportNetWorkLatency(String paramString) throws RemoteException;
  
  void reportNetWorkLevel(String paramString) throws RemoteException;
  
  Bundle requestForTelephonyEvent(int paramInt1, int paramInt2, Bundle paramBundle) throws RemoteException;
  
  void sendMultipartTextForSubscriberWithOptionsOem(int paramInt1, String paramString1, String paramString2, String paramString3, List<String> paramList, List<PendingIntent> paramList1, List<PendingIntent> paramList2, boolean paramBoolean1, int paramInt2, boolean paramBoolean2, int paramInt3, int paramInt4) throws RemoteException;
  
  void sendRecoveryRequest(int paramInt1, int paramInt2) throws RemoteException;
  
  void sendTextForSubscriberWithOptionsOem(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, boolean paramBoolean1, int paramInt2, boolean paramBoolean2, int paramInt3, int paramInt4) throws RemoteException;
  
  int setDisplayNumberExt(String paramString, int paramInt) throws RemoteException;
  
  void startMobileDataHongbaoPolicy(int paramInt1, int paramInt2, String paramString1, String paramString2) throws RemoteException;
  
  void unRegisterCallback(IOplusTelephonyExtCallback paramIOplusTelephonyExtCallback) throws RemoteException;
  
  class Default implements IOplusTelephonyExt {
    public Bundle requestForTelephonyEvent(int param1Int1, int param1Int2, Bundle param1Bundle) throws RemoteException {
      return null;
    }
    
    public void registerCallback(String param1String, IOplusTelephonyExtCallback param1IOplusTelephonyExtCallback) throws RemoteException {}
    
    public void unRegisterCallback(IOplusTelephonyExtCallback param1IOplusTelephonyExtCallback) throws RemoteException {}
    
    public int getSoftSimCardSlotId() throws RemoteException {
      return 0;
    }
    
    public String getOperatorNumericForData(int param1Int) throws RemoteException {
      return null;
    }
    
    public void startMobileDataHongbaoPolicy(int param1Int1, int param1Int2, String param1String1, String param1String2) throws RemoteException {}
    
    public void sendTextForSubscriberWithOptionsOem(int param1Int1, String param1String1, String param1String2, String param1String3, String param1String4, PendingIntent param1PendingIntent1, PendingIntent param1PendingIntent2, boolean param1Boolean1, int param1Int2, boolean param1Boolean2, int param1Int3, int param1Int4) throws RemoteException {}
    
    public void sendMultipartTextForSubscriberWithOptionsOem(int param1Int1, String param1String1, String param1String2, String param1String3, List<String> param1List, List<PendingIntent> param1List1, List<PendingIntent> param1List2, boolean param1Boolean1, int param1Int2, boolean param1Boolean2, int param1Int3, int param1Int4) throws RemoteException {}
    
    public int getCardType(int param1Int) throws RemoteException {
      return 0;
    }
    
    public void eventDataCheckDns() throws RemoteException {}
    
    public void eventDataCheckHttp() throws RemoteException {}
    
    public void eventDataCheckPdn() throws RemoteException {}
    
    public void eventDataActionAutoPlmn() throws RemoteException {}
    
    public void eventDataActionRetryPdn() throws RemoteException {}
    
    public boolean ishVoLTESupport() throws RemoteException {
      return false;
    }
    
    public void sendRecoveryRequest(int param1Int1, int param1Int2) throws RemoteException {}
    
    public long getActionExecuteTime(int param1Int1, int param1Int2) throws RemoteException {
      return 0L;
    }
    
    public int getLastAction(int param1Int) throws RemoteException {
      return 0;
    }
    
    public void reportNetWorkLatency(String param1String) throws RemoteException {}
    
    public void reportNetWorkLevel(String param1String) throws RemoteException {}
    
    public void reportGameEnterOrLeave(int param1Int, String param1String, boolean param1Boolean) throws RemoteException {}
    
    public int setDisplayNumberExt(String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public String[] getLteCdmaImsi(int param1Int) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusTelephonyExt {
    private static final String DESCRIPTOR = "com.android.internal.telephony.IOplusTelephonyExt";
    
    static final int TRANSACTION_eventDataActionAutoPlmn = 14;
    
    static final int TRANSACTION_eventDataActionRetryPdn = 15;
    
    static final int TRANSACTION_eventDataCheckDns = 11;
    
    static final int TRANSACTION_eventDataCheckHttp = 12;
    
    static final int TRANSACTION_eventDataCheckPdn = 13;
    
    static final int TRANSACTION_getActionExecuteTime = 18;
    
    static final int TRANSACTION_getCardType = 10;
    
    static final int TRANSACTION_getLastAction = 19;
    
    static final int TRANSACTION_getLteCdmaImsi = 24;
    
    static final int TRANSACTION_getOperatorNumericForData = 6;
    
    static final int TRANSACTION_getSoftSimCardSlotId = 5;
    
    static final int TRANSACTION_ishVoLTESupport = 16;
    
    static final int TRANSACTION_registerCallback = 3;
    
    static final int TRANSACTION_reportGameEnterOrLeave = 22;
    
    static final int TRANSACTION_reportNetWorkLatency = 20;
    
    static final int TRANSACTION_reportNetWorkLevel = 21;
    
    static final int TRANSACTION_requestForTelephonyEvent = 2;
    
    static final int TRANSACTION_sendMultipartTextForSubscriberWithOptionsOem = 9;
    
    static final int TRANSACTION_sendRecoveryRequest = 17;
    
    static final int TRANSACTION_sendTextForSubscriberWithOptionsOem = 8;
    
    static final int TRANSACTION_setDisplayNumberExt = 23;
    
    static final int TRANSACTION_startMobileDataHongbaoPolicy = 7;
    
    static final int TRANSACTION_unRegisterCallback = 4;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.IOplusTelephonyExt");
    }
    
    public static IOplusTelephonyExt asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.IOplusTelephonyExt");
      if (iInterface != null && iInterface instanceof IOplusTelephonyExt)
        return (IOplusTelephonyExt)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 24:
          return "getLteCdmaImsi";
        case 23:
          return "setDisplayNumberExt";
        case 22:
          return "reportGameEnterOrLeave";
        case 21:
          return "reportNetWorkLevel";
        case 20:
          return "reportNetWorkLatency";
        case 19:
          return "getLastAction";
        case 18:
          return "getActionExecuteTime";
        case 17:
          return "sendRecoveryRequest";
        case 16:
          return "ishVoLTESupport";
        case 15:
          return "eventDataActionRetryPdn";
        case 14:
          return "eventDataActionAutoPlmn";
        case 13:
          return "eventDataCheckPdn";
        case 12:
          return "eventDataCheckHttp";
        case 11:
          return "eventDataCheckDns";
        case 10:
          return "getCardType";
        case 9:
          return "sendMultipartTextForSubscriberWithOptionsOem";
        case 8:
          return "sendTextForSubscriberWithOptionsOem";
        case 7:
          return "startMobileDataHongbaoPolicy";
        case 6:
          return "getOperatorNumericForData";
        case 5:
          return "getSoftSimCardSlotId";
        case 4:
          return "unRegisterCallback";
        case 3:
          return "registerCallback";
        case 2:
          break;
      } 
      return "requestForTelephonyEvent";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        String arrayOfString[], str1;
        IOplusTelephonyExtCallback iOplusTelephonyExtCallback;
        String str3;
        ArrayList<PendingIntent> arrayList1;
        String str2;
        long l;
        String str4, str5, str6;
        ArrayList<String> arrayList;
        String str7;
        ArrayList<PendingIntent> arrayList2;
        String str8;
        boolean bool2;
        int j, k;
        boolean bool1 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 24:
            param1Parcel1.enforceInterface("com.android.internal.telephony.IOplusTelephonyExt");
            param1Int1 = param1Parcel1.readInt();
            arrayOfString = getLteCdmaImsi(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString);
            return true;
          case 23:
            arrayOfString.enforceInterface("com.android.internal.telephony.IOplusTelephonyExt");
            str3 = arrayOfString.readString();
            param1Int1 = arrayOfString.readInt();
            param1Int1 = setDisplayNumberExt(str3, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 22:
            arrayOfString.enforceInterface("com.android.internal.telephony.IOplusTelephonyExt");
            param1Int1 = arrayOfString.readInt();
            str3 = arrayOfString.readString();
            if (arrayOfString.readInt() != 0)
              bool1 = true; 
            reportGameEnterOrLeave(param1Int1, str3, bool1);
            param1Parcel2.writeNoException();
            return true;
          case 21:
            arrayOfString.enforceInterface("com.android.internal.telephony.IOplusTelephonyExt");
            str1 = arrayOfString.readString();
            reportNetWorkLevel(str1);
            param1Parcel2.writeNoException();
            return true;
          case 20:
            str1.enforceInterface("com.android.internal.telephony.IOplusTelephonyExt");
            str1 = str1.readString();
            reportNetWorkLatency(str1);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            str1.enforceInterface("com.android.internal.telephony.IOplusTelephonyExt");
            param1Int1 = str1.readInt();
            param1Int1 = getLastAction(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 18:
            str1.enforceInterface("com.android.internal.telephony.IOplusTelephonyExt");
            param1Int1 = str1.readInt();
            param1Int2 = str1.readInt();
            l = getActionExecuteTime(param1Int1, param1Int2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 17:
            str1.enforceInterface("com.android.internal.telephony.IOplusTelephonyExt");
            param1Int1 = str1.readInt();
            param1Int2 = str1.readInt();
            sendRecoveryRequest(param1Int1, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 16:
            str1.enforceInterface("com.android.internal.telephony.IOplusTelephonyExt");
            bool = ishVoLTESupport();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 15:
            str1.enforceInterface("com.android.internal.telephony.IOplusTelephonyExt");
            eventDataActionRetryPdn();
            param1Parcel2.writeNoException();
            return true;
          case 14:
            str1.enforceInterface("com.android.internal.telephony.IOplusTelephonyExt");
            eventDataActionAutoPlmn();
            param1Parcel2.writeNoException();
            return true;
          case 13:
            str1.enforceInterface("com.android.internal.telephony.IOplusTelephonyExt");
            eventDataCheckPdn();
            param1Parcel2.writeNoException();
            return true;
          case 12:
            str1.enforceInterface("com.android.internal.telephony.IOplusTelephonyExt");
            eventDataCheckHttp();
            param1Parcel2.writeNoException();
            return true;
          case 11:
            str1.enforceInterface("com.android.internal.telephony.IOplusTelephonyExt");
            eventDataCheckDns();
            param1Parcel2.writeNoException();
            return true;
          case 10:
            str1.enforceInterface("com.android.internal.telephony.IOplusTelephonyExt");
            i = str1.readInt();
            i = getCardType(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 9:
            str1.enforceInterface("com.android.internal.telephony.IOplusTelephonyExt");
            param1Int2 = str1.readInt();
            str4 = str1.readString();
            str5 = str1.readString();
            str6 = str1.readString();
            arrayList = str1.createStringArrayList();
            arrayList2 = str1.createTypedArrayList(PendingIntent.CREATOR);
            arrayList1 = str1.createTypedArrayList(PendingIntent.CREATOR);
            if (str1.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            i = str1.readInt();
            if (str1.readInt() != 0) {
              bool2 = true;
            } else {
              bool2 = false;
            } 
            j = str1.readInt();
            k = str1.readInt();
            sendMultipartTextForSubscriberWithOptionsOem(param1Int2, str4, str5, str6, arrayList, arrayList2, arrayList1, bool1, i, bool2, j, k);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            str1.enforceInterface("com.android.internal.telephony.IOplusTelephonyExt");
            i = str1.readInt();
            str7 = str1.readString();
            str6 = str1.readString();
            str8 = str1.readString();
            str5 = str1.readString();
            if (str1.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)str1);
            } else {
              arrayList1 = null;
            } 
            if (str1.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str4 = null;
            } 
            if (str1.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            k = str1.readInt();
            if (str1.readInt() != 0) {
              bool2 = true;
            } else {
              bool2 = false;
            } 
            j = str1.readInt();
            param1Int2 = str1.readInt();
            sendTextForSubscriberWithOptionsOem(i, str7, str6, str8, str5, (PendingIntent)arrayList1, (PendingIntent)str4, bool1, k, bool2, j, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            str1.enforceInterface("com.android.internal.telephony.IOplusTelephonyExt");
            param1Int2 = str1.readInt();
            i = str1.readInt();
            str2 = str1.readString();
            str1 = str1.readString();
            startMobileDataHongbaoPolicy(param1Int2, i, str2, str1);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            str1.enforceInterface("com.android.internal.telephony.IOplusTelephonyExt");
            i = str1.readInt();
            str1 = getOperatorNumericForData(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 5:
            str1.enforceInterface("com.android.internal.telephony.IOplusTelephonyExt");
            i = getSoftSimCardSlotId();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 4:
            str1.enforceInterface("com.android.internal.telephony.IOplusTelephonyExt");
            iOplusTelephonyExtCallback = IOplusTelephonyExtCallback.Stub.asInterface(str1.readStrongBinder());
            unRegisterCallback(iOplusTelephonyExtCallback);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            iOplusTelephonyExtCallback.enforceInterface("com.android.internal.telephony.IOplusTelephonyExt");
            str2 = iOplusTelephonyExtCallback.readString();
            iOplusTelephonyExtCallback = IOplusTelephonyExtCallback.Stub.asInterface(iOplusTelephonyExtCallback.readStrongBinder());
            registerCallback(str2, iOplusTelephonyExtCallback);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            break;
        } 
        iOplusTelephonyExtCallback.enforceInterface("com.android.internal.telephony.IOplusTelephonyExt");
        param1Int2 = iOplusTelephonyExtCallback.readInt();
        int i = iOplusTelephonyExtCallback.readInt();
        if (iOplusTelephonyExtCallback.readInt() != 0) {
          Bundle bundle1 = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iOplusTelephonyExtCallback);
        } else {
          iOplusTelephonyExtCallback = null;
        } 
        Bundle bundle = requestForTelephonyEvent(param1Int2, i, (Bundle)iOplusTelephonyExtCallback);
        param1Parcel2.writeNoException();
        if (bundle != null) {
          param1Parcel2.writeInt(1);
          bundle.writeToParcel(param1Parcel2, 1);
        } else {
          param1Parcel2.writeInt(0);
        } 
        return true;
      } 
      param1Parcel2.writeString("com.android.internal.telephony.IOplusTelephonyExt");
      return true;
    }
    
    private static class Proxy implements IOplusTelephonyExt {
      public static IOplusTelephonyExt sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.IOplusTelephonyExt";
      }
      
      public Bundle requestForTelephonyEvent(int param2Int1, int param2Int2, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOplusTelephonyExt");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOplusTelephonyExt.Stub.getDefaultImpl() != null) {
            param2Bundle = IOplusTelephonyExt.Stub.getDefaultImpl().requestForTelephonyEvent(param2Int1, param2Int2, param2Bundle);
            return param2Bundle;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            param2Bundle = (Bundle)Bundle.CREATOR.createFromParcel(parcel2);
          } else {
            param2Bundle = null;
          } 
          return param2Bundle;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerCallback(String param2String, IOplusTelephonyExtCallback param2IOplusTelephonyExtCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOplusTelephonyExt");
          parcel1.writeString(param2String);
          if (param2IOplusTelephonyExtCallback != null) {
            iBinder = param2IOplusTelephonyExtCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IOplusTelephonyExt.Stub.getDefaultImpl() != null) {
            IOplusTelephonyExt.Stub.getDefaultImpl().registerCallback(param2String, param2IOplusTelephonyExtCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unRegisterCallback(IOplusTelephonyExtCallback param2IOplusTelephonyExtCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOplusTelephonyExt");
          if (param2IOplusTelephonyExtCallback != null) {
            iBinder = param2IOplusTelephonyExtCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IOplusTelephonyExt.Stub.getDefaultImpl() != null) {
            IOplusTelephonyExt.Stub.getDefaultImpl().unRegisterCallback(param2IOplusTelephonyExtCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getSoftSimCardSlotId() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOplusTelephonyExt");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IOplusTelephonyExt.Stub.getDefaultImpl() != null)
            return IOplusTelephonyExt.Stub.getDefaultImpl().getSoftSimCardSlotId(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getOperatorNumericForData(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOplusTelephonyExt");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IOplusTelephonyExt.Stub.getDefaultImpl() != null)
            return IOplusTelephonyExt.Stub.getDefaultImpl().getOperatorNumericForData(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startMobileDataHongbaoPolicy(int param2Int1, int param2Int2, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOplusTelephonyExt");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IOplusTelephonyExt.Stub.getDefaultImpl() != null) {
            IOplusTelephonyExt.Stub.getDefaultImpl().startMobileDataHongbaoPolicy(param2Int1, param2Int2, param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendTextForSubscriberWithOptionsOem(int param2Int1, String param2String1, String param2String2, String param2String3, String param2String4, PendingIntent param2PendingIntent1, PendingIntent param2PendingIntent2, boolean param2Boolean1, int param2Int2, boolean param2Boolean2, int param2Int3, int param2Int4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOplusTelephonyExt");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          parcel1.writeString(param2String4);
          boolean bool1 = true;
          if (param2PendingIntent1 != null) {
            try {
              parcel1.writeInt(1);
              param2PendingIntent1.writeToParcel(parcel1, 0);
            } finally {}
          } else {
            parcel1.writeInt(0);
          } 
          if (param2PendingIntent2 != null) {
            parcel1.writeInt(1);
            param2PendingIntent2.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeInt(param2Int2);
          if (param2Boolean2) {
            bool2 = bool1;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeInt(param2Int3);
          parcel1.writeInt(param2Int4);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IOplusTelephonyExt.Stub.getDefaultImpl() != null) {
            IOplusTelephonyExt iOplusTelephonyExt = IOplusTelephonyExt.Stub.getDefaultImpl();
            try {
              iOplusTelephonyExt.sendTextForSubscriberWithOptionsOem(param2Int1, param2String1, param2String2, param2String3, param2String4, param2PendingIntent1, param2PendingIntent2, param2Boolean1, param2Int2, param2Boolean2, param2Int3, param2Int4);
              parcel2.recycle();
              parcel1.recycle();
              return;
            } finally {}
          } else {
            parcel2.readException();
            parcel2.recycle();
            parcel1.recycle();
            return;
          } 
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void sendMultipartTextForSubscriberWithOptionsOem(int param2Int1, String param2String1, String param2String2, String param2String3, List<String> param2List, List<PendingIntent> param2List1, List<PendingIntent> param2List2, boolean param2Boolean1, int param2Int2, boolean param2Boolean2, int param2Int3, int param2Int4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOplusTelephonyExt");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          parcel1.writeStringList(param2List);
          parcel1.writeTypedList(param2List1);
          parcel1.writeTypedList(param2List2);
          boolean bool1 = true;
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeInt(param2Int2);
          if (param2Boolean2) {
            bool2 = bool1;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeInt(param2Int3);
          parcel1.writeInt(param2Int4);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IOplusTelephonyExt.Stub.getDefaultImpl() != null) {
            IOplusTelephonyExt.Stub.getDefaultImpl().sendMultipartTextForSubscriberWithOptionsOem(param2Int1, param2String1, param2String2, param2String3, param2List, param2List1, param2List2, param2Boolean1, param2Int2, param2Boolean2, param2Int3, param2Int4);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getCardType(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOplusTelephonyExt");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IOplusTelephonyExt.Stub.getDefaultImpl() != null) {
            param2Int = IOplusTelephonyExt.Stub.getDefaultImpl().getCardType(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void eventDataCheckDns() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOplusTelephonyExt");
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IOplusTelephonyExt.Stub.getDefaultImpl() != null) {
            IOplusTelephonyExt.Stub.getDefaultImpl().eventDataCheckDns();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void eventDataCheckHttp() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOplusTelephonyExt");
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IOplusTelephonyExt.Stub.getDefaultImpl() != null) {
            IOplusTelephonyExt.Stub.getDefaultImpl().eventDataCheckHttp();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void eventDataCheckPdn() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOplusTelephonyExt");
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IOplusTelephonyExt.Stub.getDefaultImpl() != null) {
            IOplusTelephonyExt.Stub.getDefaultImpl().eventDataCheckPdn();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void eventDataActionAutoPlmn() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOplusTelephonyExt");
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IOplusTelephonyExt.Stub.getDefaultImpl() != null) {
            IOplusTelephonyExt.Stub.getDefaultImpl().eventDataActionAutoPlmn();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void eventDataActionRetryPdn() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOplusTelephonyExt");
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IOplusTelephonyExt.Stub.getDefaultImpl() != null) {
            IOplusTelephonyExt.Stub.getDefaultImpl().eventDataActionRetryPdn();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean ishVoLTESupport() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOplusTelephonyExt");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(16, parcel1, parcel2, 0);
          if (!bool2 && IOplusTelephonyExt.Stub.getDefaultImpl() != null) {
            bool1 = IOplusTelephonyExt.Stub.getDefaultImpl().ishVoLTESupport();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendRecoveryRequest(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOplusTelephonyExt");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IOplusTelephonyExt.Stub.getDefaultImpl() != null) {
            IOplusTelephonyExt.Stub.getDefaultImpl().sendRecoveryRequest(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getActionExecuteTime(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOplusTelephonyExt");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IOplusTelephonyExt.Stub.getDefaultImpl() != null)
            return IOplusTelephonyExt.Stub.getDefaultImpl().getActionExecuteTime(param2Int1, param2Int2); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getLastAction(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOplusTelephonyExt");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IOplusTelephonyExt.Stub.getDefaultImpl() != null) {
            param2Int = IOplusTelephonyExt.Stub.getDefaultImpl().getLastAction(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportNetWorkLatency(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOplusTelephonyExt");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IOplusTelephonyExt.Stub.getDefaultImpl() != null) {
            IOplusTelephonyExt.Stub.getDefaultImpl().reportNetWorkLatency(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportNetWorkLevel(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOplusTelephonyExt");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IOplusTelephonyExt.Stub.getDefaultImpl() != null) {
            IOplusTelephonyExt.Stub.getDefaultImpl().reportNetWorkLevel(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportGameEnterOrLeave(int param2Int, String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOplusTelephonyExt");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool1 && IOplusTelephonyExt.Stub.getDefaultImpl() != null) {
            IOplusTelephonyExt.Stub.getDefaultImpl().reportGameEnterOrLeave(param2Int, param2String, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setDisplayNumberExt(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOplusTelephonyExt");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IOplusTelephonyExt.Stub.getDefaultImpl() != null) {
            param2Int = IOplusTelephonyExt.Stub.getDefaultImpl().setDisplayNumberExt(param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getLteCdmaImsi(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOplusTelephonyExt");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IOplusTelephonyExt.Stub.getDefaultImpl() != null)
            return IOplusTelephonyExt.Stub.getDefaultImpl().getLteCdmaImsi(param2Int); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusTelephonyExt param1IOplusTelephonyExt) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusTelephonyExt != null) {
          Proxy.sDefaultImpl = param1IOplusTelephonyExt;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusTelephonyExt getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
