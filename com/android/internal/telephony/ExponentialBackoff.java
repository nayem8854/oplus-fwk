package com.android.internal.telephony;

import android.os.Handler;
import android.os.Looper;

public class ExponentialBackoff {
  private long mCurrentDelayMs;
  
  private final Handler mHandler;
  
  private HandlerAdapter mHandlerAdapter = (HandlerAdapter)new Object(this);
  
  private long mMaximumDelayMs;
  
  private int mMultiplier;
  
  private int mRetryCounter;
  
  private final Runnable mRunnable;
  
  private long mStartDelayMs;
  
  public ExponentialBackoff(long paramLong1, long paramLong2, int paramInt, Looper paramLooper, Runnable paramRunnable) {
    this(paramLong1, paramLong2, paramInt, new Handler(paramLooper), paramRunnable);
  }
  
  public ExponentialBackoff(long paramLong1, long paramLong2, int paramInt, Handler paramHandler, Runnable paramRunnable) {
    this.mRetryCounter = 0;
    this.mStartDelayMs = paramLong1;
    this.mMaximumDelayMs = paramLong2;
    this.mMultiplier = paramInt;
    this.mHandler = paramHandler;
    this.mRunnable = paramRunnable;
  }
  
  public void start() {
    this.mRetryCounter = 0;
    this.mCurrentDelayMs = this.mStartDelayMs;
    this.mHandlerAdapter.removeCallbacks(this.mRunnable);
    this.mHandlerAdapter.postDelayed(this.mRunnable, this.mCurrentDelayMs);
  }
  
  public void stop() {
    this.mRetryCounter = 0;
    this.mHandlerAdapter.removeCallbacks(this.mRunnable);
  }
  
  public void notifyFailed() {
    int i = this.mRetryCounter + 1;
    long l1 = this.mMaximumDelayMs;
    double d1 = this.mStartDelayMs, d2 = this.mMultiplier, d3 = i;
    long l2 = (long)(d1 * Math.pow(d2, d3));
    l2 = Math.min(l1, l2);
    this.mCurrentDelayMs = (long)((Math.random() + 1.0D) / 2.0D * l2);
    this.mHandlerAdapter.removeCallbacks(this.mRunnable);
    this.mHandlerAdapter.postDelayed(this.mRunnable, this.mCurrentDelayMs);
  }
  
  public long getCurrentDelay() {
    return this.mCurrentDelayMs;
  }
  
  public void setHandlerAdapter(HandlerAdapter paramHandlerAdapter) {
    this.mHandlerAdapter = paramHandlerAdapter;
  }
  
  public static interface HandlerAdapter {
    boolean postDelayed(Runnable param1Runnable, long param1Long);
    
    void removeCallbacks(Runnable param1Runnable);
  }
}
