package com.android.internal.telephony;

import android.os.RemoteException;
import com.oplus.content.OplusFeatureConfigManager;
import java.util.List;

public class OplusFeatureHelper {
  private static OplusFeatureHelper sInstance = null;
  
  OplusFeatureConfigManager mManager = null;
  
  public static OplusFeatureHelper getInstance() {
    // Byte code:
    //   0: ldc com/android/internal/telephony/OplusFeatureHelper
    //   2: monitorenter
    //   3: getstatic com/android/internal/telephony/OplusFeatureHelper.sInstance : Lcom/android/internal/telephony/OplusFeatureHelper;
    //   6: ifnonnull -> 21
    //   9: new com/android/internal/telephony/OplusFeatureHelper
    //   12: astore_0
    //   13: aload_0
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: putstatic com/android/internal/telephony/OplusFeatureHelper.sInstance : Lcom/android/internal/telephony/OplusFeatureHelper;
    //   21: getstatic com/android/internal/telephony/OplusFeatureHelper.sInstance : Lcom/android/internal/telephony/OplusFeatureHelper;
    //   24: astore_0
    //   25: ldc com/android/internal/telephony/OplusFeatureHelper
    //   27: monitorexit
    //   28: aload_0
    //   29: areturn
    //   30: astore_0
    //   31: ldc com/android/internal/telephony/OplusFeatureHelper
    //   33: monitorexit
    //   34: aload_0
    //   35: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #17	-> 0
    //   #18	-> 3
    //   #19	-> 9
    //   #21	-> 21
    //   #22	-> 30
    // Exception table:
    //   from	to	target	type
    //   3	9	30	finally
    //   9	21	30	finally
    //   21	28	30	finally
    //   31	34	30	finally
  }
  
  private OplusFeatureHelper() {
    this.mManager = OplusFeatureConfigManager.getInstance();
  }
  
  public boolean hasFeature(String paramString) {
    return this.mManager.hasFeature(paramString);
  }
  
  public boolean enableFeature(String paramString) {
    try {
      return this.mManager.enableFeature(paramString);
    } catch (RemoteException remoteException) {
      remoteException.printStackTrace();
      return false;
    } 
  }
  
  public boolean disableFeature(String paramString) {
    try {
      return this.mManager.disableFeature(paramString);
    } catch (RemoteException remoteException) {
      remoteException.printStackTrace();
      return false;
    } 
  }
  
  public void notifyFeaturesUpdate(String paramString1, String paramString2) {
    try {
      this.mManager.notifyFeaturesUpdate(paramString1, paramString2);
    } catch (RemoteException remoteException) {
      remoteException.printStackTrace();
    } 
  }
  
  public boolean registerFeatureObserver(List<String> paramList, FeatureObserver paramFeatureObserver) {
    return this.mManager.registerFeatureObserver(paramList, paramFeatureObserver);
  }
  
  public boolean unregisterFeatureObserver(FeatureObserver paramFeatureObserver) {
    return this.mManager.unregisterFeatureObserver(paramFeatureObserver);
  }
  
  class FeatureObserver implements OplusFeatureConfigManager.OnFeatureObserver {
    final OplusFeatureHelper this$0;
    
    public void onFeatureUpdate(List<String> param1List) {}
  }
}
