package com.android.internal.telephony.util;

import android.os.Handler;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;

public class HandlerExecutor implements Executor {
  private final Handler mHandler;
  
  public HandlerExecutor(Handler paramHandler) {
    if (paramHandler != null) {
      this.mHandler = paramHandler;
      return;
    } 
    throw null;
  }
  
  public void execute(Runnable paramRunnable) {
    if (this.mHandler.post(paramRunnable))
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(this.mHandler);
    stringBuilder.append(" is shutting down");
    throw new RejectedExecutionException(stringBuilder.toString());
  }
}
