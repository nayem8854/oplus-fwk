package com.android.internal.telephony.util;

import android.content.Context;
import android.content.pm.ComponentInfo;
import android.content.pm.ResolveInfo;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.SystemProperties;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public final class TelephonyUtils {
  public static boolean IS_DEBUGGABLE;
  
  public static boolean IS_USER = "user".equals(Build.TYPE);
  
  static {
    boolean bool = false;
    if (SystemProperties.getInt("ro.debuggable", 0) == 1)
      bool = true; 
    IS_DEBUGGABLE = bool;
  }
  
  public static boolean checkDumpPermission(Context paramContext, String paramString, PrintWriter paramPrintWriter) {
    if (paramContext.checkCallingOrSelfPermission("android.permission.DUMP") != 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Permission Denial: can't dump ");
      stringBuilder.append(paramString);
      stringBuilder.append(" from from pid=");
      stringBuilder.append(Binder.getCallingPid());
      stringBuilder.append(", uid=");
      stringBuilder.append(Binder.getCallingUid());
      stringBuilder.append(" due to missing android.permission.DUMP permission");
      String str = stringBuilder.toString();
      paramPrintWriter.println(str);
      return false;
    } 
    return true;
  }
  
  public static String emptyIfNull(String paramString) {
    if (paramString == null)
      paramString = ""; 
    return paramString;
  }
  
  public static <T> List<T> emptyIfNull(List<T> paramList) {
    if (paramList == null)
      paramList = Collections.emptyList(); 
    return paramList;
  }
  
  public static RuntimeException rethrowAsRuntimeException(RemoteException paramRemoteException) {
    throw new RuntimeException(paramRemoteException);
  }
  
  public static ComponentInfo getComponentInfo(ResolveInfo paramResolveInfo) {
    if (paramResolveInfo.activityInfo != null)
      return (ComponentInfo)paramResolveInfo.activityInfo; 
    if (paramResolveInfo.serviceInfo != null)
      return (ComponentInfo)paramResolveInfo.serviceInfo; 
    if (paramResolveInfo.providerInfo != null)
      return (ComponentInfo)paramResolveInfo.providerInfo; 
    throw new IllegalStateException("Missing ComponentInfo!");
  }
  
  public static void runWithCleanCallingIdentity(Runnable paramRunnable) {
    long l = Binder.clearCallingIdentity();
    try {
      paramRunnable.run();
      return;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public static <T> T runWithCleanCallingIdentity(Supplier<T> paramSupplier) {
    long l = Binder.clearCallingIdentity();
    try {
      paramSupplier = (Supplier<T>)paramSupplier.get();
      return (T)paramSupplier;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public static Bundle filterValues(Bundle paramBundle) {
    Bundle bundle = new Bundle(paramBundle);
    for (String str : paramBundle.keySet()) {
      Object object = paramBundle.get(str);
      if (object instanceof Integer || object instanceof Long || object instanceof Double || object instanceof String || object instanceof int[] || object instanceof long[] || object instanceof double[] || object instanceof String[] || object instanceof android.os.PersistableBundle || object == null || object instanceof Boolean || object instanceof boolean[])
        continue; 
      if (object instanceof Bundle) {
        bundle.putBundle(str, filterValues((Bundle)object));
        continue;
      } 
      if (object.getClass().getName().startsWith("android."))
        continue; 
      bundle.remove(str);
    } 
    return bundle;
  }
  
  public static void waitUntilReady(CountDownLatch paramCountDownLatch, long paramLong) {
    try {
      paramCountDownLatch.await(paramLong, TimeUnit.MILLISECONDS);
    } catch (InterruptedException interruptedException) {}
  }
}
