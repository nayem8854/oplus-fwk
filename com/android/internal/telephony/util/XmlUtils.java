package com.android.internal.telephony.util;

import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public final class XmlUtils {
  public static void beginDocument(XmlPullParser paramXmlPullParser, String paramString) throws XmlPullParserException, IOException {
    int i;
    while (true) {
      i = paramXmlPullParser.next();
      if (i != 2 && i != 1)
        continue; 
      break;
    } 
    if (i == 2) {
      if (paramXmlPullParser.getName().equals(paramString))
        return; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unexpected start tag: found ");
      stringBuilder.append(paramXmlPullParser.getName());
      stringBuilder.append(", expected ");
      stringBuilder.append(paramString);
      throw new XmlPullParserException(stringBuilder.toString());
    } 
    throw new XmlPullParserException("No start tag found");
  }
  
  public static void nextElement(XmlPullParser paramXmlPullParser) throws XmlPullParserException, IOException {
    while (true) {
      int i = paramXmlPullParser.next();
      if (i != 2 && i != 1)
        continue; 
      break;
    } 
  }
  
  public static boolean nextElementWithin(XmlPullParser paramXmlPullParser, int paramInt) throws IOException, XmlPullParserException {
    while (true) {
      int i = paramXmlPullParser.next();
      if (i == 1 || (i == 3 && 
        paramXmlPullParser.getDepth() == paramInt))
        break; 
      if (i == 2 && paramXmlPullParser.getDepth() == paramInt + 1)
        return true; 
    } 
    return false;
  }
}
