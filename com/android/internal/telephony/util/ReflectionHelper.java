package com.android.internal.telephony.util;

import android.os.SystemProperties;
import android.telephony.Rlog;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ReflectionHelper {
  private static final boolean SWITCH_LOG = "true".equalsIgnoreCase(SystemProperties.get("persist.sys.assert.panic", "false"));
  
  private static final String TAG = "OplusTelephonyUtils";
  
  public static <T> T typeCasting(Class<T> paramClass, Object paramObject) {
    if (paramObject != null && paramClass.isInstance(paramObject))
      return (T)paramObject; 
    return null;
  }
  
  public static Object callMethod(Object paramObject, String paramString1, String paramString2, Class[] paramArrayOfClass, Object[] paramArrayOfObject) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramObject);
    stringBuilder.append(" callMethod : ");
    stringBuilder.append(paramString1);
    stringBuilder.append(".");
    stringBuilder.append(paramString2);
    log(stringBuilder.toString());
    stringBuilder = null;
    try {
      Class<?> clazz = Class.forName(paramString1);
      Method method = clazz.getMethod(paramString2, paramArrayOfClass);
      method.setAccessible(true);
      paramObject = method.invoke(paramObject, paramArrayOfObject);
    } catch (Exception exception) {
      paramObject = new StringBuilder();
      paramObject.append("callDeclaredMethod exception caught : ");
      paramObject.append(exception.getMessage());
      log(paramObject.toString());
      exception.printStackTrace();
      paramObject = stringBuilder;
    } 
    return paramObject;
  }
  
  public static Object callMethodOrThrow(Object paramObject, String paramString1, String paramString2, Class[] paramArrayOfClass, Object[] paramArrayOfObject) {
    StringBuilder stringBuilder1, stringBuilder2 = new StringBuilder();
    stringBuilder2.append(paramObject);
    stringBuilder2.append(" callMethodOrThrow : ");
    stringBuilder2.append(paramString1);
    stringBuilder2.append(".");
    stringBuilder2.append(paramString2);
    log(stringBuilder2.toString());
    stringBuilder2 = null;
    try {
      Class<?> clazz = Class.forName(paramString1);
      Method method = clazz.getMethod(paramString2, paramArrayOfClass);
      method.setAccessible(true);
      paramObject = method.invoke(paramObject, paramArrayOfObject);
    } catch (Exception exception) {
      if (!SWITCH_LOG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("callDeclaredMethod exception caught : ");
        stringBuilder.append(exception.getMessage());
        log(stringBuilder.toString());
        exception.printStackTrace();
        stringBuilder1 = stringBuilder2;
        return stringBuilder1;
      } 
    } 
    return stringBuilder1;
  }
  
  public static Object callDeclaredMethod(Object paramObject, String paramString1, String paramString2, Class[] paramArrayOfClass, Object[] paramArrayOfObject) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramObject);
    stringBuilder.append(" callDeclaredMethod : ");
    stringBuilder.append(paramString1);
    stringBuilder.append(".");
    stringBuilder.append(paramString2);
    log(stringBuilder.toString());
    stringBuilder = null;
    try {
      Class<?> clazz = Class.forName(paramString1);
      Method method = clazz.getDeclaredMethod(paramString2, paramArrayOfClass);
      method.setAccessible(true);
      paramObject = method.invoke(paramObject, paramArrayOfObject);
    } catch (Exception exception) {
      paramObject = new StringBuilder();
      paramObject.append("callDeclaredMethod exception caught : ");
      paramObject.append(exception.getMessage());
      log(paramObject.toString());
      exception.printStackTrace();
      paramObject = stringBuilder;
    } 
    return paramObject;
  }
  
  public static Object callDeclaredMethodOrThrow(Object paramObject, String paramString1, String paramString2, Class[] paramArrayOfClass, Object[] paramArrayOfObject) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramObject);
    stringBuilder.append(" callDeclaredMethodOrThrow : ");
    stringBuilder.append(paramString1);
    stringBuilder.append(".");
    stringBuilder.append(paramString2);
    log(stringBuilder.toString());
    stringBuilder = null;
    try {
      Class<?> clazz = Class.forName(paramString1);
      Method method = clazz.getDeclaredMethod(paramString2, paramArrayOfClass);
      method.setAccessible(true);
      paramObject = method.invoke(paramObject, paramArrayOfObject);
    } catch (Exception exception) {
      if (!SWITCH_LOG) {
        paramObject = new StringBuilder();
        paramObject.append("callDeclaredMethod exception caught : ");
        paramObject.append(exception.getMessage());
        log(paramObject.toString());
        exception.printStackTrace();
        paramObject = stringBuilder;
        return paramObject;
      } 
    } 
    return paramObject;
  }
  
  public static Object callDeclaredConstructor(String paramString, Class[] paramArrayOfClass, Object[] paramArrayOfObject) {
    StringBuilder stringBuilder1, stringBuilder2 = new StringBuilder();
    stringBuilder2.append("callDeclaredConstructor : ");
    stringBuilder2.append(paramString);
    log(stringBuilder2.toString());
    stringBuilder2 = null;
    try {
      Class<?> clazz = Class.forName(paramString);
      Constructor<?> constructor = clazz.getDeclaredConstructor(paramArrayOfClass);
      constructor.setAccessible(true);
      constructor = (Constructor<?>)constructor.newInstance(paramArrayOfObject);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("callDeclaredConstructor exception caught : ");
      stringBuilder.append(exception.getMessage());
      log(stringBuilder.toString());
      exception.printStackTrace();
      stringBuilder1 = stringBuilder2;
    } 
    return stringBuilder1;
  }
  
  public static Object callDeclaredConstructorOrThrows(String paramString, Class[] paramArrayOfClass, Object[] paramArrayOfObject) {
    StringBuilder stringBuilder1, stringBuilder2 = new StringBuilder();
    stringBuilder2.append("callDeclaredConstructor : ");
    stringBuilder2.append(paramString);
    log(stringBuilder2.toString());
    stringBuilder2 = null;
    try {
      Class<?> clazz = Class.forName(paramString);
      Constructor<?> constructor = clazz.getDeclaredConstructor(paramArrayOfClass);
      constructor.setAccessible(true);
      constructor = (Constructor<?>)constructor.newInstance(paramArrayOfObject);
    } catch (Exception exception) {
      if (!SWITCH_LOG) {
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("callDeclaredConstructor exception caught : ");
        stringBuilder1.append(exception.getMessage());
        log(stringBuilder1.toString());
        exception.printStackTrace();
        stringBuilder1 = stringBuilder2;
        return stringBuilder1;
      } 
    } 
    return stringBuilder1;
  }
  
  public static Object getDeclaredField(Object paramObject, String paramString1, String paramString2) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramObject);
    stringBuilder.append(" getDeclaredField : ");
    stringBuilder.append(paramString1);
    stringBuilder.append(".");
    stringBuilder.append(paramString2);
    log(stringBuilder.toString());
    stringBuilder = null;
    try {
      Class<?> clazz = Class.forName(paramString1);
      Field field = clazz.getDeclaredField(paramString2);
      field.setAccessible(true);
      paramObject = field.get(paramObject);
    } catch (Exception exception) {
      paramObject = new StringBuilder();
      paramObject.append("getDeclaredField exception caught : ");
      paramObject.append(exception.getMessage());
      log(paramObject.toString());
      exception.printStackTrace();
      paramObject = stringBuilder;
    } 
    return paramObject;
  }
  
  public static Object getDeclaredFieldOrThrow(Object paramObject, String paramString1, String paramString2) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramObject);
    stringBuilder.append(" getDeclaredFieldOrThrow : ");
    stringBuilder.append(paramString1);
    stringBuilder.append(".");
    stringBuilder.append(paramString2);
    log(stringBuilder.toString());
    stringBuilder = null;
    try {
      Class<?> clazz = Class.forName(paramString1);
      Field field = clazz.getDeclaredField(paramString2);
      field.setAccessible(true);
      paramObject = field.get(paramObject);
    } catch (Exception exception) {
      if (!SWITCH_LOG) {
        paramObject = new StringBuilder();
        paramObject.append("getDeclaredField exception caught : ");
        paramObject.append(exception.getMessage());
        log(paramObject.toString());
        exception.printStackTrace();
        paramObject = stringBuilder;
        return paramObject;
      } 
    } 
    return paramObject;
  }
  
  public static void setDeclaredField(Object paramObject1, String paramString1, String paramString2, Object paramObject2) {
    try {
      Class<?> clazz = Class.forName(paramString1);
      Field field = clazz.getDeclaredField(paramString2);
      field.setAccessible(true);
      field.set(paramObject1, paramObject2);
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
  
  public static void setDeclaredFieldOrThrow(Object paramObject1, String paramString1, String paramString2, Object paramObject2) {
    try {
      Class<?> clazz = Class.forName(paramString1);
      Field field = clazz.getDeclaredField(paramString2);
      field.setAccessible(true);
      field.set(paramObject1, paramObject2);
    } catch (Exception exception) {
      if (!SWITCH_LOG) {
        exception.printStackTrace();
        return;
      } 
    } 
  }
  
  public static void log(String paramString) {
    Rlog.d("OplusTelephonyUtils", paramString);
  }
}
