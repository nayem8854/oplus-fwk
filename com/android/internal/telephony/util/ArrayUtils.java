package com.android.internal.telephony.util;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;

public final class ArrayUtils {
  public static <T> T[] appendElement(Class<T> paramClass, T[] paramArrayOfT, T paramT) {
    return appendElement(paramClass, paramArrayOfT, paramT, false);
  }
  
  public static <T> T[] appendElement(Class<T> paramClass, T[] paramArrayOfT, T paramT, boolean paramBoolean) {
    Object[] arrayOfObject;
    boolean bool;
    if (paramArrayOfT != null) {
      if (!paramBoolean && contains(paramArrayOfT, paramT))
        return paramArrayOfT; 
      bool = paramArrayOfT.length;
      arrayOfObject = (Object[])Array.newInstance(paramClass, bool + 1);
      System.arraycopy(paramArrayOfT, 0, arrayOfObject, 0, bool);
    } else {
      bool = false;
      arrayOfObject = (Object[])Array.newInstance((Class<?>)arrayOfObject, 1);
    } 
    arrayOfObject[bool] = paramT;
    return (T[])arrayOfObject;
  }
  
  public static <T> T[] concatElements(Class<T> paramClass, T[]... paramVarArgs) {
    if (paramVarArgs == null || paramVarArgs.length == 0)
      return createEmptyArray(paramClass); 
    int i = 0;
    int j;
    byte b;
    for (j = paramVarArgs.length, b = 0; b < j; ) {
      T[] arrayOfT = paramVarArgs[b];
      if (arrayOfT != null)
        i += arrayOfT.length; 
      b++;
    } 
    if (i == 0)
      return createEmptyArray(paramClass); 
    Object[] arrayOfObject = (Object[])Array.newInstance(paramClass, i);
    i = 0;
    for (int k = paramVarArgs.length; b < k; ) {
      T[] arrayOfT = paramVarArgs[b];
      j = i;
      if (arrayOfT != null)
        if (arrayOfT.length == 0) {
          j = i;
        } else {
          System.arraycopy(arrayOfT, 0, arrayOfObject, i, arrayOfT.length);
          j = i + arrayOfT.length;
        }  
      b++;
      i = j;
    } 
    return (T[])arrayOfObject;
  }
  
  private static <T> T[] createEmptyArray(Class<T> paramClass) {
    if (paramClass == String.class)
      return (T[])EmptyArray.STRING; 
    if (paramClass == Object.class)
      return (T[])EmptyArray.OBJECT; 
    return (T[])Array.newInstance(paramClass, 0);
  }
  
  private static final class EmptyArray {
    public static final Object[] OBJECT = new Object[0];
    
    public static final String[] STRING = new String[0];
  }
  
  public static boolean contains(char[] paramArrayOfchar, char paramChar) {
    if (paramArrayOfchar == null)
      return false; 
    int i;
    byte b;
    for (i = paramArrayOfchar.length, b = 0; b < i; ) {
      char c = paramArrayOfchar[b];
      if (c == paramChar)
        return true; 
      b++;
    } 
    return false;
  }
  
  public static <T> boolean contains(Collection<T> paramCollection, T paramT) {
    boolean bool;
    if (paramCollection != null) {
      bool = paramCollection.contains(paramT);
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean contains(int[] paramArrayOfint, int paramInt) {
    if (paramArrayOfint == null)
      return false; 
    int i;
    byte b;
    for (i = paramArrayOfint.length, b = 0; b < i; ) {
      int j = paramArrayOfint[b];
      if (j == paramInt)
        return true; 
      b++;
    } 
    return false;
  }
  
  public static boolean contains(long[] paramArrayOflong, long paramLong) {
    if (paramArrayOflong == null)
      return false; 
    int i;
    byte b;
    for (i = paramArrayOflong.length, b = 0; b < i; ) {
      long l = paramArrayOflong[b];
      if (l == paramLong)
        return true; 
      b++;
    } 
    return false;
  }
  
  public static <T> boolean contains(T[] paramArrayOfT, T paramT) {
    boolean bool;
    if (indexOf(paramArrayOfT, paramT) != -1) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static <T> int indexOf(T[] paramArrayOfT, T paramT) {
    if (paramArrayOfT == null)
      return -1; 
    for (byte b = 0; b < paramArrayOfT.length; b++) {
      if (Objects.equals(paramArrayOfT[b], paramT))
        return b; 
    } 
    return -1;
  }
  
  public static boolean isEmpty(Collection<?> paramCollection) {
    return (paramCollection == null || paramCollection.isEmpty());
  }
  
  public static boolean isEmpty(Map<?, ?> paramMap) {
    return (paramMap == null || paramMap.isEmpty());
  }
  
  public static <T> boolean isEmpty(T[] paramArrayOfT) {
    return (paramArrayOfT == null || paramArrayOfT.length == 0);
  }
  
  public static boolean isEmpty(int[] paramArrayOfint) {
    return (paramArrayOfint == null || paramArrayOfint.length == 0);
  }
  
  public static boolean isEmpty(long[] paramArrayOflong) {
    return (paramArrayOflong == null || paramArrayOflong.length == 0);
  }
  
  public static boolean isEmpty(byte[] paramArrayOfbyte) {
    return (paramArrayOfbyte == null || paramArrayOfbyte.length == 0);
  }
  
  public static boolean isEmpty(boolean[] paramArrayOfboolean) {
    return (paramArrayOfboolean == null || paramArrayOfboolean.length == 0);
  }
}
