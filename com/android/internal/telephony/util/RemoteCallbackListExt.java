package com.android.internal.telephony.util;

import android.os.IInterface;
import android.os.RemoteCallbackList;
import java.util.function.Consumer;

public class RemoteCallbackListExt<E extends IInterface> extends RemoteCallbackList<E> {
  public void broadcastAction(Consumer<E> paramConsumer) {
    int i = beginBroadcast();
    for (byte b = 0; b < i;) {
      try {
        paramConsumer.accept((E)getBroadcastItem(b));
      } finally {
        finishBroadcast();
      } 
    } 
    finishBroadcast();
  }
}
