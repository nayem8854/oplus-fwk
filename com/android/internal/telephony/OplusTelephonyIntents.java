package com.android.internal.telephony;

public class OplusTelephonyIntents {
  public static final String ACTION_COMMAND_FORCE_DISABLE_ENDC = "android.intent.force_disable_endc";
  
  public static final String ACTION_DEEPTHINKER_STARTUP = "oplus.intent.action.DEEPTHINKER_EVENTFOUNTAIN_STARTUP";
  
  public static final String ACTION_ENTER_GAME_SPACE_OPTIMIZE = "oplus.intent.action.ENTER_GAME_SPACE_OPTIMIZE";
  
  public static final String ACTION_EXIT_GAME_SPACE_OPTIMIZE = "oplus.intent.action.EXIT_GAME_SPACE_OPTIMIZE";
  
  public static final String ACTION_GET_RF_FEATURE_DONE = "oplus.intent.action.GET_RF_FEATURE_DONE";
  
  public static final String ACTION_HOTSWAP_STATE_CHANGE = "oplus.intent.action.SIM_HOTSWAP_STATE_CHANGE";
  
  public static final String ACTION_PBM_STATE_READY = "android.intent.action.PBM_STATE_READY";
  
  public static final String ACTION_THERMAL_THROTTLING = "oplus.intent.action.THERMAL_THROTTLING_5G";
  
  public static final String EXTRA_RADIO_BUG_TYPE = "radioBugType";
  
  public static final String EXTRA_SLOT_ID = "slotId";
}
