package com.android.internal.telephony;

public interface OplusTelephonyProperties {
  public static final String PROPERTY_CARRIER_BSTONE = "sys.oplus.radio.carrier.bstone";
  
  public static final String PROPERTY_CARRIER_CWTONE = "sys.oplus.radio.carrier.cwtone";
  
  public static final String PROPERTY_CARRIER_RINGTONE = "sys.oplus.radio.carrier.ringtone";
  
  public static final String PROPERTY_CT_AUTOREG_IMS_PROP = "persist.sys.oplus.radio.ct_auto_ims";
  
  public static final String PROPERTY_DATA_HONGBAO_IP = "ro.oplus.radio.data_hongbao_ip";
  
  public static final String PROPERTY_DATA_MTU = "persist.sys.oplus.radio.data_mtu";
  
  public static final String PROPERTY_DEFAULT_AUTO_NR_MODE = "ro.oplus.radio.auto_nr_mode";
  
  public static final String PROPERTY_DEFAULT_AUTO_NR_MODE_SUB = "ro.oplus.radio.auto_nr_mode_sub";
  
  public static final String PROPERTY_DISABLE_VOOC_FOR_CALL = "sys.oplus.radio.disable_vooc";
  
  public static final String PROPERTY_DISPLAY_IMS_ID = "ro.build.display.id_ims";
  
  public static final String PROPERTY_ERLVT_OFF = "persist.sys.oplus.radio.erlvt_off";
  
  public static final String PROPERTY_EVDO_ALLOWED_NO_SERVICE_INTERVAL = "ro.oplus.radio.evdo_oos_interval";
  
  public static final String PROPERTY_EVS_ENABLE = "persist.sys.oplus.radio.ims_evs_enable";
  
  public static final String PROPERTY_FAST_DORMANCY = "ro.oplus.radio.fast_dormancy";
  
  public static final String PROPERTY_FORCE_TEST_CARD = "ro.oplus.radio.force_test_card";
  
  public static final String PROPERTY_GSM_ALLOWED_NO_SERVICE_INTERVAL = "ro.oplus.radio.gsm_oos_interval";
  
  public static final String PROPERTY_JIO_SECONDARY = "persist.sys.oplus.radio.jio_secondary";
  
  public static final String PROPERTY_LTE_ALLOWED_NO_SERVICE_INTERVAL = "ro.oplus.radio.lte_oos_interval";
  
  public static final String PROPERTY_LTE_RSRP_THRESHOLDS = "ro.oplus.radio.lte_rsrp_thresholds";
  
  public static final String PROPERTY_MODEM_RESET_SMOOTH_TIME = "ro.oplus.radio.modem_reset_time";
  
  public static final String PROPERTY_NR_ICON_TYPE = "ro.oplus.radio.nr_icon_type";
  
  public static final String PROPERTY_OSL = "ro.oplus.radio.osl";
  
  public static final String PROPERTY_QCOM_FREQHOP = "persist.sys.oplus.radio.qcom_freqhop";
  
  public static final String PROPERTY_REGION_NETLOCK_COUNTRY = "ro.oplus.radio.region_netlock_country";
  
  public static final String PROPERTY_REGION_NETLOCK_ENABLE = "ro.oplus.radio.region_netlock_enable";
  
  public static final String PROPERTY_SMART5G_CMCC_EVAL_SWITCH = "ro.oplus.radio.smart5g_cmcc_eval_switch";
  
  public static final String PROPERTY_SMS_MT_BLOCK_WHITE_LIST = "ro.oplus.radio.sms_mt_block_whitelist";
  
  public static final String PROPERTY_TDS_ALLOWED_NO_SERVICE_INTERVAL = "ro.oplus.radio.tds_oos_interval";
  
  public static final String PROPERTY_TEMP_DDSSWITCH = "persist.sys.oplus.radio.data_enable_temp_dds";
  
  public static final String PROPERTY_TEMP_DDSSWITCH_LOGV = "persist.sys.oplus.radio.data_enable_temp_dds_logv";
  
  public static final String PROPERTY_WCDMA_RSRP_THRESHOLDS = "ro.oplus.radio.wcdma_rsrp_thresholds";
  
  public static final String PROPERTY_W_ALLOWED_NO_SERVICE_INTERVAL = "ro.oplus.radio.w_oos_interval";
}
