package com.android.internal.telephony;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelUuid;
import android.os.RemoteException;
import android.telephony.SubscriptionInfo;
import java.util.List;

public interface ISub extends IInterface {
  int addSubInfo(String paramString1, String paramString2, int paramInt1, int paramInt2) throws RemoteException;
  
  int addSubInfoRecord(String paramString, int paramInt) throws RemoteException;
  
  void addSubscriptionsIntoGroup(int[] paramArrayOfint, ParcelUuid paramParcelUuid, String paramString) throws RemoteException;
  
  boolean canDisablePhysicalSubscription() throws RemoteException;
  
  int clearSubInfo() throws RemoteException;
  
  ParcelUuid createSubscriptionGroup(int[] paramArrayOfint, String paramString) throws RemoteException;
  
  List<SubscriptionInfo> getAccessibleSubscriptionInfoList(String paramString) throws RemoteException;
  
  int getActiveDataSubscriptionId() throws RemoteException;
  
  int[] getActiveSubIdList(boolean paramBoolean) throws RemoteException;
  
  int getActiveSubInfoCount(String paramString1, String paramString2) throws RemoteException;
  
  int getActiveSubInfoCountMax() throws RemoteException;
  
  SubscriptionInfo getActiveSubscriptionInfo(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  SubscriptionInfo getActiveSubscriptionInfoForIccId(String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  SubscriptionInfo getActiveSubscriptionInfoForSimSlotIndex(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  List<SubscriptionInfo> getActiveSubscriptionInfoList(String paramString1, String paramString2) throws RemoteException;
  
  int getAllSubInfoCount(String paramString1, String paramString2) throws RemoteException;
  
  List<SubscriptionInfo> getAllSubInfoList(String paramString1, String paramString2) throws RemoteException;
  
  List<SubscriptionInfo> getAvailableSubscriptionInfoList(String paramString1, String paramString2) throws RemoteException;
  
  int getDefaultDataSubId() throws RemoteException;
  
  int getDefaultSmsSubId() throws RemoteException;
  
  int getDefaultSubId() throws RemoteException;
  
  int getDefaultVoiceSubId() throws RemoteException;
  
  int getEnabledSubscriptionId(int paramInt) throws RemoteException;
  
  List<SubscriptionInfo> getOpportunisticSubscriptions(String paramString1, String paramString2) throws RemoteException;
  
  int getPhoneId(int paramInt) throws RemoteException;
  
  int getPreferredDataSubscriptionId() throws RemoteException;
  
  int getSimStateForSlotIndex(int paramInt) throws RemoteException;
  
  int getSlotIndex(int paramInt) throws RemoteException;
  
  int[] getSubId(int paramInt) throws RemoteException;
  
  String getSubscriptionProperty(int paramInt, String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  List<SubscriptionInfo> getSubscriptionsInGroup(ParcelUuid paramParcelUuid, String paramString1, String paramString2) throws RemoteException;
  
  boolean isActiveSubId(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  boolean isSubscriptionEnabled(int paramInt) throws RemoteException;
  
  int removeSubInfo(String paramString, int paramInt) throws RemoteException;
  
  void removeSubscriptionsFromGroup(int[] paramArrayOfint, ParcelUuid paramParcelUuid, String paramString) throws RemoteException;
  
  void requestEmbeddedSubscriptionInfoListRefresh(int paramInt) throws RemoteException;
  
  int setDataRoaming(int paramInt1, int paramInt2) throws RemoteException;
  
  void setDefaultDataSubId(int paramInt) throws RemoteException;
  
  void setDefaultSmsSubId(int paramInt) throws RemoteException;
  
  void setDefaultVoiceSubId(int paramInt) throws RemoteException;
  
  int setDisplayNameUsingSrc(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  int setDisplayNumber(String paramString, int paramInt) throws RemoteException;
  
  int setIconTint(int paramInt1, int paramInt2) throws RemoteException;
  
  int setOpportunistic(boolean paramBoolean, int paramInt, String paramString) throws RemoteException;
  
  void setPreferredDataSubscriptionId(int paramInt, boolean paramBoolean, ISetOpportunisticDataCallback paramISetOpportunisticDataCallback) throws RemoteException;
  
  boolean setSubscriptionEnabled(boolean paramBoolean, int paramInt) throws RemoteException;
  
  int setSubscriptionProperty(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  int setUiccApplicationsEnabled(boolean paramBoolean, int paramInt) throws RemoteException;
  
  class Default implements ISub {
    public List<SubscriptionInfo> getAllSubInfoList(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public int getAllSubInfoCount(String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public SubscriptionInfo getActiveSubscriptionInfo(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public SubscriptionInfo getActiveSubscriptionInfoForIccId(String param1String1, String param1String2, String param1String3) throws RemoteException {
      return null;
    }
    
    public SubscriptionInfo getActiveSubscriptionInfoForSimSlotIndex(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public List<SubscriptionInfo> getActiveSubscriptionInfoList(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public int getActiveSubInfoCount(String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public int getActiveSubInfoCountMax() throws RemoteException {
      return 0;
    }
    
    public List<SubscriptionInfo> getAvailableSubscriptionInfoList(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public List<SubscriptionInfo> getAccessibleSubscriptionInfoList(String param1String) throws RemoteException {
      return null;
    }
    
    public void requestEmbeddedSubscriptionInfoListRefresh(int param1Int) throws RemoteException {}
    
    public int addSubInfoRecord(String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public int addSubInfo(String param1String1, String param1String2, int param1Int1, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public int removeSubInfo(String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public int setIconTint(int param1Int1, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public int setDisplayNameUsingSrc(String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public int setDisplayNumber(String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public int setDataRoaming(int param1Int1, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public int setOpportunistic(boolean param1Boolean, int param1Int, String param1String) throws RemoteException {
      return 0;
    }
    
    public ParcelUuid createSubscriptionGroup(int[] param1ArrayOfint, String param1String) throws RemoteException {
      return null;
    }
    
    public void setPreferredDataSubscriptionId(int param1Int, boolean param1Boolean, ISetOpportunisticDataCallback param1ISetOpportunisticDataCallback) throws RemoteException {}
    
    public int getPreferredDataSubscriptionId() throws RemoteException {
      return 0;
    }
    
    public List<SubscriptionInfo> getOpportunisticSubscriptions(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public void removeSubscriptionsFromGroup(int[] param1ArrayOfint, ParcelUuid param1ParcelUuid, String param1String) throws RemoteException {}
    
    public void addSubscriptionsIntoGroup(int[] param1ArrayOfint, ParcelUuid param1ParcelUuid, String param1String) throws RemoteException {}
    
    public List<SubscriptionInfo> getSubscriptionsInGroup(ParcelUuid param1ParcelUuid, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public int getSlotIndex(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int[] getSubId(int param1Int) throws RemoteException {
      return null;
    }
    
    public int getDefaultSubId() throws RemoteException {
      return 0;
    }
    
    public int clearSubInfo() throws RemoteException {
      return 0;
    }
    
    public int getPhoneId(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int getDefaultDataSubId() throws RemoteException {
      return 0;
    }
    
    public void setDefaultDataSubId(int param1Int) throws RemoteException {}
    
    public int getDefaultVoiceSubId() throws RemoteException {
      return 0;
    }
    
    public void setDefaultVoiceSubId(int param1Int) throws RemoteException {}
    
    public int getDefaultSmsSubId() throws RemoteException {
      return 0;
    }
    
    public void setDefaultSmsSubId(int param1Int) throws RemoteException {}
    
    public int[] getActiveSubIdList(boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public int setSubscriptionProperty(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public String getSubscriptionProperty(int param1Int, String param1String1, String param1String2, String param1String3) throws RemoteException {
      return null;
    }
    
    public boolean setSubscriptionEnabled(boolean param1Boolean, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isSubscriptionEnabled(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getEnabledSubscriptionId(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int getSimStateForSlotIndex(int param1Int) throws RemoteException {
      return 0;
    }
    
    public boolean isActiveSubId(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public int getActiveDataSubscriptionId() throws RemoteException {
      return 0;
    }
    
    public boolean canDisablePhysicalSubscription() throws RemoteException {
      return false;
    }
    
    public int setUiccApplicationsEnabled(boolean param1Boolean, int param1Int) throws RemoteException {
      return 0;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISub {
    private static final String DESCRIPTOR = "com.android.internal.telephony.ISub";
    
    static final int TRANSACTION_addSubInfo = 13;
    
    static final int TRANSACTION_addSubInfoRecord = 12;
    
    static final int TRANSACTION_addSubscriptionsIntoGroup = 25;
    
    static final int TRANSACTION_canDisablePhysicalSubscription = 47;
    
    static final int TRANSACTION_clearSubInfo = 30;
    
    static final int TRANSACTION_createSubscriptionGroup = 20;
    
    static final int TRANSACTION_getAccessibleSubscriptionInfoList = 10;
    
    static final int TRANSACTION_getActiveDataSubscriptionId = 46;
    
    static final int TRANSACTION_getActiveSubIdList = 38;
    
    static final int TRANSACTION_getActiveSubInfoCount = 7;
    
    static final int TRANSACTION_getActiveSubInfoCountMax = 8;
    
    static final int TRANSACTION_getActiveSubscriptionInfo = 3;
    
    static final int TRANSACTION_getActiveSubscriptionInfoForIccId = 4;
    
    static final int TRANSACTION_getActiveSubscriptionInfoForSimSlotIndex = 5;
    
    static final int TRANSACTION_getActiveSubscriptionInfoList = 6;
    
    static final int TRANSACTION_getAllSubInfoCount = 2;
    
    static final int TRANSACTION_getAllSubInfoList = 1;
    
    static final int TRANSACTION_getAvailableSubscriptionInfoList = 9;
    
    static final int TRANSACTION_getDefaultDataSubId = 32;
    
    static final int TRANSACTION_getDefaultSmsSubId = 36;
    
    static final int TRANSACTION_getDefaultSubId = 29;
    
    static final int TRANSACTION_getDefaultVoiceSubId = 34;
    
    static final int TRANSACTION_getEnabledSubscriptionId = 43;
    
    static final int TRANSACTION_getOpportunisticSubscriptions = 23;
    
    static final int TRANSACTION_getPhoneId = 31;
    
    static final int TRANSACTION_getPreferredDataSubscriptionId = 22;
    
    static final int TRANSACTION_getSimStateForSlotIndex = 44;
    
    static final int TRANSACTION_getSlotIndex = 27;
    
    static final int TRANSACTION_getSubId = 28;
    
    static final int TRANSACTION_getSubscriptionProperty = 40;
    
    static final int TRANSACTION_getSubscriptionsInGroup = 26;
    
    static final int TRANSACTION_isActiveSubId = 45;
    
    static final int TRANSACTION_isSubscriptionEnabled = 42;
    
    static final int TRANSACTION_removeSubInfo = 14;
    
    static final int TRANSACTION_removeSubscriptionsFromGroup = 24;
    
    static final int TRANSACTION_requestEmbeddedSubscriptionInfoListRefresh = 11;
    
    static final int TRANSACTION_setDataRoaming = 18;
    
    static final int TRANSACTION_setDefaultDataSubId = 33;
    
    static final int TRANSACTION_setDefaultSmsSubId = 37;
    
    static final int TRANSACTION_setDefaultVoiceSubId = 35;
    
    static final int TRANSACTION_setDisplayNameUsingSrc = 16;
    
    static final int TRANSACTION_setDisplayNumber = 17;
    
    static final int TRANSACTION_setIconTint = 15;
    
    static final int TRANSACTION_setOpportunistic = 19;
    
    static final int TRANSACTION_setPreferredDataSubscriptionId = 21;
    
    static final int TRANSACTION_setSubscriptionEnabled = 41;
    
    static final int TRANSACTION_setSubscriptionProperty = 39;
    
    static final int TRANSACTION_setUiccApplicationsEnabled = 48;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.ISub");
    }
    
    public static ISub asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.ISub");
      if (iInterface != null && iInterface instanceof ISub)
        return (ISub)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 48:
          return "setUiccApplicationsEnabled";
        case 47:
          return "canDisablePhysicalSubscription";
        case 46:
          return "getActiveDataSubscriptionId";
        case 45:
          return "isActiveSubId";
        case 44:
          return "getSimStateForSlotIndex";
        case 43:
          return "getEnabledSubscriptionId";
        case 42:
          return "isSubscriptionEnabled";
        case 41:
          return "setSubscriptionEnabled";
        case 40:
          return "getSubscriptionProperty";
        case 39:
          return "setSubscriptionProperty";
        case 38:
          return "getActiveSubIdList";
        case 37:
          return "setDefaultSmsSubId";
        case 36:
          return "getDefaultSmsSubId";
        case 35:
          return "setDefaultVoiceSubId";
        case 34:
          return "getDefaultVoiceSubId";
        case 33:
          return "setDefaultDataSubId";
        case 32:
          return "getDefaultDataSubId";
        case 31:
          return "getPhoneId";
        case 30:
          return "clearSubInfo";
        case 29:
          return "getDefaultSubId";
        case 28:
          return "getSubId";
        case 27:
          return "getSlotIndex";
        case 26:
          return "getSubscriptionsInGroup";
        case 25:
          return "addSubscriptionsIntoGroup";
        case 24:
          return "removeSubscriptionsFromGroup";
        case 23:
          return "getOpportunisticSubscriptions";
        case 22:
          return "getPreferredDataSubscriptionId";
        case 21:
          return "setPreferredDataSubscriptionId";
        case 20:
          return "createSubscriptionGroup";
        case 19:
          return "setOpportunistic";
        case 18:
          return "setDataRoaming";
        case 17:
          return "setDisplayNumber";
        case 16:
          return "setDisplayNameUsingSrc";
        case 15:
          return "setIconTint";
        case 14:
          return "removeSubInfo";
        case 13:
          return "addSubInfo";
        case 12:
          return "addSubInfoRecord";
        case 11:
          return "requestEmbeddedSubscriptionInfoListRefresh";
        case 10:
          return "getAccessibleSubscriptionInfoList";
        case 9:
          return "getAvailableSubscriptionInfoList";
        case 8:
          return "getActiveSubInfoCountMax";
        case 7:
          return "getActiveSubInfoCount";
        case 6:
          return "getActiveSubscriptionInfoList";
        case 5:
          return "getActiveSubscriptionInfoForSimSlotIndex";
        case 4:
          return "getActiveSubscriptionInfoForIccId";
        case 3:
          return "getActiveSubscriptionInfo";
        case 2:
          return "getAllSubInfoCount";
        case 1:
          break;
      } 
      return "getAllSubInfoList";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        int i;
        String str11;
        int[] arrayOfInt1;
        String str10;
        List<SubscriptionInfo> list6;
        String str9;
        List<SubscriptionInfo> list5;
        ISetOpportunisticDataCallback iSetOpportunisticDataCallback;
        String str8;
        ParcelUuid parcelUuid;
        String str7;
        List<SubscriptionInfo> list4;
        String str6;
        List<SubscriptionInfo> list3;
        String str5;
        List<SubscriptionInfo> list2;
        String str4;
        SubscriptionInfo subscriptionInfo3;
        String str3;
        SubscriptionInfo subscriptionInfo2;
        String str2;
        SubscriptionInfo subscriptionInfo1;
        String str13;
        int[] arrayOfInt2;
        String str15;
        int[] arrayOfInt3;
        String str14;
        boolean bool5 = false, bool6 = false, bool7 = false, bool8 = false, bool9 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 48:
            param1Parcel1.enforceInterface("com.android.internal.telephony.ISub");
            if (param1Parcel1.readInt() != 0)
              bool9 = true; 
            param1Int1 = param1Parcel1.readInt();
            param1Int1 = setUiccApplicationsEnabled(bool9, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 47:
            param1Parcel1.enforceInterface("com.android.internal.telephony.ISub");
            bool4 = canDisablePhysicalSubscription();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 46:
            param1Parcel1.enforceInterface("com.android.internal.telephony.ISub");
            m = getActiveDataSubscriptionId();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(m);
            return true;
          case 45:
            param1Parcel1.enforceInterface("com.android.internal.telephony.ISub");
            m = param1Parcel1.readInt();
            str13 = param1Parcel1.readString();
            str11 = param1Parcel1.readString();
            bool3 = isActiveSubId(m, str13, str11);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 44:
            str11.enforceInterface("com.android.internal.telephony.ISub");
            k = str11.readInt();
            k = getSimStateForSlotIndex(k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 43:
            str11.enforceInterface("com.android.internal.telephony.ISub");
            k = str11.readInt();
            k = getEnabledSubscriptionId(k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 42:
            str11.enforceInterface("com.android.internal.telephony.ISub");
            k = str11.readInt();
            bool2 = isSubscriptionEnabled(k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 41:
            str11.enforceInterface("com.android.internal.telephony.ISub");
            bool9 = bool5;
            if (str11.readInt() != 0)
              bool9 = true; 
            j = str11.readInt();
            bool1 = setSubscriptionEnabled(bool9, j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 40:
            str11.enforceInterface("com.android.internal.telephony.ISub");
            i = str11.readInt();
            str13 = str11.readString();
            str15 = str11.readString();
            str11 = str11.readString();
            str11 = getSubscriptionProperty(i, str13, str15, str11);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str11);
            return true;
          case 39:
            str11.enforceInterface("com.android.internal.telephony.ISub");
            i = str11.readInt();
            str13 = str11.readString();
            str11 = str11.readString();
            i = setSubscriptionProperty(i, str13, str11);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 38:
            str11.enforceInterface("com.android.internal.telephony.ISub");
            bool9 = bool6;
            if (str11.readInt() != 0)
              bool9 = true; 
            arrayOfInt1 = getActiveSubIdList(bool9);
            param1Parcel2.writeNoException();
            param1Parcel2.writeIntArray(arrayOfInt1);
            return true;
          case 37:
            arrayOfInt1.enforceInterface("com.android.internal.telephony.ISub");
            i = arrayOfInt1.readInt();
            setDefaultSmsSubId(i);
            param1Parcel2.writeNoException();
            return true;
          case 36:
            arrayOfInt1.enforceInterface("com.android.internal.telephony.ISub");
            i = getDefaultSmsSubId();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 35:
            arrayOfInt1.enforceInterface("com.android.internal.telephony.ISub");
            i = arrayOfInt1.readInt();
            setDefaultVoiceSubId(i);
            param1Parcel2.writeNoException();
            return true;
          case 34:
            arrayOfInt1.enforceInterface("com.android.internal.telephony.ISub");
            i = getDefaultVoiceSubId();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 33:
            arrayOfInt1.enforceInterface("com.android.internal.telephony.ISub");
            i = arrayOfInt1.readInt();
            setDefaultDataSubId(i);
            param1Parcel2.writeNoException();
            return true;
          case 32:
            arrayOfInt1.enforceInterface("com.android.internal.telephony.ISub");
            i = getDefaultDataSubId();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 31:
            arrayOfInt1.enforceInterface("com.android.internal.telephony.ISub");
            i = arrayOfInt1.readInt();
            i = getPhoneId(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 30:
            arrayOfInt1.enforceInterface("com.android.internal.telephony.ISub");
            i = clearSubInfo();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 29:
            arrayOfInt1.enforceInterface("com.android.internal.telephony.ISub");
            i = getDefaultSubId();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 28:
            arrayOfInt1.enforceInterface("com.android.internal.telephony.ISub");
            i = arrayOfInt1.readInt();
            arrayOfInt1 = getSubId(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeIntArray(arrayOfInt1);
            return true;
          case 27:
            arrayOfInt1.enforceInterface("com.android.internal.telephony.ISub");
            i = arrayOfInt1.readInt();
            i = getSlotIndex(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 26:
            arrayOfInt1.enforceInterface("com.android.internal.telephony.ISub");
            if (arrayOfInt1.readInt() != 0) {
              ParcelUuid parcelUuid1 = (ParcelUuid)ParcelUuid.CREATOR.createFromParcel((Parcel)arrayOfInt1);
            } else {
              str13 = null;
            } 
            str15 = arrayOfInt1.readString();
            str10 = arrayOfInt1.readString();
            list6 = getSubscriptionsInGroup((ParcelUuid)str13, str15, str10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list6);
            return true;
          case 25:
            list6.enforceInterface("com.android.internal.telephony.ISub");
            arrayOfInt3 = list6.createIntArray();
            if (list6.readInt() != 0) {
              ParcelUuid parcelUuid1 = (ParcelUuid)ParcelUuid.CREATOR.createFromParcel((Parcel)list6);
            } else {
              str13 = null;
            } 
            str9 = list6.readString();
            addSubscriptionsIntoGroup(arrayOfInt3, (ParcelUuid)str13, str9);
            param1Parcel2.writeNoException();
            return true;
          case 24:
            str9.enforceInterface("com.android.internal.telephony.ISub");
            arrayOfInt3 = str9.createIntArray();
            if (str9.readInt() != 0) {
              ParcelUuid parcelUuid1 = (ParcelUuid)ParcelUuid.CREATOR.createFromParcel((Parcel)str9);
            } else {
              str13 = null;
            } 
            str9 = str9.readString();
            removeSubscriptionsFromGroup(arrayOfInt3, (ParcelUuid)str13, str9);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            str9.enforceInterface("com.android.internal.telephony.ISub");
            str13 = str9.readString();
            str9 = str9.readString();
            list5 = getOpportunisticSubscriptions(str13, str9);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list5);
            return true;
          case 22:
            list5.enforceInterface("com.android.internal.telephony.ISub");
            i = getPreferredDataSubscriptionId();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 21:
            list5.enforceInterface("com.android.internal.telephony.ISub");
            i = list5.readInt();
            bool9 = bool7;
            if (list5.readInt() != 0)
              bool9 = true; 
            iSetOpportunisticDataCallback = ISetOpportunisticDataCallback.Stub.asInterface(list5.readStrongBinder());
            setPreferredDataSubscriptionId(i, bool9, iSetOpportunisticDataCallback);
            param1Parcel2.writeNoException();
            return true;
          case 20:
            iSetOpportunisticDataCallback.enforceInterface("com.android.internal.telephony.ISub");
            arrayOfInt2 = iSetOpportunisticDataCallback.createIntArray();
            str8 = iSetOpportunisticDataCallback.readString();
            parcelUuid = createSubscriptionGroup(arrayOfInt2, str8);
            param1Parcel2.writeNoException();
            if (parcelUuid != null) {
              param1Parcel2.writeInt(1);
              parcelUuid.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 19:
            parcelUuid.enforceInterface("com.android.internal.telephony.ISub");
            bool9 = bool8;
            if (parcelUuid.readInt() != 0)
              bool9 = true; 
            i = parcelUuid.readInt();
            str7 = parcelUuid.readString();
            i = setOpportunistic(bool9, i, str7);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 18:
            str7.enforceInterface("com.android.internal.telephony.ISub");
            i = str7.readInt();
            param1Int2 = str7.readInt();
            i = setDataRoaming(i, param1Int2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 17:
            str7.enforceInterface("com.android.internal.telephony.ISub");
            str12 = str7.readString();
            i = str7.readInt();
            i = setDisplayNumber(str12, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 16:
            str7.enforceInterface("com.android.internal.telephony.ISub");
            str12 = str7.readString();
            param1Int2 = str7.readInt();
            i = str7.readInt();
            i = setDisplayNameUsingSrc(str12, param1Int2, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 15:
            str7.enforceInterface("com.android.internal.telephony.ISub");
            i = str7.readInt();
            param1Int2 = str7.readInt();
            i = setIconTint(i, param1Int2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 14:
            str7.enforceInterface("com.android.internal.telephony.ISub");
            str12 = str7.readString();
            i = str7.readInt();
            i = removeSubInfo(str12, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 13:
            str7.enforceInterface("com.android.internal.telephony.ISub");
            str12 = str7.readString();
            str14 = str7.readString();
            i = str7.readInt();
            param1Int2 = str7.readInt();
            i = addSubInfo(str12, str14, i, param1Int2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 12:
            str7.enforceInterface("com.android.internal.telephony.ISub");
            str12 = str7.readString();
            i = str7.readInt();
            i = addSubInfoRecord(str12, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 11:
            str7.enforceInterface("com.android.internal.telephony.ISub");
            i = str7.readInt();
            requestEmbeddedSubscriptionInfoListRefresh(i);
            return true;
          case 10:
            str7.enforceInterface("com.android.internal.telephony.ISub");
            str7 = str7.readString();
            list4 = getAccessibleSubscriptionInfoList(str7);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list4);
            return true;
          case 9:
            list4.enforceInterface("com.android.internal.telephony.ISub");
            str12 = list4.readString();
            str6 = list4.readString();
            list3 = getAvailableSubscriptionInfoList(str12, str6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list3);
            return true;
          case 8:
            list3.enforceInterface("com.android.internal.telephony.ISub");
            i = getActiveSubInfoCountMax();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 7:
            list3.enforceInterface("com.android.internal.telephony.ISub");
            str12 = list3.readString();
            str5 = list3.readString();
            i = getActiveSubInfoCount(str12, str5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 6:
            str5.enforceInterface("com.android.internal.telephony.ISub");
            str12 = str5.readString();
            str5 = str5.readString();
            list2 = getActiveSubscriptionInfoList(str12, str5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list2);
            return true;
          case 5:
            list2.enforceInterface("com.android.internal.telephony.ISub");
            i = list2.readInt();
            str12 = list2.readString();
            str4 = list2.readString();
            subscriptionInfo3 = getActiveSubscriptionInfoForSimSlotIndex(i, str12, str4);
            param1Parcel2.writeNoException();
            if (subscriptionInfo3 != null) {
              param1Parcel2.writeInt(1);
              subscriptionInfo3.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 4:
            subscriptionInfo3.enforceInterface("com.android.internal.telephony.ISub");
            str12 = subscriptionInfo3.readString();
            str14 = subscriptionInfo3.readString();
            str3 = subscriptionInfo3.readString();
            subscriptionInfo2 = getActiveSubscriptionInfoForIccId(str12, str14, str3);
            param1Parcel2.writeNoException();
            if (subscriptionInfo2 != null) {
              param1Parcel2.writeInt(1);
              subscriptionInfo2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 3:
            subscriptionInfo2.enforceInterface("com.android.internal.telephony.ISub");
            i = subscriptionInfo2.readInt();
            str12 = subscriptionInfo2.readString();
            str2 = subscriptionInfo2.readString();
            subscriptionInfo1 = getActiveSubscriptionInfo(i, str12, str2);
            param1Parcel2.writeNoException();
            if (subscriptionInfo1 != null) {
              param1Parcel2.writeInt(1);
              subscriptionInfo1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 2:
            subscriptionInfo1.enforceInterface("com.android.internal.telephony.ISub");
            str12 = subscriptionInfo1.readString();
            str1 = subscriptionInfo1.readString();
            i = getAllSubInfoCount(str12, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("com.android.internal.telephony.ISub");
        String str12 = str1.readString();
        String str1 = str1.readString();
        List<SubscriptionInfo> list1 = getAllSubInfoList(str12, str1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeTypedList(list1);
        return true;
      } 
      param1Parcel2.writeString("com.android.internal.telephony.ISub");
      return true;
    }
    
    private static class Proxy implements ISub {
      public static ISub sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.ISub";
      }
      
      public List<SubscriptionInfo> getAllSubInfoList(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null)
            return ISub.Stub.getDefaultImpl().getAllSubInfoList(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.createTypedArrayList(SubscriptionInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getAllSubInfoCount(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null)
            return ISub.Stub.getDefaultImpl().getAllSubInfoCount(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public SubscriptionInfo getActiveSubscriptionInfo(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null)
            return ISub.Stub.getDefaultImpl().getActiveSubscriptionInfo(param2Int, param2String1, param2String2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            SubscriptionInfo subscriptionInfo = (SubscriptionInfo)SubscriptionInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (SubscriptionInfo)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public SubscriptionInfo getActiveSubscriptionInfoForIccId(String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null)
            return ISub.Stub.getDefaultImpl().getActiveSubscriptionInfoForIccId(param2String1, param2String2, param2String3); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            SubscriptionInfo subscriptionInfo = (SubscriptionInfo)SubscriptionInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (SubscriptionInfo)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public SubscriptionInfo getActiveSubscriptionInfoForSimSlotIndex(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null)
            return ISub.Stub.getDefaultImpl().getActiveSubscriptionInfoForSimSlotIndex(param2Int, param2String1, param2String2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            SubscriptionInfo subscriptionInfo = (SubscriptionInfo)SubscriptionInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (SubscriptionInfo)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<SubscriptionInfo> getActiveSubscriptionInfoList(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null)
            return ISub.Stub.getDefaultImpl().getActiveSubscriptionInfoList(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.createTypedArrayList(SubscriptionInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getActiveSubInfoCount(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null)
            return ISub.Stub.getDefaultImpl().getActiveSubInfoCount(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getActiveSubInfoCountMax() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null)
            return ISub.Stub.getDefaultImpl().getActiveSubInfoCountMax(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<SubscriptionInfo> getAvailableSubscriptionInfoList(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null)
            return ISub.Stub.getDefaultImpl().getAvailableSubscriptionInfoList(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.createTypedArrayList(SubscriptionInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<SubscriptionInfo> getAccessibleSubscriptionInfoList(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null)
            return ISub.Stub.getDefaultImpl().getAccessibleSubscriptionInfoList(param2String); 
          parcel2.readException();
          return parcel2.createTypedArrayList(SubscriptionInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestEmbeddedSubscriptionInfoListRefresh(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(11, parcel, null, 1);
          if (!bool && ISub.Stub.getDefaultImpl() != null) {
            ISub.Stub.getDefaultImpl().requestEmbeddedSubscriptionInfoListRefresh(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public int addSubInfoRecord(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null) {
            param2Int = ISub.Stub.getDefaultImpl().addSubInfoRecord(param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int addSubInfo(String param2String1, String param2String2, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null) {
            param2Int1 = ISub.Stub.getDefaultImpl().addSubInfo(param2String1, param2String2, param2Int1, param2Int2);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int removeSubInfo(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null) {
            param2Int = ISub.Stub.getDefaultImpl().removeSubInfo(param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setIconTint(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null) {
            param2Int1 = ISub.Stub.getDefaultImpl().setIconTint(param2Int1, param2Int2);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setDisplayNameUsingSrc(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null) {
            param2Int1 = ISub.Stub.getDefaultImpl().setDisplayNameUsingSrc(param2String, param2Int1, param2Int2);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setDisplayNumber(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null) {
            param2Int = ISub.Stub.getDefaultImpl().setDisplayNumber(param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setDataRoaming(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null) {
            param2Int1 = ISub.Stub.getDefaultImpl().setDataRoaming(param2Int1, param2Int2);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setOpportunistic(boolean param2Boolean, int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool1 = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool1 && ISub.Stub.getDefaultImpl() != null) {
            param2Int = ISub.Stub.getDefaultImpl().setOpportunistic(param2Boolean, param2Int, param2String);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParcelUuid createSubscriptionGroup(int[] param2ArrayOfint, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeIntArray(param2ArrayOfint);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null)
            return ISub.Stub.getDefaultImpl().createSubscriptionGroup(param2ArrayOfint, param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParcelUuid parcelUuid = (ParcelUuid)ParcelUuid.CREATOR.createFromParcel(parcel2);
          } else {
            param2ArrayOfint = null;
          } 
          return (ParcelUuid)param2ArrayOfint;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPreferredDataSubscriptionId(int param2Int, boolean param2Boolean, ISetOpportunisticDataCallback param2ISetOpportunisticDataCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          if (param2ISetOpportunisticDataCallback != null) {
            iBinder = param2ISetOpportunisticDataCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool1 = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool1 && ISub.Stub.getDefaultImpl() != null) {
            ISub.Stub.getDefaultImpl().setPreferredDataSubscriptionId(param2Int, param2Boolean, param2ISetOpportunisticDataCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getPreferredDataSubscriptionId() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null)
            return ISub.Stub.getDefaultImpl().getPreferredDataSubscriptionId(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<SubscriptionInfo> getOpportunisticSubscriptions(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null)
            return ISub.Stub.getDefaultImpl().getOpportunisticSubscriptions(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.createTypedArrayList(SubscriptionInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeSubscriptionsFromGroup(int[] param2ArrayOfint, ParcelUuid param2ParcelUuid, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeIntArray(param2ArrayOfint);
          if (param2ParcelUuid != null) {
            parcel1.writeInt(1);
            param2ParcelUuid.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null) {
            ISub.Stub.getDefaultImpl().removeSubscriptionsFromGroup(param2ArrayOfint, param2ParcelUuid, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addSubscriptionsIntoGroup(int[] param2ArrayOfint, ParcelUuid param2ParcelUuid, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeIntArray(param2ArrayOfint);
          if (param2ParcelUuid != null) {
            parcel1.writeInt(1);
            param2ParcelUuid.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null) {
            ISub.Stub.getDefaultImpl().addSubscriptionsIntoGroup(param2ArrayOfint, param2ParcelUuid, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<SubscriptionInfo> getSubscriptionsInGroup(ParcelUuid param2ParcelUuid, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          if (param2ParcelUuid != null) {
            parcel1.writeInt(1);
            param2ParcelUuid.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null)
            return ISub.Stub.getDefaultImpl().getSubscriptionsInGroup(param2ParcelUuid, param2String1, param2String2); 
          parcel2.readException();
          return parcel2.createTypedArrayList(SubscriptionInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getSlotIndex(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null) {
            param2Int = ISub.Stub.getDefaultImpl().getSlotIndex(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] getSubId(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null)
            return ISub.Stub.getDefaultImpl().getSubId(param2Int); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getDefaultSubId() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null)
            return ISub.Stub.getDefaultImpl().getDefaultSubId(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int clearSubInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null)
            return ISub.Stub.getDefaultImpl().clearSubInfo(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getPhoneId(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null) {
            param2Int = ISub.Stub.getDefaultImpl().getPhoneId(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getDefaultDataSubId() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null)
            return ISub.Stub.getDefaultImpl().getDefaultDataSubId(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDefaultDataSubId(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null) {
            ISub.Stub.getDefaultImpl().setDefaultDataSubId(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getDefaultVoiceSubId() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          boolean bool = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null)
            return ISub.Stub.getDefaultImpl().getDefaultVoiceSubId(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDefaultVoiceSubId(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null) {
            ISub.Stub.getDefaultImpl().setDefaultVoiceSubId(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getDefaultSmsSubId() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null)
            return ISub.Stub.getDefaultImpl().getDefaultSmsSubId(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDefaultSmsSubId(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null) {
            ISub.Stub.getDefaultImpl().setDefaultSmsSubId(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] getActiveSubIdList(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(38, parcel1, parcel2, 0);
          if (!bool1 && ISub.Stub.getDefaultImpl() != null)
            return ISub.Stub.getDefaultImpl().getActiveSubIdList(param2Boolean); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setSubscriptionProperty(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null) {
            param2Int = ISub.Stub.getDefaultImpl().setSubscriptionProperty(param2Int, param2String1, param2String2);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getSubscriptionProperty(int param2Int, String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(40, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null) {
            param2String1 = ISub.Stub.getDefaultImpl().getSubscriptionProperty(param2Int, param2String1, param2String2, param2String3);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setSubscriptionEnabled(boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          boolean bool1 = true;
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(41, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null) {
            param2Boolean = ISub.Stub.getDefaultImpl().setSubscriptionEnabled(param2Boolean, param2Int);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isSubscriptionEnabled(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(42, parcel1, parcel2, 0);
          if (!bool2 && ISub.Stub.getDefaultImpl() != null) {
            bool1 = ISub.Stub.getDefaultImpl().isSubscriptionEnabled(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getEnabledSubscriptionId(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(43, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null) {
            param2Int = ISub.Stub.getDefaultImpl().getEnabledSubscriptionId(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getSimStateForSlotIndex(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(44, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null) {
            param2Int = ISub.Stub.getDefaultImpl().getSimStateForSlotIndex(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isActiveSubId(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(45, parcel1, parcel2, 0);
          if (!bool2 && ISub.Stub.getDefaultImpl() != null) {
            bool1 = ISub.Stub.getDefaultImpl().isActiveSubId(param2Int, param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getActiveDataSubscriptionId() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          boolean bool = this.mRemote.transact(46, parcel1, parcel2, 0);
          if (!bool && ISub.Stub.getDefaultImpl() != null)
            return ISub.Stub.getDefaultImpl().getActiveDataSubscriptionId(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean canDisablePhysicalSubscription() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(47, parcel1, parcel2, 0);
          if (!bool2 && ISub.Stub.getDefaultImpl() != null) {
            bool1 = ISub.Stub.getDefaultImpl().canDisablePhysicalSubscription();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setUiccApplicationsEnabled(boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ISub");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(48, parcel1, parcel2, 0);
          if (!bool1 && ISub.Stub.getDefaultImpl() != null) {
            param2Int = ISub.Stub.getDefaultImpl().setUiccApplicationsEnabled(param2Boolean, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISub param1ISub) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISub != null) {
          Proxy.sDefaultImpl = param1ISub;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISub getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
