package com.android.internal.telephony;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

public final class HbpcdUtils {
  private static final boolean DBG = false;
  
  private static final String LOG_TAG = "HbpcdUtils";
  
  private ContentResolver resolver = null;
  
  public HbpcdUtils(Context paramContext) {
    this.resolver = paramContext.getContentResolver();
  }
  
  public int getMcc(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean) {
    ContentResolver contentResolver2 = this.resolver;
    Uri uri3 = HbpcdLookup.ArbitraryMccSidMatch.CONTENT_URI;
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append("SID=");
    stringBuilder3.append(paramInt1);
    String str3 = stringBuilder3.toString();
    Cursor cursor2 = contentResolver2.query(uri3, new String[] { "MCC" }, str3, null, null);
    if (cursor2 != null) {
      int i = cursor2.getCount();
      if (i == 1) {
        cursor2.moveToFirst();
        paramInt1 = cursor2.getInt(0);
        cursor2.close();
        return paramInt1;
      } 
      cursor2.close();
    } 
    ContentResolver contentResolver3 = this.resolver;
    Uri uri1 = HbpcdLookup.MccSidConflicts.CONTENT_URI;
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("SID_Conflict=");
    stringBuilder2.append(paramInt1);
    stringBuilder2.append(" and (((");
    stringBuilder2.append("GMT_Offset_Low");
    stringBuilder2.append("<=");
    stringBuilder2.append(paramInt2);
    stringBuilder2.append(") and (");
    stringBuilder2.append(paramInt2);
    stringBuilder2.append("<=");
    stringBuilder2.append("GMT_Offset_High");
    stringBuilder2.append(") and (0=");
    stringBuilder2.append(paramInt3);
    stringBuilder2.append(")) or ((");
    stringBuilder2.append("GMT_DST_Low");
    stringBuilder2.append("<=");
    stringBuilder2.append(paramInt2);
    stringBuilder2.append(") and (");
    stringBuilder2.append(paramInt2);
    stringBuilder2.append("<=");
    stringBuilder2.append("GMT_DST_High");
    stringBuilder2.append(") and (1=");
    stringBuilder2.append(paramInt3);
    stringBuilder2.append(")))");
    String str2 = stringBuilder2.toString();
    Cursor cursor3 = contentResolver3.query(uri1, new String[] { "MCC" }, str2, null, null);
    if (cursor3 != null) {
      paramInt2 = cursor3.getCount();
      if (paramInt2 > 0) {
        if (paramInt2 > 1) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("something wrong, get more results for 1 conflict SID: ");
          stringBuilder.append(cursor3);
          Log.w("HbpcdUtils", stringBuilder.toString());
        } 
        cursor3.moveToFirst();
        paramInt1 = cursor3.getInt(0);
        if (!paramBoolean)
          paramInt1 = 0; 
        cursor3.close();
        return paramInt1;
      } 
      cursor3.close();
    } 
    ContentResolver contentResolver1 = this.resolver;
    Uri uri2 = HbpcdLookup.MccSidRange.CONTENT_URI;
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("SID_Range_Low<=");
    stringBuilder1.append(paramInt1);
    stringBuilder1.append(" and ");
    stringBuilder1.append("SID_Range_High");
    stringBuilder1.append(">=");
    stringBuilder1.append(paramInt1);
    String str1 = stringBuilder1.toString();
    Cursor cursor1 = contentResolver1.query(uri2, new String[] { "MCC" }, str1, null, null);
    if (cursor1 != null) {
      if (cursor1.getCount() > 0) {
        cursor1.moveToFirst();
        paramInt1 = cursor1.getInt(0);
        cursor1.close();
        return paramInt1;
      } 
      cursor1.close();
    } 
    return 0;
  }
  
  public String getIddByMcc(int paramInt) {
    String str1 = "";
    ContentResolver contentResolver = this.resolver;
    Uri uri = HbpcdLookup.MccIdd.CONTENT_URI;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("MCC=");
    stringBuilder.append(paramInt);
    String str3 = stringBuilder.toString();
    Cursor cursor = contentResolver.query(uri, new String[] { "IDD" }, str3, null, null);
    String str2 = str1;
    if (cursor != null) {
      if (cursor.getCount() > 0) {
        cursor.moveToFirst();
        str1 = cursor.getString(0);
      } 
      cursor.close();
      str2 = str1;
    } 
    if (false)
      throw new NullPointerException(); 
    return str2;
  }
}
