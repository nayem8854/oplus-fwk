package com.android.internal.telephony;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface INumberVerificationCallback extends IInterface {
  void onCallReceived(String paramString) throws RemoteException;
  
  void onVerificationFailed(int paramInt) throws RemoteException;
  
  class Default implements INumberVerificationCallback {
    public void onCallReceived(String param1String) throws RemoteException {}
    
    public void onVerificationFailed(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INumberVerificationCallback {
    private static final String DESCRIPTOR = "com.android.internal.telephony.INumberVerificationCallback";
    
    static final int TRANSACTION_onCallReceived = 1;
    
    static final int TRANSACTION_onVerificationFailed = 2;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.INumberVerificationCallback");
    }
    
    public static INumberVerificationCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.INumberVerificationCallback");
      if (iInterface != null && iInterface instanceof INumberVerificationCallback)
        return (INumberVerificationCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onVerificationFailed";
      } 
      return "onCallReceived";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("com.android.internal.telephony.INumberVerificationCallback");
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.internal.telephony.INumberVerificationCallback");
        param1Int1 = param1Parcel1.readInt();
        onVerificationFailed(param1Int1);
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.telephony.INumberVerificationCallback");
      String str = param1Parcel1.readString();
      onCallReceived(str);
      return true;
    }
    
    private static class Proxy implements INumberVerificationCallback {
      public static INumberVerificationCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.INumberVerificationCallback";
      }
      
      public void onCallReceived(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.INumberVerificationCallback");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && INumberVerificationCallback.Stub.getDefaultImpl() != null) {
            INumberVerificationCallback.Stub.getDefaultImpl().onCallReceived(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onVerificationFailed(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.INumberVerificationCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && INumberVerificationCallback.Stub.getDefaultImpl() != null) {
            INumberVerificationCallback.Stub.getDefaultImpl().onVerificationFailed(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INumberVerificationCallback param1INumberVerificationCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INumberVerificationCallback != null) {
          Proxy.sDefaultImpl = param1INumberVerificationCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INumberVerificationCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
