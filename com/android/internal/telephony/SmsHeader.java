package com.android.internal.telephony;

import com.android.internal.util.HexDump;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class SmsHeader {
  public boolean equals(Object<MiscElt> paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.languageTable == ((SmsHeader)paramObject).languageTable && this.languageShiftTable == ((SmsHeader)paramObject).languageShiftTable) {
      PortAddrs portAddrs1 = this.portAddrs, portAddrs2 = ((SmsHeader)paramObject).portAddrs;
      if (Objects.equals(portAddrs1, portAddrs2)) {
        ConcatRef concatRef1 = this.concatRef, concatRef2 = ((SmsHeader)paramObject).concatRef;
        if (Objects.equals(concatRef1, concatRef2)) {
          ArrayList<SpecialSmsMsg> arrayList1 = this.specialSmsMsgList, arrayList2 = ((SmsHeader)paramObject).specialSmsMsgList;
          if (Objects.equals(arrayList1, arrayList2)) {
            ArrayList<MiscElt> arrayList = this.miscEltList;
            paramObject = (Object<MiscElt>)((SmsHeader)paramObject).miscEltList;
            if (Objects.equals(arrayList, paramObject))
              return null; 
          } 
        } 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    PortAddrs portAddrs = this.portAddrs;
    ConcatRef concatRef = this.concatRef;
    ArrayList<SpecialSmsMsg> arrayList = this.specialSmsMsgList;
    ArrayList<MiscElt> arrayList1 = this.miscEltList;
    int i = this.languageTable, j = this.languageShiftTable;
    return Objects.hash(new Object[] { portAddrs, concatRef, arrayList, arrayList1, Integer.valueOf(i), Integer.valueOf(j) });
  }
  
  public static class PortAddrs {
    public boolean areEightBits;
    
    public int destPort;
    
    public int origPort;
    
    public boolean equals(Object param1Object) {
      boolean bool = true;
      if (this == param1Object)
        return true; 
      if (param1Object == null || getClass() != param1Object.getClass())
        return false; 
      param1Object = param1Object;
      if (this.destPort != ((PortAddrs)param1Object).destPort || this.origPort != ((PortAddrs)param1Object).origPort || this.areEightBits != ((PortAddrs)param1Object).areEightBits)
        bool = false; 
      return bool;
    }
    
    public int hashCode() {
      return Objects.hash(new Object[] { Integer.valueOf(this.destPort), Integer.valueOf(this.origPort), Boolean.valueOf(this.areEightBits) });
    }
  }
  
  public static class ConcatRef {
    public boolean isEightBits;
    
    public int msgCount;
    
    public int refNumber;
    
    public int seqNumber;
    
    public boolean equals(Object param1Object) {
      boolean bool = true;
      if (this == param1Object)
        return true; 
      if (param1Object == null || getClass() != param1Object.getClass())
        return false; 
      param1Object = param1Object;
      if (this.refNumber != ((ConcatRef)param1Object).refNumber || this.seqNumber != ((ConcatRef)param1Object).seqNumber || this.msgCount != ((ConcatRef)param1Object).msgCount || this.isEightBits != ((ConcatRef)param1Object).isEightBits)
        bool = false; 
      return bool;
    }
    
    public int hashCode() {
      return Objects.hash(new Object[] { Integer.valueOf(this.refNumber), Integer.valueOf(this.seqNumber), Integer.valueOf(this.msgCount), Boolean.valueOf(this.isEightBits) });
    }
  }
  
  public static class SpecialSmsMsg {
    public int msgCount;
    
    public int msgIndType;
    
    public boolean equals(Object param1Object) {
      boolean bool = true;
      if (this == param1Object)
        return true; 
      if (param1Object == null || getClass() != param1Object.getClass())
        return false; 
      param1Object = param1Object;
      if (this.msgIndType != ((SpecialSmsMsg)param1Object).msgIndType || this.msgCount != ((SpecialSmsMsg)param1Object).msgCount)
        bool = false; 
      return bool;
    }
    
    public int hashCode() {
      return Objects.hash(new Object[] { Integer.valueOf(this.msgIndType), Integer.valueOf(this.msgCount) });
    }
  }
  
  public static class MiscElt {
    public byte[] data;
    
    public int id;
    
    public boolean equals(Object param1Object) {
      null = true;
      if (this == param1Object)
        return true; 
      if (param1Object == null || getClass() != param1Object.getClass())
        return false; 
      MiscElt miscElt = (MiscElt)param1Object;
      if (this.id == miscElt.id) {
        param1Object = this.data;
        byte[] arrayOfByte = miscElt.data;
        if (Arrays.equals((byte[])param1Object, arrayOfByte))
          return null; 
      } 
      return false;
    }
    
    public int hashCode() {
      int i = Objects.hash(new Object[] { Integer.valueOf(this.id) });
      int j = Arrays.hashCode(this.data);
      return i * 31 + j;
    }
  }
  
  public ArrayList<SpecialSmsMsg> specialSmsMsgList = new ArrayList<>();
  
  public PortAddrs portAddrs;
  
  public ArrayList<MiscElt> miscEltList = new ArrayList<>();
  
  public int languageTable;
  
  public int languageShiftTable;
  
  public ConcatRef concatRef;
  
  public static final int PORT_WAP_WSP = 9200;
  
  public static final int PORT_WAP_PUSH = 2948;
  
  public static final int ELT_ID_WIRELESS_CTRL_MSG_PROTOCOL = 9;
  
  public static final int ELT_ID_VARIABLE_PICTURE = 18;
  
  public static final int ELT_ID_USER_PROMPT_INDICATOR = 19;
  
  public static final int ELT_ID_USER_DEFINED_SOUND = 12;
  
  public static final int ELT_ID_UDH_SOURCE_INDICATION = 7;
  
  public static final int ELT_ID_TEXT_FORMATTING = 10;
  
  public static final int ELT_ID_STANDARD_WVG_OBJECT = 24;
  
  public static final int ELT_ID_SPECIAL_SMS_MESSAGE_INDICATION = 1;
  
  public static final int ELT_ID_SMSC_CONTROL_PARAMS = 6;
  
  public static final int ELT_ID_SMALL_PICTURE = 17;
  
  public static final int ELT_ID_SMALL_ANIMATION = 15;
  
  public static final int ELT_ID_RFC_822_EMAIL_HEADER = 32;
  
  public static final int ELT_ID_REUSED_EXTENDED_OBJECT = 21;
  
  public static final int ELT_ID_REPLY_ADDRESS_ELEMENT = 34;
  
  public static final int ELT_ID_PREDEFINED_SOUND = 11;
  
  public static final int ELT_ID_PREDEFINED_ANIMATION = 13;
  
  public static final int ELT_ID_OBJECT_DISTR_INDICATOR = 23;
  
  public static final int ELT_ID_NATIONAL_LANGUAGE_SINGLE_SHIFT = 36;
  
  public static final int ELT_ID_NATIONAL_LANGUAGE_LOCKING_SHIFT = 37;
  
  public static final int ELT_ID_LARGE_PICTURE = 16;
  
  public static final int ELT_ID_LARGE_ANIMATION = 14;
  
  public static final int ELT_ID_HYPERLINK_FORMAT_ELEMENT = 33;
  
  public static final int ELT_ID_EXTENDED_OBJECT_DATA_REQUEST_CMD = 26;
  
  public static final int ELT_ID_EXTENDED_OBJECT = 20;
  
  public static final int ELT_ID_ENHANCED_VOICE_MAIL_INFORMATION = 35;
  
  public static final int ELT_ID_CONCATENATED_8_BIT_REFERENCE = 0;
  
  public static final int ELT_ID_CONCATENATED_16_BIT_REFERENCE = 8;
  
  public static final int ELT_ID_COMPRESSION_CONTROL = 22;
  
  public static final int ELT_ID_CHARACTER_SIZE_WVG_OBJECT = 25;
  
  public static final int ELT_ID_APPLICATION_PORT_ADDRESSING_8_BIT = 4;
  
  public static final int ELT_ID_APPLICATION_PORT_ADDRESSING_16_BIT = 5;
  
  public static SmsHeader fromByteArray(byte[] paramArrayOfbyte) {
    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(paramArrayOfbyte);
    SmsHeader smsHeader = new SmsHeader();
    while (byteArrayInputStream.available() > 0) {
      int i = byteArrayInputStream.read();
      int j = byteArrayInputStream.read();
      if (i != 0) {
        if (i != 1) {
          if (i != 4) {
            if (i != 5) {
              if (i != 8) {
                if (i != 36) {
                  if (i != 37) {
                    MiscElt miscElt = new MiscElt();
                    miscElt.id = i;
                    miscElt.data = new byte[j];
                    byteArrayInputStream.read(miscElt.data, 0, j);
                    smsHeader.miscEltList.add(miscElt);
                    continue;
                  } 
                  smsHeader.languageTable = byteArrayInputStream.read();
                  continue;
                } 
                smsHeader.languageShiftTable = byteArrayInputStream.read();
                continue;
              } 
              ConcatRef concatRef1 = new ConcatRef();
              concatRef1.refNumber = byteArrayInputStream.read() << 8 | byteArrayInputStream.read();
              concatRef1.msgCount = byteArrayInputStream.read();
              concatRef1.seqNumber = byteArrayInputStream.read();
              concatRef1.isEightBits = false;
              if (concatRef1.msgCount != 0 && concatRef1.seqNumber != 0 && concatRef1.seqNumber <= concatRef1.msgCount)
                smsHeader.concatRef = concatRef1; 
              continue;
            } 
            PortAddrs portAddrs1 = new PortAddrs();
            portAddrs1.destPort = byteArrayInputStream.read() << 8 | byteArrayInputStream.read();
            portAddrs1.origPort = byteArrayInputStream.read() << 8 | byteArrayInputStream.read();
            portAddrs1.areEightBits = false;
            smsHeader.portAddrs = portAddrs1;
            continue;
          } 
          PortAddrs portAddrs = new PortAddrs();
          portAddrs.destPort = byteArrayInputStream.read();
          portAddrs.origPort = byteArrayInputStream.read();
          portAddrs.areEightBits = true;
          smsHeader.portAddrs = portAddrs;
          continue;
        } 
        SpecialSmsMsg specialSmsMsg = new SpecialSmsMsg();
        specialSmsMsg.msgIndType = byteArrayInputStream.read();
        specialSmsMsg.msgCount = byteArrayInputStream.read();
        smsHeader.specialSmsMsgList.add(specialSmsMsg);
        continue;
      } 
      ConcatRef concatRef = new ConcatRef();
      concatRef.refNumber = byteArrayInputStream.read();
      concatRef.msgCount = byteArrayInputStream.read();
      concatRef.seqNumber = byteArrayInputStream.read();
      concatRef.isEightBits = true;
      if (concatRef.msgCount != 0 && concatRef.seqNumber != 0 && concatRef.seqNumber <= concatRef.msgCount)
        smsHeader.concatRef = concatRef; 
    } 
    return smsHeader;
  }
  
  public static byte[] toByteArray(SmsHeader paramSmsHeader) {
    if (paramSmsHeader.portAddrs == null && paramSmsHeader.concatRef == null) {
      ArrayList<SpecialSmsMsg> arrayList = paramSmsHeader.specialSmsMsgList;
      if (arrayList.isEmpty()) {
        ArrayList<MiscElt> arrayList1 = paramSmsHeader.miscEltList;
        if (arrayList1.isEmpty() && paramSmsHeader.languageShiftTable == 0 && paramSmsHeader.languageTable == 0)
          return null; 
      } 
    } 
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(140);
    ConcatRef concatRef = paramSmsHeader.concatRef;
    if (concatRef != null) {
      if (concatRef.isEightBits) {
        byteArrayOutputStream.write(0);
        byteArrayOutputStream.write(3);
        byteArrayOutputStream.write(concatRef.refNumber);
      } else {
        byteArrayOutputStream.write(8);
        byteArrayOutputStream.write(4);
        byteArrayOutputStream.write(concatRef.refNumber >>> 8);
        byteArrayOutputStream.write(concatRef.refNumber & 0xFF);
      } 
      byteArrayOutputStream.write(concatRef.msgCount);
      byteArrayOutputStream.write(concatRef.seqNumber);
    } 
    PortAddrs portAddrs = paramSmsHeader.portAddrs;
    if (portAddrs != null)
      if (portAddrs.areEightBits) {
        byteArrayOutputStream.write(4);
        byteArrayOutputStream.write(2);
        byteArrayOutputStream.write(portAddrs.destPort);
        byteArrayOutputStream.write(portAddrs.origPort);
      } else {
        byteArrayOutputStream.write(5);
        byteArrayOutputStream.write(4);
        byteArrayOutputStream.write(portAddrs.destPort >>> 8);
        byteArrayOutputStream.write(portAddrs.destPort & 0xFF);
        byteArrayOutputStream.write(portAddrs.origPort >>> 8);
        byteArrayOutputStream.write(portAddrs.origPort & 0xFF);
      }  
    if (paramSmsHeader.languageShiftTable != 0) {
      byteArrayOutputStream.write(36);
      byteArrayOutputStream.write(1);
      byteArrayOutputStream.write(paramSmsHeader.languageShiftTable);
    } 
    if (paramSmsHeader.languageTable != 0) {
      byteArrayOutputStream.write(37);
      byteArrayOutputStream.write(1);
      byteArrayOutputStream.write(paramSmsHeader.languageTable);
    } 
    for (SpecialSmsMsg specialSmsMsg : paramSmsHeader.specialSmsMsgList) {
      byteArrayOutputStream.write(1);
      byteArrayOutputStream.write(2);
      byteArrayOutputStream.write(specialSmsMsg.msgIndType & 0xFF);
      byteArrayOutputStream.write(specialSmsMsg.msgCount & 0xFF);
    } 
    for (MiscElt miscElt : paramSmsHeader.miscEltList) {
      byteArrayOutputStream.write(miscElt.id);
      byteArrayOutputStream.write(miscElt.data.length);
      byteArrayOutputStream.write(miscElt.data, 0, miscElt.data.length);
    } 
    return byteArrayOutputStream.toByteArray();
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("UserDataHeader ");
    stringBuilder.append("{ ConcatRef ");
    if (this.concatRef == null) {
      stringBuilder.append("unset");
    } else {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("{ refNumber=");
      stringBuilder1.append(this.concatRef.refNumber);
      stringBuilder.append(stringBuilder1.toString());
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append(", msgCount=");
      stringBuilder1.append(this.concatRef.msgCount);
      stringBuilder.append(stringBuilder1.toString());
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append(", seqNumber=");
      stringBuilder1.append(this.concatRef.seqNumber);
      stringBuilder.append(stringBuilder1.toString());
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append(", isEightBits=");
      stringBuilder1.append(this.concatRef.isEightBits);
      stringBuilder.append(stringBuilder1.toString());
      stringBuilder.append(" }");
    } 
    stringBuilder.append(", PortAddrs ");
    if (this.portAddrs == null) {
      stringBuilder.append("unset");
    } else {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("{ destPort=");
      stringBuilder1.append(this.portAddrs.destPort);
      stringBuilder.append(stringBuilder1.toString());
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append(", origPort=");
      stringBuilder1.append(this.portAddrs.origPort);
      stringBuilder.append(stringBuilder1.toString());
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append(", areEightBits=");
      stringBuilder1.append(this.portAddrs.areEightBits);
      stringBuilder.append(stringBuilder1.toString());
      stringBuilder.append(" }");
    } 
    if (this.languageShiftTable != 0) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(", languageShiftTable=");
      stringBuilder1.append(this.languageShiftTable);
      stringBuilder.append(stringBuilder1.toString());
    } 
    if (this.languageTable != 0) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(", languageTable=");
      stringBuilder1.append(this.languageTable);
      stringBuilder.append(stringBuilder1.toString());
    } 
    for (SpecialSmsMsg specialSmsMsg : this.specialSmsMsgList) {
      stringBuilder.append(", SpecialSmsMsg ");
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("{ msgIndType=");
      stringBuilder1.append(specialSmsMsg.msgIndType);
      stringBuilder.append(stringBuilder1.toString());
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append(", msgCount=");
      stringBuilder1.append(specialSmsMsg.msgCount);
      stringBuilder.append(stringBuilder1.toString());
      stringBuilder.append(" }");
    } 
    for (MiscElt miscElt : this.miscEltList) {
      stringBuilder.append(", MiscElt ");
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("{ id=");
      stringBuilder1.append(miscElt.id);
      stringBuilder.append(stringBuilder1.toString());
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append(", length=");
      stringBuilder1.append(miscElt.data.length);
      stringBuilder.append(stringBuilder1.toString());
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append(", data=");
      stringBuilder1.append(HexDump.toHexString(miscElt.data));
      stringBuilder.append(stringBuilder1.toString());
      stringBuilder.append(" }");
    } 
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
}
