package com.android.internal.telephony;

import android.app.PendingIntent;
import android.net.Uri;
import android.os.Bundle;
import java.util.List;

public class ISmsImplBase extends ISms.Stub {
  public List<SmsRawData> getAllMessagesFromIccEfForSubscriber(int paramInt, String paramString) {
    throw new UnsupportedOperationException();
  }
  
  public boolean updateMessageOnIccEfForSubscriber(int paramInt1, String paramString, int paramInt2, int paramInt3, byte[] paramArrayOfbyte) {
    throw new UnsupportedOperationException();
  }
  
  public boolean copyMessageToIccEfForSubscriber(int paramInt1, String paramString, int paramInt2, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) {
    throw new UnsupportedOperationException();
  }
  
  public void sendDataForSubscriber(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, int paramInt2, byte[] paramArrayOfbyte, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2) {
    throw new UnsupportedOperationException();
  }
  
  public void sendTextForSubscriber(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, boolean paramBoolean, long paramLong) {
    throw new UnsupportedOperationException();
  }
  
  public void sendTextForSubscriberWithOptions(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, boolean paramBoolean1, int paramInt2, boolean paramBoolean2, int paramInt3) {
    throw new UnsupportedOperationException();
  }
  
  public void injectSmsPduForSubscriber(int paramInt, byte[] paramArrayOfbyte, String paramString, PendingIntent paramPendingIntent) {
    throw new UnsupportedOperationException();
  }
  
  public void sendMultipartTextForSubscriber(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4, List<String> paramList, List<PendingIntent> paramList1, List<PendingIntent> paramList2, boolean paramBoolean, long paramLong) {
    throw new UnsupportedOperationException();
  }
  
  public void sendMultipartTextForSubscriberWithOptions(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, List<String> paramList, List<PendingIntent> paramList1, List<PendingIntent> paramList2, boolean paramBoolean1, int paramInt2, boolean paramBoolean2, int paramInt3) {
    throw new UnsupportedOperationException();
  }
  
  public boolean enableCellBroadcastForSubscriber(int paramInt1, int paramInt2, int paramInt3) {
    throw new UnsupportedOperationException();
  }
  
  public boolean disableCellBroadcastForSubscriber(int paramInt1, int paramInt2, int paramInt3) {
    throw new UnsupportedOperationException();
  }
  
  public boolean enableCellBroadcastRangeForSubscriber(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    throw new UnsupportedOperationException();
  }
  
  public boolean disableCellBroadcastRangeForSubscriber(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    throw new UnsupportedOperationException();
  }
  
  public int getPremiumSmsPermission(String paramString) {
    throw new UnsupportedOperationException();
  }
  
  public int getPremiumSmsPermissionForSubscriber(int paramInt, String paramString) {
    throw new UnsupportedOperationException();
  }
  
  public void setPremiumSmsPermission(String paramString, int paramInt) {
    throw new UnsupportedOperationException();
  }
  
  public void setPremiumSmsPermissionForSubscriber(int paramInt1, String paramString, int paramInt2) {
    throw new UnsupportedOperationException();
  }
  
  public boolean isImsSmsSupportedForSubscriber(int paramInt) {
    throw new UnsupportedOperationException();
  }
  
  public boolean isSmsSimPickActivityNeeded(int paramInt) {
    throw new UnsupportedOperationException();
  }
  
  public int getPreferredSmsSubscription() {
    throw new UnsupportedOperationException();
  }
  
  public String getImsSmsFormatForSubscriber(int paramInt) {
    throw new UnsupportedOperationException();
  }
  
  public boolean isSMSPromptEnabled() {
    throw new UnsupportedOperationException();
  }
  
  public void sendStoredText(int paramInt, String paramString1, String paramString2, Uri paramUri, String paramString3, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2) {
    throw new UnsupportedOperationException();
  }
  
  public void sendStoredMultipartText(int paramInt, String paramString1, String paramString2, Uri paramUri, String paramString3, List<PendingIntent> paramList1, List<PendingIntent> paramList2) {
    throw new UnsupportedOperationException();
  }
  
  public Bundle getCarrierConfigValuesForSubscriber(int paramInt) {
    throw new UnsupportedOperationException();
  }
  
  public String createAppSpecificSmsToken(int paramInt, String paramString, PendingIntent paramPendingIntent) {
    throw new UnsupportedOperationException();
  }
  
  public String createAppSpecificSmsTokenWithPackageInfo(int paramInt, String paramString1, String paramString2, PendingIntent paramPendingIntent) {
    throw new UnsupportedOperationException();
  }
  
  public int checkSmsShortCodeDestination(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4) {
    throw new UnsupportedOperationException();
  }
  
  public String getSmscAddressFromIccEfForSubscriber(int paramInt, String paramString) {
    throw new UnsupportedOperationException();
  }
  
  public boolean setSmscAddressOnIccEfForSubscriber(String paramString1, int paramInt, String paramString2) {
    throw new UnsupportedOperationException();
  }
  
  public int getSmsCapacityOnIccForSubscriber(int paramInt) {
    throw new UnsupportedOperationException();
  }
  
  public boolean resetAllCellBroadcastRanges(int paramInt) {
    throw new UnsupportedOperationException();
  }
}
