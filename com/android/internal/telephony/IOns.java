package com.android.internal.telephony;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telephony.AvailableNetworkInfo;
import java.util.ArrayList;
import java.util.List;

public interface IOns extends IInterface {
  int getPreferredDataSubscriptionId(String paramString1, String paramString2) throws RemoteException;
  
  boolean isEnabled(String paramString) throws RemoteException;
  
  boolean setEnable(boolean paramBoolean, String paramString) throws RemoteException;
  
  void setPreferredDataSubscriptionId(int paramInt, boolean paramBoolean, ISetOpportunisticDataCallback paramISetOpportunisticDataCallback, String paramString) throws RemoteException;
  
  void updateAvailableNetworks(List<AvailableNetworkInfo> paramList, IUpdateAvailableNetworksCallback paramIUpdateAvailableNetworksCallback, String paramString) throws RemoteException;
  
  class Default implements IOns {
    public boolean setEnable(boolean param1Boolean, String param1String) throws RemoteException {
      return false;
    }
    
    public boolean isEnabled(String param1String) throws RemoteException {
      return false;
    }
    
    public void setPreferredDataSubscriptionId(int param1Int, boolean param1Boolean, ISetOpportunisticDataCallback param1ISetOpportunisticDataCallback, String param1String) throws RemoteException {}
    
    public int getPreferredDataSubscriptionId(String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public void updateAvailableNetworks(List<AvailableNetworkInfo> param1List, IUpdateAvailableNetworksCallback param1IUpdateAvailableNetworksCallback, String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOns {
    private static final String DESCRIPTOR = "com.android.internal.telephony.IOns";
    
    static final int TRANSACTION_getPreferredDataSubscriptionId = 4;
    
    static final int TRANSACTION_isEnabled = 2;
    
    static final int TRANSACTION_setEnable = 1;
    
    static final int TRANSACTION_setPreferredDataSubscriptionId = 3;
    
    static final int TRANSACTION_updateAvailableNetworks = 5;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.IOns");
    }
    
    public static IOns asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.IOns");
      if (iInterface != null && iInterface instanceof IOns)
        return (IOns)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "updateAvailableNetworks";
            } 
            return "getPreferredDataSubscriptionId";
          } 
          return "setPreferredDataSubscriptionId";
        } 
        return "isEnabled";
      } 
      return "setEnable";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool1 = false, bool2 = false;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("com.android.internal.telephony.IOns");
                return true;
              } 
              param1Parcel1.enforceInterface("com.android.internal.telephony.IOns");
              ArrayList<AvailableNetworkInfo> arrayList = param1Parcel1.createTypedArrayList(AvailableNetworkInfo.CREATOR);
              IUpdateAvailableNetworksCallback iUpdateAvailableNetworksCallback = IUpdateAvailableNetworksCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
              str = param1Parcel1.readString();
              updateAvailableNetworks(arrayList, iUpdateAvailableNetworksCallback, str);
              param1Parcel2.writeNoException();
              return true;
            } 
            str.enforceInterface("com.android.internal.telephony.IOns");
            String str1 = str.readString();
            str = str.readString();
            param1Int1 = getPreferredDataSubscriptionId(str1, str);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          } 
          str.enforceInterface("com.android.internal.telephony.IOns");
          param1Int1 = str.readInt();
          bool1 = bool2;
          if (str.readInt() != 0)
            bool1 = true; 
          ISetOpportunisticDataCallback iSetOpportunisticDataCallback = ISetOpportunisticDataCallback.Stub.asInterface(str.readStrongBinder());
          str = str.readString();
          setPreferredDataSubscriptionId(param1Int1, bool1, iSetOpportunisticDataCallback, str);
          param1Parcel2.writeNoException();
          return true;
        } 
        str.enforceInterface("com.android.internal.telephony.IOns");
        str = str.readString();
        boolean bool3 = isEnabled(str);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool3);
        return true;
      } 
      str.enforceInterface("com.android.internal.telephony.IOns");
      if (str.readInt() != 0)
        bool1 = true; 
      String str = str.readString();
      boolean bool = setEnable(bool1, str);
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(bool);
      return true;
    }
    
    private static class Proxy implements IOns {
      public static IOns sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.IOns";
      }
      
      public boolean setEnable(boolean param2Boolean, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOns");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          parcel1.writeString(param2String);
          boolean bool1 = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool1 && IOns.Stub.getDefaultImpl() != null) {
            param2Boolean = IOns.Stub.getDefaultImpl().setEnable(param2Boolean, param2String);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isEnabled(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOns");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IOns.Stub.getDefaultImpl() != null) {
            bool1 = IOns.Stub.getDefaultImpl().isEnabled(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPreferredDataSubscriptionId(int param2Int, boolean param2Boolean, ISetOpportunisticDataCallback param2ISetOpportunisticDataCallback, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOns");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          if (param2ISetOpportunisticDataCallback != null) {
            iBinder = param2ISetOpportunisticDataCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          boolean bool1 = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool1 && IOns.Stub.getDefaultImpl() != null) {
            IOns.Stub.getDefaultImpl().setPreferredDataSubscriptionId(param2Int, param2Boolean, param2ISetOpportunisticDataCallback, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getPreferredDataSubscriptionId(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOns");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IOns.Stub.getDefaultImpl() != null)
            return IOns.Stub.getDefaultImpl().getPreferredDataSubscriptionId(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateAvailableNetworks(List<AvailableNetworkInfo> param2List, IUpdateAvailableNetworksCallback param2IUpdateAvailableNetworksCallback, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.telephony.IOns");
          parcel1.writeTypedList(param2List);
          if (param2IUpdateAvailableNetworksCallback != null) {
            iBinder = param2IUpdateAvailableNetworksCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IOns.Stub.getDefaultImpl() != null) {
            IOns.Stub.getDefaultImpl().updateAvailableNetworks(param2List, param2IUpdateAvailableNetworksCallback, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOns param1IOns) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOns != null) {
          Proxy.sDefaultImpl = param1IOns;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOns getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
