package com.android.internal.telephony;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IUpdateAvailableNetworksCallback extends IInterface {
  void onComplete(int paramInt) throws RemoteException;
  
  class Default implements IUpdateAvailableNetworksCallback {
    public void onComplete(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IUpdateAvailableNetworksCallback {
    private static final String DESCRIPTOR = "com.android.internal.telephony.IUpdateAvailableNetworksCallback";
    
    static final int TRANSACTION_onComplete = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.IUpdateAvailableNetworksCallback");
    }
    
    public static IUpdateAvailableNetworksCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.IUpdateAvailableNetworksCallback");
      if (iInterface != null && iInterface instanceof IUpdateAvailableNetworksCallback)
        return (IUpdateAvailableNetworksCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onComplete";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.telephony.IUpdateAvailableNetworksCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.telephony.IUpdateAvailableNetworksCallback");
      param1Int1 = param1Parcel1.readInt();
      onComplete(param1Int1);
      return true;
    }
    
    private static class Proxy implements IUpdateAvailableNetworksCallback {
      public static IUpdateAvailableNetworksCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.IUpdateAvailableNetworksCallback";
      }
      
      public void onComplete(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IUpdateAvailableNetworksCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IUpdateAvailableNetworksCallback.Stub.getDefaultImpl() != null) {
            IUpdateAvailableNetworksCallback.Stub.getDefaultImpl().onComplete(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IUpdateAvailableNetworksCallback param1IUpdateAvailableNetworksCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IUpdateAvailableNetworksCallback != null) {
          Proxy.sDefaultImpl = param1IUpdateAvailableNetworksCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IUpdateAvailableNetworksCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
