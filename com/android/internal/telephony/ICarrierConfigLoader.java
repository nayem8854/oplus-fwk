package com.android.internal.telephony;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.PersistableBundle;
import android.os.RemoteException;

public interface ICarrierConfigLoader extends IInterface {
  PersistableBundle getConfigForSubId(int paramInt, String paramString) throws RemoteException;
  
  PersistableBundle getConfigForSubIdWithFeature(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  String getDefaultCarrierServicePackageName() throws RemoteException;
  
  void notifyConfigChangedForSubId(int paramInt) throws RemoteException;
  
  void overrideConfig(int paramInt, PersistableBundle paramPersistableBundle, boolean paramBoolean) throws RemoteException;
  
  void updateConfigForPhoneId(int paramInt, String paramString) throws RemoteException;
  
  class Default implements ICarrierConfigLoader {
    public PersistableBundle getConfigForSubId(int param1Int, String param1String) throws RemoteException {
      return null;
    }
    
    public PersistableBundle getConfigForSubIdWithFeature(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public void overrideConfig(int param1Int, PersistableBundle param1PersistableBundle, boolean param1Boolean) throws RemoteException {}
    
    public void notifyConfigChangedForSubId(int param1Int) throws RemoteException {}
    
    public void updateConfigForPhoneId(int param1Int, String param1String) throws RemoteException {}
    
    public String getDefaultCarrierServicePackageName() throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICarrierConfigLoader {
    private static final String DESCRIPTOR = "com.android.internal.telephony.ICarrierConfigLoader";
    
    static final int TRANSACTION_getConfigForSubId = 1;
    
    static final int TRANSACTION_getConfigForSubIdWithFeature = 2;
    
    static final int TRANSACTION_getDefaultCarrierServicePackageName = 6;
    
    static final int TRANSACTION_notifyConfigChangedForSubId = 4;
    
    static final int TRANSACTION_overrideConfig = 3;
    
    static final int TRANSACTION_updateConfigForPhoneId = 5;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.ICarrierConfigLoader");
    }
    
    public static ICarrierConfigLoader asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.ICarrierConfigLoader");
      if (iInterface != null && iInterface instanceof ICarrierConfigLoader)
        return (ICarrierConfigLoader)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 6:
          return "getDefaultCarrierServicePackageName";
        case 5:
          return "updateConfigForPhoneId";
        case 4:
          return "notifyConfigChangedForSubId";
        case 3:
          return "overrideConfig";
        case 2:
          return "getConfigForSubIdWithFeature";
        case 1:
          break;
      } 
      return "getConfigForSubId";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        String str2;
        PersistableBundle persistableBundle2, persistableBundle3;
        String str3;
        boolean bool = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 6:
            param1Parcel1.enforceInterface("com.android.internal.telephony.ICarrierConfigLoader");
            str2 = getDefaultCarrierServicePackageName();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str2);
            return true;
          case 5:
            str2.enforceInterface("com.android.internal.telephony.ICarrierConfigLoader");
            param1Int1 = str2.readInt();
            str2 = str2.readString();
            updateConfigForPhoneId(param1Int1, str2);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            str2.enforceInterface("com.android.internal.telephony.ICarrierConfigLoader");
            param1Int1 = str2.readInt();
            notifyConfigChangedForSubId(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            str2.enforceInterface("com.android.internal.telephony.ICarrierConfigLoader");
            param1Int1 = str2.readInt();
            if (str2.readInt() != 0) {
              persistableBundle3 = (PersistableBundle)PersistableBundle.CREATOR.createFromParcel((Parcel)str2);
            } else {
              persistableBundle3 = null;
            } 
            if (str2.readInt() != 0)
              bool = true; 
            overrideConfig(param1Int1, persistableBundle3, bool);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            str2.enforceInterface("com.android.internal.telephony.ICarrierConfigLoader");
            param1Int1 = str2.readInt();
            str3 = str2.readString();
            str2 = str2.readString();
            persistableBundle2 = getConfigForSubIdWithFeature(param1Int1, str3, str2);
            param1Parcel2.writeNoException();
            if (persistableBundle2 != null) {
              param1Parcel2.writeInt(1);
              persistableBundle2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 1:
            break;
        } 
        persistableBundle2.enforceInterface("com.android.internal.telephony.ICarrierConfigLoader");
        param1Int1 = persistableBundle2.readInt();
        String str1 = persistableBundle2.readString();
        PersistableBundle persistableBundle1 = getConfigForSubId(param1Int1, str1);
        param1Parcel2.writeNoException();
        if (persistableBundle1 != null) {
          param1Parcel2.writeInt(1);
          persistableBundle1.writeToParcel(param1Parcel2, 1);
        } else {
          param1Parcel2.writeInt(0);
        } 
        return true;
      } 
      param1Parcel2.writeString("com.android.internal.telephony.ICarrierConfigLoader");
      return true;
    }
    
    private static class Proxy implements ICarrierConfigLoader {
      public static ICarrierConfigLoader sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.ICarrierConfigLoader";
      }
      
      public PersistableBundle getConfigForSubId(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ICarrierConfigLoader");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ICarrierConfigLoader.Stub.getDefaultImpl() != null)
            return ICarrierConfigLoader.Stub.getDefaultImpl().getConfigForSubId(param2Int, param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            PersistableBundle persistableBundle = (PersistableBundle)PersistableBundle.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (PersistableBundle)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public PersistableBundle getConfigForSubIdWithFeature(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ICarrierConfigLoader");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ICarrierConfigLoader.Stub.getDefaultImpl() != null)
            return ICarrierConfigLoader.Stub.getDefaultImpl().getConfigForSubIdWithFeature(param2Int, param2String1, param2String2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            PersistableBundle persistableBundle = (PersistableBundle)PersistableBundle.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (PersistableBundle)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void overrideConfig(int param2Int, PersistableBundle param2PersistableBundle, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ICarrierConfigLoader");
          parcel1.writeInt(param2Int);
          boolean bool = true;
          if (param2PersistableBundle != null) {
            parcel1.writeInt(1);
            param2PersistableBundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool1 && ICarrierConfigLoader.Stub.getDefaultImpl() != null) {
            ICarrierConfigLoader.Stub.getDefaultImpl().overrideConfig(param2Int, param2PersistableBundle, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyConfigChangedForSubId(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ICarrierConfigLoader");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && ICarrierConfigLoader.Stub.getDefaultImpl() != null) {
            ICarrierConfigLoader.Stub.getDefaultImpl().notifyConfigChangedForSubId(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateConfigForPhoneId(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ICarrierConfigLoader");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && ICarrierConfigLoader.Stub.getDefaultImpl() != null) {
            ICarrierConfigLoader.Stub.getDefaultImpl().updateConfigForPhoneId(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getDefaultCarrierServicePackageName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ICarrierConfigLoader");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && ICarrierConfigLoader.Stub.getDefaultImpl() != null)
            return ICarrierConfigLoader.Stub.getDefaultImpl().getDefaultCarrierServicePackageName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICarrierConfigLoader param1ICarrierConfigLoader) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICarrierConfigLoader != null) {
          Proxy.sDefaultImpl = param1ICarrierConfigLoader;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICarrierConfigLoader getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
