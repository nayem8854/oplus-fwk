package com.android.internal.telephony;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusTelephonyExtCallback extends IInterface {
  void onTelephonyEventReport(int paramInt1, int paramInt2, Bundle paramBundle) throws RemoteException;
  
  class Default implements IOplusTelephonyExtCallback {
    public void onTelephonyEventReport(int param1Int1, int param1Int2, Bundle param1Bundle) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusTelephonyExtCallback {
    private static final String DESCRIPTOR = "com.android.internal.telephony.IOplusTelephonyExtCallback";
    
    static final int TRANSACTION_onTelephonyEventReport = 8;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.IOplusTelephonyExtCallback");
    }
    
    public static IOplusTelephonyExtCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.IOplusTelephonyExtCallback");
      if (iInterface != null && iInterface instanceof IOplusTelephonyExtCallback)
        return (IOplusTelephonyExtCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 8)
        return null; 
      return "onTelephonyEventReport";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 8) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.telephony.IOplusTelephonyExtCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.telephony.IOplusTelephonyExtCallback");
      param1Int1 = param1Parcel1.readInt();
      param1Int2 = param1Parcel1.readInt();
      if (param1Parcel1.readInt() != 0) {
        Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onTelephonyEventReport(param1Int1, param1Int2, (Bundle)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IOplusTelephonyExtCallback {
      public static IOplusTelephonyExtCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.IOplusTelephonyExtCallback";
      }
      
      public void onTelephonyEventReport(int param2Int1, int param2Int2, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.IOplusTelephonyExtCallback");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IOplusTelephonyExtCallback.Stub.getDefaultImpl() != null) {
            IOplusTelephonyExtCallback.Stub.getDefaultImpl().onTelephonyEventReport(param2Int1, param2Int2, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusTelephonyExtCallback param1IOplusTelephonyExtCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusTelephonyExtCallback != null) {
          Proxy.sDefaultImpl = param1IOplusTelephonyExtCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusTelephonyExtCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
