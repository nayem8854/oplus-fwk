package com.android.internal.telephony;

public class EncodeException extends Exception {
  public static final int ERROR_EXCEED_SIZE = 1;
  
  public static final int ERROR_UNENCODABLE = 0;
  
  private int mError = 0;
  
  public EncodeException(String paramString) {
    super(paramString);
  }
  
  public EncodeException(String paramString, int paramInt) {
    super(paramString);
    this.mError = paramInt;
  }
  
  public EncodeException(char paramChar) {
    super(stringBuilder.toString());
  }
  
  public int getError() {
    return this.mError;
  }
  
  public EncodeException() {}
}
