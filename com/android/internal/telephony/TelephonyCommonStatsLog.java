package com.android.internal.telephony;

import android.util.StatsEvent;
import android.util.StatsLog;

public class TelephonyCommonStatsLog {
  public static final byte ANNOTATION_ID_EXCLUSIVE_STATE = 4;
  
  public static final byte ANNOTATION_ID_IS_UID = 1;
  
  public static final byte ANNOTATION_ID_PRIMARY_FIELD = 3;
  
  public static final byte ANNOTATION_ID_PRIMARY_FIELD_FIRST_UID = 5;
  
  public static final byte ANNOTATION_ID_STATE_NESTED = 8;
  
  public static final byte ANNOTATION_ID_TRIGGER_STATE_RESET = 7;
  
  public static final byte ANNOTATION_ID_TRUNCATE_TIMESTAMP = 2;
  
  public static final int DEVICE_IDENTIFIER_ACCESS_DENIED = 172;
  
  public static void write(int paramInt, String paramString1, String paramString2, boolean paramBoolean1, boolean paramBoolean2) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt);
    builder.writeString(paramString1);
    builder.writeString(paramString2);
    builder.writeBoolean(paramBoolean1);
    builder.writeBoolean(paramBoolean2);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
}
