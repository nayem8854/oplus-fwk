package com.android.internal.telephony;

import android.app.AppOpsManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.UserHandle;
import android.permission.PermissionManager;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class TelephonyPermissions {
  private static final boolean DBG = false;
  
  private static final String LOG_TAG = "TelephonyPermissions";
  
  private static final String PROPERTY_DEVICE_IDENTIFIER_ACCESS_RESTRICTIONS_DISABLED = "device_identifier_access_restrictions_disabled";
  
  private static final Map<String, Set<String>> sReportedDeviceIDPackages = new HashMap<>();
  
  public static boolean checkCallingOrSelfReadPhoneState(Context paramContext, int paramInt, String paramString1, String paramString2, String paramString3) {
    return checkReadPhoneState(paramContext, paramInt, Binder.getCallingPid(), Binder.getCallingUid(), paramString1, paramString2, paramString3);
  }
  
  public static boolean checkCallingOrSelfReadPhoneStateNoThrow(Context paramContext, int paramInt, String paramString1, String paramString2, String paramString3) {
    try {
      return checkCallingOrSelfReadPhoneState(paramContext, paramInt, paramString1, paramString2, paramString3);
    } catch (SecurityException securityException) {
      return false;
    } 
  }
  
  public static boolean checkReadPhoneState(Context paramContext, int paramInt1, int paramInt2, int paramInt3, String paramString1, String paramString2, String paramString3) {
    boolean bool = true;
    try {
      paramContext.enforcePermission("android.permission.READ_PRIVILEGED_PHONE_STATE", paramInt2, paramInt3, paramString3);
      return true;
    } catch (SecurityException securityException) {
      AppOpsManager appOpsManager;
      try {
        paramContext.enforcePermission("android.permission.READ_PHONE_STATE", paramInt2, paramInt3, paramString3);
        appOpsManager = (AppOpsManager)paramContext.getSystemService("appops");
        if (appOpsManager.noteOp("android:read_phone_state", paramInt3, paramString1, paramString2, null) != 0)
          bool = false; 
        return bool;
      } catch (SecurityException securityException1) {
        if (SubscriptionManager.isValidSubscriptionId(paramInt1)) {
          enforceCarrierPrivilege((Context)appOpsManager, paramInt1, paramInt3, paramString3);
          return true;
        } 
        throw securityException1;
      } 
    } 
  }
  
  public static boolean checkCarrierPrivilegeForSubId(Context paramContext, int paramInt) {
    if (SubscriptionManager.isValidSubscriptionId(paramInt) && 
      getCarrierPrivilegeStatus(paramContext, paramInt, Binder.getCallingUid()) == 1)
      return true; 
    return false;
  }
  
  public static boolean checkReadPhoneStateOnAnyActiveSub(Context paramContext, int paramInt1, int paramInt2, String paramString1, String paramString2, String paramString3) {
    boolean bool = true;
    try {
      paramContext.enforcePermission("android.permission.READ_PRIVILEGED_PHONE_STATE", paramInt1, paramInt2, paramString3);
      return true;
    } catch (SecurityException securityException) {
      AppOpsManager appOpsManager;
      try {
        paramContext.enforcePermission("android.permission.READ_PHONE_STATE", paramInt1, paramInt2, paramString3);
        appOpsManager = (AppOpsManager)paramContext.getSystemService("appops");
        if (appOpsManager.noteOp("android:read_phone_state", paramInt2, paramString1, paramString2, null) != 0)
          bool = false; 
        return bool;
      } catch (SecurityException securityException1) {
        return checkCarrierPrivilegeForAnySubId((Context)appOpsManager, paramInt2);
      } 
    } 
  }
  
  public static boolean checkCallingOrSelfReadDeviceIdentifiers(Context paramContext, String paramString1, String paramString2, String paramString3) {
    return checkCallingOrSelfReadDeviceIdentifiers(paramContext, -1, paramString1, paramString2, paramString3);
  }
  
  public static boolean checkCallingOrSelfReadDeviceIdentifiers(Context paramContext, int paramInt, String paramString1, String paramString2, String paramString3) {
    return checkPrivilegedReadPermissionOrCarrierPrivilegePermission(paramContext, paramInt, paramString1, paramString2, paramString3, true);
  }
  
  public static boolean checkCallingOrSelfReadSubscriberIdentifiers(Context paramContext, int paramInt, String paramString1, String paramString2, String paramString3) {
    return checkPrivilegedReadPermissionOrCarrierPrivilegePermission(paramContext, paramInt, paramString1, paramString2, paramString3, false);
  }
  
  private static boolean checkPrivilegedReadPermissionOrCarrierPrivilegePermission(Context paramContext, int paramInt, String paramString1, String paramString2, String paramString3, boolean paramBoolean) {
    int i = Binder.getCallingUid();
    int j = Binder.getCallingPid();
    if (checkCarrierPrivilegeForSubId(paramContext, paramInt))
      return true; 
    if (paramBoolean && checkCarrierPrivilegeForAnySubId(paramContext, i))
      return true; 
    PermissionManager permissionManager = (PermissionManager)paramContext.getSystemService("permission");
    if (permissionManager.checkDeviceIdentifierAccess(paramString1, paramString3, paramString2, j, i) == 0)
      return true; 
    return reportAccessDeniedToReadIdentifiers(paramContext, paramInt, j, i, paramString1, paramString3);
  }
  
  private static boolean reportAccessDeniedToReadIdentifiers(Context paramContext, int paramInt1, int paramInt2, int paramInt3, String paramString1, String paramString2) {
    ApplicationInfo applicationInfo = null;
    try {
      PackageManager packageManager = paramContext.getPackageManager();
      UserHandle userHandle = UserHandle.getUserHandleForUid(paramInt3);
      ApplicationInfo applicationInfo1 = packageManager.getApplicationInfoAsUser(paramString1, 0, userHandle);
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Exception caught obtaining package info for package ");
      stringBuilder.append(paramString1);
      Log.e("TelephonyPermissions", stringBuilder.toString(), (Throwable)nameNotFoundException);
    } 
    boolean bool = sReportedDeviceIDPackages.containsKey(paramString1);
    if (!bool || !((Set)sReportedDeviceIDPackages.get(paramString1)).contains(paramString2)) {
      Set<String> set;
      if (!bool) {
        set = new HashSet();
        sReportedDeviceIDPackages.put(paramString1, set);
      } else {
        set = sReportedDeviceIDPackages.get(paramString1);
      } 
      set.add(paramString2);
      TelephonyCommonStatsLog.write(172, paramString1, paramString2, false, false);
    } 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("reportAccessDeniedToReadIdentifiers:");
    stringBuilder2.append(paramString1);
    stringBuilder2.append(":");
    stringBuilder2.append(paramString2);
    stringBuilder2.append(":");
    stringBuilder2.append(paramInt1);
    Log.w("TelephonyPermissions", stringBuilder2.toString());
    if (applicationInfo != null && applicationInfo.targetSdkVersion < 29) {
      if (paramContext.checkPermission("android.permission.READ_PHONE_STATE", paramInt2, paramInt3) == 0)
        return false; 
      if (checkCarrierPrivilegeForSubId(paramContext, paramInt1))
        return false; 
    } 
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append(paramString2);
    stringBuilder1.append(": The user ");
    stringBuilder1.append(paramInt3);
    stringBuilder1.append(" does not meet the requirements to access device identifiers.");
    throw new SecurityException(stringBuilder1.toString());
  }
  
  public static boolean checkReadCallLog(Context paramContext, int paramInt1, int paramInt2, int paramInt3, String paramString1, String paramString2) {
    paramInt2 = paramContext.checkPermission("android.permission.READ_CALL_LOG", paramInt2, paramInt3);
    boolean bool = true;
    if (paramInt2 != 0) {
      if (SubscriptionManager.isValidSubscriptionId(paramInt1)) {
        enforceCarrierPrivilege(paramContext, paramInt1, paramInt3, "readCallLog");
        return true;
      } 
      return false;
    } 
    AppOpsManager appOpsManager = (AppOpsManager)paramContext.getSystemService("appops");
    if (appOpsManager.noteOp("android:read_call_log", paramInt3, paramString1, paramString2, null) != 0)
      bool = false; 
    return bool;
  }
  
  public static boolean checkCallingOrSelfReadPhoneNumber(Context paramContext, int paramInt, String paramString1, String paramString2, String paramString3) {
    int i = Binder.getCallingPid(), j = Binder.getCallingUid();
    return checkReadPhoneNumber(paramContext, paramInt, i, j, paramString1, paramString2, paramString3);
  }
  
  public static boolean checkReadPhoneNumber(Context paramContext, int paramInt1, int paramInt2, int paramInt3, String paramString1, String paramString2, String paramString3) {
    try {
      PackageManager packageManager = paramContext.getPackageManager();
      UserHandle userHandle = UserHandle.getUserHandleForUid(Binder.getCallingUid());
      boolean bool1 = false;
      try {
        ApplicationInfo applicationInfo = packageManager.getApplicationInfoAsUser(paramString1, 0, userHandle);
        int i = applicationInfo.targetSdkVersion;
        if (i <= 29)
          bool1 = true; 
      } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {}
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {}
    boolean bool = false;
  }
  
  public static void enforceCallingOrSelfModifyPermissionOrCarrierPrivilege(Context paramContext, int paramInt, String paramString) {
    if (paramContext.checkCallingOrSelfPermission("android.permission.MODIFY_PHONE_STATE") == 0)
      return; 
    enforceCallingOrSelfCarrierPrivilege(paramContext, paramInt, paramString);
  }
  
  public static void enforeceCallingOrSelfReadPhoneStatePermissionOrCarrierPrivilege(Context paramContext, int paramInt, String paramString) {
    if (paramContext.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0)
      return; 
    enforceCallingOrSelfCarrierPrivilege(paramContext, paramInt, paramString);
  }
  
  public static void enforeceCallingOrSelfReadPrivilegedPhoneStatePermissionOrCarrierPrivilege(Context paramContext, int paramInt, String paramString) {
    if (paramContext.checkCallingOrSelfPermission("android.permission.READ_PRIVILEGED_PHONE_STATE") == 0)
      return; 
    enforceCallingOrSelfCarrierPrivilege(paramContext, paramInt, paramString);
  }
  
  public static void enforeceCallingOrSelfReadPrecisePhoneStatePermissionOrCarrierPrivilege(Context paramContext, int paramInt, String paramString) {
    if (paramContext.checkCallingOrSelfPermission("android.permission.READ_PRIVILEGED_PHONE_STATE") == 0)
      return; 
    if (paramContext.checkCallingOrSelfPermission("android.permission.READ_PRECISE_PHONE_STATE") == 0)
      return; 
    enforceCallingOrSelfCarrierPrivilege(paramContext, paramInt, paramString);
  }
  
  public static void enforceCallingOrSelfCarrierPrivilege(Context paramContext, int paramInt, String paramString) {
    enforceCarrierPrivilege(paramContext, paramInt, Binder.getCallingUid(), paramString);
  }
  
  private static void enforceCarrierPrivilege(Context paramContext, int paramInt1, int paramInt2, String paramString) {
    if (getCarrierPrivilegeStatus(paramContext, paramInt1, paramInt2) == 1)
      return; 
    throw new SecurityException(paramString);
  }
  
  private static boolean checkCarrierPrivilegeForAnySubId(Context paramContext, int paramInt) {
    SubscriptionManager subscriptionManager = (SubscriptionManager)paramContext.getSystemService("telephony_subscription_service");
    for (int i : subscriptionManager.getCompleteActiveSubscriptionIdList()) {
      if (getCarrierPrivilegeStatus(paramContext, i, paramInt) == 1)
        return true; 
    } 
    return false;
  }
  
  private static int getCarrierPrivilegeStatus(Context paramContext, int paramInt1, int paramInt2) {
    if (paramInt2 == 1000 || paramInt2 == 1001)
      return 1; 
    long l = Binder.clearCallingIdentity();
    try {
      TelephonyManager telephonyManager = (TelephonyManager)paramContext.getSystemService("phone");
      paramInt1 = telephonyManager.createForSubscriptionId(paramInt1).getCarrierPrivilegeStatus(paramInt2);
      Binder.restoreCallingIdentity(l);
      return paramInt1;
    } catch (Exception exception) {
      exception.printStackTrace();
      Binder.restoreCallingIdentity(l);
      return 0;
    } finally {}
    Binder.restoreCallingIdentity(l);
    throw paramContext;
  }
  
  public static void enforceShellOnly(int paramInt, String paramString) {
    if (paramInt == 2000 || paramInt == 0)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append(": Only shell user can call it");
    throw new SecurityException(stringBuilder.toString());
  }
}
