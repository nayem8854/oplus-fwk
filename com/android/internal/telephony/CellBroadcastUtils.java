package com.android.internal.telephony;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.text.TextUtils;
import android.util.Log;

public class CellBroadcastUtils {
  private static final String TAG = "CellBroadcastUtils";
  
  private static final boolean VDBG = false;
  
  public static String getDefaultCellBroadcastReceiverPackageName(Context paramContext) {
    PackageManager packageManager = paramContext.getPackageManager();
    ResolveInfo resolveInfo = packageManager.resolveActivity(new Intent("android.provider.Telephony.SMS_CB_RECEIVED"), 1048576);
    if (resolveInfo == null) {
      Log.e("CellBroadcastUtils", "getDefaultCellBroadcastReceiverPackageName: no package found");
      return null;
    } 
    String str = resolveInfo.activityInfo.applicationInfo.packageName;
    if (TextUtils.isEmpty(str) || packageManager.checkPermission("android.permission.READ_CELL_BROADCASTS", str) == -1) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getDefaultCellBroadcastReceiverPackageName: returning null; permission check failed for : ");
      stringBuilder.append(str);
      Log.e("CellBroadcastUtils", stringBuilder.toString());
      return null;
    } 
    return str;
  }
}
