package com.android.internal.telephony;

import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Messenger;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.os.WorkSource;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telephony.CallForwardingInfo;
import android.telephony.CarrierRestrictionRules;
import android.telephony.CellIdentity;
import android.telephony.CellInfo;
import android.telephony.ClientRequestStats;
import android.telephony.ICellInfoCallback;
import android.telephony.IccOpenLogicalChannelResponse;
import android.telephony.NeighboringCellInfo;
import android.telephony.NetworkScanRequest;
import android.telephony.PhoneNumberRange;
import android.telephony.RadioAccessFamily;
import android.telephony.RadioAccessSpecifier;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.TelephonyHistogram;
import android.telephony.UiccCardInfo;
import android.telephony.UiccSlotInfo;
import android.telephony.VisualVoicemailSmsFilterSettings;
import android.telephony.emergency.EmergencyNumber;
import android.telephony.ims.aidl.IImsCapabilityCallback;
import android.telephony.ims.aidl.IImsConfig;
import android.telephony.ims.aidl.IImsConfigCallback;
import android.telephony.ims.aidl.IImsMmTelFeature;
import android.telephony.ims.aidl.IImsRcsFeature;
import android.telephony.ims.aidl.IImsRegistration;
import android.telephony.ims.aidl.IImsRegistrationCallback;
import com.android.ims.internal.IImsServiceFeatureCallback;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface ITelephony extends IInterface {
  void cacheMmTelCapabilityProvisioning(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean) throws RemoteException;
  
  void call(String paramString1, String paramString2) throws RemoteException;
  
  boolean canChangeDtmfToneLength(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  boolean canConnectTo5GInDsdsMode() throws RemoteException;
  
  void carrierActionReportDefaultNetworkStatus(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void carrierActionResetAll(int paramInt) throws RemoteException;
  
  void carrierActionSetMeteredApnsEnabled(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void carrierActionSetRadioEnabled(int paramInt, boolean paramBoolean) throws RemoteException;
  
  int changeIccLockPassword(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  int checkCarrierPrivilegesForPackage(int paramInt, String paramString) throws RemoteException;
  
  int checkCarrierPrivilegesForPackageAnyPhone(String paramString) throws RemoteException;
  
  void dial(String paramString) throws RemoteException;
  
  boolean disableDataConnectivity() throws RemoteException;
  
  void disableIms(int paramInt) throws RemoteException;
  
  void disableLocationUpdates() throws RemoteException;
  
  void disableLocationUpdatesForSubscriber(int paramInt) throws RemoteException;
  
  void disableVisualVoicemailSmsFilter(String paramString, int paramInt) throws RemoteException;
  
  boolean doesSwitchMultiSimConfigTriggerReboot(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  boolean enableDataConnectivity() throws RemoteException;
  
  void enableIms(int paramInt) throws RemoteException;
  
  void enableLocationUpdates() throws RemoteException;
  
  void enableLocationUpdatesForSubscriber(int paramInt) throws RemoteException;
  
  boolean enableModemForSlot(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void enableVideoCalling(boolean paramBoolean) throws RemoteException;
  
  void enableVisualVoicemailSmsFilter(String paramString, int paramInt, VisualVoicemailSmsFilterSettings paramVisualVoicemailSmsFilterSettings) throws RemoteException;
  
  void enqueueSmsPickResult(String paramString1, String paramString2, IIntegerConsumer paramIIntegerConsumer) throws RemoteException;
  
  void factoryReset(int paramInt) throws RemoteException;
  
  int getActivePhoneType() throws RemoteException;
  
  int getActivePhoneTypeForSlot(int paramInt) throws RemoteException;
  
  VisualVoicemailSmsFilterSettings getActiveVisualVoicemailSmsFilterSettings(int paramInt) throws RemoteException;
  
  String getAidForAppType(int paramInt1, int paramInt2) throws RemoteException;
  
  List<CellInfo> getAllCellInfo(String paramString1, String paramString2) throws RemoteException;
  
  CarrierRestrictionRules getAllowedCarriers() throws RemoteException;
  
  long getAllowedNetworkTypes(int paramInt) throws RemoteException;
  
  long getAllowedNetworkTypesForReason(int paramInt1, int paramInt2) throws RemoteException;
  
  String getBoundImsServicePackage(int paramInt1, boolean paramBoolean, int paramInt2) throws RemoteException;
  
  int getCalculatedPreferredNetworkType(String paramString1, String paramString2) throws RemoteException;
  
  CallForwardingInfo getCallForwarding(int paramInt1, int paramInt2) throws RemoteException;
  
  int getCallState() throws RemoteException;
  
  int getCallStateForSlot(int paramInt) throws RemoteException;
  
  int getCallWaitingStatus(int paramInt) throws RemoteException;
  
  int getCardIdForDefaultEuicc(int paramInt, String paramString) throws RemoteException;
  
  int getCarrierIdFromMccMnc(int paramInt, String paramString, boolean paramBoolean) throws RemoteException;
  
  int getCarrierIdListVersion(int paramInt) throws RemoteException;
  
  List<String> getCarrierPackageNamesForIntentAndPhone(Intent paramIntent, int paramInt) throws RemoteException;
  
  int getCarrierPrivilegeStatus(int paramInt) throws RemoteException;
  
  int getCarrierPrivilegeStatusForUid(int paramInt1, int paramInt2) throws RemoteException;
  
  int getCdmaEriIconIndex(String paramString1, String paramString2) throws RemoteException;
  
  int getCdmaEriIconIndexForSubscriber(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  int getCdmaEriIconMode(String paramString1, String paramString2) throws RemoteException;
  
  int getCdmaEriIconModeForSubscriber(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  String getCdmaEriText(String paramString1, String paramString2) throws RemoteException;
  
  String getCdmaEriTextForSubscriber(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  String getCdmaMdn(int paramInt) throws RemoteException;
  
  String getCdmaMin(int paramInt) throws RemoteException;
  
  String getCdmaPrlVersion(int paramInt) throws RemoteException;
  
  int getCdmaRoamingMode(int paramInt) throws RemoteException;
  
  CellIdentity getCellLocation(String paramString1, String paramString2) throws RemoteException;
  
  CellNetworkScanResult getCellNetworkScanResults(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  List<String> getCertsFromCarrierPrivilegeAccessRules(int paramInt) throws RemoteException;
  
  List<ClientRequestStats> getClientRequestStats(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  String getCurrentPackageName() throws RemoteException;
  
  int getDataActivationState(int paramInt, String paramString) throws RemoteException;
  
  int getDataActivity() throws RemoteException;
  
  int getDataActivityForSubId(int paramInt) throws RemoteException;
  
  boolean getDataEnabled(int paramInt) throws RemoteException;
  
  int getDataNetworkType(String paramString1, String paramString2) throws RemoteException;
  
  int getDataNetworkTypeForSubscriber(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  int getDataState() throws RemoteException;
  
  int getDataStateForSubId(int paramInt) throws RemoteException;
  
  String getDeviceId(String paramString) throws RemoteException;
  
  String getDeviceIdWithFeature(String paramString1, String paramString2) throws RemoteException;
  
  String getDeviceSoftwareVersionForSlot(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  long getEffectiveAllowedNetworkTypes(int paramInt) throws RemoteException;
  
  boolean getEmergencyCallbackMode(int paramInt) throws RemoteException;
  
  int getEmergencyNumberDbVersion(int paramInt) throws RemoteException;
  
  Map getEmergencyNumberList(String paramString1, String paramString2) throws RemoteException;
  
  List<String> getEmergencyNumberListTestMode() throws RemoteException;
  
  String getEsn(int paramInt) throws RemoteException;
  
  String[] getForbiddenPlmns(int paramInt1, int paramInt2, String paramString1, String paramString2) throws RemoteException;
  
  String getImeiForSlot(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  IImsConfig getImsConfig(int paramInt1, int paramInt2) throws RemoteException;
  
  void getImsMmTelFeatureState(int paramInt, IIntegerConsumer paramIIntegerConsumer) throws RemoteException;
  
  void getImsMmTelRegistrationState(int paramInt, IIntegerConsumer paramIIntegerConsumer) throws RemoteException;
  
  void getImsMmTelRegistrationTransportType(int paramInt, IIntegerConsumer paramIIntegerConsumer) throws RemoteException;
  
  int getImsProvisioningInt(int paramInt1, int paramInt2) throws RemoteException;
  
  boolean getImsProvisioningStatusForCapability(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  String getImsProvisioningString(int paramInt1, int paramInt2) throws RemoteException;
  
  int getImsRegTechnologyForMmTel(int paramInt) throws RemoteException;
  
  IImsRegistration getImsRegistration(int paramInt1, int paramInt2) throws RemoteException;
  
  String getLine1AlphaTagForDisplay(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  String getLine1NumberForDisplay(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  int getLteOnCdmaMode(String paramString1, String paramString2) throws RemoteException;
  
  int getLteOnCdmaModeForSubscriber(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  String getManualNetworkSelectionPlmn(int paramInt) throws RemoteException;
  
  String getManufacturerCodeForSlot(int paramInt) throws RemoteException;
  
  String getMeidForSlot(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  String[] getMergedImsisFromGroup(int paramInt, String paramString) throws RemoteException;
  
  String[] getMergedSubscriberIds(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  IImsMmTelFeature getMmTelFeatureAndListen(int paramInt, IImsServiceFeatureCallback paramIImsServiceFeatureCallback) throws RemoteException;
  
  String getMmsUAProfUrl(int paramInt) throws RemoteException;
  
  String getMmsUserAgent(int paramInt) throws RemoteException;
  
  List<NeighboringCellInfo> getNeighboringCellInfo(String paramString1, String paramString2) throws RemoteException;
  
  String getNetworkCountryIsoForPhone(int paramInt) throws RemoteException;
  
  int getNetworkSelectionMode(int paramInt) throws RemoteException;
  
  int getNetworkTypeForSubscriber(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  int getNumberOfModemsWithSimultaneousDataConnections(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  List<String> getPackagesWithCarrierPrivileges(int paramInt) throws RemoteException;
  
  List<String> getPackagesWithCarrierPrivilegesForAllPhones() throws RemoteException;
  
  String[] getPcscfAddress(String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  PhoneAccountHandle getPhoneAccountHandleForSubscriptionId(int paramInt) throws RemoteException;
  
  int getPreferredNetworkType(int paramInt) throws RemoteException;
  
  int getRadioAccessFamily(int paramInt, String paramString) throws RemoteException;
  
  int getRadioHalVersion() throws RemoteException;
  
  int getRadioPowerState(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  IImsRcsFeature getRcsFeatureAndListen(int paramInt, IImsServiceFeatureCallback paramIImsServiceFeatureCallback) throws RemoteException;
  
  boolean getRcsProvisioningStatusForCapability(int paramInt1, int paramInt2) throws RemoteException;
  
  ServiceState getServiceStateForSubscriber(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  SignalStrength getSignalStrength(int paramInt) throws RemoteException;
  
  String getSimLocaleForSubscriber(int paramInt) throws RemoteException;
  
  int[] getSlotsMapping() throws RemoteException;
  
  int getSubIdForPhoneAccount(PhoneAccount paramPhoneAccount) throws RemoteException;
  
  int getSubIdForPhoneAccountHandle(PhoneAccountHandle paramPhoneAccountHandle, String paramString1, String paramString2) throws RemoteException;
  
  int getSubscriptionCarrierId(int paramInt) throws RemoteException;
  
  String getSubscriptionCarrierName(int paramInt) throws RemoteException;
  
  int getSubscriptionSpecificCarrierId(int paramInt) throws RemoteException;
  
  String getSubscriptionSpecificCarrierName(int paramInt) throws RemoteException;
  
  List<TelephonyHistogram> getTelephonyHistograms() throws RemoteException;
  
  String getTypeAllocationCodeForSlot(int paramInt) throws RemoteException;
  
  List<UiccCardInfo> getUiccCardsInfo(String paramString) throws RemoteException;
  
  UiccSlotInfo[] getUiccSlotsInfo() throws RemoteException;
  
  String getVisualVoicemailPackageName(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  Bundle getVisualVoicemailSettings(String paramString, int paramInt) throws RemoteException;
  
  VisualVoicemailSmsFilterSettings getVisualVoicemailSmsFilterSettings(String paramString, int paramInt) throws RemoteException;
  
  int getVoWiFiModeSetting(int paramInt) throws RemoteException;
  
  int getVoWiFiRoamingModeSetting(int paramInt) throws RemoteException;
  
  int getVoiceActivationState(int paramInt, String paramString) throws RemoteException;
  
  int getVoiceMessageCountForSubscriber(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  int getVoiceNetworkTypeForSubscriber(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  Uri getVoicemailRingtoneUri(PhoneAccountHandle paramPhoneAccountHandle) throws RemoteException;
  
  boolean handlePinMmi(String paramString) throws RemoteException;
  
  boolean handlePinMmiForSubscriber(int paramInt, String paramString) throws RemoteException;
  
  void handleUssdRequest(int paramInt, String paramString, ResultReceiver paramResultReceiver) throws RemoteException;
  
  boolean hasIccCard() throws RemoteException;
  
  boolean hasIccCardUsingSlotIndex(int paramInt) throws RemoteException;
  
  boolean iccCloseLogicalChannel(int paramInt1, int paramInt2) throws RemoteException;
  
  boolean iccCloseLogicalChannelBySlot(int paramInt1, int paramInt2) throws RemoteException;
  
  byte[] iccExchangeSimIO(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, String paramString) throws RemoteException;
  
  IccOpenLogicalChannelResponse iccOpenLogicalChannel(int paramInt1, String paramString1, String paramString2, int paramInt2) throws RemoteException;
  
  IccOpenLogicalChannelResponse iccOpenLogicalChannelBySlot(int paramInt1, String paramString1, String paramString2, int paramInt2) throws RemoteException;
  
  String iccTransmitApduBasicChannel(int paramInt1, String paramString1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, String paramString2) throws RemoteException;
  
  String iccTransmitApduBasicChannelBySlot(int paramInt1, String paramString1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, String paramString2) throws RemoteException;
  
  String iccTransmitApduLogicalChannel(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, String paramString) throws RemoteException;
  
  String iccTransmitApduLogicalChannelBySlot(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, String paramString) throws RemoteException;
  
  int invokeOemRilRequestRaw(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) throws RemoteException;
  
  boolean isAdvancedCallingSettingEnabled(int paramInt) throws RemoteException;
  
  boolean isApnMetered(int paramInt1, int paramInt2) throws RemoteException;
  
  boolean isApplicationOnUicc(int paramInt1, int paramInt2) throws RemoteException;
  
  boolean isAvailable(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  boolean isCapable(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  boolean isConcurrentVoiceAndDataAllowed(int paramInt) throws RemoteException;
  
  boolean isDataAllowedInVoiceCall(int paramInt) throws RemoteException;
  
  boolean isDataConnectivityPossible(int paramInt) throws RemoteException;
  
  boolean isDataEnabled(int paramInt) throws RemoteException;
  
  boolean isDataEnabledForApn(int paramInt1, int paramInt2, String paramString) throws RemoteException;
  
  boolean isDataRoamingEnabled(int paramInt) throws RemoteException;
  
  boolean isEmergencyNumber(String paramString, boolean paramBoolean) throws RemoteException;
  
  boolean isHearingAidCompatibilitySupported() throws RemoteException;
  
  boolean isIccLockEnabled(int paramInt) throws RemoteException;
  
  boolean isImsRegistered(int paramInt) throws RemoteException;
  
  boolean isInEmergencySmsMode() throws RemoteException;
  
  boolean isManualNetworkSelectionAllowed(int paramInt) throws RemoteException;
  
  boolean isMmTelCapabilityProvisionedInCache(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void isMmTelCapabilitySupported(int paramInt1, IIntegerConsumer paramIIntegerConsumer, int paramInt2, int paramInt3) throws RemoteException;
  
  boolean isModemEnabledForSlot(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  int isMultiSimSupported(String paramString1, String paramString2) throws RemoteException;
  
  boolean isMvnoMatched(int paramInt1, int paramInt2, String paramString) throws RemoteException;
  
  boolean isRadioOn(String paramString) throws RemoteException;
  
  boolean isRadioOnForSubscriber(int paramInt, String paramString) throws RemoteException;
  
  boolean isRadioOnForSubscriberWithFeature(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  boolean isRadioOnWithFeature(String paramString1, String paramString2) throws RemoteException;
  
  boolean isRttSupported(int paramInt) throws RemoteException;
  
  boolean isTetheringApnRequiredForSubscriber(int paramInt) throws RemoteException;
  
  boolean isTtyModeSupported() throws RemoteException;
  
  boolean isTtyOverVolteEnabled(int paramInt) throws RemoteException;
  
  boolean isUserDataEnabled(int paramInt) throws RemoteException;
  
  boolean isVideoCallingEnabled(String paramString1, String paramString2) throws RemoteException;
  
  boolean isVideoTelephonyAvailable(int paramInt) throws RemoteException;
  
  boolean isVoWiFiRoamingSettingEnabled(int paramInt) throws RemoteException;
  
  boolean isVoWiFiSettingEnabled(int paramInt) throws RemoteException;
  
  boolean isVoicemailVibrationEnabled(PhoneAccountHandle paramPhoneAccountHandle) throws RemoteException;
  
  boolean isVtSettingEnabled(int paramInt) throws RemoteException;
  
  boolean isWifiCallingAvailable(int paramInt) throws RemoteException;
  
  boolean isWorldPhone(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  boolean needMobileRadioShutdown() throws RemoteException;
  
  boolean needsOtaServiceProvisioning() throws RemoteException;
  
  void notifyOtaEmergencyNumberDbInstalled() throws RemoteException;
  
  void notifyRcsAutoConfigurationReceived(int paramInt, byte[] paramArrayOfbyte, boolean paramBoolean) throws RemoteException;
  
  String nvReadItem(int paramInt) throws RemoteException;
  
  boolean nvWriteCdmaPrl(byte[] paramArrayOfbyte) throws RemoteException;
  
  boolean nvWriteItem(int paramInt, String paramString) throws RemoteException;
  
  boolean rebootModem(int paramInt) throws RemoteException;
  
  void refreshUiccProfile(int paramInt) throws RemoteException;
  
  void registerImsProvisioningChangedCallback(int paramInt, IImsConfigCallback paramIImsConfigCallback) throws RemoteException;
  
  void registerImsRegistrationCallback(int paramInt, IImsRegistrationCallback paramIImsRegistrationCallback) throws RemoteException;
  
  void registerMmTelCapabilityCallback(int paramInt, IImsCapabilityCallback paramIImsCapabilityCallback) throws RemoteException;
  
  void requestCellInfoUpdate(int paramInt, ICellInfoCallback paramICellInfoCallback, String paramString1, String paramString2) throws RemoteException;
  
  void requestCellInfoUpdateWithWorkSource(int paramInt, ICellInfoCallback paramICellInfoCallback, String paramString1, String paramString2, WorkSource paramWorkSource) throws RemoteException;
  
  void requestModemActivityInfo(ResultReceiver paramResultReceiver) throws RemoteException;
  
  int requestNetworkScan(int paramInt, NetworkScanRequest paramNetworkScanRequest, Messenger paramMessenger, IBinder paramIBinder, String paramString1, String paramString2) throws RemoteException;
  
  void requestNumberVerification(PhoneNumberRange paramPhoneNumberRange, long paramLong, INumberVerificationCallback paramINumberVerificationCallback, String paramString) throws RemoteException;
  
  void requestUserActivityNotification() throws RemoteException;
  
  void resetIms(int paramInt) throws RemoteException;
  
  boolean resetModemConfig(int paramInt) throws RemoteException;
  
  void resetOtaEmergencyNumberDbFilePath() throws RemoteException;
  
  void sendDialerSpecialCode(String paramString1, String paramString2) throws RemoteException;
  
  String sendEnvelopeWithStatus(int paramInt, String paramString) throws RemoteException;
  
  void sendVisualVoicemailSmsForSubscriber(String paramString1, String paramString2, int paramInt1, String paramString3, int paramInt2, String paramString4, PendingIntent paramPendingIntent) throws RemoteException;
  
  void setAdvancedCallingSettingEnabled(int paramInt, boolean paramBoolean) throws RemoteException;
  
  int setAllowedCarriers(CarrierRestrictionRules paramCarrierRestrictionRules) throws RemoteException;
  
  boolean setAllowedNetworkTypes(int paramInt, long paramLong) throws RemoteException;
  
  boolean setAllowedNetworkTypesForReason(int paramInt1, int paramInt2, long paramLong) throws RemoteException;
  
  boolean setAlwaysAllowMmsData(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setAlwaysReportSignalStrength(int paramInt, boolean paramBoolean) throws RemoteException;
  
  boolean setBoundImsServiceOverride(int paramInt, boolean paramBoolean, int[] paramArrayOfint, String paramString) throws RemoteException;
  
  boolean setCallForwarding(int paramInt, CallForwardingInfo paramCallForwardingInfo) throws RemoteException;
  
  boolean setCallWaitingStatus(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setCarrierTestOverride(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9) throws RemoteException;
  
  boolean setCdmaRoamingMode(int paramInt1, int paramInt2) throws RemoteException;
  
  boolean setCdmaSubscriptionMode(int paramInt1, int paramInt2) throws RemoteException;
  
  void setCellInfoListRate(int paramInt) throws RemoteException;
  
  void setCepEnabled(boolean paramBoolean) throws RemoteException;
  
  void setDataActivationState(int paramInt1, int paramInt2) throws RemoteException;
  
  boolean setDataAllowedDuringVoiceCall(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setDataRoamingEnabled(int paramInt, boolean paramBoolean) throws RemoteException;
  
  int setForbiddenPlmns(int paramInt1, int paramInt2, List<String> paramList, String paramString1, String paramString2) throws RemoteException;
  
  int setIccLockEnabled(int paramInt, boolean paramBoolean, String paramString) throws RemoteException;
  
  int setImsProvisioningInt(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void setImsProvisioningStatusForCapability(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean) throws RemoteException;
  
  int setImsProvisioningString(int paramInt1, int paramInt2, String paramString) throws RemoteException;
  
  void setImsRegistrationState(boolean paramBoolean) throws RemoteException;
  
  boolean setLine1NumberForDisplayForSubscriber(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  void setMultiSimCarrierRestriction(boolean paramBoolean) throws RemoteException;
  
  void setNetworkSelectionModeAutomatic(int paramInt) throws RemoteException;
  
  boolean setNetworkSelectionModeManual(int paramInt, OperatorInfo paramOperatorInfo, boolean paramBoolean) throws RemoteException;
  
  boolean setOperatorBrandOverride(int paramInt, String paramString) throws RemoteException;
  
  void setPolicyDataEnabled(boolean paramBoolean, int paramInt) throws RemoteException;
  
  boolean setPreferredNetworkType(int paramInt1, int paramInt2) throws RemoteException;
  
  boolean setRadio(boolean paramBoolean) throws RemoteException;
  
  void setRadioCapability(RadioAccessFamily[] paramArrayOfRadioAccessFamily) throws RemoteException;
  
  boolean setRadioForSubscriber(int paramInt, boolean paramBoolean) throws RemoteException;
  
  boolean setRadioPower(boolean paramBoolean) throws RemoteException;
  
  void setRcsProvisioningStatusForCapability(int paramInt1, int paramInt2, boolean paramBoolean) throws RemoteException;
  
  boolean setRoamingOverride(int paramInt, List<String> paramList1, List<String> paramList2, List<String> paramList3, List<String> paramList4) throws RemoteException;
  
  void setRttCapabilitySetting(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setSimPowerStateForSlot(int paramInt1, int paramInt2) throws RemoteException;
  
  void setSystemSelectionChannels(List<RadioAccessSpecifier> paramList, int paramInt, IBooleanConsumer paramIBooleanConsumer) throws RemoteException;
  
  void setUserDataEnabled(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setVoWiFiModeSetting(int paramInt1, int paramInt2) throws RemoteException;
  
  void setVoWiFiNonPersistent(int paramInt1, boolean paramBoolean, int paramInt2) throws RemoteException;
  
  void setVoWiFiRoamingModeSetting(int paramInt1, int paramInt2) throws RemoteException;
  
  void setVoWiFiRoamingSettingEnabled(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setVoWiFiSettingEnabled(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setVoiceActivationState(int paramInt1, int paramInt2) throws RemoteException;
  
  boolean setVoiceMailNumber(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  void setVoicemailRingtoneUri(String paramString, PhoneAccountHandle paramPhoneAccountHandle, Uri paramUri) throws RemoteException;
  
  void setVoicemailVibrationEnabled(String paramString, PhoneAccountHandle paramPhoneAccountHandle, boolean paramBoolean) throws RemoteException;
  
  void setVtSettingEnabled(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void shutdownMobileRadios() throws RemoteException;
  
  void stopNetworkScan(int paramInt1, int paramInt2) throws RemoteException;
  
  boolean supplyPinForSubscriber(int paramInt, String paramString) throws RemoteException;
  
  int[] supplyPinReportResultForSubscriber(int paramInt, String paramString) throws RemoteException;
  
  boolean supplyPukForSubscriber(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  int[] supplyPukReportResultForSubscriber(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  void switchMultiSimConfig(int paramInt) throws RemoteException;
  
  boolean switchSlots(int[] paramArrayOfint) throws RemoteException;
  
  void toggleRadioOnOff() throws RemoteException;
  
  void toggleRadioOnOffForSubscriber(int paramInt) throws RemoteException;
  
  void unregisterImsFeatureCallback(int paramInt1, int paramInt2, IImsServiceFeatureCallback paramIImsServiceFeatureCallback) throws RemoteException;
  
  void unregisterImsProvisioningChangedCallback(int paramInt, IImsConfigCallback paramIImsConfigCallback) throws RemoteException;
  
  void unregisterImsRegistrationCallback(int paramInt, IImsRegistrationCallback paramIImsRegistrationCallback) throws RemoteException;
  
  void unregisterMmTelCapabilityCallback(int paramInt, IImsCapabilityCallback paramIImsCapabilityCallback) throws RemoteException;
  
  void updateEmergencyNumberListTestMode(int paramInt, EmergencyNumber paramEmergencyNumber) throws RemoteException;
  
  void updateOtaEmergencyNumberDbFilePath(ParcelFileDescriptor paramParcelFileDescriptor) throws RemoteException;
  
  void updateServiceLocation() throws RemoteException;
  
  void updateServiceLocationForSubscriber(int paramInt) throws RemoteException;
  
  void userActivity() throws RemoteException;
  
  class Default implements ITelephony {
    public void dial(String param1String) throws RemoteException {}
    
    public void call(String param1String1, String param1String2) throws RemoteException {}
    
    public boolean isRadioOn(String param1String) throws RemoteException {
      return false;
    }
    
    public boolean isRadioOnWithFeature(String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public boolean isRadioOnForSubscriber(int param1Int, String param1String) throws RemoteException {
      return false;
    }
    
    public boolean isRadioOnForSubscriberWithFeature(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public boolean supplyPinForSubscriber(int param1Int, String param1String) throws RemoteException {
      return false;
    }
    
    public boolean supplyPukForSubscriber(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public int[] supplyPinReportResultForSubscriber(int param1Int, String param1String) throws RemoteException {
      return null;
    }
    
    public int[] supplyPukReportResultForSubscriber(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public boolean handlePinMmi(String param1String) throws RemoteException {
      return false;
    }
    
    public void handleUssdRequest(int param1Int, String param1String, ResultReceiver param1ResultReceiver) throws RemoteException {}
    
    public boolean handlePinMmiForSubscriber(int param1Int, String param1String) throws RemoteException {
      return false;
    }
    
    public void toggleRadioOnOff() throws RemoteException {}
    
    public void toggleRadioOnOffForSubscriber(int param1Int) throws RemoteException {}
    
    public boolean setRadio(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean setRadioForSubscriber(int param1Int, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean setRadioPower(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public void updateServiceLocation() throws RemoteException {}
    
    public void updateServiceLocationForSubscriber(int param1Int) throws RemoteException {}
    
    public void enableLocationUpdates() throws RemoteException {}
    
    public void enableLocationUpdatesForSubscriber(int param1Int) throws RemoteException {}
    
    public void disableLocationUpdates() throws RemoteException {}
    
    public void disableLocationUpdatesForSubscriber(int param1Int) throws RemoteException {}
    
    public boolean enableDataConnectivity() throws RemoteException {
      return false;
    }
    
    public boolean disableDataConnectivity() throws RemoteException {
      return false;
    }
    
    public boolean isDataConnectivityPossible(int param1Int) throws RemoteException {
      return false;
    }
    
    public CellIdentity getCellLocation(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getNetworkCountryIsoForPhone(int param1Int) throws RemoteException {
      return null;
    }
    
    public List<NeighboringCellInfo> getNeighboringCellInfo(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public int getCallState() throws RemoteException {
      return 0;
    }
    
    public int getCallStateForSlot(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int getDataActivity() throws RemoteException {
      return 0;
    }
    
    public int getDataActivityForSubId(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int getDataState() throws RemoteException {
      return 0;
    }
    
    public int getDataStateForSubId(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int getActivePhoneType() throws RemoteException {
      return 0;
    }
    
    public int getActivePhoneTypeForSlot(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int getCdmaEriIconIndex(String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public int getCdmaEriIconIndexForSubscriber(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public int getCdmaEriIconMode(String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public int getCdmaEriIconModeForSubscriber(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public String getCdmaEriText(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getCdmaEriTextForSubscriber(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public boolean needsOtaServiceProvisioning() throws RemoteException {
      return false;
    }
    
    public boolean setVoiceMailNumber(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public void setVoiceActivationState(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void setDataActivationState(int param1Int1, int param1Int2) throws RemoteException {}
    
    public int getVoiceActivationState(int param1Int, String param1String) throws RemoteException {
      return 0;
    }
    
    public int getDataActivationState(int param1Int, String param1String) throws RemoteException {
      return 0;
    }
    
    public int getVoiceMessageCountForSubscriber(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public boolean isConcurrentVoiceAndDataAllowed(int param1Int) throws RemoteException {
      return false;
    }
    
    public Bundle getVisualVoicemailSettings(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public String getVisualVoicemailPackageName(String param1String1, String param1String2, int param1Int) throws RemoteException {
      return null;
    }
    
    public void enableVisualVoicemailSmsFilter(String param1String, int param1Int, VisualVoicemailSmsFilterSettings param1VisualVoicemailSmsFilterSettings) throws RemoteException {}
    
    public void disableVisualVoicemailSmsFilter(String param1String, int param1Int) throws RemoteException {}
    
    public VisualVoicemailSmsFilterSettings getVisualVoicemailSmsFilterSettings(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public VisualVoicemailSmsFilterSettings getActiveVisualVoicemailSmsFilterSettings(int param1Int) throws RemoteException {
      return null;
    }
    
    public void sendVisualVoicemailSmsForSubscriber(String param1String1, String param1String2, int param1Int1, String param1String3, int param1Int2, String param1String4, PendingIntent param1PendingIntent) throws RemoteException {}
    
    public void sendDialerSpecialCode(String param1String1, String param1String2) throws RemoteException {}
    
    public int getNetworkTypeForSubscriber(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public int getDataNetworkType(String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public int getDataNetworkTypeForSubscriber(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public int getVoiceNetworkTypeForSubscriber(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public boolean hasIccCard() throws RemoteException {
      return false;
    }
    
    public boolean hasIccCardUsingSlotIndex(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getLteOnCdmaMode(String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public int getLteOnCdmaModeForSubscriber(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public List<CellInfo> getAllCellInfo(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public void requestCellInfoUpdate(int param1Int, ICellInfoCallback param1ICellInfoCallback, String param1String1, String param1String2) throws RemoteException {}
    
    public void requestCellInfoUpdateWithWorkSource(int param1Int, ICellInfoCallback param1ICellInfoCallback, String param1String1, String param1String2, WorkSource param1WorkSource) throws RemoteException {}
    
    public void setCellInfoListRate(int param1Int) throws RemoteException {}
    
    public IccOpenLogicalChannelResponse iccOpenLogicalChannelBySlot(int param1Int1, String param1String1, String param1String2, int param1Int2) throws RemoteException {
      return null;
    }
    
    public IccOpenLogicalChannelResponse iccOpenLogicalChannel(int param1Int1, String param1String1, String param1String2, int param1Int2) throws RemoteException {
      return null;
    }
    
    public boolean iccCloseLogicalChannelBySlot(int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public boolean iccCloseLogicalChannel(int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public String iccTransmitApduLogicalChannelBySlot(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6, int param1Int7, String param1String) throws RemoteException {
      return null;
    }
    
    public String iccTransmitApduLogicalChannel(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6, int param1Int7, String param1String) throws RemoteException {
      return null;
    }
    
    public String iccTransmitApduBasicChannelBySlot(int param1Int1, String param1String1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6, String param1String2) throws RemoteException {
      return null;
    }
    
    public String iccTransmitApduBasicChannel(int param1Int1, String param1String1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6, String param1String2) throws RemoteException {
      return null;
    }
    
    public byte[] iccExchangeSimIO(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6, String param1String) throws RemoteException {
      return null;
    }
    
    public String sendEnvelopeWithStatus(int param1Int, String param1String) throws RemoteException {
      return null;
    }
    
    public String nvReadItem(int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean nvWriteItem(int param1Int, String param1String) throws RemoteException {
      return false;
    }
    
    public boolean nvWriteCdmaPrl(byte[] param1ArrayOfbyte) throws RemoteException {
      return false;
    }
    
    public boolean resetModemConfig(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean rebootModem(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getCalculatedPreferredNetworkType(String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public int getPreferredNetworkType(int param1Int) throws RemoteException {
      return 0;
    }
    
    public boolean isTetheringApnRequiredForSubscriber(int param1Int) throws RemoteException {
      return false;
    }
    
    public void enableIms(int param1Int) throws RemoteException {}
    
    public void disableIms(int param1Int) throws RemoteException {}
    
    public void resetIms(int param1Int) throws RemoteException {}
    
    public IImsMmTelFeature getMmTelFeatureAndListen(int param1Int, IImsServiceFeatureCallback param1IImsServiceFeatureCallback) throws RemoteException {
      return null;
    }
    
    public IImsRcsFeature getRcsFeatureAndListen(int param1Int, IImsServiceFeatureCallback param1IImsServiceFeatureCallback) throws RemoteException {
      return null;
    }
    
    public void unregisterImsFeatureCallback(int param1Int1, int param1Int2, IImsServiceFeatureCallback param1IImsServiceFeatureCallback) throws RemoteException {}
    
    public IImsRegistration getImsRegistration(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public IImsConfig getImsConfig(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public boolean setBoundImsServiceOverride(int param1Int, boolean param1Boolean, int[] param1ArrayOfint, String param1String) throws RemoteException {
      return false;
    }
    
    public String getBoundImsServicePackage(int param1Int1, boolean param1Boolean, int param1Int2) throws RemoteException {
      return null;
    }
    
    public void getImsMmTelFeatureState(int param1Int, IIntegerConsumer param1IIntegerConsumer) throws RemoteException {}
    
    public void setNetworkSelectionModeAutomatic(int param1Int) throws RemoteException {}
    
    public CellNetworkScanResult getCellNetworkScanResults(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public int requestNetworkScan(int param1Int, NetworkScanRequest param1NetworkScanRequest, Messenger param1Messenger, IBinder param1IBinder, String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public void stopNetworkScan(int param1Int1, int param1Int2) throws RemoteException {}
    
    public boolean setNetworkSelectionModeManual(int param1Int, OperatorInfo param1OperatorInfo, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public long getAllowedNetworkTypes(int param1Int) throws RemoteException {
      return 0L;
    }
    
    public boolean setAllowedNetworkTypes(int param1Int, long param1Long) throws RemoteException {
      return false;
    }
    
    public long getAllowedNetworkTypesForReason(int param1Int1, int param1Int2) throws RemoteException {
      return 0L;
    }
    
    public long getEffectiveAllowedNetworkTypes(int param1Int) throws RemoteException {
      return 0L;
    }
    
    public boolean setAllowedNetworkTypesForReason(int param1Int1, int param1Int2, long param1Long) throws RemoteException {
      return false;
    }
    
    public boolean setPreferredNetworkType(int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public void setUserDataEnabled(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public boolean getDataEnabled(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isUserDataEnabled(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isDataEnabled(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isManualNetworkSelectionAllowed(int param1Int) throws RemoteException {
      return false;
    }
    
    public void setAlwaysReportSignalStrength(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public String[] getPcscfAddress(String param1String1, String param1String2, String param1String3) throws RemoteException {
      return null;
    }
    
    public void setImsRegistrationState(boolean param1Boolean) throws RemoteException {}
    
    public String getCdmaMdn(int param1Int) throws RemoteException {
      return null;
    }
    
    public String getCdmaMin(int param1Int) throws RemoteException {
      return null;
    }
    
    public void requestNumberVerification(PhoneNumberRange param1PhoneNumberRange, long param1Long, INumberVerificationCallback param1INumberVerificationCallback, String param1String) throws RemoteException {}
    
    public int getCarrierPrivilegeStatus(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int getCarrierPrivilegeStatusForUid(int param1Int1, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public int checkCarrierPrivilegesForPackage(int param1Int, String param1String) throws RemoteException {
      return 0;
    }
    
    public int checkCarrierPrivilegesForPackageAnyPhone(String param1String) throws RemoteException {
      return 0;
    }
    
    public List<String> getCarrierPackageNamesForIntentAndPhone(Intent param1Intent, int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean setLine1NumberForDisplayForSubscriber(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public String getLine1NumberForDisplay(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getLine1AlphaTagForDisplay(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String[] getMergedSubscriberIds(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String[] getMergedImsisFromGroup(int param1Int, String param1String) throws RemoteException {
      return null;
    }
    
    public boolean setOperatorBrandOverride(int param1Int, String param1String) throws RemoteException {
      return false;
    }
    
    public boolean setRoamingOverride(int param1Int, List<String> param1List1, List<String> param1List2, List<String> param1List3, List<String> param1List4) throws RemoteException {
      return false;
    }
    
    public int invokeOemRilRequestRaw(byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2) throws RemoteException {
      return 0;
    }
    
    public boolean needMobileRadioShutdown() throws RemoteException {
      return false;
    }
    
    public void shutdownMobileRadios() throws RemoteException {}
    
    public void setRadioCapability(RadioAccessFamily[] param1ArrayOfRadioAccessFamily) throws RemoteException {}
    
    public int getRadioAccessFamily(int param1Int, String param1String) throws RemoteException {
      return 0;
    }
    
    public void enableVideoCalling(boolean param1Boolean) throws RemoteException {}
    
    public boolean isVideoCallingEnabled(String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public boolean canChangeDtmfToneLength(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public boolean isWorldPhone(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public boolean isTtyModeSupported() throws RemoteException {
      return false;
    }
    
    public boolean isRttSupported(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isHearingAidCompatibilitySupported() throws RemoteException {
      return false;
    }
    
    public boolean isImsRegistered(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isWifiCallingAvailable(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isVideoTelephonyAvailable(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getImsRegTechnologyForMmTel(int param1Int) throws RemoteException {
      return 0;
    }
    
    public String getDeviceId(String param1String) throws RemoteException {
      return null;
    }
    
    public String getDeviceIdWithFeature(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getImeiForSlot(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getTypeAllocationCodeForSlot(int param1Int) throws RemoteException {
      return null;
    }
    
    public String getMeidForSlot(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getManufacturerCodeForSlot(int param1Int) throws RemoteException {
      return null;
    }
    
    public String getDeviceSoftwareVersionForSlot(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public int getSubIdForPhoneAccount(PhoneAccount param1PhoneAccount) throws RemoteException {
      return 0;
    }
    
    public int getSubIdForPhoneAccountHandle(PhoneAccountHandle param1PhoneAccountHandle, String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public PhoneAccountHandle getPhoneAccountHandleForSubscriptionId(int param1Int) throws RemoteException {
      return null;
    }
    
    public void factoryReset(int param1Int) throws RemoteException {}
    
    public String getSimLocaleForSubscriber(int param1Int) throws RemoteException {
      return null;
    }
    
    public void requestModemActivityInfo(ResultReceiver param1ResultReceiver) throws RemoteException {}
    
    public ServiceState getServiceStateForSubscriber(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public Uri getVoicemailRingtoneUri(PhoneAccountHandle param1PhoneAccountHandle) throws RemoteException {
      return null;
    }
    
    public void setVoicemailRingtoneUri(String param1String, PhoneAccountHandle param1PhoneAccountHandle, Uri param1Uri) throws RemoteException {}
    
    public boolean isVoicemailVibrationEnabled(PhoneAccountHandle param1PhoneAccountHandle) throws RemoteException {
      return false;
    }
    
    public void setVoicemailVibrationEnabled(String param1String, PhoneAccountHandle param1PhoneAccountHandle, boolean param1Boolean) throws RemoteException {}
    
    public List<String> getPackagesWithCarrierPrivileges(int param1Int) throws RemoteException {
      return null;
    }
    
    public List<String> getPackagesWithCarrierPrivilegesForAllPhones() throws RemoteException {
      return null;
    }
    
    public String getAidForAppType(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public String getEsn(int param1Int) throws RemoteException {
      return null;
    }
    
    public String getCdmaPrlVersion(int param1Int) throws RemoteException {
      return null;
    }
    
    public List<TelephonyHistogram> getTelephonyHistograms() throws RemoteException {
      return null;
    }
    
    public int setAllowedCarriers(CarrierRestrictionRules param1CarrierRestrictionRules) throws RemoteException {
      return 0;
    }
    
    public CarrierRestrictionRules getAllowedCarriers() throws RemoteException {
      return null;
    }
    
    public int getSubscriptionCarrierId(int param1Int) throws RemoteException {
      return 0;
    }
    
    public String getSubscriptionCarrierName(int param1Int) throws RemoteException {
      return null;
    }
    
    public int getSubscriptionSpecificCarrierId(int param1Int) throws RemoteException {
      return 0;
    }
    
    public String getSubscriptionSpecificCarrierName(int param1Int) throws RemoteException {
      return null;
    }
    
    public int getCarrierIdFromMccMnc(int param1Int, String param1String, boolean param1Boolean) throws RemoteException {
      return 0;
    }
    
    public void carrierActionSetMeteredApnsEnabled(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void carrierActionSetRadioEnabled(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void carrierActionReportDefaultNetworkStatus(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void carrierActionResetAll(int param1Int) throws RemoteException {}
    
    public CallForwardingInfo getCallForwarding(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public boolean setCallForwarding(int param1Int, CallForwardingInfo param1CallForwardingInfo) throws RemoteException {
      return false;
    }
    
    public int getCallWaitingStatus(int param1Int) throws RemoteException {
      return 0;
    }
    
    public boolean setCallWaitingStatus(int param1Int, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public void setPolicyDataEnabled(boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public List<ClientRequestStats> getClientRequestStats(String param1String1, String param1String2, int param1Int) throws RemoteException {
      return null;
    }
    
    public void setSimPowerStateForSlot(int param1Int1, int param1Int2) throws RemoteException {}
    
    public String[] getForbiddenPlmns(int param1Int1, int param1Int2, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public int setForbiddenPlmns(int param1Int1, int param1Int2, List<String> param1List, String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public boolean getEmergencyCallbackMode(int param1Int) throws RemoteException {
      return false;
    }
    
    public SignalStrength getSignalStrength(int param1Int) throws RemoteException {
      return null;
    }
    
    public int getCardIdForDefaultEuicc(int param1Int, String param1String) throws RemoteException {
      return 0;
    }
    
    public List<UiccCardInfo> getUiccCardsInfo(String param1String) throws RemoteException {
      return null;
    }
    
    public UiccSlotInfo[] getUiccSlotsInfo() throws RemoteException {
      return null;
    }
    
    public boolean switchSlots(int[] param1ArrayOfint) throws RemoteException {
      return false;
    }
    
    public boolean isDataRoamingEnabled(int param1Int) throws RemoteException {
      return false;
    }
    
    public void setDataRoamingEnabled(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public int getCdmaRoamingMode(int param1Int) throws RemoteException {
      return 0;
    }
    
    public boolean setCdmaRoamingMode(int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public boolean setCdmaSubscriptionMode(int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public void setCarrierTestOverride(int param1Int, String param1String1, String param1String2, String param1String3, String param1String4, String param1String5, String param1String6, String param1String7, String param1String8, String param1String9) throws RemoteException {}
    
    public int getCarrierIdListVersion(int param1Int) throws RemoteException {
      return 0;
    }
    
    public void refreshUiccProfile(int param1Int) throws RemoteException {}
    
    public int getNumberOfModemsWithSimultaneousDataConnections(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public int getNetworkSelectionMode(int param1Int) throws RemoteException {
      return 0;
    }
    
    public boolean isInEmergencySmsMode() throws RemoteException {
      return false;
    }
    
    public int getRadioPowerState(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public void registerImsRegistrationCallback(int param1Int, IImsRegistrationCallback param1IImsRegistrationCallback) throws RemoteException {}
    
    public void unregisterImsRegistrationCallback(int param1Int, IImsRegistrationCallback param1IImsRegistrationCallback) throws RemoteException {}
    
    public void getImsMmTelRegistrationState(int param1Int, IIntegerConsumer param1IIntegerConsumer) throws RemoteException {}
    
    public void getImsMmTelRegistrationTransportType(int param1Int, IIntegerConsumer param1IIntegerConsumer) throws RemoteException {}
    
    public void registerMmTelCapabilityCallback(int param1Int, IImsCapabilityCallback param1IImsCapabilityCallback) throws RemoteException {}
    
    public void unregisterMmTelCapabilityCallback(int param1Int, IImsCapabilityCallback param1IImsCapabilityCallback) throws RemoteException {}
    
    public boolean isCapable(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {
      return false;
    }
    
    public boolean isAvailable(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {
      return false;
    }
    
    public void isMmTelCapabilitySupported(int param1Int1, IIntegerConsumer param1IIntegerConsumer, int param1Int2, int param1Int3) throws RemoteException {}
    
    public boolean isAdvancedCallingSettingEnabled(int param1Int) throws RemoteException {
      return false;
    }
    
    public void setAdvancedCallingSettingEnabled(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public boolean isVtSettingEnabled(int param1Int) throws RemoteException {
      return false;
    }
    
    public void setVtSettingEnabled(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public boolean isVoWiFiSettingEnabled(int param1Int) throws RemoteException {
      return false;
    }
    
    public void setVoWiFiSettingEnabled(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public boolean isVoWiFiRoamingSettingEnabled(int param1Int) throws RemoteException {
      return false;
    }
    
    public void setVoWiFiRoamingSettingEnabled(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void setVoWiFiNonPersistent(int param1Int1, boolean param1Boolean, int param1Int2) throws RemoteException {}
    
    public int getVoWiFiModeSetting(int param1Int) throws RemoteException {
      return 0;
    }
    
    public void setVoWiFiModeSetting(int param1Int1, int param1Int2) throws RemoteException {}
    
    public int getVoWiFiRoamingModeSetting(int param1Int) throws RemoteException {
      return 0;
    }
    
    public void setVoWiFiRoamingModeSetting(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void setRttCapabilitySetting(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public boolean isTtyOverVolteEnabled(int param1Int) throws RemoteException {
      return false;
    }
    
    public Map getEmergencyNumberList(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public boolean isEmergencyNumber(String param1String, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public List<String> getCertsFromCarrierPrivilegeAccessRules(int param1Int) throws RemoteException {
      return null;
    }
    
    public void registerImsProvisioningChangedCallback(int param1Int, IImsConfigCallback param1IImsConfigCallback) throws RemoteException {}
    
    public void unregisterImsProvisioningChangedCallback(int param1Int, IImsConfigCallback param1IImsConfigCallback) throws RemoteException {}
    
    public void setImsProvisioningStatusForCapability(int param1Int1, int param1Int2, int param1Int3, boolean param1Boolean) throws RemoteException {}
    
    public boolean getImsProvisioningStatusForCapability(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {
      return false;
    }
    
    public boolean getRcsProvisioningStatusForCapability(int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public void setRcsProvisioningStatusForCapability(int param1Int1, int param1Int2, boolean param1Boolean) throws RemoteException {}
    
    public boolean isMmTelCapabilityProvisionedInCache(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {
      return false;
    }
    
    public void cacheMmTelCapabilityProvisioning(int param1Int1, int param1Int2, int param1Int3, boolean param1Boolean) throws RemoteException {}
    
    public int getImsProvisioningInt(int param1Int1, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public String getImsProvisioningString(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public int setImsProvisioningInt(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {
      return 0;
    }
    
    public int setImsProvisioningString(int param1Int1, int param1Int2, String param1String) throws RemoteException {
      return 0;
    }
    
    public void updateEmergencyNumberListTestMode(int param1Int, EmergencyNumber param1EmergencyNumber) throws RemoteException {}
    
    public List<String> getEmergencyNumberListTestMode() throws RemoteException {
      return null;
    }
    
    public int getEmergencyNumberDbVersion(int param1Int) throws RemoteException {
      return 0;
    }
    
    public void notifyOtaEmergencyNumberDbInstalled() throws RemoteException {}
    
    public void updateOtaEmergencyNumberDbFilePath(ParcelFileDescriptor param1ParcelFileDescriptor) throws RemoteException {}
    
    public void resetOtaEmergencyNumberDbFilePath() throws RemoteException {}
    
    public boolean enableModemForSlot(int param1Int, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public void setMultiSimCarrierRestriction(boolean param1Boolean) throws RemoteException {}
    
    public int isMultiSimSupported(String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public void switchMultiSimConfig(int param1Int) throws RemoteException {}
    
    public boolean doesSwitchMultiSimConfigTriggerReboot(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public int[] getSlotsMapping() throws RemoteException {
      return null;
    }
    
    public int getRadioHalVersion() throws RemoteException {
      return 0;
    }
    
    public String getCurrentPackageName() throws RemoteException {
      return null;
    }
    
    public boolean isApplicationOnUicc(int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public boolean isModemEnabledForSlot(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public boolean isDataEnabledForApn(int param1Int1, int param1Int2, String param1String) throws RemoteException {
      return false;
    }
    
    public boolean isApnMetered(int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public void setSystemSelectionChannels(List<RadioAccessSpecifier> param1List, int param1Int, IBooleanConsumer param1IBooleanConsumer) throws RemoteException {}
    
    public boolean isMvnoMatched(int param1Int1, int param1Int2, String param1String) throws RemoteException {
      return false;
    }
    
    public void enqueueSmsPickResult(String param1String1, String param1String2, IIntegerConsumer param1IIntegerConsumer) throws RemoteException {}
    
    public String getMmsUserAgent(int param1Int) throws RemoteException {
      return null;
    }
    
    public String getMmsUAProfUrl(int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean setDataAllowedDuringVoiceCall(int param1Int, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isDataAllowedInVoiceCall(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean setAlwaysAllowMmsData(int param1Int, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public void setCepEnabled(boolean param1Boolean) throws RemoteException {}
    
    public void notifyRcsAutoConfigurationReceived(int param1Int, byte[] param1ArrayOfbyte, boolean param1Boolean) throws RemoteException {}
    
    public boolean isIccLockEnabled(int param1Int) throws RemoteException {
      return false;
    }
    
    public int setIccLockEnabled(int param1Int, boolean param1Boolean, String param1String) throws RemoteException {
      return 0;
    }
    
    public int changeIccLockPassword(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public void requestUserActivityNotification() throws RemoteException {}
    
    public void userActivity() throws RemoteException {}
    
    public String getManualNetworkSelectionPlmn(int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean canConnectTo5GInDsdsMode() throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITelephony {
    private static final String DESCRIPTOR = "com.android.internal.telephony.ITelephony";
    
    static final int TRANSACTION_cacheMmTelCapabilityProvisioning = 248;
    
    static final int TRANSACTION_call = 2;
    
    static final int TRANSACTION_canChangeDtmfToneLength = 143;
    
    static final int TRANSACTION_canConnectTo5GInDsdsMode = 287;
    
    static final int TRANSACTION_carrierActionReportDefaultNetworkStatus = 185;
    
    static final int TRANSACTION_carrierActionResetAll = 186;
    
    static final int TRANSACTION_carrierActionSetMeteredApnsEnabled = 183;
    
    static final int TRANSACTION_carrierActionSetRadioEnabled = 184;
    
    static final int TRANSACTION_changeIccLockPassword = 283;
    
    static final int TRANSACTION_checkCarrierPrivilegesForPackage = 126;
    
    static final int TRANSACTION_checkCarrierPrivilegesForPackageAnyPhone = 127;
    
    static final int TRANSACTION_dial = 1;
    
    static final int TRANSACTION_disableDataConnectivity = 26;
    
    static final int TRANSACTION_disableIms = 92;
    
    static final int TRANSACTION_disableLocationUpdates = 23;
    
    static final int TRANSACTION_disableLocationUpdatesForSubscriber = 24;
    
    static final int TRANSACTION_disableVisualVoicemailSmsFilter = 56;
    
    static final int TRANSACTION_doesSwitchMultiSimConfigTriggerReboot = 263;
    
    static final int TRANSACTION_enableDataConnectivity = 25;
    
    static final int TRANSACTION_enableIms = 91;
    
    static final int TRANSACTION_enableLocationUpdates = 21;
    
    static final int TRANSACTION_enableLocationUpdatesForSubscriber = 22;
    
    static final int TRANSACTION_enableModemForSlot = 259;
    
    static final int TRANSACTION_enableVideoCalling = 141;
    
    static final int TRANSACTION_enableVisualVoicemailSmsFilter = 55;
    
    static final int TRANSACTION_enqueueSmsPickResult = 273;
    
    static final int TRANSACTION_factoryReset = 162;
    
    static final int TRANSACTION_getActivePhoneType = 37;
    
    static final int TRANSACTION_getActivePhoneTypeForSlot = 38;
    
    static final int TRANSACTION_getActiveVisualVoicemailSmsFilterSettings = 58;
    
    static final int TRANSACTION_getAidForAppType = 172;
    
    static final int TRANSACTION_getAllCellInfo = 69;
    
    static final int TRANSACTION_getAllowedCarriers = 177;
    
    static final int TRANSACTION_getAllowedNetworkTypes = 107;
    
    static final int TRANSACTION_getAllowedNetworkTypesForReason = 109;
    
    static final int TRANSACTION_getBoundImsServicePackage = 100;
    
    static final int TRANSACTION_getCalculatedPreferredNetworkType = 88;
    
    static final int TRANSACTION_getCallForwarding = 187;
    
    static final int TRANSACTION_getCallState = 31;
    
    static final int TRANSACTION_getCallStateForSlot = 32;
    
    static final int TRANSACTION_getCallWaitingStatus = 189;
    
    static final int TRANSACTION_getCardIdForDefaultEuicc = 198;
    
    static final int TRANSACTION_getCarrierIdFromMccMnc = 182;
    
    static final int TRANSACTION_getCarrierIdListVersion = 208;
    
    static final int TRANSACTION_getCarrierPackageNamesForIntentAndPhone = 128;
    
    static final int TRANSACTION_getCarrierPrivilegeStatus = 124;
    
    static final int TRANSACTION_getCarrierPrivilegeStatusForUid = 125;
    
    static final int TRANSACTION_getCdmaEriIconIndex = 39;
    
    static final int TRANSACTION_getCdmaEriIconIndexForSubscriber = 40;
    
    static final int TRANSACTION_getCdmaEriIconMode = 41;
    
    static final int TRANSACTION_getCdmaEriIconModeForSubscriber = 42;
    
    static final int TRANSACTION_getCdmaEriText = 43;
    
    static final int TRANSACTION_getCdmaEriTextForSubscriber = 44;
    
    static final int TRANSACTION_getCdmaMdn = 121;
    
    static final int TRANSACTION_getCdmaMin = 122;
    
    static final int TRANSACTION_getCdmaPrlVersion = 174;
    
    static final int TRANSACTION_getCdmaRoamingMode = 204;
    
    static final int TRANSACTION_getCellLocation = 28;
    
    static final int TRANSACTION_getCellNetworkScanResults = 103;
    
    static final int TRANSACTION_getCertsFromCarrierPrivilegeAccessRules = 240;
    
    static final int TRANSACTION_getClientRequestStats = 192;
    
    static final int TRANSACTION_getCurrentPackageName = 266;
    
    static final int TRANSACTION_getDataActivationState = 50;
    
    static final int TRANSACTION_getDataActivity = 33;
    
    static final int TRANSACTION_getDataActivityForSubId = 34;
    
    static final int TRANSACTION_getDataEnabled = 114;
    
    static final int TRANSACTION_getDataNetworkType = 62;
    
    static final int TRANSACTION_getDataNetworkTypeForSubscriber = 63;
    
    static final int TRANSACTION_getDataState = 35;
    
    static final int TRANSACTION_getDataStateForSubId = 36;
    
    static final int TRANSACTION_getDeviceId = 152;
    
    static final int TRANSACTION_getDeviceIdWithFeature = 153;
    
    static final int TRANSACTION_getDeviceSoftwareVersionForSlot = 158;
    
    static final int TRANSACTION_getEffectiveAllowedNetworkTypes = 110;
    
    static final int TRANSACTION_getEmergencyCallbackMode = 196;
    
    static final int TRANSACTION_getEmergencyNumberDbVersion = 255;
    
    static final int TRANSACTION_getEmergencyNumberList = 238;
    
    static final int TRANSACTION_getEmergencyNumberListTestMode = 254;
    
    static final int TRANSACTION_getEsn = 173;
    
    static final int TRANSACTION_getForbiddenPlmns = 194;
    
    static final int TRANSACTION_getImeiForSlot = 154;
    
    static final int TRANSACTION_getImsConfig = 98;
    
    static final int TRANSACTION_getImsMmTelFeatureState = 101;
    
    static final int TRANSACTION_getImsMmTelRegistrationState = 216;
    
    static final int TRANSACTION_getImsMmTelRegistrationTransportType = 217;
    
    static final int TRANSACTION_getImsProvisioningInt = 249;
    
    static final int TRANSACTION_getImsProvisioningStatusForCapability = 244;
    
    static final int TRANSACTION_getImsProvisioningString = 250;
    
    static final int TRANSACTION_getImsRegTechnologyForMmTel = 151;
    
    static final int TRANSACTION_getImsRegistration = 97;
    
    static final int TRANSACTION_getLine1AlphaTagForDisplay = 131;
    
    static final int TRANSACTION_getLine1NumberForDisplay = 130;
    
    static final int TRANSACTION_getLteOnCdmaMode = 67;
    
    static final int TRANSACTION_getLteOnCdmaModeForSubscriber = 68;
    
    static final int TRANSACTION_getManualNetworkSelectionPlmn = 286;
    
    static final int TRANSACTION_getManufacturerCodeForSlot = 157;
    
    static final int TRANSACTION_getMeidForSlot = 156;
    
    static final int TRANSACTION_getMergedImsisFromGroup = 133;
    
    static final int TRANSACTION_getMergedSubscriberIds = 132;
    
    static final int TRANSACTION_getMmTelFeatureAndListen = 94;
    
    static final int TRANSACTION_getMmsUAProfUrl = 275;
    
    static final int TRANSACTION_getMmsUserAgent = 274;
    
    static final int TRANSACTION_getNeighboringCellInfo = 30;
    
    static final int TRANSACTION_getNetworkCountryIsoForPhone = 29;
    
    static final int TRANSACTION_getNetworkSelectionMode = 211;
    
    static final int TRANSACTION_getNetworkTypeForSubscriber = 61;
    
    static final int TRANSACTION_getNumberOfModemsWithSimultaneousDataConnections = 210;
    
    static final int TRANSACTION_getPackagesWithCarrierPrivileges = 170;
    
    static final int TRANSACTION_getPackagesWithCarrierPrivilegesForAllPhones = 171;
    
    static final int TRANSACTION_getPcscfAddress = 119;
    
    static final int TRANSACTION_getPhoneAccountHandleForSubscriptionId = 161;
    
    static final int TRANSACTION_getPreferredNetworkType = 89;
    
    static final int TRANSACTION_getRadioAccessFamily = 140;
    
    static final int TRANSACTION_getRadioHalVersion = 265;
    
    static final int TRANSACTION_getRadioPowerState = 213;
    
    static final int TRANSACTION_getRcsFeatureAndListen = 95;
    
    static final int TRANSACTION_getRcsProvisioningStatusForCapability = 245;
    
    static final int TRANSACTION_getServiceStateForSubscriber = 165;
    
    static final int TRANSACTION_getSignalStrength = 197;
    
    static final int TRANSACTION_getSimLocaleForSubscriber = 163;
    
    static final int TRANSACTION_getSlotsMapping = 264;
    
    static final int TRANSACTION_getSubIdForPhoneAccount = 159;
    
    static final int TRANSACTION_getSubIdForPhoneAccountHandle = 160;
    
    static final int TRANSACTION_getSubscriptionCarrierId = 178;
    
    static final int TRANSACTION_getSubscriptionCarrierName = 179;
    
    static final int TRANSACTION_getSubscriptionSpecificCarrierId = 180;
    
    static final int TRANSACTION_getSubscriptionSpecificCarrierName = 181;
    
    static final int TRANSACTION_getTelephonyHistograms = 175;
    
    static final int TRANSACTION_getTypeAllocationCodeForSlot = 155;
    
    static final int TRANSACTION_getUiccCardsInfo = 199;
    
    static final int TRANSACTION_getUiccSlotsInfo = 200;
    
    static final int TRANSACTION_getVisualVoicemailPackageName = 54;
    
    static final int TRANSACTION_getVisualVoicemailSettings = 53;
    
    static final int TRANSACTION_getVisualVoicemailSmsFilterSettings = 57;
    
    static final int TRANSACTION_getVoWiFiModeSetting = 232;
    
    static final int TRANSACTION_getVoWiFiRoamingModeSetting = 234;
    
    static final int TRANSACTION_getVoiceActivationState = 49;
    
    static final int TRANSACTION_getVoiceMessageCountForSubscriber = 51;
    
    static final int TRANSACTION_getVoiceNetworkTypeForSubscriber = 64;
    
    static final int TRANSACTION_getVoicemailRingtoneUri = 166;
    
    static final int TRANSACTION_handlePinMmi = 11;
    
    static final int TRANSACTION_handlePinMmiForSubscriber = 13;
    
    static final int TRANSACTION_handleUssdRequest = 12;
    
    static final int TRANSACTION_hasIccCard = 65;
    
    static final int TRANSACTION_hasIccCardUsingSlotIndex = 66;
    
    static final int TRANSACTION_iccCloseLogicalChannel = 76;
    
    static final int TRANSACTION_iccCloseLogicalChannelBySlot = 75;
    
    static final int TRANSACTION_iccExchangeSimIO = 81;
    
    static final int TRANSACTION_iccOpenLogicalChannel = 74;
    
    static final int TRANSACTION_iccOpenLogicalChannelBySlot = 73;
    
    static final int TRANSACTION_iccTransmitApduBasicChannel = 80;
    
    static final int TRANSACTION_iccTransmitApduBasicChannelBySlot = 79;
    
    static final int TRANSACTION_iccTransmitApduLogicalChannel = 78;
    
    static final int TRANSACTION_iccTransmitApduLogicalChannelBySlot = 77;
    
    static final int TRANSACTION_invokeOemRilRequestRaw = 136;
    
    static final int TRANSACTION_isAdvancedCallingSettingEnabled = 223;
    
    static final int TRANSACTION_isApnMetered = 270;
    
    static final int TRANSACTION_isApplicationOnUicc = 267;
    
    static final int TRANSACTION_isAvailable = 221;
    
    static final int TRANSACTION_isCapable = 220;
    
    static final int TRANSACTION_isConcurrentVoiceAndDataAllowed = 52;
    
    static final int TRANSACTION_isDataAllowedInVoiceCall = 277;
    
    static final int TRANSACTION_isDataConnectivityPossible = 27;
    
    static final int TRANSACTION_isDataEnabled = 116;
    
    static final int TRANSACTION_isDataEnabledForApn = 269;
    
    static final int TRANSACTION_isDataRoamingEnabled = 202;
    
    static final int TRANSACTION_isEmergencyNumber = 239;
    
    static final int TRANSACTION_isHearingAidCompatibilitySupported = 147;
    
    static final int TRANSACTION_isIccLockEnabled = 281;
    
    static final int TRANSACTION_isImsRegistered = 148;
    
    static final int TRANSACTION_isInEmergencySmsMode = 212;
    
    static final int TRANSACTION_isManualNetworkSelectionAllowed = 117;
    
    static final int TRANSACTION_isMmTelCapabilityProvisionedInCache = 247;
    
    static final int TRANSACTION_isMmTelCapabilitySupported = 222;
    
    static final int TRANSACTION_isModemEnabledForSlot = 268;
    
    static final int TRANSACTION_isMultiSimSupported = 261;
    
    static final int TRANSACTION_isMvnoMatched = 272;
    
    static final int TRANSACTION_isRadioOn = 3;
    
    static final int TRANSACTION_isRadioOnForSubscriber = 5;
    
    static final int TRANSACTION_isRadioOnForSubscriberWithFeature = 6;
    
    static final int TRANSACTION_isRadioOnWithFeature = 4;
    
    static final int TRANSACTION_isRttSupported = 146;
    
    static final int TRANSACTION_isTetheringApnRequiredForSubscriber = 90;
    
    static final int TRANSACTION_isTtyModeSupported = 145;
    
    static final int TRANSACTION_isTtyOverVolteEnabled = 237;
    
    static final int TRANSACTION_isUserDataEnabled = 115;
    
    static final int TRANSACTION_isVideoCallingEnabled = 142;
    
    static final int TRANSACTION_isVideoTelephonyAvailable = 150;
    
    static final int TRANSACTION_isVoWiFiRoamingSettingEnabled = 229;
    
    static final int TRANSACTION_isVoWiFiSettingEnabled = 227;
    
    static final int TRANSACTION_isVoicemailVibrationEnabled = 168;
    
    static final int TRANSACTION_isVtSettingEnabled = 225;
    
    static final int TRANSACTION_isWifiCallingAvailable = 149;
    
    static final int TRANSACTION_isWorldPhone = 144;
    
    static final int TRANSACTION_needMobileRadioShutdown = 137;
    
    static final int TRANSACTION_needsOtaServiceProvisioning = 45;
    
    static final int TRANSACTION_notifyOtaEmergencyNumberDbInstalled = 256;
    
    static final int TRANSACTION_notifyRcsAutoConfigurationReceived = 280;
    
    static final int TRANSACTION_nvReadItem = 83;
    
    static final int TRANSACTION_nvWriteCdmaPrl = 85;
    
    static final int TRANSACTION_nvWriteItem = 84;
    
    static final int TRANSACTION_rebootModem = 87;
    
    static final int TRANSACTION_refreshUiccProfile = 209;
    
    static final int TRANSACTION_registerImsProvisioningChangedCallback = 241;
    
    static final int TRANSACTION_registerImsRegistrationCallback = 214;
    
    static final int TRANSACTION_registerMmTelCapabilityCallback = 218;
    
    static final int TRANSACTION_requestCellInfoUpdate = 70;
    
    static final int TRANSACTION_requestCellInfoUpdateWithWorkSource = 71;
    
    static final int TRANSACTION_requestModemActivityInfo = 164;
    
    static final int TRANSACTION_requestNetworkScan = 104;
    
    static final int TRANSACTION_requestNumberVerification = 123;
    
    static final int TRANSACTION_requestUserActivityNotification = 284;
    
    static final int TRANSACTION_resetIms = 93;
    
    static final int TRANSACTION_resetModemConfig = 86;
    
    static final int TRANSACTION_resetOtaEmergencyNumberDbFilePath = 258;
    
    static final int TRANSACTION_sendDialerSpecialCode = 60;
    
    static final int TRANSACTION_sendEnvelopeWithStatus = 82;
    
    static final int TRANSACTION_sendVisualVoicemailSmsForSubscriber = 59;
    
    static final int TRANSACTION_setAdvancedCallingSettingEnabled = 224;
    
    static final int TRANSACTION_setAllowedCarriers = 176;
    
    static final int TRANSACTION_setAllowedNetworkTypes = 108;
    
    static final int TRANSACTION_setAllowedNetworkTypesForReason = 111;
    
    static final int TRANSACTION_setAlwaysAllowMmsData = 278;
    
    static final int TRANSACTION_setAlwaysReportSignalStrength = 118;
    
    static final int TRANSACTION_setBoundImsServiceOverride = 99;
    
    static final int TRANSACTION_setCallForwarding = 188;
    
    static final int TRANSACTION_setCallWaitingStatus = 190;
    
    static final int TRANSACTION_setCarrierTestOverride = 207;
    
    static final int TRANSACTION_setCdmaRoamingMode = 205;
    
    static final int TRANSACTION_setCdmaSubscriptionMode = 206;
    
    static final int TRANSACTION_setCellInfoListRate = 72;
    
    static final int TRANSACTION_setCepEnabled = 279;
    
    static final int TRANSACTION_setDataActivationState = 48;
    
    static final int TRANSACTION_setDataAllowedDuringVoiceCall = 276;
    
    static final int TRANSACTION_setDataRoamingEnabled = 203;
    
    static final int TRANSACTION_setForbiddenPlmns = 195;
    
    static final int TRANSACTION_setIccLockEnabled = 282;
    
    static final int TRANSACTION_setImsProvisioningInt = 251;
    
    static final int TRANSACTION_setImsProvisioningStatusForCapability = 243;
    
    static final int TRANSACTION_setImsProvisioningString = 252;
    
    static final int TRANSACTION_setImsRegistrationState = 120;
    
    static final int TRANSACTION_setLine1NumberForDisplayForSubscriber = 129;
    
    static final int TRANSACTION_setMultiSimCarrierRestriction = 260;
    
    static final int TRANSACTION_setNetworkSelectionModeAutomatic = 102;
    
    static final int TRANSACTION_setNetworkSelectionModeManual = 106;
    
    static final int TRANSACTION_setOperatorBrandOverride = 134;
    
    static final int TRANSACTION_setPolicyDataEnabled = 191;
    
    static final int TRANSACTION_setPreferredNetworkType = 112;
    
    static final int TRANSACTION_setRadio = 16;
    
    static final int TRANSACTION_setRadioCapability = 139;
    
    static final int TRANSACTION_setRadioForSubscriber = 17;
    
    static final int TRANSACTION_setRadioPower = 18;
    
    static final int TRANSACTION_setRcsProvisioningStatusForCapability = 246;
    
    static final int TRANSACTION_setRoamingOverride = 135;
    
    static final int TRANSACTION_setRttCapabilitySetting = 236;
    
    static final int TRANSACTION_setSimPowerStateForSlot = 193;
    
    static final int TRANSACTION_setSystemSelectionChannels = 271;
    
    static final int TRANSACTION_setUserDataEnabled = 113;
    
    static final int TRANSACTION_setVoWiFiModeSetting = 233;
    
    static final int TRANSACTION_setVoWiFiNonPersistent = 231;
    
    static final int TRANSACTION_setVoWiFiRoamingModeSetting = 235;
    
    static final int TRANSACTION_setVoWiFiRoamingSettingEnabled = 230;
    
    static final int TRANSACTION_setVoWiFiSettingEnabled = 228;
    
    static final int TRANSACTION_setVoiceActivationState = 47;
    
    static final int TRANSACTION_setVoiceMailNumber = 46;
    
    static final int TRANSACTION_setVoicemailRingtoneUri = 167;
    
    static final int TRANSACTION_setVoicemailVibrationEnabled = 169;
    
    static final int TRANSACTION_setVtSettingEnabled = 226;
    
    static final int TRANSACTION_shutdownMobileRadios = 138;
    
    static final int TRANSACTION_stopNetworkScan = 105;
    
    static final int TRANSACTION_supplyPinForSubscriber = 7;
    
    static final int TRANSACTION_supplyPinReportResultForSubscriber = 9;
    
    static final int TRANSACTION_supplyPukForSubscriber = 8;
    
    static final int TRANSACTION_supplyPukReportResultForSubscriber = 10;
    
    static final int TRANSACTION_switchMultiSimConfig = 262;
    
    static final int TRANSACTION_switchSlots = 201;
    
    static final int TRANSACTION_toggleRadioOnOff = 14;
    
    static final int TRANSACTION_toggleRadioOnOffForSubscriber = 15;
    
    static final int TRANSACTION_unregisterImsFeatureCallback = 96;
    
    static final int TRANSACTION_unregisterImsProvisioningChangedCallback = 242;
    
    static final int TRANSACTION_unregisterImsRegistrationCallback = 215;
    
    static final int TRANSACTION_unregisterMmTelCapabilityCallback = 219;
    
    static final int TRANSACTION_updateEmergencyNumberListTestMode = 253;
    
    static final int TRANSACTION_updateOtaEmergencyNumberDbFilePath = 257;
    
    static final int TRANSACTION_updateServiceLocation = 19;
    
    static final int TRANSACTION_updateServiceLocationForSubscriber = 20;
    
    static final int TRANSACTION_userActivity = 285;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.ITelephony");
    }
    
    public static ITelephony asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.ITelephony");
      if (iInterface != null && iInterface instanceof ITelephony)
        return (ITelephony)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 287:
          return "canConnectTo5GInDsdsMode";
        case 286:
          return "getManualNetworkSelectionPlmn";
        case 285:
          return "userActivity";
        case 284:
          return "requestUserActivityNotification";
        case 283:
          return "changeIccLockPassword";
        case 282:
          return "setIccLockEnabled";
        case 281:
          return "isIccLockEnabled";
        case 280:
          return "notifyRcsAutoConfigurationReceived";
        case 279:
          return "setCepEnabled";
        case 278:
          return "setAlwaysAllowMmsData";
        case 277:
          return "isDataAllowedInVoiceCall";
        case 276:
          return "setDataAllowedDuringVoiceCall";
        case 275:
          return "getMmsUAProfUrl";
        case 274:
          return "getMmsUserAgent";
        case 273:
          return "enqueueSmsPickResult";
        case 272:
          return "isMvnoMatched";
        case 271:
          return "setSystemSelectionChannels";
        case 270:
          return "isApnMetered";
        case 269:
          return "isDataEnabledForApn";
        case 268:
          return "isModemEnabledForSlot";
        case 267:
          return "isApplicationOnUicc";
        case 266:
          return "getCurrentPackageName";
        case 265:
          return "getRadioHalVersion";
        case 264:
          return "getSlotsMapping";
        case 263:
          return "doesSwitchMultiSimConfigTriggerReboot";
        case 262:
          return "switchMultiSimConfig";
        case 261:
          return "isMultiSimSupported";
        case 260:
          return "setMultiSimCarrierRestriction";
        case 259:
          return "enableModemForSlot";
        case 258:
          return "resetOtaEmergencyNumberDbFilePath";
        case 257:
          return "updateOtaEmergencyNumberDbFilePath";
        case 256:
          return "notifyOtaEmergencyNumberDbInstalled";
        case 255:
          return "getEmergencyNumberDbVersion";
        case 254:
          return "getEmergencyNumberListTestMode";
        case 253:
          return "updateEmergencyNumberListTestMode";
        case 252:
          return "setImsProvisioningString";
        case 251:
          return "setImsProvisioningInt";
        case 250:
          return "getImsProvisioningString";
        case 249:
          return "getImsProvisioningInt";
        case 248:
          return "cacheMmTelCapabilityProvisioning";
        case 247:
          return "isMmTelCapabilityProvisionedInCache";
        case 246:
          return "setRcsProvisioningStatusForCapability";
        case 245:
          return "getRcsProvisioningStatusForCapability";
        case 244:
          return "getImsProvisioningStatusForCapability";
        case 243:
          return "setImsProvisioningStatusForCapability";
        case 242:
          return "unregisterImsProvisioningChangedCallback";
        case 241:
          return "registerImsProvisioningChangedCallback";
        case 240:
          return "getCertsFromCarrierPrivilegeAccessRules";
        case 239:
          return "isEmergencyNumber";
        case 238:
          return "getEmergencyNumberList";
        case 237:
          return "isTtyOverVolteEnabled";
        case 236:
          return "setRttCapabilitySetting";
        case 235:
          return "setVoWiFiRoamingModeSetting";
        case 234:
          return "getVoWiFiRoamingModeSetting";
        case 233:
          return "setVoWiFiModeSetting";
        case 232:
          return "getVoWiFiModeSetting";
        case 231:
          return "setVoWiFiNonPersistent";
        case 230:
          return "setVoWiFiRoamingSettingEnabled";
        case 229:
          return "isVoWiFiRoamingSettingEnabled";
        case 228:
          return "setVoWiFiSettingEnabled";
        case 227:
          return "isVoWiFiSettingEnabled";
        case 226:
          return "setVtSettingEnabled";
        case 225:
          return "isVtSettingEnabled";
        case 224:
          return "setAdvancedCallingSettingEnabled";
        case 223:
          return "isAdvancedCallingSettingEnabled";
        case 222:
          return "isMmTelCapabilitySupported";
        case 221:
          return "isAvailable";
        case 220:
          return "isCapable";
        case 219:
          return "unregisterMmTelCapabilityCallback";
        case 218:
          return "registerMmTelCapabilityCallback";
        case 217:
          return "getImsMmTelRegistrationTransportType";
        case 216:
          return "getImsMmTelRegistrationState";
        case 215:
          return "unregisterImsRegistrationCallback";
        case 214:
          return "registerImsRegistrationCallback";
        case 213:
          return "getRadioPowerState";
        case 212:
          return "isInEmergencySmsMode";
        case 211:
          return "getNetworkSelectionMode";
        case 210:
          return "getNumberOfModemsWithSimultaneousDataConnections";
        case 209:
          return "refreshUiccProfile";
        case 208:
          return "getCarrierIdListVersion";
        case 207:
          return "setCarrierTestOverride";
        case 206:
          return "setCdmaSubscriptionMode";
        case 205:
          return "setCdmaRoamingMode";
        case 204:
          return "getCdmaRoamingMode";
        case 203:
          return "setDataRoamingEnabled";
        case 202:
          return "isDataRoamingEnabled";
        case 201:
          return "switchSlots";
        case 200:
          return "getUiccSlotsInfo";
        case 199:
          return "getUiccCardsInfo";
        case 198:
          return "getCardIdForDefaultEuicc";
        case 197:
          return "getSignalStrength";
        case 196:
          return "getEmergencyCallbackMode";
        case 195:
          return "setForbiddenPlmns";
        case 194:
          return "getForbiddenPlmns";
        case 193:
          return "setSimPowerStateForSlot";
        case 192:
          return "getClientRequestStats";
        case 191:
          return "setPolicyDataEnabled";
        case 190:
          return "setCallWaitingStatus";
        case 189:
          return "getCallWaitingStatus";
        case 188:
          return "setCallForwarding";
        case 187:
          return "getCallForwarding";
        case 186:
          return "carrierActionResetAll";
        case 185:
          return "carrierActionReportDefaultNetworkStatus";
        case 184:
          return "carrierActionSetRadioEnabled";
        case 183:
          return "carrierActionSetMeteredApnsEnabled";
        case 182:
          return "getCarrierIdFromMccMnc";
        case 181:
          return "getSubscriptionSpecificCarrierName";
        case 180:
          return "getSubscriptionSpecificCarrierId";
        case 179:
          return "getSubscriptionCarrierName";
        case 178:
          return "getSubscriptionCarrierId";
        case 177:
          return "getAllowedCarriers";
        case 176:
          return "setAllowedCarriers";
        case 175:
          return "getTelephonyHistograms";
        case 174:
          return "getCdmaPrlVersion";
        case 173:
          return "getEsn";
        case 172:
          return "getAidForAppType";
        case 171:
          return "getPackagesWithCarrierPrivilegesForAllPhones";
        case 170:
          return "getPackagesWithCarrierPrivileges";
        case 169:
          return "setVoicemailVibrationEnabled";
        case 168:
          return "isVoicemailVibrationEnabled";
        case 167:
          return "setVoicemailRingtoneUri";
        case 166:
          return "getVoicemailRingtoneUri";
        case 165:
          return "getServiceStateForSubscriber";
        case 164:
          return "requestModemActivityInfo";
        case 163:
          return "getSimLocaleForSubscriber";
        case 162:
          return "factoryReset";
        case 161:
          return "getPhoneAccountHandleForSubscriptionId";
        case 160:
          return "getSubIdForPhoneAccountHandle";
        case 159:
          return "getSubIdForPhoneAccount";
        case 158:
          return "getDeviceSoftwareVersionForSlot";
        case 157:
          return "getManufacturerCodeForSlot";
        case 156:
          return "getMeidForSlot";
        case 155:
          return "getTypeAllocationCodeForSlot";
        case 154:
          return "getImeiForSlot";
        case 153:
          return "getDeviceIdWithFeature";
        case 152:
          return "getDeviceId";
        case 151:
          return "getImsRegTechnologyForMmTel";
        case 150:
          return "isVideoTelephonyAvailable";
        case 149:
          return "isWifiCallingAvailable";
        case 148:
          return "isImsRegistered";
        case 147:
          return "isHearingAidCompatibilitySupported";
        case 146:
          return "isRttSupported";
        case 145:
          return "isTtyModeSupported";
        case 144:
          return "isWorldPhone";
        case 143:
          return "canChangeDtmfToneLength";
        case 142:
          return "isVideoCallingEnabled";
        case 141:
          return "enableVideoCalling";
        case 140:
          return "getRadioAccessFamily";
        case 139:
          return "setRadioCapability";
        case 138:
          return "shutdownMobileRadios";
        case 137:
          return "needMobileRadioShutdown";
        case 136:
          return "invokeOemRilRequestRaw";
        case 135:
          return "setRoamingOverride";
        case 134:
          return "setOperatorBrandOverride";
        case 133:
          return "getMergedImsisFromGroup";
        case 132:
          return "getMergedSubscriberIds";
        case 131:
          return "getLine1AlphaTagForDisplay";
        case 130:
          return "getLine1NumberForDisplay";
        case 129:
          return "setLine1NumberForDisplayForSubscriber";
        case 128:
          return "getCarrierPackageNamesForIntentAndPhone";
        case 127:
          return "checkCarrierPrivilegesForPackageAnyPhone";
        case 126:
          return "checkCarrierPrivilegesForPackage";
        case 125:
          return "getCarrierPrivilegeStatusForUid";
        case 124:
          return "getCarrierPrivilegeStatus";
        case 123:
          return "requestNumberVerification";
        case 122:
          return "getCdmaMin";
        case 121:
          return "getCdmaMdn";
        case 120:
          return "setImsRegistrationState";
        case 119:
          return "getPcscfAddress";
        case 118:
          return "setAlwaysReportSignalStrength";
        case 117:
          return "isManualNetworkSelectionAllowed";
        case 116:
          return "isDataEnabled";
        case 115:
          return "isUserDataEnabled";
        case 114:
          return "getDataEnabled";
        case 113:
          return "setUserDataEnabled";
        case 112:
          return "setPreferredNetworkType";
        case 111:
          return "setAllowedNetworkTypesForReason";
        case 110:
          return "getEffectiveAllowedNetworkTypes";
        case 109:
          return "getAllowedNetworkTypesForReason";
        case 108:
          return "setAllowedNetworkTypes";
        case 107:
          return "getAllowedNetworkTypes";
        case 106:
          return "setNetworkSelectionModeManual";
        case 105:
          return "stopNetworkScan";
        case 104:
          return "requestNetworkScan";
        case 103:
          return "getCellNetworkScanResults";
        case 102:
          return "setNetworkSelectionModeAutomatic";
        case 101:
          return "getImsMmTelFeatureState";
        case 100:
          return "getBoundImsServicePackage";
        case 99:
          return "setBoundImsServiceOverride";
        case 98:
          return "getImsConfig";
        case 97:
          return "getImsRegistration";
        case 96:
          return "unregisterImsFeatureCallback";
        case 95:
          return "getRcsFeatureAndListen";
        case 94:
          return "getMmTelFeatureAndListen";
        case 93:
          return "resetIms";
        case 92:
          return "disableIms";
        case 91:
          return "enableIms";
        case 90:
          return "isTetheringApnRequiredForSubscriber";
        case 89:
          return "getPreferredNetworkType";
        case 88:
          return "getCalculatedPreferredNetworkType";
        case 87:
          return "rebootModem";
        case 86:
          return "resetModemConfig";
        case 85:
          return "nvWriteCdmaPrl";
        case 84:
          return "nvWriteItem";
        case 83:
          return "nvReadItem";
        case 82:
          return "sendEnvelopeWithStatus";
        case 81:
          return "iccExchangeSimIO";
        case 80:
          return "iccTransmitApduBasicChannel";
        case 79:
          return "iccTransmitApduBasicChannelBySlot";
        case 78:
          return "iccTransmitApduLogicalChannel";
        case 77:
          return "iccTransmitApduLogicalChannelBySlot";
        case 76:
          return "iccCloseLogicalChannel";
        case 75:
          return "iccCloseLogicalChannelBySlot";
        case 74:
          return "iccOpenLogicalChannel";
        case 73:
          return "iccOpenLogicalChannelBySlot";
        case 72:
          return "setCellInfoListRate";
        case 71:
          return "requestCellInfoUpdateWithWorkSource";
        case 70:
          return "requestCellInfoUpdate";
        case 69:
          return "getAllCellInfo";
        case 68:
          return "getLteOnCdmaModeForSubscriber";
        case 67:
          return "getLteOnCdmaMode";
        case 66:
          return "hasIccCardUsingSlotIndex";
        case 65:
          return "hasIccCard";
        case 64:
          return "getVoiceNetworkTypeForSubscriber";
        case 63:
          return "getDataNetworkTypeForSubscriber";
        case 62:
          return "getDataNetworkType";
        case 61:
          return "getNetworkTypeForSubscriber";
        case 60:
          return "sendDialerSpecialCode";
        case 59:
          return "sendVisualVoicemailSmsForSubscriber";
        case 58:
          return "getActiveVisualVoicemailSmsFilterSettings";
        case 57:
          return "getVisualVoicemailSmsFilterSettings";
        case 56:
          return "disableVisualVoicemailSmsFilter";
        case 55:
          return "enableVisualVoicemailSmsFilter";
        case 54:
          return "getVisualVoicemailPackageName";
        case 53:
          return "getVisualVoicemailSettings";
        case 52:
          return "isConcurrentVoiceAndDataAllowed";
        case 51:
          return "getVoiceMessageCountForSubscriber";
        case 50:
          return "getDataActivationState";
        case 49:
          return "getVoiceActivationState";
        case 48:
          return "setDataActivationState";
        case 47:
          return "setVoiceActivationState";
        case 46:
          return "setVoiceMailNumber";
        case 45:
          return "needsOtaServiceProvisioning";
        case 44:
          return "getCdmaEriTextForSubscriber";
        case 43:
          return "getCdmaEriText";
        case 42:
          return "getCdmaEriIconModeForSubscriber";
        case 41:
          return "getCdmaEriIconMode";
        case 40:
          return "getCdmaEriIconIndexForSubscriber";
        case 39:
          return "getCdmaEriIconIndex";
        case 38:
          return "getActivePhoneTypeForSlot";
        case 37:
          return "getActivePhoneType";
        case 36:
          return "getDataStateForSubId";
        case 35:
          return "getDataState";
        case 34:
          return "getDataActivityForSubId";
        case 33:
          return "getDataActivity";
        case 32:
          return "getCallStateForSlot";
        case 31:
          return "getCallState";
        case 30:
          return "getNeighboringCellInfo";
        case 29:
          return "getNetworkCountryIsoForPhone";
        case 28:
          return "getCellLocation";
        case 27:
          return "isDataConnectivityPossible";
        case 26:
          return "disableDataConnectivity";
        case 25:
          return "enableDataConnectivity";
        case 24:
          return "disableLocationUpdatesForSubscriber";
        case 23:
          return "disableLocationUpdates";
        case 22:
          return "enableLocationUpdatesForSubscriber";
        case 21:
          return "enableLocationUpdates";
        case 20:
          return "updateServiceLocationForSubscriber";
        case 19:
          return "updateServiceLocation";
        case 18:
          return "setRadioPower";
        case 17:
          return "setRadioForSubscriber";
        case 16:
          return "setRadio";
        case 15:
          return "toggleRadioOnOffForSubscriber";
        case 14:
          return "toggleRadioOnOff";
        case 13:
          return "handlePinMmiForSubscriber";
        case 12:
          return "handleUssdRequest";
        case 11:
          return "handlePinMmi";
        case 10:
          return "supplyPukReportResultForSubscriber";
        case 9:
          return "supplyPinReportResultForSubscriber";
        case 8:
          return "supplyPukForSubscriber";
        case 7:
          return "supplyPinForSubscriber";
        case 6:
          return "isRadioOnForSubscriberWithFeature";
        case 5:
          return "isRadioOnForSubscriber";
        case 4:
          return "isRadioOnWithFeature";
        case 3:
          return "isRadioOn";
        case 2:
          return "call";
        case 1:
          break;
      } 
      return "dial";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        boolean bool67;
        int i61;
        boolean bool66;
        int i60;
        boolean bool65;
        int i59;
        boolean bool64;
        int i58;
        boolean bool63;
        int i57;
        boolean bool62;
        int i56;
        boolean bool61;
        int i55;
        boolean bool60;
        int i54;
        boolean bool59;
        int i53;
        boolean bool58;
        int i52;
        boolean bool57;
        int i51;
        boolean bool56;
        int i50;
        boolean bool55;
        int i49;
        boolean bool54;
        int i48;
        boolean bool53;
        int i47;
        boolean bool52;
        int i46;
        boolean bool51;
        int i45;
        boolean bool50;
        int i44;
        boolean bool49;
        int i43;
        boolean bool48;
        int i42;
        boolean bool47;
        int i41;
        boolean bool46;
        int i40;
        boolean bool45;
        int i39;
        boolean bool44;
        int i38;
        boolean bool43;
        int i37;
        boolean bool42;
        int i36;
        boolean bool41;
        int i35;
        boolean bool40;
        int i34;
        boolean bool39;
        int i33;
        boolean bool38;
        int i32;
        boolean bool37;
        int i31;
        boolean bool36;
        int i30;
        boolean bool35;
        int i29;
        boolean bool34;
        int i28;
        boolean bool33;
        int i27;
        boolean bool32;
        int i26;
        boolean bool31;
        int i25;
        boolean bool30;
        int i24;
        boolean bool29;
        int i23;
        boolean bool28;
        int i22;
        boolean bool27;
        int i21;
        boolean bool26;
        int i20;
        boolean bool25;
        int i19;
        boolean bool24;
        int i18;
        boolean bool23;
        int i17;
        boolean bool22;
        int i16;
        boolean bool21;
        int i15;
        boolean bool20;
        int i14;
        boolean bool19;
        int i13;
        boolean bool18;
        int i12;
        boolean bool17;
        int i11;
        boolean bool16;
        int i10;
        boolean bool15;
        int i9;
        boolean bool14;
        int i8;
        boolean bool13;
        int i7;
        boolean bool12;
        int i6;
        boolean bool11;
        int i5;
        boolean bool10;
        int i4;
        boolean bool9;
        int i3;
        boolean bool8;
        int i2;
        boolean bool7;
        int i1;
        boolean bool6;
        int n;
        boolean bool5;
        int m;
        boolean bool4;
        int k;
        boolean bool3;
        int j;
        boolean bool2;
        int i;
        boolean bool1;
        String str31;
        IIntegerConsumer iIntegerConsumer3;
        String str30;
        IBooleanConsumer iBooleanConsumer;
        String str29;
        int[] arrayOfInt4;
        String str28;
        List<String> list8;
        String str27;
        IImsConfigCallback iImsConfigCallback;
        List<String> list7;
        String str26;
        Map map;
        IImsCapabilityCallback iImsCapabilityCallback;
        IIntegerConsumer iIntegerConsumer2;
        IImsRegistrationCallback iImsRegistrationCallback;
        String str25;
        int[] arrayOfInt3;
        UiccSlotInfo[] arrayOfUiccSlotInfo;
        String str24;
        List<UiccCardInfo> list6;
        String str23;
        SignalStrength signalStrength;
        String str22, arrayOfString4[];
        List<ClientRequestStats> list5;
        CallForwardingInfo callForwardingInfo;
        String str21;
        CarrierRestrictionRules carrierRestrictionRules;
        List<TelephonyHistogram> list4;
        String str20;
        List<String> list3;
        Uri uri;
        String str19;
        ServiceState serviceState;
        String str18;
        PhoneAccountHandle phoneAccountHandle;
        String str17;
        RadioAccessFamily[] arrayOfRadioAccessFamily;
        byte[] arrayOfByte2;
        String str16, arrayOfString3[], str15, arrayOfString2[], str14;
        List<String> list2;
        String str13, arrayOfString1[], str12;
        CellNetworkScanResult cellNetworkScanResult;
        IIntegerConsumer iIntegerConsumer1;
        String str11;
        IBinder iBinder3;
        IImsServiceFeatureCallback iImsServiceFeatureCallback2;
        IImsRegistration iImsRegistration1;
        IBinder iBinder2;
        IImsServiceFeatureCallback iImsServiceFeatureCallback1;
        IImsRcsFeature iImsRcsFeature1;
        IBinder iBinder1;
        String str10;
        byte[] arrayOfByte1;
        String str9;
        IccOpenLogicalChannelResponse iccOpenLogicalChannelResponse;
        String str8;
        List<CellInfo> list1;
        String str7;
        VisualVoicemailSmsFilterSettings visualVoicemailSmsFilterSettings;
        String str6;
        Bundle bundle;
        String str5;
        List<NeighboringCellInfo> list;
        String str4;
        CellIdentity cellIdentity;
        String str3;
        int[] arrayOfInt2;
        String str2;
        int[] arrayOfInt1;
        String str32;
        ArrayList<RadioAccessSpecifier> arrayList;
        byte[] arrayOfByte4;
        String str37;
        IIntegerConsumer iIntegerConsumer4;
        String str36;
        byte[] arrayOfByte3;
        String str35;
        int[] arrayOfInt5;
        IImsConfig iImsConfig;
        IImsRegistration iImsRegistration2;
        IImsMmTelFeature iImsMmTelFeature;
        String str34;
        ICellInfoCallback iCellInfoCallback;
        String str33;
        INumberVerificationCallback iNumberVerificationCallback;
        String str40;
        IImsRcsFeature iImsRcsFeature2;
        String str39;
        int i62;
        long l;
        IBinder iBinder4 = null;
        String str38 = null, str41 = null, str42 = null;
        boolean bool68 = false, bool69 = false, bool70 = false, bool71 = false, bool72 = false, bool73 = false, bool74 = false, bool75 = false, bool76 = false, bool77 = false, bool78 = false, bool79 = false, bool80 = false, bool81 = false, bool82 = false, bool83 = false, bool84 = false, bool85 = false, bool86 = false, bool87 = false, bool88 = false, bool89 = false, bool90 = false, bool91 = false, bool92 = false, bool93 = false, bool94 = false, bool95 = false, bool96 = false, bool97 = false, bool98 = false, bool99 = false, bool100 = false, bool101 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 287:
            param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephony");
            bool67 = canConnectTo5GInDsdsMode();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool67);
            return true;
          case 286:
            param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephony");
            i61 = param1Parcel1.readInt();
            str31 = getManualNetworkSelectionPlmn(i61);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str31);
            return true;
          case 285:
            str31.enforceInterface("com.android.internal.telephony.ITelephony");
            userActivity();
            return true;
          case 284:
            str31.enforceInterface("com.android.internal.telephony.ITelephony");
            requestUserActivityNotification();
            return true;
          case 283:
            str31.enforceInterface("com.android.internal.telephony.ITelephony");
            i61 = str31.readInt();
            str38 = str31.readString();
            str31 = str31.readString();
            i61 = changeIccLockPassword(i61, str38, str31);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i61);
            return true;
          case 282:
            str31.enforceInterface("com.android.internal.telephony.ITelephony");
            i61 = str31.readInt();
            bool93 = bool101;
            if (str31.readInt() != 0)
              bool93 = true; 
            str31 = str31.readString();
            i61 = setIccLockEnabled(i61, bool93, str31);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i61);
            return true;
          case 281:
            str31.enforceInterface("com.android.internal.telephony.ITelephony");
            i61 = str31.readInt();
            bool66 = isIccLockEnabled(i61);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool66);
            return true;
          case 280:
            str31.enforceInterface("com.android.internal.telephony.ITelephony");
            i60 = str31.readInt();
            arrayOfByte4 = str31.createByteArray();
            bool93 = bool68;
            if (str31.readInt() != 0)
              bool93 = true; 
            notifyRcsAutoConfigurationReceived(i60, arrayOfByte4, bool93);
            param1Parcel2.writeNoException();
            return true;
          case 279:
            str31.enforceInterface("com.android.internal.telephony.ITelephony");
            bool93 = bool69;
            if (str31.readInt() != 0)
              bool93 = true; 
            setCepEnabled(bool93);
            return true;
          case 278:
            str31.enforceInterface("com.android.internal.telephony.ITelephony");
            i60 = str31.readInt();
            bool93 = bool70;
            if (str31.readInt() != 0)
              bool93 = true; 
            bool65 = setAlwaysAllowMmsData(i60, bool93);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool65);
            return true;
          case 277:
            str31.enforceInterface("com.android.internal.telephony.ITelephony");
            i59 = str31.readInt();
            bool64 = isDataAllowedInVoiceCall(i59);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool64);
            return true;
          case 276:
            str31.enforceInterface("com.android.internal.telephony.ITelephony");
            i58 = str31.readInt();
            bool93 = bool71;
            if (str31.readInt() != 0)
              bool93 = true; 
            bool63 = setDataAllowedDuringVoiceCall(i58, bool93);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool63);
            return true;
          case 275:
            str31.enforceInterface("com.android.internal.telephony.ITelephony");
            i57 = str31.readInt();
            str31 = getMmsUAProfUrl(i57);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str31);
            return true;
          case 274:
            str31.enforceInterface("com.android.internal.telephony.ITelephony");
            i57 = str31.readInt();
            str31 = getMmsUserAgent(i57);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str31);
            return true;
          case 273:
            str31.enforceInterface("com.android.internal.telephony.ITelephony");
            str32 = str31.readString();
            str37 = str31.readString();
            iIntegerConsumer3 = IIntegerConsumer.Stub.asInterface(str31.readStrongBinder());
            enqueueSmsPickResult(str32, str37, iIntegerConsumer3);
            return true;
          case 272:
            iIntegerConsumer3.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = iIntegerConsumer3.readInt();
            i57 = iIntegerConsumer3.readInt();
            str30 = iIntegerConsumer3.readString();
            bool62 = isMvnoMatched(param1Int2, i57, str30);
            str32.writeNoException();
            str32.writeInt(bool62);
            return true;
          case 271:
            str30.enforceInterface("com.android.internal.telephony.ITelephony");
            arrayList = str30.createTypedArrayList(RadioAccessSpecifier.CREATOR);
            i56 = str30.readInt();
            iBooleanConsumer = IBooleanConsumer.Stub.asInterface(str30.readStrongBinder());
            setSystemSelectionChannels(arrayList, i56, iBooleanConsumer);
            return true;
          case 270:
            iBooleanConsumer.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = iBooleanConsumer.readInt();
            i56 = iBooleanConsumer.readInt();
            bool61 = isApnMetered(param1Int2, i56);
            arrayList.writeNoException();
            arrayList.writeInt(bool61);
            return true;
          case 269:
            iBooleanConsumer.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = iBooleanConsumer.readInt();
            i55 = iBooleanConsumer.readInt();
            str29 = iBooleanConsumer.readString();
            bool60 = isDataEnabledForApn(param1Int2, i55, str29);
            arrayList.writeNoException();
            arrayList.writeInt(bool60);
            return true;
          case 268:
            str29.enforceInterface("com.android.internal.telephony.ITelephony");
            i54 = str29.readInt();
            str37 = str29.readString();
            str29 = str29.readString();
            bool59 = isModemEnabledForSlot(i54, str37, str29);
            arrayList.writeNoException();
            arrayList.writeInt(bool59);
            return true;
          case 267:
            str29.enforceInterface("com.android.internal.telephony.ITelephony");
            i53 = str29.readInt();
            param1Int2 = str29.readInt();
            bool58 = isApplicationOnUicc(i53, param1Int2);
            arrayList.writeNoException();
            arrayList.writeInt(bool58);
            return true;
          case 266:
            str29.enforceInterface("com.android.internal.telephony.ITelephony");
            str29 = getCurrentPackageName();
            arrayList.writeNoException();
            arrayList.writeString(str29);
            return true;
          case 265:
            str29.enforceInterface("com.android.internal.telephony.ITelephony");
            i52 = getRadioHalVersion();
            arrayList.writeNoException();
            arrayList.writeInt(i52);
            return true;
          case 264:
            str29.enforceInterface("com.android.internal.telephony.ITelephony");
            arrayOfInt4 = getSlotsMapping();
            arrayList.writeNoException();
            arrayList.writeIntArray(arrayOfInt4);
            return true;
          case 263:
            arrayOfInt4.enforceInterface("com.android.internal.telephony.ITelephony");
            i52 = arrayOfInt4.readInt();
            str37 = arrayOfInt4.readString();
            str28 = arrayOfInt4.readString();
            bool57 = doesSwitchMultiSimConfigTriggerReboot(i52, str37, str28);
            arrayList.writeNoException();
            arrayList.writeInt(bool57);
            return true;
          case 262:
            str28.enforceInterface("com.android.internal.telephony.ITelephony");
            i51 = str28.readInt();
            switchMultiSimConfig(i51);
            arrayList.writeNoException();
            return true;
          case 261:
            str28.enforceInterface("com.android.internal.telephony.ITelephony");
            str37 = str28.readString();
            str28 = str28.readString();
            i51 = isMultiSimSupported(str37, str28);
            arrayList.writeNoException();
            arrayList.writeInt(i51);
            return true;
          case 260:
            str28.enforceInterface("com.android.internal.telephony.ITelephony");
            bool93 = bool72;
            if (str28.readInt() != 0)
              bool93 = true; 
            setMultiSimCarrierRestriction(bool93);
            arrayList.writeNoException();
            return true;
          case 259:
            str28.enforceInterface("com.android.internal.telephony.ITelephony");
            i51 = str28.readInt();
            bool93 = bool73;
            if (str28.readInt() != 0)
              bool93 = true; 
            bool56 = enableModemForSlot(i51, bool93);
            arrayList.writeNoException();
            arrayList.writeInt(bool56);
            return true;
          case 258:
            str28.enforceInterface("com.android.internal.telephony.ITelephony");
            resetOtaEmergencyNumberDbFilePath();
            arrayList.writeNoException();
            return true;
          case 257:
            str28.enforceInterface("com.android.internal.telephony.ITelephony");
            if (str28.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel((Parcel)str28);
            } else {
              str28 = null;
            } 
            updateOtaEmergencyNumberDbFilePath((ParcelFileDescriptor)str28);
            arrayList.writeNoException();
            return true;
          case 256:
            str28.enforceInterface("com.android.internal.telephony.ITelephony");
            notifyOtaEmergencyNumberDbInstalled();
            arrayList.writeNoException();
            return true;
          case 255:
            str28.enforceInterface("com.android.internal.telephony.ITelephony");
            i50 = str28.readInt();
            i50 = getEmergencyNumberDbVersion(i50);
            arrayList.writeNoException();
            arrayList.writeInt(i50);
            return true;
          case 254:
            str28.enforceInterface("com.android.internal.telephony.ITelephony");
            list8 = getEmergencyNumberListTestMode();
            arrayList.writeNoException();
            arrayList.writeStringList(list8);
            return true;
          case 253:
            list8.enforceInterface("com.android.internal.telephony.ITelephony");
            i50 = list8.readInt();
            if (list8.readInt() != 0) {
              EmergencyNumber emergencyNumber = (EmergencyNumber)EmergencyNumber.CREATOR.createFromParcel((Parcel)list8);
            } else {
              list8 = null;
            } 
            updateEmergencyNumberListTestMode(i50, (EmergencyNumber)list8);
            arrayList.writeNoException();
            return true;
          case 252:
            list8.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = list8.readInt();
            i50 = list8.readInt();
            str27 = list8.readString();
            i50 = setImsProvisioningString(param1Int2, i50, str27);
            arrayList.writeNoException();
            arrayList.writeInt(i50);
            return true;
          case 251:
            str27.enforceInterface("com.android.internal.telephony.ITelephony");
            i62 = str27.readInt();
            param1Int2 = str27.readInt();
            i50 = str27.readInt();
            i50 = setImsProvisioningInt(i62, param1Int2, i50);
            arrayList.writeNoException();
            arrayList.writeInt(i50);
            return true;
          case 250:
            str27.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = str27.readInt();
            i50 = str27.readInt();
            str27 = getImsProvisioningString(param1Int2, i50);
            arrayList.writeNoException();
            arrayList.writeString(str27);
            return true;
          case 249:
            str27.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = str27.readInt();
            i50 = str27.readInt();
            i50 = getImsProvisioningInt(param1Int2, i50);
            arrayList.writeNoException();
            arrayList.writeInt(i50);
            return true;
          case 248:
            return onTransact$cacheMmTelCapabilityProvisioning$((Parcel)str27, (Parcel)arrayList);
          case 247:
            str27.enforceInterface("com.android.internal.telephony.ITelephony");
            i62 = str27.readInt();
            i50 = str27.readInt();
            param1Int2 = str27.readInt();
            bool55 = isMmTelCapabilityProvisionedInCache(i62, i50, param1Int2);
            arrayList.writeNoException();
            arrayList.writeInt(bool55);
            return true;
          case 246:
            str27.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = str27.readInt();
            i49 = str27.readInt();
            bool93 = bool74;
            if (str27.readInt() != 0)
              bool93 = true; 
            setRcsProvisioningStatusForCapability(param1Int2, i49, bool93);
            arrayList.writeNoException();
            return true;
          case 245:
            str27.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = str27.readInt();
            i49 = str27.readInt();
            bool54 = getRcsProvisioningStatusForCapability(param1Int2, i49);
            arrayList.writeNoException();
            arrayList.writeInt(bool54);
            return true;
          case 244:
            str27.enforceInterface("com.android.internal.telephony.ITelephony");
            i48 = str27.readInt();
            i62 = str27.readInt();
            param1Int2 = str27.readInt();
            bool53 = getImsProvisioningStatusForCapability(i48, i62, param1Int2);
            arrayList.writeNoException();
            arrayList.writeInt(bool53);
            return true;
          case 243:
            str27.enforceInterface("com.android.internal.telephony.ITelephony");
            i62 = str27.readInt();
            param1Int2 = str27.readInt();
            i47 = str27.readInt();
            bool93 = bool75;
            if (str27.readInt() != 0)
              bool93 = true; 
            setImsProvisioningStatusForCapability(i62, param1Int2, i47, bool93);
            arrayList.writeNoException();
            return true;
          case 242:
            str27.enforceInterface("com.android.internal.telephony.ITelephony");
            i47 = str27.readInt();
            iImsConfigCallback = IImsConfigCallback.Stub.asInterface(str27.readStrongBinder());
            unregisterImsProvisioningChangedCallback(i47, iImsConfigCallback);
            arrayList.writeNoException();
            return true;
          case 241:
            iImsConfigCallback.enforceInterface("com.android.internal.telephony.ITelephony");
            i47 = iImsConfigCallback.readInt();
            iImsConfigCallback = IImsConfigCallback.Stub.asInterface(iImsConfigCallback.readStrongBinder());
            registerImsProvisioningChangedCallback(i47, iImsConfigCallback);
            arrayList.writeNoException();
            return true;
          case 240:
            iImsConfigCallback.enforceInterface("com.android.internal.telephony.ITelephony");
            i47 = iImsConfigCallback.readInt();
            list7 = getCertsFromCarrierPrivilegeAccessRules(i47);
            arrayList.writeNoException();
            arrayList.writeStringList(list7);
            return true;
          case 239:
            list7.enforceInterface("com.android.internal.telephony.ITelephony");
            str37 = list7.readString();
            bool93 = bool76;
            if (list7.readInt() != 0)
              bool93 = true; 
            bool52 = isEmergencyNumber(str37, bool93);
            arrayList.writeNoException();
            arrayList.writeInt(bool52);
            return true;
          case 238:
            list7.enforceInterface("com.android.internal.telephony.ITelephony");
            str37 = list7.readString();
            str26 = list7.readString();
            map = getEmergencyNumberList(str37, str26);
            arrayList.writeNoException();
            arrayList.writeMap(map);
            return true;
          case 237:
            map.enforceInterface("com.android.internal.telephony.ITelephony");
            i46 = map.readInt();
            bool51 = isTtyOverVolteEnabled(i46);
            arrayList.writeNoException();
            arrayList.writeInt(bool51);
            return true;
          case 236:
            map.enforceInterface("com.android.internal.telephony.ITelephony");
            i45 = map.readInt();
            bool93 = bool77;
            if (map.readInt() != 0)
              bool93 = true; 
            setRttCapabilitySetting(i45, bool93);
            arrayList.writeNoException();
            return true;
          case 235:
            map.enforceInterface("com.android.internal.telephony.ITelephony");
            i45 = map.readInt();
            param1Int2 = map.readInt();
            setVoWiFiRoamingModeSetting(i45, param1Int2);
            arrayList.writeNoException();
            return true;
          case 234:
            map.enforceInterface("com.android.internal.telephony.ITelephony");
            i45 = map.readInt();
            i45 = getVoWiFiRoamingModeSetting(i45);
            arrayList.writeNoException();
            arrayList.writeInt(i45);
            return true;
          case 233:
            map.enforceInterface("com.android.internal.telephony.ITelephony");
            i45 = map.readInt();
            param1Int2 = map.readInt();
            setVoWiFiModeSetting(i45, param1Int2);
            arrayList.writeNoException();
            return true;
          case 232:
            map.enforceInterface("com.android.internal.telephony.ITelephony");
            i45 = map.readInt();
            i45 = getVoWiFiModeSetting(i45);
            arrayList.writeNoException();
            arrayList.writeInt(i45);
            return true;
          case 231:
            map.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = map.readInt();
            bool93 = bool78;
            if (map.readInt() != 0)
              bool93 = true; 
            i45 = map.readInt();
            setVoWiFiNonPersistent(param1Int2, bool93, i45);
            arrayList.writeNoException();
            return true;
          case 230:
            map.enforceInterface("com.android.internal.telephony.ITelephony");
            i45 = map.readInt();
            bool93 = bool79;
            if (map.readInt() != 0)
              bool93 = true; 
            setVoWiFiRoamingSettingEnabled(i45, bool93);
            arrayList.writeNoException();
            return true;
          case 229:
            map.enforceInterface("com.android.internal.telephony.ITelephony");
            i45 = map.readInt();
            bool50 = isVoWiFiRoamingSettingEnabled(i45);
            arrayList.writeNoException();
            arrayList.writeInt(bool50);
            return true;
          case 228:
            map.enforceInterface("com.android.internal.telephony.ITelephony");
            i44 = map.readInt();
            bool93 = bool80;
            if (map.readInt() != 0)
              bool93 = true; 
            setVoWiFiSettingEnabled(i44, bool93);
            arrayList.writeNoException();
            return true;
          case 227:
            map.enforceInterface("com.android.internal.telephony.ITelephony");
            i44 = map.readInt();
            bool49 = isVoWiFiSettingEnabled(i44);
            arrayList.writeNoException();
            arrayList.writeInt(bool49);
            return true;
          case 226:
            map.enforceInterface("com.android.internal.telephony.ITelephony");
            i43 = map.readInt();
            bool93 = bool81;
            if (map.readInt() != 0)
              bool93 = true; 
            setVtSettingEnabled(i43, bool93);
            arrayList.writeNoException();
            return true;
          case 225:
            map.enforceInterface("com.android.internal.telephony.ITelephony");
            i43 = map.readInt();
            bool48 = isVtSettingEnabled(i43);
            arrayList.writeNoException();
            arrayList.writeInt(bool48);
            return true;
          case 224:
            map.enforceInterface("com.android.internal.telephony.ITelephony");
            i42 = map.readInt();
            bool93 = bool82;
            if (map.readInt() != 0)
              bool93 = true; 
            setAdvancedCallingSettingEnabled(i42, bool93);
            arrayList.writeNoException();
            return true;
          case 223:
            map.enforceInterface("com.android.internal.telephony.ITelephony");
            i42 = map.readInt();
            bool47 = isAdvancedCallingSettingEnabled(i42);
            arrayList.writeNoException();
            arrayList.writeInt(bool47);
            return true;
          case 222:
            map.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = map.readInt();
            iIntegerConsumer4 = IIntegerConsumer.Stub.asInterface(map.readStrongBinder());
            i41 = map.readInt();
            i62 = map.readInt();
            isMmTelCapabilitySupported(param1Int2, iIntegerConsumer4, i41, i62);
            arrayList.writeNoException();
            return true;
          case 221:
            map.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = map.readInt();
            i41 = map.readInt();
            i62 = map.readInt();
            bool46 = isAvailable(param1Int2, i41, i62);
            arrayList.writeNoException();
            arrayList.writeInt(bool46);
            return true;
          case 220:
            map.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = map.readInt();
            i62 = map.readInt();
            i40 = map.readInt();
            bool45 = isCapable(param1Int2, i62, i40);
            arrayList.writeNoException();
            arrayList.writeInt(bool45);
            return true;
          case 219:
            map.enforceInterface("com.android.internal.telephony.ITelephony");
            i39 = map.readInt();
            iImsCapabilityCallback = IImsCapabilityCallback.Stub.asInterface(map.readStrongBinder());
            unregisterMmTelCapabilityCallback(i39, iImsCapabilityCallback);
            arrayList.writeNoException();
            return true;
          case 218:
            iImsCapabilityCallback.enforceInterface("com.android.internal.telephony.ITelephony");
            i39 = iImsCapabilityCallback.readInt();
            iImsCapabilityCallback = IImsCapabilityCallback.Stub.asInterface(iImsCapabilityCallback.readStrongBinder());
            registerMmTelCapabilityCallback(i39, iImsCapabilityCallback);
            arrayList.writeNoException();
            return true;
          case 217:
            iImsCapabilityCallback.enforceInterface("com.android.internal.telephony.ITelephony");
            i39 = iImsCapabilityCallback.readInt();
            iIntegerConsumer2 = IIntegerConsumer.Stub.asInterface(iImsCapabilityCallback.readStrongBinder());
            getImsMmTelRegistrationTransportType(i39, iIntegerConsumer2);
            arrayList.writeNoException();
            return true;
          case 216:
            iIntegerConsumer2.enforceInterface("com.android.internal.telephony.ITelephony");
            i39 = iIntegerConsumer2.readInt();
            iIntegerConsumer2 = IIntegerConsumer.Stub.asInterface(iIntegerConsumer2.readStrongBinder());
            getImsMmTelRegistrationState(i39, iIntegerConsumer2);
            arrayList.writeNoException();
            return true;
          case 215:
            iIntegerConsumer2.enforceInterface("com.android.internal.telephony.ITelephony");
            i39 = iIntegerConsumer2.readInt();
            iImsRegistrationCallback = IImsRegistrationCallback.Stub.asInterface(iIntegerConsumer2.readStrongBinder());
            unregisterImsRegistrationCallback(i39, iImsRegistrationCallback);
            arrayList.writeNoException();
            return true;
          case 214:
            iImsRegistrationCallback.enforceInterface("com.android.internal.telephony.ITelephony");
            i39 = iImsRegistrationCallback.readInt();
            iImsRegistrationCallback = IImsRegistrationCallback.Stub.asInterface(iImsRegistrationCallback.readStrongBinder());
            registerImsRegistrationCallback(i39, iImsRegistrationCallback);
            arrayList.writeNoException();
            return true;
          case 213:
            iImsRegistrationCallback.enforceInterface("com.android.internal.telephony.ITelephony");
            i39 = iImsRegistrationCallback.readInt();
            str36 = iImsRegistrationCallback.readString();
            str25 = iImsRegistrationCallback.readString();
            i39 = getRadioPowerState(i39, str36, str25);
            arrayList.writeNoException();
            arrayList.writeInt(i39);
            return true;
          case 212:
            str25.enforceInterface("com.android.internal.telephony.ITelephony");
            bool44 = isInEmergencySmsMode();
            arrayList.writeNoException();
            arrayList.writeInt(bool44);
            return true;
          case 211:
            str25.enforceInterface("com.android.internal.telephony.ITelephony");
            i38 = str25.readInt();
            i38 = getNetworkSelectionMode(i38);
            arrayList.writeNoException();
            arrayList.writeInt(i38);
            return true;
          case 210:
            str25.enforceInterface("com.android.internal.telephony.ITelephony");
            i38 = str25.readInt();
            str36 = str25.readString();
            str25 = str25.readString();
            i38 = getNumberOfModemsWithSimultaneousDataConnections(i38, str36, str25);
            arrayList.writeNoException();
            arrayList.writeInt(i38);
            return true;
          case 209:
            str25.enforceInterface("com.android.internal.telephony.ITelephony");
            i38 = str25.readInt();
            refreshUiccProfile(i38);
            arrayList.writeNoException();
            return true;
          case 208:
            str25.enforceInterface("com.android.internal.telephony.ITelephony");
            i38 = str25.readInt();
            i38 = getCarrierIdListVersion(i38);
            arrayList.writeNoException();
            arrayList.writeInt(i38);
            return true;
          case 207:
            return onTransact$setCarrierTestOverride$((Parcel)str25, (Parcel)arrayList);
          case 206:
            str25.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = str25.readInt();
            i38 = str25.readInt();
            bool43 = setCdmaSubscriptionMode(param1Int2, i38);
            arrayList.writeNoException();
            arrayList.writeInt(bool43);
            return true;
          case 205:
            str25.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = str25.readInt();
            i37 = str25.readInt();
            bool42 = setCdmaRoamingMode(param1Int2, i37);
            arrayList.writeNoException();
            arrayList.writeInt(bool42);
            return true;
          case 204:
            str25.enforceInterface("com.android.internal.telephony.ITelephony");
            i36 = str25.readInt();
            i36 = getCdmaRoamingMode(i36);
            arrayList.writeNoException();
            arrayList.writeInt(i36);
            return true;
          case 203:
            str25.enforceInterface("com.android.internal.telephony.ITelephony");
            i36 = str25.readInt();
            bool93 = bool83;
            if (str25.readInt() != 0)
              bool93 = true; 
            setDataRoamingEnabled(i36, bool93);
            arrayList.writeNoException();
            return true;
          case 202:
            str25.enforceInterface("com.android.internal.telephony.ITelephony");
            i36 = str25.readInt();
            bool41 = isDataRoamingEnabled(i36);
            arrayList.writeNoException();
            arrayList.writeInt(bool41);
            return true;
          case 201:
            str25.enforceInterface("com.android.internal.telephony.ITelephony");
            arrayOfInt3 = str25.createIntArray();
            bool41 = switchSlots(arrayOfInt3);
            arrayList.writeNoException();
            arrayList.writeInt(bool41);
            return true;
          case 200:
            arrayOfInt3.enforceInterface("com.android.internal.telephony.ITelephony");
            arrayOfUiccSlotInfo = getUiccSlotsInfo();
            arrayList.writeNoException();
            arrayList.writeTypedArray((Parcelable[])arrayOfUiccSlotInfo, 1);
            return true;
          case 199:
            arrayOfUiccSlotInfo.enforceInterface("com.android.internal.telephony.ITelephony");
            str24 = arrayOfUiccSlotInfo.readString();
            list6 = getUiccCardsInfo(str24);
            arrayList.writeNoException();
            arrayList.writeTypedList(list6);
            return true;
          case 198:
            list6.enforceInterface("com.android.internal.telephony.ITelephony");
            i35 = list6.readInt();
            str23 = list6.readString();
            i35 = getCardIdForDefaultEuicc(i35, str23);
            arrayList.writeNoException();
            arrayList.writeInt(i35);
            return true;
          case 197:
            str23.enforceInterface("com.android.internal.telephony.ITelephony");
            i35 = str23.readInt();
            signalStrength = getSignalStrength(i35);
            arrayList.writeNoException();
            if (signalStrength != null) {
              arrayList.writeInt(1);
              signalStrength.writeToParcel((Parcel)arrayList, 1);
            } else {
              arrayList.writeInt(0);
            } 
            return true;
          case 196:
            signalStrength.enforceInterface("com.android.internal.telephony.ITelephony");
            i35 = signalStrength.readInt();
            bool40 = getEmergencyCallbackMode(i35);
            arrayList.writeNoException();
            arrayList.writeInt(bool40);
            return true;
          case 195:
            return onTransact$setForbiddenPlmns$((Parcel)signalStrength, (Parcel)arrayList);
          case 194:
            signalStrength.enforceInterface("com.android.internal.telephony.ITelephony");
            i34 = signalStrength.readInt();
            param1Int2 = signalStrength.readInt();
            str36 = signalStrength.readString();
            str22 = signalStrength.readString();
            arrayOfString4 = getForbiddenPlmns(i34, param1Int2, str36, str22);
            arrayList.writeNoException();
            arrayList.writeStringArray(arrayOfString4);
            return true;
          case 193:
            arrayOfString4.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = arrayOfString4.readInt();
            i34 = arrayOfString4.readInt();
            setSimPowerStateForSlot(param1Int2, i34);
            arrayList.writeNoException();
            return true;
          case 192:
            arrayOfString4.enforceInterface("com.android.internal.telephony.ITelephony");
            str41 = arrayOfString4.readString();
            str36 = arrayOfString4.readString();
            i34 = arrayOfString4.readInt();
            list5 = getClientRequestStats(str41, str36, i34);
            arrayList.writeNoException();
            arrayList.writeTypedList(list5);
            return true;
          case 191:
            list5.enforceInterface("com.android.internal.telephony.ITelephony");
            bool93 = bool84;
            if (list5.readInt() != 0)
              bool93 = true; 
            i34 = list5.readInt();
            setPolicyDataEnabled(bool93, i34);
            arrayList.writeNoException();
            return true;
          case 190:
            list5.enforceInterface("com.android.internal.telephony.ITelephony");
            i34 = list5.readInt();
            bool93 = bool85;
            if (list5.readInt() != 0)
              bool93 = true; 
            bool39 = setCallWaitingStatus(i34, bool93);
            arrayList.writeNoException();
            arrayList.writeInt(bool39);
            return true;
          case 189:
            list5.enforceInterface("com.android.internal.telephony.ITelephony");
            i33 = list5.readInt();
            i33 = getCallWaitingStatus(i33);
            arrayList.writeNoException();
            arrayList.writeInt(i33);
            return true;
          case 188:
            list5.enforceInterface("com.android.internal.telephony.ITelephony");
            i33 = list5.readInt();
            if (list5.readInt() != 0) {
              CallForwardingInfo callForwardingInfo1 = (CallForwardingInfo)CallForwardingInfo.CREATOR.createFromParcel((Parcel)list5);
            } else {
              list5 = null;
            } 
            bool38 = setCallForwarding(i33, (CallForwardingInfo)list5);
            arrayList.writeNoException();
            arrayList.writeInt(bool38);
            return true;
          case 187:
            list5.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = list5.readInt();
            i32 = list5.readInt();
            callForwardingInfo = getCallForwarding(param1Int2, i32);
            arrayList.writeNoException();
            if (callForwardingInfo != null) {
              arrayList.writeInt(1);
              callForwardingInfo.writeToParcel((Parcel)arrayList, 1);
            } else {
              arrayList.writeInt(0);
            } 
            return true;
          case 186:
            callForwardingInfo.enforceInterface("com.android.internal.telephony.ITelephony");
            i32 = callForwardingInfo.readInt();
            carrierActionResetAll(i32);
            arrayList.writeNoException();
            return true;
          case 185:
            callForwardingInfo.enforceInterface("com.android.internal.telephony.ITelephony");
            i32 = callForwardingInfo.readInt();
            bool93 = bool86;
            if (callForwardingInfo.readInt() != 0)
              bool93 = true; 
            carrierActionReportDefaultNetworkStatus(i32, bool93);
            arrayList.writeNoException();
            return true;
          case 184:
            callForwardingInfo.enforceInterface("com.android.internal.telephony.ITelephony");
            i32 = callForwardingInfo.readInt();
            bool93 = bool87;
            if (callForwardingInfo.readInt() != 0)
              bool93 = true; 
            carrierActionSetRadioEnabled(i32, bool93);
            arrayList.writeNoException();
            return true;
          case 183:
            callForwardingInfo.enforceInterface("com.android.internal.telephony.ITelephony");
            i32 = callForwardingInfo.readInt();
            bool93 = bool88;
            if (callForwardingInfo.readInt() != 0)
              bool93 = true; 
            carrierActionSetMeteredApnsEnabled(i32, bool93);
            arrayList.writeNoException();
            return true;
          case 182:
            callForwardingInfo.enforceInterface("com.android.internal.telephony.ITelephony");
            i32 = callForwardingInfo.readInt();
            str36 = callForwardingInfo.readString();
            bool93 = bool89;
            if (callForwardingInfo.readInt() != 0)
              bool93 = true; 
            i32 = getCarrierIdFromMccMnc(i32, str36, bool93);
            arrayList.writeNoException();
            arrayList.writeInt(i32);
            return true;
          case 181:
            callForwardingInfo.enforceInterface("com.android.internal.telephony.ITelephony");
            i32 = callForwardingInfo.readInt();
            str21 = getSubscriptionSpecificCarrierName(i32);
            arrayList.writeNoException();
            arrayList.writeString(str21);
            return true;
          case 180:
            str21.enforceInterface("com.android.internal.telephony.ITelephony");
            i32 = str21.readInt();
            i32 = getSubscriptionSpecificCarrierId(i32);
            arrayList.writeNoException();
            arrayList.writeInt(i32);
            return true;
          case 179:
            str21.enforceInterface("com.android.internal.telephony.ITelephony");
            i32 = str21.readInt();
            str21 = getSubscriptionCarrierName(i32);
            arrayList.writeNoException();
            arrayList.writeString(str21);
            return true;
          case 178:
            str21.enforceInterface("com.android.internal.telephony.ITelephony");
            i32 = str21.readInt();
            i32 = getSubscriptionCarrierId(i32);
            arrayList.writeNoException();
            arrayList.writeInt(i32);
            return true;
          case 177:
            str21.enforceInterface("com.android.internal.telephony.ITelephony");
            carrierRestrictionRules = getAllowedCarriers();
            arrayList.writeNoException();
            if (carrierRestrictionRules != null) {
              arrayList.writeInt(1);
              carrierRestrictionRules.writeToParcel((Parcel)arrayList, 1);
            } else {
              arrayList.writeInt(0);
            } 
            return true;
          case 176:
            carrierRestrictionRules.enforceInterface("com.android.internal.telephony.ITelephony");
            if (carrierRestrictionRules.readInt() != 0) {
              carrierRestrictionRules = (CarrierRestrictionRules)CarrierRestrictionRules.CREATOR.createFromParcel((Parcel)carrierRestrictionRules);
            } else {
              carrierRestrictionRules = null;
            } 
            i32 = setAllowedCarriers(carrierRestrictionRules);
            arrayList.writeNoException();
            arrayList.writeInt(i32);
            return true;
          case 175:
            carrierRestrictionRules.enforceInterface("com.android.internal.telephony.ITelephony");
            list4 = getTelephonyHistograms();
            arrayList.writeNoException();
            arrayList.writeTypedList(list4);
            return true;
          case 174:
            list4.enforceInterface("com.android.internal.telephony.ITelephony");
            i32 = list4.readInt();
            str20 = getCdmaPrlVersion(i32);
            arrayList.writeNoException();
            arrayList.writeString(str20);
            return true;
          case 173:
            str20.enforceInterface("com.android.internal.telephony.ITelephony");
            i32 = str20.readInt();
            str20 = getEsn(i32);
            arrayList.writeNoException();
            arrayList.writeString(str20);
            return true;
          case 172:
            str20.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = str20.readInt();
            i32 = str20.readInt();
            str20 = getAidForAppType(param1Int2, i32);
            arrayList.writeNoException();
            arrayList.writeString(str20);
            return true;
          case 171:
            str20.enforceInterface("com.android.internal.telephony.ITelephony");
            list3 = getPackagesWithCarrierPrivilegesForAllPhones();
            arrayList.writeNoException();
            arrayList.writeStringList(list3);
            return true;
          case 170:
            list3.enforceInterface("com.android.internal.telephony.ITelephony");
            i32 = list3.readInt();
            list3 = getPackagesWithCarrierPrivileges(i32);
            arrayList.writeNoException();
            arrayList.writeStringList(list3);
            return true;
          case 169:
            list3.enforceInterface("com.android.internal.telephony.ITelephony");
            str41 = list3.readString();
            if (list3.readInt() != 0) {
              PhoneAccountHandle phoneAccountHandle1 = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel((Parcel)list3);
            } else {
              str36 = null;
            } 
            bool93 = bool90;
            if (list3.readInt() != 0)
              bool93 = true; 
            setVoicemailVibrationEnabled(str41, (PhoneAccountHandle)str36, bool93);
            arrayList.writeNoException();
            return true;
          case 168:
            list3.enforceInterface("com.android.internal.telephony.ITelephony");
            if (list3.readInt() != 0) {
              PhoneAccountHandle phoneAccountHandle1 = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel((Parcel)list3);
            } else {
              list3 = null;
            } 
            bool37 = isVoicemailVibrationEnabled((PhoneAccountHandle)list3);
            arrayList.writeNoException();
            arrayList.writeInt(bool37);
            return true;
          case 167:
            list3.enforceInterface("com.android.internal.telephony.ITelephony");
            str41 = list3.readString();
            if (list3.readInt() != 0) {
              PhoneAccountHandle phoneAccountHandle1 = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel((Parcel)list3);
            } else {
              str36 = null;
            } 
            if (list3.readInt() != 0) {
              Uri uri1 = (Uri)Uri.CREATOR.createFromParcel((Parcel)list3);
            } else {
              list3 = null;
            } 
            setVoicemailRingtoneUri(str41, (PhoneAccountHandle)str36, (Uri)list3);
            arrayList.writeNoException();
            return true;
          case 166:
            list3.enforceInterface("com.android.internal.telephony.ITelephony");
            if (list3.readInt() != 0) {
              PhoneAccountHandle phoneAccountHandle1 = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel((Parcel)list3);
            } else {
              list3 = null;
            } 
            uri = getVoicemailRingtoneUri((PhoneAccountHandle)list3);
            arrayList.writeNoException();
            if (uri != null) {
              arrayList.writeInt(1);
              uri.writeToParcel((Parcel)arrayList, 1);
            } else {
              arrayList.writeInt(0);
            } 
            return true;
          case 165:
            uri.enforceInterface("com.android.internal.telephony.ITelephony");
            i31 = uri.readInt();
            str36 = uri.readString();
            str19 = uri.readString();
            serviceState = getServiceStateForSubscriber(i31, str36, str19);
            arrayList.writeNoException();
            if (serviceState != null) {
              arrayList.writeInt(1);
              serviceState.writeToParcel((Parcel)arrayList, 1);
            } else {
              arrayList.writeInt(0);
            } 
            return true;
          case 164:
            serviceState.enforceInterface("com.android.internal.telephony.ITelephony");
            if (serviceState.readInt() != 0) {
              ResultReceiver resultReceiver = (ResultReceiver)ResultReceiver.CREATOR.createFromParcel((Parcel)serviceState);
            } else {
              serviceState = null;
            } 
            requestModemActivityInfo((ResultReceiver)serviceState);
            return true;
          case 163:
            serviceState.enforceInterface("com.android.internal.telephony.ITelephony");
            i31 = serviceState.readInt();
            str18 = getSimLocaleForSubscriber(i31);
            arrayList.writeNoException();
            arrayList.writeString(str18);
            return true;
          case 162:
            str18.enforceInterface("com.android.internal.telephony.ITelephony");
            i31 = str18.readInt();
            factoryReset(i31);
            arrayList.writeNoException();
            return true;
          case 161:
            str18.enforceInterface("com.android.internal.telephony.ITelephony");
            i31 = str18.readInt();
            phoneAccountHandle = getPhoneAccountHandleForSubscriptionId(i31);
            arrayList.writeNoException();
            if (phoneAccountHandle != null) {
              arrayList.writeInt(1);
              phoneAccountHandle.writeToParcel((Parcel)arrayList, 1);
            } else {
              arrayList.writeInt(0);
            } 
            return true;
          case 160:
            phoneAccountHandle.enforceInterface("com.android.internal.telephony.ITelephony");
            if (phoneAccountHandle.readInt() != 0) {
              PhoneAccountHandle phoneAccountHandle1 = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel((Parcel)phoneAccountHandle);
            } else {
              str36 = null;
            } 
            str41 = phoneAccountHandle.readString();
            str17 = phoneAccountHandle.readString();
            i31 = getSubIdForPhoneAccountHandle((PhoneAccountHandle)str36, str41, str17);
            arrayList.writeNoException();
            arrayList.writeInt(i31);
            return true;
          case 159:
            str17.enforceInterface("com.android.internal.telephony.ITelephony");
            if (str17.readInt() != 0) {
              PhoneAccount phoneAccount = (PhoneAccount)PhoneAccount.CREATOR.createFromParcel((Parcel)str17);
            } else {
              str17 = null;
            } 
            i31 = getSubIdForPhoneAccount((PhoneAccount)str17);
            arrayList.writeNoException();
            arrayList.writeInt(i31);
            return true;
          case 158:
            str17.enforceInterface("com.android.internal.telephony.ITelephony");
            i31 = str17.readInt();
            str36 = str17.readString();
            str17 = str17.readString();
            str17 = getDeviceSoftwareVersionForSlot(i31, str36, str17);
            arrayList.writeNoException();
            arrayList.writeString(str17);
            return true;
          case 157:
            str17.enforceInterface("com.android.internal.telephony.ITelephony");
            i31 = str17.readInt();
            str17 = getManufacturerCodeForSlot(i31);
            arrayList.writeNoException();
            arrayList.writeString(str17);
            return true;
          case 156:
            str17.enforceInterface("com.android.internal.telephony.ITelephony");
            i31 = str17.readInt();
            str36 = str17.readString();
            str17 = str17.readString();
            str17 = getMeidForSlot(i31, str36, str17);
            arrayList.writeNoException();
            arrayList.writeString(str17);
            return true;
          case 155:
            str17.enforceInterface("com.android.internal.telephony.ITelephony");
            i31 = str17.readInt();
            str17 = getTypeAllocationCodeForSlot(i31);
            arrayList.writeNoException();
            arrayList.writeString(str17);
            return true;
          case 154:
            str17.enforceInterface("com.android.internal.telephony.ITelephony");
            i31 = str17.readInt();
            str36 = str17.readString();
            str17 = str17.readString();
            str17 = getImeiForSlot(i31, str36, str17);
            arrayList.writeNoException();
            arrayList.writeString(str17);
            return true;
          case 153:
            str17.enforceInterface("com.android.internal.telephony.ITelephony");
            str36 = str17.readString();
            str17 = str17.readString();
            str17 = getDeviceIdWithFeature(str36, str17);
            arrayList.writeNoException();
            arrayList.writeString(str17);
            return true;
          case 152:
            str17.enforceInterface("com.android.internal.telephony.ITelephony");
            str17 = str17.readString();
            str17 = getDeviceId(str17);
            arrayList.writeNoException();
            arrayList.writeString(str17);
            return true;
          case 151:
            str17.enforceInterface("com.android.internal.telephony.ITelephony");
            i31 = str17.readInt();
            i31 = getImsRegTechnologyForMmTel(i31);
            arrayList.writeNoException();
            arrayList.writeInt(i31);
            return true;
          case 150:
            str17.enforceInterface("com.android.internal.telephony.ITelephony");
            i31 = str17.readInt();
            bool36 = isVideoTelephonyAvailable(i31);
            arrayList.writeNoException();
            arrayList.writeInt(bool36);
            return true;
          case 149:
            str17.enforceInterface("com.android.internal.telephony.ITelephony");
            i30 = str17.readInt();
            bool35 = isWifiCallingAvailable(i30);
            arrayList.writeNoException();
            arrayList.writeInt(bool35);
            return true;
          case 148:
            str17.enforceInterface("com.android.internal.telephony.ITelephony");
            i29 = str17.readInt();
            bool34 = isImsRegistered(i29);
            arrayList.writeNoException();
            arrayList.writeInt(bool34);
            return true;
          case 147:
            str17.enforceInterface("com.android.internal.telephony.ITelephony");
            bool34 = isHearingAidCompatibilitySupported();
            arrayList.writeNoException();
            arrayList.writeInt(bool34);
            return true;
          case 146:
            str17.enforceInterface("com.android.internal.telephony.ITelephony");
            i28 = str17.readInt();
            bool33 = isRttSupported(i28);
            arrayList.writeNoException();
            arrayList.writeInt(bool33);
            return true;
          case 145:
            str17.enforceInterface("com.android.internal.telephony.ITelephony");
            bool33 = isTtyModeSupported();
            arrayList.writeNoException();
            arrayList.writeInt(bool33);
            return true;
          case 144:
            str17.enforceInterface("com.android.internal.telephony.ITelephony");
            i27 = str17.readInt();
            str36 = str17.readString();
            str17 = str17.readString();
            bool32 = isWorldPhone(i27, str36, str17);
            arrayList.writeNoException();
            arrayList.writeInt(bool32);
            return true;
          case 143:
            str17.enforceInterface("com.android.internal.telephony.ITelephony");
            i26 = str17.readInt();
            str36 = str17.readString();
            str17 = str17.readString();
            bool31 = canChangeDtmfToneLength(i26, str36, str17);
            arrayList.writeNoException();
            arrayList.writeInt(bool31);
            return true;
          case 142:
            str17.enforceInterface("com.android.internal.telephony.ITelephony");
            str36 = str17.readString();
            str17 = str17.readString();
            bool31 = isVideoCallingEnabled(str36, str17);
            arrayList.writeNoException();
            arrayList.writeInt(bool31);
            return true;
          case 141:
            str17.enforceInterface("com.android.internal.telephony.ITelephony");
            bool93 = bool91;
            if (str17.readInt() != 0)
              bool93 = true; 
            enableVideoCalling(bool93);
            arrayList.writeNoException();
            return true;
          case 140:
            str17.enforceInterface("com.android.internal.telephony.ITelephony");
            i25 = str17.readInt();
            str17 = str17.readString();
            i25 = getRadioAccessFamily(i25, str17);
            arrayList.writeNoException();
            arrayList.writeInt(i25);
            return true;
          case 139:
            str17.enforceInterface("com.android.internal.telephony.ITelephony");
            arrayOfRadioAccessFamily = (RadioAccessFamily[])str17.createTypedArray(RadioAccessFamily.CREATOR);
            setRadioCapability(arrayOfRadioAccessFamily);
            arrayList.writeNoException();
            return true;
          case 138:
            arrayOfRadioAccessFamily.enforceInterface("com.android.internal.telephony.ITelephony");
            shutdownMobileRadios();
            arrayList.writeNoException();
            return true;
          case 137:
            arrayOfRadioAccessFamily.enforceInterface("com.android.internal.telephony.ITelephony");
            bool30 = needMobileRadioShutdown();
            arrayList.writeNoException();
            arrayList.writeInt(bool30);
            return true;
          case 136:
            arrayOfRadioAccessFamily.enforceInterface("com.android.internal.telephony.ITelephony");
            arrayOfByte3 = arrayOfRadioAccessFamily.createByteArray();
            i24 = arrayOfRadioAccessFamily.readInt();
            if (i24 < 0) {
              arrayOfRadioAccessFamily = null;
            } else {
              arrayOfByte2 = new byte[i24];
            } 
            i24 = invokeOemRilRequestRaw(arrayOfByte3, arrayOfByte2);
            arrayList.writeNoException();
            arrayList.writeInt(i24);
            arrayList.writeByteArray(arrayOfByte2);
            return true;
          case 135:
            return onTransact$setRoamingOverride$((Parcel)arrayOfByte2, (Parcel)arrayList);
          case 134:
            arrayOfByte2.enforceInterface("com.android.internal.telephony.ITelephony");
            i24 = arrayOfByte2.readInt();
            str16 = arrayOfByte2.readString();
            bool29 = setOperatorBrandOverride(i24, str16);
            arrayList.writeNoException();
            arrayList.writeInt(bool29);
            return true;
          case 133:
            str16.enforceInterface("com.android.internal.telephony.ITelephony");
            i23 = str16.readInt();
            str16 = str16.readString();
            arrayOfString3 = getMergedImsisFromGroup(i23, str16);
            arrayList.writeNoException();
            arrayList.writeStringArray(arrayOfString3);
            return true;
          case 132:
            arrayOfString3.enforceInterface("com.android.internal.telephony.ITelephony");
            i23 = arrayOfString3.readInt();
            str35 = arrayOfString3.readString();
            str15 = arrayOfString3.readString();
            arrayOfString2 = getMergedSubscriberIds(i23, str35, str15);
            arrayList.writeNoException();
            arrayList.writeStringArray(arrayOfString2);
            return true;
          case 131:
            arrayOfString2.enforceInterface("com.android.internal.telephony.ITelephony");
            i23 = arrayOfString2.readInt();
            str35 = arrayOfString2.readString();
            str14 = arrayOfString2.readString();
            str14 = getLine1AlphaTagForDisplay(i23, str35, str14);
            arrayList.writeNoException();
            arrayList.writeString(str14);
            return true;
          case 130:
            str14.enforceInterface("com.android.internal.telephony.ITelephony");
            i23 = str14.readInt();
            str35 = str14.readString();
            str14 = str14.readString();
            str14 = getLine1NumberForDisplay(i23, str35, str14);
            arrayList.writeNoException();
            arrayList.writeString(str14);
            return true;
          case 129:
            str14.enforceInterface("com.android.internal.telephony.ITelephony");
            i23 = str14.readInt();
            str35 = str14.readString();
            str14 = str14.readString();
            bool28 = setLine1NumberForDisplayForSubscriber(i23, str35, str14);
            arrayList.writeNoException();
            arrayList.writeInt(bool28);
            return true;
          case 128:
            str14.enforceInterface("com.android.internal.telephony.ITelephony");
            if (str14.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)str14);
            } else {
              str35 = null;
            } 
            i22 = str14.readInt();
            list2 = getCarrierPackageNamesForIntentAndPhone((Intent)str35, i22);
            arrayList.writeNoException();
            arrayList.writeStringList(list2);
            return true;
          case 127:
            list2.enforceInterface("com.android.internal.telephony.ITelephony");
            str13 = list2.readString();
            i22 = checkCarrierPrivilegesForPackageAnyPhone(str13);
            arrayList.writeNoException();
            arrayList.writeInt(i22);
            return true;
          case 126:
            str13.enforceInterface("com.android.internal.telephony.ITelephony");
            i22 = str13.readInt();
            str13 = str13.readString();
            i22 = checkCarrierPrivilegesForPackage(i22, str13);
            arrayList.writeNoException();
            arrayList.writeInt(i22);
            return true;
          case 125:
            str13.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = str13.readInt();
            i22 = str13.readInt();
            i22 = getCarrierPrivilegeStatusForUid(param1Int2, i22);
            arrayList.writeNoException();
            arrayList.writeInt(i22);
            return true;
          case 124:
            str13.enforceInterface("com.android.internal.telephony.ITelephony");
            i22 = str13.readInt();
            i22 = getCarrierPrivilegeStatus(i22);
            arrayList.writeNoException();
            arrayList.writeInt(i22);
            return true;
          case 123:
            str13.enforceInterface("com.android.internal.telephony.ITelephony");
            if (str13.readInt() != 0) {
              PhoneNumberRange phoneNumberRange = (PhoneNumberRange)PhoneNumberRange.CREATOR.createFromParcel((Parcel)str13);
            } else {
              str35 = null;
            } 
            l = str13.readLong();
            iNumberVerificationCallback = INumberVerificationCallback.Stub.asInterface(str13.readStrongBinder());
            str13 = str13.readString();
            requestNumberVerification((PhoneNumberRange)str35, l, iNumberVerificationCallback, str13);
            arrayList.writeNoException();
            return true;
          case 122:
            str13.enforceInterface("com.android.internal.telephony.ITelephony");
            i22 = str13.readInt();
            str13 = getCdmaMin(i22);
            arrayList.writeNoException();
            arrayList.writeString(str13);
            return true;
          case 121:
            str13.enforceInterface("com.android.internal.telephony.ITelephony");
            i22 = str13.readInt();
            str13 = getCdmaMdn(i22);
            arrayList.writeNoException();
            arrayList.writeString(str13);
            return true;
          case 120:
            str13.enforceInterface("com.android.internal.telephony.ITelephony");
            bool93 = bool92;
            if (str13.readInt() != 0)
              bool93 = true; 
            setImsRegistrationState(bool93);
            arrayList.writeNoException();
            return true;
          case 119:
            str13.enforceInterface("com.android.internal.telephony.ITelephony");
            str35 = str13.readString();
            str40 = str13.readString();
            str13 = str13.readString();
            arrayOfString1 = getPcscfAddress(str35, str40, str13);
            arrayList.writeNoException();
            arrayList.writeStringArray(arrayOfString1);
            return true;
          case 118:
            arrayOfString1.enforceInterface("com.android.internal.telephony.ITelephony");
            i22 = arrayOfString1.readInt();
            if (arrayOfString1.readInt() != 0)
              bool93 = true; 
            setAlwaysReportSignalStrength(i22, bool93);
            arrayList.writeNoException();
            return true;
          case 117:
            arrayOfString1.enforceInterface("com.android.internal.telephony.ITelephony");
            i22 = arrayOfString1.readInt();
            bool27 = isManualNetworkSelectionAllowed(i22);
            arrayList.writeNoException();
            arrayList.writeInt(bool27);
            return true;
          case 116:
            arrayOfString1.enforceInterface("com.android.internal.telephony.ITelephony");
            i21 = arrayOfString1.readInt();
            bool26 = isDataEnabled(i21);
            arrayList.writeNoException();
            arrayList.writeInt(bool26);
            return true;
          case 115:
            arrayOfString1.enforceInterface("com.android.internal.telephony.ITelephony");
            i20 = arrayOfString1.readInt();
            bool25 = isUserDataEnabled(i20);
            arrayList.writeNoException();
            arrayList.writeInt(bool25);
            return true;
          case 114:
            arrayOfString1.enforceInterface("com.android.internal.telephony.ITelephony");
            i19 = arrayOfString1.readInt();
            bool24 = getDataEnabled(i19);
            arrayList.writeNoException();
            arrayList.writeInt(bool24);
            return true;
          case 113:
            arrayOfString1.enforceInterface("com.android.internal.telephony.ITelephony");
            i18 = arrayOfString1.readInt();
            bool93 = bool94;
            if (arrayOfString1.readInt() != 0)
              bool93 = true; 
            setUserDataEnabled(i18, bool93);
            arrayList.writeNoException();
            return true;
          case 112:
            arrayOfString1.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = arrayOfString1.readInt();
            i18 = arrayOfString1.readInt();
            bool23 = setPreferredNetworkType(param1Int2, i18);
            arrayList.writeNoException();
            arrayList.writeInt(bool23);
            return true;
          case 111:
            arrayOfString1.enforceInterface("com.android.internal.telephony.ITelephony");
            i17 = arrayOfString1.readInt();
            param1Int2 = arrayOfString1.readInt();
            l = arrayOfString1.readLong();
            bool22 = setAllowedNetworkTypesForReason(i17, param1Int2, l);
            arrayList.writeNoException();
            arrayList.writeInt(bool22);
            return true;
          case 110:
            arrayOfString1.enforceInterface("com.android.internal.telephony.ITelephony");
            i16 = arrayOfString1.readInt();
            l = getEffectiveAllowedNetworkTypes(i16);
            arrayList.writeNoException();
            arrayList.writeLong(l);
            return true;
          case 109:
            arrayOfString1.enforceInterface("com.android.internal.telephony.ITelephony");
            i16 = arrayOfString1.readInt();
            param1Int2 = arrayOfString1.readInt();
            l = getAllowedNetworkTypesForReason(i16, param1Int2);
            arrayList.writeNoException();
            arrayList.writeLong(l);
            return true;
          case 108:
            arrayOfString1.enforceInterface("com.android.internal.telephony.ITelephony");
            i16 = arrayOfString1.readInt();
            l = arrayOfString1.readLong();
            bool21 = setAllowedNetworkTypes(i16, l);
            arrayList.writeNoException();
            arrayList.writeInt(bool21);
            return true;
          case 107:
            arrayOfString1.enforceInterface("com.android.internal.telephony.ITelephony");
            i15 = arrayOfString1.readInt();
            l = getAllowedNetworkTypes(i15);
            arrayList.writeNoException();
            arrayList.writeLong(l);
            return true;
          case 106:
            arrayOfString1.enforceInterface("com.android.internal.telephony.ITelephony");
            i15 = arrayOfString1.readInt();
            if (arrayOfString1.readInt() != 0) {
              OperatorInfo operatorInfo = (OperatorInfo)OperatorInfo.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              str35 = null;
            } 
            bool93 = bool95;
            if (arrayOfString1.readInt() != 0)
              bool93 = true; 
            bool20 = setNetworkSelectionModeManual(i15, (OperatorInfo)str35, bool93);
            arrayList.writeNoException();
            arrayList.writeInt(bool20);
            return true;
          case 105:
            arrayOfString1.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = arrayOfString1.readInt();
            i14 = arrayOfString1.readInt();
            stopNetworkScan(param1Int2, i14);
            arrayList.writeNoException();
            return true;
          case 104:
            return onTransact$requestNetworkScan$((Parcel)arrayOfString1, (Parcel)arrayList);
          case 103:
            arrayOfString1.enforceInterface("com.android.internal.telephony.ITelephony");
            i14 = arrayOfString1.readInt();
            str35 = arrayOfString1.readString();
            str12 = arrayOfString1.readString();
            cellNetworkScanResult = getCellNetworkScanResults(i14, str35, str12);
            arrayList.writeNoException();
            if (cellNetworkScanResult != null) {
              arrayList.writeInt(1);
              cellNetworkScanResult.writeToParcel((Parcel)arrayList, 1);
            } else {
              arrayList.writeInt(0);
            } 
            return true;
          case 102:
            cellNetworkScanResult.enforceInterface("com.android.internal.telephony.ITelephony");
            i14 = cellNetworkScanResult.readInt();
            setNetworkSelectionModeAutomatic(i14);
            arrayList.writeNoException();
            return true;
          case 101:
            cellNetworkScanResult.enforceInterface("com.android.internal.telephony.ITelephony");
            i14 = cellNetworkScanResult.readInt();
            iIntegerConsumer1 = IIntegerConsumer.Stub.asInterface(cellNetworkScanResult.readStrongBinder());
            getImsMmTelFeatureState(i14, iIntegerConsumer1);
            arrayList.writeNoException();
            return true;
          case 100:
            iIntegerConsumer1.enforceInterface("com.android.internal.telephony.ITelephony");
            i14 = iIntegerConsumer1.readInt();
            bool93 = bool96;
            if (iIntegerConsumer1.readInt() != 0)
              bool93 = true; 
            param1Int2 = iIntegerConsumer1.readInt();
            str11 = getBoundImsServicePackage(i14, bool93, param1Int2);
            arrayList.writeNoException();
            arrayList.writeString(str11);
            return true;
          case 99:
            str11.enforceInterface("com.android.internal.telephony.ITelephony");
            i14 = str11.readInt();
            bool93 = bool97;
            if (str11.readInt() != 0)
              bool93 = true; 
            arrayOfInt5 = str11.createIntArray();
            str11 = str11.readString();
            bool19 = setBoundImsServiceOverride(i14, bool93, arrayOfInt5, str11);
            arrayList.writeNoException();
            arrayList.writeInt(bool19);
            return true;
          case 98:
            str11.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = str11.readInt();
            i13 = str11.readInt();
            iImsConfig = getImsConfig(param1Int2, i13);
            arrayList.writeNoException();
            str11 = str42;
            if (iImsConfig != null)
              iBinder3 = iImsConfig.asBinder(); 
            arrayList.writeStrongBinder(iBinder3);
            return true;
          case 97:
            iBinder3.enforceInterface("com.android.internal.telephony.ITelephony");
            i13 = iBinder3.readInt();
            param1Int2 = iBinder3.readInt();
            iImsRegistration2 = getImsRegistration(i13, param1Int2);
            arrayList.writeNoException();
            iBinder3 = iBinder4;
            if (iImsRegistration2 != null)
              iBinder3 = iImsRegistration2.asBinder(); 
            arrayList.writeStrongBinder(iBinder3);
            return true;
          case 96:
            iBinder3.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = iBinder3.readInt();
            i13 = iBinder3.readInt();
            iImsServiceFeatureCallback2 = IImsServiceFeatureCallback.Stub.asInterface(iBinder3.readStrongBinder());
            unregisterImsFeatureCallback(param1Int2, i13, iImsServiceFeatureCallback2);
            arrayList.writeNoException();
            return true;
          case 95:
            iImsServiceFeatureCallback2.enforceInterface("com.android.internal.telephony.ITelephony");
            i13 = iImsServiceFeatureCallback2.readInt();
            iImsServiceFeatureCallback2 = IImsServiceFeatureCallback.Stub.asInterface(iImsServiceFeatureCallback2.readStrongBinder());
            iImsRcsFeature2 = getRcsFeatureAndListen(i13, iImsServiceFeatureCallback2);
            arrayList.writeNoException();
            iImsRegistration1 = iImsRegistration2;
            if (iImsRcsFeature2 != null)
              iBinder2 = iImsRcsFeature2.asBinder(); 
            arrayList.writeStrongBinder(iBinder2);
            return true;
          case 94:
            iBinder2.enforceInterface("com.android.internal.telephony.ITelephony");
            i13 = iBinder2.readInt();
            iImsServiceFeatureCallback1 = IImsServiceFeatureCallback.Stub.asInterface(iBinder2.readStrongBinder());
            iImsMmTelFeature = getMmTelFeatureAndListen(i13, iImsServiceFeatureCallback1);
            arrayList.writeNoException();
            iImsRcsFeature1 = iImsRcsFeature2;
            if (iImsMmTelFeature != null)
              iBinder1 = iImsMmTelFeature.asBinder(); 
            arrayList.writeStrongBinder(iBinder1);
            return true;
          case 93:
            iBinder1.enforceInterface("com.android.internal.telephony.ITelephony");
            i13 = iBinder1.readInt();
            resetIms(i13);
            arrayList.writeNoException();
            return true;
          case 92:
            iBinder1.enforceInterface("com.android.internal.telephony.ITelephony");
            i13 = iBinder1.readInt();
            disableIms(i13);
            arrayList.writeNoException();
            return true;
          case 91:
            iBinder1.enforceInterface("com.android.internal.telephony.ITelephony");
            i13 = iBinder1.readInt();
            enableIms(i13);
            arrayList.writeNoException();
            return true;
          case 90:
            iBinder1.enforceInterface("com.android.internal.telephony.ITelephony");
            i13 = iBinder1.readInt();
            bool18 = isTetheringApnRequiredForSubscriber(i13);
            arrayList.writeNoException();
            arrayList.writeInt(bool18);
            return true;
          case 89:
            iBinder1.enforceInterface("com.android.internal.telephony.ITelephony");
            i12 = iBinder1.readInt();
            i12 = getPreferredNetworkType(i12);
            arrayList.writeNoException();
            arrayList.writeInt(i12);
            return true;
          case 88:
            iBinder1.enforceInterface("com.android.internal.telephony.ITelephony");
            str34 = iBinder1.readString();
            str10 = iBinder1.readString();
            i12 = getCalculatedPreferredNetworkType(str34, str10);
            arrayList.writeNoException();
            arrayList.writeInt(i12);
            return true;
          case 87:
            str10.enforceInterface("com.android.internal.telephony.ITelephony");
            i12 = str10.readInt();
            bool17 = rebootModem(i12);
            arrayList.writeNoException();
            arrayList.writeInt(bool17);
            return true;
          case 86:
            str10.enforceInterface("com.android.internal.telephony.ITelephony");
            i11 = str10.readInt();
            bool16 = resetModemConfig(i11);
            arrayList.writeNoException();
            arrayList.writeInt(bool16);
            return true;
          case 85:
            str10.enforceInterface("com.android.internal.telephony.ITelephony");
            arrayOfByte1 = str10.createByteArray();
            bool16 = nvWriteCdmaPrl(arrayOfByte1);
            arrayList.writeNoException();
            arrayList.writeInt(bool16);
            return true;
          case 84:
            arrayOfByte1.enforceInterface("com.android.internal.telephony.ITelephony");
            i10 = arrayOfByte1.readInt();
            str9 = arrayOfByte1.readString();
            bool15 = nvWriteItem(i10, str9);
            arrayList.writeNoException();
            arrayList.writeInt(bool15);
            return true;
          case 83:
            str9.enforceInterface("com.android.internal.telephony.ITelephony");
            i9 = str9.readInt();
            str9 = nvReadItem(i9);
            arrayList.writeNoException();
            arrayList.writeString(str9);
            return true;
          case 82:
            str9.enforceInterface("com.android.internal.telephony.ITelephony");
            i9 = str9.readInt();
            str9 = str9.readString();
            str9 = sendEnvelopeWithStatus(i9, str9);
            arrayList.writeNoException();
            arrayList.writeString(str9);
            return true;
          case 81:
            return onTransact$iccExchangeSimIO$((Parcel)str9, (Parcel)arrayList);
          case 80:
            return onTransact$iccTransmitApduBasicChannel$((Parcel)str9, (Parcel)arrayList);
          case 79:
            return onTransact$iccTransmitApduBasicChannelBySlot$((Parcel)str9, (Parcel)arrayList);
          case 78:
            return onTransact$iccTransmitApduLogicalChannel$((Parcel)str9, (Parcel)arrayList);
          case 77:
            return onTransact$iccTransmitApduLogicalChannelBySlot$((Parcel)str9, (Parcel)arrayList);
          case 76:
            str9.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = str9.readInt();
            i9 = str9.readInt();
            bool14 = iccCloseLogicalChannel(param1Int2, i9);
            arrayList.writeNoException();
            arrayList.writeInt(bool14);
            return true;
          case 75:
            str9.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = str9.readInt();
            i8 = str9.readInt();
            bool13 = iccCloseLogicalChannelBySlot(param1Int2, i8);
            arrayList.writeNoException();
            arrayList.writeInt(bool13);
            return true;
          case 74:
            str9.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = str9.readInt();
            str39 = str9.readString();
            str34 = str9.readString();
            i7 = str9.readInt();
            iccOpenLogicalChannelResponse = iccOpenLogicalChannel(param1Int2, str39, str34, i7);
            arrayList.writeNoException();
            if (iccOpenLogicalChannelResponse != null) {
              arrayList.writeInt(1);
              iccOpenLogicalChannelResponse.writeToParcel((Parcel)arrayList, 1);
            } else {
              arrayList.writeInt(0);
            } 
            return true;
          case 73:
            iccOpenLogicalChannelResponse.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = iccOpenLogicalChannelResponse.readInt();
            str39 = iccOpenLogicalChannelResponse.readString();
            str34 = iccOpenLogicalChannelResponse.readString();
            i7 = iccOpenLogicalChannelResponse.readInt();
            iccOpenLogicalChannelResponse = iccOpenLogicalChannelBySlot(param1Int2, str39, str34, i7);
            arrayList.writeNoException();
            if (iccOpenLogicalChannelResponse != null) {
              arrayList.writeInt(1);
              iccOpenLogicalChannelResponse.writeToParcel((Parcel)arrayList, 1);
            } else {
              arrayList.writeInt(0);
            } 
            return true;
          case 72:
            iccOpenLogicalChannelResponse.enforceInterface("com.android.internal.telephony.ITelephony");
            i7 = iccOpenLogicalChannelResponse.readInt();
            setCellInfoListRate(i7);
            arrayList.writeNoException();
            return true;
          case 71:
            return onTransact$requestCellInfoUpdateWithWorkSource$((Parcel)iccOpenLogicalChannelResponse, (Parcel)arrayList);
          case 70:
            iccOpenLogicalChannelResponse.enforceInterface("com.android.internal.telephony.ITelephony");
            i7 = iccOpenLogicalChannelResponse.readInt();
            iCellInfoCallback = ICellInfoCallback.Stub.asInterface(iccOpenLogicalChannelResponse.readStrongBinder());
            str39 = iccOpenLogicalChannelResponse.readString();
            str8 = iccOpenLogicalChannelResponse.readString();
            requestCellInfoUpdate(i7, iCellInfoCallback, str39, str8);
            arrayList.writeNoException();
            return true;
          case 69:
            str8.enforceInterface("com.android.internal.telephony.ITelephony");
            str33 = str8.readString();
            str8 = str8.readString();
            list1 = getAllCellInfo(str33, str8);
            arrayList.writeNoException();
            arrayList.writeTypedList(list1);
            return true;
          case 68:
            list1.enforceInterface("com.android.internal.telephony.ITelephony");
            i7 = list1.readInt();
            str33 = list1.readString();
            str7 = list1.readString();
            i7 = getLteOnCdmaModeForSubscriber(i7, str33, str7);
            arrayList.writeNoException();
            arrayList.writeInt(i7);
            return true;
          case 67:
            str7.enforceInterface("com.android.internal.telephony.ITelephony");
            str33 = str7.readString();
            str7 = str7.readString();
            i7 = getLteOnCdmaMode(str33, str7);
            arrayList.writeNoException();
            arrayList.writeInt(i7);
            return true;
          case 66:
            str7.enforceInterface("com.android.internal.telephony.ITelephony");
            i7 = str7.readInt();
            bool12 = hasIccCardUsingSlotIndex(i7);
            arrayList.writeNoException();
            arrayList.writeInt(bool12);
            return true;
          case 65:
            str7.enforceInterface("com.android.internal.telephony.ITelephony");
            bool12 = hasIccCard();
            arrayList.writeNoException();
            arrayList.writeInt(bool12);
            return true;
          case 64:
            str7.enforceInterface("com.android.internal.telephony.ITelephony");
            i6 = str7.readInt();
            str33 = str7.readString();
            str7 = str7.readString();
            i6 = getVoiceNetworkTypeForSubscriber(i6, str33, str7);
            arrayList.writeNoException();
            arrayList.writeInt(i6);
            return true;
          case 63:
            str7.enforceInterface("com.android.internal.telephony.ITelephony");
            i6 = str7.readInt();
            str33 = str7.readString();
            str7 = str7.readString();
            i6 = getDataNetworkTypeForSubscriber(i6, str33, str7);
            arrayList.writeNoException();
            arrayList.writeInt(i6);
            return true;
          case 62:
            str7.enforceInterface("com.android.internal.telephony.ITelephony");
            str33 = str7.readString();
            str7 = str7.readString();
            i6 = getDataNetworkType(str33, str7);
            arrayList.writeNoException();
            arrayList.writeInt(i6);
            return true;
          case 61:
            str7.enforceInterface("com.android.internal.telephony.ITelephony");
            i6 = str7.readInt();
            str33 = str7.readString();
            str7 = str7.readString();
            i6 = getNetworkTypeForSubscriber(i6, str33, str7);
            arrayList.writeNoException();
            arrayList.writeInt(i6);
            return true;
          case 60:
            str7.enforceInterface("com.android.internal.telephony.ITelephony");
            str33 = str7.readString();
            str7 = str7.readString();
            sendDialerSpecialCode(str33, str7);
            arrayList.writeNoException();
            return true;
          case 59:
            return onTransact$sendVisualVoicemailSmsForSubscriber$((Parcel)str7, (Parcel)arrayList);
          case 58:
            str7.enforceInterface("com.android.internal.telephony.ITelephony");
            i6 = str7.readInt();
            visualVoicemailSmsFilterSettings = getActiveVisualVoicemailSmsFilterSettings(i6);
            arrayList.writeNoException();
            if (visualVoicemailSmsFilterSettings != null) {
              arrayList.writeInt(1);
              visualVoicemailSmsFilterSettings.writeToParcel((Parcel)arrayList, 1);
            } else {
              arrayList.writeInt(0);
            } 
            return true;
          case 57:
            visualVoicemailSmsFilterSettings.enforceInterface("com.android.internal.telephony.ITelephony");
            str33 = visualVoicemailSmsFilterSettings.readString();
            i6 = visualVoicemailSmsFilterSettings.readInt();
            visualVoicemailSmsFilterSettings = getVisualVoicemailSmsFilterSettings(str33, i6);
            arrayList.writeNoException();
            if (visualVoicemailSmsFilterSettings != null) {
              arrayList.writeInt(1);
              visualVoicemailSmsFilterSettings.writeToParcel((Parcel)arrayList, 1);
            } else {
              arrayList.writeInt(0);
            } 
            return true;
          case 56:
            visualVoicemailSmsFilterSettings.enforceInterface("com.android.internal.telephony.ITelephony");
            str = visualVoicemailSmsFilterSettings.readString();
            i6 = visualVoicemailSmsFilterSettings.readInt();
            disableVisualVoicemailSmsFilter(str, i6);
            return true;
          case 55:
            visualVoicemailSmsFilterSettings.enforceInterface("com.android.internal.telephony.ITelephony");
            str33 = visualVoicemailSmsFilterSettings.readString();
            i6 = visualVoicemailSmsFilterSettings.readInt();
            if (visualVoicemailSmsFilterSettings.readInt() != 0) {
              visualVoicemailSmsFilterSettings = (VisualVoicemailSmsFilterSettings)VisualVoicemailSmsFilterSettings.CREATOR.createFromParcel((Parcel)visualVoicemailSmsFilterSettings);
            } else {
              visualVoicemailSmsFilterSettings = null;
            } 
            enableVisualVoicemailSmsFilter(str33, i6, visualVoicemailSmsFilterSettings);
            str.writeNoException();
            return true;
          case 54:
            visualVoicemailSmsFilterSettings.enforceInterface("com.android.internal.telephony.ITelephony");
            str33 = visualVoicemailSmsFilterSettings.readString();
            str39 = visualVoicemailSmsFilterSettings.readString();
            i6 = visualVoicemailSmsFilterSettings.readInt();
            str6 = getVisualVoicemailPackageName(str33, str39, i6);
            str.writeNoException();
            str.writeString(str6);
            return true;
          case 53:
            str6.enforceInterface("com.android.internal.telephony.ITelephony");
            str33 = str6.readString();
            i6 = str6.readInt();
            bundle = getVisualVoicemailSettings(str33, i6);
            str.writeNoException();
            if (bundle != null) {
              str.writeInt(1);
              bundle.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 52:
            bundle.enforceInterface("com.android.internal.telephony.ITelephony");
            i6 = bundle.readInt();
            bool11 = isConcurrentVoiceAndDataAllowed(i6);
            str.writeNoException();
            str.writeInt(bool11);
            return true;
          case 51:
            bundle.enforceInterface("com.android.internal.telephony.ITelephony");
            i5 = bundle.readInt();
            str33 = bundle.readString();
            str5 = bundle.readString();
            i5 = getVoiceMessageCountForSubscriber(i5, str33, str5);
            str.writeNoException();
            str.writeInt(i5);
            return true;
          case 50:
            str5.enforceInterface("com.android.internal.telephony.ITelephony");
            i5 = str5.readInt();
            str5 = str5.readString();
            i5 = getDataActivationState(i5, str5);
            str.writeNoException();
            str.writeInt(i5);
            return true;
          case 49:
            str5.enforceInterface("com.android.internal.telephony.ITelephony");
            i5 = str5.readInt();
            str5 = str5.readString();
            i5 = getVoiceActivationState(i5, str5);
            str.writeNoException();
            str.writeInt(i5);
            return true;
          case 48:
            str5.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = str5.readInt();
            i5 = str5.readInt();
            setDataActivationState(param1Int2, i5);
            str.writeNoException();
            return true;
          case 47:
            str5.enforceInterface("com.android.internal.telephony.ITelephony");
            param1Int2 = str5.readInt();
            i5 = str5.readInt();
            setVoiceActivationState(param1Int2, i5);
            str.writeNoException();
            return true;
          case 46:
            str5.enforceInterface("com.android.internal.telephony.ITelephony");
            i5 = str5.readInt();
            str33 = str5.readString();
            str5 = str5.readString();
            bool10 = setVoiceMailNumber(i5, str33, str5);
            str.writeNoException();
            str.writeInt(bool10);
            return true;
          case 45:
            str5.enforceInterface("com.android.internal.telephony.ITelephony");
            bool10 = needsOtaServiceProvisioning();
            str.writeNoException();
            str.writeInt(bool10);
            return true;
          case 44:
            str5.enforceInterface("com.android.internal.telephony.ITelephony");
            i4 = str5.readInt();
            str33 = str5.readString();
            str5 = str5.readString();
            str5 = getCdmaEriTextForSubscriber(i4, str33, str5);
            str.writeNoException();
            str.writeString(str5);
            return true;
          case 43:
            str5.enforceInterface("com.android.internal.telephony.ITelephony");
            str33 = str5.readString();
            str5 = str5.readString();
            str5 = getCdmaEriText(str33, str5);
            str.writeNoException();
            str.writeString(str5);
            return true;
          case 42:
            str5.enforceInterface("com.android.internal.telephony.ITelephony");
            i4 = str5.readInt();
            str33 = str5.readString();
            str5 = str5.readString();
            i4 = getCdmaEriIconModeForSubscriber(i4, str33, str5);
            str.writeNoException();
            str.writeInt(i4);
            return true;
          case 41:
            str5.enforceInterface("com.android.internal.telephony.ITelephony");
            str33 = str5.readString();
            str5 = str5.readString();
            i4 = getCdmaEriIconMode(str33, str5);
            str.writeNoException();
            str.writeInt(i4);
            return true;
          case 40:
            str5.enforceInterface("com.android.internal.telephony.ITelephony");
            i4 = str5.readInt();
            str33 = str5.readString();
            str5 = str5.readString();
            i4 = getCdmaEriIconIndexForSubscriber(i4, str33, str5);
            str.writeNoException();
            str.writeInt(i4);
            return true;
          case 39:
            str5.enforceInterface("com.android.internal.telephony.ITelephony");
            str33 = str5.readString();
            str5 = str5.readString();
            i4 = getCdmaEriIconIndex(str33, str5);
            str.writeNoException();
            str.writeInt(i4);
            return true;
          case 38:
            str5.enforceInterface("com.android.internal.telephony.ITelephony");
            i4 = str5.readInt();
            i4 = getActivePhoneTypeForSlot(i4);
            str.writeNoException();
            str.writeInt(i4);
            return true;
          case 37:
            str5.enforceInterface("com.android.internal.telephony.ITelephony");
            i4 = getActivePhoneType();
            str.writeNoException();
            str.writeInt(i4);
            return true;
          case 36:
            str5.enforceInterface("com.android.internal.telephony.ITelephony");
            i4 = str5.readInt();
            i4 = getDataStateForSubId(i4);
            str.writeNoException();
            str.writeInt(i4);
            return true;
          case 35:
            str5.enforceInterface("com.android.internal.telephony.ITelephony");
            i4 = getDataState();
            str.writeNoException();
            str.writeInt(i4);
            return true;
          case 34:
            str5.enforceInterface("com.android.internal.telephony.ITelephony");
            i4 = str5.readInt();
            i4 = getDataActivityForSubId(i4);
            str.writeNoException();
            str.writeInt(i4);
            return true;
          case 33:
            str5.enforceInterface("com.android.internal.telephony.ITelephony");
            i4 = getDataActivity();
            str.writeNoException();
            str.writeInt(i4);
            return true;
          case 32:
            str5.enforceInterface("com.android.internal.telephony.ITelephony");
            i4 = str5.readInt();
            i4 = getCallStateForSlot(i4);
            str.writeNoException();
            str.writeInt(i4);
            return true;
          case 31:
            str5.enforceInterface("com.android.internal.telephony.ITelephony");
            i4 = getCallState();
            str.writeNoException();
            str.writeInt(i4);
            return true;
          case 30:
            str5.enforceInterface("com.android.internal.telephony.ITelephony");
            str33 = str5.readString();
            str5 = str5.readString();
            list = getNeighboringCellInfo(str33, str5);
            str.writeNoException();
            str.writeTypedList(list);
            return true;
          case 29:
            list.enforceInterface("com.android.internal.telephony.ITelephony");
            i4 = list.readInt();
            str4 = getNetworkCountryIsoForPhone(i4);
            str.writeNoException();
            str.writeString(str4);
            return true;
          case 28:
            str4.enforceInterface("com.android.internal.telephony.ITelephony");
            str33 = str4.readString();
            str4 = str4.readString();
            cellIdentity = getCellLocation(str33, str4);
            str.writeNoException();
            if (cellIdentity != null) {
              str.writeInt(1);
              cellIdentity.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 27:
            cellIdentity.enforceInterface("com.android.internal.telephony.ITelephony");
            i4 = cellIdentity.readInt();
            bool9 = isDataConnectivityPossible(i4);
            str.writeNoException();
            str.writeInt(bool9);
            return true;
          case 26:
            cellIdentity.enforceInterface("com.android.internal.telephony.ITelephony");
            bool9 = disableDataConnectivity();
            str.writeNoException();
            str.writeInt(bool9);
            return true;
          case 25:
            cellIdentity.enforceInterface("com.android.internal.telephony.ITelephony");
            bool9 = enableDataConnectivity();
            str.writeNoException();
            str.writeInt(bool9);
            return true;
          case 24:
            cellIdentity.enforceInterface("com.android.internal.telephony.ITelephony");
            i3 = cellIdentity.readInt();
            disableLocationUpdatesForSubscriber(i3);
            str.writeNoException();
            return true;
          case 23:
            cellIdentity.enforceInterface("com.android.internal.telephony.ITelephony");
            disableLocationUpdates();
            str.writeNoException();
            return true;
          case 22:
            cellIdentity.enforceInterface("com.android.internal.telephony.ITelephony");
            i3 = cellIdentity.readInt();
            enableLocationUpdatesForSubscriber(i3);
            str.writeNoException();
            return true;
          case 21:
            cellIdentity.enforceInterface("com.android.internal.telephony.ITelephony");
            enableLocationUpdates();
            str.writeNoException();
            return true;
          case 20:
            cellIdentity.enforceInterface("com.android.internal.telephony.ITelephony");
            i3 = cellIdentity.readInt();
            updateServiceLocationForSubscriber(i3);
            str.writeNoException();
            return true;
          case 19:
            cellIdentity.enforceInterface("com.android.internal.telephony.ITelephony");
            updateServiceLocation();
            str.writeNoException();
            return true;
          case 18:
            cellIdentity.enforceInterface("com.android.internal.telephony.ITelephony");
            bool93 = bool98;
            if (cellIdentity.readInt() != 0)
              bool93 = true; 
            bool8 = setRadioPower(bool93);
            str.writeNoException();
            str.writeInt(bool8);
            return true;
          case 17:
            cellIdentity.enforceInterface("com.android.internal.telephony.ITelephony");
            i2 = cellIdentity.readInt();
            bool93 = bool99;
            if (cellIdentity.readInt() != 0)
              bool93 = true; 
            bool7 = setRadioForSubscriber(i2, bool93);
            str.writeNoException();
            str.writeInt(bool7);
            return true;
          case 16:
            cellIdentity.enforceInterface("com.android.internal.telephony.ITelephony");
            bool93 = bool100;
            if (cellIdentity.readInt() != 0)
              bool93 = true; 
            bool7 = setRadio(bool93);
            str.writeNoException();
            str.writeInt(bool7);
            return true;
          case 15:
            cellIdentity.enforceInterface("com.android.internal.telephony.ITelephony");
            i1 = cellIdentity.readInt();
            toggleRadioOnOffForSubscriber(i1);
            str.writeNoException();
            return true;
          case 14:
            cellIdentity.enforceInterface("com.android.internal.telephony.ITelephony");
            toggleRadioOnOff();
            str.writeNoException();
            return true;
          case 13:
            cellIdentity.enforceInterface("com.android.internal.telephony.ITelephony");
            i1 = cellIdentity.readInt();
            str3 = cellIdentity.readString();
            bool6 = handlePinMmiForSubscriber(i1, str3);
            str.writeNoException();
            str.writeInt(bool6);
            return true;
          case 12:
            str3.enforceInterface("com.android.internal.telephony.ITelephony");
            n = str3.readInt();
            str33 = str3.readString();
            if (str3.readInt() != 0) {
              ResultReceiver resultReceiver = (ResultReceiver)ResultReceiver.CREATOR.createFromParcel((Parcel)str3);
            } else {
              str3 = null;
            } 
            handleUssdRequest(n, str33, (ResultReceiver)str3);
            str.writeNoException();
            return true;
          case 11:
            str3.enforceInterface("com.android.internal.telephony.ITelephony");
            str3 = str3.readString();
            bool5 = handlePinMmi(str3);
            str.writeNoException();
            str.writeInt(bool5);
            return true;
          case 10:
            str3.enforceInterface("com.android.internal.telephony.ITelephony");
            m = str3.readInt();
            str33 = str3.readString();
            str3 = str3.readString();
            arrayOfInt2 = supplyPukReportResultForSubscriber(m, str33, str3);
            str.writeNoException();
            str.writeIntArray(arrayOfInt2);
            return true;
          case 9:
            arrayOfInt2.enforceInterface("com.android.internal.telephony.ITelephony");
            m = arrayOfInt2.readInt();
            str2 = arrayOfInt2.readString();
            arrayOfInt1 = supplyPinReportResultForSubscriber(m, str2);
            str.writeNoException();
            str.writeIntArray(arrayOfInt1);
            return true;
          case 8:
            arrayOfInt1.enforceInterface("com.android.internal.telephony.ITelephony");
            m = arrayOfInt1.readInt();
            str33 = arrayOfInt1.readString();
            str1 = arrayOfInt1.readString();
            bool4 = supplyPukForSubscriber(m, str33, str1);
            str.writeNoException();
            str.writeInt(bool4);
            return true;
          case 7:
            str1.enforceInterface("com.android.internal.telephony.ITelephony");
            k = str1.readInt();
            str1 = str1.readString();
            bool3 = supplyPinForSubscriber(k, str1);
            str.writeNoException();
            str.writeInt(bool3);
            return true;
          case 6:
            str1.enforceInterface("com.android.internal.telephony.ITelephony");
            j = str1.readInt();
            str33 = str1.readString();
            str1 = str1.readString();
            bool2 = isRadioOnForSubscriberWithFeature(j, str33, str1);
            str.writeNoException();
            str.writeInt(bool2);
            return true;
          case 5:
            str1.enforceInterface("com.android.internal.telephony.ITelephony");
            i = str1.readInt();
            str1 = str1.readString();
            bool1 = isRadioOnForSubscriber(i, str1);
            str.writeNoException();
            str.writeInt(bool1);
            return true;
          case 4:
            str1.enforceInterface("com.android.internal.telephony.ITelephony");
            str33 = str1.readString();
            str1 = str1.readString();
            bool1 = isRadioOnWithFeature(str33, str1);
            str.writeNoException();
            str.writeInt(bool1);
            return true;
          case 3:
            str1.enforceInterface("com.android.internal.telephony.ITelephony");
            str1 = str1.readString();
            bool1 = isRadioOn(str1);
            str.writeNoException();
            str.writeInt(bool1);
            return true;
          case 2:
            str1.enforceInterface("com.android.internal.telephony.ITelephony");
            str33 = str1.readString();
            str1 = str1.readString();
            call(str33, str1);
            str.writeNoException();
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("com.android.internal.telephony.ITelephony");
        String str1 = str1.readString();
        dial(str1);
        str.writeNoException();
        return true;
      } 
      str.writeString("com.android.internal.telephony.ITelephony");
      return true;
    }
    
    private static class Proxy implements ITelephony {
      public static ITelephony sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.ITelephony";
      }
      
      public void dial(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().dial(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void call(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().call(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isRadioOn(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isRadioOn(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isRadioOnWithFeature(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isRadioOnWithFeature(param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isRadioOnForSubscriber(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isRadioOnForSubscriber(param2Int, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isRadioOnForSubscriberWithFeature(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(6, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isRadioOnForSubscriberWithFeature(param2Int, param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean supplyPinForSubscriber(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(7, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().supplyPinForSubscriber(param2Int, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean supplyPukForSubscriber(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(8, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().supplyPukForSubscriber(param2Int, param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] supplyPinReportResultForSubscriber(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().supplyPinReportResultForSubscriber(param2Int, param2String); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] supplyPukReportResultForSubscriber(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().supplyPukReportResultForSubscriber(param2Int, param2String1, param2String2); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean handlePinMmi(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(11, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().handlePinMmi(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void handleUssdRequest(int param2Int, String param2String, ResultReceiver param2ResultReceiver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          if (param2ResultReceiver != null) {
            parcel1.writeInt(1);
            param2ResultReceiver.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().handleUssdRequest(param2Int, param2String, param2ResultReceiver);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean handlePinMmiForSubscriber(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(13, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().handlePinMmiForSubscriber(param2Int, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void toggleRadioOnOff() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().toggleRadioOnOff();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void toggleRadioOnOffForSubscriber(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().toggleRadioOnOffForSubscriber(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setRadio(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null) {
            param2Boolean = ITelephony.Stub.getDefaultImpl().setRadio(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setRadioForSubscriber(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Boolean = ITelephony.Stub.getDefaultImpl().setRadioForSubscriber(param2Int, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setRadioPower(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null) {
            param2Boolean = ITelephony.Stub.getDefaultImpl().setRadioPower(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateServiceLocation() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().updateServiceLocation();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateServiceLocationForSubscriber(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().updateServiceLocationForSubscriber(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enableLocationUpdates() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().enableLocationUpdates();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enableLocationUpdatesForSubscriber(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().enableLocationUpdatesForSubscriber(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disableLocationUpdates() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().disableLocationUpdates();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disableLocationUpdatesForSubscriber(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().disableLocationUpdatesForSubscriber(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean enableDataConnectivity() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(25, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().enableDataConnectivity();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean disableDataConnectivity() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(26, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().disableDataConnectivity();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isDataConnectivityPossible(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(27, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isDataConnectivityPossible(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public CellIdentity getCellLocation(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getCellLocation(param2String1, param2String2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            CellIdentity cellIdentity = (CellIdentity)CellIdentity.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (CellIdentity)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getNetworkCountryIsoForPhone(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getNetworkCountryIsoForPhone(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<NeighboringCellInfo> getNeighboringCellInfo(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getNeighboringCellInfo(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.createTypedArrayList(NeighboringCellInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getCallState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getCallState(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getCallStateForSlot(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getCallStateForSlot(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getDataActivity() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getDataActivity(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getDataActivityForSubId(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getDataActivityForSubId(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getDataState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          boolean bool = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getDataState(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getDataStateForSubId(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getDataStateForSubId(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getActivePhoneType() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          boolean bool = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getActivePhoneType(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getActivePhoneTypeForSlot(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(38, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getActivePhoneTypeForSlot(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getCdmaEriIconIndex(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getCdmaEriIconIndex(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getCdmaEriIconIndexForSubscriber(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(40, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getCdmaEriIconIndexForSubscriber(param2Int, param2String1, param2String2);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getCdmaEriIconMode(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(41, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getCdmaEriIconMode(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getCdmaEriIconModeForSubscriber(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(42, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getCdmaEriIconModeForSubscriber(param2Int, param2String1, param2String2);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getCdmaEriText(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(43, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2String1 = ITelephony.Stub.getDefaultImpl().getCdmaEriText(param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getCdmaEriTextForSubscriber(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(44, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2String1 = ITelephony.Stub.getDefaultImpl().getCdmaEriTextForSubscriber(param2Int, param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean needsOtaServiceProvisioning() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(45, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().needsOtaServiceProvisioning();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setVoiceMailNumber(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(46, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().setVoiceMailNumber(param2Int, param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setVoiceActivationState(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(47, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().setVoiceActivationState(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDataActivationState(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(48, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().setDataActivationState(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getVoiceActivationState(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(49, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getVoiceActivationState(param2Int, param2String);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getDataActivationState(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(50, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getDataActivationState(param2Int, param2String);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getVoiceMessageCountForSubscriber(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(51, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getVoiceMessageCountForSubscriber(param2Int, param2String1, param2String2);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isConcurrentVoiceAndDataAllowed(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(52, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isConcurrentVoiceAndDataAllowed(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bundle getVisualVoicemailSettings(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(53, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getVisualVoicemailSettings(param2String, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (Bundle)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getVisualVoicemailPackageName(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(54, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2String1 = ITelephony.Stub.getDefaultImpl().getVisualVoicemailPackageName(param2String1, param2String2, param2Int);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enableVisualVoicemailSmsFilter(String param2String, int param2Int, VisualVoicemailSmsFilterSettings param2VisualVoicemailSmsFilterSettings) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          if (param2VisualVoicemailSmsFilterSettings != null) {
            parcel1.writeInt(1);
            param2VisualVoicemailSmsFilterSettings.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(55, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().enableVisualVoicemailSmsFilter(param2String, param2Int, param2VisualVoicemailSmsFilterSettings);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disableVisualVoicemailSmsFilter(String param2String, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(56, parcel, null, 1);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().disableVisualVoicemailSmsFilter(param2String, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public VisualVoicemailSmsFilterSettings getVisualVoicemailSmsFilterSettings(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(57, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getVisualVoicemailSmsFilterSettings(param2String, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            VisualVoicemailSmsFilterSettings visualVoicemailSmsFilterSettings = (VisualVoicemailSmsFilterSettings)VisualVoicemailSmsFilterSettings.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (VisualVoicemailSmsFilterSettings)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public VisualVoicemailSmsFilterSettings getActiveVisualVoicemailSmsFilterSettings(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          VisualVoicemailSmsFilterSettings visualVoicemailSmsFilterSettings;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(58, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            visualVoicemailSmsFilterSettings = ITelephony.Stub.getDefaultImpl().getActiveVisualVoicemailSmsFilterSettings(param2Int);
            return visualVoicemailSmsFilterSettings;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            visualVoicemailSmsFilterSettings = (VisualVoicemailSmsFilterSettings)VisualVoicemailSmsFilterSettings.CREATOR.createFromParcel(parcel2);
          } else {
            visualVoicemailSmsFilterSettings = null;
          } 
          return visualVoicemailSmsFilterSettings;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendVisualVoicemailSmsForSubscriber(String param2String1, String param2String2, int param2Int1, String param2String3, int param2Int2, String param2String4, PendingIntent param2PendingIntent) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeString(param2String2);
              try {
                parcel1.writeInt(param2Int1);
                try {
                  parcel1.writeString(param2String3);
                  parcel1.writeInt(param2Int2);
                  parcel1.writeString(param2String4);
                  if (param2PendingIntent != null) {
                    parcel1.writeInt(1);
                    param2PendingIntent.writeToParcel(parcel1, 0);
                  } else {
                    parcel1.writeInt(0);
                  } 
                  boolean bool = this.mRemote.transact(59, parcel1, parcel2, 0);
                  if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
                    ITelephony.Stub.getDefaultImpl().sendVisualVoicemailSmsForSubscriber(param2String1, param2String2, param2Int1, param2String3, param2Int2, param2String4, param2PendingIntent);
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } 
                  parcel2.readException();
                  parcel2.recycle();
                  parcel1.recycle();
                  return;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void sendDialerSpecialCode(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(60, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().sendDialerSpecialCode(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getNetworkTypeForSubscriber(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(61, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getNetworkTypeForSubscriber(param2Int, param2String1, param2String2);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getDataNetworkType(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(62, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getDataNetworkType(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getDataNetworkTypeForSubscriber(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(63, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getDataNetworkTypeForSubscriber(param2Int, param2String1, param2String2);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getVoiceNetworkTypeForSubscriber(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(64, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getVoiceNetworkTypeForSubscriber(param2Int, param2String1, param2String2);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasIccCard() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(65, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().hasIccCard();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasIccCardUsingSlotIndex(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(66, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().hasIccCardUsingSlotIndex(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getLteOnCdmaMode(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(67, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getLteOnCdmaMode(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getLteOnCdmaModeForSubscriber(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(68, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getLteOnCdmaModeForSubscriber(param2Int, param2String1, param2String2);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<CellInfo> getAllCellInfo(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(69, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getAllCellInfo(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.createTypedArrayList(CellInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestCellInfoUpdate(int param2Int, ICellInfoCallback param2ICellInfoCallback, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          if (param2ICellInfoCallback != null) {
            iBinder = param2ICellInfoCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(70, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().requestCellInfoUpdate(param2Int, param2ICellInfoCallback, param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestCellInfoUpdateWithWorkSource(int param2Int, ICellInfoCallback param2ICellInfoCallback, String param2String1, String param2String2, WorkSource param2WorkSource) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          if (param2ICellInfoCallback != null) {
            iBinder = param2ICellInfoCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2WorkSource != null) {
            parcel1.writeInt(1);
            param2WorkSource.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(71, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().requestCellInfoUpdateWithWorkSource(param2Int, param2ICellInfoCallback, param2String1, param2String2, param2WorkSource);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setCellInfoListRate(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(72, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().setCellInfoListRate(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IccOpenLogicalChannelResponse iccOpenLogicalChannelBySlot(int param2Int1, String param2String1, String param2String2, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(73, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().iccOpenLogicalChannelBySlot(param2Int1, param2String1, param2String2, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            IccOpenLogicalChannelResponse iccOpenLogicalChannelResponse = (IccOpenLogicalChannelResponse)IccOpenLogicalChannelResponse.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (IccOpenLogicalChannelResponse)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IccOpenLogicalChannelResponse iccOpenLogicalChannel(int param2Int1, String param2String1, String param2String2, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(74, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().iccOpenLogicalChannel(param2Int1, param2String1, param2String2, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            IccOpenLogicalChannelResponse iccOpenLogicalChannelResponse = (IccOpenLogicalChannelResponse)IccOpenLogicalChannelResponse.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (IccOpenLogicalChannelResponse)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean iccCloseLogicalChannelBySlot(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(75, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().iccCloseLogicalChannelBySlot(param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean iccCloseLogicalChannel(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(76, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().iccCloseLogicalChannel(param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String iccTransmitApduLogicalChannelBySlot(int param2Int1, int param2Int2, int param2Int3, int param2Int4, int param2Int5, int param2Int6, int param2Int7, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeInt(param2Int2);
              try {
                parcel1.writeInt(param2Int3);
                try {
                  parcel1.writeInt(param2Int4);
                  parcel1.writeInt(param2Int5);
                  parcel1.writeInt(param2Int6);
                  parcel1.writeInt(param2Int7);
                  parcel1.writeString(param2String);
                  boolean bool = this.mRemote.transact(77, parcel1, parcel2, 0);
                  if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
                    param2String = ITelephony.Stub.getDefaultImpl().iccTransmitApduLogicalChannelBySlot(param2Int1, param2Int2, param2Int3, param2Int4, param2Int5, param2Int6, param2Int7, param2String);
                    parcel2.recycle();
                    parcel1.recycle();
                    return param2String;
                  } 
                  parcel2.readException();
                  param2String = parcel2.readString();
                  parcel2.recycle();
                  parcel1.recycle();
                  return param2String;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public String iccTransmitApduLogicalChannel(int param2Int1, int param2Int2, int param2Int3, int param2Int4, int param2Int5, int param2Int6, int param2Int7, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeInt(param2Int2);
              try {
                parcel1.writeInt(param2Int3);
                try {
                  parcel1.writeInt(param2Int4);
                  parcel1.writeInt(param2Int5);
                  parcel1.writeInt(param2Int6);
                  parcel1.writeInt(param2Int7);
                  parcel1.writeString(param2String);
                  boolean bool = this.mRemote.transact(78, parcel1, parcel2, 0);
                  if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
                    param2String = ITelephony.Stub.getDefaultImpl().iccTransmitApduLogicalChannel(param2Int1, param2Int2, param2Int3, param2Int4, param2Int5, param2Int6, param2Int7, param2String);
                    parcel2.recycle();
                    parcel1.recycle();
                    return param2String;
                  } 
                  parcel2.readException();
                  param2String = parcel2.readString();
                  parcel2.recycle();
                  parcel1.recycle();
                  return param2String;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public String iccTransmitApduBasicChannelBySlot(int param2Int1, String param2String1, int param2Int2, int param2Int3, int param2Int4, int param2Int5, int param2Int6, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeString(param2String1);
              try {
                parcel1.writeInt(param2Int2);
                try {
                  parcel1.writeInt(param2Int3);
                  parcel1.writeInt(param2Int4);
                  parcel1.writeInt(param2Int5);
                  parcel1.writeInt(param2Int6);
                  parcel1.writeString(param2String2);
                  boolean bool = this.mRemote.transact(79, parcel1, parcel2, 0);
                  if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
                    param2String1 = ITelephony.Stub.getDefaultImpl().iccTransmitApduBasicChannelBySlot(param2Int1, param2String1, param2Int2, param2Int3, param2Int4, param2Int5, param2Int6, param2String2);
                    parcel2.recycle();
                    parcel1.recycle();
                    return param2String1;
                  } 
                  parcel2.readException();
                  param2String1 = parcel2.readString();
                  parcel2.recycle();
                  parcel1.recycle();
                  return param2String1;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public String iccTransmitApduBasicChannel(int param2Int1, String param2String1, int param2Int2, int param2Int3, int param2Int4, int param2Int5, int param2Int6, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeString(param2String1);
              try {
                parcel1.writeInt(param2Int2);
                try {
                  parcel1.writeInt(param2Int3);
                  parcel1.writeInt(param2Int4);
                  parcel1.writeInt(param2Int5);
                  parcel1.writeInt(param2Int6);
                  parcel1.writeString(param2String2);
                  boolean bool = this.mRemote.transact(80, parcel1, parcel2, 0);
                  if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
                    param2String1 = ITelephony.Stub.getDefaultImpl().iccTransmitApduBasicChannel(param2Int1, param2String1, param2Int2, param2Int3, param2Int4, param2Int5, param2Int6, param2String2);
                    parcel2.recycle();
                    parcel1.recycle();
                    return param2String1;
                  } 
                  parcel2.readException();
                  param2String1 = parcel2.readString();
                  parcel2.recycle();
                  parcel1.recycle();
                  return param2String1;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public byte[] iccExchangeSimIO(int param2Int1, int param2Int2, int param2Int3, int param2Int4, int param2Int5, int param2Int6, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeInt(param2Int2);
              try {
                parcel1.writeInt(param2Int3);
                try {
                  parcel1.writeInt(param2Int4);
                  try {
                    parcel1.writeInt(param2Int5);
                    parcel1.writeInt(param2Int6);
                    parcel1.writeString(param2String);
                    boolean bool = this.mRemote.transact(81, parcel1, parcel2, 0);
                    if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
                      byte[] arrayOfByte1 = ITelephony.Stub.getDefaultImpl().iccExchangeSimIO(param2Int1, param2Int2, param2Int3, param2Int4, param2Int5, param2Int6, param2String);
                      parcel2.recycle();
                      parcel1.recycle();
                      return arrayOfByte1;
                    } 
                    parcel2.readException();
                    byte[] arrayOfByte = parcel2.createByteArray();
                    parcel2.recycle();
                    parcel1.recycle();
                    return arrayOfByte;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public String sendEnvelopeWithStatus(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(82, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2String = ITelephony.Stub.getDefaultImpl().sendEnvelopeWithStatus(param2Int, param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String nvReadItem(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(83, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().nvReadItem(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean nvWriteItem(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(84, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().nvWriteItem(param2Int, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean nvWriteCdmaPrl(byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeByteArray(param2ArrayOfbyte);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(85, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().nvWriteCdmaPrl(param2ArrayOfbyte);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean resetModemConfig(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(86, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().resetModemConfig(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean rebootModem(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(87, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().rebootModem(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getCalculatedPreferredNetworkType(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(88, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getCalculatedPreferredNetworkType(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getPreferredNetworkType(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(89, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getPreferredNetworkType(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isTetheringApnRequiredForSubscriber(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(90, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isTetheringApnRequiredForSubscriber(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enableIms(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(91, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().enableIms(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disableIms(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(92, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().disableIms(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resetIms(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(93, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().resetIms(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IImsMmTelFeature getMmTelFeatureAndListen(int param2Int, IImsServiceFeatureCallback param2IImsServiceFeatureCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          if (param2IImsServiceFeatureCallback != null) {
            iBinder = param2IImsServiceFeatureCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(94, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getMmTelFeatureAndListen(param2Int, param2IImsServiceFeatureCallback); 
          parcel2.readException();
          return IImsMmTelFeature.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IImsRcsFeature getRcsFeatureAndListen(int param2Int, IImsServiceFeatureCallback param2IImsServiceFeatureCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          if (param2IImsServiceFeatureCallback != null) {
            iBinder = param2IImsServiceFeatureCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(95, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getRcsFeatureAndListen(param2Int, param2IImsServiceFeatureCallback); 
          parcel2.readException();
          return IImsRcsFeature.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterImsFeatureCallback(int param2Int1, int param2Int2, IImsServiceFeatureCallback param2IImsServiceFeatureCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2IImsServiceFeatureCallback != null) {
            iBinder = param2IImsServiceFeatureCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(96, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().unregisterImsFeatureCallback(param2Int1, param2Int2, param2IImsServiceFeatureCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IImsRegistration getImsRegistration(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(97, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getImsRegistration(param2Int1, param2Int2); 
          parcel2.readException();
          return IImsRegistration.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IImsConfig getImsConfig(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(98, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getImsConfig(param2Int1, param2Int2); 
          parcel2.readException();
          return IImsConfig.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setBoundImsServiceOverride(int param2Int, boolean param2Boolean, int[] param2ArrayOfint, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeIntArray(param2ArrayOfint);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(99, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Boolean = ITelephony.Stub.getDefaultImpl().setBoundImsServiceOverride(param2Int, param2Boolean, param2ArrayOfint, param2String);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getBoundImsServicePackage(int param2Int1, boolean param2Boolean, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int2);
          boolean bool1 = this.mRemote.transact(100, parcel1, parcel2, 0);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getBoundImsServicePackage(param2Int1, param2Boolean, param2Int2); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void getImsMmTelFeatureState(int param2Int, IIntegerConsumer param2IIntegerConsumer) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          if (param2IIntegerConsumer != null) {
            iBinder = param2IIntegerConsumer.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(101, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().getImsMmTelFeatureState(param2Int, param2IIntegerConsumer);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setNetworkSelectionModeAutomatic(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(102, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().setNetworkSelectionModeAutomatic(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public CellNetworkScanResult getCellNetworkScanResults(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(103, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getCellNetworkScanResults(param2Int, param2String1, param2String2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            CellNetworkScanResult cellNetworkScanResult = (CellNetworkScanResult)CellNetworkScanResult.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (CellNetworkScanResult)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int requestNetworkScan(int param2Int, NetworkScanRequest param2NetworkScanRequest, Messenger param2Messenger, IBinder param2IBinder, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          try {
            parcel1.writeInt(param2Int);
            if (param2NetworkScanRequest != null) {
              parcel1.writeInt(1);
              param2NetworkScanRequest.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            if (param2Messenger != null) {
              parcel1.writeInt(1);
              param2Messenger.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            try {
              parcel1.writeStrongBinder(param2IBinder);
              try {
                parcel1.writeString(param2String1);
                try {
                  parcel1.writeString(param2String2);
                  boolean bool = this.mRemote.transact(104, parcel1, parcel2, 0);
                  if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
                    param2Int = ITelephony.Stub.getDefaultImpl().requestNetworkScan(param2Int, param2NetworkScanRequest, param2Messenger, param2IBinder, param2String1, param2String2);
                    parcel2.recycle();
                    parcel1.recycle();
                    return param2Int;
                  } 
                  parcel2.readException();
                  param2Int = parcel2.readInt();
                  parcel2.recycle();
                  parcel1.recycle();
                  return param2Int;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2NetworkScanRequest;
      }
      
      public void stopNetworkScan(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(105, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().stopNetworkScan(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setNetworkSelectionModeManual(int param2Int, OperatorInfo param2OperatorInfo, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2OperatorInfo != null) {
            parcel1.writeInt(1);
            param2OperatorInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(106, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Boolean = ITelephony.Stub.getDefaultImpl().setNetworkSelectionModeManual(param2Int, param2OperatorInfo, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getAllowedNetworkTypes(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(107, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getAllowedNetworkTypes(param2Int); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setAllowedNetworkTypes(int param2Int, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeLong(param2Long);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(108, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().setAllowedNetworkTypes(param2Int, param2Long);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getAllowedNetworkTypesForReason(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(109, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getAllowedNetworkTypesForReason(param2Int1, param2Int2); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getEffectiveAllowedNetworkTypes(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(110, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getEffectiveAllowedNetworkTypes(param2Int); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setAllowedNetworkTypesForReason(int param2Int1, int param2Int2, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeLong(param2Long);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(111, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().setAllowedNetworkTypesForReason(param2Int1, param2Int2, param2Long);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setPreferredNetworkType(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(112, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().setPreferredNetworkType(param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUserDataEnabled(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(113, parcel1, parcel2, 0);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().setUserDataEnabled(param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getDataEnabled(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(114, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().getDataEnabled(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isUserDataEnabled(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(115, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isUserDataEnabled(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isDataEnabled(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(116, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isDataEnabled(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isManualNetworkSelectionAllowed(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(117, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isManualNetworkSelectionAllowed(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAlwaysReportSignalStrength(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(118, parcel1, parcel2, 0);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().setAlwaysReportSignalStrength(param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getPcscfAddress(String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(119, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getPcscfAddress(param2String1, param2String2, param2String3); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setImsRegistrationState(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(120, parcel1, parcel2, 0);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().setImsRegistrationState(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getCdmaMdn(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(121, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getCdmaMdn(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getCdmaMin(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(122, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getCdmaMin(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestNumberVerification(PhoneNumberRange param2PhoneNumberRange, long param2Long, INumberVerificationCallback param2INumberVerificationCallback, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          if (param2PhoneNumberRange != null) {
            parcel1.writeInt(1);
            param2PhoneNumberRange.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeLong(param2Long);
          if (param2INumberVerificationCallback != null) {
            iBinder = param2INumberVerificationCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(123, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().requestNumberVerification(param2PhoneNumberRange, param2Long, param2INumberVerificationCallback, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getCarrierPrivilegeStatus(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(124, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getCarrierPrivilegeStatus(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getCarrierPrivilegeStatusForUid(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(125, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int1 = ITelephony.Stub.getDefaultImpl().getCarrierPrivilegeStatusForUid(param2Int1, param2Int2);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int checkCarrierPrivilegesForPackage(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(126, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().checkCarrierPrivilegesForPackage(param2Int, param2String);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int checkCarrierPrivilegesForPackageAnyPhone(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(127, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().checkCarrierPrivilegesForPackageAnyPhone(param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getCarrierPackageNamesForIntentAndPhone(Intent param2Intent, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(128, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getCarrierPackageNamesForIntentAndPhone(param2Intent, param2Int); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setLine1NumberForDisplayForSubscriber(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(129, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().setLine1NumberForDisplayForSubscriber(param2Int, param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getLine1NumberForDisplay(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(130, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2String1 = ITelephony.Stub.getDefaultImpl().getLine1NumberForDisplay(param2Int, param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getLine1AlphaTagForDisplay(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(131, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2String1 = ITelephony.Stub.getDefaultImpl().getLine1AlphaTagForDisplay(param2Int, param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getMergedSubscriberIds(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(132, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getMergedSubscriberIds(param2Int, param2String1, param2String2); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getMergedImsisFromGroup(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(133, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getMergedImsisFromGroup(param2Int, param2String); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setOperatorBrandOverride(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(134, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().setOperatorBrandOverride(param2Int, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setRoamingOverride(int param2Int, List<String> param2List1, List<String> param2List2, List<String> param2List3, List<String> param2List4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          try {
            parcel1.writeInt(param2Int);
            try {
              parcel1.writeStringList(param2List1);
              try {
                parcel1.writeStringList(param2List2);
                try {
                  parcel1.writeStringList(param2List3);
                  try {
                    parcel1.writeStringList(param2List4);
                    try {
                      IBinder iBinder = this.mRemote;
                      boolean bool1 = false, bool2 = iBinder.transact(135, parcel1, parcel2, 0);
                      if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
                        bool1 = ITelephony.Stub.getDefaultImpl().setRoamingOverride(param2Int, param2List1, param2List2, param2List3, param2List4);
                        parcel2.recycle();
                        parcel1.recycle();
                        return bool1;
                      } 
                      parcel2.readException();
                      param2Int = parcel2.readInt();
                      if (param2Int != 0)
                        bool1 = true; 
                      parcel2.recycle();
                      parcel1.recycle();
                      return bool1;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2List1;
      }
      
      public int invokeOemRilRequestRaw(byte[] param2ArrayOfbyte1, byte[] param2ArrayOfbyte2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeByteArray(param2ArrayOfbyte1);
          if (param2ArrayOfbyte2 == null) {
            parcel1.writeInt(-1);
          } else {
            parcel1.writeInt(param2ArrayOfbyte2.length);
          } 
          boolean bool = this.mRemote.transact(136, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().invokeOemRilRequestRaw(param2ArrayOfbyte1, param2ArrayOfbyte2); 
          parcel2.readException();
          int i = parcel2.readInt();
          parcel2.readByteArray(param2ArrayOfbyte2);
          return i;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean needMobileRadioShutdown() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(137, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().needMobileRadioShutdown();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void shutdownMobileRadios() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          boolean bool = this.mRemote.transact(138, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().shutdownMobileRadios();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setRadioCapability(RadioAccessFamily[] param2ArrayOfRadioAccessFamily) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeTypedArray((Parcelable[])param2ArrayOfRadioAccessFamily, 0);
          boolean bool = this.mRemote.transact(139, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().setRadioCapability(param2ArrayOfRadioAccessFamily);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getRadioAccessFamily(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(140, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getRadioAccessFamily(param2Int, param2String);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enableVideoCalling(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(141, parcel1, parcel2, 0);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().enableVideoCalling(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isVideoCallingEnabled(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(142, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isVideoCallingEnabled(param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean canChangeDtmfToneLength(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(143, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().canChangeDtmfToneLength(param2Int, param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isWorldPhone(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(144, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isWorldPhone(param2Int, param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isTtyModeSupported() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(145, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isTtyModeSupported();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isRttSupported(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(146, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isRttSupported(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isHearingAidCompatibilitySupported() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(147, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isHearingAidCompatibilitySupported();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isImsRegistered(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(148, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isImsRegistered(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isWifiCallingAvailable(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(149, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isWifiCallingAvailable(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isVideoTelephonyAvailable(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(150, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isVideoTelephonyAvailable(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getImsRegTechnologyForMmTel(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(151, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getImsRegTechnologyForMmTel(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getDeviceId(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(152, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2String = ITelephony.Stub.getDefaultImpl().getDeviceId(param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getDeviceIdWithFeature(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(153, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2String1 = ITelephony.Stub.getDefaultImpl().getDeviceIdWithFeature(param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getImeiForSlot(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(154, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2String1 = ITelephony.Stub.getDefaultImpl().getImeiForSlot(param2Int, param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getTypeAllocationCodeForSlot(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(155, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getTypeAllocationCodeForSlot(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getMeidForSlot(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(156, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2String1 = ITelephony.Stub.getDefaultImpl().getMeidForSlot(param2Int, param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getManufacturerCodeForSlot(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(157, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getManufacturerCodeForSlot(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getDeviceSoftwareVersionForSlot(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(158, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2String1 = ITelephony.Stub.getDefaultImpl().getDeviceSoftwareVersionForSlot(param2Int, param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getSubIdForPhoneAccount(PhoneAccount param2PhoneAccount) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          if (param2PhoneAccount != null) {
            parcel1.writeInt(1);
            param2PhoneAccount.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(159, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getSubIdForPhoneAccount(param2PhoneAccount); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getSubIdForPhoneAccountHandle(PhoneAccountHandle param2PhoneAccountHandle, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          if (param2PhoneAccountHandle != null) {
            parcel1.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(160, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getSubIdForPhoneAccountHandle(param2PhoneAccountHandle, param2String1, param2String2); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public PhoneAccountHandle getPhoneAccountHandleForSubscriptionId(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          PhoneAccountHandle phoneAccountHandle;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(161, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            phoneAccountHandle = ITelephony.Stub.getDefaultImpl().getPhoneAccountHandleForSubscriptionId(param2Int);
            return phoneAccountHandle;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel(parcel2);
          } else {
            phoneAccountHandle = null;
          } 
          return phoneAccountHandle;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void factoryReset(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(162, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().factoryReset(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getSimLocaleForSubscriber(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(163, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getSimLocaleForSubscriber(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestModemActivityInfo(ResultReceiver param2ResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          if (param2ResultReceiver != null) {
            parcel.writeInt(1);
            param2ResultReceiver.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(164, parcel, null, 1);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().requestModemActivityInfo(param2ResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public ServiceState getServiceStateForSubscriber(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(165, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getServiceStateForSubscriber(param2Int, param2String1, param2String2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ServiceState serviceState = (ServiceState)ServiceState.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (ServiceState)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Uri getVoicemailRingtoneUri(PhoneAccountHandle param2PhoneAccountHandle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          if (param2PhoneAccountHandle != null) {
            parcel1.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(166, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getVoicemailRingtoneUri(param2PhoneAccountHandle); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Uri uri = (Uri)Uri.CREATOR.createFromParcel(parcel2);
          } else {
            param2PhoneAccountHandle = null;
          } 
          return (Uri)param2PhoneAccountHandle;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setVoicemailRingtoneUri(String param2String, PhoneAccountHandle param2PhoneAccountHandle, Uri param2Uri) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String);
          if (param2PhoneAccountHandle != null) {
            parcel1.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(167, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().setVoicemailRingtoneUri(param2String, param2PhoneAccountHandle, param2Uri);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isVoicemailVibrationEnabled(PhoneAccountHandle param2PhoneAccountHandle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          boolean bool1 = true;
          if (param2PhoneAccountHandle != null) {
            parcel1.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(168, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isVoicemailVibrationEnabled(param2PhoneAccountHandle);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setVoicemailVibrationEnabled(String param2String, PhoneAccountHandle param2PhoneAccountHandle, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String);
          boolean bool = true;
          if (param2PhoneAccountHandle != null) {
            parcel1.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(169, parcel1, parcel2, 0);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().setVoicemailVibrationEnabled(param2String, param2PhoneAccountHandle, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getPackagesWithCarrierPrivileges(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(170, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getPackagesWithCarrierPrivileges(param2Int); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getPackagesWithCarrierPrivilegesForAllPhones() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          boolean bool = this.mRemote.transact(171, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getPackagesWithCarrierPrivilegesForAllPhones(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getAidForAppType(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(172, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getAidForAppType(param2Int1, param2Int2); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getEsn(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(173, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getEsn(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getCdmaPrlVersion(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(174, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getCdmaPrlVersion(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<TelephonyHistogram> getTelephonyHistograms() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          boolean bool = this.mRemote.transact(175, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getTelephonyHistograms(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(TelephonyHistogram.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setAllowedCarriers(CarrierRestrictionRules param2CarrierRestrictionRules) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          if (param2CarrierRestrictionRules != null) {
            parcel1.writeInt(1);
            param2CarrierRestrictionRules.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(176, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().setAllowedCarriers(param2CarrierRestrictionRules); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public CarrierRestrictionRules getAllowedCarriers() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          CarrierRestrictionRules carrierRestrictionRules;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          boolean bool = this.mRemote.transact(177, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            carrierRestrictionRules = ITelephony.Stub.getDefaultImpl().getAllowedCarriers();
            return carrierRestrictionRules;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            carrierRestrictionRules = (CarrierRestrictionRules)CarrierRestrictionRules.CREATOR.createFromParcel(parcel2);
          } else {
            carrierRestrictionRules = null;
          } 
          return carrierRestrictionRules;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getSubscriptionCarrierId(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(178, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getSubscriptionCarrierId(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getSubscriptionCarrierName(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(179, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getSubscriptionCarrierName(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getSubscriptionSpecificCarrierId(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(180, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getSubscriptionSpecificCarrierId(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getSubscriptionSpecificCarrierName(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(181, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getSubscriptionSpecificCarrierName(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getCarrierIdFromMccMnc(int param2Int, String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(182, parcel1, parcel2, 0);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getCarrierIdFromMccMnc(param2Int, param2String, param2Boolean);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void carrierActionSetMeteredApnsEnabled(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(183, parcel1, parcel2, 0);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().carrierActionSetMeteredApnsEnabled(param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void carrierActionSetRadioEnabled(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(184, parcel1, parcel2, 0);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().carrierActionSetRadioEnabled(param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void carrierActionReportDefaultNetworkStatus(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(185, parcel1, parcel2, 0);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().carrierActionReportDefaultNetworkStatus(param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void carrierActionResetAll(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(186, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().carrierActionResetAll(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public CallForwardingInfo getCallForwarding(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          CallForwardingInfo callForwardingInfo;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(187, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            callForwardingInfo = ITelephony.Stub.getDefaultImpl().getCallForwarding(param2Int1, param2Int2);
            return callForwardingInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            callForwardingInfo = (CallForwardingInfo)CallForwardingInfo.CREATOR.createFromParcel(parcel2);
          } else {
            callForwardingInfo = null;
          } 
          return callForwardingInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setCallForwarding(int param2Int, CallForwardingInfo param2CallForwardingInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2CallForwardingInfo != null) {
            parcel1.writeInt(1);
            param2CallForwardingInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(188, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().setCallForwarding(param2Int, param2CallForwardingInfo);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getCallWaitingStatus(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(189, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getCallWaitingStatus(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setCallWaitingStatus(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(190, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Boolean = ITelephony.Stub.getDefaultImpl().setCallWaitingStatus(param2Int, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPolicyDataEnabled(boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(191, parcel1, parcel2, 0);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().setPolicyDataEnabled(param2Boolean, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<ClientRequestStats> getClientRequestStats(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(192, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getClientRequestStats(param2String1, param2String2, param2Int); 
          parcel2.readException();
          return parcel2.createTypedArrayList(ClientRequestStats.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSimPowerStateForSlot(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(193, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().setSimPowerStateForSlot(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getForbiddenPlmns(int param2Int1, int param2Int2, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(194, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getForbiddenPlmns(param2Int1, param2Int2, param2String1, param2String2); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setForbiddenPlmns(int param2Int1, int param2Int2, List<String> param2List, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeStringList(param2List);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(195, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int1 = ITelephony.Stub.getDefaultImpl().setForbiddenPlmns(param2Int1, param2Int2, param2List, param2String1, param2String2);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getEmergencyCallbackMode(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(196, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().getEmergencyCallbackMode(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public SignalStrength getSignalStrength(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          SignalStrength signalStrength;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(197, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            signalStrength = ITelephony.Stub.getDefaultImpl().getSignalStrength(param2Int);
            return signalStrength;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            signalStrength = (SignalStrength)SignalStrength.CREATOR.createFromParcel(parcel2);
          } else {
            signalStrength = null;
          } 
          return signalStrength;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getCardIdForDefaultEuicc(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(198, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getCardIdForDefaultEuicc(param2Int, param2String);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<UiccCardInfo> getUiccCardsInfo(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(199, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getUiccCardsInfo(param2String); 
          parcel2.readException();
          return parcel2.createTypedArrayList(UiccCardInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public UiccSlotInfo[] getUiccSlotsInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          boolean bool = this.mRemote.transact(200, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getUiccSlotsInfo(); 
          parcel2.readException();
          return (UiccSlotInfo[])parcel2.createTypedArray(UiccSlotInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean switchSlots(int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeIntArray(param2ArrayOfint);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(201, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().switchSlots(param2ArrayOfint);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isDataRoamingEnabled(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(202, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isDataRoamingEnabled(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDataRoamingEnabled(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(203, parcel1, parcel2, 0);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().setDataRoamingEnabled(param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getCdmaRoamingMode(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(204, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getCdmaRoamingMode(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setCdmaRoamingMode(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(205, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().setCdmaRoamingMode(param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setCdmaSubscriptionMode(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(206, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().setCdmaSubscriptionMode(param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setCarrierTestOverride(int param2Int, String param2String1, String param2String2, String param2String3, String param2String4, String param2String5, String param2String6, String param2String7, String param2String8, String param2String9) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          try {
            parcel1.writeInt(param2Int);
            try {
              parcel1.writeString(param2String1);
              parcel1.writeString(param2String2);
              parcel1.writeString(param2String3);
              parcel1.writeString(param2String4);
              parcel1.writeString(param2String5);
              parcel1.writeString(param2String6);
              parcel1.writeString(param2String7);
              parcel1.writeString(param2String8);
              parcel1.writeString(param2String9);
              boolean bool = this.mRemote.transact(207, parcel1, parcel2, 0);
              if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
                ITelephony.Stub.getDefaultImpl().setCarrierTestOverride(param2Int, param2String1, param2String2, param2String3, param2String4, param2String5, param2String6, param2String7, param2String8, param2String9);
                parcel2.recycle();
                parcel1.recycle();
                return;
              } 
              parcel2.readException();
              parcel2.recycle();
              parcel1.recycle();
              return;
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public int getCarrierIdListVersion(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(208, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getCarrierIdListVersion(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void refreshUiccProfile(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(209, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().refreshUiccProfile(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getNumberOfModemsWithSimultaneousDataConnections(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(210, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getNumberOfModemsWithSimultaneousDataConnections(param2Int, param2String1, param2String2);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getNetworkSelectionMode(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(211, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getNetworkSelectionMode(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isInEmergencySmsMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(212, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isInEmergencySmsMode();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getRadioPowerState(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(213, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getRadioPowerState(param2Int, param2String1, param2String2);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerImsRegistrationCallback(int param2Int, IImsRegistrationCallback param2IImsRegistrationCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          if (param2IImsRegistrationCallback != null) {
            iBinder = param2IImsRegistrationCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(214, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().registerImsRegistrationCallback(param2Int, param2IImsRegistrationCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterImsRegistrationCallback(int param2Int, IImsRegistrationCallback param2IImsRegistrationCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          if (param2IImsRegistrationCallback != null) {
            iBinder = param2IImsRegistrationCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(215, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().unregisterImsRegistrationCallback(param2Int, param2IImsRegistrationCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void getImsMmTelRegistrationState(int param2Int, IIntegerConsumer param2IIntegerConsumer) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          if (param2IIntegerConsumer != null) {
            iBinder = param2IIntegerConsumer.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(216, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().getImsMmTelRegistrationState(param2Int, param2IIntegerConsumer);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void getImsMmTelRegistrationTransportType(int param2Int, IIntegerConsumer param2IIntegerConsumer) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          if (param2IIntegerConsumer != null) {
            iBinder = param2IIntegerConsumer.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(217, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().getImsMmTelRegistrationTransportType(param2Int, param2IIntegerConsumer);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerMmTelCapabilityCallback(int param2Int, IImsCapabilityCallback param2IImsCapabilityCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          if (param2IImsCapabilityCallback != null) {
            iBinder = param2IImsCapabilityCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(218, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().registerMmTelCapabilityCallback(param2Int, param2IImsCapabilityCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterMmTelCapabilityCallback(int param2Int, IImsCapabilityCallback param2IImsCapabilityCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          if (param2IImsCapabilityCallback != null) {
            iBinder = param2IImsCapabilityCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(219, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().unregisterMmTelCapabilityCallback(param2Int, param2IImsCapabilityCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isCapable(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(220, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isCapable(param2Int1, param2Int2, param2Int3);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isAvailable(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(221, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isAvailable(param2Int1, param2Int2, param2Int3);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void isMmTelCapabilitySupported(int param2Int1, IIntegerConsumer param2IIntegerConsumer, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          if (param2IIntegerConsumer != null) {
            iBinder = param2IIntegerConsumer.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(222, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().isMmTelCapabilitySupported(param2Int1, param2IIntegerConsumer, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isAdvancedCallingSettingEnabled(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(223, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isAdvancedCallingSettingEnabled(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAdvancedCallingSettingEnabled(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(224, parcel1, parcel2, 0);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().setAdvancedCallingSettingEnabled(param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isVtSettingEnabled(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(225, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isVtSettingEnabled(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setVtSettingEnabled(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(226, parcel1, parcel2, 0);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().setVtSettingEnabled(param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isVoWiFiSettingEnabled(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(227, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isVoWiFiSettingEnabled(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setVoWiFiSettingEnabled(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(228, parcel1, parcel2, 0);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().setVoWiFiSettingEnabled(param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isVoWiFiRoamingSettingEnabled(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(229, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isVoWiFiRoamingSettingEnabled(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setVoWiFiRoamingSettingEnabled(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(230, parcel1, parcel2, 0);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().setVoWiFiRoamingSettingEnabled(param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setVoWiFiNonPersistent(int param2Int1, boolean param2Boolean, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int2);
          boolean bool1 = this.mRemote.transact(231, parcel1, parcel2, 0);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().setVoWiFiNonPersistent(param2Int1, param2Boolean, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getVoWiFiModeSetting(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(232, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getVoWiFiModeSetting(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setVoWiFiModeSetting(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(233, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().setVoWiFiModeSetting(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getVoWiFiRoamingModeSetting(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(234, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getVoWiFiRoamingModeSetting(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setVoWiFiRoamingModeSetting(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(235, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().setVoWiFiRoamingModeSetting(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setRttCapabilitySetting(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(236, parcel1, parcel2, 0);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().setRttCapabilitySetting(param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isTtyOverVolteEnabled(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(237, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isTtyOverVolteEnabled(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Map getEmergencyNumberList(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(238, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getEmergencyNumberList(param2String1, param2String2); 
          parcel2.readException();
          ClassLoader classLoader = getClass().getClassLoader();
          return parcel2.readHashMap(classLoader);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isEmergencyNumber(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String);
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(239, parcel1, parcel2, 0);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null) {
            param2Boolean = ITelephony.Stub.getDefaultImpl().isEmergencyNumber(param2String, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getCertsFromCarrierPrivilegeAccessRules(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(240, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getCertsFromCarrierPrivilegeAccessRules(param2Int); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerImsProvisioningChangedCallback(int param2Int, IImsConfigCallback param2IImsConfigCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          if (param2IImsConfigCallback != null) {
            iBinder = param2IImsConfigCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(241, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().registerImsProvisioningChangedCallback(param2Int, param2IImsConfigCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterImsProvisioningChangedCallback(int param2Int, IImsConfigCallback param2IImsConfigCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          if (param2IImsConfigCallback != null) {
            iBinder = param2IImsConfigCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(242, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().unregisterImsProvisioningChangedCallback(param2Int, param2IImsConfigCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setImsProvisioningStatusForCapability(int param2Int1, int param2Int2, int param2Int3, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(243, parcel1, parcel2, 0);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().setImsProvisioningStatusForCapability(param2Int1, param2Int2, param2Int3, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getImsProvisioningStatusForCapability(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(244, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().getImsProvisioningStatusForCapability(param2Int1, param2Int2, param2Int3);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getRcsProvisioningStatusForCapability(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(245, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().getRcsProvisioningStatusForCapability(param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setRcsProvisioningStatusForCapability(int param2Int1, int param2Int2, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(246, parcel1, parcel2, 0);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().setRcsProvisioningStatusForCapability(param2Int1, param2Int2, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isMmTelCapabilityProvisionedInCache(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(247, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isMmTelCapabilityProvisionedInCache(param2Int1, param2Int2, param2Int3);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cacheMmTelCapabilityProvisioning(int param2Int1, int param2Int2, int param2Int3, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(248, parcel1, parcel2, 0);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().cacheMmTelCapabilityProvisioning(param2Int1, param2Int2, param2Int3, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getImsProvisioningInt(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(249, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int1 = ITelephony.Stub.getDefaultImpl().getImsProvisioningInt(param2Int1, param2Int2);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getImsProvisioningString(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(250, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getImsProvisioningString(param2Int1, param2Int2); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setImsProvisioningInt(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(251, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int1 = ITelephony.Stub.getDefaultImpl().setImsProvisioningInt(param2Int1, param2Int2, param2Int3);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setImsProvisioningString(int param2Int1, int param2Int2, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(252, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int1 = ITelephony.Stub.getDefaultImpl().setImsProvisioningString(param2Int1, param2Int2, param2String);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateEmergencyNumberListTestMode(int param2Int, EmergencyNumber param2EmergencyNumber) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          if (param2EmergencyNumber != null) {
            parcel1.writeInt(1);
            param2EmergencyNumber.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(253, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().updateEmergencyNumberListTestMode(param2Int, param2EmergencyNumber);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getEmergencyNumberListTestMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          boolean bool = this.mRemote.transact(254, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getEmergencyNumberListTestMode(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getEmergencyNumberDbVersion(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(255, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().getEmergencyNumberDbVersion(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyOtaEmergencyNumberDbInstalled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          boolean bool = this.mRemote.transact(256, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().notifyOtaEmergencyNumberDbInstalled();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateOtaEmergencyNumberDbFilePath(ParcelFileDescriptor param2ParcelFileDescriptor) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          if (param2ParcelFileDescriptor != null) {
            parcel1.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(257, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().updateOtaEmergencyNumberDbFilePath(param2ParcelFileDescriptor);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resetOtaEmergencyNumberDbFilePath() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          boolean bool = this.mRemote.transact(258, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().resetOtaEmergencyNumberDbFilePath();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean enableModemForSlot(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(259, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Boolean = ITelephony.Stub.getDefaultImpl().enableModemForSlot(param2Int, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setMultiSimCarrierRestriction(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(260, parcel1, parcel2, 0);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().setMultiSimCarrierRestriction(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int isMultiSimSupported(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(261, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().isMultiSimSupported(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void switchMultiSimConfig(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(262, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().switchMultiSimConfig(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean doesSwitchMultiSimConfigTriggerReboot(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(263, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().doesSwitchMultiSimConfigTriggerReboot(param2Int, param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] getSlotsMapping() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          boolean bool = this.mRemote.transact(264, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getSlotsMapping(); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getRadioHalVersion() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          boolean bool = this.mRemote.transact(265, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getRadioHalVersion(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getCurrentPackageName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          boolean bool = this.mRemote.transact(266, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getCurrentPackageName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isApplicationOnUicc(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(267, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isApplicationOnUicc(param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isModemEnabledForSlot(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(268, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isModemEnabledForSlot(param2Int, param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isDataEnabledForApn(int param2Int1, int param2Int2, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(269, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isDataEnabledForApn(param2Int1, param2Int2, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isApnMetered(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(270, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isApnMetered(param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSystemSelectionChannels(List<RadioAccessSpecifier> param2List, int param2Int, IBooleanConsumer param2IBooleanConsumer) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel.writeTypedList(param2List);
          parcel.writeInt(param2Int);
          if (param2IBooleanConsumer != null) {
            iBinder = param2IBooleanConsumer.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(271, parcel, null, 1);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().setSystemSelectionChannels(param2List, param2Int, param2IBooleanConsumer);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public boolean isMvnoMatched(int param2Int1, int param2Int2, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(272, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isMvnoMatched(param2Int1, param2Int2, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enqueueSmsPickResult(String param2String1, String param2String2, IIntegerConsumer param2IIntegerConsumer) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          if (param2IIntegerConsumer != null) {
            iBinder = param2IIntegerConsumer.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(273, parcel, null, 1);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().enqueueSmsPickResult(param2String1, param2String2, param2IIntegerConsumer);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public String getMmsUserAgent(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(274, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getMmsUserAgent(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getMmsUAProfUrl(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(275, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getMmsUAProfUrl(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setDataAllowedDuringVoiceCall(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(276, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Boolean = ITelephony.Stub.getDefaultImpl().setDataAllowedDuringVoiceCall(param2Int, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isDataAllowedInVoiceCall(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(277, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isDataAllowedInVoiceCall(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setAlwaysAllowMmsData(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(278, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Boolean = ITelephony.Stub.getDefaultImpl().setAlwaysAllowMmsData(param2Int, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setCepEnabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(279, parcel, null, 1);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().setCepEnabled(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyRcsAutoConfigurationReceived(int param2Int, byte[] param2ArrayOfbyte, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeByteArray(param2ArrayOfbyte);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(280, parcel1, parcel2, 0);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().notifyRcsAutoConfigurationReceived(param2Int, param2ArrayOfbyte, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isIccLockEnabled(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(281, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().isIccLockEnabled(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setIccLockEnabled(int param2Int, boolean param2Boolean, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeString(param2String);
          boolean bool1 = this.mRemote.transact(282, parcel1, parcel2, 0);
          if (!bool1 && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().setIccLockEnabled(param2Int, param2Boolean, param2String);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int changeIccLockPassword(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(283, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            param2Int = ITelephony.Stub.getDefaultImpl().changeIccLockPassword(param2Int, param2String1, param2String2);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestUserActivityNotification() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          boolean bool = this.mRemote.transact(284, parcel, null, 1);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().requestUserActivityNotification();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void userActivity() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          boolean bool = this.mRemote.transact(285, parcel, null, 1);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null) {
            ITelephony.Stub.getDefaultImpl().userActivity();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public String getManualNetworkSelectionPlmn(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(286, parcel1, parcel2, 0);
          if (!bool && ITelephony.Stub.getDefaultImpl() != null)
            return ITelephony.Stub.getDefaultImpl().getManualNetworkSelectionPlmn(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean canConnectTo5GInDsdsMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.ITelephony");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(287, parcel1, parcel2, 0);
          if (!bool2 && ITelephony.Stub.getDefaultImpl() != null) {
            bool1 = ITelephony.Stub.getDefaultImpl().canConnectTo5GInDsdsMode();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    private boolean onTransact$sendVisualVoicemailSmsForSubscriber$(Parcel param1Parcel1, Parcel param1Parcel2) throws RemoteException {
      param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephony");
      String str1 = param1Parcel1.readString();
      String str2 = param1Parcel1.readString();
      int i = param1Parcel1.readInt();
      String str3 = param1Parcel1.readString();
      int j = param1Parcel1.readInt();
      String str4 = param1Parcel1.readString();
      if (param1Parcel1.readInt() != 0) {
        PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      sendVisualVoicemailSmsForSubscriber(str1, str2, i, str3, j, str4, (PendingIntent)param1Parcel1);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private boolean onTransact$requestCellInfoUpdateWithWorkSource$(Parcel param1Parcel1, Parcel param1Parcel2) throws RemoteException {
      param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephony");
      int i = param1Parcel1.readInt();
      ICellInfoCallback iCellInfoCallback = ICellInfoCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
      String str1 = param1Parcel1.readString();
      String str2 = param1Parcel1.readString();
      if (param1Parcel1.readInt() != 0) {
        WorkSource workSource = (WorkSource)WorkSource.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      requestCellInfoUpdateWithWorkSource(i, iCellInfoCallback, str1, str2, (WorkSource)param1Parcel1);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private boolean onTransact$iccTransmitApduLogicalChannelBySlot$(Parcel param1Parcel1, Parcel param1Parcel2) throws RemoteException {
      param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephony");
      int i = param1Parcel1.readInt();
      int j = param1Parcel1.readInt();
      int k = param1Parcel1.readInt();
      int m = param1Parcel1.readInt();
      int n = param1Parcel1.readInt();
      int i1 = param1Parcel1.readInt();
      int i2 = param1Parcel1.readInt();
      String str = param1Parcel1.readString();
      str = iccTransmitApduLogicalChannelBySlot(i, j, k, m, n, i1, i2, str);
      param1Parcel2.writeNoException();
      param1Parcel2.writeString(str);
      return true;
    }
    
    private boolean onTransact$iccTransmitApduLogicalChannel$(Parcel param1Parcel1, Parcel param1Parcel2) throws RemoteException {
      param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephony");
      int i = param1Parcel1.readInt();
      int j = param1Parcel1.readInt();
      int k = param1Parcel1.readInt();
      int m = param1Parcel1.readInt();
      int n = param1Parcel1.readInt();
      int i1 = param1Parcel1.readInt();
      int i2 = param1Parcel1.readInt();
      String str = param1Parcel1.readString();
      str = iccTransmitApduLogicalChannel(i, j, k, m, n, i1, i2, str);
      param1Parcel2.writeNoException();
      param1Parcel2.writeString(str);
      return true;
    }
    
    private boolean onTransact$iccTransmitApduBasicChannelBySlot$(Parcel param1Parcel1, Parcel param1Parcel2) throws RemoteException {
      param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephony");
      int i = param1Parcel1.readInt();
      String str2 = param1Parcel1.readString();
      int j = param1Parcel1.readInt();
      int k = param1Parcel1.readInt();
      int m = param1Parcel1.readInt();
      int n = param1Parcel1.readInt();
      int i1 = param1Parcel1.readInt();
      String str1 = param1Parcel1.readString();
      str1 = iccTransmitApduBasicChannelBySlot(i, str2, j, k, m, n, i1, str1);
      param1Parcel2.writeNoException();
      param1Parcel2.writeString(str1);
      return true;
    }
    
    private boolean onTransact$iccTransmitApduBasicChannel$(Parcel param1Parcel1, Parcel param1Parcel2) throws RemoteException {
      param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephony");
      int i = param1Parcel1.readInt();
      String str2 = param1Parcel1.readString();
      int j = param1Parcel1.readInt();
      int k = param1Parcel1.readInt();
      int m = param1Parcel1.readInt();
      int n = param1Parcel1.readInt();
      int i1 = param1Parcel1.readInt();
      String str1 = param1Parcel1.readString();
      str1 = iccTransmitApduBasicChannel(i, str2, j, k, m, n, i1, str1);
      param1Parcel2.writeNoException();
      param1Parcel2.writeString(str1);
      return true;
    }
    
    private boolean onTransact$iccExchangeSimIO$(Parcel param1Parcel1, Parcel param1Parcel2) throws RemoteException {
      param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephony");
      int i = param1Parcel1.readInt();
      int j = param1Parcel1.readInt();
      int k = param1Parcel1.readInt();
      int m = param1Parcel1.readInt();
      int n = param1Parcel1.readInt();
      int i1 = param1Parcel1.readInt();
      String str = param1Parcel1.readString();
      byte[] arrayOfByte = iccExchangeSimIO(i, j, k, m, n, i1, str);
      param1Parcel2.writeNoException();
      param1Parcel2.writeByteArray(arrayOfByte);
      return true;
    }
    
    private boolean onTransact$requestNetworkScan$(Parcel param1Parcel1, Parcel param1Parcel2) throws RemoteException {
      NetworkScanRequest networkScanRequest;
      Messenger messenger;
      param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephony");
      int i = param1Parcel1.readInt();
      if (param1Parcel1.readInt() != 0) {
        networkScanRequest = (NetworkScanRequest)NetworkScanRequest.CREATOR.createFromParcel(param1Parcel1);
      } else {
        networkScanRequest = null;
      } 
      if (param1Parcel1.readInt() != 0) {
        messenger = (Messenger)Messenger.CREATOR.createFromParcel(param1Parcel1);
      } else {
        messenger = null;
      } 
      IBinder iBinder = param1Parcel1.readStrongBinder();
      String str2 = param1Parcel1.readString();
      String str1 = param1Parcel1.readString();
      i = requestNetworkScan(i, networkScanRequest, messenger, iBinder, str2, str1);
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(i);
      return true;
    }
    
    private boolean onTransact$setRoamingOverride$(Parcel param1Parcel1, Parcel param1Parcel2) throws RemoteException {
      param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephony");
      int i = param1Parcel1.readInt();
      ArrayList<String> arrayList2 = param1Parcel1.createStringArrayList();
      ArrayList<String> arrayList3 = param1Parcel1.createStringArrayList();
      ArrayList<String> arrayList4 = param1Parcel1.createStringArrayList();
      ArrayList<String> arrayList1 = param1Parcel1.createStringArrayList();
      boolean bool = setRoamingOverride(i, arrayList2, arrayList3, arrayList4, arrayList1);
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(bool);
      return true;
    }
    
    private boolean onTransact$setForbiddenPlmns$(Parcel param1Parcel1, Parcel param1Parcel2) throws RemoteException {
      param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephony");
      int i = param1Parcel1.readInt();
      int j = param1Parcel1.readInt();
      ArrayList<String> arrayList = param1Parcel1.createStringArrayList();
      String str2 = param1Parcel1.readString();
      String str1 = param1Parcel1.readString();
      i = setForbiddenPlmns(i, j, arrayList, str2, str1);
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(i);
      return true;
    }
    
    private boolean onTransact$setCarrierTestOverride$(Parcel param1Parcel1, Parcel param1Parcel2) throws RemoteException {
      param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephony");
      int i = param1Parcel1.readInt();
      String str2 = param1Parcel1.readString();
      String str3 = param1Parcel1.readString();
      String str4 = param1Parcel1.readString();
      String str5 = param1Parcel1.readString();
      String str6 = param1Parcel1.readString();
      String str7 = param1Parcel1.readString();
      String str8 = param1Parcel1.readString();
      String str9 = param1Parcel1.readString();
      String str1 = param1Parcel1.readString();
      setCarrierTestOverride(i, str2, str3, str4, str5, str6, str7, str8, str9, str1);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private boolean onTransact$cacheMmTelCapabilityProvisioning$(Parcel param1Parcel1, Parcel param1Parcel2) throws RemoteException {
      boolean bool;
      param1Parcel1.enforceInterface("com.android.internal.telephony.ITelephony");
      int i = param1Parcel1.readInt();
      int j = param1Parcel1.readInt();
      int k = param1Parcel1.readInt();
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      cacheMmTelCapabilityProvisioning(i, j, k, bool);
      param1Parcel2.writeNoException();
      return true;
    }
    
    public static boolean setDefaultImpl(ITelephony param1ITelephony) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITelephony != null) {
          Proxy.sDefaultImpl = param1ITelephony;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITelephony getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
