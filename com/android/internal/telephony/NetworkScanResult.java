package com.android.internal.telephony;

import android.os.Parcel;
import android.os.Parcelable;
import android.telephony.CellInfo;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public final class NetworkScanResult implements Parcelable {
  public NetworkScanResult(int paramInt1, int paramInt2, List<CellInfo> paramList) {
    this.scanStatus = paramInt1;
    this.scanError = paramInt2;
    this.networkInfos = paramList;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.scanStatus);
    paramParcel.writeInt(this.scanError);
    paramParcel.writeParcelableList(this.networkInfos, paramInt);
  }
  
  private NetworkScanResult(Parcel paramParcel) {
    this.scanStatus = paramParcel.readInt();
    this.scanError = paramParcel.readInt();
    ArrayList<CellInfo> arrayList = new ArrayList();
    paramParcel.readParcelableList(arrayList, Object.class.getClassLoader());
    this.networkInfos = arrayList;
  }
  
  public boolean equals(Object<CellInfo> paramObject) {
    boolean bool = false;
    try {
      NetworkScanResult networkScanResult = (NetworkScanResult)paramObject;
      if (paramObject == null)
        return false; 
      if (this.scanStatus == networkScanResult.scanStatus && this.scanError == networkScanResult.scanError) {
        paramObject = (Object<CellInfo>)this.networkInfos;
        List<CellInfo> list = networkScanResult.networkInfos;
        if (paramObject.equals(list))
          bool = true; 
      } 
      return bool;
    } catch (ClassCastException classCastException) {
      return false;
    } 
  }
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("{");
    StringBuilder stringBuilder4 = new StringBuilder();
    stringBuilder4.append("scanStatus=");
    stringBuilder4.append(this.scanStatus);
    String str3 = stringBuilder4.toString();
    stringBuilder1.append(str3);
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append(", scanError=");
    stringBuilder3.append(this.scanError);
    String str2 = stringBuilder3.toString();
    stringBuilder1.append(str2);
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", networkInfos=");
    stringBuilder2.append(this.networkInfos);
    String str1 = stringBuilder2.toString();
    stringBuilder1.append(str1);
    stringBuilder1.append("}");
    return stringBuilder1.toString();
  }
  
  public int hashCode() {
    int i = this.scanStatus, j = this.scanError;
    List<CellInfo> list = this.networkInfos;
    int k = Objects.hashCode(list);
    return i * 31 + j * 23 + k * 37;
  }
  
  public static final Parcelable.Creator<NetworkScanResult> CREATOR = new Parcelable.Creator<NetworkScanResult>() {
      public NetworkScanResult createFromParcel(Parcel param1Parcel) {
        return new NetworkScanResult(param1Parcel);
      }
      
      public NetworkScanResult[] newArray(int param1Int) {
        return new NetworkScanResult[param1Int];
      }
    };
  
  public static final int SCAN_STATUS_COMPLETE = 2;
  
  public static final int SCAN_STATUS_PARTIAL = 1;
  
  public List<CellInfo> networkInfos;
  
  public int scanError;
  
  public int scanStatus;
}
