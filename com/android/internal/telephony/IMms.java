package com.android.internal.telephony;

import android.app.PendingIntent;
import android.content.ContentValues;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IMms extends IInterface {
  Uri addMultimediaMessageDraft(String paramString, Uri paramUri) throws RemoteException;
  
  Uri addTextMessageDraft(String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  boolean archiveStoredConversation(String paramString, long paramLong, boolean paramBoolean) throws RemoteException;
  
  boolean deleteStoredConversation(String paramString, long paramLong) throws RemoteException;
  
  boolean deleteStoredMessage(String paramString, Uri paramUri) throws RemoteException;
  
  void downloadMessage(int paramInt, String paramString1, String paramString2, Uri paramUri, Bundle paramBundle, PendingIntent paramPendingIntent, long paramLong) throws RemoteException;
  
  boolean getAutoPersisting() throws RemoteException;
  
  Uri importMultimediaMessage(String paramString1, Uri paramUri, String paramString2, long paramLong, boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  Uri importTextMessage(String paramString1, String paramString2, int paramInt, String paramString3, long paramLong, boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  void sendMessage(int paramInt, String paramString1, Uri paramUri, String paramString2, Bundle paramBundle, PendingIntent paramPendingIntent, long paramLong) throws RemoteException;
  
  void sendStoredMessage(int paramInt, String paramString, Uri paramUri, Bundle paramBundle, PendingIntent paramPendingIntent) throws RemoteException;
  
  void setAutoPersisting(String paramString, boolean paramBoolean) throws RemoteException;
  
  boolean updateStoredMessageStatus(String paramString, Uri paramUri, ContentValues paramContentValues) throws RemoteException;
  
  class Default implements IMms {
    public void sendMessage(int param1Int, String param1String1, Uri param1Uri, String param1String2, Bundle param1Bundle, PendingIntent param1PendingIntent, long param1Long) throws RemoteException {}
    
    public void downloadMessage(int param1Int, String param1String1, String param1String2, Uri param1Uri, Bundle param1Bundle, PendingIntent param1PendingIntent, long param1Long) throws RemoteException {}
    
    public Uri importTextMessage(String param1String1, String param1String2, int param1Int, String param1String3, long param1Long, boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {
      return null;
    }
    
    public Uri importMultimediaMessage(String param1String1, Uri param1Uri, String param1String2, long param1Long, boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {
      return null;
    }
    
    public boolean deleteStoredMessage(String param1String, Uri param1Uri) throws RemoteException {
      return false;
    }
    
    public boolean deleteStoredConversation(String param1String, long param1Long) throws RemoteException {
      return false;
    }
    
    public boolean updateStoredMessageStatus(String param1String, Uri param1Uri, ContentValues param1ContentValues) throws RemoteException {
      return false;
    }
    
    public boolean archiveStoredConversation(String param1String, long param1Long, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public Uri addTextMessageDraft(String param1String1, String param1String2, String param1String3) throws RemoteException {
      return null;
    }
    
    public Uri addMultimediaMessageDraft(String param1String, Uri param1Uri) throws RemoteException {
      return null;
    }
    
    public void sendStoredMessage(int param1Int, String param1String, Uri param1Uri, Bundle param1Bundle, PendingIntent param1PendingIntent) throws RemoteException {}
    
    public void setAutoPersisting(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public boolean getAutoPersisting() throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMms {
    private static final String DESCRIPTOR = "com.android.internal.telephony.IMms";
    
    static final int TRANSACTION_addMultimediaMessageDraft = 10;
    
    static final int TRANSACTION_addTextMessageDraft = 9;
    
    static final int TRANSACTION_archiveStoredConversation = 8;
    
    static final int TRANSACTION_deleteStoredConversation = 6;
    
    static final int TRANSACTION_deleteStoredMessage = 5;
    
    static final int TRANSACTION_downloadMessage = 2;
    
    static final int TRANSACTION_getAutoPersisting = 13;
    
    static final int TRANSACTION_importMultimediaMessage = 4;
    
    static final int TRANSACTION_importTextMessage = 3;
    
    static final int TRANSACTION_sendMessage = 1;
    
    static final int TRANSACTION_sendStoredMessage = 11;
    
    static final int TRANSACTION_setAutoPersisting = 12;
    
    static final int TRANSACTION_updateStoredMessageStatus = 7;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telephony.IMms");
    }
    
    public static IMms asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telephony.IMms");
      if (iInterface != null && iInterface instanceof IMms)
        return (IMms)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 13:
          return "getAutoPersisting";
        case 12:
          return "setAutoPersisting";
        case 11:
          return "sendStoredMessage";
        case 10:
          return "addMultimediaMessageDraft";
        case 9:
          return "addTextMessageDraft";
        case 8:
          return "archiveStoredConversation";
        case 7:
          return "updateStoredMessageStatus";
        case 6:
          return "deleteStoredConversation";
        case 5:
          return "deleteStoredMessage";
        case 4:
          return "importMultimediaMessage";
        case 3:
          return "importTextMessage";
        case 2:
          return "downloadMessage";
        case 1:
          break;
      } 
      return "sendMessage";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool2;
        int j;
        boolean bool1;
        Uri uri2;
        String str1;
        Uri uri1;
        String str2, str3;
        Bundle bundle;
        String str4;
        boolean bool3 = false, bool4 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 13:
            param1Parcel1.enforceInterface("com.android.internal.telephony.IMms");
            bool2 = getAutoPersisting();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 12:
            param1Parcel1.enforceInterface("com.android.internal.telephony.IMms");
            str2 = param1Parcel1.readString();
            bool3 = bool4;
            if (param1Parcel1.readInt() != 0)
              bool3 = true; 
            setAutoPersisting(str2, bool3);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            param1Parcel1.enforceInterface("com.android.internal.telephony.IMms");
            j = param1Parcel1.readInt();
            str3 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str2 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              bundle = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            sendStoredMessage(j, str3, (Uri)str2, bundle, (PendingIntent)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            param1Parcel1.enforceInterface("com.android.internal.telephony.IMms");
            str2 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            uri2 = addMultimediaMessageDraft(str2, (Uri)param1Parcel1);
            param1Parcel2.writeNoException();
            if (uri2 != null) {
              param1Parcel2.writeInt(1);
              uri2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 9:
            uri2.enforceInterface("com.android.internal.telephony.IMms");
            str2 = uri2.readString();
            str4 = uri2.readString();
            str1 = uri2.readString();
            uri1 = addTextMessageDraft(str2, str4, str1);
            param1Parcel2.writeNoException();
            if (uri1 != null) {
              param1Parcel2.writeInt(1);
              uri1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 8:
            uri1.enforceInterface("com.android.internal.telephony.IMms");
            str2 = uri1.readString();
            l = uri1.readLong();
            if (uri1.readInt() != 0)
              bool3 = true; 
            bool1 = archiveStoredConversation(str2, l, bool3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 7:
            uri1.enforceInterface("com.android.internal.telephony.IMms");
            str4 = uri1.readString();
            if (uri1.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel((Parcel)uri1);
            } else {
              str2 = null;
            } 
            if (uri1.readInt() != 0) {
              ContentValues contentValues = (ContentValues)ContentValues.CREATOR.createFromParcel((Parcel)uri1);
            } else {
              uri1 = null;
            } 
            bool1 = updateStoredMessageStatus(str4, (Uri)str2, (ContentValues)uri1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 6:
            uri1.enforceInterface("com.android.internal.telephony.IMms");
            str2 = uri1.readString();
            l = uri1.readLong();
            bool1 = deleteStoredConversation(str2, l);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 5:
            uri1.enforceInterface("com.android.internal.telephony.IMms");
            str2 = uri1.readString();
            if (uri1.readInt() != 0) {
              uri1 = (Uri)Uri.CREATOR.createFromParcel((Parcel)uri1);
            } else {
              uri1 = null;
            } 
            bool1 = deleteStoredMessage(str2, uri1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 4:
            uri1.enforceInterface("com.android.internal.telephony.IMms");
            str4 = uri1.readString();
            if (uri1.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel((Parcel)uri1);
            } else {
              str2 = null;
            } 
            str3 = uri1.readString();
            l = uri1.readLong();
            if (uri1.readInt() != 0) {
              bool3 = true;
            } else {
              bool3 = false;
            } 
            if (uri1.readInt() != 0) {
              bool4 = true;
            } else {
              bool4 = false;
            } 
            uri1 = importMultimediaMessage(str4, (Uri)str2, str3, l, bool3, bool4);
            param1Parcel2.writeNoException();
            if (uri1 != null) {
              param1Parcel2.writeInt(1);
              uri1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 3:
            uri1.enforceInterface("com.android.internal.telephony.IMms");
            str4 = uri1.readString();
            str3 = uri1.readString();
            i = uri1.readInt();
            str2 = uri1.readString();
            l = uri1.readLong();
            if (uri1.readInt() != 0) {
              bool3 = true;
            } else {
              bool3 = false;
            } 
            if (uri1.readInt() != 0) {
              bool4 = true;
            } else {
              bool4 = false;
            } 
            uri1 = importTextMessage(str4, str3, i, str2, l, bool3, bool4);
            param1Parcel2.writeNoException();
            if (uri1 != null) {
              param1Parcel2.writeInt(1);
              uri1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 2:
            uri1.enforceInterface("com.android.internal.telephony.IMms");
            i = uri1.readInt();
            str5 = uri1.readString();
            str6 = uri1.readString();
            if (uri1.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel((Parcel)uri1);
            } else {
              str2 = null;
            } 
            if (uri1.readInt() != 0) {
              Bundle bundle1 = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)uri1);
            } else {
              str4 = null;
            } 
            if (uri1.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)uri1);
            } else {
              str3 = null;
            } 
            l = uri1.readLong();
            downloadMessage(i, str5, str6, (Uri)str2, (Bundle)str4, (PendingIntent)str3, l);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        uri1.enforceInterface("com.android.internal.telephony.IMms");
        int i = uri1.readInt();
        String str5 = uri1.readString();
        if (uri1.readInt() != 0) {
          Uri uri = (Uri)Uri.CREATOR.createFromParcel((Parcel)uri1);
        } else {
          str2 = null;
        } 
        String str6 = uri1.readString();
        if (uri1.readInt() != 0) {
          Bundle bundle1 = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)uri1);
        } else {
          str4 = null;
        } 
        if (uri1.readInt() != 0) {
          PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)uri1);
        } else {
          str3 = null;
        } 
        long l = uri1.readLong();
        sendMessage(i, str5, (Uri)str2, str6, (Bundle)str4, (PendingIntent)str3, l);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("com.android.internal.telephony.IMms");
      return true;
    }
    
    private static class Proxy implements IMms {
      public static IMms sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telephony.IMms";
      }
      
      public void sendMessage(int param2Int, String param2String1, Uri param2Uri, String param2String2, Bundle param2Bundle, PendingIntent param2PendingIntent, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IMms");
          try {
            parcel1.writeInt(param2Int);
            parcel1.writeString(param2String1);
            if (param2Uri != null) {
              parcel1.writeInt(1);
              param2Uri.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            parcel1.writeString(param2String2);
            if (param2Bundle != null) {
              parcel1.writeInt(1);
              param2Bundle.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            if (param2PendingIntent != null) {
              parcel1.writeInt(1);
              param2PendingIntent.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            parcel1.writeLong(param2Long);
            boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
            if (!bool && IMms.Stub.getDefaultImpl() != null) {
              IMms.Stub.getDefaultImpl().sendMessage(param2Int, param2String1, param2Uri, param2String2, param2Bundle, param2PendingIntent, param2Long);
              parcel2.recycle();
              parcel1.recycle();
              return;
            } 
            parcel2.readException();
            parcel2.recycle();
            parcel1.recycle();
            return;
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void downloadMessage(int param2Int, String param2String1, String param2String2, Uri param2Uri, Bundle param2Bundle, PendingIntent param2PendingIntent, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IMms");
          try {
            parcel1.writeInt(param2Int);
            parcel1.writeString(param2String1);
            parcel1.writeString(param2String2);
            if (param2Uri != null) {
              parcel1.writeInt(1);
              param2Uri.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            if (param2Bundle != null) {
              parcel1.writeInt(1);
              param2Bundle.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            if (param2PendingIntent != null) {
              parcel1.writeInt(1);
              param2PendingIntent.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            parcel1.writeLong(param2Long);
            boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
            if (!bool && IMms.Stub.getDefaultImpl() != null) {
              IMms.Stub.getDefaultImpl().downloadMessage(param2Int, param2String1, param2String2, param2Uri, param2Bundle, param2PendingIntent, param2Long);
              parcel2.recycle();
              parcel1.recycle();
              return;
            } 
            parcel2.readException();
            parcel2.recycle();
            parcel1.recycle();
            return;
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public Uri importTextMessage(String param2String1, String param2String2, int param2Int, String param2String3, long param2Long, boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IMms");
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeString(param2String2);
              try {
                parcel1.writeInt(param2Int);
                try {
                  boolean bool2;
                  parcel1.writeString(param2String3);
                  parcel1.writeLong(param2Long);
                  boolean bool1 = true;
                  if (param2Boolean1) {
                    bool2 = true;
                  } else {
                    bool2 = false;
                  } 
                  parcel1.writeInt(bool2);
                  if (param2Boolean2) {
                    bool2 = bool1;
                  } else {
                    bool2 = false;
                  } 
                  parcel1.writeInt(bool2);
                  boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
                  if (!bool && IMms.Stub.getDefaultImpl() != null) {
                    Uri uri = IMms.Stub.getDefaultImpl().importTextMessage(param2String1, param2String2, param2Int, param2String3, param2Long, param2Boolean1, param2Boolean2);
                    parcel2.recycle();
                    parcel1.recycle();
                    return uri;
                  } 
                  parcel2.readException();
                  if (parcel2.readInt() != 0) {
                    Uri uri = (Uri)Uri.CREATOR.createFromParcel(parcel2);
                  } else {
                    param2String1 = null;
                  } 
                  parcel2.recycle();
                  parcel1.recycle();
                  return (Uri)param2String1;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public Uri importMultimediaMessage(String param2String1, Uri param2Uri, String param2String2, long param2Long, boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IMms");
          try {
            parcel1.writeString(param2String1);
            boolean bool = true;
            if (param2Uri != null) {
              parcel1.writeInt(1);
              param2Uri.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            try {
              parcel1.writeString(param2String2);
              try {
                boolean bool1;
                parcel1.writeLong(param2Long);
                if (param2Boolean1) {
                  bool1 = true;
                } else {
                  bool1 = false;
                } 
                parcel1.writeInt(bool1);
                if (param2Boolean2) {
                  bool1 = bool;
                } else {
                  bool1 = false;
                } 
                parcel1.writeInt(bool1);
                boolean bool2 = this.mRemote.transact(4, parcel1, parcel2, 0);
                if (!bool2 && IMms.Stub.getDefaultImpl() != null) {
                  Uri uri = IMms.Stub.getDefaultImpl().importMultimediaMessage(param2String1, param2Uri, param2String2, param2Long, param2Boolean1, param2Boolean2);
                  parcel2.recycle();
                  parcel1.recycle();
                  return uri;
                } 
                parcel2.readException();
                if (parcel2.readInt() != 0) {
                  Uri uri = (Uri)Uri.CREATOR.createFromParcel(parcel2);
                } else {
                  param2String1 = null;
                } 
                parcel2.recycle();
                parcel1.recycle();
                return (Uri)param2String1;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public boolean deleteStoredMessage(String param2String, Uri param2Uri) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IMms");
          parcel1.writeString(param2String);
          boolean bool1 = true;
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IMms.Stub.getDefaultImpl() != null) {
            bool1 = IMms.Stub.getDefaultImpl().deleteStoredMessage(param2String, param2Uri);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean deleteStoredConversation(String param2String, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IMms");
          parcel1.writeString(param2String);
          parcel1.writeLong(param2Long);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IMms.Stub.getDefaultImpl() != null) {
            bool1 = IMms.Stub.getDefaultImpl().deleteStoredConversation(param2String, param2Long);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean updateStoredMessageStatus(String param2String, Uri param2Uri, ContentValues param2ContentValues) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IMms");
          parcel1.writeString(param2String);
          boolean bool1 = true;
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2ContentValues != null) {
            parcel1.writeInt(1);
            param2ContentValues.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool2 && IMms.Stub.getDefaultImpl() != null) {
            bool1 = IMms.Stub.getDefaultImpl().updateStoredMessageStatus(param2String, param2Uri, param2ContentValues);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean archiveStoredConversation(String param2String, long param2Long, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IMms");
          parcel1.writeString(param2String);
          parcel1.writeLong(param2Long);
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool1 && IMms.Stub.getDefaultImpl() != null) {
            param2Boolean = IMms.Stub.getDefaultImpl().archiveStoredConversation(param2String, param2Long, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Uri addTextMessageDraft(String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IMms");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IMms.Stub.getDefaultImpl() != null)
            return IMms.Stub.getDefaultImpl().addTextMessageDraft(param2String1, param2String2, param2String3); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Uri uri = (Uri)Uri.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (Uri)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Uri addMultimediaMessageDraft(String param2String, Uri param2Uri) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IMms");
          parcel1.writeString(param2String);
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IMms.Stub.getDefaultImpl() != null)
            return IMms.Stub.getDefaultImpl().addMultimediaMessageDraft(param2String, param2Uri); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Uri uri = (Uri)Uri.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (Uri)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendStoredMessage(int param2Int, String param2String, Uri param2Uri, Bundle param2Bundle, PendingIntent param2PendingIntent) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IMms");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2PendingIntent != null) {
            parcel1.writeInt(1);
            param2PendingIntent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IMms.Stub.getDefaultImpl() != null) {
            IMms.Stub.getDefaultImpl().sendStoredMessage(param2Int, param2String, param2Uri, param2Bundle, param2PendingIntent);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAutoPersisting(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telephony.IMms");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool1 && IMms.Stub.getDefaultImpl() != null) {
            IMms.Stub.getDefaultImpl().setAutoPersisting(param2String, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getAutoPersisting() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telephony.IMms");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(13, parcel1, parcel2, 0);
          if (!bool2 && IMms.Stub.getDefaultImpl() != null) {
            bool1 = IMms.Stub.getDefaultImpl().getAutoPersisting();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMms param1IMms) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMms != null) {
          Proxy.sDefaultImpl = param1IMms;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMms getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
