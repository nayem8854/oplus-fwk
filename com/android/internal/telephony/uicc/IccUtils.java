package com.android.internal.telephony.uicc;

import android.content.res.Resources;
import android.graphics.Bitmap;
import com.android.internal.telephony.EncodeException;
import com.android.internal.telephony.GsmAlphabet;
import com.android.telephony.Rlog;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.List;

public class IccUtils {
  static final int FPLMN_BYTE_SIZE = 3;
  
  private static final char[] HEX_CHARS = new char[] { 
      '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
      'A', 'B', 'C', 'D', 'E', 'F' };
  
  static final String LOG_TAG = "IccUtils";
  
  public static String bcdToString(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    StringBuilder stringBuilder = new StringBuilder(paramInt2 * 2);
    for (int i = paramInt1; i < paramInt1 + paramInt2; i++) {
      int j = paramArrayOfbyte[i] & 0xF;
      if (j > 9)
        break; 
      stringBuilder.append((char)(j + 48));
      j = paramArrayOfbyte[i] >> 4 & 0xF;
      if (j != 15) {
        if (j > 9)
          break; 
        stringBuilder.append((char)(j + 48));
      } 
    } 
    return stringBuilder.toString();
  }
  
  public static String bcdToString(byte[] paramArrayOfbyte) {
    return bcdToString(paramArrayOfbyte, 0, paramArrayOfbyte.length);
  }
  
  public static byte[] bcdToBytes(String paramString) {
    byte[] arrayOfByte = new byte[(paramString.length() + 1) / 2];
    bcdToBytes(paramString, arrayOfByte);
    return arrayOfByte;
  }
  
  public static void bcdToBytes(String paramString, byte[] paramArrayOfbyte) {
    bcdToBytes(paramString, paramArrayOfbyte, 0);
  }
  
  public static void bcdToBytes(String paramString, byte[] paramArrayOfbyte, int paramInt) {
    String str = paramString;
    if (paramString.length() % 2 != 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append("0");
      str = stringBuilder.toString();
    } 
    int i = Math.min((paramArrayOfbyte.length - paramInt) * 2, str.length());
    for (byte b = 0; b + 1 < i; b += 2, paramInt++)
      paramArrayOfbyte[paramInt] = (byte)(charToByte(str.charAt(b + 1)) << 4 | charToByte(str.charAt(b))); 
  }
  
  public static String bcdPlmnToString(byte[] paramArrayOfbyte, int paramInt) {
    if (paramInt + 3 > paramArrayOfbyte.length)
      return null; 
    byte b1 = (byte)(paramArrayOfbyte[paramInt + 0] << 4 | paramArrayOfbyte[paramInt + 0] >> 4 & 0xF);
    byte b2 = (byte)(paramArrayOfbyte[paramInt + 1] << 4 | paramArrayOfbyte[paramInt + 2] & 0xF);
    byte b3 = (byte)(paramArrayOfbyte[paramInt + 2] & 0xF0 | paramArrayOfbyte[paramInt + 1] >> 4 & 0xF);
    String str2 = bytesToHexString(new byte[] { b1, b2, b3 });
    String str1 = str2;
    if (str2.contains("F"))
      str1 = str2.replaceAll("F", ""); 
    return str1;
  }
  
  public static void stringToBcdPlmn(String paramString, byte[] paramArrayOfbyte, int paramInt) {
    byte b;
    if (paramString.length() > 5) {
      b = paramString.charAt(5);
    } else {
      b = 70;
    } 
    paramArrayOfbyte[paramInt] = (byte)(charToByte(paramString.charAt(1)) << 4 | charToByte(paramString.charAt(0)));
    paramArrayOfbyte[paramInt + 1] = (byte)(charToByte(b) << 4 | charToByte(paramString.charAt(2)));
    paramArrayOfbyte[paramInt + 2] = (byte)(charToByte(paramString.charAt(4)) << 4 | charToByte(paramString.charAt(3)));
  }
  
  public static String bchToString(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    StringBuilder stringBuilder = new StringBuilder(paramInt2 * 2);
    for (int i = paramInt1; i < paramInt1 + paramInt2; i++) {
      byte b = paramArrayOfbyte[i];
      stringBuilder.append(HEX_CHARS[b & 0xF]);
      b = paramArrayOfbyte[i];
      stringBuilder.append(HEX_CHARS[b >> 4 & 0xF]);
    } 
    return stringBuilder.toString();
  }
  
  public static String cdmaBcdToString(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    StringBuilder stringBuilder = new StringBuilder(paramInt2);
    int i = 0;
    for (; i < paramInt2; paramInt1++) {
      int j = paramArrayOfbyte[paramInt1] & 0xF;
      int k = j;
      if (j > 9)
        k = 0; 
      stringBuilder.append((char)(k + 48));
      j = i + 1;
      if (j == paramInt2)
        break; 
      k = paramArrayOfbyte[paramInt1] >> 4 & 0xF;
      i = k;
      if (k > 9)
        i = 0; 
      stringBuilder.append((char)(i + 48));
      i = j + 1;
    } 
    return stringBuilder.toString();
  }
  
  public static int gsmBcdByteToInt(byte paramByte) {
    int i = 0;
    if ((paramByte & 0xF0) <= 144)
      i = paramByte >> 4 & 0xF; 
    int j = i;
    if ((paramByte & 0xF) <= 9)
      j = i + (paramByte & 0xF) * 10; 
    return j;
  }
  
  public static int cdmaBcdByteToInt(byte paramByte) {
    int i = 0;
    if ((paramByte & 0xF0) <= 144)
      i = (paramByte >> 4 & 0xF) * 10; 
    int j = i;
    if ((paramByte & 0xF) <= 9)
      j = i + (paramByte & 0xF); 
    return j;
  }
  
  public static String adnStringFieldToString(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    int i, j;
    char c2;
    boolean bool2;
    if (paramInt2 == 0)
      return ""; 
    if (paramInt2 >= 1 && 
      paramArrayOfbyte[paramInt1] == Byte.MIN_VALUE) {
      i = (paramInt2 - 1) / 2;
      String str1 = null;
      try {
        String str2 = new String();
        this(paramArrayOfbyte, paramInt1 + 1, i * 2, "utf-16be");
        str1 = str2;
      } catch (UnsupportedEncodingException unsupportedEncodingException) {
        Rlog.e("IccUtils", "implausible UnsupportedEncodingException", unsupportedEncodingException);
      } 
      if (str1 != null) {
        paramInt1 = str1.length();
        while (paramInt1 > 0 && str1.charAt(paramInt1 - 1) == Character.MAX_VALUE)
          paramInt1--; 
        return str1.substring(0, paramInt1);
      } 
    } 
    boolean bool1 = false;
    char c1 = Character.MIN_VALUE;
    byte b = 0;
    if (paramInt2 >= 3 && paramArrayOfbyte[paramInt1] == -127) {
      j = paramArrayOfbyte[paramInt1 + 1] & 0xFF;
      i = j;
      if (j > paramInt2 - 3)
        i = paramInt2 - 3; 
      c2 = (char)((paramArrayOfbyte[paramInt1 + 2] & 0xFF) << 7);
      j = paramInt1 + 3;
      bool2 = true;
    } else {
      bool2 = bool1;
      c2 = c1;
      i = b;
      j = paramInt1;
      if (paramInt2 >= 4) {
        bool2 = bool1;
        c2 = c1;
        i = b;
        j = paramInt1;
        if (paramArrayOfbyte[paramInt1] == -126) {
          j = paramArrayOfbyte[paramInt1 + 1] & 0xFF;
          i = j;
          if (j > paramInt2 - 4)
            i = paramInt2 - 4; 
          c2 = (char)((paramArrayOfbyte[paramInt1 + 2] & 0xFF) << 8 | paramArrayOfbyte[paramInt1 + 3] & 0xFF);
          j = paramInt1 + 4;
          bool2 = true;
        } 
      } 
    } 
    if (bool2) {
      StringBuilder stringBuilder = new StringBuilder();
      while (i > 0) {
        paramInt2 = i;
        paramInt1 = j;
        if (paramArrayOfbyte[j] < 0) {
          stringBuilder.append((char)((paramArrayOfbyte[j] & Byte.MAX_VALUE) + c2));
          paramInt1 = j + 1;
          paramInt2 = i - 1;
        } 
        i = 0;
        while (i < paramInt2 && paramArrayOfbyte[paramInt1 + i] >= 0)
          i++; 
        stringBuilder.append(GsmAlphabet.gsm8BitUnpackedToString(paramArrayOfbyte, paramInt1, i));
        j = paramInt1 + i;
        i = paramInt2 - i;
      } 
      return stringBuilder.toString();
    } 
    Resources resources = Resources.getSystem();
    String str = "";
    try {
      String str1 = resources.getString(17040299);
    } catch (android.content.res.Resources.NotFoundException notFoundException) {}
    return GsmAlphabet.gsm8BitUnpackedToString(paramArrayOfbyte, j, paramInt2, str.trim());
  }
  
  public static int hexCharToInt(char paramChar) {
    if (paramChar >= '0' && paramChar <= '9')
      return paramChar - 48; 
    if (paramChar >= 'A' && paramChar <= 'F')
      return paramChar - 65 + 10; 
    if (paramChar >= 'a' && paramChar <= 'f')
      return paramChar - 97 + 10; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("invalid hex char '");
    stringBuilder.append(paramChar);
    stringBuilder.append("'");
    throw new RuntimeException(stringBuilder.toString());
  }
  
  public static byte[] hexStringToBytes(String paramString) {
    if (paramString == null)
      return null; 
    int i = paramString.length();
    byte[] arrayOfByte = new byte[i / 2];
    for (byte b = 0; b < i; b += 2) {
      int j = b / 2, k = hexCharToInt(paramString.charAt(b));
      arrayOfByte[j] = (byte)(k << 4 | hexCharToInt(paramString.charAt(b + 1)));
    } 
    return arrayOfByte;
  }
  
  public static String bytesToHexString(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte == null)
      return null; 
    StringBuilder stringBuilder = new StringBuilder(paramArrayOfbyte.length * 2);
    for (byte b = 0; b < paramArrayOfbyte.length; b++) {
      byte b1 = paramArrayOfbyte[b];
      stringBuilder.append(HEX_CHARS[b1 >> 4 & 0xF]);
      b1 = paramArrayOfbyte[b];
      stringBuilder.append(HEX_CHARS[b1 & 0xF]);
    } 
    return stringBuilder.toString();
  }
  
  public static String networkNameToString(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    String str;
    if ((paramArrayOfbyte[paramInt1] & 0x80) != 128 || paramInt2 < 1)
      return ""; 
    int i = paramArrayOfbyte[paramInt1] >>> 4 & 0x7;
    if (i != 0) {
      if (i != 1) {
        str = "";
      } else {
        try {
          str = new String();
          this(paramArrayOfbyte, paramInt1 + 1, paramInt2 - 1, "utf-16");
        } catch (UnsupportedEncodingException unsupportedEncodingException) {
          Rlog.e("IccUtils", "implausible UnsupportedEncodingException", unsupportedEncodingException);
          str = "";
        } 
      } 
    } else {
      i = paramArrayOfbyte[paramInt1];
      paramInt2 = ((paramInt2 - 1) * 8 - (i & 0x7)) / 7;
      str = GsmAlphabet.gsm7BitPackedToString(paramArrayOfbyte, paramInt1 + 1, paramInt2);
    } 
    paramInt1 = paramArrayOfbyte[paramInt1];
    return str;
  }
  
  public static Bitmap parseToBnW(byte[] paramArrayOfbyte, int paramInt) {
    paramInt = 0 + 1;
    int i = paramArrayOfbyte[0] & 0xFF;
    int j = paramInt + 1, k = paramArrayOfbyte[paramInt] & 0xFF;
    int m = i * k;
    int[] arrayOfInt = new int[m];
    byte b = 0;
    paramInt = 7;
    byte b1 = 0;
    while (b < m) {
      int n = j;
      if (b % 8 == 0) {
        b1 = paramArrayOfbyte[j];
        paramInt = 7;
        n = j + 1;
      } 
      arrayOfInt[b] = bitToRGB(b1 >> paramInt & 0x1);
      b++;
      paramInt--;
      j = n;
    } 
    if (b != m)
      Rlog.e("IccUtils", "parse end and size error"); 
    return Bitmap.createBitmap(arrayOfInt, i, k, Bitmap.Config.ARGB_8888);
  }
  
  private static int bitToRGB(int paramInt) {
    if (paramInt == 1)
      return -1; 
    return -16777216;
  }
  
  public static Bitmap parseToRGB(byte[] paramArrayOfbyte, int paramInt, boolean paramBoolean) {
    int arrayOfInt1[], i = 0 + 1;
    paramInt = paramArrayOfbyte[0] & 0xFF;
    int j = i + 1;
    i = paramArrayOfbyte[i] & 0xFF;
    int k = j + 1;
    j = paramArrayOfbyte[j] & 0xFF;
    int m = k + 1;
    k = paramArrayOfbyte[k] & 0xFF;
    int n = m + 1;
    m = paramArrayOfbyte[m];
    int i1 = n + 1;
    n = paramArrayOfbyte[n];
    int[] arrayOfInt2 = getCLUT(paramArrayOfbyte, (m & 0xFF) << 8 | n & 0xFF, k);
    if (true == paramBoolean)
      arrayOfInt2[k - 1] = 0; 
    if (8 % j == 0) {
      arrayOfInt1 = mapTo2OrderBitColor(paramArrayOfbyte, i1, paramInt * i, arrayOfInt2, j);
    } else {
      arrayOfInt1 = mapToNon2OrderBitColor((byte[])arrayOfInt1, i1, paramInt * i, arrayOfInt2, j);
    } 
    return Bitmap.createBitmap(arrayOfInt1, paramInt, i, Bitmap.Config.RGB_565);
  }
  
  private static int[] mapTo2OrderBitColor(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3) {
    if (8 % paramInt3 != 0) {
      Rlog.e("IccUtils", "not event number of color");
      return mapToNon2OrderBitColor(paramArrayOfbyte, paramInt1, paramInt2, paramArrayOfint, paramInt3);
    } 
    char c = '\001';
    if (paramInt3 != 1) {
      if (paramInt3 != 2) {
        if (paramInt3 != 4) {
          if (paramInt3 == 8)
            c = 'ÿ'; 
        } else {
          c = '\017';
        } 
      } else {
        c = '\003';
      } 
    } else {
      c = '\001';
    } 
    int[] arrayOfInt = new int[paramInt2];
    byte b = 0;
    int i = 8 / paramInt3;
    while (b < paramInt2) {
      byte b1 = paramArrayOfbyte[paramInt1];
      for (byte b2 = 0; b2 < i; b2++, b++)
        arrayOfInt[b] = paramArrayOfint[b1 >> (i - b2 - 1) * paramInt3 & c]; 
      paramInt1++;
    } 
    return arrayOfInt;
  }
  
  private static int[] mapToNon2OrderBitColor(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3) {
    if (8 % paramInt3 == 0) {
      Rlog.e("IccUtils", "not odd number of color");
      return mapTo2OrderBitColor(paramArrayOfbyte, paramInt1, paramInt2, paramArrayOfint, paramInt3);
    } 
    return new int[paramInt2];
  }
  
  private static int[] getCLUT(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    if (paramArrayOfbyte == null)
      return null; 
    int[] arrayOfInt = new int[paramInt2];
    int i = paramInt1;
    byte b = 0;
    while (true) {
      int j = i + 1;
      byte b1 = paramArrayOfbyte[i];
      int k = j + 1;
      j = paramArrayOfbyte[j];
      i = k + 1;
      arrayOfInt[b] = (b1 & 0xFF) << 16 | 0xFF000000 | (j & 0xFF) << 8 | paramArrayOfbyte[k] & 0xFF;
      if (i >= paramInt2 * 3 + paramInt1)
        return arrayOfInt; 
      b++;
    } 
  }
  
  public static String getDecimalSubstring(String paramString) {
    byte b;
    for (b = 0; b < paramString.length() && 
      Character.isDigit(paramString.charAt(b)); b++);
    return paramString.substring(0, b);
  }
  
  public static int bytesToInt(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    if (paramInt2 <= 4) {
      StringBuilder stringBuilder1;
      if (paramInt1 >= 0 && paramInt2 >= 0 && paramInt1 + paramInt2 <= paramArrayOfbyte.length) {
        int i = 0;
        for (byte b = 0; b < paramInt2; b++)
          i = i << 8 | paramArrayOfbyte[paramInt1 + b] & 0xFF; 
        if (i >= 0)
          return i; 
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("src cannot be parsed as a positive integer: ");
        stringBuilder1.append(i);
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("Out of the bounds: src=[");
      stringBuilder2.append(stringBuilder1.length);
      stringBuilder2.append("], offset=");
      stringBuilder2.append(paramInt1);
      stringBuilder2.append(", length=");
      stringBuilder2.append(paramInt2);
      throw new IndexOutOfBoundsException(stringBuilder2.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("length must be <= 4 (only 32-bit integer supported): ");
    stringBuilder.append(paramInt2);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static long bytesToRawLong(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    if (paramInt2 <= 8) {
      if (paramInt1 >= 0 && paramInt2 >= 0 && paramInt1 + paramInt2 <= paramArrayOfbyte.length) {
        long l = 0L;
        for (byte b = 0; b < paramInt2; b++)
          l = l << 8L | (paramArrayOfbyte[paramInt1 + b] & 0xFF); 
        return l;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Out of the bounds: src=[");
      stringBuilder1.append(paramArrayOfbyte.length);
      stringBuilder1.append("], offset=");
      stringBuilder1.append(paramInt1);
      stringBuilder1.append(", length=");
      stringBuilder1.append(paramInt2);
      throw new IndexOutOfBoundsException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("length must be <= 8 (only 64-bit long supported): ");
    stringBuilder.append(paramInt2);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static byte[] unsignedIntToBytes(int paramInt) {
    if (paramInt >= 0) {
      byte[] arrayOfByte = new byte[byteNumForUnsignedInt(paramInt)];
      unsignedIntToBytes(paramInt, arrayOfByte, 0);
      return arrayOfByte;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("value must be 0 or positive: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static byte[] signedIntToBytes(int paramInt) {
    if (paramInt >= 0) {
      byte[] arrayOfByte = new byte[byteNumForSignedInt(paramInt)];
      signedIntToBytes(paramInt, arrayOfByte, 0);
      return arrayOfByte;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("value must be 0 or positive: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static int unsignedIntToBytes(int paramInt1, byte[] paramArrayOfbyte, int paramInt2) {
    return intToBytes(paramInt1, paramArrayOfbyte, paramInt2, false);
  }
  
  public static int signedIntToBytes(int paramInt1, byte[] paramArrayOfbyte, int paramInt2) {
    return intToBytes(paramInt1, paramArrayOfbyte, paramInt2, true);
  }
  
  public static int byteNumForUnsignedInt(int paramInt) {
    return byteNumForInt(paramInt, false);
  }
  
  public static int byteNumForSignedInt(int paramInt) {
    return byteNumForInt(paramInt, true);
  }
  
  private static int intToBytes(int paramInt1, byte[] paramArrayOfbyte, int paramInt2, boolean paramBoolean) {
    int i = byteNumForInt(paramInt1, paramBoolean);
    if (paramInt2 >= 0 && paramInt2 + i <= paramArrayOfbyte.length) {
      for (int j = i - 1; j >= 0; j--, paramInt1 >>>= 8) {
        byte b = (byte)(paramInt1 & 0xFF);
        paramArrayOfbyte[paramInt2 + j] = b;
      } 
      return i;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Not enough space to write. Required bytes: ");
    stringBuilder.append(i);
    throw new IndexOutOfBoundsException(stringBuilder.toString());
  }
  
  private static int byteNumForInt(int paramInt, boolean paramBoolean) {
    if (paramInt >= 0) {
      if (paramBoolean) {
        if (paramInt <= 127)
          return 1; 
        if (paramInt <= 32767)
          return 2; 
        if (paramInt <= 8388607)
          return 3; 
      } else {
        if (paramInt <= 255)
          return 1; 
        if (paramInt <= 65535)
          return 2; 
        if (paramInt <= 16777215)
          return 3; 
      } 
      return 4;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("value must be 0 or positive: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static byte countTrailingZeros(byte paramByte) {
    if (paramByte == 0)
      return 8; 
    int i = paramByte & 0xFF;
    byte b1 = 7;
    if ((i & 0xF) != 0)
      b1 = (byte)(7 - 4); 
    byte b2 = b1;
    if ((i & 0x33) != 0)
      b2 = (byte)(b1 - 2); 
    b1 = b2;
    if ((i & 0x55) != 0)
      b1 = (byte)(b2 - 1); 
    return b1;
  }
  
  public static String byteToHex(byte paramByte) {
    char[] arrayOfChar = HEX_CHARS;
    return new String(new char[] { arrayOfChar[(paramByte & 0xFF) >>> 4], arrayOfChar[paramByte & 0xF] });
  }
  
  public static String stripTrailingFs(String paramString) {
    if (paramString == null) {
      paramString = null;
    } else {
      paramString = paramString.replaceAll("(?i)f*$", "");
    } 
    return paramString;
  }
  
  private static byte charToByte(char paramChar) {
    if (paramChar >= '0' && paramChar <= '9')
      return (byte)(paramChar - 48); 
    if (paramChar >= 'A' && paramChar <= 'F')
      return (byte)(paramChar - 55); 
    if (paramChar >= 'a' && paramChar <= 'f')
      return (byte)(paramChar - 87); 
    return 0;
  }
  
  public static byte[] encodeFplmns(List<String> paramList, int paramInt) {
    byte b2;
    byte[] arrayOfByte = new byte[paramInt];
    byte b1 = 0;
    Iterator<String> iterator = paramList.iterator();
    while (true) {
      b2 = b1;
      if (iterator.hasNext()) {
        String str = iterator.next();
        if (b1 >= paramInt) {
          b2 = b1;
          break;
        } 
        stringToBcdPlmn(str, arrayOfByte, b1);
        b1 += 3;
        continue;
      } 
      break;
    } 
    while (b2 < paramInt) {
      arrayOfByte[b2] = -1;
      b2++;
    } 
    return arrayOfByte;
  }
  
  static byte[] stringToAdnStringField(String paramString) {
    boolean bool = false;
    byte b = 0;
    try {
      for (; b < paramString.length(); b++)
        GsmAlphabet.countGsmSeptets(paramString.charAt(b), true); 
    } catch (EncodeException encodeException) {
      bool = true;
    } 
    return stringToAdnStringField(paramString, bool);
  }
  
  static byte[] stringToAdnStringField(String paramString, boolean paramBoolean) {
    if (!paramBoolean)
      return GsmAlphabet.stringToGsm8BitPacked(paramString); 
    byte[] arrayOfByte2 = paramString.getBytes(Charset.forName("UTF-16BE"));
    byte[] arrayOfByte1 = new byte[arrayOfByte2.length + 1];
    arrayOfByte1[0] = Byte.MIN_VALUE;
    System.arraycopy(arrayOfByte2, 0, arrayOfByte1, 1, arrayOfByte2.length);
    return arrayOfByte1;
  }
}
