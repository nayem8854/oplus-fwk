package com.android.internal.telephony.uicc.asn1;

public class InvalidAsn1DataException extends Exception {
  private final int mTag;
  
  public InvalidAsn1DataException(int paramInt, String paramString) {
    super(paramString);
    this.mTag = paramInt;
  }
  
  public InvalidAsn1DataException(int paramInt, String paramString, Throwable paramThrowable) {
    super(paramString, paramThrowable);
    this.mTag = paramInt;
  }
  
  public int getTag() {
    return this.mTag;
  }
  
  public String getMessage() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(super.getMessage());
    stringBuilder.append(" (tag=");
    stringBuilder.append(this.mTag);
    stringBuilder.append(")");
    return stringBuilder.toString();
  }
}
