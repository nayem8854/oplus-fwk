package com.android.internal.telephony.uicc.asn1;

import com.android.internal.telephony.uicc.IccUtils;

public final class Asn1Decoder {
  private final int mEnd;
  
  private int mPosition;
  
  private final byte[] mSrc;
  
  public Asn1Decoder(String paramString) {
    this(IccUtils.hexStringToBytes(paramString));
  }
  
  public Asn1Decoder(byte[] paramArrayOfbyte) {
    this(paramArrayOfbyte, 0, paramArrayOfbyte.length);
  }
  
  public Asn1Decoder(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    if (paramInt1 >= 0 && paramInt2 >= 0 && paramInt1 + paramInt2 <= paramArrayOfbyte.length) {
      this.mSrc = paramArrayOfbyte;
      this.mPosition = paramInt1;
      this.mEnd = paramInt1 + paramInt2;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Out of the bounds: bytes=[");
    stringBuilder.append(paramArrayOfbyte.length);
    stringBuilder.append("], offset=");
    stringBuilder.append(paramInt1);
    stringBuilder.append(", length=");
    stringBuilder.append(paramInt2);
    throw new IndexOutOfBoundsException(stringBuilder.toString());
  }
  
  public int getPosition() {
    return this.mPosition;
  }
  
  public boolean hasNextNode() {
    boolean bool;
    if (this.mPosition < this.mEnd) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public Asn1Node nextNode() throws InvalidAsn1DataException {
    if (this.mPosition < this.mEnd) {
      int i = this.mPosition;
      byte[] arrayOfByte = this.mSrc;
      int j = i + 1;
      byte b = arrayOfByte[i];
      int k = j;
      if ((b & 0x1F) == 31)
        while (true) {
          k = j;
          if (j < this.mEnd) {
            b = this.mSrc[j];
            k = ++j;
            if ((b & 0x80) != 0)
              continue; 
          } 
          break;
        }  
      if (k < this.mEnd)
        try {
          int m = IccUtils.bytesToInt(this.mSrc, i, k - i);
          arrayOfByte = this.mSrc;
          j = k + 1;
          k = arrayOfByte[k];
          if ((k & 0x80) != 0) {
            i = k & 0x7F;
            if (j + i <= this.mEnd)
              try {
                k = IccUtils.bytesToInt(arrayOfByte, j, i);
                j += i;
                if (j + k <= this.mEnd) {
                  Asn1Node asn1Node = new Asn1Node(m, this.mSrc, j, k);
                  this.mPosition = j + k;
                  return asn1Node;
                } 
                StringBuilder stringBuilder3 = new StringBuilder();
                stringBuilder3.append("Incomplete data at position: ");
                stringBuilder3.append(j);
                stringBuilder3.append(", expected bytes: ");
                stringBuilder3.append(k);
                stringBuilder3.append(", actual bytes: ");
                stringBuilder3.append(this.mEnd - j);
                throw new InvalidAsn1DataException(m, stringBuilder3.toString());
              } catch (IllegalArgumentException illegalArgumentException) {
                StringBuilder stringBuilder3 = new StringBuilder();
                stringBuilder3.append("Cannot parse length at position: ");
                stringBuilder3.append(j);
                throw new InvalidAsn1DataException(m, stringBuilder3.toString(), illegalArgumentException);
              }  
            StringBuilder stringBuilder2 = new StringBuilder();
            stringBuilder2.append("Cannot parse length at position: ");
            stringBuilder2.append(j);
            throw new InvalidAsn1DataException(m, stringBuilder2.toString());
          } 
          if (j + k <= this.mEnd) {
            Asn1Node asn1Node = new Asn1Node(m, this.mSrc, j, k);
            this.mPosition = j + k;
            return asn1Node;
          } 
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("Incomplete data at position: ");
          stringBuilder1.append(j);
          stringBuilder1.append(", expected bytes: ");
          stringBuilder1.append(k);
          stringBuilder1.append(", actual bytes: ");
          stringBuilder1.append(this.mEnd - j);
          throw new InvalidAsn1DataException(m, stringBuilder1.toString());
        } catch (IllegalArgumentException illegalArgumentException) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("Cannot parse tag at position: ");
          stringBuilder1.append(i);
          throw new InvalidAsn1DataException(0, stringBuilder1.toString(), illegalArgumentException);
        }  
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid length at position: ");
      stringBuilder.append(k);
      throw new InvalidAsn1DataException(0, stringBuilder.toString());
    } 
    throw new IllegalStateException("No bytes to parse.");
  }
}
