package com.android.internal.cabc;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ICabcManager extends IInterface {
  void closeCabc() throws RemoteException;
  
  int getMode() throws RemoteException;
  
  void openCabc() throws RemoteException;
  
  void setMode(int paramInt) throws RemoteException;
  
  class Default implements ICabcManager {
    public void setMode(int param1Int) throws RemoteException {}
    
    public int getMode() throws RemoteException {
      return 0;
    }
    
    public void closeCabc() throws RemoteException {}
    
    public void openCabc() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICabcManager {
    private static final String DESCRIPTOR = "com.android.internal.cabc.ICabcManager";
    
    static final int TRANSACTION_closeCabc = 3;
    
    static final int TRANSACTION_getMode = 2;
    
    static final int TRANSACTION_openCabc = 4;
    
    static final int TRANSACTION_setMode = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.cabc.ICabcManager");
    }
    
    public static ICabcManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.cabc.ICabcManager");
      if (iInterface != null && iInterface instanceof ICabcManager)
        return (ICabcManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "openCabc";
          } 
          return "closeCabc";
        } 
        return "getMode";
      } 
      return "setMode";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("com.android.internal.cabc.ICabcManager");
              return true;
            } 
            param1Parcel1.enforceInterface("com.android.internal.cabc.ICabcManager");
            openCabc();
            param1Parcel2.writeNoException();
            return true;
          } 
          param1Parcel1.enforceInterface("com.android.internal.cabc.ICabcManager");
          closeCabc();
          param1Parcel2.writeNoException();
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.internal.cabc.ICabcManager");
        param1Int1 = getMode();
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(param1Int1);
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.cabc.ICabcManager");
      param1Int1 = param1Parcel1.readInt();
      setMode(param1Int1);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements ICabcManager {
      public static ICabcManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.cabc.ICabcManager";
      }
      
      public void setMode(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.cabc.ICabcManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ICabcManager.Stub.getDefaultImpl() != null) {
            ICabcManager.Stub.getDefaultImpl().setMode(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.cabc.ICabcManager");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ICabcManager.Stub.getDefaultImpl() != null)
            return ICabcManager.Stub.getDefaultImpl().getMode(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void closeCabc() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.cabc.ICabcManager");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && ICabcManager.Stub.getDefaultImpl() != null) {
            ICabcManager.Stub.getDefaultImpl().closeCabc();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void openCabc() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.cabc.ICabcManager");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && ICabcManager.Stub.getDefaultImpl() != null) {
            ICabcManager.Stub.getDefaultImpl().openCabc();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICabcManager param1ICabcManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICabcManager != null) {
          Proxy.sDefaultImpl = param1ICabcManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICabcManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
