package com.android.internal.ml.clustering;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class KMeans {
  private static final boolean DEBUG = false;
  
  private static final String TAG = "KMeans";
  
  private final int mMaxIterations;
  
  private final Random mRandomState;
  
  private float mSqConvergenceEpsilon;
  
  public KMeans() {
    this(new Random());
  }
  
  public KMeans(Random paramRandom) {
    this(paramRandom, 30, 0.005F);
  }
  
  public KMeans(Random paramRandom, int paramInt, float paramFloat) {
    this.mRandomState = paramRandom;
    this.mMaxIterations = paramInt;
    this.mSqConvergenceEpsilon = paramFloat * paramFloat;
  }
  
  public List<Mean> predict(int paramInt, float[][] paramArrayOffloat) {
    checkDataSetSanity(paramArrayOffloat);
    int i = (paramArrayOffloat[0]).length;
    ArrayList<Mean> arrayList = new ArrayList();
    for (byte b = 0; b < paramInt; b++) {
      Mean mean = new Mean(i);
      for (byte b1 = 0; b1 < i; b1++)
        mean.mCentroid[b1] = this.mRandomState.nextFloat(); 
      arrayList.add(mean);
    } 
    for (paramInt = 0; paramInt < this.mMaxIterations; paramInt++) {
      boolean bool = step(arrayList, paramArrayOffloat);
      if (bool)
        break; 
    } 
    return arrayList;
  }
  
  public static double score(List<Mean> paramList) {
    double d = 0.0D;
    int i = paramList.size();
    for (byte b = 0; b < i; b++) {
      Mean mean = paramList.get(b);
      for (byte b1 = 0; b1 < i; b1++) {
        Mean mean1 = paramList.get(b1);
        if (mean != mean1) {
          double d1 = Math.sqrt(sqDistance(mean.mCentroid, mean1.mCentroid));
          d += d1;
        } 
      } 
    } 
    return d;
  }
  
  public void checkDataSetSanity(float[][] paramArrayOffloat) {
    if (paramArrayOffloat != null) {
      if (paramArrayOffloat.length != 0) {
        if (paramArrayOffloat[0] != null) {
          int i = (paramArrayOffloat[0]).length;
          int j = paramArrayOffloat.length;
          for (byte b = 1; b < j; ) {
            if (paramArrayOffloat[b] != null && (paramArrayOffloat[b]).length == i) {
              b++;
              continue;
            } 
            throw new IllegalArgumentException("Bad data set format.");
          } 
          return;
        } 
        throw new IllegalArgumentException("Bad data set format.");
      } 
      throw new IllegalArgumentException("Data set is empty.");
    } 
    throw new IllegalArgumentException("Data set is null.");
  }
  
  private boolean step(ArrayList<Mean> paramArrayList, float[][] paramArrayOffloat) {
    int i;
    for (i = paramArrayList.size() - 1; i >= 0; i--) {
      Mean mean = paramArrayList.get(i);
      mean.mClosestItems.clear();
    } 
    for (i = paramArrayOffloat.length - 1; i >= 0; i--) {
      float[] arrayOfFloat = paramArrayOffloat[i];
      Mean mean = nearestMean(arrayOfFloat, paramArrayList);
      mean.mClosestItems.add(arrayOfFloat);
    } 
    boolean bool = true;
    for (i = paramArrayList.size() - 1; i >= 0; i--) {
      Mean mean = paramArrayList.get(i);
      if (mean.mClosestItems.size() != 0) {
        float[] arrayOfFloat = mean.mCentroid;
        mean.mCentroid = new float[arrayOfFloat.length];
        byte b;
        for (b = 0; b < mean.mClosestItems.size(); b++) {
          for (byte b1 = 0; b1 < mean.mCentroid.length; b1++) {
            float[] arrayOfFloat1 = mean.mCentroid;
            arrayOfFloat1[b1] = arrayOfFloat1[b1] + ((float[])mean.mClosestItems.get(b))[b1];
          } 
        } 
        for (b = 0; b < mean.mCentroid.length; b++) {
          float[] arrayOfFloat1 = mean.mCentroid;
          arrayOfFloat1[b] = arrayOfFloat1[b] / mean.mClosestItems.size();
        } 
        if (sqDistance(arrayOfFloat, mean.mCentroid) > this.mSqConvergenceEpsilon)
          bool = false; 
      } 
    } 
    return bool;
  }
  
  public static Mean nearestMean(float[] paramArrayOffloat, List<Mean> paramList) {
    Mean mean = null;
    float f = Float.MAX_VALUE;
    int i = paramList.size();
    for (byte b = 0; b < i; b++, f = f2) {
      Mean mean1 = paramList.get(b);
      float f1 = sqDistance(paramArrayOffloat, mean1.mCentroid);
      float f2 = f;
      if (f1 < f) {
        mean = mean1;
        f2 = f1;
      } 
    } 
    return mean;
  }
  
  public static float sqDistance(float[] paramArrayOffloat1, float[] paramArrayOffloat2) {
    float f = 0.0F;
    int i = paramArrayOffloat1.length;
    for (byte b = 0; b < i; b++)
      f += (paramArrayOffloat1[b] - paramArrayOffloat2[b]) * (paramArrayOffloat1[b] - paramArrayOffloat2[b]); 
    return f;
  }
  
  public static class Mean {
    float[] mCentroid;
    
    final ArrayList<float[]> mClosestItems = (ArrayList)new ArrayList<>();
    
    public Mean(int param1Int) {
      this.mCentroid = new float[param1Int];
    }
    
    public Mean(float... param1VarArgs) {
      this.mCentroid = param1VarArgs;
    }
    
    public float[] getCentroid() {
      return this.mCentroid;
    }
    
    public List<float[]> getItems() {
      return (List<float[]>)this.mClosestItems;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Mean(centroid: ");
      stringBuilder.append(Arrays.toString(this.mCentroid));
      stringBuilder.append(", size: ");
      ArrayList<float[]> arrayList = this.mClosestItems;
      stringBuilder.append(arrayList.size());
      stringBuilder.append(")");
      return stringBuilder.toString();
    }
  }
}
