package com.android.internal.content;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.UserHandle;
import android.util.Slog;
import com.android.internal.os.BackgroundThread;
import java.util.HashSet;
import java.util.Objects;

public abstract class PackageMonitor extends BroadcastReceiver {
  static final IntentFilter sPackageFilt = new IntentFilter();
  
  static {
    sNonDataFilt = new IntentFilter();
    sExternalFilt = new IntentFilter();
    sPackageFilt.addAction("android.intent.action.PACKAGE_ADDED");
    sPackageFilt.addAction("android.intent.action.PACKAGE_REMOVED");
    sPackageFilt.addAction("android.intent.action.PACKAGE_CHANGED");
    sPackageFilt.addAction("android.intent.action.QUERY_PACKAGE_RESTART");
    sPackageFilt.addAction("android.intent.action.PACKAGE_RESTARTED");
    sPackageFilt.addAction("android.intent.action.PACKAGE_DATA_CLEARED");
    sPackageFilt.addDataScheme("package");
    sNonDataFilt.addAction("android.intent.action.UID_REMOVED");
    sNonDataFilt.addAction("android.intent.action.USER_STOPPED");
    sNonDataFilt.addAction("android.intent.action.PACKAGES_SUSPENDED");
    sNonDataFilt.addAction("android.intent.action.PACKAGES_UNSUSPENDED");
    sExternalFilt.addAction("android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE");
    sExternalFilt.addAction("android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE");
  }
  
  final HashSet<String> mUpdatingPackages = new HashSet<>();
  
  int mChangeUserId = -10000;
  
  String[] mTempArray = new String[1];
  
  public static final int PACKAGE_PERMANENT_CHANGE = 3;
  
  public static final int PACKAGE_TEMPORARY_CHANGE = 2;
  
  public static final int PACKAGE_UNCHANGED = 0;
  
  public static final int PACKAGE_UPDATING = 1;
  
  static final IntentFilter sExternalFilt;
  
  static final IntentFilter sNonDataFilt;
  
  String[] mAppearingPackages;
  
  int mChangeType;
  
  String[] mDisappearingPackages;
  
  String[] mModifiedComponents;
  
  String[] mModifiedPackages;
  
  Context mRegisteredContext;
  
  Handler mRegisteredHandler;
  
  boolean mSomePackagesChanged;
  
  public void register(Context paramContext, Looper paramLooper, boolean paramBoolean) {
    register(paramContext, paramLooper, (UserHandle)null, paramBoolean);
  }
  
  public void register(Context paramContext, Looper paramLooper, UserHandle paramUserHandle, boolean paramBoolean) {
    Handler handler;
    if (paramLooper == null) {
      handler = BackgroundThread.getHandler();
    } else {
      handler = new Handler((Looper)handler);
    } 
    register(paramContext, paramUserHandle, paramBoolean, handler);
  }
  
  public void register(Context paramContext, UserHandle paramUserHandle, boolean paramBoolean, Handler paramHandler) {
    if (this.mRegisteredContext == null) {
      this.mRegisteredContext = paramContext;
      Objects.requireNonNull(paramHandler);
      this.mRegisteredHandler = paramHandler = paramHandler;
      if (paramUserHandle != null) {
        paramContext.registerReceiverAsUser(this, paramUserHandle, sPackageFilt, null, paramHandler);
        paramContext.registerReceiverAsUser(this, paramUserHandle, sNonDataFilt, null, this.mRegisteredHandler);
        if (paramBoolean)
          paramContext.registerReceiverAsUser(this, paramUserHandle, sExternalFilt, null, this.mRegisteredHandler); 
      } else {
        paramContext.registerReceiver(this, sPackageFilt, null, paramHandler);
        paramContext.registerReceiver(this, sNonDataFilt, null, this.mRegisteredHandler);
        if (paramBoolean)
          paramContext.registerReceiver(this, sExternalFilt, null, this.mRegisteredHandler); 
      } 
      return;
    } 
    throw new IllegalStateException("Already registered");
  }
  
  public Handler getRegisteredHandler() {
    return this.mRegisteredHandler;
  }
  
  public void unregister() {
    Context context = this.mRegisteredContext;
    if (context != null) {
      context.unregisterReceiver(this);
      this.mRegisteredContext = null;
      return;
    } 
    throw new IllegalStateException("Not registered");
  }
  
  boolean isPackageUpdating(String paramString) {
    synchronized (this.mUpdatingPackages) {
      return this.mUpdatingPackages.contains(paramString);
    } 
  }
  
  public void onBeginPackageChanges() {}
  
  public void onPackageAdded(String paramString, int paramInt) {}
  
  public void onPackageRemoved(String paramString, int paramInt) {}
  
  public void onPackageRemovedAllUsers(String paramString, int paramInt) {}
  
  public void onPackageUpdateStarted(String paramString, int paramInt) {}
  
  public void onPackageUpdateFinished(String paramString, int paramInt) {}
  
  public boolean onPackageChanged(String paramString, int paramInt, String[] paramArrayOfString) {
    if (paramArrayOfString != null)
      for (int i = paramArrayOfString.length; paramInt < i; ) {
        String str = paramArrayOfString[paramInt];
        if (paramString.equals(str))
          return true; 
        paramInt++;
      }  
    return false;
  }
  
  public boolean onHandleForceStop(Intent paramIntent, String[] paramArrayOfString, int paramInt, boolean paramBoolean) {
    return false;
  }
  
  public void onHandleUserStop(Intent paramIntent, int paramInt) {}
  
  public void onUidRemoved(int paramInt) {}
  
  public void onPackagesAvailable(String[] paramArrayOfString) {}
  
  public void onPackagesUnavailable(String[] paramArrayOfString) {}
  
  public void onPackagesSuspended(String[] paramArrayOfString) {}
  
  public void onPackagesUnsuspended(String[] paramArrayOfString) {}
  
  public void onPackageDisappeared(String paramString, int paramInt) {}
  
  public void onPackageAppeared(String paramString, int paramInt) {}
  
  public void onPackageModified(String paramString) {}
  
  public boolean didSomePackagesChange() {
    return this.mSomePackagesChanged;
  }
  
  public int isPackageAppearing(String paramString) {
    String[] arrayOfString = this.mAppearingPackages;
    if (arrayOfString != null)
      for (int i = arrayOfString.length - 1; i >= 0; i--) {
        if (paramString.equals(this.mAppearingPackages[i]))
          return this.mChangeType; 
      }  
    return 0;
  }
  
  public boolean anyPackagesAppearing() {
    boolean bool;
    if (this.mAppearingPackages != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int isPackageDisappearing(String paramString) {
    String[] arrayOfString = this.mDisappearingPackages;
    if (arrayOfString != null)
      for (int i = arrayOfString.length - 1; i >= 0; i--) {
        if (paramString.equals(this.mDisappearingPackages[i]))
          return this.mChangeType; 
      }  
    return 0;
  }
  
  public boolean anyPackagesDisappearing() {
    boolean bool;
    if (this.mDisappearingPackages != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isReplacing() {
    int i = this.mChangeType;
    boolean bool = true;
    if (i != 1)
      bool = false; 
    return bool;
  }
  
  public boolean isPackageModified(String paramString) {
    String[] arrayOfString = this.mModifiedPackages;
    if (arrayOfString != null)
      for (int i = arrayOfString.length - 1; i >= 0; i--) {
        if (paramString.equals(this.mModifiedPackages[i]))
          return true; 
      }  
    return false;
  }
  
  public boolean isComponentModified(String paramString) {
    if (paramString != null) {
      String[] arrayOfString = this.mModifiedComponents;
      if (arrayOfString != null) {
        for (int i = arrayOfString.length - 1; i >= 0; i--) {
          if (paramString.equals(this.mModifiedComponents[i]))
            return true; 
        } 
        return false;
      } 
    } 
    return false;
  }
  
  public void onSomePackagesChanged() {}
  
  public void onFinishPackageChanges() {}
  
  public void onPackageDataCleared(String paramString, int paramInt) {}
  
  public int getChangingUserId() {
    return this.mChangeUserId;
  }
  
  String getPackageName(Intent paramIntent) {
    Uri uri = paramIntent.getData();
    if (uri != null) {
      String str = uri.getSchemeSpecificPart();
    } else {
      uri = null;
    } 
    return (String)uri;
  }
  
  public void onReceive(Context paramContext, Intent paramIntent) {
    int i = paramIntent.getIntExtra("android.intent.extra.user_handle", -10000);
    if (i == -10000) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Intent broadcast does not contain user handle: ");
      stringBuilder.append(paramIntent);
      Slog.w("PackageMonitor", stringBuilder.toString());
      return;
    } 
    onBeginPackageChanges();
    this.mAppearingPackages = null;
    this.mDisappearingPackages = null;
    this.mSomePackagesChanged = false;
    this.mModifiedComponents = null;
    String str = paramIntent.getAction();
    if ("android.intent.action.PACKAGE_ADDED".equals(str)) {
      str = getPackageName(paramIntent);
      i = paramIntent.getIntExtra("android.intent.extra.UID", 0);
      this.mSomePackagesChanged = true;
      if (str != null) {
        String[] arrayOfString = this.mTempArray;
        arrayOfString[0] = str;
        if (paramIntent.getBooleanExtra("android.intent.extra.REPLACING", false)) {
          this.mModifiedPackages = this.mTempArray;
          this.mChangeType = 1;
          onPackageUpdateFinished(str, i);
          onPackageModified(str);
        } else {
          this.mChangeType = 3;
          onPackageAdded(str, i);
        } 
        onPackageAppeared(str, this.mChangeType);
        if (this.mChangeType == 1)
          synchronized (this.mUpdatingPackages) {
            this.mUpdatingPackages.remove(str);
          }  
      } 
    } else if ("android.intent.action.PACKAGE_REMOVED".equals(str)) {
      str = getPackageName(paramIntent);
      i = paramIntent.getIntExtra("android.intent.extra.UID", 0);
      if (str != null) {
        String[] arrayOfString = this.mTempArray;
        arrayOfString[0] = str;
        if (paramIntent.getBooleanExtra("android.intent.extra.REPLACING", false)) {
          this.mChangeType = 1;
          synchronized (this.mUpdatingPackages) {
            onPackageUpdateStarted(str, i);
          } 
        } else {
          this.mChangeType = 3;
          this.mSomePackagesChanged = true;
          onPackageRemoved(str, i);
          if (paramIntent.getBooleanExtra("android.intent.extra.REMOVED_FOR_ALL_USERS", false))
            onPackageRemovedAllUsers(str, i); 
        } 
        onPackageDisappeared(str, this.mChangeType);
      } 
    } else {
      String[] arrayOfString;
      if ("android.intent.action.PACKAGE_CHANGED".equals(str)) {
        str = getPackageName(paramIntent);
        i = paramIntent.getIntExtra("android.intent.extra.UID", 0);
        String[] arrayOfString1 = paramIntent.getStringArrayExtra("android.intent.extra.changed_component_name_list");
        if (str != null) {
          this.mModifiedPackages = arrayOfString = this.mTempArray;
          arrayOfString[0] = str;
          this.mChangeType = 3;
          if (onPackageChanged(str, i, arrayOfString1))
            this.mSomePackagesChanged = true; 
          onPackageModified(str);
        } 
      } else if ("android.intent.action.PACKAGE_DATA_CLEARED".equals(str)) {
        str = getPackageName((Intent)arrayOfString);
        i = arrayOfString.getIntExtra("android.intent.extra.UID", 0);
        if (str != null)
          onPackageDataCleared(str, i); 
      } else {
        String[] arrayOfString1;
        boolean bool = "android.intent.action.QUERY_PACKAGE_RESTART".equals(str);
        i = 2;
        if (bool) {
          this.mDisappearingPackages = arrayOfString1 = arrayOfString.getStringArrayExtra("android.intent.extra.PACKAGES");
          this.mChangeType = 2;
          i = arrayOfString.getIntExtra("android.intent.extra.UID", 0);
          bool = onHandleForceStop((Intent)arrayOfString, arrayOfString1, i, false);
          if (bool)
            setResultCode(-1); 
        } else if ("android.intent.action.PACKAGE_RESTARTED".equals(arrayOfString1)) {
          arrayOfString1 = new String[1];
          arrayOfString1[0] = getPackageName((Intent)arrayOfString);
          this.mDisappearingPackages = arrayOfString1;
          this.mChangeType = 2;
          i = arrayOfString.getIntExtra("android.intent.extra.UID", 0);
          onHandleForceStop((Intent)arrayOfString, arrayOfString1, i, true);
        } else if ("android.intent.action.UID_REMOVED".equals(arrayOfString1)) {
          onUidRemoved(arrayOfString.getIntExtra("android.intent.extra.UID", 0));
        } else if ("android.intent.action.USER_STOPPED".equals(arrayOfString1)) {
          if (arrayOfString.hasExtra("android.intent.extra.user_handle"))
            onHandleUserStop((Intent)arrayOfString, arrayOfString.getIntExtra("android.intent.extra.user_handle", 0)); 
        } else if ("android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE".equals(arrayOfString1)) {
          arrayOfString1 = arrayOfString.getStringArrayExtra("android.intent.extra.changed_package_list");
          this.mAppearingPackages = arrayOfString1;
          if (arrayOfString.getBooleanExtra("android.intent.extra.REPLACING", false))
            i = 1; 
          this.mChangeType = i;
          this.mSomePackagesChanged = true;
          if (arrayOfString1 != null) {
            onPackagesAvailable(arrayOfString1);
            for (i = 0; i < arrayOfString1.length; i++)
              onPackageAppeared(arrayOfString1[i], this.mChangeType); 
          } 
        } else if ("android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE".equals(arrayOfString1)) {
          arrayOfString1 = arrayOfString.getStringArrayExtra("android.intent.extra.changed_package_list");
          this.mDisappearingPackages = arrayOfString1;
          if (arrayOfString.getBooleanExtra("android.intent.extra.REPLACING", false))
            i = 1; 
          this.mChangeType = i;
          this.mSomePackagesChanged = true;
          if (arrayOfString1 != null) {
            onPackagesUnavailable(arrayOfString1);
            for (i = 0; i < arrayOfString1.length; i++)
              onPackageDisappeared(arrayOfString1[i], this.mChangeType); 
          } 
        } else if ("android.intent.action.PACKAGES_SUSPENDED".equals(arrayOfString1)) {
          arrayOfString1 = arrayOfString.getStringArrayExtra("android.intent.extra.changed_package_list");
          this.mSomePackagesChanged = true;
          onPackagesSuspended(arrayOfString1);
        } else if ("android.intent.action.PACKAGES_UNSUSPENDED".equals(arrayOfString1)) {
          arrayOfString1 = arrayOfString.getStringArrayExtra("android.intent.extra.changed_package_list");
          this.mSomePackagesChanged = true;
          onPackagesUnsuspended(arrayOfString1);
        } 
      } 
    } 
    if (this.mSomePackagesChanged)
      onSomePackagesChanged(); 
    onFinishPackageChanges();
    this.mChangeUserId = -10000;
  }
}
