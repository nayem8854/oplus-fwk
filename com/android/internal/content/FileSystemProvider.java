package com.android.internal.content;

import android.content.ContentResolver;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.MatrixCursor;
import android.graphics.Point;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.FileObserver;
import android.os.FileUtils;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.provider.DocumentsContract;
import android.provider.DocumentsProvider;
import android.provider.MediaStore;
import android.provider.MetadataReader;
import android.system.Int64Ref;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.webkit.MimeTypeMap;
import com.android.internal.util.ArrayUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import libcore.io.IoUtils;

public abstract class FileSystemProvider extends DocumentsProvider {
  private static final boolean LOG_INOTIFY = false;
  
  private static final Pattern PATTERN_HIDDEN_PATH;
  
  protected static final String SUPPORTED_QUERY_ARGS = joinNewline(new String[] { "android:query-arg-display-name", "android:query-arg-file-size-over", "android:query-arg-last-modified-after", "android:query-arg-mime-types" });
  
  private static final String TAG = "FileSystemProvider";
  
  private String[] mDefaultProjection;
  
  private Handler mHandler;
  
  private static String joinNewline(String... paramVarArgs) {
    return TextUtils.join("\n", (Object[])paramVarArgs);
  }
  
  private final ArrayMap<File, DirectoryObserver> mObservers = new ArrayMap<>();
  
  protected void onDocIdChanged(String paramString) {}
  
  protected void onDocIdDeleted(String paramString) {}
  
  public boolean onCreate() {
    throw new UnsupportedOperationException("Subclass should override this and call onCreate(defaultDocumentProjection)");
  }
  
  protected void onCreate(String[] paramArrayOfString) {
    this.mHandler = new Handler();
    this.mDefaultProjection = paramArrayOfString;
  }
  
  public boolean isChildDocument(String paramString1, String paramString2) {
    try {
      File file1 = getFileForDocId(paramString1).getCanonicalFile();
      File file2 = getFileForDocId(paramString2).getCanonicalFile();
      return FileUtils.contains(file1, file2);
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to determine if ");
      stringBuilder.append(paramString2);
      stringBuilder.append(" is child of ");
      stringBuilder.append(paramString1);
      stringBuilder.append(": ");
      stringBuilder.append(iOException);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
  }
  
  public Bundle getDocumentMetadata(String paramString) throws FileNotFoundException {
    FileInputStream fileInputStream;
    File file = getFileForDocId(paramString);
    if (file.exists()) {
      String str1 = getDocumentType(paramString);
      if ("vnd.android.document/directory".equals(str1)) {
        Int64Ref int64Ref1 = new Int64Ref(0L);
        Int64Ref int64Ref2 = new Int64Ref(0L);
        try {
          Path path = FileSystems.getDefault().getPath(file.getAbsolutePath(), new String[0]);
          Object object = new Object();
          super(this, int64Ref1, int64Ref2);
          Files.walkFileTree(path, (FileVisitor<? super Path>)object);
          Bundle bundle = new Bundle();
          bundle.putLong("android:metadataTreeCount", int64Ref1.value);
          bundle.putLong("android:metadataTreeSize", int64Ref2.value);
          return bundle;
        } catch (IOException iOException) {
          Log.e("FileSystemProvider", "An error occurred retrieving the metadata", iOException);
          return null;
        } 
      } 
      if (!file.isFile()) {
        Log.w("FileSystemProvider", "Can't stream non-regular file. Returning empty metadata.");
        return null;
      } 
      if (!file.canRead()) {
        Log.w("FileSystemProvider", "Can't stream non-readable file. Returning empty metadata.");
        return null;
      } 
      if (!MetadataReader.isSupportedMimeType(str1)) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Unsupported type ");
        stringBuilder1.append(str1);
        stringBuilder1.append(". Returning empty metadata.");
        Log.w("FileSystemProvider", stringBuilder1.toString());
        return null;
      } 
      FileInputStream fileInputStream2 = null;
      String str2 = null;
      paramString = str2;
      FileInputStream fileInputStream1 = fileInputStream2;
      try {
        Bundle bundle = new Bundle();
        paramString = str2;
        fileInputStream1 = fileInputStream2;
        this();
        paramString = str2;
        fileInputStream1 = fileInputStream2;
        FileInputStream fileInputStream3 = new FileInputStream();
        paramString = str2;
        fileInputStream1 = fileInputStream2;
        this(file.getAbsolutePath());
        fileInputStream2 = fileInputStream3;
        fileInputStream = fileInputStream2;
        fileInputStream1 = fileInputStream2;
        MetadataReader.getMetadata(bundle, fileInputStream2, str1, null);
        IoUtils.closeQuietly(fileInputStream2);
        return bundle;
      } catch (IOException iOException) {
        fileInputStream = fileInputStream1;
        Log.e("FileSystemProvider", "An error occurred retrieving the metadata", iOException);
        IoUtils.closeQuietly(fileInputStream1);
        return null;
      } finally {}
      IoUtils.closeQuietly(fileInputStream);
      throw fileInputStream1;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Can't find the file for documentId: ");
    stringBuilder.append((String)fileInputStream);
    throw new FileNotFoundException(stringBuilder.toString());
  }
  
  protected final List<String> findDocumentPath(File paramFile1, File paramFile2) throws FileNotFoundException {
    if (paramFile2.exists()) {
      if (FileUtils.contains(paramFile1, paramFile2)) {
        LinkedList<String> linkedList = new LinkedList();
        while (paramFile2 != null && FileUtils.contains(paramFile1, paramFile2)) {
          linkedList.addFirst(getDocIdForFile(paramFile2));
          paramFile2 = paramFile2.getParentFile();
        } 
        return linkedList;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramFile2);
      stringBuilder1.append(" is not found under ");
      stringBuilder1.append(paramFile1);
      throw new FileNotFoundException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramFile2);
    stringBuilder.append(" is not found.");
    throw new FileNotFoundException(stringBuilder.toString());
  }
  
  public String createDocument(String paramString1, String paramString2, String paramString3) throws FileNotFoundException {
    paramString3 = FileUtils.buildValidFatFilename(paramString3);
    File file = getFileForDocId(paramString1);
    if (file.isDirectory()) {
      File file1 = FileUtils.buildUniqueFile(file, paramString2, paramString3);
      if ("vnd.android.document/directory".equals(paramString2)) {
        if (file1.mkdir()) {
          String str = getDocIdForFile(file1);
          onDocIdChanged(str);
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Failed to mkdir ");
          stringBuilder.append(file1);
          throw new IllegalStateException(stringBuilder.toString());
        } 
      } else {
        try {
          if (file1.createNewFile()) {
            String str = getDocIdForFile(file1);
            onDocIdChanged(str);
            MediaStore.scanFile(getContext().getContentResolver(), file1);
            return str;
          } 
          IllegalStateException illegalStateException = new IllegalStateException();
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Failed to touch ");
          stringBuilder.append(file1);
          this(stringBuilder.toString());
          throw illegalStateException;
        } catch (IOException iOException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Failed to touch ");
          stringBuilder.append(file1);
          stringBuilder.append(": ");
          stringBuilder.append(iOException);
          throw new IllegalStateException(stringBuilder.toString());
        } 
      } 
      MediaStore.scanFile(getContext().getContentResolver(), file1);
      return (String)iOException;
    } 
    throw new IllegalArgumentException("Parent document isn't a directory");
  }
  
  public String renameDocument(String paramString1, String paramString2) throws FileNotFoundException {
    String str1, str2 = FileUtils.buildValidFatFilename(paramString2);
    File file3 = getFileForDocId(paramString1);
    File file1 = getFileForDocId(paramString1, true);
    File file2 = FileUtils.buildUniqueFile(file3.getParentFile(), str2);
    if (file3.renameTo(file2)) {
      str1 = getDocIdForFile(file2);
      onDocIdChanged(paramString1);
      onDocIdDeleted(paramString1);
      onDocIdChanged(str1);
      file3 = getFileForDocId(str1, true);
      moveInMediaStore(file1, file3);
      if (!TextUtils.equals(paramString1, str1))
        return str1; 
      return null;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Failed to rename to ");
    stringBuilder.append(str1);
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public String moveDocument(String paramString1, String paramString2, String paramString3) throws FileNotFoundException {
    String str;
    File file3 = getFileForDocId(paramString1);
    File file1 = new File(getFileForDocId(paramString3), file3.getName());
    File file2 = getFileForDocId(paramString1, true);
    if (!file1.exists()) {
      if (file3.renameTo(file1)) {
        str = getDocIdForFile(file1);
        onDocIdChanged(paramString1);
        onDocIdDeleted(paramString1);
        onDocIdChanged(str);
        moveInMediaStore(file2, getFileForDocId(str, true));
        return str;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Failed to move to ");
      stringBuilder1.append(str);
      throw new IllegalStateException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Already exists ");
    stringBuilder.append(str);
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  private void moveInMediaStore(File paramFile1, File paramFile2) {
    if (paramFile1 != null)
      MediaStore.scanFile(getContext().getContentResolver(), paramFile1); 
    if (paramFile2 != null)
      MediaStore.scanFile(getContext().getContentResolver(), paramFile2); 
  }
  
  public void deleteDocument(String paramString) throws FileNotFoundException {
    File file1 = getFileForDocId(paramString);
    File file2 = getFileForDocId(paramString, true);
    boolean bool = file1.isDirectory();
    if (bool)
      FileUtils.deleteContents(file1); 
    if (!file1.exists() || file1.delete()) {
      onDocIdChanged(paramString);
      onDocIdDeleted(paramString);
      removeFromMediaStore(file2);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Failed to delete ");
    stringBuilder.append(file1);
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  private void removeFromMediaStore(File paramFile) throws FileNotFoundException {
    if (paramFile != null) {
      long l = Binder.clearCallingIdentity();
      try {
        ContentResolver contentResolver = getContext().getContentResolver();
        Uri uri = MediaStore.Files.getContentUri("external");
        Bundle bundle = new Bundle();
        this();
        bundle.putInt("android:query-arg-match-pending", 1);
        String str1 = paramFile.getAbsolutePath();
        String str2 = DatabaseUtils.escapeForLike(str1);
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append(str2);
        stringBuilder.append("/%");
        ContentResolver.includeSqlSelectionArgs(bundle, "_data LIKE ? ESCAPE '\\' OR _data LIKE ? ESCAPE '\\'", new String[] { stringBuilder.toString(), str2 });
        contentResolver.delete(uri, bundle);
      } finally {
        Binder.restoreCallingIdentity(l);
      } 
    } 
  }
  
  public Cursor queryDocument(String paramString, String[] paramArrayOfString) throws FileNotFoundException {
    MatrixCursor matrixCursor = new MatrixCursor(resolveProjection(paramArrayOfString));
    includeFile(matrixCursor, paramString, null);
    return (Cursor)matrixCursor;
  }
  
  protected Cursor queryChildDocumentsShowAll(String paramString1, String[] paramArrayOfString, String paramString2) throws FileNotFoundException {
    return queryChildDocuments(paramString1, paramArrayOfString, paramString2, (Predicate<File>)_$$Lambda$FileSystemProvider$ytk6fgF3pMK_GQ_HWUoYxxXHZXM.INSTANCE);
  }
  
  public Cursor queryChildDocuments(String paramString1, String[] paramArrayOfString, String paramString2) throws FileNotFoundException {
    return queryChildDocuments(paramString1, paramArrayOfString, paramString2, new _$$Lambda$FileSystemProvider$wdd3IYyJrETFNprLDtbduosGS8I(this));
  }
  
  private Cursor queryChildDocuments(String paramString1, String[] paramArrayOfString, String paramString2, Predicate<File> paramPredicate) throws FileNotFoundException {
    File file2 = getFileForDocId(paramString1);
    DirectoryCursor directoryCursor = new DirectoryCursor(resolveProjection(paramArrayOfString), paramString1, file2);
    if (file2.isDirectory()) {
      for (File file1 : FileUtils.listFilesOrEmpty(file2)) {
        if (paramPredicate.test(file1))
          includeFile(directoryCursor, null, file1); 
      } 
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("parentDocumentId '");
      stringBuilder.append((String)file1);
      stringBuilder.append("' is not Directory");
      Log.w("FileSystemProvider", stringBuilder.toString());
    } 
    return (Cursor)directoryCursor;
  }
  
  protected final Cursor querySearchDocuments(File paramFile, String[] paramArrayOfString, Set<String> paramSet, Bundle paramBundle) throws FileNotFoundException {
    MatrixCursor matrixCursor = new MatrixCursor(resolveProjection(paramArrayOfString));
    LinkedList<File> linkedList = new LinkedList();
    linkedList.add(paramFile);
    while (!linkedList.isEmpty() && matrixCursor.getCount() < 24) {
      paramFile = linkedList.removeFirst();
      if (shouldHide(paramFile))
        continue; 
      if (paramFile.isDirectory())
        for (File file : FileUtils.listFilesOrEmpty(paramFile))
          linkedList.add(file);  
      if (!paramSet.contains(paramFile.getAbsolutePath()) && matchSearchQueryArguments(paramFile, paramBundle))
        includeFile(matrixCursor, null, paramFile); 
    } 
    String[] arrayOfString = DocumentsContract.getHandledQueryArguments(paramBundle);
    if (arrayOfString.length > 0) {
      Bundle bundle = new Bundle();
      bundle.putStringArray("android.content.extra.HONORED_ARGS", arrayOfString);
      matrixCursor.setExtras(bundle);
    } 
    return (Cursor)matrixCursor;
  }
  
  public String getDocumentType(String paramString) throws FileNotFoundException {
    return getDocumentType(paramString, getFileForDocId(paramString));
  }
  
  private String getDocumentType(String paramString, File paramFile) throws FileNotFoundException {
    if (paramFile.isDirectory())
      return "vnd.android.document/directory"; 
    int i = paramString.lastIndexOf('.');
    if (i >= 0) {
      paramString = paramString.substring(i + 1).toLowerCase();
      paramString = MimeTypeMap.getSingleton().getMimeTypeFromExtension(paramString);
      if (paramString != null)
        return paramString; 
    } 
    return "application/octet-stream";
  }
  
  public ParcelFileDescriptor openDocument(String paramString1, String paramString2, CancellationSignal paramCancellationSignal) throws FileNotFoundException {
    File file2 = getFileForDocId(paramString1);
    File file1 = getFileForDocId(paramString1, true);
    int i = ParcelFileDescriptor.parseMode(paramString2);
    if (i == 268435456 || file1 == null)
      return ParcelFileDescriptor.open(file2, i); 
    try {
      Handler handler = this.mHandler;
      _$$Lambda$FileSystemProvider$ckJway_R_1_Nz9h7IMaXmGekKKg _$$Lambda$FileSystemProvider$ckJway_R_1_Nz9h7IMaXmGekKKg = new _$$Lambda$FileSystemProvider$ckJway_R_1_Nz9h7IMaXmGekKKg();
      this(this, paramString1, file1);
      return ParcelFileDescriptor.open(file2, i, handler, _$$Lambda$FileSystemProvider$ckJway_R_1_Nz9h7IMaXmGekKKg);
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to open for writing: ");
      stringBuilder.append(iOException);
      throw new FileNotFoundException(stringBuilder.toString());
    } 
  }
  
  private boolean matchSearchQueryArguments(File paramFile, Bundle paramBundle) {
    String str2;
    if (paramFile == null)
      return false; 
    String str1 = paramFile.getName();
    if (paramFile.isDirectory()) {
      str2 = "vnd.android.document/directory";
    } else {
      int i = str1.lastIndexOf('.');
      if (i < 0)
        return false; 
      str2 = str1.substring(i + 1);
      str2 = MimeTypeMap.getSingleton().getMimeTypeFromExtension(str2);
    } 
    long l1 = paramFile.lastModified(), l2 = paramFile.length();
    return DocumentsContract.matchSearchQueryArguments(paramBundle, str1, str2, l1, l2);
  }
  
  private void scanFile(File paramFile) {
    Intent intent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
    intent.setData(Uri.fromFile(paramFile));
    getContext().sendBroadcast(intent);
  }
  
  public AssetFileDescriptor openDocumentThumbnail(String paramString, Point paramPoint, CancellationSignal paramCancellationSignal) throws FileNotFoundException {
    File file = getFileForDocId(paramString);
    return DocumentsContract.openImageThumbnail(file);
  }
  
  protected MatrixCursor.RowBuilder includeFile(MatrixCursor paramMatrixCursor, String paramString, File paramFile) throws FileNotFoundException {
    String[] arrayOfString = paramMatrixCursor.getColumnNames();
    MatrixCursor.RowBuilder rowBuilder = paramMatrixCursor.newRow();
    if (paramString == null) {
      paramString = getDocIdForFile(paramFile);
    } else {
      paramFile = getFileForDocId(paramString);
    } 
    String str = getDocumentType(paramString, paramFile);
    rowBuilder.add("document_id", paramString);
    rowBuilder.add("mime_type", str);
    int i = ArrayUtils.indexOf((Object[])arrayOfString, "flags");
    if (i != -1) {
      int k = 0;
      if (paramFile.canWrite())
        if (str.equals("vnd.android.document/directory")) {
          int n = 0x0 | 0x8 | 0x4 | 0x40 | 0x100;
          k = n;
          if (shouldBlockFromTree(paramString))
            k = n | 0x8000; 
        } else {
          k = 0x0 | 0x2 | 0x4 | 0x40 | 0x100;
        }  
      int m = k;
      if (str.startsWith("image/"))
        m = k | 0x1; 
      k = m;
      if (typeSupportsMetadata(str))
        k = m | 0x4000; 
      rowBuilder.add(i, Integer.valueOf(k));
    } 
    int j = ArrayUtils.indexOf((Object[])arrayOfString, "_display_name");
    if (j != -1)
      rowBuilder.add(j, paramFile.getName()); 
    j = ArrayUtils.indexOf((Object[])arrayOfString, "last_modified");
    if (j != -1) {
      long l = paramFile.lastModified();
      if (l > 31536000000L)
        rowBuilder.add(j, Long.valueOf(l)); 
    } 
    j = ArrayUtils.indexOf((Object[])arrayOfString, "_size");
    if (j != -1)
      rowBuilder.add(j, Long.valueOf(paramFile.length())); 
    return rowBuilder;
  }
  
  static {
    PATTERN_HIDDEN_PATH = Pattern.compile("(?i)^/storage/[^/]+/(?:[0-9]+/)?Android/(?:data|obb|sandbox)$");
  }
  
  protected boolean shouldHide(File paramFile) {
    return PATTERN_HIDDEN_PATH.matcher(paramFile.getAbsolutePath()).matches();
  }
  
  private boolean shouldShow(File paramFile) {
    return shouldHide(paramFile) ^ true;
  }
  
  protected boolean shouldBlockFromTree(String paramString) {
    return false;
  }
  
  protected boolean typeSupportsMetadata(String paramString) {
    return (MetadataReader.isSupportedMimeType(paramString) || 
      "vnd.android.document/directory".equals(paramString));
  }
  
  protected final File getFileForDocId(String paramString) throws FileNotFoundException {
    return getFileForDocId(paramString, false);
  }
  
  private String[] resolveProjection(String[] paramArrayOfString) {
    if (paramArrayOfString == null)
      paramArrayOfString = this.mDefaultProjection; 
    return paramArrayOfString;
  }
  
  private void startObserving(File paramFile, Uri paramUri, DirectoryCursor paramDirectoryCursor) {
    synchronized (this.mObservers) {
      DirectoryObserver directoryObserver1 = this.mObservers.get(paramFile);
      DirectoryObserver directoryObserver2 = directoryObserver1;
      if (directoryObserver1 == null) {
        directoryObserver2 = new DirectoryObserver();
        this(paramFile, getContext().getContentResolver(), paramUri);
        directoryObserver2.startWatching();
        this.mObservers.put(paramFile, directoryObserver2);
      } 
      directoryObserver2.mCursors.add(paramDirectoryCursor);
      return;
    } 
  }
  
  private void stopObserving(File paramFile, DirectoryCursor paramDirectoryCursor) {
    synchronized (this.mObservers) {
      DirectoryObserver directoryObserver = this.mObservers.get(paramFile);
      if (directoryObserver == null)
        return; 
      directoryObserver.mCursors.remove(paramDirectoryCursor);
      if (directoryObserver.mCursors.size() == 0) {
        this.mObservers.remove(paramFile);
        directoryObserver.stopWatching();
      } 
      return;
    } 
  }
  
  protected abstract Uri buildNotificationUri(String paramString);
  
  protected abstract String getDocIdForFile(File paramFile) throws FileNotFoundException;
  
  protected abstract File getFileForDocId(String paramString, boolean paramBoolean) throws FileNotFoundException;
  
  class DirectoryObserver extends FileObserver {
    private static final int NOTIFY_EVENTS = 4044;
    
    private final CopyOnWriteArrayList<FileSystemProvider.DirectoryCursor> mCursors;
    
    private final File mFile;
    
    private final Uri mNotifyUri;
    
    private final ContentResolver mResolver;
    
    DirectoryObserver(FileSystemProvider this$0, ContentResolver param1ContentResolver, Uri param1Uri) {
      super(this$0.getAbsolutePath(), 4044);
      this.mFile = (File)this$0;
      this.mResolver = param1ContentResolver;
      this.mNotifyUri = param1Uri;
      this.mCursors = new CopyOnWriteArrayList<>();
    }
    
    public void onEvent(int param1Int, String param1String) {
      if ((param1Int & 0xFCC) != 0) {
        for (FileSystemProvider.DirectoryCursor directoryCursor : this.mCursors)
          directoryCursor.notifyChanged(); 
        this.mResolver.notifyChange(this.mNotifyUri, null, false);
      } 
    }
    
    public String toString() {
      String str = this.mFile.getAbsolutePath();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("DirectoryObserver{file=");
      stringBuilder.append(str);
      stringBuilder.append(", ref=");
      stringBuilder.append(this.mCursors.size());
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }
  
  private class DirectoryCursor extends MatrixCursor {
    private final File mFile;
    
    final FileSystemProvider this$0;
    
    public DirectoryCursor(String[] param1ArrayOfString, String param1String, File param1File) {
      super(param1ArrayOfString);
      Uri uri = FileSystemProvider.this.buildNotificationUri(param1String);
      ContentResolver contentResolver = FileSystemProvider.this.getContext().getContentResolver();
      List<Uri> list = Arrays.asList(new Uri[] { uri });
      int i = FileSystemProvider.this.getContext().getContentResolver().getUserId();
      setNotificationUris(contentResolver, list, i, false);
      this.mFile = param1File;
      FileSystemProvider.this.startObserving(param1File, uri, this);
    }
    
    public void notifyChanged() {
      onChange(false);
    }
    
    public void close() {
      super.close();
      FileSystemProvider.this.stopObserving(this.mFile, this);
    }
  }
}
