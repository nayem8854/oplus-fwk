package com.android.internal.content;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

public class ReferrerIntent extends Intent {
  public ReferrerIntent(Intent paramIntent, String paramString) {
    super(paramIntent);
    this.mReferrer = paramString;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    super.writeToParcel(paramParcel, paramInt);
    paramParcel.writeString(this.mReferrer);
  }
  
  ReferrerIntent(Parcel paramParcel) {
    readFromParcel(paramParcel);
    this.mReferrer = paramParcel.readString();
  }
  
  public static final Parcelable.Creator<ReferrerIntent> CREATOR = (Parcelable.Creator<ReferrerIntent>)new Object();
  
  public final String mReferrer;
  
  public boolean equals(Object paramObject) {
    boolean bool1 = false;
    if (paramObject == null || !(paramObject instanceof ReferrerIntent))
      return false; 
    paramObject = paramObject;
    boolean bool2 = bool1;
    if (filterEquals((Intent)paramObject)) {
      bool2 = bool1;
      if (Objects.equals(this.mReferrer, ((ReferrerIntent)paramObject).mReferrer))
        bool2 = true; 
    } 
    return bool2;
  }
  
  public int hashCode() {
    int i = filterHashCode();
    int j = Objects.hashCode(this.mReferrer);
    return (17 * 31 + i) * 31 + j;
  }
}
