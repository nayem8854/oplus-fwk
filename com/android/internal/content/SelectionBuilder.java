package com.android.internal.content;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import java.util.ArrayList;

public class SelectionBuilder {
  private StringBuilder mSelection = new StringBuilder();
  
  private ArrayList<String> mSelectionArgs = new ArrayList<>();
  
  public SelectionBuilder reset() {
    this.mSelection.setLength(0);
    this.mSelectionArgs.clear();
    return this;
  }
  
  public SelectionBuilder append(String paramString, Object... paramVarArgs) {
    if (TextUtils.isEmpty(paramString)) {
      if (paramVarArgs == null || paramVarArgs.length <= 0)
        return this; 
      throw new IllegalArgumentException("Valid selection required when including arguments");
    } 
    if (this.mSelection.length() > 0)
      this.mSelection.append(" AND "); 
    StringBuilder stringBuilder = this.mSelection;
    stringBuilder.append("(");
    stringBuilder.append(paramString);
    stringBuilder.append(")");
    if (paramVarArgs != null) {
      int i;
      byte b;
      for (i = paramVarArgs.length, b = 0; b < i; ) {
        Object object = paramVarArgs[b];
        this.mSelectionArgs.add(String.valueOf(object));
        b++;
      } 
    } 
    return this;
  }
  
  public String getSelection() {
    return this.mSelection.toString();
  }
  
  public String[] getSelectionArgs() {
    ArrayList<String> arrayList = this.mSelectionArgs;
    return arrayList.<String>toArray(new String[arrayList.size()]);
  }
  
  public Cursor query(SQLiteDatabase paramSQLiteDatabase, String paramString1, String[] paramArrayOfString, String paramString2) {
    return query(paramSQLiteDatabase, paramString1, paramArrayOfString, null, null, paramString2, null);
  }
  
  public Cursor query(SQLiteDatabase paramSQLiteDatabase, String paramString1, String[] paramArrayOfString, String paramString2, String paramString3, String paramString4, String paramString5) {
    return paramSQLiteDatabase.query(paramString1, paramArrayOfString, getSelection(), getSelectionArgs(), paramString2, paramString3, paramString4, paramString5);
  }
  
  public int update(SQLiteDatabase paramSQLiteDatabase, String paramString, ContentValues paramContentValues) {
    return paramSQLiteDatabase.update(paramString, paramContentValues, getSelection(), getSelectionArgs());
  }
  
  public int delete(SQLiteDatabase paramSQLiteDatabase, String paramString) {
    return paramSQLiteDatabase.delete(paramString, getSelection(), getSelectionArgs());
  }
}
