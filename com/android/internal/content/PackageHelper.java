package com.android.internal.content;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInstaller;
import android.content.pm.PackageManager;
import android.content.pm.PackageParser;
import android.content.pm.dex.DexMetadataHelper;
import android.os.Environment;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.storage.IStorageManager;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.util.Log;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.util.UUID;

public class PackageHelper {
  public static final int APP_INSTALL_AUTO = 0;
  
  public static final int APP_INSTALL_EXTERNAL = 2;
  
  public static final int APP_INSTALL_INTERNAL = 1;
  
  public static final int RECOMMEND_FAILED_ALREADY_EXISTS = -4;
  
  public static final int RECOMMEND_FAILED_INSUFFICIENT_STORAGE = -1;
  
  public static final int RECOMMEND_FAILED_INVALID_APK = -2;
  
  public static final int RECOMMEND_FAILED_INVALID_LOCATION = -3;
  
  public static final int RECOMMEND_FAILED_INVALID_URI = -6;
  
  public static final int RECOMMEND_FAILED_VERSION_DOWNGRADE = -7;
  
  public static final int RECOMMEND_FAILED_WRONG_INSTALLED_VERSION = -8;
  
  public static final int RECOMMEND_INSTALL_EPHEMERAL = 3;
  
  public static final int RECOMMEND_INSTALL_EXTERNAL = 2;
  
  public static final int RECOMMEND_INSTALL_INTERNAL = 1;
  
  public static final int RECOMMEND_MEDIA_UNAVAILABLE = -5;
  
  private static final String TAG = "PackageHelper";
  
  private static TestableInterface sDefaultTestableInterface = null;
  
  public static IStorageManager getStorageManager() throws RemoteException {
    IBinder iBinder = ServiceManager.getService("mount");
    if (iBinder != null)
      return IStorageManager.Stub.asInterface(iBinder); 
    Log.e("PackageHelper", "Can't get storagemanager service");
    throw new RemoteException("Could not contact storagemanager service");
  }
  
  public static abstract class TestableInterface {
    public abstract boolean getAllow3rdPartyOnInternalConfig(Context param1Context);
    
    public abstract File getDataDirectory();
    
    public abstract ApplicationInfo getExistingAppInfo(Context param1Context, String param1String);
    
    public abstract boolean getForceAllowOnExternalSetting(Context param1Context);
    
    public abstract StorageManager getStorageManager(Context param1Context);
  }
  
  private static TestableInterface getDefaultTestableInterface() {
    // Byte code:
    //   0: ldc com/android/internal/content/PackageHelper
    //   2: monitorenter
    //   3: getstatic com/android/internal/content/PackageHelper.sDefaultTestableInterface : Lcom/android/internal/content/PackageHelper$TestableInterface;
    //   6: ifnonnull -> 21
    //   9: new com/android/internal/content/PackageHelper$1
    //   12: astore_0
    //   13: aload_0
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: putstatic com/android/internal/content/PackageHelper.sDefaultTestableInterface : Lcom/android/internal/content/PackageHelper$TestableInterface;
    //   21: getstatic com/android/internal/content/PackageHelper.sDefaultTestableInterface : Lcom/android/internal/content/PackageHelper$TestableInterface;
    //   24: astore_0
    //   25: ldc com/android/internal/content/PackageHelper
    //   27: monitorexit
    //   28: aload_0
    //   29: areturn
    //   30: astore_0
    //   31: ldc com/android/internal/content/PackageHelper
    //   33: monitorexit
    //   34: aload_0
    //   35: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #102	-> 3
    //   #103	-> 9
    //   #138	-> 21
    //   #101	-> 30
    // Exception table:
    //   from	to	target	type
    //   3	9	30	finally
    //   9	21	30	finally
    //   21	25	30	finally
  }
  
  @Deprecated
  public static String resolveInstallVolume(Context paramContext, String paramString, int paramInt, long paramLong, TestableInterface paramTestableInterface) throws IOException {
    PackageInstaller.SessionParams sessionParams = new PackageInstaller.SessionParams(-1);
    sessionParams.appPackageName = paramString;
    sessionParams.installLocation = paramInt;
    sessionParams.sizeBytes = paramLong;
    return resolveInstallVolume(paramContext, sessionParams, paramTestableInterface);
  }
  
  public static String resolveInstallVolume(Context paramContext, PackageInstaller.SessionParams paramSessionParams) throws IOException {
    TestableInterface testableInterface = getDefaultTestableInterface();
    return resolveInstallVolume(paramContext, paramSessionParams.appPackageName, paramSessionParams.installLocation, paramSessionParams.sizeBytes, testableInterface);
  }
  
  private static boolean checkFitOnVolume(StorageManager paramStorageManager, String paramString, PackageInstaller.SessionParams paramSessionParams) throws IOException {
    boolean bool = false;
    if (paramString == null)
      return false; 
    int i = translateAllocateFlags(paramSessionParams.installFlags);
    UUID uUID = paramStorageManager.getUuidForPath(new File(paramString));
    long l1 = paramStorageManager.getAllocatableBytes(uUID, i | 0x8);
    if (paramSessionParams.sizeBytes <= l1)
      return true; 
    long l2 = paramStorageManager.getAllocatableBytes(uUID, i | 0x10);
    if (paramSessionParams.sizeBytes <= l1 + l2)
      bool = true; 
    return bool;
  }
  
  public static String resolveInstallVolume(Context paramContext, PackageInstaller.SessionParams paramSessionParams, TestableInterface paramTestableInterface) throws IOException {
    // Byte code:
    //   0: aload_2
    //   1: aload_0
    //   2: invokevirtual getStorageManager : (Landroid/content/Context;)Landroid/os/storage/StorageManager;
    //   5: astore_3
    //   6: aload_2
    //   7: aload_0
    //   8: invokevirtual getForceAllowOnExternalSetting : (Landroid/content/Context;)Z
    //   11: istore #4
    //   13: aload_2
    //   14: aload_0
    //   15: invokevirtual getAllow3rdPartyOnInternalConfig : (Landroid/content/Context;)Z
    //   18: istore #5
    //   20: aload_2
    //   21: aload_0
    //   22: aload_1
    //   23: getfield appPackageName : Ljava/lang/String;
    //   26: invokevirtual getExistingAppInfo : (Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/ApplicationInfo;
    //   29: astore #6
    //   31: new android/util/ArrayMap
    //   34: dup
    //   35: invokespecial <init> : ()V
    //   38: astore #7
    //   40: aconst_null
    //   41: astore_0
    //   42: aload_3
    //   43: invokevirtual getVolumes : ()Ljava/util/List;
    //   46: invokeinterface iterator : ()Ljava/util/Iterator;
    //   51: astore #8
    //   53: aload #8
    //   55: invokeinterface hasNext : ()Z
    //   60: ifeq -> 155
    //   63: aload #8
    //   65: invokeinterface next : ()Ljava/lang/Object;
    //   70: checkcast android/os/storage/VolumeInfo
    //   73: astore #9
    //   75: aload_0
    //   76: astore_2
    //   77: aload #9
    //   79: getfield type : I
    //   82: iconst_1
    //   83: if_icmpne -> 150
    //   86: aload_0
    //   87: astore_2
    //   88: aload #9
    //   90: invokevirtual isMountedWritable : ()Z
    //   93: ifeq -> 150
    //   96: ldc_w 'private'
    //   99: aload #9
    //   101: getfield id : Ljava/lang/String;
    //   104: invokevirtual equals : (Ljava/lang/Object;)Z
    //   107: istore #10
    //   109: iload #10
    //   111: ifeq -> 120
    //   114: aload #9
    //   116: getfield path : Ljava/lang/String;
    //   119: astore_0
    //   120: iload #10
    //   122: ifeq -> 132
    //   125: aload_0
    //   126: astore_2
    //   127: iload #5
    //   129: ifeq -> 150
    //   132: aload #7
    //   134: aload #9
    //   136: getfield fsUuid : Ljava/lang/String;
    //   139: aload #9
    //   141: getfield path : Ljava/lang/String;
    //   144: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   147: pop
    //   148: aload_0
    //   149: astore_2
    //   150: aload_2
    //   151: astore_0
    //   152: goto -> 53
    //   155: aload #6
    //   157: ifnull -> 244
    //   160: aload #6
    //   162: invokevirtual isSystemApp : ()Z
    //   165: ifeq -> 244
    //   168: aload_3
    //   169: aload_0
    //   170: aload_1
    //   171: invokestatic checkFitOnVolume : (Landroid/os/storage/StorageManager;Ljava/lang/String;Landroid/content/pm/PackageInstaller$SessionParams;)Z
    //   174: ifeq -> 181
    //   177: getstatic android/os/storage/StorageManager.UUID_PRIVATE_INTERNAL : Ljava/lang/String;
    //   180: areturn
    //   181: new java/lang/StringBuilder
    //   184: dup
    //   185: invokespecial <init> : ()V
    //   188: astore_0
    //   189: aload_0
    //   190: ldc_w 'Not enough space on existing volume '
    //   193: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   196: pop
    //   197: aload_0
    //   198: aload #6
    //   200: getfield volumeUuid : Ljava/lang/String;
    //   203: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   206: pop
    //   207: aload_0
    //   208: ldc_w ' for system app '
    //   211: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   214: pop
    //   215: aload_0
    //   216: aload_1
    //   217: getfield appPackageName : Ljava/lang/String;
    //   220: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   223: pop
    //   224: aload_0
    //   225: ldc_w ' upgrade'
    //   228: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   231: pop
    //   232: new java/io/IOException
    //   235: dup
    //   236: aload_0
    //   237: invokevirtual toString : ()Ljava/lang/String;
    //   240: invokespecial <init> : (Ljava/lang/String;)V
    //   243: athrow
    //   244: iload #4
    //   246: ifne -> 382
    //   249: aload_1
    //   250: getfield installLocation : I
    //   253: iconst_1
    //   254: if_icmpne -> 382
    //   257: aload #6
    //   259: ifnull -> 342
    //   262: aload #6
    //   264: getfield volumeUuid : Ljava/lang/String;
    //   267: getstatic android/os/storage/StorageManager.UUID_PRIVATE_INTERNAL : Ljava/lang/String;
    //   270: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   273: ifeq -> 279
    //   276: goto -> 342
    //   279: new java/lang/StringBuilder
    //   282: dup
    //   283: invokespecial <init> : ()V
    //   286: astore_0
    //   287: aload_0
    //   288: ldc_w 'Cannot automatically move '
    //   291: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   294: pop
    //   295: aload_0
    //   296: aload_1
    //   297: getfield appPackageName : Ljava/lang/String;
    //   300: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   303: pop
    //   304: aload_0
    //   305: ldc_w ' from '
    //   308: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   311: pop
    //   312: aload_0
    //   313: aload #6
    //   315: getfield volumeUuid : Ljava/lang/String;
    //   318: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   321: pop
    //   322: aload_0
    //   323: ldc_w ' to internal storage'
    //   326: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   329: pop
    //   330: new java/io/IOException
    //   333: dup
    //   334: aload_0
    //   335: invokevirtual toString : ()Ljava/lang/String;
    //   338: invokespecial <init> : (Ljava/lang/String;)V
    //   341: athrow
    //   342: iload #5
    //   344: ifeq -> 371
    //   347: aload_3
    //   348: aload_0
    //   349: aload_1
    //   350: invokestatic checkFitOnVolume : (Landroid/os/storage/StorageManager;Ljava/lang/String;Landroid/content/pm/PackageInstaller$SessionParams;)Z
    //   353: ifeq -> 360
    //   356: getstatic android/os/storage/StorageManager.UUID_PRIVATE_INTERNAL : Ljava/lang/String;
    //   359: areturn
    //   360: new java/io/IOException
    //   363: dup
    //   364: ldc_w 'Requested internal only, but not enough space'
    //   367: invokespecial <init> : (Ljava/lang/String;)V
    //   370: athrow
    //   371: new java/io/IOException
    //   374: dup
    //   375: ldc_w 'Not allowed to install non-system apps on internal storage'
    //   378: invokespecial <init> : (Ljava/lang/String;)V
    //   381: athrow
    //   382: aload #6
    //   384: ifnull -> 513
    //   387: aconst_null
    //   388: astore_2
    //   389: aload #6
    //   391: getfield volumeUuid : Ljava/lang/String;
    //   394: getstatic android/os/storage/StorageManager.UUID_PRIVATE_INTERNAL : Ljava/lang/String;
    //   397: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   400: ifeq -> 406
    //   403: goto -> 435
    //   406: aload_2
    //   407: astore_0
    //   408: aload #7
    //   410: aload #6
    //   412: getfield volumeUuid : Ljava/lang/String;
    //   415: invokevirtual containsKey : (Ljava/lang/Object;)Z
    //   418: ifeq -> 435
    //   421: aload #7
    //   423: aload #6
    //   425: getfield volumeUuid : Ljava/lang/String;
    //   428: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   431: checkcast java/lang/String
    //   434: astore_0
    //   435: aload_3
    //   436: aload_0
    //   437: aload_1
    //   438: invokestatic checkFitOnVolume : (Landroid/os/storage/StorageManager;Ljava/lang/String;Landroid/content/pm/PackageInstaller$SessionParams;)Z
    //   441: ifeq -> 450
    //   444: aload #6
    //   446: getfield volumeUuid : Ljava/lang/String;
    //   449: areturn
    //   450: new java/lang/StringBuilder
    //   453: dup
    //   454: invokespecial <init> : ()V
    //   457: astore_0
    //   458: aload_0
    //   459: ldc_w 'Not enough space on existing volume '
    //   462: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   465: pop
    //   466: aload_0
    //   467: aload #6
    //   469: getfield volumeUuid : Ljava/lang/String;
    //   472: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   475: pop
    //   476: aload_0
    //   477: ldc_w ' for '
    //   480: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   483: pop
    //   484: aload_0
    //   485: aload_1
    //   486: getfield appPackageName : Ljava/lang/String;
    //   489: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   492: pop
    //   493: aload_0
    //   494: ldc_w ' upgrade'
    //   497: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   500: pop
    //   501: new java/io/IOException
    //   504: dup
    //   505: aload_0
    //   506: invokevirtual toString : ()Ljava/lang/String;
    //   509: invokespecial <init> : (Ljava/lang/String;)V
    //   512: athrow
    //   513: aload #7
    //   515: invokevirtual size : ()I
    //   518: iconst_1
    //   519: if_icmpne -> 549
    //   522: aload_3
    //   523: aload #7
    //   525: iconst_0
    //   526: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   529: checkcast java/lang/String
    //   532: aload_1
    //   533: invokestatic checkFitOnVolume : (Landroid/os/storage/StorageManager;Ljava/lang/String;Landroid/content/pm/PackageInstaller$SessionParams;)Z
    //   536: ifeq -> 675
    //   539: aload #7
    //   541: iconst_0
    //   542: invokevirtual keyAt : (I)Ljava/lang/Object;
    //   545: checkcast java/lang/String
    //   548: areturn
    //   549: aconst_null
    //   550: astore_0
    //   551: ldc2_w -9223372036854775808
    //   554: lstore #11
    //   556: aload #7
    //   558: invokevirtual keySet : ()Ljava/util/Set;
    //   561: invokeinterface iterator : ()Ljava/util/Iterator;
    //   566: astore #6
    //   568: aload #6
    //   570: invokeinterface hasNext : ()Z
    //   575: ifeq -> 663
    //   578: aload #6
    //   580: invokeinterface next : ()Ljava/lang/Object;
    //   585: checkcast java/lang/String
    //   588: astore_2
    //   589: aload #7
    //   591: aload_2
    //   592: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   595: checkcast java/lang/String
    //   598: astore #8
    //   600: aload_3
    //   601: new java/io/File
    //   604: dup
    //   605: aload #8
    //   607: invokespecial <init> : (Ljava/lang/String;)V
    //   610: invokevirtual getUuidForPath : (Ljava/io/File;)Ljava/util/UUID;
    //   613: astore #8
    //   615: aload_1
    //   616: getfield installFlags : I
    //   619: istore #13
    //   621: iload #13
    //   623: invokestatic translateAllocateFlags : (I)I
    //   626: istore #13
    //   628: aload_3
    //   629: aload #8
    //   631: iload #13
    //   633: invokevirtual getAllocatableBytes : (Ljava/util/UUID;I)J
    //   636: lstore #14
    //   638: lload #11
    //   640: lstore #16
    //   642: lload #14
    //   644: lload #11
    //   646: lcmp
    //   647: iflt -> 656
    //   650: lload #14
    //   652: lstore #16
    //   654: aload_2
    //   655: astore_0
    //   656: lload #16
    //   658: lstore #11
    //   660: goto -> 568
    //   663: lload #11
    //   665: aload_1
    //   666: getfield sizeBytes : J
    //   669: lcmp
    //   670: iflt -> 675
    //   673: aload_0
    //   674: areturn
    //   675: new java/lang/StringBuilder
    //   678: dup
    //   679: invokespecial <init> : ()V
    //   682: astore_0
    //   683: aload_0
    //   684: ldc_w 'No special requests, but no room on allowed volumes.  allow3rdPartyOnInternal? '
    //   687: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   690: pop
    //   691: aload_0
    //   692: iload #5
    //   694: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   697: pop
    //   698: new java/io/IOException
    //   701: dup
    //   702: aload_0
    //   703: invokevirtual toString : ()Ljava/lang/String;
    //   706: invokespecial <init> : (Ljava/lang/String;)V
    //   709: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #189	-> 0
    //   #190	-> 6
    //   #191	-> 13
    //   #192	-> 13
    //   #195	-> 20
    //   #198	-> 31
    //   #199	-> 40
    //   #200	-> 42
    //   #201	-> 75
    //   #202	-> 96
    //   #203	-> 109
    //   #204	-> 114
    //   #206	-> 120
    //   #207	-> 132
    //   #210	-> 150
    //   #213	-> 155
    //   #214	-> 168
    //   #215	-> 177
    //   #217	-> 181
    //   #224	-> 244
    //   #226	-> 257
    //   #228	-> 279
    //   #232	-> 342
    //   #236	-> 347
    //   #237	-> 356
    //   #239	-> 360
    //   #233	-> 371
    //   #244	-> 382
    //   #245	-> 387
    //   #246	-> 389
    //   #247	-> 403
    //   #248	-> 406
    //   #249	-> 421
    //   #252	-> 435
    //   #253	-> 444
    //   #255	-> 450
    //   #262	-> 513
    //   #263	-> 522
    //   #264	-> 539
    //   #267	-> 549
    //   #268	-> 551
    //   #269	-> 556
    //   #270	-> 589
    //   #271	-> 600
    //   #275	-> 615
    //   #276	-> 621
    //   #275	-> 628
    //   #278	-> 638
    //   #279	-> 650
    //   #280	-> 650
    //   #282	-> 656
    //   #284	-> 663
    //   #285	-> 673
    //   #290	-> 675
  }
  
  public static boolean fitsOnInternal(Context paramContext, PackageInstaller.SessionParams paramSessionParams) throws IOException {
    StorageManager storageManager = (StorageManager)paramContext.getSystemService(StorageManager.class);
    UUID uUID = storageManager.getUuidForPath(Environment.getDataDirectory());
    int i = translateAllocateFlags(paramSessionParams.installFlags);
    long l1 = storageManager.getAllocatableBytes(uUID, i | 0x8);
    long l2 = paramSessionParams.sizeBytes;
    boolean bool = true;
    if (l2 <= l1)
      return true; 
    l2 = storageManager.getAllocatableBytes(uUID, i | 0x10);
    if (paramSessionParams.sizeBytes > l1 + l2)
      bool = false; 
    return bool;
  }
  
  public static boolean fitsOnExternal(Context paramContext, PackageInstaller.SessionParams paramSessionParams) {
    StorageManager storageManager = (StorageManager)paramContext.getSystemService(StorageManager.class);
    StorageVolume storageVolume = storageManager.getPrimaryVolume();
    if (paramSessionParams.sizeBytes > 0L && !storageVolume.isEmulated() && 
      "mounted".equals(storageVolume.getState())) {
      long l = paramSessionParams.sizeBytes;
      if (l <= storageManager.getStorageBytesUntilLow(storageVolume.getPathFile()))
        return true; 
    } 
    return false;
  }
  
  @Deprecated
  public static int resolveInstallLocation(Context paramContext, String paramString, int paramInt1, long paramLong, int paramInt2) {
    PackageInstaller.SessionParams sessionParams = new PackageInstaller.SessionParams(-1);
    sessionParams.appPackageName = paramString;
    sessionParams.installLocation = paramInt1;
    sessionParams.sizeBytes = paramLong;
    sessionParams.installFlags = paramInt2;
    try {
      return resolveInstallLocation(paramContext, sessionParams);
    } catch (IOException iOException) {
      throw new IllegalStateException(iOException);
    } 
  }
  
  public static int resolveInstallLocation(Context paramContext, PackageInstaller.SessionParams paramSessionParams) throws IOException {
    boolean bool3;
    ApplicationInfo applicationInfo = null;
    try {
      ApplicationInfo applicationInfo1 = paramContext.getPackageManager().getApplicationInfo(paramSessionParams.appPackageName, 4194304);
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {}
    boolean bool1 = false;
    int i = paramSessionParams.installFlags;
    boolean bool2 = true;
    if ((i & 0x800) != 0) {
      i = 1;
      bool1 = true;
      bool3 = false;
    } else if ((paramSessionParams.installFlags & 0x10) != 0) {
      i = 1;
      bool3 = false;
    } else if (paramSessionParams.installLocation == 1) {
      i = 1;
      bool3 = false;
    } else if (paramSessionParams.installLocation == 2) {
      i = 2;
      bool3 = true;
    } else if (paramSessionParams.installLocation == 0) {
      if (applicationInfo != null) {
        if ((applicationInfo.flags & 0x40000) != 0) {
          i = 2;
        } else {
          i = 1;
        } 
      } else {
        i = 1;
      } 
      bool3 = true;
    } else {
      i = 1;
      bool3 = false;
    } 
    boolean bool4 = false;
    if (bool3 || i == 1)
      bool4 = fitsOnInternal(paramContext, paramSessionParams); 
    boolean bool5 = false;
    if (bool3 || i == 2)
      bool5 = fitsOnExternal(paramContext, paramSessionParams); 
    if (i == 1) {
      if (bool4) {
        if (bool1) {
          i = 3;
        } else {
          i = bool2;
        } 
        return i;
      } 
    } else if (i == 2 && 
      bool5) {
      return 2;
    } 
    if (bool3) {
      if (bool4)
        return 1; 
      if (bool5)
        return 2; 
    } 
    return -1;
  }
  
  @Deprecated
  public static long calculateInstalledSize(PackageParser.PackageLite paramPackageLite, boolean paramBoolean, String paramString) throws IOException {
    return calculateInstalledSize(paramPackageLite, paramString);
  }
  
  public static long calculateInstalledSize(PackageParser.PackageLite paramPackageLite, String paramString) throws IOException {
    return calculateInstalledSize(paramPackageLite, paramString, (FileDescriptor)null);
  }
  
  public static long calculateInstalledSize(PackageParser.PackageLite paramPackageLite, String paramString, FileDescriptor paramFileDescriptor) throws IOException {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: aload_2
    //   3: ifnull -> 15
    //   6: aload_0
    //   7: aload_2
    //   8: invokestatic createFd : (Landroid/content/pm/PackageParser$PackageLite;Ljava/io/FileDescriptor;)Lcom/android/internal/content/NativeLibraryHelper$Handle;
    //   11: astore_2
    //   12: goto -> 20
    //   15: aload_0
    //   16: invokestatic create : (Landroid/content/pm/PackageParser$PackageLite;)Lcom/android/internal/content/NativeLibraryHelper$Handle;
    //   19: astore_2
    //   20: aload_2
    //   21: astore_3
    //   22: aload_0
    //   23: aload_2
    //   24: aload_1
    //   25: invokestatic calculateInstalledSize : (Landroid/content/pm/PackageParser$PackageLite;Lcom/android/internal/content/NativeLibraryHelper$Handle;Ljava/lang/String;)J
    //   28: lstore #4
    //   30: aload_2
    //   31: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   34: lload #4
    //   36: lreturn
    //   37: astore_0
    //   38: aload_3
    //   39: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   42: aload_0
    //   43: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #432	-> 0
    //   #434	-> 2
    //   #435	-> 15
    //   #436	-> 20
    //   #438	-> 30
    //   #436	-> 34
    //   #438	-> 37
    //   #439	-> 42
    // Exception table:
    //   from	to	target	type
    //   6	12	37	finally
    //   15	20	37	finally
    //   22	30	37	finally
  }
  
  @Deprecated
  public static long calculateInstalledSize(PackageParser.PackageLite paramPackageLite, boolean paramBoolean, NativeLibraryHelper.Handle paramHandle, String paramString) throws IOException {
    return calculateInstalledSize(paramPackageLite, paramHandle, paramString);
  }
  
  public static long calculateInstalledSize(PackageParser.PackageLite paramPackageLite, NativeLibraryHelper.Handle paramHandle, String paramString) throws IOException {
    long l1 = 0L;
    for (String str : paramPackageLite.getAllCodePaths()) {
      File file = new File(str);
      l1 += file.length();
    } 
    long l2 = DexMetadataHelper.getPackageDexMetadataSize(paramPackageLite);
    long l3 = NativeLibraryHelper.sumNativeBinariesWithOverride(paramHandle, paramString);
    return l1 + l2 + l3;
  }
  
  public static String replaceEnd(String paramString1, String paramString2, String paramString3) {
    if (paramString1.endsWith(paramString2)) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString1.substring(0, paramString1.length() - paramString2.length()));
      stringBuilder1.append(paramString3);
      return stringBuilder1.toString();
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Expected ");
    stringBuilder.append(paramString1);
    stringBuilder.append(" to end with ");
    stringBuilder.append(paramString2);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static int translateAllocateFlags(int paramInt) {
    if ((0x8000 & paramInt) != 0)
      return 1; 
    return 0;
  }
}
