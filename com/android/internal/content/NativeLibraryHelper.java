package com.android.internal.content;

import android.content.pm.PackageParser;
import android.os.Build;
import android.os.IBinder;
import android.os.SELinux;
import android.os.ServiceManager;
import android.os.incremental.IIncrementalService;
import android.os.incremental.IncrementalManager;
import android.os.incremental.IncrementalStorage;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.util.ArraySet;
import android.util.Slog;
import dalvik.system.CloseGuard;
import dalvik.system.VMRuntime;
import java.io.Closeable;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class NativeLibraryHelper {
  private static final int BITCODE_PRESENT = 1;
  
  public static final String CLEAR_ABI_OVERRIDE = "-";
  
  private static final boolean DEBUG_NATIVE = false;
  
  public static final String LIB64_DIR_NAME = "lib64";
  
  public static final String LIB_DIR_NAME = "lib";
  
  private static final String TAG = "NativeHelper";
  
  public static class Handle implements Closeable {
    final long[] apkHandles;
    
    final String[] apkPaths;
    
    final boolean debuggable;
    
    final boolean extractNativeLibs;
    
    private volatile boolean mClosed;
    
    private final CloseGuard mGuard;
    
    final boolean multiArch;
    
    public static Handle create(File param1File) throws IOException {
      try {
        PackageParser.PackageLite packageLite = PackageParser.parsePackageLite(param1File, 0);
        return create(packageLite);
      } catch (android.content.pm.PackageParser.PackageParserException packageParserException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to parse package: ");
        stringBuilder.append(param1File);
        throw new IOException(stringBuilder.toString(), packageParserException);
      } 
    }
    
    public static Handle create(PackageParser.PackageLite param1PackageLite) throws IOException {
      return create(param1PackageLite.getAllCodePaths(), param1PackageLite.multiArch, param1PackageLite.extractNativeLibs, param1PackageLite.debuggable);
    }
    
    public static Handle create(List<String> param1List, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3) throws IOException {
      int i = param1List.size();
      String[] arrayOfString = new String[i];
      long[] arrayOfLong = new long[i];
      for (byte b = 0; b < i; b++) {
        String str = param1List.get(b);
        arrayOfString[b] = str;
        arrayOfLong[b] = NativeLibraryHelper.nativeOpenApk(str);
        if (arrayOfLong[b] == 0L) {
          for (i = 0; i < b; i++)
            NativeLibraryHelper.nativeClose(arrayOfLong[i]); 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Unable to open APK: ");
          stringBuilder.append(str);
          throw new IOException(stringBuilder.toString());
        } 
      } 
      return new Handle(arrayOfString, arrayOfLong, param1Boolean1, param1Boolean2, param1Boolean3);
    }
    
    public static Handle createFd(PackageParser.PackageLite param1PackageLite, FileDescriptor param1FileDescriptor) throws IOException {
      long[] arrayOfLong = new long[1];
      String str = param1PackageLite.baseCodePath;
      arrayOfLong[0] = NativeLibraryHelper.nativeOpenApkFd(param1FileDescriptor, str);
      if (arrayOfLong[0] != 0L) {
        boolean bool1 = param1PackageLite.multiArch, bool2 = param1PackageLite.extractNativeLibs, bool3 = param1PackageLite.debuggable;
        return new Handle(new String[] { str }, arrayOfLong, bool1, bool2, bool3);
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to open APK ");
      stringBuilder.append(str);
      stringBuilder.append(" from fd ");
      stringBuilder.append(param1FileDescriptor);
      throw new IOException(stringBuilder.toString());
    }
    
    Handle(String[] param1ArrayOfString, long[] param1ArrayOflong, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3) {
      CloseGuard closeGuard = CloseGuard.get();
      this.apkPaths = param1ArrayOfString;
      this.apkHandles = param1ArrayOflong;
      this.multiArch = param1Boolean1;
      this.extractNativeLibs = param1Boolean2;
      this.debuggable = param1Boolean3;
      closeGuard.open("close");
    }
    
    public void close() {
      for (long l : this.apkHandles)
        NativeLibraryHelper.nativeClose(l); 
      this.mGuard.close();
      this.mClosed = true;
    }
    
    protected void finalize() throws Throwable {
      null = this.mGuard;
      if (null != null)
        null.warnIfOpen(); 
      try {
        if (!this.mClosed)
          close(); 
        return;
      } finally {
        super.finalize();
      } 
    }
  }
  
  private static long sumNativeBinaries(Handle paramHandle, String paramString) {
    long l = 0L;
    for (long l1 : paramHandle.apkHandles)
      l += nativeSumNativeBinaries(l1, paramString, paramHandle.debuggable); 
    return l;
  }
  
  public static int copyNativeBinaries(Handle paramHandle, File paramFile, String paramString) {
    for (long l : paramHandle.apkHandles) {
      int i = nativeCopyNativeBinaries(l, paramFile.getPath(), paramString, paramHandle.extractNativeLibs, paramHandle.debuggable);
      if (i != 1)
        return i; 
    } 
    return 1;
  }
  
  public static int findSupportedAbi(Handle paramHandle, String[] paramArrayOfString) {
    // Byte code:
    //   0: bipush #-114
    //   2: istore_2
    //   3: aload_0
    //   4: getfield apkHandles : [J
    //   7: astore_3
    //   8: aload_3
    //   9: arraylength
    //   10: istore #4
    //   12: iconst_0
    //   13: istore #5
    //   15: iload #5
    //   17: iload #4
    //   19: if_icmpge -> 108
    //   22: aload_3
    //   23: iload #5
    //   25: laload
    //   26: lstore #6
    //   28: lload #6
    //   30: aload_1
    //   31: aload_0
    //   32: getfield debuggable : Z
    //   35: invokestatic nativeFindSupportedAbi : (J[Ljava/lang/String;Z)I
    //   38: istore #8
    //   40: iload #8
    //   42: bipush #-114
    //   44: if_icmpne -> 53
    //   47: iload_2
    //   48: istore #9
    //   50: goto -> 96
    //   53: iload #8
    //   55: bipush #-113
    //   57: if_icmpne -> 74
    //   60: iload_2
    //   61: istore #9
    //   63: iload_2
    //   64: ifge -> 96
    //   67: bipush #-113
    //   69: istore #9
    //   71: goto -> 96
    //   74: iload #8
    //   76: iflt -> 105
    //   79: iload_2
    //   80: iflt -> 92
    //   83: iload_2
    //   84: istore #9
    //   86: iload #8
    //   88: iload_2
    //   89: if_icmpge -> 96
    //   92: iload #8
    //   94: istore #9
    //   96: iinc #5, 1
    //   99: iload #9
    //   101: istore_2
    //   102: goto -> 15
    //   105: iload #8
    //   107: ireturn
    //   108: iload_2
    //   109: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #214	-> 0
    //   #215	-> 3
    //   #216	-> 28
    //   #217	-> 40
    //   #219	-> 53
    //   #222	-> 60
    //   #223	-> 67
    //   #225	-> 74
    //   #227	-> 79
    //   #228	-> 92
    //   #215	-> 96
    //   #232	-> 105
    //   #235	-> 108
  }
  
  public static void removeNativeBinariesLI(String paramString) {
    if (paramString == null)
      return; 
    removeNativeBinariesFromDirLI(new File(paramString), false);
  }
  
  public static void removeNativeBinariesFromDirLI(File paramFile, boolean paramBoolean) {
    if (paramFile.exists()) {
      File[] arrayOfFile = paramFile.listFiles();
      if (arrayOfFile != null)
        for (byte b = 0; b < arrayOfFile.length; b++) {
          if (arrayOfFile[b].isDirectory()) {
            removeNativeBinariesFromDirLI(arrayOfFile[b], true);
          } else if (!arrayOfFile[b].delete()) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Could not delete native binary: ");
            stringBuilder.append(arrayOfFile[b].getPath());
            Slog.w("NativeHelper", stringBuilder.toString());
          } 
        }  
      if (paramBoolean && 
        !paramFile.delete()) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Could not delete native binary directory: ");
        stringBuilder.append(paramFile.getPath());
        String str = stringBuilder.toString();
        Slog.w("NativeHelper", str);
      } 
    } 
  }
  
  public static void createNativeLibrarySubdir(File paramFile) throws IOException {
    if (!paramFile.isDirectory()) {
      paramFile.delete();
      if (paramFile.mkdir()) {
        try {
          Os.chmod(paramFile.getPath(), OsConstants.S_IRWXU | OsConstants.S_IRGRP | OsConstants.S_IXGRP | OsConstants.S_IROTH | OsConstants.S_IXOTH);
        } catch (ErrnoException errnoException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Cannot chmod native library directory ");
          stringBuilder.append(paramFile.getPath());
          throw new IOException(stringBuilder.toString(), errnoException);
        } 
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Cannot create ");
        stringBuilder.append(paramFile.getPath());
        throw new IOException(stringBuilder.toString());
      } 
    } else if (!SELinux.restorecon(paramFile)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Cannot set SELinux context for ");
      stringBuilder.append(paramFile.getPath());
      throw new IOException(stringBuilder.toString());
    } 
  }
  
  private static long sumNativeBinariesForSupportedAbi(Handle paramHandle, String[] paramArrayOfString) {
    int i = findSupportedAbi(paramHandle, paramArrayOfString);
    if (i >= 0)
      return sumNativeBinaries(paramHandle, paramArrayOfString[i]); 
    return 0L;
  }
  
  public static int copyNativeBinariesForSupportedAbi(Handle paramHandle, File paramFile, String[] paramArrayOfString, boolean paramBoolean1, boolean paramBoolean2) throws IOException {
    File file;
    int i = findSupportedAbi(paramHandle, paramArrayOfString);
    if (i < 0)
      return i; 
    String str2 = paramArrayOfString[i];
    String str1 = VMRuntime.getInstructionSet(str2);
    if (paramBoolean1) {
      file = new File(paramFile, str1);
    } else {
      file = paramFile;
    } 
    if (paramBoolean2) {
      int k = incrementalConfigureNativeBinariesForSupportedAbi(paramHandle, file, str2);
      if (k != 1)
        return k; 
      return i;
    } 
    createNativeLibrarySubdir(paramFile);
    if (file != paramFile)
      createNativeLibrarySubdir(file); 
    int j = copyNativeBinaries(paramHandle, file, str2);
    if (j != 1)
      return j; 
    return i;
  }
  
  public static int copyNativeBinariesWithOverride(Handle paramHandle, File paramFile, String paramString, boolean paramBoolean) {
    try {
      StringBuilder stringBuilder;
      boolean bool = paramHandle.multiArch;
      if (bool) {
        if (paramString != null && !"-".equals(paramString))
          Slog.w("NativeHelper", "Ignoring abiOverride for multi arch application."); 
        if (Build.SUPPORTED_32_BIT_ABIS.length > 0) {
          int i = copyNativeBinariesForSupportedAbi(paramHandle, paramFile, Build.SUPPORTED_32_BIT_ABIS, true, paramBoolean);
          if (i < 0 && i != -114 && i != -113) {
            stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("Failure copying 32 bit native libraries; copyRet=");
            stringBuilder.append(i);
            Slog.w("NativeHelper", stringBuilder.toString());
            return i;
          } 
        } 
        if (Build.SUPPORTED_64_BIT_ABIS.length > 0) {
          int i = copyNativeBinariesForSupportedAbi((Handle)stringBuilder, paramFile, Build.SUPPORTED_64_BIT_ABIS, true, paramBoolean);
          if (i < 0 && i != -114 && i != -113) {
            stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("Failure copying 64 bit native libraries; copyRet=");
            stringBuilder.append(i);
            Slog.w("NativeHelper", stringBuilder.toString());
            return i;
          } 
        } 
      } else {
        String arrayOfString1[], str = null;
        if ("-".equals(paramString)) {
          str = null;
        } else if (paramString != null) {
          str = paramString;
        } 
        if (str != null) {
          arrayOfString1 = new String[1];
          arrayOfString1[0] = str;
        } else {
          arrayOfString1 = Build.SUPPORTED_ABIS;
        } 
        String[] arrayOfString2 = arrayOfString1;
        if (Build.SUPPORTED_64_BIT_ABIS.length > 0) {
          arrayOfString2 = arrayOfString1;
          if (str == null) {
            arrayOfString2 = arrayOfString1;
            if (hasRenderscriptBitcode((Handle)stringBuilder))
              arrayOfString2 = Build.SUPPORTED_32_BIT_ABIS; 
          } 
        } 
        int i = copyNativeBinariesForSupportedAbi((Handle)stringBuilder, paramFile, arrayOfString2, true, paramBoolean);
        if (i < 0 && i != -114) {
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Failure copying native libraries [errorCode=");
          stringBuilder.append(i);
          stringBuilder.append("]");
          Slog.w("NativeHelper", stringBuilder.toString());
          return i;
        } 
      } 
      return 1;
    } catch (IOException iOException) {
      Slog.e("NativeHelper", "Copying native libraries failed", iOException);
      return -110;
    } 
  }
  
  public static long sumNativeBinariesWithOverride(Handle paramHandle, String paramString) throws IOException {
    long l2, l1 = 0L;
    if (paramHandle.multiArch) {
      if (paramString != null && !"-".equals(paramString))
        Slog.w("NativeHelper", "Ignoring abiOverride for multi arch application."); 
      if (Build.SUPPORTED_32_BIT_ABIS.length > 0)
        l1 = 0L + sumNativeBinariesForSupportedAbi(paramHandle, Build.SUPPORTED_32_BIT_ABIS); 
      l2 = l1;
      if (Build.SUPPORTED_64_BIT_ABIS.length > 0)
        l2 = l1 + sumNativeBinariesForSupportedAbi(paramHandle, Build.SUPPORTED_64_BIT_ABIS); 
    } else {
      String arrayOfString1[], str = null;
      if ("-".equals(paramString)) {
        str = null;
      } else if (paramString != null) {
        str = paramString;
      } 
      if (str != null) {
        arrayOfString1 = new String[1];
        arrayOfString1[0] = str;
      } else {
        arrayOfString1 = Build.SUPPORTED_ABIS;
      } 
      String[] arrayOfString2 = arrayOfString1;
      if (Build.SUPPORTED_64_BIT_ABIS.length > 0) {
        arrayOfString2 = arrayOfString1;
        if (str == null) {
          arrayOfString2 = arrayOfString1;
          if (hasRenderscriptBitcode(paramHandle))
            arrayOfString2 = Build.SUPPORTED_32_BIT_ABIS; 
        } 
      } 
      l2 = 0L + sumNativeBinariesForSupportedAbi(paramHandle, arrayOfString2);
    } 
    return l2;
  }
  
  private static int incrementalConfigureNativeBinariesForSupportedAbi(Handle paramHandle, File paramFile, String paramString) {
    String[] arrayOfString = paramHandle.apkPaths;
    if (arrayOfString == null || arrayOfString.length == 0) {
      Slog.e("NativeHelper", "No apks to extract native libraries from.");
      return -110;
    } 
    IBinder iBinder = ServiceManager.getService("incremental");
    if (iBinder == null)
      return -110; 
    IncrementalManager incrementalManager = new IncrementalManager(IIncrementalService.Stub.asInterface(iBinder));
    File file = (new File(arrayOfString[0])).getParentFile();
    IncrementalStorage incrementalStorage = incrementalManager.openStorage(file.getAbsolutePath());
    if (incrementalStorage == null) {
      Slog.e("NativeHelper", "Failed to find incremental storage");
      return -110;
    } 
    String str = getRelativePath(file, paramFile);
    if (str == null)
      return -110; 
    for (byte b = 0; b < arrayOfString.length; b++) {
      if (!incrementalStorage.configureNativeBinaries(arrayOfString[b], str, paramString, paramHandle.extractNativeLibs))
        return -110; 
    } 
    return 1;
  }
  
  private static String getRelativePath(File paramFile1, File paramFile2) {
    try {
      Path path1 = paramFile1.toPath();
      Path path2 = paramFile2.toPath();
      path2 = path1.relativize(path2);
      if (path2.toString().isEmpty())
        return ""; 
      return path2.toString();
    } catch (IllegalArgumentException illegalArgumentException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to find relative path between: ");
      stringBuilder.append(paramFile1.getAbsolutePath());
      stringBuilder.append(" and: ");
      stringBuilder.append(paramFile2.getAbsolutePath());
      String str = stringBuilder.toString();
      Slog.e("NativeHelper", str);
      return null;
    } 
  }
  
  public static boolean hasRenderscriptBitcode(Handle paramHandle) throws IOException {
    long[] arrayOfLong;
    int i;
    byte b;
    for (arrayOfLong = paramHandle.apkHandles, i = arrayOfLong.length, b = 0; b < i; ) {
      long l = arrayOfLong[b];
      int j = hasRenderscriptBitcode(l);
      if (j >= 0) {
        if (j == 1)
          return true; 
        b++;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error scanning APK, code: ");
      stringBuilder.append(j);
      throw new IOException(stringBuilder.toString());
    } 
    return false;
  }
  
  public static void waitForNativeBinariesExtraction(ArraySet<IncrementalStorage> paramArraySet) {
    for (byte b = 0; b < paramArraySet.size(); b++) {
      IncrementalStorage incrementalStorage = paramArraySet.valueAtUnchecked(b);
      incrementalStorage.waitForNativeBinariesExtraction();
    } 
  }
  
  private static native int hasRenderscriptBitcode(long paramLong);
  
  private static native void nativeClose(long paramLong);
  
  private static native int nativeCopyNativeBinaries(long paramLong, String paramString1, String paramString2, boolean paramBoolean1, boolean paramBoolean2);
  
  private static native int nativeFindSupportedAbi(long paramLong, String[] paramArrayOfString, boolean paramBoolean);
  
  private static native long nativeOpenApk(String paramString);
  
  private static native long nativeOpenApkFd(FileDescriptor paramFileDescriptor, String paramString);
  
  private static native long nativeSumNativeBinaries(long paramLong, String paramString, boolean paramBoolean);
}
