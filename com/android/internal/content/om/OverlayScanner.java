package com.android.internal.content.om;

import android.content.pm.PackageParser;
import android.util.ArrayMap;
import android.util.Log;
import java.io.File;
import java.util.Collection;

public class OverlayScanner {
  public static class ParsedOverlayInfo {
    public final boolean isStatic;
    
    public final String packageName;
    
    public final File path;
    
    public final int priority;
    
    public final String targetPackageName;
    
    public final int targetSdkVersion;
    
    public ParsedOverlayInfo(String param1String1, String param1String2, int param1Int1, boolean param1Boolean, int param1Int2, File param1File) {
      this.packageName = param1String1;
      this.targetPackageName = param1String2;
      this.targetSdkVersion = param1Int1;
      this.isStatic = param1Boolean;
      this.priority = param1Int2;
      this.path = param1File;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(getClass().getSimpleName());
      String str1 = this.packageName, str2 = this.targetPackageName;
      int i = this.targetSdkVersion;
      boolean bool = this.isStatic;
      int j = this.priority;
      File file = this.path;
      stringBuilder.append(String.format("{packageName=%s, targetPackageName=%s, targetSdkVersion=%s, isStatic=%s, priority=%s, path=%s}", new Object[] { str1, str2, Integer.valueOf(i), Boolean.valueOf(bool), Integer.valueOf(j), file }));
      return stringBuilder.toString();
    }
  }
  
  private final ArrayMap<String, ParsedOverlayInfo> mParsedOverlayInfos = new ArrayMap<>();
  
  public final ParsedOverlayInfo getParsedInfo(String paramString) {
    return this.mParsedOverlayInfos.get(paramString);
  }
  
  final Collection<ParsedOverlayInfo> getAllParsedInfos() {
    return this.mParsedOverlayInfos.values();
  }
  
  public void scanDir(File paramFile) {
    if (!paramFile.exists() || !paramFile.isDirectory())
      return; 
    if (!paramFile.canRead()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Directory ");
      stringBuilder.append(paramFile);
      stringBuilder.append(" cannot be read");
      Log.w("OverlayConfig", stringBuilder.toString());
      return;
    } 
    File[] arrayOfFile = paramFile.listFiles();
    if (arrayOfFile == null)
      return; 
    for (byte b = 0; b < arrayOfFile.length; b++) {
      File file = arrayOfFile[b];
      if (file.isDirectory())
        scanDir(file); 
      if (file.isFile() && file.getPath().endsWith(".apk")) {
        ParsedOverlayInfo parsedOverlayInfo = parseOverlayManifest(file);
        if (parsedOverlayInfo != null)
          this.mParsedOverlayInfos.put(parsedOverlayInfo.packageName, parsedOverlayInfo); 
      } 
    } 
  }
  
  public ParsedOverlayInfo parseOverlayManifest(File paramFile) {
    PackageParser.ApkLite apkLite = null;
    try {
      ParsedOverlayInfo parsedOverlayInfo;
      PackageParser.ApkLite apkLite1 = PackageParser.parseApkLite(paramFile, 0);
      if (apkLite1.targetPackageName == null) {
        apkLite1 = apkLite;
      } else {
        String str1 = apkLite1.packageName, str2 = apkLite1.targetPackageName;
        int i = apkLite1.targetSdkVersion;
        boolean bool = apkLite1.overlayIsStatic;
        int j = apkLite1.overlayPriority;
        File file = new File();
        this(apkLite1.codePath);
        parsedOverlayInfo = new ParsedOverlayInfo(str1, str2, i, bool, j, file);
      } 
      return parsedOverlayInfo;
    } catch (android.content.pm.PackageParser.PackageParserException packageParserException) {
      Log.w("OverlayConfig", "Got exception loading overlay.", (Throwable)packageParserException);
      return null;
    } 
  }
}
