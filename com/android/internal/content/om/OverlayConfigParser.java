package com.android.internal.content.om;

import android.content.pm.PackagePartitions;
import android.os.FileUtils;
import android.util.ArraySet;
import android.util.Log;
import android.util.Xml;
import com.android.internal.util.XmlUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import libcore.io.IoUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

final class OverlayConfigParser {
  private static final String CONFIG_DEFAULT_FILENAME = "config/config.xml";
  
  private static final String CONFIG_DIRECTORY = "config";
  
  static final boolean DEFAULT_ENABLED_STATE = false;
  
  static final boolean DEFAULT_MUTABILITY = true;
  
  private static final int MAXIMUM_MERGE_DEPTH = 5;
  
  public static class ParsedConfiguration {
    public final boolean enabled;
    
    public final boolean mutable;
    
    public final String packageName;
    
    public final OverlayScanner.ParsedOverlayInfo parsedInfo;
    
    public final String policy;
    
    ParsedConfiguration(String param1String1, boolean param1Boolean1, boolean param1Boolean2, String param1String2, OverlayScanner.ParsedOverlayInfo param1ParsedOverlayInfo) {
      this.packageName = param1String1;
      this.enabled = param1Boolean1;
      this.mutable = param1Boolean2;
      this.policy = param1String2;
      this.parsedInfo = param1ParsedOverlayInfo;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(getClass().getSimpleName());
      String str1 = this.packageName;
      boolean bool1 = this.enabled;
      boolean bool2 = this.mutable;
      String str2 = this.policy;
      OverlayScanner.ParsedOverlayInfo parsedOverlayInfo = this.parsedInfo;
      stringBuilder.append(String.format("{packageName=%s, enabled=%s, mutable=%s, policy=%s, parsedInfo=%s}", new Object[] { str1, Boolean.valueOf(bool1), Boolean.valueOf(bool2), str2, parsedOverlayInfo }));
      return stringBuilder.toString();
    }
  }
  
  class OverlayPartition extends PackagePartitions.SystemPartition {
    static final String POLICY_ODM = "odm";
    
    static final String POLICY_OEM = "oem";
    
    static final String POLICY_PRODUCT = "product";
    
    static final String POLICY_PUBLIC = "public";
    
    static final String POLICY_SYSTEM = "system";
    
    static final String POLICY_VENDOR = "vendor";
    
    public final String policy;
    
    OverlayPartition(OverlayConfigParser this$0) {
      super((PackagePartitions.SystemPartition)this$0);
      this.policy = policyForPartition((PackagePartitions.SystemPartition)this$0);
    }
    
    OverlayPartition(OverlayConfigParser this$0, PackagePartitions.SystemPartition param1SystemPartition) {
      super((File)this$0, param1SystemPartition);
      this.policy = policyForPartition(param1SystemPartition);
    }
    
    private static String policyForPartition(PackagePartitions.SystemPartition param1SystemPartition) {
      int i = param1SystemPartition.type;
      if (i != 0)
        if (i != 1) {
          if (i != 2) {
            if (i != 3) {
              if (i != 4) {
                if (i != 5) {
                  StringBuilder stringBuilder = new StringBuilder();
                  stringBuilder.append("Unable to determine policy for ");
                  stringBuilder.append(param1SystemPartition.getFolder());
                  throw new IllegalStateException(stringBuilder.toString());
                } 
              } else {
                return "product";
              } 
            } else {
              return "oem";
            } 
          } else {
            return "odm";
          } 
        } else {
          return "vendor";
        }  
      return "system";
    }
  }
  
  private static class ParsingContext {
    private final OverlayConfigParser.OverlayPartition mPartition;
    
    private final ArrayList<OverlayConfigParser.ParsedConfiguration> mOrderedConfigurations = new ArrayList<>();
    
    private int mMergeDepth;
    
    private boolean mFoundMutableOverlay;
    
    private final ArraySet<String> mConfiguredOverlays = new ArraySet<>();
    
    private ParsingContext(OverlayConfigParser.OverlayPartition param1OverlayPartition) {
      this.mPartition = param1OverlayPartition;
    }
  }
  
  static ArrayList<ParsedConfiguration> getConfigurations(OverlayPartition paramOverlayPartition, OverlayScanner paramOverlayScanner) {
    if (paramOverlayPartition.getOverlayFolder() == null)
      return null; 
    if (paramOverlayScanner != null)
      paramOverlayScanner.scanDir(paramOverlayPartition.getOverlayFolder()); 
    File file = new File(paramOverlayPartition.getOverlayFolder(), "config/config.xml");
    if (!file.exists())
      return null; 
    ParsingContext parsingContext = new ParsingContext(paramOverlayPartition);
    readConfigFile(file, paramOverlayScanner, parsingContext);
    return parsingContext.mOrderedConfigurations;
  }
  
  private static void readConfigFile(File paramFile, OverlayScanner paramOverlayScanner, ParsingContext paramParsingContext) {
    try {
      FileReader fileReader = new FileReader(paramFile);
      try {
        XmlPullParser xmlPullParser = Xml.newPullParser();
        xmlPullParser.setInput(fileReader);
        XmlUtils.beginDocument(xmlPullParser, "config");
        int i = xmlPullParser.getDepth();
        while (XmlUtils.nextElementWithin(xmlPullParser, i)) {
          String str = xmlPullParser.getName();
          byte b = -1;
          int j = str.hashCode();
          if (j != -1091287984) {
            if (j == 103785528 && str.equals("merge"))
              b = 0; 
          } else if (str.equals("overlay")) {
            b = 1;
          } 
          if (b != 0) {
            if (b != 1) {
              String str1 = xmlPullParser.getPositionDescription();
              Log.w("OverlayConfig", String.format("Tag %s is unknown in %s at %s", new Object[] { str, paramFile, str1 }));
              continue;
            } 
            parseOverlay(paramFile, xmlPullParser, paramOverlayScanner, paramParsingContext);
            continue;
          } 
          parseMerge(paramFile, xmlPullParser, paramOverlayScanner, paramParsingContext);
        } 
      } catch (XmlPullParserException|IOException xmlPullParserException) {
        Log.w("OverlayConfig", "Got exception parsing overlay configuration.", (Throwable)xmlPullParserException);
      } finally {}
      IoUtils.closeQuietly(fileReader);
      return;
    } catch (FileNotFoundException fileNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Couldn't find or open overlay configuration file ");
      stringBuilder.append(paramFile);
      Log.w("OverlayConfig", stringBuilder.toString());
      return;
    } 
  }
  
  private static void parseMerge(File paramFile, XmlPullParser paramXmlPullParser, OverlayScanner paramOverlayScanner, ParsingContext paramParsingContext) {
    String str3 = paramXmlPullParser.getAttributeValue(null, "path");
    if (str3 != null) {
      if (!str3.startsWith("/")) {
        if (ParsingContext.access$208(paramParsingContext) != 5)
          try {
            File file1 = new File();
            this(paramParsingContext.mPartition.getOverlayFolder(), "config");
            File file2 = file1.getCanonicalFile();
            file1 = new File();
            this(file2, str3);
            file1 = file1.getCanonicalFile();
            if (file1.exists()) {
              if (FileUtils.contains(file2, file1)) {
                readConfigFile(file1, paramOverlayScanner, paramParsingContext);
                ParsingContext.access$210(paramParsingContext);
                return;
              } 
              str1 = file1.getAbsolutePath();
              str2 = paramXmlPullParser.getPositionDescription();
              throw new IllegalStateException(String.format("Merged file %s outside of configuration directory in %s at %s", new Object[] { str1, file1, str2 }));
            } 
            str2 = str2.getPositionDescription();
            throw new IllegalStateException(String.format("Merged configuration file %s does not exist in %s at %s", new Object[] { str3, str1, str2 }));
          } catch (IOException iOException) {
            str2 = str2.getPositionDescription();
            throw new IllegalStateException(String.format("Couldn't find or open merged configuration file %s in %s at %s", new Object[] { str3, str1, str2 }), iOException);
          }  
        str2 = str2.getPositionDescription();
        throw new IllegalStateException(String.format("Maximum <merge> depth exceeded in %s at %s", new Object[] { str1, str2 }));
      } 
      str2 = str2.getPositionDescription();
      throw new IllegalStateException(String.format("Path %s must be relative to the directory containing overlay configurations  files in %s at %s ", new Object[] { str3, str1, str2 }));
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("<merge> without path in %s at %s");
    stringBuilder.append(str1);
    String str1 = stringBuilder.toString();
    String str2 = str2.getPositionDescription();
    throw new IllegalStateException(String.format(str1, new Object[] { str2 }));
  }
  
  private static void parseOverlay(File paramFile, XmlPullParser paramXmlPullParser, OverlayScanner paramOverlayScanner, ParsingContext paramParsingContext) {
    ParsedConfiguration parsedConfiguration;
    String str2 = paramXmlPullParser.getAttributeValue(null, "package");
    if (str2 != null) {
      if (paramOverlayScanner != null) {
        OverlayScanner.ParsedOverlayInfo parsedOverlayInfo = paramOverlayScanner.getParsedInfo(str2);
        if (parsedOverlayInfo == null || !paramParsingContext.mPartition.containsOverlay(parsedOverlayInfo.path)) {
          File file = paramParsingContext.mPartition.getOverlayFolder();
          str1 = paramXmlPullParser.getPositionDescription();
          throw new IllegalStateException(String.format("overlay %s not present in partition %s in %s at %s", new Object[] { str2, file, paramFile, str1 }));
        } 
      } else {
        paramOverlayScanner = null;
      } 
      if (!paramParsingContext.mConfiguredOverlays.contains(str2)) {
        boolean bool1, bool2;
        String str = str1.getAttributeValue(null, "enabled");
        if (str != null) {
          bool1 = "false".equals(str) ^ true;
        } else {
          bool1 = false;
        } 
        str = str1.getAttributeValue(null, "mutable");
        if (str != null) {
          bool2 = "false".equals(str) ^ true;
          if (bool2 == 0 && paramParsingContext.mFoundMutableOverlay) {
            str1 = str1.getPositionDescription();
            throw new IllegalStateException(String.format("immutable overlays must precede mutable overlays: found in %s at %s", new Object[] { paramFile, str1 }));
          } 
        } else {
          bool2 = true;
        } 
        if (bool2) {
          ParsingContext.access$502(paramParsingContext, true);
        } else if (!bool1) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("found default-disabled immutable overlay ");
          stringBuilder.append(str2);
          Log.w("OverlayConfig", stringBuilder.toString());
        } 
        parsedConfiguration = new ParsedConfiguration(str2, bool1, bool2, paramParsingContext.mPartition.policy, (OverlayScanner.ParsedOverlayInfo)paramOverlayScanner);
        paramParsingContext.mConfiguredOverlays.add(str2);
        paramParsingContext.mOrderedConfigurations.add(parsedConfiguration);
        return;
      } 
      str1 = str1.getPositionDescription();
      throw new IllegalStateException(String.format("overlay %s configured multiple times in a single partition in %s at %s", new Object[] { str2, parsedConfiguration, str1 }));
    } 
    String str1 = str1.getPositionDescription();
    throw new IllegalStateException(String.format("\"<overlay> without package in %s at %s", new Object[] { parsedConfiguration, str1 }));
  }
}
