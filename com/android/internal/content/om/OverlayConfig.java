package com.android.internal.content.om;

import android.content.pm.PackagePartitions;
import android.content.pm.parsing.ParsingPackageRead;
import android.os.Trace;
import android.util.ArrayMap;
import android.util.Log;
import com.android.internal.util.Preconditions;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;

public class OverlayConfig {
  public static final int DEFAULT_PRIORITY = 2147483647;
  
  static final String TAG = "OverlayConfig";
  
  private static OverlayConfig sInstance;
  
  public static final class Configuration {
    public final int configIndex;
    
    public final OverlayConfigParser.ParsedConfiguration parsedConfig;
    
    public Configuration(OverlayConfigParser.ParsedConfiguration param1ParsedConfiguration, int param1Int) {
      this.parsedConfig = param1ParsedConfiguration;
      this.configIndex = param1Int;
    }
  }
  
  private static final Comparator<OverlayConfigParser.ParsedConfiguration> sStaticOverlayComparator = (Comparator<OverlayConfigParser.ParsedConfiguration>)_$$Lambda$OverlayConfig$bPcIgkiZP3FiPOvGMosWxnh9KGA.INSTANCE;
  
  private final ArrayMap<String, Configuration> mConfigurations;
  
  public OverlayConfig(File paramFile, Supplier<OverlayScanner> paramSupplier, PackageProvider paramPackageProvider) {
    ArrayList<OverlayConfigParser.OverlayPartition> arrayList;
    this.mConfigurations = new ArrayMap<>();
    boolean bool = false;
    if (paramSupplier == null) {
      i = 1;
    } else {
      i = 0;
    } 
    if (paramPackageProvider == null) {
      j = 1;
    } else {
      j = 0;
    } 
    if (i != j)
      bool = true; 
    Preconditions.checkArgument(bool, "scannerFactory and packageProvider cannot be both null or both non-null");
    if (paramFile == null) {
      -$.Lambda.ZY6dnO1FkDIliprAt8K651mwFE zY6dnO1FkDIliprAt8K651mwFE = _$$Lambda$8ZY6dnO1FkDIliprAt8K651mwFE.INSTANCE;
      arrayList = new ArrayList(PackagePartitions.getOrderedPartitions((Function)zY6dnO1FkDIliprAt8K651mwFE));
    } else {
      arrayList = new ArrayList(PackagePartitions.getOrderedPartitions(new _$$Lambda$OverlayConfig$xYJTFUYrmDVwxs6NvAezi_C_FF0((File)arrayList)));
    } 
    int j = 0;
    OverlayScanner overlayScanner = null;
    ArrayList<OverlayConfigParser.ParsedConfiguration> arrayList1 = new ArrayList();
    int i, k;
    for (i = 0, k = arrayList.size(); i < k; i++) {
      OverlayScanner overlayScanner1;
      OverlayConfigParser.OverlayPartition overlayPartition = arrayList.get(i);
      if (paramSupplier == null) {
        overlayScanner1 = null;
      } else {
        overlayScanner1 = paramSupplier.get();
      } 
      ArrayList<OverlayConfigParser.ParsedConfiguration> arrayList2 = OverlayConfigParser.getConfigurations(overlayPartition, overlayScanner1);
      if (arrayList2 != null) {
        j = 1;
        arrayList1.addAll(arrayList2);
      } else {
        ArrayList<OverlayScanner.ParsedOverlayInfo> arrayList4;
        if (paramSupplier != null) {
          arrayList2 = new ArrayList(overlayScanner1.getAllParsedInfos());
        } else {
          ArrayList<OverlayScanner.ParsedOverlayInfo> arrayList5;
          overlayScanner1 = overlayScanner;
          if (overlayScanner == null)
            arrayList5 = getOverlayPackageInfos(paramPackageProvider); 
          ArrayList<OverlayScanner.ParsedOverlayInfo> arrayList6 = new ArrayList<>(arrayList5);
          int n = arrayList6.size() - 1;
          while (true) {
            ArrayList<OverlayScanner.ParsedOverlayInfo> arrayList7 = arrayList5;
            arrayList4 = arrayList6;
            if (n >= 0) {
              if (!overlayPartition.containsFile(((OverlayScanner.ParsedOverlayInfo)arrayList6.get(n)).path))
                arrayList6.remove(n); 
              n--;
              continue;
            } 
            break;
          } 
        } 
        ArrayList<OverlayConfigParser.ParsedConfiguration> arrayList3 = new ArrayList();
        byte b;
        int m;
        for (b = 0, m = arrayList4.size(); b < m; b++) {
          OverlayScanner.ParsedOverlayInfo parsedOverlayInfo = arrayList4.get(b);
          if (parsedOverlayInfo.isStatic)
            arrayList3.add(new OverlayConfigParser.ParsedConfiguration(parsedOverlayInfo.packageName, true, false, overlayPartition.policy, parsedOverlayInfo)); 
        } 
        arrayList3.sort(sStaticOverlayComparator);
        arrayList1.addAll(arrayList3);
      } 
    } 
    if (j == 0)
      arrayList1.sort(sStaticOverlayComparator); 
    for (i = 0, j = arrayList1.size(); i < j; i++) {
      OverlayConfigParser.ParsedConfiguration parsedConfiguration = arrayList1.get(i);
      this.mConfigurations.put(parsedConfiguration.packageName, new Configuration(parsedConfiguration, i));
    } 
  }
  
  public static OverlayConfig getZygoteInstance() {
    Trace.traceBegin(67108864L, "OverlayConfig#getZygoteInstance");
    try {
      return new OverlayConfig(null, (Supplier<OverlayScanner>)_$$Lambda$TnMimLdK_xwmEZLrRzFg7LG1Yfg.INSTANCE, null);
    } finally {
      Trace.traceEnd(67108864L);
    } 
  }
  
  public static OverlayConfig initializeSystemInstance(PackageProvider paramPackageProvider) {
    Trace.traceBegin(67108864L, "OverlayConfig#initializeSystemInstance");
    try {
      OverlayConfig overlayConfig = new OverlayConfig();
      this(null, null, paramPackageProvider);
      sInstance = overlayConfig;
      return sInstance;
    } finally {
      Trace.traceEnd(67108864L);
    } 
  }
  
  public static OverlayConfig getSystemInstance() {
    OverlayConfig overlayConfig = sInstance;
    if (overlayConfig != null)
      return overlayConfig; 
    throw new IllegalStateException("System instance not initialized");
  }
  
  public Configuration getConfiguration(String paramString) {
    return this.mConfigurations.get(paramString);
  }
  
  public boolean isEnabled(String paramString) {
    boolean bool;
    Configuration configuration = this.mConfigurations.get(paramString);
    if (configuration == null) {
      bool = false;
    } else {
      bool = configuration.parsedConfig.enabled;
    } 
    return bool;
  }
  
  public boolean isMutable(String paramString) {
    boolean bool;
    Configuration configuration = this.mConfigurations.get(paramString);
    if (configuration == null) {
      bool = true;
    } else {
      bool = configuration.parsedConfig.mutable;
    } 
    return bool;
  }
  
  public int getPriority(String paramString) {
    int i;
    Configuration configuration = this.mConfigurations.get(paramString);
    if (configuration == null) {
      i = Integer.MAX_VALUE;
    } else {
      i = configuration.configIndex;
    } 
    return i;
  }
  
  private ArrayList<Configuration> getSortedOverlays() {
    ArrayList<Configuration> arrayList = new ArrayList();
    byte b;
    int i;
    for (b = 0, i = this.mConfigurations.size(); b < i; b++)
      arrayList.add(this.mConfigurations.valueAt(b)); 
    arrayList.sort(Comparator.comparingInt((ToIntFunction<? super Configuration>)_$$Lambda$OverlayConfig$2_NucNRcrJn4xnLpjFKSY7827lQ.INSTANCE));
    return arrayList;
  }
  
  private static ArrayList<OverlayScanner.ParsedOverlayInfo> getOverlayPackageInfos(PackageProvider paramPackageProvider) {
    ArrayList<OverlayScanner.ParsedOverlayInfo> arrayList = new ArrayList();
    paramPackageProvider.forEachPackage(new _$$Lambda$OverlayConfig$ot6Y2nGi_Oszn0xMxXGn0yFBZE8(arrayList));
    return arrayList;
  }
  
  public static class IdmapInvocation {
    public final boolean enforceOverlayable;
    
    public final ArrayList<String> overlayPaths = new ArrayList<>();
    
    public final String policy;
    
    IdmapInvocation(boolean param1Boolean, String param1String) {
      this.enforceOverlayable = param1Boolean;
      this.policy = param1String;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(getClass().getSimpleName());
      boolean bool = this.enforceOverlayable;
      String str1 = this.policy;
      ArrayList<String> arrayList = this.overlayPaths;
      String str2 = String.join(", ", (Iterable)arrayList);
      stringBuilder.append(String.format("{enforceOverlayable=%s, policy=%s, overlayPaths=[%s]}", new Object[] { Boolean.valueOf(bool), str1, str2 }));
      return stringBuilder.toString();
    }
  }
  
  public ArrayList<IdmapInvocation> getImmutableFrameworkOverlayIdmapInvocations() {
    ArrayList<IdmapInvocation> arrayList = new ArrayList();
    ArrayList<Configuration> arrayList1 = getSortedOverlays();
    byte b;
    int i;
    for (b = 0, i = arrayList1.size(); b < i; b++) {
      Configuration configuration = arrayList1.get(b);
      if (!configuration.parsedConfig.mutable && configuration.parsedConfig.enabled) {
        String str = configuration.parsedConfig.parsedInfo.targetPackageName;
        if ("android".equals(str)) {
          IdmapInvocation idmapInvocation1;
          boolean bool;
          if (configuration.parsedConfig.parsedInfo.targetSdkVersion >= 29) {
            bool = true;
          } else {
            bool = false;
          } 
          String str1 = null;
          str = str1;
          if (!arrayList.isEmpty()) {
            IdmapInvocation idmapInvocation = arrayList.get(arrayList.size() - 1);
            str = str1;
            if (idmapInvocation.enforceOverlayable == bool) {
              String str2 = idmapInvocation.policy, str3 = configuration.parsedConfig.policy;
              str = str1;
              if (str2.equals(str3))
                idmapInvocation1 = idmapInvocation; 
            } 
          } 
          IdmapInvocation idmapInvocation2 = idmapInvocation1;
          if (idmapInvocation1 == null) {
            idmapInvocation2 = new IdmapInvocation(bool, configuration.parsedConfig.policy);
            arrayList.add(idmapInvocation2);
          } 
          idmapInvocation2.overlayPaths.add(configuration.parsedConfig.parsedInfo.path.getAbsolutePath());
        } 
      } 
    } 
    return arrayList;
  }
  
  public String[] createImmutableFrameworkIdmapsInZygote() {
    ArrayList arrayList = new ArrayList();
    ArrayList<IdmapInvocation> arrayList1 = getImmutableFrameworkOverlayIdmapInvocations();
    byte b;
    int i;
    for (b = 0, i = arrayList1.size(); b < i; b++) {
      IdmapInvocation idmapInvocation = arrayList1.get(b);
      ArrayList<String> arrayList2 = idmapInvocation.overlayPaths;
      String arrayOfString2[] = arrayList2.<String>toArray(new String[0]), str = idmapInvocation.policy;
      boolean bool = idmapInvocation.enforceOverlayable;
      String[] arrayOfString1 = createIdmap("/system/framework/framework-res.apk", arrayOfString2, new String[] { "public", str }, bool);
      if (arrayOfString1 == null) {
        Log.w("OverlayConfig", "'idmap2 create-multiple' failed: no mutable=\"false\" overlays targeting \"android\" will be loaded");
        return new String[0];
      } 
      arrayList.addAll(Arrays.asList(arrayOfString1));
    } 
    return (String[])arrayList.toArray((Object[])new String[0]);
  }
  
  public ArrayList<IdmapInvocation> getImmutableOplusFrameworkOverlayIdmapInvocations() {
    ArrayList<IdmapInvocation> arrayList = new ArrayList();
    ArrayList<Configuration> arrayList1 = getSortedOverlays();
    byte b;
    int i;
    for (b = 0, i = arrayList1.size(); b < i; b++) {
      Configuration configuration = arrayList1.get(b);
      if (!configuration.parsedConfig.mutable && configuration.parsedConfig.enabled) {
        String str = configuration.parsedConfig.parsedInfo.targetPackageName;
        if ("oplus".equals(str)) {
          IdmapInvocation idmapInvocation1;
          boolean bool;
          if (configuration.parsedConfig.parsedInfo.targetSdkVersion >= 29) {
            bool = true;
          } else {
            bool = false;
          } 
          String str1 = null;
          str = str1;
          if (!arrayList.isEmpty()) {
            IdmapInvocation idmapInvocation = arrayList.get(arrayList.size() - 1);
            str = str1;
            if (idmapInvocation.enforceOverlayable == bool) {
              String str2 = idmapInvocation.policy, str3 = configuration.parsedConfig.policy;
              str = str1;
              if (str2.equals(str3))
                idmapInvocation1 = idmapInvocation; 
            } 
          } 
          IdmapInvocation idmapInvocation2 = idmapInvocation1;
          if (idmapInvocation1 == null) {
            idmapInvocation2 = new IdmapInvocation(bool, configuration.parsedConfig.policy);
            arrayList.add(idmapInvocation2);
          } 
          idmapInvocation2.overlayPaths.add(configuration.parsedConfig.parsedInfo.path.getAbsolutePath());
        } 
      } 
    } 
    return arrayList;
  }
  
  public String[] createImmutableOplusFrameworkIdmapsInZygote() {
    ArrayList arrayList = new ArrayList();
    ArrayList<IdmapInvocation> arrayList1 = getImmutableOplusFrameworkOverlayIdmapInvocations();
    byte b;
    int i;
    for (b = 0, i = arrayList1.size(); b < i; b++) {
      IdmapInvocation idmapInvocation = arrayList1.get(b);
      ArrayList<String> arrayList2 = idmapInvocation.overlayPaths;
      String arrayOfString2[] = arrayList2.<String>toArray(new String[0]), str = idmapInvocation.policy;
      boolean bool = idmapInvocation.enforceOverlayable;
      String[] arrayOfString1 = createIdmap("/system_ext/framework/oplus-framework-res.apk", arrayOfString2, new String[] { "public", str }, bool);
      if (arrayOfString1 == null) {
        Log.w("OverlayConfig", "'idmap2 create-multiple' failed: no mutable=\"false\" overlays targeting \"oplus\" will be loaded");
        return new String[0];
      } 
      arrayList.addAll(Arrays.asList(arrayOfString1));
    } 
    return (String[])arrayList.toArray((Object[])new String[0]);
  }
  
  private static native String[] createIdmap(String paramString, String[] paramArrayOfString1, String[] paramArrayOfString2, boolean paramBoolean);
  
  public static interface PackageProvider {
    void forEachPackage(BiConsumer<ParsingPackageRead, Boolean> param1BiConsumer);
  }
}
