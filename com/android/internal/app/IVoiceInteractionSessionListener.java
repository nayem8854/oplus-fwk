package com.android.internal.app;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IVoiceInteractionSessionListener extends IInterface {
  void onSetUiHints(Bundle paramBundle) throws RemoteException;
  
  void onVoiceSessionHidden() throws RemoteException;
  
  void onVoiceSessionShown() throws RemoteException;
  
  class Default implements IVoiceInteractionSessionListener {
    public void onVoiceSessionShown() throws RemoteException {}
    
    public void onVoiceSessionHidden() throws RemoteException {}
    
    public void onSetUiHints(Bundle param1Bundle) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IVoiceInteractionSessionListener {
    private static final String DESCRIPTOR = "com.android.internal.app.IVoiceInteractionSessionListener";
    
    static final int TRANSACTION_onSetUiHints = 3;
    
    static final int TRANSACTION_onVoiceSessionHidden = 2;
    
    static final int TRANSACTION_onVoiceSessionShown = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.app.IVoiceInteractionSessionListener");
    }
    
    public static IVoiceInteractionSessionListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.app.IVoiceInteractionSessionListener");
      if (iInterface != null && iInterface instanceof IVoiceInteractionSessionListener)
        return (IVoiceInteractionSessionListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "onSetUiHints";
        } 
        return "onVoiceSessionHidden";
      } 
      return "onVoiceSessionShown";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("com.android.internal.app.IVoiceInteractionSessionListener");
            return true;
          } 
          param1Parcel1.enforceInterface("com.android.internal.app.IVoiceInteractionSessionListener");
          if (param1Parcel1.readInt() != 0) {
            Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel1 = null;
          } 
          onSetUiHints((Bundle)param1Parcel1);
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.internal.app.IVoiceInteractionSessionListener");
        onVoiceSessionHidden();
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.app.IVoiceInteractionSessionListener");
      onVoiceSessionShown();
      return true;
    }
    
    private static class Proxy implements IVoiceInteractionSessionListener {
      public static IVoiceInteractionSessionListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.app.IVoiceInteractionSessionListener";
      }
      
      public void onVoiceSessionShown() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.app.IVoiceInteractionSessionListener");
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IVoiceInteractionSessionListener.Stub.getDefaultImpl() != null) {
            IVoiceInteractionSessionListener.Stub.getDefaultImpl().onVoiceSessionShown();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onVoiceSessionHidden() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.app.IVoiceInteractionSessionListener");
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IVoiceInteractionSessionListener.Stub.getDefaultImpl() != null) {
            IVoiceInteractionSessionListener.Stub.getDefaultImpl().onVoiceSessionHidden();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSetUiHints(Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.app.IVoiceInteractionSessionListener");
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IVoiceInteractionSessionListener.Stub.getDefaultImpl() != null) {
            IVoiceInteractionSessionListener.Stub.getDefaultImpl().onSetUiHints(param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IVoiceInteractionSessionListener param1IVoiceInteractionSessionListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IVoiceInteractionSessionListener != null) {
          Proxy.sDefaultImpl = param1IVoiceInteractionSessionListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IVoiceInteractionSessionListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
