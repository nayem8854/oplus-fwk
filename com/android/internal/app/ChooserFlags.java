package com.android.internal.app;

public class ChooserFlags {
  static final boolean USE_PREDICTION_MANAGER_FOR_DIRECT_TARGETS = true;
  
  public static final boolean USE_SHORTCUT_MANAGER_FOR_DIRECT_TARGETS = true;
}
