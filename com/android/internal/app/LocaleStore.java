package com.android.internal.app;

import android.content.ContentResolver;
import android.content.Context;
import android.os.LocaleList;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IllformedLocaleException;
import java.util.Locale;
import java.util.Set;

public class LocaleStore {
  private static boolean sFullyInitialized;
  
  private static final HashMap<String, LocaleInfo> sLocaleCache = new HashMap<>();
  
  static {
    sFullyInitialized = false;
  }
  
  public static class LocaleInfo implements Serializable {
    private static final int SUGGESTION_TYPE_CFG = 2;
    
    private static final int SUGGESTION_TYPE_NONE = 0;
    
    private static final int SUGGESTION_TYPE_SIM = 1;
    
    private String mFullCountryNameNative;
    
    private String mFullNameNative;
    
    private final String mId;
    
    private boolean mIsChecked;
    
    private boolean mIsPseudo;
    
    private boolean mIsTranslated;
    
    private String mLangScriptKey;
    
    private final Locale mLocale;
    
    private final Locale mParent;
    
    private int mSuggestionFlags;
    
    private LocaleInfo(Locale param1Locale) {
      this.mLocale = param1Locale;
      this.mId = param1Locale.toLanguageTag();
      this.mParent = getParent(param1Locale);
      this.mIsChecked = false;
      this.mSuggestionFlags = 0;
      this.mIsTranslated = false;
      this.mIsPseudo = false;
    }
    
    private LocaleInfo(String param1String) {
      this(Locale.forLanguageTag(param1String));
    }
    
    private static Locale getParent(Locale param1Locale) {
      if (param1Locale.getCountry().isEmpty())
        return null; 
      Locale.Builder builder2 = new Locale.Builder();
      Locale.Builder builder1 = builder2.setLocale(param1Locale);
      builder1 = builder1.setRegion("");
      builder1 = builder1.setExtension('u', "");
      return builder1.build();
    }
    
    public String toString() {
      return this.mId;
    }
    
    public Locale getLocale() {
      return this.mLocale;
    }
    
    public Locale getParent() {
      return this.mParent;
    }
    
    public String getId() {
      return this.mId;
    }
    
    public boolean isTranslated() {
      return this.mIsTranslated;
    }
    
    public void setTranslated(boolean param1Boolean) {
      this.mIsTranslated = param1Boolean;
    }
    
    boolean isSuggested() {
      boolean bool = this.mIsTranslated;
      boolean bool1 = false;
      if (!bool)
        return false; 
      if (this.mSuggestionFlags != 0)
        bool1 = true; 
      return bool1;
    }
    
    private boolean isSuggestionOfType(int param1Int) {
      boolean bool = this.mIsTranslated;
      boolean bool1 = false;
      if (!bool)
        return false; 
      if ((this.mSuggestionFlags & param1Int) == param1Int)
        bool1 = true; 
      return bool1;
    }
    
    public String getFullNameNative() {
      if (this.mFullNameNative == null) {
        Locale locale = this.mLocale;
        this.mFullNameNative = LocaleHelper.getDisplayName(locale, locale, true);
      } 
      return this.mFullNameNative;
    }
    
    String getFullCountryNameNative() {
      if (this.mFullCountryNameNative == null) {
        Locale locale = this.mLocale;
        this.mFullCountryNameNative = LocaleHelper.getDisplayCountry(locale, locale);
      } 
      return this.mFullCountryNameNative;
    }
    
    String getFullCountryNameInUiLanguage() {
      return LocaleHelper.getDisplayCountry(this.mLocale);
    }
    
    public String getFullNameInUiLanguage() {
      return LocaleHelper.getDisplayName(this.mLocale, true);
    }
    
    private String getLangScriptKey() {
      if (this.mLangScriptKey == null) {
        String str;
        Locale.Builder builder = new Locale.Builder();
        Locale locale2 = this.mLocale;
        builder = builder.setLocale(locale2);
        builder = builder.setExtension('u', "");
        Locale locale1 = builder.build();
        locale1 = getParent(LocaleHelper.addLikelySubtags(locale1));
        if (locale1 == null) {
          str = this.mLocale.toLanguageTag();
        } else {
          str = str.toLanguageTag();
        } 
        this.mLangScriptKey = str;
      } 
      return this.mLangScriptKey;
    }
    
    String getLabel(boolean param1Boolean) {
      if (param1Boolean)
        return getFullCountryNameNative(); 
      return getFullNameNative();
    }
    
    String getContentDescription(boolean param1Boolean) {
      if (param1Boolean)
        return getFullCountryNameInUiLanguage(); 
      return getFullNameInUiLanguage();
    }
    
    public boolean getChecked() {
      return this.mIsChecked;
    }
    
    public void setChecked(boolean param1Boolean) {
      this.mIsChecked = param1Boolean;
    }
  }
  
  private static Set<String> getSimCountries(Context paramContext) {
    HashSet<String> hashSet = new HashSet();
    TelephonyManager telephonyManager = (TelephonyManager)paramContext.getSystemService(TelephonyManager.class);
    if (telephonyManager != null) {
      String str2 = telephonyManager.getSimCountryIso().toUpperCase(Locale.US);
      if (!str2.isEmpty())
        hashSet.add(str2); 
      String str1 = telephonyManager.getNetworkCountryIso().toUpperCase(Locale.US);
      if (!str1.isEmpty())
        hashSet.add(str1); 
    } 
    return hashSet;
  }
  
  public static void updateSimCountries(Context paramContext) {
    Set<String> set = getSimCountries(paramContext);
    for (LocaleInfo localeInfo : sLocaleCache.values()) {
      if (set.contains(localeInfo.getLocale().getCountry()))
        LocaleInfo.access$076(localeInfo, 1); 
    } 
  }
  
  private static void addSuggestedLocalesForRegion(Locale paramLocale) {
    if (paramLocale == null)
      return; 
    String str = paramLocale.getCountry();
    if (str.isEmpty())
      return; 
    for (LocaleInfo localeInfo : sLocaleCache.values()) {
      if (str.equals(localeInfo.getLocale().getCountry()))
        LocaleInfo.access$076(localeInfo, 1); 
    } 
  }
  
  public static void fillCache(Context paramContext) {
    if (sFullyInitialized)
      return; 
    Set<String> set = getSimCountries(paramContext);
    ContentResolver contentResolver = paramContext.getContentResolver();
    byte b1 = 0;
    if (Settings.Global.getInt(contentResolver, "development_settings_enabled", 0) != 0) {
      b2 = 1;
    } else {
      b2 = 0;
    } 
    for (String str : LocalePicker.getSupportedLocales(paramContext)) {
      if (!str.isEmpty()) {
        LocaleInfo localeInfo = new LocaleInfo(str);
        if (LocaleList.isPseudoLocale(localeInfo.getLocale()))
          if (b2) {
            localeInfo.setTranslated(true);
            LocaleInfo.access$202(localeInfo, true);
            LocaleInfo.access$076(localeInfo, 1);
          } else {
            continue;
          }  
        if (set.contains(localeInfo.getLocale().getCountry()))
          LocaleInfo.access$076(localeInfo, 1); 
        sLocaleCache.put(localeInfo.getId(), localeInfo);
        Locale locale = localeInfo.getParent();
        if (locale != null) {
          String str1 = locale.toLanguageTag();
          if (!sLocaleCache.containsKey(str1))
            sLocaleCache.put(str1, new LocaleInfo(locale)); 
        } 
        continue;
      } 
      throw new IllformedLocaleException("Bad locale entry in locale_config.xml");
    } 
    set = new HashSet<>();
    String[] arrayOfString;
    byte b2;
    int i;
    for (arrayOfString = LocalePicker.getSystemAssetLocales(), i = arrayOfString.length, b2 = b1; b2 < i; ) {
      String str1 = arrayOfString[b2];
      LocaleInfo localeInfo = new LocaleInfo(str1);
      String str2 = localeInfo.getLocale().getCountry();
      if (!str2.isEmpty()) {
        LocaleInfo localeInfo1;
        str1 = null;
        if (sLocaleCache.containsKey(localeInfo.getId())) {
          localeInfo1 = sLocaleCache.get(localeInfo.getId());
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(localeInfo.getLangScriptKey());
          stringBuilder.append("-");
          stringBuilder.append(str2);
          String str = stringBuilder.toString();
          if (sLocaleCache.containsKey(str))
            localeInfo1 = sLocaleCache.get(str); 
        } 
        if (localeInfo1 != null)
          LocaleInfo.access$076(localeInfo1, 2); 
      } 
      set.add(localeInfo.getLangScriptKey());
      b2++;
    } 
    for (LocaleInfo localeInfo : sLocaleCache.values())
      localeInfo.setTranslated(set.contains(localeInfo.getLangScriptKey())); 
    addSuggestedLocalesForRegion(Locale.getDefault());
    sFullyInitialized = true;
  }
  
  private static int getLevel(Set<String> paramSet, LocaleInfo paramLocaleInfo, boolean paramBoolean) {
    if (paramSet.contains(paramLocaleInfo.getId()))
      return 0; 
    if (paramLocaleInfo.mIsPseudo)
      return 2; 
    if (paramBoolean && !paramLocaleInfo.isTranslated())
      return 0; 
    if (paramLocaleInfo.getParent() != null)
      return 2; 
    return 0;
  }
  
  public static Set<LocaleInfo> getLevelLocales(Context paramContext, Set<String> paramSet, LocaleInfo paramLocaleInfo, boolean paramBoolean) {
    String str;
    fillCache(paramContext);
    if (paramLocaleInfo == null) {
      paramContext = null;
    } else {
      str = paramLocaleInfo.getId();
    } 
    HashSet<LocaleInfo> hashSet = new HashSet();
    for (LocaleInfo localeInfo : sLocaleCache.values()) {
      int i = getLevel(paramSet, localeInfo, paramBoolean);
      if (i == 2) {
        if (paramLocaleInfo != null) {
          if (str.equals(localeInfo.getParent().toLanguageTag()))
            hashSet.add(localeInfo); 
          continue;
        } 
        if (localeInfo.isSuggestionOfType(1)) {
          hashSet.add(localeInfo);
          continue;
        } 
        hashSet.add(getLocaleInfo(localeInfo.getParent()));
      } 
    } 
    return hashSet;
  }
  
  public static LocaleInfo getLocaleInfo(Locale paramLocale) {
    LocaleInfo localeInfo;
    String str = paramLocale.toLanguageTag();
    if (!sLocaleCache.containsKey(str)) {
      localeInfo = new LocaleInfo(paramLocale);
      sLocaleCache.put(str, localeInfo);
    } else {
      localeInfo = sLocaleCache.get(str);
    } 
    return localeInfo;
  }
}
