package com.android.internal.app;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.prediction.AppTarget;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ShortcutManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.UserHandle;
import android.service.chooser.ChooserTarget;
import android.service.chooser.IChooserTargetResult;
import android.service.chooser.IChooserTargetService;
import android.text.TextUtils;
import com.android.internal.app.chooser.DisplayResolveInfo;
import java.util.ArrayList;
import java.util.List;

public class OplusChooserCtsConnection {
  private final ChooserHandler mChooserHandler = new ChooserHandler();
  
  private Context mContext;
  
  private ResolverListAdapter mResolverListAdapter;
  
  private OplusResolverManager mResolverManager;
  
  public void testCts(Context paramContext, OplusResolverManager paramOplusResolverManager, ResolverListAdapter paramResolverListAdapter, UserHandle paramUserHandle) {
    if (ActivityManager.isLowRamDeviceStatic())
      return; 
    this.mContext = paramContext;
    this.mResolverListAdapter = paramResolverListAdapter;
    this.mResolverManager = paramOplusResolverManager;
    Intent intent = ((Activity)paramContext).getIntent();
    Parcelable[] arrayOfParcelable = intent.getParcelableArrayExtra("android.intent.extra.CHOOSER_TARGETS");
    if (arrayOfParcelable != null) {
      int i = Math.min(arrayOfParcelable.length, 2);
      for (byte b = 0; b < i && 
        arrayOfParcelable[b] instanceof ChooserTarget; b++) {
        ChooserTarget chooserTarget = (ChooserTarget)arrayOfParcelable[b];
        Intent intent1 = new Intent();
        intent1.setComponent(chooserTarget.getComponentName());
        ResolveInfo resolveInfo = paramOplusResolverManager.getResolveInfo(intent1, this.mContext.getPackageManager());
        if (resolveInfo != null) {
          Intent intent2 = paramOplusResolverManager.getTargetIntent();
          DisplayResolveInfo displayResolveInfo = new DisplayResolveInfo(intent2, resolveInfo, paramOplusResolverManager.getTargetIntent(), null);
          (displayResolveInfo.getResolveInfo()).nonLocalizedLabel = chooserTarget.getTitle();
          paramResolverListAdapter.mDisplayList.add(0, displayResolveInfo);
        } 
      } 
    } 
    queryDirectShareTargets(paramContext, paramResolverListAdapter, paramUserHandle);
    queryTargetServices(paramContext, paramResolverListAdapter, paramUserHandle);
  }
  
  private void queryDirectShareTargets(Context paramContext, ResolverListAdapter paramResolverListAdapter, UserHandle paramUserHandle) {
    IntentFilter intentFilter = getTargetIntentFilter();
    if (intentFilter == null)
      return; 
    List list = paramResolverListAdapter.mDisplayList;
    AsyncTask.execute(new _$$Lambda$OplusChooserCtsConnection$V81cq1mE9Q8ymMbvlZSwlh3ssKg(this, paramContext, paramUserHandle, intentFilter, list));
  }
  
  private void queryTargetServices(Context paramContext, ResolverListAdapter paramResolverListAdapter, UserHandle paramUserHandle) {
    Context context = paramContext.createContextAsUser(paramUserHandle, 0);
    PackageManager packageManager = context.getPackageManager();
    ShortcutManager shortcutManager = (ShortcutManager)context.getSystemService(ShortcutManager.class);
    int i;
    byte b;
    for (i = paramResolverListAdapter.getDisplayResolveInfoCount(), b = 0; b < i; b++) {
      DisplayResolveInfo displayResolveInfo = paramResolverListAdapter.getDisplayResolveInfo(b);
      if (paramResolverListAdapter.getScore(displayResolveInfo) != 0.0F) {
        ActivityInfo activityInfo = (displayResolveInfo.getResolveInfo()).activityInfo;
        String str = activityInfo.packageName;
        if (!shortcutManager.hasShareTargets(str)) {
          Bundle bundle = activityInfo.metaData;
          if (bundle != null) {
            str = activityInfo.packageName;
            String str1 = bundle.getString("android.service.chooser.chooser_target_service");
            str = convertServiceName(str, str1);
          } else {
            str = null;
          } 
          if (str != null) {
            ComponentName componentName = new ComponentName(activityInfo.packageName, str);
            Intent intent = new Intent("android.service.chooser.ChooserTargetService");
            intent = intent.setComponent(componentName);
            try {
              String str1 = (packageManager.getServiceInfo(componentName, 0)).permission;
              boolean bool = "android.permission.BIND_CHOOSER_TARGET_SERVICE".equals(str1);
              if (bool) {
                OplusChooserTargetServiceConnection oplusChooserTargetServiceConnection = new OplusChooserTargetServiceConnection(paramContext, this, displayResolveInfo, paramUserHandle);
                paramContext.bindServiceAsUser(intent, oplusChooserTargetServiceConnection, 5, paramUserHandle);
              } 
            } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {}
          } 
        } 
      } 
    } 
  }
  
  static class OplusChooserTargetServiceConnection implements ServiceConnection {
    private final UserHandle mUserHandle;
    
    private DisplayResolveInfo mOriginalTarget;
    
    private final Object mLock = new Object();
    
    private Context mContext;
    
    private OplusChooserCtsConnection mConnection;
    
    private final IChooserTargetResult mChooserTargetResult = (IChooserTargetResult)new IChooserTargetResult.Stub() {
        final OplusChooserCtsConnection.OplusChooserTargetServiceConnection this$0;
        
        public void sendResult(List<ChooserTarget> param2List) throws RemoteException {
          synchronized (OplusChooserCtsConnection.OplusChooserTargetServiceConnection.this.mLock) {
            if (OplusChooserCtsConnection.OplusChooserTargetServiceConnection.this.mContext == null)
              return; 
            OplusChooserCtsConnection.OplusChooserTargetServiceConnection oplusChooserTargetServiceConnection1 = OplusChooserCtsConnection.OplusChooserTargetServiceConnection.this;
            Context context = oplusChooserTargetServiceConnection1.mContext.createContextAsUser(OplusChooserCtsConnection.OplusChooserTargetServiceConnection.this.mUserHandle, 0);
            oplusChooserTargetServiceConnection1 = OplusChooserCtsConnection.OplusChooserTargetServiceConnection.this;
            OplusChooserCtsConnection.OplusChooserTargetServiceConnection oplusChooserTargetServiceConnection3 = OplusChooserCtsConnection.OplusChooserTargetServiceConnection.this;
            String str = (oplusChooserTargetServiceConnection3.mOriginalTarget.getResolveInfo()).activityInfo.packageName;
            oplusChooserTargetServiceConnection1.filterServiceTargets(context, str, param2List);
            Message message = Message.obtain();
            message.what = 1;
            OplusChooserCtsConnection.ServiceResultInfo serviceResultInfo = new OplusChooserCtsConnection.ServiceResultInfo();
            DisplayResolveInfo displayResolveInfo = OplusChooserCtsConnection.OplusChooserTargetServiceConnection.this.mOriginalTarget;
            OplusChooserCtsConnection.OplusChooserTargetServiceConnection oplusChooserTargetServiceConnection4 = OplusChooserCtsConnection.OplusChooserTargetServiceConnection.this, oplusChooserTargetServiceConnection2 = OplusChooserCtsConnection.OplusChooserTargetServiceConnection.this;
            this(displayResolveInfo, param2List, oplusChooserTargetServiceConnection4, oplusChooserTargetServiceConnection2.mUserHandle);
            message.obj = serviceResultInfo;
            OplusChooserCtsConnection.OplusChooserTargetServiceConnection.this.mConnection.mChooserHandler.sendMessage(message);
            return;
          } 
        }
      };
    
    public OplusChooserTargetServiceConnection(Context param1Context, OplusChooserCtsConnection param1OplusChooserCtsConnection, DisplayResolveInfo param1DisplayResolveInfo, UserHandle param1UserHandle) {
      this.mContext = param1Context;
      this.mConnection = param1OplusChooserCtsConnection;
      this.mOriginalTarget = param1DisplayResolveInfo;
      this.mUserHandle = param1UserHandle;
    }
    
    private void filterServiceTargets(Context param1Context, String param1String, List<ChooserTarget> param1List) {
      // Byte code:
      //   0: aload_3
      //   1: ifnonnull -> 5
      //   4: return
      //   5: aload_1
      //   6: invokevirtual getPackageManager : ()Landroid/content/pm/PackageManager;
      //   9: astore_1
      //   10: aload_3
      //   11: invokeinterface size : ()I
      //   16: iconst_1
      //   17: isub
      //   18: istore #4
      //   20: iload #4
      //   22: iflt -> 127
      //   25: aload_3
      //   26: iload #4
      //   28: invokeinterface get : (I)Ljava/lang/Object;
      //   33: checkcast android/service/chooser/ChooserTarget
      //   36: astore #5
      //   38: aload #5
      //   40: invokevirtual getComponentName : ()Landroid/content/ComponentName;
      //   43: astore #5
      //   45: aload_2
      //   46: ifnull -> 64
      //   49: aload_2
      //   50: aload #5
      //   52: invokevirtual getPackageName : ()Ljava/lang/String;
      //   55: invokevirtual equals : (Ljava/lang/Object;)Z
      //   58: ifeq -> 64
      //   61: goto -> 121
      //   64: iconst_0
      //   65: istore #6
      //   67: aload_1
      //   68: aload #5
      //   70: iconst_0
      //   71: invokevirtual getActivityInfo : (Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
      //   74: astore #5
      //   76: aload #5
      //   78: getfield exported : Z
      //   81: ifeq -> 96
      //   84: aload #5
      //   86: getfield permission : Ljava/lang/String;
      //   89: astore #5
      //   91: aload #5
      //   93: ifnull -> 99
      //   96: iconst_1
      //   97: istore #6
      //   99: goto -> 107
      //   102: astore #5
      //   104: iconst_1
      //   105: istore #6
      //   107: iload #6
      //   109: ifeq -> 121
      //   112: aload_3
      //   113: iload #4
      //   115: invokeinterface remove : (I)Ljava/lang/Object;
      //   120: pop
      //   121: iinc #4, -1
      //   124: goto -> 20
      //   127: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #199	-> 0
      //   #200	-> 4
      //   #203	-> 5
      //   #204	-> 10
      //   #205	-> 25
      //   #206	-> 38
      //   #207	-> 45
      //   #209	-> 61
      //   #212	-> 64
      //   #214	-> 64
      //   #215	-> 76
      //   #218	-> 99
      //   #216	-> 102
      //   #217	-> 104
      //   #220	-> 107
      //   #221	-> 112
      //   #204	-> 121
      //   #224	-> 127
      // Exception table:
      //   from	to	target	type
      //   67	76	102	android/content/pm/PackageManager$NameNotFoundException
      //   76	91	102	android/content/pm/PackageManager$NameNotFoundException
    }
    
    public void onServiceConnected(ComponentName param1ComponentName, IBinder param1IBinder) {
      synchronized (this.mLock) {
        if (this.mContext == null)
          return; 
        IChooserTargetService iChooserTargetService = IChooserTargetService.Stub.asInterface(param1IBinder);
        try {
          ComponentName componentName = this.mOriginalTarget.getResolvedComponentName();
          DisplayResolveInfo displayResolveInfo = this.mOriginalTarget;
          IntentFilter intentFilter = (displayResolveInfo.getResolveInfo()).filter;
          IChooserTargetResult iChooserTargetResult = this.mChooserTargetResult;
          iChooserTargetService.getChooserTargets(componentName, intentFilter, iChooserTargetResult);
        } catch (RemoteException remoteException) {
          this.mContext.unbindService(this);
          destroy();
        } 
        return;
      } 
    }
    
    public void onServiceDisconnected(ComponentName param1ComponentName) {
      synchronized (this.mLock) {
        if (this.mContext == null)
          return; 
        this.mContext.unbindService(this);
        destroy();
        return;
      } 
    }
    
    public void destroy() {
      synchronized (this.mLock) {
        this.mContext = null;
        this.mOriginalTarget = null;
        return;
      } 
    }
    
    public ComponentName getComponentName() {
      return (this.mOriginalTarget.getResolveInfo()).activityInfo.getComponentName();
    }
  }
  
  class null extends IChooserTargetResult.Stub {
    final OplusChooserCtsConnection.OplusChooserTargetServiceConnection this$0;
    
    public void sendResult(List<ChooserTarget> param1List) throws RemoteException {
      synchronized (this.this$0.mLock) {
        if (this.this$0.mContext == null)
          return; 
        OplusChooserCtsConnection.OplusChooserTargetServiceConnection oplusChooserTargetServiceConnection1 = this.this$0;
        Context context = oplusChooserTargetServiceConnection1.mContext.createContextAsUser(this.this$0.mUserHandle, 0);
        oplusChooserTargetServiceConnection1 = this.this$0;
        OplusChooserCtsConnection.OplusChooserTargetServiceConnection oplusChooserTargetServiceConnection3 = this.this$0;
        String str = (oplusChooserTargetServiceConnection3.mOriginalTarget.getResolveInfo()).activityInfo.packageName;
        oplusChooserTargetServiceConnection1.filterServiceTargets(context, str, param1List);
        Message message = Message.obtain();
        message.what = 1;
        OplusChooserCtsConnection.ServiceResultInfo serviceResultInfo = new OplusChooserCtsConnection.ServiceResultInfo();
        DisplayResolveInfo displayResolveInfo = this.this$0.mOriginalTarget;
        OplusChooserCtsConnection.OplusChooserTargetServiceConnection oplusChooserTargetServiceConnection4 = this.this$0, oplusChooserTargetServiceConnection2 = this.this$0;
        this(displayResolveInfo, param1List, oplusChooserTargetServiceConnection4, oplusChooserTargetServiceConnection2.mUserHandle);
        message.obj = serviceResultInfo;
        this.this$0.mConnection.mChooserHandler.sendMessage(message);
        return;
      } 
    }
  }
  
  private static class ServiceResultInfo {
    public final OplusChooserCtsConnection.OplusChooserTargetServiceConnection connection;
    
    public final DisplayResolveInfo originalTarget;
    
    public final List<ChooserTarget> resultTargets;
    
    public final UserHandle userHandle;
    
    public ServiceResultInfo(DisplayResolveInfo param1DisplayResolveInfo, List<ChooserTarget> param1List, OplusChooserCtsConnection.OplusChooserTargetServiceConnection param1OplusChooserTargetServiceConnection, UserHandle param1UserHandle) {
      this.originalTarget = param1DisplayResolveInfo;
      this.resultTargets = param1List;
      this.connection = param1OplusChooserTargetServiceConnection;
      this.userHandle = param1UserHandle;
    }
  }
  
  class ChooserHandler extends Handler {
    private static final int CHOOSER_TARGET_SERVICE_RESULT = 1;
    
    private static final int SHORTCUT_MANAGER_SHARE_TARGET_RESULT = 4;
    
    final OplusChooserCtsConnection this$0;
    
    private ChooserHandler() {}
    
    public void handleMessage(Message param1Message) {
      if (((Activity)OplusChooserCtsConnection.this.mContext).isDestroyed())
        return; 
      int i = param1Message.what;
      if (i != 1) {
        if (i == 4) {
          OplusChooserCtsConnection.ServiceResultInfo serviceResultInfo = (OplusChooserCtsConnection.ServiceResultInfo)param1Message.obj;
          if (serviceResultInfo.resultTargets != null) {
            for (ChooserTarget chooserTarget : serviceResultInfo.resultTargets) {
              Intent intent1 = new Intent(serviceResultInfo.originalTarget.getResolvedIntent());
              intent1.setComponent(serviceResultInfo.originalTarget.getResolvedComponentName());
              ResolveInfo resolveInfo = OplusChooserCtsConnection.this.mResolverManager.getResolveInfo(intent1, OplusChooserCtsConnection.this.mContext.getPackageManager());
              if (resolveInfo == null)
                continue; 
              Intent intent2 = serviceResultInfo.originalTarget.getResolvedIntent();
              DisplayResolveInfo displayResolveInfo = serviceResultInfo.originalTarget;
              displayResolveInfo = new DisplayResolveInfo(intent2, resolveInfo, displayResolveInfo.getResolvedIntent(), null);
              (displayResolveInfo.getResolveInfo()).nonLocalizedLabel = chooserTarget.getTitle();
              (displayResolveInfo.getResolveInfo()).activityInfo.name = chooserTarget.getComponentName().getClassName();
              OplusChooserCtsConnection.this.mResolverListAdapter.mDisplayList.add(0, displayResolveInfo);
            } 
            OplusChooserCtsConnection.this.mResolverManager.resortListAndNotifyChange();
          } 
        } 
      } else {
        OplusChooserCtsConnection.ServiceResultInfo serviceResultInfo = (OplusChooserCtsConnection.ServiceResultInfo)param1Message.obj;
        if (serviceResultInfo.resultTargets != null) {
          for (ChooserTarget chooserTarget : serviceResultInfo.resultTargets) {
            Intent intent1 = new Intent(serviceResultInfo.originalTarget.getResolvedIntent());
            intent1.setComponent(serviceResultInfo.originalTarget.getResolvedComponentName());
            ResolveInfo resolveInfo = OplusChooserCtsConnection.this.mResolverManager.getResolveInfo(intent1, OplusChooserCtsConnection.this.mContext.getPackageManager());
            if (resolveInfo == null)
              continue; 
            Intent intent2 = serviceResultInfo.originalTarget.getResolvedIntent();
            DisplayResolveInfo displayResolveInfo = serviceResultInfo.originalTarget;
            displayResolveInfo = new DisplayResolveInfo(intent2, resolveInfo, displayResolveInfo.getResolvedIntent(), null);
            (displayResolveInfo.getResolveInfo()).nonLocalizedLabel = chooserTarget.getTitle();
            (displayResolveInfo.getResolveInfo()).activityInfo.name = chooserTarget.getComponentName().getClassName();
            OplusChooserCtsConnection.this.mResolverListAdapter.mDisplayList.add(0, displayResolveInfo);
          } 
          OplusChooserCtsConnection.this.mResolverManager.resortListAndNotifyChange();
        } 
        OplusChooserCtsConnection.this.mContext.unbindService(serviceResultInfo.connection);
        serviceResultInfo.connection.destroy();
      } 
    }
  }
  
  private String convertServiceName(String paramString1, String paramString2) {
    if (TextUtils.isEmpty(paramString2))
      return null; 
    if (paramString2.startsWith(".")) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append(paramString2);
      paramString1 = stringBuilder.toString();
    } else if (paramString2.indexOf('.') >= 0) {
      paramString1 = paramString2;
    } else {
      paramString1 = null;
    } 
    return paramString1;
  }
  
  private IntentFilter getTargetIntentFilter() {
    try {
      Uri uri;
      Intent intent = this.mResolverManager.getTargetIntent();
      String str = intent.getDataString();
      if (!TextUtils.isEmpty(str))
        return new IntentFilter(intent.getAction(), str); 
      if (intent.getType() == null)
        return null; 
      IntentFilter intentFilter = new IntentFilter();
      this(intent.getAction(), intent.getType());
      ArrayList<Uri> arrayList = new ArrayList();
      this();
      boolean bool = "android.intent.action.SEND".equals(intent.getAction());
      if (bool) {
        uri = (Uri)intent.getParcelableExtra("android.intent.extra.STREAM");
        if (uri != null)
          arrayList.add(uri); 
      } else {
        ArrayList<? extends Uri> arrayList1 = uri.getParcelableArrayListExtra("android.intent.extra.STREAM");
        if (arrayList1 != null)
          arrayList.addAll(arrayList1); 
      } 
      for (Uri uri1 : arrayList) {
        intentFilter.addDataScheme(uri1.getScheme());
        intentFilter.addDataAuthority(uri1.getAuthority(), null);
        intentFilter.addDataPath(uri1.getPath(), 0);
      } 
      return intentFilter;
    } catch (Exception exception) {
      return null;
    } 
  }
  
  private void sendShareShortcutInfoList(List<ShortcutManager.ShareShortcutInfo> paramList, List<DisplayResolveInfo> paramList1, List<AppTarget> paramList2, UserHandle paramUserHandle) {
    if (paramList2 == null || paramList2.size() == paramList.size()) {
      byte b1;
      if (paramList2 == null) {
        b1 = 2;
      } else {
        b1 = 3;
      } 
      for (byte b2 = 0; b2 < paramList1.size(); b2++) {
        ArrayList<ShortcutManager.ShareShortcutInfo> arrayList = new ArrayList();
        for (byte b = 0; b < paramList.size(); b++) {
          ComponentName componentName1 = ((DisplayResolveInfo)paramList1.get(b2)).getResolvedComponentName();
          ComponentName componentName2 = ((ShortcutManager.ShareShortcutInfo)paramList.get(b)).getTargetComponent();
          if (componentName1.equals(componentName2))
            arrayList.add(paramList.get(b)); 
        } 
        if (!arrayList.isEmpty()) {
          List<ChooserTarget> list = ((ChooserActivity)this.mContext).convertToChooserTarget(arrayList, paramList, paramList2, b1);
          Message message = Message.obtain();
          message.what = 4;
          message.obj = new ServiceResultInfo(paramList1.get(b2), list, null, paramUserHandle);
          message.arg1 = b1;
          this.mChooserHandler.sendMessage(message);
        } 
      } 
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("resultList and appTargets must have the same size. resultList.size()=");
    stringBuilder.append(paramList.size());
    stringBuilder.append(" appTargets.size()=");
    stringBuilder.append(paramList2.size());
    throw new RuntimeException(stringBuilder.toString());
  }
}
