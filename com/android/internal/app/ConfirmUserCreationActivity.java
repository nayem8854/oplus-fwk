package com.android.internal.app;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.UserInfo;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.os.UserManager;
import android.util.Log;

public class ConfirmUserCreationActivity extends AlertActivity implements DialogInterface.OnClickListener {
  private static final String TAG = "CreateUser";
  
  private String mAccountName;
  
  private PersistableBundle mAccountOptions;
  
  private String mAccountType;
  
  private boolean mCanProceed;
  
  private UserManager mUserManager;
  
  private String mUserName;
  
  public void onCreate(Bundle paramBundle) {
    super.onCreate(paramBundle);
    Intent intent = getIntent();
    this.mUserName = intent.getStringExtra("android.os.extra.USER_NAME");
    this.mAccountName = intent.getStringExtra("android.os.extra.USER_ACCOUNT_NAME");
    this.mAccountType = intent.getStringExtra("android.os.extra.USER_ACCOUNT_TYPE");
    this.mAccountOptions = (PersistableBundle)intent.getParcelableExtra("android.os.extra.USER_ACCOUNT_OPTIONS");
    this.mUserManager = (UserManager)getSystemService(UserManager.class);
    String str = checkUserCreationRequirements();
    if (str == null) {
      finish();
      return;
    } 
    AlertController.AlertParams alertParams = this.mAlertParams;
    alertParams.mMessage = str;
    alertParams.mPositiveButtonText = getString(17039370);
    alertParams.mPositiveButtonListener = this;
    if (this.mCanProceed) {
      alertParams.mNegativeButtonText = getString(17039360);
      alertParams.mNegativeButtonListener = this;
    } 
    setupAlert();
  }
  
  private String checkUserCreationRequirements() {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual getCallingPackage : ()Ljava/lang/String;
    //   4: astore_1
    //   5: aload_1
    //   6: ifnull -> 251
    //   9: aload_0
    //   10: invokevirtual getPackageManager : ()Landroid/content/pm/PackageManager;
    //   13: aload_1
    //   14: iconst_0
    //   15: invokevirtual getApplicationInfo : (Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    //   18: astore_1
    //   19: aload_0
    //   20: getfield mUserManager : Landroid/os/UserManager;
    //   23: ldc 'no_add_user'
    //   25: invokevirtual hasUserRestriction : (Ljava/lang/String;)Z
    //   28: ifne -> 51
    //   31: aload_0
    //   32: getfield mUserManager : Landroid/os/UserManager;
    //   35: astore_2
    //   36: aload_2
    //   37: invokevirtual isAdminUser : ()Z
    //   40: ifne -> 46
    //   43: goto -> 51
    //   46: iconst_0
    //   47: istore_3
    //   48: goto -> 53
    //   51: iconst_1
    //   52: istore_3
    //   53: aload_0
    //   54: getfield mUserManager : Landroid/os/UserManager;
    //   57: invokevirtual canAddMoreUsers : ()Z
    //   60: istore #4
    //   62: new android/accounts/Account
    //   65: dup
    //   66: aload_0
    //   67: getfield mAccountName : Ljava/lang/String;
    //   70: aload_0
    //   71: getfield mAccountType : Ljava/lang/String;
    //   74: invokespecial <init> : (Ljava/lang/String;Ljava/lang/String;)V
    //   77: astore_2
    //   78: aload_0
    //   79: getfield mAccountName : Ljava/lang/String;
    //   82: ifnull -> 139
    //   85: aload_0
    //   86: getfield mAccountType : Ljava/lang/String;
    //   89: ifnull -> 139
    //   92: aload_0
    //   93: invokestatic get : (Landroid/content/Context;)Landroid/accounts/AccountManager;
    //   96: aload_2
    //   97: invokevirtual someUserHasAccount : (Landroid/accounts/Account;)Z
    //   100: istore #5
    //   102: aload_0
    //   103: getfield mUserManager : Landroid/os/UserManager;
    //   106: astore #6
    //   108: aload_0
    //   109: getfield mAccountName : Ljava/lang/String;
    //   112: astore #7
    //   114: aload_0
    //   115: getfield mAccountType : Ljava/lang/String;
    //   118: astore_2
    //   119: iload #5
    //   121: aload #6
    //   123: aload #7
    //   125: aload_2
    //   126: invokevirtual someUserHasSeedAccount : (Ljava/lang/String;Ljava/lang/String;)Z
    //   129: ior
    //   130: ifeq -> 139
    //   133: iconst_1
    //   134: istore #8
    //   136: goto -> 142
    //   139: iconst_0
    //   140: istore #8
    //   142: aload_0
    //   143: iconst_1
    //   144: putfield mCanProceed : Z
    //   147: aload_1
    //   148: aload_0
    //   149: invokevirtual getPackageManager : ()Landroid/content/pm/PackageManager;
    //   152: invokevirtual loadLabel : (Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
    //   155: invokeinterface toString : ()Ljava/lang/String;
    //   160: astore_1
    //   161: iload_3
    //   162: ifeq -> 172
    //   165: aload_0
    //   166: iconst_1
    //   167: invokevirtual setResult : (I)V
    //   170: aconst_null
    //   171: areturn
    //   172: iload #4
    //   174: iconst_1
    //   175: ixor
    //   176: ifeq -> 186
    //   179: aload_0
    //   180: iconst_2
    //   181: invokevirtual setResult : (I)V
    //   184: aconst_null
    //   185: areturn
    //   186: iload #8
    //   188: ifeq -> 216
    //   191: aload_0
    //   192: ldc 17041453
    //   194: iconst_2
    //   195: anewarray java/lang/Object
    //   198: dup
    //   199: iconst_0
    //   200: aload_1
    //   201: aastore
    //   202: dup
    //   203: iconst_1
    //   204: aload_0
    //   205: getfield mAccountName : Ljava/lang/String;
    //   208: aastore
    //   209: invokevirtual getString : (I[Ljava/lang/Object;)Ljava/lang/String;
    //   212: astore_1
    //   213: goto -> 238
    //   216: aload_0
    //   217: ldc 17041454
    //   219: iconst_2
    //   220: anewarray java/lang/Object
    //   223: dup
    //   224: iconst_0
    //   225: aload_1
    //   226: aastore
    //   227: dup
    //   228: iconst_1
    //   229: aload_0
    //   230: getfield mAccountName : Ljava/lang/String;
    //   233: aastore
    //   234: invokevirtual getString : (I[Ljava/lang/Object;)Ljava/lang/String;
    //   237: astore_1
    //   238: aload_1
    //   239: areturn
    //   240: astore_1
    //   241: new java/lang/SecurityException
    //   244: dup
    //   245: ldc 'Cannot find the calling package'
    //   247: invokespecial <init> : (Ljava/lang/String;)V
    //   250: athrow
    //   251: new java/lang/SecurityException
    //   254: dup
    //   255: ldc 'User Creation intent must be launched with startActivityForResult'
    //   257: invokespecial <init> : (Ljava/lang/String;)V
    //   260: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #85	-> 0
    //   #86	-> 5
    //   #92	-> 9
    //   #96	-> 19
    //   #99	-> 19
    //   #100	-> 36
    //   #102	-> 53
    //   #104	-> 62
    //   #105	-> 78
    //   #106	-> 92
    //   #107	-> 119
    //   #108	-> 142
    //   #109	-> 147
    //   #110	-> 161
    //   #111	-> 165
    //   #112	-> 170
    //   #113	-> 172
    //   #114	-> 179
    //   #115	-> 184
    //   #116	-> 186
    //   #117	-> 191
    //   #119	-> 216
    //   #121	-> 238
    //   #93	-> 240
    //   #94	-> 241
    //   #87	-> 251
    // Exception table:
    //   from	to	target	type
    //   9	19	240	android/content/pm/PackageManager$NameNotFoundException
  }
  
  public void onClick(DialogInterface paramDialogInterface, int paramInt) {
    setResult(0);
    if (paramInt == -1 && this.mCanProceed) {
      Log.i("CreateUser", "Ok, creating user");
      UserInfo userInfo = this.mUserManager.createUser(this.mUserName, 0);
      if (userInfo == null) {
        Log.e("CreateUser", "Couldn't create user");
        finish();
        return;
      } 
      this.mUserManager.setSeedAccountData(userInfo.id, this.mAccountName, this.mAccountType, this.mAccountOptions);
      setResult(-1);
    } 
    finish();
  }
}
