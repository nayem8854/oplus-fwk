package com.android.internal.app;

import android.common.OplusFeatureCache;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.text.method.MovementMethod;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewStub;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.android.internal.R;
import java.lang.ref.WeakReference;

public class AlertController extends OplusBaseAlertController {
  protected final Window mWindow;
  
  private int mViewSpacingTop;
  
  private boolean mViewSpacingSpecified = false;
  
  private int mViewSpacingRight;
  
  private int mViewSpacingLeft;
  
  private int mViewSpacingBottom;
  
  private int mViewLayoutResId;
  
  private View mView;
  
  private TextView mTitleView;
  
  private CharSequence mTitle;
  
  private int mSingleChoiceItemLayout;
  
  private boolean mShowTitle;
  
  protected ScrollView mScrollView;
  
  private int mMultiChoiceItemLayout;
  
  protected TextView mMessageView;
  
  private MovementMethod mMessageMovementMethod;
  
  private Integer mMessageHyphenationFrequency;
  
  protected CharSequence mMessage;
  
  protected ListView mListView;
  
  private int mListLayout;
  
  private int mListItemLayout;
  
  private ImageView mIconView;
  
  private int mIconId = 0;
  
  private Drawable mIcon;
  
  private Handler mHandler;
  
  private boolean mForceInverseBackground;
  
  private final DialogInterface mDialogInterface;
  
  private View mCustomTitleView;
  
  private final Context mContext;
  
  private int mCheckedItem = -1;
  
  private CharSequence mButtonPositiveText;
  
  private Message mButtonPositiveMessage;
  
  private Button mButtonPositive;
  
  private int mButtonPanelSideLayout;
  
  private int mButtonPanelLayoutHint = 0;
  
  private CharSequence mButtonNeutralText;
  
  private Message mButtonNeutralMessage;
  
  private Button mButtonNeutral;
  
  private CharSequence mButtonNegativeText;
  
  private Message mButtonNegativeMessage;
  
  private Button mButtonNegative;
  
  private final View.OnClickListener mButtonHandler = new View.OnClickListener() {
      final AlertController this$0;
      
      public void onClick(View param1View) {
        if (param1View == AlertController.this.mButtonPositive && AlertController.this.mButtonPositiveMessage != null) {
          message = Message.obtain(AlertController.this.mButtonPositiveMessage);
        } else if (message == AlertController.this.mButtonNegative && AlertController.this.mButtonNegativeMessage != null) {
          message = Message.obtain(AlertController.this.mButtonNegativeMessage);
        } else if (message == AlertController.this.mButtonNeutral && AlertController.this.mButtonNeutralMessage != null) {
          message = Message.obtain(AlertController.this.mButtonNeutralMessage);
        } else {
          message = null;
        } 
        if (message != null)
          message.sendToTarget(); 
        Message message = AlertController.this.mHandler.obtainMessage(1, AlertController.this.mDialogInterface);
        message.sendToTarget();
      }
    };
  
  private int mAlertDialogLayout;
  
  private ListAdapter mAdapter;
  
  public static final int MICRO = 1;
  
  private static final class ButtonHandler extends Handler {
    private static final int MSG_DISMISS_DIALOG = 1;
    
    private WeakReference<DialogInterface> mDialog;
    
    public ButtonHandler(DialogInterface param1DialogInterface) {
      this.mDialog = new WeakReference<>(param1DialogInterface);
    }
    
    public void handleMessage(Message param1Message) {
      int i = param1Message.what;
      if (i != -3 && i != -2 && i != -1) {
        if (i == 1)
          ((DialogInterface)param1Message.obj).dismiss(); 
      } else {
        ((DialogInterface.OnClickListener)param1Message.obj).onClick(this.mDialog.get(), param1Message.what);
      } 
    }
  }
  
  private static boolean shouldCenterSingleButton(Context paramContext) {
    TypedValue typedValue = new TypedValue();
    Resources.Theme theme = paramContext.getTheme();
    boolean bool = true;
    theme.resolveAttribute(17956877, typedValue, true);
    if (typedValue.data == 0)
      bool = false; 
    return bool;
  }
  
  public static final AlertController create(Context paramContext, DialogInterface paramDialogInterface, Window paramWindow) {
    TypedArray typedArray = paramContext.obtainStyledAttributes(null, R.styleable.AlertDialog, 16842845, 16974371);
    int i = typedArray.getInt(12, 0);
    typedArray.recycle();
    if (i != 1)
      return ((IOplusAlertControllerEuclidManager)OplusFeatureCache.getOrCreate(IOplusAlertControllerEuclidManager.DEFAULT, new Object[0])).getAlertController(paramContext, paramDialogInterface, paramWindow); 
    return new MicroAlertController(paramContext, paramDialogInterface, paramWindow);
  }
  
  protected AlertController(Context paramContext, DialogInterface paramDialogInterface, Window paramWindow) {
    this.mContext = paramContext;
    this.mDialogInterface = paramDialogInterface;
    this.mWindow = paramWindow;
    this.mHandler = new ButtonHandler(paramDialogInterface);
    TypedArray typedArray = paramContext.obtainStyledAttributes(null, R.styleable.AlertDialog, 16842845, 0);
    this.mAlertDialogLayout = typedArray.getResourceId(10, 17367082);
    this.mButtonPanelSideLayout = typedArray.getResourceId(11, 0);
    this.mListLayout = typedArray.getResourceId(15, 17367292);
    this.mMultiChoiceItemLayout = typedArray.getResourceId(16, 17367059);
    this.mSingleChoiceItemLayout = typedArray.getResourceId(21, 17367058);
    this.mListItemLayout = typedArray.getResourceId(14, 17367057);
    this.mShowTitle = typedArray.getBoolean(20, true);
    typedArray.recycle();
    paramWindow.requestFeature(1);
  }
  
  static boolean canTextInput(View paramView) {
    if (paramView.onCheckIsTextEditor())
      return true; 
    if (!(paramView instanceof ViewGroup))
      return false; 
    ViewGroup viewGroup = (ViewGroup)paramView;
    int i = viewGroup.getChildCount();
    while (i > 0) {
      i--;
      paramView = viewGroup.getChildAt(i);
      if (canTextInput(paramView))
        return true; 
    } 
    return false;
  }
  
  public void installContent(AlertParams paramAlertParams) {
    paramAlertParams.apply(this);
    installContent();
  }
  
  public void installContent() {
    int i = selectContentView();
    this.mWindow.setContentView(i);
    setupView();
  }
  
  private int selectContentView() {
    int i = this.mButtonPanelSideLayout;
    if (i == 0)
      return this.mAlertDialogLayout; 
    if (this.mButtonPanelLayoutHint == 1)
      return i; 
    return this.mAlertDialogLayout;
  }
  
  public void setTitle(CharSequence paramCharSequence) {
    this.mTitle = paramCharSequence;
    TextView textView = this.mTitleView;
    if (textView != null)
      textView.setText(paramCharSequence); 
    this.mWindow.setTitle(paramCharSequence);
  }
  
  public void setCustomTitle(View paramView) {
    this.mCustomTitleView = paramView;
  }
  
  public void setMessage(CharSequence paramCharSequence) {
    this.mMessage = paramCharSequence;
    TextView textView = this.mMessageView;
    if (textView != null)
      textView.setText(paramCharSequence); 
  }
  
  public void setMessageMovementMethod(MovementMethod paramMovementMethod) {
    this.mMessageMovementMethod = paramMovementMethod;
    TextView textView = this.mMessageView;
    if (textView != null)
      textView.setMovementMethod(paramMovementMethod); 
  }
  
  public void setMessageHyphenationFrequency(int paramInt) {
    this.mMessageHyphenationFrequency = Integer.valueOf(paramInt);
    TextView textView = this.mMessageView;
    if (textView != null)
      textView.setHyphenationFrequency(paramInt); 
  }
  
  public void setView(int paramInt) {
    this.mView = null;
    this.mViewLayoutResId = paramInt;
    this.mViewSpacingSpecified = false;
  }
  
  public void setView(View paramView) {
    this.mView = paramView;
    this.mViewLayoutResId = 0;
    this.mViewSpacingSpecified = false;
  }
  
  public void setView(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.mView = paramView;
    this.mViewLayoutResId = 0;
    this.mViewSpacingSpecified = true;
    this.mViewSpacingLeft = paramInt1;
    this.mViewSpacingTop = paramInt2;
    this.mViewSpacingRight = paramInt3;
    this.mViewSpacingBottom = paramInt4;
  }
  
  public void setButtonPanelLayoutHint(int paramInt) {
    this.mButtonPanelLayoutHint = paramInt;
  }
  
  public void setButton(int paramInt, CharSequence paramCharSequence, DialogInterface.OnClickListener paramOnClickListener, Message paramMessage) {
    Message message = paramMessage;
    if (paramMessage == null) {
      message = paramMessage;
      if (paramOnClickListener != null)
        message = this.mHandler.obtainMessage(paramInt, paramOnClickListener); 
    } 
    if (paramInt != -3) {
      if (paramInt != -2) {
        if (paramInt == -1) {
          this.mButtonPositiveText = paramCharSequence;
          this.mButtonPositiveMessage = message;
        } else {
          throw new IllegalArgumentException("Button does not exist");
        } 
      } else {
        this.mButtonNegativeText = paramCharSequence;
        this.mButtonNegativeMessage = message;
      } 
    } else {
      this.mButtonNeutralText = paramCharSequence;
      this.mButtonNeutralMessage = message;
    } 
  }
  
  public void setIcon(int paramInt) {
    this.mIcon = null;
    this.mIconId = paramInt;
    ImageView imageView = this.mIconView;
    if (imageView != null)
      if (paramInt != 0) {
        imageView.setVisibility(0);
        this.mIconView.setImageResource(this.mIconId);
      } else {
        imageView.setVisibility(8);
      }  
  }
  
  public void setIcon(Drawable paramDrawable) {
    this.mIcon = paramDrawable;
    this.mIconId = 0;
    ImageView imageView = this.mIconView;
    if (imageView != null)
      if (paramDrawable != null) {
        imageView.setVisibility(0);
        this.mIconView.setImageDrawable(paramDrawable);
      } else {
        imageView.setVisibility(8);
      }  
  }
  
  public int getIconAttributeResId(int paramInt) {
    TypedValue typedValue = new TypedValue();
    this.mContext.getTheme().resolveAttribute(paramInt, typedValue, true);
    return typedValue.resourceId;
  }
  
  public void setInverseBackgroundForced(boolean paramBoolean) {
    this.mForceInverseBackground = paramBoolean;
  }
  
  public ListView getListView() {
    return this.mListView;
  }
  
  public Button getButton(int paramInt) {
    if (paramInt != -3) {
      if (paramInt != -2) {
        if (paramInt != -1)
          return null; 
        return this.mButtonPositive;
      } 
      return this.mButtonNegative;
    } 
    return this.mButtonNeutral;
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
    boolean bool;
    ScrollView scrollView = this.mScrollView;
    if (scrollView != null && scrollView.executeKeyEvent(paramKeyEvent)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent) {
    boolean bool;
    ScrollView scrollView = this.mScrollView;
    if (scrollView != null && scrollView.executeKeyEvent(paramKeyEvent)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private ViewGroup resolvePanel(View paramView1, View paramView2) {
    if (paramView1 == null) {
      paramView1 = paramView2;
      if (paramView2 instanceof ViewStub)
        paramView1 = ((ViewStub)paramView2).inflate(); 
      return (ViewGroup)paramView1;
    } 
    if (paramView2 != null) {
      ViewParent viewParent = paramView2.getParent();
      if (viewParent instanceof ViewGroup)
        ((ViewGroup)viewParent).removeView(paramView2); 
    } 
    paramView2 = paramView1;
    if (paramView1 instanceof ViewStub)
      paramView2 = ((ViewStub)paramView1).inflate(); 
    return (ViewGroup)paramView2;
  }
  
  private void setupView() {
    boolean bool1, bool2, bool3;
    View view1 = (View)this.mWindow.findViewById(16909279);
    ViewGroup viewGroup1 = (ViewGroup)view1.findViewById(16909558);
    View view2 = (View)view1.findViewById(16908864);
    View view3 = (View)view1.findViewById(16908809);
    ViewGroup viewGroup2 = view1.<ViewGroup>findViewById(16908901);
    setupCustomContent(viewGroup2);
    View view4 = (View)viewGroup2.findViewById(16909558);
    ViewGroup viewGroup3 = (ViewGroup)viewGroup2.findViewById(16908864);
    ViewGroup viewGroup4 = (ViewGroup)viewGroup2.findViewById(16908809);
    viewGroup1 = resolvePanel(view4, viewGroup1);
    viewGroup3 = resolvePanel(viewGroup3, view2);
    viewGroup4 = resolvePanel(viewGroup4, view3);
    setupContent(viewGroup3);
    setupButtons(viewGroup4);
    setupTitle(viewGroup1);
    if (viewGroup2 != null && 
      viewGroup2.getVisibility() != 8) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    if (viewGroup1 != null && 
      viewGroup1.getVisibility() != 8) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if (viewGroup4 != null && 
      viewGroup4.getVisibility() != 8) {
      bool3 = true;
    } else {
      bool3 = false;
    } 
    if (!view1.isInTouchMode()) {
      if (bool1) {
        view1 = viewGroup2;
      } else {
        view1 = viewGroup3;
      } 
      if (!requestFocusForContent(view1))
        requestFocusForDefaultButton(); 
    } 
    if (!bool3) {
      if (viewGroup3 != null) {
        view1 = viewGroup3.findViewById(16909525);
        if (view1 != null)
          view1.setVisibility(0); 
      } 
      this.mWindow.setCloseOnTouchOutsideIfNotSet(true);
    } 
    if (bool2) {
      view1 = this.mScrollView;
      if (view1 != null)
        view1.setClipToPadding(true); 
      view1 = null;
      if (this.mMessage != null || this.mListView != null || bool1) {
        if (!bool1)
          view1 = viewGroup1.findViewById(16909544); 
        view3 = view1;
        if (view1 == null)
          view3 = viewGroup1.findViewById(16909543); 
      } else {
        view3 = viewGroup1.findViewById(16909545);
      } 
      if (view3 != null)
        view3.setVisibility(0); 
    } else if (viewGroup3 != null) {
      view1 = viewGroup3.findViewById(16909526);
      if (view1 != null)
        view1.setVisibility(0); 
    } 
    view1 = this.mListView;
    if (view1 instanceof RecycleListView)
      ((RecycleListView)view1).setHasDecor(bool2, bool3); 
    if (!bool1) {
      view1 = this.mListView;
      if (view1 == null)
        view1 = this.mScrollView; 
      if (view1 != null) {
        boolean bool4, bool5;
        if (bool2) {
          bool4 = true;
        } else {
          bool4 = false;
        } 
        if (bool3) {
          bool5 = true;
        } else {
          bool5 = false;
        } 
        view1.setScrollIndicators(bool4 | bool5, 3);
      } 
    } 
    TypedArray typedArray = this.mContext.obtainStyledAttributes(null, R.styleable.AlertDialog, 16842845, 0);
    setBackground(typedArray, viewGroup1, viewGroup3, viewGroup2, viewGroup4, bool2, bool1, bool3);
    typedArray.recycle();
  }
  
  private boolean requestFocusForContent(View paramView) {
    if (paramView != null && paramView.requestFocus())
      return true; 
    paramView = this.mListView;
    if (paramView != null) {
      paramView.setSelection(0);
      return true;
    } 
    return false;
  }
  
  private void requestFocusForDefaultButton() {
    if (this.mButtonPositive.getVisibility() == 0) {
      this.mButtonPositive.requestFocus();
    } else if (this.mButtonNegative.getVisibility() == 0) {
      this.mButtonNegative.requestFocus();
    } else if (this.mButtonNeutral.getVisibility() == 0) {
      this.mButtonNeutral.requestFocus();
    } 
  }
  
  private void setupCustomContent(ViewGroup paramViewGroup) {
    View view = this.mView;
    boolean bool = false;
    if (view != null) {
      view = this.mView;
    } else if (this.mViewLayoutResId != 0) {
      LayoutInflater layoutInflater = LayoutInflater.from(this.mContext);
      View view1 = layoutInflater.inflate(this.mViewLayoutResId, paramViewGroup, false);
    } else {
      view = null;
    } 
    if (view != null)
      bool = true; 
    if (!bool || !canTextInput(view))
      this.mWindow.setFlags(131072, 131072); 
    if (bool) {
      FrameLayout frameLayout = this.mWindow.<FrameLayout>findViewById(16908331);
      frameLayout.addView(view, new ViewGroup.LayoutParams(-1, -1));
      if (this.mViewSpacingSpecified)
        frameLayout.setPadding(this.mViewSpacingLeft, this.mViewSpacingTop, this.mViewSpacingRight, this.mViewSpacingBottom); 
      if (this.mListView != null)
        ((LinearLayout.LayoutParams)paramViewGroup.getLayoutParams()).weight = 0.0F; 
    } else {
      paramViewGroup.setVisibility(8);
    } 
  }
  
  protected void setupTitle(ViewGroup paramViewGroup) {
    if (this.mCustomTitleView != null && this.mShowTitle) {
      ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -2);
      paramViewGroup.addView(this.mCustomTitleView, 0, layoutParams);
      paramViewGroup = this.mWindow.findViewById(16909548);
      paramViewGroup.setVisibility(8);
    } else {
      TextView textView;
      this.mIconView = this.mWindow.<ImageView>findViewById(16908294);
      boolean bool = TextUtils.isEmpty(this.mTitle);
      if ((bool ^ true) != 0 && this.mShowTitle) {
        this.mTitleView = textView = this.mWindow.<TextView>findViewById(16908739);
        textView.setText(this.mTitle);
        int i = this.mIconId;
        if (i != 0) {
          this.mIconView.setImageResource(i);
        } else {
          Drawable drawable = this.mIcon;
          if (drawable != null) {
            this.mIconView.setImageDrawable(drawable);
          } else {
            textView = this.mTitleView;
            int j = this.mIconView.getPaddingLeft();
            ImageView imageView = this.mIconView;
            int k = imageView.getPaddingTop();
            imageView = this.mIconView;
            int m = imageView.getPaddingRight();
            imageView = this.mIconView;
            i = imageView.getPaddingBottom();
            textView.setPadding(j, k, m, i);
            this.mIconView.setVisibility(8);
          } 
        } 
      } else {
        View view = (View)this.mWindow.findViewById(16909548);
        view.setVisibility(8);
        this.mIconView.setVisibility(8);
        textView.setVisibility(8);
      } 
    } 
  }
  
  protected void setupContent(ViewGroup paramViewGroup) {
    ScrollView scrollView = paramViewGroup.<ScrollView>findViewById(16909386);
    scrollView.setFocusable(false);
    TextView textView = paramViewGroup.<TextView>findViewById(16908299);
    if (textView == null)
      return; 
    CharSequence charSequence = this.mMessage;
    if (charSequence != null) {
      textView.setText(charSequence);
      MovementMethod movementMethod = this.mMessageMovementMethod;
      if (movementMethod != null)
        this.mMessageView.setMovementMethod(movementMethod); 
      Integer integer = this.mMessageHyphenationFrequency;
      if (integer != null)
        this.mMessageView.setHyphenationFrequency(integer.intValue()); 
    } else {
      textView.setVisibility(8);
      this.mScrollView.removeView(this.mMessageView);
      if (this.mListView != null) {
        paramViewGroup = (ViewGroup)this.mScrollView.getParent();
        int i = paramViewGroup.indexOfChild(this.mScrollView);
        paramViewGroup.removeViewAt(i);
        paramViewGroup.addView(this.mListView, i, new ViewGroup.LayoutParams(-1, -1));
      } else {
        paramViewGroup.setVisibility(8);
      } 
    } 
  }
  
  private static void manageScrollIndicators(View paramView1, View paramView2, View paramView3) {
    boolean bool = false;
    if (paramView2 != null) {
      byte b;
      if (paramView1.canScrollVertically(-1)) {
        b = 0;
      } else {
        b = 4;
      } 
      paramView2.setVisibility(b);
    } 
    if (paramView3 != null) {
      byte b;
      if (paramView1.canScrollVertically(1)) {
        b = bool;
      } else {
        b = 4;
      } 
      paramView3.setVisibility(b);
    } 
  }
  
  protected void setupButtons(ViewGroup paramViewGroup) {
    int i = 0;
    Button button = paramViewGroup.<Button>findViewById(16908313);
    button.setOnClickListener(this.mButtonHandler);
    boolean bool = TextUtils.isEmpty(this.mButtonPositiveText);
    boolean bool1 = false;
    if (bool) {
      this.mButtonPositive.setVisibility(8);
    } else {
      this.mButtonPositive.setText(this.mButtonPositiveText);
      this.mButtonPositive.setVisibility(0);
      i = false | true;
    } 
    this.mButtonNegative = button = paramViewGroup.<Button>findViewById(16908314);
    button.setOnClickListener(this.mButtonHandler);
    if (TextUtils.isEmpty(this.mButtonNegativeText)) {
      this.mButtonNegative.setVisibility(8);
    } else {
      this.mButtonNegative.setText(this.mButtonNegativeText);
      this.mButtonNegative.setVisibility(0);
      i |= 0x2;
    } 
    this.mButtonNeutral = button = paramViewGroup.<Button>findViewById(16908315);
    button.setOnClickListener(this.mButtonHandler);
    if (TextUtils.isEmpty(this.mButtonNeutralText)) {
      this.mButtonNeutral.setVisibility(8);
    } else {
      this.mButtonNeutral.setText(this.mButtonNeutralText);
      this.mButtonNeutral.setVisibility(0);
      i |= 0x4;
    } 
    if (shouldCenterSingleButton(this.mContext))
      if (i == 1) {
        centerButton(this.mButtonPositive);
      } else if (i == 2) {
        centerButton(this.mButtonNegative);
      } else if (i == 4) {
        centerButton(this.mButtonNeutral);
      }  
    if (i != 0)
      bool1 = true; 
    if (!bool1)
      paramViewGroup.setVisibility(8); 
  }
  
  private void centerButton(Button paramButton) {
    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)paramButton.getLayoutParams();
    layoutParams.gravity = 1;
    layoutParams.weight = 0.5F;
    paramButton.setLayoutParams(layoutParams);
    paramButton = this.mWindow.findViewById(16909122);
    if (paramButton != null)
      paramButton.setVisibility(0); 
    paramButton = this.mWindow.findViewById(16909367);
    if (paramButton != null)
      paramButton.setVisibility(0); 
  }
  
  private void setBackground(TypedArray paramTypedArray, View paramView1, View paramView2, View paramView3, View paramView4, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3) {
    int i = 0;
    int j = 0;
    int k = 0;
    int m = 0;
    int n = 0;
    int i1 = 0;
    int i2 = 0;
    int i3 = 0;
    int i4 = 0;
    boolean bool = paramTypedArray.getBoolean(17, true);
    if (bool) {
      i = 17303300;
      j = 17303314;
      k = 17303297;
      m = 17303294;
      n = 17303299;
      i1 = 17303313;
      i2 = 17303296;
      i3 = 17303293;
      i4 = 17303295;
    } 
    i1 = paramTypedArray.getResourceId(5, i1);
    int i5 = paramTypedArray.getResourceId(1, j);
    int i6 = paramTypedArray.getResourceId(6, i2);
    i2 = paramTypedArray.getResourceId(2, k);
    View[] arrayOfView = new View[4];
    boolean[] arrayOfBoolean = new boolean[4];
    bool = false;
    k = 0;
    if (paramBoolean1) {
      arrayOfView[0] = paramView1;
      arrayOfBoolean[0] = false;
      k = 0 + 1;
    } 
    if (paramView2.getVisibility() == 8)
      paramView2 = null; 
    arrayOfView[k] = paramView2;
    if (this.mListView != null) {
      paramBoolean1 = true;
    } else {
      paramBoolean1 = false;
    } 
    arrayOfBoolean[k] = paramBoolean1;
    j = k + 1;
    k = j;
    if (paramBoolean2) {
      arrayOfView[j] = paramView3;
      arrayOfBoolean[j] = this.mForceInverseBackground;
      k = j + 1;
    } 
    if (paramBoolean3) {
      arrayOfView[k] = paramView4;
      arrayOfBoolean[k] = true;
    } 
    j = 0;
    boolean bool1;
    for (paramView1 = null, bool1 = false, paramBoolean1 = bool, k = i1, i1 = bool1; i1 < arrayOfView.length; i1++) {
      paramView2 = arrayOfView[i1];
      if (paramView2 != null) {
        if (paramView1 != null) {
          if (j == 0) {
            if (paramBoolean1) {
              j = k;
            } else {
              j = i5;
            } 
            paramView1.setBackgroundResource(j);
          } else {
            if (paramBoolean1) {
              j = i6;
            } else {
              j = i2;
            } 
            paramView1.setBackgroundResource(j);
          } 
          j = 1;
        } 
        paramBoolean1 = arrayOfBoolean[i1];
        paramView1 = paramView2;
      } 
    } 
    k = i;
    if (paramView1 != null)
      if (j != 0) {
        k = paramTypedArray.getResourceId(7, i3);
        i2 = paramTypedArray.getResourceId(8, i4);
        j = paramTypedArray.getResourceId(3, m);
        if (paramBoolean1) {
          if (paramBoolean3)
            k = i2; 
        } else {
          k = j;
        } 
        paramView1.setBackgroundResource(k);
        k = i;
      } else {
        i2 = paramTypedArray.getResourceId(4, n);
        k = paramTypedArray.getResourceId(0, i);
        if (paramBoolean1) {
          i = i2;
        } else {
          i = k;
        } 
        paramView1.setBackgroundResource(i);
      }  
    paramView1 = this.mListView;
    if (paramView1 != null) {
      ListAdapter listAdapter = this.mAdapter;
      if (listAdapter != null) {
        paramView1.setAdapter(listAdapter);
        k = this.mCheckedItem;
        if (k > -1) {
          paramView1.setItemChecked(k, true);
          i = paramTypedArray.getDimensionPixelSize(19, 0);
          paramView1.setSelectionFromTop(k, i);
        } 
      } 
    } 
  }
  
  class RecycleListView extends ListView {
    private final int mPaddingBottomNoButtons;
    
    private final int mPaddingTopNoTitle;
    
    boolean mRecycleOnMeasure = true;
    
    public RecycleListView() {
      this((Context)this$0, (AttributeSet)null);
    }
    
    public RecycleListView(AttributeSet param1AttributeSet) {
      super((Context)this$0, param1AttributeSet);
      TypedArray typedArray = this$0.obtainStyledAttributes(param1AttributeSet, R.styleable.RecycleListView);
      this.mPaddingBottomNoButtons = typedArray.getDimensionPixelOffset(0, -1);
      this.mPaddingTopNoTitle = typedArray.getDimensionPixelOffset(1, -1);
    }
    
    public void setHasDecor(boolean param1Boolean1, boolean param1Boolean2) {
      if (!param1Boolean2 || !param1Boolean1) {
        int j, m, i = getPaddingLeft();
        if (param1Boolean1) {
          j = getPaddingTop();
        } else {
          j = this.mPaddingTopNoTitle;
        } 
        int k = getPaddingRight();
        if (param1Boolean2) {
          m = getPaddingBottom();
        } else {
          m = this.mPaddingBottomNoButtons;
        } 
        setPadding(i, j, k, m);
      } 
    }
    
    protected boolean recycleOnMeasure() {
      return this.mRecycleOnMeasure;
    }
  }
  
  public static class AlertParams extends OplusBaseAlertController.BaseAlertParams {
    public int mIconId = 0;
    
    public int mIconAttrId = 0;
    
    public boolean mViewSpacingSpecified = false;
    
    public int mCheckedItem = -1;
    
    public boolean mRecycleOnMeasure = true;
    
    public ListAdapter mAdapter;
    
    public boolean mCancelable;
    
    public boolean[] mCheckedItems;
    
    public final Context mContext;
    
    public Cursor mCursor;
    
    public View mCustomTitleView;
    
    public boolean mForceInverseBackground;
    
    public Drawable mIcon;
    
    public final LayoutInflater mInflater;
    
    public String mIsCheckedColumn;
    
    public boolean mIsMultiChoice;
    
    public boolean mIsSingleChoice;
    
    public CharSequence[] mItems;
    
    public String mLabelColumn;
    
    public CharSequence mMessage;
    
    public DialogInterface.OnClickListener mNegativeButtonListener;
    
    public CharSequence mNegativeButtonText;
    
    public DialogInterface.OnClickListener mNeutralButtonListener;
    
    public CharSequence mNeutralButtonText;
    
    public DialogInterface.OnCancelListener mOnCancelListener;
    
    public DialogInterface.OnMultiChoiceClickListener mOnCheckboxClickListener;
    
    public DialogInterface.OnClickListener mOnClickListener;
    
    public DialogInterface.OnDismissListener mOnDismissListener;
    
    public AdapterView.OnItemSelectedListener mOnItemSelectedListener;
    
    public DialogInterface.OnKeyListener mOnKeyListener;
    
    public OnPrepareListViewListener mOnPrepareListViewListener;
    
    public DialogInterface.OnClickListener mPositiveButtonListener;
    
    public CharSequence mPositiveButtonText;
    
    public CharSequence mTitle;
    
    public View mView;
    
    public int mViewLayoutResId;
    
    public int mViewSpacingBottom;
    
    public int mViewSpacingLeft;
    
    public int mViewSpacingRight;
    
    public int mViewSpacingTop;
    
    public AlertParams(Context param1Context) {
      this.mContext = param1Context;
      this.mCancelable = true;
      this.mInflater = (LayoutInflater)param1Context.getSystemService("layout_inflater");
    }
    
    public Context getContext() {
      return this.mContext;
    }
    
    public void setItems(CharSequence[] param1ArrayOfCharSequence) {
      this.mItems = param1ArrayOfCharSequence;
    }
    
    public void setOnClickListener(DialogInterface.OnClickListener param1OnClickListener) {
      this.mOnClickListener = param1OnClickListener;
    }
    
    public void apply(AlertController param1AlertController) {
      View view2 = this.mCustomTitleView;
      if (view2 != null) {
        param1AlertController.setCustomTitle(view2);
      } else {
        CharSequence charSequence1 = this.mTitle;
        if (charSequence1 != null)
          param1AlertController.setTitle(charSequence1); 
        Drawable drawable = this.mIcon;
        if (drawable != null)
          param1AlertController.setIcon(drawable); 
        int i = this.mIconId;
        if (i != 0)
          param1AlertController.setIcon(i); 
        i = this.mIconAttrId;
        if (i != 0)
          param1AlertController.setIcon(param1AlertController.getIconAttributeResId(i)); 
      } 
      CharSequence charSequence = this.mMessage;
      if (charSequence != null)
        param1AlertController.setMessage(charSequence); 
      charSequence = this.mPositiveButtonText;
      if (charSequence != null)
        param1AlertController.setButton(-1, charSequence, this.mPositiveButtonListener, null); 
      charSequence = this.mNegativeButtonText;
      if (charSequence != null)
        param1AlertController.setButton(-2, charSequence, this.mNegativeButtonListener, null); 
      charSequence = this.mNeutralButtonText;
      if (charSequence != null)
        param1AlertController.setButton(-3, charSequence, this.mNeutralButtonListener, null); 
      if (this.mForceInverseBackground)
        param1AlertController.setInverseBackgroundForced(true); 
      if (this.mItems != null || this.mCursor != null || this.mAdapter != null)
        createListView(param1AlertController); 
      View view1 = this.mView;
      if (view1 != null) {
        if (this.mViewSpacingSpecified) {
          param1AlertController.setView(view1, this.mViewSpacingLeft, this.mViewSpacingTop, this.mViewSpacingRight, this.mViewSpacingBottom);
        } else {
          param1AlertController.setView(view1);
        } 
      } else {
        int i = this.mViewLayoutResId;
        if (i != 0)
          param1AlertController.setView(i); 
      } 
      hookApply(param1AlertController);
    }
    
    private void createListView(final AlertController dialog) {
      LayoutInflater layoutInflater = this.mInflater;
      final AlertController.RecycleListView listView = (AlertController.RecycleListView)layoutInflater.inflate(dialog.mListLayout, (ViewGroup)null);
      if (needHookAdapter(this.mIsSingleChoice, this.mIsMultiChoice, this.mSummaries)) {
        object = getHookAdapter(this.mContext, this.mTitle, this.mMessage, this.mItems, this.mSummaries);
      } else if (this.mIsMultiChoice) {
        object = this.mCursor;
        if (object == null) {
          Context context = this.mContext;
          object = new Object(this, context, dialog.mMultiChoiceItemLayout, 16908308, this.mItems, recycleListView);
        } else {
          object = new Object(this, this.mContext, (Cursor)object, false, recycleListView, dialog);
        } 
      } else {
        int i;
        if (this.mIsSingleChoice) {
          i = dialog.mSingleChoiceItemLayout;
        } else {
          i = dialog.mListItemLayout;
        } 
        object = this.mCursor;
        if (object != null) {
          SimpleCursorAdapter simpleCursorAdapter = new SimpleCursorAdapter(this.mContext, i, (Cursor)object, new String[] { this.mLabelColumn }, new int[] { 16908308 });
        } else if (this.mAdapter != null) {
          ListAdapter listAdapter = this.mAdapter;
        } else {
          object = new AlertController.CheckedItemAdapter(this.mContext, i, 16908308, this.mItems);
        } 
      } 
      OnPrepareListViewListener onPrepareListViewListener = this.mOnPrepareListViewListener;
      if (onPrepareListViewListener != null)
        onPrepareListViewListener.onPrepareListView(recycleListView); 
      AlertController.access$1202(dialog, (ListAdapter)object);
      AlertController.access$1302(dialog, this.mCheckedItem);
      if (this.mOnClickListener != null) {
        recycleListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
              final AlertController.AlertParams this$0;
              
              final AlertController val$dialog;
              
              public void onItemClick(AdapterView<?> param2AdapterView, View param2View, int param2Int, long param2Long) {
                AlertController.AlertParams.this.mOnClickListener.onClick(dialog.mDialogInterface, param2Int);
                if (!AlertController.AlertParams.this.mIsSingleChoice)
                  dialog.mDialogInterface.dismiss(); 
              }
            });
      } else if (this.mOnCheckboxClickListener != null) {
        recycleListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
              final AlertController.AlertParams this$0;
              
              final AlertController val$dialog;
              
              final AlertController.RecycleListView val$listView;
              
              public void onItemClick(AdapterView<?> param2AdapterView, View param2View, int param2Int, long param2Long) {
                if (AlertController.AlertParams.this.mCheckedItems != null)
                  AlertController.AlertParams.this.mCheckedItems[param2Int] = listView.isItemChecked(param2Int); 
                DialogInterface.OnMultiChoiceClickListener onMultiChoiceClickListener = AlertController.AlertParams.this.mOnCheckboxClickListener;
                AlertController alertController = dialog;
                DialogInterface dialogInterface = alertController.mDialogInterface;
                boolean bool = listView.isItemChecked(param2Int);
                onMultiChoiceClickListener.onClick(dialogInterface, param2Int, bool);
              }
            });
      } 
      Object object = this.mOnItemSelectedListener;
      if (object != null)
        recycleListView.setOnItemSelectedListener((AdapterView.OnItemSelectedListener)object); 
      ((IOplusAlertControllerEuclidManager)OplusFeatureCache.getOrCreate(IOplusAlertControllerEuclidManager.DEFAULT, new Object[0])).setListStyle(recycleListView, this.mIsSingleChoice, this.mIsMultiChoice);
      recycleListView.mRecycleOnMeasure = this.mRecycleOnMeasure;
      dialog.mListView = recycleListView;
    }
    
    class OnPrepareListViewListener {
      public abstract void onPrepareListView(ListView param2ListView);
    }
  }
  
  class null implements AdapterView.OnItemClickListener {
    final AlertController.AlertParams this$0;
    
    final AlertController val$dialog;
    
    public void onItemClick(AdapterView<?> param1AdapterView, View param1View, int param1Int, long param1Long) {
      this.this$0.mOnClickListener.onClick(dialog.mDialogInterface, param1Int);
      if (!this.this$0.mIsSingleChoice)
        dialog.mDialogInterface.dismiss(); 
    }
  }
  
  class null implements AdapterView.OnItemClickListener {
    final AlertController.AlertParams this$0;
    
    final AlertController val$dialog;
    
    final AlertController.RecycleListView val$listView;
    
    public void onItemClick(AdapterView<?> param1AdapterView, View param1View, int param1Int, long param1Long) {
      if (this.this$0.mCheckedItems != null)
        this.this$0.mCheckedItems[param1Int] = listView.isItemChecked(param1Int); 
      DialogInterface.OnMultiChoiceClickListener onMultiChoiceClickListener = this.this$0.mOnCheckboxClickListener;
      AlertController alertController = dialog;
      DialogInterface dialogInterface = alertController.mDialogInterface;
      boolean bool = listView.isItemChecked(param1Int);
      onMultiChoiceClickListener.onClick(dialogInterface, param1Int, bool);
    }
  }
  
  class CheckedItemAdapter extends ArrayAdapter<CharSequence> {
    public CheckedItemAdapter(AlertController this$0, int param1Int1, int param1Int2, CharSequence[] param1ArrayOfCharSequence) {
      super((Context)this$0, param1Int1, param1Int2, param1ArrayOfCharSequence);
    }
    
    public boolean hasStableIds() {
      return true;
    }
    
    public long getItemId(int param1Int) {
      return param1Int;
    }
    
    public View getView(int param1Int, View param1View, ViewGroup param1ViewGroup) {
      param1View = super.getView(param1Int, param1View, param1ViewGroup);
      IOplusAlertControllerEuclidManager iOplusAlertControllerEuclidManager = (IOplusAlertControllerEuclidManager)OplusFeatureCache.getOrCreate(IOplusAlertControllerEuclidManager.DEFAULT, new Object[0]);
      int i = getCount();
      View view = iOplusAlertControllerEuclidManager.getConvertView(param1View, param1Int, i);
      if (view != null)
        param1View = view; 
      return param1View;
    }
  }
  
  public CharSequence getAlertControllerTitle() {
    return this.mTitle;
  }
  
  public Button getAlertControllerButtonPositive() {
    return this.mButtonPositive;
  }
  
  public CharSequence getAlertControllerButtonPositiveText() {
    return this.mButtonPositiveText;
  }
  
  public Button getAlertControllerButtonNegative() {
    return this.mButtonNegative;
  }
  
  public CharSequence getAlertControllerButtonNegativeText() {
    return this.mButtonNegativeText;
  }
  
  public Button getAlertControllerButtonNeutral() {
    return this.mButtonNeutral;
  }
  
  public CharSequence getAlertControllerButtonNeutralText() {
    return this.mButtonNeutralText;
  }
  
  protected void setupViewWrapper() {
    setupView();
  }
  
  protected int selectContentViewWrapper() {
    return selectContentView();
  }
  
  class OnPrepareListViewListener {
    public abstract void onPrepareListView(ListView param1ListView);
  }
}
