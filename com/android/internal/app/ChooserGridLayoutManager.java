package com.android.internal.app;

import android.content.Context;
import android.util.AttributeSet;
import com.android.internal.widget.GridLayoutManager;
import com.android.internal.widget.RecyclerView;

public class ChooserGridLayoutManager extends GridLayoutManager {
  private boolean mVerticalScrollEnabled = true;
  
  public ChooserGridLayoutManager(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  public ChooserGridLayoutManager(Context paramContext, int paramInt) {
    super(paramContext, paramInt);
  }
  
  public ChooserGridLayoutManager(Context paramContext, int paramInt1, int paramInt2, boolean paramBoolean) {
    super(paramContext, paramInt1, paramInt2, paramBoolean);
  }
  
  public int getRowCountForAccessibility(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState) {
    return super.getRowCountForAccessibility(paramRecycler, paramState) - 1;
  }
  
  void setVerticalScrollEnabled(boolean paramBoolean) {
    this.mVerticalScrollEnabled = paramBoolean;
  }
  
  public boolean canScrollVertically() {
    boolean bool;
    if (this.mVerticalScrollEnabled && super.canScrollVertically()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
}
