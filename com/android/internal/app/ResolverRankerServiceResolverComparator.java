package com.android.internal.app;

import android.app.usage.UsageStats;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.metrics.LogMaker;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.os.UserHandle;
import android.service.resolver.IResolverRankerResult;
import android.service.resolver.IResolverRankerService;
import android.service.resolver.ResolverTarget;
import android.util.ArrayMap;
import android.util.Log;
import com.android.internal.logging.MetricsLogger;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class ResolverRankerServiceResolverComparator extends AbstractResolverComparator {
  private final LinkedHashMap<ComponentName, ResolverTarget> mTargetsDict = new LinkedHashMap<>();
  
  private ArrayList<ResolverTarget> mTargets;
  
  private final Map<String, UsageStats> mStats;
  
  private final long mSinceTime;
  
  private ComponentName mResolvedRankerName;
  
  private final String mReferrerPackage;
  
  private ComponentName mRankerServiceName;
  
  private IResolverRankerService mRanker;
  
  private final Object mLock = new Object();
  
  private final long mCurrentTime;
  
  private Context mContext;
  
  private ResolverRankerServiceConnection mConnection;
  
  private CountDownLatch mConnectSignal;
  
  private final Collator mCollator;
  
  private String mAction;
  
  private static final long USAGE_STATS_PERIOD = 604800000L;
  
  private static final String TAG = "RRSResolverComparator";
  
  private static final long RECENCY_TIME_PERIOD = 43200000L;
  
  private static final float RECENCY_MULTIPLIER = 2.0F;
  
  private static final boolean DEBUG = false;
  
  private static final int CONNECTION_COST_TIMEOUT_MILLIS = 200;
  
  public ResolverRankerServiceResolverComparator(Context paramContext, Intent paramIntent, String paramString, AbstractResolverComparator.AfterCompute paramAfterCompute) {
    super(paramContext, paramIntent);
    this.mCollator = Collator.getInstance((paramContext.getResources().getConfiguration()).locale);
    this.mReferrerPackage = paramString;
    this.mContext = paramContext;
    long l = System.currentTimeMillis();
    this.mSinceTime = l - 604800000L;
    this.mStats = this.mUsm.queryAndAggregateUsageStats(this.mSinceTime, this.mCurrentTime);
    this.mAction = paramIntent.getAction();
    this.mRankerServiceName = new ComponentName(this.mContext, getClass());
    setCallBack(paramAfterCompute);
  }
  
  public void handleResultMessage(Message paramMessage) {
    if (paramMessage.what != 0)
      return; 
    if (paramMessage.obj == null) {
      Log.e("RRSResolverComparator", "Receiving null prediction results.");
      return;
    } 
    List<ResolverTarget> list = (List)paramMessage.obj;
    if (list != null && this.mTargets != null && 
      list.size() == this.mTargets.size()) {
      int i = this.mTargets.size();
      boolean bool = false;
      for (byte b = 0; b < i; b++) {
        float f = ((ResolverTarget)list.get(b)).getSelectProbability();
        if (f != ((ResolverTarget)this.mTargets.get(b)).getSelectProbability()) {
          ((ResolverTarget)this.mTargets.get(b)).setSelectProbability(f);
          bool = true;
        } 
      } 
      if (bool)
        this.mRankerServiceName = this.mResolvedRankerName; 
    } else {
      Log.e("RRSResolverComparator", "Sizes of sent and received ResolverTargets diff.");
    } 
  }
  
  public void doCompute(List<ResolverActivity.ResolvedComponentInfo> paramList) {
    long l = this.mCurrentTime;
    float f1, f2, f3, f4;
    for (Iterator<ResolverActivity.ResolvedComponentInfo> iterator = paramList.iterator(); iterator.hasNext(); ) {
      float f5, f6, f7, f8;
      ResolverActivity.ResolvedComponentInfo resolvedComponentInfo = iterator.next();
      ResolverTarget resolverTarget = new ResolverTarget();
      this.mTargetsDict.put(resolvedComponentInfo.name, resolverTarget);
      UsageStats usageStats = this.mStats.get(resolvedComponentInfo.name.getPackageName());
      if (usageStats != null) {
        float f;
        if (!resolvedComponentInfo.name.getPackageName().equals(this.mReferrerPackage)) {
          if (!isPersistentProcess(resolvedComponentInfo)) {
            float f9 = (float)Math.max(usageStats.getLastTimeUsed() - l - 43200000L, 0L);
            resolverTarget.setRecencyScore(f9);
            f = f1;
            if (f9 > f1)
              f = f9; 
          } else {
            f = f1;
          } 
        } else {
          f = f1;
        } 
        f5 = (float)usageStats.getTotalTimeInForeground();
        resolverTarget.setTimeSpentScore(f5);
        f1 = f2;
        if (f5 > f2)
          f1 = f5; 
        f5 = usageStats.mLaunchCount;
        resolverTarget.setLaunchScore(f5);
        f2 = f3;
        if (f5 > f3)
          f2 = f5; 
        f3 = 0.0F;
        if (usageStats.mChooserCounts != null && this.mAction != null) {
          ArrayMap arrayMap = usageStats.mChooserCounts;
          String str = this.mAction;
          if (arrayMap.get(str) != null) {
            arrayMap = (ArrayMap)usageStats.mChooserCounts.get(this.mAction);
            str = this.mContentType;
            f3 = ((Integer)arrayMap.getOrDefault(str, Integer.valueOf(0))).intValue();
            if (this.mAnnotations != null) {
              int i = this.mAnnotations.length;
              for (byte b = 0; b < i; b++) {
                ArrayMap arrayMap1 = (ArrayMap)usageStats.mChooserCounts.get(this.mAction);
                String str1 = this.mAnnotations[b];
                f3 += ((Integer)arrayMap1.getOrDefault(str1, Integer.valueOf(0))).intValue();
              } 
            } 
          } 
        } 
        resolverTarget.setChooserScore(f3);
        f6 = f;
        f7 = f1;
        f5 = f2;
        f8 = f4;
        if (f3 > f4) {
          f6 = f;
          f7 = f1;
          f5 = f2;
          f8 = f3;
        } 
      } else {
        f8 = f4;
        f5 = f3;
        f7 = f2;
        f6 = f1;
      } 
      f1 = f6;
      f2 = f7;
      f3 = f5;
      f4 = f8;
    } 
    ArrayList<ResolverTarget> arrayList = new ArrayList(this.mTargetsDict.values());
    for (ResolverTarget resolverTarget : arrayList) {
      float f7 = resolverTarget.getRecencyScore() / f1;
      float f5 = resolverTarget.getLaunchScore() / f3;
      float f8 = resolverTarget.getTimeSpentScore() / f2;
      float f6 = resolverTarget.getChooserScore() / f4;
      setFeatures(resolverTarget, f7 * f7 * 2.0F, f5, f8, f6);
      addDefaultSelectProbability(resolverTarget);
    } 
    predictSelectProbabilities(this.mTargets);
  }
  
  public int compare(ResolveInfo paramResolveInfo1, ResolveInfo paramResolveInfo2) {
    if (this.mStats != null) {
      ResolverTarget resolverTarget1 = this.mTargetsDict.get(new ComponentName(paramResolveInfo1.activityInfo.packageName, paramResolveInfo1.activityInfo.name));
      ResolverTarget resolverTarget2 = this.mTargetsDict.get(new ComponentName(paramResolveInfo2.activityInfo.packageName, paramResolveInfo2.activityInfo.name));
      if (resolverTarget1 != null && resolverTarget2 != null) {
        float f1 = resolverTarget2.getSelectProbability(), f2 = resolverTarget1.getSelectProbability();
        int i = Float.compare(f1, f2);
        if (i != 0) {
          if (i > 0) {
            i = 1;
          } else {
            i = -1;
          } 
          return i;
        } 
      } 
    } 
    CharSequence charSequence3 = paramResolveInfo1.loadLabel(this.mPm);
    CharSequence charSequence2 = charSequence3;
    if (charSequence3 == null)
      charSequence2 = paramResolveInfo1.activityInfo.name; 
    charSequence3 = paramResolveInfo2.loadLabel(this.mPm);
    CharSequence charSequence1 = charSequence3;
    if (charSequence3 == null)
      charSequence1 = paramResolveInfo2.activityInfo.name; 
    return this.mCollator.compare(charSequence2.toString().trim(), charSequence1.toString().trim());
  }
  
  public float getScore(ComponentName paramComponentName) {
    ResolverTarget resolverTarget = this.mTargetsDict.get(paramComponentName);
    if (resolverTarget != null)
      return resolverTarget.getSelectProbability(); 
    return 0.0F;
  }
  
  List<ComponentName> getTopComponentNames(int paramInt) {
    Stream stream = this.mTargetsDict.entrySet().stream();
    _$$Lambda$ResolverRankerServiceResolverComparator$eaaHaPUIpbqa_7QYRw6uiy_Yglw _$$Lambda$ResolverRankerServiceResolverComparator$eaaHaPUIpbqa_7QYRw6uiy_Yglw = new _$$Lambda$ResolverRankerServiceResolverComparator$eaaHaPUIpbqa_7QYRw6uiy_Yglw(this);
    stream = stream.sorted(_$$Lambda$ResolverRankerServiceResolverComparator$eaaHaPUIpbqa_7QYRw6uiy_Yglw);
    long l = paramInt;
    stream = stream.limit(l);
    -$.Lambda.vbUiOaY5W_R3oio_uxy-QbdJwl0 vbUiOaY5W_R3oio_uxy-QbdJwl0 = _$$Lambda$vbUiOaY5W_R3oio_uxy_QbdJwl0.INSTANCE;
    stream = stream.map((Function)vbUiOaY5W_R3oio_uxy-QbdJwl0);
    return (List)stream.collect(Collectors.toList());
  }
  
  public void updateModel(ComponentName paramComponentName) {
    synchronized (this.mLock) {
      IResolverRankerService iResolverRankerService = this.mRanker;
      if (iResolverRankerService != null)
        try {
          ArrayList arrayList = new ArrayList();
          this((Collection)this.mTargetsDict.keySet());
          int i = arrayList.indexOf(paramComponentName);
          if (i >= 0 && this.mTargets != null) {
            float f = getScore(paramComponentName);
            int j = 0;
            for (ResolverTarget resolverTarget : this.mTargets) {
              int k = j;
              if (resolverTarget.getSelectProbability() > f)
                k = j + 1; 
              j = k;
            } 
            logMetrics(j);
            this.mRanker.train(this.mTargets, i);
          } 
        } catch (RemoteException remoteException) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Error in Train: ");
          stringBuilder.append(remoteException);
          Log.e("RRSResolverComparator", stringBuilder.toString());
        }  
      return;
    } 
  }
  
  public void destroy() {
    this.mHandler.removeMessages(0);
    this.mHandler.removeMessages(1);
    ResolverRankerServiceConnection resolverRankerServiceConnection = this.mConnection;
    if (resolverRankerServiceConnection != null) {
      this.mContext.unbindService(resolverRankerServiceConnection);
      this.mConnection.destroy();
    } 
    afterCompute();
  }
  
  private void logMetrics(int paramInt) {
    if (this.mRankerServiceName != null) {
      boolean bool;
      MetricsLogger metricsLogger = new MetricsLogger();
      LogMaker logMaker = new LogMaker(1085);
      logMaker.setComponentName(this.mRankerServiceName);
      if (this.mAnnotations == null) {
        bool = false;
      } else {
        bool = true;
      } 
      logMaker.addTaggedData(1086, Integer.valueOf(bool));
      logMaker.addTaggedData(1087, Integer.valueOf(paramInt));
      metricsLogger.write(logMaker);
    } 
  }
  
  private void initRanker(Context paramContext) {
    synchronized (this.mLock) {
      if (this.mConnection != null && this.mRanker != null)
        return; 
      null = resolveRankerService();
      if (null == null)
        return; 
      CountDownLatch countDownLatch = new CountDownLatch(1);
      ResolverRankerServiceConnection resolverRankerServiceConnection = new ResolverRankerServiceConnection(countDownLatch);
      paramContext.bindServiceAsUser((Intent)null, resolverRankerServiceConnection, 1, UserHandle.SYSTEM);
      return;
    } 
  }
  
  private Intent resolveRankerService() {
    Intent intent = new Intent("android.service.resolver.ResolverRankerService");
    List list = this.mPm.queryIntentServices(intent, 0);
    for (ResolveInfo resolveInfo : list) {
      if (resolveInfo == null || resolveInfo.serviceInfo == null || resolveInfo.serviceInfo.applicationInfo == null)
        continue; 
      ComponentName componentName = new ComponentName(resolveInfo.serviceInfo.applicationInfo.packageName, resolveInfo.serviceInfo.name);
      try {
        StringBuilder stringBuilder;
        String str = (this.mPm.getServiceInfo(componentName, 0)).permission;
        boolean bool = "android.permission.BIND_RESOLVER_RANKER_SERVICE".equals(str);
        if (!bool) {
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("ResolverRankerService ");
          stringBuilder.append(componentName);
          stringBuilder.append(" does not require permission ");
          stringBuilder.append("android.permission.BIND_RESOLVER_RANKER_SERVICE");
          stringBuilder.append(" - this service will not be queried for ResolverRankerServiceResolverComparator. add android:permission=\"");
          stringBuilder.append("android.permission.BIND_RESOLVER_RANKER_SERVICE");
          stringBuilder.append("\" to the <service> tag for ");
          stringBuilder.append(componentName);
          stringBuilder.append(" in the manifest.");
          Log.w("RRSResolverComparator", stringBuilder.toString());
          continue;
        } 
        if (this.mPm.checkPermission("android.permission.PROVIDE_RESOLVER_RANKER_SERVICE", ((ResolveInfo)stringBuilder).serviceInfo.packageName) != 0) {
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("ResolverRankerService ");
          stringBuilder.append(componentName);
          stringBuilder.append(" does not hold permission ");
          stringBuilder.append("android.permission.PROVIDE_RESOLVER_RANKER_SERVICE");
          stringBuilder.append(" - this service will not be queried for ResolverRankerServiceResolverComparator.");
          Log.w("RRSResolverComparator", stringBuilder.toString());
          continue;
        } 
        this.mResolvedRankerName = componentName;
        intent.setComponent(componentName);
        return intent;
      } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Could not look up service ");
        stringBuilder.append(componentName);
        stringBuilder.append("; component name not found");
        Log.e("RRSResolverComparator", stringBuilder.toString());
      } 
    } 
    return null;
  }
  
  private class ResolverRankerServiceConnection implements ServiceConnection {
    private final CountDownLatch mConnectSignal;
    
    public final IResolverRankerResult resolverRankerResult;
    
    final ResolverRankerServiceResolverComparator this$0;
    
    public ResolverRankerServiceConnection(CountDownLatch param1CountDownLatch) {
      this.resolverRankerResult = (IResolverRankerResult)new Object(this);
      this.mConnectSignal = param1CountDownLatch;
    }
    
    public void onServiceConnected(ComponentName param1ComponentName, IBinder param1IBinder) {
      synchronized (ResolverRankerServiceResolverComparator.this.mLock) {
        ResolverRankerServiceResolverComparator.access$102(ResolverRankerServiceResolverComparator.this, IResolverRankerService.Stub.asInterface(param1IBinder));
        this.mConnectSignal.countDown();
        return;
      } 
    }
    
    public void onServiceDisconnected(ComponentName param1ComponentName) {
      synchronized (ResolverRankerServiceResolverComparator.this.mLock) {
        destroy();
        return;
      } 
    }
    
    public void destroy() {
      synchronized (ResolverRankerServiceResolverComparator.this.mLock) {
        ResolverRankerServiceResolverComparator.access$102(ResolverRankerServiceResolverComparator.this, (IResolverRankerService)null);
        return;
      } 
    }
  }
  
  void beforeCompute() {
    super.beforeCompute();
    this.mTargetsDict.clear();
    this.mTargets = null;
    this.mRankerServiceName = new ComponentName(this.mContext, getClass());
    this.mResolvedRankerName = null;
    initRanker(this.mContext);
  }
  
  private void predictSelectProbabilities(List<ResolverTarget> paramList) {
    if (this.mConnection != null)
      try {
        this.mConnectSignal.await(200L, TimeUnit.MILLISECONDS);
        synchronized (this.mLock) {
          if (this.mRanker != null) {
            this.mRanker.predict(paramList, this.mConnection.resolverRankerResult);
            return;
          } 
        } 
      } catch (InterruptedException interruptedException) {
        Log.e("RRSResolverComparator", "Error in Wait for Service Connection.");
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Error in Predict: ");
        stringBuilder.append(remoteException);
        Log.e("RRSResolverComparator", stringBuilder.toString());
      }  
    afterCompute();
  }
  
  private void addDefaultSelectProbability(ResolverTarget paramResolverTarget) {
    float f1 = paramResolverTarget.getLaunchScore(), f2 = paramResolverTarget.getTimeSpentScore();
    float f3 = paramResolverTarget.getRecencyScore(), f4 = paramResolverTarget.getChooserScore();
    paramResolverTarget.setSelectProbability((float)(1.0D / (Math.exp((1.6568F - f1 * 2.5543F + f2 * 2.8412F + f3 * 0.269F + f4 * 4.2222F)) + 1.0D)));
  }
  
  private void setFeatures(ResolverTarget paramResolverTarget, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    paramResolverTarget.setRecencyScore(paramFloat1);
    paramResolverTarget.setLaunchScore(paramFloat2);
    paramResolverTarget.setTimeSpentScore(paramFloat3);
    paramResolverTarget.setChooserScore(paramFloat4);
  }
  
  static boolean isPersistentProcess(ResolverActivity.ResolvedComponentInfo paramResolvedComponentInfo) {
    boolean bool = false;
    if (paramResolvedComponentInfo != null && paramResolvedComponentInfo.getCount() > 0) {
      if (((paramResolvedComponentInfo.getResolveInfoAt(0)).activityInfo.applicationInfo.flags & 0x8) != 0)
        bool = true; 
      return bool;
    } 
    return false;
  }
}
