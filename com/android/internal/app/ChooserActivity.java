package com.android.internal.app;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.ActivityManager;
import android.app.prediction.AppPredictionContext;
import android.app.prediction.AppPredictionManager;
import android.app.prediction.AppPredictor;
import android.app.prediction.AppTarget;
import android.app.prediction.AppTargetEvent;
import android.app.prediction.AppTargetId;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.metrics.LogMaker;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.os.UserHandle;
import android.os.UserManager;
import android.os.storage.StorageManager;
import android.provider.DeviceConfig;
import android.provider.Settings;
import android.service.chooser.ChooserTarget;
import android.service.chooser.IChooserTargetResult;
import android.service.chooser.IChooserTargetService;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Pair;
import android.util.Size;
import android.util.Slog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowInsets;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;
import com.android.internal.app.chooser.ChooserTargetInfo;
import com.android.internal.app.chooser.DisplayResolveInfo;
import com.android.internal.app.chooser.MultiDisplayResolveInfo;
import com.android.internal.app.chooser.NotSelectableTargetInfo;
import com.android.internal.app.chooser.SelectableTargetInfo;
import com.android.internal.app.chooser.TargetInfo;
import com.android.internal.content.PackageMonitor;
import com.android.internal.logging.MetricsLogger;
import com.android.internal.widget.GridLayoutManager;
import com.android.internal.widget.RecyclerView;
import com.android.internal.widget.ResolverDrawerLayout;
import com.android.internal.widget.ViewPager;
import com.google.android.collect.Lists;
import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.net.URISyntaxException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ChooserActivity extends ResolverActivity implements ChooserListAdapter.ChooserListCommunicator, SelectableTargetInfo.SelectableTargetInfoCommunicator {
  private int mMaxHashSaltDays = DeviceConfig.getInt("systemui", "hash_salt_max_days", 7);
  
  private boolean mAppendDirectShareEnabled = DeviceConfig.getBoolean("systemui", "append_direct_share_enabled", true);
  
  private boolean mChooserTargetRankingEnabled = DeviceConfig.getBoolean("systemui", "chooser_target_ranking_enabled", true);
  
  private int mCurrAvailableWidth = 0;
  
  private int mLastNumberOfChildren = -1;
  
  private final List<ChooserTargetServiceConnection> mServiceConnections = new ArrayList<>();
  
  private final Set<Pair<ComponentName, UserHandle>> mServicesRequested = new HashSet<>();
  
  private int mScrollStatus = 0;
  
  class ContentPreviewCoordinator {
    private static final int IMAGE_FADE_IN_MILLIS = 150;
    
    private static final int IMAGE_LOAD_INTO_VIEW = 2;
    
    private static final int IMAGE_LOAD_TIMEOUT = 1;
    
    private boolean mAtLeastOneLoaded;
    
    private final Handler mHandler;
    
    private boolean mHideParentOnFail;
    
    private final int mImageLoadTimeoutMillis;
    
    private final View mParentView;
    
    final ChooserActivity this$0;
    
    class LoadUriTask {
      public final Bitmap mBmp;
      
      public final int mExtraCount;
      
      public final int mImageResourceId;
      
      public final Uri mUri;
      
      final ChooserActivity.ContentPreviewCoordinator this$1;
      
      LoadUriTask(int param2Int1, Uri param2Uri, int param2Int2, Bitmap param2Bitmap) {
        this.mImageResourceId = param2Int1;
        this.mUri = param2Uri;
        this.mExtraCount = param2Int2;
        this.mBmp = param2Bitmap;
      }
    }
    
    ContentPreviewCoordinator(View param1View, boolean param1Boolean) {
      ChooserActivity.this = ChooserActivity.this;
      this.mImageLoadTimeoutMillis = ChooserActivity.this.getResources().getInteger(17694720);
      this.mAtLeastOneLoaded = false;
      this.mHandler = (Handler)new Object(this);
      this.mParentView = param1View;
      this.mHideParentOnFail = param1Boolean;
    }
    
    private void loadUriIntoView(int param1Int1, Uri param1Uri, int param1Int2) {
      this.mHandler.sendEmptyMessageDelayed(1, this.mImageLoadTimeoutMillis);
      AsyncTask.THREAD_POOL_EXECUTOR.execute(new _$$Lambda$ChooserActivity$ContentPreviewCoordinator$4EA4_6wC7DBv77gLolqI2_lsDQI(this, param1Uri, param1Int1, param1Int2));
    }
    
    private void cancelLoads() {
      this.mHandler.removeMessages(2);
      this.mHandler.removeMessages(1);
    }
    
    private void maybeHideContentPreview() {
      if (!this.mAtLeastOneLoaded && this.mHideParentOnFail) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Hiding image preview area. Timed out waiting for preview to load within ");
        stringBuilder.append(this.mImageLoadTimeoutMillis);
        stringBuilder.append("ms.");
        Log.i("ChooserActivity", stringBuilder.toString());
        collapseParentView();
        if (!ChooserActivity.this.isOriginUi())
          ChooserActivity.this.hideStickyContentPreview(); 
        if (ChooserActivity.this.shouldShowTabs()) {
          ChooserActivity.this.hideStickyContentPreview();
        } else if (ChooserActivity.this.mChooserMultiProfilePagerAdapter.getCurrentRootAdapter() != null) {
          ChooserActivity.this.mChooserMultiProfilePagerAdapter.getCurrentRootAdapter().hideContentPreview();
        } 
        this.mHideParentOnFail = false;
      } 
    }
    
    private void collapseParentView() {
      View view = this.mParentView;
      int i = View.MeasureSpec.makeMeasureSpec(view.getWidth(), 1073741824);
      int j = View.MeasureSpec.makeMeasureSpec(0, 1073741824);
      view.measure(i, j);
      (view.getLayoutParams()).height = 0;
      view.layout(view.getLeft(), view.getTop(), view.getRight(), view.getTop());
      view.invalidate();
    }
  }
  
  private final ChooserHandler mChooserHandler = new ChooserHandler();
  
  public static final String APP_PREDICTION_INTENT_FILTER_KEY = "intent_filter";
  
  private static final int APP_PREDICTION_SHARE_TARGET_QUERY_PACKAGE_LIMIT = 20;
  
  private static final String APP_PREDICTION_SHARE_UI_SURFACE = "share";
  
  private static final String CHIP_ICON_METADATA_KEY = "android.service.chooser.chip_icon";
  
  private static final String CHIP_LABEL_METADATA_KEY = "android.service.chooser.chip_label";
  
  public static final String CHOOSER_TARGET = "chooser_target";
  
  protected static final int CONTENT_PREVIEW_FILE = 2;
  
  protected static final int CONTENT_PREVIEW_IMAGE = 1;
  
  protected static final int CONTENT_PREVIEW_TEXT = 3;
  
  private static final boolean DEBUG = true;
  
  private static final int DEFAULT_SALT_EXPIRATION_DAYS = 7;
  
  private static final float DIRECT_SHARE_EXPANSION_RATE = 0.78F;
  
  public static final String EXTRA_PRIVATE_RETAIN_IN_ON_STOP = "com.android.internal.app.ChooserActivity.EXTRA_PRIVATE_RETAIN_IN_ON_STOP";
  
  public static final String LAUNCH_LOCATION_DIRECT_SHARE = "direct_share";
  
  public static final int LIST_VIEW_UPDATE_INTERVAL_IN_MILLIS = 250;
  
  private static final int MAX_EXTRA_CHOOSER_TARGETS = 2;
  
  private static final int MAX_EXTRA_INITIAL_INTENTS = 2;
  
  private static final int MAX_LOG_RANK_POSITION = 12;
  
  private static final int MAX_RANKED_TARGETS = 4;
  
  private static final int NO_DIRECT_SHARE_ANIM_IN_MILLIS = 200;
  
  private static final String PINNED_SHARED_PREFS_NAME = "chooser_pin_settings";
  
  private static final String PREF_NUM_SHEET_EXPANSIONS = "pref_num_sheet_expansions";
  
  private static final int QUERY_TARGET_SERVICE_LIMIT = 5;
  
  private static final int SCROLL_STATUS_IDLE = 0;
  
  private static final int SCROLL_STATUS_SCROLLING_HORIZONTAL = 2;
  
  private static final int SCROLL_STATUS_SCROLLING_VERTICAL = 1;
  
  public static final int SELECTION_TYPE_APP = 2;
  
  public static final int SELECTION_TYPE_COPY = 4;
  
  public static final int SELECTION_TYPE_SERVICE = 1;
  
  public static final int SELECTION_TYPE_STANDARD = 3;
  
  private static final int SHARE_TARGET_QUERY_PACKAGE_LIMIT = 20;
  
  private static final String SHORTCUT_TARGET = "shortcut_target";
  
  private static final String TAG = "ChooserActivity";
  
  private static final String TARGET_DETAILS_FRAGMENT_TAG = "targetDetailsFragment";
  
  public static final int TARGET_TYPE_CHOOSER_TARGET = 1;
  
  public static final int TARGET_TYPE_DEFAULT = 0;
  
  public static final int TARGET_TYPE_SHORTCUTS_FROM_PREDICTION_SERVICE = 3;
  
  public static final int TARGET_TYPE_SHORTCUTS_FROM_SHORTCUT_MANAGER = 2;
  
  private static final boolean USE_CHOOSER_TARGET_SERVICE_FOR_DIRECT_TARGETS = true;
  
  private static final boolean USE_PREDICTION_MANAGER_FOR_SHARE_ACTIVITIES = true;
  
  private ChooserTarget[] mCallerChooserTargets;
  
  protected ChooserActivityLogger mChooserActivityLogger;
  
  protected ChooserMultiProfilePagerAdapter mChooserMultiProfilePagerAdapter;
  
  private int mChooserRowServiceSpacing;
  
  private long mChooserShownTime;
  
  private Map<ComponentName, ComponentName> mChooserTargetComponentNameCache;
  
  private IntentSender mChosenComponentSender;
  
  private Map<ChooserTarget, AppTarget> mDirectShareAppTargetCache;
  
  private Map<ChooserTarget, ShortcutInfo> mDirectShareShortcutInfoCache;
  
  private ComponentName[] mFilteredComponentNames;
  
  private boolean mIsAppPredictorComponentAvailable;
  
  protected boolean mIsSuccessfullySelected;
  
  protected MetricsLogger mMetricsLogger;
  
  private AppPredictor mPersonalAppPredictor;
  
  private SharedPreferences mPinnedSharedPrefs;
  
  private ContentPreviewCoordinator mPreviewCoord;
  
  private long mQueriedSharingShortcutsTimeMs;
  
  private long mQueriedTargetServicesTimeMs;
  
  private Intent mReferrerFillInIntent;
  
  private IntentSender mRefinementIntentSender;
  
  private RefinementResultReceiver mRefinementResultReceiver;
  
  private Bundle mReplacementExtras;
  
  private boolean mShouldDisplayLandscape;
  
  private AppPredictor mWorkAppPredictor;
  
  class ChooserHandler extends Handler {
    private static final int CHOOSER_TARGET_RANKING_SCORE = 7;
    
    private static final int CHOOSER_TARGET_SERVICE_RESULT = 1;
    
    private static final int CHOOSER_TARGET_SERVICE_WATCHDOG_MAX_TIMEOUT = 3;
    
    private static final int CHOOSER_TARGET_SERVICE_WATCHDOG_MIN_TIMEOUT = 2;
    
    private static final int DEFAULT_DIRECT_SHARE_TIMEOUT_MILLIS = 1500;
    
    private static final int LIST_VIEW_UPDATE_MESSAGE = 6;
    
    private static final int SHORTCUT_MANAGER_SHARE_TARGET_RESULT = 4;
    
    private static final int SHORTCUT_MANAGER_SHARE_TARGET_RESULT_COMPLETED = 5;
    
    private static final int WATCHDOG_TIMEOUT_MAX_MILLIS = 10000;
    
    private static final int WATCHDOG_TIMEOUT_MIN_MILLIS = 3000;
    
    private int mDirectShareTimeout = DeviceConfig.getInt("systemui", "share_sheet_direct_share_timeout", 1500);
    
    private boolean mMinTimeoutPassed = false;
    
    final ChooserActivity this$0;
    
    private void removeAllMessages() {
      removeMessages(6);
      removeMessages(2);
      removeMessages(3);
      removeMessages(1);
      removeMessages(4);
      removeMessages(5);
      removeMessages(7);
    }
    
    private void restartServiceRequestTimer() {
      long l;
      this.mMinTimeoutPassed = false;
      removeMessages(2);
      removeMessages(3);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("queryTargets setting watchdog timer for ");
      stringBuilder.append(this.mDirectShareTimeout);
      stringBuilder.append("-");
      stringBuilder.append(10000);
      stringBuilder.append("ms");
      Log.d("ChooserActivity", stringBuilder.toString());
      sendEmptyMessageDelayed(2, 3000L);
      if (ChooserActivity.this.mAppendDirectShareEnabled) {
        l = this.mDirectShareTimeout;
      } else {
        l = 10000L;
      } 
      sendEmptyMessageDelayed(3, l);
    }
    
    private void maybeStopServiceRequestTimer() {
      if (this.mMinTimeoutPassed && ChooserActivity.this.mServiceConnections.isEmpty()) {
        ChooserActivity.this.logDirectShareTargetReceived(1719);
        ChooserActivity.this.sendVoiceChoicesIfNeeded();
        ChooserListAdapter chooserListAdapter = ChooserActivity.this.mChooserMultiProfilePagerAdapter.getActiveListAdapter();
        chooserListAdapter.completeServiceTargetLoading();
      } 
    }
    
    public void handleMessage(Message param1Message) {
      ChooserActivity.ChooserTargetRankingInfo chooserTargetRankingInfo;
      UserHandle userHandle1;
      ChooserListAdapter chooserListAdapter1;
      Map<ChooserTarget, ShortcutInfo> map;
      String str;
      ChooserMultiProfilePagerAdapter chooserMultiProfilePagerAdapter;
      ChooserListAdapter chooserListAdapter2;
      UserHandle userHandle2;
      ChooserActivity.ServiceResultInfo serviceResultInfo2;
      if (ChooserActivity.this.mChooserMultiProfilePagerAdapter.getActiveListAdapter() == null || ChooserActivity.this.isDestroyed())
        return; 
      switch (param1Message.what) {
        default:
          super.handleMessage(param1Message);
          return;
        case 7:
          Log.d("ChooserActivity", "CHOOSER_TARGET_RANKING_SCORE");
          chooserTargetRankingInfo = (ChooserActivity.ChooserTargetRankingInfo)param1Message.obj;
          chooserMultiProfilePagerAdapter = ChooserActivity.this.mChooserMultiProfilePagerAdapter;
          userHandle2 = chooserTargetRankingInfo.userHandle;
          chooserListAdapter2 = chooserMultiProfilePagerAdapter.getListAdapterForUserHandle(userHandle2);
          if (chooserListAdapter2 != null)
            chooserListAdapter2.addChooserTargetRankingScore(chooserTargetRankingInfo.scores); 
          return;
        case 6:
          Log.d("ChooserActivity", "LIST_VIEW_UPDATE_MESSAGE; ");
          userHandle1 = (UserHandle)((Message)chooserTargetRankingInfo).obj;
          chooserListAdapter1 = ChooserActivity.this.mChooserMultiProfilePagerAdapter.getListAdapterForUserHandle(userHandle1);
          chooserListAdapter1.refreshListView();
          return;
        case 5:
          ChooserActivity.this.logDirectShareTargetReceived(1718);
          ChooserActivity.this.sendVoiceChoicesIfNeeded();
          ChooserActivity.this.getChooserActivityLogger().logSharesheetDirectLoadComplete();
          return;
        case 4:
          Log.d("ChooserActivity", "SHORTCUT_MANAGER_SHARE_TARGET_RESULT");
          serviceResultInfo2 = (ChooserActivity.ServiceResultInfo)((Message)chooserListAdapter1).obj;
          if (serviceResultInfo2.resultTargets != null) {
            ChooserMultiProfilePagerAdapter chooserMultiProfilePagerAdapter1 = ChooserActivity.this.mChooserMultiProfilePagerAdapter;
            userHandle2 = serviceResultInfo2.userHandle;
            ChooserListAdapter chooserListAdapter = chooserMultiProfilePagerAdapter1.getListAdapterForUserHandle(userHandle2);
            if (chooserListAdapter != null) {
              DisplayResolveInfo displayResolveInfo = serviceResultInfo2.originalTarget;
              List<ChooserTarget> list = serviceResultInfo2.resultTargets;
              int i = ((Message)chooserListAdapter1).arg1;
              ChooserActivity chooserActivity = ChooserActivity.this;
              map = chooserActivity.mDirectShareShortcutInfoCache;
              List<ChooserActivity.ChooserTargetServiceConnection> list1 = ChooserActivity.this.mServiceConnections;
              chooserListAdapter.addServiceResults(displayResolveInfo, list, i, map, list1);
            } 
          } 
          return;
        case 3:
          this.mMinTimeoutPassed = true;
          if (!ChooserActivity.this.mServiceConnections.isEmpty())
            ChooserActivity.this.getChooserActivityLogger().logSharesheetDirectLoadTimeout(); 
          ChooserActivity.this.unbindRemainingServices();
          maybeStopServiceRequestTimer();
          return;
        case 2:
          this.mMinTimeoutPassed = true;
          maybeStopServiceRequestTimer();
          return;
        case 1:
          break;
      } 
      Log.d("ChooserActivity", "CHOOSER_TARGET_SERVICE_RESULT");
      ChooserActivity.ServiceResultInfo serviceResultInfo1 = (ChooserActivity.ServiceResultInfo)((Message)map).obj;
      if (!ChooserActivity.this.mServiceConnections.contains(serviceResultInfo1.connection)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("ChooserTargetServiceConnection ");
        stringBuilder.append(serviceResultInfo1.connection);
        DisplayResolveInfo displayResolveInfo = serviceResultInfo1.originalTarget;
        stringBuilder.append((displayResolveInfo.getResolveInfo()).activityInfo.packageName);
        stringBuilder.append(" returned after being removed from active connections. Have you considered returning results faster?");
        str = stringBuilder.toString();
        Log.w("ChooserActivity", str);
      } else {
        if (((ChooserActivity.ServiceResultInfo)str).resultTargets != null) {
          ChooserMultiProfilePagerAdapter chooserMultiProfilePagerAdapter1 = ChooserActivity.this.mChooserMultiProfilePagerAdapter;
          userHandle2 = ((ChooserActivity.ServiceResultInfo)str).userHandle;
          ChooserListAdapter chooserListAdapter = chooserMultiProfilePagerAdapter1.getListAdapterForUserHandle(userHandle2);
          if (chooserListAdapter != null) {
            DisplayResolveInfo displayResolveInfo = ((ChooserActivity.ServiceResultInfo)str).originalTarget;
            List<ChooserTarget> list = ((ChooserActivity.ServiceResultInfo)str).resultTargets;
            ChooserActivity chooserActivity = ChooserActivity.this;
            List<ChooserActivity.ChooserTargetServiceConnection> list1 = chooserActivity.mServiceConnections;
            chooserListAdapter.addServiceResults(displayResolveInfo, list, 1, (Map<ChooserTarget, ShortcutInfo>)null, list1);
            if (!((ChooserActivity.ServiceResultInfo)str).resultTargets.isEmpty() && ((ChooserActivity.ServiceResultInfo)str).originalTarget != null) {
              Map<ComponentName, ComponentName> map1 = ChooserActivity.this.mChooserTargetComponentNameCache;
              List<ChooserTarget> list2 = ((ChooserActivity.ServiceResultInfo)str).resultTargets;
              ComponentName componentName1 = ((ChooserTarget)list2.get(0)).getComponentName();
              DisplayResolveInfo displayResolveInfo1 = ((ChooserActivity.ServiceResultInfo)str).originalTarget;
              ComponentName componentName2 = displayResolveInfo1.getResolvedComponentName();
              map1.put(componentName1, componentName2);
            } 
          } 
        } 
        ChooserActivity.this.unbindService(((ChooserActivity.ServiceResultInfo)str).connection);
        ((ChooserActivity.ServiceResultInfo)str).connection.destroy();
        ChooserActivity.this.mServiceConnections.remove(((ChooserActivity.ServiceResultInfo)str).connection);
        maybeStopServiceRequestTimer();
      } 
    }
    
    private ChooserHandler() {}
  }
  
  protected void onCreate(Bundle paramBundle) {
    long l = System.currentTimeMillis();
    getChooserActivityLogger().logSharesheetTriggered();
    this.mIsAppPredictorComponentAvailable = isAppPredictionServiceAvailable();
    this.mIsSuccessfullySelected = false;
    Intent intent1 = getIntent();
    Parcelable parcelable1 = intent1.getParcelableExtra("android.intent.extra.INTENT");
    Parcelable parcelable2 = intent1.getParcelableExtra("android.app.extra.OPTIONS");
    if (parcelable2 != null && parcelable2 instanceof Bundle)
      this.mBundle = (Bundle)parcelable2; 
    if (parcelable1 instanceof Uri)
      try {
        Intent intent = Intent.parseUri(parcelable1.toString(), 1);
        intent2 = intent;
      } catch (URISyntaxException uRISyntaxException) {} 
    if (!(intent2 instanceof Intent)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Target is not an intent: ");
      stringBuilder.append(intent2);
      Log.w("ChooserActivity", stringBuilder.toString());
      finish();
      super.onCreate((Bundle)null);
      return;
    } 
    Intent intent2 = intent2;
    if (intent2 != null)
      modifyTargetIntent(intent2); 
    Parcelable[] arrayOfParcelable = intent1.getParcelableArrayExtra("android.intent.extra.ALTERNATE_INTENTS");
    if (arrayOfParcelable != null) {
      boolean bool;
      if (intent2 == null) {
        bool = true;
      } else {
        bool = false;
      } 
      int i = arrayOfParcelable.length, j = i;
      if (bool)
        j = i - 1; 
      Intent[] arrayOfIntent = new Intent[j];
      for (j = 0; j < arrayOfParcelable.length; j++) {
        if (!(arrayOfParcelable[j] instanceof Intent)) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("EXTRA_ALTERNATE_INTENTS array entry #");
          stringBuilder.append(j);
          stringBuilder.append(" is not an Intent: ");
          stringBuilder.append(arrayOfParcelable[j]);
          Log.w("ChooserActivity", stringBuilder.toString());
          finish();
          super.onCreate((Bundle)null);
          return;
        } 
        Intent intent = (Intent)arrayOfParcelable[j];
        if (j == 0 && intent2 == null) {
          intent2 = intent;
          modifyTargetIntent(intent2);
        } else {
          if (bool) {
            i = j - 1;
          } else {
            i = j;
          } 
          arrayOfIntent[i] = intent;
          modifyTargetIntent(intent);
        } 
      } 
      setAdditionalTargets(arrayOfIntent);
    } 
    this.mReplacementExtras = intent1.getBundleExtra("android.intent.extra.REPLACEMENT_EXTRAS");
    if (intent2 != null) {
      StringBuilder stringBuilder2;
      int k;
      if (!isSendAction(intent2)) {
        CharSequence charSequence = intent1.getCharSequenceExtra("android.intent.extra.TITLE");
      } else {
        Log.w("ChooserActivity", "Ignoring intent's EXTRA_TITLE, deprecated in P. You may wish to set a preview title by using EXTRA_TITLE property of the wrapped EXTRA_INTENT.");
        arrayOfParcelable = null;
      } 
      if (arrayOfParcelable == null) {
        k = 17039816;
      } else {
        k = 0;
      } 
      Parcelable[] arrayOfParcelable1 = intent1.getParcelableArrayExtra("android.intent.extra.INITIAL_INTENTS");
      if (arrayOfParcelable1 != null) {
        int m = Math.min(arrayOfParcelable1.length, 2);
        Intent[] arrayOfIntent = new Intent[m];
        for (i = 0; i < m; i++) {
          if (!(arrayOfParcelable1[i] instanceof Intent)) {
            stringBuilder2 = new StringBuilder();
            stringBuilder2.append("Initial intent #");
            stringBuilder2.append(i);
            stringBuilder2.append(" not an Intent: ");
            stringBuilder2.append(arrayOfParcelable1[i]);
            Log.w("ChooserActivity", stringBuilder2.toString());
            finish();
            super.onCreate((Bundle)null);
            return;
          } 
          Intent intent = (Intent)arrayOfParcelable1[i];
          modifyTargetIntent(intent);
          arrayOfIntent[i] = intent;
        } 
      } else {
        parcelable2 = null;
      } 
      this.mReferrerFillInIntent = (new Intent()).putExtra("android.intent.extra.REFERRER", (Parcelable)getReferrer());
      this.mChosenComponentSender = (IntentSender)intent1.getParcelableExtra("android.intent.extra.CHOSEN_COMPONENT_INTENT_SENDER");
      this.mRefinementIntentSender = (IntentSender)intent1.getParcelableExtra("android.intent.extra.CHOOSER_REFINEMENT_INTENT_SENDER");
      setSafeForwardingMode(true);
      this.mPinnedSharedPrefs = getPinnedSharedPrefs((Context)this);
      Parcelable[] arrayOfParcelable2 = intent1.getParcelableArrayExtra("android.intent.extra.EXCLUDE_COMPONENTS");
      ComponentName componentName = getNearbySharingComponent();
      if (componentName != null) {
        i = 1;
      } else {
        i = 0;
      } 
      if (arrayOfParcelable2 != null) {
        StringBuilder stringBuilder;
        int m = arrayOfParcelable2.length;
        if (i) {
          b = 1;
        } else {
          b = 0;
        } 
        ComponentName[] arrayOfComponentName = new ComponentName[m + b];
        byte b = 0;
        while (true) {
          ComponentName[] arrayOfComponentName1 = arrayOfComponentName;
          if (b < arrayOfParcelable2.length) {
            if (!(arrayOfParcelable2[b] instanceof ComponentName)) {
              stringBuilder = new StringBuilder();
              stringBuilder.append("Filtered component #");
              stringBuilder.append(b);
              stringBuilder.append(" not a ComponentName: ");
              stringBuilder.append(arrayOfParcelable2[b]);
              Log.w("ChooserActivity", stringBuilder.toString());
              stringBuilder = null;
              break;
            } 
            arrayOfComponentName[b] = (ComponentName)arrayOfParcelable2[b];
            b++;
            continue;
          } 
          break;
        } 
        if (i)
          stringBuilder[stringBuilder.length - 1] = (StringBuilder)componentName; 
        this.mFilteredComponentNames = (ComponentName[])stringBuilder;
      } else if (i) {
        ComponentName[] arrayOfComponentName = new ComponentName[1];
        arrayOfComponentName[0] = componentName;
      } 
      Parcelable[] arrayOfParcelable3 = intent1.getParcelableArrayExtra("android.intent.extra.CHOOSER_TARGETS");
      if (arrayOfParcelable3 != null) {
        StringBuilder stringBuilder;
        int m = Math.min(arrayOfParcelable3.length, 2);
        ChooserTarget[] arrayOfChooserTarget1 = new ChooserTarget[m];
        i = 0;
        while (true) {
          ChooserTarget[] arrayOfChooserTarget2 = arrayOfChooserTarget1;
          if (i < m) {
            if (!(arrayOfParcelable3[i] instanceof ChooserTarget)) {
              stringBuilder = new StringBuilder();
              stringBuilder.append("Chooser target #");
              stringBuilder.append(i);
              stringBuilder.append(" not a ChooserTarget: ");
              stringBuilder.append(arrayOfParcelable3[i]);
              Log.w("ChooserActivity", stringBuilder.toString());
              stringBuilder = null;
              break;
            } 
            arrayOfChooserTarget1[i] = (ChooserTarget)arrayOfParcelable3[i];
            i++;
            continue;
          } 
          break;
        } 
        this.mCallerChooserTargets = (ChooserTarget[])stringBuilder;
      } 
      int i = (getResources().getConfiguration()).orientation;
      this.mShouldDisplayLandscape = shouldDisplayLandscape(i);
      setRetainInOnStop(intent1.getBooleanExtra("com.android.internal.app.ChooserActivity.EXTRA_PRIVATE_RETAIN_IN_ON_STOP", false));
      onCreate((Bundle)stringBuilder2, intent2, (CharSequence)arrayOfParcelable, k, (Intent[])parcelable2, (List<ResolveInfo>)null, false);
      long l1 = System.currentTimeMillis();
      l1 -= l;
      MetricsLogger metricsLogger = getMetricsLogger();
      LogMaker logMaker = new LogMaker(214);
      if (isWorkProfile()) {
        k = 2;
      } else {
        k = 1;
      } 
      logMaker = logMaker.setSubtype(k);
      logMaker = logMaker.addTaggedData(1649, intent2.getType());
      logMaker = logMaker.addTaggedData(1653, Long.valueOf(l1));
      metricsLogger.write(logMaker);
      Resources resources = getResources();
      this.mChooserRowServiceSpacing = resources.getDimensionPixelSize(17105045);
      if (this.mResolverDrawerLayout != null) {
        this.mResolverDrawerLayout.addOnLayoutChangeListener(new _$$Lambda$ChooserActivity$mSpb8JdVEdN3REmKTSrORHIDnIo(this));
        if (isSendAction(intent2))
          this.mResolverDrawerLayout.setOnScrollChangeListener(new _$$Lambda$ChooserActivity$n2FimsQN3_RG5vs7T6aVc1Pt9v0(this)); 
        this.mResolverDrawerLayout.setOnCollapsedChangedListener((ResolverDrawerLayout.OnCollapsedChangedListener)new Object(this));
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("System Time Cost is ");
      stringBuilder1.append(l1);
      Log.d("ChooserActivity", stringBuilder1.toString());
      ChooserActivityLogger chooserActivityLogger = getChooserActivityLogger();
      String str1 = getReferrerPackageName();
      String str3 = intent2.getType();
      if (parcelable2 == null) {
        k = 0;
      } else {
        k = parcelable2.length;
      } 
      ChooserTarget[] arrayOfChooserTarget = this.mCallerChooserTargets;
      if (arrayOfChooserTarget == null) {
        i = 0;
      } else {
        i = arrayOfChooserTarget.length;
      } 
      boolean bool = isWorkProfile();
      int j = findPreferredContentPreview(getTargetIntent(), getContentResolver());
      String str2 = intent2.getAction();
      chooserActivityLogger.logShareStarted(259, str1, str3, k, i, bool, j, str2);
      this.mDirectShareShortcutInfoCache = new HashMap<>();
      this.mChooserTargetComponentNameCache = new HashMap<>();
      return;
    } 
    arrayOfParcelable = null;
  }
  
  protected int appliedThemeResId() {
    return 16974818;
  }
  
  private AppPredictor setupAppPredictorForUser(UserHandle paramUserHandle, AppPredictor.Callback paramCallback) {
    AppPredictor appPredictor = getAppPredictorForDirectShareIfEnabled(paramUserHandle);
    if (appPredictor == null)
      return null; 
    this.mDirectShareAppTargetCache = new HashMap<>();
    appPredictor.registerPredictionUpdates(getMainExecutor(), paramCallback);
    return appPredictor;
  }
  
  private AppPredictor.Callback createAppPredictorCallback(ChooserListAdapter paramChooserListAdapter) {
    return new _$$Lambda$ChooserActivity$FljvS3yUXhDmWRqv_tXKrsXC_DQ(this, paramChooserListAdapter);
  }
  
  static SharedPreferences getPinnedSharedPrefs(Context paramContext) {
    String str1 = StorageManager.UUID_PRIVATE_INTERNAL;
    int i = paramContext.getUserId();
    String str2 = paramContext.getPackageName();
    File file = new File(new File(Environment.getDataUserCePackageDirectory(str1, i, str2), "shared_prefs"), "chooser_pin_settings.xml");
    return paramContext.getSharedPreferences(file, 0);
  }
  
  protected AbstractMultiProfilePagerAdapter createMultiProfilePagerAdapter(Intent[] paramArrayOfIntent, List<ResolveInfo> paramList, boolean paramBoolean) {
    if (shouldShowTabs()) {
      this.mChooserMultiProfilePagerAdapter = createChooserMultiProfilePagerAdapterForTwoProfiles(paramArrayOfIntent, paramList, paramBoolean);
    } else {
      this.mChooserMultiProfilePagerAdapter = createChooserMultiProfilePagerAdapterForOneProfile(paramArrayOfIntent, paramList, paramBoolean);
    } 
    if (isOriginUi())
      return this.mChooserMultiProfilePagerAdapter; 
    return super.createMultiProfilePagerAdapter(paramArrayOfIntent, paramList, paramBoolean);
  }
  
  private ChooserMultiProfilePagerAdapter createChooserMultiProfilePagerAdapterForOneProfile(Intent[] paramArrayOfIntent, List<ResolveInfo> paramList, boolean paramBoolean) {
    ArrayList<Intent> arrayList = this.mIntents;
    UserHandle userHandle2 = UserHandle.of(UserHandle.myUserId());
    ChooserGridAdapter chooserGridAdapter = createChooserGridAdapter((Context)this, arrayList, paramArrayOfIntent, paramList, paramBoolean, userHandle2);
    UserHandle userHandle1 = getPersonalProfileUserHandle();
    return new ChooserMultiProfilePagerAdapter((Context)this, chooserGridAdapter, userHandle1, null, isSendAction(getTargetIntent()));
  }
  
  private ChooserMultiProfilePagerAdapter createChooserMultiProfilePagerAdapterForTwoProfiles(Intent[] paramArrayOfIntent, List<ResolveInfo> paramList, boolean paramBoolean) {
    int i = findSelectedProfile();
    ArrayList<Intent> arrayList = this.mIntents;
    if (i == 0) {
      chooserGridAdapter2 = (ChooserGridAdapter)paramArrayOfIntent;
    } else {
      chooserGridAdapter2 = null;
    } 
    UserHandle userHandle3 = getPersonalProfileUserHandle();
    ChooserGridAdapter chooserGridAdapter2 = createChooserGridAdapter((Context)this, arrayList, (Intent[])chooserGridAdapter2, paramList, paramBoolean, userHandle3);
    arrayList = this.mIntents;
    if (i != 1)
      paramArrayOfIntent = null; 
    userHandle3 = getWorkProfileUserHandle();
    ChooserGridAdapter chooserGridAdapter1 = createChooserGridAdapter((Context)this, arrayList, paramArrayOfIntent, paramList, paramBoolean, userHandle3);
    UserHandle userHandle2 = getPersonalProfileUserHandle();
    UserHandle userHandle1 = getWorkProfileUserHandle();
    return new ChooserMultiProfilePagerAdapter((Context)this, chooserGridAdapter2, chooserGridAdapter1, i, userHandle2, userHandle1, isSendAction(getTargetIntent()));
  }
  
  private int findSelectedProfile() {
    int i = getSelectedProfileExtra();
    int j = i;
    if (i == -1)
      j = getProfileForUser(getUser()); 
    return j;
  }
  
  protected boolean postRebuildList(boolean paramBoolean) {
    if (!isOriginUi())
      return super.postRebuildList(paramBoolean); 
    updateStickyContentPreview();
    if (!shouldShowStickyContentPreview()) {
      ChooserMultiProfilePagerAdapter chooserMultiProfilePagerAdapter = this.mChooserMultiProfilePagerAdapter;
      if (chooserMultiProfilePagerAdapter.getCurrentRootAdapter().getContentPreviewRowCount() != 0) {
        logActionShareWithPreview();
        return postRebuildListInternal(paramBoolean);
      } 
      return postRebuildListInternal(paramBoolean);
    } 
    logActionShareWithPreview();
    return postRebuildListInternal(paramBoolean);
  }
  
  public boolean isAppPredictionServiceAvailable() {
    if (getPackageManager().getAppPredictionServicePackageName() == null)
      return false; 
    String str = getString(17039869);
    if (str == null)
      return false; 
    ComponentName componentName = ComponentName.unflattenFromString(str);
    if (componentName == null)
      return false; 
    Intent intent = new Intent();
    intent.setComponent(componentName);
    if (getPackageManager().resolveService(intent, 131072) == null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("App prediction service is defined, but does not exist: ");
      stringBuilder.append(str);
      Log.e("ChooserActivity", stringBuilder.toString());
      return false;
    } 
    return true;
  }
  
  protected boolean isWorkProfile() {
    UserManager userManager = (UserManager)getSystemService(UserManager.class);
    return 
      userManager.getUserInfo(UserHandle.myUserId()).isManagedProfile();
  }
  
  protected PackageMonitor createPackageMonitor(ResolverListAdapter paramResolverListAdapter) {
    return (PackageMonitor)new Object(this, paramResolverListAdapter);
  }
  
  public void handlePackagesChanged() {
    handlePackagesChanged((ResolverListAdapter)null);
  }
  
  private void handlePackagesChanged(ResolverListAdapter paramResolverListAdapter) {
    this.mPinnedSharedPrefs = getPinnedSharedPrefs((Context)this);
    if (paramResolverListAdapter == null) {
      this.mChooserMultiProfilePagerAdapter.getActiveListAdapter().handlePackagesChanged();
      if (this.mChooserMultiProfilePagerAdapter.getCount() > 1)
        this.mChooserMultiProfilePagerAdapter.getInactiveListAdapter().handlePackagesChanged(); 
    } else {
      paramResolverListAdapter.handlePackagesChanged();
    } 
    updateProfileViewButton();
  }
  
  private void onCopyButtonClicked(View paramView) {
    Intent intent = getTargetIntent();
    if (intent == null) {
      finish();
    } else {
      ClipData clipData;
      Uri uri;
      String str = intent.getAction();
      if ("android.intent.action.SEND".equals(str)) {
        str = intent.getStringExtra("android.intent.extra.TEXT");
        uri = (Uri)intent.getParcelableExtra("android.intent.extra.STREAM");
        if (str != null) {
          clipData = ClipData.newPlainText(null, str);
        } else if (uri != null) {
          clipData = ClipData.newUri(getContentResolver(), null, uri);
        } else {
          Log.w("ChooserActivity", "No data available to copy to clipboard");
          return;
        } 
      } else if ("android.intent.action.SEND_MULTIPLE".equals(clipData)) {
        ArrayList<Uri> arrayList = uri.getParcelableArrayListExtra("android.intent.extra.STREAM");
        clipData = ClipData.newUri(getContentResolver(), null, arrayList.get(0));
        for (byte b = 1; b < arrayList.size(); b++)
          clipData.addItem(getContentResolver(), new ClipData.Item(arrayList.get(b))); 
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Action (");
        stringBuilder.append((String)clipData);
        stringBuilder.append(") not supported for copying to clipboard");
        Log.w("ChooserActivity", stringBuilder.toString());
        return;
      } 
      ClipboardManager clipboardManager = (ClipboardManager)getSystemService("clipboard");
      clipboardManager.setPrimaryClip(clipData);
      Toast.makeText(getApplicationContext(), 17040005, 0).show();
      LogMaker logMaker = new LogMaker(1749);
      logMaker = logMaker.setSubtype(1);
      getMetricsLogger().write(logMaker);
      getChooserActivityLogger().logShareTargetSelected(4, "", -1);
      finish();
    } 
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration) {
    super.onConfigurationChanged(paramConfiguration);
    if (!isOriginUi())
      return; 
    ViewPager viewPager = (ViewPager)findViewById(16909322);
    if (shouldShowTabs() && viewPager.isLayoutRtl())
      this.mMultiProfilePagerAdapter.setupViewPager(viewPager); 
    this.mShouldDisplayLandscape = shouldDisplayLandscape(paramConfiguration.orientation);
    adjustPreviewWidth(paramConfiguration.orientation, (View)null);
    updateStickyContentPreview();
  }
  
  private boolean shouldDisplayLandscape(int paramInt) {
    boolean bool;
    if (paramInt == 2 && !isInMultiWindowMode()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void adjustPreviewWidth(int paramInt, View paramView) {
    if (!isOriginUi())
      return; 
    paramInt = -1;
    if (this.mShouldDisplayLandscape)
      paramInt = getResources().getDimensionPixelSize(17105043); 
    if (paramView == null)
      paramView = getWindow().getDecorView(); 
    updateLayoutWidth(16908878, paramInt, paramView);
    updateLayoutWidth(16908881, paramInt, paramView);
    updateLayoutWidth(16908868, paramInt, paramView);
  }
  
  private void updateLayoutWidth(int paramInt1, int paramInt2, View paramView) {
    View view = (View)paramView.findViewById(paramInt1);
    if (view != null && view.getLayoutParams() != null) {
      ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
      layoutParams.width = paramInt2;
      view.setLayoutParams(layoutParams);
    } 
  }
  
  private ViewGroup createContentPreviewView(ViewGroup paramViewGroup) {
    Intent intent = getTargetIntent();
    int i = findPreferredContentPreview(intent, getContentResolver());
    return displayContentPreview(i, intent, getLayoutInflater(), paramViewGroup);
  }
  
  private ComponentName getNearbySharingComponent() {
    ContentResolver contentResolver = getContentResolver();
    String str2 = Settings.Secure.getString(contentResolver, "nearby_sharing_component");
    String str1 = str2;
    if (TextUtils.isEmpty(str2))
      str1 = getString(17039880); 
    if (TextUtils.isEmpty(str1))
      return null; 
    return ComponentName.unflattenFromString(str1);
  }
  
  private TargetInfo getNearbySharingTarget(Intent paramIntent) {
    StringBuilder stringBuilder;
    ComponentName componentName = getNearbySharingComponent();
    if (componentName == null)
      return null; 
    Intent intent = new Intent(paramIntent);
    intent.setComponent(componentName);
    ResolveInfo resolveInfo = getPackageManager().resolveActivity(intent, 128);
    if (resolveInfo == null || resolveInfo.activityInfo == null) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Device-specified nearby sharing component (");
      stringBuilder.append(componentName);
      stringBuilder.append(") not available");
      Log.e("ChooserActivity", stringBuilder.toString());
      return null;
    } 
    CharSequence charSequence = null;
    String str1 = null, str2 = null;
    String str3 = null;
    Bundle bundle = resolveInfo.activityInfo.metaData;
    String str4 = str3;
    if (bundle != null) {
      str4 = str2;
      str1 = (String)charSequence;
      try {
        Resources resources = getPackageManager().getResourcesForActivity(componentName);
        str4 = str2;
        str1 = (String)charSequence;
        int i = bundle.getInt("android.service.chooser.chip_label");
        str4 = str2;
        str1 = (String)charSequence;
        charSequence = resources.getString(i);
        str4 = (String)charSequence;
        str1 = (String)charSequence;
        i = bundle.getInt("android.service.chooser.chip_icon");
        str4 = (String)charSequence;
        str1 = (String)charSequence;
        Drawable drawable1 = resources.getDrawable(i), drawable2 = drawable1;
        str1 = (String)charSequence;
      } catch (android.content.res.Resources.NotFoundException notFoundException) {
        str4 = str3;
      } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
        str1 = str4;
        str4 = str3;
      } 
    } 
    charSequence = str1;
    if (TextUtils.isEmpty(str1))
      charSequence = resolveInfo.loadLabel(getPackageManager()); 
    if (str4 == null) {
      Drawable drawable = resolveInfo.loadIcon(getPackageManager());
    } else {
      str1 = str4;
    } 
    DisplayResolveInfo displayResolveInfo = new DisplayResolveInfo((Intent)stringBuilder, resolveInfo, charSequence, "", intent, null);
    displayResolveInfo.setDisplayIcon((Drawable)str1);
    return displayResolveInfo;
  }
  
  private Button createActionButton(Drawable paramDrawable, CharSequence paramCharSequence, View.OnClickListener paramOnClickListener) {
    Button button = (Button)LayoutInflater.from((Context)this).inflate(17367123, (ViewGroup)null);
    if (paramDrawable != null) {
      Resources resources = getResources();
      int i = resources.getDimensionPixelSize(17105030);
      paramDrawable.setBounds(0, 0, i, i);
      button.setCompoundDrawablesRelative(paramDrawable, (Drawable)null, (Drawable)null, (Drawable)null);
    } 
    button.setText(paramCharSequence);
    button.setOnClickListener(paramOnClickListener);
    return button;
  }
  
  private Button createCopyButton() {
    Drawable drawable = getDrawable(17302699);
    String str = getString(17039361);
    _$$Lambda$ChooserActivity$59FvMzyIg7yJzeX3NNdkiEmiSgI _$$Lambda$ChooserActivity$59FvMzyIg7yJzeX3NNdkiEmiSgI = new _$$Lambda$ChooserActivity$59FvMzyIg7yJzeX3NNdkiEmiSgI(this);
    Button button = createActionButton(drawable, str, _$$Lambda$ChooserActivity$59FvMzyIg7yJzeX3NNdkiEmiSgI);
    button.setId(16908839);
    return button;
  }
  
  private Button createNearbyButton(Intent paramIntent) {
    TargetInfo targetInfo = getNearbySharingTarget(paramIntent);
    if (targetInfo == null)
      return null; 
    Drawable drawable = targetInfo.getDisplayIcon((Context)this);
    CharSequence charSequence = targetInfo.getDisplayLabel();
    _$$Lambda$ChooserActivity$g8GWuiuRf5KzDVaf7rfp_PM_fX4 _$$Lambda$ChooserActivity$g8GWuiuRf5KzDVaf7rfp_PM_fX4 = new _$$Lambda$ChooserActivity$g8GWuiuRf5KzDVaf7rfp_PM_fX4(this, targetInfo);
    return createActionButton(drawable, charSequence, _$$Lambda$ChooserActivity$g8GWuiuRf5KzDVaf7rfp_PM_fX4);
  }
  
  private void addActionButton(ViewGroup paramViewGroup, Button paramButton) {
    if (paramButton == null)
      return; 
    ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(-2, -2);
    int i = getResources().getDimensionPixelSize(17105443) / 2;
    marginLayoutParams.setMarginsRelative(i, 0, i, 0);
    paramViewGroup.addView(paramButton, marginLayoutParams);
  }
  
  private ViewGroup displayContentPreview(int paramInt, Intent paramIntent, LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup) {
    ViewGroup viewGroup;
    StringBuilder stringBuilder = null;
    if (paramInt != 1) {
      if (paramInt != 2) {
        StringBuilder stringBuilder1;
        if (paramInt != 3) {
          stringBuilder1 = new StringBuilder();
          stringBuilder1.append("Unexpected content preview type: ");
          stringBuilder1.append(paramInt);
          Log.e("ChooserActivity", stringBuilder1.toString());
          stringBuilder1 = stringBuilder;
        } else {
          viewGroup = displayTextContentPreview((Intent)stringBuilder1, paramLayoutInflater, paramViewGroup);
        } 
      } else {
        viewGroup = displayFileContentPreview((Intent)viewGroup, paramLayoutInflater, paramViewGroup);
      } 
    } else {
      viewGroup = displayImageContentPreview((Intent)viewGroup, paramLayoutInflater, paramViewGroup);
    } 
    if (viewGroup != null)
      adjustPreviewWidth((getResources().getConfiguration()).orientation, viewGroup); 
    return viewGroup;
  }
  
  private ViewGroup displayTextContentPreview(Intent paramIntent, LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup) {
    paramViewGroup = getDisplayTextContentPreview(paramLayoutInflater, paramViewGroup);
    ViewGroup viewGroup = paramViewGroup.<ViewGroup>findViewById(16908838);
    if (isOriginUi()) {
      addActionButton(viewGroup, createCopyButton());
      addActionButton(viewGroup, createNearbyButton(paramIntent));
    } else {
      displayTextAddActionButton(viewGroup, new _$$Lambda$ChooserActivity$59FvMzyIg7yJzeX3NNdkiEmiSgI(this));
      addNearbyAction(paramViewGroup, paramIntent);
    } 
    CharSequence charSequence = paramIntent.getCharSequenceExtra("android.intent.extra.TEXT");
    if (charSequence == null) {
      paramViewGroup.<View>findViewById(16908878).setVisibility(8);
    } else {
      TextView textView = paramViewGroup.<TextView>findViewById(16908876);
      textView.setText(charSequence);
    } 
    charSequence = paramIntent.getStringExtra("android.intent.extra.TITLE");
    if (TextUtils.isEmpty(charSequence)) {
      paramViewGroup.<View>findViewById(16908881).setVisibility(8);
    } else {
      Uri uri;
      TextView textView = paramViewGroup.<TextView>findViewById(16908880);
      textView.setText(charSequence);
      ClipData clipData = paramIntent.getClipData();
      charSequence = null;
      CharSequence charSequence1 = charSequence;
      if (clipData != null) {
        charSequence1 = charSequence;
        if (clipData.getItemCount() > 0) {
          ClipData.Item item = clipData.getItemAt(0);
          uri = item.getUri();
        } 
      } 
      ImageView imageView = paramViewGroup.<ImageView>findViewById(16908879);
      if (uri == null) {
        imageView.setVisibility(8);
      } else {
        ContentPreviewCoordinator contentPreviewCoordinator = new ContentPreviewCoordinator(paramViewGroup, false);
        contentPreviewCoordinator.loadUriIntoView(16908879, uri, 0);
      } 
    } 
    return paramViewGroup;
  }
  
  private ViewGroup displayImageContentPreview(Intent paramIntent, LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup) {
    Uri uri;
    ViewGroup viewGroup = getDisplayImageContentPreview(paramLayoutInflater, paramViewGroup);
    paramViewGroup = viewGroup.<ViewGroup>findViewById(16908838);
    if (isOriginUi()) {
      addActionButton(paramViewGroup, createNearbyButton(paramIntent));
    } else {
      addNearbyAction(viewGroup, paramIntent);
    } 
    this.mPreviewCoord = new ContentPreviewCoordinator(viewGroup, true);
    String str = paramIntent.getAction();
    if ("android.intent.action.SEND".equals(str)) {
      uri = (Uri)paramIntent.getParcelableExtra("android.intent.extra.STREAM");
      this.mPreviewCoord.loadUriIntoView(16908871, uri, 0);
    } else {
      Uri<Uri> uri1;
      ContentResolver contentResolver = getContentResolver();
      ArrayList arrayList1 = uri.getParcelableArrayListExtra("android.intent.extra.STREAM");
      ArrayList<Uri> arrayList = new ArrayList();
      for (Uri uri2 : arrayList1) {
        if (isImageType(contentResolver.getType(uri2)))
          arrayList.add(uri2); 
      } 
      if (arrayList.size() == 0) {
        Log.i("ChooserActivity", "Attempted to display image preview area with zero available images detected in EXTRA_STREAM list");
        viewGroup.setVisibility(8);
        return viewGroup;
      } 
      this.mPreviewCoord.loadUriIntoView(16908871, arrayList.get(0), 0);
      if (arrayList.size() == 2) {
        ContentPreviewCoordinator contentPreviewCoordinator = this.mPreviewCoord;
        uri1 = arrayList.get(1);
        contentPreviewCoordinator.loadUriIntoView(16908872, uri1, 0);
      } else if (uri1.size() > 2) {
        ContentPreviewCoordinator contentPreviewCoordinator = this.mPreviewCoord;
        Uri uri2 = uri1.get(1);
        contentPreviewCoordinator.loadUriIntoView(16908873, uri2, 0);
        contentPreviewCoordinator = this.mPreviewCoord;
        uri2 = uri1.get(2);
        int i = uri1.size();
        contentPreviewCoordinator.loadUriIntoView(16908874, uri2, i - 3);
      } 
    } 
    return viewGroup;
  }
  
  class FileInfo {
    public final boolean hasThumbnail;
    
    public final String name;
    
    FileInfo(ChooserActivity this$0, boolean param1Boolean) {
      this.name = (String)this$0;
      this.hasThumbnail = param1Boolean;
    }
  }
  
  public Cursor queryResolver(ContentResolver paramContentResolver, Uri paramUri) {
    return paramContentResolver.query(paramUri, null, null, null, null);
  }
  
  private FileInfo extractFileInfo(Uri paramUri, ContentResolver paramContentResolver) {
    boolean bool4;
    String str2 = null, str3 = null, str4 = null;
    ContentResolver contentResolver = null;
    boolean bool1 = false, bool2 = false;
    boolean bool3 = bool1;
    try {
      Cursor cursor = queryResolver(paramContentResolver, paramUri);
      String str = str3;
      bool4 = bool2;
      if (cursor != null)
        String str5 = str2; 
      if (cursor != null) {
        str4 = str;
        bool3 = bool4;
        cursor.close();
      } 
      str4 = str;
    } catch (SecurityException|NullPointerException securityException) {
      logContentPreviewWarning(paramUri);
      bool4 = bool3;
    } 
    String str1 = str4;
    if (TextUtils.isEmpty(str4)) {
      String str = paramUri.getPath();
      int i = str.lastIndexOf('/');
      str1 = str;
      if (i != -1)
        str1 = str.substring(i + 1); 
    } 
    return new FileInfo(str1, bool4);
  }
  
  private void logContentPreviewWarning(Uri paramUri) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Could not load (");
    stringBuilder.append(paramUri.toString());
    stringBuilder.append(") thumbnail/name for preview. If desired, consider using Intent#createChooser to launch the ChooserActivity, and set your Intent's clipData and flags in accordance with that method's documentation");
    Log.w("ChooserActivity", stringBuilder.toString());
  }
  
  private ViewGroup displayFileContentPreview(Intent paramIntent, LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup) {
    Uri uri;
    ViewGroup viewGroup = getDisplayFileContentPreview(paramLayoutInflater, paramViewGroup);
    paramViewGroup = viewGroup.<ViewGroup>findViewById(16908838);
    if (isOriginUi()) {
      addActionButton(paramViewGroup, createNearbyButton(paramIntent));
    } else {
      addNearbyAction(viewGroup, paramIntent);
    } 
    String str = paramIntent.getAction();
    if ("android.intent.action.SEND".equals(str)) {
      uri = (Uri)paramIntent.getParcelableExtra("android.intent.extra.STREAM");
      loadFileUriIntoView(uri, viewGroup);
    } else {
      ArrayList<Uri> arrayList = uri.getParcelableArrayListExtra("android.intent.extra.STREAM");
      int i = arrayList.size();
      if (i == 0) {
        viewGroup.setVisibility(8);
        Log.i("ChooserActivity", "Appears to be no uris available in EXTRA_STREAM, removing preview area");
        return viewGroup;
      } 
      if (i == 1) {
        loadFileUriIntoView(arrayList.get(0), viewGroup);
      } else {
        FileInfo fileInfo = extractFileInfo(arrayList.get(0), getContentResolver());
        i--;
        Resources resources = getResources();
        String str2 = fileInfo.name, str1 = resources.getQuantityString(18153490, i, new Object[] { str2, Integer.valueOf(i) });
        TextView textView = viewGroup.<TextView>findViewById(16908870);
        textView.setText(str1);
        str1 = viewGroup.findViewById(16908869);
        str1.setVisibility(8);
        ImageView imageView = viewGroup.<ImageView>findViewById(16908867);
        imageView.setVisibility(0);
        imageView.setImageResource(getChooserPreFileSingleIconView(false, (String)null, (Uri)null));
      } 
    } 
    return viewGroup;
  }
  
  private void loadFileUriIntoView(Uri paramUri, View paramView) {
    ContentPreviewCoordinator contentPreviewCoordinator;
    FileInfo fileInfo = extractFileInfo(paramUri, getContentResolver());
    TextView textView = paramView.<TextView>findViewById(16908870);
    textView.setText(fileInfo.name);
    if (fileInfo.hasThumbnail) {
      this.mPreviewCoord = contentPreviewCoordinator = new ContentPreviewCoordinator(paramView, false);
      contentPreviewCoordinator.loadUriIntoView(16908869, paramUri, 0);
    } else {
      View view = (View)contentPreviewCoordinator.findViewById(16908869);
      view.setVisibility(8);
      ImageView imageView = contentPreviewCoordinator.<ImageView>findViewById(16908867);
      imageView.setVisibility(0);
      if (!tryApkResourceName(paramUri, imageView, textView))
        imageView.setImageResource(getChooserPreFileSingleIconView(true, fileInfo.name, paramUri)); 
    } 
  }
  
  protected boolean isImageType(String paramString) {
    boolean bool;
    if (paramString != null && paramString.startsWith("image/")) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private int findPreferredContentPreview(Uri paramUri, ContentResolver paramContentResolver) {
    byte b;
    if (paramUri == null)
      return 3; 
    String str = paramContentResolver.getType(paramUri);
    if (isImageType(str)) {
      b = 1;
    } else {
      b = 2;
    } 
    return b;
  }
  
  private int findPreferredContentPreview(Intent paramIntent, ContentResolver paramContentResolver) {
    Uri uri;
    String str = paramIntent.getAction();
    if ("android.intent.action.SEND".equals(str)) {
      uri = (Uri)paramIntent.getParcelableExtra("android.intent.extra.STREAM");
      return findPreferredContentPreview(uri, paramContentResolver);
    } 
    if ("android.intent.action.SEND_MULTIPLE".equals(str)) {
      ArrayList arrayList = uri.getParcelableArrayListExtra("android.intent.extra.STREAM");
      if (arrayList == null || arrayList.isEmpty())
        return 3; 
      for (Uri uri1 : arrayList) {
        if (findPreferredContentPreview(uri1, paramContentResolver) == 2)
          return 2; 
      } 
      return 1;
    } 
    return 3;
  }
  
  private int getNumSheetExpansions() {
    return getPreferences(0).getInt("pref_num_sheet_expansions", 0);
  }
  
  private void incrementNumSheetExpansions() {
    SharedPreferences.Editor editor = getPreferences(0).edit();
    int i = getNumSheetExpansions();
    editor = editor.putInt("pref_num_sheet_expansions", i + 1);
    editor.apply();
  }
  
  protected void onDestroy() {
    super.onDestroy();
    RefinementResultReceiver refinementResultReceiver = this.mRefinementResultReceiver;
    if (refinementResultReceiver != null) {
      refinementResultReceiver.destroy();
      this.mRefinementResultReceiver = null;
    } 
    unbindRemainingServices();
    this.mChooserHandler.removeAllMessages();
    ContentPreviewCoordinator contentPreviewCoordinator = this.mPreviewCoord;
    if (contentPreviewCoordinator != null)
      contentPreviewCoordinator.cancelLoads(); 
    this.mChooserMultiProfilePagerAdapter.getActiveListAdapter().destroyAppPredictor();
    if (this.mChooserMultiProfilePagerAdapter.getInactiveListAdapter() != null)
      this.mChooserMultiProfilePagerAdapter.getInactiveListAdapter().destroyAppPredictor(); 
  }
  
  public Intent getReplacementIntent(ActivityInfo paramActivityInfo, Intent paramIntent) {
    // Byte code:
    //   0: aload_2
    //   1: astore_3
    //   2: aload_0
    //   3: getfield mReplacementExtras : Landroid/os/Bundle;
    //   6: astore #4
    //   8: aload_3
    //   9: astore #5
    //   11: aload #4
    //   13: ifnull -> 53
    //   16: aload #4
    //   18: aload_1
    //   19: getfield packageName : Ljava/lang/String;
    //   22: invokevirtual getBundle : (Ljava/lang/String;)Landroid/os/Bundle;
    //   25: astore #4
    //   27: aload_3
    //   28: astore #5
    //   30: aload #4
    //   32: ifnull -> 53
    //   35: new android/content/Intent
    //   38: dup
    //   39: aload_2
    //   40: invokespecial <init> : (Landroid/content/Intent;)V
    //   43: astore #5
    //   45: aload #5
    //   47: aload #4
    //   49: invokevirtual putExtras : (Landroid/os/Bundle;)Landroid/content/Intent;
    //   52: pop
    //   53: aload_1
    //   54: getfield name : Ljava/lang/String;
    //   57: getstatic com/android/internal/app/IntentForwarderActivity.FORWARD_INTENT_TO_PARENT : Ljava/lang/String;
    //   60: invokevirtual equals : (Ljava/lang/Object;)Z
    //   63: ifne -> 86
    //   66: aload_1
    //   67: getfield name : Ljava/lang/String;
    //   70: astore_2
    //   71: getstatic com/android/internal/app/IntentForwarderActivity.FORWARD_INTENT_TO_MANAGED_PROFILE : Ljava/lang/String;
    //   74: astore_3
    //   75: aload #5
    //   77: astore_1
    //   78: aload_2
    //   79: aload_3
    //   80: invokevirtual equals : (Ljava/lang/Object;)Z
    //   83: ifeq -> 113
    //   86: aload_0
    //   87: invokevirtual getIntent : ()Landroid/content/Intent;
    //   90: ldc_w 'android.intent.extra.TITLE'
    //   93: invokevirtual getCharSequenceExtra : (Ljava/lang/String;)Ljava/lang/CharSequence;
    //   96: astore_1
    //   97: aload #5
    //   99: aload_1
    //   100: invokestatic createChooser : (Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;
    //   103: astore_1
    //   104: aload_1
    //   105: ldc_w 'android.intent.extra.AUTO_LAUNCH_SINGLE_CHOICE'
    //   108: iconst_0
    //   109: invokevirtual putExtra : (Ljava/lang/String;Z)Landroid/content/Intent;
    //   112: pop
    //   113: aload_1
    //   114: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1697	-> 0
    //   #1698	-> 2
    //   #1699	-> 16
    //   #1700	-> 27
    //   #1701	-> 35
    //   #1702	-> 45
    //   #1705	-> 53
    //   #1706	-> 75
    //   #1707	-> 86
    //   #1708	-> 86
    //   #1707	-> 97
    //   #1713	-> 104
    //   #1715	-> 113
  }
  
  public void onActivityStarted(TargetInfo paramTargetInfo) {
    if (this.mChosenComponentSender != null) {
      ComponentName componentName = paramTargetInfo.getResolvedComponentName();
      if (componentName != null) {
        Intent intent = (new Intent()).putExtra("android.intent.extra.CHOSEN_COMPONENT", (Parcelable)componentName);
        try {
          this.mChosenComponentSender.sendIntent((Context)this, -1, intent, null, null);
        } catch (android.content.IntentSender.SendIntentException sendIntentException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Unable to launch supplied IntentSender to report the chosen component: ");
          stringBuilder.append(sendIntentException);
          Slog.e("ChooserActivity", stringBuilder.toString());
        } 
      } 
    } 
  }
  
  public void addUseDifferentAppLabelIfNecessary(ResolverListAdapter paramResolverListAdapter) {
    ChooserTarget[] arrayOfChooserTarget = this.mCallerChooserTargets;
    if (arrayOfChooserTarget != null && arrayOfChooserTarget.length > 0) {
      ChooserListAdapter chooserListAdapter = this.mChooserMultiProfilePagerAdapter.getActiveListAdapter();
      ChooserTarget[] arrayOfChooserTarget1 = this.mCallerChooserTargets;
      ArrayList<ChooserTarget> arrayList = Lists.newArrayList((Object[])arrayOfChooserTarget1);
      List<ChooserTargetServiceConnection> list = this.mServiceConnections;
      chooserListAdapter.addServiceResults((DisplayResolveInfo)null, arrayList, 0, (Map<ChooserTarget, ShortcutInfo>)null, list);
    } 
  }
  
  public int getLayoutResource() {
    return 17367128;
  }
  
  public boolean shouldGetActivityMetadata() {
    return true;
  }
  
  public boolean shouldAutoLaunchSingleChoice(TargetInfo paramTargetInfo) {
    if (!super.shouldAutoLaunchSingleChoice(paramTargetInfo))
      return false; 
    return getIntent().getBooleanExtra("android.intent.extra.AUTO_LAUNCH_SINGLE_CHOICE", true);
  }
  
  private void showTargetDetails(DisplayResolveInfo paramDisplayResolveInfo) {
    List<DisplayResolveInfo> list;
    if (paramDisplayResolveInfo == null)
      return; 
    if (paramDisplayResolveInfo instanceof MultiDisplayResolveInfo) {
      paramDisplayResolveInfo = paramDisplayResolveInfo;
      list = paramDisplayResolveInfo.getTargets();
    } else {
      list = Collections.singletonList(list);
    } 
    ChooserMultiProfilePagerAdapter chooserMultiProfilePagerAdapter = this.mChooserMultiProfilePagerAdapter;
    ChooserTargetActionsDialogFragment chooserTargetActionsDialogFragment = new ChooserTargetActionsDialogFragment(list, chooserMultiProfilePagerAdapter.getCurrentUserHandle());
    chooserTargetActionsDialogFragment.show(getFragmentManager(), "targetDetailsFragment");
  }
  
  private void modifyTargetIntent(Intent paramIntent) {
    if (isSendAction(paramIntent))
      paramIntent.addFlags(134742016); 
  }
  
  protected boolean onTargetSelected(TargetInfo paramTargetInfo, boolean paramBoolean) {
    if (this.mRefinementIntentSender != null) {
      Intent intent = new Intent();
      List<Intent> list = paramTargetInfo.getAllSourceIntents();
      if (!list.isEmpty()) {
        intent.putExtra("android.intent.extra.INTENT", (Parcelable)list.get(0));
        if (list.size() > 1) {
          Intent[] arrayOfIntent = new Intent[list.size() - 1];
          byte b;
          int i;
          for (b = 1, i = list.size(); b < i; b++)
            arrayOfIntent[b - 1] = list.get(b); 
          intent.putExtra("android.intent.extra.ALTERNATE_INTENTS", (Parcelable[])arrayOfIntent);
        } 
        RefinementResultReceiver refinementResultReceiver = this.mRefinementResultReceiver;
        if (refinementResultReceiver != null)
          refinementResultReceiver.destroy(); 
        this.mRefinementResultReceiver = refinementResultReceiver = new RefinementResultReceiver(this, paramTargetInfo, null);
        intent.putExtra("android.intent.extra.RESULT_RECEIVER", (Parcelable)refinementResultReceiver);
        try {
          this.mRefinementIntentSender.sendIntent((Context)this, 0, intent, null, null);
          return false;
        } catch (android.content.IntentSender.SendIntentException sendIntentException) {
          Log.e("ChooserActivity", "Refinement IntentSender failed to send", (Throwable)sendIntentException);
        } 
      } 
    } 
    updateModelAndChooserCounts(paramTargetInfo);
    return super.onTargetSelected(paramTargetInfo, paramBoolean);
  }
  
  public void startSelected(int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual isOriginUi : ()Z
    //   4: ifne -> 15
    //   7: aload_0
    //   8: iload_1
    //   9: iload_2
    //   10: iload_3
    //   11: invokespecial startSelected : (IZZ)V
    //   14: return
    //   15: aload_0
    //   16: getfield mChooserMultiProfilePagerAdapter : Lcom/android/internal/app/ChooserMultiProfilePagerAdapter;
    //   19: astore #4
    //   21: aload #4
    //   23: invokevirtual getActiveListAdapter : ()Lcom/android/internal/app/ChooserListAdapter;
    //   26: astore #5
    //   28: aload #5
    //   30: iload_1
    //   31: iload_3
    //   32: invokevirtual targetInfoForPosition : (IZ)Lcom/android/internal/app/chooser/TargetInfo;
    //   35: astore #6
    //   37: aload #6
    //   39: ifnull -> 51
    //   42: aload #6
    //   44: instanceof com/android/internal/app/chooser/NotSelectableTargetInfo
    //   47: ifeq -> 51
    //   50: return
    //   51: invokestatic currentTimeMillis : ()J
    //   54: aload_0
    //   55: getfield mChooserShownTime : J
    //   58: lsub
    //   59: lstore #7
    //   61: aload #6
    //   63: instanceof com/android/internal/app/chooser/MultiDisplayResolveInfo
    //   66: ifeq -> 119
    //   69: aload #6
    //   71: checkcast com/android/internal/app/chooser/MultiDisplayResolveInfo
    //   74: astore #4
    //   76: aload #4
    //   78: invokevirtual hasSelected : ()Z
    //   81: ifne -> 119
    //   84: aload_0
    //   85: getfield mChooserMultiProfilePagerAdapter : Lcom/android/internal/app/ChooserMultiProfilePagerAdapter;
    //   88: astore #6
    //   90: new com/android/internal/app/ChooserStackedAppDialogFragment
    //   93: dup
    //   94: aload #4
    //   96: iload_1
    //   97: aload #6
    //   99: invokevirtual getCurrentUserHandle : ()Landroid/os/UserHandle;
    //   102: invokespecial <init> : (Lcom/android/internal/app/chooser/MultiDisplayResolveInfo;ILandroid/os/UserHandle;)V
    //   105: astore #4
    //   107: aload #4
    //   109: aload_0
    //   110: invokevirtual getFragmentManager : ()Landroid/app/FragmentManager;
    //   113: ldc 'targetDetailsFragment'
    //   115: invokevirtual show : (Landroid/app/FragmentManager;Ljava/lang/String;)V
    //   118: return
    //   119: aload_0
    //   120: iload_1
    //   121: iload_2
    //   122: iload_3
    //   123: invokespecial startSelected : (IZZ)V
    //   126: aload #5
    //   128: invokevirtual getCount : ()I
    //   131: ifle -> 632
    //   134: iconst_0
    //   135: istore #9
    //   137: iload_1
    //   138: istore #10
    //   140: iconst_m1
    //   141: istore #11
    //   143: iconst_0
    //   144: istore #12
    //   146: iconst_0
    //   147: istore #13
    //   149: aconst_null
    //   150: astore #4
    //   152: aload #5
    //   154: iload_1
    //   155: invokevirtual getPositionTargetType : (I)I
    //   158: istore_1
    //   159: iload_1
    //   160: ifeq -> 377
    //   163: iload_1
    //   164: iconst_1
    //   165: if_icmpeq -> 226
    //   168: iload_1
    //   169: iconst_2
    //   170: if_icmpeq -> 377
    //   173: iload_1
    //   174: iconst_3
    //   175: if_icmpeq -> 184
    //   178: iload #9
    //   180: istore_1
    //   181: goto -> 431
    //   184: iconst_m1
    //   185: istore #10
    //   187: sipush #217
    //   190: istore_1
    //   191: aload_0
    //   192: invokevirtual getChooserActivityLogger : ()Lcom/android/internal/app/ChooserActivityLogger;
    //   195: astore #5
    //   197: aload #6
    //   199: invokeinterface getResolveInfo : ()Landroid/content/pm/ResolveInfo;
    //   204: getfield activityInfo : Landroid/content/pm/ActivityInfo;
    //   207: getfield processName : Ljava/lang/String;
    //   210: astore #6
    //   212: aload #5
    //   214: iconst_3
    //   215: aload #6
    //   217: iconst_m1
    //   218: invokeinterface logShareTargetSelected : (ILjava/lang/String;I)V
    //   223: goto -> 431
    //   226: sipush #216
    //   229: istore_1
    //   230: aload #5
    //   232: iload #10
    //   234: invokevirtual getChooserTargetForValue : (I)Landroid/service/chooser/ChooserTarget;
    //   237: astore #14
    //   239: invokestatic getInstance : ()Landroid/util/HashedStringCache;
    //   242: astore #4
    //   244: new java/lang/StringBuilder
    //   247: dup
    //   248: invokespecial <init> : ()V
    //   251: astore #5
    //   253: aload #5
    //   255: aload #14
    //   257: invokevirtual getComponentName : ()Landroid/content/ComponentName;
    //   260: invokevirtual getPackageName : ()Ljava/lang/String;
    //   263: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   266: pop
    //   267: aload #5
    //   269: aload #14
    //   271: invokevirtual getTitle : ()Ljava/lang/CharSequence;
    //   274: invokeinterface toString : ()Ljava/lang/String;
    //   279: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   282: pop
    //   283: aload #5
    //   285: invokevirtual toString : ()Ljava/lang/String;
    //   288: astore #5
    //   290: aload_0
    //   291: getfield mMaxHashSaltDays : I
    //   294: istore #12
    //   296: aload #4
    //   298: aload_0
    //   299: ldc 'ChooserActivity'
    //   301: aload #5
    //   303: iload #12
    //   305: invokevirtual hashString : (Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/util/HashedStringCache$HashResult;
    //   308: astore #4
    //   310: aload_0
    //   311: aload #6
    //   313: checkcast com/android/internal/app/chooser/SelectableTargetInfo
    //   316: invokespecial getRankedPosition : (Lcom/android/internal/app/chooser/SelectableTargetInfo;)I
    //   319: istore #11
    //   321: aload_0
    //   322: getfield mCallerChooserTargets : [Landroid/service/chooser/ChooserTarget;
    //   325: astore #5
    //   327: iload #13
    //   329: istore #12
    //   331: aload #5
    //   333: ifnull -> 341
    //   336: aload #5
    //   338: arraylength
    //   339: istore #12
    //   341: aload_0
    //   342: invokevirtual getChooserActivityLogger : ()Lcom/android/internal/app/ChooserActivityLogger;
    //   345: astore #5
    //   347: aload #6
    //   349: invokeinterface getResolveInfo : ()Landroid/content/pm/ResolveInfo;
    //   354: getfield activityInfo : Landroid/content/pm/ActivityInfo;
    //   357: getfield processName : Ljava/lang/String;
    //   360: astore #6
    //   362: aload #5
    //   364: iconst_1
    //   365: aload #6
    //   367: iload #10
    //   369: invokeinterface logShareTargetSelected : (ILjava/lang/String;I)V
    //   374: goto -> 431
    //   377: sipush #215
    //   380: istore_1
    //   381: iload #10
    //   383: aload #5
    //   385: invokevirtual getSelectableServiceTargetCount : ()I
    //   388: isub
    //   389: istore #10
    //   391: aload #5
    //   393: invokevirtual getCallerTargetCount : ()I
    //   396: istore #12
    //   398: aload_0
    //   399: invokevirtual getChooserActivityLogger : ()Lcom/android/internal/app/ChooserActivityLogger;
    //   402: astore #5
    //   404: aload #6
    //   406: invokeinterface getResolveInfo : ()Landroid/content/pm/ResolveInfo;
    //   411: getfield activityInfo : Landroid/content/pm/ActivityInfo;
    //   414: getfield processName : Ljava/lang/String;
    //   417: astore #6
    //   419: aload #5
    //   421: iconst_2
    //   422: aload #6
    //   424: iload #10
    //   426: invokeinterface logShareTargetSelected : (ILjava/lang/String;I)V
    //   431: iload_1
    //   432: ifeq -> 525
    //   435: new android/metrics/LogMaker
    //   438: dup
    //   439: iload_1
    //   440: invokespecial <init> : (I)V
    //   443: iload #10
    //   445: invokevirtual setSubtype : (I)Landroid/metrics/LogMaker;
    //   448: astore #6
    //   450: aload #4
    //   452: ifnull -> 502
    //   455: aload #6
    //   457: sipush #1704
    //   460: aload #4
    //   462: getfield hashedString : Ljava/lang/String;
    //   465: invokevirtual addTaggedData : (ILjava/lang/Object;)Landroid/metrics/LogMaker;
    //   468: pop
    //   469: aload #4
    //   471: getfield saltGeneration : I
    //   474: istore_1
    //   475: aload #6
    //   477: sipush #1705
    //   480: iload_1
    //   481: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   484: invokevirtual addTaggedData : (ILjava/lang/Object;)Landroid/metrics/LogMaker;
    //   487: pop
    //   488: aload #6
    //   490: sipush #1087
    //   493: iload #11
    //   495: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   498: invokevirtual addTaggedData : (ILjava/lang/Object;)Landroid/metrics/LogMaker;
    //   501: pop
    //   502: aload #6
    //   504: sipush #1086
    //   507: iload #12
    //   509: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   512: invokevirtual addTaggedData : (ILjava/lang/Object;)Landroid/metrics/LogMaker;
    //   515: pop
    //   516: aload_0
    //   517: invokevirtual getMetricsLogger : ()Lcom/android/internal/logging/MetricsLogger;
    //   520: aload #6
    //   522: invokevirtual write : (Landroid/metrics/LogMaker;)V
    //   525: aload_0
    //   526: getfield mIsSuccessfullySelected : Z
    //   529: ifeq -> 632
    //   532: new java/lang/StringBuilder
    //   535: dup
    //   536: invokespecial <init> : ()V
    //   539: astore #4
    //   541: aload #4
    //   543: ldc_w 'User Selection Time Cost is '
    //   546: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   549: pop
    //   550: aload #4
    //   552: lload #7
    //   554: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   557: pop
    //   558: ldc 'ChooserActivity'
    //   560: aload #4
    //   562: invokevirtual toString : ()Ljava/lang/String;
    //   565: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   568: pop
    //   569: new java/lang/StringBuilder
    //   572: dup
    //   573: invokespecial <init> : ()V
    //   576: astore #4
    //   578: aload #4
    //   580: ldc_w 'position of selected app/service/caller is '
    //   583: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   586: pop
    //   587: aload #4
    //   589: iload #10
    //   591: invokestatic toString : (I)Ljava/lang/String;
    //   594: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   597: pop
    //   598: aload #4
    //   600: invokevirtual toString : ()Ljava/lang/String;
    //   603: astore #4
    //   605: ldc 'ChooserActivity'
    //   607: aload #4
    //   609: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   612: pop
    //   613: aconst_null
    //   614: ldc_w 'user_selection_cost_for_smart_sharing'
    //   617: lload #7
    //   619: l2i
    //   620: invokestatic histogram : (Landroid/content/Context;Ljava/lang/String;I)V
    //   623: aconst_null
    //   624: ldc_w 'app_position_for_smart_sharing'
    //   627: iload #10
    //   629: invokestatic histogram : (Landroid/content/Context;Ljava/lang/String;I)V
    //   632: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1829	-> 0
    //   #1830	-> 7
    //   #1831	-> 14
    //   #1834	-> 15
    //   #1835	-> 21
    //   #1836	-> 28
    //   #1837	-> 28
    //   #1838	-> 37
    //   #1839	-> 50
    //   #1842	-> 51
    //   #1844	-> 61
    //   #1845	-> 69
    //   #1846	-> 76
    //   #1847	-> 84
    //   #1849	-> 90
    //   #1851	-> 107
    //   #1852	-> 118
    //   #1856	-> 119
    //   #1859	-> 126
    //   #1862	-> 134
    //   #1863	-> 137
    //   #1864	-> 140
    //   #1865	-> 143
    //   #1866	-> 149
    //   #1867	-> 152
    //   #1904	-> 184
    //   #1905	-> 187
    //   #1906	-> 191
    //   #1908	-> 197
    //   #1906	-> 212
    //   #1869	-> 226
    //   #1872	-> 230
    //   #1873	-> 239
    //   #1876	-> 253
    //   #1877	-> 267
    //   #1873	-> 296
    //   #1879	-> 310
    //   #1881	-> 321
    //   #1882	-> 336
    //   #1884	-> 341
    //   #1886	-> 347
    //   #1884	-> 362
    //   #1889	-> 374
    //   #1892	-> 377
    //   #1893	-> 381
    //   #1894	-> 391
    //   #1895	-> 398
    //   #1897	-> 404
    //   #1895	-> 419
    //   #1900	-> 431
    //   #1914	-> 431
    //   #1915	-> 435
    //   #1916	-> 450
    //   #1917	-> 455
    //   #1919	-> 469
    //   #1921	-> 475
    //   #1919	-> 475
    //   #1922	-> 488
    //   #1923	-> 488
    //   #1922	-> 488
    //   #1925	-> 502
    //   #1926	-> 502
    //   #1925	-> 502
    //   #1927	-> 516
    //   #1930	-> 525
    //   #1932	-> 532
    //   #1933	-> 569
    //   #1934	-> 587
    //   #1933	-> 605
    //   #1936	-> 613
    //   #1938	-> 623
    //   #1941	-> 632
  }
  
  private int getRankedPosition(SelectableTargetInfo paramSelectableTargetInfo) {
    String str = paramSelectableTargetInfo.getChooserTarget().getComponentName().getPackageName();
    ChooserMultiProfilePagerAdapter chooserMultiProfilePagerAdapter = this.mChooserMultiProfilePagerAdapter;
    ChooserListAdapter chooserListAdapter = chooserMultiProfilePagerAdapter.getActiveListAdapter();
    int i = Math.min(chooserListAdapter.mDisplayList.size(), 12);
    for (byte b = 0; b < i; b++) {
      DisplayResolveInfo displayResolveInfo = chooserListAdapter.mDisplayList.get(b);
      if ((displayResolveInfo.getResolveInfo()).activityInfo.packageName.equals(str))
        return b; 
    } 
    return -1;
  }
  
  protected boolean shouldAddFooterView() {
    return true;
  }
  
  protected void applyFooterView(int paramInt) {
    int i = this.mChooserMultiProfilePagerAdapter.getItemCount();
    for (byte b = 0; b < i; b++)
      this.mChooserMultiProfilePagerAdapter.getAdapterForIndex(b).setFooterHeight(paramInt); 
  }
  
  protected void queryTargetServices(ChooserListAdapter paramChooserListAdapter) {
    String str = "android.permission.BIND_CHOOSER_TARGET_SERVICE";
    this.mQueriedTargetServicesTimeMs = System.currentTimeMillis();
    UserHandle userHandle = paramChooserListAdapter.getUserHandle();
    Context context = createContextAsUser(userHandle, 0);
    PackageManager packageManager = context.getPackageManager();
    ShortcutManager shortcutManager = (ShortcutManager)context.getSystemService(ShortcutManager.class);
    int i = paramChooserListAdapter.getDisplayResolveInfoCount();
    byte b = 0;
    int j = 0;
    while (true) {
      if (b < i) {
        DisplayResolveInfo displayResolveInfo = paramChooserListAdapter.getDisplayResolveInfo(b);
        if (paramChooserListAdapter.getScore(displayResolveInfo) != 0.0F) {
          ActivityInfo activityInfo = (displayResolveInfo.getResolveInfo()).activityInfo;
          String str1 = activityInfo.packageName;
          if (!shortcutManager.hasShareTargets(str1)) {
            Bundle bundle = activityInfo.metaData;
            if (bundle != null) {
              str1 = activityInfo.packageName;
              String str2 = bundle.getString("android.service.chooser.chooser_target_service");
              str1 = convertServiceName(str1, str2);
            } else {
              str1 = null;
            } 
            if (str1 != null) {
              ComponentName componentName = new ComponentName(activityInfo.packageName, str1);
              UserHandle userHandle1 = paramChooserListAdapter.getUserHandle();
              Pair<ComponentName, UserHandle> pair = new Pair<>(componentName, userHandle1);
              if (!this.mServicesRequested.contains(pair)) {
                ChooserTargetServiceConnection chooserTargetServiceConnection;
                this.mServicesRequested.add(pair);
                Intent intent = new Intent("android.service.chooser.ChooserTargetService");
                intent = intent.setComponent(componentName);
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("queryTargets found target with service ");
                stringBuilder.append(componentName);
                Log.d("ChooserActivity", stringBuilder.toString());
                try {
                  StringBuilder stringBuilder1;
                  String str2 = (packageManager.getServiceInfo(componentName, 0)).permission;
                  if (!str.equals(str2)) {
                    stringBuilder1 = new StringBuilder();
                    this();
                    stringBuilder1.append("ChooserTargetService ");
                    stringBuilder1.append(componentName);
                    stringBuilder1.append(" does not require permission ");
                    stringBuilder1.append(str);
                    stringBuilder1.append(" - this service will not be queried for ChooserTargets. add android:permission=\"");
                    stringBuilder1.append(str);
                    stringBuilder1.append("\" to the <service> tag for ");
                    stringBuilder1.append(componentName);
                    stringBuilder1.append(" in the manifest.");
                    Log.w("ChooserActivity", stringBuilder1.toString());
                  } else {
                    chooserTargetServiceConnection = new ChooserTargetServiceConnection(this, (DisplayResolveInfo)stringBuilder1, paramChooserListAdapter.getUserHandle());
                    UserHandle userHandle2 = paramChooserListAdapter.getUserHandle();
                    int k = j;
                    if (bindServiceAsUser(intent, chooserTargetServiceConnection, 5, userHandle2)) {
                      StringBuilder stringBuilder2 = new StringBuilder();
                      stringBuilder2.append("Binding service connection for target ");
                      stringBuilder2.append(stringBuilder1);
                      stringBuilder2.append(" intent ");
                      stringBuilder2.append(intent);
                      Log.d("ChooserActivity", stringBuilder2.toString());
                      this.mServiceConnections.add(chooserTargetServiceConnection);
                      k = j + 1;
                    } 
                    j = k;
                  } 
                } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
                  StringBuilder stringBuilder1 = new StringBuilder();
                  stringBuilder1.append("Could not look up service ");
                  stringBuilder1.append(chooserTargetServiceConnection);
                  stringBuilder1.append("; component name not found");
                  Log.e("ChooserActivity", stringBuilder1.toString());
                } 
              } 
            } else {
              int k = j;
              j = k;
            } 
          } 
        } 
      } else {
        break;
      } 
      b++;
    } 
    this.mChooserHandler.restartServiceRequestTimer();
  }
  
  private IntentFilter getTargetIntentFilter() {
    try {
      Uri uri;
      Intent intent = getTargetIntent();
      String str = intent.getDataString();
      if (!TextUtils.isEmpty(str))
        return new IntentFilter(intent.getAction(), str); 
      if (intent.getType() == null) {
        Log.e("ChooserActivity", "Failed to get target intent filter: intent data and type are null");
        return null;
      } 
      IntentFilter intentFilter = new IntentFilter();
      this(intent.getAction(), intent.getType());
      ArrayList<Uri> arrayList = new ArrayList();
      this();
      boolean bool = "android.intent.action.SEND".equals(intent.getAction());
      if (bool) {
        uri = (Uri)intent.getParcelableExtra("android.intent.extra.STREAM");
        if (uri != null)
          arrayList.add(uri); 
      } else {
        ArrayList<? extends Uri> arrayList1 = uri.getParcelableArrayListExtra("android.intent.extra.STREAM");
        if (arrayList1 != null)
          arrayList.addAll(arrayList1); 
      } 
      for (Uri uri1 : arrayList) {
        intentFilter.addDataScheme(uri1.getScheme());
        intentFilter.addDataAuthority(uri1.getAuthority(), null);
        intentFilter.addDataPath(uri1.getPath(), 0);
      } 
      return intentFilter;
    } catch (Exception exception) {
      Log.e("ChooserActivity", "Failed to get target intent filter", exception);
      return null;
    } 
  }
  
  private List<DisplayResolveInfo> getDisplayResolveInfos(ChooserListAdapter paramChooserListAdapter) {
    ArrayList<DisplayResolveInfo> arrayList = new ArrayList();
    int i = 0;
    byte b;
    int j;
    for (b = 0, j = paramChooserListAdapter.getDisplayResolveInfoCount(); b < j; b++) {
      DisplayResolveInfo displayResolveInfo = paramChooserListAdapter.getDisplayResolveInfo(b);
      if (paramChooserListAdapter.getScore(displayResolveInfo) != 0.0F) {
        arrayList.add(displayResolveInfo);
        int k = i + 1;
        i = k;
        if (k >= 20) {
          Log.d("ChooserActivity", "queryTargets hit query target limit 20");
          break;
        } 
      } 
    } 
    return arrayList;
  }
  
  protected void queryDirectShareTargets(ChooserListAdapter paramChooserListAdapter, boolean paramBoolean) {
    this.mQueriedSharingShortcutsTimeMs = System.currentTimeMillis();
    UserHandle userHandle = paramChooserListAdapter.getUserHandle();
    if (!paramBoolean) {
      AppPredictor appPredictor = getAppPredictorForDirectShareIfEnabled(userHandle);
      if (appPredictor != null) {
        appPredictor.requestPredictionUpdate();
        return;
      } 
    } 
    IntentFilter intentFilter = getTargetIntentFilter();
    if (intentFilter == null)
      return; 
    List<DisplayResolveInfo> list = getDisplayResolveInfos(paramChooserListAdapter);
    AsyncTask.execute(new _$$Lambda$ChooserActivity$mxeAr2AePjaKhu0ZyVnWFOAHyI4(this, userHandle, intentFilter, list));
  }
  
  private boolean shouldQueryShortcutManager(UserHandle paramUserHandle) {
    if (!shouldShowTabs())
      return true; 
    if (!getWorkProfileUserHandle().equals(paramUserHandle))
      return true; 
    if (!isUserRunning(paramUserHandle))
      return false; 
    if (!isUserUnlocked(paramUserHandle))
      return false; 
    if (isQuietModeEnabled(paramUserHandle))
      return false; 
    return true;
  }
  
  private void sendChooserTargetRankingScore(List<AppTarget> paramList, UserHandle paramUserHandle) {
    Message message = Message.obtain();
    message.what = 7;
    message.obj = new ChooserTargetRankingInfo(paramList, paramUserHandle);
    this.mChooserHandler.sendMessage(message);
  }
  
  private void sendShareShortcutInfoList(List<ShortcutManager.ShareShortcutInfo> paramList, List<DisplayResolveInfo> paramList1, List<AppTarget> paramList2, UserHandle paramUserHandle) {
    if (paramList2 == null || paramList2.size() == paramList.size()) {
      int i;
      for (i = paramList.size() - 1; i >= 0; i--) {
        String str = ((ShortcutManager.ShareShortcutInfo)paramList.get(i)).getTargetComponent().getPackageName();
        if (!isPackageEnabled(str)) {
          paramList.remove(i);
          if (paramList2 != null)
            paramList2.remove(i); 
        } 
      } 
      if (paramList2 == null) {
        i = 2;
      } else {
        i = 3;
      } 
      boolean bool = false;
      for (byte b = 0; b < paramList1.size(); b++) {
        ArrayList<ShortcutManager.ShareShortcutInfo> arrayList = new ArrayList();
        for (byte b1 = 0; b1 < paramList.size(); b1++) {
          ComponentName componentName2 = ((DisplayResolveInfo)paramList1.get(b)).getResolvedComponentName();
          ComponentName componentName1 = ((ShortcutManager.ShareShortcutInfo)paramList.get(b1)).getTargetComponent();
          if (componentName2.equals(componentName1))
            arrayList.add(paramList.get(b1)); 
        } 
        if (!arrayList.isEmpty()) {
          List<ChooserTarget> list = convertToChooserTarget(arrayList, paramList, paramList2, i);
          Message message = Message.obtain();
          message.what = 4;
          message.obj = new ServiceResultInfo(paramList1.get(b), list, null, paramUserHandle);
          message.arg1 = i;
          this.mChooserHandler.sendMessage(message);
          bool = true;
        } 
      } 
      if (bool)
        sendShortcutManagerShareTargetResultCompleted(); 
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("resultList and appTargets must have the same size. resultList.size()=");
    stringBuilder.append(paramList.size());
    stringBuilder.append(" appTargets.size()=");
    stringBuilder.append(paramList2.size());
    throw new RuntimeException(stringBuilder.toString());
  }
  
  private void sendShortcutManagerShareTargetResultCompleted() {
    Message message = Message.obtain();
    message.what = 5;
    this.mChooserHandler.sendMessage(message);
  }
  
  private boolean isPackageEnabled(String paramString) {
    if (TextUtils.isEmpty(paramString))
      return false; 
    try {
      ApplicationInfo applicationInfo = getPackageManager().getApplicationInfo(paramString, 0);
      if (applicationInfo != null && applicationInfo.enabled && (applicationInfo.flags & 0x40000000) == 0)
        return true; 
      return false;
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      return false;
    } 
  }
  
  public List<ChooserTarget> convertToChooserTarget(List<ShortcutManager.ShareShortcutInfo> paramList1, List<ShortcutManager.ShareShortcutInfo> paramList2, List<AppTarget> paramList, int paramInt) {
    ArrayList<Integer> arrayList = new ArrayList();
    if (paramInt == 2) {
      for (byte b1 = 0; b1 < paramList1.size(); b1++) {
        int i = ((ShortcutManager.ShareShortcutInfo)paramList1.get(b1)).getShortcutInfo().getRank();
        if (!arrayList.contains(Integer.valueOf(i)))
          arrayList.add(Integer.valueOf(i)); 
      } 
      Collections.sort(arrayList);
    } 
    ArrayList<ChooserTarget> arrayList1 = new ArrayList(paramList1.size());
    for (byte b = 0; b < paramList1.size(); b++) {
      float f;
      ShortcutInfo shortcutInfo = ((ShortcutManager.ShareShortcutInfo)paramList1.get(b)).getShortcutInfo();
      int i = paramList2.indexOf(paramList1.get(b));
      if (paramInt == 3) {
        f = Math.max(1.0F - i * 0.01F, 0.0F);
      } else {
        int j = arrayList.indexOf(Integer.valueOf(shortcutInfo.getRank()));
        f = Math.max(1.0F - j * 0.01F, 0.0F);
      } 
      Bundle bundle = new Bundle();
      bundle.putString("android.intent.extra.shortcut.ID", shortcutInfo.getId());
      CharSequence charSequence = shortcutInfo.getLabel();
      ChooserTarget chooserTarget = new ChooserTarget(charSequence, null, f, ((ShortcutManager.ShareShortcutInfo)paramList1.get(b)).getTargetComponent().clone(), bundle);
      arrayList1.add(chooserTarget);
      Map<ChooserTarget, AppTarget> map1 = this.mDirectShareAppTargetCache;
      if (map1 != null && paramList != null) {
        AppTarget appTarget = paramList.get(i);
        map1.put(chooserTarget, appTarget);
      } 
      Map<ChooserTarget, ShortcutInfo> map = this.mDirectShareShortcutInfoCache;
      if (map != null)
        map.put(chooserTarget, shortcutInfo); 
    } 
    -$.Lambda.ChooserActivity._VTDVaCqBlg9iWxX_Tt6C9h0CH8 _VTDVaCqBlg9iWxX_Tt6C9h0CH8 = _$$Lambda$ChooserActivity$_VTDVaCqBlg9iWxX_Tt6C9h0CH8.INSTANCE;
    Collections.sort(arrayList1, (Comparator<? super ChooserTarget>)_VTDVaCqBlg9iWxX_Tt6C9h0CH8);
    return arrayList1;
  }
  
  private String convertServiceName(String paramString1, String paramString2) {
    if (TextUtils.isEmpty(paramString2))
      return null; 
    if (paramString2.startsWith(".")) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append(paramString2);
      paramString1 = stringBuilder.toString();
    } else if (paramString2.indexOf('.') >= 0) {
      paramString1 = paramString2;
    } else {
      paramString1 = null;
    } 
    return paramString1;
  }
  
  void unbindRemainingServices() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("unbindRemainingServices, ");
    stringBuilder.append(this.mServiceConnections.size());
    stringBuilder.append(" left");
    Log.d("ChooserActivity", stringBuilder.toString());
    byte b;
    int i;
    for (b = 0, i = this.mServiceConnections.size(); b < i; b++) {
      ChooserTargetServiceConnection chooserTargetServiceConnection = this.mServiceConnections.get(b);
      stringBuilder = new StringBuilder();
      stringBuilder.append("unbinding ");
      stringBuilder.append(chooserTargetServiceConnection);
      Log.d("ChooserActivity", stringBuilder.toString());
      unbindService(chooserTargetServiceConnection);
      chooserTargetServiceConnection.destroy();
    } 
    this.mServicesRequested.clear();
    this.mServiceConnections.clear();
  }
  
  private void logDirectShareTargetReceived(int paramInt) {
    long l;
    if (paramInt == 1718) {
      l = this.mQueriedSharingShortcutsTimeMs;
    } else {
      l = this.mQueriedTargetServicesTimeMs;
    } 
    int i = (int)(System.currentTimeMillis() - l);
    getMetricsLogger().write((new LogMaker(paramInt)).setSubtype(i));
  }
  
  void updateModelAndChooserCounts(TargetInfo paramTargetInfo) {
    TargetInfo targetInfo = paramTargetInfo;
    if (paramTargetInfo != null) {
      targetInfo = paramTargetInfo;
      if (paramTargetInfo instanceof MultiDisplayResolveInfo)
        targetInfo = ((MultiDisplayResolveInfo)paramTargetInfo).getSelectedTarget(); 
    } 
    if (targetInfo != null) {
      sendClickToAppPredictor(targetInfo);
      ResolveInfo resolveInfo = targetInfo.getResolveInfo();
      Intent intent = getTargetIntent();
      if (resolveInfo != null && resolveInfo.activityInfo != null && intent != null) {
        ChooserMultiProfilePagerAdapter chooserMultiProfilePagerAdapter = this.mChooserMultiProfilePagerAdapter;
        ChooserListAdapter chooserListAdapter = chooserMultiProfilePagerAdapter.getActiveListAdapter();
        if (chooserListAdapter != null) {
          sendImpressionToAppPredictor(targetInfo, chooserListAdapter);
          chooserListAdapter.updateModel(targetInfo.getResolvedComponentName());
          String str2 = resolveInfo.activityInfo.packageName;
          String str1 = intent.getAction();
          chooserListAdapter.updateChooserCounts(str2, str1);
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("ResolveInfo Package is ");
        stringBuilder.append(resolveInfo.activityInfo.packageName);
        Log.d("ChooserActivity", stringBuilder.toString());
        stringBuilder = new StringBuilder();
        stringBuilder.append("Action to be updated is ");
        stringBuilder.append(intent.getAction());
        Log.d("ChooserActivity", stringBuilder.toString());
      } else {
        Log.d("ChooserActivity", "Can not log Chooser Counts of null ResovleInfo");
      } 
    } 
    this.mIsSuccessfullySelected = true;
  }
  
  private void sendImpressionToAppPredictor(TargetInfo paramTargetInfo, ChooserListAdapter paramChooserListAdapter) {
    if (!this.mChooserTargetRankingEnabled)
      return; 
    ChooserMultiProfilePagerAdapter chooserMultiProfilePagerAdapter = this.mChooserMultiProfilePagerAdapter;
    UserHandle userHandle = chooserMultiProfilePagerAdapter.getCurrentUserHandle();
    AppPredictor appPredictor = getAppPredictorForDirectShareIfEnabled(userHandle);
    if (appPredictor == null)
      return; 
    if (paramTargetInfo instanceof ChooserTargetInfo)
      return; 
    List<ChooserTargetInfo> list = paramChooserListAdapter.getSurfacedTargetInfo();
    ArrayList<AppTargetId> arrayList = new ArrayList();
    for (ChooserTargetInfo chooserTargetInfo : list) {
      AppTargetId appTargetId2;
      ChooserTarget chooserTarget = chooserTargetInfo.getChooserTarget();
      Map<ComponentName, ComponentName> map = this.mChooserTargetComponentNameCache;
      ComponentName componentName2 = chooserTarget.getComponentName(), componentName3 = chooserTarget.getComponentName();
      ComponentName componentName1 = map.getOrDefault(componentName2, componentName3);
      if (this.mDirectShareShortcutInfoCache.containsKey(chooserTarget)) {
        String str1 = ((ShortcutInfo)this.mDirectShareShortcutInfoCache.get(chooserTarget)).getId();
        appTargetId2 = new AppTargetId(String.format("%s/%s/%s", new Object[] { str1, componentName1.flattenToString(), "shortcut_target" }));
        arrayList.add(appTargetId2);
        continue;
      } 
      String str = ChooserUtil.md5(appTargetId2.getTitle().toString());
      AppTargetId appTargetId1 = new AppTargetId(String.format("%s/%s/%s", new Object[] { str, componentName1.flattenToString(), "chooser_target" }));
      arrayList.add(appTargetId1);
    } 
    appPredictor.notifyLaunchLocationShown("direct_share", arrayList);
  }
  
  private void sendClickToAppPredictor(TargetInfo paramTargetInfo) {
    AppTarget appTarget1;
    ChooserMultiProfilePagerAdapter chooserMultiProfilePagerAdapter = this.mChooserMultiProfilePagerAdapter;
    UserHandle userHandle = chooserMultiProfilePagerAdapter.getCurrentUserHandle();
    AppPredictor appPredictor = getAppPredictorForDirectShareIfEnabled(userHandle);
    if (appPredictor == null)
      return; 
    if (!(paramTargetInfo instanceof ChooserTargetInfo))
      return; 
    ChooserTarget chooserTarget = ((ChooserTargetInfo)paramTargetInfo).getChooserTarget();
    paramTargetInfo = null;
    Map<ChooserTarget, AppTarget> map = this.mDirectShareAppTargetCache;
    if (map != null)
      appTarget1 = map.get(chooserTarget); 
    AppTarget appTarget2 = appTarget1;
    if (this.mChooserTargetRankingEnabled) {
      appTarget2 = appTarget1;
      if (appTarget1 == null) {
        Map<ComponentName, ComponentName> map1 = this.mChooserTargetComponentNameCache;
        ComponentName componentName2 = chooserTarget.getComponentName(), componentName1 = chooserTarget.getComponentName();
        componentName2 = map1.getOrDefault(componentName2, componentName1);
        try {
          AppTarget.Builder builder = new AppTarget.Builder();
          AppTargetId appTargetId = new AppTargetId();
          this(componentName2.flattenToString());
          ShortcutInfo.Builder builder2 = new ShortcutInfo.Builder();
          String str = componentName2.getPackageName();
          UserHandle userHandle1 = getUser();
          this(createPackageContextAsUser(str, 0, userHandle1), "chooser_target");
          builder2 = builder2.setActivity(componentName2);
          ShortcutInfo.Builder builder1 = builder2.setShortLabel(ChooserUtil.md5(chooserTarget.getTitle().toString()));
          this(appTargetId, builder1.build());
          builder = builder.setClassName(componentName2.getClassName());
          AppTarget appTarget = builder.build();
        } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Could not look up service ");
          stringBuilder.append(componentName2);
          stringBuilder.append("; component name not found");
          Log.e("ChooserActivity", stringBuilder.toString());
          appTarget2 = appTarget1;
        } 
      } 
    } 
    if (appTarget2 != null) {
      AppTargetEvent.Builder builder = new AppTargetEvent.Builder(appTarget2, 1);
      builder = builder.setLaunchLocation("direct_share");
      AppTargetEvent appTargetEvent = builder.build();
      appPredictor.notifyAppTargetEvent(appTargetEvent);
    } 
  }
  
  private AppPredictor createAppPredictor(UserHandle paramUserHandle) {
    if (!this.mIsAppPredictorComponentAvailable)
      return null; 
    if (getPersonalProfileUserHandle().equals(paramUserHandle)) {
      AppPredictor appPredictor1 = this.mPersonalAppPredictor;
      if (appPredictor1 != null)
        return appPredictor1; 
    } else {
      AppPredictor appPredictor1 = this.mWorkAppPredictor;
      if (appPredictor1 != null)
        return appPredictor1; 
    } 
    Context context = createContextAsUser(paramUserHandle, 0);
    IntentFilter intentFilter = getTargetIntentFilter();
    Bundle bundle = new Bundle();
    bundle.putParcelable("intent_filter", (Parcelable)intentFilter);
    AppPredictionContext.Builder builder1 = new AppPredictionContext.Builder(context);
    builder1 = builder1.setUiSurface("share");
    builder1 = builder1.setPredictedTargetCount(20);
    AppPredictionContext.Builder builder2 = builder1.setExtras(bundle);
    AppPredictionContext appPredictionContext = builder2.build();
    AppPredictionManager appPredictionManager = (AppPredictionManager)context.getSystemService(AppPredictionManager.class);
    AppPredictor appPredictor = appPredictionManager.createAppPredictionSession(appPredictionContext);
    if (getPersonalProfileUserHandle().equals(paramUserHandle)) {
      this.mPersonalAppPredictor = appPredictor;
    } else {
      this.mWorkAppPredictor = appPredictor;
    } 
    return appPredictor;
  }
  
  private AppPredictor getAppPredictorForDirectShareIfEnabled(UserHandle paramUserHandle) {
    if (!ActivityManager.isLowRamDeviceStatic()) {
      AppPredictor appPredictor = createAppPredictor(paramUserHandle);
    } else {
      paramUserHandle = null;
    } 
    return (AppPredictor)paramUserHandle;
  }
  
  private AppPredictor getAppPredictorForShareActivitiesIfEnabled(UserHandle paramUserHandle) {
    return createAppPredictor(paramUserHandle);
  }
  
  void onRefinementResult(TargetInfo paramTargetInfo, Intent paramIntent) {
    RefinementResultReceiver refinementResultReceiver = this.mRefinementResultReceiver;
    if (refinementResultReceiver != null) {
      refinementResultReceiver.destroy();
      this.mRefinementResultReceiver = null;
    } 
    if (paramTargetInfo == null) {
      Log.e("ChooserActivity", "Refinement result intent did not match any known targets; canceling");
    } else if (!checkTargetSourceIntent(paramTargetInfo, paramIntent)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onRefinementResult: Selected target ");
      stringBuilder.append(paramTargetInfo);
      stringBuilder.append(" cannot match refined source intent ");
      stringBuilder.append(paramIntent);
      Log.e("ChooserActivity", stringBuilder.toString());
    } else {
      paramTargetInfo = paramTargetInfo.cloneFilledIn(paramIntent, 0);
      if (super.onTargetSelected(paramTargetInfo, false)) {
        updateModelAndChooserCounts(paramTargetInfo);
        finish();
        return;
      } 
    } 
    onRefinementCanceled();
  }
  
  void onRefinementCanceled() {
    RefinementResultReceiver refinementResultReceiver = this.mRefinementResultReceiver;
    if (refinementResultReceiver != null) {
      refinementResultReceiver.destroy();
      this.mRefinementResultReceiver = null;
    } 
    finish();
  }
  
  boolean checkTargetSourceIntent(TargetInfo paramTargetInfo, Intent paramIntent) {
    List<Intent> list = paramTargetInfo.getAllSourceIntents();
    byte b;
    int i;
    for (b = 0, i = list.size(); b < i; b++) {
      Intent intent = list.get(b);
      if (intent.filterEquals(paramIntent))
        return true; 
    } 
    return false;
  }
  
  void filterServiceTargets(Context paramContext, String paramString, List<ChooserTarget> paramList) {
    // Byte code:
    //   0: aload_3
    //   1: ifnonnull -> 5
    //   4: return
    //   5: aload_1
    //   6: invokevirtual getPackageManager : ()Landroid/content/pm/PackageManager;
    //   9: astore_1
    //   10: aload_3
    //   11: invokeinterface size : ()I
    //   16: iconst_1
    //   17: isub
    //   18: istore #4
    //   20: iload #4
    //   22: iflt -> 189
    //   25: aload_3
    //   26: iload #4
    //   28: invokeinterface get : (I)Ljava/lang/Object;
    //   33: checkcast android/service/chooser/ChooserTarget
    //   36: astore #5
    //   38: aload #5
    //   40: invokevirtual getComponentName : ()Landroid/content/ComponentName;
    //   43: astore #6
    //   45: aload_2
    //   46: ifnull -> 64
    //   49: aload_2
    //   50: aload #6
    //   52: invokevirtual getPackageName : ()Ljava/lang/String;
    //   55: invokevirtual equals : (Ljava/lang/Object;)Z
    //   58: ifeq -> 64
    //   61: goto -> 183
    //   64: iconst_0
    //   65: istore #7
    //   67: aload_1
    //   68: aload #6
    //   70: iconst_0
    //   71: invokevirtual getActivityInfo : (Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    //   74: astore #6
    //   76: aload #6
    //   78: getfield exported : Z
    //   81: ifeq -> 96
    //   84: aload #6
    //   86: getfield permission : Ljava/lang/String;
    //   89: astore #6
    //   91: aload #6
    //   93: ifnull -> 99
    //   96: iconst_1
    //   97: istore #7
    //   99: goto -> 169
    //   102: astore #6
    //   104: new java/lang/StringBuilder
    //   107: dup
    //   108: invokespecial <init> : ()V
    //   111: astore #6
    //   113: aload #6
    //   115: ldc_w 'Target '
    //   118: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   121: pop
    //   122: aload #6
    //   124: aload #5
    //   126: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   129: pop
    //   130: aload #6
    //   132: ldc_w ' returned by '
    //   135: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   138: pop
    //   139: aload #6
    //   141: aload_2
    //   142: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   145: pop
    //   146: aload #6
    //   148: ldc_w ' component not found'
    //   151: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   154: pop
    //   155: ldc 'ChooserActivity'
    //   157: aload #6
    //   159: invokevirtual toString : ()Ljava/lang/String;
    //   162: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   165: pop
    //   166: iconst_1
    //   167: istore #7
    //   169: iload #7
    //   171: ifeq -> 183
    //   174: aload_3
    //   175: iload #4
    //   177: invokeinterface remove : (I)Ljava/lang/Object;
    //   182: pop
    //   183: iinc #4, -1
    //   186: goto -> 20
    //   189: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2588	-> 0
    //   #2589	-> 4
    //   #2592	-> 5
    //   #2593	-> 10
    //   #2594	-> 25
    //   #2595	-> 38
    //   #2596	-> 45
    //   #2598	-> 61
    //   #2603	-> 64
    //   #2604	-> 76
    //   #2609	-> 99
    //   #2605	-> 102
    //   #2606	-> 104
    //   #2608	-> 166
    //   #2611	-> 169
    //   #2612	-> 174
    //   #2593	-> 183
    //   #2615	-> 189
    // Exception table:
    //   from	to	target	type
    //   67	76	102	android/content/pm/PackageManager$NameNotFoundException
    //   76	91	102	android/content/pm/PackageManager$NameNotFoundException
  }
  
  class AzInfoComparator implements Comparator<DisplayResolveInfo> {
    Collator mCollator;
    
    AzInfoComparator(ChooserActivity this$0) {
      this.mCollator = Collator.getInstance((this$0.getResources().getConfiguration()).locale);
    }
    
    public int compare(DisplayResolveInfo param1DisplayResolveInfo1, DisplayResolveInfo param1DisplayResolveInfo2) {
      return this.mCollator.compare(param1DisplayResolveInfo1.getDisplayLabel(), param1DisplayResolveInfo2.getDisplayLabel());
    }
  }
  
  protected MetricsLogger getMetricsLogger() {
    if (this.mMetricsLogger == null)
      this.mMetricsLogger = new MetricsLogger(); 
    return this.mMetricsLogger;
  }
  
  protected ChooserActivityLogger getChooserActivityLogger() {
    if (this.mChooserActivityLogger == null)
      this.mChooserActivityLogger = new ChooserActivityLoggerImpl(); 
    return this.mChooserActivityLogger;
  }
  
  class ChooserListController extends ResolverListController {
    final ChooserActivity this$0;
    
    public ChooserListController(Context param1Context, PackageManager param1PackageManager, Intent param1Intent, String param1String, int param1Int, UserHandle param1UserHandle, AbstractResolverComparator param1AbstractResolverComparator) {
      super(param1Context, param1PackageManager, param1Intent, param1String, param1Int, param1UserHandle, param1AbstractResolverComparator);
    }
    
    boolean isComponentFiltered(ComponentName param1ComponentName) {
      if (ChooserActivity.this.mFilteredComponentNames == null)
        return false; 
      for (ComponentName componentName : ChooserActivity.this.mFilteredComponentNames) {
        if (param1ComponentName.equals(componentName))
          return true; 
      } 
      return false;
    }
    
    public boolean isComponentPinned(ComponentName param1ComponentName) {
      return ChooserActivity.this.mPinnedSharedPrefs.getBoolean(param1ComponentName.flattenToString(), false);
    }
  }
  
  public ChooserGridAdapter createChooserGridAdapter(Context paramContext, List<Intent> paramList, Intent[] paramArrayOfIntent, List<ResolveInfo> paramList1, boolean paramBoolean, UserHandle paramUserHandle) {
    ResolverListController resolverListController = createListController(paramUserHandle);
    ChooserListAdapter chooserListAdapter = createChooserListAdapter(paramContext, paramList, paramArrayOfIntent, paramList1, paramBoolean, resolverListController);
    AppPredictor.Callback callback = createAppPredictorCallback(chooserListAdapter);
    AppPredictor appPredictor = setupAppPredictorForUser(paramUserHandle, callback);
    chooserListAdapter.setAppPredictor(appPredictor);
    chooserListAdapter.setAppPredictorCallback(callback);
    return new ChooserGridAdapter(chooserListAdapter);
  }
  
  public ChooserListAdapter createChooserListAdapter(Context paramContext, List<Intent> paramList, Intent[] paramArrayOfIntent, List<ResolveInfo> paramList1, boolean paramBoolean, ResolverListController paramResolverListController) {
    return 
      
      new ChooserListAdapter(paramContext, paramList, paramArrayOfIntent, paramList1, paramBoolean, paramResolverListController, this, this, paramContext.getPackageManager());
  }
  
  protected ResolverListController createListController(UserHandle paramUserHandle) {
    ResolverRankerServiceResolverComparator resolverRankerServiceResolverComparator;
    AppPredictor appPredictor = getAppPredictorForShareActivitiesIfEnabled(paramUserHandle);
    if (appPredictor != null) {
      Intent intent1 = getTargetIntent();
      AppPredictionServiceResolverComparator appPredictionServiceResolverComparator = new AppPredictionServiceResolverComparator((Context)this, intent1, getReferrerPackageName(), appPredictor, paramUserHandle);
    } else {
      Intent intent1 = getTargetIntent();
      resolverRankerServiceResolverComparator = new ResolverRankerServiceResolverComparator((Context)this, intent1, getReferrerPackageName(), null);
    } 
    PackageManager packageManager = this.mPm;
    Intent intent = getTargetIntent();
    return new ChooserListController((Context)this, packageManager, intent, getReferrerPackageName(), this.mLaunchedFromUid, paramUserHandle, resolverRankerServiceResolverComparator);
  }
  
  protected Bitmap loadThumbnail(Uri paramUri, Size paramSize) {
    if (paramUri == null || paramSize == null)
      return null; 
    try {
      return getContentResolver().loadThumbnail(paramUri, paramSize, null);
    } catch (IOException|NullPointerException|SecurityException iOException) {
      logContentPreviewWarning(paramUri);
      return null;
    } 
  }
  
  class PlaceHolderTargetInfo extends NotSelectableTargetInfo {
    public Drawable getDisplayIcon(Context param1Context) {
      AnimatedVectorDrawable animatedVectorDrawable = (AnimatedVectorDrawable)param1Context.getDrawable(17302115);
      animatedVectorDrawable.start();
      return (Drawable)animatedVectorDrawable;
    }
  }
  
  class EmptyTargetInfo extends NotSelectableTargetInfo {
    public Drawable getDisplayIcon(Context param1Context) {
      return null;
    }
  }
  
  private void handleScroll(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (this.mChooserMultiProfilePagerAdapter.getCurrentRootAdapter() != null)
      this.mChooserMultiProfilePagerAdapter.getCurrentRootAdapter().handleScroll(paramView, paramInt2, paramInt4); 
  }
  
  private void handleLayoutChange(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mChooserMultiProfilePagerAdapter : Lcom/android/internal/app/ChooserMultiProfilePagerAdapter;
    //   4: astore #10
    //   6: aload #10
    //   8: ifnonnull -> 12
    //   11: return
    //   12: aload #10
    //   14: invokevirtual getActiveAdapterView : ()Lcom/android/internal/widget/RecyclerView;
    //   17: astore #11
    //   19: aload_0
    //   20: getfield mChooserMultiProfilePagerAdapter : Lcom/android/internal/app/ChooserMultiProfilePagerAdapter;
    //   23: invokevirtual getCurrentRootAdapter : ()Lcom/android/internal/app/ChooserActivity$ChooserGridAdapter;
    //   26: astore #10
    //   28: aload #10
    //   30: ifnull -> 223
    //   33: aload #11
    //   35: ifnonnull -> 41
    //   38: goto -> 223
    //   41: iload #4
    //   43: iload_2
    //   44: isub
    //   45: aload_1
    //   46: invokevirtual getPaddingLeft : ()I
    //   49: isub
    //   50: aload_1
    //   51: invokevirtual getPaddingRight : ()I
    //   54: isub
    //   55: istore #4
    //   57: aload #10
    //   59: invokevirtual consumeLayoutRequest : ()Z
    //   62: ifne -> 100
    //   65: aload #10
    //   67: iload #4
    //   69: invokevirtual calculateChooserTargetWidth : (I)Z
    //   72: ifne -> 100
    //   75: aload #11
    //   77: invokevirtual getAdapter : ()Lcom/android/internal/widget/RecyclerView$Adapter;
    //   80: ifnull -> 100
    //   83: iload #4
    //   85: aload_0
    //   86: getfield mCurrAvailableWidth : I
    //   89: if_icmpeq -> 95
    //   92: goto -> 100
    //   95: iconst_0
    //   96: istore_2
    //   97: goto -> 102
    //   100: iconst_1
    //   101: istore_2
    //   102: iload_2
    //   103: ifne -> 122
    //   106: aload_0
    //   107: getfield mLastNumberOfChildren : I
    //   110: istore #6
    //   112: iload #6
    //   114: aload #11
    //   116: invokevirtual getChildCount : ()I
    //   119: if_icmpeq -> 222
    //   122: aload_0
    //   123: iload #4
    //   125: putfield mCurrAvailableWidth : I
    //   128: iload_2
    //   129: ifeq -> 159
    //   132: aload #11
    //   134: aload #10
    //   136: invokevirtual setAdapter : (Lcom/android/internal/widget/RecyclerView$Adapter;)V
    //   139: aload #11
    //   141: invokevirtual getLayoutManager : ()Lcom/android/internal/widget/RecyclerView$LayoutManager;
    //   144: checkcast com/android/internal/widget/GridLayoutManager
    //   147: astore_1
    //   148: aload #10
    //   150: invokevirtual getMaxTargetsPerRow : ()I
    //   153: istore_2
    //   154: aload_1
    //   155: iload_2
    //   156: invokevirtual setSpanCount : (I)V
    //   159: aload_0
    //   160: getfield mChooserMultiProfilePagerAdapter : Lcom/android/internal/app/ChooserMultiProfilePagerAdapter;
    //   163: invokevirtual getCurrentUserHandle : ()Landroid/os/UserHandle;
    //   166: astore_1
    //   167: aload_0
    //   168: aload_1
    //   169: invokespecial getProfileForUser : (Landroid/os/UserHandle;)I
    //   172: istore #4
    //   174: aload_0
    //   175: invokespecial findSelectedProfile : ()I
    //   178: istore_2
    //   179: iload #4
    //   181: iload_2
    //   182: if_icmpeq -> 186
    //   185: return
    //   186: aload_0
    //   187: getfield mLastNumberOfChildren : I
    //   190: aload #11
    //   192: invokevirtual getChildCount : ()I
    //   195: if_icmpne -> 199
    //   198: return
    //   199: aload_0
    //   200: invokevirtual getMainThreadHandler : ()Landroid/os/Handler;
    //   203: new com/android/internal/app/_$$Lambda$ChooserActivity$dWUTOqlT87tmcofyNKrR69p1dGE
    //   206: dup
    //   207: aload_0
    //   208: aload #10
    //   210: aload #11
    //   212: iload #5
    //   214: iload_3
    //   215: invokespecial <init> : (Lcom/android/internal/app/ChooserActivity;Lcom/android/internal/app/ChooserActivity$ChooserGridAdapter;Lcom/android/internal/widget/RecyclerView;II)V
    //   218: invokevirtual post : (Ljava/lang/Runnable;)Z
    //   221: pop
    //   222: return
    //   223: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2766	-> 0
    //   #2767	-> 11
    //   #2769	-> 12
    //   #2770	-> 19
    //   #2771	-> 28
    //   #2775	-> 41
    //   #2776	-> 57
    //   #2777	-> 65
    //   #2778	-> 75
    //   #2780	-> 102
    //   #2781	-> 112
    //   #2782	-> 122
    //   #2783	-> 128
    //   #2787	-> 132
    //   #2788	-> 139
    //   #2789	-> 148
    //   #2788	-> 154
    //   #2792	-> 159
    //   #2793	-> 167
    //   #2794	-> 174
    //   #2795	-> 179
    //   #2796	-> 185
    //   #2799	-> 186
    //   #2800	-> 198
    //   #2803	-> 199
    //   #2894	-> 222
    //   #2772	-> 223
  }
  
  private boolean shouldShowExtraRow(int paramInt) {
    boolean bool = shouldShowTabs();
    null = true;
    if (bool && paramInt == 1) {
      ChooserMultiProfilePagerAdapter chooserMultiProfilePagerAdapter = this.mChooserMultiProfilePagerAdapter;
      ChooserListAdapter chooserListAdapter = chooserMultiProfilePagerAdapter.getInactiveListAdapter();
      if (chooserMultiProfilePagerAdapter.shouldShowEmptyStateScreen(chooserListAdapter))
        return null; 
    } 
    return false;
  }
  
  private int getProfileForUser(UserHandle paramUserHandle) {
    if (paramUserHandle.equals(getPersonalProfileUserHandle()))
      return 0; 
    if (paramUserHandle.equals(getWorkProfileUserHandle()))
      return 1; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("User ");
    stringBuilder.append(paramUserHandle);
    stringBuilder.append(" does not belong to a personal or work profile.");
    Log.e("ChooserActivity", stringBuilder.toString());
    return -1;
  }
  
  private ViewGroup getActiveEmptyStateView() {
    int i = this.mChooserMultiProfilePagerAdapter.getCurrentPage();
    return this.mChooserMultiProfilePagerAdapter.getItem(i).getEmptyStateView();
  }
  
  class BaseChooserTargetComparator implements Comparator<ChooserTarget> {
    public int compare(ChooserTarget param1ChooserTarget1, ChooserTarget param1ChooserTarget2) {
      return (int)Math.signum(param1ChooserTarget2.getScore() - param1ChooserTarget1.getScore());
    }
  }
  
  public void onHandlePackagesChanged(ResolverListAdapter paramResolverListAdapter) {
    this.mServicesRequested.clear();
    this.mChooserMultiProfilePagerAdapter.getActiveListAdapter().notifyDataSetChanged();
    super.onHandlePackagesChanged(paramResolverListAdapter);
  }
  
  public ResolverListAdapter.ActivityInfoPresentationGetter makePresentationGetter(ActivityInfo paramActivityInfo) {
    return this.mChooserMultiProfilePagerAdapter.getActiveListAdapter().makePresentationGetter(paramActivityInfo);
  }
  
  public Intent getReferrerFillInIntent() {
    return this.mReferrerFillInIntent;
  }
  
  public int getMaxRankedTargets() {
    int i;
    if (this.mChooserMultiProfilePagerAdapter.getCurrentRootAdapter() == null) {
      i = 4;
    } else {
      i = this.mChooserMultiProfilePagerAdapter.getCurrentRootAdapter().getMaxTargetsPerRow();
    } 
    return i;
  }
  
  public void sendListViewUpdateMessage(UserHandle paramUserHandle) {
    Message message = Message.obtain();
    message.what = 6;
    message.obj = paramUserHandle;
    this.mChooserHandler.sendMessageDelayed(message, 250L);
  }
  
  public void onListRebuilt(ResolverListAdapter paramResolverListAdapter) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual isOriginUi : ()Z
    //   4: ifne -> 13
    //   7: aload_0
    //   8: aload_1
    //   9: invokespecial onListRebuilt : (Lcom/android/internal/app/ResolverListAdapter;)V
    //   12: return
    //   13: aload_0
    //   14: invokespecial setupScrollListener : ()V
    //   17: aload_0
    //   18: invokespecial maybeSetupGlobalLayoutListener : ()V
    //   21: aload_1
    //   22: checkcast com/android/internal/app/ChooserListAdapter
    //   25: astore_1
    //   26: aload_1
    //   27: invokevirtual getUserHandle : ()Landroid/os/UserHandle;
    //   30: astore_2
    //   31: aload_0
    //   32: getfield mChooserMultiProfilePagerAdapter : Lcom/android/internal/app/ChooserMultiProfilePagerAdapter;
    //   35: astore_3
    //   36: aload_2
    //   37: aload_3
    //   38: invokevirtual getCurrentUserHandle : ()Landroid/os/UserHandle;
    //   41: invokevirtual equals : (Ljava/lang/Object;)Z
    //   44: ifeq -> 81
    //   47: aload_0
    //   48: getfield mChooserMultiProfilePagerAdapter : Lcom/android/internal/app/ChooserMultiProfilePagerAdapter;
    //   51: invokevirtual getActiveAdapterView : ()Lcom/android/internal/widget/RecyclerView;
    //   54: astore_2
    //   55: aload_0
    //   56: getfield mChooserMultiProfilePagerAdapter : Lcom/android/internal/app/ChooserMultiProfilePagerAdapter;
    //   59: astore_3
    //   60: aload_2
    //   61: aload_3
    //   62: invokevirtual getCurrentRootAdapter : ()Lcom/android/internal/app/ChooserActivity$ChooserGridAdapter;
    //   65: invokevirtual setAdapter : (Lcom/android/internal/widget/RecyclerView$Adapter;)V
    //   68: aload_0
    //   69: getfield mChooserMultiProfilePagerAdapter : Lcom/android/internal/app/ChooserMultiProfilePagerAdapter;
    //   72: astore_3
    //   73: aload_3
    //   74: aload_3
    //   75: invokevirtual getCurrentPage : ()I
    //   78: invokevirtual setupListAdapter : (I)V
    //   81: aload_1
    //   82: getfield mDisplayList : Ljava/util/List;
    //   85: ifnull -> 112
    //   88: aload_1
    //   89: getfield mDisplayList : Ljava/util/List;
    //   92: astore_3
    //   93: aload_3
    //   94: invokeinterface isEmpty : ()Z
    //   99: ifeq -> 105
    //   102: goto -> 112
    //   105: aload_1
    //   106: invokevirtual updateAlphabeticalList : ()V
    //   109: goto -> 116
    //   112: aload_1
    //   113: invokevirtual notifyDataSetChanged : ()V
    //   116: invokestatic isLowRamDeviceStatic : ()Z
    //   119: ifeq -> 132
    //   122: aload_0
    //   123: invokevirtual getChooserActivityLogger : ()Lcom/android/internal/app/ChooserActivityLogger;
    //   126: invokeinterface logSharesheetAppLoadComplete : ()V
    //   131: return
    //   132: aload_0
    //   133: aload_1
    //   134: invokevirtual getUserHandle : ()Landroid/os/UserHandle;
    //   137: invokespecial shouldQueryShortcutManager : (Landroid/os/UserHandle;)Z
    //   140: ifne -> 153
    //   143: aload_0
    //   144: invokevirtual getChooserActivityLogger : ()Lcom/android/internal/app/ChooserActivityLogger;
    //   147: invokeinterface logSharesheetAppLoadComplete : ()V
    //   152: return
    //   153: ldc 'ChooserActivity'
    //   155: ldc_w 'querying direct share targets from ShortcutManager'
    //   158: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   161: pop
    //   162: aload_0
    //   163: aload_1
    //   164: iconst_0
    //   165: invokevirtual queryDirectShareTargets : (Lcom/android/internal/app/ChooserListAdapter;Z)V
    //   168: ldc 'ChooserActivity'
    //   170: ldc_w 'List built querying services'
    //   173: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   176: pop
    //   177: aload_0
    //   178: aload_1
    //   179: invokevirtual queryTargetServices : (Lcom/android/internal/app/ChooserListAdapter;)V
    //   182: aload_0
    //   183: invokevirtual getChooserActivityLogger : ()Lcom/android/internal/app/ChooserActivityLogger;
    //   186: invokeinterface logSharesheetAppLoadComplete : ()V
    //   191: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2971	-> 0
    //   #2972	-> 7
    //   #2973	-> 12
    //   #2976	-> 13
    //   #2977	-> 17
    //   #2979	-> 21
    //   #2980	-> 26
    //   #2981	-> 36
    //   #2982	-> 47
    //   #2983	-> 60
    //   #2984	-> 68
    //   #2985	-> 73
    //   #2988	-> 81
    //   #2989	-> 93
    //   #2992	-> 105
    //   #2990	-> 112
    //   #2996	-> 116
    //   #2997	-> 122
    //   #2998	-> 131
    //   #3002	-> 132
    //   #3003	-> 143
    //   #3004	-> 152
    //   #3010	-> 153
    //   #3013	-> 162
    //   #3017	-> 168
    //   #3020	-> 177
    //   #3023	-> 182
    //   #3024	-> 191
  }
  
  protected boolean isUserRunning(UserHandle paramUserHandle) {
    UserManager userManager = (UserManager)getSystemService(UserManager.class);
    return userManager.isUserRunning(paramUserHandle);
  }
  
  protected boolean isUserUnlocked(UserHandle paramUserHandle) {
    UserManager userManager = (UserManager)getSystemService(UserManager.class);
    return userManager.isUserUnlocked(paramUserHandle);
  }
  
  protected boolean isQuietModeEnabled(UserHandle paramUserHandle) {
    UserManager userManager = (UserManager)getSystemService(UserManager.class);
    return userManager.isQuietModeEnabled(paramUserHandle);
  }
  
  private void setupScrollListener() {
    int i;
    if (this.mResolverDrawerLayout == null)
      return; 
    if (shouldShowTabs()) {
      i = 16909359;
    } else {
      i = 16908840;
    } 
    View view = this.mResolverDrawerLayout.findViewById(i);
    float f1 = view.getElevation();
    float f2 = getResources().getDimensionPixelSize(17105037);
    this.mChooserMultiProfilePagerAdapter.getActiveAdapterView().addOnScrollListener((RecyclerView.OnScrollListener)new Object(this, view, f2, f1));
  }
  
  private void maybeSetupGlobalLayoutListener() {
    if (shouldShowTabs())
      return; 
    RecyclerView recyclerView = this.mChooserMultiProfilePagerAdapter.getActiveAdapterView();
    ViewTreeObserver viewTreeObserver = recyclerView.getViewTreeObserver();
    Object object = new Object(this, (View)recyclerView);
    viewTreeObserver.addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener)object);
  }
  
  public boolean isSendAction(Intent paramIntent) {
    if (paramIntent == null)
      return false; 
    String str = paramIntent.getAction();
    if (str == null)
      return false; 
    if ("android.intent.action.SEND".equals(str) || "android.intent.action.SEND_MULTIPLE".equals(str))
      return true; 
    return false;
  }
  
  private boolean shouldShowStickyContentPreview() {
    boolean bool;
    if (shouldShowStickyContentPreviewNoOrientationCheck() && 
      !getResources().getBoolean(17891613)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean shouldShowStickyContentPreviewNoOrientationCheck() {
    if (shouldShowTabs()) {
      AbstractMultiProfilePagerAdapter abstractMultiProfilePagerAdapter = this.mMultiProfilePagerAdapter;
      UserHandle userHandle = UserHandle.of(UserHandle.myUserId());
      ResolverListAdapter resolverListAdapter = abstractMultiProfilePagerAdapter.getListAdapterForUserHandle(userHandle);
      if (resolverListAdapter.getCount() > 0 && 
        isSendAction(getTargetIntent()))
        return true; 
    } 
    return false;
  }
  
  private void updateStickyContentPreview() {
    if (shouldShowStickyContentPreviewNoOrientationCheck()) {
      ViewGroup viewGroup = (ViewGroup)findViewById(16908865);
      if (viewGroup.getChildCount() == 0) {
        ViewGroup viewGroup1 = createContentPreviewView(viewGroup);
        viewGroup.addView(viewGroup1);
      } 
    } 
    if (shouldShowStickyContentPreview()) {
      showStickyContentPreview();
    } else {
      hideStickyContentPreview();
    } 
  }
  
  private void showStickyContentPreview() {
    if (isStickyContentPreviewShowing())
      return; 
    ViewGroup viewGroup = (ViewGroup)findViewById(16908865);
    viewGroup.setVisibility(0);
  }
  
  private boolean isStickyContentPreviewShowing() {
    boolean bool;
    ViewGroup viewGroup = (ViewGroup)findViewById(16908865);
    if (viewGroup.getVisibility() == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void hideStickyContentPreview() {
    if (!isStickyContentPreviewShowing())
      return; 
    ViewGroup viewGroup = (ViewGroup)findViewById(16908865);
    viewGroup.setVisibility(8);
  }
  
  private void logActionShareWithPreview() {
    Intent intent = getTargetIntent();
    int i = findPreferredContentPreview(intent, getContentResolver());
    MetricsLogger metricsLogger = getMetricsLogger();
    LogMaker logMaker = new LogMaker(1652);
    logMaker = logMaker.setSubtype(i);
    metricsLogger.write(logMaker);
  }
  
  class ViewHolderBase extends RecyclerView.ViewHolder {
    private int mViewType;
    
    final ChooserActivity this$0;
    
    ViewHolderBase(View param1View, int param1Int) {
      super(param1View);
      this.mViewType = param1Int;
    }
    
    int getViewType() {
      return this.mViewType;
    }
  }
  
  class ItemViewHolder extends ViewHolderBase {
    int mListPosition = -1;
    
    ResolverListAdapter.ViewHolder mWrappedViewHolder;
    
    final ChooserActivity this$0;
    
    ItemViewHolder(View param1View, boolean param1Boolean, int param1Int) {
      super(param1View, param1Int);
      this.mWrappedViewHolder = new ResolverListAdapter.ViewHolder(param1View);
      if (param1Boolean) {
        param1View.setOnClickListener(new _$$Lambda$ChooserActivity$ItemViewHolder$_2HyToffGjOTVeFhLII66yqQv8o(this));
        param1View.setOnLongClickListener(new _$$Lambda$ChooserActivity$ItemViewHolder$VYTb2VU1iSkCNOge9YY0P_aAa6o(this));
      } 
    }
  }
  
  class FooterViewHolder extends ViewHolderBase {
    final ChooserActivity this$0;
    
    FooterViewHolder(View param1View, int param1Int) {
      super(param1View, param1Int);
    }
  }
  
  public void onButtonClick(View paramView) {}
  
  protected void resetButtonBar() {}
  
  protected String getMetricsCategory() {
    return "intent_chooser";
  }
  
  protected void onProfileTabSelected() {
    ChooserMultiProfilePagerAdapter chooserMultiProfilePagerAdapter = this.mChooserMultiProfilePagerAdapter;
    ChooserGridAdapter chooserGridAdapter = chooserMultiProfilePagerAdapter.getCurrentRootAdapter();
    chooserGridAdapter.updateDirectShareExpansion();
  }
  
  protected WindowInsets onApplyWindowInsets(View paramView, WindowInsets paramWindowInsets) {
    if (shouldShowTabs()) {
      ChooserMultiProfilePagerAdapter chooserMultiProfilePagerAdapter = this.mChooserMultiProfilePagerAdapter;
      chooserMultiProfilePagerAdapter.setEmptyStateBottomOffset(paramWindowInsets.getSystemWindowInsetBottom());
      chooserMultiProfilePagerAdapter = this.mChooserMultiProfilePagerAdapter;
      View view = (View)getActiveEmptyStateView().findViewById(16909353);
      chooserMultiProfilePagerAdapter.setupContainerPadding(view);
    } 
    return super.onApplyWindowInsets(paramView, paramWindowInsets);
  }
  
  private void setHorizontalScrollingEnabled(boolean paramBoolean) {
    ResolverViewPager resolverViewPager = (ResolverViewPager)findViewById(16909322);
    resolverViewPager.setSwipingEnabled(paramBoolean);
  }
  
  private void setVerticalScrollEnabled(boolean paramBoolean) {
    ChooserMultiProfilePagerAdapter chooserMultiProfilePagerAdapter = this.mChooserMultiProfilePagerAdapter;
    RecyclerView recyclerView = chooserMultiProfilePagerAdapter.getActiveAdapterView();
    ChooserGridLayoutManager chooserGridLayoutManager = (ChooserGridLayoutManager)recyclerView.getLayoutManager();
    chooserGridLayoutManager.setVerticalScrollEnabled(paramBoolean);
  }
  
  void onHorizontalSwipeStateChanged(int paramInt) {
    if (paramInt == 1) {
      if (this.mScrollStatus == 0) {
        this.mScrollStatus = 2;
        setVerticalScrollEnabled(false);
      } 
    } else if (paramInt == 0 && 
      this.mScrollStatus == 2) {
      this.mScrollStatus = 0;
      setVerticalScrollEnabled(true);
    } 
  }
  
  class ChooserGridAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int MAX_TARGETS_PER_ROW_LANDSCAPE = 8;
    
    private static final int MAX_TARGETS_PER_ROW_PORTRAIT = 4;
    
    private static final int NUM_EXPANSIONS_TO_HIDE_AZ_LABEL = 20;
    
    private static final int VIEW_TYPE_AZ_LABEL = 4;
    
    private static final int VIEW_TYPE_CALLER_AND_RANK = 5;
    
    private static final int VIEW_TYPE_CONTENT_PREVIEW = 2;
    
    private static final int VIEW_TYPE_DIRECT_SHARE = 0;
    
    private static final int VIEW_TYPE_FOOTER = 6;
    
    private static final int VIEW_TYPE_NORMAL = 1;
    
    private static final int VIEW_TYPE_PROFILE = 3;
    
    private ChooserListAdapter mChooserListAdapter;
    
    private int mChooserTargetWidth;
    
    private ChooserActivity.DirectShareViewHolder mDirectShareViewHolder;
    
    private int mFooterHeight;
    
    private boolean mHideContentPreview;
    
    private final LayoutInflater mLayoutInflater;
    
    private boolean mLayoutRequested;
    
    private boolean mShowAzLabelIfPoss;
    
    final ChooserActivity this$0;
    
    ChooserGridAdapter(ChooserListAdapter param1ChooserListAdapter) {
      boolean bool = false;
      this.mChooserTargetWidth = 0;
      this.mHideContentPreview = false;
      this.mLayoutRequested = false;
      this.mFooterHeight = 0;
      this.mChooserListAdapter = param1ChooserListAdapter;
      this.mLayoutInflater = LayoutInflater.from((Context)ChooserActivity.this);
      if (ChooserActivity.this.getNumSheetExpansions() < 20)
        bool = true; 
      this.mShowAzLabelIfPoss = bool;
      param1ChooserListAdapter.registerDataSetObserver(new DataSetObserver() {
            final ChooserActivity.ChooserGridAdapter this$1;
            
            final ChooserActivity val$this$0;
            
            public void onChanged() {
              super.onChanged();
              ChooserActivity.ChooserGridAdapter.this.notifyDataSetChanged();
            }
            
            public void onInvalidated() {
              super.onInvalidated();
              ChooserActivity.ChooserGridAdapter.this.notifyDataSetChanged();
            }
          });
    }
    
    public void setFooterHeight(int param1Int) {
      this.mFooterHeight = param1Int;
    }
    
    public boolean calculateChooserTargetWidth(int param1Int) {
      if (param1Int == 0)
        return false; 
      param1Int /= getMaxTargetsPerRow();
      if (param1Int != this.mChooserTargetWidth) {
        this.mChooserTargetWidth = param1Int;
        return true;
      } 
      return false;
    }
    
    int getMaxTargetsPerRow() {
      byte b = 4;
      if (ChooserActivity.this.mShouldDisplayLandscape)
        b = 8; 
      return b;
    }
    
    public void hideContentPreview() {
      this.mHideContentPreview = true;
      this.mLayoutRequested = true;
      notifyDataSetChanged();
    }
    
    public boolean consumeLayoutRequest() {
      boolean bool = this.mLayoutRequested;
      this.mLayoutRequested = false;
      return bool;
    }
    
    public int getRowCount() {
      int i = getContentPreviewRowCount();
      int j = getProfileRowCount();
      int k = getServiceTargetRowCount();
      null = getCallerAndRankedTargetRowCount();
      double d1 = (i + j + k + null + getAzLabelRowCount());
      ChooserListAdapter chooserListAdapter = this.mChooserListAdapter;
      float f = chooserListAdapter.getAlphaTargetCount();
      double d2 = (f / getMaxTargetsPerRow());
      return (int)(d1 + Math.ceil(d2));
    }
    
    public int getContentPreviewRowCount() {
      if (ChooserActivity.this.shouldShowTabs())
        return 0; 
      ChooserActivity chooserActivity = ChooserActivity.this;
      if (!chooserActivity.isSendAction(chooserActivity.getTargetIntent()))
        return 0; 
      if (!this.mHideContentPreview) {
        ChooserListAdapter chooserListAdapter = this.mChooserListAdapter;
        if (chooserListAdapter != null && 
          chooserListAdapter.getCount() != 0)
          return 1; 
      } 
      return 0;
    }
    
    public int getProfileRowCount() {
      boolean bool = ChooserActivity.this.shouldShowTabs();
      boolean bool1 = false;
      if (bool)
        return 0; 
      if (this.mChooserListAdapter.getOtherProfile() != null)
        bool1 = true; 
      return bool1;
    }
    
    public int getFooterRowCount() {
      return 1;
    }
    
    public int getCallerAndRankedTargetRowCount() {
      ChooserListAdapter chooserListAdapter = this.mChooserListAdapter;
      float f = chooserListAdapter.getCallerTargetCount();
      chooserListAdapter = this.mChooserListAdapter;
      double d = ((f + chooserListAdapter.getRankedTargetCount()) / getMaxTargetsPerRow());
      return (int)Math.ceil(d);
    }
    
    public int getServiceTargetRowCount() {
      ChooserActivity chooserActivity = ChooserActivity.this;
      if (chooserActivity.isSendAction(chooserActivity.getTargetIntent()) && 
        !ActivityManager.isLowRamDeviceStatic())
        return 1; 
      return 0;
    }
    
    public int getAzLabelRowCount() {
      boolean bool;
      if (this.mShowAzLabelIfPoss && this.mChooserListAdapter.getAlphaTargetCount() > 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public int getItemCount() {
      int i = getContentPreviewRowCount();
      int j = getProfileRowCount();
      int k = getServiceTargetRowCount();
      int m = getCallerAndRankedTargetRowCount();
      int n = getAzLabelRowCount();
      ChooserListAdapter chooserListAdapter = this.mChooserListAdapter;
      int i1 = chooserListAdapter.getAlphaTargetCount();
      int i2 = getFooterRowCount();
      return i + j + k + m + n + i1 + i2;
    }
    
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup param1ViewGroup, int param1Int) {
      Space space;
      ChooserActivity chooserActivity1;
      ChooserListAdapter chooserListAdapter;
      ChooserActivity chooserActivity2;
      switch (param1Int) {
        default:
          return null;
        case 6:
          space = new Space(param1ViewGroup.getContext());
          space.setLayoutParams((ViewGroup.LayoutParams)new RecyclerView.LayoutParams(-1, this.mFooterHeight));
          return new ChooserActivity.FooterViewHolder(space, param1Int);
        case 4:
          return new ChooserActivity.ItemViewHolder(createAzLabelView((ViewGroup)space), false, param1Int);
        case 3:
          return new ChooserActivity.ItemViewHolder(createProfileView((ViewGroup)space), false, param1Int);
        case 2:
          chooserActivity1 = ChooserActivity.this;
          return new ChooserActivity.ItemViewHolder(chooserActivity1.createContentPreviewView((ViewGroup)space), false, param1Int);
        case 1:
          chooserActivity2 = ChooserActivity.this;
          chooserListAdapter = this.mChooserListAdapter;
          return new ChooserActivity.ItemViewHolder(chooserListAdapter.createView((ViewGroup)space), true, param1Int);
        case 0:
        case 5:
          break;
      } 
      return createItemGroupViewHolder(param1Int, (ViewGroup)space);
    }
    
    public void onBindViewHolder(RecyclerView.ViewHolder param1ViewHolder, int param1Int) {
      int i = ((ChooserActivity.ViewHolderBase)param1ViewHolder).getViewType();
      if (i != 0)
        if (i != 1) {
          if (i != 5)
            return; 
        } else {
          bindItemViewHolder(param1Int, (ChooserActivity.ItemViewHolder)param1ViewHolder);
          return;
        }  
      bindItemGroupViewHolder(param1Int, (ChooserActivity.ItemGroupViewHolder)param1ViewHolder);
    }
    
    public int getItemViewType(int param1Int) {
      int i = getContentPreviewRowCount();
      if (i > 0 && param1Int < i)
        return 2; 
      int j = getProfileRowCount();
      i += j;
      if (j > 0 && param1Int < i)
        return 3; 
      j = getServiceTargetRowCount();
      i += j;
      if (j > 0 && param1Int < i)
        return 0; 
      j = getCallerAndRankedTargetRowCount();
      i += j;
      if (j > 0 && param1Int < i)
        return 5; 
      j = getAzLabelRowCount();
      if (j > 0 && param1Int < i + j)
        return 4; 
      if (param1Int == getItemCount() - 1)
        return 6; 
      return 1;
    }
    
    public int getTargetType(int param1Int) {
      return this.mChooserListAdapter.getPositionTargetType(getListPosition(param1Int));
    }
    
    private View createProfileView(ViewGroup param1ViewGroup) {
      View view = this.mLayoutInflater.inflate(17367133, param1ViewGroup, false);
      ChooserActivity chooserActivity = ChooserActivity.this;
      Drawable drawable = chooserActivity.getResources().getDrawable(17302120, null);
      view.setBackground(drawable);
      ChooserActivity.this.mProfileView = view.findViewById(16909321);
      ChooserActivity.this.mProfileView.setOnClickListener(new _$$Lambda$KV7a09lZoRu37HsBE4cW2uLB7o8(ChooserActivity.this));
      ChooserActivity.this.updateProfileViewButton();
      return view;
    }
    
    private View createAzLabelView(ViewGroup param1ViewGroup) {
      return this.mLayoutInflater.inflate(17367125, param1ViewGroup, false);
    }
    
    private ChooserActivity.ItemGroupViewHolder loadViewsIntoGroup(final ChooserActivity.ItemGroupViewHolder holder) {
      int i = View.MeasureSpec.makeMeasureSpec(0, 0);
      int j = View.MeasureSpec.makeMeasureSpec(this.mChooserTargetWidth, 1073741824);
      int k = holder.getColumnCount();
      boolean bool = holder instanceof ChooserActivity.DirectShareViewHolder;
      for (final byte column = 0; b < k; b++) {
        View view = this.mChooserListAdapter.createView(holder.getRowByIndex(b));
        view.setOnClickListener(new View.OnClickListener() {
              final ChooserActivity.ChooserGridAdapter this$1;
              
              final int val$column;
              
              final ChooserActivity.ItemGroupViewHolder val$holder;
              
              public void onClick(View param2View) {
                ChooserActivity.this.startSelected(holder.getItemIndex(column), false, true);
              }
            });
        if (!bool)
          view.setOnLongClickListener(new _$$Lambda$ChooserActivity$ChooserGridAdapter$2swpezlubBT8DYtHhCZoMx_tHFc(this, holder, b)); 
        holder.addView(b, view);
        if (bool) {
          ResolverListAdapter.ViewHolder viewHolder = (ResolverListAdapter.ViewHolder)view.getTag();
          viewHolder.text.setLines(2);
          viewHolder.text.setHorizontallyScrolling(false);
          viewHolder.text2.setVisibility(8);
        } 
        view.measure(j, i);
        setViewBounds(view, view.getMeasuredWidth(), view.getMeasuredHeight());
      } 
      ViewGroup viewGroup = holder.getViewGroup();
      holder.measure();
      setViewBounds(viewGroup, -1, holder.getMeasuredRowHeight());
      if (bool) {
        ChooserActivity.DirectShareViewHolder directShareViewHolder = (ChooserActivity.DirectShareViewHolder)holder;
        setViewBounds(directShareViewHolder.getRow(0), -1, directShareViewHolder.getMinRowHeight());
        setViewBounds(directShareViewHolder.getRow(1), -1, directShareViewHolder.getMinRowHeight());
      } 
      viewGroup.setTag(holder);
      return holder;
    }
    
    private void setViewBounds(View param1View, int param1Int1, int param1Int2) {
      ViewGroup.LayoutParams layoutParams = param1View.getLayoutParams();
      if (layoutParams == null) {
        layoutParams = new ViewGroup.LayoutParams(param1Int1, param1Int2);
        param1View.setLayoutParams(layoutParams);
      } else {
        layoutParams.height = param1Int2;
        layoutParams.width = param1Int1;
      } 
    }
    
    ChooserActivity.ItemGroupViewHolder createItemGroupViewHolder(int param1Int, ViewGroup param1ViewGroup) {
      ChooserActivity.DirectShareViewHolder directShareViewHolder;
      if (param1Int == 0) {
        ViewGroup viewGroup1 = (ViewGroup)this.mLayoutInflater.inflate(17367135, param1ViewGroup, false);
        param1ViewGroup = (ViewGroup)this.mLayoutInflater.inflate(17367134, viewGroup1, false);
        ViewGroup viewGroup2 = (ViewGroup)this.mLayoutInflater.inflate(17367134, viewGroup1, false);
        viewGroup1.addView(param1ViewGroup);
        viewGroup1.addView(viewGroup2);
        ChooserActivity chooserActivity1 = ChooserActivity.this;
        this.mDirectShareViewHolder = directShareViewHolder = new ChooserActivity.DirectShareViewHolder(viewGroup1, Lists.newArrayList((Object[])new ViewGroup[] { param1ViewGroup, viewGroup2 }, ), getMaxTargetsPerRow(), param1Int);
        loadViewsIntoGroup(directShareViewHolder);
        return this.mDirectShareViewHolder;
      } 
      ViewGroup viewGroup = (ViewGroup)this.mLayoutInflater.inflate(17367134, (ViewGroup)directShareViewHolder, false);
      ChooserActivity chooserActivity = ChooserActivity.this;
      ChooserActivity.SingleRowViewHolder singleRowViewHolder = new ChooserActivity.SingleRowViewHolder(viewGroup, getMaxTargetsPerRow(), param1Int);
      loadViewsIntoGroup(singleRowViewHolder);
      return singleRowViewHolder;
    }
    
    int getRowType(int param1Int) {
      param1Int = this.mChooserListAdapter.getPositionTargetType(param1Int);
      if (param1Int == 0)
        return 2; 
      if (getAzLabelRowCount() > 0 && param1Int == 3)
        return 2; 
      return param1Int;
    }
    
    void bindItemViewHolder(int param1Int, ChooserActivity.ItemViewHolder param1ItemViewHolder) {
      View view = param1ItemViewHolder.itemView;
      param1Int = getListPosition(param1Int);
      param1ItemViewHolder.mListPosition = param1Int;
      this.mChooserListAdapter.bindView(param1Int, view);
    }
    
    void bindItemGroupViewHolder(int param1Int, ChooserActivity.ItemGroupViewHolder param1ItemGroupViewHolder) {
      ViewGroup viewGroup = (ViewGroup)param1ItemGroupViewHolder.itemView;
      int i = getListPosition(param1Int);
      int j = getRowType(i);
      if (viewGroup.getForeground() == null && param1Int > 0) {
        ChooserActivity chooserActivity = ChooserActivity.this;
        Drawable drawable = chooserActivity.getResources().getDrawable(17302120, null);
        viewGroup.setForeground(drawable);
      } 
      int k = param1ItemGroupViewHolder.getColumnCount();
      param1Int = i + k - 1;
      while (getRowType(param1Int) != j && param1Int >= i)
        param1Int--; 
      if (param1Int == i && this.mChooserListAdapter.getItem(i) instanceof ChooserActivity.EmptyTargetInfo) {
        TextView textView = viewGroup.<TextView>findViewById(16908841);
        if (textView.getVisibility() != 0) {
          textView.setAlpha(0.0F);
          textView.setVisibility(0);
          textView.setText(17039820);
          ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(textView, "alpha", new float[] { 0.0F, 1.0F });
          objectAnimator1.setInterpolator(new DecelerateInterpolator(1.0F));
          float f = ChooserActivity.this.getResources().getDimensionPixelSize(17105044);
          textView.setTranslationY(f);
          ObjectAnimator objectAnimator2 = ObjectAnimator.ofFloat(textView, "translationY", new float[] { 0.0F });
          objectAnimator2.setInterpolator(new DecelerateInterpolator(1.0F));
          AnimatorSet animatorSet = new AnimatorSet();
          animatorSet.setDuration(200L);
          animatorSet.setStartDelay(200L);
          animatorSet.playTogether(new Animator[] { (Animator)objectAnimator1, (Animator)objectAnimator2 });
          animatorSet.start();
        } 
      } 
      for (j = 0; j < k; j++) {
        View view = param1ItemGroupViewHolder.getView(j);
        if (i + j <= param1Int) {
          param1ItemGroupViewHolder.setViewVisibility(j, 0);
          param1ItemGroupViewHolder.setItemIndex(j, i + j);
          this.mChooserListAdapter.bindView(param1ItemGroupViewHolder.getItemIndex(j), view);
        } else {
          param1ItemGroupViewHolder.setViewVisibility(j, 4);
        } 
      } 
    }
    
    int getListPosition(int param1Int) {
      int i = param1Int - getContentPreviewRowCount() + getProfileRowCount();
      param1Int = this.mChooserListAdapter.getServiceTargetCount();
      int j = (int)Math.ceil((param1Int / 8.0F));
      if (i < j)
        return getMaxTargetsPerRow() * i; 
      int k = i - j;
      i = this.mChooserListAdapter.getCallerTargetCount();
      ChooserListAdapter chooserListAdapter = this.mChooserListAdapter;
      int m = chooserListAdapter.getRankedTargetCount();
      int n = getCallerAndRankedTargetRowCount();
      if (k < n)
        return getMaxTargetsPerRow() * k + param1Int; 
      j = getAzLabelRowCount();
      return i + m + param1Int + k - j + n;
    }
    
    public void handleScroll(View param1View, int param1Int1, int param1Int2) {
      boolean bool = canExpandDirectShare();
      ChooserActivity.DirectShareViewHolder directShareViewHolder = this.mDirectShareViewHolder;
      if (directShareViewHolder != null && bool) {
        ChooserMultiProfilePagerAdapter chooserMultiProfilePagerAdapter = ChooserActivity.this.mChooserMultiProfilePagerAdapter;
        RecyclerView recyclerView = chooserMultiProfilePagerAdapter.getActiveAdapterView();
        int i = getMaxTargetsPerRow();
        directShareViewHolder.handleScroll(recyclerView, param1Int1, param1Int2, i);
      } 
    }
    
    private boolean canExpandDirectShare() {
      return false;
    }
    
    public ChooserListAdapter getListAdapter() {
      return this.mChooserListAdapter;
    }
    
    boolean shouldCellSpan(int param1Int) {
      param1Int = getItemViewType(param1Int);
      boolean bool = true;
      if (param1Int != 1)
        bool = false; 
      return bool;
    }
    
    void updateDirectShareExpansion() {
      if (this.mDirectShareViewHolder == null || !canExpandDirectShare())
        return; 
      ChooserMultiProfilePagerAdapter chooserMultiProfilePagerAdapter = ChooserActivity.this.mChooserMultiProfilePagerAdapter;
      RecyclerView recyclerView = chooserMultiProfilePagerAdapter.getActiveAdapterView();
      if (ChooserActivity.this.mResolverDrawerLayout.isCollapsed()) {
        this.mDirectShareViewHolder.collapse(recyclerView);
      } else {
        this.mDirectShareViewHolder.expand(recyclerView);
      } 
    }
  }
  
  class ItemGroupViewHolder extends ViewHolderBase {
    protected final View[] mCells;
    
    private final int mColumnCount;
    
    private int[] mItemIndices;
    
    protected int mMeasuredRowHeight;
    
    final ChooserActivity this$0;
    
    ItemGroupViewHolder(int param1Int1, View param1View, int param1Int2) {
      super(param1View, param1Int2);
      this.mCells = new View[param1Int1];
      this.mItemIndices = new int[param1Int1];
      this.mColumnCount = param1Int1;
    }
    
    public int getColumnCount() {
      return this.mColumnCount;
    }
    
    public void measure() {
      int i = View.MeasureSpec.makeMeasureSpec(0, 0);
      getViewGroup().measure(i, i);
      this.mMeasuredRowHeight = getViewGroup().getMeasuredHeight();
    }
    
    public int getMeasuredRowHeight() {
      return this.mMeasuredRowHeight;
    }
    
    public void setItemIndex(int param1Int1, int param1Int2) {
      this.mItemIndices[param1Int1] = param1Int2;
    }
    
    public int getItemIndex(int param1Int) {
      return this.mItemIndices[param1Int];
    }
    
    public View getView(int param1Int) {
      return this.mCells[param1Int];
    }
    
    abstract ViewGroup addView(int param1Int, View param1View);
    
    abstract ViewGroup getRow(int param1Int);
    
    abstract ViewGroup getRowByIndex(int param1Int);
    
    abstract ViewGroup getViewGroup();
    
    abstract void setViewVisibility(int param1Int1, int param1Int2);
  }
  
  class SingleRowViewHolder extends ItemGroupViewHolder {
    private final ViewGroup mRow;
    
    final ChooserActivity this$0;
    
    SingleRowViewHolder(ViewGroup param1ViewGroup, int param1Int1, int param1Int2) {
      super(param1Int1, param1ViewGroup, param1Int2);
      this.mRow = param1ViewGroup;
    }
    
    public ViewGroup getViewGroup() {
      return this.mRow;
    }
    
    public ViewGroup getRowByIndex(int param1Int) {
      return this.mRow;
    }
    
    public ViewGroup getRow(int param1Int) {
      if (param1Int == 0)
        return this.mRow; 
      return null;
    }
    
    public ViewGroup addView(int param1Int, View param1View) {
      this.mRow.addView(param1View);
      this.mCells[param1Int] = param1View;
      return this.mRow;
    }
    
    public void setViewVisibility(int param1Int1, int param1Int2) {
      getView(param1Int1).setVisibility(param1Int2);
    }
  }
  
  class DirectShareViewHolder extends ItemGroupViewHolder {
    private boolean mHideDirectShareExpansion = false;
    
    private int mDirectShareMinHeight = 0;
    
    private int mDirectShareCurrHeight = 0;
    
    private int mDirectShareMaxHeight = 0;
    
    private int mCellCountPerRow;
    
    private final boolean[] mCellVisibility;
    
    private final ViewGroup mParent;
    
    private final List<ViewGroup> mRows;
    
    final ChooserActivity this$0;
    
    DirectShareViewHolder(ViewGroup param1ViewGroup, List<ViewGroup> param1List, int param1Int1, int param1Int2) {
      super(param1List.size() * param1Int1, param1ViewGroup, param1Int2);
      this.mParent = param1ViewGroup;
      this.mRows = param1List;
      this.mCellCountPerRow = param1Int1;
      this.mCellVisibility = new boolean[param1List.size() * param1Int1];
    }
    
    public ViewGroup addView(int param1Int, View param1View) {
      ViewGroup viewGroup = getRowByIndex(param1Int);
      viewGroup.addView(param1View);
      this.mCells[param1Int] = param1View;
      return viewGroup;
    }
    
    public ViewGroup getViewGroup() {
      return this.mParent;
    }
    
    public ViewGroup getRowByIndex(int param1Int) {
      return this.mRows.get(param1Int / this.mCellCountPerRow);
    }
    
    public ViewGroup getRow(int param1Int) {
      return this.mRows.get(param1Int);
    }
    
    public void measure() {
      int i = View.MeasureSpec.makeMeasureSpec(0, 0);
      getRow(0).measure(i, i);
      getRow(1).measure(i, i);
      int j = getRow(0).getMeasuredHeight();
      i = this.mDirectShareCurrHeight;
      if (i <= 0)
        i = j; 
      this.mDirectShareCurrHeight = i;
      this.mDirectShareMaxHeight = this.mDirectShareMinHeight * 2;
    }
    
    public int getMeasuredRowHeight() {
      return this.mDirectShareCurrHeight;
    }
    
    public int getMinRowHeight() {
      return this.mDirectShareMinHeight;
    }
    
    public void setViewVisibility(int param1Int1, int param1Int2) {
      View view = getView(param1Int1);
      if (param1Int2 == 0) {
        this.mCellVisibility[param1Int1] = true;
        view.setVisibility(param1Int2);
        view.setAlpha(1.0F);
      } else if (param1Int2 == 4) {
        boolean[] arrayOfBoolean = this.mCellVisibility;
        if (arrayOfBoolean[param1Int1]) {
          arrayOfBoolean[param1Int1] = false;
          ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(view, "alpha", new float[] { 1.0F, 0.0F });
          objectAnimator.setDuration(200L);
          objectAnimator.setInterpolator(new AccelerateInterpolator(1.0F));
          objectAnimator.addListener((Animator.AnimatorListener)new Object(this, view));
          objectAnimator.start();
        } 
      } 
    }
    
    public void handleScroll(RecyclerView param1RecyclerView, int param1Int1, int param1Int2, int param1Int3) {
      int i;
      if (this.mDirectShareCurrHeight == this.mDirectShareMinHeight) {
        i = 1;
      } else {
        i = 0;
      } 
      if (i) {
        if (this.mHideDirectShareExpansion)
          return; 
        ChooserMultiProfilePagerAdapter chooserMultiProfilePagerAdapter = ChooserActivity.this.mChooserMultiProfilePagerAdapter;
        ChooserListAdapter chooserListAdapter = chooserMultiProfilePagerAdapter.getActiveListAdapter();
        if (ChooserActivity.this.mAppendDirectShareEnabled) {
          i = chooserListAdapter.getNumServiceTargetsForExpand();
        } else {
          i = chooserListAdapter.getSelectableServiceTargetCount();
        } 
        if (i <= param1Int3) {
          this.mHideDirectShareExpansion = true;
          return;
        } 
      } 
      param1Int2 = (int)((param1Int2 - param1Int1) * 0.78F);
      param1Int1 = this.mDirectShareCurrHeight;
      param1Int2 = Math.min(param1Int1 + param1Int2, this.mDirectShareMaxHeight);
      param1Int2 = Math.max(param1Int2, this.mDirectShareMinHeight);
      updateDirectShareRowHeight(param1RecyclerView, param1Int2 - param1Int1, param1Int2);
    }
    
    void expand(RecyclerView param1RecyclerView) {
      int i = this.mDirectShareMaxHeight;
      updateDirectShareRowHeight(param1RecyclerView, i - this.mDirectShareCurrHeight, i);
    }
    
    void collapse(RecyclerView param1RecyclerView) {
      int i = this.mDirectShareMinHeight;
      updateDirectShareRowHeight(param1RecyclerView, i - this.mDirectShareCurrHeight, i);
    }
    
    private void updateDirectShareRowHeight(RecyclerView param1RecyclerView, int param1Int1, int param1Int2) {
      if (param1RecyclerView == null || param1RecyclerView.getChildCount() == 0 || param1Int1 == 0)
        return; 
      int i = 0;
      for (byte b = 0; b < param1RecyclerView.getChildCount(); b++, i = j) {
        int j;
        View view = param1RecyclerView.getChildAt(b);
        if (i) {
          view.offsetTopAndBottom(param1Int1);
          j = i;
        } else {
          j = i;
          if (view.getTag() != null) {
            j = i;
            if (view.getTag() instanceof DirectShareViewHolder) {
              i = View.MeasureSpec.makeMeasureSpec(view.getWidth(), 1073741824);
              j = View.MeasureSpec.makeMeasureSpec(param1Int2, 1073741824);
              view.measure(i, j);
              (view.getLayoutParams()).height = view.getMeasuredHeight();
              int k = view.getLeft();
              i = view.getTop();
              int m = view.getRight();
              j = view.getTop();
              int n = view.getMeasuredHeight();
              view.layout(k, i, m, j + n);
              j = 1;
            } 
          } 
        } 
      } 
      if (i != 0)
        this.mDirectShareCurrHeight = param1Int2; 
    }
  }
  
  class ChooserTargetServiceConnection implements ServiceConnection {
    private final UserHandle mUserHandle;
    
    private DisplayResolveInfo mOriginalTarget;
    
    private final Object mLock = new Object();
    
    private ComponentName mConnectedComponent;
    
    private final IChooserTargetResult mChooserTargetResult = (IChooserTargetResult)new Object(this);
    
    private ChooserActivity mChooserActivity;
    
    public ChooserTargetServiceConnection(ChooserActivity this$0, DisplayResolveInfo param1DisplayResolveInfo, UserHandle param1UserHandle) {
      this.mChooserActivity = this$0;
      this.mOriginalTarget = param1DisplayResolveInfo;
      this.mUserHandle = param1UserHandle;
    }
    
    public void onServiceConnected(ComponentName param1ComponentName, IBinder param1IBinder) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onServiceConnected: ");
      stringBuilder.append(param1ComponentName);
      Log.d("ChooserActivity", stringBuilder.toString());
      synchronized (this.mLock) {
        if (this.mChooserActivity == null) {
          Log.e("ChooserActivity", "destroyed ChooserTargetServiceConnection got onServiceConnected");
          return;
        } 
        IChooserTargetService iChooserTargetService = IChooserTargetService.Stub.asInterface(param1IBinder);
        try {
          ComponentName componentName = this.mOriginalTarget.getResolvedComponentName();
          DisplayResolveInfo displayResolveInfo = this.mOriginalTarget;
          IntentFilter intentFilter = (displayResolveInfo.getResolveInfo()).filter;
          IChooserTargetResult iChooserTargetResult = this.mChooserTargetResult;
          iChooserTargetService.getChooserTargets(componentName, intentFilter, iChooserTargetResult);
        } catch (RemoteException remoteException) {
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("Querying ChooserTargetService ");
          stringBuilder1.append(param1ComponentName);
          stringBuilder1.append(" failed.");
          Log.e("ChooserActivity", stringBuilder1.toString(), (Throwable)remoteException);
          this.mChooserActivity.unbindService(this);
          this.mChooserActivity.mServiceConnections.remove(this);
          destroy();
        } 
        return;
      } 
    }
    
    public void onServiceDisconnected(ComponentName param1ComponentName) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onServiceDisconnected: ");
      stringBuilder.append(param1ComponentName);
      Log.d("ChooserActivity", stringBuilder.toString());
      synchronized (this.mLock) {
        if (this.mChooserActivity == null) {
          Log.e("ChooserActivity", "destroyed ChooserTargetServiceConnection got onServiceDisconnected");
          return;
        } 
        this.mChooserActivity.unbindService(this);
        this.mChooserActivity.mServiceConnections.remove(this);
        if (this.mChooserActivity.mServiceConnections.isEmpty())
          this.mChooserActivity.sendVoiceChoicesIfNeeded(); 
        this.mConnectedComponent = null;
        destroy();
        return;
      } 
    }
    
    public void destroy() {
      synchronized (this.mLock) {
        this.mChooserActivity = null;
        this.mOriginalTarget = null;
        return;
      } 
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ChooserTargetServiceConnection{service=");
      stringBuilder.append(this.mConnectedComponent);
      stringBuilder.append(", activity=");
      DisplayResolveInfo displayResolveInfo = this.mOriginalTarget;
      if (displayResolveInfo != null) {
        null = (displayResolveInfo.getResolveInfo()).activityInfo.toString();
      } else {
        null = "<connection destroyed>";
      } 
      stringBuilder.append(null);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public ComponentName getComponentName() {
      return (this.mOriginalTarget.getResolveInfo()).activityInfo.getComponentName();
    }
  }
  
  class ServiceResultInfo {
    public final ChooserActivity.ChooserTargetServiceConnection connection;
    
    public final DisplayResolveInfo originalTarget;
    
    public final List<ChooserTarget> resultTargets;
    
    public final UserHandle userHandle;
    
    public ServiceResultInfo(ChooserActivity this$0, List<ChooserTarget> param1List, ChooserActivity.ChooserTargetServiceConnection param1ChooserTargetServiceConnection, UserHandle param1UserHandle) {
      this.originalTarget = (DisplayResolveInfo)this$0;
      this.resultTargets = param1List;
      this.connection = param1ChooserTargetServiceConnection;
      this.userHandle = param1UserHandle;
    }
  }
  
  class ChooserTargetRankingInfo {
    public final List<AppTarget> scores;
    
    public final UserHandle userHandle;
    
    ChooserTargetRankingInfo(ChooserActivity this$0, UserHandle param1UserHandle) {
      this.scores = (List<AppTarget>)this$0;
      this.userHandle = param1UserHandle;
    }
  }
  
  class RefinementResultReceiver extends ResultReceiver {
    private ChooserActivity mChooserActivity;
    
    private TargetInfo mSelectedTarget;
    
    public RefinementResultReceiver(ChooserActivity this$0, TargetInfo param1TargetInfo, Handler param1Handler) {
      super(param1Handler);
      this.mChooserActivity = this$0;
      this.mSelectedTarget = param1TargetInfo;
    }
    
    protected void onReceiveResult(int param1Int, Bundle param1Bundle) {
      StringBuilder stringBuilder;
      ChooserActivity chooserActivity = this.mChooserActivity;
      if (chooserActivity == null) {
        Log.e("ChooserActivity", "Destroyed RefinementResultReceiver received a result");
        return;
      } 
      if (param1Bundle == null) {
        Log.e("ChooserActivity", "RefinementResultReceiver received null resultData");
        return;
      } 
      if (param1Int != -1) {
        if (param1Int != 0) {
          stringBuilder = new StringBuilder();
          stringBuilder.append("Unknown result code ");
          stringBuilder.append(param1Int);
          stringBuilder.append(" sent to RefinementResultReceiver");
          Log.w("ChooserActivity", stringBuilder.toString());
        } else {
          chooserActivity.onRefinementCanceled();
        } 
      } else {
        Parcelable parcelable = stringBuilder.getParcelable("android.intent.extra.INTENT");
        if (parcelable instanceof Intent) {
          this.mChooserActivity.onRefinementResult(this.mSelectedTarget, (Intent)parcelable);
        } else {
          Log.e("ChooserActivity", "RefinementResultReceiver received RESULT_OK but no Intent in resultData with key Intent.EXTRA_INTENT");
        } 
      } 
    }
    
    public void destroy() {
      this.mChooserActivity = null;
      this.mSelectedTarget = null;
    }
  }
  
  class RoundedRectImageView extends ImageView {
    private int mRadius = 0;
    
    private Path mPath = new Path();
    
    private Paint mOverlayPaint = new Paint(0);
    
    private Paint mRoundRectPaint = new Paint(0);
    
    private Paint mTextPaint = new Paint(1);
    
    private String mExtraImageCount = null;
    
    public RoundedRectImageView(ChooserActivity this$0) {
      super((Context)this$0);
    }
    
    public RoundedRectImageView(AttributeSet param1AttributeSet) {
      this((Context)this$0, param1AttributeSet, 0);
    }
    
    public RoundedRectImageView(AttributeSet param1AttributeSet, int param1Int) {
      this((Context)this$0, param1AttributeSet, param1Int, 0);
    }
    
    public RoundedRectImageView(AttributeSet param1AttributeSet, int param1Int1, int param1Int2) {
      super((Context)this$0, param1AttributeSet, param1Int1, param1Int2);
      this.mRadius = this$0.getResources().getDimensionPixelSize(17105032);
      this.mOverlayPaint.setColor(-1728053248);
      this.mOverlayPaint.setStyle(Paint.Style.FILL);
      this.mRoundRectPaint.setColor(this$0.getResources().getColor(17170725));
      this.mRoundRectPaint.setStyle(Paint.Style.STROKE);
      Paint paint = this.mRoundRectPaint;
      Resources resources2 = this$0.getResources();
      float f = resources2.getDimensionPixelSize(17105040);
      paint.setStrokeWidth(f);
      this.mTextPaint.setColor(-1);
      paint = this.mTextPaint;
      Resources resources1 = this$0.getResources();
      f = resources1.getDimensionPixelSize(17105041);
      paint.setTextSize(f);
      if (this.mContext instanceof OplusBaseResolverActivity)
        ((OplusBaseResolverActivity)this.mContext).setCustomRoundImage(this.mRoundRectPaint, this.mTextPaint, this.mOverlayPaint); 
      this.mRoundRectPaint.setAntiAlias(true);
      this.mTextPaint.setTextAlign(Paint.Align.CENTER);
    }
    
    private void updatePath(int param1Int1, int param1Int2) {
      this.mPath.reset();
      int i = getPaddingRight(), j = getPaddingLeft();
      int k = getPaddingBottom(), m = getPaddingTop();
      Path path = this.mPath;
      float f1 = getPaddingLeft(), f2 = getPaddingTop(), f3 = (param1Int1 - i - j), f4 = (param1Int2 - k - m);
      param1Int1 = this.mRadius;
      path.addRoundRect(f1, f2, f3, f4, param1Int1, param1Int1, Path.Direction.CW);
    }
    
    public void setRadius(int param1Int) {
      this.mRadius = param1Int;
      updatePath(getWidth(), getHeight());
    }
    
    public void setExtraImageCount(int param1Int) {
      if (param1Int > 0) {
        if (getContext() instanceof OplusBaseResolverActivity && !((OplusBaseResolverActivity)getContext()).isOriginUi()) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(" + ");
          stringBuilder.append(param1Int);
          this.mExtraImageCount = stringBuilder.toString();
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("+");
          stringBuilder.append(param1Int);
          this.mExtraImageCount = stringBuilder.toString();
        } 
      } else {
        this.mExtraImageCount = null;
      } 
    }
    
    protected void onSizeChanged(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      super.onSizeChanged(param1Int1, param1Int2, param1Int3, param1Int4);
      updatePath(param1Int1, param1Int2);
    }
    
    protected void onDraw(Canvas param1Canvas) {
      if (this.mRadius != 0)
        param1Canvas.clipPath(this.mPath); 
      super.onDraw(param1Canvas);
      int i = getPaddingLeft();
      int j = getPaddingRight();
      int k = getWidth() - getPaddingRight() - getPaddingLeft();
      int m = getHeight() - getPaddingBottom() - getPaddingTop();
      if (this.mExtraImageCount != null) {
        param1Canvas.drawRect(i, j, k, m, this.mOverlayPaint);
        int n = param1Canvas.getWidth() / 2;
        float f = param1Canvas.getHeight() / 2.0F;
        Paint paint = this.mTextPaint;
        int i1 = (int)(f - (paint.descent() + this.mTextPaint.ascent()) / 2.0F);
        param1Canvas.drawText(this.mExtraImageCount, n, i1, this.mTextPaint);
      } 
      float f1 = i, f2 = j, f3 = k, f4 = m;
      i = this.mRadius;
      param1Canvas.drawRoundRect(f1, f2, f3, f4, i, i, this.mRoundRectPaint);
    }
  }
  
  protected void maybeLogProfileChange() {
    getChooserActivityLogger().logShareheetProfileChanged();
  }
  
  protected ViewGroup createContentPreviewViewWrapper(ViewGroup paramViewGroup) {
    return createContentPreviewView(paramViewGroup);
  }
  
  protected TargetInfo getNearbySharingTargetWrapper(Intent paramIntent) {
    return getNearbySharingTarget(paramIntent);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class ContentPreviewType implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class ShareTargetType implements Annotation {}
}
