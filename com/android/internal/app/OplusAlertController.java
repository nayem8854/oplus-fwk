package com.android.internal.app;

import android.content.ComponentCallbacks;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Point;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.provider.Settings;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManagerGlobal;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import com.oplus.util.OplusTypeCastingHelper;
import com.oplus.widget.OplusAlertLinearLayout;
import java.lang.ref.WeakReference;

public class OplusAlertController extends AlertController {
  private ContentObserver mObserver = new ContentObserver(this.mHandler) {
      final OplusAlertController this$0;
      
      public void onChange(boolean param1Boolean) {
        OplusAlertController.this.mHandler.sendEmptyMessage(1);
      }
    };
  
  private boolean mIsValidateNavigationBar;
  
  private Handler mHandler;
  
  private Context mContext;
  
  private ComponentCallbacks mComponentCallbacks = new ComponentCallbacks() {
      final OplusAlertController this$0;
      
      public void onConfigurationChanged(Configuration param1Configuration) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("configurationChangedThread is ");
        stringBuilder.append(Thread.currentThread());
        Log.d("ColorAlertController", stringBuilder.toString());
        OplusAlertController.this.mHandler.sendEmptyMessage(2);
      }
      
      public void onLowMemory() {}
    };
  
  private ViewStub mButtonPanelStub;
  
  private TextPaint mButtonPaint;
  
  private static final String TAG = "ColorAlertController";
  
  private static final int FULL_SCREEN_FLAG = -2147482112;
  
  private static final int DEFAULT_DISPALY_ID = 0;
  
  private void onConfigurationChanged() {
    updateWindowAttributes();
    updateSpaceHeight();
    updateBg();
  }
  
  public OplusAlertController(Context paramContext, DialogInterface paramDialogInterface, Window paramWindow) {
    super(paramContext, paramDialogInterface, paramWindow);
    this.mContext = paramContext;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Handler Thread = ");
    stringBuilder.append(Thread.currentThread());
    Log.d("ColorAlertController", stringBuilder.toString());
    this.mHandler = new BottomSpaceHandler(this);
    initButtonPaint();
  }
  
  private void initButtonPaint() {
    TextPaint textPaint = new TextPaint();
    textPaint.setTextSize(this.mContext.getResources().getDimensionPixelSize(201654648));
  }
  
  public void installContent() {
    int i = selectContentViewInner();
    this.mWindow.setContentView(i);
    setupViewInner();
  }
  
  protected int selectContentViewInner() {
    if (isCenterDialog()) {
      OplusBaseAlertController oplusBaseAlertController = (OplusBaseAlertController)OplusTypeCastingHelper.typeCasting(OplusBaseAlertController.class, this);
      if (oplusBaseAlertController != null)
        return oplusBaseAlertController.selectContentViewWrapper(); 
    } 
    return 202440712;
  }
  
  private boolean isCenterDialog() {
    boolean bool;
    OplusBaseAlertController oplusBaseAlertController = (OplusBaseAlertController)OplusTypeCastingHelper.typeCasting(OplusBaseAlertController.class, this);
    if (oplusBaseAlertController != null && oplusBaseAlertController.getDialogType() == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected void setupViewInner() {
    updateWindowAttributes();
    setupAnimationAndGravity();
    addBottomSpace();
    updateBg();
    ListView listView = getListView();
    if (listView instanceof OplusRecyclerListView) {
      OplusRecyclerListView oplusRecyclerListView = (OplusRecyclerListView)listView;
      oplusRecyclerListView.setNeedClip(needClipListView());
    } 
    this.mButtonPanelStub = (ViewStub)this.mWindow.findViewById(201457682);
    if (isCenterDialog()) {
      if (needSetButtonsVertical()) {
        setButtonsVertical();
      } else {
        setButtonsHorizontal();
      } 
    } else {
      setButtonsVertical();
    } 
    OplusBaseAlertController oplusBaseAlertController = (OplusBaseAlertController)OplusTypeCastingHelper.typeCasting(OplusBaseAlertController.class, this);
    if (oplusBaseAlertController != null)
      oplusBaseAlertController.setupViewWrapper(); 
  }
  
  protected void setupContent(final ViewGroup contentPanel) {
    super.setupContent(contentPanel);
    ViewGroup viewGroup = (ViewGroup)contentPanel.findViewById(201457746);
    if (this.mMessage != null && viewGroup != null && this.mListView != null)
      viewGroup.addView((View)this.mListView, new ViewGroup.LayoutParams(-1, -1)); 
    if (isCenterDialog()) {
      if (this.mMessage != null)
        relayoutMessageView(contentPanel); 
    } else {
      relayoutListAndMessage(viewGroup);
    } 
    contentPanel.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
          final OplusAlertController this$0;
          
          final ViewGroup val$contentPanel;
          
          public void onViewAttachedToWindow(View param1View) {}
          
          public void onViewDetachedFromWindow(View param1View) {
            contentPanel.removeOnAttachStateChangeListener(this);
            if (OplusAlertController.this.mComponentCallbacks != null) {
              OplusAlertController.this.mContext.unregisterComponentCallbacks(OplusAlertController.this.mComponentCallbacks);
              OplusAlertController.access$102(OplusAlertController.this, (ComponentCallbacks)null);
            } 
            OplusAlertController.this.mContext.getContentResolver().unregisterContentObserver(OplusAlertController.this.mObserver);
          }
        });
  }
  
  protected void setupButtons(ViewGroup paramViewGroup) {
    super.setupButtons(paramViewGroup);
    resetButtonsPadding();
    setButtonsBackground();
  }
  
  private void relayoutListAndMessage(ViewGroup paramViewGroup) {
    OplusBaseAlertController oplusBaseAlertController = (OplusBaseAlertController)OplusTypeCastingHelper.typeCasting(OplusBaseAlertController.class, this);
    if (oplusBaseAlertController != null && oplusBaseAlertController.isMessageNeedScroll()) {
      if (this.mScrollView != null) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)this.mScrollView.getLayoutParams();
        layoutParams.height = 0;
        layoutParams.weight = 1.0F;
        this.mScrollView.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
      } 
      if (paramViewGroup != null) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)paramViewGroup.getLayoutParams();
        layoutParams.height = 0;
        layoutParams.weight = 1.0F;
        paramViewGroup.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
      } 
    } 
  }
  
  private void relayoutMessageView(ViewGroup paramViewGroup) {
    final TextView messageView = (TextView)paramViewGroup.findViewById(16908299);
    textView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
          final OplusAlertController this$0;
          
          final TextView val$messageView;
          
          public void onGlobalLayout() {
            int i = messageView.getLineCount();
            if (i > 1) {
              messageView.setTextAlignment(2);
            } else {
              messageView.setTextAlignment(4);
            } 
            TextView textView = messageView;
            textView.setText(textView.getText());
            messageView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
          }
        });
  }
  
  private void updateWindowAttributes() {
    boolean bool;
    Point point = getScreenSize();
    if (point.x < point.y) {
      bool = true;
    } else {
      bool = false;
    } 
    DisplayMetrics displayMetrics = this.mContext.getResources().getDisplayMetrics();
    WindowManager.LayoutParams layoutParams = this.mWindow.getAttributes();
    if (bool) {
      this.mWindow.setGravity(81);
      this.mWindow.clearFlags(-2147482112);
      layoutParams.width = Math.min(point.x, displayMetrics.widthPixels);
      layoutParams.height = -2;
    } else {
      this.mWindow.setGravity(17);
      this.mWindow.addFlags(-2147482112);
      layoutParams.width = Math.min(point.y, displayMetrics.widthPixels);
      layoutParams.height = this.mContext.getResources().getDimensionPixelSize(201654371);
    } 
  }
  
  private void setupAnimationAndGravity() {
    WindowManager.LayoutParams layoutParams = this.mWindow.getAttributes();
    if (isCenterDialog()) {
      layoutParams.windowAnimations = 201523236;
      layoutParams.gravity = 17;
    } else {
      layoutParams.windowAnimations = 201523235;
    } 
    this.mWindow.setAttributes(layoutParams);
  }
  
  private void addBottomSpace() {
    if (!isCenterDialog()) {
      observeHideNavigationBar();
      this.mContext.registerComponentCallbacks(this.mComponentCallbacks);
    } 
    if (needAddBottomView()) {
      updateSpaceHeight();
      updateWindowFlag();
      WindowManager.LayoutParams layoutParams = this.mWindow.getAttributes();
      addPrivateFlag(layoutParams);
      if (isSystemDialog(layoutParams))
        layoutParams.y -= spaceHeight(); 
      this.mWindow.setAttributes(layoutParams);
    } 
  }
  
  private void observeHideNavigationBar() {
    ContentResolver contentResolver = this.mContext.getContentResolver();
    contentResolver.registerContentObserver(Settings.Secure.getUriFor("manual_hide_navigationbar"), false, this.mObserver);
  }
  
  private int spaceHeight() {
    boolean bool;
    if (!isFullScreen())
      return 0; 
    if (isGravityCenter()) {
      bool = false;
    } else if (isNavigationBarShow()) {
      bool = navigationBarHeight();
    } else {
      bool = this.mContext.getResources().getDimensionPixelSize(201654378);
    } 
    if (!this.mIsValidateNavigationBar)
      bool = false; 
    return bool;
  }
  
  private int navigationBarHeight() {
    Resources resources = this.mContext.getResources();
    int i = resources.getIdentifier("navigation_bar_height", "dimen", "android");
    return resources.getDimensionPixelSize(i);
  }
  
  private boolean isNavigationBarShow() {
    null = supportNavigationBar();
    boolean bool = false;
    if (!null)
      return false; 
    int i = Settings.Secure.getInt(this.mContext.getContentResolver(), "hide_navigationbar_enable", 0);
    int j = Settings.Secure.getInt(this.mContext.getContentResolver(), "manual_hide_navigationbar", 0);
    if (i != -1 && j != -1) {
      null = true;
    } else {
      null = false;
    } 
    this.mIsValidateNavigationBar = null;
    if (i != 0) {
      null = bool;
      if (i == 1) {
        null = bool;
        if (j == 0)
          return true; 
      } 
      return null;
    } 
    return true;
  }
  
  private boolean supportNavigationBar() {
    try {
      return WindowManagerGlobal.getWindowManagerService().hasNavigationBar(0);
    } catch (RemoteException remoteException) {
      Log.d("ColorAlertController", "fail to get navigationBar's status, return false");
      return false;
    } 
  }
  
  private boolean isGravityCenter() {
    boolean bool;
    if ((this.mWindow.getAttributes()).gravity == 17) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean needAddBottomView() {
    boolean bool;
    if (!isCenterDialog() && 
      isFullScreen()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isFullScreen() {
    boolean bool = false;
    try {
      int i = WindowManagerGlobal.getWindowManagerService().getDockedStackSide();
      if (i == -1)
        bool = true; 
      return bool;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isFullScreen operation failed.Return false.Failed msg is ");
      stringBuilder.append(exception.getMessage());
      Log.d("ColorAlertController", stringBuilder.toString());
      return false;
    } 
  }
  
  private boolean isPortrait() {
    boolean bool;
    int i = (getScreenSize()).x;
    int j = (getScreenSize()).y;
    if (i < j) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private Point getScreenSize() {
    Point point = new Point();
    WindowManager windowManager = (WindowManager)this.mContext.getSystemService("window");
    windowManager.getDefaultDisplay().getRealSize(point);
    return point;
  }
  
  private void updateWindowFlag() {
    if (isGravityCenter()) {
      this.mWindow.clearFlags(-2147482112);
    } else if (isNavigationBarShow()) {
      this.mWindow.setNavigationBarColor(-1);
      this.mWindow.clearFlags(134217728);
      this.mWindow.getDecorView().setSystemUiVisibility(16);
      this.mWindow.addFlags(-2147482112);
    } 
  }
  
  private void addPrivateFlag(WindowManager.LayoutParams paramLayoutParams) {
    paramLayoutParams.privateFlags |= 0x1000000;
    paramLayoutParams.privateFlags |= 0x40;
  }
  
  private void updateBg() {
    View view = this.mWindow.findViewById(16909279);
    if (view != null && 
      view instanceof OplusAlertLinearLayout) {
      OplusAlertLinearLayout oplusAlertLinearLayout = (OplusAlertLinearLayout)view;
      int i = (this.mWindow.getAttributes()).gravity;
      if (i == 17) {
        oplusAlertLinearLayout.setNeedClip(true);
        oplusAlertLinearLayout.setHasShadow(true);
      } else {
        oplusAlertLinearLayout.setNeedClip(false);
        oplusAlertLinearLayout.setHasShadow(false);
      } 
    } 
  }
  
  private boolean needClipListView() {
    boolean bool;
    if (!hasMessage() && !hasTitle() && !isCenterDialog()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void resetButtonsPadding() {
    OplusBaseAlertController oplusBaseAlertController = (OplusBaseAlertController)OplusTypeCastingHelper.typeCasting(OplusBaseAlertController.class, this);
    if (oplusBaseAlertController != null) {
      int i = oplusBaseAlertController.getAlertControllerButtonNeutral().getPaddingLeft();
      int j = oplusBaseAlertController.getAlertControllerButtonNeutral().getPaddingTop();
      int k = oplusBaseAlertController.getAlertControllerButtonNeutral().getPaddingRight();
      int m = oplusBaseAlertController.getAlertControllerButtonNeutral().getPaddingBottom();
      int n = this.mContext.getResources().getDimensionPixelSize(201654372);
      if (!isCenterDialog()) {
        int i1 = this.mContext.getResources().getColor(201720081);
        oplusBaseAlertController.getAlertControllerButtonPositive().setTextColor(i1);
        oplusBaseAlertController.getAlertControllerButtonNegative().setTextColor(i1);
        boolean bool1 = TextUtils.isEmpty(this.mMessage);
        boolean bool2 = TextUtils.isEmpty(oplusBaseAlertController.getAlertControllerTitle());
        oplusBaseAlertController.getAlertControllerButtonNeutral().setPadding(i, j, k, m + n);
        oplusBaseAlertController.getAlertControllerButtonNeutral().setMinHeight(oplusBaseAlertController.getAlertControllerButtonNeutral().getMinHeight() + n);
        if ((bool1 ^ true) == 0 && (bool2 ^ true) == 0 && 
          hasNeutralText() && !hasPositiveText()) {
          oplusBaseAlertController.getAlertControllerButtonNeutral().setPadding(i, j + n, k, m);
          oplusBaseAlertController.getAlertControllerButtonNeutral().setMinHeight(oplusBaseAlertController.getAlertControllerButtonNeutral().getMinHeight() + n);
        } 
      } 
      if (needSetButtonsVertical()) {
        oplusBaseAlertController.getAlertControllerButtonPositive().setPadding(i, j + n, k, m);
        oplusBaseAlertController.getAlertControllerButtonPositive().setMinHeight(oplusBaseAlertController.getAlertControllerButtonPositive().getMinHeight() + n);
        oplusBaseAlertController.getAlertControllerButtonNegative().setPadding(i, j, k, m + n);
        oplusBaseAlertController.getAlertControllerButtonNegative().setMinHeight(oplusBaseAlertController.getAlertControllerButtonNegative().getMinHeight() + n);
      } 
    } 
  }
  
  private void setButtonsBackground() {
    if (!isCenterDialog() && !hasTitle() && !hasMessage() && this.mListView == null && !hasCustomView()) {
      OplusBaseAlertController oplusBaseAlertController = (OplusBaseAlertController)OplusTypeCastingHelper.typeCasting(OplusBaseAlertController.class, this);
      if (oplusBaseAlertController != null) {
        Button button;
        if (isSingleButton()) {
          if (hasPositiveText()) {
            button = oplusBaseAlertController.getAlertControllerButtonPositive();
          } else if (hasNeutralText()) {
            button = button.getAlertControllerButtonNeutral();
          } else {
            button = button.getAlertControllerButtonNegative();
          } 
          button.setBackgroundResource(201851221);
        } else if (isDoubleButtons()) {
          if (hasPositiveText()) {
            button = button.getAlertControllerButtonPositive();
          } else {
            button = button.getAlertControllerButtonNeutral();
          } 
          button.setBackgroundResource(201851221);
        } else if (isTripleButtons()) {
          button = button.getAlertControllerButtonPositive();
          button.setBackgroundResource(201851221);
        } 
      } 
    } 
  }
  
  private boolean hasCustomView() {
    boolean bool;
    FrameLayout frameLayout = (FrameLayout)this.mWindow.findViewById(16908331);
    if (frameLayout.getChildCount() != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isSingleButton() {
    int i = buttonCount();
    boolean bool = true;
    if (i != 1)
      bool = false; 
    return bool;
  }
  
  private boolean isDoubleButtons() {
    boolean bool;
    if (buttonCount() == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isTripleButtons() {
    boolean bool;
    if (buttonCount() == 3) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean needSetButtonsVertical() {
    int i = buttonCount();
    boolean bool = false;
    if (i == 0)
      return false; 
    OplusBaseAlertController oplusBaseAlertController = (OplusBaseAlertController)OplusTypeCastingHelper.typeCasting(OplusBaseAlertController.class, this);
    if (oplusBaseAlertController != null) {
      byte b1, b2;
      i = this.mContext.getResources().getDimensionPixelOffset(201654395);
      int j = parentWidth() / buttonCount() - i * 2;
      if (hasPositiveText()) {
        i = (int)this.mButtonPaint.measureText(oplusBaseAlertController.getAlertControllerButtonPositiveText().toString());
      } else {
        i = 0;
      } 
      if (hasNegativeText()) {
        b1 = (int)this.mButtonPaint.measureText(oplusBaseAlertController.getAlertControllerButtonNegativeText().toString());
      } else {
        b1 = 0;
      } 
      if (hasNeutralText()) {
        b2 = (int)this.mButtonPaint.measureText(oplusBaseAlertController.getAlertControllerButtonNeutralText().toString());
      } else {
        b2 = 0;
      } 
      if (i > j || b1 > j || b2 > j)
        bool = true; 
      return bool;
    } 
    return false;
  }
  
  private int buttonCount() {
    int i = 0;
    if (hasPositiveText())
      i = 0 + 1; 
    int j = i;
    if (hasNegativeText())
      j = i + 1; 
    i = j;
    if (hasNeutralText())
      i = j + 1; 
    return i;
  }
  
  private boolean hasPositiveText() {
    boolean bool;
    OplusBaseAlertController oplusBaseAlertController = (OplusBaseAlertController)OplusTypeCastingHelper.typeCasting(OplusBaseAlertController.class, this);
    if (oplusBaseAlertController != null && !TextUtils.isEmpty(oplusBaseAlertController.getAlertControllerButtonPositiveText())) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean hasNegativeText() {
    boolean bool;
    OplusBaseAlertController oplusBaseAlertController = (OplusBaseAlertController)OplusTypeCastingHelper.typeCasting(OplusBaseAlertController.class, this);
    if (oplusBaseAlertController != null && !TextUtils.isEmpty(oplusBaseAlertController.getAlertControllerButtonNegativeText())) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean hasNeutralText() {
    boolean bool;
    OplusBaseAlertController oplusBaseAlertController = (OplusBaseAlertController)OplusTypeCastingHelper.typeCasting(OplusBaseAlertController.class, this);
    if (oplusBaseAlertController != null && !TextUtils.isEmpty(oplusBaseAlertController.getAlertControllerButtonNeutralText())) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean hasMessage() {
    return TextUtils.isEmpty(this.mMessage) ^ true;
  }
  
  private boolean hasTitle() {
    boolean bool;
    OplusBaseAlertController oplusBaseAlertController = (OplusBaseAlertController)OplusTypeCastingHelper.typeCasting(OplusBaseAlertController.class, this);
    if (oplusBaseAlertController != null && !TextUtils.isEmpty(oplusBaseAlertController.getAlertControllerTitle())) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private int parentWidth() {
    View view = this.mWindow.findViewById(16909279);
    int i = 0;
    if (view != null)
      i = view.getPaddingLeft(); 
    return (this.mWindow.getAttributes()).width - i * 2;
  }
  
  private void setButtonsVertical() {
    ViewStub viewStub = this.mButtonPanelStub;
    if (viewStub == null) {
      Log.e("ColorAlertController", "mButtonPanelStub is null when setButtonsVertical");
      return;
    } 
    viewStub.setLayoutResource(202440705);
    this.mButtonPanelStub.inflate();
    View view1 = this.mWindow.findViewById(201457689);
    View view2 = this.mWindow.findViewById(201457690);
    if (isCenterDialog() && !TextUtils.isEmpty(this.mMessage)) {
      view1.setVisibility(0);
    } else {
      view2.setVisibility(0);
    } 
  }
  
  private void setButtonsHorizontal() {
    ViewStub viewStub = this.mButtonPanelStub;
    if (viewStub == null) {
      Log.e("ColorAlertController", "mButtonPanelStub is null when setButtonsHorizontal");
      return;
    } 
    viewStub.setLayoutResource(202440704);
    this.mButtonPanelStub.inflate();
    showHorizontalDivider();
  }
  
  private void showHorizontalDivider() {
    ImageView imageView1 = (ImageView)this.mWindow.findViewById(201457737);
    ImageView imageView2 = (ImageView)this.mWindow.findViewById(201457738);
    if (buttonCount() == 2)
      imageView1.setVisibility(0); 
    if (buttonCount() == 3) {
      imageView1.setVisibility(0);
      imageView2.setVisibility(0);
    } 
  }
  
  private void updateSpaceHeight() {
    ViewGroup viewGroup = (ViewGroup)this.mWindow.findViewById(16909279);
    if (viewGroup != null) {
      View view = viewGroup.findViewById(201457669);
      if (view != null) {
        ViewGroup.LayoutParams layoutParams1 = view.getLayoutParams();
        layoutParams1.height = spaceHeight();
        view.setLayoutParams(layoutParams1);
      } 
    } 
    updateWindowFlag();
    WindowManager.LayoutParams layoutParams = this.mWindow.getAttributes();
    if (isSystemDialog(layoutParams))
      if (isNavigationBarShow()) {
        if (isGravityCenter())
          layoutParams.y = 0; 
      } else {
        layoutParams.y = 0;
      }  
    this.mWindow.setAttributes(layoutParams);
  }
  
  private boolean isSystemDialog(WindowManager.LayoutParams paramLayoutParams) {
    return (paramLayoutParams.type == 2003 || paramLayoutParams.type == 2038);
  }
  
  private static final class BottomSpaceHandler extends Handler {
    private static final int MSG_CONFIGURATION_CHANGED = 2;
    
    private static final int MSG_UPDATE_SPACE_HEIGHT = 1;
    
    private WeakReference<OplusAlertController> mReference;
    
    public BottomSpaceHandler(OplusAlertController param1OplusAlertController) {
      this.mReference = new WeakReference<>(param1OplusAlertController);
    }
    
    public void handleMessage(Message param1Message) {
      OplusAlertController oplusAlertController = this.mReference.get();
      if (oplusAlertController == null)
        return; 
      int i = param1Message.what;
      if (i != 1) {
        if (i == 2)
          oplusAlertController.onConfigurationChanged(); 
      } else {
        oplusAlertController.updateSpaceHeight();
      } 
    }
  }
  
  public static class FadingScrollView extends ScrollView {
    public FadingScrollView(Context param1Context) {
      super(param1Context);
    }
    
    public FadingScrollView(Context param1Context, AttributeSet param1AttributeSet) {
      super(param1Context, param1AttributeSet);
    }
    
    public FadingScrollView(Context param1Context, AttributeSet param1AttributeSet, int param1Int) {
      super(param1Context, param1AttributeSet, param1Int);
    }
  }
  
  public static class OplusRecyclerListView extends AlertController.RecycleListView {
    private Path mClipPath;
    
    private int mCornerRadius;
    
    private boolean mNeedClip;
    
    public OplusRecyclerListView(Context param1Context) {
      this(param1Context, (AttributeSet)null);
    }
    
    public OplusRecyclerListView(Context param1Context, AttributeSet param1AttributeSet) {
      super(param1Context, param1AttributeSet);
      this.mCornerRadius = param1Context.getResources().getDimensionPixelOffset(201654369);
    }
    
    public void draw(Canvas param1Canvas) {
      param1Canvas.save();
      if (this.mNeedClip)
        clipRoundBounds(param1Canvas); 
      super.draw(param1Canvas);
      param1Canvas.restore();
    }
    
    private void clipRoundBounds(Canvas param1Canvas) {
      int i = this.mCornerRadius;
      float f1 = i, f2 = i, f3 = i, f4 = i;
      if (this.mClipPath == null) {
        Path path = new Path();
        float f5 = getLeft(), f6 = getTop(), f7 = getRight(), f8 = getBottom();
        Path.Direction direction = Path.Direction.CW;
        path.addRoundRect(f5, f6, f7, f8, new float[] { f1, f2, f3, f4, 0.0F, 0.0F, 0.0F, 0.0F }, direction);
      } 
      param1Canvas.clipPath(this.mClipPath);
    }
    
    public void setNeedClip(boolean param1Boolean) {
      this.mNeedClip = param1Boolean;
    }
  }
}
