package com.android.internal.app;

import android.app.usage.UsageStatsManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractResolverComparator implements Comparator<ResolverActivity.ResolvedComponentInfo> {
  private static final boolean DEBUG = true;
  
  private static final int NUM_OF_TOP_ANNOTATIONS_TO_USE = 3;
  
  static final int RANKER_RESULT_TIMEOUT = 1;
  
  static final int RANKER_SERVICE_RESULT = 0;
  
  private static final String TAG = "AbstractResolverComp";
  
  private static final int WATCHDOG_TIMEOUT_MILLIS = 500;
  
  protected AfterCompute mAfterCompute;
  
  protected String[] mAnnotations;
  
  private final Comparator<ResolveInfo> mAzComparator;
  
  protected String mContentType;
  
  protected final Handler mHandler;
  
  private final boolean mHttp;
  
  protected final PackageManager mPm;
  
  protected final UsageStatsManager mUsm;
  
  public AbstractResolverComparator(Context paramContext, Intent paramIntent) {
    boolean bool;
    this.mHandler = (Handler)new Object(this, Looper.getMainLooper());
    String str = paramIntent.getScheme();
    if ("http".equals(str) || "https".equals(str)) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mHttp = bool;
    this.mContentType = paramIntent.getType();
    getContentAnnotations(paramIntent);
    this.mPm = paramContext.getPackageManager();
    this.mUsm = (UsageStatsManager)paramContext.getSystemService("usagestats");
    this.mAzComparator = new AzInfoComparator(paramContext);
  }
  
  private void getContentAnnotations(Intent paramIntent) {
    ArrayList<String> arrayList = paramIntent.getStringArrayListExtra("android.intent.extra.CONTENT_ANNOTATIONS");
    if (arrayList != null) {
      int i = arrayList.size();
      int j = i;
      if (i > 3)
        j = 3; 
      this.mAnnotations = new String[j];
      for (i = 0; i < j; i++)
        this.mAnnotations[i] = arrayList.get(i); 
    } 
  }
  
  void setCallBack(AfterCompute paramAfterCompute) {
    this.mAfterCompute = paramAfterCompute;
  }
  
  protected final void afterCompute() {
    AfterCompute afterCompute = this.mAfterCompute;
    if (afterCompute != null)
      afterCompute.afterCompute(); 
  }
  
  public final int compare(ResolverActivity.ResolvedComponentInfo paramResolvedComponentInfo1, ResolverActivity.ResolvedComponentInfo paramResolvedComponentInfo2) {
    byte b1 = 0;
    ResolveInfo resolveInfo1 = paramResolvedComponentInfo1.getResolveInfoAt(0);
    ResolveInfo resolveInfo2 = paramResolvedComponentInfo2.getResolveInfoAt(0);
    int i = resolveInfo1.targetUserId;
    byte b2 = 1;
    if (i != -2) {
      if (resolveInfo2.targetUserId != -2) {
        b2 = b1;
      } else {
        b2 = 1;
      } 
      return b2;
    } 
    if (resolveInfo2.targetUserId != -2)
      return -1; 
    if (this.mHttp) {
      boolean bool3 = ResolverActivity.isSpecificUriMatch(resolveInfo1.match);
      boolean bool4 = ResolverActivity.isSpecificUriMatch(resolveInfo2.match);
      if (bool3 != bool4) {
        if (bool3)
          b2 = -1; 
        return b2;
      } 
    } 
    boolean bool1 = paramResolvedComponentInfo1.isPinned();
    boolean bool2 = paramResolvedComponentInfo2.isPinned();
    if (bool1 && !bool2)
      return -1; 
    if (!bool1 && bool2)
      return 1; 
    if (bool1 && bool2)
      return this.mAzComparator.compare(paramResolvedComponentInfo1.getResolveInfoAt(0), paramResolvedComponentInfo2.getResolveInfoAt(0)); 
    return compare(resolveInfo1, resolveInfo2);
  }
  
  final void compute(List<ResolverActivity.ResolvedComponentInfo> paramList) {
    beforeCompute();
    doCompute(paramList);
  }
  
  final void updateChooserCounts(String paramString1, int paramInt, String paramString2) {
    UsageStatsManager usageStatsManager = this.mUsm;
    if (usageStatsManager != null)
      usageStatsManager.reportChooserSelection(paramString1, paramInt, this.mContentType, this.mAnnotations, paramString2); 
  }
  
  void updateModel(ComponentName paramComponentName) {}
  
  void beforeCompute() {
    Log.d("AbstractResolverComp", "Setting watchdog timer for 500ms");
    Handler handler = this.mHandler;
    if (handler == null) {
      Log.d("AbstractResolverComp", "Error: Handler is Null; Needs to be initialized.");
      return;
    } 
    handler.sendEmptyMessageDelayed(1, 500L);
  }
  
  void destroy() {
    this.mHandler.removeMessages(0);
    this.mHandler.removeMessages(1);
    afterCompute();
    this.mAfterCompute = null;
  }
  
  abstract int compare(ResolveInfo paramResolveInfo1, ResolveInfo paramResolveInfo2);
  
  abstract void doCompute(List<ResolverActivity.ResolvedComponentInfo> paramList);
  
  abstract float getScore(ComponentName paramComponentName);
  
  abstract List<ComponentName> getTopComponentNames(int paramInt);
  
  abstract void handleResultMessage(Message paramMessage);
  
  static interface AfterCompute {
    void afterCompute();
  }
  
  class AzInfoComparator implements Comparator<ResolveInfo> {
    Collator mCollator;
    
    final AbstractResolverComparator this$0;
    
    AzInfoComparator(Context param1Context) {
      this.mCollator = Collator.getInstance((param1Context.getResources().getConfiguration()).locale);
    }
    
    public int compare(ResolveInfo param1ResolveInfo1, ResolveInfo param1ResolveInfo2) {
      if (param1ResolveInfo1 == null)
        return -1; 
      if (param1ResolveInfo2 == null)
        return 1; 
      return this.mCollator.compare(param1ResolveInfo1.activityInfo.packageName, param1ResolveInfo2.activityInfo.packageName);
    }
  }
}
