package com.android.internal.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IAppOpsStartedCallback extends IInterface {
  void opStarted(int paramInt1, int paramInt2, String paramString, int paramInt3) throws RemoteException;
  
  class Default implements IAppOpsStartedCallback {
    public void opStarted(int param1Int1, int param1Int2, String param1String, int param1Int3) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAppOpsStartedCallback {
    private static final String DESCRIPTOR = "com.android.internal.app.IAppOpsStartedCallback";
    
    static final int TRANSACTION_opStarted = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.app.IAppOpsStartedCallback");
    }
    
    public static IAppOpsStartedCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.app.IAppOpsStartedCallback");
      if (iInterface != null && iInterface instanceof IAppOpsStartedCallback)
        return (IAppOpsStartedCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "opStarted";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.app.IAppOpsStartedCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.app.IAppOpsStartedCallback");
      int i = param1Parcel1.readInt();
      param1Int1 = param1Parcel1.readInt();
      String str = param1Parcel1.readString();
      param1Int2 = param1Parcel1.readInt();
      opStarted(i, param1Int1, str, param1Int2);
      return true;
    }
    
    private static class Proxy implements IAppOpsStartedCallback {
      public static IAppOpsStartedCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.app.IAppOpsStartedCallback";
      }
      
      public void opStarted(int param2Int1, int param2Int2, String param2String, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.app.IAppOpsStartedCallback");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeString(param2String);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IAppOpsStartedCallback.Stub.getDefaultImpl() != null) {
            IAppOpsStartedCallback.Stub.getDefaultImpl().opStarted(param2Int1, param2Int2, param2String, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAppOpsStartedCallback param1IAppOpsStartedCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAppOpsStartedCallback != null) {
          Proxy.sDefaultImpl = param1IAppOpsStartedCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAppOpsStartedCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
