package com.android.internal.app;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;

public interface IOplusResolverStyle extends IOplusCommonFeature {
  public static final IOplusResolverStyle DEFAULT = (IOplusResolverStyle)new Object();
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusResolverStyle;
  }
  
  default IOplusCommonFeature getDefault() {
    return DEFAULT;
  }
  
  default int getActivityStartAnimationRes() {
    return 0;
  }
  
  default int getActivityEndAnimationRes() {
    return 0;
  }
}
