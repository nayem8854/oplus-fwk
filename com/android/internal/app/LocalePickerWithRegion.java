package com.android.internal.app;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.os.LocaleList;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SearchView;
import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

public class LocalePickerWithRegion extends ListFragment implements SearchView.OnQueryTextListener {
  private boolean mTranslatedOnly = false;
  
  private SearchView mSearchView = null;
  
  private CharSequence mPreviousSearch = null;
  
  private boolean mPreviousSearchHadFocus = false;
  
  private int mFirstVisiblePosition = 0;
  
  private int mTopDistance = 0;
  
  private static final String PARENT_FRAGMENT_NAME = "localeListEditor";
  
  private SuggestedLocaleAdapter mAdapter;
  
  private LocaleSelectedListener mListener;
  
  private Set<LocaleStore.LocaleInfo> mLocaleList;
  
  private LocaleStore.LocaleInfo mParentLocale;
  
  private static LocalePickerWithRegion createCountryPicker(Context paramContext, LocaleSelectedListener paramLocaleSelectedListener, LocaleStore.LocaleInfo paramLocaleInfo, boolean paramBoolean) {
    LocalePickerWithRegion localePickerWithRegion = new LocalePickerWithRegion();
    paramBoolean = localePickerWithRegion.setListener(paramContext, paramLocaleSelectedListener, paramLocaleInfo, paramBoolean);
    if (paramBoolean) {
      LocalePickerWithRegion localePickerWithRegion1 = localePickerWithRegion;
    } else {
      paramContext = null;
    } 
    return (LocalePickerWithRegion)paramContext;
  }
  
  public static LocalePickerWithRegion createLanguagePicker(Context paramContext, LocaleSelectedListener paramLocaleSelectedListener, boolean paramBoolean) {
    LocalePickerWithRegion localePickerWithRegion = new LocalePickerWithRegion();
    localePickerWithRegion.setListener(paramContext, paramLocaleSelectedListener, (LocaleStore.LocaleInfo)null, paramBoolean);
    return localePickerWithRegion;
  }
  
  private boolean setListener(Context paramContext, LocaleSelectedListener paramLocaleSelectedListener, LocaleStore.LocaleInfo paramLocaleInfo, boolean paramBoolean) {
    Set<LocaleStore.LocaleInfo> set;
    this.mParentLocale = paramLocaleInfo;
    this.mListener = paramLocaleSelectedListener;
    this.mTranslatedOnly = paramBoolean;
    setRetainInstance(true);
    HashSet<? super String> hashSet = new HashSet();
    if (!paramBoolean) {
      LocaleList localeList = LocalePicker.getLocales();
      String[] arrayOfString = localeList.toLanguageTags().split(",");
      Collections.addAll(hashSet, arrayOfString);
    } 
    if (paramLocaleInfo != null) {
      this.mLocaleList = set = LocaleStore.getLevelLocales(paramContext, (Set)hashSet, paramLocaleInfo, paramBoolean);
      if (set.size() <= 1) {
        if (paramLocaleSelectedListener != null && this.mLocaleList.size() == 1)
          paramLocaleSelectedListener.onLocaleSelected(this.mLocaleList.iterator().next()); 
        return false;
      } 
    } else {
      this.mLocaleList = LocaleStore.getLevelLocales((Context)set, (Set)hashSet, null, paramBoolean);
    } 
    return true;
  }
  
  private void returnToParentFrame() {
    getFragmentManager().popBackStack("localeListEditor", 1);
  }
  
  public void onCreate(Bundle paramBundle) {
    Locale locale;
    super.onCreate(paramBundle);
    boolean bool = true;
    setHasOptionsMenu(true);
    if (this.mLocaleList == null) {
      returnToParentFrame();
      return;
    } 
    if (this.mParentLocale == null)
      bool = false; 
    if (bool) {
      locale = this.mParentLocale.getLocale();
    } else {
      locale = Locale.getDefault();
    } 
    this.mAdapter = new SuggestedLocaleAdapter(this.mLocaleList, bool);
    LocaleHelper.LocaleInfoComparator localeInfoComparator = new LocaleHelper.LocaleInfoComparator(locale, bool);
    this.mAdapter.sort(localeInfoComparator);
    setListAdapter(this.mAdapter);
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem) {
    int i = paramMenuItem.getItemId();
    if (i != 16908332)
      return super.onOptionsItemSelected(paramMenuItem); 
    getFragmentManager().popBackStack();
    return true;
  }
  
  public void onResume() {
    super.onResume();
    if (this.mParentLocale != null) {
      getActivity().setTitle(this.mParentLocale.getFullNameNative());
    } else {
      getActivity().setTitle(17040446);
    } 
    getListView().requestFocus();
  }
  
  public void onPause() {
    super.onPause();
    SearchView searchView = this.mSearchView;
    int i = 0;
    if (searchView != null) {
      this.mPreviousSearchHadFocus = searchView.hasFocus();
      this.mPreviousSearch = this.mSearchView.getQuery();
    } else {
      this.mPreviousSearchHadFocus = false;
      this.mPreviousSearch = null;
    } 
    ListView listView = getListView();
    View view = listView.getChildAt(0);
    this.mFirstVisiblePosition = listView.getFirstVisiblePosition();
    if (view != null)
      i = view.getTop() - listView.getPaddingTop(); 
    this.mTopDistance = i;
  }
  
  public void onListItemClick(ListView paramListView, View paramView, int paramInt, long paramLong) {
    LocaleStore.LocaleInfo localeInfo = (LocaleStore.LocaleInfo)getListAdapter().getItem(paramInt);
    if (localeInfo.getParent() != null) {
      LocaleSelectedListener localeSelectedListener = this.mListener;
      if (localeSelectedListener != null)
        localeSelectedListener.onLocaleSelected(localeInfo); 
      returnToParentFrame();
    } else {
      Context context = getContext();
      LocaleSelectedListener localeSelectedListener = this.mListener;
      boolean bool = this.mTranslatedOnly;
      LocalePickerWithRegion localePickerWithRegion = createCountryPicker(context, localeSelectedListener, localeInfo, bool);
      if (localePickerWithRegion != null) {
        FragmentTransaction fragmentTransaction2 = getFragmentManager().beginTransaction();
        fragmentTransaction2 = fragmentTransaction2.setTransition(4097);
        FragmentTransaction fragmentTransaction1 = fragmentTransaction2.replace(getId(), (Fragment)localePickerWithRegion).addToBackStack(null);
        fragmentTransaction1.commit();
      } else {
        returnToParentFrame();
      } 
    } 
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater) {
    if (this.mParentLocale == null) {
      paramMenuInflater.inflate(18087936, paramMenu);
      MenuItem menuItem = paramMenu.findItem(16909135);
      SearchView searchView = (SearchView)menuItem.getActionView();
      searchView.setQueryHint(getText(17041236));
      this.mSearchView.setOnQueryTextListener(this);
      if (!TextUtils.isEmpty(this.mPreviousSearch)) {
        menuItem.expandActionView();
        this.mSearchView.setIconified(false);
        this.mSearchView.setActivated(true);
        if (this.mPreviousSearchHadFocus)
          this.mSearchView.requestFocus(); 
        this.mSearchView.setQuery(this.mPreviousSearch, true);
      } else {
        this.mSearchView.setQuery((CharSequence)null, false);
      } 
      getListView().setSelectionFromTop(this.mFirstVisiblePosition, this.mTopDistance);
    } 
  }
  
  public boolean onQueryTextSubmit(String paramString) {
    return false;
  }
  
  public boolean onQueryTextChange(String paramString) {
    SuggestedLocaleAdapter suggestedLocaleAdapter = this.mAdapter;
    if (suggestedLocaleAdapter != null)
      suggestedLocaleAdapter.getFilter().filter(paramString); 
    return false;
  }
  
  class LocaleSelectedListener {
    public abstract void onLocaleSelected(LocaleStore.LocaleInfo param1LocaleInfo);
  }
}
