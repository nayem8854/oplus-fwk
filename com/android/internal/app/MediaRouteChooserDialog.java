package com.android.internal.app;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.media.MediaRouter;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import java.util.Comparator;

public class MediaRouteChooserDialog extends Dialog {
  private RouteAdapter mAdapter;
  
  private boolean mAttachedToWindow;
  
  private final MediaRouterCallback mCallback;
  
  private Button mExtendedSettingsButton;
  
  private View.OnClickListener mExtendedSettingsClickListener;
  
  private ListView mListView;
  
  private int mRouteTypes;
  
  private final MediaRouter mRouter;
  
  public MediaRouteChooserDialog(Context paramContext, int paramInt) {
    super(paramContext, paramInt);
    this.mRouter = (MediaRouter)paramContext.getSystemService("media_router");
    this.mCallback = new MediaRouterCallback();
  }
  
  public int getRouteTypes() {
    return this.mRouteTypes;
  }
  
  public void setRouteTypes(int paramInt) {
    if (this.mRouteTypes != paramInt) {
      this.mRouteTypes = paramInt;
      if (this.mAttachedToWindow) {
        this.mRouter.removeCallback((MediaRouter.Callback)this.mCallback);
        this.mRouter.addCallback(paramInt, (MediaRouter.Callback)this.mCallback, 1);
      } 
      refreshRoutes();
    } 
  }
  
  public void setExtendedSettingsClickListener(View.OnClickListener paramOnClickListener) {
    if (paramOnClickListener != this.mExtendedSettingsClickListener) {
      this.mExtendedSettingsClickListener = paramOnClickListener;
      updateExtendedSettingsButton();
    } 
  }
  
  public boolean onFilterRoute(MediaRouter.RouteInfo paramRouteInfo) {
    boolean bool;
    if (!paramRouteInfo.isDefault() && paramRouteInfo.isEnabled() && paramRouteInfo.matchesTypes(this.mRouteTypes)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected void onCreate(Bundle paramBundle) {
    int i;
    super.onCreate(paramBundle);
    getWindow().requestFeature(3);
    setContentView(17367194);
    if (this.mRouteTypes == 4) {
      i = 17040534;
    } else {
      i = 17040533;
    } 
    setTitle(i);
    Window window = getWindow();
    if (isLightTheme(getContext())) {
      i = 17302669;
    } else {
      i = 17302668;
    } 
    window.setFeatureDrawableResource(3, i);
    this.mAdapter = new RouteAdapter(getContext());
    ListView listView = (ListView)findViewById(16909157);
    listView.setAdapter(this.mAdapter);
    this.mListView.setOnItemClickListener(this.mAdapter);
    this.mListView.setEmptyView(findViewById(16908292));
    this.mExtendedSettingsButton = (Button)findViewById(16909156);
    updateExtendedSettingsButton();
  }
  
  private void updateExtendedSettingsButton() {
    Button button = this.mExtendedSettingsButton;
    if (button != null) {
      byte b;
      button.setOnClickListener(this.mExtendedSettingsClickListener);
      button = this.mExtendedSettingsButton;
      if (this.mExtendedSettingsClickListener != null) {
        b = 0;
      } else {
        b = 8;
      } 
      button.setVisibility(b);
    } 
  }
  
  public void onAttachedToWindow() {
    super.onAttachedToWindow();
    this.mAttachedToWindow = true;
    this.mRouter.addCallback(this.mRouteTypes, (MediaRouter.Callback)this.mCallback, 1);
    refreshRoutes();
  }
  
  public void onDetachedFromWindow() {
    this.mAttachedToWindow = false;
    this.mRouter.removeCallback((MediaRouter.Callback)this.mCallback);
    super.onDetachedFromWindow();
  }
  
  public void refreshRoutes() {
    if (this.mAttachedToWindow)
      this.mAdapter.update(); 
  }
  
  static boolean isLightTheme(Context paramContext) {
    TypedValue typedValue = new TypedValue();
    Resources.Theme theme = paramContext.getTheme();
    boolean bool = true;
    if (!theme.resolveAttribute(16844176, typedValue, true) || typedValue.data == 0)
      bool = false; 
    return bool;
  }
  
  class RouteAdapter extends ArrayAdapter<MediaRouter.RouteInfo> implements AdapterView.OnItemClickListener {
    private final LayoutInflater mInflater;
    
    final MediaRouteChooserDialog this$0;
    
    public RouteAdapter(Context param1Context) {
      super(param1Context, 0);
      this.mInflater = LayoutInflater.from(param1Context);
    }
    
    public void update() {
      clear();
      int i = MediaRouteChooserDialog.this.mRouter.getRouteCount();
      for (byte b = 0; b < i; b++) {
        MediaRouter.RouteInfo routeInfo = MediaRouteChooserDialog.this.mRouter.getRouteAt(b);
        if (MediaRouteChooserDialog.this.onFilterRoute(routeInfo))
          add(routeInfo); 
      } 
      sort(MediaRouteChooserDialog.RouteComparator.sInstance);
      notifyDataSetChanged();
    }
    
    public boolean areAllItemsEnabled() {
      return false;
    }
    
    public boolean isEnabled(int param1Int) {
      return getItem(param1Int).isEnabled();
    }
    
    public View getView(int param1Int, View param1View, ViewGroup param1ViewGroup) {
      View view = param1View;
      param1View = view;
      if (view == null)
        param1View = this.mInflater.inflate(17367196, param1ViewGroup, false); 
      MediaRouter.RouteInfo routeInfo = getItem(param1Int);
      TextView textView2 = param1View.<TextView>findViewById(16908308);
      TextView textView1 = param1View.<TextView>findViewById(16908309);
      textView2.setText(routeInfo.getName());
      CharSequence charSequence = routeInfo.getDescription();
      if (TextUtils.isEmpty(charSequence)) {
        textView1.setVisibility(8);
        textView1.setText("");
      } else {
        textView1.setVisibility(0);
        textView1.setText(charSequence);
      } 
      param1View.setEnabled(routeInfo.isEnabled());
      return param1View;
    }
    
    public void onItemClick(AdapterView<?> param1AdapterView, View param1View, int param1Int, long param1Long) {
      MediaRouter.RouteInfo routeInfo = getItem(param1Int);
      if (routeInfo.isEnabled()) {
        routeInfo.select();
        MediaRouteChooserDialog.this.dismiss();
      } 
    }
  }
  
  class MediaRouterCallback extends MediaRouter.SimpleCallback {
    final MediaRouteChooserDialog this$0;
    
    private MediaRouterCallback() {}
    
    public void onRouteAdded(MediaRouter param1MediaRouter, MediaRouter.RouteInfo param1RouteInfo) {
      MediaRouteChooserDialog.this.refreshRoutes();
    }
    
    public void onRouteRemoved(MediaRouter param1MediaRouter, MediaRouter.RouteInfo param1RouteInfo) {
      MediaRouteChooserDialog.this.refreshRoutes();
    }
    
    public void onRouteChanged(MediaRouter param1MediaRouter, MediaRouter.RouteInfo param1RouteInfo) {
      MediaRouteChooserDialog.this.refreshRoutes();
    }
    
    public void onRouteSelected(MediaRouter param1MediaRouter, int param1Int, MediaRouter.RouteInfo param1RouteInfo) {
      MediaRouteChooserDialog.this.dismiss();
    }
  }
  
  class RouteComparator implements Comparator<MediaRouter.RouteInfo> {
    public static final RouteComparator sInstance = new RouteComparator();
    
    public int compare(MediaRouter.RouteInfo param1RouteInfo1, MediaRouter.RouteInfo param1RouteInfo2) {
      return param1RouteInfo1.getName().toString().compareTo(param1RouteInfo2.getName().toString());
    }
  }
}
