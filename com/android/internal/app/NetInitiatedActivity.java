package com.android.internal.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManagerInternal;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;
import com.android.server.LocalServices;

public class NetInitiatedActivity extends AlertActivity implements DialogInterface.OnClickListener {
  private int notificationId = -1;
  
  private int timeout = -1;
  
  private int default_response = -1;
  
  private int default_response_timeout = 6;
  
  private BroadcastReceiver mNetInitiatedReceiver = (BroadcastReceiver)new Object(this);
  
  private final Handler mHandler = (Handler)new Object(this);
  
  private static final boolean DEBUG = true;
  
  private static final int GPS_NO_RESPONSE_TIME_OUT = 1;
  
  private static final int NEGATIVE_BUTTON = -2;
  
  private static final int POSITIVE_BUTTON = -1;
  
  private static final String TAG = "NetInitiatedActivity";
  
  private static final boolean VERBOSE = false;
  
  protected void onCreate(Bundle paramBundle) {
    super.onCreate(paramBundle);
    Intent intent = getIntent();
    AlertController.AlertParams alertParams = this.mAlertParams;
    Context context = getApplicationContext();
    alertParams.mTitle = intent.getStringExtra("title");
    alertParams.mMessage = intent.getStringExtra("message");
    alertParams.mPositiveButtonText = String.format(context.getString(17040291), new Object[0]);
    alertParams.mPositiveButtonListener = this;
    alertParams.mNegativeButtonText = String.format(context.getString(17040290), new Object[0]);
    alertParams.mNegativeButtonListener = this;
    this.notificationId = intent.getIntExtra("notif_id", -1);
    this.timeout = intent.getIntExtra("timeout", this.default_response_timeout);
    this.default_response = intent.getIntExtra("default_resp", 1);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("onCreate() : notificationId: ");
    stringBuilder.append(this.notificationId);
    stringBuilder.append(" timeout: ");
    stringBuilder.append(this.timeout);
    stringBuilder.append(" default_response:");
    stringBuilder.append(this.default_response);
    Log.d("NetInitiatedActivity", stringBuilder.toString());
    Handler handler = this.mHandler;
    handler.sendMessageDelayed(handler.obtainMessage(1), (this.timeout * 1000));
    setupAlert();
  }
  
  protected void onResume() {
    super.onResume();
    Log.d("NetInitiatedActivity", "onResume");
    registerReceiver(this.mNetInitiatedReceiver, new IntentFilter("android.intent.action.NETWORK_INITIATED_VERIFY"));
  }
  
  protected void onPause() {
    super.onPause();
    Log.d("NetInitiatedActivity", "onPause");
    unregisterReceiver(this.mNetInitiatedReceiver);
  }
  
  public void onClick(DialogInterface paramDialogInterface, int paramInt) {
    if (paramInt == -1)
      sendUserResponse(1); 
    if (paramInt == -2)
      sendUserResponse(2); 
    finish();
    this.notificationId = -1;
  }
  
  private void sendUserResponse(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("sendUserResponse, response: ");
    stringBuilder.append(paramInt);
    Log.d("NetInitiatedActivity", stringBuilder.toString());
    LocationManagerInternal locationManagerInternal = (LocationManagerInternal)LocalServices.getService(LocationManagerInternal.class);
    locationManagerInternal.sendNiResponse(this.notificationId, paramInt);
  }
  
  private void handleNIVerify(Intent paramIntent) {
    int i = paramIntent.getIntExtra("notif_id", -1);
    this.notificationId = i;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("handleNIVerify action: ");
    stringBuilder.append(paramIntent.getAction());
    Log.d("NetInitiatedActivity", stringBuilder.toString());
  }
  
  private void showNIError() {
    Toast toast = Toast.makeText((Context)this, "NI error", 1);
    toast.show();
  }
}
