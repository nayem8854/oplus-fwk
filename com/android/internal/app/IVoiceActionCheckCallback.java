package com.android.internal.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IVoiceActionCheckCallback extends IInterface {
  void onComplete(List<String> paramList) throws RemoteException;
  
  class Default implements IVoiceActionCheckCallback {
    public void onComplete(List<String> param1List) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IVoiceActionCheckCallback {
    private static final String DESCRIPTOR = "com.android.internal.app.IVoiceActionCheckCallback";
    
    static final int TRANSACTION_onComplete = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.app.IVoiceActionCheckCallback");
    }
    
    public static IVoiceActionCheckCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.app.IVoiceActionCheckCallback");
      if (iInterface != null && iInterface instanceof IVoiceActionCheckCallback)
        return (IVoiceActionCheckCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onComplete";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.app.IVoiceActionCheckCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.app.IVoiceActionCheckCallback");
      ArrayList<String> arrayList = param1Parcel1.createStringArrayList();
      onComplete(arrayList);
      return true;
    }
    
    private static class Proxy implements IVoiceActionCheckCallback {
      public static IVoiceActionCheckCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.app.IVoiceActionCheckCallback";
      }
      
      public void onComplete(List<String> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.app.IVoiceActionCheckCallback");
          parcel.writeStringList(param2List);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IVoiceActionCheckCallback.Stub.getDefaultImpl() != null) {
            IVoiceActionCheckCallback.Stub.getDefaultImpl().onComplete(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IVoiceActionCheckCallback param1IVoiceActionCheckCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IVoiceActionCheckCallback != null) {
          Proxy.sDefaultImpl = param1IVoiceActionCheckCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IVoiceActionCheckCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
