package com.android.internal.app;

import android.app.AsyncNotedAppOp;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IAppOpsAsyncNotedCallback extends IInterface {
  void opNoted(AsyncNotedAppOp paramAsyncNotedAppOp) throws RemoteException;
  
  class Default implements IAppOpsAsyncNotedCallback {
    public void opNoted(AsyncNotedAppOp param1AsyncNotedAppOp) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAppOpsAsyncNotedCallback {
    private static final String DESCRIPTOR = "com.android.internal.app.IAppOpsAsyncNotedCallback";
    
    static final int TRANSACTION_opNoted = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.app.IAppOpsAsyncNotedCallback");
    }
    
    public static IAppOpsAsyncNotedCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.app.IAppOpsAsyncNotedCallback");
      if (iInterface != null && iInterface instanceof IAppOpsAsyncNotedCallback)
        return (IAppOpsAsyncNotedCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "opNoted";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.app.IAppOpsAsyncNotedCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.app.IAppOpsAsyncNotedCallback");
      if (param1Parcel1.readInt() != 0) {
        AsyncNotedAppOp asyncNotedAppOp = (AsyncNotedAppOp)AsyncNotedAppOp.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      opNoted((AsyncNotedAppOp)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IAppOpsAsyncNotedCallback {
      public static IAppOpsAsyncNotedCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.app.IAppOpsAsyncNotedCallback";
      }
      
      public void opNoted(AsyncNotedAppOp param2AsyncNotedAppOp) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.app.IAppOpsAsyncNotedCallback");
          if (param2AsyncNotedAppOp != null) {
            parcel.writeInt(1);
            param2AsyncNotedAppOp.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IAppOpsAsyncNotedCallback.Stub.getDefaultImpl() != null) {
            IAppOpsAsyncNotedCallback.Stub.getDefaultImpl().opNoted(param2AsyncNotedAppOp);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAppOpsAsyncNotedCallback param1IAppOpsAsyncNotedCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAppOpsAsyncNotedCallback != null) {
          Proxy.sDefaultImpl = param1IAppOpsAsyncNotedCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAppOpsAsyncNotedCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
