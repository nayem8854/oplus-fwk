package com.android.internal.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IVoiceInteractionSessionShowCallback extends IInterface {
  void onFailed() throws RemoteException;
  
  void onShown() throws RemoteException;
  
  class Default implements IVoiceInteractionSessionShowCallback {
    public void onFailed() throws RemoteException {}
    
    public void onShown() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IVoiceInteractionSessionShowCallback {
    private static final String DESCRIPTOR = "com.android.internal.app.IVoiceInteractionSessionShowCallback";
    
    static final int TRANSACTION_onFailed = 1;
    
    static final int TRANSACTION_onShown = 2;
    
    public Stub() {
      attachInterface(this, "com.android.internal.app.IVoiceInteractionSessionShowCallback");
    }
    
    public static IVoiceInteractionSessionShowCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.app.IVoiceInteractionSessionShowCallback");
      if (iInterface != null && iInterface instanceof IVoiceInteractionSessionShowCallback)
        return (IVoiceInteractionSessionShowCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onShown";
      } 
      return "onFailed";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("com.android.internal.app.IVoiceInteractionSessionShowCallback");
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.internal.app.IVoiceInteractionSessionShowCallback");
        onShown();
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.app.IVoiceInteractionSessionShowCallback");
      onFailed();
      return true;
    }
    
    private static class Proxy implements IVoiceInteractionSessionShowCallback {
      public static IVoiceInteractionSessionShowCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.app.IVoiceInteractionSessionShowCallback";
      }
      
      public void onFailed() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.app.IVoiceInteractionSessionShowCallback");
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IVoiceInteractionSessionShowCallback.Stub.getDefaultImpl() != null) {
            IVoiceInteractionSessionShowCallback.Stub.getDefaultImpl().onFailed();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onShown() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.app.IVoiceInteractionSessionShowCallback");
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IVoiceInteractionSessionShowCallback.Stub.getDefaultImpl() != null) {
            IVoiceInteractionSessionShowCallback.Stub.getDefaultImpl().onShown();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IVoiceInteractionSessionShowCallback param1IVoiceInteractionSessionShowCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IVoiceInteractionSessionShowCallback != null) {
          Proxy.sDefaultImpl = param1IVoiceInteractionSessionShowCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IVoiceInteractionSessionShowCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
