package com.android.internal.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IVoiceInteractorRequest extends IInterface {
  void cancel() throws RemoteException;
  
  class Default implements IVoiceInteractorRequest {
    public void cancel() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IVoiceInteractorRequest {
    private static final String DESCRIPTOR = "com.android.internal.app.IVoiceInteractorRequest";
    
    static final int TRANSACTION_cancel = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.app.IVoiceInteractorRequest");
    }
    
    public static IVoiceInteractorRequest asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.app.IVoiceInteractorRequest");
      if (iInterface != null && iInterface instanceof IVoiceInteractorRequest)
        return (IVoiceInteractorRequest)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "cancel";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.app.IVoiceInteractorRequest");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.app.IVoiceInteractorRequest");
      cancel();
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IVoiceInteractorRequest {
      public static IVoiceInteractorRequest sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.app.IVoiceInteractorRequest";
      }
      
      public void cancel() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractorRequest");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractorRequest.Stub.getDefaultImpl() != null) {
            IVoiceInteractorRequest.Stub.getDefaultImpl().cancel();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IVoiceInteractorRequest param1IVoiceInteractorRequest) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IVoiceInteractorRequest != null) {
          Proxy.sDefaultImpl = param1IVoiceInteractorRequest;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IVoiceInteractorRequest getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
