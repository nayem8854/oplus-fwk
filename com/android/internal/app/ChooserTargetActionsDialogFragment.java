package com.android.internal.app;

import android.app.ActivityManager;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.UserHandle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.internal.app.chooser.DisplayResolveInfo;
import com.android.internal.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ChooserTargetActionsDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {
  protected List<DisplayResolveInfo> mTargetInfos = new ArrayList<>();
  
  protected UserHandle mUserHandle;
  
  public ChooserTargetActionsDialogFragment(List<DisplayResolveInfo> paramList, UserHandle paramUserHandle) {
    this.mUserHandle = paramUserHandle;
    this.mTargetInfos = paramList;
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle) {
    Optional optional = Optional.<Dialog>of(getDialog()).map((Function)_$$Lambda$vYiecjY4iY9i1hZm55q8wMqrKMI.INSTANCE);
    -$.Lambda.ChooserTargetActionsDialogFragment.fTSMr5qDc0izJp2Gm3KHN2hwt9o fTSMr5qDc0izJp2Gm3KHN2hwt9o = _$$Lambda$ChooserTargetActionsDialogFragment$fTSMr5qDc0izJp2Gm3KHN2hwt9o.INSTANCE;
    optional.ifPresent((Consumer)fTSMr5qDc0izJp2Gm3KHN2hwt9o);
    Stream stream = this.mTargetInfos.stream().map(new _$$Lambda$ChooserTargetActionsDialogFragment$fI4psZChlLIMMHRVCuV7MASyfko(this));
    List<Pair<Drawable, CharSequence>> list = (List)stream.collect(Collectors.toList());
    View view = paramLayoutInflater.inflate(17367126, paramViewGroup, false);
    TextView textView = view.<TextView>findViewById(16908310);
    ImageView imageView = view.<ImageView>findViewById(16908294);
    RecyclerView recyclerView = view.<RecyclerView>findViewById(16909128);
    ResolverListAdapter.ResolveInfoPresentationGetter resolveInfoPresentationGetter = getProvidingAppPresentationGetter();
    textView.setText(resolveInfoPresentationGetter.getLabel());
    imageView.setImageDrawable(resolveInfoPresentationGetter.getIcon(this.mUserHandle));
    recyclerView.setAdapter(new VHAdapter(list));
    return view;
  }
  
  class VHAdapter extends RecyclerView.Adapter<VH> {
    List<Pair<Drawable, CharSequence>> mItems;
    
    final ChooserTargetActionsDialogFragment this$0;
    
    VHAdapter(List<Pair<Drawable, CharSequence>> param1List) {
      this.mItems = param1List;
    }
    
    public ChooserTargetActionsDialogFragment.VH onCreateViewHolder(ViewGroup param1ViewGroup, int param1Int) {
      return new ChooserTargetActionsDialogFragment.VH(LayoutInflater.from(param1ViewGroup.getContext()).inflate(17367127, param1ViewGroup, false));
    }
    
    public void onBindViewHolder(ChooserTargetActionsDialogFragment.VH param1VH, int param1Int) {
      param1VH.bind(this.mItems.get(param1Int), param1Int);
    }
    
    public int getItemCount() {
      return this.mItems.size();
    }
  }
  
  class VH extends RecyclerView.ViewHolder {
    ImageView mIcon;
    
    TextView mLabel;
    
    final ChooserTargetActionsDialogFragment this$0;
    
    VH(View param1View) {
      super(param1View);
      this.mLabel = param1View.<TextView>findViewById(16909505);
      this.mIcon = param1View.<ImageView>findViewById(16908294);
    }
    
    public void bind(Pair<Drawable, CharSequence> param1Pair, int param1Int) {
      this.mLabel.setText((CharSequence)param1Pair.second);
      if (param1Pair.first == null) {
        this.mIcon.setVisibility(8);
      } else {
        this.mIcon.setVisibility(0);
        this.mIcon.setImageDrawable((Drawable)param1Pair.first);
      } 
      this.itemView.setOnClickListener(new _$$Lambda$ChooserTargetActionsDialogFragment$VH$_qUOTPR4BqyhKM1rxJEUUUrO6eY(this, param1Int));
    }
  }
  
  public void onClick(DialogInterface paramDialogInterface, int paramInt) {
    pinComponent(((DisplayResolveInfo)this.mTargetInfos.get(paramInt)).getResolvedComponentName());
    ((ChooserActivity)getActivity()).handlePackagesChanged();
    dismiss();
  }
  
  private void pinComponent(ComponentName paramComponentName) {
    SharedPreferences sharedPreferences = ChooserActivity.getPinnedSharedPrefs(getContext());
    String str = paramComponentName.flattenToString();
    boolean bool = sharedPreferences.getBoolean(paramComponentName.flattenToString(), false);
    if (bool) {
      sharedPreferences.edit().remove(str).apply();
    } else {
      sharedPreferences.edit().putBoolean(str, true).apply();
    } 
  }
  
  private Drawable getPinIcon(boolean paramBoolean) {
    Drawable drawable;
    if (paramBoolean) {
      drawable = getContext().getDrawable(17302365);
    } else {
      drawable = getContext().getDrawable(17302353);
    } 
    return drawable;
  }
  
  private CharSequence getPinLabel(boolean paramBoolean, CharSequence paramCharSequence) {
    if (paramBoolean) {
      paramCharSequence = getResources().getString(17041422, new Object[] { paramCharSequence });
    } else {
      paramCharSequence = getResources().getString(17041093, new Object[] { paramCharSequence });
    } 
    return paramCharSequence;
  }
  
  protected CharSequence getItemLabel(DisplayResolveInfo paramDisplayResolveInfo) {
    PackageManager packageManager = getContext().getPackageManager();
    return getPinLabel(paramDisplayResolveInfo.isPinned(), paramDisplayResolveInfo.getResolveInfo().loadLabel(packageManager));
  }
  
  protected Drawable getItemIcon(DisplayResolveInfo paramDisplayResolveInfo) {
    return getPinIcon(paramDisplayResolveInfo.isPinned());
  }
  
  private ResolverListAdapter.ResolveInfoPresentationGetter getProvidingAppPresentationGetter() {
    Context context1 = getContext();
    ActivityManager activityManager = (ActivityManager)context1.getSystemService("activity");
    int i = activityManager.getLauncherLargeIconDensity();
    Context context2 = getContext();
    List<DisplayResolveInfo> list = this.mTargetInfos;
    return 
      new ResolverListAdapter.ResolveInfoPresentationGetter(context2, i, ((DisplayResolveInfo)list.get(0)).getResolveInfo());
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration) {
    super.onConfigurationChanged(paramConfiguration);
    dismiss();
  }
  
  public ChooserTargetActionsDialogFragment() {}
}
