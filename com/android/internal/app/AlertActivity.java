package com.android.internal.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;

public abstract class AlertActivity extends Activity implements DialogInterface {
  protected AlertController mAlert;
  
  protected AlertController.AlertParams mAlertParams;
  
  protected void onCreate(Bundle paramBundle) {
    super.onCreate(paramBundle);
    this.mAlert = AlertController.create((Context)this, this, getWindow());
    this.mAlertParams = new AlertController.AlertParams((Context)this);
  }
  
  public void cancel() {
    finish();
  }
  
  public void dismiss() {
    if (!isFinishing())
      finish(); 
  }
  
  public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent) {
    return dispatchPopulateAccessibilityEvent(this, paramAccessibilityEvent);
  }
  
  public static boolean dispatchPopulateAccessibilityEvent(Activity paramActivity, AccessibilityEvent paramAccessibilityEvent) {
    boolean bool;
    paramAccessibilityEvent.setClassName(Dialog.class.getName());
    paramAccessibilityEvent.setPackageName(paramActivity.getPackageName());
    WindowManager.LayoutParams layoutParams = paramActivity.getWindow().getAttributes();
    if (layoutParams.width == -1 && layoutParams.height == -1) {
      bool = true;
    } else {
      bool = false;
    } 
    paramAccessibilityEvent.setFullScreen(bool);
    return false;
  }
  
  protected void setupAlert() {
    this.mAlert.installContent(this.mAlertParams);
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
    if (this.mAlert.onKeyDown(paramInt, paramKeyEvent))
      return true; 
    return super.onKeyDown(paramInt, paramKeyEvent);
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent) {
    if (this.mAlert.onKeyUp(paramInt, paramKeyEvent))
      return true; 
    return super.onKeyUp(paramInt, paramKeyEvent);
  }
}
