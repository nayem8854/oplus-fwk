package com.android.internal.app;

import android.app.ActionBar;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.view.ActionMode;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowCallbackWrapper;
import android.widget.SpinnerAdapter;
import android.widget.Toolbar;
import com.android.internal.view.menu.MenuBuilder;
import com.android.internal.view.menu.MenuPresenter;
import com.android.internal.widget.DecorToolbar;
import com.android.internal.widget.ToolbarWidgetWrapper;
import java.util.ArrayList;

public class ToolbarActionBar extends ActionBar {
  private Window.Callback mWindowCallback;
  
  private boolean mToolbarMenuPrepared;
  
  private ArrayList<ActionBar.OnMenuVisibilityListener> mMenuVisibilityListeners = new ArrayList<>();
  
  private final Runnable mMenuInvalidator = (Runnable)new Object(this);
  
  private final Toolbar.OnMenuItemClickListener mMenuClicker = new Toolbar.OnMenuItemClickListener() {
      final ToolbarActionBar this$0;
      
      public boolean onMenuItemClick(MenuItem param1MenuItem) {
        return ToolbarActionBar.this.mWindowCallback.onMenuItemSelected(0, param1MenuItem);
      }
    };
  
  private boolean mMenuCallbackSet;
  
  private boolean mLastMenuVisibility;
  
  private DecorToolbar mDecorToolbar;
  
  public ToolbarActionBar(Toolbar paramToolbar, CharSequence paramCharSequence, Window.Callback paramCallback) {
    this.mDecorToolbar = (DecorToolbar)new ToolbarWidgetWrapper(paramToolbar, false);
    this.mWindowCallback = paramCallback = new ToolbarCallbackWrapper(paramCallback);
    this.mDecorToolbar.setWindowCallback(paramCallback);
    paramToolbar.setOnMenuItemClickListener(this.mMenuClicker);
    this.mDecorToolbar.setWindowTitle(paramCharSequence);
  }
  
  public Window.Callback getWrappedWindowCallback() {
    return this.mWindowCallback;
  }
  
  public void setCustomView(View paramView) {
    setCustomView(paramView, new ActionBar.LayoutParams(-2, -2));
  }
  
  public void setCustomView(View paramView, ActionBar.LayoutParams paramLayoutParams) {
    if (paramView != null)
      paramView.setLayoutParams((ViewGroup.LayoutParams)paramLayoutParams); 
    this.mDecorToolbar.setCustomView(paramView);
  }
  
  public void setCustomView(int paramInt) {
    LayoutInflater layoutInflater = LayoutInflater.from(this.mDecorToolbar.getContext());
    setCustomView(layoutInflater.inflate(paramInt, this.mDecorToolbar.getViewGroup(), false));
  }
  
  public void setIcon(int paramInt) {
    this.mDecorToolbar.setIcon(paramInt);
  }
  
  public void setIcon(Drawable paramDrawable) {
    this.mDecorToolbar.setIcon(paramDrawable);
  }
  
  public void setLogo(int paramInt) {
    this.mDecorToolbar.setLogo(paramInt);
  }
  
  public void setLogo(Drawable paramDrawable) {
    this.mDecorToolbar.setLogo(paramDrawable);
  }
  
  public void setStackedBackgroundDrawable(Drawable paramDrawable) {}
  
  public void setSplitBackgroundDrawable(Drawable paramDrawable) {}
  
  public void setHomeButtonEnabled(boolean paramBoolean) {}
  
  public void setElevation(float paramFloat) {
    this.mDecorToolbar.getViewGroup().setElevation(paramFloat);
  }
  
  public float getElevation() {
    return this.mDecorToolbar.getViewGroup().getElevation();
  }
  
  public Context getThemedContext() {
    return this.mDecorToolbar.getContext();
  }
  
  public boolean isTitleTruncated() {
    return super.isTitleTruncated();
  }
  
  public void setHomeAsUpIndicator(Drawable paramDrawable) {
    this.mDecorToolbar.setNavigationIcon(paramDrawable);
  }
  
  public void setHomeAsUpIndicator(int paramInt) {
    this.mDecorToolbar.setNavigationIcon(paramInt);
  }
  
  public void setHomeActionContentDescription(CharSequence paramCharSequence) {
    this.mDecorToolbar.setNavigationContentDescription(paramCharSequence);
  }
  
  public void setDefaultDisplayHomeAsUpEnabled(boolean paramBoolean) {}
  
  public void setHomeActionContentDescription(int paramInt) {
    this.mDecorToolbar.setNavigationContentDescription(paramInt);
  }
  
  public void setShowHideAnimationEnabled(boolean paramBoolean) {}
  
  public void onConfigurationChanged(Configuration paramConfiguration) {
    super.onConfigurationChanged(paramConfiguration);
  }
  
  public ActionMode startActionMode(ActionMode.Callback paramCallback) {
    return null;
  }
  
  public void setListNavigationCallbacks(SpinnerAdapter paramSpinnerAdapter, ActionBar.OnNavigationListener paramOnNavigationListener) {
    this.mDecorToolbar.setDropdownParams(paramSpinnerAdapter, new NavItemSelectedListener(paramOnNavigationListener));
  }
  
  public void setSelectedNavigationItem(int paramInt) {
    if (this.mDecorToolbar.getNavigationMode() == 1) {
      this.mDecorToolbar.setDropdownSelectedPosition(paramInt);
      return;
    } 
    throw new IllegalStateException("setSelectedNavigationIndex not valid for current navigation mode");
  }
  
  public int getSelectedNavigationIndex() {
    return -1;
  }
  
  public int getNavigationItemCount() {
    return 0;
  }
  
  public void setTitle(CharSequence paramCharSequence) {
    this.mDecorToolbar.setTitle(paramCharSequence);
  }
  
  public void setTitle(int paramInt) {
    CharSequence charSequence;
    DecorToolbar decorToolbar = this.mDecorToolbar;
    if (paramInt != 0) {
      charSequence = decorToolbar.getContext().getText(paramInt);
    } else {
      charSequence = null;
    } 
    decorToolbar.setTitle(charSequence);
  }
  
  public void setWindowTitle(CharSequence paramCharSequence) {
    this.mDecorToolbar.setWindowTitle(paramCharSequence);
  }
  
  public void setSubtitle(CharSequence paramCharSequence) {
    this.mDecorToolbar.setSubtitle(paramCharSequence);
  }
  
  public void setSubtitle(int paramInt) {
    CharSequence charSequence;
    DecorToolbar decorToolbar = this.mDecorToolbar;
    if (paramInt != 0) {
      charSequence = decorToolbar.getContext().getText(paramInt);
    } else {
      charSequence = null;
    } 
    decorToolbar.setSubtitle(charSequence);
  }
  
  public void setDisplayOptions(int paramInt) {
    setDisplayOptions(paramInt, -1);
  }
  
  public void setDisplayOptions(int paramInt1, int paramInt2) {
    int i = this.mDecorToolbar.getDisplayOptions();
    this.mDecorToolbar.setDisplayOptions(paramInt1 & paramInt2 | (paramInt2 ^ 0xFFFFFFFF) & i);
  }
  
  public void setDisplayUseLogoEnabled(boolean paramBoolean) {
    setDisplayOptions(paramBoolean, 1);
  }
  
  public void setDisplayShowHomeEnabled(boolean paramBoolean) {
    boolean bool;
    if (paramBoolean) {
      bool = true;
    } else {
      bool = false;
    } 
    setDisplayOptions(bool, 2);
  }
  
  public void setDisplayHomeAsUpEnabled(boolean paramBoolean) {
    boolean bool;
    if (paramBoolean) {
      bool = true;
    } else {
      bool = false;
    } 
    setDisplayOptions(bool, 4);
  }
  
  public void setDisplayShowTitleEnabled(boolean paramBoolean) {
    boolean bool;
    if (paramBoolean) {
      bool = true;
    } else {
      bool = false;
    } 
    setDisplayOptions(bool, 8);
  }
  
  public void setDisplayShowCustomEnabled(boolean paramBoolean) {
    boolean bool;
    if (paramBoolean) {
      bool = true;
    } else {
      bool = false;
    } 
    setDisplayOptions(bool, 16);
  }
  
  public void setBackgroundDrawable(Drawable paramDrawable) {
    this.mDecorToolbar.setBackgroundDrawable(paramDrawable);
  }
  
  public View getCustomView() {
    return this.mDecorToolbar.getCustomView();
  }
  
  public CharSequence getTitle() {
    return this.mDecorToolbar.getTitle();
  }
  
  public CharSequence getSubtitle() {
    return this.mDecorToolbar.getSubtitle();
  }
  
  public int getNavigationMode() {
    return 0;
  }
  
  public void setNavigationMode(int paramInt) {
    if (paramInt != 2) {
      this.mDecorToolbar.setNavigationMode(paramInt);
      return;
    } 
    throw new IllegalArgumentException("Tabs not supported in this configuration");
  }
  
  public int getDisplayOptions() {
    return this.mDecorToolbar.getDisplayOptions();
  }
  
  public ActionBar.Tab newTab() {
    throw new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
  }
  
  public void addTab(ActionBar.Tab paramTab) {
    throw new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
  }
  
  public void addTab(ActionBar.Tab paramTab, boolean paramBoolean) {
    throw new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
  }
  
  public void addTab(ActionBar.Tab paramTab, int paramInt) {
    throw new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
  }
  
  public void addTab(ActionBar.Tab paramTab, int paramInt, boolean paramBoolean) {
    throw new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
  }
  
  public void removeTab(ActionBar.Tab paramTab) {
    throw new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
  }
  
  public void removeTabAt(int paramInt) {
    throw new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
  }
  
  public void removeAllTabs() {
    throw new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
  }
  
  public void selectTab(ActionBar.Tab paramTab) {
    throw new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
  }
  
  public ActionBar.Tab getSelectedTab() {
    throw new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
  }
  
  public ActionBar.Tab getTabAt(int paramInt) {
    throw new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
  }
  
  public int getTabCount() {
    return 0;
  }
  
  public int getHeight() {
    return this.mDecorToolbar.getHeight();
  }
  
  public void show() {
    this.mDecorToolbar.setVisibility(0);
  }
  
  public void hide() {
    this.mDecorToolbar.setVisibility(8);
  }
  
  public boolean isShowing() {
    boolean bool;
    if (this.mDecorToolbar.getVisibility() == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean openOptionsMenu() {
    return this.mDecorToolbar.showOverflowMenu();
  }
  
  public boolean closeOptionsMenu() {
    return this.mDecorToolbar.hideOverflowMenu();
  }
  
  public boolean invalidateOptionsMenu() {
    this.mDecorToolbar.getViewGroup().removeCallbacks(this.mMenuInvalidator);
    this.mDecorToolbar.getViewGroup().postOnAnimation(this.mMenuInvalidator);
    return true;
  }
  
  public boolean collapseActionView() {
    if (this.mDecorToolbar.hasExpandedActionView()) {
      this.mDecorToolbar.collapseActionView();
      return true;
    } 
    return false;
  }
  
  void populateOptionsMenu() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mMenuCallbackSet : Z
    //   4: ifne -> 39
    //   7: aload_0
    //   8: getfield mDecorToolbar : Lcom/android/internal/widget/DecorToolbar;
    //   11: new com/android/internal/app/ToolbarActionBar$ActionMenuPresenterCallback
    //   14: dup
    //   15: aload_0
    //   16: aconst_null
    //   17: invokespecial <init> : (Lcom/android/internal/app/ToolbarActionBar;Lcom/android/internal/app/ToolbarActionBar$1;)V
    //   20: new com/android/internal/app/ToolbarActionBar$MenuBuilderCallback
    //   23: dup
    //   24: aload_0
    //   25: aconst_null
    //   26: invokespecial <init> : (Lcom/android/internal/app/ToolbarActionBar;Lcom/android/internal/app/ToolbarActionBar$1;)V
    //   29: invokeinterface setMenuCallbacks : (Lcom/android/internal/view/menu/MenuPresenter$Callback;Lcom/android/internal/view/menu/MenuBuilder$Callback;)V
    //   34: aload_0
    //   35: iconst_1
    //   36: putfield mMenuCallbackSet : Z
    //   39: aload_0
    //   40: getfield mDecorToolbar : Lcom/android/internal/widget/DecorToolbar;
    //   43: invokeinterface getMenu : ()Landroid/view/Menu;
    //   48: astore_1
    //   49: aload_1
    //   50: instanceof com/android/internal/view/menu/MenuBuilder
    //   53: ifeq -> 64
    //   56: aload_1
    //   57: checkcast com/android/internal/view/menu/MenuBuilder
    //   60: astore_2
    //   61: goto -> 66
    //   64: aconst_null
    //   65: astore_2
    //   66: aload_2
    //   67: ifnull -> 74
    //   70: aload_2
    //   71: invokevirtual stopDispatchingItemsChanged : ()V
    //   74: aload_1
    //   75: invokeinterface clear : ()V
    //   80: aload_0
    //   81: getfield mWindowCallback : Landroid/view/Window$Callback;
    //   84: iconst_0
    //   85: aload_1
    //   86: invokeinterface onCreatePanelMenu : (ILandroid/view/Menu;)Z
    //   91: ifeq -> 111
    //   94: aload_0
    //   95: getfield mWindowCallback : Landroid/view/Window$Callback;
    //   98: astore_3
    //   99: aload_3
    //   100: iconst_0
    //   101: aconst_null
    //   102: aload_1
    //   103: invokeinterface onPreparePanel : (ILandroid/view/View;Landroid/view/Menu;)Z
    //   108: ifne -> 117
    //   111: aload_1
    //   112: invokeinterface clear : ()V
    //   117: aload_2
    //   118: ifnull -> 125
    //   121: aload_2
    //   122: invokevirtual startDispatchingItemsChanged : ()V
    //   125: return
    //   126: astore_1
    //   127: aload_2
    //   128: ifnull -> 135
    //   131: aload_2
    //   132: invokevirtual startDispatchingItemsChanged : ()V
    //   135: aload_1
    //   136: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #443	-> 0
    //   #444	-> 7
    //   #445	-> 34
    //   #447	-> 39
    //   #448	-> 49
    //   #449	-> 66
    //   #450	-> 70
    //   #453	-> 74
    //   #454	-> 80
    //   #455	-> 99
    //   #456	-> 111
    //   #459	-> 117
    //   #460	-> 121
    //   #463	-> 125
    //   #459	-> 126
    //   #460	-> 131
    //   #462	-> 135
    // Exception table:
    //   from	to	target	type
    //   74	80	126	finally
    //   80	99	126	finally
    //   99	111	126	finally
    //   111	117	126	finally
  }
  
  public boolean onMenuKeyEvent(KeyEvent paramKeyEvent) {
    if (paramKeyEvent.getAction() == 1)
      openOptionsMenu(); 
    return true;
  }
  
  public boolean onKeyShortcut(int paramInt, KeyEvent paramKeyEvent) {
    Menu menu = this.mDecorToolbar.getMenu();
    if (menu != null) {
      if (paramKeyEvent != null) {
        i = paramKeyEvent.getDeviceId();
      } else {
        i = -1;
      } 
      KeyCharacterMap keyCharacterMap = KeyCharacterMap.load(i);
      int i = keyCharacterMap.getKeyboardType();
      boolean bool = true;
      if (i == 1)
        bool = false; 
      menu.setQwertyMode(bool);
      return menu.performShortcut(paramInt, paramKeyEvent, 0);
    } 
    return false;
  }
  
  public void onDestroy() {
    this.mDecorToolbar.getViewGroup().removeCallbacks(this.mMenuInvalidator);
  }
  
  public void addOnMenuVisibilityListener(ActionBar.OnMenuVisibilityListener paramOnMenuVisibilityListener) {
    this.mMenuVisibilityListeners.add(paramOnMenuVisibilityListener);
  }
  
  public void removeOnMenuVisibilityListener(ActionBar.OnMenuVisibilityListener paramOnMenuVisibilityListener) {
    this.mMenuVisibilityListeners.remove(paramOnMenuVisibilityListener);
  }
  
  public void dispatchMenuVisibilityChanged(boolean paramBoolean) {
    if (paramBoolean == this.mLastMenuVisibility)
      return; 
    this.mLastMenuVisibility = paramBoolean;
    int i = this.mMenuVisibilityListeners.size();
    for (byte b = 0; b < i; b++)
      ((ActionBar.OnMenuVisibilityListener)this.mMenuVisibilityListeners.get(b)).onMenuVisibilityChanged(paramBoolean); 
  }
  
  class ToolbarCallbackWrapper extends WindowCallbackWrapper {
    final ToolbarActionBar this$0;
    
    public ToolbarCallbackWrapper(Window.Callback param1Callback) {
      super(param1Callback);
    }
    
    public boolean onPreparePanel(int param1Int, View param1View, Menu param1Menu) {
      boolean bool = super.onPreparePanel(param1Int, param1View, param1Menu);
      if (bool && !ToolbarActionBar.this.mToolbarMenuPrepared) {
        ToolbarActionBar.this.mDecorToolbar.setMenuPrepared();
        ToolbarActionBar.access$302(ToolbarActionBar.this, true);
      } 
      return bool;
    }
    
    public View onCreatePanelView(int param1Int) {
      if (param1Int == 0)
        return new View(ToolbarActionBar.this.mDecorToolbar.getContext()); 
      return super.onCreatePanelView(param1Int);
    }
  }
  
  private final class ActionMenuPresenterCallback implements MenuPresenter.Callback {
    private boolean mClosingActionMenu;
    
    final ToolbarActionBar this$0;
    
    private ActionMenuPresenterCallback() {}
    
    public boolean onOpenSubMenu(MenuBuilder param1MenuBuilder) {
      if (ToolbarActionBar.this.mWindowCallback != null) {
        ToolbarActionBar.this.mWindowCallback.onMenuOpened(8, (Menu)param1MenuBuilder);
        return true;
      } 
      return false;
    }
    
    public void onCloseMenu(MenuBuilder param1MenuBuilder, boolean param1Boolean) {
      if (this.mClosingActionMenu)
        return; 
      this.mClosingActionMenu = true;
      ToolbarActionBar.this.mDecorToolbar.dismissPopupMenus();
      if (ToolbarActionBar.this.mWindowCallback != null)
        ToolbarActionBar.this.mWindowCallback.onPanelClosed(8, (Menu)param1MenuBuilder); 
      this.mClosingActionMenu = false;
    }
  }
  
  private final class MenuBuilderCallback implements MenuBuilder.Callback {
    final ToolbarActionBar this$0;
    
    private MenuBuilderCallback() {}
    
    public boolean onMenuItemSelected(MenuBuilder param1MenuBuilder, MenuItem param1MenuItem) {
      return false;
    }
    
    public void onMenuModeChange(MenuBuilder param1MenuBuilder) {
      if (ToolbarActionBar.this.mWindowCallback != null)
        if (ToolbarActionBar.this.mDecorToolbar.isOverflowMenuShowing()) {
          ToolbarActionBar.this.mWindowCallback.onPanelClosed(8, (Menu)param1MenuBuilder);
        } else if (ToolbarActionBar.this.mWindowCallback.onPreparePanel(0, null, (Menu)param1MenuBuilder)) {
          ToolbarActionBar.this.mWindowCallback.onMenuOpened(8, (Menu)param1MenuBuilder);
        }  
    }
  }
}
