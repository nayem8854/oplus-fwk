package com.android.internal.app;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ChooserUtil {
  private static final Charset UTF_8 = Charset.forName("UTF-8");
  
  public static String md5(String paramString) {
    try {
      MessageDigest messageDigest = MessageDigest.getInstance("MD5");
      messageDigest.update(paramString.getBytes(UTF_8));
      return convertBytesToHexString(messageDigest.digest());
    } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
      throw new IllegalStateException(noSuchAlgorithmException);
    } 
  }
  
  private static String convertBytesToHexString(byte[] paramArrayOfbyte) {
    char[] arrayOfChar = new char[paramArrayOfbyte.length * 2];
    for (byte b = 0; b < paramArrayOfbyte.length; b++) {
      byte b1 = paramArrayOfbyte[b];
      arrayOfChar[b * 2] = Character.forDigit(b1 >> 4 & 0xF, 16);
      arrayOfChar[b * 2 + 1] = Character.forDigit(b1 & 0xF, 16);
    } 
    return new String(arrayOfChar);
  }
}
