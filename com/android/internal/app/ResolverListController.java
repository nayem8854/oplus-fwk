package com.android.internal.app;

import android.app.AppGlobals;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.IPackageManager;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.RemoteException;
import android.os.UserHandle;
import android.util.Log;
import com.android.internal.app.chooser.DisplayResolveInfo;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;
import java.util.concurrent.CountDownLatch;

public class ResolverListController {
  private static final boolean DEBUG = false;
  
  private static final String TAG = "ResolverListController";
  
  private boolean isComputed = false;
  
  private final Context mContext;
  
  private final int mLaunchedFromUid;
  
  private final String mReferrerPackage;
  
  private AbstractResolverComparator mResolverComparator;
  
  private final Intent mTargetIntent;
  
  private final UserHandle mUserHandle;
  
  private final PackageManager mpm;
  
  public ResolverListController(Context paramContext, PackageManager paramPackageManager, Intent paramIntent, String paramString, int paramInt, UserHandle paramUserHandle) {
    this(paramContext, paramPackageManager, paramIntent, paramString, paramInt, paramUserHandle, new ResolverRankerServiceResolverComparator(paramContext, paramIntent, paramString, null));
  }
  
  public ResolverListController(Context paramContext, PackageManager paramPackageManager, Intent paramIntent, String paramString, int paramInt, UserHandle paramUserHandle, AbstractResolverComparator paramAbstractResolverComparator) {
    this.mContext = paramContext;
    this.mpm = paramPackageManager;
    this.mLaunchedFromUid = paramInt;
    this.mTargetIntent = paramIntent;
    this.mReferrerPackage = paramString;
    this.mUserHandle = paramUserHandle;
    this.mResolverComparator = paramAbstractResolverComparator;
  }
  
  public ResolveInfo getLastChosen() throws RemoteException {
    IPackageManager iPackageManager = AppGlobals.getPackageManager();
    Intent intent = this.mTargetIntent;
    Context context = this.mContext;
    String str = intent.resolveTypeIfNeeded(context.getContentResolver());
    return iPackageManager.getLastChosenActivity(intent, str, 65536);
  }
  
  public void setLastChosen(Intent paramIntent, IntentFilter paramIntentFilter, int paramInt) throws RemoteException {
    IPackageManager iPackageManager = AppGlobals.getPackageManager();
    Context context = this.mContext;
    String str = paramIntent.resolveType(context.getContentResolver());
    ComponentName componentName = paramIntent.getComponent();
    iPackageManager.setLastChosenActivity(paramIntent, str, 65536, paramIntentFilter, paramInt, componentName);
  }
  
  public List<ResolverActivity.ResolvedComponentInfo> getResolversForIntent(boolean paramBoolean1, boolean paramBoolean2, List<Intent> paramList) {
    return getResolversForIntentAsUser(paramBoolean1, paramBoolean2, paramList, this.mUserHandle);
  }
  
  public List<ResolverActivity.ResolvedComponentInfo> getResolversForIntentAsUser(boolean paramBoolean1, boolean paramBoolean2, List<Intent> paramList, UserHandle paramUserHandle) {
    boolean bool;
    char c = Character.MIN_VALUE;
    if (paramBoolean1) {
      bool = true;
    } else {
      bool = false;
    } 
    if (paramBoolean2)
      c = ''; 
    return getResolversForIntentAsUserInternal(paramList, paramUserHandle, c | bool | 0xD0000);
  }
  
  private List<ResolverActivity.ResolvedComponentInfo> getResolversForIntentAsUserInternal(List<Intent> paramList, UserHandle paramUserHandle, int paramInt) {
    // Byte code:
    //   0: aconst_null
    //   1: astore #4
    //   3: iconst_0
    //   4: istore #5
    //   6: aload_1
    //   7: invokeinterface size : ()I
    //   12: istore #6
    //   14: iload #5
    //   16: iload #6
    //   18: if_icmpge -> 129
    //   21: aload_1
    //   22: iload #5
    //   24: invokeinterface get : (I)Ljava/lang/Object;
    //   29: checkcast android/content/Intent
    //   32: astore #7
    //   34: iload_3
    //   35: istore #8
    //   37: aload #7
    //   39: invokevirtual isWebIntent : ()Z
    //   42: ifne -> 61
    //   45: iload #8
    //   47: istore #9
    //   49: aload #7
    //   51: invokevirtual getFlags : ()I
    //   54: sipush #2048
    //   57: iand
    //   58: ifeq -> 68
    //   61: iload #8
    //   63: ldc 8388608
    //   65: ior
    //   66: istore #9
    //   68: aload_0
    //   69: getfield mpm : Landroid/content/pm/PackageManager;
    //   72: aload #7
    //   74: iload #9
    //   76: aload_2
    //   77: invokevirtual queryIntentActivitiesAsUser : (Landroid/content/Intent;ILandroid/os/UserHandle;)Ljava/util/List;
    //   80: astore #10
    //   82: aload #4
    //   84: astore #11
    //   86: aload #10
    //   88: ifnull -> 119
    //   91: aload #4
    //   93: astore #11
    //   95: aload #4
    //   97: ifnonnull -> 109
    //   100: new java/util/ArrayList
    //   103: dup
    //   104: invokespecial <init> : ()V
    //   107: astore #11
    //   109: aload_0
    //   110: aload #11
    //   112: aload #7
    //   114: aload #10
    //   116: invokevirtual addResolveListDedupe : (Ljava/util/List;Landroid/content/Intent;Ljava/util/List;)V
    //   119: iinc #5, 1
    //   122: aload #11
    //   124: astore #4
    //   126: goto -> 14
    //   129: aload #4
    //   131: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #135	-> 0
    //   #136	-> 3
    //   #137	-> 21
    //   #138	-> 34
    //   #139	-> 37
    //   #140	-> 45
    //   #141	-> 61
    //   #143	-> 68
    //   #145	-> 82
    //   #146	-> 91
    //   #147	-> 100
    //   #149	-> 109
    //   #136	-> 119
    //   #152	-> 129
  }
  
  public UserHandle getUserHandle() {
    return this.mUserHandle;
  }
  
  public void addResolveListDedupe(List<ResolverActivity.ResolvedComponentInfo> paramList, Intent paramIntent, List<ResolveInfo> paramList1) {
    int i = paramList1.size();
    int j = paramList.size();
    for (byte b = 0; b < i; b++) {
      boolean bool2;
      ResolveInfo resolveInfo = paramList1.get(b);
      boolean bool1 = false;
      byte b1 = 0;
      while (true) {
        bool2 = bool1;
        if (b1 < j) {
          ResolverActivity.ResolvedComponentInfo resolvedComponentInfo = paramList.get(b1);
          if (isSameResolvedComponent(resolveInfo, resolvedComponentInfo)) {
            bool2 = true;
            resolvedComponentInfo.add(paramIntent, resolveInfo);
            break;
          } 
          b1++;
          continue;
        } 
        break;
      } 
      if (!bool2) {
        ComponentName componentName = new ComponentName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
        ResolverActivity.ResolvedComponentInfo resolvedComponentInfo = new ResolverActivity.ResolvedComponentInfo(componentName, paramIntent, resolveInfo);
        resolvedComponentInfo.setPinned(isComponentPinned(componentName));
        paramList.add(resolvedComponentInfo);
      } 
    } 
  }
  
  public boolean isComponentPinned(ComponentName paramComponentName) {
    return false;
  }
  
  public ArrayList<ResolverActivity.ResolvedComponentInfo> filterIneligibleActivities(List<ResolverActivity.ResolvedComponentInfo> paramList, boolean paramBoolean) {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: aload_1
    //   3: invokeinterface size : ()I
    //   8: iconst_1
    //   9: isub
    //   10: istore #4
    //   12: iload #4
    //   14: iflt -> 130
    //   17: aload_1
    //   18: iload #4
    //   20: invokeinterface get : (I)Ljava/lang/Object;
    //   25: checkcast com/android/internal/app/ResolverActivity$ResolvedComponentInfo
    //   28: astore #5
    //   30: aload #5
    //   32: iconst_0
    //   33: invokevirtual getResolveInfoAt : (I)Landroid/content/pm/ResolveInfo;
    //   36: getfield activityInfo : Landroid/content/pm/ActivityInfo;
    //   39: astore #6
    //   41: aload #6
    //   43: getfield permission : Ljava/lang/String;
    //   46: aload_0
    //   47: getfield mLaunchedFromUid : I
    //   50: aload #6
    //   52: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   55: getfield uid : I
    //   58: aload #6
    //   60: getfield exported : Z
    //   63: invokestatic checkComponentPermission : (Ljava/lang/String;IIZ)I
    //   66: istore #7
    //   68: iload #7
    //   70: ifne -> 88
    //   73: aload_3
    //   74: astore #5
    //   76: aload_0
    //   77: aload #6
    //   79: invokevirtual getComponentName : ()Landroid/content/ComponentName;
    //   82: invokevirtual isComponentFiltered : (Landroid/content/ComponentName;)Z
    //   85: ifeq -> 121
    //   88: aload_3
    //   89: astore #5
    //   91: iload_2
    //   92: ifeq -> 112
    //   95: aload_3
    //   96: astore #5
    //   98: aload_3
    //   99: ifnonnull -> 112
    //   102: new java/util/ArrayList
    //   105: dup
    //   106: aload_1
    //   107: invokespecial <init> : (Ljava/util/Collection;)V
    //   110: astore #5
    //   112: aload_1
    //   113: iload #4
    //   115: invokeinterface remove : (I)Ljava/lang/Object;
    //   120: pop
    //   121: iinc #4, -1
    //   124: aload #5
    //   126: astore_3
    //   127: goto -> 12
    //   130: aload_3
    //   131: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #205	-> 0
    //   #206	-> 2
    //   #207	-> 17
    //   #208	-> 30
    //   #209	-> 41
    //   #213	-> 68
    //   #214	-> 73
    //   #217	-> 88
    //   #218	-> 102
    //   #220	-> 112
    //   #206	-> 121
    //   #223	-> 130
  }
  
  public ArrayList<ResolverActivity.ResolvedComponentInfo> filterLowPriority(List<ResolverActivity.ResolvedComponentInfo> paramList, boolean paramBoolean) {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: aload_1
    //   3: iconst_0
    //   4: invokeinterface get : (I)Ljava/lang/Object;
    //   9: checkcast com/android/internal/app/ResolverActivity$ResolvedComponentInfo
    //   12: astore #4
    //   14: aload #4
    //   16: iconst_0
    //   17: invokevirtual getResolveInfoAt : (I)Landroid/content/pm/ResolveInfo;
    //   20: astore #5
    //   22: aload_1
    //   23: invokeinterface size : ()I
    //   28: istore #6
    //   30: iconst_1
    //   31: istore #7
    //   33: iload #7
    //   35: iload #6
    //   37: if_icmpge -> 174
    //   40: aload_1
    //   41: iload #7
    //   43: invokeinterface get : (I)Ljava/lang/Object;
    //   48: checkcast com/android/internal/app/ResolverActivity$ResolvedComponentInfo
    //   51: iconst_0
    //   52: invokevirtual getResolveInfoAt : (I)Landroid/content/pm/ResolveInfo;
    //   55: astore #8
    //   57: aload_3
    //   58: astore #4
    //   60: iload #6
    //   62: istore #9
    //   64: aload #5
    //   66: getfield priority : I
    //   69: aload #8
    //   71: getfield priority : I
    //   74: if_icmpne -> 104
    //   77: aload_3
    //   78: astore #10
    //   80: iload #6
    //   82: istore #11
    //   84: aload #5
    //   86: getfield isDefault : Z
    //   89: aload #8
    //   91: getfield isDefault : Z
    //   94: if_icmpeq -> 161
    //   97: iload #6
    //   99: istore #9
    //   101: aload_3
    //   102: astore #4
    //   104: aload #4
    //   106: astore #10
    //   108: iload #9
    //   110: istore #11
    //   112: iload #7
    //   114: iload #9
    //   116: if_icmpge -> 161
    //   119: aload #4
    //   121: astore_3
    //   122: iload_2
    //   123: ifeq -> 143
    //   126: aload #4
    //   128: astore_3
    //   129: aload #4
    //   131: ifnonnull -> 143
    //   134: new java/util/ArrayList
    //   137: dup
    //   138: aload_1
    //   139: invokespecial <init> : (Ljava/util/Collection;)V
    //   142: astore_3
    //   143: aload_1
    //   144: iload #7
    //   146: invokeinterface remove : (I)Ljava/lang/Object;
    //   151: pop
    //   152: iinc #9, -1
    //   155: aload_3
    //   156: astore #4
    //   158: goto -> 104
    //   161: iinc #7, 1
    //   164: aload #10
    //   166: astore_3
    //   167: iload #11
    //   169: istore #6
    //   171: goto -> 33
    //   174: aload_3
    //   175: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #234	-> 0
    //   #237	-> 2
    //   #238	-> 14
    //   #239	-> 22
    //   #240	-> 30
    //   #241	-> 40
    //   #248	-> 57
    //   #250	-> 104
    //   #251	-> 119
    //   #252	-> 134
    //   #254	-> 143
    //   #255	-> 152
    //   #240	-> 161
    //   #259	-> 174
  }
  
  class ComputeCallback implements AbstractResolverComparator.AfterCompute {
    private CountDownLatch mFinishComputeSignal;
    
    final ResolverListController this$0;
    
    public ComputeCallback(CountDownLatch param1CountDownLatch) {
      this.mFinishComputeSignal = param1CountDownLatch;
    }
    
    public void afterCompute() {
      this.mFinishComputeSignal.countDown();
    }
  }
  
  private void compute(List<ResolverActivity.ResolvedComponentInfo> paramList) throws InterruptedException {
    if (this.mResolverComparator == null) {
      Log.d("ResolverListController", "Comparator has already been destroyed; skipped.");
      return;
    } 
    CountDownLatch countDownLatch = new CountDownLatch(1);
    ComputeCallback computeCallback = new ComputeCallback(countDownLatch);
    this.mResolverComparator.setCallBack(computeCallback);
    this.mResolverComparator.compute(paramList);
    countDownLatch.await();
    this.isComputed = true;
  }
  
  public void sort(List<ResolverActivity.ResolvedComponentInfo> paramList) {
    try {
      System.currentTimeMillis();
      if (!this.isComputed)
        compute(paramList); 
      Collections.sort(paramList, this.mResolverComparator);
      System.currentTimeMillis();
    } catch (InterruptedException interruptedException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Compute & Sort was interrupted: ");
      stringBuilder.append(interruptedException);
      Log.e("ResolverListController", stringBuilder.toString());
    } 
  }
  
  public void topK(List<ResolverActivity.ResolvedComponentInfo> paramList, int paramInt) {
    if (paramList == null || paramList.isEmpty() || paramInt <= 0)
      return; 
    if (paramList.size() <= paramInt) {
      sort(paramList);
      return;
    } 
    try {
      System.currentTimeMillis();
      if (!this.isComputed)
        compute(paramList); 
      PriorityQueue<ResolverActivity.ResolvedComponentInfo> priorityQueue = new PriorityQueue();
      _$$Lambda$ResolverListController$1yUwnJRSKoStFZLoSuluJkf2p0E _$$Lambda$ResolverListController$1yUwnJRSKoStFZLoSuluJkf2p0E = new _$$Lambda$ResolverListController$1yUwnJRSKoStFZLoSuluJkf2p0E();
      this(this);
      this(paramInt, _$$Lambda$ResolverListController$1yUwnJRSKoStFZLoSuluJkf2p0E);
      int i = paramList.size();
      int j = i - 1;
      priorityQueue.addAll(paramList.subList(i - paramInt, i));
      i = i - paramInt - 1;
      paramInt = j;
      while (true) {
        j = paramInt;
        if (i >= 0) {
          ResolverActivity.ResolvedComponentInfo resolvedComponentInfo = paramList.get(i);
          if (-this.mResolverComparator.compare(resolvedComponentInfo, (ResolverActivity.ResolvedComponentInfo)priorityQueue.peek()) > 0) {
            paramList.set(paramInt, priorityQueue.poll());
            priorityQueue.add(resolvedComponentInfo);
            paramInt--;
          } else {
            paramList.set(paramInt, resolvedComponentInfo);
            paramInt--;
          } 
          i--;
          continue;
        } 
        break;
      } 
      while (!priorityQueue.isEmpty()) {
        paramList.set(j, priorityQueue.poll());
        j--;
      } 
      System.currentTimeMillis();
    } catch (InterruptedException interruptedException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Compute & greatestOf was interrupted: ");
      stringBuilder.append(interruptedException);
      Log.e("ResolverListController", stringBuilder.toString());
    } 
  }
  
  private static boolean isSameResolvedComponent(ResolveInfo paramResolveInfo, ResolverActivity.ResolvedComponentInfo paramResolvedComponentInfo) {
    ActivityInfo activityInfo = paramResolveInfo.activityInfo;
    if (activityInfo.packageName.equals(paramResolvedComponentInfo.name.getPackageName())) {
      String str = activityInfo.name;
      ComponentName componentName = paramResolvedComponentInfo.name;
      if (str.equals(componentName.getClassName()))
        return true; 
    } 
    return false;
  }
  
  boolean isComponentFiltered(ComponentName paramComponentName) {
    return false;
  }
  
  public float getScore(DisplayResolveInfo paramDisplayResolveInfo) {
    return this.mResolverComparator.getScore(paramDisplayResolveInfo.getResolvedComponentName());
  }
  
  public float getScore(ComponentName paramComponentName) {
    return this.mResolverComparator.getScore(paramComponentName);
  }
  
  public List<ComponentName> getTopComponentNames(int paramInt) {
    return this.mResolverComparator.getTopComponentNames(paramInt);
  }
  
  public void updateModel(ComponentName paramComponentName) {
    this.mResolverComparator.updateModel(paramComponentName);
  }
  
  public void updateChooserCounts(String paramString1, int paramInt, String paramString2) {
    this.mResolverComparator.updateChooserCounts(paramString1, paramInt, paramString2);
  }
  
  public void destroy() {
    this.mResolverComparator.destroy();
  }
}
