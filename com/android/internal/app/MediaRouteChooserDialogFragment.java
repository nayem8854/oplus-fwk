package com.android.internal.app;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

public class MediaRouteChooserDialogFragment extends DialogFragment {
  private final String ARGUMENT_ROUTE_TYPES;
  
  private View.OnClickListener mExtendedSettingsClickListener;
  
  public MediaRouteChooserDialogFragment() {
    int i;
    this.ARGUMENT_ROUTE_TYPES = "routeTypes";
    if (MediaRouteChooserDialog.isLightTheme(getContext())) {
      i = 16974130;
    } else {
      i = 16974126;
    } 
    setCancelable(true);
    setStyle(0, i);
  }
  
  public int getRouteTypes() {
    boolean bool;
    Bundle bundle = getArguments();
    if (bundle != null) {
      bool = bundle.getInt("routeTypes");
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setRouteTypes(int paramInt) {
    if (paramInt != getRouteTypes()) {
      Bundle bundle1 = getArguments();
      Bundle bundle2 = bundle1;
      if (bundle1 == null)
        bundle2 = new Bundle(); 
      bundle2.putInt("routeTypes", paramInt);
      setArguments(bundle2);
      MediaRouteChooserDialog mediaRouteChooserDialog = (MediaRouteChooserDialog)getDialog();
      if (mediaRouteChooserDialog != null)
        mediaRouteChooserDialog.setRouteTypes(paramInt); 
    } 
  }
  
  public void setExtendedSettingsClickListener(View.OnClickListener paramOnClickListener) {
    if (paramOnClickListener != this.mExtendedSettingsClickListener) {
      this.mExtendedSettingsClickListener = paramOnClickListener;
      MediaRouteChooserDialog mediaRouteChooserDialog = (MediaRouteChooserDialog)getDialog();
      if (mediaRouteChooserDialog != null)
        mediaRouteChooserDialog.setExtendedSettingsClickListener(paramOnClickListener); 
    } 
  }
  
  public MediaRouteChooserDialog onCreateChooserDialog(Context paramContext, Bundle paramBundle) {
    return new MediaRouteChooserDialog(paramContext, getTheme());
  }
  
  public Dialog onCreateDialog(Bundle paramBundle) {
    MediaRouteChooserDialog mediaRouteChooserDialog = onCreateChooserDialog((Context)getActivity(), paramBundle);
    mediaRouteChooserDialog.setRouteTypes(getRouteTypes());
    mediaRouteChooserDialog.setExtendedSettingsClickListener(this.mExtendedSettingsClickListener);
    return mediaRouteChooserDialog;
  }
}
