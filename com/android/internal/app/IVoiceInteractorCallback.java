package com.android.internal.app;

import android.app.VoiceInteractor;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public interface IVoiceInteractorCallback extends IInterface {
  void deliverAbortVoiceResult(IVoiceInteractorRequest paramIVoiceInteractorRequest, Bundle paramBundle) throws RemoteException;
  
  void deliverCancel(IVoiceInteractorRequest paramIVoiceInteractorRequest) throws RemoteException;
  
  void deliverCommandResult(IVoiceInteractorRequest paramIVoiceInteractorRequest, boolean paramBoolean, Bundle paramBundle) throws RemoteException;
  
  void deliverCompleteVoiceResult(IVoiceInteractorRequest paramIVoiceInteractorRequest, Bundle paramBundle) throws RemoteException;
  
  void deliverConfirmationResult(IVoiceInteractorRequest paramIVoiceInteractorRequest, boolean paramBoolean, Bundle paramBundle) throws RemoteException;
  
  void deliverPickOptionResult(IVoiceInteractorRequest paramIVoiceInteractorRequest, boolean paramBoolean, VoiceInteractor.PickOptionRequest.Option[] paramArrayOfOption, Bundle paramBundle) throws RemoteException;
  
  void destroy() throws RemoteException;
  
  class Default implements IVoiceInteractorCallback {
    public void deliverConfirmationResult(IVoiceInteractorRequest param1IVoiceInteractorRequest, boolean param1Boolean, Bundle param1Bundle) throws RemoteException {}
    
    public void deliverPickOptionResult(IVoiceInteractorRequest param1IVoiceInteractorRequest, boolean param1Boolean, VoiceInteractor.PickOptionRequest.Option[] param1ArrayOfOption, Bundle param1Bundle) throws RemoteException {}
    
    public void deliverCompleteVoiceResult(IVoiceInteractorRequest param1IVoiceInteractorRequest, Bundle param1Bundle) throws RemoteException {}
    
    public void deliverAbortVoiceResult(IVoiceInteractorRequest param1IVoiceInteractorRequest, Bundle param1Bundle) throws RemoteException {}
    
    public void deliverCommandResult(IVoiceInteractorRequest param1IVoiceInteractorRequest, boolean param1Boolean, Bundle param1Bundle) throws RemoteException {}
    
    public void deliverCancel(IVoiceInteractorRequest param1IVoiceInteractorRequest) throws RemoteException {}
    
    public void destroy() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IVoiceInteractorCallback {
    private static final String DESCRIPTOR = "com.android.internal.app.IVoiceInteractorCallback";
    
    static final int TRANSACTION_deliverAbortVoiceResult = 4;
    
    static final int TRANSACTION_deliverCancel = 6;
    
    static final int TRANSACTION_deliverCommandResult = 5;
    
    static final int TRANSACTION_deliverCompleteVoiceResult = 3;
    
    static final int TRANSACTION_deliverConfirmationResult = 1;
    
    static final int TRANSACTION_deliverPickOptionResult = 2;
    
    static final int TRANSACTION_destroy = 7;
    
    public Stub() {
      attachInterface(this, "com.android.internal.app.IVoiceInteractorCallback");
    }
    
    public static IVoiceInteractorCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.app.IVoiceInteractorCallback");
      if (iInterface != null && iInterface instanceof IVoiceInteractorCallback)
        return (IVoiceInteractorCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 7:
          return "destroy";
        case 6:
          return "deliverCancel";
        case 5:
          return "deliverCommandResult";
        case 4:
          return "deliverAbortVoiceResult";
        case 3:
          return "deliverCompleteVoiceResult";
        case 2:
          return "deliverPickOptionResult";
        case 1:
          break;
      } 
      return "deliverConfirmationResult";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IVoiceInteractorRequest iVoiceInteractorRequest;
      if (param1Int1 != 1598968902) {
        IVoiceInteractorRequest iVoiceInteractorRequest1, iVoiceInteractorRequest2;
        VoiceInteractor.PickOptionRequest.Option[] arrayOfOption;
        IVoiceInteractorRequest iVoiceInteractorRequest3;
        boolean bool1 = false, bool2 = false, bool3 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 7:
            param1Parcel1.enforceInterface("com.android.internal.app.IVoiceInteractorCallback");
            destroy();
            return true;
          case 6:
            param1Parcel1.enforceInterface("com.android.internal.app.IVoiceInteractorCallback");
            iVoiceInteractorRequest1 = IVoiceInteractorRequest.Stub.asInterface(param1Parcel1.readStrongBinder());
            deliverCancel(iVoiceInteractorRequest1);
            return true;
          case 5:
            iVoiceInteractorRequest1.enforceInterface("com.android.internal.app.IVoiceInteractorCallback");
            iVoiceInteractorRequest2 = IVoiceInteractorRequest.Stub.asInterface(iVoiceInteractorRequest1.readStrongBinder());
            if (iVoiceInteractorRequest1.readInt() != 0)
              bool3 = true; 
            if (iVoiceInteractorRequest1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iVoiceInteractorRequest1);
            } else {
              iVoiceInteractorRequest1 = null;
            } 
            deliverCommandResult(iVoiceInteractorRequest2, bool3, (Bundle)iVoiceInteractorRequest1);
            return true;
          case 4:
            iVoiceInteractorRequest1.enforceInterface("com.android.internal.app.IVoiceInteractorCallback");
            iVoiceInteractorRequest2 = IVoiceInteractorRequest.Stub.asInterface(iVoiceInteractorRequest1.readStrongBinder());
            if (iVoiceInteractorRequest1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iVoiceInteractorRequest1);
            } else {
              iVoiceInteractorRequest1 = null;
            } 
            deliverAbortVoiceResult(iVoiceInteractorRequest2, (Bundle)iVoiceInteractorRequest1);
            return true;
          case 3:
            iVoiceInteractorRequest1.enforceInterface("com.android.internal.app.IVoiceInteractorCallback");
            iVoiceInteractorRequest2 = IVoiceInteractorRequest.Stub.asInterface(iVoiceInteractorRequest1.readStrongBinder());
            if (iVoiceInteractorRequest1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iVoiceInteractorRequest1);
            } else {
              iVoiceInteractorRequest1 = null;
            } 
            deliverCompleteVoiceResult(iVoiceInteractorRequest2, (Bundle)iVoiceInteractorRequest1);
            return true;
          case 2:
            iVoiceInteractorRequest1.enforceInterface("com.android.internal.app.IVoiceInteractorCallback");
            iVoiceInteractorRequest3 = IVoiceInteractorRequest.Stub.asInterface(iVoiceInteractorRequest1.readStrongBinder());
            bool3 = bool1;
            if (iVoiceInteractorRequest1.readInt() != 0)
              bool3 = true; 
            arrayOfOption = (VoiceInteractor.PickOptionRequest.Option[])iVoiceInteractorRequest1.createTypedArray(VoiceInteractor.PickOptionRequest.Option.CREATOR);
            if (iVoiceInteractorRequest1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iVoiceInteractorRequest1);
            } else {
              iVoiceInteractorRequest1 = null;
            } 
            deliverPickOptionResult(iVoiceInteractorRequest3, bool3, arrayOfOption, (Bundle)iVoiceInteractorRequest1);
            return true;
          case 1:
            break;
        } 
        iVoiceInteractorRequest1.enforceInterface("com.android.internal.app.IVoiceInteractorCallback");
        iVoiceInteractorRequest = IVoiceInteractorRequest.Stub.asInterface(iVoiceInteractorRequest1.readStrongBinder());
        bool3 = bool2;
        if (iVoiceInteractorRequest1.readInt() != 0)
          bool3 = true; 
        if (iVoiceInteractorRequest1.readInt() != 0) {
          Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iVoiceInteractorRequest1);
        } else {
          iVoiceInteractorRequest1 = null;
        } 
        deliverConfirmationResult(iVoiceInteractorRequest, bool3, (Bundle)iVoiceInteractorRequest1);
        return true;
      } 
      iVoiceInteractorRequest.writeString("com.android.internal.app.IVoiceInteractorCallback");
      return true;
    }
    
    private static class Proxy implements IVoiceInteractorCallback {
      public static IVoiceInteractorCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.app.IVoiceInteractorCallback";
      }
      
      public void deliverConfirmationResult(IVoiceInteractorRequest param2IVoiceInteractorRequest, boolean param2Boolean, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.app.IVoiceInteractorCallback");
          if (param2IVoiceInteractorRequest != null) {
            iBinder = param2IVoiceInteractorRequest.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool1 = this.mRemote.transact(1, parcel, null, 1);
          if (!bool1 && IVoiceInteractorCallback.Stub.getDefaultImpl() != null) {
            IVoiceInteractorCallback.Stub.getDefaultImpl().deliverConfirmationResult(param2IVoiceInteractorRequest, param2Boolean, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void deliverPickOptionResult(IVoiceInteractorRequest param2IVoiceInteractorRequest, boolean param2Boolean, VoiceInteractor.PickOptionRequest.Option[] param2ArrayOfOption, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.app.IVoiceInteractorCallback");
          if (param2IVoiceInteractorRequest != null) {
            iBinder = param2IVoiceInteractorRequest.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          parcel.writeTypedArray((Parcelable[])param2ArrayOfOption, 0);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool1 = this.mRemote.transact(2, parcel, null, 1);
          if (!bool1 && IVoiceInteractorCallback.Stub.getDefaultImpl() != null) {
            IVoiceInteractorCallback.Stub.getDefaultImpl().deliverPickOptionResult(param2IVoiceInteractorRequest, param2Boolean, param2ArrayOfOption, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void deliverCompleteVoiceResult(IVoiceInteractorRequest param2IVoiceInteractorRequest, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.app.IVoiceInteractorCallback");
          if (param2IVoiceInteractorRequest != null) {
            iBinder = param2IVoiceInteractorRequest.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IVoiceInteractorCallback.Stub.getDefaultImpl() != null) {
            IVoiceInteractorCallback.Stub.getDefaultImpl().deliverCompleteVoiceResult(param2IVoiceInteractorRequest, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void deliverAbortVoiceResult(IVoiceInteractorRequest param2IVoiceInteractorRequest, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.app.IVoiceInteractorCallback");
          if (param2IVoiceInteractorRequest != null) {
            iBinder = param2IVoiceInteractorRequest.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IVoiceInteractorCallback.Stub.getDefaultImpl() != null) {
            IVoiceInteractorCallback.Stub.getDefaultImpl().deliverAbortVoiceResult(param2IVoiceInteractorRequest, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void deliverCommandResult(IVoiceInteractorRequest param2IVoiceInteractorRequest, boolean param2Boolean, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.app.IVoiceInteractorCallback");
          if (param2IVoiceInteractorRequest != null) {
            iBinder = param2IVoiceInteractorRequest.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool1 = this.mRemote.transact(5, parcel, null, 1);
          if (!bool1 && IVoiceInteractorCallback.Stub.getDefaultImpl() != null) {
            IVoiceInteractorCallback.Stub.getDefaultImpl().deliverCommandResult(param2IVoiceInteractorRequest, param2Boolean, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void deliverCancel(IVoiceInteractorRequest param2IVoiceInteractorRequest) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.app.IVoiceInteractorCallback");
          if (param2IVoiceInteractorRequest != null) {
            iBinder = param2IVoiceInteractorRequest.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IVoiceInteractorCallback.Stub.getDefaultImpl() != null) {
            IVoiceInteractorCallback.Stub.getDefaultImpl().deliverCancel(param2IVoiceInteractorRequest);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void destroy() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.app.IVoiceInteractorCallback");
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IVoiceInteractorCallback.Stub.getDefaultImpl() != null) {
            IVoiceInteractorCallback.Stub.getDefaultImpl().destroy();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IVoiceInteractorCallback param1IVoiceInteractorCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IVoiceInteractorCallback != null) {
          Proxy.sDefaultImpl = param1IVoiceInteractorCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IVoiceInteractorCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
