package com.android.internal.app;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.provider.Settings;
import android.util.Log;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

public class AssistUtils {
  private static final String TAG = "AssistUtils";
  
  private final Context mContext;
  
  private final IVoiceInteractionManagerService mVoiceInteractionManagerService;
  
  public AssistUtils(Context paramContext) {
    this.mContext = paramContext;
    IBinder iBinder = ServiceManager.getService("voiceinteraction");
    this.mVoiceInteractionManagerService = IVoiceInteractionManagerService.Stub.asInterface(iBinder);
  }
  
  public boolean showSessionForActiveService(Bundle paramBundle, int paramInt, IVoiceInteractionSessionShowCallback paramIVoiceInteractionSessionShowCallback, IBinder paramIBinder) {
    try {
      if (this.mVoiceInteractionManagerService != null)
        return this.mVoiceInteractionManagerService.showSessionForActiveService(paramBundle, paramInt, paramIVoiceInteractionSessionShowCallback, paramIBinder); 
    } catch (RemoteException remoteException) {
      Log.w("AssistUtils", "Failed to call showSessionForActiveService", (Throwable)remoteException);
    } 
    return false;
  }
  
  public void getActiveServiceSupportedActions(Set<String> paramSet, IVoiceActionCheckCallback paramIVoiceActionCheckCallback) {
    try {
      if (this.mVoiceInteractionManagerService != null) {
        IVoiceInteractionManagerService iVoiceInteractionManagerService = this.mVoiceInteractionManagerService;
        ArrayList<String> arrayList = new ArrayList();
        this((Collection)paramSet);
        iVoiceInteractionManagerService.getActiveServiceSupportedActions(arrayList, paramIVoiceActionCheckCallback);
      } 
    } catch (RemoteException remoteException) {
      Log.w("AssistUtils", "Failed to call activeServiceSupportedActions", (Throwable)remoteException);
      try {
        paramIVoiceActionCheckCallback.onComplete(null);
      } catch (RemoteException remoteException1) {}
    } 
  }
  
  public void launchVoiceAssistFromKeyguard() {
    try {
      if (this.mVoiceInteractionManagerService != null)
        this.mVoiceInteractionManagerService.launchVoiceAssistFromKeyguard(); 
    } catch (RemoteException remoteException) {
      Log.w("AssistUtils", "Failed to call launchVoiceAssistFromKeyguard", (Throwable)remoteException);
    } 
  }
  
  public boolean activeServiceSupportsAssistGesture() {
    boolean bool = false;
    try {
      if (this.mVoiceInteractionManagerService != null) {
        IVoiceInteractionManagerService iVoiceInteractionManagerService = this.mVoiceInteractionManagerService;
        boolean bool1 = iVoiceInteractionManagerService.activeServiceSupportsAssist();
        if (bool1)
          bool = true; 
      } 
      return bool;
    } catch (RemoteException remoteException) {
      Log.w("AssistUtils", "Failed to call activeServiceSupportsAssistGesture", (Throwable)remoteException);
      return false;
    } 
  }
  
  public boolean activeServiceSupportsLaunchFromKeyguard() {
    boolean bool = false;
    try {
      if (this.mVoiceInteractionManagerService != null) {
        IVoiceInteractionManagerService iVoiceInteractionManagerService = this.mVoiceInteractionManagerService;
        boolean bool1 = iVoiceInteractionManagerService.activeServiceSupportsLaunchFromKeyguard();
        if (bool1)
          bool = true; 
      } 
      return bool;
    } catch (RemoteException remoteException) {
      Log.w("AssistUtils", "Failed to call activeServiceSupportsLaunchFromKeyguard", (Throwable)remoteException);
      return false;
    } 
  }
  
  public ComponentName getActiveServiceComponentName() {
    try {
      if (this.mVoiceInteractionManagerService != null)
        return this.mVoiceInteractionManagerService.getActiveServiceComponentName(); 
      return null;
    } catch (RemoteException remoteException) {
      Log.w("AssistUtils", "Failed to call getActiveServiceComponentName", (Throwable)remoteException);
      return null;
    } 
  }
  
  public boolean isSessionRunning() {
    boolean bool = false;
    try {
      if (this.mVoiceInteractionManagerService != null) {
        IVoiceInteractionManagerService iVoiceInteractionManagerService = this.mVoiceInteractionManagerService;
        boolean bool1 = iVoiceInteractionManagerService.isSessionRunning();
        if (bool1)
          bool = true; 
      } 
      return bool;
    } catch (RemoteException remoteException) {
      Log.w("AssistUtils", "Failed to call isSessionRunning", (Throwable)remoteException);
      return false;
    } 
  }
  
  public void hideCurrentSession() {
    try {
      if (this.mVoiceInteractionManagerService != null)
        this.mVoiceInteractionManagerService.hideCurrentSession(); 
    } catch (RemoteException remoteException) {
      Log.w("AssistUtils", "Failed to call hideCurrentSession", (Throwable)remoteException);
    } 
  }
  
  public void onLockscreenShown() {
    try {
      if (this.mVoiceInteractionManagerService != null)
        this.mVoiceInteractionManagerService.onLockscreenShown(); 
    } catch (RemoteException remoteException) {
      Log.w("AssistUtils", "Failed to call onLockscreenShown", (Throwable)remoteException);
    } 
  }
  
  public void registerVoiceInteractionSessionListener(IVoiceInteractionSessionListener paramIVoiceInteractionSessionListener) {
    try {
      if (this.mVoiceInteractionManagerService != null)
        this.mVoiceInteractionManagerService.registerVoiceInteractionSessionListener(paramIVoiceInteractionSessionListener); 
    } catch (RemoteException remoteException) {
      Log.w("AssistUtils", "Failed to register voice interaction listener", (Throwable)remoteException);
    } 
  }
  
  public ComponentName getAssistComponentForUser(int paramInt) {
    String str = Settings.Secure.getStringForUser(this.mContext.getContentResolver(), "assistant", paramInt);
    if (str != null)
      return ComponentName.unflattenFromString(str); 
    return null;
  }
  
  public static boolean isPreinstalledAssistant(Context paramContext, ComponentName paramComponentName) {
    boolean bool = false;
    if (paramComponentName == null)
      return false; 
    try {
      PackageManager packageManager = paramContext.getPackageManager();
      String str = paramComponentName.getPackageName();
      ApplicationInfo applicationInfo = packageManager.getApplicationInfo(str, 0);
      if (applicationInfo.isSystemApp() || applicationInfo.isUpdatedSystemApp())
        bool = true; 
      return bool;
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      return false;
    } 
  }
  
  public static boolean isDisclosureEnabled(Context paramContext) {
    ContentResolver contentResolver = paramContext.getContentResolver();
    boolean bool = false;
    if (Settings.Secure.getInt(contentResolver, "assist_disclosure_enabled", 0) != 0)
      bool = true; 
    return bool;
  }
  
  public static boolean shouldDisclose(Context paramContext, ComponentName paramComponentName) {
    boolean bool = allowDisablingAssistDisclosure(paramContext);
    boolean bool1 = true;
    if (!bool)
      return true; 
    bool = bool1;
    if (!isDisclosureEnabled(paramContext))
      if (!isPreinstalledAssistant(paramContext, paramComponentName)) {
        bool = bool1;
      } else {
        bool = false;
      }  
    return bool;
  }
  
  public static boolean allowDisablingAssistDisclosure(Context paramContext) {
    return paramContext.getResources().getBoolean(17891344);
  }
}
