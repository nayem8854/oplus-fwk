package com.android.internal.app;

import android.animation.ObjectAnimator;
import android.app.ActionBar;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.Settings;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.animation.PathInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import org.json.JSONObject;

public class PlatLogoActivity extends Activity {
  private static final String R_EGG_UNLOCK_SETTING = "egg_mode_r";
  
  static final String TOUCH_STATS = "touch.stats";
  
  private static final int UNLOCK_TRIES = 3;
  
  private static final boolean WRITE_SETTINGS = true;
  
  BigDialView mDialView;
  
  protected void onPause() {
    super.onPause();
  }
  
  protected void onCreate(Bundle paramBundle) {
    super.onCreate(paramBundle);
    float f = (getResources().getDisplayMetrics()).density;
    getWindow().getDecorView().setSystemUiVisibility(768);
    getWindow().setNavigationBarColor(0);
    getWindow().setStatusBarColor(0);
    ActionBar actionBar = getActionBar();
    if (actionBar != null)
      actionBar.hide(); 
    this.mDialView = new BigDialView((Context)this, null);
    if (Settings.System.getLong(getContentResolver(), "egg_mode_r", 0L) == 0L) {
      this.mDialView.setUnlockTries(3);
    } else {
      this.mDialView.setUnlockTries(0);
    } 
    FrameLayout frameLayout = new FrameLayout((Context)this);
    frameLayout.setBackgroundColor(-65536);
    frameLayout.addView(this.mDialView, -1, -1);
    setContentView(frameLayout);
  }
  
  private void launchNextStage(boolean paramBoolean) {
    long l;
    ContentResolver contentResolver = getContentResolver();
    if (paramBoolean) {
      l = 0L;
    } else {
      try {
        l = System.currentTimeMillis();
        Settings.System.putLong(contentResolver, "egg_mode_r", l);
      } catch (RuntimeException runtimeException) {
        Log.e("com.android.internal.app.PlatLogoActivity", "Can't write settings", runtimeException);
      } 
      try {
        Intent intent = new Intent();
        this("android.intent.action.MAIN");
        intent = intent.setFlags(268468224);
        intent = intent.addCategory("com.android.internal.category.PLATLOGO");
        startActivity(intent);
      } catch (ActivityNotFoundException activityNotFoundException) {
        Log.e("com.android.internal.app.PlatLogoActivity", "No more eggs.");
      } 
      return;
    } 
    Settings.System.putLong((ContentResolver)activityNotFoundException, "egg_mode_r", l);
  }
  
  double mPressureMax = -1.0D, mPressureMin = 0.0D;
  
  private void measureTouchPressure(MotionEvent paramMotionEvent) {
    float f = paramMotionEvent.getPressure();
    int i = paramMotionEvent.getActionMasked();
    if (i != 0) {
      if (i == 2) {
        if (f < this.mPressureMin)
          this.mPressureMin = f; 
        if (f > this.mPressureMax)
          this.mPressureMax = f; 
      } 
    } else if (this.mPressureMax < 0.0D) {
      double d = f;
      this.mPressureMin = d;
    } 
  }
  
  private void syncTouchPressure() {
    try {
      ContentResolver contentResolver = getContentResolver();
      String str = Settings.System.getString(contentResolver, "touch.stats");
      JSONObject jSONObject = new JSONObject();
      if (str == null)
        str = "{}"; 
      this(str);
      if (jSONObject.has("min"))
        this.mPressureMin = Math.min(this.mPressureMin, jSONObject.getDouble("min")); 
      if (jSONObject.has("max"))
        this.mPressureMax = Math.max(this.mPressureMax, jSONObject.getDouble("max")); 
      if (this.mPressureMax >= 0.0D) {
        jSONObject.put("min", this.mPressureMin);
        jSONObject.put("max", this.mPressureMax);
        ContentResolver contentResolver1 = getContentResolver();
        String str1 = jSONObject.toString();
        Settings.System.putString(contentResolver1, "touch.stats", str1);
      } 
    } catch (Exception exception) {
      Log.e("com.android.internal.app.PlatLogoActivity", "Can't write touch settings", exception);
    } 
  }
  
  public void onStart() {
    super.onStart();
    syncTouchPressure();
  }
  
  public void onStop() {
    syncTouchPressure();
    super.onStop();
  }
  
  class BigDialView extends ImageView {
    private static final int COLOR_BLUE = -12417548;
    
    private static final int COLOR_CHARTREUSE = -1050673;
    
    private static final int COLOR_GREEN = -12723068;
    
    private static final int COLOR_LIGHTBLUE = -2625538;
    
    private static final int COLOR_NAVY = -16306110;
    
    private static final int COLOR_ORANGE = -497868;
    
    private static final int STEPS = 11;
    
    private static final float VALUE_CHANGE_MAX = 0.09090909F;
    
    private BigDialDrawable mDialDrawable;
    
    private boolean mWasLocked;
    
    final PlatLogoActivity this$0;
    
    BigDialView(Context param1Context, AttributeSet param1AttributeSet) {
      super(param1Context, param1AttributeSet);
      init();
    }
    
    BigDialView(Context param1Context, AttributeSet param1AttributeSet, int param1Int) {
      super(param1Context, param1AttributeSet, param1Int);
      init();
    }
    
    BigDialView(Context param1Context, AttributeSet param1AttributeSet, int param1Int1, int param1Int2) {
      super(param1Context, param1AttributeSet, param1Int1, param1Int2);
      init();
    }
    
    private void init() {
      BigDialDrawable bigDialDrawable = new BigDialDrawable();
      setImageDrawable(bigDialDrawable);
    }
    
    public void onDraw(Canvas param1Canvas) {
      super.onDraw(param1Canvas);
    }
    
    double toPositiveDegrees(double param1Double) {
      return (Math.toDegrees(param1Double) + 360.0D - 90.0D) % 360.0D;
    }
    
    public boolean onTouchEvent(MotionEvent param1MotionEvent) {
      int i = param1MotionEvent.getActionMasked();
      if (i != 0) {
        if (i != 1) {
          if (i != 2)
            return false; 
        } else {
          if (this.mWasLocked != this.mDialDrawable.isLocked())
            PlatLogoActivity.this.launchNextStage(this.mDialDrawable.isLocked()); 
          return true;
        } 
      } else {
        this.mWasLocked = this.mDialDrawable.isLocked();
      } 
      float f1 = param1MotionEvent.getX();
      float f2 = param1MotionEvent.getY();
      float f3 = (getLeft() + getRight()) / 2.0F;
      float f4 = (getTop() + getBottom()) / 2.0F;
      f4 = (float)toPositiveDegrees(Math.atan2((f1 - f3), (f2 - f4)));
      i = this.mDialDrawable.getUserLevel();
      this.mDialDrawable.touchAngle(f4);
      int j = this.mDialDrawable.getUserLevel();
      if (i != j) {
        if (j == 11) {
          i = 16;
        } else {
          i = 4;
        } 
        performHapticFeedback(i);
      } 
      return true;
    }
    
    public boolean performClick() {
      if (this.mDialDrawable.getUserLevel() < 10) {
        BigDialDrawable bigDialDrawable = this.mDialDrawable;
        bigDialDrawable.setUserLevel(bigDialDrawable.getUserLevel() + 1);
        performHapticFeedback(4);
      } 
      return true;
    }
    
    void setUnlockTries(int param1Int) {
      this.mDialDrawable.setUnlockTries(param1Int);
    }
    
    class BigDialDrawable extends Drawable {
      public final int STEPS = 10;
      
      private int mUnlockTries = 0;
      
      final Paint mPaint = new Paint();
      
      private float mValue = 0.0F;
      
      float mElevenAnim = 0.0F;
      
      final Drawable mEleven;
      
      ObjectAnimator mElevenHideAnimator;
      
      ObjectAnimator mElevenShowAnimator;
      
      private boolean mNightMode;
      
      final PlatLogoActivity.BigDialView this$1;
      
      BigDialDrawable() {
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(this, "elevenAnim", new float[] { 0.0F, 1.0F });
        this.mElevenShowAnimator = objectAnimator.setDuration(300L);
        objectAnimator = ObjectAnimator.ofFloat(this, "elevenAnim", new float[] { 1.0F, 0.0F });
        this.mElevenHideAnimator = objectAnimator.setDuration(500L);
        this.mNightMode = PlatLogoActivity.BigDialView.this.getContext().getResources().getConfiguration().isNightModeActive();
        this.mEleven = PlatLogoActivity.BigDialView.this.getContext().getDrawable(17302763);
        this.mElevenShowAnimator.setInterpolator(new PathInterpolator(0.4F, 0.0F, 0.2F, 1.0F));
        this.mElevenHideAnimator.setInterpolator(new PathInterpolator(0.8F, 0.2F, 0.6F, 1.0F));
      }
      
      public void setUnlockTries(int param2Int) {
        if (this.mUnlockTries != param2Int) {
          this.mUnlockTries = param2Int;
          setValue(getValue());
          invalidateSelf();
        } 
      }
      
      boolean isLocked() {
        boolean bool;
        if (this.mUnlockTries > 0) {
          bool = true;
        } else {
          bool = false;
        } 
        return bool;
      }
      
      public void setValue(float param2Float) {
        float f1;
        if (isLocked()) {
          f1 = 0.9F;
        } else {
          f1 = 1.0F;
        } 
        float f2 = 0.0F;
        if (param2Float < 0.0F) {
          param2Float = f2;
        } else if (param2Float > f1) {
          param2Float = f1;
        } 
        this.mValue = param2Float;
        invalidateSelf();
      }
      
      public float getValue() {
        return this.mValue;
      }
      
      public int getUserLevel() {
        return Math.round(getValue() * 10.0F - 0.25F);
      }
      
      public void setUserLevel(int param2Int) {
        setValue(getValue() + param2Int / 10.0F);
      }
      
      public float getElevenAnim() {
        return this.mElevenAnim;
      }
      
      public void setElevenAnim(float param2Float) {
        if (this.mElevenAnim != param2Float) {
          this.mElevenAnim = param2Float;
          invalidateSelf();
        } 
      }
      
      public void draw(Canvas param2Canvas) {
        Rect rect = getBounds();
        int i = rect.width();
        int j = rect.height();
        float f1 = i / 2.0F;
        float f2 = j / 2.0F;
        float f3 = i / 4.0F;
        if (this.mNightMode) {
          k = -16306110;
        } else {
          k = -2625538;
        } 
        param2Canvas.drawColor(k);
        param2Canvas.save();
        param2Canvas.rotate(45.0F, f1, f2);
        param2Canvas.clipRect(f1, f2 - f3, Math.min(i, j), f2 + f3);
        if (this.mNightMode) {
          k = 1610612768;
        } else {
          k = 268906562;
        } 
        Paint paint = this.mPaint;
        LinearGradient linearGradient = new LinearGradient(f1, f2, Math.min(i, j), f2, k, k & 0xFFFFFF, Shader.TileMode.CLAMP);
        paint.setShader((Shader)linearGradient);
        this.mPaint.setColor(-16777216);
        param2Canvas.drawPaint(this.mPaint);
        this.mPaint.setShader(null);
        param2Canvas.restore();
        this.mPaint.setStyle(Paint.Style.FILL);
        this.mPaint.setColor(-12723068);
        param2Canvas.drawCircle(f1, f2, f3, this.mPaint);
        paint = this.mPaint;
        if (this.mNightMode) {
          k = -2625538;
        } else {
          k = -16306110;
        } 
        paint.setColor(k);
        float f4 = i * 0.85F;
        int k;
        for (k = 0; k < 10; k++) {
          float f = k / 10.0F;
          param2Canvas.save();
          f = valueToAngle(f);
          param2Canvas.rotate(-f, f1, f2);
          if (k <= getUserLevel()) {
            f = 20.0F;
          } else {
            f = 5.0F;
          } 
          param2Canvas.drawCircle(f4, f2, f, this.mPaint);
          param2Canvas.restore();
        } 
        float f5 = this.mElevenAnim;
        if (f5 > 0.0F) {
          k = (int)(((f5 * 0.5F) + 0.5D) * i / 14.0D);
          f5 = k / 4.0F + f4;
          this.mEleven.setBounds((int)f5 - k, (int)f2 - k, (int)f5 + k, (int)f2 + k);
          k = (int)clamp(this.mElevenAnim * 510.0F, 0.0F, 255.0F);
          this.mEleven.setTint(0xFFF86734 & (k << 24 | 0xFFFFFF));
          this.mEleven.draw(param2Canvas);
        } 
        f5 = valueToAngle(this.mValue);
        param2Canvas.rotate(-f5, f1, f2);
        this.mPaint.setColor(-1);
        f5 = f1 / 12.0F;
        param2Canvas.drawCircle(i - f3 - 2.0F * f5, f2, f5, this.mPaint);
      }
      
      float clamp(float param2Float1, float param2Float2, float param2Float3) {
        if (param2Float1 < param2Float2) {
          param2Float1 = param2Float2;
        } else if (param2Float1 > param2Float3) {
          param2Float1 = param2Float3;
        } 
        return param2Float1;
      }
      
      float angleToValue(float param2Float) {
        return 1.0F - clamp(param2Float / 315.0F, 0.0F, 1.0F);
      }
      
      float valueToAngle(float param2Float) {
        return (1.0F - param2Float) * 315.0F;
      }
      
      public void touchAngle(float param2Float) {
        int i = getUserLevel();
        param2Float = angleToValue(param2Float);
        if (Math.abs(param2Float - getValue()) < 0.09090909F) {
          setValue(param2Float);
          if (isLocked() && i != 9 && getUserLevel() == 9) {
            this.mUnlockTries--;
          } else if (!isLocked() && getUserLevel() == 0) {
            this.mUnlockTries = 3;
          } 
          if (!isLocked()) {
            if (getUserLevel() == 10 && this.mElevenAnim != 1.0F) {
              ObjectAnimator objectAnimator = this.mElevenShowAnimator;
              if (!objectAnimator.isRunning()) {
                this.mElevenHideAnimator.cancel();
                this.mElevenShowAnimator.start();
                return;
              } 
            } 
            if (getUserLevel() != 10 && this.mElevenAnim == 1.0F) {
              ObjectAnimator objectAnimator = this.mElevenHideAnimator;
              if (!objectAnimator.isRunning()) {
                this.mElevenShowAnimator.cancel();
                this.mElevenHideAnimator.start();
              } 
            } 
          } 
        } 
      }
      
      public void setAlpha(int param2Int) {}
      
      public void setColorFilter(ColorFilter param2ColorFilter) {}
      
      public int getOpacity() {
        return -3;
      }
    }
  }
  
  class BigDialDrawable extends Drawable {
    public final int STEPS = 10;
    
    private int mUnlockTries = 0;
    
    final Paint mPaint = new Paint();
    
    private float mValue = 0.0F;
    
    float mElevenAnim = 0.0F;
    
    final Drawable mEleven;
    
    ObjectAnimator mElevenHideAnimator;
    
    ObjectAnimator mElevenShowAnimator;
    
    private boolean mNightMode;
    
    final PlatLogoActivity.BigDialView this$1;
    
    BigDialDrawable() {
      ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(this, "elevenAnim", new float[] { 0.0F, 1.0F });
      this.mElevenShowAnimator = objectAnimator.setDuration(300L);
      objectAnimator = ObjectAnimator.ofFloat(this, "elevenAnim", new float[] { 1.0F, 0.0F });
      this.mElevenHideAnimator = objectAnimator.setDuration(500L);
      this.mNightMode = PlatLogoActivity.this.getContext().getResources().getConfiguration().isNightModeActive();
      this.mEleven = PlatLogoActivity.this.getContext().getDrawable(17302763);
      this.mElevenShowAnimator.setInterpolator(new PathInterpolator(0.4F, 0.0F, 0.2F, 1.0F));
      this.mElevenHideAnimator.setInterpolator(new PathInterpolator(0.8F, 0.2F, 0.6F, 1.0F));
    }
    
    public void setUnlockTries(int param1Int) {
      if (this.mUnlockTries != param1Int) {
        this.mUnlockTries = param1Int;
        setValue(getValue());
        invalidateSelf();
      } 
    }
    
    boolean isLocked() {
      boolean bool;
      if (this.mUnlockTries > 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public void setValue(float param1Float) {
      float f1;
      if (isLocked()) {
        f1 = 0.9F;
      } else {
        f1 = 1.0F;
      } 
      float f2 = 0.0F;
      if (param1Float < 0.0F) {
        param1Float = f2;
      } else if (param1Float > f1) {
        param1Float = f1;
      } 
      this.mValue = param1Float;
      invalidateSelf();
    }
    
    public float getValue() {
      return this.mValue;
    }
    
    public int getUserLevel() {
      return Math.round(getValue() * 10.0F - 0.25F);
    }
    
    public void setUserLevel(int param1Int) {
      setValue(getValue() + param1Int / 10.0F);
    }
    
    public float getElevenAnim() {
      return this.mElevenAnim;
    }
    
    public void setElevenAnim(float param1Float) {
      if (this.mElevenAnim != param1Float) {
        this.mElevenAnim = param1Float;
        invalidateSelf();
      } 
    }
    
    public void draw(Canvas param1Canvas) {
      Rect rect = getBounds();
      int i = rect.width();
      int j = rect.height();
      float f1 = i / 2.0F;
      float f2 = j / 2.0F;
      float f3 = i / 4.0F;
      if (this.mNightMode) {
        k = -16306110;
      } else {
        k = -2625538;
      } 
      param1Canvas.drawColor(k);
      param1Canvas.save();
      param1Canvas.rotate(45.0F, f1, f2);
      param1Canvas.clipRect(f1, f2 - f3, Math.min(i, j), f2 + f3);
      if (this.mNightMode) {
        k = 1610612768;
      } else {
        k = 268906562;
      } 
      Paint paint = this.mPaint;
      LinearGradient linearGradient = new LinearGradient(f1, f2, Math.min(i, j), f2, k, k & 0xFFFFFF, Shader.TileMode.CLAMP);
      paint.setShader((Shader)linearGradient);
      this.mPaint.setColor(-16777216);
      param1Canvas.drawPaint(this.mPaint);
      this.mPaint.setShader(null);
      param1Canvas.restore();
      this.mPaint.setStyle(Paint.Style.FILL);
      this.mPaint.setColor(-12723068);
      param1Canvas.drawCircle(f1, f2, f3, this.mPaint);
      paint = this.mPaint;
      if (this.mNightMode) {
        k = -2625538;
      } else {
        k = -16306110;
      } 
      paint.setColor(k);
      float f4 = i * 0.85F;
      int k;
      for (k = 0; k < 10; k++) {
        float f = k / 10.0F;
        param1Canvas.save();
        f = valueToAngle(f);
        param1Canvas.rotate(-f, f1, f2);
        if (k <= getUserLevel()) {
          f = 20.0F;
        } else {
          f = 5.0F;
        } 
        param1Canvas.drawCircle(f4, f2, f, this.mPaint);
        param1Canvas.restore();
      } 
      float f5 = this.mElevenAnim;
      if (f5 > 0.0F) {
        k = (int)(((f5 * 0.5F) + 0.5D) * i / 14.0D);
        f5 = k / 4.0F + f4;
        this.mEleven.setBounds((int)f5 - k, (int)f2 - k, (int)f5 + k, (int)f2 + k);
        k = (int)clamp(this.mElevenAnim * 510.0F, 0.0F, 255.0F);
        this.mEleven.setTint(0xFFF86734 & (k << 24 | 0xFFFFFF));
        this.mEleven.draw(param1Canvas);
      } 
      f5 = valueToAngle(this.mValue);
      param1Canvas.rotate(-f5, f1, f2);
      this.mPaint.setColor(-1);
      f5 = f1 / 12.0F;
      param1Canvas.drawCircle(i - f3 - 2.0F * f5, f2, f5, this.mPaint);
    }
    
    float clamp(float param1Float1, float param1Float2, float param1Float3) {
      if (param1Float1 < param1Float2) {
        param1Float1 = param1Float2;
      } else if (param1Float1 > param1Float3) {
        param1Float1 = param1Float3;
      } 
      return param1Float1;
    }
    
    float angleToValue(float param1Float) {
      return 1.0F - clamp(param1Float / 315.0F, 0.0F, 1.0F);
    }
    
    float valueToAngle(float param1Float) {
      return (1.0F - param1Float) * 315.0F;
    }
    
    public void touchAngle(float param1Float) {
      int i = getUserLevel();
      param1Float = angleToValue(param1Float);
      if (Math.abs(param1Float - getValue()) < 0.09090909F) {
        setValue(param1Float);
        if (isLocked() && i != 9 && getUserLevel() == 9) {
          this.mUnlockTries--;
        } else if (!isLocked() && getUserLevel() == 0) {
          this.mUnlockTries = 3;
        } 
        if (!isLocked()) {
          if (getUserLevel() == 10 && this.mElevenAnim != 1.0F) {
            ObjectAnimator objectAnimator = this.mElevenShowAnimator;
            if (!objectAnimator.isRunning()) {
              this.mElevenHideAnimator.cancel();
              this.mElevenShowAnimator.start();
              return;
            } 
          } 
          if (getUserLevel() != 10 && this.mElevenAnim == 1.0F) {
            ObjectAnimator objectAnimator = this.mElevenHideAnimator;
            if (!objectAnimator.isRunning()) {
              this.mElevenShowAnimator.cancel();
              this.mElevenHideAnimator.start();
            } 
          } 
        } 
      } 
    }
    
    public void setAlpha(int param1Int) {}
    
    public void setColorFilter(ColorFilter param1ColorFilter) {}
    
    public int getOpacity() {
      return -3;
    }
  }
}
