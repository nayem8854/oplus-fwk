package com.android.internal.app;

import android.app.AppOpsManager;
import android.app.AsyncNotedAppOp;
import android.app.RuntimeAppOpAccessMessage;
import android.app.SyncNotedAppOp;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteCallback;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IAppOpsService extends IInterface {
  void addHistoricalOps(AppOpsManager.HistoricalOps paramHistoricalOps) throws RemoteException;
  
  int checkAudioOperation(int paramInt1, int paramInt2, int paramInt3, String paramString) throws RemoteException;
  
  int checkOperation(int paramInt1, int paramInt2, String paramString) throws RemoteException;
  
  int checkOperationRaw(int paramInt1, int paramInt2, String paramString) throws RemoteException;
  
  int checkPackage(int paramInt, String paramString) throws RemoteException;
  
  void clearHistory() throws RemoteException;
  
  void collectNoteOpCallsForValidation(String paramString1, int paramInt, String paramString2, long paramLong) throws RemoteException;
  
  RuntimeAppOpAccessMessage collectRuntimeAppOpAccessMessage() throws RemoteException;
  
  List<AsyncNotedAppOp> extractAsyncOps(String paramString) throws RemoteException;
  
  void finishOperation(IBinder paramIBinder, int paramInt1, int paramInt2, String paramString1, String paramString2) throws RemoteException;
  
  void getHistoricalOps(int paramInt1, String paramString1, String paramString2, List<String> paramList, int paramInt2, long paramLong1, long paramLong2, int paramInt3, RemoteCallback paramRemoteCallback) throws RemoteException;
  
  void getHistoricalOpsFromDiskRaw(int paramInt1, String paramString1, String paramString2, List<String> paramList, int paramInt2, long paramLong1, long paramLong2, int paramInt3, RemoteCallback paramRemoteCallback) throws RemoteException;
  
  List<AppOpsManager.PackageOps> getOpsForPackage(int paramInt, String paramString, int[] paramArrayOfint) throws RemoteException;
  
  List<AppOpsManager.PackageOps> getPackagesForOps(int[] paramArrayOfint) throws RemoteException;
  
  List<AppOpsManager.PackageOps> getUidOps(int paramInt, int[] paramArrayOfint) throws RemoteException;
  
  boolean isOperationActive(int paramInt1, int paramInt2, String paramString) throws RemoteException;
  
  int noteOperation(int paramInt1, int paramInt2, String paramString1, String paramString2, boolean paramBoolean1, String paramString3, boolean paramBoolean2) throws RemoteException;
  
  int noteProxyOperation(int paramInt1, int paramInt2, String paramString1, String paramString2, int paramInt3, String paramString3, String paramString4, boolean paramBoolean1, String paramString5, boolean paramBoolean2) throws RemoteException;
  
  void offsetHistory(long paramLong) throws RemoteException;
  
  int permissionToOpCode(String paramString) throws RemoteException;
  
  void rebootHistory(long paramLong) throws RemoteException;
  
  void reloadNonHistoricalState() throws RemoteException;
  
  void removeUser(int paramInt) throws RemoteException;
  
  MessageSamplingConfig reportRuntimeAppOpAccessMessageAndGetConfig(String paramString1, SyncNotedAppOp paramSyncNotedAppOp, String paramString2) throws RemoteException;
  
  void resetAllModes(int paramInt, String paramString) throws RemoteException;
  
  void resetHistoryParameters() throws RemoteException;
  
  void setAudioRestriction(int paramInt1, int paramInt2, int paramInt3, int paramInt4, String[] paramArrayOfString) throws RemoteException;
  
  void setCameraAudioRestriction(int paramInt) throws RemoteException;
  
  void setHistoryParameters(int paramInt1, long paramLong, int paramInt2) throws RemoteException;
  
  void setMode(int paramInt1, int paramInt2, String paramString, int paramInt3) throws RemoteException;
  
  void setUidMode(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void setUserRestriction(int paramInt1, boolean paramBoolean, IBinder paramIBinder, int paramInt2, String[] paramArrayOfString) throws RemoteException;
  
  void setUserRestrictions(Bundle paramBundle, IBinder paramIBinder, int paramInt) throws RemoteException;
  
  boolean shouldCollectNotes(int paramInt) throws RemoteException;
  
  int startOperation(IBinder paramIBinder, int paramInt1, int paramInt2, String paramString1, String paramString2, boolean paramBoolean1, boolean paramBoolean2, String paramString3, boolean paramBoolean3) throws RemoteException;
  
  void startWatchingActive(int[] paramArrayOfint, IAppOpsActiveCallback paramIAppOpsActiveCallback) throws RemoteException;
  
  void startWatchingAsyncNoted(String paramString, IAppOpsAsyncNotedCallback paramIAppOpsAsyncNotedCallback) throws RemoteException;
  
  void startWatchingMode(int paramInt, String paramString, IAppOpsCallback paramIAppOpsCallback) throws RemoteException;
  
  void startWatchingModeWithFlags(int paramInt1, String paramString, int paramInt2, IAppOpsCallback paramIAppOpsCallback) throws RemoteException;
  
  void startWatchingNoted(int[] paramArrayOfint, IAppOpsNotedCallback paramIAppOpsNotedCallback) throws RemoteException;
  
  void startWatchingStarted(int[] paramArrayOfint, IAppOpsStartedCallback paramIAppOpsStartedCallback) throws RemoteException;
  
  void stopWatchingActive(IAppOpsActiveCallback paramIAppOpsActiveCallback) throws RemoteException;
  
  void stopWatchingAsyncNoted(String paramString, IAppOpsAsyncNotedCallback paramIAppOpsAsyncNotedCallback) throws RemoteException;
  
  void stopWatchingMode(IAppOpsCallback paramIAppOpsCallback) throws RemoteException;
  
  void stopWatchingNoted(IAppOpsNotedCallback paramIAppOpsNotedCallback) throws RemoteException;
  
  void stopWatchingStarted(IAppOpsStartedCallback paramIAppOpsStartedCallback) throws RemoteException;
  
  class Default implements IAppOpsService {
    public int checkOperation(int param1Int1, int param1Int2, String param1String) throws RemoteException {
      return 0;
    }
    
    public int noteOperation(int param1Int1, int param1Int2, String param1String1, String param1String2, boolean param1Boolean1, String param1String3, boolean param1Boolean2) throws RemoteException {
      return 0;
    }
    
    public int startOperation(IBinder param1IBinder, int param1Int1, int param1Int2, String param1String1, String param1String2, boolean param1Boolean1, boolean param1Boolean2, String param1String3, boolean param1Boolean3) throws RemoteException {
      return 0;
    }
    
    public void finishOperation(IBinder param1IBinder, int param1Int1, int param1Int2, String param1String1, String param1String2) throws RemoteException {}
    
    public void startWatchingMode(int param1Int, String param1String, IAppOpsCallback param1IAppOpsCallback) throws RemoteException {}
    
    public void stopWatchingMode(IAppOpsCallback param1IAppOpsCallback) throws RemoteException {}
    
    public int permissionToOpCode(String param1String) throws RemoteException {
      return 0;
    }
    
    public int checkAudioOperation(int param1Int1, int param1Int2, int param1Int3, String param1String) throws RemoteException {
      return 0;
    }
    
    public boolean shouldCollectNotes(int param1Int) throws RemoteException {
      return false;
    }
    
    public void setCameraAudioRestriction(int param1Int) throws RemoteException {}
    
    public int noteProxyOperation(int param1Int1, int param1Int2, String param1String1, String param1String2, int param1Int3, String param1String3, String param1String4, boolean param1Boolean1, String param1String5, boolean param1Boolean2) throws RemoteException {
      return 0;
    }
    
    public int checkPackage(int param1Int, String param1String) throws RemoteException {
      return 0;
    }
    
    public RuntimeAppOpAccessMessage collectRuntimeAppOpAccessMessage() throws RemoteException {
      return null;
    }
    
    public MessageSamplingConfig reportRuntimeAppOpAccessMessageAndGetConfig(String param1String1, SyncNotedAppOp param1SyncNotedAppOp, String param1String2) throws RemoteException {
      return null;
    }
    
    public List<AppOpsManager.PackageOps> getPackagesForOps(int[] param1ArrayOfint) throws RemoteException {
      return null;
    }
    
    public List<AppOpsManager.PackageOps> getOpsForPackage(int param1Int, String param1String, int[] param1ArrayOfint) throws RemoteException {
      return null;
    }
    
    public void getHistoricalOps(int param1Int1, String param1String1, String param1String2, List<String> param1List, int param1Int2, long param1Long1, long param1Long2, int param1Int3, RemoteCallback param1RemoteCallback) throws RemoteException {}
    
    public void getHistoricalOpsFromDiskRaw(int param1Int1, String param1String1, String param1String2, List<String> param1List, int param1Int2, long param1Long1, long param1Long2, int param1Int3, RemoteCallback param1RemoteCallback) throws RemoteException {}
    
    public void offsetHistory(long param1Long) throws RemoteException {}
    
    public void setHistoryParameters(int param1Int1, long param1Long, int param1Int2) throws RemoteException {}
    
    public void addHistoricalOps(AppOpsManager.HistoricalOps param1HistoricalOps) throws RemoteException {}
    
    public void resetHistoryParameters() throws RemoteException {}
    
    public void clearHistory() throws RemoteException {}
    
    public void rebootHistory(long param1Long) throws RemoteException {}
    
    public List<AppOpsManager.PackageOps> getUidOps(int param1Int, int[] param1ArrayOfint) throws RemoteException {
      return null;
    }
    
    public void setUidMode(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void setMode(int param1Int1, int param1Int2, String param1String, int param1Int3) throws RemoteException {}
    
    public void resetAllModes(int param1Int, String param1String) throws RemoteException {}
    
    public void setAudioRestriction(int param1Int1, int param1Int2, int param1Int3, int param1Int4, String[] param1ArrayOfString) throws RemoteException {}
    
    public void setUserRestrictions(Bundle param1Bundle, IBinder param1IBinder, int param1Int) throws RemoteException {}
    
    public void setUserRestriction(int param1Int1, boolean param1Boolean, IBinder param1IBinder, int param1Int2, String[] param1ArrayOfString) throws RemoteException {}
    
    public void removeUser(int param1Int) throws RemoteException {}
    
    public void startWatchingActive(int[] param1ArrayOfint, IAppOpsActiveCallback param1IAppOpsActiveCallback) throws RemoteException {}
    
    public void stopWatchingActive(IAppOpsActiveCallback param1IAppOpsActiveCallback) throws RemoteException {}
    
    public boolean isOperationActive(int param1Int1, int param1Int2, String param1String) throws RemoteException {
      return false;
    }
    
    public void startWatchingStarted(int[] param1ArrayOfint, IAppOpsStartedCallback param1IAppOpsStartedCallback) throws RemoteException {}
    
    public void stopWatchingStarted(IAppOpsStartedCallback param1IAppOpsStartedCallback) throws RemoteException {}
    
    public void startWatchingModeWithFlags(int param1Int1, String param1String, int param1Int2, IAppOpsCallback param1IAppOpsCallback) throws RemoteException {}
    
    public void startWatchingNoted(int[] param1ArrayOfint, IAppOpsNotedCallback param1IAppOpsNotedCallback) throws RemoteException {}
    
    public void stopWatchingNoted(IAppOpsNotedCallback param1IAppOpsNotedCallback) throws RemoteException {}
    
    public void startWatchingAsyncNoted(String param1String, IAppOpsAsyncNotedCallback param1IAppOpsAsyncNotedCallback) throws RemoteException {}
    
    public void stopWatchingAsyncNoted(String param1String, IAppOpsAsyncNotedCallback param1IAppOpsAsyncNotedCallback) throws RemoteException {}
    
    public List<AsyncNotedAppOp> extractAsyncOps(String param1String) throws RemoteException {
      return null;
    }
    
    public int checkOperationRaw(int param1Int1, int param1Int2, String param1String) throws RemoteException {
      return 0;
    }
    
    public void reloadNonHistoricalState() throws RemoteException {}
    
    public void collectNoteOpCallsForValidation(String param1String1, int param1Int, String param1String2, long param1Long) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAppOpsService {
    private static final String DESCRIPTOR = "com.android.internal.app.IAppOpsService";
    
    static final int TRANSACTION_addHistoricalOps = 21;
    
    static final int TRANSACTION_checkAudioOperation = 8;
    
    static final int TRANSACTION_checkOperation = 1;
    
    static final int TRANSACTION_checkOperationRaw = 44;
    
    static final int TRANSACTION_checkPackage = 12;
    
    static final int TRANSACTION_clearHistory = 23;
    
    static final int TRANSACTION_collectNoteOpCallsForValidation = 46;
    
    static final int TRANSACTION_collectRuntimeAppOpAccessMessage = 13;
    
    static final int TRANSACTION_extractAsyncOps = 43;
    
    static final int TRANSACTION_finishOperation = 4;
    
    static final int TRANSACTION_getHistoricalOps = 17;
    
    static final int TRANSACTION_getHistoricalOpsFromDiskRaw = 18;
    
    static final int TRANSACTION_getOpsForPackage = 16;
    
    static final int TRANSACTION_getPackagesForOps = 15;
    
    static final int TRANSACTION_getUidOps = 25;
    
    static final int TRANSACTION_isOperationActive = 35;
    
    static final int TRANSACTION_noteOperation = 2;
    
    static final int TRANSACTION_noteProxyOperation = 11;
    
    static final int TRANSACTION_offsetHistory = 19;
    
    static final int TRANSACTION_permissionToOpCode = 7;
    
    static final int TRANSACTION_rebootHistory = 24;
    
    static final int TRANSACTION_reloadNonHistoricalState = 45;
    
    static final int TRANSACTION_removeUser = 32;
    
    static final int TRANSACTION_reportRuntimeAppOpAccessMessageAndGetConfig = 14;
    
    static final int TRANSACTION_resetAllModes = 28;
    
    static final int TRANSACTION_resetHistoryParameters = 22;
    
    static final int TRANSACTION_setAudioRestriction = 29;
    
    static final int TRANSACTION_setCameraAudioRestriction = 10;
    
    static final int TRANSACTION_setHistoryParameters = 20;
    
    static final int TRANSACTION_setMode = 27;
    
    static final int TRANSACTION_setUidMode = 26;
    
    static final int TRANSACTION_setUserRestriction = 31;
    
    static final int TRANSACTION_setUserRestrictions = 30;
    
    static final int TRANSACTION_shouldCollectNotes = 9;
    
    static final int TRANSACTION_startOperation = 3;
    
    static final int TRANSACTION_startWatchingActive = 33;
    
    static final int TRANSACTION_startWatchingAsyncNoted = 41;
    
    static final int TRANSACTION_startWatchingMode = 5;
    
    static final int TRANSACTION_startWatchingModeWithFlags = 38;
    
    static final int TRANSACTION_startWatchingNoted = 39;
    
    static final int TRANSACTION_startWatchingStarted = 36;
    
    static final int TRANSACTION_stopWatchingActive = 34;
    
    static final int TRANSACTION_stopWatchingAsyncNoted = 42;
    
    static final int TRANSACTION_stopWatchingMode = 6;
    
    static final int TRANSACTION_stopWatchingNoted = 40;
    
    static final int TRANSACTION_stopWatchingStarted = 37;
    
    public Stub() {
      attachInterface(this, "com.android.internal.app.IAppOpsService");
    }
    
    public static IAppOpsService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.app.IAppOpsService");
      if (iInterface != null && iInterface instanceof IAppOpsService)
        return (IAppOpsService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 46:
          return "collectNoteOpCallsForValidation";
        case 45:
          return "reloadNonHistoricalState";
        case 44:
          return "checkOperationRaw";
        case 43:
          return "extractAsyncOps";
        case 42:
          return "stopWatchingAsyncNoted";
        case 41:
          return "startWatchingAsyncNoted";
        case 40:
          return "stopWatchingNoted";
        case 39:
          return "startWatchingNoted";
        case 38:
          return "startWatchingModeWithFlags";
        case 37:
          return "stopWatchingStarted";
        case 36:
          return "startWatchingStarted";
        case 35:
          return "isOperationActive";
        case 34:
          return "stopWatchingActive";
        case 33:
          return "startWatchingActive";
        case 32:
          return "removeUser";
        case 31:
          return "setUserRestriction";
        case 30:
          return "setUserRestrictions";
        case 29:
          return "setAudioRestriction";
        case 28:
          return "resetAllModes";
        case 27:
          return "setMode";
        case 26:
          return "setUidMode";
        case 25:
          return "getUidOps";
        case 24:
          return "rebootHistory";
        case 23:
          return "clearHistory";
        case 22:
          return "resetHistoryParameters";
        case 21:
          return "addHistoricalOps";
        case 20:
          return "setHistoryParameters";
        case 19:
          return "offsetHistory";
        case 18:
          return "getHistoricalOpsFromDiskRaw";
        case 17:
          return "getHistoricalOps";
        case 16:
          return "getOpsForPackage";
        case 15:
          return "getPackagesForOps";
        case 14:
          return "reportRuntimeAppOpAccessMessageAndGetConfig";
        case 13:
          return "collectRuntimeAppOpAccessMessage";
        case 12:
          return "checkPackage";
        case 11:
          return "noteProxyOperation";
        case 10:
          return "setCameraAudioRestriction";
        case 9:
          return "shouldCollectNotes";
        case 8:
          return "checkAudioOperation";
        case 7:
          return "permissionToOpCode";
        case 6:
          return "stopWatchingMode";
        case 5:
          return "startWatchingMode";
        case 4:
          return "finishOperation";
        case 3:
          return "startOperation";
        case 2:
          return "noteOperation";
        case 1:
          break;
      } 
      return "checkOperation";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool2;
        int j;
        boolean bool1;
        String str6;
        List<AsyncNotedAppOp> list;
        IAppOpsAsyncNotedCallback iAppOpsAsyncNotedCallback;
        IAppOpsNotedCallback iAppOpsNotedCallback;
        IAppOpsCallback iAppOpsCallback2;
        IAppOpsStartedCallback iAppOpsStartedCallback;
        String str5;
        IAppOpsActiveCallback iAppOpsActiveCallback;
        String arrayOfString[], str4;
        int[] arrayOfInt3;
        List<AppOpsManager.PackageOps> list3;
        int[] arrayOfInt2;
        List<AppOpsManager.PackageOps> list2;
        int[] arrayOfInt1;
        List<AppOpsManager.PackageOps> list1;
        String str3;
        MessageSamplingConfig messageSamplingConfig;
        RuntimeAppOpAccessMessage runtimeAppOpAccessMessage;
        String str2;
        IAppOpsCallback iAppOpsCallback1;
        String str10;
        int[] arrayOfInt5;
        String str9;
        int[] arrayOfInt4;
        IBinder iBinder2;
        String str8;
        IBinder iBinder1;
        String str7, str13;
        IBinder iBinder4;
        ArrayList<String> arrayList;
        String str12;
        IBinder iBinder3;
        String str11;
        long l1;
        boolean bool3;
        int k, m;
        String str14;
        long l2;
        String str15, str16;
        boolean bool4, bool5;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 46:
            param1Parcel1.enforceInterface("com.android.internal.app.IAppOpsService");
            str10 = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            str13 = param1Parcel1.readString();
            l1 = param1Parcel1.readLong();
            collectNoteOpCallsForValidation(str10, param1Int1, str13, l1);
            param1Parcel2.writeNoException();
            return true;
          case 45:
            param1Parcel1.enforceInterface("com.android.internal.app.IAppOpsService");
            reloadNonHistoricalState();
            param1Parcel2.writeNoException();
            return true;
          case 44:
            param1Parcel1.enforceInterface("com.android.internal.app.IAppOpsService");
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            str6 = param1Parcel1.readString();
            param1Int1 = checkOperationRaw(param1Int2, param1Int1, str6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 43:
            str6.enforceInterface("com.android.internal.app.IAppOpsService");
            str6 = str6.readString();
            list = extractAsyncOps(str6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 42:
            list.enforceInterface("com.android.internal.app.IAppOpsService");
            str10 = list.readString();
            iAppOpsAsyncNotedCallback = IAppOpsAsyncNotedCallback.Stub.asInterface(list.readStrongBinder());
            stopWatchingAsyncNoted(str10, iAppOpsAsyncNotedCallback);
            param1Parcel2.writeNoException();
            return true;
          case 41:
            iAppOpsAsyncNotedCallback.enforceInterface("com.android.internal.app.IAppOpsService");
            str10 = iAppOpsAsyncNotedCallback.readString();
            iAppOpsAsyncNotedCallback = IAppOpsAsyncNotedCallback.Stub.asInterface(iAppOpsAsyncNotedCallback.readStrongBinder());
            startWatchingAsyncNoted(str10, iAppOpsAsyncNotedCallback);
            param1Parcel2.writeNoException();
            return true;
          case 40:
            iAppOpsAsyncNotedCallback.enforceInterface("com.android.internal.app.IAppOpsService");
            iAppOpsNotedCallback = IAppOpsNotedCallback.Stub.asInterface(iAppOpsAsyncNotedCallback.readStrongBinder());
            stopWatchingNoted(iAppOpsNotedCallback);
            param1Parcel2.writeNoException();
            return true;
          case 39:
            iAppOpsNotedCallback.enforceInterface("com.android.internal.app.IAppOpsService");
            arrayOfInt5 = iAppOpsNotedCallback.createIntArray();
            iAppOpsNotedCallback = IAppOpsNotedCallback.Stub.asInterface(iAppOpsNotedCallback.readStrongBinder());
            startWatchingNoted(arrayOfInt5, iAppOpsNotedCallback);
            param1Parcel2.writeNoException();
            return true;
          case 38:
            iAppOpsNotedCallback.enforceInterface("com.android.internal.app.IAppOpsService");
            param1Int2 = iAppOpsNotedCallback.readInt();
            str9 = iAppOpsNotedCallback.readString();
            param1Int1 = iAppOpsNotedCallback.readInt();
            iAppOpsCallback2 = IAppOpsCallback.Stub.asInterface(iAppOpsNotedCallback.readStrongBinder());
            startWatchingModeWithFlags(param1Int2, str9, param1Int1, iAppOpsCallback2);
            param1Parcel2.writeNoException();
            return true;
          case 37:
            iAppOpsCallback2.enforceInterface("com.android.internal.app.IAppOpsService");
            iAppOpsStartedCallback = IAppOpsStartedCallback.Stub.asInterface(iAppOpsCallback2.readStrongBinder());
            stopWatchingStarted(iAppOpsStartedCallback);
            param1Parcel2.writeNoException();
            return true;
          case 36:
            iAppOpsStartedCallback.enforceInterface("com.android.internal.app.IAppOpsService");
            arrayOfInt4 = iAppOpsStartedCallback.createIntArray();
            iAppOpsStartedCallback = IAppOpsStartedCallback.Stub.asInterface(iAppOpsStartedCallback.readStrongBinder());
            startWatchingStarted(arrayOfInt4, iAppOpsStartedCallback);
            param1Parcel2.writeNoException();
            return true;
          case 35:
            iAppOpsStartedCallback.enforceInterface("com.android.internal.app.IAppOpsService");
            param1Int1 = iAppOpsStartedCallback.readInt();
            param1Int2 = iAppOpsStartedCallback.readInt();
            str5 = iAppOpsStartedCallback.readString();
            bool2 = isOperationActive(param1Int1, param1Int2, str5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 34:
            str5.enforceInterface("com.android.internal.app.IAppOpsService");
            iAppOpsActiveCallback = IAppOpsActiveCallback.Stub.asInterface(str5.readStrongBinder());
            stopWatchingActive(iAppOpsActiveCallback);
            param1Parcel2.writeNoException();
            return true;
          case 33:
            iAppOpsActiveCallback.enforceInterface("com.android.internal.app.IAppOpsService");
            arrayOfInt4 = iAppOpsActiveCallback.createIntArray();
            iAppOpsActiveCallback = IAppOpsActiveCallback.Stub.asInterface(iAppOpsActiveCallback.readStrongBinder());
            startWatchingActive(arrayOfInt4, iAppOpsActiveCallback);
            param1Parcel2.writeNoException();
            return true;
          case 32:
            iAppOpsActiveCallback.enforceInterface("com.android.internal.app.IAppOpsService");
            j = iAppOpsActiveCallback.readInt();
            removeUser(j);
            param1Parcel2.writeNoException();
            return true;
          case 31:
            iAppOpsActiveCallback.enforceInterface("com.android.internal.app.IAppOpsService");
            j = iAppOpsActiveCallback.readInt();
            if (iAppOpsActiveCallback.readInt() != 0) {
              bool3 = true;
            } else {
              bool3 = false;
            } 
            iBinder2 = iAppOpsActiveCallback.readStrongBinder();
            param1Int2 = iAppOpsActiveCallback.readInt();
            arrayOfString = iAppOpsActiveCallback.createStringArray();
            setUserRestriction(j, bool3, iBinder2, param1Int2, arrayOfString);
            param1Parcel2.writeNoException();
            return true;
          case 30:
            arrayOfString.enforceInterface("com.android.internal.app.IAppOpsService");
            if (arrayOfString.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)arrayOfString);
            } else {
              iBinder2 = null;
            } 
            iBinder4 = arrayOfString.readStrongBinder();
            j = arrayOfString.readInt();
            setUserRestrictions((Bundle)iBinder2, iBinder4, j);
            param1Parcel2.writeNoException();
            return true;
          case 29:
            arrayOfString.enforceInterface("com.android.internal.app.IAppOpsService");
            k = arrayOfString.readInt();
            m = arrayOfString.readInt();
            j = arrayOfString.readInt();
            param1Int2 = arrayOfString.readInt();
            arrayOfString = arrayOfString.createStringArray();
            setAudioRestriction(k, m, j, param1Int2, arrayOfString);
            param1Parcel2.writeNoException();
            return true;
          case 28:
            arrayOfString.enforceInterface("com.android.internal.app.IAppOpsService");
            j = arrayOfString.readInt();
            str4 = arrayOfString.readString();
            resetAllModes(j, str4);
            param1Parcel2.writeNoException();
            return true;
          case 27:
            str4.enforceInterface("com.android.internal.app.IAppOpsService");
            param1Int2 = str4.readInt();
            k = str4.readInt();
            str8 = str4.readString();
            j = str4.readInt();
            setMode(param1Int2, k, str8, j);
            param1Parcel2.writeNoException();
            return true;
          case 26:
            str4.enforceInterface("com.android.internal.app.IAppOpsService");
            j = str4.readInt();
            param1Int2 = str4.readInt();
            k = str4.readInt();
            setUidMode(j, param1Int2, k);
            param1Parcel2.writeNoException();
            return true;
          case 25:
            str4.enforceInterface("com.android.internal.app.IAppOpsService");
            j = str4.readInt();
            arrayOfInt3 = str4.createIntArray();
            list3 = getUidOps(j, arrayOfInt3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list3);
            return true;
          case 24:
            list3.enforceInterface("com.android.internal.app.IAppOpsService");
            l1 = list3.readLong();
            rebootHistory(l1);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            list3.enforceInterface("com.android.internal.app.IAppOpsService");
            clearHistory();
            param1Parcel2.writeNoException();
            return true;
          case 22:
            list3.enforceInterface("com.android.internal.app.IAppOpsService");
            resetHistoryParameters();
            param1Parcel2.writeNoException();
            return true;
          case 21:
            list3.enforceInterface("com.android.internal.app.IAppOpsService");
            if (list3.readInt() != 0) {
              AppOpsManager.HistoricalOps historicalOps = (AppOpsManager.HistoricalOps)AppOpsManager.HistoricalOps.CREATOR.createFromParcel((Parcel)list3);
            } else {
              list3 = null;
            } 
            addHistoricalOps((AppOpsManager.HistoricalOps)list3);
            param1Parcel2.writeNoException();
            return true;
          case 20:
            list3.enforceInterface("com.android.internal.app.IAppOpsService");
            j = list3.readInt();
            l1 = list3.readLong();
            param1Int2 = list3.readInt();
            setHistoryParameters(j, l1, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            list3.enforceInterface("com.android.internal.app.IAppOpsService");
            l1 = list3.readLong();
            offsetHistory(l1);
            param1Parcel2.writeNoException();
            return true;
          case 18:
            list3.enforceInterface("com.android.internal.app.IAppOpsService");
            k = list3.readInt();
            str8 = list3.readString();
            str14 = list3.readString();
            arrayList = list3.createStringArrayList();
            param1Int2 = list3.readInt();
            l2 = list3.readLong();
            l1 = list3.readLong();
            j = list3.readInt();
            if (list3.readInt() != 0) {
              RemoteCallback remoteCallback = (RemoteCallback)RemoteCallback.CREATOR.createFromParcel((Parcel)list3);
            } else {
              list3 = null;
            } 
            getHistoricalOpsFromDiskRaw(k, str8, str14, arrayList, param1Int2, l2, l1, j, (RemoteCallback)list3);
            param1Parcel2.writeNoException();
            return true;
          case 17:
            list3.enforceInterface("com.android.internal.app.IAppOpsService");
            k = list3.readInt();
            str8 = list3.readString();
            str14 = list3.readString();
            arrayList = list3.createStringArrayList();
            j = list3.readInt();
            l1 = list3.readLong();
            l2 = list3.readLong();
            param1Int2 = list3.readInt();
            if (list3.readInt() != 0) {
              RemoteCallback remoteCallback = (RemoteCallback)RemoteCallback.CREATOR.createFromParcel((Parcel)list3);
            } else {
              list3 = null;
            } 
            getHistoricalOps(k, str8, str14, arrayList, j, l1, l2, param1Int2, (RemoteCallback)list3);
            param1Parcel2.writeNoException();
            return true;
          case 16:
            list3.enforceInterface("com.android.internal.app.IAppOpsService");
            j = list3.readInt();
            str8 = list3.readString();
            arrayOfInt2 = list3.createIntArray();
            list2 = getOpsForPackage(j, str8, arrayOfInt2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list2);
            return true;
          case 15:
            list2.enforceInterface("com.android.internal.app.IAppOpsService");
            arrayOfInt1 = list2.createIntArray();
            list1 = getPackagesForOps(arrayOfInt1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list1);
            return true;
          case 14:
            list1.enforceInterface("com.android.internal.app.IAppOpsService");
            str12 = list1.readString();
            if (list1.readInt() != 0) {
              SyncNotedAppOp syncNotedAppOp = (SyncNotedAppOp)SyncNotedAppOp.CREATOR.createFromParcel((Parcel)list1);
            } else {
              str8 = null;
            } 
            str3 = list1.readString();
            messageSamplingConfig = reportRuntimeAppOpAccessMessageAndGetConfig(str12, (SyncNotedAppOp)str8, str3);
            param1Parcel2.writeNoException();
            if (messageSamplingConfig != null) {
              param1Parcel2.writeInt(1);
              messageSamplingConfig.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 13:
            messageSamplingConfig.enforceInterface("com.android.internal.app.IAppOpsService");
            runtimeAppOpAccessMessage = collectRuntimeAppOpAccessMessage();
            param1Parcel2.writeNoException();
            if (runtimeAppOpAccessMessage != null) {
              param1Parcel2.writeInt(1);
              runtimeAppOpAccessMessage.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 12:
            runtimeAppOpAccessMessage.enforceInterface("com.android.internal.app.IAppOpsService");
            j = runtimeAppOpAccessMessage.readInt();
            str2 = runtimeAppOpAccessMessage.readString();
            j = checkPackage(j, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 11:
            str2.enforceInterface("com.android.internal.app.IAppOpsService");
            j = str2.readInt();
            k = str2.readInt();
            str8 = str2.readString();
            str12 = str2.readString();
            param1Int2 = str2.readInt();
            str15 = str2.readString();
            str14 = str2.readString();
            if (str2.readInt() != 0) {
              bool3 = true;
            } else {
              bool3 = false;
            } 
            str16 = str2.readString();
            if (str2.readInt() != 0) {
              bool4 = true;
            } else {
              bool4 = false;
            } 
            j = noteProxyOperation(j, k, str8, str12, param1Int2, str15, str14, bool3, str16, bool4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 10:
            str2.enforceInterface("com.android.internal.app.IAppOpsService");
            j = str2.readInt();
            setCameraAudioRestriction(j);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            str2.enforceInterface("com.android.internal.app.IAppOpsService");
            j = str2.readInt();
            bool1 = shouldCollectNotes(j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 8:
            str2.enforceInterface("com.android.internal.app.IAppOpsService");
            param1Int2 = str2.readInt();
            i = str2.readInt();
            k = str2.readInt();
            str2 = str2.readString();
            i = checkAudioOperation(param1Int2, i, k, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 7:
            str2.enforceInterface("com.android.internal.app.IAppOpsService");
            str2 = str2.readString();
            i = permissionToOpCode(str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 6:
            str2.enforceInterface("com.android.internal.app.IAppOpsService");
            iAppOpsCallback1 = IAppOpsCallback.Stub.asInterface(str2.readStrongBinder());
            stopWatchingMode(iAppOpsCallback1);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            iAppOpsCallback1.enforceInterface("com.android.internal.app.IAppOpsService");
            i = iAppOpsCallback1.readInt();
            str8 = iAppOpsCallback1.readString();
            iAppOpsCallback1 = IAppOpsCallback.Stub.asInterface(iAppOpsCallback1.readStrongBinder());
            startWatchingMode(i, str8, iAppOpsCallback1);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            iAppOpsCallback1.enforceInterface("com.android.internal.app.IAppOpsService");
            iBinder1 = iAppOpsCallback1.readStrongBinder();
            i = iAppOpsCallback1.readInt();
            param1Int2 = iAppOpsCallback1.readInt();
            str12 = iAppOpsCallback1.readString();
            str1 = iAppOpsCallback1.readString();
            finishOperation(iBinder1, i, param1Int2, str12, str1);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            str1.enforceInterface("com.android.internal.app.IAppOpsService");
            iBinder3 = str1.readStrongBinder();
            i = str1.readInt();
            param1Int2 = str1.readInt();
            str16 = str1.readString();
            str7 = str1.readString();
            if (str1.readInt() != 0) {
              bool3 = true;
            } else {
              bool3 = false;
            } 
            if (str1.readInt() != 0) {
              bool4 = true;
            } else {
              bool4 = false;
            } 
            str14 = str1.readString();
            if (str1.readInt() != 0) {
              bool5 = true;
            } else {
              bool5 = false;
            } 
            i = startOperation(iBinder3, i, param1Int2, str16, str7, bool3, bool4, str14, bool5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 2:
            str1.enforceInterface("com.android.internal.app.IAppOpsService");
            i = str1.readInt();
            param1Int2 = str1.readInt();
            str11 = str1.readString();
            str14 = str1.readString();
            if (str1.readInt() != 0) {
              bool3 = true;
            } else {
              bool3 = false;
            } 
            str7 = str1.readString();
            if (str1.readInt() != 0) {
              bool4 = true;
            } else {
              bool4 = false;
            } 
            i = noteOperation(i, param1Int2, str11, str14, bool3, str7, bool4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("com.android.internal.app.IAppOpsService");
        param1Int2 = str1.readInt();
        int i = str1.readInt();
        String str1 = str1.readString();
        i = checkOperation(param1Int2, i, str1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(i);
        return true;
      } 
      param1Parcel2.writeString("com.android.internal.app.IAppOpsService");
      return true;
    }
    
    private static class Proxy implements IAppOpsService {
      public static IAppOpsService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.app.IAppOpsService";
      }
      
      public int checkOperation(int param2Int1, int param2Int2, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            param2Int1 = IAppOpsService.Stub.getDefaultImpl().checkOperation(param2Int1, param2Int2, param2String);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int noteOperation(int param2Int1, int param2Int2, String param2String1, String param2String2, boolean param2Boolean1, String param2String3, boolean param2Boolean2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeInt(param2Int2);
              try {
                parcel1.writeString(param2String1);
                try {
                  boolean bool2;
                  parcel1.writeString(param2String2);
                  boolean bool1 = true;
                  if (param2Boolean1) {
                    bool2 = true;
                  } else {
                    bool2 = false;
                  } 
                  parcel1.writeInt(bool2);
                  try {
                    parcel1.writeString(param2String3);
                    if (param2Boolean2) {
                      bool2 = bool1;
                    } else {
                      bool2 = false;
                    } 
                    parcel1.writeInt(bool2);
                    boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
                    if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
                      param2Int1 = IAppOpsService.Stub.getDefaultImpl().noteOperation(param2Int1, param2Int2, param2String1, param2String2, param2Boolean1, param2String3, param2Boolean2);
                      parcel2.recycle();
                      parcel1.recycle();
                      return param2Int1;
                    } 
                    parcel2.readException();
                    param2Int1 = parcel2.readInt();
                    parcel2.recycle();
                    parcel1.recycle();
                    return param2Int1;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public int startOperation(IBinder param2IBinder, int param2Int1, int param2Int2, String param2String1, String param2String2, boolean param2Boolean1, boolean param2Boolean2, String param2String3, boolean param2Boolean3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          try {
            parcel1.writeStrongBinder(param2IBinder);
            try {
              parcel1.writeInt(param2Int1);
              try {
                boolean bool2;
                parcel1.writeInt(param2Int2);
                parcel1.writeString(param2String1);
                parcel1.writeString(param2String2);
                boolean bool1 = true;
                if (param2Boolean1) {
                  bool2 = true;
                } else {
                  bool2 = false;
                } 
                parcel1.writeInt(bool2);
                if (param2Boolean2) {
                  bool2 = true;
                } else {
                  bool2 = false;
                } 
                parcel1.writeInt(bool2);
                parcel1.writeString(param2String3);
                if (param2Boolean3) {
                  bool2 = bool1;
                } else {
                  bool2 = false;
                } 
                parcel1.writeInt(bool2);
                boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
                if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
                  param2Int1 = IAppOpsService.Stub.getDefaultImpl().startOperation(param2IBinder, param2Int1, param2Int2, param2String1, param2String2, param2Boolean1, param2Boolean2, param2String3, param2Boolean3);
                  parcel2.recycle();
                  parcel1.recycle();
                  return param2Int1;
                } 
                parcel2.readException();
                param2Int1 = parcel2.readInt();
                parcel2.recycle();
                parcel1.recycle();
                return param2Int1;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IBinder;
      }
      
      public void finishOperation(IBinder param2IBinder, int param2Int1, int param2Int2, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().finishOperation(param2IBinder, param2Int1, param2Int2, param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startWatchingMode(int param2Int, String param2String, IAppOpsCallback param2IAppOpsCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          if (param2IAppOpsCallback != null) {
            iBinder = param2IAppOpsCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().startWatchingMode(param2Int, param2String, param2IAppOpsCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopWatchingMode(IAppOpsCallback param2IAppOpsCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          if (param2IAppOpsCallback != null) {
            iBinder = param2IAppOpsCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().stopWatchingMode(param2IAppOpsCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int permissionToOpCode(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null)
            return IAppOpsService.Stub.getDefaultImpl().permissionToOpCode(param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int checkAudioOperation(int param2Int1, int param2Int2, int param2Int3, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            param2Int1 = IAppOpsService.Stub.getDefaultImpl().checkAudioOperation(param2Int1, param2Int2, param2Int3, param2String);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean shouldCollectNotes(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(9, parcel1, parcel2, 0);
          if (!bool2 && IAppOpsService.Stub.getDefaultImpl() != null) {
            bool1 = IAppOpsService.Stub.getDefaultImpl().shouldCollectNotes(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setCameraAudioRestriction(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().setCameraAudioRestriction(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int noteProxyOperation(int param2Int1, int param2Int2, String param2String1, String param2String2, int param2Int3, String param2String3, String param2String4, boolean param2Boolean1, String param2String5, boolean param2Boolean2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          try {
            parcel1.writeInt(param2Int1);
            try {
              boolean bool2;
              parcel1.writeInt(param2Int2);
              parcel1.writeString(param2String1);
              parcel1.writeString(param2String2);
              parcel1.writeInt(param2Int3);
              parcel1.writeString(param2String3);
              parcel1.writeString(param2String4);
              boolean bool1 = true;
              if (param2Boolean1) {
                bool2 = true;
              } else {
                bool2 = false;
              } 
              parcel1.writeInt(bool2);
              parcel1.writeString(param2String5);
              if (param2Boolean2) {
                bool2 = bool1;
              } else {
                bool2 = false;
              } 
              parcel1.writeInt(bool2);
              boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
              if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
                param2Int1 = IAppOpsService.Stub.getDefaultImpl().noteProxyOperation(param2Int1, param2Int2, param2String1, param2String2, param2Int3, param2String3, param2String4, param2Boolean1, param2String5, param2Boolean2);
                parcel2.recycle();
                parcel1.recycle();
                return param2Int1;
              } 
              parcel2.readException();
              param2Int1 = parcel2.readInt();
              parcel2.recycle();
              parcel1.recycle();
              return param2Int1;
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public int checkPackage(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            param2Int = IAppOpsService.Stub.getDefaultImpl().checkPackage(param2Int, param2String);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public RuntimeAppOpAccessMessage collectRuntimeAppOpAccessMessage() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          RuntimeAppOpAccessMessage runtimeAppOpAccessMessage;
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            runtimeAppOpAccessMessage = IAppOpsService.Stub.getDefaultImpl().collectRuntimeAppOpAccessMessage();
            return runtimeAppOpAccessMessage;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            runtimeAppOpAccessMessage = (RuntimeAppOpAccessMessage)RuntimeAppOpAccessMessage.CREATOR.createFromParcel(parcel2);
          } else {
            runtimeAppOpAccessMessage = null;
          } 
          return runtimeAppOpAccessMessage;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public MessageSamplingConfig reportRuntimeAppOpAccessMessageAndGetConfig(String param2String1, SyncNotedAppOp param2SyncNotedAppOp, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeString(param2String1);
          if (param2SyncNotedAppOp != null) {
            parcel1.writeInt(1);
            param2SyncNotedAppOp.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null)
            return IAppOpsService.Stub.getDefaultImpl().reportRuntimeAppOpAccessMessageAndGetConfig(param2String1, param2SyncNotedAppOp, param2String2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            MessageSamplingConfig messageSamplingConfig = (MessageSamplingConfig)MessageSamplingConfig.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (MessageSamplingConfig)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<AppOpsManager.PackageOps> getPackagesForOps(int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null)
            return IAppOpsService.Stub.getDefaultImpl().getPackagesForOps(param2ArrayOfint); 
          parcel2.readException();
          return parcel2.createTypedArrayList(AppOpsManager.PackageOps.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<AppOpsManager.PackageOps> getOpsForPackage(int param2Int, String param2String, int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null)
            return IAppOpsService.Stub.getDefaultImpl().getOpsForPackage(param2Int, param2String, param2ArrayOfint); 
          parcel2.readException();
          return parcel2.createTypedArrayList(AppOpsManager.PackageOps.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void getHistoricalOps(int param2Int1, String param2String1, String param2String2, List<String> param2List, int param2Int2, long param2Long1, long param2Long2, int param2Int3, RemoteCallback param2RemoteCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeStringList(param2List);
          parcel1.writeInt(param2Int2);
          parcel1.writeLong(param2Long1);
          parcel1.writeLong(param2Long2);
          parcel1.writeInt(param2Int3);
          if (param2RemoteCallback != null) {
            parcel1.writeInt(1);
            param2RemoteCallback.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().getHistoricalOps(param2Int1, param2String1, param2String2, param2List, param2Int2, param2Long1, param2Long2, param2Int3, param2RemoteCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void getHistoricalOpsFromDiskRaw(int param2Int1, String param2String1, String param2String2, List<String> param2List, int param2Int2, long param2Long1, long param2Long2, int param2Int3, RemoteCallback param2RemoteCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeStringList(param2List);
          parcel1.writeInt(param2Int2);
          parcel1.writeLong(param2Long1);
          parcel1.writeLong(param2Long2);
          parcel1.writeInt(param2Int3);
          if (param2RemoteCallback != null) {
            parcel1.writeInt(1);
            param2RemoteCallback.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().getHistoricalOpsFromDiskRaw(param2Int1, param2String1, param2String2, param2List, param2Int2, param2Long1, param2Long2, param2Int3, param2RemoteCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void offsetHistory(long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().offsetHistory(param2Long);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setHistoryParameters(int param2Int1, long param2Long, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeInt(param2Int1);
          parcel1.writeLong(param2Long);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().setHistoryParameters(param2Int1, param2Long, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addHistoricalOps(AppOpsManager.HistoricalOps param2HistoricalOps) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          if (param2HistoricalOps != null) {
            parcel1.writeInt(1);
            param2HistoricalOps.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().addHistoricalOps(param2HistoricalOps);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resetHistoryParameters() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().resetHistoryParameters();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearHistory() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().clearHistory();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void rebootHistory(long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().rebootHistory(param2Long);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<AppOpsManager.PackageOps> getUidOps(int param2Int, int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeInt(param2Int);
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null)
            return IAppOpsService.Stub.getDefaultImpl().getUidOps(param2Int, param2ArrayOfint); 
          parcel2.readException();
          return parcel2.createTypedArrayList(AppOpsManager.PackageOps.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUidMode(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().setUidMode(param2Int1, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setMode(int param2Int1, int param2Int2, String param2String, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().setMode(param2Int1, param2Int2, param2String, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resetAllModes(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().resetAllModes(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAudioRestriction(int param2Int1, int param2Int2, int param2Int3, int param2Int4, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          parcel1.writeInt(param2Int4);
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().setAudioRestriction(param2Int1, param2Int2, param2Int3, param2Int4, param2ArrayOfString);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUserRestrictions(Bundle param2Bundle, IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().setUserRestrictions(param2Bundle, param2IBinder, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUserRestriction(int param2Int1, boolean param2Boolean, IBinder param2IBinder, int param2Int2, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeInt(param2Int1);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int2);
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool1 = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool1 && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().setUserRestriction(param2Int1, param2Boolean, param2IBinder, param2Int2, param2ArrayOfString);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeUser(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().removeUser(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startWatchingActive(int[] param2ArrayOfint, IAppOpsActiveCallback param2IAppOpsActiveCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeIntArray(param2ArrayOfint);
          if (param2IAppOpsActiveCallback != null) {
            iBinder = param2IAppOpsActiveCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().startWatchingActive(param2ArrayOfint, param2IAppOpsActiveCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopWatchingActive(IAppOpsActiveCallback param2IAppOpsActiveCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          if (param2IAppOpsActiveCallback != null) {
            iBinder = param2IAppOpsActiveCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().stopWatchingActive(param2IAppOpsActiveCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isOperationActive(int param2Int1, int param2Int2, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(35, parcel1, parcel2, 0);
          if (!bool2 && IAppOpsService.Stub.getDefaultImpl() != null) {
            bool1 = IAppOpsService.Stub.getDefaultImpl().isOperationActive(param2Int1, param2Int2, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startWatchingStarted(int[] param2ArrayOfint, IAppOpsStartedCallback param2IAppOpsStartedCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeIntArray(param2ArrayOfint);
          if (param2IAppOpsStartedCallback != null) {
            iBinder = param2IAppOpsStartedCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().startWatchingStarted(param2ArrayOfint, param2IAppOpsStartedCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopWatchingStarted(IAppOpsStartedCallback param2IAppOpsStartedCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          if (param2IAppOpsStartedCallback != null) {
            iBinder = param2IAppOpsStartedCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().stopWatchingStarted(param2IAppOpsStartedCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startWatchingModeWithFlags(int param2Int1, String param2String, int param2Int2, IAppOpsCallback param2IAppOpsCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          if (param2IAppOpsCallback != null) {
            iBinder = param2IAppOpsCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(38, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().startWatchingModeWithFlags(param2Int1, param2String, param2Int2, param2IAppOpsCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startWatchingNoted(int[] param2ArrayOfint, IAppOpsNotedCallback param2IAppOpsNotedCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeIntArray(param2ArrayOfint);
          if (param2IAppOpsNotedCallback != null) {
            iBinder = param2IAppOpsNotedCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().startWatchingNoted(param2ArrayOfint, param2IAppOpsNotedCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopWatchingNoted(IAppOpsNotedCallback param2IAppOpsNotedCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          if (param2IAppOpsNotedCallback != null) {
            iBinder = param2IAppOpsNotedCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(40, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().stopWatchingNoted(param2IAppOpsNotedCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startWatchingAsyncNoted(String param2String, IAppOpsAsyncNotedCallback param2IAppOpsAsyncNotedCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeString(param2String);
          if (param2IAppOpsAsyncNotedCallback != null) {
            iBinder = param2IAppOpsAsyncNotedCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(41, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().startWatchingAsyncNoted(param2String, param2IAppOpsAsyncNotedCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopWatchingAsyncNoted(String param2String, IAppOpsAsyncNotedCallback param2IAppOpsAsyncNotedCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeString(param2String);
          if (param2IAppOpsAsyncNotedCallback != null) {
            iBinder = param2IAppOpsAsyncNotedCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(42, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().stopWatchingAsyncNoted(param2String, param2IAppOpsAsyncNotedCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<AsyncNotedAppOp> extractAsyncOps(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(43, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null)
            return IAppOpsService.Stub.getDefaultImpl().extractAsyncOps(param2String); 
          parcel2.readException();
          return parcel2.createTypedArrayList(AsyncNotedAppOp.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int checkOperationRaw(int param2Int1, int param2Int2, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(44, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            param2Int1 = IAppOpsService.Stub.getDefaultImpl().checkOperationRaw(param2Int1, param2Int2, param2String);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reloadNonHistoricalState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          boolean bool = this.mRemote.transact(45, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().reloadNonHistoricalState();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void collectNoteOpCallsForValidation(String param2String1, int param2Int, String param2String2, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IAppOpsService");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String2);
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(46, parcel1, parcel2, 0);
          if (!bool && IAppOpsService.Stub.getDefaultImpl() != null) {
            IAppOpsService.Stub.getDefaultImpl().collectNoteOpCallsForValidation(param2String1, param2Int, param2String2, param2Long);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAppOpsService param1IAppOpsService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAppOpsService != null) {
          Proxy.sDefaultImpl = param1IAppOpsService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAppOpsService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
