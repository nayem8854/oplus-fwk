package com.android.internal.app;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class MicroAlertController extends AlertController {
  public MicroAlertController(Context paramContext, DialogInterface paramDialogInterface, Window paramWindow) {
    super(paramContext, paramDialogInterface, paramWindow);
  }
  
  protected void setupContent(ViewGroup paramViewGroup) {
    this.mScrollView = this.mWindow.<ScrollView>findViewById(16909386);
    this.mMessageView = paramViewGroup.<TextView>findViewById(16908299);
    if (this.mMessageView == null)
      return; 
    if (this.mMessage != null) {
      this.mMessageView.setText(this.mMessage);
    } else {
      this.mMessageView.setVisibility(8);
      paramViewGroup.removeView(this.mMessageView);
      if (this.mListView != null) {
        paramViewGroup = this.mScrollView.findViewById(16909558);
        ((ViewGroup)paramViewGroup.getParent()).removeView(paramViewGroup);
        FrameLayout.LayoutParams layoutParams1 = new FrameLayout.LayoutParams(paramViewGroup.getLayoutParams());
        layoutParams1.gravity = 48;
        paramViewGroup.setLayoutParams(layoutParams1);
        layoutParams1 = this.mScrollView.findViewById(16908809);
        ((ViewGroup)layoutParams1.getParent()).removeView((View)layoutParams1);
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(layoutParams1.getLayoutParams());
        layoutParams2.gravity = 80;
        layoutParams1.setLayoutParams(layoutParams2);
        ViewGroup viewGroup = (ViewGroup)this.mScrollView.getParent();
        int i = viewGroup.indexOfChild(this.mScrollView);
        viewGroup.removeViewAt(i);
        viewGroup.addView(this.mListView, new ViewGroup.LayoutParams(-1, -1));
        viewGroup.addView(paramViewGroup);
        viewGroup.addView((View)layoutParams1);
      } else {
        paramViewGroup.setVisibility(8);
      } 
    } 
  }
  
  protected void setupTitle(ViewGroup paramViewGroup) {
    super.setupTitle(paramViewGroup);
    if (paramViewGroup.getVisibility() == 8)
      paramViewGroup.setVisibility(4); 
  }
  
  protected void setupButtons(ViewGroup paramViewGroup) {
    super.setupButtons(paramViewGroup);
    if (paramViewGroup.getVisibility() == 8)
      paramViewGroup.setVisibility(4); 
  }
}
