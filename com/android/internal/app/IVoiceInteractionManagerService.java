package com.android.internal.app;

import android.content.ComponentName;
import android.content.Intent;
import android.hardware.soundtrigger.IRecognitionStatusCallback;
import android.hardware.soundtrigger.KeyphraseMetadata;
import android.hardware.soundtrigger.SoundTrigger;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteCallback;
import android.os.RemoteException;
import android.service.voice.IVoiceInteractionSession;
import java.util.ArrayList;
import java.util.List;

public interface IVoiceInteractionManagerService extends IInterface {
  boolean activeServiceSupportsAssist() throws RemoteException;
  
  boolean activeServiceSupportsLaunchFromKeyguard() throws RemoteException;
  
  void closeSystemDialogs(IBinder paramIBinder) throws RemoteException;
  
  int deleteKeyphraseSoundModel(int paramInt, String paramString) throws RemoteException;
  
  boolean deliverNewSession(IBinder paramIBinder, IVoiceInteractionSession paramIVoiceInteractionSession, IVoiceInteractor paramIVoiceInteractor) throws RemoteException;
  
  void finish(IBinder paramIBinder) throws RemoteException;
  
  ComponentName getActiveServiceComponentName() throws RemoteException;
  
  void getActiveServiceSupportedActions(List<String> paramList, IVoiceActionCheckCallback paramIVoiceActionCheckCallback) throws RemoteException;
  
  int getDisabledShowContext() throws RemoteException;
  
  SoundTrigger.ModuleProperties getDspModuleProperties() throws RemoteException;
  
  KeyphraseMetadata getEnrolledKeyphraseMetadata(String paramString1, String paramString2) throws RemoteException;
  
  SoundTrigger.KeyphraseSoundModel getKeyphraseSoundModel(int paramInt, String paramString) throws RemoteException;
  
  int getParameter(int paramInt1, int paramInt2) throws RemoteException;
  
  int getUserDisabledShowContext() throws RemoteException;
  
  void hideCurrentSession() throws RemoteException;
  
  boolean hideSessionFromSession(IBinder paramIBinder) throws RemoteException;
  
  boolean isEnrolledForKeyphrase(int paramInt, String paramString) throws RemoteException;
  
  boolean isSessionRunning() throws RemoteException;
  
  void launchVoiceAssistFromKeyguard() throws RemoteException;
  
  void onLockscreenShown() throws RemoteException;
  
  void performDirectAction(IBinder paramIBinder1, String paramString, Bundle paramBundle, int paramInt, IBinder paramIBinder2, RemoteCallback paramRemoteCallback1, RemoteCallback paramRemoteCallback2) throws RemoteException;
  
  SoundTrigger.ModelParamRange queryParameter(int paramInt1, int paramInt2) throws RemoteException;
  
  void registerVoiceInteractionSessionListener(IVoiceInteractionSessionListener paramIVoiceInteractionSessionListener) throws RemoteException;
  
  void requestDirectActions(IBinder paramIBinder1, int paramInt, IBinder paramIBinder2, RemoteCallback paramRemoteCallback1, RemoteCallback paramRemoteCallback2) throws RemoteException;
  
  void setDisabled(boolean paramBoolean) throws RemoteException;
  
  void setDisabledShowContext(int paramInt) throws RemoteException;
  
  void setKeepAwake(IBinder paramIBinder, boolean paramBoolean) throws RemoteException;
  
  int setParameter(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void setUiHints(Bundle paramBundle) throws RemoteException;
  
  void showSession(Bundle paramBundle, int paramInt) throws RemoteException;
  
  boolean showSessionForActiveService(Bundle paramBundle, int paramInt, IVoiceInteractionSessionShowCallback paramIVoiceInteractionSessionShowCallback, IBinder paramIBinder) throws RemoteException;
  
  boolean showSessionFromSession(IBinder paramIBinder, Bundle paramBundle, int paramInt) throws RemoteException;
  
  int startAssistantActivity(IBinder paramIBinder, Intent paramIntent, String paramString1, String paramString2) throws RemoteException;
  
  int startRecognition(int paramInt, String paramString, IRecognitionStatusCallback paramIRecognitionStatusCallback, SoundTrigger.RecognitionConfig paramRecognitionConfig) throws RemoteException;
  
  int startVoiceActivity(IBinder paramIBinder, Intent paramIntent, String paramString1, String paramString2) throws RemoteException;
  
  int stopRecognition(int paramInt, IRecognitionStatusCallback paramIRecognitionStatusCallback) throws RemoteException;
  
  int updateKeyphraseSoundModel(SoundTrigger.KeyphraseSoundModel paramKeyphraseSoundModel) throws RemoteException;
  
  class Default implements IVoiceInteractionManagerService {
    public void showSession(Bundle param1Bundle, int param1Int) throws RemoteException {}
    
    public boolean deliverNewSession(IBinder param1IBinder, IVoiceInteractionSession param1IVoiceInteractionSession, IVoiceInteractor param1IVoiceInteractor) throws RemoteException {
      return false;
    }
    
    public boolean showSessionFromSession(IBinder param1IBinder, Bundle param1Bundle, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean hideSessionFromSession(IBinder param1IBinder) throws RemoteException {
      return false;
    }
    
    public int startVoiceActivity(IBinder param1IBinder, Intent param1Intent, String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public int startAssistantActivity(IBinder param1IBinder, Intent param1Intent, String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public void setKeepAwake(IBinder param1IBinder, boolean param1Boolean) throws RemoteException {}
    
    public void closeSystemDialogs(IBinder param1IBinder) throws RemoteException {}
    
    public void finish(IBinder param1IBinder) throws RemoteException {}
    
    public void setDisabledShowContext(int param1Int) throws RemoteException {}
    
    public int getDisabledShowContext() throws RemoteException {
      return 0;
    }
    
    public int getUserDisabledShowContext() throws RemoteException {
      return 0;
    }
    
    public SoundTrigger.KeyphraseSoundModel getKeyphraseSoundModel(int param1Int, String param1String) throws RemoteException {
      return null;
    }
    
    public int updateKeyphraseSoundModel(SoundTrigger.KeyphraseSoundModel param1KeyphraseSoundModel) throws RemoteException {
      return 0;
    }
    
    public int deleteKeyphraseSoundModel(int param1Int, String param1String) throws RemoteException {
      return 0;
    }
    
    public SoundTrigger.ModuleProperties getDspModuleProperties() throws RemoteException {
      return null;
    }
    
    public boolean isEnrolledForKeyphrase(int param1Int, String param1String) throws RemoteException {
      return false;
    }
    
    public KeyphraseMetadata getEnrolledKeyphraseMetadata(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public int startRecognition(int param1Int, String param1String, IRecognitionStatusCallback param1IRecognitionStatusCallback, SoundTrigger.RecognitionConfig param1RecognitionConfig) throws RemoteException {
      return 0;
    }
    
    public int stopRecognition(int param1Int, IRecognitionStatusCallback param1IRecognitionStatusCallback) throws RemoteException {
      return 0;
    }
    
    public int setParameter(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {
      return 0;
    }
    
    public int getParameter(int param1Int1, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public SoundTrigger.ModelParamRange queryParameter(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public ComponentName getActiveServiceComponentName() throws RemoteException {
      return null;
    }
    
    public boolean showSessionForActiveService(Bundle param1Bundle, int param1Int, IVoiceInteractionSessionShowCallback param1IVoiceInteractionSessionShowCallback, IBinder param1IBinder) throws RemoteException {
      return false;
    }
    
    public void hideCurrentSession() throws RemoteException {}
    
    public void launchVoiceAssistFromKeyguard() throws RemoteException {}
    
    public boolean isSessionRunning() throws RemoteException {
      return false;
    }
    
    public boolean activeServiceSupportsAssist() throws RemoteException {
      return false;
    }
    
    public boolean activeServiceSupportsLaunchFromKeyguard() throws RemoteException {
      return false;
    }
    
    public void onLockscreenShown() throws RemoteException {}
    
    public void registerVoiceInteractionSessionListener(IVoiceInteractionSessionListener param1IVoiceInteractionSessionListener) throws RemoteException {}
    
    public void getActiveServiceSupportedActions(List<String> param1List, IVoiceActionCheckCallback param1IVoiceActionCheckCallback) throws RemoteException {}
    
    public void setUiHints(Bundle param1Bundle) throws RemoteException {}
    
    public void requestDirectActions(IBinder param1IBinder1, int param1Int, IBinder param1IBinder2, RemoteCallback param1RemoteCallback1, RemoteCallback param1RemoteCallback2) throws RemoteException {}
    
    public void performDirectAction(IBinder param1IBinder1, String param1String, Bundle param1Bundle, int param1Int, IBinder param1IBinder2, RemoteCallback param1RemoteCallback1, RemoteCallback param1RemoteCallback2) throws RemoteException {}
    
    public void setDisabled(boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IVoiceInteractionManagerService {
    private static final String DESCRIPTOR = "com.android.internal.app.IVoiceInteractionManagerService";
    
    static final int TRANSACTION_activeServiceSupportsAssist = 29;
    
    static final int TRANSACTION_activeServiceSupportsLaunchFromKeyguard = 30;
    
    static final int TRANSACTION_closeSystemDialogs = 8;
    
    static final int TRANSACTION_deleteKeyphraseSoundModel = 15;
    
    static final int TRANSACTION_deliverNewSession = 2;
    
    static final int TRANSACTION_finish = 9;
    
    static final int TRANSACTION_getActiveServiceComponentName = 24;
    
    static final int TRANSACTION_getActiveServiceSupportedActions = 33;
    
    static final int TRANSACTION_getDisabledShowContext = 11;
    
    static final int TRANSACTION_getDspModuleProperties = 16;
    
    static final int TRANSACTION_getEnrolledKeyphraseMetadata = 18;
    
    static final int TRANSACTION_getKeyphraseSoundModel = 13;
    
    static final int TRANSACTION_getParameter = 22;
    
    static final int TRANSACTION_getUserDisabledShowContext = 12;
    
    static final int TRANSACTION_hideCurrentSession = 26;
    
    static final int TRANSACTION_hideSessionFromSession = 4;
    
    static final int TRANSACTION_isEnrolledForKeyphrase = 17;
    
    static final int TRANSACTION_isSessionRunning = 28;
    
    static final int TRANSACTION_launchVoiceAssistFromKeyguard = 27;
    
    static final int TRANSACTION_onLockscreenShown = 31;
    
    static final int TRANSACTION_performDirectAction = 36;
    
    static final int TRANSACTION_queryParameter = 23;
    
    static final int TRANSACTION_registerVoiceInteractionSessionListener = 32;
    
    static final int TRANSACTION_requestDirectActions = 35;
    
    static final int TRANSACTION_setDisabled = 37;
    
    static final int TRANSACTION_setDisabledShowContext = 10;
    
    static final int TRANSACTION_setKeepAwake = 7;
    
    static final int TRANSACTION_setParameter = 21;
    
    static final int TRANSACTION_setUiHints = 34;
    
    static final int TRANSACTION_showSession = 1;
    
    static final int TRANSACTION_showSessionForActiveService = 25;
    
    static final int TRANSACTION_showSessionFromSession = 3;
    
    static final int TRANSACTION_startAssistantActivity = 6;
    
    static final int TRANSACTION_startRecognition = 19;
    
    static final int TRANSACTION_startVoiceActivity = 5;
    
    static final int TRANSACTION_stopRecognition = 20;
    
    static final int TRANSACTION_updateKeyphraseSoundModel = 14;
    
    public Stub() {
      attachInterface(this, "com.android.internal.app.IVoiceInteractionManagerService");
    }
    
    public static IVoiceInteractionManagerService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.app.IVoiceInteractionManagerService");
      if (iInterface != null && iInterface instanceof IVoiceInteractionManagerService)
        return (IVoiceInteractionManagerService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 37:
          return "setDisabled";
        case 36:
          return "performDirectAction";
        case 35:
          return "requestDirectActions";
        case 34:
          return "setUiHints";
        case 33:
          return "getActiveServiceSupportedActions";
        case 32:
          return "registerVoiceInteractionSessionListener";
        case 31:
          return "onLockscreenShown";
        case 30:
          return "activeServiceSupportsLaunchFromKeyguard";
        case 29:
          return "activeServiceSupportsAssist";
        case 28:
          return "isSessionRunning";
        case 27:
          return "launchVoiceAssistFromKeyguard";
        case 26:
          return "hideCurrentSession";
        case 25:
          return "showSessionForActiveService";
        case 24:
          return "getActiveServiceComponentName";
        case 23:
          return "queryParameter";
        case 22:
          return "getParameter";
        case 21:
          return "setParameter";
        case 20:
          return "stopRecognition";
        case 19:
          return "startRecognition";
        case 18:
          return "getEnrolledKeyphraseMetadata";
        case 17:
          return "isEnrolledForKeyphrase";
        case 16:
          return "getDspModuleProperties";
        case 15:
          return "deleteKeyphraseSoundModel";
        case 14:
          return "updateKeyphraseSoundModel";
        case 13:
          return "getKeyphraseSoundModel";
        case 12:
          return "getUserDisabledShowContext";
        case 11:
          return "getDisabledShowContext";
        case 10:
          return "setDisabledShowContext";
        case 9:
          return "finish";
        case 8:
          return "closeSystemDialogs";
        case 7:
          return "setKeepAwake";
        case 6:
          return "startAssistantActivity";
        case 5:
          return "startVoiceActivity";
        case 4:
          return "hideSessionFromSession";
        case 3:
          return "showSessionFromSession";
        case 2:
          return "deliverNewSession";
        case 1:
          break;
      } 
      return "showSession";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool5;
        int n;
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        IVoiceActionCheckCallback iVoiceActionCheckCallback;
        IVoiceInteractionSessionListener iVoiceInteractionSessionListener;
        IBinder iBinder3;
        ComponentName componentName;
        SoundTrigger.ModelParamRange modelParamRange;
        IRecognitionStatusCallback iRecognitionStatusCallback1;
        String str4;
        KeyphraseMetadata keyphraseMetadata;
        String str3;
        SoundTrigger.ModuleProperties moduleProperties;
        String str2;
        SoundTrigger.KeyphraseSoundModel keyphraseSoundModel;
        IBinder iBinder2;
        String str1;
        IBinder iBinder1;
        IVoiceInteractor iVoiceInteractor;
        IBinder iBinder4;
        String str6;
        IBinder iBinder5;
        String str5;
        Bundle bundle;
        ArrayList<String> arrayList;
        String str7;
        IBinder iBinder6;
        IVoiceInteractionSession iVoiceInteractionSession;
        IBinder iBinder7;
        RemoteCallback remoteCallback;
        IBinder iBinder9;
        IVoiceInteractionSessionShowCallback iVoiceInteractionSessionShowCallback;
        IRecognitionStatusCallback iRecognitionStatusCallback2;
        IBinder iBinder8;
        int i1;
        boolean bool6 = false, bool7 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 37:
            param1Parcel1.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            bool6 = bool7;
            if (param1Parcel1.readInt() != 0)
              bool6 = true; 
            setDisabled(bool6);
            param1Parcel2.writeNoException();
            return true;
          case 36:
            param1Parcel1.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            iBinder4 = param1Parcel1.readStrongBinder();
            str6 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              bundle = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            iBinder7 = param1Parcel1.readStrongBinder();
            if (param1Parcel1.readInt() != 0) {
              remoteCallback = (RemoteCallback)RemoteCallback.CREATOR.createFromParcel(param1Parcel1);
            } else {
              remoteCallback = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              RemoteCallback remoteCallback1 = (RemoteCallback)RemoteCallback.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            performDirectAction(iBinder4, str6, bundle, param1Int1, iBinder7, remoteCallback, (RemoteCallback)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 35:
            param1Parcel1.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            iBinder5 = param1Parcel1.readStrongBinder();
            param1Int1 = param1Parcel1.readInt();
            iBinder9 = param1Parcel1.readStrongBinder();
            if (param1Parcel1.readInt() != 0) {
              RemoteCallback remoteCallback1 = (RemoteCallback)RemoteCallback.CREATOR.createFromParcel(param1Parcel1);
            } else {
              bundle = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              RemoteCallback remoteCallback1 = (RemoteCallback)RemoteCallback.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            requestDirectActions(iBinder5, param1Int1, iBinder9, (RemoteCallback)bundle, (RemoteCallback)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 34:
            param1Parcel1.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle1 = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setUiHints((Bundle)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 33:
            param1Parcel1.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            arrayList = param1Parcel1.createStringArrayList();
            iVoiceActionCheckCallback = IVoiceActionCheckCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            getActiveServiceSupportedActions(arrayList, iVoiceActionCheckCallback);
            param1Parcel2.writeNoException();
            return true;
          case 32:
            iVoiceActionCheckCallback.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            iVoiceInteractionSessionListener = IVoiceInteractionSessionListener.Stub.asInterface(iVoiceActionCheckCallback.readStrongBinder());
            registerVoiceInteractionSessionListener(iVoiceInteractionSessionListener);
            param1Parcel2.writeNoException();
            return true;
          case 31:
            iVoiceInteractionSessionListener.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            onLockscreenShown();
            param1Parcel2.writeNoException();
            return true;
          case 30:
            iVoiceInteractionSessionListener.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            bool5 = activeServiceSupportsLaunchFromKeyguard();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 29:
            iVoiceInteractionSessionListener.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            bool5 = activeServiceSupportsAssist();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 28:
            iVoiceInteractionSessionListener.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            bool5 = isSessionRunning();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 27:
            iVoiceInteractionSessionListener.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            launchVoiceAssistFromKeyguard();
            param1Parcel2.writeNoException();
            return true;
          case 26:
            iVoiceInteractionSessionListener.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            hideCurrentSession();
            param1Parcel2.writeNoException();
            return true;
          case 25:
            iVoiceInteractionSessionListener.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            if (iVoiceInteractionSessionListener.readInt() != 0) {
              Bundle bundle1 = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iVoiceInteractionSessionListener);
            } else {
              arrayList = null;
            } 
            n = iVoiceInteractionSessionListener.readInt();
            iVoiceInteractionSessionShowCallback = IVoiceInteractionSessionShowCallback.Stub.asInterface(iVoiceInteractionSessionListener.readStrongBinder());
            iBinder3 = iVoiceInteractionSessionListener.readStrongBinder();
            bool4 = showSessionForActiveService((Bundle)arrayList, n, iVoiceInteractionSessionShowCallback, iBinder3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 24:
            iBinder3.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            componentName = getActiveServiceComponentName();
            param1Parcel2.writeNoException();
            if (componentName != null) {
              param1Parcel2.writeInt(1);
              componentName.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 23:
            componentName.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            m = componentName.readInt();
            param1Int2 = componentName.readInt();
            modelParamRange = queryParameter(m, param1Int2);
            param1Parcel2.writeNoException();
            if (modelParamRange != null) {
              param1Parcel2.writeInt(1);
              modelParamRange.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 22:
            modelParamRange.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            m = modelParamRange.readInt();
            param1Int2 = modelParamRange.readInt();
            m = getParameter(m, param1Int2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(m);
            return true;
          case 21:
            modelParamRange.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            param1Int2 = modelParamRange.readInt();
            i1 = modelParamRange.readInt();
            m = modelParamRange.readInt();
            m = setParameter(param1Int2, i1, m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(m);
            return true;
          case 20:
            modelParamRange.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            m = modelParamRange.readInt();
            iRecognitionStatusCallback1 = IRecognitionStatusCallback.Stub.asInterface(modelParamRange.readStrongBinder());
            m = stopRecognition(m, iRecognitionStatusCallback1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(m);
            return true;
          case 19:
            iRecognitionStatusCallback1.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            m = iRecognitionStatusCallback1.readInt();
            str7 = iRecognitionStatusCallback1.readString();
            iRecognitionStatusCallback2 = IRecognitionStatusCallback.Stub.asInterface(iRecognitionStatusCallback1.readStrongBinder());
            if (iRecognitionStatusCallback1.readInt() != 0) {
              SoundTrigger.RecognitionConfig recognitionConfig = (SoundTrigger.RecognitionConfig)SoundTrigger.RecognitionConfig.CREATOR.createFromParcel((Parcel)iRecognitionStatusCallback1);
            } else {
              iRecognitionStatusCallback1 = null;
            } 
            m = startRecognition(m, str7, iRecognitionStatusCallback2, (SoundTrigger.RecognitionConfig)iRecognitionStatusCallback1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(m);
            return true;
          case 18:
            iRecognitionStatusCallback1.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            str7 = iRecognitionStatusCallback1.readString();
            str4 = iRecognitionStatusCallback1.readString();
            keyphraseMetadata = getEnrolledKeyphraseMetadata(str7, str4);
            param1Parcel2.writeNoException();
            if (keyphraseMetadata != null) {
              param1Parcel2.writeInt(1);
              keyphraseMetadata.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 17:
            keyphraseMetadata.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            m = keyphraseMetadata.readInt();
            str3 = keyphraseMetadata.readString();
            bool3 = isEnrolledForKeyphrase(m, str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 16:
            str3.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            moduleProperties = getDspModuleProperties();
            param1Parcel2.writeNoException();
            if (moduleProperties != null) {
              param1Parcel2.writeInt(1);
              moduleProperties.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 15:
            moduleProperties.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            k = moduleProperties.readInt();
            str2 = moduleProperties.readString();
            k = deleteKeyphraseSoundModel(k, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 14:
            str2.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            if (str2.readInt() != 0) {
              SoundTrigger.KeyphraseSoundModel keyphraseSoundModel1 = (SoundTrigger.KeyphraseSoundModel)SoundTrigger.KeyphraseSoundModel.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            k = updateKeyphraseSoundModel((SoundTrigger.KeyphraseSoundModel)str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 13:
            str2.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            k = str2.readInt();
            str2 = str2.readString();
            keyphraseSoundModel = getKeyphraseSoundModel(k, str2);
            param1Parcel2.writeNoException();
            if (keyphraseSoundModel != null) {
              param1Parcel2.writeInt(1);
              keyphraseSoundModel.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 12:
            keyphraseSoundModel.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            k = getUserDisabledShowContext();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 11:
            keyphraseSoundModel.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            k = getDisabledShowContext();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 10:
            keyphraseSoundModel.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            k = keyphraseSoundModel.readInt();
            setDisabledShowContext(k);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            keyphraseSoundModel.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            iBinder2 = keyphraseSoundModel.readStrongBinder();
            finish(iBinder2);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            iBinder2.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            iBinder2 = iBinder2.readStrongBinder();
            closeSystemDialogs(iBinder2);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            iBinder2.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            iBinder6 = iBinder2.readStrongBinder();
            if (iBinder2.readInt() != 0)
              bool6 = true; 
            setKeepAwake(iBinder6, bool6);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            iBinder2.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            iBinder8 = iBinder2.readStrongBinder();
            if (iBinder2.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)iBinder2);
            } else {
              iBinder6 = null;
            } 
            str5 = iBinder2.readString();
            str1 = iBinder2.readString();
            k = startAssistantActivity(iBinder8, (Intent)iBinder6, str5, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 5:
            str1.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            iBinder8 = str1.readStrongBinder();
            if (str1.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)str1);
            } else {
              iBinder6 = null;
            } 
            str5 = str1.readString();
            str1 = str1.readString();
            k = startVoiceActivity(iBinder8, (Intent)iBinder6, str5, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 4:
            str1.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            iBinder1 = str1.readStrongBinder();
            bool2 = hideSessionFromSession(iBinder1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 3:
            iBinder1.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            iBinder8 = iBinder1.readStrongBinder();
            if (iBinder1.readInt() != 0) {
              Bundle bundle1 = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              iBinder6 = null;
            } 
            j = iBinder1.readInt();
            bool1 = showSessionFromSession(iBinder8, (Bundle)iBinder6, j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 2:
            iBinder1.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
            iBinder8 = iBinder1.readStrongBinder();
            iVoiceInteractionSession = IVoiceInteractionSession.Stub.asInterface(iBinder1.readStrongBinder());
            iVoiceInteractor = IVoiceInteractor.Stub.asInterface(iBinder1.readStrongBinder());
            bool1 = deliverNewSession(iBinder8, iVoiceInteractionSession, iVoiceInteractor);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 1:
            break;
        } 
        iVoiceInteractor.enforceInterface("com.android.internal.app.IVoiceInteractionManagerService");
        if (iVoiceInteractor.readInt() != 0) {
          Bundle bundle1 = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iVoiceInteractor);
        } else {
          iVoiceInteractionSession = null;
        } 
        int i = iVoiceInteractor.readInt();
        showSession((Bundle)iVoiceInteractionSession, i);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("com.android.internal.app.IVoiceInteractionManagerService");
      return true;
    }
    
    private static class Proxy implements IVoiceInteractionManagerService {
      public static IVoiceInteractionManagerService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.app.IVoiceInteractionManagerService";
      }
      
      public void showSession(Bundle param2Bundle, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            IVoiceInteractionManagerService.Stub.getDefaultImpl().showSession(param2Bundle, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean deliverNewSession(IBinder param2IBinder, IVoiceInteractionSession param2IVoiceInteractionSession, IVoiceInteractor param2IVoiceInteractor) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          parcel1.writeStrongBinder(param2IBinder);
          IBinder iBinder1 = null;
          if (param2IVoiceInteractionSession != null) {
            iBinder2 = param2IVoiceInteractionSession.asBinder();
          } else {
            iBinder2 = null;
          } 
          parcel1.writeStrongBinder(iBinder2);
          IBinder iBinder2 = iBinder1;
          if (param2IVoiceInteractor != null)
            iBinder2 = param2IVoiceInteractor.asBinder(); 
          parcel1.writeStrongBinder(iBinder2);
          iBinder2 = this.mRemote;
          boolean bool1 = false, bool2 = iBinder2.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IVoiceInteractionManagerService.Stub.getDefaultImpl().deliverNewSession(param2IBinder, param2IVoiceInteractionSession, param2IVoiceInteractor);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean showSessionFromSession(IBinder param2IBinder, Bundle param2Bundle, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool1 = true;
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IVoiceInteractionManagerService.Stub.getDefaultImpl().showSessionFromSession(param2IBinder, param2Bundle, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hideSessionFromSession(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          parcel1.writeStrongBinder(param2IBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IVoiceInteractionManagerService.Stub.getDefaultImpl().hideSessionFromSession(param2IBinder);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int startVoiceActivity(IBinder param2IBinder, Intent param2Intent, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null)
            return IVoiceInteractionManagerService.Stub.getDefaultImpl().startVoiceActivity(param2IBinder, param2Intent, param2String1, param2String2); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int startAssistantActivity(IBinder param2IBinder, Intent param2Intent, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null)
            return IVoiceInteractionManagerService.Stub.getDefaultImpl().startAssistantActivity(param2IBinder, param2Intent, param2String1, param2String2); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setKeepAwake(IBinder param2IBinder, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool1 && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            IVoiceInteractionManagerService.Stub.getDefaultImpl().setKeepAwake(param2IBinder, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void closeSystemDialogs(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            IVoiceInteractionManagerService.Stub.getDefaultImpl().closeSystemDialogs(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void finish(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            IVoiceInteractionManagerService.Stub.getDefaultImpl().finish(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDisabledShowContext(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            IVoiceInteractionManagerService.Stub.getDefaultImpl().setDisabledShowContext(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getDisabledShowContext() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null)
            return IVoiceInteractionManagerService.Stub.getDefaultImpl().getDisabledShowContext(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getUserDisabledShowContext() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null)
            return IVoiceInteractionManagerService.Stub.getDefaultImpl().getUserDisabledShowContext(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public SoundTrigger.KeyphraseSoundModel getKeyphraseSoundModel(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null)
            return IVoiceInteractionManagerService.Stub.getDefaultImpl().getKeyphraseSoundModel(param2Int, param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            SoundTrigger.KeyphraseSoundModel keyphraseSoundModel = (SoundTrigger.KeyphraseSoundModel)SoundTrigger.KeyphraseSoundModel.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (SoundTrigger.KeyphraseSoundModel)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int updateKeyphraseSoundModel(SoundTrigger.KeyphraseSoundModel param2KeyphraseSoundModel) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          if (param2KeyphraseSoundModel != null) {
            parcel1.writeInt(1);
            param2KeyphraseSoundModel.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null)
            return IVoiceInteractionManagerService.Stub.getDefaultImpl().updateKeyphraseSoundModel(param2KeyphraseSoundModel); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int deleteKeyphraseSoundModel(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            param2Int = IVoiceInteractionManagerService.Stub.getDefaultImpl().deleteKeyphraseSoundModel(param2Int, param2String);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public SoundTrigger.ModuleProperties getDspModuleProperties() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          SoundTrigger.ModuleProperties moduleProperties;
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            moduleProperties = IVoiceInteractionManagerService.Stub.getDefaultImpl().getDspModuleProperties();
            return moduleProperties;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            moduleProperties = (SoundTrigger.ModuleProperties)SoundTrigger.ModuleProperties.CREATOR.createFromParcel(parcel2);
          } else {
            moduleProperties = null;
          } 
          return moduleProperties;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isEnrolledForKeyphrase(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(17, parcel1, parcel2, 0);
          if (!bool2 && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IVoiceInteractionManagerService.Stub.getDefaultImpl().isEnrolledForKeyphrase(param2Int, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public KeyphraseMetadata getEnrolledKeyphraseMetadata(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null)
            return IVoiceInteractionManagerService.Stub.getDefaultImpl().getEnrolledKeyphraseMetadata(param2String1, param2String2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            KeyphraseMetadata keyphraseMetadata = (KeyphraseMetadata)KeyphraseMetadata.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (KeyphraseMetadata)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int startRecognition(int param2Int, String param2String, IRecognitionStatusCallback param2IRecognitionStatusCallback, SoundTrigger.RecognitionConfig param2RecognitionConfig) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          if (param2IRecognitionStatusCallback != null) {
            iBinder = param2IRecognitionStatusCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2RecognitionConfig != null) {
            parcel1.writeInt(1);
            param2RecognitionConfig.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            param2Int = IVoiceInteractionManagerService.Stub.getDefaultImpl().startRecognition(param2Int, param2String, param2IRecognitionStatusCallback, param2RecognitionConfig);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int stopRecognition(int param2Int, IRecognitionStatusCallback param2IRecognitionStatusCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          parcel1.writeInt(param2Int);
          if (param2IRecognitionStatusCallback != null) {
            iBinder = param2IRecognitionStatusCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            param2Int = IVoiceInteractionManagerService.Stub.getDefaultImpl().stopRecognition(param2Int, param2IRecognitionStatusCallback);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setParameter(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            param2Int1 = IVoiceInteractionManagerService.Stub.getDefaultImpl().setParameter(param2Int1, param2Int2, param2Int3);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getParameter(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            param2Int1 = IVoiceInteractionManagerService.Stub.getDefaultImpl().getParameter(param2Int1, param2Int2);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public SoundTrigger.ModelParamRange queryParameter(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          SoundTrigger.ModelParamRange modelParamRange;
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            modelParamRange = IVoiceInteractionManagerService.Stub.getDefaultImpl().queryParameter(param2Int1, param2Int2);
            return modelParamRange;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            modelParamRange = (SoundTrigger.ModelParamRange)SoundTrigger.ModelParamRange.CREATOR.createFromParcel(parcel2);
          } else {
            modelParamRange = null;
          } 
          return modelParamRange;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ComponentName getActiveServiceComponentName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ComponentName componentName;
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            componentName = IVoiceInteractionManagerService.Stub.getDefaultImpl().getActiveServiceComponentName();
            return componentName;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            componentName = (ComponentName)ComponentName.CREATOR.createFromParcel(parcel2);
          } else {
            componentName = null;
          } 
          return componentName;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean showSessionForActiveService(Bundle param2Bundle, int param2Int, IVoiceInteractionSessionShowCallback param2IVoiceInteractionSessionShowCallback, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          boolean bool1 = true;
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          if (param2IVoiceInteractionSessionShowCallback != null) {
            iBinder = param2IVoiceInteractionSessionShowCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool2 = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool2 && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IVoiceInteractionManagerService.Stub.getDefaultImpl().showSessionForActiveService(param2Bundle, param2Int, param2IVoiceInteractionSessionShowCallback, param2IBinder);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void hideCurrentSession() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            IVoiceInteractionManagerService.Stub.getDefaultImpl().hideCurrentSession();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void launchVoiceAssistFromKeyguard() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            IVoiceInteractionManagerService.Stub.getDefaultImpl().launchVoiceAssistFromKeyguard();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isSessionRunning() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(28, parcel1, parcel2, 0);
          if (!bool2 && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IVoiceInteractionManagerService.Stub.getDefaultImpl().isSessionRunning();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean activeServiceSupportsAssist() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(29, parcel1, parcel2, 0);
          if (!bool2 && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IVoiceInteractionManagerService.Stub.getDefaultImpl().activeServiceSupportsAssist();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean activeServiceSupportsLaunchFromKeyguard() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(30, parcel1, parcel2, 0);
          if (!bool2 && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IVoiceInteractionManagerService.Stub.getDefaultImpl().activeServiceSupportsLaunchFromKeyguard();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onLockscreenShown() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            IVoiceInteractionManagerService.Stub.getDefaultImpl().onLockscreenShown();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerVoiceInteractionSessionListener(IVoiceInteractionSessionListener param2IVoiceInteractionSessionListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          if (param2IVoiceInteractionSessionListener != null) {
            iBinder = param2IVoiceInteractionSessionListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            IVoiceInteractionManagerService.Stub.getDefaultImpl().registerVoiceInteractionSessionListener(param2IVoiceInteractionSessionListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void getActiveServiceSupportedActions(List<String> param2List, IVoiceActionCheckCallback param2IVoiceActionCheckCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          parcel1.writeStringList(param2List);
          if (param2IVoiceActionCheckCallback != null) {
            iBinder = param2IVoiceActionCheckCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            IVoiceInteractionManagerService.Stub.getDefaultImpl().getActiveServiceSupportedActions(param2List, param2IVoiceActionCheckCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUiHints(Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            IVoiceInteractionManagerService.Stub.getDefaultImpl().setUiHints(param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestDirectActions(IBinder param2IBinder1, int param2Int, IBinder param2IBinder2, RemoteCallback param2RemoteCallback1, RemoteCallback param2RemoteCallback2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          parcel1.writeStrongBinder(param2IBinder1);
          parcel1.writeInt(param2Int);
          parcel1.writeStrongBinder(param2IBinder2);
          if (param2RemoteCallback1 != null) {
            parcel1.writeInt(1);
            param2RemoteCallback1.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2RemoteCallback2 != null) {
            parcel1.writeInt(1);
            param2RemoteCallback2.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            IVoiceInteractionManagerService.Stub.getDefaultImpl().requestDirectActions(param2IBinder1, param2Int, param2IBinder2, param2RemoteCallback1, param2RemoteCallback2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void performDirectAction(IBinder param2IBinder1, String param2String, Bundle param2Bundle, int param2Int, IBinder param2IBinder2, RemoteCallback param2RemoteCallback1, RemoteCallback param2RemoteCallback2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          try {
            parcel1.writeStrongBinder(param2IBinder1);
            try {
              parcel1.writeString(param2String);
              if (param2Bundle != null) {
                parcel1.writeInt(1);
                param2Bundle.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              parcel1.writeInt(param2Int);
              parcel1.writeStrongBinder(param2IBinder2);
              if (param2RemoteCallback1 != null) {
                parcel1.writeInt(1);
                param2RemoteCallback1.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              if (param2RemoteCallback2 != null) {
                parcel1.writeInt(1);
                param2RemoteCallback2.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
              if (!bool && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
                IVoiceInteractionManagerService.Stub.getDefaultImpl().performDirectAction(param2IBinder1, param2String, param2Bundle, param2Int, param2IBinder2, param2RemoteCallback1, param2RemoteCallback2);
                parcel2.recycle();
                parcel1.recycle();
                return;
              } 
              parcel2.readException();
              parcel2.recycle();
              parcel1.recycle();
              return;
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IBinder1;
      }
      
      public void setDisabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractionManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool1 && IVoiceInteractionManagerService.Stub.getDefaultImpl() != null) {
            IVoiceInteractionManagerService.Stub.getDefaultImpl().setDisabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IVoiceInteractionManagerService param1IVoiceInteractionManagerService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IVoiceInteractionManagerService != null) {
          Proxy.sDefaultImpl = param1IVoiceInteractionManagerService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IVoiceInteractionManagerService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
