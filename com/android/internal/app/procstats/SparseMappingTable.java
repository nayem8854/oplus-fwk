package com.android.internal.app.procstats;

import android.os.Build;
import android.os.Parcel;
import android.util.EventLog;
import android.util.Slog;
import com.android.internal.util.GrowingArrayUtils;
import java.util.ArrayList;
import libcore.util.EmptyArray;

public class SparseMappingTable {
  private static final int ARRAY_MASK = 255;
  
  private static final int ARRAY_SHIFT = 8;
  
  public static final int ARRAY_SIZE = 4096;
  
  private static final int ID_MASK = 255;
  
  private static final int ID_SHIFT = 0;
  
  private static final int INDEX_MASK = 65535;
  
  private static final int INDEX_SHIFT = 16;
  
  public static final int INVALID_KEY = -1;
  
  private static final String TAG = "SparseMappingTable";
  
  private final ArrayList<long[]> mLongs;
  
  private int mNextIndex;
  
  private int mSequence;
  
  class Table {
    private SparseMappingTable mParent;
    
    private int mSequence = 1;
    
    private int mSize;
    
    private int[] mTable;
    
    public Table(SparseMappingTable this$0) {
      this.mParent = this$0;
      this.mSequence = this$0.mSequence;
    }
    
    public void copyFrom(Table param1Table, int param1Int) {
      this.mTable = null;
      this.mSize = 0;
      int i = param1Table.getKeyCount();
      for (byte b = 0; b < i; b++) {
        int j = param1Table.getKeyAt(b);
        long[] arrayOfLong1 = param1Table.mParent.mLongs.get(SparseMappingTable.getArrayFromKey(j));
        byte b1 = SparseMappingTable.getIdFromKey(j);
        int k = getOrAddKey(b1, param1Int);
        long[] arrayOfLong2 = this.mParent.mLongs.get(SparseMappingTable.getArrayFromKey(k));
        j = SparseMappingTable.getIndexFromKey(j);
        k = SparseMappingTable.getIndexFromKey(k);
        System.arraycopy(arrayOfLong1, j, arrayOfLong2, k, param1Int);
      } 
    }
    
    public int getOrAddKey(byte param1Byte, int param1Int) {
      assertConsistency();
      int i = binarySearch(param1Byte);
      if (i >= 0)
        return this.mTable[i]; 
      ArrayList<long[]> arrayList = this.mParent.mLongs;
      int j = arrayList.size() - 1;
      long[] arrayOfLong = arrayList.get(j);
      int k = j;
      if (this.mParent.mNextIndex + param1Int > arrayOfLong.length) {
        arrayOfLong = new long[4096];
        arrayList.add(arrayOfLong);
        k = j + 1;
        SparseMappingTable.access$202(this.mParent, 0);
      } 
      SparseMappingTable sparseMappingTable = this.mParent;
      k = k << 8 | sparseMappingTable.mNextIndex << 16 | param1Byte << 0;
      SparseMappingTable.access$212(this.mParent, param1Int);
      int[] arrayOfInt = this.mTable;
      if (arrayOfInt == null)
        arrayOfInt = EmptyArray.INT; 
      this.mTable = GrowingArrayUtils.insert(arrayOfInt, this.mSize, i ^ 0xFFFFFFFF, k);
      this.mSize++;
      return k;
    }
    
    public int getKey(byte param1Byte) {
      assertConsistency();
      int i = binarySearch(param1Byte);
      if (i >= 0)
        return this.mTable[i]; 
      return -1;
    }
    
    public long getValue(int param1Int) {
      return getValue(param1Int, 0);
    }
    
    public long getValue(int param1Int1, int param1Int2) {
      assertConsistency();
      try {
        long[] arrayOfLong = this.mParent.mLongs.get(SparseMappingTable.getArrayFromKey(param1Int1));
        return arrayOfLong[SparseMappingTable.getIndexFromKey(param1Int1) + param1Int2];
      } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("key=0x");
        stringBuilder.append(Integer.toHexString(param1Int1));
        stringBuilder.append(" index=");
        stringBuilder.append(param1Int2);
        stringBuilder.append(" -- ");
        stringBuilder.append(dumpInternalState());
        String str = stringBuilder.toString();
        SparseMappingTable.logOrThrow(str, indexOutOfBoundsException);
        return 0L;
      } 
    }
    
    public long getValueForId(byte param1Byte) {
      return getValueForId(param1Byte, 0);
    }
    
    public long getValueForId(byte param1Byte, int param1Int) {
      assertConsistency();
      int i = binarySearch(param1Byte);
      if (i >= 0) {
        int j = this.mTable[i];
        try {
          long[] arrayOfLong = this.mParent.mLongs.get(SparseMappingTable.getArrayFromKey(j));
          return arrayOfLong[SparseMappingTable.getIndexFromKey(j) + param1Int];
        } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("id=0x");
          stringBuilder.append(Integer.toHexString(param1Byte));
          stringBuilder.append(" idx=");
          stringBuilder.append(i);
          stringBuilder.append(" key=0x");
          stringBuilder.append(Integer.toHexString(j));
          stringBuilder.append(" index=");
          stringBuilder.append(param1Int);
          stringBuilder.append(" -- ");
          stringBuilder.append(dumpInternalState());
          String str = stringBuilder.toString();
          SparseMappingTable.logOrThrow(str, indexOutOfBoundsException);
          return 0L;
        } 
      } 
      return 0L;
    }
    
    public long[] getArrayForKey(int param1Int) {
      assertConsistency();
      int i = SparseMappingTable.getArrayFromKey(param1Int);
      int j = this.mParent.mLongs.size();
      if (i >= j) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Invalid key at index ");
        stringBuilder.append(i);
        stringBuilder.append(" -- ");
        stringBuilder.append(dumpInternalState());
        Slog.w("SparseMappingTable", stringBuilder.toString());
        return this.mParent.mLongs.get(j - 1);
      } 
      return this.mParent.mLongs.get(SparseMappingTable.getArrayFromKey(param1Int));
    }
    
    public void setValue(int param1Int, long param1Long) {
      setValue(param1Int, 0, param1Long);
    }
    
    public void setValue(int param1Int1, int param1Int2, long param1Long) {
      assertConsistency();
      if (param1Long < 0L) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("can't store negative values key=0x");
        stringBuilder.append(Integer.toHexString(param1Int1));
        stringBuilder.append(" index=");
        stringBuilder.append(param1Int2);
        stringBuilder.append(" value=");
        stringBuilder.append(param1Long);
        stringBuilder.append(" -- ");
        stringBuilder.append(dumpInternalState());
        String str = stringBuilder.toString();
        SparseMappingTable.logOrThrow(str);
        return;
      } 
      try {
        long[] arrayOfLong = this.mParent.mLongs.get(SparseMappingTable.getArrayFromKey(param1Int1));
        arrayOfLong[SparseMappingTable.getIndexFromKey(param1Int1) + param1Int2] = param1Long;
        return;
      } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("key=0x");
        stringBuilder.append(Integer.toHexString(param1Int1));
        stringBuilder.append(" index=");
        stringBuilder.append(param1Int2);
        stringBuilder.append(" value=");
        stringBuilder.append(param1Long);
        stringBuilder.append(" -- ");
        stringBuilder.append(dumpInternalState());
        String str = stringBuilder.toString();
        SparseMappingTable.logOrThrow(str, indexOutOfBoundsException);
        return;
      } 
    }
    
    public void resetTable() {
      this.mTable = null;
      this.mSize = 0;
      this.mSequence = this.mParent.mSequence;
    }
    
    public void writeToParcel(Parcel param1Parcel) {
      param1Parcel.writeInt(this.mSequence);
      param1Parcel.writeInt(this.mSize);
      for (byte b = 0; b < this.mSize; b++)
        param1Parcel.writeInt(this.mTable[b]); 
    }
    
    public boolean readFromParcel(Parcel param1Parcel) {
      this.mSequence = param1Parcel.readInt();
      int i = param1Parcel.readInt();
      if (i != 0) {
        this.mTable = new int[i];
        for (i = 0; i < this.mSize; i++)
          this.mTable[i] = param1Parcel.readInt(); 
      } else {
        this.mTable = null;
      } 
      if (validateKeys(true))
        return true; 
      this.mSize = 0;
      this.mTable = null;
      return false;
    }
    
    public int getKeyCount() {
      return this.mSize;
    }
    
    public int getKeyAt(int param1Int) {
      return this.mTable[param1Int];
    }
    
    private void assertConsistency() {}
    
    private int binarySearch(byte param1Byte) {
      int i = 0;
      int j = this.mSize - 1;
      while (i <= j) {
        int k = i + j >>> 1;
        byte b = (byte)(this.mTable[k] >> 0 & 0xFF);
        if (b < param1Byte) {
          i = k + 1;
          continue;
        } 
        if (b > param1Byte) {
          j = k - 1;
          continue;
        } 
        return k;
      } 
      return i ^ 0xFFFFFFFF;
    }
    
    private boolean validateKeys(boolean param1Boolean) {
      ArrayList arrayList = this.mParent.mLongs;
      int i = arrayList.size();
      int j = this.mSize;
      for (byte b = 0; b < j; b++) {
        int k = this.mTable[b];
        int m = SparseMappingTable.getArrayFromKey(k);
        k = SparseMappingTable.getIndexFromKey(k);
        if (m >= i || k >= ((long[])arrayList.get(m)).length) {
          if (param1Boolean) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Invalid stats at index ");
            stringBuilder.append(b);
            stringBuilder.append(" -- ");
            stringBuilder.append(dumpInternalState());
            Slog.w("SparseMappingTable", stringBuilder.toString());
          } 
          return false;
        } 
      } 
      return true;
    }
    
    public String dumpInternalState() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("SparseMappingTable.Table{mSequence=");
      stringBuilder.append(this.mSequence);
      stringBuilder.append(" mParent.mSequence=");
      stringBuilder.append(this.mParent.mSequence);
      stringBuilder.append(" mParent.mLongs.size()=");
      stringBuilder.append(this.mParent.mLongs.size());
      stringBuilder.append(" mSize=");
      stringBuilder.append(this.mSize);
      stringBuilder.append(" mTable=");
      int[] arrayOfInt = this.mTable;
      if (arrayOfInt == null) {
        stringBuilder.append("null");
      } else {
        int i = arrayOfInt.length;
        stringBuilder.append('[');
        for (byte b = 0; b < i; b++) {
          int j = this.mTable[b];
          stringBuilder.append("0x");
          stringBuilder.append(Integer.toHexString(j >> 0 & 0xFF));
          stringBuilder.append("/0x");
          stringBuilder.append(Integer.toHexString(j >> 8 & 0xFF));
          stringBuilder.append("/0x");
          stringBuilder.append(Integer.toHexString(j >> 16 & 0xFFFF));
          if (b != i - 1)
            stringBuilder.append(", "); 
        } 
        stringBuilder.append(']');
      } 
      stringBuilder.append(" clazz=");
      stringBuilder.append(getClass().getName());
      stringBuilder.append('}');
      return stringBuilder.toString();
    }
  }
  
  public SparseMappingTable() {
    ArrayList<long[]> arrayList = new ArrayList();
    arrayList.add(new long[4096]);
  }
  
  public void reset() {
    this.mLongs.clear();
    this.mLongs.add(new long[4096]);
    this.mNextIndex = 0;
    this.mSequence++;
  }
  
  public void writeToParcel(Parcel paramParcel) {
    paramParcel.writeInt(this.mSequence);
    paramParcel.writeInt(this.mNextIndex);
    int i = this.mLongs.size();
    paramParcel.writeInt(i);
    for (byte b = 0; b < i - 1; b++) {
      long[] arrayOfLong1 = this.mLongs.get(b);
      paramParcel.writeInt(arrayOfLong1.length);
      writeCompactedLongArray(paramParcel, arrayOfLong1, arrayOfLong1.length);
    } 
    long[] arrayOfLong = this.mLongs.get(i - 1);
    paramParcel.writeInt(this.mNextIndex);
    writeCompactedLongArray(paramParcel, arrayOfLong, this.mNextIndex);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mSequence = paramParcel.readInt();
    this.mNextIndex = paramParcel.readInt();
    this.mLongs.clear();
    int i = paramParcel.readInt();
    for (byte b = 0; b < i; b++) {
      int j = paramParcel.readInt();
      long[] arrayOfLong = new long[j];
      readCompactedLongArray(paramParcel, arrayOfLong, j);
      this.mLongs.add(arrayOfLong);
    } 
    if (i <= 0 || ((long[])this.mLongs.get(i - 1)).length == this.mNextIndex)
      return; 
    EventLog.writeEvent(1397638484, new Object[] { "73252178", Integer.valueOf(-1), "" });
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Expected array of length ");
    stringBuilder.append(this.mNextIndex);
    stringBuilder.append(" but was ");
    ArrayList<long[]> arrayList = this.mLongs;
    stringBuilder.append(((long[])arrayList.get(i - 1)).length);
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public String dumpInternalState(boolean paramBoolean) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SparseMappingTable{");
    stringBuilder.append("mSequence=");
    stringBuilder.append(this.mSequence);
    stringBuilder.append(" mNextIndex=");
    stringBuilder.append(this.mNextIndex);
    stringBuilder.append(" mLongs.size=");
    int i = this.mLongs.size();
    stringBuilder.append(i);
    stringBuilder.append("\n");
    if (paramBoolean)
      for (byte b = 0; b < i; b++) {
        long[] arrayOfLong = this.mLongs.get(b);
        for (byte b1 = 0; b1 < arrayOfLong.length && (
          b != i - 1 || b1 != this.mNextIndex); b1++) {
          stringBuilder.append(String.format(" %4d %d 0x%016x %-19d\n", new Object[] { Integer.valueOf(b), Integer.valueOf(b1), Long.valueOf(arrayOfLong[b1]), Long.valueOf(arrayOfLong[b1]) }));
        } 
      }  
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  private static void writeCompactedLongArray(Parcel paramParcel, long[] paramArrayOflong, int paramInt) {
    for (byte b = 0; b < paramInt; b++) {
      long l1 = paramArrayOflong[b];
      long l2 = l1;
      if (l1 < 0L) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Time val negative: ");
        stringBuilder.append(l1);
        Slog.w("SparseMappingTable", stringBuilder.toString());
        l2 = 0L;
      } 
      if (l2 <= 2147483647L) {
        paramParcel.writeInt((int)l2);
      } else {
        int i = (int)(0x7FFFFFFFL & l2 >> 32L);
        int j = (int)(0xFFFFFFFFL & l2);
        paramParcel.writeInt(i ^ 0xFFFFFFFF);
        paramParcel.writeInt(j);
      } 
    } 
  }
  
  private static void readCompactedLongArray(Parcel paramParcel, long[] paramArrayOflong, int paramInt) {
    StringBuilder stringBuilder;
    int j, i = paramArrayOflong.length;
    if (paramInt > i) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("bad array lengths: got ");
      stringBuilder.append(paramInt);
      stringBuilder.append(" array is ");
      stringBuilder.append(i);
      logOrThrow(stringBuilder.toString());
      return;
    } 
    byte b = 0;
    while (true) {
      j = b;
      if (b < paramInt) {
        j = stringBuilder.readInt();
        if (j >= 0) {
          paramArrayOflong[b] = j;
        } else {
          int k = stringBuilder.readInt();
          paramArrayOflong[b] = (j ^ 0xFFFFFFFF) << 32L | k;
        } 
        b++;
        continue;
      } 
      break;
    } 
    while (j < i) {
      paramArrayOflong[j] = 0L;
      j++;
    } 
  }
  
  public static byte getIdFromKey(int paramInt) {
    return (byte)(paramInt >> 0 & 0xFF);
  }
  
  public static int getArrayFromKey(int paramInt) {
    return paramInt >> 8 & 0xFF;
  }
  
  public static int getIndexFromKey(int paramInt) {
    return paramInt >> 16 & 0xFFFF;
  }
  
  private static void logOrThrow(String paramString) {
    logOrThrow(paramString, new RuntimeException("Stack trace"));
  }
  
  private static void logOrThrow(String paramString, Throwable paramThrowable) {
    Slog.e("SparseMappingTable", paramString, paramThrowable);
    if (!Build.IS_ENG)
      return; 
    throw new RuntimeException(paramString, paramThrowable);
  }
}
