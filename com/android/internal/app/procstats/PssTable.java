package com.android.internal.app.procstats;

import android.util.proto.ProtoOutputStream;
import android.util.proto.ProtoUtils;

public class PssTable extends SparseMappingTable.Table {
  public PssTable(SparseMappingTable paramSparseMappingTable) {
    super(paramSparseMappingTable);
  }
  
  public void mergeStats(PssTable paramPssTable) {
    int i = paramPssTable.getKeyCount();
    for (byte b = 0; b < i; b++) {
      int j = paramPssTable.getKeyAt(b);
      byte b1 = SparseMappingTable.getIdFromKey(j);
      int k = getOrAddKey((byte)b1, 10);
      long[] arrayOfLong1 = getArrayForKey(k);
      k = SparseMappingTable.getIndexFromKey(k);
      long[] arrayOfLong2 = paramPssTable.getArrayForKey(j);
      j = SparseMappingTable.getIndexFromKey(j);
      mergeStats(arrayOfLong1, k, arrayOfLong2, j);
    } 
  }
  
  public void mergeStats(int paramInt1, int paramInt2, long paramLong1, long paramLong2, long paramLong3, long paramLong4, long paramLong5, long paramLong6, long paramLong7, long paramLong8, long paramLong9) {
    paramInt1 = getOrAddKey((byte)paramInt1, 10);
    long[] arrayOfLong = getArrayForKey(paramInt1);
    paramInt1 = SparseMappingTable.getIndexFromKey(paramInt1);
    mergeStats(arrayOfLong, paramInt1, paramInt2, paramLong1, paramLong2, paramLong3, paramLong4, paramLong5, paramLong6, paramLong7, paramLong8, paramLong9);
  }
  
  public static void mergeStats(long[] paramArrayOflong1, int paramInt1, long[] paramArrayOflong2, int paramInt2) {
    mergeStats(paramArrayOflong1, paramInt1, (int)paramArrayOflong2[paramInt2 + 0], paramArrayOflong2[paramInt2 + 1], paramArrayOflong2[paramInt2 + 2], paramArrayOflong2[paramInt2 + 3], paramArrayOflong2[paramInt2 + 4], paramArrayOflong2[paramInt2 + 5], paramArrayOflong2[paramInt2 + 6], paramArrayOflong2[paramInt2 + 7], paramArrayOflong2[paramInt2 + 8], paramArrayOflong2[paramInt2 + 9]);
  }
  
  public static void mergeStats(long[] paramArrayOflong, int paramInt1, int paramInt2, long paramLong1, long paramLong2, long paramLong3, long paramLong4, long paramLong5, long paramLong6, long paramLong7, long paramLong8, long paramLong9) {
    long l = paramArrayOflong[paramInt1 + 0];
    if (l == 0L) {
      paramArrayOflong[paramInt1 + 0] = paramInt2;
      paramArrayOflong[paramInt1 + 1] = paramLong1;
      paramArrayOflong[paramInt1 + 2] = paramLong2;
      paramArrayOflong[paramInt1 + 3] = paramLong3;
      paramArrayOflong[paramInt1 + 4] = paramLong4;
      paramArrayOflong[paramInt1 + 5] = paramLong5;
      paramArrayOflong[paramInt1 + 6] = paramLong6;
      paramArrayOflong[paramInt1 + 7] = paramLong7;
      paramArrayOflong[paramInt1 + 8] = paramLong8;
      paramArrayOflong[paramInt1 + 9] = paramLong9;
    } else {
      paramArrayOflong[paramInt1 + 0] = paramInt2 + l;
      if (paramArrayOflong[paramInt1 + 1] > paramLong1)
        paramArrayOflong[paramInt1 + 1] = paramLong1; 
      paramArrayOflong[paramInt1 + 2] = (long)((paramArrayOflong[paramInt1 + 2] * l + paramLong2 * paramInt2) / (paramInt2 + l));
      if (paramArrayOflong[paramInt1 + 3] < paramLong3)
        paramArrayOflong[paramInt1 + 3] = paramLong3; 
      if (paramArrayOflong[paramInt1 + 4] > paramLong4)
        paramArrayOflong[paramInt1 + 4] = paramLong4; 
      paramArrayOflong[paramInt1 + 5] = (long)((paramArrayOflong[paramInt1 + 5] * l + paramLong5 * paramInt2) / (paramInt2 + l));
      if (paramArrayOflong[paramInt1 + 6] < paramLong6)
        paramArrayOflong[paramInt1 + 6] = paramLong6; 
      paramLong1 = paramArrayOflong[paramInt1 + 7];
      paramArrayOflong[paramInt1 + 8] = (long)((paramArrayOflong[paramInt1 + 8] * l + paramLong8 * paramInt2) / (paramInt2 + l));
      if (paramArrayOflong[paramInt1 + 9] < paramLong9)
        paramArrayOflong[paramInt1 + 9] = paramLong9; 
    } 
  }
  
  public void writeStatsToProtoForKey(ProtoOutputStream paramProtoOutputStream, int paramInt) {
    long[] arrayOfLong = getArrayForKey(paramInt);
    paramInt = SparseMappingTable.getIndexFromKey(paramInt);
    writeStatsToProto(paramProtoOutputStream, arrayOfLong, paramInt);
  }
  
  public static void writeStatsToProto(ProtoOutputStream paramProtoOutputStream, long[] paramArrayOflong, int paramInt) {
    paramProtoOutputStream.write(1120986464261L, paramArrayOflong[paramInt + 0]);
    ProtoUtils.toAggStatsProto(paramProtoOutputStream, 1146756268038L, paramArrayOflong[paramInt + 1], paramArrayOflong[paramInt + 2], paramArrayOflong[paramInt + 3]);
    ProtoUtils.toAggStatsProto(paramProtoOutputStream, 1146756268039L, paramArrayOflong[paramInt + 4], paramArrayOflong[paramInt + 5], paramArrayOflong[paramInt + 6]);
    ProtoUtils.toAggStatsProto(paramProtoOutputStream, 1146756268040L, paramArrayOflong[paramInt + 7], paramArrayOflong[paramInt + 8], paramArrayOflong[paramInt + 9]);
  }
  
  long[] getRssMeanAndMax(int paramInt) {
    long[] arrayOfLong = getArrayForKey(paramInt);
    paramInt = SparseMappingTable.getIndexFromKey(paramInt);
    return new long[] { arrayOfLong[paramInt + 8], arrayOfLong[paramInt + 9] };
  }
}
