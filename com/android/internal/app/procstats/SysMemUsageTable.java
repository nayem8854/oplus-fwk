package com.android.internal.app.procstats;

import android.util.DebugUtils;
import java.io.PrintWriter;

public class SysMemUsageTable extends SparseMappingTable.Table {
  public SysMemUsageTable(SparseMappingTable paramSparseMappingTable) {
    super(paramSparseMappingTable);
  }
  
  public void mergeStats(SysMemUsageTable paramSysMemUsageTable) {
    int i = paramSysMemUsageTable.getKeyCount();
    for (byte b = 0; b < i; b++) {
      int j = paramSysMemUsageTable.getKeyAt(b);
      byte b1 = SparseMappingTable.getIdFromKey(j);
      long[] arrayOfLong = paramSysMemUsageTable.getArrayForKey(j);
      j = SparseMappingTable.getIndexFromKey(j);
      mergeStats(b1, arrayOfLong, j);
    } 
  }
  
  public void mergeStats(int paramInt1, long[] paramArrayOflong, int paramInt2) {
    paramInt1 = getOrAddKey((byte)paramInt1, 16);
    long[] arrayOfLong = getArrayForKey(paramInt1);
    paramInt1 = SparseMappingTable.getIndexFromKey(paramInt1);
    mergeSysMemUsage(arrayOfLong, paramInt1, paramArrayOflong, paramInt2);
  }
  
  public long[] getTotalMemUsage() {
    long[] arrayOfLong = new long[16];
    int i = getKeyCount();
    for (byte b = 0; b < i; b++) {
      int j = getKeyAt(b);
      long[] arrayOfLong1 = getArrayForKey(j);
      j = SparseMappingTable.getIndexFromKey(j);
      mergeSysMemUsage(arrayOfLong, 0, arrayOfLong1, j);
    } 
    return arrayOfLong;
  }
  
  public static void mergeSysMemUsage(long[] paramArrayOflong1, int paramInt1, long[] paramArrayOflong2, int paramInt2) {
    long l1 = paramArrayOflong1[paramInt1 + 0];
    long l2 = paramArrayOflong2[paramInt2 + 0];
    if (l1 == 0L) {
      paramArrayOflong1[paramInt1 + 0] = l2;
      for (byte b = 1; b < 16; b++)
        paramArrayOflong1[paramInt1 + b] = paramArrayOflong2[paramInt2 + b]; 
    } else if (l2 > 0L) {
      paramArrayOflong1[paramInt1 + 0] = l1 + l2;
      for (byte b = 1; b < 16; b += 3) {
        if (paramArrayOflong1[paramInt1 + b] > paramArrayOflong2[paramInt2 + b])
          paramArrayOflong1[paramInt1 + b] = paramArrayOflong2[paramInt2 + b]; 
        paramArrayOflong1[paramInt1 + b + 1] = (long)((paramArrayOflong1[paramInt1 + b + 1] * l1 + paramArrayOflong2[paramInt2 + b + 1] * l2) / (l1 + l2));
        if (paramArrayOflong1[paramInt1 + b + 2] < paramArrayOflong2[paramInt2 + b + 2])
          paramArrayOflong1[paramInt1 + b + 2] = paramArrayOflong2[paramInt2 + b + 2]; 
      } 
    } 
  }
  
  public void dump(PrintWriter paramPrintWriter, String paramString, int[] paramArrayOfint1, int[] paramArrayOfint2) {
    int i = -1;
    for (byte b = 0; b < paramArrayOfint1.length; b++) {
      int j = -1;
      for (byte b1 = 0; b1 < paramArrayOfint2.length; b1++) {
        int k = paramArrayOfint1[b];
        int m = paramArrayOfint2[b1];
        int n = (k + m) * 14;
        long l = getValueForId((byte)n, 0);
        if (l > 0L) {
          paramPrintWriter.print(paramString);
          if (paramArrayOfint1.length > 1) {
            if (i != k) {
              i = k;
            } else {
              i = -1;
            } 
            DumpUtils.printScreenLabel(paramPrintWriter, i);
            i = k;
          } 
          if (paramArrayOfint2.length > 1) {
            if (j != m) {
              j = m;
            } else {
              j = -1;
            } 
            DumpUtils.printMemLabel(paramPrintWriter, j, false);
            j = m;
          } 
          paramPrintWriter.print(": ");
          paramPrintWriter.print(l);
          paramPrintWriter.println(" samples:");
          dumpCategory(paramPrintWriter, paramString, "  Cached", n, 1);
          dumpCategory(paramPrintWriter, paramString, "  Free", n, 4);
          dumpCategory(paramPrintWriter, paramString, "  ZRam", n, 7);
          dumpCategory(paramPrintWriter, paramString, "  Kernel", n, 10);
          dumpCategory(paramPrintWriter, paramString, "  Native", n, 13);
        } 
      } 
    } 
  }
  
  private void dumpCategory(PrintWriter paramPrintWriter, String paramString1, String paramString2, int paramInt1, int paramInt2) {
    paramPrintWriter.print(paramString1);
    paramPrintWriter.print(paramString2);
    paramPrintWriter.print(": ");
    DebugUtils.printSizeValue(paramPrintWriter, getValueForId((byte)paramInt1, paramInt2) * 1024L);
    paramPrintWriter.print(" min, ");
    DebugUtils.printSizeValue(paramPrintWriter, getValueForId((byte)paramInt1, paramInt2 + 1) * 1024L);
    paramPrintWriter.print(" avg, ");
    DebugUtils.printSizeValue(paramPrintWriter, getValueForId((byte)paramInt1, paramInt2 + 2) * 1024L);
    paramPrintWriter.println(" max");
  }
}
