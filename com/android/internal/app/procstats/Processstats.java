package com.android.internal.app.procstats;

import android.content.ComponentName;
import android.os.Debug;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.ArrayMap;
import android.util.ArraySet;
import android.util.DebugUtils;
import android.util.LongSparseArray;
import android.util.Pair;
import android.util.Slog;
import android.util.SparseArray;
import android.util.TimeUtils;
import android.util.proto.ProtoOutputStream;
import com.android.internal.app.ProcessMap;
import dalvik.system.VMRuntime;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ProcessStats implements Parcelable {
  public static long COMMIT_PERIOD = 10800000L;
  
  public static long COMMIT_UPTIME_PERIOD = 3600000L;
  
  static {
    ALL_MEM_ADJ = new int[] { 0, 1, 2, 3 };
    ALL_SCREEN_ADJ = new int[] { 0, 4 };
    NON_CACHED_PROC_STATES = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
    BACKGROUND_PROC_STATES = new int[] { 2, 3, 4, 8, 5, 6, 7 };
    ALL_PROC_STATES = new int[] { 
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 
        10, 11, 12, 13 };
    OPTIONS = new int[] { 1, 2, 4, 8, 14, 15 };
    OPTIONS_STR = new String[] { "proc", "pkg-proc", "pkg-svc", "pkg-asc", "pkg-all", "all" };
    sPageTypeRegex = Pattern.compile("^Node\\s+(\\d+),.* zone\\s+(\\w+),.* type\\s+(\\w+)\\s+([\\s\\d]+?)\\s*$");
    CREATOR = new Parcelable.Creator<ProcessStats>() {
        public ProcessStats createFromParcel(Parcel param1Parcel) {
          return new ProcessStats(param1Parcel);
        }
        
        public ProcessStats[] newArray(int param1Int) {
          return new ProcessStats[param1Int];
        }
      };
    BAD_TABLE = new int[0];
    ASSOCIATION_COMPARATOR = (Comparator<AssociationDumpContainer>)_$$Lambda$ProcessStats$6CxEiT4FvK_P75G9LzEfE1zL88Q.INSTANCE;
  }
  
  public final ProcessMap<LongSparseArray<PackageState>> mPackages = new ProcessMap<>();
  
  public final ProcessMap<ProcessState> mProcesses = new ProcessMap<>();
  
  public final ArrayList<AssociationState.SourceState> mTrackingAssociations = new ArrayList<>();
  
  public final long[] mMemFactorDurations = new long[8];
  
  public int mMemFactor = -1;
  
  public int mNumAggregated = 1;
  
  public static final int ADD_PSS_EXTERNAL = 3;
  
  public static final int ADD_PSS_EXTERNAL_SLOW = 4;
  
  public static final int ADD_PSS_INTERNAL_ALL_MEM = 1;
  
  public static final int ADD_PSS_INTERNAL_ALL_POLL = 2;
  
  public static final int ADD_PSS_INTERNAL_SINGLE = 0;
  
  public static final int ADJ_COUNT = 8;
  
  public static final int ADJ_MEM_FACTOR_COUNT = 4;
  
  public static final int ADJ_MEM_FACTOR_CRITICAL = 3;
  
  public static final int ADJ_MEM_FACTOR_LOW = 2;
  
  public static final int ADJ_MEM_FACTOR_MODERATE = 1;
  
  public static final int ADJ_MEM_FACTOR_NORMAL = 0;
  
  public static final int ADJ_NOTHING = -1;
  
  public static final int ADJ_SCREEN_MOD = 4;
  
  public static final int ADJ_SCREEN_OFF = 0;
  
  public static final int ADJ_SCREEN_ON = 4;
  
  public static final int[] ALL_MEM_ADJ;
  
  public static final int[] ALL_PROC_STATES;
  
  public static final int[] ALL_SCREEN_ADJ;
  
  static final Comparator<AssociationDumpContainer> ASSOCIATION_COMPARATOR;
  
  public static final int[] BACKGROUND_PROC_STATES;
  
  static final int[] BAD_TABLE;
  
  public static final Parcelable.Creator<ProcessStats> CREATOR;
  
  static final boolean DEBUG = false;
  
  static final boolean DEBUG_PARCEL = false;
  
  public static final int FLAG_COMPLETE = 1;
  
  public static final int FLAG_SHUTDOWN = 2;
  
  public static final int FLAG_SYSPROPS = 4;
  
  private static final long INVERSE_PROC_STATE_WARNING_MIN_INTERVAL_MS = 10000L;
  
  private static final int MAGIC = 1347638356;
  
  public static final int[] NON_CACHED_PROC_STATES;
  
  public static final int[] OPTIONS;
  
  public static final String[] OPTIONS_STR;
  
  private static final int PARCEL_VERSION = 38;
  
  public static final int PSS_AVERAGE = 2;
  
  public static final int PSS_COUNT = 10;
  
  public static final int PSS_MAXIMUM = 3;
  
  public static final int PSS_MINIMUM = 1;
  
  public static final int PSS_RSS_AVERAGE = 8;
  
  public static final int PSS_RSS_MAXIMUM = 9;
  
  public static final int PSS_RSS_MINIMUM = 7;
  
  public static final int PSS_SAMPLE_COUNT = 0;
  
  public static final int PSS_USS_AVERAGE = 5;
  
  public static final int PSS_USS_MAXIMUM = 6;
  
  public static final int PSS_USS_MINIMUM = 4;
  
  public static final int REPORT_ALL = 15;
  
  public static final int REPORT_PKG_ASC_STATS = 8;
  
  public static final int REPORT_PKG_PROC_STATS = 2;
  
  public static final int REPORT_PKG_STATS = 14;
  
  public static final int REPORT_PKG_SVC_STATS = 4;
  
  public static final int REPORT_PROC_STATS = 1;
  
  public static final String SERVICE_NAME = "procstats";
  
  public static final int STATE_BACKUP = 4;
  
  public static final int STATE_CACHED_ACTIVITY = 11;
  
  public static final int STATE_CACHED_ACTIVITY_CLIENT = 12;
  
  public static final int STATE_CACHED_EMPTY = 13;
  
  public static final int STATE_COUNT = 14;
  
  public static final int STATE_HEAVY_WEIGHT = 8;
  
  public static final int STATE_HOME = 9;
  
  public static final int STATE_IMPORTANT_BACKGROUND = 3;
  
  public static final int STATE_IMPORTANT_FOREGROUND = 2;
  
  public static final int STATE_LAST_ACTIVITY = 10;
  
  public static final int STATE_NOTHING = -1;
  
  public static final int STATE_PERSISTENT = 0;
  
  public static final int STATE_RECEIVER = 7;
  
  public static final int STATE_SERVICE = 5;
  
  public static final int STATE_SERVICE_RESTARTING = 6;
  
  public static final int STATE_TOP = 1;
  
  public static final int SYS_MEM_USAGE_CACHED_AVERAGE = 2;
  
  public static final int SYS_MEM_USAGE_CACHED_MAXIMUM = 3;
  
  public static final int SYS_MEM_USAGE_CACHED_MINIMUM = 1;
  
  public static final int SYS_MEM_USAGE_COUNT = 16;
  
  public static final int SYS_MEM_USAGE_FREE_AVERAGE = 5;
  
  public static final int SYS_MEM_USAGE_FREE_MAXIMUM = 6;
  
  public static final int SYS_MEM_USAGE_FREE_MINIMUM = 4;
  
  public static final int SYS_MEM_USAGE_KERNEL_AVERAGE = 11;
  
  public static final int SYS_MEM_USAGE_KERNEL_MAXIMUM = 12;
  
  public static final int SYS_MEM_USAGE_KERNEL_MINIMUM = 10;
  
  public static final int SYS_MEM_USAGE_NATIVE_AVERAGE = 14;
  
  public static final int SYS_MEM_USAGE_NATIVE_MAXIMUM = 15;
  
  public static final int SYS_MEM_USAGE_NATIVE_MINIMUM = 13;
  
  public static final int SYS_MEM_USAGE_SAMPLE_COUNT = 0;
  
  public static final int SYS_MEM_USAGE_ZRAM_AVERAGE = 8;
  
  public static final int SYS_MEM_USAGE_ZRAM_MAXIMUM = 9;
  
  public static final int SYS_MEM_USAGE_ZRAM_MINIMUM = 7;
  
  public static final String TAG = "ProcessStats";
  
  private static final Pattern sPageTypeRegex;
  
  ArrayMap<String, Integer> mCommonStringToIndex;
  
  public long mExternalPssCount;
  
  public long mExternalPssTime;
  
  public long mExternalSlowPssCount;
  
  public long mExternalSlowPssTime;
  
  public int mFlags;
  
  boolean mHasSwappedOutPss;
  
  ArrayList<String> mIndexToCommonString;
  
  public long mInternalAllMemPssCount;
  
  public long mInternalAllMemPssTime;
  
  public long mInternalAllPollPssCount;
  
  public long mInternalAllPollPssTime;
  
  public long mInternalSinglePssCount;
  
  public long mInternalSinglePssTime;
  
  private long mNextInverseProcStateWarningUptime;
  
  private final ArrayList<String> mPageTypeLabels;
  
  private final ArrayList<Integer> mPageTypeNodes;
  
  private final ArrayList<int[]> mPageTypeSizes;
  
  private final ArrayList<String> mPageTypeZones;
  
  public String mReadError;
  
  boolean mRunning;
  
  String mRuntime;
  
  private int mSkippedInverseProcStateWarningCount;
  
  public long mStartTime;
  
  public final SysMemUsageTable mSysMemUsage;
  
  public final long[] mSysMemUsageArgs;
  
  public final SparseMappingTable mTableData;
  
  public long mTimePeriodEndRealtime;
  
  public long mTimePeriodEndUptime;
  
  public long mTimePeriodStartClock;
  
  public String mTimePeriodStartClockStr;
  
  public long mTimePeriodStartRealtime;
  
  public long mTimePeriodStartUptime;
  
  public ProcessStats(Parcel paramParcel) {
    SparseMappingTable sparseMappingTable = new SparseMappingTable();
    this.mSysMemUsageArgs = new long[16];
    this.mSysMemUsage = new SysMemUsageTable(sparseMappingTable);
    this.mPageTypeNodes = new ArrayList<>();
    this.mPageTypeZones = new ArrayList<>();
    this.mPageTypeLabels = new ArrayList<>();
    this.mPageTypeSizes = (ArrayList)new ArrayList<>();
    reset();
    readFromParcel(paramParcel);
  }
  
  public ProcessStats(boolean paramBoolean) {
    SparseMappingTable sparseMappingTable = new SparseMappingTable();
    this.mSysMemUsageArgs = new long[16];
    this.mSysMemUsage = new SysMemUsageTable(sparseMappingTable);
    this.mPageTypeNodes = new ArrayList<>();
    this.mPageTypeZones = new ArrayList<>();
    this.mPageTypeLabels = new ArrayList<>();
    this.mPageTypeSizes = (ArrayList)new ArrayList<>();
    this.mRunning = paramBoolean;
    reset();
    if (paramBoolean) {
      Debug.MemoryInfo memoryInfo = new Debug.MemoryInfo();
      Debug.getMemoryInfo(Process.myPid(), memoryInfo);
      this.mHasSwappedOutPss = memoryInfo.hasSwappedOutPss();
    } 
  }
  
  public ProcessStats() {
    this(false);
  }
  
  public void add(ProcessStats paramProcessStats) {
    ProcessMap<LongSparseArray<PackageState>> processMap = paramProcessStats.mPackages;
    ArrayMap<String, SparseArray<LongSparseArray<PackageState>>> arrayMap1 = processMap.getMap();
    byte b1;
    for (b1 = 0; b1 < arrayMap1.size(); b1++) {
      String str = arrayMap1.keyAt(b1);
      SparseArray<LongSparseArray> sparseArray = (SparseArray)arrayMap1.valueAt(b1);
      for (byte b = 0; b < sparseArray.size(); b++) {
        int i = sparseArray.keyAt(b);
        LongSparseArray<PackageState> longSparseArray = sparseArray.valueAt(b);
        for (byte b3 = 0; b3 < longSparseArray.size(); b3++) {
          long l1 = longSparseArray.keyAt(b3);
          PackageState packageState = longSparseArray.valueAt(b3);
          int j = packageState.mProcesses.size();
          int k = packageState.mServices.size();
          int m = packageState.mAssociations.size();
          for (byte b4 = 0; b4 < j; b4++) {
            ProcessState processState = packageState.mProcesses.valueAt(b4);
            if (processState.getCommonProcess() != processState) {
              String str1 = processState.getName();
              ProcessState processState1 = getProcessStateLocked(str, i, l1, str1);
              if (processState1.getCommonProcess() == processState1) {
                processState1.setMultiPackage(true);
                long l2 = SystemClock.uptimeMillis();
                PackageState packageState1 = getPackageStateLocked(str, i, l1);
                processState1 = processState1.clone(l2);
                packageState1.mProcesses.put(processState1.getName(), processState1);
              } 
              processState1.add(processState);
            } 
          } 
          for (j = 0; j < k; j++) {
            ServiceState serviceState2 = packageState.mServices.valueAt(j);
            String str2 = serviceState2.getProcessName(), str1 = serviceState2.getName();
            ServiceState serviceState1 = getServiceStateLocked(str, i, l1, str2, str1);
            serviceState1.add(serviceState2);
          } 
          for (k = 0; k < m; k++) {
            AssociationState associationState2 = packageState.mAssociations.valueAt(k);
            String str1 = associationState2.getProcessName(), str2 = associationState2.getName();
            AssociationState associationState1 = getAssociationStateLocked(str, i, l1, str1, str2);
            associationState1.add(associationState2);
          } 
        } 
      } 
    } 
    ArrayMap<String, SparseArray<ProcessState>> arrayMap = paramProcessStats.mProcesses.getMap();
    byte b2;
    for (b2 = 0; b2 < arrayMap.size(); b2++) {
      SparseArray<ProcessState> sparseArray = arrayMap.valueAt(b2);
      for (b1 = 0; b1 < sparseArray.size(); b1++) {
        int i = sparseArray.keyAt(b1);
        ProcessState processState2 = sparseArray.valueAt(b1);
        String str1 = processState2.getName();
        String str2 = processState2.getPackage();
        long l1 = processState2.getVersion();
        ProcessState processState1 = this.mProcesses.get(str1, i);
        if (processState1 == null) {
          ProcessState processState = new ProcessState(this, str2, i, l1, str1);
          this.mProcesses.put(str1, i, processState);
          PackageState packageState = getPackageStateLocked(str2, i, l1);
          processState1 = processState;
          if (!packageState.mProcesses.containsKey(str1)) {
            packageState.mProcesses.put(str1, processState);
            processState1 = processState;
          } 
        } 
        processState1.add(processState2);
      } 
    } 
    for (b2 = 0; b2 < 8; b2++) {
      long[] arrayOfLong = this.mMemFactorDurations;
      arrayOfLong[b2] = arrayOfLong[b2] + paramProcessStats.mMemFactorDurations[b2];
    } 
    this.mSysMemUsage.mergeStats(paramProcessStats.mSysMemUsage);
    this.mNumAggregated += paramProcessStats.mNumAggregated;
    long l = paramProcessStats.mTimePeriodStartClock;
    if (l < this.mTimePeriodStartClock) {
      this.mTimePeriodStartClock = l;
      this.mTimePeriodStartClockStr = paramProcessStats.mTimePeriodStartClockStr;
    } 
    this.mTimePeriodEndRealtime += paramProcessStats.mTimePeriodEndRealtime - paramProcessStats.mTimePeriodStartRealtime;
    this.mTimePeriodEndUptime += paramProcessStats.mTimePeriodEndUptime - paramProcessStats.mTimePeriodStartUptime;
    this.mInternalSinglePssCount += paramProcessStats.mInternalSinglePssCount;
    this.mInternalSinglePssTime += paramProcessStats.mInternalSinglePssTime;
    this.mInternalAllMemPssCount += paramProcessStats.mInternalAllMemPssCount;
    this.mInternalAllMemPssTime += paramProcessStats.mInternalAllMemPssTime;
    this.mInternalAllPollPssCount += paramProcessStats.mInternalAllPollPssCount;
    this.mInternalAllPollPssTime += paramProcessStats.mInternalAllPollPssTime;
    this.mExternalPssCount += paramProcessStats.mExternalPssCount;
    this.mExternalPssTime += paramProcessStats.mExternalPssTime;
    this.mExternalSlowPssCount += paramProcessStats.mExternalSlowPssCount;
    this.mExternalSlowPssTime += paramProcessStats.mExternalSlowPssTime;
    this.mHasSwappedOutPss |= paramProcessStats.mHasSwappedOutPss;
  }
  
  public void addSysMemUsage(long paramLong1, long paramLong2, long paramLong3, long paramLong4, long paramLong5) {
    int i = this.mMemFactor;
    if (i != -1) {
      this.mSysMemUsageArgs[0] = 1L;
      for (byte b = 0; b < 3; b++) {
        long[] arrayOfLong = this.mSysMemUsageArgs;
        arrayOfLong[b + 1] = paramLong1;
        arrayOfLong[b + 4] = paramLong2;
        arrayOfLong[b + 7] = paramLong3;
        arrayOfLong[b + 10] = paramLong4;
        arrayOfLong[b + 13] = paramLong5;
      } 
      this.mSysMemUsage.mergeStats(i * 14, this.mSysMemUsageArgs, 0);
    } 
  }
  
  public void computeTotalMemoryUse(TotalMemoryUseCollection paramTotalMemoryUseCollection, long paramLong) {
    paramTotalMemoryUseCollection.totalTime = 0L;
    byte b;
    for (b = 0; b < 14; b++) {
      paramTotalMemoryUseCollection.processStateWeight[b] = 0.0D;
      paramTotalMemoryUseCollection.processStatePss[b] = 0L;
      paramTotalMemoryUseCollection.processStateTime[b] = 0L;
      paramTotalMemoryUseCollection.processStateSamples[b] = 0;
    } 
    for (b = 0; b < 16; b++)
      paramTotalMemoryUseCollection.sysMemUsage[b] = 0L; 
    paramTotalMemoryUseCollection.sysMemCachedWeight = 0.0D;
    paramTotalMemoryUseCollection.sysMemFreeWeight = 0.0D;
    paramTotalMemoryUseCollection.sysMemZRamWeight = 0.0D;
    paramTotalMemoryUseCollection.sysMemKernelWeight = 0.0D;
    paramTotalMemoryUseCollection.sysMemNativeWeight = 0.0D;
    paramTotalMemoryUseCollection.sysMemSamples = 0;
    long[] arrayOfLong = this.mSysMemUsage.getTotalMemUsage();
    for (b = 0; b < paramTotalMemoryUseCollection.screenStates.length; b++) {
      for (byte b1 = 0; b1 < paramTotalMemoryUseCollection.memStates.length; b1++) {
        int i = paramTotalMemoryUseCollection.screenStates[b] + paramTotalMemoryUseCollection.memStates[b1];
        long l1 = this.mMemFactorDurations[i];
        long l2 = l1;
        if (this.mMemFactor == i)
          l2 = l1 + paramLong - this.mStartTime; 
        paramTotalMemoryUseCollection.totalTime += l2;
        int j = this.mSysMemUsage.getKey((byte)(i * 14));
        long[] arrayOfLong1 = arrayOfLong;
        i = 0;
        if (j != -1) {
          long[] arrayOfLong2 = this.mSysMemUsage.getArrayForKey(j);
          j = SparseMappingTable.getIndexFromKey(j);
          if (arrayOfLong2[j + 0] >= 3L) {
            SysMemUsageTable.mergeSysMemUsage(paramTotalMemoryUseCollection.sysMemUsage, 0, arrayOfLong1, 0);
            arrayOfLong1 = arrayOfLong2;
            i = j;
          } 
        } 
        paramTotalMemoryUseCollection.sysMemCachedWeight += arrayOfLong1[i + 2] * l2;
        paramTotalMemoryUseCollection.sysMemFreeWeight += arrayOfLong1[i + 5] * l2;
        paramTotalMemoryUseCollection.sysMemZRamWeight += arrayOfLong1[i + 8] * l2;
        paramTotalMemoryUseCollection.sysMemKernelWeight += arrayOfLong1[i + 11] * l2;
        paramTotalMemoryUseCollection.sysMemNativeWeight += arrayOfLong1[i + 14] * l2;
        paramTotalMemoryUseCollection.sysMemSamples = (int)(paramTotalMemoryUseCollection.sysMemSamples + arrayOfLong1[i + 0]);
      } 
    } 
    paramTotalMemoryUseCollection.hasSwappedOutPss = this.mHasSwappedOutPss;
    ArrayMap<String, SparseArray<ProcessState>> arrayMap = this.mProcesses.getMap();
    for (b = 0; b < arrayMap.size(); b++) {
      SparseArray<ProcessState> sparseArray = arrayMap.valueAt(b);
      for (byte b1 = 0; b1 < sparseArray.size(); b1++) {
        ProcessState processState = sparseArray.valueAt(b1);
        processState.aggregatePss(paramTotalMemoryUseCollection, paramLong);
      } 
    } 
  }
  
  public void reset() {
    resetCommon();
    this.mPackages.getMap().clear();
    this.mProcesses.getMap().clear();
    this.mMemFactor = -1;
    this.mStartTime = 0L;
  }
  
  public void resetSafely() {
    resetCommon();
    long l = SystemClock.uptimeMillis();
    ArrayMap<String, SparseArray<ProcessState>> arrayMap = this.mProcesses.getMap();
    int i;
    for (i = arrayMap.size() - 1; i >= 0; i--) {
      SparseArray sparseArray = arrayMap.valueAt(i);
      for (int j = sparseArray.size() - 1; j >= 0; j--)
        ((ProcessState)sparseArray.valueAt(j)).tmpNumInUse = 0; 
    } 
    ProcessMap<LongSparseArray<PackageState>> processMap = this.mPackages;
    ArrayMap<String, SparseArray<LongSparseArray<PackageState>>> arrayMap1 = processMap.getMap();
    for (i = arrayMap1.size() - 1; i >= 0; i--) {
      SparseArray<LongSparseArray> sparseArray = (SparseArray)arrayMap1.valueAt(i);
      for (int j = sparseArray.size() - 1; j >= 0; j--) {
        LongSparseArray<PackageState> longSparseArray = sparseArray.valueAt(j);
        for (int k = longSparseArray.size() - 1; k >= 0; k--) {
          PackageState packageState = longSparseArray.valueAt(k);
          int m;
          for (m = packageState.mProcesses.size() - 1; m >= 0; m--) {
            ProcessState processState = packageState.mProcesses.valueAt(m);
            if (processState.isInUse()) {
              processState.resetSafely(l);
              ProcessState processState1 = processState.getCommonProcess();
              processState1.tmpNumInUse++;
              (processState.getCommonProcess()).tmpFoundSubProc = processState;
            } else {
              ((ProcessState)packageState.mProcesses.valueAt(m)).makeDead();
              packageState.mProcesses.removeAt(m);
            } 
          } 
          for (m = packageState.mServices.size() - 1; m >= 0; m--) {
            ServiceState serviceState = packageState.mServices.valueAt(m);
            if (serviceState.isInUse()) {
              serviceState.resetSafely(l);
            } else {
              packageState.mServices.removeAt(m);
            } 
          } 
          for (m = packageState.mAssociations.size() - 1; m >= 0; m--) {
            AssociationState associationState = packageState.mAssociations.valueAt(m);
            if (associationState.isInUse()) {
              associationState.resetSafely(l);
            } else {
              packageState.mAssociations.removeAt(m);
            } 
          } 
          if (packageState.mProcesses.size() <= 0 && packageState.mServices.size() <= 0) {
            ArrayMap<String, AssociationState> arrayMap2 = packageState.mAssociations;
            if (arrayMap2.size() <= 0)
              longSparseArray.removeAt(k); 
          } 
        } 
        if (longSparseArray.size() <= 0)
          sparseArray.removeAt(j); 
      } 
      if (sparseArray.size() <= 0)
        arrayMap1.removeAt(i); 
    } 
    for (i = arrayMap.size() - 1; i >= 0; i--) {
      SparseArray<ProcessState> sparseArray = arrayMap.valueAt(i);
      for (int j = sparseArray.size() - 1; j >= 0; j--) {
        ProcessState processState = sparseArray.valueAt(j);
        if (processState.isInUse() || processState.tmpNumInUse > 0) {
          if (!processState.isActive() && processState.isMultiPackage() && processState.tmpNumInUse == 1) {
            processState = processState.tmpFoundSubProc;
            processState.makeStandalone();
            sparseArray.setValueAt(j, processState);
          } else {
            processState.resetSafely(l);
          } 
        } else {
          processState.makeDead();
          sparseArray.removeAt(j);
        } 
      } 
      if (sparseArray.size() <= 0)
        arrayMap.removeAt(i); 
    } 
    this.mStartTime = l;
  }
  
  private void resetCommon() {
    this.mNumAggregated = 1;
    this.mTimePeriodStartClock = System.currentTimeMillis();
    buildTimePeriodStartClockStr();
    long l = SystemClock.elapsedRealtime();
    this.mTimePeriodStartRealtime = l;
    this.mTimePeriodEndUptime = l = SystemClock.uptimeMillis();
    this.mTimePeriodStartUptime = l;
    this.mInternalSinglePssCount = 0L;
    this.mInternalSinglePssTime = 0L;
    this.mInternalAllMemPssCount = 0L;
    this.mInternalAllMemPssTime = 0L;
    this.mInternalAllPollPssCount = 0L;
    this.mInternalAllPollPssTime = 0L;
    this.mExternalPssCount = 0L;
    this.mExternalPssTime = 0L;
    this.mExternalSlowPssCount = 0L;
    this.mExternalSlowPssTime = 0L;
    this.mTableData.reset();
    Arrays.fill(this.mMemFactorDurations, 0L);
    this.mSysMemUsage.resetTable();
    this.mStartTime = 0L;
    this.mReadError = null;
    this.mFlags = 0;
    evaluateSystemProperties(true);
    updateFragmentation();
  }
  
  public boolean evaluateSystemProperties(boolean paramBoolean) {
    boolean bool = false;
    String str = VMRuntime.getRuntime().vmLibrary();
    str = SystemProperties.get("persist.sys.dalvik.vm.lib.2", str);
    if (!Objects.equals(str, this.mRuntime)) {
      boolean bool1 = true;
      bool = bool1;
      if (paramBoolean) {
        this.mRuntime = str;
        bool = bool1;
      } 
    } 
    return bool;
  }
  
  private void buildTimePeriodStartClockStr() {
    CharSequence charSequence = DateFormat.format("yyyy-MM-dd-HH-mm-ss", this.mTimePeriodStartClock);
    this.mTimePeriodStartClockStr = charSequence.toString();
  }
  
  public void updateFragmentation() {
    BufferedReader bufferedReader1 = null, bufferedReader2 = null;
    BufferedReader bufferedReader3 = bufferedReader2, bufferedReader4 = bufferedReader1;
    try {
      BufferedReader bufferedReader = new BufferedReader();
      bufferedReader3 = bufferedReader2;
      bufferedReader4 = bufferedReader1;
      FileReader fileReader = new FileReader();
      bufferedReader3 = bufferedReader2;
      bufferedReader4 = bufferedReader1;
      this("/proc/pagetypeinfo");
      bufferedReader3 = bufferedReader2;
      bufferedReader4 = bufferedReader1;
      this(fileReader);
      bufferedReader2 = bufferedReader;
      bufferedReader3 = bufferedReader2;
      bufferedReader4 = bufferedReader2;
      Matcher matcher = sPageTypeRegex.matcher("");
      bufferedReader3 = bufferedReader2;
      bufferedReader4 = bufferedReader2;
      this.mPageTypeNodes.clear();
      bufferedReader3 = bufferedReader2;
      bufferedReader4 = bufferedReader2;
      this.mPageTypeZones.clear();
      bufferedReader3 = bufferedReader2;
      bufferedReader4 = bufferedReader2;
      this.mPageTypeLabels.clear();
      bufferedReader3 = bufferedReader2;
      bufferedReader4 = bufferedReader2;
      this.mPageTypeSizes.clear();
      while (true) {
        bufferedReader3 = bufferedReader2;
        bufferedReader4 = bufferedReader2;
        String str = bufferedReader2.readLine();
        if (str == null) {
          try {
            bufferedReader2.close();
          } catch (IOException iOException) {}
          return;
        } 
        bufferedReader3 = bufferedReader2;
        bufferedReader4 = bufferedReader2;
        matcher.reset(str);
        bufferedReader3 = bufferedReader2;
        bufferedReader4 = bufferedReader2;
        if (matcher.matches()) {
          bufferedReader3 = bufferedReader2;
          bufferedReader4 = bufferedReader2;
          Integer integer = Integer.valueOf(matcher.group(1), 10);
          if (integer == null)
            continue; 
          bufferedReader3 = bufferedReader2;
          bufferedReader4 = bufferedReader2;
          this.mPageTypeNodes.add(integer);
          bufferedReader3 = bufferedReader2;
          bufferedReader4 = bufferedReader2;
          this.mPageTypeZones.add(matcher.group(2));
          bufferedReader3 = bufferedReader2;
          bufferedReader4 = bufferedReader2;
          this.mPageTypeLabels.add(matcher.group(3));
          bufferedReader3 = bufferedReader2;
          bufferedReader4 = bufferedReader2;
          this.mPageTypeSizes.add(splitAndParseNumbers(matcher.group(4)));
        } 
      } 
    } catch (IOException iOException) {
      bufferedReader3 = bufferedReader4;
      this.mPageTypeNodes.clear();
      bufferedReader3 = bufferedReader4;
      this.mPageTypeZones.clear();
      bufferedReader3 = bufferedReader4;
      this.mPageTypeLabels.clear();
      bufferedReader3 = bufferedReader4;
      this.mPageTypeSizes.clear();
      if (bufferedReader4 != null)
        try {
          bufferedReader4.close();
        } catch (IOException iOException1) {} 
      return;
    } finally {}
    if (bufferedReader3 != null)
      try {
        bufferedReader3.close();
      } catch (IOException iOException) {} 
    throw bufferedReader4;
  }
  
  private static int[] splitAndParseNumbers(String paramString) {
    int i = 0;
    byte b1 = 0;
    int j = paramString.length();
    byte b2;
    for (b2 = 0; b2 < j; b2++, i = n, b1 = b) {
      byte b;
      int n = paramString.charAt(b2);
      if (n >= 48 && n <= 57) {
        n = i;
        b = b1;
        if (!i) {
          n = 1;
          b = b1 + 1;
        } 
      } else {
        n = 0;
        b = b1;
      } 
    } 
    int[] arrayOfInt = new int[b1];
    int k = 0;
    byte b3 = 0;
    int m;
    for (b2 = 0, m = i; b2 < j; b2++, bool2 = bool1, k = n, b3 = b) {
      byte b;
      boolean bool1, bool2;
      int n;
      i = paramString.charAt(b2);
      if (i >= 48 && i <= 57) {
        if (m == 0) {
          bool1 = true;
          i -= '0';
          n = k;
        } else {
          i = b3 * 10 + i - 48;
          int i1 = m;
          n = k;
        } 
      } else {
        int i1 = m;
        n = k;
        b = b3;
        if (m != 0) {
          bool1 = false;
          arrayOfInt[k] = b3;
          n = k + 1;
          b = b3;
        } 
      } 
    } 
    if (b1 > 0)
      arrayOfInt[b1 - 1] = b3; 
    return arrayOfInt;
  }
  
  private void writeCompactedLongArray(Parcel paramParcel, long[] paramArrayOflong, int paramInt) {
    for (byte b = 0; b < paramInt; b++) {
      long l1 = paramArrayOflong[b];
      long l2 = l1;
      if (l1 < 0L) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Time val negative: ");
        stringBuilder.append(l1);
        Slog.w("ProcessStats", stringBuilder.toString());
        l2 = 0L;
      } 
      if (l2 <= 2147483647L) {
        paramParcel.writeInt((int)l2);
      } else {
        int i = (int)(0x7FFFFFFFL & l2 >> 32L);
        int j = (int)(0xFFFFFFFFL & l2);
        paramParcel.writeInt(i ^ 0xFFFFFFFF);
        paramParcel.writeInt(j);
      } 
    } 
  }
  
  private void readCompactedLongArray(Parcel paramParcel, int paramInt1, long[] paramArrayOflong, int paramInt2) {
    if (paramInt1 <= 10) {
      paramParcel.readLongArray(paramArrayOflong);
      return;
    } 
    int i = paramArrayOflong.length;
    if (paramInt2 <= i) {
      int j;
      paramInt1 = 0;
      while (true) {
        j = paramInt1;
        if (paramInt1 < paramInt2) {
          j = paramParcel.readInt();
          if (j >= 0) {
            paramArrayOflong[paramInt1] = j;
          } else {
            int k = paramParcel.readInt();
            paramArrayOflong[paramInt1] = (j ^ 0xFFFFFFFF) << 32L | k;
          } 
          paramInt1++;
          continue;
        } 
        break;
      } 
      while (j < i) {
        paramArrayOflong[j] = 0L;
        j++;
      } 
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("bad array lengths: got ");
    stringBuilder.append(paramInt2);
    stringBuilder.append(" array is ");
    stringBuilder.append(i);
    throw new RuntimeException(stringBuilder.toString());
  }
  
  void writeCommonString(Parcel paramParcel, String paramString) {
    Integer integer = this.mCommonStringToIndex.get(paramString);
    if (integer != null) {
      paramParcel.writeInt(integer.intValue());
      return;
    } 
    integer = Integer.valueOf(this.mCommonStringToIndex.size());
    this.mCommonStringToIndex.put(paramString, integer);
    paramParcel.writeInt(integer.intValue() ^ 0xFFFFFFFF);
    paramParcel.writeString(paramString);
  }
  
  String readCommonString(Parcel paramParcel, int paramInt) {
    if (paramInt <= 9)
      return paramParcel.readString(); 
    paramInt = paramParcel.readInt();
    if (paramInt >= 0)
      return this.mIndexToCommonString.get(paramInt); 
    paramInt ^= 0xFFFFFFFF;
    String str = paramParcel.readString();
    while (this.mIndexToCommonString.size() <= paramInt)
      this.mIndexToCommonString.add(null); 
    this.mIndexToCommonString.set(paramInt, str);
    return str;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcel(paramParcel, SystemClock.uptimeMillis(), paramInt);
  }
  
  public void writeToParcel(Parcel paramParcel, long paramLong, int paramInt) {
    paramParcel.writeInt(1347638356);
    paramParcel.writeInt(38);
    paramParcel.writeInt(14);
    paramParcel.writeInt(8);
    paramParcel.writeInt(10);
    paramParcel.writeInt(16);
    paramParcel.writeInt(4096);
    this.mCommonStringToIndex = new ArrayMap<>(this.mProcesses.size());
    ArrayMap<String, SparseArray<ProcessState>> arrayMap1 = this.mProcesses.getMap();
    int i = arrayMap1.size();
    for (paramInt = 0; paramInt < i; paramInt++) {
      SparseArray<ProcessState> sparseArray = arrayMap1.valueAt(paramInt);
      int n = sparseArray.size();
      for (byte b = 0; b < n; b++)
        ((ProcessState)sparseArray.valueAt(b)).commitStateTime(paramLong); 
    } 
    ProcessMap<LongSparseArray<PackageState>> processMap = this.mPackages;
    ArrayMap<String, SparseArray<LongSparseArray<PackageState>>> arrayMap3 = processMap.getMap();
    int k = arrayMap3.size();
    for (paramInt = 0; paramInt < k; paramInt++) {
      SparseArray<LongSparseArray> sparseArray = (SparseArray)arrayMap3.valueAt(paramInt);
      int n = sparseArray.size();
      for (byte b = 0; b < n; b++) {
        LongSparseArray<PackageState> longSparseArray = sparseArray.valueAt(b);
        int i1 = longSparseArray.size();
        for (byte b1 = 0; b1 < i1; b1++) {
          PackageState packageState = longSparseArray.valueAt(b1);
          int i2 = packageState.mProcesses.size();
          byte b2;
          for (b2 = 0; b2 < i2; b2++) {
            ProcessState processState = packageState.mProcesses.valueAt(b2);
            if (processState.getCommonProcess() != processState)
              processState.commitStateTime(paramLong); 
          } 
          i2 = packageState.mServices.size();
          for (b2 = 0; b2 < i2; b2++)
            ((ServiceState)packageState.mServices.valueAt(b2)).commitStateTime(paramLong); 
          int i3 = packageState.mAssociations.size();
          for (b2 = 0; b2 < i3; b2++)
            ((AssociationState)packageState.mAssociations.valueAt(b2)).commitStateTime(paramLong); 
        } 
      } 
    } 
    paramParcel.writeInt(this.mNumAggregated);
    paramParcel.writeLong(this.mTimePeriodStartClock);
    paramParcel.writeLong(this.mTimePeriodStartRealtime);
    paramParcel.writeLong(this.mTimePeriodEndRealtime);
    paramParcel.writeLong(this.mTimePeriodStartUptime);
    paramParcel.writeLong(this.mTimePeriodEndUptime);
    paramParcel.writeLong(this.mInternalSinglePssCount);
    paramParcel.writeLong(this.mInternalSinglePssTime);
    paramParcel.writeLong(this.mInternalAllMemPssCount);
    paramParcel.writeLong(this.mInternalAllMemPssTime);
    paramParcel.writeLong(this.mInternalAllPollPssCount);
    paramParcel.writeLong(this.mInternalAllPollPssTime);
    paramParcel.writeLong(this.mExternalPssCount);
    paramParcel.writeLong(this.mExternalPssTime);
    paramParcel.writeLong(this.mExternalSlowPssCount);
    paramParcel.writeLong(this.mExternalSlowPssTime);
    paramParcel.writeString(this.mRuntime);
    paramParcel.writeInt(this.mHasSwappedOutPss);
    paramParcel.writeInt(this.mFlags);
    this.mTableData.writeToParcel(paramParcel);
    paramInt = this.mMemFactor;
    if (paramInt != -1) {
      long[] arrayOfLong1 = this.mMemFactorDurations;
      arrayOfLong1[paramInt] = arrayOfLong1[paramInt] + paramLong - this.mStartTime;
      this.mStartTime = paramLong;
    } 
    long[] arrayOfLong = this.mMemFactorDurations;
    writeCompactedLongArray(paramParcel, arrayOfLong, arrayOfLong.length);
    this.mSysMemUsage.writeToParcel(paramParcel);
    paramParcel.writeInt(i);
    for (paramInt = 0; paramInt < i; paramInt++) {
      writeCommonString(paramParcel, arrayMap1.keyAt(paramInt));
      SparseArray<ProcessState> sparseArray = arrayMap1.valueAt(paramInt);
      int n = sparseArray.size();
      paramParcel.writeInt(n);
      for (byte b = 0; b < n; b++) {
        paramParcel.writeInt(sparseArray.keyAt(b));
        ProcessState processState = sparseArray.valueAt(b);
        writeCommonString(paramParcel, processState.getPackage());
        paramParcel.writeLong(processState.getVersion());
        processState.writeToParcel(paramParcel, paramLong);
      } 
    } 
    paramParcel.writeInt(k);
    ArrayMap<String, SparseArray<LongSparseArray<PackageState>>> arrayMap2;
    int j;
    ArrayMap<String, SparseArray<ProcessState>> arrayMap4;
    int m;
    for (j = 0, paramInt = k, arrayMap2 = arrayMap3, m = i, arrayMap4 = arrayMap1; j < paramInt; j++) {
      writeCommonString(paramParcel, arrayMap2.keyAt(j));
      SparseArray<LongSparseArray> sparseArray = (SparseArray)arrayMap2.valueAt(j);
      k = sparseArray.size();
      paramParcel.writeInt(k);
      for (byte b = 0; b < k; b++) {
        paramParcel.writeInt(sparseArray.keyAt(b));
        LongSparseArray<PackageState> longSparseArray = sparseArray.valueAt(b);
        int n = longSparseArray.size();
        paramParcel.writeInt(n);
        for (byte b1 = 0; b1 < n; b1++) {
          paramParcel.writeLong(longSparseArray.keyAt(b1));
          PackageState packageState = longSparseArray.valueAt(b1);
          i = packageState.mProcesses.size();
          paramParcel.writeInt(i);
          int i1;
          for (i1 = 0; i1 < i; i1++) {
            writeCommonString(paramParcel, packageState.mProcesses.keyAt(i1));
            ProcessState processState = packageState.mProcesses.valueAt(i1);
            if (processState.getCommonProcess() == processState) {
              paramParcel.writeInt(0);
            } else {
              paramParcel.writeInt(1);
              processState.writeToParcel(paramParcel, paramLong);
            } 
          } 
          i1 = packageState.mServices.size();
          paramParcel.writeInt(i1);
          for (i = 0; i < i1; i++) {
            paramParcel.writeString(packageState.mServices.keyAt(i));
            ServiceState serviceState = packageState.mServices.valueAt(i);
            writeCommonString(paramParcel, serviceState.getProcessName());
            serviceState.writeToParcel(paramParcel, paramLong);
          } 
          i = packageState.mAssociations.size();
          paramParcel.writeInt(i);
          for (i1 = 0; i1 < i; i1++) {
            writeCommonString(paramParcel, packageState.mAssociations.keyAt(i1));
            AssociationState associationState = packageState.mAssociations.valueAt(i1);
            writeCommonString(paramParcel, associationState.getProcessName());
            associationState.writeToParcel(this, paramParcel, paramLong);
          } 
        } 
      } 
    } 
    j = this.mPageTypeLabels.size();
    paramParcel.writeInt(j);
    for (paramInt = 0; paramInt < j; paramInt++) {
      paramParcel.writeInt(((Integer)this.mPageTypeNodes.get(paramInt)).intValue());
      paramParcel.writeString(this.mPageTypeZones.get(paramInt));
      paramParcel.writeString(this.mPageTypeLabels.get(paramInt));
      paramParcel.writeIntArray(this.mPageTypeSizes.get(paramInt));
    } 
    this.mCommonStringToIndex = null;
  }
  
  private boolean readCheckedInt(Parcel paramParcel, int paramInt, String paramString) {
    int i = paramParcel.readInt();
    if (i != paramInt) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("bad ");
      stringBuilder.append(paramString);
      stringBuilder.append(": ");
      stringBuilder.append(i);
      this.mReadError = stringBuilder.toString();
      return false;
    } 
    return true;
  }
  
  static byte[] readFully(InputStream paramInputStream, int[] paramArrayOfint) throws IOException {
    int i = 0;
    int j = paramInputStream.available();
    if (j > 0) {
      j++;
    } else {
      j = 16384;
    } 
    byte[] arrayOfByte = new byte[j];
    j = i;
    while (true) {
      i = paramInputStream.read(arrayOfByte, j, arrayOfByte.length - j);
      if (i < 0) {
        paramArrayOfint[0] = j;
        return arrayOfByte;
      } 
      j += i;
      byte[] arrayOfByte1 = arrayOfByte;
      if (j >= arrayOfByte.length) {
        arrayOfByte1 = new byte[j + 16384];
        System.arraycopy(arrayOfByte, 0, arrayOfByte1, 0, j);
      } 
      arrayOfByte = arrayOfByte1;
    } 
  }
  
  public void read(InputStream paramInputStream) {
    try {
      int[] arrayOfInt = new int[1];
      byte[] arrayOfByte = readFully(paramInputStream, arrayOfInt);
      Parcel parcel = Parcel.obtain();
      parcel.unmarshall(arrayOfByte, 0, arrayOfInt[0]);
      parcel.setDataPosition(0);
      paramInputStream.close();
      readFromParcel(parcel);
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("caught exception: ");
      stringBuilder.append(iOException);
      this.mReadError = stringBuilder.toString();
    } 
  }
  
  public void readFromParcel(Parcel paramParcel) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mPackages : Lcom/android/internal/app/ProcessMap;
    //   4: invokevirtual getMap : ()Landroid/util/ArrayMap;
    //   7: invokevirtual size : ()I
    //   10: ifgt -> 36
    //   13: aload_0
    //   14: getfield mProcesses : Lcom/android/internal/app/ProcessMap;
    //   17: astore_2
    //   18: aload_2
    //   19: invokevirtual getMap : ()Landroid/util/ArrayMap;
    //   22: invokevirtual size : ()I
    //   25: ifle -> 31
    //   28: goto -> 36
    //   31: iconst_0
    //   32: istore_3
    //   33: goto -> 38
    //   36: iconst_1
    //   37: istore_3
    //   38: iload_3
    //   39: ifeq -> 46
    //   42: aload_0
    //   43: invokevirtual resetSafely : ()V
    //   46: aload_0
    //   47: aload_1
    //   48: ldc 1347638356
    //   50: ldc_w 'magic number'
    //   53: invokespecial readCheckedInt : (Landroid/os/Parcel;ILjava/lang/String;)Z
    //   56: ifne -> 60
    //   59: return
    //   60: aload_1
    //   61: invokevirtual readInt : ()I
    //   64: istore #4
    //   66: iload #4
    //   68: bipush #38
    //   70: if_icmpeq -> 105
    //   73: new java/lang/StringBuilder
    //   76: dup
    //   77: invokespecial <init> : ()V
    //   80: astore_1
    //   81: aload_1
    //   82: ldc_w 'bad version: '
    //   85: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   88: pop
    //   89: aload_1
    //   90: iload #4
    //   92: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   95: pop
    //   96: aload_0
    //   97: aload_1
    //   98: invokevirtual toString : ()Ljava/lang/String;
    //   101: putfield mReadError : Ljava/lang/String;
    //   104: return
    //   105: aload_0
    //   106: aload_1
    //   107: bipush #14
    //   109: ldc_w 'state count'
    //   112: invokespecial readCheckedInt : (Landroid/os/Parcel;ILjava/lang/String;)Z
    //   115: ifne -> 119
    //   118: return
    //   119: aload_0
    //   120: aload_1
    //   121: bipush #8
    //   123: ldc_w 'adj count'
    //   126: invokespecial readCheckedInt : (Landroid/os/Parcel;ILjava/lang/String;)Z
    //   129: ifne -> 133
    //   132: return
    //   133: aload_0
    //   134: aload_1
    //   135: bipush #10
    //   137: ldc_w 'pss count'
    //   140: invokespecial readCheckedInt : (Landroid/os/Parcel;ILjava/lang/String;)Z
    //   143: ifne -> 147
    //   146: return
    //   147: aload_0
    //   148: aload_1
    //   149: bipush #16
    //   151: ldc_w 'sys mem usage count'
    //   154: invokespecial readCheckedInt : (Landroid/os/Parcel;ILjava/lang/String;)Z
    //   157: ifne -> 161
    //   160: return
    //   161: aload_0
    //   162: aload_1
    //   163: sipush #4096
    //   166: ldc_w 'longs size'
    //   169: invokespecial readCheckedInt : (Landroid/os/Parcel;ILjava/lang/String;)Z
    //   172: ifne -> 176
    //   175: return
    //   176: aload_0
    //   177: new java/util/ArrayList
    //   180: dup
    //   181: invokespecial <init> : ()V
    //   184: putfield mIndexToCommonString : Ljava/util/ArrayList;
    //   187: aload_0
    //   188: aload_1
    //   189: invokevirtual readInt : ()I
    //   192: putfield mNumAggregated : I
    //   195: aload_0
    //   196: aload_1
    //   197: invokevirtual readLong : ()J
    //   200: putfield mTimePeriodStartClock : J
    //   203: aload_0
    //   204: invokespecial buildTimePeriodStartClockStr : ()V
    //   207: aload_0
    //   208: aload_1
    //   209: invokevirtual readLong : ()J
    //   212: putfield mTimePeriodStartRealtime : J
    //   215: aload_0
    //   216: aload_1
    //   217: invokevirtual readLong : ()J
    //   220: putfield mTimePeriodEndRealtime : J
    //   223: aload_0
    //   224: aload_1
    //   225: invokevirtual readLong : ()J
    //   228: putfield mTimePeriodStartUptime : J
    //   231: aload_0
    //   232: aload_1
    //   233: invokevirtual readLong : ()J
    //   236: putfield mTimePeriodEndUptime : J
    //   239: aload_0
    //   240: aload_1
    //   241: invokevirtual readLong : ()J
    //   244: putfield mInternalSinglePssCount : J
    //   247: aload_0
    //   248: aload_1
    //   249: invokevirtual readLong : ()J
    //   252: putfield mInternalSinglePssTime : J
    //   255: aload_0
    //   256: aload_1
    //   257: invokevirtual readLong : ()J
    //   260: putfield mInternalAllMemPssCount : J
    //   263: aload_0
    //   264: aload_1
    //   265: invokevirtual readLong : ()J
    //   268: putfield mInternalAllMemPssTime : J
    //   271: aload_0
    //   272: aload_1
    //   273: invokevirtual readLong : ()J
    //   276: putfield mInternalAllPollPssCount : J
    //   279: aload_0
    //   280: aload_1
    //   281: invokevirtual readLong : ()J
    //   284: putfield mInternalAllPollPssTime : J
    //   287: aload_0
    //   288: aload_1
    //   289: invokevirtual readLong : ()J
    //   292: putfield mExternalPssCount : J
    //   295: aload_0
    //   296: aload_1
    //   297: invokevirtual readLong : ()J
    //   300: putfield mExternalPssTime : J
    //   303: aload_0
    //   304: aload_1
    //   305: invokevirtual readLong : ()J
    //   308: putfield mExternalSlowPssCount : J
    //   311: aload_0
    //   312: aload_1
    //   313: invokevirtual readLong : ()J
    //   316: putfield mExternalSlowPssTime : J
    //   319: aload_0
    //   320: aload_1
    //   321: invokevirtual readString : ()Ljava/lang/String;
    //   324: putfield mRuntime : Ljava/lang/String;
    //   327: aload_1
    //   328: invokevirtual readInt : ()I
    //   331: ifeq -> 340
    //   334: iconst_1
    //   335: istore #5
    //   337: goto -> 343
    //   340: iconst_0
    //   341: istore #5
    //   343: aload_0
    //   344: iload #5
    //   346: putfield mHasSwappedOutPss : Z
    //   349: aload_0
    //   350: aload_1
    //   351: invokevirtual readInt : ()I
    //   354: putfield mFlags : I
    //   357: aload_0
    //   358: getfield mTableData : Lcom/android/internal/app/procstats/SparseMappingTable;
    //   361: aload_1
    //   362: invokevirtual readFromParcel : (Landroid/os/Parcel;)V
    //   365: aload_0
    //   366: getfield mMemFactorDurations : [J
    //   369: astore_2
    //   370: aload_0
    //   371: aload_1
    //   372: iload #4
    //   374: aload_2
    //   375: aload_2
    //   376: arraylength
    //   377: invokespecial readCompactedLongArray : (Landroid/os/Parcel;I[JI)V
    //   380: aload_0
    //   381: getfield mSysMemUsage : Lcom/android/internal/app/procstats/SysMemUsageTable;
    //   384: aload_1
    //   385: invokevirtual readFromParcel : (Landroid/os/Parcel;)Z
    //   388: ifne -> 392
    //   391: return
    //   392: aload_1
    //   393: invokevirtual readInt : ()I
    //   396: istore #6
    //   398: iload #6
    //   400: ifge -> 435
    //   403: new java/lang/StringBuilder
    //   406: dup
    //   407: invokespecial <init> : ()V
    //   410: astore_1
    //   411: aload_1
    //   412: ldc_w 'bad process count: '
    //   415: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   418: pop
    //   419: aload_1
    //   420: iload #6
    //   422: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   425: pop
    //   426: aload_0
    //   427: aload_1
    //   428: invokevirtual toString : ()Ljava/lang/String;
    //   431: putfield mReadError : Ljava/lang/String;
    //   434: return
    //   435: iload #6
    //   437: ifle -> 683
    //   440: aload_0
    //   441: aload_1
    //   442: iload #4
    //   444: invokevirtual readCommonString : (Landroid/os/Parcel;I)Ljava/lang/String;
    //   447: astore #7
    //   449: aload #7
    //   451: ifnonnull -> 462
    //   454: aload_0
    //   455: ldc_w 'bad process name'
    //   458: putfield mReadError : Ljava/lang/String;
    //   461: return
    //   462: aload_1
    //   463: invokevirtual readInt : ()I
    //   466: istore #8
    //   468: iload #8
    //   470: istore #9
    //   472: iload #8
    //   474: ifge -> 509
    //   477: new java/lang/StringBuilder
    //   480: dup
    //   481: invokespecial <init> : ()V
    //   484: astore_1
    //   485: aload_1
    //   486: ldc_w 'bad uid count: '
    //   489: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   492: pop
    //   493: aload_1
    //   494: iload #8
    //   496: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   499: pop
    //   500: aload_0
    //   501: aload_1
    //   502: invokevirtual toString : ()Ljava/lang/String;
    //   505: putfield mReadError : Ljava/lang/String;
    //   508: return
    //   509: iload #9
    //   511: ifle -> 677
    //   514: aload_1
    //   515: invokevirtual readInt : ()I
    //   518: istore #8
    //   520: iload #8
    //   522: ifge -> 557
    //   525: new java/lang/StringBuilder
    //   528: dup
    //   529: invokespecial <init> : ()V
    //   532: astore_1
    //   533: aload_1
    //   534: ldc_w 'bad uid: '
    //   537: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   540: pop
    //   541: aload_1
    //   542: iload #8
    //   544: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   547: pop
    //   548: aload_0
    //   549: aload_1
    //   550: invokevirtual toString : ()Ljava/lang/String;
    //   553: putfield mReadError : Ljava/lang/String;
    //   556: return
    //   557: aload_0
    //   558: aload_1
    //   559: iload #4
    //   561: invokevirtual readCommonString : (Landroid/os/Parcel;I)Ljava/lang/String;
    //   564: astore #10
    //   566: aload #10
    //   568: ifnonnull -> 579
    //   571: aload_0
    //   572: ldc_w 'bad process package name'
    //   575: putfield mReadError : Ljava/lang/String;
    //   578: return
    //   579: aload_1
    //   580: invokevirtual readLong : ()J
    //   583: lstore #11
    //   585: iload_3
    //   586: ifeq -> 607
    //   589: aload_0
    //   590: getfield mProcesses : Lcom/android/internal/app/ProcessMap;
    //   593: aload #7
    //   595: iload #8
    //   597: invokevirtual get : (Ljava/lang/String;I)Ljava/lang/Object;
    //   600: checkcast com/android/internal/app/procstats/ProcessState
    //   603: astore_2
    //   604: goto -> 609
    //   607: aconst_null
    //   608: astore_2
    //   609: aload_2
    //   610: ifnull -> 626
    //   613: aload_2
    //   614: aload_1
    //   615: iconst_0
    //   616: invokevirtual readFromParcel : (Landroid/os/Parcel;Z)Z
    //   619: ifne -> 623
    //   622: return
    //   623: goto -> 658
    //   626: new com/android/internal/app/procstats/ProcessState
    //   629: dup
    //   630: aload_0
    //   631: aload #10
    //   633: iload #8
    //   635: lload #11
    //   637: aload #7
    //   639: invokespecial <init> : (Lcom/android/internal/app/procstats/ProcessStats;Ljava/lang/String;IJLjava/lang/String;)V
    //   642: astore #10
    //   644: aload #10
    //   646: astore_2
    //   647: aload #10
    //   649: aload_1
    //   650: iconst_1
    //   651: invokevirtual readFromParcel : (Landroid/os/Parcel;Z)Z
    //   654: ifne -> 658
    //   657: return
    //   658: aload_0
    //   659: getfield mProcesses : Lcom/android/internal/app/ProcessMap;
    //   662: aload #7
    //   664: iload #8
    //   666: aload_2
    //   667: invokevirtual put : (Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/Object;
    //   670: pop
    //   671: iinc #9, -1
    //   674: goto -> 509
    //   677: iinc #6, -1
    //   680: goto -> 435
    //   683: aload_1
    //   684: invokevirtual readInt : ()I
    //   687: istore #9
    //   689: iload #9
    //   691: istore #8
    //   693: iload #9
    //   695: ifge -> 730
    //   698: new java/lang/StringBuilder
    //   701: dup
    //   702: invokespecial <init> : ()V
    //   705: astore_1
    //   706: aload_1
    //   707: ldc_w 'bad package count: '
    //   710: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   713: pop
    //   714: aload_1
    //   715: iload #9
    //   717: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   720: pop
    //   721: aload_0
    //   722: aload_1
    //   723: invokevirtual toString : ()Ljava/lang/String;
    //   726: putfield mReadError : Ljava/lang/String;
    //   729: return
    //   730: iload #8
    //   732: ifle -> 1583
    //   735: aload_0
    //   736: aload_1
    //   737: iload #4
    //   739: invokevirtual readCommonString : (Landroid/os/Parcel;I)Ljava/lang/String;
    //   742: astore #7
    //   744: aload #7
    //   746: ifnonnull -> 757
    //   749: aload_0
    //   750: ldc_w 'bad package name'
    //   753: putfield mReadError : Ljava/lang/String;
    //   756: return
    //   757: aload_1
    //   758: invokevirtual readInt : ()I
    //   761: istore #9
    //   763: iload #9
    //   765: istore #13
    //   767: iload #9
    //   769: ifge -> 804
    //   772: new java/lang/StringBuilder
    //   775: dup
    //   776: invokespecial <init> : ()V
    //   779: astore_1
    //   780: aload_1
    //   781: ldc_w 'bad uid count: '
    //   784: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   787: pop
    //   788: aload_1
    //   789: iload #9
    //   791: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   794: pop
    //   795: aload_0
    //   796: aload_1
    //   797: invokevirtual toString : ()Ljava/lang/String;
    //   800: putfield mReadError : Ljava/lang/String;
    //   803: return
    //   804: iload #13
    //   806: ifle -> 1577
    //   809: aload_1
    //   810: invokevirtual readInt : ()I
    //   813: istore #9
    //   815: iload #9
    //   817: ifge -> 852
    //   820: new java/lang/StringBuilder
    //   823: dup
    //   824: invokespecial <init> : ()V
    //   827: astore_1
    //   828: aload_1
    //   829: ldc_w 'bad uid: '
    //   832: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   835: pop
    //   836: aload_1
    //   837: iload #9
    //   839: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   842: pop
    //   843: aload_0
    //   844: aload_1
    //   845: invokevirtual toString : ()Ljava/lang/String;
    //   848: putfield mReadError : Ljava/lang/String;
    //   851: return
    //   852: aload_1
    //   853: invokevirtual readInt : ()I
    //   856: istore #14
    //   858: iload #14
    //   860: istore #15
    //   862: iload #14
    //   864: ifge -> 899
    //   867: new java/lang/StringBuilder
    //   870: dup
    //   871: invokespecial <init> : ()V
    //   874: astore_1
    //   875: aload_1
    //   876: ldc_w 'bad versions count: '
    //   879: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   882: pop
    //   883: aload_1
    //   884: iload #14
    //   886: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   889: pop
    //   890: aload_0
    //   891: aload_1
    //   892: invokevirtual toString : ()Ljava/lang/String;
    //   895: putfield mReadError : Ljava/lang/String;
    //   898: return
    //   899: iload #15
    //   901: ifle -> 1571
    //   904: aload_1
    //   905: invokevirtual readLong : ()J
    //   908: lstore #11
    //   910: iload #9
    //   912: istore #14
    //   914: new com/android/internal/app/procstats/ProcessStats$PackageState
    //   917: dup
    //   918: aload_0
    //   919: aload #7
    //   921: iload #9
    //   923: lload #11
    //   925: invokespecial <init> : (Lcom/android/internal/app/procstats/ProcessStats;Ljava/lang/String;IJ)V
    //   928: astore #16
    //   930: aload_0
    //   931: getfield mPackages : Lcom/android/internal/app/ProcessMap;
    //   934: aload #7
    //   936: iload #14
    //   938: invokevirtual get : (Ljava/lang/String;I)Ljava/lang/Object;
    //   941: checkcast android/util/LongSparseArray
    //   944: astore_2
    //   945: aload_2
    //   946: ifnonnull -> 973
    //   949: new android/util/LongSparseArray
    //   952: dup
    //   953: invokespecial <init> : ()V
    //   956: astore_2
    //   957: aload_0
    //   958: getfield mPackages : Lcom/android/internal/app/ProcessMap;
    //   961: aload #7
    //   963: iload #14
    //   965: aload_2
    //   966: invokevirtual put : (Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/Object;
    //   969: pop
    //   970: goto -> 973
    //   973: aload_2
    //   974: lload #11
    //   976: aload #16
    //   978: invokevirtual put : (JLjava/lang/Object;)V
    //   981: aload_1
    //   982: invokevirtual readInt : ()I
    //   985: istore #9
    //   987: iload #9
    //   989: ifge -> 1024
    //   992: new java/lang/StringBuilder
    //   995: dup
    //   996: invokespecial <init> : ()V
    //   999: astore_1
    //   1000: aload_1
    //   1001: ldc_w 'bad package process count: '
    //   1004: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1007: pop
    //   1008: aload_1
    //   1009: iload #9
    //   1011: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1014: pop
    //   1015: aload_0
    //   1016: aload_1
    //   1017: invokevirtual toString : ()Ljava/lang/String;
    //   1020: putfield mReadError : Ljava/lang/String;
    //   1023: return
    //   1024: aload_2
    //   1025: astore #10
    //   1027: iload #9
    //   1029: ifle -> 1226
    //   1032: iinc #9, -1
    //   1035: aload_0
    //   1036: aload_1
    //   1037: iload #4
    //   1039: invokevirtual readCommonString : (Landroid/os/Parcel;I)Ljava/lang/String;
    //   1042: astore #17
    //   1044: aload #17
    //   1046: ifnonnull -> 1057
    //   1049: aload_0
    //   1050: ldc_w 'bad package process name'
    //   1053: putfield mReadError : Ljava/lang/String;
    //   1056: return
    //   1057: aload_1
    //   1058: invokevirtual readInt : ()I
    //   1061: istore #18
    //   1063: aload_0
    //   1064: getfield mProcesses : Lcom/android/internal/app/ProcessMap;
    //   1067: aload #17
    //   1069: iload #14
    //   1071: invokevirtual get : (Ljava/lang/String;I)Ljava/lang/Object;
    //   1074: checkcast com/android/internal/app/procstats/ProcessState
    //   1077: astore #19
    //   1079: aload #19
    //   1081: ifnonnull -> 1116
    //   1084: new java/lang/StringBuilder
    //   1087: dup
    //   1088: invokespecial <init> : ()V
    //   1091: astore_1
    //   1092: aload_1
    //   1093: ldc_w 'no common proc: '
    //   1096: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1099: pop
    //   1100: aload_1
    //   1101: aload #17
    //   1103: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1106: pop
    //   1107: aload_0
    //   1108: aload_1
    //   1109: invokevirtual toString : ()Ljava/lang/String;
    //   1112: putfield mReadError : Ljava/lang/String;
    //   1115: return
    //   1116: iload #18
    //   1118: ifeq -> 1210
    //   1121: iload_3
    //   1122: ifeq -> 1142
    //   1125: aload #16
    //   1127: getfield mProcesses : Landroid/util/ArrayMap;
    //   1130: aload #17
    //   1132: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   1135: checkcast com/android/internal/app/procstats/ProcessState
    //   1138: astore_2
    //   1139: goto -> 1144
    //   1142: aconst_null
    //   1143: astore_2
    //   1144: aload_2
    //   1145: ifnull -> 1161
    //   1148: aload_2
    //   1149: aload_1
    //   1150: iconst_0
    //   1151: invokevirtual readFromParcel : (Landroid/os/Parcel;Z)Z
    //   1154: ifne -> 1158
    //   1157: return
    //   1158: goto -> 1195
    //   1161: new com/android/internal/app/procstats/ProcessState
    //   1164: dup
    //   1165: aload #19
    //   1167: aload #7
    //   1169: iload #14
    //   1171: lload #11
    //   1173: aload #17
    //   1175: lconst_0
    //   1176: invokespecial <init> : (Lcom/android/internal/app/procstats/ProcessState;Ljava/lang/String;IJLjava/lang/String;J)V
    //   1179: astore #19
    //   1181: aload #19
    //   1183: astore_2
    //   1184: aload #19
    //   1186: aload_1
    //   1187: iconst_1
    //   1188: invokevirtual readFromParcel : (Landroid/os/Parcel;Z)Z
    //   1191: ifne -> 1195
    //   1194: return
    //   1195: aload #16
    //   1197: getfield mProcesses : Landroid/util/ArrayMap;
    //   1200: aload #17
    //   1202: aload_2
    //   1203: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   1206: pop
    //   1207: goto -> 1223
    //   1210: aload #16
    //   1212: getfield mProcesses : Landroid/util/ArrayMap;
    //   1215: aload #17
    //   1217: aload #19
    //   1219: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   1222: pop
    //   1223: goto -> 1027
    //   1226: aload_1
    //   1227: invokevirtual readInt : ()I
    //   1230: istore #9
    //   1232: iload #9
    //   1234: ifge -> 1269
    //   1237: new java/lang/StringBuilder
    //   1240: dup
    //   1241: invokespecial <init> : ()V
    //   1244: astore_1
    //   1245: aload_1
    //   1246: ldc_w 'bad package service count: '
    //   1249: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1252: pop
    //   1253: aload_1
    //   1254: iload #9
    //   1256: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1259: pop
    //   1260: aload_0
    //   1261: aload_1
    //   1262: invokevirtual toString : ()Ljava/lang/String;
    //   1265: putfield mReadError : Ljava/lang/String;
    //   1268: return
    //   1269: aload #16
    //   1271: astore #10
    //   1273: iload #9
    //   1275: ifle -> 1392
    //   1278: iinc #9, -1
    //   1281: aload_1
    //   1282: invokevirtual readString : ()Ljava/lang/String;
    //   1285: astore #19
    //   1287: aload #19
    //   1289: ifnonnull -> 1300
    //   1292: aload_0
    //   1293: ldc_w 'bad package service name'
    //   1296: putfield mReadError : Ljava/lang/String;
    //   1299: return
    //   1300: iload #4
    //   1302: bipush #9
    //   1304: if_icmple -> 1319
    //   1307: aload_0
    //   1308: aload_1
    //   1309: iload #4
    //   1311: invokevirtual readCommonString : (Landroid/os/Parcel;I)Ljava/lang/String;
    //   1314: astore #16
    //   1316: goto -> 1322
    //   1319: aconst_null
    //   1320: astore #16
    //   1322: iload_3
    //   1323: ifeq -> 1343
    //   1326: aload #10
    //   1328: getfield mServices : Landroid/util/ArrayMap;
    //   1331: aload #19
    //   1333: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   1336: checkcast com/android/internal/app/procstats/ServiceState
    //   1339: astore_2
    //   1340: goto -> 1345
    //   1343: aconst_null
    //   1344: astore_2
    //   1345: aload_2
    //   1346: ifnonnull -> 1368
    //   1349: new com/android/internal/app/procstats/ServiceState
    //   1352: dup
    //   1353: aload_0
    //   1354: aload #7
    //   1356: aload #19
    //   1358: aload #16
    //   1360: aconst_null
    //   1361: invokespecial <init> : (Lcom/android/internal/app/procstats/ProcessStats;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/app/procstats/ProcessState;)V
    //   1364: astore_2
    //   1365: goto -> 1368
    //   1368: aload_2
    //   1369: aload_1
    //   1370: invokevirtual readFromParcel : (Landroid/os/Parcel;)Z
    //   1373: ifne -> 1377
    //   1376: return
    //   1377: aload #10
    //   1379: getfield mServices : Landroid/util/ArrayMap;
    //   1382: aload #19
    //   1384: aload_2
    //   1385: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   1388: pop
    //   1389: goto -> 1273
    //   1392: aload_1
    //   1393: invokevirtual readInt : ()I
    //   1396: istore #20
    //   1398: iload #20
    //   1400: istore #18
    //   1402: iload #14
    //   1404: istore #9
    //   1406: iload #20
    //   1408: ifge -> 1443
    //   1411: new java/lang/StringBuilder
    //   1414: dup
    //   1415: invokespecial <init> : ()V
    //   1418: astore_1
    //   1419: aload_1
    //   1420: ldc_w 'bad package association count: '
    //   1423: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1426: pop
    //   1427: aload_1
    //   1428: iload #20
    //   1430: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1433: pop
    //   1434: aload_0
    //   1435: aload_1
    //   1436: invokevirtual toString : ()Ljava/lang/String;
    //   1439: putfield mReadError : Ljava/lang/String;
    //   1442: return
    //   1443: iload #18
    //   1445: ifle -> 1565
    //   1448: aload_0
    //   1449: aload_1
    //   1450: iload #4
    //   1452: invokevirtual readCommonString : (Landroid/os/Parcel;I)Ljava/lang/String;
    //   1455: astore #16
    //   1457: aload #16
    //   1459: ifnonnull -> 1470
    //   1462: aload_0
    //   1463: ldc_w 'bad package association name'
    //   1466: putfield mReadError : Ljava/lang/String;
    //   1469: return
    //   1470: aload_0
    //   1471: aload_1
    //   1472: iload #4
    //   1474: invokevirtual readCommonString : (Landroid/os/Parcel;I)Ljava/lang/String;
    //   1477: astore #19
    //   1479: iload_3
    //   1480: ifeq -> 1500
    //   1483: aload #10
    //   1485: getfield mAssociations : Landroid/util/ArrayMap;
    //   1488: aload #16
    //   1490: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   1493: checkcast com/android/internal/app/procstats/AssociationState
    //   1496: astore_2
    //   1497: goto -> 1502
    //   1500: aconst_null
    //   1501: astore_2
    //   1502: aload_2
    //   1503: ifnonnull -> 1525
    //   1506: new com/android/internal/app/procstats/AssociationState
    //   1509: dup
    //   1510: aload_0
    //   1511: aload #10
    //   1513: aload #16
    //   1515: aload #19
    //   1517: aconst_null
    //   1518: invokespecial <init> : (Lcom/android/internal/app/procstats/ProcessStats;Lcom/android/internal/app/procstats/ProcessStats$PackageState;Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/app/procstats/ProcessState;)V
    //   1521: astore_2
    //   1522: goto -> 1525
    //   1525: aload_2
    //   1526: aload_0
    //   1527: aload_1
    //   1528: iload #4
    //   1530: invokevirtual readFromParcel : (Lcom/android/internal/app/procstats/ProcessStats;Landroid/os/Parcel;I)Ljava/lang/String;
    //   1533: astore #19
    //   1535: aload #19
    //   1537: ifnull -> 1547
    //   1540: aload_0
    //   1541: aload #19
    //   1543: putfield mReadError : Ljava/lang/String;
    //   1546: return
    //   1547: aload #10
    //   1549: getfield mAssociations : Landroid/util/ArrayMap;
    //   1552: aload #16
    //   1554: aload_2
    //   1555: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   1558: pop
    //   1559: iinc #18, -1
    //   1562: goto -> 1443
    //   1565: iinc #15, -1
    //   1568: goto -> 899
    //   1571: iinc #13, -1
    //   1574: goto -> 804
    //   1577: iinc #8, -1
    //   1580: goto -> 730
    //   1583: aload_1
    //   1584: invokevirtual readInt : ()I
    //   1587: istore #9
    //   1589: aload_0
    //   1590: getfield mPageTypeNodes : Ljava/util/ArrayList;
    //   1593: invokevirtual clear : ()V
    //   1596: aload_0
    //   1597: getfield mPageTypeNodes : Ljava/util/ArrayList;
    //   1600: iload #9
    //   1602: invokevirtual ensureCapacity : (I)V
    //   1605: aload_0
    //   1606: getfield mPageTypeZones : Ljava/util/ArrayList;
    //   1609: invokevirtual clear : ()V
    //   1612: aload_0
    //   1613: getfield mPageTypeZones : Ljava/util/ArrayList;
    //   1616: iload #9
    //   1618: invokevirtual ensureCapacity : (I)V
    //   1621: aload_0
    //   1622: getfield mPageTypeLabels : Ljava/util/ArrayList;
    //   1625: invokevirtual clear : ()V
    //   1628: aload_0
    //   1629: getfield mPageTypeLabels : Ljava/util/ArrayList;
    //   1632: iload #9
    //   1634: invokevirtual ensureCapacity : (I)V
    //   1637: aload_0
    //   1638: getfield mPageTypeSizes : Ljava/util/ArrayList;
    //   1641: invokevirtual clear : ()V
    //   1644: aload_0
    //   1645: getfield mPageTypeSizes : Ljava/util/ArrayList;
    //   1648: iload #9
    //   1650: invokevirtual ensureCapacity : (I)V
    //   1653: iconst_0
    //   1654: istore #6
    //   1656: iload #6
    //   1658: iload #9
    //   1660: if_icmpge -> 1720
    //   1663: aload_0
    //   1664: getfield mPageTypeNodes : Ljava/util/ArrayList;
    //   1667: aload_1
    //   1668: invokevirtual readInt : ()I
    //   1671: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1674: invokevirtual add : (Ljava/lang/Object;)Z
    //   1677: pop
    //   1678: aload_0
    //   1679: getfield mPageTypeZones : Ljava/util/ArrayList;
    //   1682: aload_1
    //   1683: invokevirtual readString : ()Ljava/lang/String;
    //   1686: invokevirtual add : (Ljava/lang/Object;)Z
    //   1689: pop
    //   1690: aload_0
    //   1691: getfield mPageTypeLabels : Ljava/util/ArrayList;
    //   1694: aload_1
    //   1695: invokevirtual readString : ()Ljava/lang/String;
    //   1698: invokevirtual add : (Ljava/lang/Object;)Z
    //   1701: pop
    //   1702: aload_0
    //   1703: getfield mPageTypeSizes : Ljava/util/ArrayList;
    //   1706: aload_1
    //   1707: invokevirtual createIntArray : ()[I
    //   1710: invokevirtual add : (Ljava/lang/Object;)Z
    //   1713: pop
    //   1714: iinc #6, 1
    //   1717: goto -> 1656
    //   1720: aload_0
    //   1721: aconst_null
    //   1722: putfield mIndexToCommonString : Ljava/util/ArrayList;
    //   1725: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1026	-> 0
    //   #1027	-> 18
    //   #1028	-> 38
    //   #1029	-> 42
    //   #1032	-> 46
    //   #1033	-> 59
    //   #1035	-> 60
    //   #1036	-> 66
    //   #1037	-> 73
    //   #1038	-> 104
    //   #1040	-> 105
    //   #1041	-> 118
    //   #1043	-> 119
    //   #1044	-> 132
    //   #1046	-> 133
    //   #1047	-> 146
    //   #1049	-> 147
    //   #1050	-> 160
    //   #1052	-> 161
    //   #1053	-> 175
    //   #1056	-> 176
    //   #1058	-> 187
    //   #1059	-> 195
    //   #1060	-> 203
    //   #1061	-> 207
    //   #1062	-> 215
    //   #1063	-> 223
    //   #1064	-> 231
    //   #1065	-> 239
    //   #1066	-> 247
    //   #1067	-> 255
    //   #1068	-> 263
    //   #1069	-> 271
    //   #1070	-> 279
    //   #1071	-> 287
    //   #1072	-> 295
    //   #1073	-> 303
    //   #1074	-> 311
    //   #1075	-> 319
    //   #1076	-> 327
    //   #1077	-> 349
    //   #1078	-> 357
    //   #1079	-> 365
    //   #1080	-> 380
    //   #1081	-> 391
    //   #1084	-> 392
    //   #1085	-> 398
    //   #1086	-> 403
    //   #1087	-> 434
    //   #1085	-> 435
    //   #1089	-> 435
    //   #1090	-> 440
    //   #1091	-> 440
    //   #1092	-> 449
    //   #1093	-> 454
    //   #1094	-> 461
    //   #1096	-> 462
    //   #1097	-> 468
    //   #1098	-> 477
    //   #1099	-> 508
    //   #1101	-> 509
    //   #1102	-> 514
    //   #1103	-> 514
    //   #1104	-> 520
    //   #1105	-> 525
    //   #1106	-> 556
    //   #1108	-> 557
    //   #1109	-> 566
    //   #1110	-> 571
    //   #1111	-> 578
    //   #1113	-> 579
    //   #1114	-> 585
    //   #1115	-> 609
    //   #1116	-> 613
    //   #1117	-> 622
    //   #1116	-> 623
    //   #1120	-> 626
    //   #1121	-> 644
    //   #1122	-> 657
    //   #1127	-> 658
    //   #1128	-> 671
    //   #1101	-> 677
    //   #1129	-> 677
    //   #1133	-> 683
    //   #1134	-> 689
    //   #1135	-> 698
    //   #1136	-> 729
    //   #1138	-> 730
    //   #1139	-> 735
    //   #1140	-> 735
    //   #1141	-> 744
    //   #1142	-> 749
    //   #1143	-> 756
    //   #1145	-> 757
    //   #1146	-> 763
    //   #1147	-> 772
    //   #1148	-> 803
    //   #1150	-> 804
    //   #1151	-> 809
    //   #1152	-> 809
    //   #1153	-> 815
    //   #1154	-> 820
    //   #1155	-> 851
    //   #1157	-> 852
    //   #1158	-> 858
    //   #1159	-> 867
    //   #1160	-> 898
    //   #1162	-> 899
    //   #1163	-> 904
    //   #1164	-> 904
    //   #1165	-> 910
    //   #1166	-> 930
    //   #1167	-> 945
    //   #1168	-> 949
    //   #1169	-> 957
    //   #1167	-> 973
    //   #1171	-> 973
    //   #1172	-> 981
    //   #1173	-> 987
    //   #1174	-> 992
    //   #1175	-> 1023
    //   #1173	-> 1024
    //   #1177	-> 1027
    //   #1178	-> 1032
    //   #1179	-> 1035
    //   #1180	-> 1044
    //   #1181	-> 1049
    //   #1182	-> 1056
    //   #1184	-> 1057
    //   #1187	-> 1063
    //   #1190	-> 1079
    //   #1191	-> 1084
    //   #1192	-> 1115
    //   #1194	-> 1116
    //   #1199	-> 1121
    //   #1200	-> 1144
    //   #1201	-> 1148
    //   #1202	-> 1157
    //   #1201	-> 1158
    //   #1205	-> 1161
    //   #1207	-> 1181
    //   #1208	-> 1194
    //   #1213	-> 1195
    //   #1214	-> 1207
    //   #1217	-> 1210
    //   #1219	-> 1223
    //   #1220	-> 1226
    //   #1221	-> 1232
    //   #1222	-> 1237
    //   #1223	-> 1268
    //   #1221	-> 1269
    //   #1225	-> 1273
    //   #1226	-> 1278
    //   #1227	-> 1281
    //   #1228	-> 1287
    //   #1229	-> 1292
    //   #1230	-> 1299
    //   #1232	-> 1300
    //   #1233	-> 1322
    //   #1234	-> 1345
    //   #1235	-> 1349
    //   #1234	-> 1368
    //   #1237	-> 1368
    //   #1238	-> 1376
    //   #1242	-> 1377
    //   #1243	-> 1389
    //   #1244	-> 1392
    //   #1245	-> 1398
    //   #1246	-> 1411
    //   #1247	-> 1442
    //   #1249	-> 1443
    //   #1250	-> 1448
    //   #1251	-> 1448
    //   #1252	-> 1457
    //   #1253	-> 1462
    //   #1254	-> 1469
    //   #1256	-> 1470
    //   #1257	-> 1479
    //   #1258	-> 1483
    //   #1259	-> 1502
    //   #1260	-> 1506
    //   #1259	-> 1525
    //   #1263	-> 1525
    //   #1264	-> 1535
    //   #1265	-> 1540
    //   #1266	-> 1546
    //   #1270	-> 1547
    //   #1271	-> 1559
    //   #1249	-> 1565
    //   #1272	-> 1565
    //   #1162	-> 1571
    //   #1273	-> 1571
    //   #1150	-> 1577
    //   #1274	-> 1577
    //   #1277	-> 1583
    //   #1278	-> 1589
    //   #1279	-> 1596
    //   #1280	-> 1605
    //   #1281	-> 1612
    //   #1282	-> 1621
    //   #1283	-> 1628
    //   #1284	-> 1637
    //   #1285	-> 1644
    //   #1286	-> 1653
    //   #1287	-> 1663
    //   #1288	-> 1678
    //   #1289	-> 1690
    //   #1290	-> 1702
    //   #1286	-> 1714
    //   #1293	-> 1720
    //   #1296	-> 1725
  }
  
  public PackageState getPackageStateLocked(String paramString, int paramInt, long paramLong) {
    LongSparseArray<PackageState> longSparseArray1 = this.mPackages.get(paramString, paramInt);
    LongSparseArray<PackageState> longSparseArray2 = longSparseArray1;
    if (longSparseArray1 == null) {
      longSparseArray2 = new LongSparseArray();
      this.mPackages.put(paramString, paramInt, longSparseArray2);
    } 
    PackageState packageState2 = longSparseArray2.get(paramLong);
    if (packageState2 != null)
      return packageState2; 
    PackageState packageState1 = new PackageState(this, paramString, paramInt, paramLong);
    longSparseArray2.put(paramLong, packageState1);
    return packageState1;
  }
  
  public ProcessState getProcessStateLocked(String paramString1, int paramInt, long paramLong, String paramString2) {
    return getProcessStateLocked(getPackageStateLocked(paramString1, paramInt, paramLong), paramString2);
  }
  
  public ProcessState getProcessStateLocked(PackageState paramPackageState, String paramString) {
    ProcessState processState = paramPackageState.mProcesses.get(paramString);
    if (processState != null)
      return processState; 
    processState = this.mProcesses.get(paramString, paramPackageState.mUid);
    if (processState == null) {
      processState = new ProcessState(this, paramPackageState.mPackageName, paramPackageState.mUid, paramPackageState.mVersionCode, paramString);
      this.mProcesses.put(paramString, paramPackageState.mUid, processState);
    } 
    if (!processState.isMultiPackage()) {
      if (paramPackageState.mPackageName.equals(processState.getPackage())) {
        long l = paramPackageState.mVersionCode;
        if (l == processState.getVersion()) {
          paramPackageState.mProcesses.put(paramString, processState);
          return processState;
        } 
      } 
      processState.setMultiPackage(true);
      long l2 = SystemClock.uptimeMillis();
      String str = processState.getPackage();
      int i = paramPackageState.mUid;
      long l1 = processState.getVersion();
      PackageState packageState = getPackageStateLocked(str, i, l1);
      if (packageState != null) {
        ProcessState processState1 = processState.clone(l2);
        packageState.mProcesses.put(processState.getName(), processState1);
        for (i = packageState.mServices.size() - 1; i >= 0; i--) {
          ServiceState serviceState = packageState.mServices.valueAt(i);
          if (serviceState.getProcess() == processState)
            serviceState.setProcess(processState1); 
        } 
        for (i = packageState.mAssociations.size() - 1; i >= 0; i--) {
          AssociationState associationState = packageState.mAssociations.valueAt(i);
          if (associationState.getProcess() == processState)
            associationState.setProcess(processState1); 
        } 
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Cloning proc state: no package state ");
        stringBuilder.append(processState.getPackage());
        stringBuilder.append("/");
        stringBuilder.append(paramPackageState.mUid);
        stringBuilder.append(" for proc ");
        stringBuilder.append(processState.getName());
        String str1 = stringBuilder.toString();
        Slog.w("ProcessStats", str1);
      } 
      processState = new ProcessState(processState, paramPackageState.mPackageName, paramPackageState.mUid, paramPackageState.mVersionCode, paramString, l2);
    } else {
      String str = paramPackageState.mPackageName;
      int i = paramPackageState.mUid;
      long l = paramPackageState.mVersionCode;
      processState = new ProcessState(processState, str, i, l, paramString, SystemClock.uptimeMillis());
    } 
    paramPackageState.mProcesses.put(paramString, processState);
    return processState;
  }
  
  public ServiceState getServiceStateLocked(String paramString1, int paramInt, long paramLong, String paramString2, String paramString3) {
    PackageState packageState = getPackageStateLocked(paramString1, paramInt, paramLong);
    ServiceState serviceState2 = packageState.mServices.get(paramString3);
    if (serviceState2 != null)
      return serviceState2; 
    if (paramString2 != null) {
      ProcessState processState = getProcessStateLocked(paramString1, paramInt, paramLong, paramString2);
    } else {
      serviceState2 = null;
    } 
    ServiceState serviceState1 = new ServiceState(this, paramString1, paramString3, paramString2, (ProcessState)serviceState2);
    packageState.mServices.put(paramString3, serviceState1);
    return serviceState1;
  }
  
  public AssociationState getAssociationStateLocked(String paramString1, int paramInt, long paramLong, String paramString2, String paramString3) {
    PackageState packageState = getPackageStateLocked(paramString1, paramInt, paramLong);
    AssociationState associationState2 = packageState.mAssociations.get(paramString3);
    if (associationState2 != null)
      return associationState2; 
    if (paramString2 != null) {
      ProcessState processState = getProcessStateLocked(paramString1, paramInt, paramLong, paramString2);
    } else {
      paramString1 = null;
    } 
    AssociationState associationState1 = new AssociationState(this, packageState, paramString3, paramString2, (ProcessState)paramString1);
    packageState.mAssociations.put(paramString3, associationState1);
    return associationState1;
  }
  
  public void updateTrackingAssociationsLocked(int paramInt, long paramLong) {
    int i = this.mTrackingAssociations.size();
    for (; --i >= 0; i--) {
      AssociationState.SourceState sourceState = this.mTrackingAssociations.get(i);
      if (sourceState.mProcStateSeq != paramInt || sourceState.mProcState >= 9) {
        sourceState.stopActive(paramLong);
        sourceState.mInTrackingList = false;
        sourceState.mProcState = -1;
        this.mTrackingAssociations.remove(i);
      } else {
        ProcessState processState = sourceState.getAssociationState().getProcess();
        if (processState != null) {
          int j = processState.getCombinedState() % 14;
          if (sourceState.mProcState == j) {
            sourceState.startActive(paramLong);
          } else {
            sourceState.stopActive(paramLong);
            if (sourceState.mProcState < j) {
              long l = SystemClock.uptimeMillis();
              if (this.mNextInverseProcStateWarningUptime > l) {
                this.mSkippedInverseProcStateWarningCount++;
              } else {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Tracking association ");
                stringBuilder.append(sourceState);
                stringBuilder.append(" whose proc state ");
                stringBuilder.append(sourceState.mProcState);
                stringBuilder.append(" is better than process ");
                stringBuilder.append(processState);
                stringBuilder.append(" proc state ");
                stringBuilder.append(j);
                stringBuilder.append(" (");
                stringBuilder.append(this.mSkippedInverseProcStateWarningCount);
                stringBuilder.append(" skipped)");
                Slog.w("ProcessStats", stringBuilder.toString());
                this.mSkippedInverseProcStateWarningCount = 0;
                this.mNextInverseProcStateWarningUptime = 10000L + l;
              } 
            } 
          } 
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Tracking association without process: ");
          stringBuilder.append(sourceState);
          stringBuilder.append(" in ");
          stringBuilder.append(sourceState.getAssociationState());
          String str = stringBuilder.toString();
          Slog.wtf("ProcessStats", str);
        } 
      } 
    } 
  }
  
  class AssociationDumpContainer {
    long mActiveTime;
    
    ArrayList<Pair<AssociationState.SourceKey, AssociationState.SourceDumpContainer>> mSources;
    
    final AssociationState mState;
    
    long mTotalTime;
    
    final ProcessStats this$0;
    
    AssociationDumpContainer(AssociationState param1AssociationState) {
      this.mState = param1AssociationState;
    }
  }
  
  public void dumpLocked(PrintWriter paramPrintWriter, String paramString, long paramLong, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, int paramInt) {
    long l2;
    String str8;
    boolean bool = paramBoolean3;
    long l1 = DumpUtils.dumpSingleTime(null, null, this.mMemFactorDurations, this.mMemFactor, this.mStartTime, paramLong);
    paramPrintWriter.print("          Start time: ");
    paramPrintWriter.print(DateFormat.format("yyyy-MM-dd HH:mm:ss", this.mTimePeriodStartClock));
    paramPrintWriter.println();
    paramPrintWriter.print("        Total uptime: ");
    if (this.mRunning) {
      l2 = SystemClock.uptimeMillis();
    } else {
      l2 = this.mTimePeriodEndUptime;
    } 
    long l3 = this.mTimePeriodStartUptime;
    TimeUtils.formatDuration(l2 - l3, paramPrintWriter);
    paramPrintWriter.println();
    paramPrintWriter.print("  Total elapsed time: ");
    if (this.mRunning) {
      l2 = SystemClock.elapsedRealtime();
    } else {
      l2 = this.mTimePeriodEndRealtime;
    } 
    l3 = this.mTimePeriodStartRealtime;
    TimeUtils.formatDuration(l2 - l3, paramPrintWriter);
    int i = 1;
    if ((this.mFlags & 0x2) != 0) {
      paramPrintWriter.print(" (shutdown)");
      i = 0;
    } 
    if ((this.mFlags & 0x4) != 0) {
      paramPrintWriter.print(" (sysprops)");
      i = 0;
    } 
    if ((this.mFlags & 0x1) != 0) {
      paramPrintWriter.print(" (complete)");
      i = 0;
    } 
    if (i)
      paramPrintWriter.print(" (partial)"); 
    if (this.mHasSwappedOutPss)
      paramPrintWriter.print(" (swapped-out-pss)"); 
    paramPrintWriter.print(' ');
    paramPrintWriter.print(this.mRuntime);
    paramPrintWriter.println();
    paramPrintWriter.print("     Aggregated over: ");
    paramPrintWriter.println(this.mNumAggregated);
    if (this.mSysMemUsage.getKeyCount() > 0) {
      paramPrintWriter.println();
      paramPrintWriter.println("System memory usage:");
      this.mSysMemUsage.dump(paramPrintWriter, "  ", ALL_SCREEN_ADJ, ALL_MEM_ADJ);
    } 
    String str1 = "      (Not active: ", str2 = "        ", str3 = " entries)", str4 = " / ", str5 = "  * ", str6 = ")";
    int j = 0;
    String str7 = ":";
    if ((paramInt & 0xE) != 0) {
      String str11;
      ProcessMap<LongSparseArray<PackageState>> processMap = this.mPackages;
      ArrayMap<String, SparseArray<LongSparseArray<PackageState>>> arrayMap3 = processMap.getMap();
      ArrayMap<String, SparseArray<LongSparseArray<PackageState>>> arrayMap1;
      for (i = 0; i < arrayMap3.size(); i = m + 1, arrayMap3 = arrayMap1, str5 = str19, str4 = str18, str3 = str1, arrayMap1 = arrayMap, str1 = str17) {
        String str15 = arrayMap3.keyAt(i);
        SparseArray sparseArray = (SparseArray)arrayMap3.valueAt(i);
        SparseArray<LongSparseArray> sparseArray2 = sparseArray;
        SparseArray<LongSparseArray> sparseArray1;
        String str16;
        ArrayMap<String, SparseArray<LongSparseArray<PackageState>>> arrayMap;
        int k, m;
        String str17, str18, str19;
        for (k = 0, m = i, str17 = str1, str16 = str2, str1 = str3, str18 = str4, str19 = str5, arrayMap1 = arrayMap3, i = k, str5 = str15, sparseArray1 = sparseArray2; i < sparseArray1.size(); arrayMap1 = arrayMap3, i = k + 1, arrayMap = arrayMap4, str17 = str4) {
          ArrayMap<String, SparseArray<LongSparseArray<PackageState>>> arrayMap4;
          int n = sparseArray1.keyAt(i);
          LongSparseArray<PackageState> longSparseArray1 = sparseArray1.valueAt(i);
          ArrayMap<String, SparseArray<LongSparseArray<PackageState>>> arrayMap5;
          LongSparseArray<PackageState> longSparseArray2;
          int i1;
          for (i1 = 0, k = i, arrayMap3 = arrayMap1, str4 = str17, str3 = str16, i = n, longSparseArray2 = longSparseArray1; i1 < longSparseArray2.size(); str16 = str6, arrayMap6 = arrayMap1, str = str7, str4 = str3, i1++, arrayMap5 = arrayMap6, str11 = str16, str3 = str) {
            ArrayMap<String, SparseArray<LongSparseArray<PackageState>>> arrayMap6;
            String str20, str21;
            ArrayMap<String, SparseArray<LongSparseArray<PackageState>>> arrayMap7, arrayMap8;
            String str22;
            boolean bool1;
            l2 = longSparseArray2.keyAt(i1);
            PackageState packageState = longSparseArray2.valueAt(i1);
            int i2 = packageState.mProcesses.size();
            int i3 = packageState.mServices.size();
            int i4 = packageState.mAssociations.size();
            if (paramString == null || paramString.equals(str5)) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            boolean bool2 = false, bool3 = false;
            int i5 = 0, i6 = 0;
            if (!bool1) {
              for (n = 0; n < i2; n++) {
                ProcessState processState = packageState.mProcesses.valueAt(n);
                if (paramString.equals(processState.getName())) {
                  i6 = 1;
                  break;
                } 
              } 
              n = i1;
              i5 = i6;
              i1 = n;
              if (!i6) {
                boolean bool4;
                i1 = 0;
                while (true) {
                  bool4 = bool3;
                  if (i1 < i4) {
                    AssociationState associationState = packageState.mAssociations.valueAt(i1);
                    if (associationState.hasProcessOrPackage(paramString)) {
                      bool4 = true;
                      break;
                    } 
                    i1++;
                    continue;
                  } 
                  break;
                } 
                bool2 = bool4;
                i5 = i6;
                i1 = n;
                if (!bool4) {
                  bool = paramBoolean3;
                  str16 = str3;
                  str3 = str4;
                  arrayMap6 = arrayMap5;
                  i1 = n;
                  str21 = str16;
                  continue;
                } 
              } 
            } 
            if (i2 > 0 || i3 > 0 || i4 > 0) {
              n = j;
              if (!j) {
                paramPrintWriter.println();
                paramPrintWriter.println("Per-Package Stats:");
                n = 1;
              } 
              paramPrintWriter.print(str19);
              paramPrintWriter.print(str5);
              paramPrintWriter.print(str18);
              UserHandle.formatUid(paramPrintWriter, i);
              paramPrintWriter.print(" / v");
              paramPrintWriter.print(l2);
              paramPrintWriter.println(str21);
              j = n;
            } 
            if ((paramInt & 0x2) != 0 && !bool2) {
              if (!paramBoolean1 || paramBoolean3) {
                for (i6 = 0, n = i2; i6 < n; i6++) {
                  ProcessState processState = ((PackageState)arrayMap6).mProcesses.valueAt(i6);
                  if (bool1 || paramString.equals(processState.getName()))
                    if (paramBoolean4 && !processState.isInUse()) {
                      paramPrintWriter.print(str4);
                      paramPrintWriter.print(((PackageState)arrayMap6).mProcesses.keyAt(i6));
                      paramPrintWriter.println(str11);
                    } else {
                      paramPrintWriter.print("      Process ");
                      paramPrintWriter.print(((PackageState)arrayMap6).mProcesses.keyAt(i6));
                      if (processState.getCommonProcess().isMultiPackage()) {
                        paramPrintWriter.print(" (multi, ");
                      } else {
                        paramPrintWriter.print(" (unique, ");
                      } 
                      paramPrintWriter.print(processState.getDurationsBucketCount());
                      paramPrintWriter.print(str1);
                      paramPrintWriter.println(str21);
                      processState.dumpProcessState(paramPrintWriter, "        ", ALL_SCREEN_ADJ, ALL_MEM_ADJ, ALL_PROC_STATES, paramLong);
                      processState.dumpPss(paramPrintWriter, "        ", ALL_SCREEN_ADJ, ALL_MEM_ADJ, ALL_PROC_STATES, paramLong);
                      processState.dumpInternalLocked(paramPrintWriter, str3, paramBoolean3);
                    }  
                } 
                arrayMap8 = arrayMap6;
                str20 = str21;
                arrayMap7 = arrayMap8;
              } else {
                ArrayList<ProcessState> arrayList = new ArrayList();
                for (n = 0; n < i2; n++) {
                  ProcessState processState = ((PackageState)str20).mProcesses.valueAt(n);
                  if (bool1 || paramString.equals(processState.getName()))
                    if (!paramBoolean4 || processState.isInUse())
                      arrayList.add(processState);  
                } 
                int[] arrayOfInt1 = ALL_SCREEN_ADJ, arrayOfInt2 = ALL_MEM_ADJ, arrayOfInt3 = NON_CACHED_PROC_STATES;
                DumpUtils.dumpProcessSummaryLocked(paramPrintWriter, "      ", "Prc ", arrayList, arrayOfInt1, arrayOfInt2, arrayOfInt3, paramLong, l1);
                str22 = str20;
                arrayMap6 = arrayMap7;
                str21 = str22;
              } 
            } else {
              arrayMap8 = arrayMap6;
              str20 = str21;
              arrayMap7 = arrayMap8;
            } 
            n = i3;
            bool = paramBoolean3;
            str16 = str4;
            str4 = str3;
            str3 = "        Process: ";
            if ((paramInt & 0x4) != 0 && !bool2) {
              for (i6 = 0; i6 < n; i6++) {
                ServiceState serviceState = ((PackageState)arrayMap7).mServices.valueAt(i6);
                if (bool1 || paramString.equals(serviceState.getProcessName()))
                  if (paramBoolean4 && !serviceState.isInUse()) {
                    paramPrintWriter.print("      (Not active service: ");
                    paramPrintWriter.print(((PackageState)arrayMap7).mServices.keyAt(i6));
                    paramPrintWriter.println(str11);
                  } else {
                    if (bool) {
                      paramPrintWriter.print("      Service ");
                    } else {
                      paramPrintWriter.print("      * Svc ");
                    } 
                    paramPrintWriter.print(((PackageState)arrayMap7).mServices.keyAt(i6));
                    paramPrintWriter.println(str20);
                    paramPrintWriter.print(str3);
                    paramPrintWriter.println(serviceState.getProcessName());
                    serviceState.dumpStats(paramPrintWriter, "        ", "          ", "    ", paramLong, l1, paramBoolean1, paramBoolean3);
                  }  
              } 
              String str24 = str4, str23 = str16;
              arrayMap8 = arrayMap7;
              str4 = str3;
              str12 = str24;
              str3 = str23;
            } else {
              arrayMap4 = arrayMap8;
              String str = "        Process: ";
              str22 = str12;
              str12 = str4;
              str4 = str;
            } 
            if ((paramInt & 0x8) != 0) {
              n = i4;
              ArrayList<AssociationDumpContainer> arrayList = new ArrayList(n);
              for (i6 = 0; i6 < n; i6++) {
                AssociationState associationState = ((PackageState)str22).mAssociations.valueAt(i6);
                if (bool1 || paramString.equals(associationState.getProcessName()) || (
                  bool2 && associationState.hasProcessOrPackage(paramString))) {
                  AssociationDumpContainer associationDumpContainer = new AssociationDumpContainer(associationState);
                  associationDumpContainer.mSources = associationState.createSortedAssociations(paramLong, l1);
                  associationDumpContainer.mTotalTime = associationState.getTotalDuration(paramLong);
                  associationDumpContainer.mActiveTime = associationState.getActiveDuration(paramLong);
                  arrayList.add(associationDumpContainer);
                } 
              } 
              Collections.sort(arrayList, ASSOCIATION_COMPARATOR);
              i6 = arrayList.size();
              for (byte b = 0; b < i6; b++) {
                AssociationDumpContainer associationDumpContainer = arrayList.get(b);
                AssociationState associationState = associationDumpContainer.mState;
                if (paramBoolean4 && !associationState.isInUse()) {
                  paramPrintWriter.print("      (Not active association: ");
                  paramPrintWriter.print(((PackageState)str22).mAssociations.keyAt(b));
                  paramPrintWriter.println(str11);
                } else {
                  if (bool) {
                    paramPrintWriter.print("      Association ");
                  } else {
                    paramPrintWriter.print("      * Asc ");
                  } 
                  paramPrintWriter.print(associationDumpContainer.mState.getName());
                  paramPrintWriter.println(str20);
                  paramPrintWriter.print(str4);
                  paramPrintWriter.println(associationState.getProcessName());
                  ArrayList<Pair<AssociationState.SourceKey, AssociationState.SourceDumpContainer>> arrayList1 = associationDumpContainer.mSources;
                  if (bool2 && !bool1 && i5 == 0 && !associationState.getProcessName().equals(paramString)) {
                    String str = paramString;
                  } else {
                    associationDumpContainer = null;
                  } 
                  associationState.dumpStats(paramPrintWriter, "        ", "          ", "    ", arrayList1, paramLong, l1, (String)associationDumpContainer, paramBoolean2, paramBoolean3);
                } 
              } 
            } 
            continue;
          } 
        } 
      } 
      String str14 = str5;
      str8 = str4;
      str4 = str3;
      ArrayMap<String, SparseArray<LongSparseArray<PackageState>>> arrayMap2 = arrayMap1;
      String str13 = str1;
      str5 = str12;
      String str12 = str11;
      ProcessStats processStats1 = this;
      String str9 = str14, str10 = str8;
      str8 = str13;
    } else {
      str5 = ":";
      str7 = ")";
      str1 = "  * ";
      str2 = " / ";
      str4 = " entries)";
      str3 = "        ";
      str8 = "      (Not active: ";
      ProcessStats processStats1 = this;
    } 
    ProcessStats processStats = this;
    if ((paramInt & 0x1) != 0) {
      ArrayMap<String, SparseArray<ProcessState>> arrayMap = processStats.mProcesses.getMap();
      byte b3 = 0, b1 = 0;
      String str;
      byte b2;
      for (paramInt = 0, b2 = 0, str = str1; b2 < arrayMap.size(); b2++, str = str2, str2 = str3, str3 = str8, str9 = str6, str8 = str1) {
        String str9, str10 = arrayMap.keyAt(b2);
        SparseArray<ProcessState> sparseArray2 = arrayMap.valueAt(b2);
        SparseArray<ProcessState> sparseArray1;
        for (j = 0, str1 = str8, str6 = str4, str8 = str3, str3 = str2, str2 = str, sparseArray1 = sparseArray2, str = str10; j < sparseArray1.size(); j++, b1++) {
          int k = sparseArray1.keyAt(j);
          ProcessState processState = sparseArray1.valueAt(j);
          if (processState.hasAnyData())
            if (processState.isMultiPackage())
              if (paramString == null || paramString.equals(str) || 
                paramString.equals(processState.getPackage())) {
                paramPrintWriter.println();
                i = paramInt;
                if (paramInt == 0) {
                  paramPrintWriter.println("Multi-Package Common Processes:");
                  i = 1;
                } 
                if (paramBoolean4 && !processState.isInUse()) {
                  paramPrintWriter.print(str1);
                  paramPrintWriter.print(str);
                  paramPrintWriter.println(str7);
                } else {
                  paramPrintWriter.print(str2);
                  paramPrintWriter.print(str);
                  paramPrintWriter.print(str3);
                  UserHandle.formatUid(paramPrintWriter, k);
                  paramPrintWriter.print(" (");
                  paramPrintWriter.print(processState.getDurationsBucketCount());
                  paramPrintWriter.print(str6);
                  paramPrintWriter.println(str5);
                  processState.dumpProcessState(paramPrintWriter, "        ", ALL_SCREEN_ADJ, ALL_MEM_ADJ, ALL_PROC_STATES, paramLong);
                  processState.dumpPss(paramPrintWriter, "        ", ALL_SCREEN_ADJ, ALL_MEM_ADJ, ALL_PROC_STATES, paramLong);
                  processState.dumpInternalLocked(paramPrintWriter, str8, bool);
                } 
                b3++;
                paramInt = i;
              }   
        } 
      } 
      paramPrintWriter.print("  Total procs: ");
      paramPrintWriter.print(b3);
      paramPrintWriter.print(" shown of ");
      paramPrintWriter.print(b1);
      paramPrintWriter.println(" total");
    } 
    if (bool) {
      paramPrintWriter.println();
      if (processStats.mTrackingAssociations.size() > 0) {
        paramPrintWriter.println();
        paramPrintWriter.println("Tracking associations:");
        for (paramInt = 0; paramInt < processStats.mTrackingAssociations.size(); paramInt++) {
          AssociationState.SourceState sourceState = processStats.mTrackingAssociations.get(paramInt);
          AssociationState associationState = sourceState.getAssociationState();
          paramPrintWriter.print("  #");
          paramPrintWriter.print(paramInt);
          paramPrintWriter.print(": ");
          paramPrintWriter.print(associationState.getProcessName());
          paramPrintWriter.print("/");
          UserHandle.formatUid(paramPrintWriter, associationState.getUid());
          paramPrintWriter.print(" <- ");
          paramPrintWriter.print(sourceState.getProcessName());
          paramPrintWriter.print("/");
          UserHandle.formatUid(paramPrintWriter, sourceState.getUid());
          paramPrintWriter.println(str5);
          paramPrintWriter.print("    Tracking for: ");
          TimeUtils.formatDuration(paramLong - sourceState.mTrackingUptime, paramPrintWriter);
          paramPrintWriter.println();
          paramPrintWriter.print("    Component: ");
          ComponentName componentName = new ComponentName(associationState.getPackage(), associationState.getName());
          String str = componentName.flattenToShortString();
          paramPrintWriter.print(str);
          paramPrintWriter.println();
          paramPrintWriter.print("    Proc state: ");
          if (sourceState.mProcState != -1) {
            paramPrintWriter.print(DumpUtils.STATE_NAMES[sourceState.mProcState]);
          } else {
            paramPrintWriter.print("--");
          } 
          paramPrintWriter.print(" #");
          paramPrintWriter.println(sourceState.mProcStateSeq);
          paramPrintWriter.print("    Process: ");
          paramPrintWriter.println(associationState.getProcess());
          if (sourceState.mActiveCount > 0) {
            paramPrintWriter.print("    Active count ");
            paramPrintWriter.print(sourceState.mActiveCount);
            paramPrintWriter.print(": ");
            associationState.dumpActiveDurationSummary(paramPrintWriter, sourceState, l1, paramLong, paramBoolean3);
            paramPrintWriter.println();
          } 
        } 
      } 
    } 
    paramPrintWriter.println();
    if (paramBoolean1) {
      paramPrintWriter.println("Process summary:");
      dumpSummaryLocked(paramPrintWriter, paramString, paramLong, paramBoolean4);
    } else {
      processStats.dumpTotalsLocked(paramPrintWriter, paramLong);
    } 
    if (paramBoolean3) {
      paramPrintWriter.println();
      paramPrintWriter.println("Internal state:");
      paramPrintWriter.print("  mRunning=");
      paramPrintWriter.println(processStats.mRunning);
    } 
    if (paramString == null)
      dumpFragmentationLocked(paramPrintWriter); 
  }
  
  public void dumpSummaryLocked(PrintWriter paramPrintWriter, String paramString, long paramLong, boolean paramBoolean) {
    long l = DumpUtils.dumpSingleTime(null, null, this.mMemFactorDurations, this.mMemFactor, this.mStartTime, paramLong);
    dumpFilteredSummaryLocked(paramPrintWriter, null, "  ", null, ALL_SCREEN_ADJ, ALL_MEM_ADJ, ALL_PROC_STATES, NON_CACHED_PROC_STATES, paramLong, l, paramString, paramBoolean);
    paramPrintWriter.println();
    dumpTotalsLocked(paramPrintWriter, paramLong);
  }
  
  private void dumpFragmentationLocked(PrintWriter paramPrintWriter) {
    paramPrintWriter.println();
    paramPrintWriter.println("Available pages by page size:");
    int i = this.mPageTypeLabels.size();
    for (byte b = 0; b < i; b++) {
      int j;
      Object object = this.mPageTypeNodes.get(b);
      int[] arrayOfInt = (int[])this.mPageTypeZones.get(b);
      ArrayList<String> arrayList = this.mPageTypeLabels;
      arrayList = (ArrayList<String>)arrayList.get(b);
      paramPrintWriter.format("Node %3d Zone %7s  %14s ", new Object[] { object, arrayOfInt, arrayList });
      arrayOfInt = this.mPageTypeSizes.get(b);
      if (arrayOfInt == null) {
        j = 0;
      } else {
        j = arrayOfInt.length;
      } 
      for (byte b1 = 0; b1 < j; b1++) {
        paramPrintWriter.format("%6d", new Object[] { Integer.valueOf(arrayOfInt[b1]) });
      } 
      paramPrintWriter.println();
    } 
  }
  
  long printMemoryCategory(PrintWriter paramPrintWriter, String paramString1, String paramString2, double paramDouble, long paramLong1, long paramLong2, int paramInt) {
    if (paramDouble != 0.0D) {
      paramLong1 = (long)(1024.0D * paramDouble / paramLong1);
      paramPrintWriter.print(paramString1);
      paramPrintWriter.print(paramString2);
      paramPrintWriter.print(": ");
      DebugUtils.printSizeValue(paramPrintWriter, paramLong1);
      paramPrintWriter.print(" (");
      paramPrintWriter.print(paramInt);
      paramPrintWriter.print(" samples)");
      paramPrintWriter.println();
      return paramLong2 + paramLong1;
    } 
    return paramLong2;
  }
  
  void dumpTotalsLocked(PrintWriter paramPrintWriter, long paramLong) {
    paramPrintWriter.println("Run time Stats:");
    DumpUtils.dumpSingleTime(paramPrintWriter, "  ", this.mMemFactorDurations, this.mMemFactor, this.mStartTime, paramLong);
    paramPrintWriter.println();
    paramPrintWriter.println("Memory usage:");
    TotalMemoryUseCollection totalMemoryUseCollection = new TotalMemoryUseCollection(ALL_SCREEN_ADJ, ALL_MEM_ADJ);
    computeTotalMemoryUse(totalMemoryUseCollection, paramLong);
    paramLong = printMemoryCategory(paramPrintWriter, "  ", "Kernel ", totalMemoryUseCollection.sysMemKernelWeight, totalMemoryUseCollection.totalTime, 0L, totalMemoryUseCollection.sysMemSamples);
    paramLong = printMemoryCategory(paramPrintWriter, "  ", "Native ", totalMemoryUseCollection.sysMemNativeWeight, totalMemoryUseCollection.totalTime, paramLong, totalMemoryUseCollection.sysMemSamples);
    for (byte b = 0; b < 14; b++) {
      if (b != 6)
        paramLong = printMemoryCategory(paramPrintWriter, "  ", DumpUtils.STATE_NAMES[b], totalMemoryUseCollection.processStateWeight[b], totalMemoryUseCollection.totalTime, paramLong, totalMemoryUseCollection.processStateSamples[b]); 
    } 
    paramLong = printMemoryCategory(paramPrintWriter, "  ", "Cached ", totalMemoryUseCollection.sysMemCachedWeight, totalMemoryUseCollection.totalTime, paramLong, totalMemoryUseCollection.sysMemSamples);
    paramLong = printMemoryCategory(paramPrintWriter, "  ", "Free   ", totalMemoryUseCollection.sysMemFreeWeight, totalMemoryUseCollection.totalTime, paramLong, totalMemoryUseCollection.sysMemSamples);
    paramLong = printMemoryCategory(paramPrintWriter, "  ", "Z-Ram  ", totalMemoryUseCollection.sysMemZRamWeight, totalMemoryUseCollection.totalTime, paramLong, totalMemoryUseCollection.sysMemSamples);
    paramPrintWriter.print("  TOTAL  : ");
    DebugUtils.printSizeValue(paramPrintWriter, paramLong);
    paramPrintWriter.println();
    printMemoryCategory(paramPrintWriter, "  ", DumpUtils.STATE_NAMES[6], totalMemoryUseCollection.processStateWeight[6], totalMemoryUseCollection.totalTime, paramLong, totalMemoryUseCollection.processStateSamples[6]);
    paramPrintWriter.println();
    paramPrintWriter.println("PSS collection stats:");
    paramPrintWriter.print("  Internal Single: ");
    paramPrintWriter.print(this.mInternalSinglePssCount);
    paramPrintWriter.print("x over ");
    TimeUtils.formatDuration(this.mInternalSinglePssTime, paramPrintWriter);
    paramPrintWriter.println();
    paramPrintWriter.print("  Internal All Procs (Memory Change): ");
    paramPrintWriter.print(this.mInternalAllMemPssCount);
    paramPrintWriter.print("x over ");
    TimeUtils.formatDuration(this.mInternalAllMemPssTime, paramPrintWriter);
    paramPrintWriter.println();
    paramPrintWriter.print("  Internal All Procs (Polling): ");
    paramPrintWriter.print(this.mInternalAllPollPssCount);
    paramPrintWriter.print("x over ");
    TimeUtils.formatDuration(this.mInternalAllPollPssTime, paramPrintWriter);
    paramPrintWriter.println();
    paramPrintWriter.print("  External: ");
    paramPrintWriter.print(this.mExternalPssCount);
    paramPrintWriter.print("x over ");
    TimeUtils.formatDuration(this.mExternalPssTime, paramPrintWriter);
    paramPrintWriter.println();
    paramPrintWriter.print("  External Slow: ");
    paramPrintWriter.print(this.mExternalSlowPssCount);
    paramPrintWriter.print("x over ");
    TimeUtils.formatDuration(this.mExternalSlowPssTime, paramPrintWriter);
    paramPrintWriter.println();
  }
  
  void dumpFilteredSummaryLocked(PrintWriter paramPrintWriter, String paramString1, String paramString2, String paramString3, int[] paramArrayOfint1, int[] paramArrayOfint2, int[] paramArrayOfint3, int[] paramArrayOfint4, long paramLong1, long paramLong2, String paramString4, boolean paramBoolean) {
    ArrayList<ProcessState> arrayList = collectProcessesLocked(paramArrayOfint1, paramArrayOfint2, paramArrayOfint3, paramArrayOfint4, paramLong1, paramString4, paramBoolean);
    if (arrayList.size() > 0) {
      if (paramString1 != null) {
        paramPrintWriter.println();
        paramPrintWriter.println(paramString1);
      } 
      DumpUtils.dumpProcessSummaryLocked(paramPrintWriter, paramString2, paramString3, arrayList, paramArrayOfint1, paramArrayOfint2, paramArrayOfint4, paramLong1, paramLong2);
    } 
  }
  
  public ArrayList<ProcessState> collectProcessesLocked(int[] paramArrayOfint1, int[] paramArrayOfint2, int[] paramArrayOfint3, int[] paramArrayOfint4, long paramLong, String paramString, boolean paramBoolean) {
    ArraySet<ProcessState> arraySet = new ArraySet();
    ProcessMap<LongSparseArray<PackageState>> processMap = this.mPackages;
    ArrayMap<String, SparseArray<LongSparseArray<PackageState>>> arrayMap = processMap.getMap();
    byte b;
    for (b = 0; b < arrayMap.size(); b++) {
      String str = arrayMap.keyAt(b);
      SparseArray<LongSparseArray> sparseArray = (SparseArray)arrayMap.valueAt(b);
      for (byte b1 = 0; b1 < sparseArray.size(); b1++) {
        LongSparseArray<PackageState> longSparseArray = sparseArray.valueAt(b1);
        int i = longSparseArray.size();
        for (byte b2 = 0; b2 < i; b2++) {
          boolean bool;
          PackageState packageState = longSparseArray.valueAt(b2);
          int j = packageState.mProcesses.size();
          if (paramString == null || paramString.equals(str)) {
            bool = true;
          } else {
            bool = false;
          } 
          for (byte b3 = 0; b3 < j; b3++) {
            ProcessState processState = packageState.mProcesses.valueAt(b3);
            if (bool || paramString.equals(processState.getName()))
              if (!paramBoolean || processState.isInUse())
                arraySet.add(processState.getCommonProcess());  
          } 
        } 
      } 
    } 
    ArrayList<ProcessState> arrayList = new ArrayList(arraySet.size());
    for (b = 0; b < arraySet.size(); b++) {
      ProcessState processState = arraySet.valueAt(b);
      if (processState.computeProcessTimeLocked(paramArrayOfint1, paramArrayOfint2, paramArrayOfint3, paramLong) > 0L) {
        arrayList.add(processState);
        if (paramArrayOfint3 != paramArrayOfint4)
          processState.computeProcessTimeLocked(paramArrayOfint1, paramArrayOfint2, paramArrayOfint4, paramLong); 
      } 
    } 
    Collections.sort(arrayList, ProcessState.COMPARATOR);
    return arrayList;
  }
  
  public void dumpCheckinLocked(PrintWriter paramPrintWriter, String paramString, int paramInt) {
    long l2, l1 = SystemClock.uptimeMillis();
    ProcessMap<LongSparseArray<PackageState>> processMap = this.mPackages;
    ArrayMap<String, SparseArray<LongSparseArray<PackageState>>> arrayMap = processMap.getMap();
    paramPrintWriter.println("vers,5");
    paramPrintWriter.print("period,");
    paramPrintWriter.print(this.mTimePeriodStartClockStr);
    String str = ",";
    paramPrintWriter.print(",");
    paramPrintWriter.print(this.mTimePeriodStartRealtime);
    paramPrintWriter.print(",");
    if (this.mRunning) {
      l2 = SystemClock.elapsedRealtime();
    } else {
      l2 = this.mTimePeriodEndRealtime;
    } 
    paramPrintWriter.print(l2);
    int i = 1;
    if ((this.mFlags & 0x2) != 0) {
      paramPrintWriter.print(",shutdown");
      i = 0;
    } 
    if ((this.mFlags & 0x4) != 0) {
      paramPrintWriter.print(",sysprops");
      i = 0;
    } 
    if ((this.mFlags & 0x1) != 0) {
      paramPrintWriter.print(",complete");
      i = 0;
    } 
    if (i)
      paramPrintWriter.print(",partial"); 
    if (this.mHasSwappedOutPss)
      paramPrintWriter.print(",swapped-out-pss"); 
    paramPrintWriter.println();
    paramPrintWriter.print("config,");
    paramPrintWriter.println(this.mRuntime);
    if ((paramInt & 0xE) != 0) {
      ArrayMap<String, SparseArray<LongSparseArray<PackageState>>> arrayMap2;
      for (i = 0; i < arrayMap.size(); i++) {
        String str1 = arrayMap.keyAt(i);
        if (paramString == null || paramString.equals(str1)) {
          SparseArray<LongSparseArray> sparseArray = (SparseArray)arrayMap.valueAt(i);
          for (int m = 0; m < sparseArray.size(); m++, sparseArray = sparseArray1, longSparseArray2 = longSparseArray1, str2 = str5, arrayMap2 = arrayMap3) {
            String str2;
            int n = sparseArray.keyAt(m);
            LongSparseArray<PackageState> longSparseArray3 = sparseArray.valueAt(m);
            SparseArray<LongSparseArray> sparseArray1;
            String str3;
            LongSparseArray<PackageState> longSparseArray1, longSparseArray2;
            byte b;
            String str4;
            ArrayMap<String, SparseArray<LongSparseArray<PackageState>>> arrayMap3, arrayMap4;
            String str5;
            for (b = 0, str4 = str, arrayMap4 = arrayMap, str3 = str1, sparseArray1 = sparseArray, longSparseArray2 = longSparseArray3; b < longSparseArray2.size(); b++, longSparseArray6 = longSparseArray5, i4 = m, arrayMap5 = arrayMap7, str7 = str6, m = i, longSparseArray1 = longSparseArray6, i = i4, str5 = str9, arrayMap3 = arrayMap5) {
              LongSparseArray<PackageState> longSparseArray4;
              String str6, str8;
              LongSparseArray<PackageState> longSparseArray5;
              String str7;
              SparseArray<LongSparseArray> sparseArray2;
              LongSparseArray<PackageState> longSparseArray6;
              SparseArray<LongSparseArray> sparseArray3;
              ArrayMap<String, SparseArray<LongSparseArray<PackageState>>> arrayMap5, arrayMap6;
              String str9, str10;
              ArrayMap<String, SparseArray<LongSparseArray<PackageState>>> arrayMap7;
              int i4;
              l2 = longSparseArray2.keyAt(b);
              PackageState packageState = longSparseArray2.valueAt(b);
              int i1 = packageState.mProcesses.size();
              int i2 = packageState.mServices.size();
              int i3 = packageState.mAssociations.size();
              if ((paramInt & 0x2) != 0) {
                PackageState packageState1;
                SparseArray<LongSparseArray> sparseArray4;
                String str11;
                for (byte b1 = 0; b1 < i3; b1++) {
                  ProcessState processState = packageState1.mProcesses.valueAt(b1);
                  ArrayMap<String, ProcessState> arrayMap9 = packageState1.mProcesses;
                  String str12 = arrayMap9.keyAt(b1);
                  processState.dumpPackageProcCheckin(paramPrintWriter, str11, n, l2, str12, l1);
                } 
                LongSparseArray<PackageState> longSparseArray = longSparseArray2;
                sparseArray3 = sparseArray4;
                i3 = i4;
                str8 = str11;
                ArrayMap<String, SparseArray<LongSparseArray<PackageState>>> arrayMap8 = arrayMap4;
                str10 = str4;
                PackageState packageState2 = packageState1;
                i4 = m;
                m = i;
                arrayMap6 = arrayMap8;
                longSparseArray4 = longSparseArray;
                sparseArray1 = sparseArray3;
                i = i3;
              } else {
                String str11 = str8;
                longSparseArray5 = longSparseArray4;
                int i5 = i;
                String str12 = str10;
                arrayMap7 = arrayMap6;
                i4 = i3;
                i = m;
                str6 = str11;
                str9 = str12;
                m = i5;
                sparseArray2 = sparseArray3;
              } 
              if ((paramInt & 0x4) != 0)
                for (i3 = 0; i3 < i2; i3++) {
                  ArrayMap<String, ServiceState> arrayMap8 = ((PackageState)sparseArray2).mServices;
                  String str11 = arrayMap8.keyAt(i3);
                  String str12 = DumpUtils.collapseString((String)longSparseArray5, str11);
                  ServiceState serviceState = ((PackageState)sparseArray2).mServices.valueAt(i3);
                  serviceState.dumpTimesCheckin(paramPrintWriter, (String)longSparseArray5, n, l2, str12, l1);
                }  
              if ((paramInt & 0x8) != 0)
                for (i2 = 0; i2 < i4; i2++) {
                  ArrayMap<String, AssociationState> arrayMap8 = ((PackageState)sparseArray2).mAssociations;
                  String str11 = arrayMap8.keyAt(i2);
                  str11 = DumpUtils.collapseString((String)longSparseArray5, str11);
                  AssociationState associationState = ((PackageState)sparseArray2).mAssociations.valueAt(i2);
                  associationState.dumpTimesCheckin(paramPrintWriter, (String)longSparseArray5, n, l2, str11, l1);
                }  
            } 
          } 
        } 
      } 
      ArrayMap<String, SparseArray<LongSparseArray<PackageState>>> arrayMap1 = arrayMap2;
    } else {
      paramString = ",";
    } 
    if ((paramInt & 0x1) != 0) {
      ArrayMap<String, SparseArray<ProcessState>> arrayMap1 = this.mProcesses.getMap();
      for (paramInt = 0; paramInt < arrayMap1.size(); paramInt++) {
        str = arrayMap1.keyAt(paramInt);
        SparseArray<ProcessState> sparseArray = arrayMap1.valueAt(paramInt);
        for (i = 0; i < sparseArray.size(); i++) {
          int m = sparseArray.keyAt(i);
          ProcessState processState = sparseArray.valueAt(i);
          processState.dumpProcCheckin(paramPrintWriter, str, m, l1);
        } 
      } 
    } 
    paramPrintWriter.print("total");
    DumpUtils.dumpAdjTimesCheckin(paramPrintWriter, ",", this.mMemFactorDurations, this.mMemFactor, this.mStartTime, l1);
    paramPrintWriter.println();
    int j = this.mSysMemUsage.getKeyCount();
    if (j > 0) {
      paramPrintWriter.print("sysmemusage");
      for (paramInt = 0; paramInt < j; paramInt++) {
        int m = this.mSysMemUsage.getKeyAt(paramInt);
        i = SparseMappingTable.getIdFromKey(m);
        paramPrintWriter.print(paramString);
        DumpUtils.printProcStateTag(paramPrintWriter, i);
        for (i = 0; i < 16; i++) {
          if (i > 1)
            paramPrintWriter.print(":"); 
          paramPrintWriter.print(this.mSysMemUsage.getValue(m, i));
        } 
      } 
    } 
    paramPrintWriter.println();
    TotalMemoryUseCollection totalMemoryUseCollection = new TotalMemoryUseCollection(ALL_SCREEN_ADJ, ALL_MEM_ADJ);
    computeTotalMemoryUse(totalMemoryUseCollection, l1);
    paramPrintWriter.print("weights,");
    paramPrintWriter.print(totalMemoryUseCollection.totalTime);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print(totalMemoryUseCollection.sysMemCachedWeight);
    paramPrintWriter.print(":");
    paramPrintWriter.print(totalMemoryUseCollection.sysMemSamples);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print(totalMemoryUseCollection.sysMemFreeWeight);
    paramPrintWriter.print(":");
    paramPrintWriter.print(totalMemoryUseCollection.sysMemSamples);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print(totalMemoryUseCollection.sysMemZRamWeight);
    paramPrintWriter.print(":");
    paramPrintWriter.print(totalMemoryUseCollection.sysMemSamples);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print(totalMemoryUseCollection.sysMemKernelWeight);
    paramPrintWriter.print(":");
    paramPrintWriter.print(totalMemoryUseCollection.sysMemSamples);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print(totalMemoryUseCollection.sysMemNativeWeight);
    paramPrintWriter.print(":");
    paramPrintWriter.print(totalMemoryUseCollection.sysMemSamples);
    for (paramInt = 0; paramInt < 14; paramInt++) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print(totalMemoryUseCollection.processStateWeight[paramInt]);
      paramPrintWriter.print(":");
      paramPrintWriter.print(totalMemoryUseCollection.processStateSamples[paramInt]);
    } 
    paramPrintWriter.println();
    int k = this.mPageTypeLabels.size();
    for (paramInt = 0; paramInt < k; paramInt++) {
      paramPrintWriter.print("availablepages,");
      paramPrintWriter.print(this.mPageTypeLabels.get(paramInt));
      paramPrintWriter.print(paramString);
      paramPrintWriter.print(this.mPageTypeZones.get(paramInt));
      paramPrintWriter.print(paramString);
      int[] arrayOfInt = this.mPageTypeSizes.get(paramInt);
      if (arrayOfInt == null) {
        i = 0;
      } else {
        i = arrayOfInt.length;
      } 
      for (j = 0; j < i; j++) {
        if (j != 0)
          paramPrintWriter.print(paramString); 
        paramPrintWriter.print(arrayOfInt[j]);
      } 
      paramPrintWriter.println();
    } 
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong, int paramInt) {
    dumpProtoPreamble(paramProtoOutputStream);
    int i = this.mPageTypeLabels.size();
    byte b;
    for (b = 0; b < i; b++) {
      int j;
      long l = paramProtoOutputStream.start(2246267895818L);
      paramProtoOutputStream.write(1120986464257L, ((Integer)this.mPageTypeNodes.get(b)).intValue());
      paramProtoOutputStream.write(1138166333442L, this.mPageTypeZones.get(b));
      paramProtoOutputStream.write(1138166333443L, this.mPageTypeLabels.get(b));
      int[] arrayOfInt = this.mPageTypeSizes.get(b);
      if (arrayOfInt == null) {
        j = 0;
      } else {
        j = arrayOfInt.length;
      } 
      for (byte b1 = 0; b1 < j; b1++)
        paramProtoOutputStream.write(2220498092036L, arrayOfInt[b1]); 
      paramProtoOutputStream.end(l);
    } 
    ArrayMap<String, SparseArray<ProcessState>> arrayMap = this.mProcesses.getMap();
    if ((paramInt & 0x1) != 0)
      for (b = 0; b < arrayMap.size(); b++) {
        String str = arrayMap.keyAt(b);
        SparseArray<ProcessState> sparseArray = arrayMap.valueAt(b);
        for (byte b1 = 0; b1 < sparseArray.size(); b1++) {
          int j = sparseArray.keyAt(b1);
          ProcessState processState = sparseArray.valueAt(b1);
          processState.dumpDebug(paramProtoOutputStream, 2246267895816L, str, j, paramLong);
        } 
      }  
    if ((paramInt & 0xE) != 0) {
      ProcessMap<LongSparseArray<PackageState>> processMap = this.mPackages;
      ArrayMap<String, SparseArray<LongSparseArray<PackageState>>> arrayMap1 = processMap.getMap();
      for (b = 0; b < arrayMap1.size(); b++) {
        SparseArray<LongSparseArray> sparseArray = (SparseArray)arrayMap1.valueAt(b);
        for (byte b1 = 0; b1 < sparseArray.size(); b1++) {
          LongSparseArray<PackageState> longSparseArray = sparseArray.valueAt(b1);
          for (byte b2 = 0; b2 < longSparseArray.size(); b2++) {
            PackageState packageState = longSparseArray.valueAt(b2);
            packageState.dumpDebug(paramProtoOutputStream, 2246267895817L, paramLong, paramInt);
          } 
        } 
      } 
    } 
  }
  
  public void dumpAggregatedProtoForStatsd(ProtoOutputStream[] paramArrayOfProtoOutputStream, long paramLong) {
    byte b1 = 0;
    dumpProtoPreamble(paramArrayOfProtoOutputStream[0]);
    ArrayMap<String, SparseArray<ProcessState>> arrayMap = this.mProcesses.getMap();
    ProcessMap<ArraySet<PackageState>> processMap = new ProcessMap();
    SparseArray<ArraySet<String>> sparseArray = new SparseArray();
    boolean bool = false;
    collectProcessPackageMaps(null, false, processMap, sparseArray);
    byte b2;
    for (b2 = 0; b2 < arrayMap.size(); b2++, b1 = b) {
      String str = arrayMap.keyAt(b2);
      byte b = b1;
      if (paramArrayOfProtoOutputStream[b1].getRawSize() > paramLong) {
        b1++;
        if (b1 >= paramArrayOfProtoOutputStream.length) {
          Object[] arrayOfObject = new Object[2];
          arrayOfObject[bool] = Integer.valueOf(b2);
          arrayOfObject[1] = Integer.valueOf(arrayMap.size());
          Slog.d("ProcessStats", String.format("Dropping process indices from %d to %d from statsd proto (too large)", arrayOfObject));
          break;
        } 
        dumpProtoPreamble(paramArrayOfProtoOutputStream[b1]);
        b = b1;
      } 
      SparseArray<ProcessState> sparseArray1 = arrayMap.valueAt(b2);
      for (b1 = 0; b1 < sparseArray1.size(); b1++) {
        int i = sparseArray1.keyAt(b1);
        ProcessState processState = sparseArray1.valueAt(b1);
        processState.dumpAggregatedProtoForStatsd(paramArrayOfProtoOutputStream[b], 2246267895816L, str, i, this.mTimePeriodEndRealtime, processMap, sparseArray);
      } 
    } 
    for (b2 = 0; b2 <= b1; b2++)
      paramArrayOfProtoOutputStream[b2].flush(); 
  }
  
  private void dumpProtoPreamble(ProtoOutputStream paramProtoOutputStream) {
    long l;
    paramProtoOutputStream.write(1112396529665L, this.mTimePeriodStartRealtime);
    if (this.mRunning) {
      l = SystemClock.elapsedRealtime();
    } else {
      l = this.mTimePeriodEndRealtime;
    } 
    paramProtoOutputStream.write(1112396529666L, l);
    paramProtoOutputStream.write(1112396529667L, this.mTimePeriodStartUptime);
    paramProtoOutputStream.write(1112396529668L, this.mTimePeriodEndUptime);
    paramProtoOutputStream.write(1138166333445L, this.mRuntime);
    paramProtoOutputStream.write(1133871366150L, this.mHasSwappedOutPss);
    boolean bool = true;
    if ((this.mFlags & 0x2) != 0) {
      paramProtoOutputStream.write(2259152797703L, 3);
      bool = false;
    } 
    if ((this.mFlags & 0x4) != 0) {
      paramProtoOutputStream.write(2259152797703L, 4);
      bool = false;
    } 
    if ((this.mFlags & 0x1) != 0) {
      paramProtoOutputStream.write(2259152797703L, 1);
      bool = false;
    } 
    if (bool)
      paramProtoOutputStream.write(2259152797703L, 2); 
  }
  
  private void collectProcessPackageMaps(String paramString, boolean paramBoolean, ProcessMap<ArraySet<PackageState>> paramProcessMap, SparseArray<ArraySet<String>> paramSparseArray) {
    ProcessMap<LongSparseArray<PackageState>> processMap = this.mPackages;
    ArrayMap<String, SparseArray<LongSparseArray<PackageState>>> arrayMap = processMap.getMap();
    for (int i = arrayMap.size() - 1; i >= 0; i--) {
      String str = arrayMap.keyAt(i);
      SparseArray<LongSparseArray> sparseArray = (SparseArray)arrayMap.valueAt(i);
      for (int j = sparseArray.size() - 1; j >= 0; j--) {
        LongSparseArray<PackageState> longSparseArray = sparseArray.valueAt(j);
        for (int k = longSparseArray.size() - 1; k >= 0; k--) {
          boolean bool;
          PackageState packageState = longSparseArray.valueAt(k);
          if (paramString == null || paramString.equals(str)) {
            bool = true;
          } else {
            bool = false;
          } 
          for (int m = packageState.mProcesses.size() - 1; m >= 0; m--) {
            ProcessState processState = packageState.mProcesses.valueAt(m);
            if (bool || paramString.equals(processState.getName()))
              if (!paramBoolean || processState.isInUse()) {
                String str1 = processState.getName();
                int n = processState.getUid();
                ArraySet<PackageState> arraySet = paramProcessMap.get(str1, n);
                if (arraySet == null) {
                  arraySet = new ArraySet();
                  paramProcessMap.put(str1, n, arraySet);
                } 
                arraySet.add(packageState);
                arraySet = (ArraySet<PackageState>)paramSparseArray.get(n);
                if (arraySet == null) {
                  arraySet = new ArraySet<>();
                  paramSparseArray.put(n, arraySet);
                } 
                arraySet.add(packageState.mPackageName);
              }  
          } 
        } 
      } 
    } 
  }
  
  public void dumpFilteredAssociationStatesProtoForProc(ProtoOutputStream paramProtoOutputStream, long paramLong1, long paramLong2, ProcessState paramProcessState, ProcessMap<ArraySet<PackageState>> paramProcessMap, SparseArray<ArraySet<String>> paramSparseArray) {
    ArraySet<PackageState> arraySet2;
    ArrayMap<String, AssociationState> arrayMap1;
    PackageState packageState;
    if (paramProcessState.isMultiPackage() && paramProcessState.getCommonProcess() != paramProcessState)
      return; 
    ArrayMap<Object, Object> arrayMap = new ArrayMap<>();
    String str2 = paramProcessState.getName();
    int i = paramProcessState.getUid();
    paramProcessState.getVersion();
    ArraySet<PackageState> arraySet1 = paramProcessMap.get(str2, i);
    if (arraySet1 == null || arraySet1.isEmpty())
      return; 
    String str1;
    ArraySet<PackageState> arraySet3;
    int j;
    for (j = arraySet1.size() - 1, str1 = str2, arraySet3 = arraySet1; j >= 0; j--) {
      packageState = arraySet3.valueAt(j);
      ArrayMap<String, AssociationState> arrayMap2 = packageState.mAssociations;
      for (int k = arrayMap2.size() - 1; k >= 0; k--, arrayMap6 = arrayMap3, arraySet2 = arraySet4, str3 = str5, arrayMap1 = arrayMap6) {
        String str4;
        ArraySet<PackageState> arraySet4;
        String str3;
        ArraySet<PackageState> arraySet5;
        ArrayMap<String, AssociationState> arrayMap3, arrayMap4;
        String str5;
        ArrayMap<String, AssociationState> arrayMap5, arrayMap6;
        AssociationState associationState = arrayMap2.valueAt(k);
        if (!TextUtils.equals(associationState.getProcessName(), str1)) {
          arrayMap5 = arrayMap2;
          arraySet4 = arraySet3;
          String str = str1;
          arrayMap4 = arrayMap5;
          arraySet5 = arraySet4;
          str4 = str;
        } else {
          ArrayMap<AssociationState.SourceKey, AssociationState.SourceState> arrayMap7 = ((AssociationState)arrayMap5).mSources;
          for (int m = arrayMap7.size() - 1; m >= 0; m--) {
            AssociationState.SourceKey sourceKey = arrayMap7.keyAt(m);
            AssociationState.SourceState sourceState = arrayMap7.valueAt(m);
            long[] arrayOfLong = (long[])arrayMap.get(sourceKey);
            if (arrayOfLong == null) {
              arrayOfLong = new long[2];
              arrayMap.put(sourceKey, arrayOfLong);
            } 
            arrayOfLong[0] = arrayOfLong[0] + sourceState.mDuration;
            arrayOfLong[1] = arrayOfLong[1] + sourceState.mCount;
            if (sourceState.mNesting > 0)
              arrayOfLong[0] = arrayOfLong[0] + paramLong2 - sourceState.mStartUptime; 
          } 
          String str = str4;
          arraySet4 = arraySet5;
          arrayMap3 = arrayMap4;
          str5 = str;
        } 
      } 
    } 
    IBinder iBinder = ServiceManager.getService("procstats");
    IProcessStats iProcessStats = IProcessStats.Stub.asInterface(iBinder);
    if (iProcessStats != null)
      try {
        paramLong2 = iProcessStats.getMinAssociationDumpDuration();
        if (paramLong2 > 0L)
          for (i = arrayMap.size() - 1; i >= 0; i--) {
            long[] arrayOfLong = (long[])arrayMap.valueAt(i);
            if (arrayOfLong[0] < paramLong2)
              arrayMap.removeAt(i); 
          }  
      } catch (RemoteException remoteException) {} 
    if (!arrayMap.isEmpty())
      for (i = arrayMap.size() - 1; i >= 0; i--) {
        AssociationState.SourceKey sourceKey = (AssociationState.SourceKey)arrayMap.keyAt(i);
        long[] arrayOfLong = (long[])arrayMap.valueAt(i);
        paramLong2 = paramProtoOutputStream.start(paramLong1);
        j = paramSparseArray.indexOfKey(sourceKey.mUid);
        String str4 = sourceKey.mProcess, str3 = sourceKey.mPackage;
        if (j >= 0)
          if (((ArraySet)paramSparseArray.valueAt(j)).size() > 1) {
            boolean bool1 = true;
            continue;
          }  
        boolean bool = false;
        continue;
        ProcessState.writeCompressedProcessName(paramProtoOutputStream, 1138166333441L, (String)arrayMap1, (String)arraySet2, SYNTHETIC_LOCAL_VARIABLE_21);
        paramProtoOutputStream.write(1120986464261L, ((AssociationState.SourceKey)packageState).mUid);
        paramProtoOutputStream.write(1120986464259L, (int)remoteException[1]);
        paramProtoOutputStream.write(1120986464260L, (int)(remoteException[0] / 1000L));
        paramProtoOutputStream.end(paramLong2);
      }  
  }
  
  class ProcessStateHolder {
    public final long appVersion;
    
    public ProcessStats.PackageState pkg;
    
    public ProcessState state;
    
    public ProcessStateHolder(ProcessStats this$0) {
      this.appVersion = this$0;
    }
  }
  
  class PackageState {
    public final ArrayMap<String, ProcessState> mProcesses = new ArrayMap<>();
    
    public final ArrayMap<String, ServiceState> mServices = new ArrayMap<>();
    
    public final ArrayMap<String, AssociationState> mAssociations = new ArrayMap<>();
    
    public final String mPackageName;
    
    public final ProcessStats mProcessStats;
    
    public final int mUid;
    
    public final long mVersionCode;
    
    public PackageState(ProcessStats this$0, String param1String, int param1Int, long param1Long) {
      this.mProcessStats = this$0;
      this.mUid = param1Int;
      this.mPackageName = param1String;
      this.mVersionCode = param1Long;
    }
    
    public AssociationState getAssociationStateLocked(ProcessState param1ProcessState, String param1String) {
      AssociationState associationState2 = this.mAssociations.get(param1String);
      if (associationState2 != null) {
        if (param1ProcessState != null)
          associationState2.setProcess(param1ProcessState); 
        return associationState2;
      } 
      AssociationState associationState1 = new AssociationState(this.mProcessStats, this, param1String, param1ProcessState.getName(), param1ProcessState);
      this.mAssociations.put(param1String, associationState1);
      return associationState1;
    }
    
    public void dumpDebug(ProtoOutputStream param1ProtoOutputStream, long param1Long1, long param1Long2, int param1Int) {
      param1Long1 = param1ProtoOutputStream.start(param1Long1);
      param1ProtoOutputStream.write(1138166333441L, this.mPackageName);
      param1ProtoOutputStream.write(1120986464258L, this.mUid);
      param1ProtoOutputStream.write(1112396529667L, this.mVersionCode);
      if ((param1Int & 0x2) != 0)
        for (byte b = 0; b < this.mProcesses.size(); b++) {
          String str = this.mProcesses.keyAt(b);
          ProcessState processState = this.mProcesses.valueAt(b);
          processState.dumpDebug(param1ProtoOutputStream, 2246267895812L, str, this.mUid, param1Long2);
        }  
      if ((param1Int & 0x4) != 0)
        for (byte b = 0; b < this.mServices.size(); b++) {
          ServiceState serviceState = this.mServices.valueAt(b);
          serviceState.dumpDebug(param1ProtoOutputStream, 2246267895813L, param1Long2);
        }  
      if ((param1Int & 0x8) != 0)
        for (param1Int = 0; param1Int < this.mAssociations.size(); param1Int++) {
          AssociationState associationState = this.mAssociations.valueAt(param1Int);
          associationState.dumpDebug(param1ProtoOutputStream, 2246267895814L, param1Long2);
        }  
      param1ProtoOutputStream.end(param1Long1);
    }
  }
  
  class ProcessDataCollection {
    public long avgPss;
    
    public long avgRss;
    
    public long avgUss;
    
    public long maxPss;
    
    public long maxRss;
    
    public long maxUss;
    
    final int[] memStates;
    
    public long minPss;
    
    public long minRss;
    
    public long minUss;
    
    public long numPss;
    
    final int[] procStates;
    
    final int[] screenStates;
    
    public long totalTime;
    
    public ProcessDataCollection(ProcessStats this$0, int[] param1ArrayOfint1, int[] param1ArrayOfint2) {
      this.screenStates = (int[])this$0;
      this.memStates = param1ArrayOfint1;
      this.procStates = param1ArrayOfint2;
    }
    
    void print(PrintWriter param1PrintWriter, long param1Long, boolean param1Boolean) {
      if (this.totalTime > param1Long)
        param1PrintWriter.print("*"); 
      DumpUtils.printPercent(param1PrintWriter, this.totalTime / param1Long);
      if (this.numPss > 0L) {
        param1PrintWriter.print(" (");
        DebugUtils.printSizeValue(param1PrintWriter, this.minPss * 1024L);
        param1PrintWriter.print("-");
        DebugUtils.printSizeValue(param1PrintWriter, this.avgPss * 1024L);
        param1PrintWriter.print("-");
        DebugUtils.printSizeValue(param1PrintWriter, this.maxPss * 1024L);
        param1PrintWriter.print("/");
        DebugUtils.printSizeValue(param1PrintWriter, this.minUss * 1024L);
        param1PrintWriter.print("-");
        DebugUtils.printSizeValue(param1PrintWriter, this.avgUss * 1024L);
        param1PrintWriter.print("-");
        DebugUtils.printSizeValue(param1PrintWriter, this.maxUss * 1024L);
        param1PrintWriter.print("/");
        DebugUtils.printSizeValue(param1PrintWriter, this.minRss * 1024L);
        param1PrintWriter.print("-");
        DebugUtils.printSizeValue(param1PrintWriter, this.avgRss * 1024L);
        param1PrintWriter.print("-");
        DebugUtils.printSizeValue(param1PrintWriter, this.maxRss * 1024L);
        if (param1Boolean) {
          param1PrintWriter.print(" over ");
          param1PrintWriter.print(this.numPss);
        } 
        param1PrintWriter.print(")");
      } 
    }
  }
  
  class TotalMemoryUseCollection {
    public boolean hasSwappedOutPss;
    
    final int[] memStates;
    
    public long[] processStatePss;
    
    public int[] processStateSamples;
    
    public long[] processStateTime;
    
    public double[] processStateWeight;
    
    final int[] screenStates;
    
    public double sysMemCachedWeight;
    
    public double sysMemFreeWeight;
    
    public double sysMemKernelWeight;
    
    public double sysMemNativeWeight;
    
    public int sysMemSamples;
    
    public long[] sysMemUsage;
    
    public double sysMemZRamWeight;
    
    public long totalTime;
    
    public TotalMemoryUseCollection(ProcessStats this$0, int[] param1ArrayOfint1) {
      this.processStatePss = new long[14];
      this.processStateWeight = new double[14];
      this.processStateTime = new long[14];
      this.processStateSamples = new int[14];
      this.sysMemUsage = new long[16];
      this.screenStates = (int[])this$0;
      this.memStates = param1ArrayOfint1;
    }
  }
}
