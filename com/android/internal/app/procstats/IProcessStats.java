package com.android.internal.app.procstats;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IProcessStats extends IInterface {
  long getCommittedStats(long paramLong, int paramInt, boolean paramBoolean, List<ParcelFileDescriptor> paramList) throws RemoteException;
  
  long getCommittedStatsMerged(long paramLong, int paramInt, boolean paramBoolean, List<ParcelFileDescriptor> paramList, ProcessStats paramProcessStats) throws RemoteException;
  
  int getCurrentMemoryState() throws RemoteException;
  
  byte[] getCurrentStats(List<ParcelFileDescriptor> paramList) throws RemoteException;
  
  long getMinAssociationDumpDuration() throws RemoteException;
  
  ParcelFileDescriptor getStatsOverTime(long paramLong) throws RemoteException;
  
  class Default implements IProcessStats {
    public byte[] getCurrentStats(List<ParcelFileDescriptor> param1List) throws RemoteException {
      return null;
    }
    
    public ParcelFileDescriptor getStatsOverTime(long param1Long) throws RemoteException {
      return null;
    }
    
    public int getCurrentMemoryState() throws RemoteException {
      return 0;
    }
    
    public long getCommittedStats(long param1Long, int param1Int, boolean param1Boolean, List<ParcelFileDescriptor> param1List) throws RemoteException {
      return 0L;
    }
    
    public long getCommittedStatsMerged(long param1Long, int param1Int, boolean param1Boolean, List<ParcelFileDescriptor> param1List, ProcessStats param1ProcessStats) throws RemoteException {
      return 0L;
    }
    
    public long getMinAssociationDumpDuration() throws RemoteException {
      return 0L;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IProcessStats {
    private static final String DESCRIPTOR = "com.android.internal.app.procstats.IProcessStats";
    
    static final int TRANSACTION_getCommittedStats = 4;
    
    static final int TRANSACTION_getCommittedStatsMerged = 5;
    
    static final int TRANSACTION_getCurrentMemoryState = 3;
    
    static final int TRANSACTION_getCurrentStats = 1;
    
    static final int TRANSACTION_getMinAssociationDumpDuration = 6;
    
    static final int TRANSACTION_getStatsOverTime = 2;
    
    public Stub() {
      attachInterface(this, "com.android.internal.app.procstats.IProcessStats");
    }
    
    public static IProcessStats asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.app.procstats.IProcessStats");
      if (iInterface != null && iInterface instanceof IProcessStats)
        return (IProcessStats)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 6:
          return "getMinAssociationDumpDuration";
        case 5:
          return "getCommittedStatsMerged";
        case 4:
          return "getCommittedStats";
        case 3:
          return "getCurrentMemoryState";
        case 2:
          return "getStatsOverTime";
        case 1:
          break;
      } 
      return "getCurrentStats";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        ArrayList<ParcelFileDescriptor> arrayList2;
        ParcelFileDescriptor parcelFileDescriptor;
        long l;
        boolean bool;
        ProcessStats processStats;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 6:
            param1Parcel1.enforceInterface("com.android.internal.app.procstats.IProcessStats");
            l = getMinAssociationDumpDuration();
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 5:
            param1Parcel1.enforceInterface("com.android.internal.app.procstats.IProcessStats");
            l = param1Parcel1.readLong();
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            arrayList2 = new ArrayList();
            processStats = new ProcessStats();
            l = getCommittedStatsMerged(l, param1Int1, bool, arrayList2, processStats);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            param1Parcel2.writeInt(1);
            processStats.writeToParcel(param1Parcel2, 1);
            return true;
          case 4:
            arrayList2.enforceInterface("com.android.internal.app.procstats.IProcessStats");
            l = arrayList2.readLong();
            param1Int1 = arrayList2.readInt();
            if (arrayList2.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            arrayList2 = new ArrayList<>();
            l = getCommittedStats(l, param1Int1, bool, arrayList2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 3:
            arrayList2.enforceInterface("com.android.internal.app.procstats.IProcessStats");
            param1Int1 = getCurrentMemoryState();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 2:
            arrayList2.enforceInterface("com.android.internal.app.procstats.IProcessStats");
            l = arrayList2.readLong();
            parcelFileDescriptor = getStatsOverTime(l);
            param1Parcel2.writeNoException();
            if (parcelFileDescriptor != null) {
              param1Parcel2.writeInt(1);
              parcelFileDescriptor.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 1:
            break;
        } 
        parcelFileDescriptor.enforceInterface("com.android.internal.app.procstats.IProcessStats");
        ArrayList<ParcelFileDescriptor> arrayList1 = new ArrayList();
        byte[] arrayOfByte = getCurrentStats(arrayList1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeByteArray(arrayOfByte);
        return true;
      } 
      param1Parcel2.writeString("com.android.internal.app.procstats.IProcessStats");
      return true;
    }
    
    private static class Proxy implements IProcessStats {
      public static IProcessStats sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.app.procstats.IProcessStats";
      }
      
      public byte[] getCurrentStats(List<ParcelFileDescriptor> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.procstats.IProcessStats");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IProcessStats.Stub.getDefaultImpl() != null)
            return IProcessStats.Stub.getDefaultImpl().getCurrentStats(param2List); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParcelFileDescriptor getStatsOverTime(long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ParcelFileDescriptor parcelFileDescriptor;
          parcel1.writeInterfaceToken("com.android.internal.app.procstats.IProcessStats");
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IProcessStats.Stub.getDefaultImpl() != null) {
            parcelFileDescriptor = IProcessStats.Stub.getDefaultImpl().getStatsOverTime(param2Long);
            return parcelFileDescriptor;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(parcel2);
          } else {
            parcelFileDescriptor = null;
          } 
          return parcelFileDescriptor;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getCurrentMemoryState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.procstats.IProcessStats");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IProcessStats.Stub.getDefaultImpl() != null)
            return IProcessStats.Stub.getDefaultImpl().getCurrentMemoryState(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getCommittedStats(long param2Long, int param2Int, boolean param2Boolean, List<ParcelFileDescriptor> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.app.procstats.IProcessStats");
          parcel1.writeLong(param2Long);
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool1 && IProcessStats.Stub.getDefaultImpl() != null) {
            param2Long = IProcessStats.Stub.getDefaultImpl().getCommittedStats(param2Long, param2Int, param2Boolean, param2List);
            return param2Long;
          } 
          parcel2.readException();
          param2Long = parcel2.readLong();
          return param2Long;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getCommittedStatsMerged(long param2Long, int param2Int, boolean param2Boolean, List<ParcelFileDescriptor> param2List, ProcessStats param2ProcessStats) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.procstats.IProcessStats");
          try {
            parcel1.writeLong(param2Long);
            try {
              boolean bool;
              parcel1.writeInt(param2Int);
              if (param2Boolean) {
                bool = true;
              } else {
                bool = false;
              } 
              parcel1.writeInt(bool);
              try {
                boolean bool1 = this.mRemote.transact(5, parcel1, parcel2, 0);
                if (!bool1 && IProcessStats.Stub.getDefaultImpl() != null) {
                  param2Long = IProcessStats.Stub.getDefaultImpl().getCommittedStatsMerged(param2Long, param2Int, param2Boolean, param2List, param2ProcessStats);
                  parcel2.recycle();
                  parcel1.recycle();
                  return param2Long;
                } 
                parcel2.readException();
                param2Long = parcel2.readLong();
                param2Int = parcel2.readInt();
                if (param2Int != 0)
                  try {
                    param2ProcessStats.readFromParcel(parcel2);
                  } finally {} 
                parcel2.recycle();
                parcel1.recycle();
                return param2Long;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2List;
      }
      
      public long getMinAssociationDumpDuration() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.procstats.IProcessStats");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IProcessStats.Stub.getDefaultImpl() != null)
            return IProcessStats.Stub.getDefaultImpl().getMinAssociationDumpDuration(); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IProcessStats param1IProcessStats) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IProcessStats != null) {
          Proxy.sDefaultImpl = param1IProcessStats;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IProcessStats getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
