package com.android.internal.app.procstats;

import android.os.Parcel;
import android.os.SystemClock;
import android.util.Slog;
import android.util.TimeUtils;
import android.util.proto.ProtoOutputStream;
import java.io.PrintWriter;

public final class ServiceState {
  private int mRunState = -1;
  
  private int mStartedState = -1;
  
  private int mBoundState = -1;
  
  private int mExecState = -1;
  
  private int mForegroundState = -1;
  
  private static final boolean DEBUG = false;
  
  public static final int SERVICE_BOUND = 2;
  
  public static final int SERVICE_COUNT = 5;
  
  public static final int SERVICE_EXEC = 3;
  
  public static final int SERVICE_FOREGROUND = 4;
  
  public static final int SERVICE_RUN = 0;
  
  public static final int SERVICE_STARTED = 1;
  
  private static final String TAG = "ProcessStats";
  
  private int mBoundCount;
  
  private long mBoundStartTime;
  
  private final DurationsTable mDurations;
  
  private int mExecCount;
  
  private long mExecStartTime;
  
  private int mForegroundCount;
  
  private long mForegroundStartTime;
  
  private final String mName;
  
  private Object mOwner;
  
  private final String mPackage;
  
  private ProcessState mProc;
  
  private final String mProcessName;
  
  private boolean mRestarting;
  
  private int mRunCount;
  
  private long mRunStartTime;
  
  private boolean mStarted;
  
  private int mStartedCount;
  
  private long mStartedStartTime;
  
  public ServiceState(ProcessStats paramProcessStats, String paramString1, String paramString2, String paramString3, ProcessState paramProcessState) {
    this.mPackage = paramString1;
    this.mName = paramString2;
    this.mProcessName = paramString3;
    this.mProc = paramProcessState;
    this.mDurations = new DurationsTable(paramProcessStats.mTableData);
  }
  
  public String getPackage() {
    return this.mPackage;
  }
  
  public String getProcessName() {
    return this.mProcessName;
  }
  
  public String getName() {
    return this.mName;
  }
  
  public ProcessState getProcess() {
    return this.mProc;
  }
  
  public void setProcess(ProcessState paramProcessState) {
    this.mProc = paramProcessState;
  }
  
  public void setMemFactor(int paramInt, long paramLong) {
    if (isRestarting()) {
      setRestarting(true, paramInt, paramLong);
    } else if (isInUse()) {
      if (this.mStartedState != -1)
        setStarted(true, paramInt, paramLong); 
      if (this.mBoundState != -1)
        setBound(true, paramInt, paramLong); 
      if (this.mExecState != -1)
        setExecuting(true, paramInt, paramLong); 
      if (this.mForegroundState != -1)
        setForeground(true, paramInt, paramLong); 
    } 
  }
  
  public void applyNewOwner(Object paramObject) {
    Object object = this.mOwner;
    if (object != paramObject)
      if (object == null) {
        this.mOwner = paramObject;
        this.mProc.incActiveServices(this.mName);
      } else {
        this.mOwner = paramObject;
        if (this.mStarted || this.mBoundState != -1 || this.mExecState != -1 || this.mForegroundState != -1) {
          long l = SystemClock.uptimeMillis();
          if (this.mStarted)
            setStarted(false, 0, l); 
          if (this.mBoundState != -1)
            setBound(false, 0, l); 
          if (this.mExecState != -1)
            setExecuting(false, 0, l); 
          if (this.mForegroundState != -1)
            setForeground(false, 0, l); 
        } 
      }  
  }
  
  public void clearCurrentOwner(Object paramObject, boolean paramBoolean) {
    if (this.mOwner == paramObject) {
      this.mProc.decActiveServices(this.mName);
      if (this.mStarted || this.mBoundState != -1 || this.mExecState != -1 || this.mForegroundState != -1) {
        long l = SystemClock.uptimeMillis();
        if (this.mStarted) {
          if (!paramBoolean) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Service owner ");
            stringBuilder.append(paramObject);
            stringBuilder.append(" cleared while started: pkg=");
            stringBuilder.append(this.mPackage);
            stringBuilder.append(" service=");
            stringBuilder.append(this.mName);
            stringBuilder.append(" proc=");
            stringBuilder.append(this.mProc);
            Slog.wtfStack("ProcessStats", stringBuilder.toString());
          } 
          setStarted(false, 0, l);
        } 
        if (this.mBoundState != -1) {
          if (!paramBoolean) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Service owner ");
            stringBuilder.append(paramObject);
            stringBuilder.append(" cleared while bound: pkg=");
            stringBuilder.append(this.mPackage);
            stringBuilder.append(" service=");
            stringBuilder.append(this.mName);
            stringBuilder.append(" proc=");
            stringBuilder.append(this.mProc);
            Slog.wtfStack("ProcessStats", stringBuilder.toString());
          } 
          setBound(false, 0, l);
        } 
        if (this.mExecState != -1) {
          if (!paramBoolean) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Service owner ");
            stringBuilder.append(paramObject);
            stringBuilder.append(" cleared while exec: pkg=");
            stringBuilder.append(this.mPackage);
            stringBuilder.append(" service=");
            stringBuilder.append(this.mName);
            stringBuilder.append(" proc=");
            stringBuilder.append(this.mProc);
            Slog.wtfStack("ProcessStats", stringBuilder.toString());
          } 
          setExecuting(false, 0, l);
        } 
        if (this.mForegroundState != -1) {
          if (!paramBoolean) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Service owner ");
            stringBuilder.append(paramObject);
            stringBuilder.append(" cleared while foreground: pkg=");
            stringBuilder.append(this.mPackage);
            stringBuilder.append(" service=");
            stringBuilder.append(this.mName);
            stringBuilder.append(" proc=");
            stringBuilder.append(this.mProc);
            Slog.wtfStack("ProcessStats", stringBuilder.toString());
          } 
          setForeground(false, 0, l);
        } 
      } 
      this.mOwner = null;
    } 
  }
  
  public boolean isInUse() {
    return (this.mOwner != null || this.mRestarting);
  }
  
  public boolean isRestarting() {
    return this.mRestarting;
  }
  
  public void add(ServiceState paramServiceState) {
    this.mDurations.addDurations(paramServiceState.mDurations);
    this.mRunCount += paramServiceState.mRunCount;
    this.mStartedCount += paramServiceState.mStartedCount;
    this.mBoundCount += paramServiceState.mBoundCount;
    this.mExecCount += paramServiceState.mExecCount;
    this.mForegroundCount += paramServiceState.mForegroundCount;
  }
  
  public void resetSafely(long paramLong) {
    this.mDurations.resetTable();
    int i = this.mRunState;
    boolean bool = true;
    if (i != -1) {
      i = 1;
    } else {
      i = 0;
    } 
    this.mRunCount = i;
    if (this.mStartedState != -1) {
      i = 1;
    } else {
      i = 0;
    } 
    this.mStartedCount = i;
    if (this.mBoundState != -1) {
      i = 1;
    } else {
      i = 0;
    } 
    this.mBoundCount = i;
    if (this.mExecState != -1) {
      i = 1;
    } else {
      i = 0;
    } 
    this.mExecCount = i;
    if (this.mForegroundState != -1) {
      i = bool;
    } else {
      i = 0;
    } 
    this.mForegroundCount = i;
    this.mForegroundStartTime = paramLong;
    this.mExecStartTime = paramLong;
    this.mBoundStartTime = paramLong;
    this.mStartedStartTime = paramLong;
    this.mRunStartTime = paramLong;
  }
  
  public void writeToParcel(Parcel paramParcel, long paramLong) {
    this.mDurations.writeToParcel(paramParcel);
    paramParcel.writeInt(this.mRunCount);
    paramParcel.writeInt(this.mStartedCount);
    paramParcel.writeInt(this.mBoundCount);
    paramParcel.writeInt(this.mExecCount);
    paramParcel.writeInt(this.mForegroundCount);
  }
  
  public boolean readFromParcel(Parcel paramParcel) {
    if (!this.mDurations.readFromParcel(paramParcel))
      return false; 
    this.mRunCount = paramParcel.readInt();
    this.mStartedCount = paramParcel.readInt();
    this.mBoundCount = paramParcel.readInt();
    this.mExecCount = paramParcel.readInt();
    this.mForegroundCount = paramParcel.readInt();
    return true;
  }
  
  public void commitStateTime(long paramLong) {
    int i = this.mRunState;
    if (i != -1) {
      this.mDurations.addDuration(i * 5 + 0, paramLong - this.mRunStartTime);
      this.mRunStartTime = paramLong;
    } 
    i = this.mStartedState;
    if (i != -1) {
      this.mDurations.addDuration(i * 5 + 1, paramLong - this.mStartedStartTime);
      this.mStartedStartTime = paramLong;
    } 
    i = this.mBoundState;
    if (i != -1) {
      this.mDurations.addDuration(i * 5 + 2, paramLong - this.mBoundStartTime);
      this.mBoundStartTime = paramLong;
    } 
    i = this.mExecState;
    if (i != -1) {
      this.mDurations.addDuration(i * 5 + 3, paramLong - this.mExecStartTime);
      this.mExecStartTime = paramLong;
    } 
    i = this.mForegroundState;
    if (i != -1) {
      this.mDurations.addDuration(i * 5 + 4, paramLong - this.mForegroundStartTime);
      this.mForegroundStartTime = paramLong;
    } 
  }
  
  private void updateRunning(int paramInt, long paramLong) {
    if (this.mStartedState == -1 && this.mBoundState == -1 && this.mExecState == -1 && this.mForegroundState == -1)
      paramInt = -1; 
    int i = this.mRunState;
    if (i != paramInt) {
      if (i != -1) {
        this.mDurations.addDuration(i * 5 + 0, paramLong - this.mRunStartTime);
      } else if (paramInt != -1) {
        this.mRunCount++;
      } 
      this.mRunState = paramInt;
      this.mRunStartTime = paramLong;
    } 
  }
  
  public void setStarted(boolean paramBoolean, int paramInt, long paramLong) {
    if (this.mOwner == null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Starting service ");
      stringBuilder.append(this);
      stringBuilder.append(" without owner");
      Slog.wtf("ProcessStats", stringBuilder.toString());
    } 
    this.mStarted = paramBoolean;
    updateStartedState(paramInt, paramLong);
  }
  
  public void setRestarting(boolean paramBoolean, int paramInt, long paramLong) {
    this.mRestarting = paramBoolean;
    updateStartedState(paramInt, paramLong);
  }
  
  public void updateStartedState(int paramInt, long paramLong) {
    byte b1;
    int i = this.mStartedState;
    byte b = 0;
    if (i != -1) {
      i = 1;
    } else {
      i = 0;
    } 
    if (this.mStarted || this.mRestarting)
      b = 1; 
    if (b) {
      b1 = paramInt;
    } else {
      b1 = -1;
    } 
    int j = this.mStartedState;
    if (j != b1) {
      if (j != -1) {
        this.mDurations.addDuration(j * 5 + 1, paramLong - this.mStartedStartTime);
      } else if (b) {
        this.mStartedCount++;
      } 
      this.mStartedState = b1;
      this.mStartedStartTime = paramLong;
      ProcessState processState = this.mProc.pullFixedProc(this.mPackage);
      if (i != b)
        if (b != 0) {
          processState.incStartedServices(paramInt, paramLong, this.mName);
        } else {
          processState.decStartedServices(paramInt, paramLong, this.mName);
        }  
      updateRunning(paramInt, paramLong);
    } 
  }
  
  public void setBound(boolean paramBoolean, int paramInt, long paramLong) {
    byte b;
    if (this.mOwner == null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Binding service ");
      stringBuilder.append(this);
      stringBuilder.append(" without owner");
      Slog.wtf("ProcessStats", stringBuilder.toString());
    } 
    if (paramBoolean) {
      b = paramInt;
    } else {
      b = -1;
    } 
    int i = this.mBoundState;
    if (i != b) {
      if (i != -1) {
        this.mDurations.addDuration(i * 5 + 2, paramLong - this.mBoundStartTime);
      } else if (paramBoolean) {
        this.mBoundCount++;
      } 
      this.mBoundState = b;
      this.mBoundStartTime = paramLong;
      updateRunning(paramInt, paramLong);
    } 
  }
  
  public void setExecuting(boolean paramBoolean, int paramInt, long paramLong) {
    byte b;
    if (this.mOwner == null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Executing service ");
      stringBuilder.append(this);
      stringBuilder.append(" without owner");
      Slog.wtf("ProcessStats", stringBuilder.toString());
    } 
    if (paramBoolean) {
      b = paramInt;
    } else {
      b = -1;
    } 
    int i = this.mExecState;
    if (i != b) {
      if (i != -1) {
        this.mDurations.addDuration(i * 5 + 3, paramLong - this.mExecStartTime);
      } else if (paramBoolean) {
        this.mExecCount++;
      } 
      this.mExecState = b;
      this.mExecStartTime = paramLong;
      updateRunning(paramInt, paramLong);
    } 
  }
  
  public void setForeground(boolean paramBoolean, int paramInt, long paramLong) {
    byte b;
    if (this.mOwner == null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Foregrounding service ");
      stringBuilder.append(this);
      stringBuilder.append(" without owner");
      Slog.wtf("ProcessStats", stringBuilder.toString());
    } 
    if (paramBoolean) {
      b = paramInt;
    } else {
      b = -1;
    } 
    int i = this.mForegroundState;
    if (i != b) {
      if (i != -1) {
        this.mDurations.addDuration(i * 5 + 4, paramLong - this.mForegroundStartTime);
      } else if (paramBoolean) {
        this.mForegroundCount++;
      } 
      this.mForegroundState = b;
      this.mForegroundStartTime = paramLong;
      updateRunning(paramInt, paramLong);
    } 
  }
  
  public long getDuration(int paramInt1, int paramInt2, long paramLong1, int paramInt3, long paramLong2) {
    long l1 = this.mDurations.getValueForId((byte)(paramInt3 * 5 + paramInt1));
    long l2 = l1;
    if (paramInt2 == paramInt3)
      l2 = l1 + paramLong2 - paramLong1; 
    return l2;
  }
  
  public void dumpStats(PrintWriter paramPrintWriter, String paramString1, String paramString2, String paramString3, long paramLong1, long paramLong2, boolean paramBoolean1, boolean paramBoolean2) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mRunCount : I
    //   4: istore #11
    //   6: aload_0
    //   7: getfield mRunState : I
    //   10: istore #12
    //   12: aload_0
    //   13: getfield mRunStartTime : J
    //   16: lstore #13
    //   18: iconst_0
    //   19: istore #15
    //   21: iload #9
    //   23: ifeq -> 40
    //   26: iload #10
    //   28: ifeq -> 34
    //   31: goto -> 40
    //   34: iconst_0
    //   35: istore #16
    //   37: goto -> 43
    //   40: iconst_1
    //   41: istore #16
    //   43: aload_0
    //   44: aload_1
    //   45: aload_2
    //   46: aload_3
    //   47: aload #4
    //   49: ldc_w 'Running'
    //   52: iload #11
    //   54: iconst_0
    //   55: iload #12
    //   57: lload #13
    //   59: lload #5
    //   61: lload #7
    //   63: iload #16
    //   65: invokespecial dumpStats : (Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIJJJZ)V
    //   68: aload_0
    //   69: getfield mStartedCount : I
    //   72: istore #12
    //   74: aload_0
    //   75: getfield mStartedState : I
    //   78: istore #11
    //   80: aload_0
    //   81: getfield mStartedStartTime : J
    //   84: lstore #13
    //   86: iload #9
    //   88: ifeq -> 105
    //   91: iload #10
    //   93: ifeq -> 99
    //   96: goto -> 105
    //   99: iconst_0
    //   100: istore #16
    //   102: goto -> 108
    //   105: iconst_1
    //   106: istore #16
    //   108: aload_0
    //   109: aload_1
    //   110: aload_2
    //   111: aload_3
    //   112: aload #4
    //   114: ldc_w 'Started'
    //   117: iload #12
    //   119: iconst_1
    //   120: iload #11
    //   122: lload #13
    //   124: lload #5
    //   126: lload #7
    //   128: iload #16
    //   130: invokespecial dumpStats : (Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIJJJZ)V
    //   133: aload_0
    //   134: getfield mForegroundCount : I
    //   137: istore #11
    //   139: aload_0
    //   140: getfield mForegroundState : I
    //   143: istore #12
    //   145: aload_0
    //   146: getfield mForegroundStartTime : J
    //   149: lstore #13
    //   151: iload #9
    //   153: ifeq -> 170
    //   156: iload #10
    //   158: ifeq -> 164
    //   161: goto -> 170
    //   164: iconst_0
    //   165: istore #16
    //   167: goto -> 173
    //   170: iconst_1
    //   171: istore #16
    //   173: aload_0
    //   174: aload_1
    //   175: aload_2
    //   176: aload_3
    //   177: aload #4
    //   179: ldc_w 'Foreground'
    //   182: iload #11
    //   184: iconst_4
    //   185: iload #12
    //   187: lload #13
    //   189: lload #5
    //   191: lload #7
    //   193: iload #16
    //   195: invokespecial dumpStats : (Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIJJJZ)V
    //   198: aload_0
    //   199: getfield mBoundCount : I
    //   202: istore #12
    //   204: aload_0
    //   205: getfield mBoundState : I
    //   208: istore #11
    //   210: aload_0
    //   211: getfield mBoundStartTime : J
    //   214: lstore #13
    //   216: iload #9
    //   218: ifeq -> 235
    //   221: iload #10
    //   223: ifeq -> 229
    //   226: goto -> 235
    //   229: iconst_0
    //   230: istore #16
    //   232: goto -> 238
    //   235: iconst_1
    //   236: istore #16
    //   238: aload_0
    //   239: aload_1
    //   240: aload_2
    //   241: aload_3
    //   242: aload #4
    //   244: ldc_w 'Bound'
    //   247: iload #12
    //   249: iconst_2
    //   250: iload #11
    //   252: lload #13
    //   254: lload #5
    //   256: lload #7
    //   258: iload #16
    //   260: invokespecial dumpStats : (Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIJJJZ)V
    //   263: aload_0
    //   264: getfield mExecCount : I
    //   267: istore #11
    //   269: aload_0
    //   270: getfield mExecState : I
    //   273: istore #12
    //   275: aload_0
    //   276: getfield mExecStartTime : J
    //   279: lstore #13
    //   281: iload #9
    //   283: ifeq -> 295
    //   286: iload #15
    //   288: istore #9
    //   290: iload #10
    //   292: ifeq -> 298
    //   295: iconst_1
    //   296: istore #9
    //   298: aload_0
    //   299: aload_1
    //   300: aload_2
    //   301: aload_3
    //   302: aload #4
    //   304: ldc_w 'Executing'
    //   307: iload #11
    //   309: iconst_3
    //   310: iload #12
    //   312: lload #13
    //   314: lload #5
    //   316: lload #7
    //   318: iload #9
    //   320: invokespecial dumpStats : (Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIJJJZ)V
    //   323: iload #10
    //   325: ifeq -> 400
    //   328: aload_0
    //   329: getfield mOwner : Ljava/lang/Object;
    //   332: ifnull -> 353
    //   335: aload_1
    //   336: ldc_w '        mOwner='
    //   339: invokevirtual print : (Ljava/lang/String;)V
    //   342: aload_1
    //   343: aload_0
    //   344: getfield mOwner : Ljava/lang/Object;
    //   347: invokevirtual println : (Ljava/lang/Object;)V
    //   350: goto -> 353
    //   353: aload_0
    //   354: getfield mStarted : Z
    //   357: ifne -> 367
    //   360: aload_0
    //   361: getfield mRestarting : Z
    //   364: ifeq -> 400
    //   367: aload_1
    //   368: ldc_w '        mStarted='
    //   371: invokevirtual print : (Ljava/lang/String;)V
    //   374: aload_1
    //   375: aload_0
    //   376: getfield mStarted : Z
    //   379: invokevirtual print : (Z)V
    //   382: aload_1
    //   383: ldc_w ' mRestarting='
    //   386: invokevirtual print : (Ljava/lang/String;)V
    //   389: aload_1
    //   390: aload_0
    //   391: getfield mRestarting : Z
    //   394: invokevirtual println : (Z)V
    //   397: goto -> 400
    //   400: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #403	-> 0
    //   #406	-> 68
    //   #409	-> 133
    //   #412	-> 198
    //   #415	-> 263
    //   #418	-> 323
    //   #419	-> 328
    //   #420	-> 335
    //   #419	-> 353
    //   #422	-> 353
    //   #423	-> 367
    //   #424	-> 382
    //   #418	-> 400
    //   #427	-> 400
  }
  
  private void dumpStats(PrintWriter paramPrintWriter, String paramString1, String paramString2, String paramString3, String paramString4, int paramInt1, int paramInt2, int paramInt3, long paramLong1, long paramLong2, long paramLong3, boolean paramBoolean) {
    if (paramInt1 != 0)
      if (paramBoolean) {
        paramPrintWriter.print(paramString1);
        paramPrintWriter.print(paramString4);
        paramPrintWriter.print(" op count ");
        paramPrintWriter.print(paramInt1);
        paramPrintWriter.println(":");
        dumpTime(paramPrintWriter, paramString2, paramInt2, paramInt3, paramLong1, paramLong2);
      } else {
        paramLong2 = dumpTimeInternal(null, null, paramInt2, paramInt3, paramLong1, paramLong2, true);
        paramPrintWriter.print(paramString1);
        paramPrintWriter.print(paramString3);
        paramPrintWriter.print(paramString4);
        paramPrintWriter.print(" count ");
        paramPrintWriter.print(paramInt1);
        paramPrintWriter.print(" / time ");
        if (paramLong2 < 0L) {
          paramInt1 = 1;
        } else {
          paramInt1 = 0;
        } 
        paramLong1 = paramLong2;
        if (paramInt1 != 0)
          paramLong1 = -paramLong2; 
        DumpUtils.printPercent(paramPrintWriter, paramLong1 / paramLong3);
        if (paramInt1 != 0)
          paramPrintWriter.print(" (running)"); 
        paramPrintWriter.println();
      }  
  }
  
  public long dumpTime(PrintWriter paramPrintWriter, String paramString, int paramInt1, int paramInt2, long paramLong1, long paramLong2) {
    return dumpTimeInternal(paramPrintWriter, paramString, paramInt1, paramInt2, paramLong1, paramLong2, false);
  }
  
  long dumpTimeInternal(PrintWriter paramPrintWriter, String paramString, int paramInt1, int paramInt2, long paramLong1, long paramLong2, boolean paramBoolean) {
    long l = 0L;
    byte b1 = -1;
    byte b2 = 0;
    for (byte b = 0; b < 8; b += 4, l = l1) {
      int i = -1;
      long l1;
      for (byte b3 = 0; b3 < 4; b3++, l1 = l, b1 = b2, b2 = b4, i = j) {
        int j = b3 + b;
        long l2 = getDuration(paramInt1, paramInt2, paramLong1, j, paramLong2);
        String str1 = "";
        byte b4 = b2;
        String str2 = str1;
        if (paramInt2 == j) {
          b4 = b2;
          str2 = str1;
          if (paramPrintWriter != null) {
            str2 = " (running)";
            b4 = 1;
          } 
        } 
        l = l1;
        b2 = b1;
        j = i;
        if (l2 != 0L) {
          b2 = b1;
          j = i;
          if (paramPrintWriter != null) {
            paramPrintWriter.print(paramString);
            if (b1 != b) {
              b1 = b;
            } else {
              b1 = -1;
            } 
            DumpUtils.printScreenLabel(paramPrintWriter, b1);
            b2 = b;
            if (i != b3) {
              i = b3;
            } else {
              i = -1;
            } 
            DumpUtils.printMemLabel(paramPrintWriter, i, false);
            j = b3;
            paramPrintWriter.print(": ");
            TimeUtils.formatDuration(l2, paramPrintWriter);
            paramPrintWriter.println(str2);
          } 
          l = l1 + l2;
        } 
      } 
    } 
    if (l != 0L && paramPrintWriter != null) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("    TOTAL: ");
      TimeUtils.formatDuration(l, paramPrintWriter);
      paramPrintWriter.println();
    } 
    if (b2 != 0 && paramBoolean)
      l = -l; 
    return l;
  }
  
  public void dumpTimesCheckin(PrintWriter paramPrintWriter, String paramString1, int paramInt, long paramLong1, String paramString2, long paramLong2) {
    dumpTimeCheckin(paramPrintWriter, "pkgsvc-run", paramString1, paramInt, paramLong1, paramString2, 0, this.mRunCount, this.mRunState, this.mRunStartTime, paramLong2);
    dumpTimeCheckin(paramPrintWriter, "pkgsvc-start", paramString1, paramInt, paramLong1, paramString2, 1, this.mStartedCount, this.mStartedState, this.mStartedStartTime, paramLong2);
    dumpTimeCheckin(paramPrintWriter, "pkgsvc-fg", paramString1, paramInt, paramLong1, paramString2, 4, this.mForegroundCount, this.mForegroundState, this.mForegroundStartTime, paramLong2);
    dumpTimeCheckin(paramPrintWriter, "pkgsvc-bound", paramString1, paramInt, paramLong1, paramString2, 2, this.mBoundCount, this.mBoundState, this.mBoundStartTime, paramLong2);
    dumpTimeCheckin(paramPrintWriter, "pkgsvc-exec", paramString1, paramInt, paramLong1, paramString2, 3, this.mExecCount, this.mExecState, this.mExecStartTime, paramLong2);
  }
  
  private void dumpTimeCheckin(PrintWriter paramPrintWriter, String paramString1, String paramString2, int paramInt1, long paramLong1, String paramString3, int paramInt2, int paramInt3, int paramInt4, long paramLong2, long paramLong3) {
    if (paramInt3 <= 0)
      return; 
    paramPrintWriter.print(paramString1);
    paramPrintWriter.print(",");
    paramPrintWriter.print(paramString2);
    paramPrintWriter.print(",");
    paramPrintWriter.print(paramInt1);
    paramPrintWriter.print(",");
    paramPrintWriter.print(paramLong1);
    paramPrintWriter.print(",");
    paramPrintWriter.print(paramString3);
    paramPrintWriter.print(",");
    paramPrintWriter.print(paramInt3);
    paramInt1 = 0;
    int i = this.mDurations.getKeyCount();
    for (paramInt3 = 0; paramInt3 < i; paramInt3++) {
      int j = this.mDurations.getKeyAt(paramInt3);
      long l = this.mDurations.getValue(j);
      byte b = SparseMappingTable.getIdFromKey(j);
      j = b / 5;
      if (b % 5 == paramInt2) {
        paramLong1 = l;
        if (paramInt4 == j) {
          paramInt1 = 1;
          paramLong1 = l + paramLong3 - paramLong2;
        } 
        DumpUtils.printAdjTagAndValue(paramPrintWriter, j, paramLong1);
      } 
    } 
    if (paramInt1 == 0 && paramInt4 != -1)
      DumpUtils.printAdjTagAndValue(paramPrintWriter, paramInt4, paramLong3 - paramLong2); 
    paramPrintWriter.println();
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong1, long paramLong2) {
    paramLong1 = paramProtoOutputStream.start(paramLong1);
    paramProtoOutputStream.write(1138166333441L, this.mName);
    writeTypeToProto(paramProtoOutputStream, 2246267895810L, 1, 0, this.mRunCount, this.mRunState, this.mRunStartTime, paramLong2);
    writeTypeToProto(paramProtoOutputStream, 2246267895810L, 2, 1, this.mStartedCount, this.mStartedState, this.mStartedStartTime, paramLong2);
    writeTypeToProto(paramProtoOutputStream, 2246267895810L, 3, 4, this.mForegroundCount, this.mForegroundState, this.mForegroundStartTime, paramLong2);
    writeTypeToProto(paramProtoOutputStream, 2246267895810L, 4, 2, this.mBoundCount, this.mBoundState, this.mBoundStartTime, paramLong2);
    writeTypeToProto(paramProtoOutputStream, 2246267895810L, 5, 3, this.mExecCount, this.mExecState, this.mExecStartTime, paramLong2);
    paramProtoOutputStream.end(paramLong1);
  }
  
  public void writeTypeToProto(ProtoOutputStream paramProtoOutputStream, long paramLong1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, long paramLong2, long paramLong3) {
    if (paramInt3 <= 0)
      return; 
    long l = paramProtoOutputStream.start(paramLong1);
    paramProtoOutputStream.write(1159641169921L, paramInt1);
    paramProtoOutputStream.write(1120986464258L, paramInt3);
    int i = this.mDurations.getKeyCount();
    for (paramInt1 = 0, paramInt3 = 0; paramInt3 < i; paramInt3++) {
      int j = this.mDurations.getKeyAt(paramInt3);
      paramLong1 = this.mDurations.getValue(j);
      j = SparseMappingTable.getIdFromKey(j);
      int k = j / 5;
      j %= 5;
      if (j == paramInt2) {
        if (paramInt4 == k) {
          paramInt1 = 1;
          paramLong1 += paramLong3 - paramLong2;
        } 
        long l1 = paramProtoOutputStream.start(2246267895811L);
        DumpUtils.printProcStateAdjTagProto(paramProtoOutputStream, 1159641169921L, 1159641169922L, j);
        paramProtoOutputStream.write(1112396529667L, paramLong1);
        paramProtoOutputStream.end(l1);
      } 
    } 
    if (paramInt1 == 0 && paramInt4 != -1) {
      paramLong1 = paramProtoOutputStream.start(2246267895811L);
      DumpUtils.printProcStateAdjTagProto(paramProtoOutputStream, 1159641169921L, 1159641169922L, paramInt4);
      paramProtoOutputStream.write(1112396529667L, paramLong3 - paramLong2);
      paramProtoOutputStream.end(paramLong1);
    } 
    paramProtoOutputStream.end(l);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ServiceState{");
    stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
    stringBuilder.append(" ");
    stringBuilder.append(this.mName);
    stringBuilder.append(" pkg=");
    stringBuilder.append(this.mPackage);
    stringBuilder.append(" proc=");
    ProcessState processState = this.mProc;
    stringBuilder.append(Integer.toHexString(System.identityHashCode(processState)));
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
}
