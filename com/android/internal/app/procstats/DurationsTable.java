package com.android.internal.app.procstats;

public class DurationsTable extends SparseMappingTable.Table {
  public DurationsTable(SparseMappingTable paramSparseMappingTable) {
    super(paramSparseMappingTable);
  }
  
  public void addDurations(DurationsTable paramDurationsTable) {
    int i = paramDurationsTable.getKeyCount();
    for (byte b = 0; b < i; b++) {
      int j = paramDurationsTable.getKeyAt(b);
      addDuration(SparseMappingTable.getIdFromKey(j), paramDurationsTable.getValue(j));
    } 
  }
  
  public void addDuration(int paramInt, long paramLong) {
    paramInt = getOrAddKey((byte)paramInt, 1);
    setValue(paramInt, getValue(paramInt) + paramLong);
  }
}
