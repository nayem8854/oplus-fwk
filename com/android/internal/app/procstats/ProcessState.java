package com.android.internal.app.procstats;

import android.os.Parcel;
import android.os.SystemClock;
import android.os.UserHandle;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.ArraySet;
import android.util.DebugUtils;
import android.util.Log;
import android.util.LongSparseArray;
import android.util.Slog;
import android.util.SparseArray;
import android.util.SparseLongArray;
import android.util.TimeUtils;
import android.util.proto.ProtoOutputStream;
import android.util.proto.ProtoUtils;
import com.android.internal.app.ProcessMap;
import java.io.PrintWriter;
import java.util.Comparator;

public final class ProcessState {
  static final int[] PROCESS_STATE_TO_STATE = new int[] { 
      0, 0, 1, 2, 2, 2, 2, 3, 3, 4, 
      5, 7, 1, 8, 9, 10, 11, 12, 11, 13 };
  
  static {
    COMPARATOR = new Comparator<ProcessState>() {
        public int compare(ProcessState param1ProcessState1, ProcessState param1ProcessState2) {
          if (param1ProcessState1.mTmpTotalTime < param1ProcessState2.mTmpTotalTime)
            return -1; 
          if (param1ProcessState1.mTmpTotalTime > param1ProcessState2.mTmpTotalTime)
            return 1; 
          return 0;
        }
      };
  }
  
  static class PssAggr {
    long pss;
    
    long samples;
    
    PssAggr() {
      this.pss = 0L;
      this.samples = 0L;
    }
    
    void add(long param1Long1, long param1Long2) {
      double d = this.pss;
      long l = this.samples;
      this.pss = (long)(d * l + param1Long1 * param1Long2) / (l + param1Long2);
      this.samples = l + param1Long2;
    }
  }
  
  private final long[] mTotalRunningPss = new long[10];
  
  private int mCurCombinedState = -1;
  
  private int mLastPssState = -1;
  
  public static final Comparator<ProcessState> COMPARATOR;
  
  private static final boolean DEBUG = false;
  
  private static final boolean DEBUG_PARCEL = false;
  
  private static final String TAG = "ProcessStats";
  
  private boolean mActive;
  
  private long mAvgCachedKillPss;
  
  private ProcessState mCommonProcess;
  
  private boolean mDead;
  
  private final DurationsTable mDurations;
  
  private long mLastPssTime;
  
  private long mMaxCachedKillPss;
  
  private long mMinCachedKillPss;
  
  private boolean mMultiPackage;
  
  private final String mName;
  
  private int mNumActiveServices;
  
  private int mNumCachedKill;
  
  private int mNumExcessiveCpu;
  
  private int mNumStartedServices;
  
  private final String mPackage;
  
  private final PssTable mPssTable;
  
  private long mStartTime;
  
  private final ProcessStats mStats;
  
  private long mTmpTotalTime;
  
  private long mTotalRunningDuration;
  
  private long mTotalRunningStartTime;
  
  private final int mUid;
  
  private final long mVersion;
  
  public ProcessState tmpFoundSubProc;
  
  public int tmpNumInUse;
  
  public ProcessState(ProcessStats paramProcessStats, String paramString1, int paramInt, long paramLong, String paramString2) {
    this.mStats = paramProcessStats;
    this.mName = paramString2;
    this.mCommonProcess = this;
    this.mPackage = paramString1;
    this.mUid = paramInt;
    this.mVersion = paramLong;
    this.mDurations = new DurationsTable(paramProcessStats.mTableData);
    this.mPssTable = new PssTable(paramProcessStats.mTableData);
  }
  
  public ProcessState(ProcessState paramProcessState, String paramString1, int paramInt, long paramLong1, String paramString2, long paramLong2) {
    this.mStats = paramProcessState.mStats;
    this.mName = paramString2;
    this.mCommonProcess = paramProcessState;
    this.mPackage = paramString1;
    this.mUid = paramInt;
    this.mVersion = paramLong1;
    this.mCurCombinedState = paramInt = paramProcessState.mCurCombinedState;
    this.mStartTime = paramLong2;
    if (paramInt != -1)
      this.mTotalRunningStartTime = paramLong2; 
    this.mDurations = new DurationsTable(paramProcessState.mStats.mTableData);
    this.mPssTable = new PssTable(paramProcessState.mStats.mTableData);
  }
  
  public ProcessState clone(long paramLong) {
    ProcessState processState = new ProcessState(this, this.mPackage, this.mUid, this.mVersion, this.mName, paramLong);
    processState.mDurations.addDurations(this.mDurations);
    processState.mPssTable.copyFrom(this.mPssTable, 10);
    System.arraycopy(this.mTotalRunningPss, 0, processState.mTotalRunningPss, 0, 10);
    processState.mTotalRunningDuration = getTotalRunningDuration(paramLong);
    processState.mNumExcessiveCpu = this.mNumExcessiveCpu;
    processState.mNumCachedKill = this.mNumCachedKill;
    processState.mMinCachedKillPss = this.mMinCachedKillPss;
    processState.mAvgCachedKillPss = this.mAvgCachedKillPss;
    processState.mMaxCachedKillPss = this.mMaxCachedKillPss;
    processState.mActive = this.mActive;
    processState.mNumActiveServices = this.mNumActiveServices;
    processState.mNumStartedServices = this.mNumStartedServices;
    return processState;
  }
  
  public String getName() {
    return this.mName;
  }
  
  public ProcessState getCommonProcess() {
    return this.mCommonProcess;
  }
  
  public void makeStandalone() {
    this.mCommonProcess = this;
  }
  
  public String getPackage() {
    return this.mPackage;
  }
  
  public int getUid() {
    return this.mUid;
  }
  
  public long getVersion() {
    return this.mVersion;
  }
  
  public boolean isMultiPackage() {
    return this.mMultiPackage;
  }
  
  public void setMultiPackage(boolean paramBoolean) {
    this.mMultiPackage = paramBoolean;
  }
  
  public int getDurationsBucketCount() {
    return this.mDurations.getKeyCount();
  }
  
  public void add(ProcessState paramProcessState) {
    this.mDurations.addDurations(paramProcessState.mDurations);
    this.mPssTable.mergeStats(paramProcessState.mPssTable);
    this.mNumExcessiveCpu += paramProcessState.mNumExcessiveCpu;
    int i = paramProcessState.mNumCachedKill;
    if (i > 0)
      addCachedKill(i, paramProcessState.mMinCachedKillPss, paramProcessState.mAvgCachedKillPss, paramProcessState.mMaxCachedKillPss); 
  }
  
  public void resetSafely(long paramLong) {
    this.mDurations.resetTable();
    this.mPssTable.resetTable();
    this.mStartTime = paramLong;
    this.mLastPssState = -1;
    this.mLastPssTime = 0L;
    this.mNumExcessiveCpu = 0;
    this.mNumCachedKill = 0;
    this.mMaxCachedKillPss = 0L;
    this.mAvgCachedKillPss = 0L;
    this.mMinCachedKillPss = 0L;
  }
  
  public void makeDead() {
    this.mDead = true;
  }
  
  private void ensureNotDead() {
    if (!this.mDead)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ProcessState dead: name=");
    stringBuilder.append(this.mName);
    stringBuilder.append(" pkg=");
    stringBuilder.append(this.mPackage);
    stringBuilder.append(" uid=");
    stringBuilder.append(this.mUid);
    stringBuilder.append(" common.name=");
    stringBuilder.append(this.mCommonProcess.mName);
    Slog.w("ProcessStats", stringBuilder.toString());
  }
  
  public void writeToParcel(Parcel paramParcel, long paramLong) {
    paramParcel.writeInt(this.mMultiPackage);
    this.mDurations.writeToParcel(paramParcel);
    this.mPssTable.writeToParcel(paramParcel);
    for (byte b = 0; b < 10; b++)
      paramParcel.writeLong(this.mTotalRunningPss[b]); 
    paramParcel.writeLong(getTotalRunningDuration(paramLong));
    paramParcel.writeInt(0);
    paramParcel.writeInt(this.mNumExcessiveCpu);
    paramParcel.writeInt(this.mNumCachedKill);
    if (this.mNumCachedKill > 0) {
      paramParcel.writeLong(this.mMinCachedKillPss);
      paramParcel.writeLong(this.mAvgCachedKillPss);
      paramParcel.writeLong(this.mMaxCachedKillPss);
    } 
  }
  
  public boolean readFromParcel(Parcel paramParcel, boolean paramBoolean) {
    boolean bool;
    if (paramParcel.readInt() != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    if (paramBoolean)
      this.mMultiPackage = bool; 
    if (!this.mDurations.readFromParcel(paramParcel))
      return false; 
    if (!this.mPssTable.readFromParcel(paramParcel))
      return false; 
    int i;
    for (i = 0; i < 10; i++)
      this.mTotalRunningPss[i] = paramParcel.readLong(); 
    this.mTotalRunningDuration = paramParcel.readLong();
    paramParcel.readInt();
    this.mNumExcessiveCpu = paramParcel.readInt();
    this.mNumCachedKill = i = paramParcel.readInt();
    if (i > 0) {
      this.mMinCachedKillPss = paramParcel.readLong();
      this.mAvgCachedKillPss = paramParcel.readLong();
      this.mMaxCachedKillPss = paramParcel.readLong();
    } else {
      this.mMaxCachedKillPss = 0L;
      this.mAvgCachedKillPss = 0L;
      this.mMinCachedKillPss = 0L;
    } 
    return true;
  }
  
  public void makeActive() {
    ensureNotDead();
    this.mActive = true;
  }
  
  public void makeInactive() {
    this.mActive = false;
  }
  
  public boolean isInUse() {
    return (this.mActive || this.mNumActiveServices > 0 || this.mNumStartedServices > 0 || this.mCurCombinedState != -1);
  }
  
  public boolean isActive() {
    return this.mActive;
  }
  
  public boolean hasAnyData() {
    int i = this.mDurations.getKeyCount();
    null = false;
    if (i == 0 && this.mCurCombinedState == -1) {
      PssTable pssTable = this.mPssTable;
      return 
        
        (pssTable.getKeyCount() != 0 || this.mTotalRunningPss[0] != 0L) ? true : null;
    } 
    return true;
  }
  
  public void setState(int paramInt1, int paramInt2, long paramLong, ArrayMap<String, ProcessStats.ProcessStateHolder> paramArrayMap) {
    if (paramInt1 < 0) {
      if (this.mNumStartedServices > 0) {
        paramInt1 = paramInt2 * 14 + 6;
      } else {
        paramInt1 = -1;
      } 
    } else {
      paramInt1 = PROCESS_STATE_TO_STATE[paramInt1] + paramInt2 * 14;
    } 
    this.mCommonProcess.setCombinedState(paramInt1, paramLong);
    if (!this.mCommonProcess.mMultiPackage)
      return; 
    if (paramArrayMap != null)
      for (paramInt2 = paramArrayMap.size() - 1; paramInt2 >= 0; paramInt2--)
        pullFixedProc(paramArrayMap, paramInt2).setCombinedState(paramInt1, paramLong);  
  }
  
  public void setCombinedState(int paramInt, long paramLong) {
    ensureNotDead();
    if (!this.mDead && this.mCurCombinedState != paramInt) {
      commitStateTime(paramLong);
      if (paramInt == -1) {
        this.mTotalRunningDuration += paramLong - this.mTotalRunningStartTime;
        this.mTotalRunningStartTime = 0L;
      } else if (this.mCurCombinedState == -1) {
        this.mTotalRunningDuration = 0L;
        this.mTotalRunningStartTime = paramLong;
        for (byte b = 9; b >= 0; b--)
          this.mTotalRunningPss[b] = 0L; 
      } 
      this.mCurCombinedState = paramInt;
    } 
  }
  
  public int getCombinedState() {
    return this.mCurCombinedState;
  }
  
  public void commitStateTime(long paramLong) {
    int i = this.mCurCombinedState;
    if (i != -1) {
      long l = paramLong - this.mStartTime;
      if (l > 0L)
        this.mDurations.addDuration(i, l); 
      this.mTotalRunningDuration += paramLong - this.mTotalRunningStartTime;
      this.mTotalRunningStartTime = paramLong;
    } 
    this.mStartTime = paramLong;
  }
  
  public void incActiveServices(String paramString) {
    ProcessState processState = this.mCommonProcess;
    if (processState != this)
      processState.incActiveServices(paramString); 
    this.mNumActiveServices++;
  }
  
  public void decActiveServices(String paramString) {
    ProcessState processState = this.mCommonProcess;
    if (processState != this)
      processState.decActiveServices(paramString); 
    int i = this.mNumActiveServices - 1;
    if (i < 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Proc active services underrun: pkg=");
      stringBuilder.append(this.mPackage);
      stringBuilder.append(" uid=");
      stringBuilder.append(this.mUid);
      stringBuilder.append(" proc=");
      stringBuilder.append(this.mName);
      stringBuilder.append(" service=");
      stringBuilder.append(paramString);
      Slog.wtfStack("ProcessStats", stringBuilder.toString());
      this.mNumActiveServices = 0;
    } 
  }
  
  public void incStartedServices(int paramInt, long paramLong, String paramString) {
    ProcessState processState = this.mCommonProcess;
    if (processState != this)
      processState.incStartedServices(paramInt, paramLong, paramString); 
    int i = this.mNumStartedServices + 1;
    if (i == 1 && this.mCurCombinedState == -1)
      setCombinedState(paramInt * 14 + 6, paramLong); 
  }
  
  public void decStartedServices(int paramInt, long paramLong, String paramString) {
    ProcessState processState = this.mCommonProcess;
    if (processState != this)
      processState.decStartedServices(paramInt, paramLong, paramString); 
    this.mNumStartedServices = paramInt = this.mNumStartedServices - 1;
    if (paramInt == 0 && this.mCurCombinedState % 14 == 6) {
      setCombinedState(-1, paramLong);
    } else if (this.mNumStartedServices < 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Proc started services underrun: pkg=");
      stringBuilder.append(this.mPackage);
      stringBuilder.append(" uid=");
      stringBuilder.append(this.mUid);
      stringBuilder.append(" name=");
      stringBuilder.append(this.mName);
      Slog.wtfStack("ProcessStats", stringBuilder.toString());
      this.mNumStartedServices = 0;
    } 
  }
  
  public void addPss(long paramLong1, long paramLong2, long paramLong3, boolean paramBoolean, int paramInt, long paramLong4, ArrayMap<String, ProcessStats.ProcessStateHolder> paramArrayMap) {
    ensureNotDead();
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt == 4) {
              ProcessStats processStats = this.mStats;
              processStats.mExternalSlowPssCount++;
              processStats = this.mStats;
              processStats.mExternalSlowPssTime += paramLong4;
            } 
          } else {
            ProcessStats processStats = this.mStats;
            processStats.mExternalPssCount++;
            processStats = this.mStats;
            processStats.mExternalPssTime += paramLong4;
          } 
        } else {
          ProcessStats processStats = this.mStats;
          processStats.mInternalAllPollPssCount++;
          processStats = this.mStats;
          processStats.mInternalAllPollPssTime += paramLong4;
        } 
      } else {
        ProcessStats processStats = this.mStats;
        processStats.mInternalAllMemPssCount++;
        processStats = this.mStats;
        processStats.mInternalAllMemPssTime += paramLong4;
      } 
    } else {
      ProcessStats processStats = this.mStats;
      processStats.mInternalSinglePssCount++;
      processStats = this.mStats;
      processStats.mInternalSinglePssTime += paramLong4;
    } 
    if (!paramBoolean && 
      this.mLastPssState == this.mCurCombinedState && SystemClock.uptimeMillis() < this.mLastPssTime + 30000L)
      return; 
    this.mLastPssState = this.mCurCombinedState;
    this.mLastPssTime = SystemClock.uptimeMillis();
    paramInt = this.mCurCombinedState;
    if (paramInt != -1) {
      this.mCommonProcess.mPssTable.mergeStats(paramInt, 1, paramLong1, paramLong1, paramLong1, paramLong2, paramLong2, paramLong2, paramLong3, paramLong3, paramLong3);
      PssTable.mergeStats(this.mCommonProcess.mTotalRunningPss, 0, 1, paramLong1, paramLong1, paramLong1, paramLong2, paramLong2, paramLong2, paramLong3, paramLong3, paramLong3);
      if (!this.mCommonProcess.mMultiPackage)
        return; 
      if (paramArrayMap != null)
        for (paramInt = paramArrayMap.size() - 1; paramInt >= 0; paramInt--) {
          ProcessState processState = pullFixedProc(paramArrayMap, paramInt);
          processState.mPssTable.mergeStats(this.mCurCombinedState, 1, paramLong1, paramLong1, paramLong1, paramLong2, paramLong2, paramLong2, paramLong3, paramLong3, paramLong3);
          PssTable.mergeStats(processState.mTotalRunningPss, 0, 1, paramLong1, paramLong1, paramLong1, paramLong2, paramLong2, paramLong2, paramLong3, paramLong3, paramLong3);
        }  
    } 
  }
  
  public void reportExcessiveCpu(ArrayMap<String, ProcessStats.ProcessStateHolder> paramArrayMap) {
    ensureNotDead();
    ProcessState processState = this.mCommonProcess;
    processState.mNumExcessiveCpu++;
    if (!processState.mMultiPackage)
      return; 
    for (int i = paramArrayMap.size() - 1; i >= 0; i--) {
      processState = pullFixedProc(paramArrayMap, i);
      processState.mNumExcessiveCpu++;
    } 
  }
  
  private void addCachedKill(int paramInt, long paramLong1, long paramLong2, long paramLong3) {
    if (this.mNumCachedKill <= 0) {
      this.mNumCachedKill = paramInt;
      this.mMinCachedKillPss = paramLong1;
      this.mAvgCachedKillPss = paramLong2;
      this.mMaxCachedKillPss = paramLong3;
    } else {
      if (paramLong1 < this.mMinCachedKillPss)
        this.mMinCachedKillPss = paramLong1; 
      if (paramLong3 > this.mMaxCachedKillPss)
        this.mMaxCachedKillPss = paramLong3; 
      double d = this.mAvgCachedKillPss;
      int i = this.mNumCachedKill;
      this.mAvgCachedKillPss = (long)((d * i + paramLong2) / (i + paramInt));
      this.mNumCachedKill = i + paramInt;
    } 
  }
  
  public void reportCachedKill(ArrayMap<String, ProcessStats.ProcessStateHolder> paramArrayMap, long paramLong) {
    ensureNotDead();
    this.mCommonProcess.addCachedKill(1, paramLong, paramLong, paramLong);
    if (!this.mCommonProcess.mMultiPackage)
      return; 
    for (int i = paramArrayMap.size() - 1; i >= 0; i--)
      pullFixedProc(paramArrayMap, i).addCachedKill(1, paramLong, paramLong, paramLong); 
  }
  
  public ProcessState pullFixedProc(String paramString) {
    if (this.mMultiPackage) {
      LongSparseArray<ProcessStats.PackageState> longSparseArray = this.mStats.mPackages.get(paramString, this.mUid);
      if (longSparseArray != null) {
        ProcessStats.PackageState packageState = longSparseArray.get(this.mVersion);
        if (packageState != null) {
          ProcessState processState = packageState.mProcesses.get(this.mName);
          if (processState != null)
            return processState; 
          StringBuilder stringBuilder2 = new StringBuilder();
          stringBuilder2.append("Didn't create per-package process ");
          stringBuilder2.append(this.mName);
          stringBuilder2.append(" in pkg ");
          stringBuilder2.append(paramString);
          stringBuilder2.append(" / ");
          stringBuilder2.append(this.mUid);
          stringBuilder2.append(" vers ");
          stringBuilder2.append(this.mVersion);
          throw new IllegalStateException(stringBuilder2.toString());
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Didn't find package ");
        stringBuilder1.append(paramString);
        stringBuilder1.append(" / ");
        stringBuilder1.append(this.mUid);
        stringBuilder1.append(" vers ");
        stringBuilder1.append(this.mVersion);
        throw new IllegalStateException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Didn't find package ");
      stringBuilder.append(paramString);
      stringBuilder.append(" / ");
      stringBuilder.append(this.mUid);
      throw new IllegalStateException(stringBuilder.toString());
    } 
    return this;
  }
  
  private ProcessState pullFixedProc(ArrayMap<String, ProcessStats.ProcessStateHolder> paramArrayMap, int paramInt) {
    StringBuilder stringBuilder;
    ProcessStats.ProcessStateHolder processStateHolder = paramArrayMap.valueAt(paramInt);
    ProcessState processState1 = processStateHolder.state;
    ProcessState processState2 = processState1;
    if (this.mDead) {
      processState2 = processState1;
      if (processState1.mCommonProcess != processState1) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Pulling dead proc: name=");
        stringBuilder1.append(this.mName);
        stringBuilder1.append(" pkg=");
        stringBuilder1.append(this.mPackage);
        stringBuilder1.append(" uid=");
        stringBuilder1.append(this.mUid);
        stringBuilder1.append(" common.name=");
        stringBuilder1.append(this.mCommonProcess.mName);
        Log.wtf("ProcessStats", stringBuilder1.toString());
        processState2 = this.mStats.getProcessStateLocked(processState1.mPackage, processState1.mUid, processState1.mVersion, processState1.mName);
      } 
    } 
    processState1 = processState2;
    if (processState2.mMultiPackage) {
      String str;
      StringBuilder stringBuilder1;
      LongSparseArray<ProcessStats.PackageState> longSparseArray = this.mStats.mPackages.get(paramArrayMap.keyAt(paramInt), processState2.mUid);
      if (longSparseArray != null) {
        ProcessStats.PackageState packageState = longSparseArray.get(processState2.mVersion);
        if (packageState != null) {
          str = processState2.mName;
          ProcessState processState = packageState.mProcesses.get(processState2.mName);
          if (processState != null) {
            processStateHolder.state = processState;
          } else {
            stringBuilder1 = new StringBuilder();
            stringBuilder1.append("Didn't create per-package process ");
            stringBuilder1.append(str);
            stringBuilder1.append(" in pkg ");
            stringBuilder1.append(packageState.mPackageName);
            stringBuilder1.append("/");
            stringBuilder1.append(packageState.mUid);
            throw new IllegalStateException(stringBuilder1.toString());
          } 
        } else {
          stringBuilder = new StringBuilder();
          stringBuilder.append("No existing package ");
          stringBuilder.append((String)str.keyAt(paramInt));
          stringBuilder.append("/");
          stringBuilder.append(((ProcessState)stringBuilder1).mUid);
          stringBuilder.append(" for multi-proc ");
          stringBuilder.append(((ProcessState)stringBuilder1).mName);
          stringBuilder.append(" version ");
          stringBuilder.append(((ProcessState)stringBuilder1).mVersion);
          throw new IllegalStateException(stringBuilder.toString());
        } 
      } else {
        stringBuilder = new StringBuilder();
        stringBuilder.append("No existing package ");
        stringBuilder.append((String)str.keyAt(paramInt));
        stringBuilder.append("/");
        stringBuilder.append(((ProcessState)stringBuilder1).mUid);
        stringBuilder.append(" for multi-proc ");
        stringBuilder.append(((ProcessState)stringBuilder1).mName);
        throw new IllegalStateException(stringBuilder.toString());
      } 
    } 
    return (ProcessState)stringBuilder;
  }
  
  public long getTotalRunningDuration(long paramLong) {
    long l1 = this.mTotalRunningDuration;
    long l2 = this.mTotalRunningStartTime, l3 = 0L;
    if (l2 != 0L)
      l3 = paramLong - l2; 
    return l1 + l3;
  }
  
  public long getDuration(int paramInt, long paramLong) {
    long l1 = this.mDurations.getValueForId((byte)paramInt);
    long l2 = l1;
    if (this.mCurCombinedState == paramInt)
      l2 = l1 + paramLong - this.mStartTime; 
    return l2;
  }
  
  public long getPssSampleCount(int paramInt) {
    return this.mPssTable.getValueForId((byte)paramInt, 0);
  }
  
  public long getPssMinimum(int paramInt) {
    return this.mPssTable.getValueForId((byte)paramInt, 1);
  }
  
  public long getPssAverage(int paramInt) {
    return this.mPssTable.getValueForId((byte)paramInt, 2);
  }
  
  public long getPssMaximum(int paramInt) {
    return this.mPssTable.getValueForId((byte)paramInt, 3);
  }
  
  public long getPssUssMinimum(int paramInt) {
    return this.mPssTable.getValueForId((byte)paramInt, 4);
  }
  
  public long getPssUssAverage(int paramInt) {
    return this.mPssTable.getValueForId((byte)paramInt, 5);
  }
  
  public long getPssUssMaximum(int paramInt) {
    return this.mPssTable.getValueForId((byte)paramInt, 6);
  }
  
  public long getPssRssMinimum(int paramInt) {
    return this.mPssTable.getValueForId((byte)paramInt, 7);
  }
  
  public long getPssRssAverage(int paramInt) {
    return this.mPssTable.getValueForId((byte)paramInt, 8);
  }
  
  public long getPssRssMaximum(int paramInt) {
    return this.mPssTable.getValueForId((byte)paramInt, 9);
  }
  
  public void aggregatePss(ProcessStats.TotalMemoryUseCollection paramTotalMemoryUseCollection, long paramLong) {
    PssAggr pssAggr1 = new PssAggr();
    PssAggr pssAggr2 = new PssAggr();
    PssAggr pssAggr3 = new PssAggr();
    boolean bool = false;
    byte b1;
    for (b1 = 0; b1 < this.mDurations.getKeyCount(); b1++) {
      int i = this.mDurations.getKeyAt(b1);
      byte b = SparseMappingTable.getIdFromKey(i);
      i = b % 14;
      long l = getPssSampleCount(b);
      if (l > 0L) {
        long l1 = getPssAverage(b);
        bool = true;
        if (i <= 2) {
          pssAggr1.add(l1, l);
        } else if (i <= 7) {
          pssAggr2.add(l1, l);
        } else {
          pssAggr3.add(l1, l);
        } 
      } 
    } 
    if (!bool)
      return; 
    byte b2 = 0;
    byte b3 = 0;
    byte b4 = 0;
    b1 = b2;
    if (pssAggr1.samples < 3L) {
      b1 = b2;
      if (pssAggr2.samples > 0L) {
        b1 = 1;
        pssAggr1.add(pssAggr2.pss, pssAggr2.samples);
      } 
    } 
    b2 = b3;
    if (pssAggr1.samples < 3L) {
      b2 = b3;
      if (pssAggr3.samples > 0L) {
        b2 = 1;
        pssAggr1.add(pssAggr3.pss, pssAggr3.samples);
      } 
    } 
    b3 = b4;
    if (pssAggr2.samples < 3L) {
      b3 = b4;
      if (pssAggr3.samples > 0L) {
        b3 = 1;
        pssAggr2.add(pssAggr3.pss, pssAggr3.samples);
      } 
    } 
    if (pssAggr2.samples < 3L && b1 == 0 && pssAggr1.samples > 0L)
      pssAggr2.add(pssAggr1.pss, pssAggr1.samples); 
    if (pssAggr3.samples < 3L && !b3 && pssAggr2.samples > 0L)
      pssAggr3.add(pssAggr2.pss, pssAggr2.samples); 
    if (pssAggr3.samples < 3L && !b2 && pssAggr1.samples > 0L)
      pssAggr3.add(pssAggr1.pss, pssAggr1.samples); 
    for (b4 = 0; b4 < this.mDurations.getKeyCount(); b4++) {
      int i = this.mDurations.getKeyAt(b4);
      byte b = SparseMappingTable.getIdFromKey(i);
      long l1 = this.mDurations.getValue(i);
      long l3 = l1;
      if (this.mCurCombinedState == b)
        l3 = l1 + paramLong - this.mStartTime; 
      i = b % 14;
      long[] arrayOfLong = paramTotalMemoryUseCollection.processStateTime;
      arrayOfLong[i] = arrayOfLong[i] + l3;
      long l2 = getPssSampleCount(b);
      if (l2 > 0L) {
        l1 = getPssAverage(b);
      } else if (i <= 2) {
        l2 = pssAggr1.samples;
        l1 = pssAggr1.pss;
      } else if (i <= 7) {
        l2 = pssAggr2.samples;
        l1 = pssAggr2.pss;
      } else {
        l2 = pssAggr3.samples;
        l1 = pssAggr3.pss;
      } 
      double d = (paramTotalMemoryUseCollection.processStatePss[i] * paramTotalMemoryUseCollection.processStateSamples[i] + l1 * l2) / (paramTotalMemoryUseCollection.processStateSamples[i] + l2);
      paramTotalMemoryUseCollection.processStatePss[i] = (long)d;
      int[] arrayOfInt = paramTotalMemoryUseCollection.processStateSamples;
      arrayOfInt[i] = (int)(arrayOfInt[i] + l2);
      double[] arrayOfDouble = paramTotalMemoryUseCollection.processStateWeight;
      arrayOfDouble[i] = arrayOfDouble[i] + l1 * l3;
    } 
  }
  
  public long computeProcessTimeLocked(int[] paramArrayOfint1, int[] paramArrayOfint2, int[] paramArrayOfint3, long paramLong) {
    long l = 0L;
    for (byte b = 0; b < paramArrayOfint1.length; b++) {
      for (byte b1 = 0; b1 < paramArrayOfint2.length; b1++) {
        for (byte b2 = 0; b2 < paramArrayOfint3.length; b2++) {
          int i = paramArrayOfint1[b], j = paramArrayOfint2[b1], k = paramArrayOfint3[b2];
          l += getDuration((i + j) * 14 + k, paramLong);
        } 
      } 
    } 
    this.mTmpTotalTime = l;
    return l;
  }
  
  public void dumpSummary(PrintWriter paramPrintWriter, String paramString1, String paramString2, int[] paramArrayOfint1, int[] paramArrayOfint2, int[] paramArrayOfint3, long paramLong1, long paramLong2) {
    paramPrintWriter.print(paramString1);
    paramPrintWriter.print("* ");
    if (paramString2 != null)
      paramPrintWriter.print(paramString2); 
    paramPrintWriter.print(this.mName);
    paramPrintWriter.print(" / ");
    UserHandle.formatUid(paramPrintWriter, this.mUid);
    paramPrintWriter.print(" / v");
    paramPrintWriter.print(this.mVersion);
    paramPrintWriter.println(":");
    dumpProcessSummaryDetails(paramPrintWriter, paramString1, DumpUtils.STATE_LABEL_TOTAL, paramArrayOfint1, paramArrayOfint2, paramArrayOfint3, paramLong1, paramLong2, true);
    dumpProcessSummaryDetails(paramPrintWriter, paramString1, DumpUtils.STATE_LABELS[0], paramArrayOfint1, paramArrayOfint2, new int[] { 0 }, paramLong1, paramLong2, true);
    dumpProcessSummaryDetails(paramPrintWriter, paramString1, DumpUtils.STATE_LABELS[1], paramArrayOfint1, paramArrayOfint2, new int[] { 1 }, paramLong1, paramLong2, true);
    dumpProcessSummaryDetails(paramPrintWriter, paramString1, DumpUtils.STATE_LABELS[2], paramArrayOfint1, paramArrayOfint2, new int[] { 2 }, paramLong1, paramLong2, true);
    dumpProcessSummaryDetails(paramPrintWriter, paramString1, DumpUtils.STATE_LABELS[3], paramArrayOfint1, paramArrayOfint2, new int[] { 3 }, paramLong1, paramLong2, true);
    dumpProcessSummaryDetails(paramPrintWriter, paramString1, DumpUtils.STATE_LABELS[4], paramArrayOfint1, paramArrayOfint2, new int[] { 4 }, paramLong1, paramLong2, true);
    dumpProcessSummaryDetails(paramPrintWriter, paramString1, DumpUtils.STATE_LABELS[5], paramArrayOfint1, paramArrayOfint2, new int[] { 5 }, paramLong1, paramLong2, true);
    dumpProcessSummaryDetails(paramPrintWriter, paramString1, DumpUtils.STATE_LABELS[6], paramArrayOfint1, paramArrayOfint2, new int[] { 6 }, paramLong1, paramLong2, true);
    dumpProcessSummaryDetails(paramPrintWriter, paramString1, DumpUtils.STATE_LABELS[7], paramArrayOfint1, paramArrayOfint2, new int[] { 7 }, paramLong1, paramLong2, true);
    dumpProcessSummaryDetails(paramPrintWriter, paramString1, DumpUtils.STATE_LABELS[8], paramArrayOfint1, paramArrayOfint2, new int[] { 8 }, paramLong1, paramLong2, true);
    dumpProcessSummaryDetails(paramPrintWriter, paramString1, DumpUtils.STATE_LABELS[9], paramArrayOfint1, paramArrayOfint2, new int[] { 9 }, paramLong1, paramLong2, true);
    dumpProcessSummaryDetails(paramPrintWriter, paramString1, DumpUtils.STATE_LABELS[10], paramArrayOfint1, paramArrayOfint2, new int[] { 10 }, paramLong1, paramLong2, true);
    dumpProcessSummaryDetails(paramPrintWriter, paramString1, DumpUtils.STATE_LABEL_CACHED, paramArrayOfint1, paramArrayOfint2, new int[] { 11, 12, 13 }, paramLong1, paramLong2, true);
  }
  
  public void dumpProcessState(PrintWriter paramPrintWriter, String paramString, int[] paramArrayOfint1, int[] paramArrayOfint2, int[] paramArrayOfint3, long paramLong) {
    long l = 0L;
    int i = -1;
    int j;
    for (j = 0; j < paramArrayOfint1.length; m = n + 1, i = j, j = m) {
      int k = -1;
      int m, n;
      for (m = 0, n = j, j = i; m < paramArrayOfint2.length; i++, k = m, m = i) {
        for (byte b = 0; b < paramArrayOfint3.length; b++, l = l2, j = k, m = i3) {
          int i1 = paramArrayOfint1[n];
          int i2 = paramArrayOfint2[i];
          k = (i1 + i2) * 14 + paramArrayOfint3[b];
          long l1 = this.mDurations.getValueForId((byte)k);
          String str = "";
          if (this.mCurCombinedState == k) {
            str = " (running)";
            l1 += paramLong - this.mStartTime;
          } 
          long l2 = l;
          k = j;
          int i3 = m;
          if (l1 != 0L) {
            paramPrintWriter.print(paramString);
            k = j;
            if (paramArrayOfint1.length > 1) {
              if (j != i1) {
                j = i1;
              } else {
                j = -1;
              } 
              DumpUtils.printScreenLabel(paramPrintWriter, j);
              k = i1;
            } 
            j = m;
            if (paramArrayOfint2.length > 1) {
              if (m != i2) {
                m = i2;
              } else {
                m = -1;
              } 
              DumpUtils.printMemLabel(paramPrintWriter, m, '/');
              j = i2;
            } 
            paramPrintWriter.print(DumpUtils.STATE_LABELS[paramArrayOfint3[b]]);
            paramPrintWriter.print(": ");
            TimeUtils.formatDuration(l1, paramPrintWriter);
            paramPrintWriter.println(str);
            l2 = l + l1;
            i3 = j;
          } 
        } 
      } 
    } 
    if (l != 0L) {
      paramPrintWriter.print(paramString);
      if (paramArrayOfint1.length > 1)
        DumpUtils.printScreenLabel(paramPrintWriter, -1); 
      if (paramArrayOfint2.length > 1)
        DumpUtils.printMemLabel(paramPrintWriter, -1, '/'); 
      paramPrintWriter.print(DumpUtils.STATE_LABEL_TOTAL);
      paramPrintWriter.print(": ");
      TimeUtils.formatDuration(l, paramPrintWriter);
      paramPrintWriter.println();
    } 
  }
  
  public void dumpPss(PrintWriter paramPrintWriter, String paramString, int[] paramArrayOfint1, int[] paramArrayOfint2, int[] paramArrayOfint3, long paramLong) {
    boolean bool = false;
    int i = -1;
    int j;
    for (j = 0; j < paramArrayOfint1.length; j = m + 1) {
      int k = -1;
      int m;
      for (byte b = 0; b < paramArrayOfint2.length; b++, k = j) {
        for (byte b1 = 0; b1 < paramArrayOfint3.length; b1++) {
          int n = paramArrayOfint1[m];
          int i1 = paramArrayOfint2[b];
          k = paramArrayOfint3[b1];
          k = this.mPssTable.getKey((byte)((n + i1) * 14 + k));
          if (k != -1) {
            long[] arrayOfLong = this.mPssTable.getArrayForKey(k);
            int i2 = SparseMappingTable.getIndexFromKey(k);
            if (!bool) {
              paramPrintWriter.print(paramString);
              paramPrintWriter.print("PSS/USS (");
              paramPrintWriter.print(this.mPssTable.getKeyCount());
              paramPrintWriter.println(" entries):");
              bool = true;
            } 
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("  ");
            k = i;
            if (paramArrayOfint1.length > 1) {
              if (i != n) {
                i = n;
              } else {
                i = -1;
              } 
              DumpUtils.printScreenLabel(paramPrintWriter, i);
              k = n;
            } 
            i = j;
            if (paramArrayOfint2.length > 1) {
              if (j != i1) {
                j = i1;
              } else {
                j = -1;
              } 
              DumpUtils.printMemLabel(paramPrintWriter, j, '/');
              i = i1;
            } 
            paramPrintWriter.print(DumpUtils.STATE_LABELS[paramArrayOfint3[b1]]);
            paramPrintWriter.print(": ");
            dumpPssSamples(paramPrintWriter, arrayOfLong, i2);
            paramPrintWriter.println();
            j = i;
            i = k;
          } 
        } 
      } 
    } 
    paramLong = getTotalRunningDuration(paramLong);
    if (paramLong != 0L) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("Cur time ");
      TimeUtils.formatDuration(paramLong, paramPrintWriter);
      if (this.mTotalRunningStartTime != 0L)
        paramPrintWriter.print(" (running)"); 
      if (this.mTotalRunningPss[0] != 0L) {
        paramPrintWriter.print(": ");
        dumpPssSamples(paramPrintWriter, this.mTotalRunningPss, 0);
      } 
      paramPrintWriter.println();
    } 
    if (this.mNumExcessiveCpu != 0) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("Killed for excessive CPU use: ");
      paramPrintWriter.print(this.mNumExcessiveCpu);
      paramPrintWriter.println(" times");
    } 
    if (this.mNumCachedKill != 0) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("Killed from cached state: ");
      paramPrintWriter.print(this.mNumCachedKill);
      paramPrintWriter.print(" times from pss ");
      DebugUtils.printSizeValue(paramPrintWriter, this.mMinCachedKillPss * 1024L);
      paramPrintWriter.print("-");
      DebugUtils.printSizeValue(paramPrintWriter, this.mAvgCachedKillPss * 1024L);
      paramPrintWriter.print("-");
      DebugUtils.printSizeValue(paramPrintWriter, this.mMaxCachedKillPss * 1024L);
      paramPrintWriter.println();
    } 
  }
  
  public static void dumpPssSamples(PrintWriter paramPrintWriter, long[] paramArrayOflong, int paramInt) {
    DebugUtils.printSizeValue(paramPrintWriter, paramArrayOflong[paramInt + 1] * 1024L);
    paramPrintWriter.print("-");
    DebugUtils.printSizeValue(paramPrintWriter, paramArrayOflong[paramInt + 2] * 1024L);
    paramPrintWriter.print("-");
    DebugUtils.printSizeValue(paramPrintWriter, paramArrayOflong[paramInt + 3] * 1024L);
    paramPrintWriter.print("/");
    DebugUtils.printSizeValue(paramPrintWriter, paramArrayOflong[paramInt + 4] * 1024L);
    paramPrintWriter.print("-");
    DebugUtils.printSizeValue(paramPrintWriter, paramArrayOflong[paramInt + 5] * 1024L);
    paramPrintWriter.print("-");
    DebugUtils.printSizeValue(paramPrintWriter, paramArrayOflong[paramInt + 6] * 1024L);
    paramPrintWriter.print("/");
    DebugUtils.printSizeValue(paramPrintWriter, paramArrayOflong[paramInt + 7] * 1024L);
    paramPrintWriter.print("-");
    DebugUtils.printSizeValue(paramPrintWriter, paramArrayOflong[paramInt + 8] * 1024L);
    paramPrintWriter.print("-");
    DebugUtils.printSizeValue(paramPrintWriter, paramArrayOflong[paramInt + 9] * 1024L);
    paramPrintWriter.print(" over ");
    paramPrintWriter.print(paramArrayOflong[paramInt + 0]);
  }
  
  private void dumpProcessSummaryDetails(PrintWriter paramPrintWriter, String paramString1, String paramString2, int[] paramArrayOfint1, int[] paramArrayOfint2, int[] paramArrayOfint3, long paramLong1, long paramLong2, boolean paramBoolean) {
    ProcessStats.ProcessDataCollection processDataCollection = new ProcessStats.ProcessDataCollection(paramArrayOfint1, paramArrayOfint2, paramArrayOfint3);
    computeProcessData(processDataCollection, paramLong1);
    double d = processDataCollection.totalTime / paramLong2;
    if (d * 100.0D >= 0.005D || processDataCollection.numPss != 0L) {
      if (paramString1 != null)
        paramPrintWriter.print(paramString1); 
      if (paramString2 != null) {
        paramPrintWriter.print("  ");
        paramPrintWriter.print(paramString2);
        paramPrintWriter.print(": ");
      } 
      processDataCollection.print(paramPrintWriter, paramLong2, paramBoolean);
      if (paramString1 != null)
        paramPrintWriter.println(); 
    } 
  }
  
  public void dumpInternalLocked(PrintWriter paramPrintWriter, String paramString, boolean paramBoolean) {
    if (paramBoolean) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("myID=");
      paramPrintWriter.print(Integer.toHexString(System.identityHashCode(this)));
      paramPrintWriter.print(" mCommonProcess=");
      paramPrintWriter.print(Integer.toHexString(System.identityHashCode(this.mCommonProcess)));
      paramPrintWriter.print(" mPackage=");
      paramPrintWriter.println(this.mPackage);
      if (this.mMultiPackage) {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mMultiPackage=");
        paramPrintWriter.println(this.mMultiPackage);
      } 
      if (this != this.mCommonProcess) {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("Common Proc: ");
        paramPrintWriter.print(this.mCommonProcess.mName);
        paramPrintWriter.print("/");
        paramPrintWriter.print(this.mCommonProcess.mUid);
        paramPrintWriter.print(" pkg=");
        paramPrintWriter.println(this.mCommonProcess.mPackage);
      } 
    } 
    if (this.mActive) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("mActive=");
      paramPrintWriter.println(this.mActive);
    } 
    if (this.mDead) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("mDead=");
      paramPrintWriter.println(this.mDead);
    } 
    if (this.mNumActiveServices != 0 || this.mNumStartedServices != 0) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("mNumActiveServices=");
      paramPrintWriter.print(this.mNumActiveServices);
      paramPrintWriter.print(" mNumStartedServices=");
      paramPrintWriter.println(this.mNumStartedServices);
    } 
  }
  
  public void computeProcessData(ProcessStats.ProcessDataCollection paramProcessDataCollection, long paramLong) {
    long l = 0L;
    paramProcessDataCollection.totalTime = 0L;
    paramProcessDataCollection.maxRss = 0L;
    paramProcessDataCollection.avgRss = 0L;
    paramProcessDataCollection.minRss = 0L;
    paramProcessDataCollection.maxUss = 0L;
    paramProcessDataCollection.avgUss = 0L;
    paramProcessDataCollection.minUss = 0L;
    paramProcessDataCollection.maxPss = 0L;
    paramProcessDataCollection.avgPss = 0L;
    paramProcessDataCollection.minPss = 0L;
    paramProcessDataCollection.numPss = 0L;
    for (byte b = 0; b < paramProcessDataCollection.screenStates.length; b++) {
      for (byte b1 = 0; b1 < paramProcessDataCollection.memStates.length; b1++) {
        for (byte b2 = 0; b2 < paramProcessDataCollection.procStates.length; b2++) {
          int i = (paramProcessDataCollection.screenStates[b] + paramProcessDataCollection.memStates[b1]) * 14 + paramProcessDataCollection.procStates[b2];
          paramProcessDataCollection.totalTime += getDuration(i, paramLong);
          long l1 = getPssSampleCount(i);
          if (l1 > l) {
            long l2 = getPssMinimum(i);
            long l3 = getPssAverage(i);
            long l4 = getPssMaximum(i);
            long l5 = getPssUssMinimum(i);
            long l6 = getPssUssAverage(i);
            long l7 = getPssUssMaximum(i);
            long l8 = getPssRssMinimum(i);
            long l9 = getPssRssAverage(i);
            long l10 = getPssRssMaximum(i);
            long l11 = paramProcessDataCollection.numPss;
            l = 0L;
            if (l11 == 0L) {
              paramProcessDataCollection.minPss = l2;
              paramProcessDataCollection.avgPss = l3;
              paramProcessDataCollection.maxPss = l4;
              paramProcessDataCollection.minUss = l5;
              paramProcessDataCollection.avgUss = l6;
              paramProcessDataCollection.maxUss = l7;
              paramProcessDataCollection.minRss = l8;
              paramProcessDataCollection.avgRss = l9;
              paramProcessDataCollection.maxRss = l10;
            } else {
              if (l2 < paramProcessDataCollection.minPss)
                paramProcessDataCollection.minPss = l2; 
              double d1 = paramProcessDataCollection.avgPss, d2 = paramProcessDataCollection.numPss, d3 = l3;
              paramProcessDataCollection.avgPss = (long)((d1 * d2 + d3 * l1) / (paramProcessDataCollection.numPss + l1));
              if (l4 > paramProcessDataCollection.maxPss)
                paramProcessDataCollection.maxPss = l4; 
              if (l5 < paramProcessDataCollection.minUss)
                paramProcessDataCollection.minUss = l5; 
              paramProcessDataCollection.avgUss = (long)((paramProcessDataCollection.avgUss * paramProcessDataCollection.numPss + l6 * l1) / (paramProcessDataCollection.numPss + l1));
              if (l7 > paramProcessDataCollection.maxUss)
                paramProcessDataCollection.maxUss = l7; 
              if (l8 < paramProcessDataCollection.minRss)
                paramProcessDataCollection.minRss = l8; 
              paramProcessDataCollection.avgRss = (long)((paramProcessDataCollection.avgRss * paramProcessDataCollection.numPss + l9 * l1) / (paramProcessDataCollection.numPss + l1));
              if (l10 > paramProcessDataCollection.maxRss)
                paramProcessDataCollection.maxRss = l10; 
            } 
            paramProcessDataCollection.numPss += l1;
          } 
        } 
      } 
    } 
  }
  
  public void dumpCsv(PrintWriter paramPrintWriter, boolean paramBoolean1, int[] paramArrayOfint1, boolean paramBoolean2, int[] paramArrayOfint2, boolean paramBoolean3, int[] paramArrayOfint3, long paramLong) {
    byte b1;
    if (paramBoolean1) {
      i = paramArrayOfint1.length;
    } else {
      i = 1;
    } 
    if (paramBoolean2) {
      b1 = paramArrayOfint2.length;
    } else {
      b1 = 1;
    } 
    if (paramBoolean3) {
      b2 = paramArrayOfint3.length;
    } else {
      b2 = 1;
    } 
    int i;
    for (byte b3 = 0, b4 = b2, b2 = b1; b3 < i; b3++, i = j) {
      int j;
      for (b1 = 0, j = i; b1 < b2; b1++) {
        for (byte b = 0; b < b4; b++) {
          byte b5, b6, b7;
          int k, m;
          if (paramBoolean1) {
            b5 = paramArrayOfint1[b3];
          } else {
            b5 = 0;
          } 
          if (paramBoolean2) {
            b6 = paramArrayOfint2[b1];
          } else {
            b6 = 0;
          } 
          if (paramBoolean3) {
            b7 = paramArrayOfint3[b];
          } else {
            b7 = 0;
          } 
          if (paramBoolean1) {
            k = 1;
          } else {
            k = paramArrayOfint1.length;
          } 
          if (paramBoolean2) {
            i = 1;
          } else {
            i = paramArrayOfint2.length;
          } 
          if (paramBoolean3) {
            m = 1;
          } else {
            m = paramArrayOfint3.length;
          } 
          long l;
          byte b8;
          for (l = 0L, b8 = 0; b8 < k; b8++) {
            for (byte b9 = 0; b9 < i; b9++) {
              for (byte b10 = 0; b10 < m; b10++) {
                int n, i1, i2;
                if (paramBoolean1) {
                  n = 0;
                } else {
                  n = paramArrayOfint1[b8];
                } 
                if (paramBoolean2) {
                  i1 = 0;
                } else {
                  i1 = paramArrayOfint2[b9];
                } 
                if (paramBoolean3) {
                  i2 = 0;
                } else {
                  i2 = paramArrayOfint3[b10];
                } 
                l += getDuration((b5 + n + b6 + i1) * 14 + b7 + i2, paramLong);
              } 
            } 
          } 
          paramPrintWriter.print("\t");
          paramPrintWriter.print(l);
        } 
      } 
    } 
  }
  
  public void dumpPackageProcCheckin(PrintWriter paramPrintWriter, String paramString1, int paramInt, long paramLong1, String paramString2, long paramLong2) {
    paramPrintWriter.print("pkgproc,");
    paramPrintWriter.print(paramString1);
    paramPrintWriter.print(",");
    paramPrintWriter.print(paramInt);
    paramPrintWriter.print(",");
    paramPrintWriter.print(paramLong1);
    paramPrintWriter.print(",");
    paramPrintWriter.print(DumpUtils.collapseString(paramString1, paramString2));
    dumpAllStateCheckin(paramPrintWriter, paramLong2);
    paramPrintWriter.println();
    if (this.mPssTable.getKeyCount() > 0) {
      paramPrintWriter.print("pkgpss,");
      paramPrintWriter.print(paramString1);
      paramPrintWriter.print(",");
      paramPrintWriter.print(paramInt);
      paramPrintWriter.print(",");
      paramPrintWriter.print(paramLong1);
      paramPrintWriter.print(",");
      paramPrintWriter.print(DumpUtils.collapseString(paramString1, paramString2));
      dumpAllPssCheckin(paramPrintWriter);
      paramPrintWriter.println();
    } 
    if (this.mTotalRunningPss[0] != 0L) {
      paramPrintWriter.print("pkgrun,");
      paramPrintWriter.print(paramString1);
      paramPrintWriter.print(",");
      paramPrintWriter.print(paramInt);
      paramPrintWriter.print(",");
      paramPrintWriter.print(paramLong1);
      paramPrintWriter.print(",");
      paramPrintWriter.print(DumpUtils.collapseString(paramString1, paramString2));
      paramPrintWriter.print(",");
      paramPrintWriter.print(getTotalRunningDuration(paramLong2));
      paramPrintWriter.print(",");
      dumpPssSamplesCheckin(paramPrintWriter, this.mTotalRunningPss, 0);
      paramPrintWriter.println();
    } 
    if (this.mNumExcessiveCpu > 0 || this.mNumCachedKill > 0) {
      paramPrintWriter.print("pkgkills,");
      paramPrintWriter.print(paramString1);
      paramPrintWriter.print(",");
      paramPrintWriter.print(paramInt);
      paramPrintWriter.print(",");
      paramPrintWriter.print(paramLong1);
      paramPrintWriter.print(",");
      paramPrintWriter.print(DumpUtils.collapseString(paramString1, paramString2));
      paramPrintWriter.print(",");
      paramPrintWriter.print("0");
      paramPrintWriter.print(",");
      paramPrintWriter.print(this.mNumExcessiveCpu);
      paramPrintWriter.print(",");
      paramPrintWriter.print(this.mNumCachedKill);
      paramPrintWriter.print(",");
      paramPrintWriter.print(this.mMinCachedKillPss);
      paramPrintWriter.print(":");
      paramPrintWriter.print(this.mAvgCachedKillPss);
      paramPrintWriter.print(":");
      paramPrintWriter.print(this.mMaxCachedKillPss);
      paramPrintWriter.println();
    } 
  }
  
  public void dumpProcCheckin(PrintWriter paramPrintWriter, String paramString, int paramInt, long paramLong) {
    if (this.mDurations.getKeyCount() > 0) {
      paramPrintWriter.print("proc,");
      paramPrintWriter.print(paramString);
      paramPrintWriter.print(",");
      paramPrintWriter.print(paramInt);
      dumpAllStateCheckin(paramPrintWriter, paramLong);
      paramPrintWriter.println();
    } 
    if (this.mPssTable.getKeyCount() > 0) {
      paramPrintWriter.print("pss,");
      paramPrintWriter.print(paramString);
      paramPrintWriter.print(",");
      paramPrintWriter.print(paramInt);
      dumpAllPssCheckin(paramPrintWriter);
      paramPrintWriter.println();
    } 
    if (this.mTotalRunningPss[0] != 0L) {
      paramPrintWriter.print("procrun,");
      paramPrintWriter.print(paramString);
      paramPrintWriter.print(",");
      paramPrintWriter.print(paramInt);
      paramPrintWriter.print(",");
      paramPrintWriter.print(getTotalRunningDuration(paramLong));
      paramPrintWriter.print(",");
      dumpPssSamplesCheckin(paramPrintWriter, this.mTotalRunningPss, 0);
      paramPrintWriter.println();
    } 
    if (this.mNumExcessiveCpu > 0 || this.mNumCachedKill > 0) {
      paramPrintWriter.print("kills,");
      paramPrintWriter.print(paramString);
      paramPrintWriter.print(",");
      paramPrintWriter.print(paramInt);
      paramPrintWriter.print(",");
      paramPrintWriter.print("0");
      paramPrintWriter.print(",");
      paramPrintWriter.print(this.mNumExcessiveCpu);
      paramPrintWriter.print(",");
      paramPrintWriter.print(this.mNumCachedKill);
      paramPrintWriter.print(",");
      paramPrintWriter.print(this.mMinCachedKillPss);
      paramPrintWriter.print(":");
      paramPrintWriter.print(this.mAvgCachedKillPss);
      paramPrintWriter.print(":");
      paramPrintWriter.print(this.mMaxCachedKillPss);
      paramPrintWriter.println();
    } 
  }
  
  public void dumpAllStateCheckin(PrintWriter paramPrintWriter, long paramLong) {
    boolean bool = false;
    int i;
    for (i = 0; i < this.mDurations.getKeyCount(); i++) {
      int j = this.mDurations.getKeyAt(i);
      byte b = SparseMappingTable.getIdFromKey(j);
      long l1 = this.mDurations.getValue(j);
      long l2 = l1;
      if (this.mCurCombinedState == b) {
        bool = true;
        l2 = l1 + paramLong - this.mStartTime;
      } 
      DumpUtils.printProcStateTagAndValue(paramPrintWriter, b, l2);
    } 
    if (!bool) {
      i = this.mCurCombinedState;
      if (i != -1)
        DumpUtils.printProcStateTagAndValue(paramPrintWriter, i, paramLong - this.mStartTime); 
    } 
  }
  
  public void dumpAllPssCheckin(PrintWriter paramPrintWriter) {
    int i = this.mPssTable.getKeyCount();
    for (byte b = 0; b < i; b++) {
      int j = this.mPssTable.getKeyAt(b);
      byte b1 = SparseMappingTable.getIdFromKey(j);
      paramPrintWriter.print(',');
      DumpUtils.printProcStateTag(paramPrintWriter, b1);
      paramPrintWriter.print(':');
      long[] arrayOfLong = this.mPssTable.getArrayForKey(j);
      j = SparseMappingTable.getIndexFromKey(j);
      dumpPssSamplesCheckin(paramPrintWriter, arrayOfLong, j);
    } 
  }
  
  public static void dumpPssSamplesCheckin(PrintWriter paramPrintWriter, long[] paramArrayOflong, int paramInt) {
    paramPrintWriter.print(paramArrayOflong[paramInt + 0]);
    paramPrintWriter.print(':');
    paramPrintWriter.print(paramArrayOflong[paramInt + 1]);
    paramPrintWriter.print(':');
    paramPrintWriter.print(paramArrayOflong[paramInt + 2]);
    paramPrintWriter.print(':');
    paramPrintWriter.print(paramArrayOflong[paramInt + 3]);
    paramPrintWriter.print(':');
    paramPrintWriter.print(paramArrayOflong[paramInt + 4]);
    paramPrintWriter.print(':');
    paramPrintWriter.print(paramArrayOflong[paramInt + 5]);
    paramPrintWriter.print(':');
    paramPrintWriter.print(paramArrayOflong[paramInt + 6]);
    paramPrintWriter.print(':');
    paramPrintWriter.print(paramArrayOflong[paramInt + 7]);
    paramPrintWriter.print(':');
    paramPrintWriter.print(paramArrayOflong[paramInt + 8]);
    paramPrintWriter.print(':');
    paramPrintWriter.print(paramArrayOflong[paramInt + 9]);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(128);
    stringBuilder.append("ProcessState{");
    stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
    stringBuilder.append(" ");
    stringBuilder.append(this.mName);
    stringBuilder.append("/");
    stringBuilder.append(this.mUid);
    stringBuilder.append(" pkg=");
    stringBuilder.append(this.mPackage);
    if (this.mMultiPackage)
      stringBuilder.append(" (multi)"); 
    if (this.mCommonProcess != this)
      stringBuilder.append(" (sub)"); 
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong1, String paramString, int paramInt, long paramLong2) {
    long l = paramProtoOutputStream.start(paramLong1);
    paramProtoOutputStream.write(1138166333441L, paramString);
    paramProtoOutputStream.write(1120986464258L, paramInt);
    if (this.mNumExcessiveCpu > 0 || this.mNumCachedKill > 0) {
      paramLong1 = paramProtoOutputStream.start(1146756268035L);
      paramProtoOutputStream.write(1120986464257L, this.mNumExcessiveCpu);
      paramProtoOutputStream.write(1120986464258L, this.mNumCachedKill);
      ProtoUtils.toAggStatsProto(paramProtoOutputStream, 1146756268035L, this.mMinCachedKillPss, this.mAvgCachedKillPss, this.mMaxCachedKillPss);
      paramProtoOutputStream.end(paramLong1);
    } 
    SparseLongArray sparseLongArray = new SparseLongArray();
    int i;
    for (paramInt = 0, i = 0; paramInt < this.mDurations.getKeyCount(); paramInt++) {
      int j = this.mDurations.getKeyAt(paramInt);
      byte b = SparseMappingTable.getIdFromKey(j);
      long l1 = this.mDurations.getValue(j);
      paramLong1 = l1;
      if (this.mCurCombinedState == b) {
        paramLong1 = l1 + paramLong2 - this.mStartTime;
        i = 1;
      } 
      sparseLongArray.put(b, paramLong1);
    } 
    if (!i) {
      paramInt = this.mCurCombinedState;
      if (paramInt != -1)
        sparseLongArray.put(paramInt, paramLong2 - this.mStartTime); 
    } 
    paramInt = 0;
    while (true) {
      i = this.mPssTable.getKeyCount();
      paramLong1 = 2246267895813L;
      if (paramInt < i) {
        int j = this.mPssTable.getKeyAt(paramInt);
        i = SparseMappingTable.getIdFromKey(j);
        if (sparseLongArray.indexOfKey(i) >= 0) {
          paramLong1 = paramProtoOutputStream.start(2246267895813L);
          DumpUtils.printProcStateTagProto(paramProtoOutputStream, 1159641169921L, 1159641169922L, 1159641169923L, i);
          long l1 = sparseLongArray.get(i);
          sparseLongArray.delete(i);
          paramProtoOutputStream.write(1112396529668L, l1);
          this.mPssTable.writeStatsToProtoForKey(paramProtoOutputStream, j);
          paramProtoOutputStream.end(paramLong1);
        } 
        paramInt++;
        continue;
      } 
      break;
    } 
    for (paramInt = 0; paramInt < sparseLongArray.size(); paramInt++) {
      long l1 = paramProtoOutputStream.start(paramLong1);
      i = sparseLongArray.keyAt(paramInt);
      DumpUtils.printProcStateTagProto(paramProtoOutputStream, 1159641169921L, 1159641169922L, 1159641169923L, i);
      paramProtoOutputStream.write(1112396529668L, sparseLongArray.valueAt(paramInt));
      paramProtoOutputStream.end(l1);
    } 
    paramLong1 = getTotalRunningDuration(paramLong2);
    if (paramLong1 > 0L) {
      paramLong2 = paramProtoOutputStream.start(1146756268038L);
      paramProtoOutputStream.write(1112396529668L, paramLong1);
      long[] arrayOfLong = this.mTotalRunningPss;
      if (arrayOfLong[0] != 0L)
        PssTable.writeStatsToProto(paramProtoOutputStream, arrayOfLong, 0); 
      paramProtoOutputStream.end(paramLong2);
    } 
    paramProtoOutputStream.end(l);
  }
  
  static void writeCompressedProcessName(ProtoOutputStream paramProtoOutputStream, long paramLong, String paramString1, String paramString2, boolean paramBoolean) {
    if (paramBoolean) {
      paramProtoOutputStream.write(paramLong, paramString1);
      return;
    } 
    if (TextUtils.equals(paramString1, paramString2))
      return; 
    if (paramString1.startsWith(paramString2)) {
      int i = paramString2.length();
      if (paramString1.charAt(i) == ':') {
        paramProtoOutputStream.write(paramLong, paramString1.substring(i));
        return;
      } 
    } 
    paramProtoOutputStream.write(paramLong, paramString1);
  }
  
  public void dumpAggregatedProtoForStatsd(ProtoOutputStream paramProtoOutputStream, long paramLong1, String paramString, int paramInt, long paramLong2, ProcessMap<ArraySet<ProcessStats.PackageState>> paramProcessMap, SparseArray<ArraySet<String>> paramSparseArray) {
    // Byte code:
    //   0: new android/util/SparseLongArray
    //   3: dup
    //   4: invokespecial <init> : ()V
    //   7: astore #10
    //   9: iconst_0
    //   10: istore #11
    //   12: iconst_0
    //   13: istore #12
    //   15: iload #11
    //   17: aload_0
    //   18: getfield mDurations : Lcom/android/internal/app/procstats/DurationsTable;
    //   21: invokevirtual getKeyCount : ()I
    //   24: if_icmpge -> 140
    //   27: aload_0
    //   28: getfield mDurations : Lcom/android/internal/app/procstats/DurationsTable;
    //   31: iload #11
    //   33: invokevirtual getKeyAt : (I)I
    //   36: istore #13
    //   38: iload #13
    //   40: invokestatic getIdFromKey : (I)B
    //   43: istore #14
    //   45: iload #14
    //   47: invokestatic aggregateCurrentProcessState : (I)I
    //   50: istore #15
    //   52: aload_0
    //   53: getfield mDurations : Lcom/android/internal/app/procstats/DurationsTable;
    //   56: iload #13
    //   58: invokevirtual getValue : (I)J
    //   61: lstore #16
    //   63: lload #16
    //   65: lstore #18
    //   67: aload_0
    //   68: getfield mCurCombinedState : I
    //   71: iload #14
    //   73: if_icmpne -> 91
    //   76: lload #16
    //   78: lload #6
    //   80: aload_0
    //   81: getfield mStartTime : J
    //   84: lsub
    //   85: ladd
    //   86: lstore #18
    //   88: iconst_1
    //   89: istore #12
    //   91: aload #10
    //   93: iload #15
    //   95: invokevirtual indexOfKey : (I)I
    //   98: istore #14
    //   100: iload #14
    //   102: iflt -> 125
    //   105: aload #10
    //   107: iload #15
    //   109: aload #10
    //   111: iload #14
    //   113: invokevirtual valueAt : (I)J
    //   116: lload #18
    //   118: ladd
    //   119: invokevirtual put : (IJ)V
    //   122: goto -> 134
    //   125: aload #10
    //   127: iload #15
    //   129: lload #18
    //   131: invokevirtual put : (IJ)V
    //   134: iinc #11, 1
    //   137: goto -> 15
    //   140: iload #12
    //   142: ifne -> 225
    //   145: aload_0
    //   146: getfield mCurCombinedState : I
    //   149: istore #11
    //   151: iload #11
    //   153: iconst_m1
    //   154: if_icmpeq -> 225
    //   157: iload #11
    //   159: invokestatic aggregateCurrentProcessState : (I)I
    //   162: istore #12
    //   164: aload #10
    //   166: iload #12
    //   168: invokevirtual indexOfKey : (I)I
    //   171: istore #11
    //   173: iload #11
    //   175: iflt -> 211
    //   178: aload_0
    //   179: getfield mStartTime : J
    //   182: lstore #16
    //   184: aload #10
    //   186: iload #11
    //   188: invokevirtual valueAt : (I)J
    //   191: lstore #18
    //   193: aload #10
    //   195: iload #12
    //   197: lload #6
    //   199: lload #16
    //   201: lsub
    //   202: lload #18
    //   204: ladd
    //   205: invokevirtual put : (IJ)V
    //   208: goto -> 225
    //   211: aload #10
    //   213: iload #12
    //   215: lload #6
    //   217: aload_0
    //   218: getfield mStartTime : J
    //   221: lsub
    //   222: invokevirtual put : (IJ)V
    //   225: new android/util/SparseLongArray
    //   228: dup
    //   229: invokespecial <init> : ()V
    //   232: astore #20
    //   234: new android/util/SparseLongArray
    //   237: dup
    //   238: invokespecial <init> : ()V
    //   241: astore #21
    //   243: iconst_0
    //   244: istore #11
    //   246: iload #11
    //   248: aload_0
    //   249: getfield mPssTable : Lcom/android/internal/app/procstats/PssTable;
    //   252: invokevirtual getKeyCount : ()I
    //   255: if_icmpge -> 433
    //   258: aload_0
    //   259: getfield mPssTable : Lcom/android/internal/app/procstats/PssTable;
    //   262: iload #11
    //   264: invokevirtual getKeyAt : (I)I
    //   267: istore #14
    //   269: iload #14
    //   271: invokestatic getIdFromKey : (I)B
    //   274: istore #15
    //   276: iload #15
    //   278: invokestatic aggregateCurrentProcessState : (I)I
    //   281: istore #12
    //   283: aload #10
    //   285: iload #12
    //   287: invokevirtual indexOfKey : (I)I
    //   290: ifge -> 296
    //   293: goto -> 427
    //   296: aload_0
    //   297: getfield mPssTable : Lcom/android/internal/app/procstats/PssTable;
    //   300: iload #14
    //   302: invokevirtual getRssMeanAndMax : (I)[J
    //   305: astore #22
    //   307: aload #22
    //   309: iconst_0
    //   310: laload
    //   311: aload_0
    //   312: getfield mDurations : Lcom/android/internal/app/procstats/DurationsTable;
    //   315: iload #15
    //   317: i2b
    //   318: invokevirtual getValueForId : (B)J
    //   321: lmul
    //   322: lstore #16
    //   324: aload #20
    //   326: iload #12
    //   328: invokevirtual indexOfKey : (I)I
    //   331: iflt -> 358
    //   334: aload #20
    //   336: iload #12
    //   338: invokevirtual get : (I)J
    //   341: lstore #18
    //   343: aload #20
    //   345: iload #12
    //   347: lload #18
    //   349: lload #16
    //   351: ladd
    //   352: invokevirtual put : (IJ)V
    //   355: goto -> 367
    //   358: aload #20
    //   360: iload #12
    //   362: lload #16
    //   364: invokevirtual put : (IJ)V
    //   367: aload #21
    //   369: iload #12
    //   371: invokevirtual indexOfKey : (I)I
    //   374: iflt -> 406
    //   377: aload #21
    //   379: iload #12
    //   381: invokevirtual get : (I)J
    //   384: aload #22
    //   386: iconst_1
    //   387: laload
    //   388: lcmp
    //   389: ifge -> 406
    //   392: aload #21
    //   394: iload #12
    //   396: aload #22
    //   398: iconst_1
    //   399: laload
    //   400: invokevirtual put : (IJ)V
    //   403: goto -> 427
    //   406: aload #21
    //   408: iload #12
    //   410: invokevirtual indexOfKey : (I)I
    //   413: ifge -> 427
    //   416: aload #21
    //   418: iload #12
    //   420: aload #22
    //   422: iconst_1
    //   423: laload
    //   424: invokevirtual put : (IJ)V
    //   427: iinc #11, 1
    //   430: goto -> 246
    //   433: iconst_0
    //   434: istore #11
    //   436: iload #11
    //   438: aload #10
    //   440: invokevirtual size : ()I
    //   443: if_icmpge -> 523
    //   446: aload #10
    //   448: iload #11
    //   450: invokevirtual keyAt : (I)I
    //   453: istore #12
    //   455: aload #20
    //   457: iload #12
    //   459: invokevirtual indexOfKey : (I)I
    //   462: ifge -> 468
    //   465: goto -> 517
    //   468: aload #10
    //   470: iload #12
    //   472: invokevirtual get : (I)J
    //   475: lstore #18
    //   477: lload #18
    //   479: lconst_0
    //   480: lcmp
    //   481: ifle -> 499
    //   484: aload #20
    //   486: iload #12
    //   488: invokevirtual get : (I)J
    //   491: lload #18
    //   493: ldiv
    //   494: lstore #18
    //   496: goto -> 508
    //   499: aload #20
    //   501: iload #12
    //   503: invokevirtual get : (I)J
    //   506: lstore #18
    //   508: aload #20
    //   510: iload #12
    //   512: lload #18
    //   514: invokevirtual put : (IJ)V
    //   517: iinc #11, 1
    //   520: goto -> 436
    //   523: aload_1
    //   524: lload_2
    //   525: invokevirtual start : (J)J
    //   528: lstore_2
    //   529: aload_0
    //   530: getfield mPackage : Ljava/lang/String;
    //   533: astore #22
    //   535: aload_0
    //   536: getfield mMultiPackage : Z
    //   539: ifne -> 574
    //   542: aload_0
    //   543: getfield mUid : I
    //   546: istore #11
    //   548: aload #9
    //   550: iload #11
    //   552: invokevirtual get : (I)Ljava/lang/Object;
    //   555: checkcast android/util/ArraySet
    //   558: invokevirtual size : ()I
    //   561: iconst_1
    //   562: if_icmple -> 568
    //   565: goto -> 574
    //   568: iconst_0
    //   569: istore #23
    //   571: goto -> 577
    //   574: iconst_1
    //   575: istore #23
    //   577: aload_1
    //   578: ldc2_w 1138166333441
    //   581: aload #4
    //   583: aload #22
    //   585: iload #23
    //   587: invokestatic writeCompressedProcessName : (Landroid/util/proto/ProtoOutputStream;JLjava/lang/String;Ljava/lang/String;Z)V
    //   590: aload_1
    //   591: ldc2_w 1120986464258
    //   594: iload #5
    //   596: invokevirtual write : (JI)V
    //   599: iconst_0
    //   600: istore #5
    //   602: aload #21
    //   604: astore #4
    //   606: iload #5
    //   608: aload #10
    //   610: invokevirtual size : ()I
    //   613: if_icmpge -> 706
    //   616: aload_1
    //   617: ldc2_w 2246267895813
    //   620: invokevirtual start : (J)J
    //   623: lstore #18
    //   625: aload #10
    //   627: iload #5
    //   629: invokevirtual keyAt : (I)I
    //   632: istore #12
    //   634: aload_1
    //   635: ldc2_w 1159641169921
    //   638: ldc2_w 1159641169930
    //   641: iload #12
    //   643: invokestatic printAggregatedProcStateTagProto : (Landroid/util/proto/ProtoOutputStream;JJI)V
    //   646: aload_1
    //   647: ldc2_w 1112396529668
    //   650: aload #10
    //   652: iload #12
    //   654: invokevirtual get : (I)J
    //   657: invokevirtual write : (JJ)V
    //   660: aload #20
    //   662: iload #12
    //   664: invokevirtual get : (I)J
    //   667: l2i
    //   668: istore #11
    //   670: aload #4
    //   672: iload #12
    //   674: invokevirtual get : (I)J
    //   677: l2i
    //   678: istore #12
    //   680: aload_1
    //   681: ldc2_w 1146756268040
    //   684: lconst_0
    //   685: lconst_0
    //   686: lconst_0
    //   687: iload #11
    //   689: iload #12
    //   691: invokestatic toAggStatsProto : (Landroid/util/proto/ProtoOutputStream;JJJJII)V
    //   694: aload_1
    //   695: lload #18
    //   697: invokevirtual end : (J)V
    //   700: iinc #5, 1
    //   703: goto -> 606
    //   706: aload_0
    //   707: getfield mStats : Lcom/android/internal/app/procstats/ProcessStats;
    //   710: aload_1
    //   711: ldc2_w 2246267895815
    //   714: lload #6
    //   716: aload_0
    //   717: aload #8
    //   719: aload #9
    //   721: invokevirtual dumpFilteredAssociationStatesProtoForProc : (Landroid/util/proto/ProtoOutputStream;JJLcom/android/internal/app/procstats/ProcessState;Lcom/android/internal/app/ProcessMap;Landroid/util/SparseArray;)V
    //   724: aload_1
    //   725: lload_2
    //   726: invokevirtual end : (J)V
    //   729: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1458	-> 0
    //   #1459	-> 9
    //   #1460	-> 9
    //   #1461	-> 27
    //   #1462	-> 38
    //   #1463	-> 45
    //   #1465	-> 52
    //   #1466	-> 63
    //   #1467	-> 76
    //   #1468	-> 76
    //   #1470	-> 91
    //   #1471	-> 100
    //   #1472	-> 105
    //   #1474	-> 125
    //   #1460	-> 134
    //   #1477	-> 140
    //   #1478	-> 157
    //   #1479	-> 164
    //   #1480	-> 173
    //   #1481	-> 178
    //   #1482	-> 184
    //   #1481	-> 193
    //   #1484	-> 211
    //   #1489	-> 225
    //   #1490	-> 234
    //   #1492	-> 243
    //   #1493	-> 258
    //   #1494	-> 269
    //   #1495	-> 276
    //   #1496	-> 283
    //   #1498	-> 293
    //   #1501	-> 296
    //   #1504	-> 307
    //   #1505	-> 324
    //   #1506	-> 334
    //   #1507	-> 334
    //   #1506	-> 343
    //   #1509	-> 358
    //   #1513	-> 367
    //   #1514	-> 377
    //   #1515	-> 392
    //   #1516	-> 406
    //   #1517	-> 416
    //   #1492	-> 427
    //   #1522	-> 433
    //   #1523	-> 446
    //   #1524	-> 455
    //   #1526	-> 465
    //   #1528	-> 468
    //   #1529	-> 477
    //   #1530	-> 477
    //   #1531	-> 499
    //   #1529	-> 508
    //   #1522	-> 517
    //   #1535	-> 523
    //   #1536	-> 529
    //   #1537	-> 548
    //   #1536	-> 574
    //   #1537	-> 574
    //   #1536	-> 577
    //   #1538	-> 590
    //   #1540	-> 599
    //   #1541	-> 616
    //   #1543	-> 625
    //   #1545	-> 634
    //   #1549	-> 646
    //   #1551	-> 660
    //   #1555	-> 660
    //   #1556	-> 670
    //   #1551	-> 680
    //   #1558	-> 694
    //   #1540	-> 700
    //   #1561	-> 706
    //   #1563	-> 724
    //   #1564	-> 729
  }
}
