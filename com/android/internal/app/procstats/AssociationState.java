package com.android.internal.app.procstats;

import android.os.Parcel;
import android.os.SystemClock;
import android.os.UserHandle;
import android.util.ArrayMap;
import android.util.Pair;
import android.util.Slog;
import android.util.TimeUtils;
import android.util.proto.ProtoOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Objects;

public final class AssociationState {
  private long mTotalStartUptime;
  
  private int mTotalNesting;
  
  private long mTotalDuration;
  
  private int mTotalCount;
  
  private long mTotalActiveStartUptime;
  
  private int mTotalActiveNesting;
  
  private long mTotalActiveDuration;
  
  private int mTotalActiveCount;
  
  public final class SourceState {
    final AssociationState this$0;
    
    long mTrackingUptime;
    
    long mStartUptime;
    
    int mProcStateSeq = -1;
    
    int mProcState = -1;
    
    int mNesting;
    
    final AssociationState.SourceKey mKey;
    
    boolean mInTrackingList;
    
    long mDuration;
    
    int mCount;
    
    long mActiveStartUptime;
    
    int mActiveProcState = -1;
    
    DurationsTable mActiveDurations;
    
    long mActiveDuration;
    
    int mActiveCount;
    
    SourceState(AssociationState.SourceKey param1SourceKey) {
      this.mKey = param1SourceKey;
    }
    
    public AssociationState getAssociationState() {
      return AssociationState.this;
    }
    
    public String getProcessName() {
      return this.mKey.mProcess;
    }
    
    public int getUid() {
      return this.mKey.mUid;
    }
    
    public void trackProcState(int param1Int1, int param1Int2, long param1Long) {
      param1Int1 = ProcessState.PROCESS_STATE_TO_STATE[param1Int1];
      if (param1Int2 != this.mProcStateSeq) {
        this.mProcStateSeq = param1Int2;
        this.mProcState = param1Int1;
      } else if (param1Int1 < this.mProcState) {
        this.mProcState = param1Int1;
      } 
      if (param1Int1 < 9)
        if (!this.mInTrackingList) {
          this.mInTrackingList = true;
          this.mTrackingUptime = param1Long;
          AssociationState.this.mProcessStats.mTrackingAssociations.add(this);
        }  
    }
    
    public void stop() {
      int i = this.mNesting - 1;
      if (i == 0) {
        long l = SystemClock.uptimeMillis();
        this.mDuration += l - this.mStartUptime;
        stopTracking(l);
      } 
    }
    
    void startActive(long param1Long) {
      if (this.mInTrackingList) {
        if (this.mActiveStartUptime == 0L) {
          this.mActiveStartUptime = param1Long;
          this.mActiveCount++;
          AssociationState.access$108(AssociationState.this);
          if (AssociationState.this.mTotalActiveNesting == 1) {
            AssociationState.access$208(AssociationState.this);
            AssociationState.access$302(AssociationState.this, param1Long);
          } 
        } 
        int i = this.mActiveProcState;
        if (i != this.mProcState) {
          if (i != -1) {
            long l = this.mActiveDuration + param1Long - this.mActiveStartUptime;
            this.mActiveStartUptime = param1Long;
            if (l != 0L) {
              if (this.mActiveDurations == null)
                makeDurations(); 
              this.mActiveDurations.addDuration(this.mActiveProcState, l);
              this.mActiveDuration = 0L;
            } 
          } 
          this.mActiveProcState = this.mProcState;
        } 
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("startActive while not tracking: ");
        stringBuilder.append(this);
        Slog.wtf("ProcessStats", stringBuilder.toString());
      } 
    }
    
    void stopActive(long param1Long) {
      if (this.mActiveStartUptime != 0L) {
        if (!this.mInTrackingList) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("stopActive while not tracking: ");
          stringBuilder.append(this);
          Slog.wtf("ProcessStats", stringBuilder.toString());
        } 
        long l = param1Long - this.mActiveStartUptime;
        this.mActiveStartUptime = 0L;
        DurationsTable durationsTable = this.mActiveDurations;
        if (durationsTable != null) {
          durationsTable.addDuration(this.mActiveProcState, l);
        } else {
          this.mActiveDuration += l;
        } 
        AssociationState.access$110(AssociationState.this);
        if (AssociationState.this.mTotalActiveNesting == 0) {
          AssociationState associationState = AssociationState.this;
          l = associationState.mTotalActiveStartUptime;
          AssociationState.access$414(associationState, param1Long - l);
          AssociationState.access$302(AssociationState.this, 0L);
        } 
      } 
    }
    
    void makeDurations() {
      this.mActiveDurations = new DurationsTable(AssociationState.this.mProcessStats.mTableData);
    }
    
    void stopTracking(long param1Long) {
      AssociationState.access$510(AssociationState.this);
      if (AssociationState.this.mTotalNesting == 0) {
        AssociationState associationState = AssociationState.this;
        long l = associationState.mTotalStartUptime;
        AssociationState.access$614(associationState, param1Long - l);
      } 
      stopActive(param1Long);
      if (this.mInTrackingList) {
        this.mInTrackingList = false;
        this.mProcState = -1;
        ArrayList<SourceState> arrayList = AssociationState.this.mProcessStats.mTrackingAssociations;
        for (int i = arrayList.size() - 1; i >= 0; i--) {
          if (arrayList.get(i) == this) {
            arrayList.remove(i);
            return;
          } 
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Stop tracking didn't find in tracking list: ");
        stringBuilder.append(this);
        Slog.wtf("ProcessStats", stringBuilder.toString());
      } 
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder(64);
      stringBuilder.append("SourceState{");
      stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
      stringBuilder.append(" ");
      stringBuilder.append(this.mKey.mProcess);
      stringBuilder.append("/");
      stringBuilder.append(this.mKey.mUid);
      if (this.mProcState != -1) {
        stringBuilder.append(" ");
        stringBuilder.append(DumpUtils.STATE_NAMES[this.mProcState]);
        stringBuilder.append(" #");
        int i = this.mProcStateSeq;
        stringBuilder.append(i);
      } 
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }
  
  public final class SourceDumpContainer {
    public long mActiveTime;
    
    public final AssociationState.SourceState mState;
    
    public long mTotalTime;
    
    final AssociationState this$0;
    
    public SourceDumpContainer(AssociationState.SourceState param1SourceState) {
      this.mState = param1SourceState;
    }
  }
  
  public static final class SourceKey {
    String mPackage;
    
    String mProcess;
    
    int mUid;
    
    SourceKey(int param1Int, String param1String1, String param1String2) {
      this.mUid = param1Int;
      this.mProcess = param1String1;
      this.mPackage = param1String2;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = param1Object instanceof SourceKey;
      boolean bool1 = false;
      if (!bool)
        return false; 
      param1Object = param1Object;
      if (((SourceKey)param1Object).mUid == this.mUid && Objects.equals(((SourceKey)param1Object).mProcess, this.mProcess)) {
        param1Object = ((SourceKey)param1Object).mPackage;
        String str = this.mPackage;
        if (Objects.equals(param1Object, str))
          bool1 = true; 
      } 
      return bool1;
    }
    
    public int hashCode() {
      int k, i = Integer.hashCode(this.mUid);
      String str = this.mProcess;
      int j = 0;
      if (str == null) {
        k = 0;
      } else {
        k = str.hashCode();
      } 
      str = this.mPackage;
      if (str != null)
        j = str.hashCode() * 33; 
      return i ^ k ^ j;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder(64);
      stringBuilder.append("SourceKey{");
      UserHandle.formatUid(stringBuilder, this.mUid);
      stringBuilder.append(' ');
      stringBuilder.append(this.mProcess);
      stringBuilder.append(' ');
      stringBuilder.append(this.mPackage);
      stringBuilder.append('}');
      return stringBuilder.toString();
    }
  }
  
  final ArrayMap<SourceKey, SourceState> mSources = new ArrayMap<>();
  
  private final ProcessStats mProcessStats;
  
  private final String mProcessName;
  
  private ProcessState mProc;
  
  private final ProcessStats.PackageState mPackageState;
  
  private final String mName;
  
  private static final SourceKey sTmpSourceKey = new SourceKey(0, null, null);
  
  private static final boolean VALIDATE_TIMES = false;
  
  private static final String TAG = "ProcessStats";
  
  private static final boolean DEBUG = false;
  
  static final Comparator<Pair<SourceKey, SourceDumpContainer>> ASSOCIATION_COMPARATOR;
  
  public AssociationState(ProcessStats paramProcessStats, ProcessStats.PackageState paramPackageState, String paramString1, String paramString2, ProcessState paramProcessState) {
    this.mProcessStats = paramProcessStats;
    this.mPackageState = paramPackageState;
    this.mName = paramString1;
    this.mProcessName = paramString2;
    this.mProc = paramProcessState;
  }
  
  public int getUid() {
    return this.mPackageState.mUid;
  }
  
  public String getPackage() {
    return this.mPackageState.mPackageName;
  }
  
  public String getProcessName() {
    return this.mProcessName;
  }
  
  public String getName() {
    return this.mName;
  }
  
  public ProcessState getProcess() {
    return this.mProc;
  }
  
  public void setProcess(ProcessState paramProcessState) {
    this.mProc = paramProcessState;
  }
  
  public long getTotalDuration(long paramLong) {
    long l = this.mTotalDuration;
    if (this.mTotalNesting > 0) {
      paramLong -= this.mTotalStartUptime;
    } else {
      paramLong = 0L;
    } 
    return l + paramLong;
  }
  
  public long getActiveDuration(long paramLong) {
    long l = this.mTotalActiveDuration;
    if (this.mTotalActiveNesting > 0) {
      paramLong -= this.mTotalActiveStartUptime;
    } else {
      paramLong = 0L;
    } 
    return l + paramLong;
  }
  
  public SourceState startSource(int paramInt, String paramString1, String paramString2) {
    SourceKey sourceKey;
    SourceState sourceState;
    synchronized (sTmpSourceKey) {
      sTmpSourceKey.mUid = paramInt;
      sTmpSourceKey.mProcess = paramString1;
      sTmpSourceKey.mPackage = paramString2;
      SourceState sourceState1 = this.mSources.get(sTmpSourceKey);
      sourceState = sourceState1;
      if (sourceState1 == null) {
        SourceKey sourceKey1 = new SourceKey(paramInt, paramString1, paramString2);
        sourceState = new SourceState(sourceKey1);
        this.mSources.put(sourceKey1, sourceState);
      } 
      sourceState.mNesting++;
      if (sourceState.mNesting == 1) {
        long l = SystemClock.uptimeMillis();
        sourceState.mCount++;
        sourceState.mStartUptime = l;
        this.mTotalNesting = paramInt = this.mTotalNesting + 1;
        if (paramInt == 1) {
          this.mTotalCount++;
          this.mTotalStartUptime = l;
        } 
      } 
      return sourceState;
    } 
  }
  
  public void add(AssociationState paramAssociationState) {
    this.mTotalCount += paramAssociationState.mTotalCount;
    long l = this.mTotalDuration;
    this.mTotalDuration += paramAssociationState.mTotalDuration;
    this.mTotalActiveCount += paramAssociationState.mTotalActiveCount;
    this.mTotalActiveDuration += paramAssociationState.mTotalActiveDuration;
    for (int i = paramAssociationState.mSources.size() - 1; i >= 0; i--) {
      SourceKey sourceKey = paramAssociationState.mSources.keyAt(i);
      SourceState sourceState1 = paramAssociationState.mSources.valueAt(i);
      SourceState sourceState2 = this.mSources.get(sourceKey);
      SourceState sourceState3 = sourceState2;
      if (sourceState2 == null) {
        sourceState3 = new SourceState(sourceKey);
        this.mSources.put(sourceKey, sourceState3);
      } 
      sourceState3.mCount += sourceState1.mCount;
      sourceState3.mDuration += sourceState1.mDuration;
      sourceState3.mActiveCount += sourceState1.mActiveCount;
      if (sourceState1.mActiveDuration != 0L || sourceState1.mActiveDurations != null)
        if (sourceState3.mActiveDurations != null) {
          if (sourceState1.mActiveDurations != null) {
            sourceState3.mActiveDurations.addDurations(sourceState1.mActiveDurations);
          } else {
            sourceState3.mActiveDurations.addDuration(sourceState1.mActiveProcState, sourceState1.mActiveDuration);
          } 
        } else if (sourceState1.mActiveDurations != null) {
          sourceState3.makeDurations();
          sourceState3.mActiveDurations.addDurations(sourceState1.mActiveDurations);
          if (sourceState3.mActiveDuration != 0L) {
            sourceState3.mActiveDurations.addDuration(sourceState3.mActiveProcState, sourceState3.mActiveDuration);
            sourceState3.mActiveDuration = 0L;
            sourceState3.mActiveProcState = -1;
          } 
        } else if (sourceState3.mActiveDuration != 0L) {
          if (sourceState3.mActiveProcState == sourceState1.mActiveProcState) {
            sourceState3.mActiveDuration += sourceState1.mActiveDuration;
          } else {
            sourceState3.makeDurations();
            sourceState3.mActiveDurations.addDuration(sourceState3.mActiveProcState, sourceState3.mActiveDuration);
            sourceState3.mActiveDurations.addDuration(sourceState1.mActiveProcState, sourceState1.mActiveDuration);
            sourceState3.mActiveDuration = 0L;
            sourceState3.mActiveProcState = -1;
          } 
        } else {
          sourceState3.mActiveProcState = sourceState1.mActiveProcState;
          sourceState3.mActiveDuration = sourceState1.mActiveDuration;
        }  
    } 
  }
  
  public boolean isInUse() {
    boolean bool;
    if (this.mTotalNesting > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void resetSafely(long paramLong) {
    if (!isInUse()) {
      this.mSources.clear();
      this.mTotalActiveCount = 0;
      this.mTotalCount = 0;
    } else {
      for (int i = this.mSources.size() - 1; i >= 0; i--) {
        SourceState sourceState = this.mSources.valueAt(i);
        if (sourceState.mNesting > 0) {
          sourceState.mCount = 1;
          sourceState.mStartUptime = paramLong;
          sourceState.mDuration = 0L;
          if (sourceState.mActiveStartUptime > 0L) {
            sourceState.mActiveCount = 1;
            sourceState.mActiveStartUptime = paramLong;
          } else {
            sourceState.mActiveCount = 0;
          } 
          sourceState.mActiveDuration = 0L;
          sourceState.mActiveDurations = null;
        } else {
          this.mSources.removeAt(i);
        } 
      } 
      this.mTotalCount = 1;
      this.mTotalStartUptime = paramLong;
      if (this.mTotalActiveNesting > 0) {
        this.mTotalActiveCount = 1;
        this.mTotalActiveStartUptime = paramLong;
      } else {
        this.mTotalActiveCount = 0;
      } 
    } 
    this.mTotalActiveDuration = 0L;
    this.mTotalDuration = 0L;
  }
  
  public void writeToParcel(ProcessStats paramProcessStats, Parcel paramParcel, long paramLong) {
    paramParcel.writeInt(this.mTotalCount);
    paramParcel.writeLong(this.mTotalDuration);
    paramParcel.writeInt(this.mTotalActiveCount);
    paramParcel.writeLong(this.mTotalActiveDuration);
    int i = this.mSources.size();
    paramParcel.writeInt(i);
    for (byte b = 0; b < i; b++) {
      SourceKey sourceKey = this.mSources.keyAt(b);
      SourceState sourceState = this.mSources.valueAt(b);
      paramParcel.writeInt(sourceKey.mUid);
      paramProcessStats.writeCommonString(paramParcel, sourceKey.mProcess);
      paramProcessStats.writeCommonString(paramParcel, sourceKey.mPackage);
      paramParcel.writeInt(sourceState.mCount);
      paramParcel.writeLong(sourceState.mDuration);
      paramParcel.writeInt(sourceState.mActiveCount);
      if (sourceState.mActiveDurations != null) {
        paramParcel.writeInt(1);
        sourceState.mActiveDurations.writeToParcel(paramParcel);
      } else {
        paramParcel.writeInt(0);
        paramParcel.writeInt(sourceState.mActiveProcState);
        paramParcel.writeLong(sourceState.mActiveDuration);
      } 
    } 
  }
  
  public String readFromParcel(ProcessStats paramProcessStats, Parcel paramParcel, int paramInt) {
    StringBuilder stringBuilder;
    this.mTotalCount = paramParcel.readInt();
    this.mTotalDuration = paramParcel.readLong();
    this.mTotalActiveCount = paramParcel.readInt();
    this.mTotalActiveDuration = paramParcel.readLong();
    int i = paramParcel.readInt();
    if (i < 0 || i > 100000) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Association with bad src count: ");
      stringBuilder.append(i);
      return stringBuilder.toString();
    } 
    for (byte b = 0; b < i; b++) {
      int j = paramParcel.readInt();
      String str1 = stringBuilder.readCommonString(paramParcel, paramInt);
      String str2 = stringBuilder.readCommonString(paramParcel, paramInt);
      SourceKey sourceKey = new SourceKey(j, str1, str2);
      SourceState sourceState = new SourceState(sourceKey);
      sourceState.mCount = paramParcel.readInt();
      sourceState.mDuration = paramParcel.readLong();
      sourceState.mActiveCount = paramParcel.readInt();
      if (paramParcel.readInt() != 0) {
        sourceState.makeDurations();
        if (!sourceState.mActiveDurations.readFromParcel(paramParcel)) {
          stringBuilder = new StringBuilder();
          stringBuilder.append("Duration table corrupt: ");
          stringBuilder.append(sourceKey);
          stringBuilder.append(" <- ");
          stringBuilder.append(sourceState);
          return stringBuilder.toString();
        } 
      } else {
        sourceState.mActiveProcState = paramParcel.readInt();
        sourceState.mActiveDuration = paramParcel.readLong();
      } 
      this.mSources.put(sourceKey, sourceState);
    } 
    return null;
  }
  
  public void commitStateTime(long paramLong) {
    if (isInUse()) {
      for (int i = this.mSources.size() - 1; i >= 0; i--) {
        SourceState sourceState = this.mSources.valueAt(i);
        if (sourceState.mNesting > 0) {
          sourceState.mDuration += paramLong - sourceState.mStartUptime;
          sourceState.mStartUptime = paramLong;
        } 
        if (sourceState.mActiveStartUptime > 0L) {
          long l = paramLong - sourceState.mActiveStartUptime;
          sourceState.mActiveStartUptime = paramLong;
          if (sourceState.mActiveDurations != null) {
            sourceState.mActiveDurations.addDuration(sourceState.mActiveProcState, l);
          } else {
            sourceState.mActiveDuration += l;
          } 
        } 
      } 
      if (this.mTotalNesting > 0) {
        this.mTotalDuration += paramLong - this.mTotalStartUptime;
        this.mTotalStartUptime = paramLong;
      } 
      if (this.mTotalActiveNesting > 0) {
        this.mTotalActiveDuration += paramLong - this.mTotalActiveStartUptime;
        this.mTotalActiveStartUptime = paramLong;
      } 
    } 
  }
  
  public boolean hasProcessOrPackage(String paramString) {
    if (this.mProcessName.equals(paramString))
      return true; 
    int i = this.mSources.size();
    for (byte b = 0; b < i; b++) {
      SourceKey sourceKey = this.mSources.keyAt(b);
      if (paramString.equals(sourceKey.mProcess) || paramString.equals(sourceKey.mPackage))
        return true; 
    } 
    return false;
  }
  
  static {
    ASSOCIATION_COMPARATOR = (Comparator<Pair<SourceKey, SourceDumpContainer>>)_$$Lambda$AssociationState$kgfxYpOOyQWCFPwGaRqRz0N4_zg.INSTANCE;
  }
  
  public ArrayList<Pair<SourceKey, SourceDumpContainer>> createSortedAssociations(long paramLong1, long paramLong2) {
    int i = this.mSources.size();
    ArrayList<Pair<SourceKey, SourceDumpContainer>> arrayList = new ArrayList(i);
    for (byte b = 0; b < i; b++) {
      SourceState sourceState = this.mSources.valueAt(b);
      SourceDumpContainer sourceDumpContainer = new SourceDumpContainer(sourceState);
      long l = sourceState.mDuration;
      if (sourceState.mNesting > 0)
        l += paramLong1 - sourceState.mStartUptime; 
      sourceDumpContainer.mTotalTime = l;
      sourceDumpContainer.mActiveTime = dumpTime(null, null, sourceState, paramLong2, paramLong1, false, false);
      if (sourceDumpContainer.mActiveTime < 0L)
        sourceDumpContainer.mActiveTime = -sourceDumpContainer.mActiveTime; 
      arrayList.add(new Pair<>(this.mSources.keyAt(b), sourceDumpContainer));
    } 
    Collections.sort(arrayList, ASSOCIATION_COMPARATOR);
    return arrayList;
  }
  
  public void dumpStats(PrintWriter paramPrintWriter, String paramString1, String paramString2, String paramString3, ArrayList<Pair<SourceKey, SourceDumpContainer>> paramArrayList, long paramLong1, long paramLong2, String paramString4, boolean paramBoolean1, boolean paramBoolean2) {
    long l1 = paramLong2;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString2);
    stringBuilder.append("     ");
    String str2 = stringBuilder.toString();
    long l2 = this.mTotalActiveDuration;
    long l3 = l2;
    if (this.mTotalActiveNesting > 0)
      l3 = l2 + paramLong1 - this.mTotalActiveStartUptime; 
    String str1 = ": time ", str3 = " / ", str4 = ": ";
    if (l3 > 0L || this.mTotalActiveCount != 0) {
      paramPrintWriter.print(paramString1);
      paramPrintWriter.print("Active count ");
      paramPrintWriter.print(this.mTotalActiveCount);
      if (paramBoolean2) {
        paramPrintWriter.print(": ");
        TimeUtils.formatDuration(l3, paramPrintWriter);
        paramPrintWriter.print(" / ");
      } else {
        paramPrintWriter.print(": time ");
      } 
      DumpUtils.printPercent(paramPrintWriter, l3 / l1);
      paramPrintWriter.println();
    } 
    if (paramBoolean2 && this.mTotalActiveNesting != 0) {
      paramPrintWriter.print(paramString1);
      paramPrintWriter.print("mTotalActiveNesting=");
      paramPrintWriter.print(this.mTotalActiveNesting);
      paramPrintWriter.print(" mTotalActiveStartUptime=");
      TimeUtils.formatDuration(this.mTotalActiveStartUptime, paramLong1, paramPrintWriter);
      paramPrintWriter.println();
    } 
    l3 = this.mTotalDuration;
    if (this.mTotalNesting > 0)
      l3 += paramLong1 - this.mTotalStartUptime; 
    if (l3 > 0L || this.mTotalCount != 0) {
      paramPrintWriter.print(paramString1);
      paramPrintWriter.print("Total count ");
      paramPrintWriter.print(this.mTotalCount);
      if (paramBoolean2) {
        paramPrintWriter.print(": ");
        TimeUtils.formatDuration(l3, paramPrintWriter);
        paramPrintWriter.print(" / ");
      } else {
        paramPrintWriter.print(": time ");
      } 
      DumpUtils.printPercent(paramPrintWriter, l3 / l1);
      paramPrintWriter.println();
    } 
    if (paramBoolean2 && this.mTotalNesting != 0) {
      paramPrintWriter.print(paramString1);
      paramPrintWriter.print("mTotalNesting=");
      paramPrintWriter.print(this.mTotalNesting);
      paramPrintWriter.print(" mTotalStartUptime=");
      TimeUtils.formatDuration(this.mTotalStartUptime, paramLong1, paramPrintWriter);
      paramPrintWriter.println();
    } 
    int i = paramArrayList.size();
    for (byte b = 0; b < i; b++) {
      SourceKey sourceKey = (SourceKey)((Pair)paramArrayList.get(b)).first;
      SourceDumpContainer sourceDumpContainer = (SourceDumpContainer)((Pair)paramArrayList.get(b)).second;
      SourceState sourceState = sourceDumpContainer.mState;
      paramPrintWriter.print(paramString1);
      paramPrintWriter.print("<- ");
      paramPrintWriter.print(sourceKey.mProcess);
      paramPrintWriter.print("/");
      UserHandle.formatUid(paramPrintWriter, sourceKey.mUid);
      if (sourceKey.mPackage != null) {
        paramPrintWriter.print(" (");
        paramPrintWriter.print(sourceKey.mPackage);
        paramPrintWriter.print(")");
      } 
      if (paramString4 != null && !paramString4.equals(sourceKey.mProcess)) {
        String str5 = sourceKey.mPackage;
        if (!paramString4.equals(str5)) {
          paramPrintWriter.println();
          str5 = str1;
          continue;
        } 
      } 
      paramPrintWriter.println(":");
      if (sourceState.mActiveCount != 0 || sourceState.mActiveDurations != null || sourceState.mActiveDuration != 0L || sourceState.mActiveStartUptime != 0L) {
        paramPrintWriter.print(paramString2);
        paramPrintWriter.print("   Active count ");
        paramPrintWriter.print(sourceState.mActiveCount);
        if (paramBoolean1) {
          if (paramBoolean2)
            if (sourceState.mActiveDurations != null) {
              paramPrintWriter.print(" (multi-state)");
            } else if (sourceState.mActiveProcState >= 0) {
              paramPrintWriter.print(" (");
              paramPrintWriter.print(DumpUtils.STATE_NAMES[sourceState.mActiveProcState]);
              paramPrintWriter.print(")");
            } else {
              paramPrintWriter.print(" (*UNKNOWN STATE*)");
            }  
          if (paramBoolean2) {
            paramPrintWriter.print(str4);
            TimeUtils.formatDuration(sourceDumpContainer.mActiveTime, paramPrintWriter);
            paramPrintWriter.print(str3);
          } else {
            paramPrintWriter.print(str1);
          } 
          DumpUtils.printPercent(paramPrintWriter, sourceDumpContainer.mActiveTime / l1);
          if (sourceState.mActiveStartUptime != 0L)
            paramPrintWriter.print(" (running)"); 
          paramPrintWriter.println();
          if (sourceState.mActiveDurations != null)
            dumpTime(paramPrintWriter, str2, sourceState, paramLong2, paramLong1, paramBoolean1, paramBoolean2); 
        } else {
          paramPrintWriter.print(str4);
          dumpActiveDurationSummary(paramPrintWriter, sourceState, paramLong2, paramLong1, paramBoolean2);
        } 
      } 
      paramPrintWriter.print(paramString2);
      paramPrintWriter.print("   Total count ");
      paramPrintWriter.print(sourceState.mCount);
      if (paramBoolean2) {
        paramPrintWriter.print(str4);
        TimeUtils.formatDuration(sourceDumpContainer.mTotalTime, paramPrintWriter);
        paramPrintWriter.print(str3);
      } else {
        paramPrintWriter.print(str1);
      } 
      String str = str1;
      double d = sourceDumpContainer.mTotalTime;
      l3 = paramLong2;
      DumpUtils.printPercent(paramPrintWriter, d / l3);
      if (sourceState.mNesting > 0) {
        paramPrintWriter.print(" (running");
        if (paramBoolean2) {
          paramPrintWriter.print(" nest=");
          paramPrintWriter.print(sourceState.mNesting);
        } 
        if (sourceState.mProcState != -1) {
          paramPrintWriter.print(str3);
          paramPrintWriter.print(DumpUtils.STATE_NAMES[sourceState.mProcState]);
          paramPrintWriter.print(" #");
          paramPrintWriter.print(sourceState.mProcStateSeq);
        } 
        paramPrintWriter.print(")");
      } 
      paramPrintWriter.println();
      l1 = l3;
      if (paramBoolean2) {
        if (sourceState.mInTrackingList) {
          paramPrintWriter.print(paramString2);
          paramPrintWriter.print("   mInTrackingList=");
          paramPrintWriter.println(sourceState.mInTrackingList);
        } 
        l1 = l3;
        if (sourceState.mProcState != -1) {
          paramPrintWriter.print(paramString2);
          paramPrintWriter.print("   mProcState=");
          paramPrintWriter.print(DumpUtils.STATE_NAMES[sourceState.mProcState]);
          paramPrintWriter.print(" mProcStateSeq=");
          paramPrintWriter.println(sourceState.mProcStateSeq);
          l1 = l3;
        } 
      } 
      continue;
    } 
  }
  
  void dumpActiveDurationSummary(PrintWriter paramPrintWriter, SourceState paramSourceState, long paramLong1, long paramLong2, boolean paramBoolean) {
    boolean bool;
    long l = dumpTime(null, null, paramSourceState, paramLong1, paramLong2, false, false);
    if (l < 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    paramLong2 = l;
    if (bool)
      paramLong2 = -l; 
    if (paramBoolean) {
      TimeUtils.formatDuration(paramLong2, paramPrintWriter);
      paramPrintWriter.print(" / ");
    } else {
      paramPrintWriter.print("time ");
    } 
    DumpUtils.printPercent(paramPrintWriter, paramLong2 / paramLong1);
    if (paramSourceState.mActiveStartUptime > 0L)
      paramPrintWriter.print(" (running)"); 
    paramPrintWriter.println();
  }
  
  long dumpTime(PrintWriter paramPrintWriter, String paramString, SourceState paramSourceState, long paramLong1, long paramLong2, boolean paramBoolean1, boolean paramBoolean2) {
    long l = 0L;
    boolean bool = false;
    for (byte b = 0; b < 14; b++) {
      long l1;
      String str;
      if (paramSourceState.mActiveDurations != null) {
        l1 = paramSourceState.mActiveDurations.getValueForId((byte)b);
      } else if (paramSourceState.mActiveProcState == b) {
        l1 = paramSourceState.mActiveDuration;
      } else {
        l1 = 0L;
      } 
      if (paramSourceState.mActiveStartUptime != 0L && paramSourceState.mActiveProcState == b) {
        str = " (running)";
        bool = true;
        l1 += paramLong2 - paramSourceState.mActiveStartUptime;
      } else {
        str = null;
      } 
      if (l1 != 0L) {
        if (paramPrintWriter != null) {
          paramPrintWriter.print(paramString);
          paramPrintWriter.print(DumpUtils.STATE_LABELS[b]);
          paramPrintWriter.print(": ");
          if (paramBoolean2) {
            TimeUtils.formatDuration(l1, paramPrintWriter);
            paramPrintWriter.print(" / ");
          } else {
            paramPrintWriter.print("time ");
          } 
          DumpUtils.printPercent(paramPrintWriter, l1 / paramLong1);
          if (str != null)
            paramPrintWriter.print(str); 
          paramPrintWriter.println();
        } 
        l += l1;
      } 
    } 
    if (bool) {
      paramLong1 = -l;
    } else {
      paramLong1 = l;
    } 
    return paramLong1;
  }
  
  public void dumpTimesCheckin(PrintWriter paramPrintWriter, String paramString1, int paramInt, long paramLong1, String paramString2, long paramLong2) {
    int i = this.mSources.size();
    for (byte b = 0; b < i; b++) {
      SourceKey sourceKey = this.mSources.keyAt(b);
      SourceState sourceState = this.mSources.valueAt(b);
      paramPrintWriter.print("pkgasc");
      paramPrintWriter.print(",");
      paramPrintWriter.print(paramString1);
      paramPrintWriter.print(",");
      paramPrintWriter.print(paramInt);
      paramPrintWriter.print(",");
      paramPrintWriter.print(paramLong1);
      paramPrintWriter.print(",");
      paramPrintWriter.print(paramString2);
      paramPrintWriter.print(",");
      paramPrintWriter.print(sourceKey.mProcess);
      paramPrintWriter.print(",");
      paramPrintWriter.print(sourceKey.mUid);
      paramPrintWriter.print(",");
      paramPrintWriter.print(sourceState.mCount);
      long l1 = sourceState.mDuration;
      long l2 = l1;
      if (sourceState.mNesting > 0)
        l2 = l1 + paramLong2 - sourceState.mStartUptime; 
      paramPrintWriter.print(",");
      paramPrintWriter.print(l2);
      paramPrintWriter.print(",");
      paramPrintWriter.print(sourceState.mActiveCount);
      if (sourceState.mActiveStartUptime != 0L) {
        l2 = paramLong2 - sourceState.mActiveStartUptime;
      } else {
        l2 = 0L;
      } 
      if (sourceState.mActiveDurations != null) {
        int j = sourceState.mActiveDurations.getKeyCount();
        for (byte b1 = 0; b1 < j; b1++) {
          int k = sourceState.mActiveDurations.getKeyAt(b1);
          long l = sourceState.mActiveDurations.getValue(k);
          l1 = l;
          if (k == sourceState.mActiveProcState)
            l1 = l + l2; 
          k = SparseMappingTable.getIdFromKey(k);
          paramPrintWriter.print(",");
          DumpUtils.printArrayEntry(paramPrintWriter, DumpUtils.STATE_TAGS, k, 1);
          paramPrintWriter.print(':');
          paramPrintWriter.print(l1);
        } 
      } else {
        int j = i;
        l2 = sourceState.mActiveDuration + l2;
        i = j;
        if (l2 != 0L) {
          paramPrintWriter.print(",");
          DumpUtils.printArrayEntry(paramPrintWriter, DumpUtils.STATE_TAGS, sourceState.mActiveProcState, 1);
          paramPrintWriter.print(':');
          paramPrintWriter.print(l2);
          i = j;
        } 
      } 
      paramPrintWriter.println();
    } 
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong1, long paramLong2) {
    long l = paramProtoOutputStream.start(paramLong1);
    paramProtoOutputStream.write(1138166333441L, this.mName);
    paramProtoOutputStream.write(1120986464259L, this.mTotalCount);
    paramProtoOutputStream.write(1112396529668L, getTotalDuration(paramLong2));
    int i = this.mTotalActiveCount;
    if (i != 0) {
      paramProtoOutputStream.write(1120986464261L, i);
      paramLong1 = getActiveDuration(paramLong2);
      paramProtoOutputStream.write(1112396529670L, paramLong1);
    } 
    i = this.mSources.size();
    for (byte b = 0; b < i; b++) {
      SourceKey sourceKey = this.mSources.keyAt(b);
      SourceState sourceState = this.mSources.valueAt(b);
      paramLong1 = paramProtoOutputStream.start(2246267895810L);
      paramProtoOutputStream.write(1138166333442L, sourceKey.mProcess);
      paramProtoOutputStream.write(1138166333447L, sourceKey.mPackage);
      paramProtoOutputStream.write(1120986464257L, sourceKey.mUid);
      paramProtoOutputStream.write(1120986464259L, sourceState.mCount);
      long l1 = sourceState.mDuration;
      if (sourceState.mNesting > 0)
        l1 += paramLong2 - sourceState.mStartUptime; 
      paramProtoOutputStream.write(1112396529668L, l1);
      if (sourceState.mActiveCount != 0)
        paramProtoOutputStream.write(1120986464261L, sourceState.mActiveCount); 
      if (sourceState.mActiveStartUptime != 0L) {
        l1 = paramLong2 - sourceState.mActiveStartUptime;
      } else {
        l1 = 0L;
      } 
      if (sourceState.mActiveDurations != null) {
        int j = sourceState.mActiveDurations.getKeyCount();
        for (byte b1 = 0; b1 < j; b1++) {
          int k = sourceState.mActiveDurations.getKeyAt(b1);
          long l2 = sourceState.mActiveDurations.getValue(k);
          if (k == sourceState.mActiveProcState)
            l2 += l1; 
          k = SparseMappingTable.getIdFromKey(k);
          long l3 = paramProtoOutputStream.start(2246267895814L);
          DumpUtils.printProto(paramProtoOutputStream, 1159641169921L, DumpUtils.STATE_PROTO_ENUMS, k, 1);
          paramProtoOutputStream.write(1112396529666L, l2);
          paramProtoOutputStream.end(l3);
        } 
        l1 = paramLong1;
      } else {
        long l2 = sourceState.mActiveDuration + l1;
        l1 = paramLong1;
        if (l2 != 0L) {
          l1 = paramProtoOutputStream.start(2246267895814L);
          DumpUtils.printProto(paramProtoOutputStream, 1159641169921L, DumpUtils.STATE_PROTO_ENUMS, sourceState.mActiveProcState, 1);
          paramProtoOutputStream.write(1112396529666L, l2);
          paramProtoOutputStream.end(l1);
          l1 = paramLong1;
        } 
      } 
      paramProtoOutputStream.end(l1);
    } 
    paramProtoOutputStream.end(l);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("AssociationState{");
    stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
    stringBuilder.append(" ");
    stringBuilder.append(this.mName);
    stringBuilder.append(" pkg=");
    stringBuilder.append(this.mPackageState.mPackageName);
    stringBuilder.append(" proc=");
    ProcessState processState = this.mProc;
    stringBuilder.append(Integer.toHexString(System.identityHashCode(processState)));
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
}
