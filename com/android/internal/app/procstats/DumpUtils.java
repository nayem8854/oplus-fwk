package com.android.internal.app.procstats;

import android.os.UserHandle;
import android.util.TimeUtils;
import android.util.proto.ProtoOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

public final class DumpUtils {
  public static final String[] ADJ_MEM_NAMES_CSV;
  
  static final int[] ADJ_MEM_PROTO_ENUMS;
  
  static final String[] ADJ_MEM_TAGS;
  
  public static final String[] ADJ_SCREEN_NAMES_CSV;
  
  static final int[] ADJ_SCREEN_PROTO_ENUMS;
  
  static final String[] ADJ_SCREEN_TAGS;
  
  static final String CSV_SEP = "\t";
  
  private static final int[] PROCESS_STATS_STATE_TO_AGGREGATED_STATE;
  
  public static final String[] STATE_LABELS;
  
  static {
    String[] arrayOfString = new String[14];
    arrayOfString[0] = "Persist";
    arrayOfString[1] = "Top";
    arrayOfString[2] = "ImpFg";
    arrayOfString[3] = "ImpBg";
    arrayOfString[4] = "Backup";
    arrayOfString[5] = "Service";
    arrayOfString[6] = "ServRst";
    arrayOfString[7] = "Receivr";
    arrayOfString[8] = "HeavyWt";
    arrayOfString[9] = "Home";
    arrayOfString[10] = "LastAct";
    arrayOfString[11] = "CchAct";
    arrayOfString[12] = "CchCAct";
    arrayOfString[13] = "CchEmty";
    STATE_LABELS = arrayOfString = new String[14];
    arrayOfString[0] = "Persistent";
    arrayOfString[1] = "       Top";
    arrayOfString[2] = "    Imp Fg";
    arrayOfString[3] = "    Imp Bg";
    arrayOfString[4] = "    Backup";
    arrayOfString[5] = "   Service";
    arrayOfString[6] = "Service Rs";
    arrayOfString[7] = "  Receiver";
    arrayOfString[8] = " Heavy Wgt";
    arrayOfString[9] = "    (Home)";
    arrayOfString[10] = "(Last Act)";
    arrayOfString[11] = " (Cch Act)";
    arrayOfString[12] = "(Cch CAct)";
    arrayOfString[13] = "(Cch Emty)";
  }
  
  public static final String STATE_LABEL_CACHED = "  (Cached)";
  
  public static final String STATE_LABEL_TOTAL = "     TOTAL";
  
  public static final String[] STATE_NAMES;
  
  public static final String[] STATE_NAMES_CSV;
  
  static final int[] STATE_PROTO_ENUMS;
  
  static final String[] STATE_TAGS;
  
  static {
    STATE_NAMES_CSV = arrayOfString = new String[14];
    arrayOfString[0] = "pers";
    arrayOfString[1] = "top";
    arrayOfString[2] = "impfg";
    arrayOfString[3] = "impbg";
    arrayOfString[4] = "backup";
    arrayOfString[5] = "service";
    arrayOfString[6] = "service-rs";
    arrayOfString[7] = "receiver";
    arrayOfString[8] = "heavy";
    arrayOfString[9] = "home";
    arrayOfString[10] = "lastact";
    arrayOfString[11] = "cch-activity";
    arrayOfString[12] = "cch-aclient";
    arrayOfString[13] = "cch-empty";
    STATE_TAGS = arrayOfString = new String[14];
    arrayOfString[0] = "p";
    arrayOfString[1] = "t";
    arrayOfString[2] = "f";
    arrayOfString[3] = "b";
    arrayOfString[4] = "u";
    arrayOfString[5] = "s";
    arrayOfString[6] = "x";
    arrayOfString[7] = "r";
    arrayOfString[8] = "w";
    arrayOfString[9] = "h";
    arrayOfString[10] = "l";
    arrayOfString[11] = "a";
    arrayOfString[12] = "c";
    arrayOfString[13] = "e";
    int[] arrayOfInt = new int[14];
    arrayOfInt[0] = 1;
    arrayOfInt[1] = 2;
    arrayOfInt[2] = 3;
    arrayOfInt[3] = 4;
    arrayOfInt[4] = 5;
    arrayOfInt[5] = 6;
    arrayOfInt[6] = 7;
    arrayOfInt[7] = 8;
    arrayOfInt[8] = 9;
    arrayOfInt[9] = 10;
    arrayOfInt[10] = 11;
    arrayOfInt[11] = 12;
    arrayOfInt[12] = 13;
    arrayOfInt[13] = 14;
    PROCESS_STATS_STATE_TO_AGGREGATED_STATE = arrayOfInt = new int[14];
    arrayOfInt[0] = 1;
    arrayOfInt[1] = 2;
    arrayOfInt[2] = 5;
    arrayOfInt[3] = 6;
    arrayOfInt[4] = 6;
    arrayOfInt[5] = 6;
    arrayOfInt[6] = 0;
    arrayOfInt[7] = 7;
    arrayOfInt[8] = 6;
    arrayOfInt[9] = 8;
    arrayOfInt[10] = 8;
    arrayOfInt[11] = 8;
    arrayOfInt[12] = 8;
    arrayOfInt[13] = 8;
    ADJ_SCREEN_NAMES_CSV = new String[] { "off", "on" };
    ADJ_MEM_NAMES_CSV = new String[] { "norm", "mod", "low", "crit" };
    ADJ_SCREEN_TAGS = new String[] { "0", "1" };
    ADJ_SCREEN_PROTO_ENUMS = new int[] { 1, 2 };
    ADJ_MEM_TAGS = new String[] { "n", "m", "l", "c" };
    ADJ_MEM_PROTO_ENUMS = new int[] { 1, 2, 3, 4 };
  }
  
  public static void printScreenLabel(PrintWriter paramPrintWriter, int paramInt) {
    if (paramInt != -1) {
      if (paramInt != 0) {
        if (paramInt != 4) {
          paramPrintWriter.print("????/");
        } else {
          paramPrintWriter.print(" SOn/");
        } 
      } else {
        paramPrintWriter.print("SOff/");
      } 
    } else {
      paramPrintWriter.print("     ");
    } 
  }
  
  public static void printScreenLabelCsv(PrintWriter paramPrintWriter, int paramInt) {
    if (paramInt != -1)
      if (paramInt != 0) {
        if (paramInt != 4) {
          paramPrintWriter.print("???");
        } else {
          paramPrintWriter.print(ADJ_SCREEN_NAMES_CSV[1]);
        } 
      } else {
        paramPrintWriter.print(ADJ_SCREEN_NAMES_CSV[0]);
      }  
  }
  
  public static void printMemLabel(PrintWriter paramPrintWriter, int paramInt, char paramChar) {
    if (paramInt != -1) {
      if (paramInt != 0) {
        if (paramInt != 1) {
          if (paramInt != 2) {
            if (paramInt != 3) {
              paramPrintWriter.print("????");
              if (paramChar != '\000')
                paramPrintWriter.print(paramChar); 
            } else {
              paramPrintWriter.print("Crit");
              if (paramChar != '\000')
                paramPrintWriter.print(paramChar); 
            } 
          } else {
            paramPrintWriter.print(" Low");
            if (paramChar != '\000')
              paramPrintWriter.print(paramChar); 
          } 
        } else {
          paramPrintWriter.print(" Mod");
          if (paramChar != '\000')
            paramPrintWriter.print(paramChar); 
        } 
      } else {
        paramPrintWriter.print("Norm");
        if (paramChar != '\000')
          paramPrintWriter.print(paramChar); 
      } 
    } else {
      paramPrintWriter.print("    ");
      if (paramChar != '\000')
        paramPrintWriter.print(' '); 
    } 
  }
  
  public static void printMemLabelCsv(PrintWriter paramPrintWriter, int paramInt) {
    if (paramInt >= 0)
      if (paramInt <= 3) {
        paramPrintWriter.print(ADJ_MEM_NAMES_CSV[paramInt]);
      } else {
        paramPrintWriter.print("???");
      }  
  }
  
  public static void printPercent(PrintWriter paramPrintWriter, double paramDouble) {
    paramDouble *= 100.0D;
    if (paramDouble < 1.0D) {
      paramPrintWriter.print(String.format("%.2f", new Object[] { Double.valueOf(paramDouble) }));
    } else if (paramDouble < 10.0D) {
      paramPrintWriter.print(String.format("%.1f", new Object[] { Double.valueOf(paramDouble) }));
    } else {
      paramPrintWriter.print(String.format("%.0f", new Object[] { Double.valueOf(paramDouble) }));
    } 
    paramPrintWriter.print("%");
  }
  
  public static void printProcStateTag(PrintWriter paramPrintWriter, int paramInt) {
    paramInt = printArrayEntry(paramPrintWriter, ADJ_SCREEN_TAGS, paramInt, 56);
    paramInt = printArrayEntry(paramPrintWriter, ADJ_MEM_TAGS, paramInt, 14);
    printArrayEntry(paramPrintWriter, STATE_TAGS, paramInt, 1);
  }
  
  public static void printProcStateTagProto(ProtoOutputStream paramProtoOutputStream, long paramLong1, long paramLong2, long paramLong3, int paramInt) {
    paramInt = printProto(paramProtoOutputStream, paramLong1, ADJ_SCREEN_PROTO_ENUMS, paramInt, 56);
    paramInt = printProto(paramProtoOutputStream, paramLong2, ADJ_MEM_PROTO_ENUMS, paramInt, 14);
    printProto(paramProtoOutputStream, paramLong3, STATE_PROTO_ENUMS, paramInt, 1);
  }
  
  public static void printAdjTag(PrintWriter paramPrintWriter, int paramInt) {
    paramInt = printArrayEntry(paramPrintWriter, ADJ_SCREEN_TAGS, paramInt, 4);
    printArrayEntry(paramPrintWriter, ADJ_MEM_TAGS, paramInt, 1);
  }
  
  public static void printProcStateAdjTagProto(ProtoOutputStream paramProtoOutputStream, long paramLong1, long paramLong2, int paramInt) {
    paramInt = printProto(paramProtoOutputStream, paramLong1, ADJ_SCREEN_PROTO_ENUMS, paramInt, 56);
    printProto(paramProtoOutputStream, paramLong2, ADJ_MEM_PROTO_ENUMS, paramInt, 14);
  }
  
  public static void printProcStateDurationProto(ProtoOutputStream paramProtoOutputStream, long paramLong1, int paramInt, long paramLong2) {
    paramLong1 = paramProtoOutputStream.start(paramLong1);
    printProto(paramProtoOutputStream, 1159641169923L, STATE_PROTO_ENUMS, paramInt, 1);
    paramProtoOutputStream.write(1112396529668L, paramLong2);
    paramProtoOutputStream.end(paramLong1);
  }
  
  public static void printProcStateTagAndValue(PrintWriter paramPrintWriter, int paramInt, long paramLong) {
    paramPrintWriter.print(',');
    printProcStateTag(paramPrintWriter, paramInt);
    paramPrintWriter.print(':');
    paramPrintWriter.print(paramLong);
  }
  
  public static void printAdjTagAndValue(PrintWriter paramPrintWriter, int paramInt, long paramLong) {
    paramPrintWriter.print(',');
    printAdjTag(paramPrintWriter, paramInt);
    paramPrintWriter.print(':');
    paramPrintWriter.print(paramLong);
  }
  
  public static long dumpSingleTime(PrintWriter paramPrintWriter, String paramString, long[] paramArrayOflong, int paramInt, long paramLong1, long paramLong2) {
    long l = 0L;
    int i = -1;
    for (byte b = 0; b < 8; b += 4) {
      int j = -1;
      for (byte b1 = 0; b1 < 4; b1++, l = l1, i = m, j = k) {
        int k = b1 + b;
        long l1 = paramArrayOflong[k];
        String str1 = "";
        long l2 = l1;
        String str2 = str1;
        if (paramInt == k) {
          l1 += paramLong2 - paramLong1;
          l2 = l1;
          str2 = str1;
          if (paramPrintWriter != null) {
            str2 = " (running)";
            l2 = l1;
          } 
        } 
        l1 = l;
        int m = i;
        k = j;
        if (l2 != 0L) {
          m = i;
          k = j;
          if (paramPrintWriter != null) {
            paramPrintWriter.print(paramString);
            k = -1;
            if (i != b) {
              i = b;
            } else {
              i = -1;
            } 
            printScreenLabel(paramPrintWriter, i);
            m = b;
            i = k;
            if (j != b1)
              i = b1; 
            printMemLabel(paramPrintWriter, i, false);
            k = b1;
            paramPrintWriter.print(": ");
            TimeUtils.formatDuration(l2, paramPrintWriter);
            paramPrintWriter.println(str2);
          } 
          l1 = l + l2;
        } 
      } 
    } 
    if (l != 0L && paramPrintWriter != null) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("    TOTAL: ");
      TimeUtils.formatDuration(l, paramPrintWriter);
      paramPrintWriter.println();
    } 
    return l;
  }
  
  public static void dumpAdjTimesCheckin(PrintWriter paramPrintWriter, String paramString, long[] paramArrayOflong, int paramInt, long paramLong1, long paramLong2) {
    for (byte b = 0; b < 8; b += 4) {
      for (byte b1 = 0; b1 < 4; b1++) {
        int i = b1 + b;
        long l1 = paramArrayOflong[i];
        long l2 = l1;
        if (paramInt == i)
          l2 = l1 + paramLong2 - paramLong1; 
        if (l2 != 0L)
          printAdjTagAndValue(paramPrintWriter, i, l2); 
      } 
    } 
  }
  
  private static void dumpStateHeadersCsv(PrintWriter paramPrintWriter, String paramString, int[] paramArrayOfint1, int[] paramArrayOfint2, int[] paramArrayOfint3) {
    byte b1;
    byte b2;
    byte b3;
    if (paramArrayOfint1 != null) {
      b1 = paramArrayOfint1.length;
    } else {
      b1 = 1;
    } 
    if (paramArrayOfint2 != null) {
      b2 = paramArrayOfint2.length;
    } else {
      b2 = 1;
    } 
    if (paramArrayOfint3 != null) {
      b3 = paramArrayOfint3.length;
    } else {
      b3 = 1;
    } 
    for (byte b4 = 0; b4 < b1; b4++) {
      for (byte b = 0; b < b2; b++) {
        for (byte b5 = 0; b5 < b3; b5++) {
          paramPrintWriter.print(paramString);
          boolean bool1 = false;
          boolean bool2 = bool1;
          if (paramArrayOfint1 != null) {
            bool2 = bool1;
            if (paramArrayOfint1.length > 1) {
              printScreenLabelCsv(paramPrintWriter, paramArrayOfint1[b4]);
              bool2 = true;
            } 
          } 
          bool1 = bool2;
          if (paramArrayOfint2 != null) {
            bool1 = bool2;
            if (paramArrayOfint2.length > 1) {
              if (bool2)
                paramPrintWriter.print("-"); 
              printMemLabelCsv(paramPrintWriter, paramArrayOfint2[b]);
              bool1 = true;
            } 
          } 
          if (paramArrayOfint3 != null && paramArrayOfint3.length > 1) {
            if (bool1)
              paramPrintWriter.print("-"); 
            paramPrintWriter.print(STATE_NAMES_CSV[paramArrayOfint3[b5]]);
          } 
        } 
      } 
    } 
  }
  
  public static void dumpProcessSummaryLocked(PrintWriter paramPrintWriter, String paramString1, String paramString2, ArrayList<ProcessState> paramArrayList, int[] paramArrayOfint1, int[] paramArrayOfint2, int[] paramArrayOfint3, long paramLong1, long paramLong2) {
    for (int i = paramArrayList.size() - 1; i >= 0; i--) {
      ProcessState processState = paramArrayList.get(i);
      processState.dumpSummary(paramPrintWriter, paramString1, paramString2, paramArrayOfint1, paramArrayOfint2, paramArrayOfint3, paramLong1, paramLong2);
    } 
  }
  
  public static void dumpProcessListCsv(PrintWriter paramPrintWriter, ArrayList<ProcessState> paramArrayList, boolean paramBoolean1, int[] paramArrayOfint1, boolean paramBoolean2, int[] paramArrayOfint2, boolean paramBoolean3, int[] paramArrayOfint3, long paramLong) {
    int[] arrayOfInt2, arrayOfInt3;
    paramPrintWriter.print("process");
    paramPrintWriter.print("\t");
    paramPrintWriter.print("uid");
    paramPrintWriter.print("\t");
    paramPrintWriter.print("vers");
    int[] arrayOfInt1 = null;
    if (paramBoolean1) {
      arrayOfInt2 = paramArrayOfint1;
    } else {
      arrayOfInt2 = null;
    } 
    if (paramBoolean2) {
      arrayOfInt3 = paramArrayOfint2;
    } else {
      arrayOfInt3 = null;
    } 
    if (paramBoolean3)
      arrayOfInt1 = paramArrayOfint3; 
    dumpStateHeadersCsv(paramPrintWriter, "\t", arrayOfInt2, arrayOfInt3, arrayOfInt1);
    paramPrintWriter.println();
    for (int i = paramArrayList.size() - 1; i >= 0; i--) {
      ProcessState processState = paramArrayList.get(i);
      paramPrintWriter.print(processState.getName());
      paramPrintWriter.print("\t");
      UserHandle.formatUid(paramPrintWriter, processState.getUid());
      paramPrintWriter.print("\t");
      paramPrintWriter.print(processState.getVersion());
      processState.dumpCsv(paramPrintWriter, paramBoolean1, paramArrayOfint1, paramBoolean2, paramArrayOfint2, paramBoolean3, paramArrayOfint3, paramLong);
      paramPrintWriter.println();
    } 
  }
  
  public static int printArrayEntry(PrintWriter paramPrintWriter, String[] paramArrayOfString, int paramInt1, int paramInt2) {
    int i = paramInt1 / paramInt2;
    if (i >= 0 && i < paramArrayOfString.length) {
      paramPrintWriter.print(paramArrayOfString[i]);
    } else {
      paramPrintWriter.print('?');
    } 
    return paramInt1 - i * paramInt2;
  }
  
  public static int printProto(ProtoOutputStream paramProtoOutputStream, long paramLong, int[] paramArrayOfint, int paramInt1, int paramInt2) {
    int i = paramInt1 / paramInt2;
    if (i >= 0 && i < paramArrayOfint.length)
      paramProtoOutputStream.write(paramLong, paramArrayOfint[i]); 
    return paramInt1 - i * paramInt2;
  }
  
  public static String collapseString(String paramString1, String paramString2) {
    if (paramString2.startsWith(paramString1)) {
      int i = paramString2.length();
      int j = paramString1.length();
      if (i == j)
        return ""; 
      if (i >= j && 
        paramString2.charAt(j) == '.')
        return paramString2.substring(j); 
    } 
    return paramString2;
  }
  
  public static int aggregateCurrentProcessState(int paramInt) {
    int i = paramInt / 56;
    try {
      paramInt = PROCESS_STATS_STATE_TO_AGGREGATED_STATE[paramInt % 14];
    } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
      paramInt = 0;
    } 
    return paramInt << 15 | i;
  }
  
  public static void printAggregatedProcStateTagProto(ProtoOutputStream paramProtoOutputStream, long paramLong1, long paramLong2, int paramInt) {
    try {
      paramProtoOutputStream.write(paramLong2, paramInt >> 15);
    } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
      paramProtoOutputStream.write(paramLong2, 0);
    } 
    try {
      paramProtoOutputStream.write(paramLong1, ADJ_SCREEN_PROTO_ENUMS[paramInt & 0xF]);
    } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
      paramProtoOutputStream.write(paramLong1, 0);
    } 
  }
}
