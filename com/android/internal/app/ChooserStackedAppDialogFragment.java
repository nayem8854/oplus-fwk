package com.android.internal.app;

import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.UserHandle;
import com.android.internal.app.chooser.DisplayResolveInfo;
import com.android.internal.app.chooser.MultiDisplayResolveInfo;

public class ChooserStackedAppDialogFragment extends ChooserTargetActionsDialogFragment implements DialogInterface.OnClickListener {
  private MultiDisplayResolveInfo mMultiDisplayResolveInfo;
  
  private int mParentWhich;
  
  public ChooserStackedAppDialogFragment() {}
  
  public ChooserStackedAppDialogFragment(MultiDisplayResolveInfo paramMultiDisplayResolveInfo, int paramInt, UserHandle paramUserHandle) {
    super(paramMultiDisplayResolveInfo.getTargets(), paramUserHandle);
    this.mMultiDisplayResolveInfo = paramMultiDisplayResolveInfo;
    this.mParentWhich = paramInt;
  }
  
  protected CharSequence getItemLabel(DisplayResolveInfo paramDisplayResolveInfo) {
    PackageManager packageManager = getContext().getPackageManager();
    return paramDisplayResolveInfo.getResolveInfo().loadLabel(packageManager);
  }
  
  protected Drawable getItemIcon(DisplayResolveInfo paramDisplayResolveInfo) {
    return null;
  }
  
  public void onClick(DialogInterface paramDialogInterface, int paramInt) {
    this.mMultiDisplayResolveInfo.setSelected(paramInt);
    ((ChooserActivity)getActivity()).startSelected(this.mParentWhich, false, true);
    dismiss();
  }
}
