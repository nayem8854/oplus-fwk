package com.android.internal.app;

import android.app.ActivityManager;
import android.app.prediction.AppPredictor;
import android.app.prediction.AppTarget;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ShortcutInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.UserHandle;
import android.os.UserManager;
import android.provider.DeviceConfig;
import android.service.chooser.ChooserTarget;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import com.android.internal.app.chooser.ChooserTargetInfo;
import com.android.internal.app.chooser.DisplayResolveInfo;
import com.android.internal.app.chooser.SelectableTargetInfo;
import com.android.internal.app.chooser.TargetInfo;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ChooserListAdapter extends ResolverListAdapter {
  private boolean mAppendDirectShareEnabled = DeviceConfig.getBoolean("systemui", "append_direct_share_enabled", true);
  
  private boolean mEnableStackedApps = true;
  
  private int mNumShortcutResults = 0;
  
  private Map<DisplayResolveInfo, ResolverListAdapter.LoadIconTask> mIconLoaders = new HashMap<>();
  
  private ChooserTargetInfo mPlaceHolderTargetInfo = new ChooserActivity.PlaceHolderTargetInfo();
  
  private int mValidServiceTargetsNum = 0;
  
  private int mAvailableServiceTargetsNum = 0;
  
  private final Map<ComponentName, Pair<List<ChooserTargetInfo>, Integer>> mParkingDirectShareTargets = new HashMap<>();
  
  private final Map<ComponentName, Map<String, Integer>> mChooserTargetScores = new HashMap<>();
  
  private Set<ComponentName> mPendingChooserTargetService = new HashSet<>();
  
  private Set<ComponentName> mShortcutComponents = new HashSet<>();
  
  private final List<ChooserTargetInfo> mServiceTargets = new ArrayList<>();
  
  private final List<DisplayResolveInfo> mCallerTargets = new ArrayList<>();
  
  private final ChooserActivity.BaseChooserTargetComparator mBaseTargetComparator = new ChooserActivity.BaseChooserTargetComparator();
  
  private boolean mListViewDataChanged = false;
  
  private List<DisplayResolveInfo> mSortedList = new ArrayList<>();
  
  public static final float CALLER_TARGET_SCORE_BOOST = 900.0F;
  
  private static final boolean DEBUG = false;
  
  private static final int DEFAULT_DIRECT_SHARE_RANKING_SCORE = 1000;
  
  private static final int MAX_CHOOSER_TARGETS_PER_APP = 2;
  
  static final int MAX_SERVICE_TARGETS = 8;
  
  private static final int MAX_SERVICE_TARGET_APP = 8;
  
  private static final int MAX_SUGGESTED_APP_TARGETS = 4;
  
  public static final int NO_POSITION = -1;
  
  public static final float SHORTCUT_TARGET_SCORE_BOOST = 90.0F;
  
  private static final String TAG = "ChooserListAdapter";
  
  public static final int TARGET_BAD = -1;
  
  public static final int TARGET_CALLER = 0;
  
  public static final int TARGET_SERVICE = 1;
  
  public static final int TARGET_STANDARD = 2;
  
  public static final int TARGET_STANDARD_AZ = 3;
  
  private AppPredictor mAppPredictor;
  
  private AppPredictor.Callback mAppPredictorCallback;
  
  private final ChooserListCommunicator mChooserListCommunicator;
  
  private final int mMaxShortcutTargetsPerApp;
  
  private final SelectableTargetInfo.SelectableTargetInfoCommunicator mSelectableTargetInfoCommunicator;
  
  public ChooserListAdapter(Context paramContext, List<Intent> paramList, Intent[] paramArrayOfIntent, List<ResolveInfo> paramList1, boolean paramBoolean, ResolverListController paramResolverListController, ChooserListCommunicator paramChooserListCommunicator, SelectableTargetInfo.SelectableTargetInfoCommunicator paramSelectableTargetInfoCommunicator, PackageManager paramPackageManager) {
    super(paramContext, paramList, (Intent[])null, paramList1, paramBoolean, paramResolverListController, paramChooserListCommunicator, false);
    createPlaceHolders();
    this.mMaxShortcutTargetsPerApp = paramContext.getResources().getInteger(17694837);
    this.mChooserListCommunicator = paramChooserListCommunicator;
    this.mSelectableTargetInfoCommunicator = paramSelectableTargetInfoCommunicator;
    if (paramArrayOfIntent != null)
      for (byte b = 0; b < paramArrayOfIntent.length; b++) {
        Intent intent = paramArrayOfIntent[b];
        if (intent != null) {
          ActivityInfo activityInfo1;
          ResolveInfo resolveInfo;
          paramList1 = null;
          paramResolverListController = null;
          paramList = null;
          paramChooserListCommunicator = null;
          ComponentName componentName = intent.getComponent();
          if (componentName != null) {
            ResolverListController resolverListController = paramResolverListController;
            ChooserListCommunicator chooserListCommunicator = paramChooserListCommunicator;
            try {
              ActivityInfo activityInfo = paramPackageManager.getActivityInfo(intent.getComponent(), 0);
              resolverListController = paramResolverListController;
              activityInfo1 = activityInfo;
              ResolveInfo resolveInfo2 = new ResolveInfo();
              resolverListController = paramResolverListController;
              activityInfo1 = activityInfo;
              this();
              ResolveInfo resolveInfo1 = resolveInfo2;
              resolveInfo = resolveInfo1;
              activityInfo1 = activityInfo;
              resolveInfo1.activityInfo = activityInfo;
              resolveInfo = resolveInfo1;
              activityInfo1 = activityInfo;
            } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {}
          } 
          ActivityInfo activityInfo2 = activityInfo1;
          if (activityInfo1 == null) {
            resolveInfo = paramPackageManager.resolveActivity(intent, 65536);
            if (resolveInfo != null) {
              activityInfo1 = resolveInfo.activityInfo;
            } else {
              activityInfo1 = null;
            } 
            activityInfo2 = activityInfo1;
          } 
          if (activityInfo2 == null) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("No activity found for ");
            stringBuilder.append(intent);
            Log.w("ChooserListAdapter", stringBuilder.toString());
          } else {
            UserManager userManager = (UserManager)paramContext.getSystemService("user");
            if (intent instanceof LabeledIntent) {
              LabeledIntent labeledIntent = (LabeledIntent)intent;
              resolveInfo.resolvePackageName = labeledIntent.getSourcePackage();
              resolveInfo.labelRes = labeledIntent.getLabelResource();
              resolveInfo.nonLocalizedLabel = labeledIntent.getNonLocalizedLabel();
              resolveInfo.icon = labeledIntent.getIconResource();
              resolveInfo.iconResourceId = resolveInfo.icon;
            } 
            if (userManager.isManagedProfile()) {
              resolveInfo.noResourceId = true;
              resolveInfo.icon = 0;
            } 
            this.mCallerTargets.add(new DisplayResolveInfo(intent, resolveInfo, intent, makePresentationGetter(resolveInfo)));
            if (this.mCallerTargets.size() == 4)
              break; 
          } 
        } 
      }  
  }
  
  AppPredictor getAppPredictor() {
    return this.mAppPredictor;
  }
  
  public void handlePackagesChanged() {
    createPlaceHolders();
    this.mChooserListCommunicator.onHandlePackagesChanged(this);
  }
  
  public void notifyDataSetChanged() {
    if (!this.mListViewDataChanged) {
      this.mChooserListCommunicator.sendListViewUpdateMessage(getUserHandle());
      this.mListViewDataChanged = true;
    } 
  }
  
  void refreshListView() {
    if (this.mListViewDataChanged) {
      if (this.mAppendDirectShareEnabled)
        appendServiceTargetsWithQuota(); 
      super.notifyDataSetChanged();
    } 
    this.mListViewDataChanged = false;
  }
  
  private void createPlaceHolders() {
    this.mNumShortcutResults = 0;
    this.mServiceTargets.clear();
    this.mValidServiceTargetsNum = 0;
    this.mParkingDirectShareTargets.clear();
    this.mPendingChooserTargetService.clear();
    this.mShortcutComponents.clear();
    for (byte b = 0; b < 8; b++)
      this.mServiceTargets.add(this.mPlaceHolderTargetInfo); 
  }
  
  View onCreateView(ViewGroup paramViewGroup) {
    return this.mInflater.inflate(17367270, paramViewGroup, false);
  }
  
  protected void onBindView(View paramView, TargetInfo paramTargetInfo, int paramInt) {
    Drawable drawable;
    ResolverListAdapter.ViewHolder viewHolder = (ResolverListAdapter.ViewHolder)paramView.getTag();
    if (paramTargetInfo == null) {
      paramView = viewHolder.icon;
      Context context = this.mContext;
      drawable = context.getDrawable(17303426);
      paramView.setImageDrawable(drawable);
      return;
    } 
    if (!(drawable instanceof DisplayResolveInfo)) {
      viewHolder.bindLabel(drawable.getDisplayLabel(), drawable.getExtendedInfo(), alwaysShowSubLabel());
      viewHolder.bindIcon((TargetInfo)drawable);
      if (drawable instanceof SelectableTargetInfo) {
        DisplayResolveInfo displayResolveInfo = ((SelectableTargetInfo)drawable).getDisplayResolveInfo();
        CharSequence charSequence2 = "";
        if (displayResolveInfo != null) {
          charSequence1 = displayResolveInfo.getDisplayLabel();
        } else {
          charSequence1 = "";
        } 
        CharSequence charSequence3 = drawable.getExtendedInfo();
        CharSequence charSequence4 = drawable.getDisplayLabel();
        if (charSequence3 != null)
          charSequence2 = charSequence3; 
        CharSequence charSequence1 = String.join(" ", new CharSequence[] { charSequence4, charSequence2, charSequence1 });
        viewHolder.updateContentDescription((String)charSequence1);
      } 
    } else {
      DisplayResolveInfo displayResolveInfo = (DisplayResolveInfo)drawable;
      viewHolder.bindLabel(displayResolveInfo.getDisplayLabel(), displayResolveInfo.getExtendedInfo(), alwaysShowSubLabel());
      ResolverListAdapter.LoadIconTask loadIconTask = this.mIconLoaders.get(displayResolveInfo);
      if (loadIconTask == null) {
        loadIconTask = new ResolverListAdapter.LoadIconTask(this, displayResolveInfo, viewHolder);
        this.mIconLoaders.put(displayResolveInfo, loadIconTask);
        loadIconTask.execute((Object[])new Void[0]);
      } else {
        loadIconTask.setViewHolder(viewHolder);
      } 
    } 
    if (drawable instanceof ChooserActivity.PlaceHolderTargetInfo) {
      int i = this.mContext.getResources().getDimensionPixelSize(17105033);
      viewHolder.text.setMaxWidth(i);
      paramView = viewHolder.text;
      Resources resources = this.mContext.getResources();
      Context context = this.mContext;
      Resources.Theme theme = context.getTheme();
      paramView.setBackground(resources.getDrawable(17302116, theme));
      viewHolder.itemView.setBackground(null);
    } else {
      viewHolder.text.setMaxWidth(2147483647);
      viewHolder.text.setBackground(null);
      viewHolder.itemView.setBackground(viewHolder.defaultItemViewBackground);
    } 
    if (drawable instanceof com.android.internal.app.chooser.MultiDisplayResolveInfo) {
      Drawable drawable1 = this.mContext.getDrawable(17302118);
      viewHolder.text.setPaddingRelative(0, 0, drawable1.getIntrinsicWidth(), 0);
      viewHolder.text.setBackground(drawable1);
    } else if (drawable.isPinned() && getPositionTargetType(paramInt) == 2) {
      Drawable drawable1 = this.mContext.getDrawable(17302119);
      viewHolder.text.setPaddingRelative(drawable1.getIntrinsicWidth(), 0, 0, 0);
      viewHolder.text.setBackground(drawable1);
    } else {
      viewHolder.text.setBackground(null);
      viewHolder.text.setPaddingRelative(0, 0, 0, 0);
    } 
  }
  
  void updateAlphabeticalList() {
    Object object = new Object(this);
    object.execute((Object[])new Void[0]);
  }
  
  public int getCount() {
    int i = getRankedTargetCount(), j = getAlphaTargetCount();
    int k = getSelectableServiceTargetCount(), m = getCallerTargetCount();
    return i + j + k + m;
  }
  
  public int getUnfilteredCount() {
    int i = super.getUnfilteredCount();
    int j = i;
    if (i > this.mChooserListCommunicator.getMaxRankedTargets())
      j = i + this.mChooserListCommunicator.getMaxRankedTargets(); 
    return getSelectableServiceTargetCount() + j + getCallerTargetCount();
  }
  
  public int getCallerTargetCount() {
    return this.mCallerTargets.size();
  }
  
  public int getSelectableServiceTargetCount() {
    int i = 0;
    for (ChooserTargetInfo chooserTargetInfo : this.mServiceTargets) {
      int j = i;
      if (chooserTargetInfo instanceof SelectableTargetInfo)
        j = i + 1; 
      i = j;
    } 
    return i;
  }
  
  public int getServiceTargetCount() {
    ChooserListCommunicator chooserListCommunicator = this.mChooserListCommunicator;
    if (chooserListCommunicator.isSendAction(chooserListCommunicator.getTargetIntent()) && 
      !ActivityManager.isLowRamDeviceStatic())
      return Math.min(this.mServiceTargets.size(), 8); 
    return 0;
  }
  
  int getAlphaTargetCount() {
    int i = this.mSortedList.size();
    int j = this.mCallerTargets.size(), k = this.mDisplayList.size();
    if (j + k <= this.mChooserListCommunicator.getMaxRankedTargets())
      i = 0; 
    return i;
  }
  
  public int getRankedTargetCount() {
    ChooserListCommunicator chooserListCommunicator = this.mChooserListCommunicator;
    int i = chooserListCommunicator.getMaxRankedTargets(), j = getCallerTargetCount();
    return Math.min(i - j, super.getCount());
  }
  
  public int getPositionTargetType(int paramInt) {
    int i = getServiceTargetCount();
    if (paramInt < i)
      return 1; 
    int j = 0 + i;
    i = getCallerTargetCount();
    if (paramInt - j < i)
      return 0; 
    i = j + i;
    int k = getRankedTargetCount();
    if (paramInt - i < k)
      return 2; 
    j = getAlphaTargetCount();
    if (paramInt - i + k < j)
      return 3; 
    return -1;
  }
  
  public TargetInfo getItem(int paramInt) {
    return targetInfoForPosition(paramInt, true);
  }
  
  public TargetInfo targetInfoForPosition(int paramInt, boolean paramBoolean) {
    if (paramInt == -1)
      return null; 
    if (paramBoolean) {
      i = getServiceTargetCount();
    } else {
      i = getSelectableServiceTargetCount();
    } 
    if (paramInt < i)
      return this.mServiceTargets.get(paramInt); 
    int j = 0 + i;
    int i = getCallerTargetCount();
    if (paramInt - j < i)
      return this.mCallerTargets.get(paramInt - j); 
    j += i;
    i = getRankedTargetCount();
    if (paramInt - j < i) {
      TargetInfo targetInfo;
      if (paramBoolean) {
        targetInfo = super.getItem(paramInt - j);
      } else {
        targetInfo = getDisplayResolveInfo(paramInt - j);
      } 
      return targetInfo;
    } 
    i = j + i;
    if (paramInt - i < getAlphaTargetCount() && !this.mSortedList.isEmpty())
      return this.mSortedList.get(paramInt - i); 
    return null;
  }
  
  protected boolean shouldAddResolveInfo(DisplayResolveInfo paramDisplayResolveInfo) {
    for (TargetInfo targetInfo : this.mCallerTargets) {
      ResolverListAdapter.ResolverListCommunicator resolverListCommunicator = this.mResolverListCommunicator;
      if (resolverListCommunicator.resolveInfoMatch(paramDisplayResolveInfo.getResolveInfo(), targetInfo.getResolveInfo()))
        return false; 
    } 
    return super.shouldAddResolveInfo(paramDisplayResolveInfo);
  }
  
  public List<ChooserTargetInfo> getSurfacedTargetInfo() {
    int i = this.mChooserListCommunicator.getMaxRankedTargets();
    List<ChooserTargetInfo> list = this.mServiceTargets;
    i = Math.min(i, getSelectableServiceTargetCount());
    return list.subList(0, i);
  }
  
  public void addServiceResults(DisplayResolveInfo paramDisplayResolveInfo, List<ChooserTarget> paramList, int paramInt, Map<ChooserTarget, ShortcutInfo> paramMap, List<ChooserActivity.ChooserTargetServiceConnection> paramList1) {
    if (this.mAppendDirectShareEnabled) {
      parkTargetIntoMemory(paramDisplayResolveInfo, paramList, paramInt, paramMap, paramList1);
      return;
    } 
    if (paramList.size() == 0)
      return; 
    float f1 = getBaseScore(paramDisplayResolveInfo, paramInt);
    Collections.sort(paramList, this.mBaseTargetComparator);
    int i = 2;
    if (paramInt == 2 || paramInt == 3) {
      paramInt = 1;
    } else {
      paramInt = 0;
    } 
    if (paramInt != 0)
      i = this.mMaxShortcutTargetsPerApp; 
    float f2;
    boolean bool;
    byte b;
    for (i = Math.min(paramList.size(), i), f2 = 0.0F, bool = false, b = 0; b < i; b++) {
      ChooserTarget chooserTarget = paramList.get(b);
      float f = chooserTarget.getScore();
      f *= f1;
      if (b > 0 && f >= f2) {
        f2 *= 0.95F;
      } else {
        f2 = f;
      } 
      UserHandle userHandle = getUserHandle();
      Context context = this.mContext.createContextAsUser(userHandle, 0);
      SelectableTargetInfo.SelectableTargetInfoCommunicator selectableTargetInfoCommunicator = this.mSelectableTargetInfoCommunicator;
      if (paramInt != 0) {
        ShortcutInfo shortcutInfo = paramMap.get(chooserTarget);
      } else {
        userHandle = null;
      } 
      SelectableTargetInfo selectableTargetInfo = new SelectableTargetInfo(context, paramDisplayResolveInfo, chooserTarget, f2, selectableTargetInfoCommunicator, (ShortcutInfo)userHandle);
      boolean bool1 = insertServiceTarget(selectableTargetInfo);
      if (bool1 && paramInt != 0)
        this.mNumShortcutResults++; 
      bool |= bool1;
    } 
    if (bool)
      notifyDataSetChanged(); 
  }
  
  public void addChooserTargetRankingScore(List<AppTarget> paramList) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("addChooserTargetRankingScore ");
    stringBuilder.append(paramList.size());
    stringBuilder.append(" targets score.");
    Log.i("ChooserListAdapter", stringBuilder.toString());
    for (AppTarget appTarget : paramList) {
      if (appTarget.getShortcutInfo() == null)
        continue; 
      ShortcutInfo shortcutInfo = appTarget.getShortcutInfo();
      if (!shortcutInfo.getId().equals("chooser_target") || 
        shortcutInfo.getActivity() == null)
        continue; 
      ComponentName componentName = shortcutInfo.getActivity();
      if (!this.mChooserTargetScores.containsKey(componentName))
        this.mChooserTargetScores.put(componentName, new HashMap<>()); 
      Map<String, Integer> map = this.mChooserTargetScores.get(componentName);
      String str = shortcutInfo.getShortLabel().toString();
      int i = appTarget.getRank();
      map.put(str, Integer.valueOf(i));
    } 
    this.mChooserTargetScores.keySet().forEach(new _$$Lambda$ChooserListAdapter$BPqHx_vC6QGjxaqE_AOsfarff_k(this));
  }
  
  private void rankTargetsWithinComponent(ComponentName paramComponentName) {
    if (this.mParkingDirectShareTargets.containsKey(paramComponentName)) {
      Map<ComponentName, Map<String, Integer>> map = this.mChooserTargetScores;
      if (map.containsKey(paramComponentName)) {
        map = (Map<ComponentName, Map<String, Integer>>)this.mChooserTargetScores.get(paramComponentName);
        Collections.sort((List)((Pair)this.mParkingDirectShareTargets.get(paramComponentName)).first, new _$$Lambda$ChooserListAdapter$m4s9b945UP7D1fhargxJSp6PfrQ(map));
        return;
      } 
    } 
  }
  
  private void parkTargetIntoMemory(DisplayResolveInfo paramDisplayResolveInfo, List<ChooserTarget> paramList, int paramInt, Map<ChooserTarget, ShortcutInfo> paramMap, List<ChooserActivity.ChooserTargetServiceConnection> paramList1) {
    ComponentName componentName;
    if (paramDisplayResolveInfo != null) {
      componentName = paramDisplayResolveInfo.getResolvedComponentName();
    } else if (!paramList.isEmpty()) {
      componentName = ((ChooserTarget)paramList.get(0)).getComponentName();
    } else {
      componentName = null;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("parkTargetIntoMemory ");
    stringBuilder.append(componentName);
    stringBuilder.append(", ");
    stringBuilder.append(paramList.size());
    stringBuilder.append(" targets");
    String str = stringBuilder.toString();
    Log.i("ChooserListAdapter", str);
    Stream<ChooserActivity.ChooserTargetServiceConnection> stream2 = paramList1.stream();
    -$.Lambda.eUIP3HCCOJv_84jco_cNoee_sJE eUIP3HCCOJv_84jco_cNoee_sJE = _$$Lambda$eUIP3HCCOJv_84jco_cNoee_sJE.INSTANCE;
    stream2 = stream2.map((Function<? super ChooserActivity.ChooserTargetServiceConnection, ? extends ChooserActivity.ChooserTargetServiceConnection>)eUIP3HCCOJv_84jco_cNoee_sJE);
    _$$Lambda$ChooserListAdapter$9fcWpLCX10Fv87hEFnpnFupxJkA _$$Lambda$ChooserListAdapter$9fcWpLCX10Fv87hEFnpnFupxJkA = new _$$Lambda$ChooserListAdapter$9fcWpLCX10Fv87hEFnpnFupxJkA(componentName);
    Stream<ChooserActivity.ChooserTargetServiceConnection> stream1 = stream2.filter(_$$Lambda$ChooserListAdapter$9fcWpLCX10Fv87hEFnpnFupxJkA);
    this.mPendingChooserTargetService = stream1.collect((Collector)Collectors.toSet());
    if (!paramList.isEmpty()) {
      boolean bool;
      if (paramInt == 2 || paramInt == 3) {
        bool = true;
      } else {
        bool = false;
      } 
      Context context = this.mContext.createContextAsUser(getUserHandle(), 0);
      Stream<ChooserTarget> stream3 = paramList.stream();
      _$$Lambda$ChooserListAdapter$8HtMdVlUWw1aFqhES60XWwoBEJA _$$Lambda$ChooserListAdapter$8HtMdVlUWw1aFqhES60XWwoBEJA = new _$$Lambda$ChooserListAdapter$8HtMdVlUWw1aFqhES60XWwoBEJA(this, context, paramDisplayResolveInfo, bool, paramMap);
      Stream<?> stream = stream3.map(_$$Lambda$ChooserListAdapter$8HtMdVlUWw1aFqhES60XWwoBEJA);
      List<ChooserTargetInfo> list = stream.collect((Collector)Collectors.toList());
      Map<ComponentName, Pair<List<ChooserTargetInfo>, Integer>> map = this.mParkingDirectShareTargets;
      ArrayList arrayList = new ArrayList();
      Pair<ArrayList, Integer> pair1 = new Pair<>(arrayList, Integer.valueOf(0));
      Pair<List<ChooserTargetInfo>, Integer> pair = (Pair)map.getOrDefault(componentName, pair1);
      for (ChooserTargetInfo chooserTargetInfo : list) {
        if (!checkDuplicateTarget(chooserTargetInfo, (List<ChooserTargetInfo>)pair.first)) {
          list = this.mServiceTargets;
          if (!checkDuplicateTarget(chooserTargetInfo, list)) {
            ((List<ChooserTargetInfo>)pair.first).add(chooserTargetInfo);
            this.mAvailableServiceTargetsNum++;
          } 
        } 
      } 
      this.mParkingDirectShareTargets.put(componentName, pair);
      rankTargetsWithinComponent(componentName);
      if (bool)
        this.mShortcutComponents.add(componentName); 
    } 
    notifyDataSetChanged();
  }
  
  private void appendServiceTargetsWithQuota() {
    int i = this.mChooserListCommunicator.getMaxRankedTargets();
    List<ComponentName> list = getTopComponentNames(i);
    float f = 0.0F;
    for (ComponentName componentName : list) {
      if (!this.mPendingChooserTargetService.contains(componentName)) {
        Map<ComponentName, Pair<List<ChooserTargetInfo>, Integer>> map = this.mParkingDirectShareTargets;
        if (!map.containsKey(componentName))
          continue; 
      } 
      f += getScore(componentName);
    } 
    int j = 0;
    for (ComponentName componentName : list) {
      if (!this.mPendingChooserTargetService.contains(componentName)) {
        Map<ComponentName, Pair<List<ChooserTargetInfo>, Integer>> map1 = this.mParkingDirectShareTargets;
        if (!map1.containsKey(componentName))
          continue; 
      } 
      float f1 = getScore(componentName);
      int k = Math.round(i * f1 / f);
      int m = j;
      if (this.mPendingChooserTargetService.contains(componentName)) {
        m = j;
        if (k >= 1)
          m = 1; 
      } 
      if (!this.mParkingDirectShareTargets.containsKey(componentName)) {
        j = m;
        continue;
      } 
      Map<ComponentName, Pair<List<ChooserTargetInfo>, Integer>> map = this.mParkingDirectShareTargets;
      Pair pair = map.get(componentName);
      List<ChooserTargetInfo> list1 = (List)pair.first;
      int n = ((Integer)pair.second).intValue();
      while (n < k && !list1.isEmpty()) {
        j = n;
        if (!checkDuplicateTarget(list1.get(0), this.mServiceTargets)) {
          this.mServiceTargets.add(this.mValidServiceTargetsNum, list1.get(0));
          this.mValidServiceTargetsNum++;
          j = n + 1;
        } 
        list1.remove(0);
        n = j;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(" appendServiceTargetsWithQuota component=");
      stringBuilder.append(componentName);
      stringBuilder.append(" appendNum=");
      Integer integer = (Integer)pair.second;
      stringBuilder.append(n - integer.intValue());
      String str = stringBuilder.toString();
      Log.i("ChooserListAdapter", str);
      this.mParkingDirectShareTargets.put(componentName, new Pair<>(list1, Integer.valueOf(n)));
      j = m;
    } 
    if (j == 0)
      fillAllServiceTargets(); 
  }
  
  private void fillAllServiceTargets() {
    if (this.mParkingDirectShareTargets.isEmpty())
      return; 
    Log.i("ChooserListAdapter", " fillAllServiceTargets");
    List<ComponentName> list = getTopComponentNames(8);
    for (ComponentName componentName : list) {
      if (!this.mParkingDirectShareTargets.containsKey(componentName))
        continue; 
      Stream stream = ((List)((Pair)this.mParkingDirectShareTargets.get(componentName)).first).stream();
      _$$Lambda$ChooserListAdapter$t1fxZkPY66xDVAw4rhWJz79dzdo _$$Lambda$ChooserListAdapter$t1fxZkPY66xDVAw4rhWJz79dzdo = new _$$Lambda$ChooserListAdapter$t1fxZkPY66xDVAw4rhWJz79dzdo(this);
      stream = stream.filter(_$$Lambda$ChooserListAdapter$t1fxZkPY66xDVAw4rhWJz79dzdo);
      _$$Lambda$ChooserListAdapter$Nwx4mGR8tVNyHPJ4o1Cv_NzUD2w _$$Lambda$ChooserListAdapter$Nwx4mGR8tVNyHPJ4o1Cv_NzUD2w = new _$$Lambda$ChooserListAdapter$Nwx4mGR8tVNyHPJ4o1Cv_NzUD2w(this);
      stream.forEach(_$$Lambda$ChooserListAdapter$Nwx4mGR8tVNyHPJ4o1Cv_NzUD2w);
      this.mParkingDirectShareTargets.remove(componentName);
    } 
    Stream stream1 = this.mParkingDirectShareTargets.entrySet().stream();
    _$$Lambda$ChooserListAdapter$Khh7sa_N2QGpDvgRJ__JLOOB_s8 _$$Lambda$ChooserListAdapter$Khh7sa_N2QGpDvgRJ__JLOOB_s8 = new _$$Lambda$ChooserListAdapter$Khh7sa_N2QGpDvgRJ__JLOOB_s8(this);
    Stream stream2 = stream1.filter(_$$Lambda$ChooserListAdapter$Khh7sa_N2QGpDvgRJ__JLOOB_s8);
    -$.Lambda.ChooserListAdapter.-y1PMD8VhneiKR1eaA1eOCTG3yI -y1PMD8VhneiKR1eaA1eOCTG3yI = _$$Lambda$ChooserListAdapter$_y1PMD8VhneiKR1eaA1eOCTG3yI.INSTANCE;
    stream2 = stream2.map((Function)-y1PMD8VhneiKR1eaA1eOCTG3yI);
    -$.Lambda.ChooserListAdapter.ykAZrfXZaaDsIfVL4lfLhsTJwOM ykAZrfXZaaDsIfVL4lfLhsTJwOM = _$$Lambda$ChooserListAdapter$ykAZrfXZaaDsIfVL4lfLhsTJwOM.INSTANCE;
    stream2 = stream2.map((Function)ykAZrfXZaaDsIfVL4lfLhsTJwOM);
    _$$Lambda$ChooserListAdapter$N30acEPcmN7qH6i_USfrNyHBL1o _$$Lambda$ChooserListAdapter$N30acEPcmN7qH6i_USfrNyHBL1o = new _$$Lambda$ChooserListAdapter$N30acEPcmN7qH6i_USfrNyHBL1o(this);
    stream2.forEach(_$$Lambda$ChooserListAdapter$N30acEPcmN7qH6i_USfrNyHBL1o);
    this.mParkingDirectShareTargets.clear();
  }
  
  private boolean checkDuplicateTarget(ChooserTargetInfo paramChooserTargetInfo, List<ChooserTargetInfo> paramList) {
    for (ChooserTargetInfo chooserTargetInfo : paramList) {
      if (paramChooserTargetInfo.isSimilar(chooserTargetInfo))
        return true; 
    } 
    return false;
  }
  
  int getNumServiceTargetsForExpand() {
    int i;
    if (this.mAppendDirectShareEnabled) {
      i = this.mAvailableServiceTargetsNum;
    } else {
      i = this.mNumShortcutResults;
    } 
    return i;
  }
  
  public float getBaseScore(DisplayResolveInfo paramDisplayResolveInfo, int paramInt) {
    if (paramDisplayResolveInfo == null)
      return 900.0F; 
    if (paramInt == 3)
      return 90.0F; 
    float f = getScore(paramDisplayResolveInfo);
    if (paramInt == 2)
      return 90.0F * f; 
    return f;
  }
  
  public void completeServiceTargetLoading() {
    this.mServiceTargets.removeIf((Predicate<? super ChooserTargetInfo>)_$$Lambda$ChooserListAdapter$wCx26bRTLs1mYJZo1f9xyVwjoCY.INSTANCE);
    if (this.mAppendDirectShareEnabled)
      fillAllServiceTargets(); 
    if (this.mServiceTargets.isEmpty())
      this.mServiceTargets.add(new ChooserActivity.EmptyTargetInfo()); 
    notifyDataSetChanged();
  }
  
  private boolean insertServiceTarget(ChooserTargetInfo paramChooserTargetInfo) {
    if (this.mServiceTargets.size() == 1) {
      List<ChooserTargetInfo> list = this.mServiceTargets;
      if (list.get(0) instanceof ChooserActivity.EmptyTargetInfo)
        return false; 
    } 
    for (ChooserTargetInfo chooserTargetInfo : this.mServiceTargets) {
      if (paramChooserTargetInfo.isSimilar(chooserTargetInfo))
        return false; 
    } 
    int i = this.mServiceTargets.size();
    float f = paramChooserTargetInfo.getModifiedScore();
    for (byte b = 0; b < Math.min(i, 8); b++) {
      ChooserTargetInfo chooserTargetInfo = this.mServiceTargets.get(b);
      if (chooserTargetInfo == null) {
        this.mServiceTargets.set(b, paramChooserTargetInfo);
        return true;
      } 
      if (f > chooserTargetInfo.getModifiedScore()) {
        this.mServiceTargets.add(b, paramChooserTargetInfo);
        return true;
      } 
    } 
    if (i < 8) {
      this.mServiceTargets.add(paramChooserTargetInfo);
      return true;
    } 
    return false;
  }
  
  public ChooserTarget getChooserTargetForValue(int paramInt) {
    return ((ChooserTargetInfo)this.mServiceTargets.get(paramInt)).getChooserTarget();
  }
  
  protected boolean alwaysShowSubLabel() {
    return true;
  }
  
  AsyncTask<List<ResolverActivity.ResolvedComponentInfo>, Void, List<ResolverActivity.ResolvedComponentInfo>> createSortingTask(boolean paramBoolean) {
    return (AsyncTask<List<ResolverActivity.ResolvedComponentInfo>, Void, List<ResolverActivity.ResolvedComponentInfo>>)new Object(this, paramBoolean);
  }
  
  public void setAppPredictor(AppPredictor paramAppPredictor) {
    this.mAppPredictor = paramAppPredictor;
  }
  
  public void setAppPredictorCallback(AppPredictor.Callback paramCallback) {
    this.mAppPredictorCallback = paramCallback;
  }
  
  public void destroyAppPredictor() {
    if (getAppPredictor() != null) {
      getAppPredictor().unregisterPredictionUpdates(this.mAppPredictorCallback);
      getAppPredictor().destroy();
      setAppPredictor((AppPredictor)null);
    } 
  }
  
  class ChooserListCommunicator implements ResolverListAdapter.ResolverListCommunicator {
    public abstract int getMaxRankedTargets();
    
    public abstract boolean isSendAction(Intent param1Intent);
    
    public abstract void sendListViewUpdateMessage(UserHandle param1UserHandle);
  }
}
