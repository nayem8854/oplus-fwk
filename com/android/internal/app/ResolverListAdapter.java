package com.android.internal.app;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.PermissionChecker;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.RemoteException;
import android.os.UserHandle;
import android.os.UserManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.internal.app.chooser.DisplayResolveInfo;
import com.android.internal.app.chooser.TargetInfo;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ResolverListAdapter extends OplusBaseResolverListAdapter {
  private static final String TAG = "ResolverListAdapter";
  
  private static ColorMatrixColorFilter sSuspendedMatrixColorFilter;
  
  private final List<ResolveInfo> mBaseResolveList;
  
  protected final Context mContext;
  
  List<DisplayResolveInfo> mDisplayList;
  
  private boolean mFilterLastUsed;
  
  private final int mIconDpi;
  
  protected final LayoutInflater mInflater;
  
  private final Intent[] mInitialIntents;
  
  private final List<Intent> mIntents;
  
  private final boolean mIsAudioCaptureDevice;
  
  private boolean mIsTabLoaded;
  
  protected ResolveInfo mLastChosen;
  
  private int mLastChosenPosition = -1;
  
  private DisplayResolveInfo mOtherProfile;
  
  private int mPlaceholderCount;
  
  private final PackageManager mPm;
  
  private Runnable mPostListReadyRunnable;
  
  final ResolverListCommunicator mResolverListCommunicator;
  
  ResolverListController mResolverListController;
  
  private List<ResolverActivity.ResolvedComponentInfo> mUnfilteredResolveList;
  
  public ResolverListAdapter(Context paramContext, List<Intent> paramList, Intent[] paramArrayOfIntent, List<ResolveInfo> paramList1, boolean paramBoolean1, ResolverListController paramResolverListController, ResolverListCommunicator paramResolverListCommunicator, boolean paramBoolean2) {
    this.mContext = paramContext;
    this.mIntents = paramList;
    this.mInitialIntents = paramArrayOfIntent;
    this.mBaseResolveList = paramList1;
    this.mInflater = LayoutInflater.from(paramContext);
    this.mPm = paramContext.getPackageManager();
    this.mDisplayList = new ArrayList<>();
    this.mFilterLastUsed = paramBoolean1;
    this.mResolverListController = paramResolverListController;
    this.mResolverListCommunicator = paramResolverListCommunicator;
    this.mIsAudioCaptureDevice = paramBoolean2;
    ActivityManager activityManager = (ActivityManager)this.mContext.getSystemService("activity");
    this.mIconDpi = activityManager.getLauncherLargeIconDensity();
  }
  
  public void handlePackagesChanged() {
    this.mResolverListCommunicator.onHandlePackagesChanged(this);
  }
  
  public void setPlaceholderCount(int paramInt) {
    this.mPlaceholderCount = paramInt;
  }
  
  public int getPlaceholderCount() {
    return this.mPlaceholderCount;
  }
  
  public DisplayResolveInfo getFilteredItem() {
    if (this.mFilterLastUsed) {
      int i = this.mLastChosenPosition;
      if (i >= 0)
        return this.mDisplayList.get(i); 
    } 
    return null;
  }
  
  public DisplayResolveInfo getOtherProfile() {
    return this.mOtherProfile;
  }
  
  public int getFilteredPosition() {
    if (this.mFilterLastUsed) {
      int i = this.mLastChosenPosition;
      if (i >= 0)
        return i; 
    } 
    return -1;
  }
  
  public boolean hasFilteredItem() {
    boolean bool;
    if (this.mFilterLastUsed && this.mLastChosen != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public float getScore(DisplayResolveInfo paramDisplayResolveInfo) {
    return this.mResolverListController.getScore(paramDisplayResolveInfo);
  }
  
  public float getScore(ComponentName paramComponentName) {
    return this.mResolverListController.getScore(paramComponentName);
  }
  
  public List<ComponentName> getTopComponentNames(int paramInt) {
    return this.mResolverListController.getTopComponentNames(paramInt);
  }
  
  public void updateModel(ComponentName paramComponentName) {
    this.mResolverListController.updateModel(paramComponentName);
  }
  
  public void updateChooserCounts(String paramString1, String paramString2) {
    ResolverListController resolverListController = this.mResolverListController;
    int i = getUserHandle().getIdentifier();
    resolverListController.updateChooserCounts(paramString1, i, paramString2);
  }
  
  List<ResolverActivity.ResolvedComponentInfo> getUnfilteredResolveList() {
    return this.mUnfilteredResolveList;
  }
  
  protected boolean rebuildList(boolean paramBoolean) {
    List<ResolverActivity.ResolvedComponentInfo> list;
    this.mOtherProfile = null;
    this.mLastChosen = null;
    this.mLastChosenPosition = -1;
    this.mDisplayList.clear();
    this.mIsTabLoaded = false;
    if (this.mBaseResolveList != null) {
      this.mUnfilteredResolveList = list = new ArrayList();
      ResolverListController resolverListController = this.mResolverListController;
      ResolverListCommunicator resolverListCommunicator = this.mResolverListCommunicator;
      Intent intent = resolverListCommunicator.getTargetIntent();
      List<ResolveInfo> list1 = this.mBaseResolveList;
      resolverListController.addResolveListDedupe(list, intent, list1);
    } else {
      ResolverListController resolverListController1 = this.mResolverListController;
      ResolverListCommunicator resolverListCommunicator = this.mResolverListCommunicator;
      boolean bool = resolverListCommunicator.shouldGetActivityMetadata();
      List<Intent> list1 = this.mIntents;
      this.mUnfilteredResolveList = list = resolverListController1.getResolversForIntent(true, bool, list1);
      if (list == null) {
        processSortedList(list, paramBoolean);
        return true;
      } 
      ResolverListController resolverListController2 = this.mResolverListController;
      ArrayList<ResolverActivity.ResolvedComponentInfo> arrayList = resolverListController2.filterIneligibleActivities(list, true);
      if (arrayList != null)
        this.mUnfilteredResolveList = arrayList; 
    } 
    for (Iterator<ResolverActivity.ResolvedComponentInfo> iterator = list.iterator(); iterator.hasNext(); ) {
      ResolverActivity.ResolvedComponentInfo resolvedComponentInfo = iterator.next();
      ResolveInfo resolveInfo = resolvedComponentInfo.getResolveInfoAt(0);
      if (resolveInfo.targetUserId != -2) {
        ResolverListCommunicator resolverListCommunicator1 = this.mResolverListCommunicator;
        ActivityInfo activityInfo2 = resolveInfo.activityInfo;
        Intent intent3 = resolvedComponentInfo.getIntentAt(0);
        Intent intent1 = resolverListCommunicator1.getReplacementIntent(activityInfo2, intent3);
        ResolverListCommunicator resolverListCommunicator2 = this.mResolverListCommunicator;
        ActivityInfo activityInfo1 = resolveInfo.activityInfo;
        ResolverListCommunicator resolverListCommunicator3 = this.mResolverListCommunicator;
        Intent intent5 = resolverListCommunicator3.getTargetIntent();
        Intent intent2 = resolverListCommunicator2.getReplacementIntent(activityInfo1, intent5);
        Intent intent4 = resolvedComponentInfo.getIntentAt(0);
        PackageManager packageManager1 = this.mPm;
        CharSequence charSequence1 = resolveInfo.loadLabel(packageManager1);
        PackageManager packageManager2 = this.mPm;
        CharSequence charSequence2 = resolveInfo.loadLabel(packageManager2);
        if (intent1 == null)
          intent1 = intent2; 
        this.mOtherProfile = new DisplayResolveInfo(intent4, resolveInfo, charSequence1, charSequence2, intent1, makePresentationGetter(resolveInfo));
        list.remove(resolvedComponentInfo);
        break;
      } 
    } 
    if (this.mOtherProfile == null)
      try {
        this.mLastChosen = this.mResolverListController.getLastChosen();
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Error calling getLastChosenActivity\n");
        stringBuilder.append(remoteException);
        Log.d("ResolverListAdapter", stringBuilder.toString());
      }  
    setPlaceholderCount(0);
    if (list != null && list.size() > 0) {
      boolean bool;
      ResolverListController resolverListController = this.mResolverListController;
      if (this.mUnfilteredResolveList == list) {
        bool = true;
      } else {
        bool = false;
      } 
      ArrayList<ResolverActivity.ResolvedComponentInfo> arrayList = resolverListController.filterLowPriority(list, bool);
      if (arrayList != null)
        this.mUnfilteredResolveList = arrayList; 
      if (list.size() <= 1)
        if (list.size() < 1 || !this.mResolverListCommunicator.hasCustomFlag(512)) {
          processSortedList(list, paramBoolean);
          return true;
        }  
      int i = list.size();
      int j = i;
      if (this.mResolverListCommunicator.useLayoutWithDefault())
        j = i - 1; 
      setPlaceholderCount(j);
      setPlaceholderResolveList(list);
      createSortingTask(paramBoolean).execute((Object[])new List[] { list });
      postListReadyRunnable(paramBoolean, false);
      return false;
    } 
    processSortedList(list, paramBoolean);
    return true;
  }
  
  AsyncTask<List<ResolverActivity.ResolvedComponentInfo>, Void, List<ResolverActivity.ResolvedComponentInfo>> createSortingTask(boolean paramBoolean) {
    return (AsyncTask<List<ResolverActivity.ResolvedComponentInfo>, Void, List<ResolverActivity.ResolvedComponentInfo>>)new Object(this, paramBoolean);
  }
  
  protected void processSortedList(List<ResolverActivity.ResolvedComponentInfo> paramList, boolean paramBoolean) {
    if (sortComponentsNull(paramList, true)) {
      if (this.mInitialIntents != null) {
        byte b = 0;
        while (true) {
          Intent[] arrayOfIntent = this.mInitialIntents;
          if (b < arrayOfIntent.length) {
            Intent intent = arrayOfIntent[b];
            if (intent != null) {
              ResolveInfo resolveInfo = getResolveInfo(intent, this.mPm);
              if (resolveInfo != null) {
                DisplayResolveInfo displayResolveInfo;
                List<DisplayResolveInfo> list;
                Context context2 = this.mContext;
                UserManager userManager = (UserManager)context2.getSystemService("user");
                if (intent instanceof LabeledIntent) {
                  LabeledIntent labeledIntent = (LabeledIntent)intent;
                  resolveInfo.resolvePackageName = labeledIntent.getSourcePackage();
                  resolveInfo.labelRes = labeledIntent.getLabelResource();
                  resolveInfo.nonLocalizedLabel = labeledIntent.getNonLocalizedLabel();
                  resolveInfo.icon = labeledIntent.getIconResource();
                  resolveInfo.iconResourceId = resolveInfo.icon;
                } 
                if (userManager.isManagedProfile()) {
                  resolveInfo.noResourceId = true;
                  resolveInfo.icon = 0;
                } 
                Context context1 = this.mContext;
                if (context1 instanceof ChooserActivity) {
                  ResolveInfoPresentationGetter resolveInfoPresentationGetter = makePresentationGetter(resolveInfo);
                  list = this.mDisplayList;
                  displayResolveInfo = new DisplayResolveInfo(intent, resolveInfo, resolveInfoPresentationGetter.getLabel(), resolveInfoPresentationGetter.getSubLabel(), intent, makePresentationGetter(resolveInfo));
                  list.add(displayResolveInfo);
                } else {
                  displayResolveInfo = new DisplayResolveInfo(intent, (ResolveInfo)displayResolveInfo, displayResolveInfo.loadLabel(list.getPackageManager()), null, intent, makePresentationGetter((ResolveInfo)displayResolveInfo));
                  addResolveInfo(displayResolveInfo);
                } 
              } 
            } 
            b++;
            continue;
          } 
          break;
        } 
      } 
      if (sortComponentsNull(paramList, false))
        for (ResolverActivity.ResolvedComponentInfo resolvedComponentInfo : paramList) {
          ResolveInfo resolveInfo = resolvedComponentInfo.getResolveInfoAt(0);
          if (resolveInfo != null)
            addResolveInfoWithAlternates(resolvedComponentInfo); 
        }  
    } 
    addMultiAppResolveInfoIfNeed(paramList, this.mIntents, this.mResolverListController, this.mPm, this.mDisplayList);
    this.mResolverListCommunicator.sendVoiceChoicesIfNeeded();
    postListReadyRunnable(paramBoolean, true);
    this.mIsTabLoaded = true;
  }
  
  void postListReadyRunnable(boolean paramBoolean1, boolean paramBoolean2) {
    if (this.mPostListReadyRunnable == null) {
      this.mPostListReadyRunnable = (Runnable)new Object(this, paramBoolean1, paramBoolean2);
      this.mContext.getMainThreadHandler().post(this.mPostListReadyRunnable);
    } 
  }
  
  private void addResolveInfoWithAlternates(ResolverActivity.ResolvedComponentInfo paramResolvedComponentInfo) {
    int i = paramResolvedComponentInfo.getCount();
    Intent intent1 = paramResolvedComponentInfo.getIntentAt(0);
    ResolveInfo resolveInfo = paramResolvedComponentInfo.getResolveInfoAt(0);
    ResolverListCommunicator resolverListCommunicator1 = this.mResolverListCommunicator;
    ActivityInfo activityInfo = resolveInfo.activityInfo;
    Intent intent2 = resolverListCommunicator1.getReplacementIntent(activityInfo, intent1);
    ResolverListCommunicator resolverListCommunicator2 = this.mResolverListCommunicator;
    activityInfo = resolveInfo.activityInfo;
    ResolverListCommunicator resolverListCommunicator3 = this.mResolverListCommunicator;
    Intent intent4 = resolverListCommunicator3.getTargetIntent();
    Intent intent3 = resolverListCommunicator2.getReplacementIntent(activityInfo, intent4);
    if (intent2 != null)
      intent3 = intent2; 
    DisplayResolveInfo displayResolveInfo = new DisplayResolveInfo(intent1, resolveInfo, intent3, makePresentationGetter(resolveInfo));
    displayResolveInfo.setPinned(paramResolvedComponentInfo.isPinned());
    if (paramResolvedComponentInfo.isPinned()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Pinned item: ");
      stringBuilder.append(paramResolvedComponentInfo.name);
      Log.i("ResolverListAdapter", stringBuilder.toString());
    } 
    addResolveInfo(displayResolveInfo);
    if (intent2 == intent1)
      for (byte b = 1; b < i; b++) {
        intent2 = paramResolvedComponentInfo.getIntentAt(b);
        displayResolveInfo.addAlternateSourceIntent(intent2);
      }  
    updateLastChosenPosition(resolveInfo);
  }
  
  private void updateLastChosenPosition(ResolveInfo paramResolveInfo) {
    if (this.mOtherProfile != null) {
      this.mLastChosenPosition = -1;
      return;
    } 
    ResolveInfo resolveInfo = this.mLastChosen;
    if (resolveInfo != null) {
      String str1 = resolveInfo.activityInfo.packageName, str2 = paramResolveInfo.activityInfo.packageName;
      if (str1.equals(str2)) {
        str1 = this.mLastChosen.activityInfo.name;
        String str = paramResolveInfo.activityInfo.name;
        if (str1.equals(str))
          this.mLastChosenPosition = this.mDisplayList.size() - 1; 
      } 
    } 
  }
  
  private void addResolveInfo(DisplayResolveInfo paramDisplayResolveInfo) {
    if (paramDisplayResolveInfo != null && paramDisplayResolveInfo.getResolveInfo() != null && 
      (paramDisplayResolveInfo.getResolveInfo()).targetUserId == -2 && 
      shouldAddResolveInfo(paramDisplayResolveInfo)) {
      this.mDisplayList.add(paramDisplayResolveInfo);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Add DisplayResolveInfo component: ");
      stringBuilder.append(paramDisplayResolveInfo.getResolvedComponentName());
      stringBuilder.append(", intent component: ");
      stringBuilder.append(paramDisplayResolveInfo.getResolvedIntent().getComponent());
      String str = stringBuilder.toString();
      Log.i("ResolverListAdapter", str);
    } 
  }
  
  protected boolean shouldAddResolveInfo(DisplayResolveInfo paramDisplayResolveInfo) {
    for (DisplayResolveInfo displayResolveInfo : this.mDisplayList) {
      ResolverListCommunicator resolverListCommunicator = this.mResolverListCommunicator;
      if (resolverListCommunicator.resolveInfoMatch(paramDisplayResolveInfo.getResolveInfo(), displayResolveInfo.getResolveInfo()))
        return false; 
    } 
    return true;
  }
  
  public ResolveInfo resolveInfoForPosition(int paramInt, boolean paramBoolean) {
    TargetInfo targetInfo = targetInfoForPosition(paramInt, paramBoolean);
    if (targetInfo != null)
      return targetInfo.getResolveInfo(); 
    return null;
  }
  
  public TargetInfo targetInfoForPosition(int paramInt, boolean paramBoolean) {
    if (paramBoolean)
      return getItem(paramInt); 
    if (this.mDisplayList.size() > paramInt)
      return this.mDisplayList.get(paramInt); 
    return null;
  }
  
  public int getCount() {
    int i;
    List<DisplayResolveInfo> list = this.mDisplayList;
    if (list == null || list.isEmpty()) {
      i = this.mPlaceholderCount;
    } else {
      i = this.mDisplayList.size();
    } 
    int j = i;
    if (this.mFilterLastUsed) {
      j = i;
      if (this.mLastChosenPosition >= 0)
        j = i - 1; 
    } 
    return j;
  }
  
  public int getUnfilteredCount() {
    return this.mDisplayList.size();
  }
  
  public TargetInfo getItem(int paramInt) {
    int i = paramInt;
    if (this.mFilterLastUsed) {
      int j = this.mLastChosenPosition;
      i = paramInt;
      if (j >= 0) {
        i = paramInt;
        if (paramInt >= j)
          i = paramInt + 1; 
      } 
    } 
    if (this.mDisplayList.size() > i)
      return this.mDisplayList.get(i); 
    return null;
  }
  
  public long getItemId(int paramInt) {
    return paramInt;
  }
  
  public int getDisplayResolveInfoCount() {
    return this.mDisplayList.size();
  }
  
  public DisplayResolveInfo getDisplayResolveInfo(int paramInt) {
    return this.mDisplayList.get(paramInt);
  }
  
  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup) {
    View view = paramView;
    if (paramView == null)
      view = createView(paramViewGroup); 
    onBindView(view, getItem(paramInt), paramInt);
    return view;
  }
  
  public final View createView(ViewGroup paramViewGroup) {
    View view = onCreateView(paramViewGroup);
    ViewHolder viewHolder = new ViewHolder(view);
    view.setTag(viewHolder);
    return view;
  }
  
  View onCreateView(ViewGroup paramViewGroup) {
    return this.mInflater.inflate(17367271, paramViewGroup, false);
  }
  
  public final void bindView(int paramInt, View paramView) {
    onBindView(paramView, getItem(paramInt), paramInt);
  }
  
  protected void onBindView(View paramView, TargetInfo paramTargetInfo, int paramInt) {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual getTag : ()Ljava/lang/Object;
    //   4: checkcast com/android/internal/app/ResolverListAdapter$ViewHolder
    //   7: astore_1
    //   8: aload_2
    //   9: ifnonnull -> 36
    //   12: aload_1
    //   13: getfield icon : Landroid/widget/ImageView;
    //   16: astore_1
    //   17: aload_0
    //   18: getfield mContext : Landroid/content/Context;
    //   21: astore_2
    //   22: aload_2
    //   23: ldc_w 17303426
    //   26: invokevirtual getDrawable : (I)Landroid/graphics/drawable/Drawable;
    //   29: astore_2
    //   30: aload_1
    //   31: aload_2
    //   32: invokevirtual setImageDrawable : (Landroid/graphics/drawable/Drawable;)V
    //   35: return
    //   36: aload_2
    //   37: instanceof com/android/internal/app/chooser/DisplayResolveInfo
    //   40: ifeq -> 77
    //   43: aload_2
    //   44: checkcast com/android/internal/app/chooser/DisplayResolveInfo
    //   47: astore #4
    //   49: aload #4
    //   51: invokevirtual hasDisplayLabel : ()Z
    //   54: ifne -> 77
    //   57: aload_0
    //   58: aload_2
    //   59: checkcast com/android/internal/app/chooser/DisplayResolveInfo
    //   62: aload_1
    //   63: invokevirtual getLoadLabelTask : (Lcom/android/internal/app/chooser/DisplayResolveInfo;Lcom/android/internal/app/ResolverListAdapter$ViewHolder;)Lcom/android/internal/app/ResolverListAdapter$LoadLabelTask;
    //   66: iconst_0
    //   67: anewarray java/lang/Void
    //   70: invokevirtual execute : ([Ljava/lang/Object;)Landroid/os/AsyncTask;
    //   73: pop
    //   74: goto -> 97
    //   77: aload_1
    //   78: aload_2
    //   79: invokeinterface getDisplayLabel : ()Ljava/lang/CharSequence;
    //   84: aload_2
    //   85: invokeinterface getExtendedInfo : ()Ljava/lang/CharSequence;
    //   90: aload_0
    //   91: invokevirtual alwaysShowSubLabel : ()Z
    //   94: invokevirtual bindLabel : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V
    //   97: aload_2
    //   98: instanceof com/android/internal/app/chooser/DisplayResolveInfo
    //   101: ifeq -> 142
    //   104: aload_2
    //   105: checkcast com/android/internal/app/chooser/DisplayResolveInfo
    //   108: astore #4
    //   110: aload #4
    //   112: invokevirtual hasDisplayIcon : ()Z
    //   115: ifne -> 142
    //   118: new com/android/internal/app/ResolverListAdapter$LoadIconTask
    //   121: dup
    //   122: aload_0
    //   123: aload_2
    //   124: checkcast com/android/internal/app/chooser/DisplayResolveInfo
    //   127: aload_1
    //   128: invokespecial <init> : (Lcom/android/internal/app/ResolverListAdapter;Lcom/android/internal/app/chooser/DisplayResolveInfo;Lcom/android/internal/app/ResolverListAdapter$ViewHolder;)V
    //   131: iconst_0
    //   132: anewarray java/lang/Void
    //   135: invokevirtual execute : ([Ljava/lang/Object;)Landroid/os/AsyncTask;
    //   138: pop
    //   139: goto -> 147
    //   142: aload_1
    //   143: aload_2
    //   144: invokevirtual bindIcon : (Lcom/android/internal/app/chooser/TargetInfo;)V
    //   147: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #594	-> 0
    //   #595	-> 8
    //   #596	-> 12
    //   #597	-> 22
    //   #596	-> 30
    //   #598	-> 35
    //   #601	-> 36
    //   #602	-> 49
    //   #603	-> 57
    //   #605	-> 77
    //   #608	-> 97
    //   #609	-> 110
    //   #610	-> 118
    //   #612	-> 142
    //   #614	-> 147
  }
  
  protected LoadLabelTask getLoadLabelTask(DisplayResolveInfo paramDisplayResolveInfo, ViewHolder paramViewHolder) {
    return new LoadLabelTask(paramDisplayResolveInfo, paramViewHolder);
  }
  
  public void onDestroy() {
    if (this.mPostListReadyRunnable != null) {
      this.mContext.getMainThreadHandler().removeCallbacks(this.mPostListReadyRunnable);
      this.mPostListReadyRunnable = null;
    } 
    ResolverListController resolverListController = this.mResolverListController;
    if (resolverListController != null)
      resolverListController.destroy(); 
  }
  
  private static ColorMatrixColorFilter getSuspendedColorMatrix() {
    if (sSuspendedMatrixColorFilter == null) {
      ColorMatrix colorMatrix1 = new ColorMatrix();
      float[] arrayOfFloat = colorMatrix1.getArray();
      arrayOfFloat[0] = 0.5F;
      arrayOfFloat[6] = 0.5F;
      arrayOfFloat[12] = 0.5F;
      arrayOfFloat[4] = 127;
      arrayOfFloat[9] = 127;
      arrayOfFloat[14] = 127;
      ColorMatrix colorMatrix2 = new ColorMatrix();
      colorMatrix2.setSaturation(0.0F);
      colorMatrix2.preConcat(colorMatrix1);
      sSuspendedMatrixColorFilter = new ColorMatrixColorFilter(colorMatrix2);
    } 
    return sSuspendedMatrixColorFilter;
  }
  
  ActivityInfoPresentationGetter makePresentationGetter(ActivityInfo paramActivityInfo) {
    return new ActivityInfoPresentationGetter(this.mContext, this.mIconDpi, paramActivityInfo);
  }
  
  ResolveInfoPresentationGetter makePresentationGetter(ResolveInfo paramResolveInfo) {
    return new ResolveInfoPresentationGetter(this.mContext, this.mIconDpi, paramResolveInfo);
  }
  
  ResolverListCommunicator getResolverListCommunicator() {
    return this.mResolverListCommunicator;
  }
  
  Drawable loadIconForResolveInfo(ResolveInfo paramResolveInfo) {
    return makePresentationGetter(paramResolveInfo).getIcon(getUserHandle());
  }
  
  void loadFilteredItemIconTaskAsync(ImageView paramImageView) {
    DisplayResolveInfo displayResolveInfo = getFilteredItem();
    if (paramImageView != null && displayResolveInfo != null) {
      Object object = new Object(this, displayResolveInfo, paramImageView);
      object.execute((Object[])new Void[0]);
    } 
  }
  
  public UserHandle getUserHandle() {
    return this.mResolverListController.getUserHandle();
  }
  
  protected List<ResolverActivity.ResolvedComponentInfo> getResolversForUser(UserHandle paramUserHandle) {
    ResolverListController resolverListController = this.mResolverListController;
    ResolverListCommunicator resolverListCommunicator = this.mResolverListCommunicator;
    boolean bool = resolverListCommunicator.shouldGetActivityMetadata();
    List<Intent> list = this.mIntents;
    return resolverListController.getResolversForIntentAsUser(true, bool, list, paramUserHandle);
  }
  
  protected List<Intent> getIntents() {
    return this.mIntents;
  }
  
  protected boolean isTabLoaded() {
    return this.mIsTabLoaded;
  }
  
  protected void markTabLoaded() {
    this.mIsTabLoaded = true;
  }
  
  protected boolean alwaysShowSubLabel() {
    return false;
  }
  
  class ViewHolder {
    public Drawable defaultItemViewBackground;
    
    public ImageView icon;
    
    public View itemView;
    
    public TextView text;
    
    public TextView text2;
    
    ViewHolder(ResolverListAdapter this$0) {
      this.itemView = (View)this$0;
      this.defaultItemViewBackground = this$0.getBackground();
      this.text = this$0.<TextView>findViewById(16908308);
      this.text2 = this$0.<TextView>findViewById(16908309);
      this.icon = this$0.<ImageView>findViewById(16908294);
    }
    
    public void bindLabel(CharSequence param1CharSequence1, CharSequence param1CharSequence2, boolean param1Boolean) {
      this.text.setText(param1CharSequence1);
      CharSequence charSequence = param1CharSequence2;
      if (TextUtils.equals(param1CharSequence1, param1CharSequence2))
        charSequence = null; 
      this.text2.setText(charSequence);
      if (param1Boolean || charSequence != null) {
        this.text2.setVisibility(0);
      } else {
        this.text2.setVisibility(8);
      } 
      this.itemView.setContentDescription(null);
    }
    
    public void updateContentDescription(String param1String) {
      this.itemView.setContentDescription(param1String);
    }
    
    public void bindIcon(TargetInfo param1TargetInfo) {
      this.icon.setImageDrawable(param1TargetInfo.getDisplayIcon(this.itemView.getContext()));
      if (param1TargetInfo.isSuspended()) {
        this.icon.setColorFilter((ColorFilter)ResolverListAdapter.getSuspendedColorMatrix());
      } else {
        this.icon.setColorFilter((ColorFilter)null);
      } 
    }
  }
  
  class LoadLabelTask extends AsyncTask<Void, Void, CharSequence[]> {
    private final DisplayResolveInfo mDisplayResolveInfo;
    
    private final ResolverListAdapter.ViewHolder mHolder;
    
    final ResolverListAdapter this$0;
    
    protected LoadLabelTask(DisplayResolveInfo param1DisplayResolveInfo, ResolverListAdapter.ViewHolder param1ViewHolder) {
      this.mDisplayResolveInfo = param1DisplayResolveInfo;
      this.mHolder = param1ViewHolder;
    }
    
    protected CharSequence[] doInBackground(Void... param1VarArgs) {
      ResolverListAdapter resolverListAdapter = ResolverListAdapter.this;
      DisplayResolveInfo displayResolveInfo = this.mDisplayResolveInfo;
      ResolverListAdapter.ResolveInfoPresentationGetter resolveInfoPresentationGetter = resolverListAdapter.makePresentationGetter(displayResolveInfo.getResolveInfo());
      if (ResolverListAdapter.this.mIsAudioCaptureDevice) {
        ActivityInfo activityInfo = (this.mDisplayResolveInfo.getResolveInfo()).activityInfo;
        String str = activityInfo.packageName;
        int i = activityInfo.applicationInfo.uid;
        Context context = ResolverListAdapter.this.mContext;
        if (PermissionChecker.checkPermissionForPreflight(context, "android.permission.RECORD_AUDIO", -1, i, str) == 0) {
          i = 1;
        } else {
          i = 0;
        } 
        if (i == 0) {
          str1 = resolveInfoPresentationGetter.getLabel();
          Context context1 = ResolverListAdapter.this.mContext;
          String str3 = context1.getString(17041437);
          return new CharSequence[] { str1, str3 };
        } 
      } 
      String str2 = str1.getLabel();
      String str1 = str1.getSubLabel();
      return new CharSequence[] { str2, str1 };
    }
    
    protected void onPostExecute(CharSequence[] param1ArrayOfCharSequence) {
      this.mDisplayResolveInfo.setDisplayLabel(param1ArrayOfCharSequence[0]);
      this.mDisplayResolveInfo.setExtendedInfo(param1ArrayOfCharSequence[1]);
      this.mHolder.bindLabel(param1ArrayOfCharSequence[0], param1ArrayOfCharSequence[1], ResolverListAdapter.this.alwaysShowSubLabel());
    }
  }
  
  class LoadIconTask extends AsyncTask<Void, Void, Drawable> {
    protected final DisplayResolveInfo mDisplayResolveInfo;
    
    private ResolverListAdapter.ViewHolder mHolder;
    
    private final ResolveInfo mResolveInfo;
    
    final ResolverListAdapter this$0;
    
    LoadIconTask(DisplayResolveInfo param1DisplayResolveInfo, ResolverListAdapter.ViewHolder param1ViewHolder) {
      this.mDisplayResolveInfo = param1DisplayResolveInfo;
      this.mResolveInfo = param1DisplayResolveInfo.getResolveInfo();
      this.mHolder = param1ViewHolder;
    }
    
    protected Drawable doInBackground(Void... param1VarArgs) {
      return ResolverListAdapter.this.loadIconForResolveInfo(this.mResolveInfo);
    }
    
    protected void onPostExecute(Drawable param1Drawable) {
      DisplayResolveInfo displayResolveInfo1 = ResolverListAdapter.this.getOtherProfile(), displayResolveInfo2 = this.mDisplayResolveInfo;
      if (displayResolveInfo1 == displayResolveInfo2) {
        ResolverListAdapter.this.mResolverListCommunicator.updateProfileViewButton();
      } else {
        displayResolveInfo2.setDisplayIcon(param1Drawable);
        this.mHolder.bindIcon(this.mDisplayResolveInfo);
      } 
    }
    
    public void setViewHolder(ResolverListAdapter.ViewHolder param1ViewHolder) {
      this.mHolder = param1ViewHolder;
      param1ViewHolder.bindIcon(this.mDisplayResolveInfo);
    }
  }
  
  class ResolveInfoPresentationGetter extends ActivityInfoPresentationGetter {
    private final ResolveInfo mRi;
    
    public ResolveInfoPresentationGetter(ResolverListAdapter this$0, int param1Int, ResolveInfo param1ResolveInfo) {
      super((Context)this$0, param1Int, param1ResolveInfo.activityInfo);
      this.mRi = param1ResolveInfo;
    }
    
    Drawable getIconSubstituteInternal() {
      Drawable drawable;
      PackageManager.NameNotFoundException nameNotFoundException1 = null;
      PackageManager packageManager1 = null;
      PackageManager packageManager2 = packageManager1;
      try {
        if (this.mRi.resolvePackageName != null) {
          packageManager2 = packageManager1;
          if (this.mRi.icon != 0) {
            packageManager1 = this.mPm;
            String str = this.mRi.resolvePackageName;
            Resources resources = packageManager1.getResourcesForApplication(str);
            int i = this.mRi.icon;
            Drawable drawable1 = loadIconFromResource(resources, i);
          } 
        } 
      } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException2) {
        Log.e("ResolverListAdapter", "SUBSTITUTE_SHARE_TARGET_APP_NAME_AND_ICON permission granted but couldn't find resources for package", (Throwable)nameNotFoundException2);
        nameNotFoundException2 = nameNotFoundException1;
      } 
      nameNotFoundException1 = nameNotFoundException2;
      if (nameNotFoundException2 == null)
        drawable = super.getIconSubstituteInternal(); 
      return drawable;
    }
    
    String getAppSubLabelInternal() {
      return (String)this.mRi.loadLabel(this.mPm);
    }
  }
  
  class ActivityInfoPresentationGetter extends TargetPresentationGetter {
    private final ActivityInfo mActivityInfo;
    
    public ActivityInfoPresentationGetter(ResolverListAdapter this$0, int param1Int, ActivityInfo param1ActivityInfo) {
      super((Context)this$0, param1Int, param1ActivityInfo.applicationInfo);
      this.mActivityInfo = param1ActivityInfo;
    }
    
    Drawable getIconSubstituteInternal() {
      PackageManager.NameNotFoundException nameNotFoundException1 = null;
      PackageManager packageManager = null;
      try {
        if (this.mActivityInfo.icon != 0) {
          packageManager = this.mPm;
          ApplicationInfo applicationInfo = this.mActivityInfo.applicationInfo;
          Resources resources = packageManager.getResourcesForApplication(applicationInfo);
          int i = this.mActivityInfo.icon;
          Drawable drawable = loadIconFromResource(resources, i);
        } 
      } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException2) {
        Log.e("ResolverListAdapter", "SUBSTITUTE_SHARE_TARGET_APP_NAME_AND_ICON permission granted but couldn't find resources for package", (Throwable)nameNotFoundException2);
        nameNotFoundException2 = nameNotFoundException1;
      } 
      return (Drawable)nameNotFoundException2;
    }
    
    String getAppSubLabelInternal() {
      return (String)this.mActivityInfo.loadLabel(this.mPm);
    }
  }
  
  class TargetPresentationGetter {
    private final ApplicationInfo mAi;
    
    private Context mCtx;
    
    private final boolean mHasSubstitutePermission;
    
    private final int mIconDpi;
    
    protected PackageManager mPm;
    
    TargetPresentationGetter(ResolverListAdapter this$0, int param1Int, ApplicationInfo param1ApplicationInfo) {
      boolean bool;
      this.mCtx = (Context)this$0;
      PackageManager packageManager = this$0.getPackageManager();
      this.mAi = param1ApplicationInfo;
      this.mIconDpi = param1Int;
      if (packageManager.checkPermission("android.permission.SUBSTITUTE_SHARE_TARGET_APP_NAME_AND_ICON", param1ApplicationInfo.packageName) == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mHasSubstitutePermission = bool;
    }
    
    public Drawable getIcon(UserHandle param1UserHandle) {
      return (Drawable)new BitmapDrawable(this.mCtx.getResources(), getIconBitmap(param1UserHandle));
    }
    
    public Bitmap getIconBitmap(UserHandle param1UserHandle) {
      Drawable drawable1 = null;
      if (this.mHasSubstitutePermission)
        drawable1 = getIconSubstituteInternal(); 
      Drawable drawable2 = drawable1;
      if (drawable1 == null) {
        drawable2 = drawable1;
        try {
          if (this.mAi.icon != 0)
            drawable2 = loadIconFromResource(this.mPm.getResourcesForApplication(this.mAi), this.mAi.icon); 
        } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
          drawable2 = drawable1;
        } 
      } 
      drawable1 = drawable2;
      if (drawable2 == null)
        drawable1 = this.mAi.loadIcon(this.mPm); 
      SimpleIconFactory simpleIconFactory = SimpleIconFactory.obtain(this.mCtx);
      Bitmap bitmap = simpleIconFactory.createUserBadgedIconBitmap(drawable1, param1UserHandle);
      simpleIconFactory.recycle();
      return bitmap;
    }
    
    public String getLabel() {
      String str1 = null;
      if (this.mHasSubstitutePermission)
        str1 = getAppSubLabelInternal(); 
      String str2 = str1;
      if (str1 == null)
        str2 = (String)this.mAi.loadLabel(this.mPm); 
      return str2;
    }
    
    public String getSubLabel() {
      if (this.mHasSubstitutePermission)
        return null; 
      return getAppSubLabelInternal();
    }
    
    protected String loadLabelFromResource(Resources param1Resources, int param1Int) {
      return param1Resources.getString(param1Int);
    }
    
    protected Drawable loadIconFromResource(Resources param1Resources, int param1Int) {
      return param1Resources.getDrawableForDensity(param1Int, this.mIconDpi);
    }
    
    abstract String getAppSubLabelInternal();
    
    abstract Drawable getIconSubstituteInternal();
  }
  
  class ResolverListCommunicator implements IOplusResolverListCommunicatorEx {
    public abstract Intent getReplacementIntent(ActivityInfo param1ActivityInfo, Intent param1Intent);
    
    public abstract Intent getTargetIntent();
    
    public abstract void onHandlePackagesChanged(ResolverListAdapter param1ResolverListAdapter);
    
    public abstract void onPostListReady(ResolverListAdapter param1ResolverListAdapter, boolean param1Boolean1, boolean param1Boolean2);
    
    public abstract boolean resolveInfoMatch(ResolveInfo param1ResolveInfo1, ResolveInfo param1ResolveInfo2);
    
    public abstract void sendVoiceChoicesIfNeeded();
    
    public abstract boolean shouldGetActivityMetadata();
    
    public abstract void updateProfileViewButton();
    
    public abstract boolean useLayoutWithDefault();
  }
}
