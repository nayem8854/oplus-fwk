package com.android.internal.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.common.IOplusCommonFeature;
import android.common.OplusFeatureCache;
import android.content.ComponentCallbacks;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.os.RemoteException;
import android.os.storage.StorageManager;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.internal.app.chooser.DisplayResolveInfo;
import com.android.internal.app.chooser.TargetInfo;
import com.oplus.resolver.OplusResolverApkPreView;
import com.oplus.resolver.OplusResolverDialogHelper;
import com.oplus.resolver.OplusResolverInfoHelper;
import com.oplus.resolver.widget.OplusResolverDrawerLayout;
import com.oplus.theme.IOplusThemeStyle;
import com.oplus.util.OplusResolverIntentUtil;
import com.oplus.util.OplusResolverUtil;
import com.oplus.util.OplusTypeCastingHelper;
import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class OplusResolverManager implements IOplusResolverManager {
  protected boolean mIsActionSend = false;
  
  private Set<Integer> mLoadedPages = new HashSet<>();
  
  private int mLastSelected = -1;
  
  public boolean isLoadTheme() {
    return false;
  }
  
  public boolean isOriginUi() {
    return false;
  }
  
  public boolean addExtraOneAppFinish() {
    return this.mIsActionSend ^ true;
  }
  
  public void onCreate(OplusBaseResolverActivity paramOplusBaseResolverActivity) {
    this.mBaseResolverActivity = paramOplusBaseResolverActivity;
    paramOplusBaseResolverActivity.setTheme(((IOplusThemeStyle)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusThemeStyle.DEFAULT, new Object[0])).getDialogAlertShareThemeStyle(201523271));
    paramOplusBaseResolverActivity.getWindow().addFlags(16777216);
    this.mBaseResolverActivity.getWindow().addFlags(-2147483648);
    this.mBaseResolverActivity.overridePendingTransition(201916421, 201916422);
    this.mBaseResolverActivity.registerComponentCallbacks(this.mComponentCallbacks);
  }
  
  public void onResume() {
    OplusResolverDialogHelper oplusResolverDialogHelper = this.mResolverDialogHelper;
    if (oplusResolverDialogHelper != null)
      oplusResolverDialogHelper.oShareResume(); 
  }
  
  public void onPause() {
    OplusResolverDialogHelper oplusResolverDialogHelper = this.mResolverDialogHelper;
    if (oplusResolverDialogHelper != null)
      oplusResolverDialogHelper.oSharePause(); 
  }
  
  public void onDestroy() {
    OplusResolverDialogHelper oplusResolverDialogHelper = this.mResolverDialogHelper;
    if (oplusResolverDialogHelper != null) {
      oplusResolverDialogHelper.dialogHelperDestroy();
      this.mResolverDialogHelper.removeProfileSelectedListener(this.mContentPanel);
      this.mResolverDialogHelper.removeProfileSelectedListener(this.mTabListener);
    } 
    OplusResolverApkPreView oplusResolverApkPreView = this.mApkPreView;
    if (oplusResolverApkPreView != null)
      oplusResolverApkPreView.onDestroy(); 
    this.mBaseResolverActivity.unregisterComponentCallbacks(this.mComponentCallbacks);
  }
  
  public void initActionSend() {
    String str = this.mBaseResolverActivity.getTargetIntent().getAction();
    if (str != null && (
      str.equalsIgnoreCase("android.intent.action.SEND") || 
      str.equalsIgnoreCase("android.intent.action.SEND_MULTIPLE")) && 
      getTargetIntent().getPackage() == null) {
      this.mIsActionSend = true;
    } else {
      this.mIsActionSend = false;
    } 
  }
  
  public void setLastChosen(ResolverListController paramResolverListController, Intent paramIntent, IntentFilter paramIntentFilter, int paramInt) throws RemoteException {
    if (!this.mOpenFlag)
      paramResolverListController.setLastChosen(paramIntent, paramIntentFilter, paramInt); 
  }
  
  public void initView(boolean paramBoolean1, boolean paramBoolean2) {
    initDialogHelper(paramBoolean2);
    if (getTargetIntent().getBooleanExtra("oplus_drag2Sharing_flag", false) && ((
      getActiveResolverAdapter().isTabLoaded() && (getActiveResolverAdapter()).mDisplayList.isEmpty()) || 
      shouldShowEmptyState())) {
      Toast.makeText((Context)this.mBaseResolverActivity, 201588860, 0).show();
      this.mBaseResolverActivity.finish();
      return;
    } 
    updateView(paramBoolean1, paramBoolean2);
    this.mLoadedPages.add(Integer.valueOf(getMultiProfilePagerAdapter().getCurrentPage()));
    this.mResolverDialogHelper.initChooserTopBroadcast();
  }
  
  public void updateView(boolean paramBoolean1, boolean paramBoolean2) {
    if (this.mResolveInfoHelper != null) {
      OplusResolverDialogHelper oplusResolverDialogHelper = this.mResolverDialogHelper;
      if (oplusResolverDialogHelper != null && this.mContentPanel == null) {
        boolean bool = getActiveResolverAdapter().getUserHandle().equals(this.mBaseResolverActivity.getWorkProfileUserHandle());
        int i = getPlaceholderCount();
        View view = oplusResolverDialogHelper.createView(bool, paramBoolean1, i);
        OplusBaseResolverActivity oplusBaseResolverActivity1 = this.mBaseResolverActivity;
        ChooserActivity chooserActivity = (ChooserActivity)OplusTypeCastingHelper.typeCasting(ChooserActivity.class, oplusBaseResolverActivity1);
        if (chooserActivity != null) {
          OplusBaseResolverActivity oplusBaseResolverActivity = this.mBaseResolverActivity;
          ViewGroup viewGroup = (ViewGroup)view.findViewById(16908865);
          this.mPreView = (View)oplusBaseResolverActivity.createContentPreviewViewWrapper(viewGroup);
        } 
        setContentView(view);
        OplusResolverDialogHelper oplusResolverDialogHelper1 = this.mResolverDialogHelper;
        OplusBaseResolverActivity oplusBaseResolverActivity2 = this.mBaseResolverActivity;
        this.mOpenFlag = oplusResolverDialogHelper1.initCheckBox(oplusBaseResolverActivity2.getIntent(), view, paramBoolean2);
        this.mResolverDialogHelper.showRecommend((Activity)this.mBaseResolverActivity);
        ItemClickListener itemClickListener = new ItemClickListener();
        this.mResolverDialogHelper.setOnItemClickListener(itemClickListener);
        if (this.mIsActionSend)
          this.mResolverDialogHelper.setOnItemLongClickListener(itemClickListener); 
        changeOShareAndNfc(view, getMultiProfilePagerAdapter().getCurrentPage());
        if (this.mBaseResolverActivity.getWorkProfileUserHandle() != null && ResolverActivity.ENABLE_TABBED_VIEW) {
          OplusResolverDrawerLayout oplusResolverDrawerLayout = this.mContentPanel;
          if (chooserActivity != null) {
            paramBoolean1 = true;
          } else {
            paramBoolean1 = false;
          } 
          oplusResolverDrawerLayout.setIsUserMaxHeight(paramBoolean1, view.findViewById(201457768));
          this.mResolverDialogHelper.addProfileSelectedListener(this.mContentPanel);
          OplusResolverDialogHelper oplusResolverDialogHelper2 = this.mResolverDialogHelper;
          TabChangeListener tabChangeListener = this.mTabListener;
          if (tabChangeListener == null)
            this.mTabListener = tabChangeListener = new TabChangeListener(getMultiProfilePagerAdapter()); 
          i = getMultiProfilePagerAdapter().getCurrentPage();
          oplusResolverDialogHelper2.initTabView(view, tabChangeListener, i);
        } 
        return;
      } 
    } 
  }
  
  public void onMultiWindowModeChanged() {
    OplusBaseResolverActivity oplusBaseResolverActivity = this.mBaseResolverActivity;
    if (oplusBaseResolverActivity != null)
      oplusBaseResolverActivity.getWindow().getDecorView().post(new _$$Lambda$OplusResolverManager$VlSjSG4NZzdY9dw_2qESRskjPK0(this)); 
  }
  
  public void setResolverContent() {
    if (this.mResolverDialogHelper != null && !this.mBaseResolverActivity.isFinishing()) {
      OplusBaseResolverActivity oplusBaseResolverActivity = this.mBaseResolverActivity;
      if (oplusBaseResolverActivity.getWindow().getDecorView() != null) {
        if (OplusResolverUtil.isChooserCtsTest((Context)this.mBaseResolverActivity, getTargetIntent()))
          (new OplusChooserCtsConnection()).testCts((Context)this.mBaseResolverActivity, this, getActiveResolverAdapter(), getActiveResolverAdapter().getUserHandle()); 
        List<ResolveInfo> list = getResolveInfoList((getActiveResolverAdapter()).mDisplayList);
        OplusResolverUtil.sortCtsTest((Context)this.mBaseResolverActivity, getTargetIntent(), list);
        resortListAndNotifyChange(list);
        return;
      } 
    } 
  }
  
  public void initPreferenceAndPinList() {
    if (this.mResolveInfoHelper == null)
      return; 
    this.mPinnedSharedPrefs = getPinnedSharedPrefs((Context)this.mBaseResolverActivity);
    boolean bool = OplusResolverIntentUtil.isChooserAction(getTargetIntent());
    String str = OplusResolverIntentUtil.getIntentType(getTargetIntent());
    if (bool && "gallery".equals(str))
      this.mGalleryPinList = Settings.Secure.getString(this.mBaseResolverActivity.getContentResolver(), "gallery_pin_list"); 
  }
  
  public void statisticsData(ResolveInfo paramResolveInfo, int paramInt) {
    String str = this.mBaseResolverActivity.getReferrerPackageName();
    OplusResolverInfoHelper oplusResolverInfoHelper = this.mResolveInfoHelper;
    if (oplusResolverInfoHelper != null)
      oplusResolverInfoHelper.statisticsData(paramResolveInfo, getTargetIntent(), paramInt, str); 
  }
  
  public ResolveInfo getResolveInfo(Intent paramIntent, PackageManager paramPackageManager) {
    PackageManager packageManager;
    ResolveInfo resolveInfo1 = null, resolveInfo2 = null;
    ActivityInfo activityInfo2 = null, activityInfo3 = null;
    ComponentName componentName = paramIntent.getComponent();
    if (componentName != null) {
      resolveInfo1 = resolveInfo2;
      activityInfo2 = activityInfo3;
      try {
        activityInfo3 = paramPackageManager.getActivityInfo(paramIntent.getComponent(), 0);
        resolveInfo1 = resolveInfo2;
        activityInfo2 = activityInfo3;
        ResolveInfo resolveInfo = new ResolveInfo();
        resolveInfo1 = resolveInfo2;
        activityInfo2 = activityInfo3;
        this();
        resolveInfo2 = resolveInfo;
        resolveInfo1 = resolveInfo2;
        activityInfo2 = activityInfo3;
        resolveInfo2.activityInfo = activityInfo3;
        resolveInfo1 = resolveInfo2;
        activityInfo2 = activityInfo3;
      } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {}
    } 
    resolveInfo2 = resolveInfo1;
    ActivityInfo activityInfo1 = activityInfo2;
    if (activityInfo2 == null) {
      resolveInfo2 = paramPackageManager.resolveActivity(paramIntent, 65536);
      if (resolveInfo2 != null) {
        ActivityInfo activityInfo = resolveInfo2.activityInfo;
      } else {
        paramPackageManager = null;
      } 
      packageManager = paramPackageManager;
    } 
    if (packageManager == null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("No activity found for ");
      stringBuilder.append(paramIntent);
      Log.w("OplusResolverManager", stringBuilder.toString());
      return null;
    } 
    return resolveInfo2;
  }
  
  public int getChooserPreFileSingleIconView(boolean paramBoolean, String paramString, Uri paramUri) {
    int j;
    if (!paramBoolean)
      return 201851299; 
    String str2 = this.mBaseResolverActivity.getContentResolver().getType(paramUri);
    Intent intent = new Intent();
    intent.setType(str2);
    int i = 201851300;
    String str1 = OplusResolverIntentUtil.getIntentType(intent);
    switch (str1.hashCode()) {
      default:
        j = -1;
        break;
      case 1554253136:
        if (str1.equals("application")) {
          j = 6;
          break;
        } 
      case 96948919:
        if (str1.equals("excel")) {
          j = 2;
          break;
        } 
      case 93166550:
        if (str1.equals("audio")) {
          j = 5;
          break;
        } 
      case 3655434:
        if (str1.equals("word")) {
          j = 1;
          break;
        } 
      case 115312:
        if (str1.equals("txt")) {
          j = 0;
          break;
        } 
      case 111220:
        if (str1.equals("ppt")) {
          j = 4;
          break;
        } 
      case 110834:
        if (str1.equals("pdf")) {
          j = 3;
          break;
        } 
    } 
    switch (j) {
      default:
        j = i;
        break;
      case 6:
        j = 201851290;
        break;
      case 5:
        j = 201851291;
        break;
      case 4:
        j = 201851302;
        break;
      case 3:
        j = 201851301;
        break;
      case 2:
        j = 201851296;
        break;
      case 1:
        j = 201851294;
        break;
      case 0:
        j = 201851303;
        break;
    } 
    int k = paramString.lastIndexOf(".");
    i = j;
    if (k != -1) {
      paramString = paramString.substring(k + 1).toLowerCase();
      switch (paramString.hashCode()) {
        default:
          i = -1;
          break;
        case 96305358:
          if (paramString.equals("ebook")) {
            i = 5;
            break;
          } 
        case 3213227:
          if (paramString.equals("html")) {
            i = 3;
            break;
          } 
        case 120609:
          if (paramString.equals("zip")) {
            i = 0;
            break;
          } 
        case 112675:
          if (paramString.equals("rar")) {
            i = 1;
            break;
          } 
        case 107421:
          if (paramString.equals("lrc")) {
            i = 4;
            break;
          } 
        case 96796:
          if (paramString.equals("apk")) {
            i = 2;
            break;
          } 
      } 
      if (i != 0) {
        if (i != 1) {
          if (i != 2) {
            if (i != 3) {
              if (i != 4) {
                if (i != 5) {
                  i = j;
                } else {
                  i = 201851295;
                } 
              } else {
                i = 201851298;
              } 
            } else {
              i = 201851297;
            } 
          } else {
            i = 201851290;
          } 
        } else {
          i = 201851292;
        } 
      } else {
        i = 201851293;
      } 
    } 
    return i;
  }
  
  public boolean tryApkResourceName(Uri paramUri, ImageView paramImageView, TextView paramTextView) {
    if (paramImageView == null || paramTextView == null || paramImageView.getVisibility() != 0 || paramTextView.getVisibility() != 0)
      return false; 
    String str = this.mBaseResolverActivity.getContentResolver().getType(paramUri);
    Intent intent = new Intent();
    intent.setType(str);
    str = OplusResolverIntentUtil.getIntentType(intent);
    if (!"application".equals(str))
      return false; 
    Log.d("OplusResolverManager", "try apk preview");
    OplusResolverApkPreView oplusResolverApkPreView = new OplusResolverApkPreView((Context)this.mBaseResolverActivity);
    oplusResolverApkPreView.execute(paramUri, paramImageView, paramTextView);
    return true;
  }
  
  public ViewGroup getDisplayFileContentPreview(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup) {
    return (ViewGroup)paramLayoutInflater.inflate(202440722, paramViewGroup, false);
  }
  
  public ViewGroup getDisplayImageContentPreview(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup) {
    return (ViewGroup)paramLayoutInflater.inflate(202440723, paramViewGroup, false);
  }
  
  public ViewGroup getDisplayTextContentPreview(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup) {
    return (ViewGroup)paramLayoutInflater.inflate(202440724, paramViewGroup, false);
  }
  
  public void clearInactiveProfileCache(int paramInt) {
    if (this.mLoadedPages.size() == 1)
      return; 
    this.mLoadedPages.remove(Integer.valueOf(1 - paramInt));
  }
  
  public void showActiveEmptyViewState() {
    if (shouldShowEmptyState())
      showEmptyViewState(); 
  }
  
  public void showWorkProfileOffEmptyState(AbstractMultiProfilePagerAdapter paramAbstractMultiProfilePagerAdapter, ResolverListAdapter paramResolverListAdapter, View.OnClickListener paramOnClickListener) {
    paramAbstractMultiProfilePagerAdapter.showEmptyState(paramResolverListAdapter, 201851338, 201588868, 0, new _$$Lambda$OplusResolverManager$_wYolQvSXCL9rOrzgG6ZyXEm1LQ(this, paramOnClickListener));
  }
  
  public void showNoPersonalToWorkIntentsEmptyState(AbstractMultiProfilePagerAdapter paramAbstractMultiProfilePagerAdapter, ResolverListAdapter paramResolverListAdapter) {
    int i;
    if (paramAbstractMultiProfilePagerAdapter instanceof ChooserMultiProfilePagerAdapter) {
      i = 201588859;
    } else {
      i = 201588855;
    } 
    paramAbstractMultiProfilePagerAdapter.showEmptyState(paramResolverListAdapter, 201851338, i, 0);
  }
  
  public void showNoWorkToPersonalIntentsEmptyState(AbstractMultiProfilePagerAdapter paramAbstractMultiProfilePagerAdapter, ResolverListAdapter paramResolverListAdapter) {
    int i;
    if (paramAbstractMultiProfilePagerAdapter instanceof ChooserMultiProfilePagerAdapter) {
      i = 201588858;
    } else {
      i = 201588854;
    } 
    paramAbstractMultiProfilePagerAdapter.showEmptyState(paramResolverListAdapter, 201851338, i, 0);
  }
  
  public void showNoPersonalAppsAvailableEmptyState(AbstractMultiProfilePagerAdapter paramAbstractMultiProfilePagerAdapter, ResolverListAdapter paramResolverListAdapter) {
    showNoWorkAppsAvailableEmptyState(paramAbstractMultiProfilePagerAdapter, paramResolverListAdapter);
  }
  
  public void showNoWorkAppsAvailableEmptyState(AbstractMultiProfilePagerAdapter paramAbstractMultiProfilePagerAdapter, ResolverListAdapter paramResolverListAdapter) {
    int i;
    if (paramAbstractMultiProfilePagerAdapter instanceof ChooserMultiProfilePagerAdapter) {
      i = 201588862;
    } else {
      i = 201588863;
    } 
    paramAbstractMultiProfilePagerAdapter.showEmptyState(paramResolverListAdapter, 201851339, i, 0);
  }
  
  public void restoreProfilePager(int paramInt) {
    OplusResolverDialogHelper oplusResolverDialogHelper = this.mResolverDialogHelper;
    if (oplusResolverDialogHelper != null) {
      OplusBaseResolverActivity oplusBaseResolverActivity = this.mBaseResolverActivity;
      if (oplusBaseResolverActivity != null)
        oplusResolverDialogHelper.restoreProfilePager(oplusBaseResolverActivity.getWindow().getDecorView(), paramInt); 
    } 
  }
  
  public ViewGroup getChooserProfileDescriptor(LayoutInflater paramLayoutInflater) {
    return (ViewGroup)paramLayoutInflater.inflate(202440725, null, false);
  }
  
  public ViewGroup getResolverProfileDescriptor(LayoutInflater paramLayoutInflater) {
    return (ViewGroup)paramLayoutInflater.inflate(202440738, null, false);
  }
  
  public void displayTextAddActionButton(ViewGroup paramViewGroup, View.OnClickListener paramOnClickListener) {
    ImageView imageView = new ImageView((Context)this.mBaseResolverActivity);
    imageView.setImageResource(201851308);
    paramViewGroup.addView((View)imageView);
    paramViewGroup.setPadding(0, 0, 0, 0);
    imageView.setOnClickListener(paramOnClickListener);
  }
  
  public void setCustomRoundImage(Paint paramPaint1, Paint paramPaint2, Paint paramPaint3) {
    Resources resources2 = this.mBaseResolverActivity.getResources();
    float f = resources2.getDimensionPixelSize(201654558);
    paramPaint1.setStrokeWidth(f);
    Resources resources1 = this.mBaseResolverActivity.getResources();
    f = resources1.getDimensionPixelSize(201654562);
    paramPaint2.setTextSize(f);
    paramPaint3.setColor(this.mBaseResolverActivity.getResources().getColor(201720047));
  }
  
  public int getCornerRadius(Context paramContext) {
    return paramContext.getResources().getDimensionPixelSize(201654555);
  }
  
  public void addNearbyAction(ViewGroup paramViewGroup, Intent paramIntent) {
    TargetInfo targetInfo = this.mBaseResolverActivity.getNearbySharingTargetWrapper(paramIntent);
    if (targetInfo == null)
      return; 
    Drawable drawable = targetInfo.getDisplayIcon((Context)this.mBaseResolverActivity);
    CharSequence charSequence = targetInfo.getDisplayLabel();
    ViewGroup viewGroup = (ViewGroup)LayoutInflater.from((Context)this.mBaseResolverActivity).inflate(202440721, null);
    TextView textView = (TextView)viewGroup.findViewById(201457760);
    if (drawable != null) {
      Resources resources = this.mBaseResolverActivity.getResources();
      int i = resources.getDimensionPixelSize(17105030);
      drawable.setBounds(0, 0, i, i);
      textView.setCompoundDrawablesRelative(drawable, null, null, null);
    } 
    textView.setText(charSequence);
    viewGroup.setOnClickListener(new _$$Lambda$OplusResolverManager$zu0YJxUlOsu9zH6Dbn5HO1QAG2M(this, targetInfo));
    ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -2);
    paramViewGroup.addView((View)viewGroup, layoutParams);
  }
  
  private void changeOShareAndNfc(View paramView, int paramInt) {
    if (paramInt == 1) {
      this.mResolverDialogHelper.tearDown(paramView);
    } else if (this.mIsActionSend) {
      this.mResolverDialogHelper.initOShareView(paramView);
      this.mBaseResolverActivity.getWindow().getDecorView().post(new _$$Lambda$OplusResolverManager$_Lk01LygQGxgms7EMqc_hQwjuew(this));
    } else {
      this.mResolverDialogHelper.initNfcView(paramView);
    } 
  }
  
  class TabChangeListener implements OplusResolverDialogHelper.OnProfileSelectedListener {
    private AbstractMultiProfilePagerAdapter mMultiProfilePagerAdapter;
    
    final OplusResolverManager this$0;
    
    TabChangeListener(AbstractMultiProfilePagerAdapter param1AbstractMultiProfilePagerAdapter) {
      this.mMultiProfilePagerAdapter = param1AbstractMultiProfilePagerAdapter;
    }
    
    public void onProfileSelected(int param1Int) {
      IOplusAbstractMultiProfilePagerAdapterEx iOplusAbstractMultiProfilePagerAdapterEx = (IOplusAbstractMultiProfilePagerAdapterEx)OplusTypeCastingHelper.typeCasting(IOplusAbstractMultiProfilePagerAdapterEx.class, this.mMultiProfilePagerAdapter);
      if (iOplusAbstractMultiProfilePagerAdapterEx != null)
        iOplusAbstractMultiProfilePagerAdapterEx.setCurrentPage(param1Int); 
      OplusResolverManager oplusResolverManager = OplusResolverManager.this;
      oplusResolverManager.changeOShareAndNfc(oplusResolverManager.mBaseResolverActivity.getWindow().getDecorView(), param1Int);
      if (!OplusResolverManager.this.mLoadedPages.contains(Integer.valueOf(param1Int))) {
        OplusResolverManager.this.mLoadedPages.add(Integer.valueOf(param1Int));
        this.mMultiProfilePagerAdapter.rebuildActiveTab(true);
        if (OplusResolverManager.this.shouldShowEmptyState()) {
          OplusResolverManager.this.showEmptyViewState();
          return;
        } 
        OplusResolverDialogHelper oplusResolverDialogHelper = OplusResolverManager.this.mResolverDialogHelper;
        boolean bool = OplusResolverManager.this.getActiveResolverAdapter().getUserHandle().equals(OplusResolverManager.this.mBaseResolverActivity.getWorkProfileUserHandle());
        oplusResolverManager = OplusResolverManager.this;
        param1Int = oplusResolverManager.getPlaceholderCount();
        oplusResolverDialogHelper.reLoadTabPlaceholderCount(bool, param1Int);
      } else if (!OplusResolverManager.this.shouldShowEmptyState()) {
        OplusResolverManager.this.setResolverContent();
      } else {
        OplusResolverManager.this.showEmptyViewState();
      } 
    }
  }
  
  private boolean shouldShowEmptyState() {
    boolean bool;
    AbstractMultiProfilePagerAdapter.ProfileDescriptor profileDescriptor = getMultiProfilePagerAdapter().getItem(getMultiProfilePagerAdapter().getCurrentPage());
    if (profileDescriptor.rootView.findViewById(16909351).getVisibility() == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void showEmptyViewState() {
    AbstractMultiProfilePagerAdapter.ProfileDescriptor profileDescriptor = getMultiProfilePagerAdapter().getItem(getMultiProfilePagerAdapter().getCurrentPage());
    ViewGroup viewGroup = (ViewGroup)this.mBaseResolverActivity.findViewById(16908305);
    if (viewGroup == null)
      return; 
    Drawable drawable = ((ImageView)profileDescriptor.rootView.findViewById(16909354)).getDrawable();
    if (viewGroup.getChildCount() == 2) {
      if (viewGroup.getChildAt(1) == profileDescriptor.rootView) {
        if (drawable instanceof Animatable)
          ((Animatable)drawable).start(); 
        return;
      } 
      Drawable drawable1 = ((ImageView)viewGroup.getChildAt(1).findViewById(16909354)).getDrawable();
      if (drawable1 != null && drawable1 instanceof Animatable && ((Animatable)drawable1).isRunning())
        ((Animatable)drawable1).stop(); 
      viewGroup.removeViewAt(1);
    } else {
      viewGroup.getChildAt(0).setVisibility(8);
    } 
    viewGroup.addView((View)profileDescriptor.rootView);
    if (drawable instanceof Animatable)
      ((Animatable)drawable).start(); 
  }
  
  private int getPlaceholderCount() {
    OplusBaseResolverListAdapter oplusBaseResolverListAdapter = (OplusBaseResolverListAdapter)OplusTypeCastingHelper.typeCasting(OplusBaseResolverListAdapter.class, getActiveResolverAdapter());
    if (oplusBaseResolverListAdapter != null) {
      List<ResolveInfo> list = getUnsortResolveInfo(oplusBaseResolverListAdapter.mPlaceholderResolveList);
      int i = this.mResolveInfoHelper.getExpandSizeWithoutMoreIcon(list, getTargetIntent());
      if (!OplusResolverIntentUtil.isChooserAction(getTargetIntent()) && i > 0 && i < list.size()) {
        i++;
      } else {
        i = list.size();
      } 
      return i;
    } 
    return -1;
  }
  
  private ResolverListAdapter getActiveResolverAdapter() {
    return getMultiProfilePagerAdapter().getActiveListAdapter();
  }
  
  private ResolverListAdapter getInactiveResolverAdapter() {
    return getMultiProfilePagerAdapter().getInactiveListAdapter();
  }
  
  private AbstractMultiProfilePagerAdapter getMultiProfilePagerAdapter() {
    return this.mBaseResolverActivity.getMultiProfilePagerAdapter();
  }
  
  Intent getTargetIntent() {
    return this.mBaseResolverActivity.getTargetIntent();
  }
  
  private List<ResolveInfo> getUnsortResolveInfo(List<ResolverActivity.ResolvedComponentInfo> paramList) {
    ArrayList<ResolveInfo> arrayList = new ArrayList();
    boolean bool = true;
    if ((getActiveResolverAdapter()).mDisplayList == null || (getActiveResolverAdapter()).mDisplayList.isEmpty()) {
      bool = false;
      getActiveResolverAdapter().processSortedList(paramList, false);
    } 
    if ((getActiveResolverAdapter()).mDisplayList != null && !(getActiveResolverAdapter()).mDisplayList.isEmpty())
      for (DisplayResolveInfo displayResolveInfo : (getActiveResolverAdapter()).mDisplayList) {
        if (displayResolveInfo != null && displayResolveInfo.getResolveInfo() != null)
          arrayList.add(displayResolveInfo.getResolveInfo()); 
      }  
    if (!bool && (getActiveResolverAdapter()).mDisplayList != null)
      (getActiveResolverAdapter()).mDisplayList.clear(); 
    return arrayList;
  }
  
  public void resortListAndNotifyChange() {
    resortListAndNotifyChange(getResolveInfoList((getActiveResolverAdapter()).mDisplayList));
  }
  
  private void resortListAndNotifyChange(List<ResolveInfo> paramList) {
    if (!(getActiveResolverAdapter()).mDisplayList.isEmpty()) {
      ViewGroup viewGroup = (ViewGroup)this.mBaseResolverActivity.findViewById(16908305);
      if (viewGroup.getChildCount() == 2) {
        viewGroup.getChildAt(0).setVisibility(0);
        viewGroup.removeViewAt(1);
      } 
      this.mResolverDialogHelper.resortListAndNotifyChange(getActiveResolverAdapter().getUserHandle().equals(this.mBaseResolverActivity.getWorkProfileUserHandle()), paramList);
      resortDisplayList(this.mResolverDialogHelper.getResolveInforList());
    } else if (shouldShowEmptyState()) {
      showEmptyViewState();
    } 
  }
  
  private void resortDisplayList(List<ResolveInfo> paramList) {
    ArrayList<DisplayResolveInfo> arrayList = new ArrayList();
    if (paramList != null) {
      for (ResolveInfo resolveInfo : paramList) {
        for (DisplayResolveInfo displayResolveInfo : (getActiveResolverAdapter()).mDisplayList) {
          if (resolveInfo.equals(displayResolveInfo.getResolveInfo()))
            arrayList.add(displayResolveInfo); 
        } 
      } 
      (getActiveResolverAdapter()).mDisplayList.clear();
      (getActiveResolverAdapter()).mDisplayList.addAll(arrayList);
    } 
  }
  
  private void initDialogHelper(boolean paramBoolean) {
    this.mResolveInfoHelper = OplusResolverInfoHelper.getInstance((Context)this.mBaseResolverActivity);
    OplusBaseResolverActivity oplusBaseResolverActivity = this.mBaseResolverActivity;
    Intent intent = getTargetIntent();
    this.mResolverDialogHelper = new OplusResolverDialogHelper((Activity)oplusBaseResolverActivity, intent, null, paramBoolean, getResolveInfoList((getActiveResolverAdapter()).mDisplayList));
  }
  
  private List<ResolveInfo> getResolveInfoList(List<DisplayResolveInfo> paramList) {
    ArrayList<ResolveInfo> arrayList = new ArrayList();
    if (paramList != null) {
      arrayList.clear();
      for (DisplayResolveInfo displayResolveInfo : paramList) {
        if (displayResolveInfo.getResolveInfo() != null)
          arrayList.add(displayResolveInfo.getResolveInfo()); 
      } 
    } 
    return arrayList;
  }
  
  class ItemClickListener implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {
    final OplusResolverManager this$0;
    
    public void onItemClick(AdapterView<?> param1AdapterView, View param1View, int param1Int, long param1Long) {
      OplusResolverManager.this.handleClickEvent(param1View, param1Int);
    }
    
    public boolean onItemLongClick(AdapterView<?> param1AdapterView, View param1View, int param1Int, long param1Long) {
      if (OplusResolverManager.this.mIsActionSend) {
        List<ResolveInfo> list = OplusResolverManager.this.mResolverDialogHelper.getResolveInforList();
        if (list == null || list.size() <= param1Int) {
          Log.e("OplusResolverManager", "onItemLongClick size error");
          return true;
        } 
        ResolveInfo resolveInfo = list.get(param1Int);
        String str = OplusResolverIntentUtil.getIntentType(OplusResolverManager.this.getTargetIntent());
        OplusResolverManager.this.mResolverDialogHelper.showTargetDetails(resolveInfo, OplusResolverManager.this.mPinnedSharedPrefs, str, OplusResolverManager.this.getActiveResolverAdapter());
      } 
      return true;
    }
  }
  
  private void handleClickEvent(View paramView, int paramInt) {
    if ((getActiveResolverAdapter()).mDisplayList == null || (getActiveResolverAdapter()).mDisplayList.size() <= paramInt) {
      Log.e("OplusResolverManager", "handleClickEvent size error");
      return;
    } 
    if (OplusResolverUtil.isResolverCtsTest((Context)this.mBaseResolverActivity, getTargetIntent(), ((DisplayResolveInfo)(getActiveResolverAdapter()).mDisplayList.get(paramInt)).getResolveInfo())) {
      this.mLastSelected = paramInt;
      setButtonAlwaysListener();
      return;
    } 
    boolean bool1 = false;
    CheckBox checkBox = (CheckBox)this.mBaseResolverActivity.findViewById(16908810);
    boolean bool2 = bool1;
    if (checkBox != null) {
      bool2 = bool1;
      if (checkBox.getVisibility() == 0)
        bool2 = checkBox.isChecked(); 
    } 
    this.mBaseResolverActivity.startSelected(paramInt, bool2, false);
  }
  
  static SharedPreferences getPinnedSharedPrefs(Context paramContext) {
    String str1 = StorageManager.UUID_PRIVATE_INTERNAL;
    int i = paramContext.getUserId();
    String str2 = paramContext.getPackageName();
    File file = new File(new File(Environment.getDataUserCePackageDirectory(str1, i, str2), "shared_prefs"), "chooser_pin_settings.xml");
    return paramContext.getSharedPreferences(file, 0);
  }
  
  private void setContentView(View paramView) {
    this.mBaseResolverActivity.setContentView(paramView);
    if (shouldShowEmptyState())
      showEmptyViewState(); 
    OplusResolverDrawerLayout oplusResolverDrawerLayout = (OplusResolverDrawerLayout)this.mBaseResolverActivity.findViewById(16908864);
    oplusResolverDrawerLayout.setOnDismissedListener(new _$$Lambda$OplusResolverManager$8JIQe19QBvH5oLu6EKJIXFmNKEw(this));
    updatePreviewArea();
  }
  
  private Point getScreenSize() {
    Point point = new Point();
    WindowManager windowManager = (WindowManager)this.mBaseResolverActivity.getSystemService("window");
    windowManager.getDefaultDisplay().getRealSize(point);
    return point;
  }
  
  private ComponentCallbacks mComponentCallbacks = new ComponentCallbacks() {
      final OplusResolverManager this$0;
      
      public void onConfigurationChanged(Configuration param1Configuration) {
        if (OplusResolverManager.this.mBaseResolverActivity != null)
          OplusResolverManager.this.mBaseResolverActivity.getWindow().getDecorView().post(new _$$Lambda$OplusResolverManager$1$oNClCNlyEwJLaflNVL919T7i054(this)); 
      }
      
      public void onLowMemory() {}
    };
  
  private static final String GALLERY_PIN_LIST = "gallery_pin_list";
  
  private static final String PINNED_SHARED_PREFS_NAME = "chooser_pin_settings";
  
  private static final String TAG = "OplusResolverManager";
  
  private static final String TYPE_GALLERY = "gallery";
  
  private OplusResolverApkPreView mApkPreView;
  
  private OplusBaseResolverActivity mBaseResolverActivity;
  
  private OplusResolverDrawerLayout mContentPanel;
  
  private String mGalleryPinList;
  
  protected boolean mOpenFlag;
  
  private SharedPreferences mPinnedSharedPrefs;
  
  private View mPreView;
  
  private ViewGroup mPreviewContainer;
  
  private OplusResolverInfoHelper mResolveInfoHelper;
  
  private OplusResolverDialogHelper mResolverDialogHelper;
  
  private TabChangeListener mTabListener;
  
  private void updatePreviewArea() {
    boolean bool;
    if (this.mPreviewContainer == null)
      return; 
    Point point = getScreenSize();
    if (point.x < point.y) {
      bool = true;
    } else {
      bool = false;
    } 
    if (this.mPreView != null && bool && !this.mBaseResolverActivity.isInMultiWindowMode()) {
      this.mPreviewContainer.setVisibility(0);
      if (this.mPreView.getParent() == null && !preViewAllGone(this.mPreView))
        this.mPreviewContainer.addView(this.mPreView, 0); 
    } else if (this.mPreView != null) {
      this.mPreviewContainer.setVisibility(8);
      this.mPreviewContainer.removeView(this.mPreView);
    } 
  }
  
  private void setButtonAlwaysListener() {
    if (this.mLastSelected != -1) {
      CheckBox checkBox = (CheckBox)this.mBaseResolverActivity.findViewById(16908810);
      if (checkBox != null && !checkBox.hasOnClickListeners())
        checkBox.setOnClickListener(new _$$Lambda$OplusResolverManager$u2wP9IajWvWUyOjazU4RLLhCfuk(this)); 
    } 
  }
  
  private boolean preViewAllGone(View paramView) {
    if (paramView == null || paramView.getVisibility() == 8)
      return true; 
    if (paramView instanceof ViewGroup) {
      byte b;
      int i;
      for (b = 0, i = ((ViewGroup)paramView).getChildCount(); b < i; b++) {
        if (((ViewGroup)paramView).getChildAt(b).getVisibility() != 8)
          return false; 
      } 
      return true;
    } 
    return false;
  }
}
