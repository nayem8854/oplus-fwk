package com.android.internal.app;

import android.common.ColorFrameworkFactory;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.os.UserHandle;
import android.util.Log;
import android.widget.BaseAdapter;
import com.android.internal.app.chooser.DisplayResolveInfo;
import com.android.internal.app.chooser.OplusBaseDisplayResolveInfo;
import com.oplus.util.OplusTypeCastingHelper;
import java.util.ArrayList;
import java.util.List;

public abstract class OplusBaseResolverListAdapter extends BaseAdapter {
  protected IOplusResolverManager iOplusResolverManager = (IOplusResolverManager)ColorFrameworkFactory.getInstance().getFeature(IOplusResolverManager.DEFAULT, new Object[0]);
  
  protected List<ResolverActivity.ResolvedComponentInfo> mPlaceholderResolveList;
  
  protected void setPlaceholderResolveList(List<ResolverActivity.ResolvedComponentInfo> paramList) {
    List<ResolverActivity.ResolvedComponentInfo> list = this.mPlaceholderResolveList;
    if (list == null) {
      this.mPlaceholderResolveList = new ArrayList<>();
    } else {
      list.clear();
    } 
    this.mPlaceholderResolveList.addAll(paramList);
  }
  
  ResolverListAdapter.ResolveInfoPresentationGetter makePresentationGetter(ResolveInfo paramResolveInfo) {
    return null;
  }
  
  ResolverListAdapter.ResolverListCommunicator getResolverListCommunicator() {
    return null;
  }
  
  void addMultiAppResolveInfoIfNeed(List<ResolverActivity.ResolvedComponentInfo> paramList, List<Intent> paramList1, ResolverListController paramResolverListController, PackageManager paramPackageManager, List<DisplayResolveInfo> paramList2) {
    if (paramList != null && paramList.size() == 1 && paramList1 != null && paramList1.size() > 0) {
      if (getResolverListCommunicator() == null)
        return; 
      ResolverListAdapter.ResolverListCommunicator resolverListCommunicator = getResolverListCommunicator();
      IOplusResolverListCommunicatorEx iOplusResolverListCommunicatorEx = (IOplusResolverListCommunicatorEx)OplusTypeCastingHelper.typeCasting(IOplusResolverListCommunicatorEx.class, resolverListCommunicator);
      if (iOplusResolverListCommunicatorEx != null && !iOplusResolverListCommunicatorEx.hasCustomFlag(512))
        return; 
      try {
        UserHandle userHandle = new UserHandle();
        this(999);
        try {
          List<ResolverActivity.ResolvedComponentInfo> list = paramResolverListController.getResolversForIntentAsUser(false, false, paramList1, userHandle);
          if (list != null && list.size() > 0 && list.get(0) != null) {
            ResolveInfo resolveInfo = ((ResolverActivity.ResolvedComponentInfo)list.get(0)).getResolveInfoAt(0);
            if (resolveInfo != null) {
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append(resolveInfo.loadLabel(paramPackageManager));
              stringBuilder.append(Resources.getSystem().getString(201588804));
              String str = stringBuilder.toString();
              Intent intent1 = ((ResolverActivity.ResolvedComponentInfo)list.get(0)).getIntentAt(0);
              DisplayResolveInfo displayResolveInfo = new DisplayResolveInfo();
              Intent intent2 = paramList1.get(0);
              CharSequence charSequence = resolveInfo.loadLabel(paramPackageManager);
              if (intent1 == null)
                intent1 = paramList1.get(0); 
              this(intent2, resolveInfo, str, charSequence, intent1, null);
              OplusBaseDisplayResolveInfo oplusBaseDisplayResolveInfo = (OplusBaseDisplayResolveInfo)OplusTypeCastingHelper.typeCasting(OplusBaseDisplayResolveInfo.class, displayResolveInfo);
              if (oplusBaseDisplayResolveInfo != null)
                oplusBaseDisplayResolveInfo.setIsMultiApp(true); 
              if ((displayResolveInfo.getResolveInfo()).targetUserId != -2)
                (displayResolveInfo.getResolveInfo()).targetUserId = -2; 
              try {
                if (!isDisplayResolveInfoExist(paramList2, displayResolveInfo))
                  paramList2.add(displayResolveInfo); 
              } catch (Exception exception) {
                Log.e("MultiApp", "addMultiAppResolveInfoIfNeed", exception);
              } 
            } 
          } 
        } catch (Exception exception) {
          Log.e("MultiApp", "addMultiAppResolveInfoIfNeed", exception);
        } 
      } catch (Exception exception) {
        Log.e("MultiApp", "addMultiAppResolveInfoIfNeed", exception);
      } 
    } 
  }
  
  private boolean isDisplayResolveInfoExist(List<DisplayResolveInfo> paramList, DisplayResolveInfo paramDisplayResolveInfo) {
    for (DisplayResolveInfo displayResolveInfo : paramList) {
      if (displayResolveInfo.getDisplayLabel().equals(paramDisplayResolveInfo.getDisplayLabel()))
        return true; 
    } 
    return false;
  }
  
  protected ResolveInfo getResolveInfo(Intent paramIntent, PackageManager paramPackageManager) {
    IOplusResolverManager iOplusResolverManager = this.iOplusResolverManager;
    if (iOplusResolverManager != null)
      return iOplusResolverManager.getResolveInfo(paramIntent, paramPackageManager); 
    return null;
  }
  
  protected boolean sortComponentsNull(List paramList, boolean paramBoolean) {
    boolean bool1 = false, bool2 = false;
    if (paramBoolean && isOriginUi()) {
      paramBoolean = bool2;
      if (paramList != null) {
        paramBoolean = bool2;
        if (!paramList.isEmpty())
          paramBoolean = true; 
      } 
      return paramBoolean;
    } 
    if (paramBoolean && !isOriginUi())
      return true; 
    if (!paramBoolean && isOriginUi())
      return true; 
    if (!paramBoolean && !isOriginUi()) {
      paramBoolean = bool1;
      if (paramList != null) {
        paramBoolean = bool1;
        if (!paramList.isEmpty())
          paramBoolean = true; 
      } 
      return paramBoolean;
    } 
    return true;
  }
  
  protected boolean isOriginUi() {
    IOplusResolverManager iOplusResolverManager = this.iOplusResolverManager;
    if (iOplusResolverManager != null)
      return iOplusResolverManager.isOriginUi(); 
    return true;
  }
}
