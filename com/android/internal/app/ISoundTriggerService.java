package com.android.internal.app;

import android.content.ComponentName;
import android.hardware.soundtrigger.IRecognitionStatusCallback;
import android.hardware.soundtrigger.SoundTrigger;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelUuid;
import android.os.RemoteException;

public interface ISoundTriggerService extends IInterface {
  void deleteSoundModel(ParcelUuid paramParcelUuid) throws RemoteException;
  
  int getModelState(ParcelUuid paramParcelUuid) throws RemoteException;
  
  SoundTrigger.ModuleProperties getModuleProperties() throws RemoteException;
  
  int getParameter(ParcelUuid paramParcelUuid, int paramInt) throws RemoteException;
  
  SoundTrigger.GenericSoundModel getSoundModel(ParcelUuid paramParcelUuid) throws RemoteException;
  
  boolean isRecognitionActive(ParcelUuid paramParcelUuid) throws RemoteException;
  
  int loadGenericSoundModel(SoundTrigger.GenericSoundModel paramGenericSoundModel) throws RemoteException;
  
  int loadKeyphraseSoundModel(SoundTrigger.KeyphraseSoundModel paramKeyphraseSoundModel) throws RemoteException;
  
  SoundTrigger.ModelParamRange queryParameter(ParcelUuid paramParcelUuid, int paramInt) throws RemoteException;
  
  int setParameter(ParcelUuid paramParcelUuid, int paramInt1, int paramInt2) throws RemoteException;
  
  int startRecognition(ParcelUuid paramParcelUuid, IRecognitionStatusCallback paramIRecognitionStatusCallback, SoundTrigger.RecognitionConfig paramRecognitionConfig) throws RemoteException;
  
  int startRecognitionForService(ParcelUuid paramParcelUuid, Bundle paramBundle, ComponentName paramComponentName, SoundTrigger.RecognitionConfig paramRecognitionConfig) throws RemoteException;
  
  int stopRecognition(ParcelUuid paramParcelUuid, IRecognitionStatusCallback paramIRecognitionStatusCallback) throws RemoteException;
  
  int stopRecognitionForService(ParcelUuid paramParcelUuid) throws RemoteException;
  
  int unloadSoundModel(ParcelUuid paramParcelUuid) throws RemoteException;
  
  void updateSoundModel(SoundTrigger.GenericSoundModel paramGenericSoundModel) throws RemoteException;
  
  class Default implements ISoundTriggerService {
    public SoundTrigger.GenericSoundModel getSoundModel(ParcelUuid param1ParcelUuid) throws RemoteException {
      return null;
    }
    
    public void updateSoundModel(SoundTrigger.GenericSoundModel param1GenericSoundModel) throws RemoteException {}
    
    public void deleteSoundModel(ParcelUuid param1ParcelUuid) throws RemoteException {}
    
    public int startRecognition(ParcelUuid param1ParcelUuid, IRecognitionStatusCallback param1IRecognitionStatusCallback, SoundTrigger.RecognitionConfig param1RecognitionConfig) throws RemoteException {
      return 0;
    }
    
    public int stopRecognition(ParcelUuid param1ParcelUuid, IRecognitionStatusCallback param1IRecognitionStatusCallback) throws RemoteException {
      return 0;
    }
    
    public int loadGenericSoundModel(SoundTrigger.GenericSoundModel param1GenericSoundModel) throws RemoteException {
      return 0;
    }
    
    public int loadKeyphraseSoundModel(SoundTrigger.KeyphraseSoundModel param1KeyphraseSoundModel) throws RemoteException {
      return 0;
    }
    
    public int startRecognitionForService(ParcelUuid param1ParcelUuid, Bundle param1Bundle, ComponentName param1ComponentName, SoundTrigger.RecognitionConfig param1RecognitionConfig) throws RemoteException {
      return 0;
    }
    
    public int stopRecognitionForService(ParcelUuid param1ParcelUuid) throws RemoteException {
      return 0;
    }
    
    public int unloadSoundModel(ParcelUuid param1ParcelUuid) throws RemoteException {
      return 0;
    }
    
    public boolean isRecognitionActive(ParcelUuid param1ParcelUuid) throws RemoteException {
      return false;
    }
    
    public int getModelState(ParcelUuid param1ParcelUuid) throws RemoteException {
      return 0;
    }
    
    public SoundTrigger.ModuleProperties getModuleProperties() throws RemoteException {
      return null;
    }
    
    public int setParameter(ParcelUuid param1ParcelUuid, int param1Int1, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public int getParameter(ParcelUuid param1ParcelUuid, int param1Int) throws RemoteException {
      return 0;
    }
    
    public SoundTrigger.ModelParamRange queryParameter(ParcelUuid param1ParcelUuid, int param1Int) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISoundTriggerService {
    private static final String DESCRIPTOR = "com.android.internal.app.ISoundTriggerService";
    
    static final int TRANSACTION_deleteSoundModel = 3;
    
    static final int TRANSACTION_getModelState = 12;
    
    static final int TRANSACTION_getModuleProperties = 13;
    
    static final int TRANSACTION_getParameter = 15;
    
    static final int TRANSACTION_getSoundModel = 1;
    
    static final int TRANSACTION_isRecognitionActive = 11;
    
    static final int TRANSACTION_loadGenericSoundModel = 6;
    
    static final int TRANSACTION_loadKeyphraseSoundModel = 7;
    
    static final int TRANSACTION_queryParameter = 16;
    
    static final int TRANSACTION_setParameter = 14;
    
    static final int TRANSACTION_startRecognition = 4;
    
    static final int TRANSACTION_startRecognitionForService = 8;
    
    static final int TRANSACTION_stopRecognition = 5;
    
    static final int TRANSACTION_stopRecognitionForService = 9;
    
    static final int TRANSACTION_unloadSoundModel = 10;
    
    static final int TRANSACTION_updateSoundModel = 2;
    
    public Stub() {
      attachInterface(this, "com.android.internal.app.ISoundTriggerService");
    }
    
    public static ISoundTriggerService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.app.ISoundTriggerService");
      if (iInterface != null && iInterface instanceof ISoundTriggerService)
        return (ISoundTriggerService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 16:
          return "queryParameter";
        case 15:
          return "getParameter";
        case 14:
          return "setParameter";
        case 13:
          return "getModuleProperties";
        case 12:
          return "getModelState";
        case 11:
          return "isRecognitionActive";
        case 10:
          return "unloadSoundModel";
        case 9:
          return "stopRecognitionForService";
        case 8:
          return "startRecognitionForService";
        case 7:
          return "loadKeyphraseSoundModel";
        case 6:
          return "loadGenericSoundModel";
        case 5:
          return "stopRecognition";
        case 4:
          return "startRecognition";
        case 3:
          return "deleteSoundModel";
        case 2:
          return "updateSoundModel";
        case 1:
          break;
      } 
      return "getSoundModel";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        int i;
        SoundTrigger.ModelParamRange modelParamRange;
        SoundTrigger.ModuleProperties moduleProperties;
        IRecognitionStatusCallback iRecognitionStatusCallback1;
        ParcelUuid parcelUuid;
        Bundle bundle;
        IRecognitionStatusCallback iRecognitionStatusCallback2;
        ComponentName componentName;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 16:
            param1Parcel1.enforceInterface("com.android.internal.app.ISoundTriggerService");
            if (param1Parcel1.readInt() != 0) {
              parcelUuid = (ParcelUuid)ParcelUuid.CREATOR.createFromParcel(param1Parcel1);
            } else {
              parcelUuid = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            modelParamRange = queryParameter(parcelUuid, param1Int1);
            param1Parcel2.writeNoException();
            if (modelParamRange != null) {
              param1Parcel2.writeInt(1);
              modelParamRange.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 15:
            modelParamRange.enforceInterface("com.android.internal.app.ISoundTriggerService");
            if (modelParamRange.readInt() != 0) {
              parcelUuid = (ParcelUuid)ParcelUuid.CREATOR.createFromParcel((Parcel)modelParamRange);
            } else {
              parcelUuid = null;
            } 
            param1Int1 = modelParamRange.readInt();
            param1Int1 = getParameter(parcelUuid, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 14:
            modelParamRange.enforceInterface("com.android.internal.app.ISoundTriggerService");
            if (modelParamRange.readInt() != 0) {
              parcelUuid = (ParcelUuid)ParcelUuid.CREATOR.createFromParcel((Parcel)modelParamRange);
            } else {
              parcelUuid = null;
            } 
            param1Int2 = modelParamRange.readInt();
            param1Int1 = modelParamRange.readInt();
            param1Int1 = setParameter(parcelUuid, param1Int2, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 13:
            modelParamRange.enforceInterface("com.android.internal.app.ISoundTriggerService");
            moduleProperties = getModuleProperties();
            param1Parcel2.writeNoException();
            if (moduleProperties != null) {
              param1Parcel2.writeInt(1);
              moduleProperties.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 12:
            moduleProperties.enforceInterface("com.android.internal.app.ISoundTriggerService");
            if (moduleProperties.readInt() != 0) {
              ParcelUuid parcelUuid1 = (ParcelUuid)ParcelUuid.CREATOR.createFromParcel((Parcel)moduleProperties);
            } else {
              moduleProperties = null;
            } 
            param1Int1 = getModelState((ParcelUuid)moduleProperties);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 11:
            moduleProperties.enforceInterface("com.android.internal.app.ISoundTriggerService");
            if (moduleProperties.readInt() != 0) {
              ParcelUuid parcelUuid1 = (ParcelUuid)ParcelUuid.CREATOR.createFromParcel((Parcel)moduleProperties);
            } else {
              moduleProperties = null;
            } 
            bool = isRecognitionActive((ParcelUuid)moduleProperties);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 10:
            moduleProperties.enforceInterface("com.android.internal.app.ISoundTriggerService");
            if (moduleProperties.readInt() != 0) {
              ParcelUuid parcelUuid1 = (ParcelUuid)ParcelUuid.CREATOR.createFromParcel((Parcel)moduleProperties);
            } else {
              moduleProperties = null;
            } 
            i = unloadSoundModel((ParcelUuid)moduleProperties);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 9:
            moduleProperties.enforceInterface("com.android.internal.app.ISoundTriggerService");
            if (moduleProperties.readInt() != 0) {
              ParcelUuid parcelUuid1 = (ParcelUuid)ParcelUuid.CREATOR.createFromParcel((Parcel)moduleProperties);
            } else {
              moduleProperties = null;
            } 
            i = stopRecognitionForService((ParcelUuid)moduleProperties);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 8:
            moduleProperties.enforceInterface("com.android.internal.app.ISoundTriggerService");
            if (moduleProperties.readInt() != 0) {
              parcelUuid = (ParcelUuid)ParcelUuid.CREATOR.createFromParcel((Parcel)moduleProperties);
            } else {
              parcelUuid = null;
            } 
            if (moduleProperties.readInt() != 0) {
              bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)moduleProperties);
            } else {
              bundle = null;
            } 
            if (moduleProperties.readInt() != 0) {
              componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)moduleProperties);
            } else {
              componentName = null;
            } 
            if (moduleProperties.readInt() != 0) {
              SoundTrigger.RecognitionConfig recognitionConfig = (SoundTrigger.RecognitionConfig)SoundTrigger.RecognitionConfig.CREATOR.createFromParcel((Parcel)moduleProperties);
            } else {
              moduleProperties = null;
            } 
            i = startRecognitionForService(parcelUuid, bundle, componentName, (SoundTrigger.RecognitionConfig)moduleProperties);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 7:
            moduleProperties.enforceInterface("com.android.internal.app.ISoundTriggerService");
            if (moduleProperties.readInt() != 0) {
              SoundTrigger.KeyphraseSoundModel keyphraseSoundModel = (SoundTrigger.KeyphraseSoundModel)SoundTrigger.KeyphraseSoundModel.CREATOR.createFromParcel((Parcel)moduleProperties);
            } else {
              moduleProperties = null;
            } 
            i = loadKeyphraseSoundModel((SoundTrigger.KeyphraseSoundModel)moduleProperties);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 6:
            moduleProperties.enforceInterface("com.android.internal.app.ISoundTriggerService");
            if (moduleProperties.readInt() != 0) {
              SoundTrigger.GenericSoundModel genericSoundModel1 = (SoundTrigger.GenericSoundModel)SoundTrigger.GenericSoundModel.CREATOR.createFromParcel((Parcel)moduleProperties);
            } else {
              moduleProperties = null;
            } 
            i = loadGenericSoundModel((SoundTrigger.GenericSoundModel)moduleProperties);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 5:
            moduleProperties.enforceInterface("com.android.internal.app.ISoundTriggerService");
            if (moduleProperties.readInt() != 0) {
              parcelUuid = (ParcelUuid)ParcelUuid.CREATOR.createFromParcel((Parcel)moduleProperties);
            } else {
              parcelUuid = null;
            } 
            iRecognitionStatusCallback1 = IRecognitionStatusCallback.Stub.asInterface(moduleProperties.readStrongBinder());
            i = stopRecognition(parcelUuid, iRecognitionStatusCallback1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 4:
            iRecognitionStatusCallback1.enforceInterface("com.android.internal.app.ISoundTriggerService");
            if (iRecognitionStatusCallback1.readInt() != 0) {
              parcelUuid = (ParcelUuid)ParcelUuid.CREATOR.createFromParcel((Parcel)iRecognitionStatusCallback1);
            } else {
              parcelUuid = null;
            } 
            iRecognitionStatusCallback2 = IRecognitionStatusCallback.Stub.asInterface(iRecognitionStatusCallback1.readStrongBinder());
            if (iRecognitionStatusCallback1.readInt() != 0) {
              SoundTrigger.RecognitionConfig recognitionConfig = (SoundTrigger.RecognitionConfig)SoundTrigger.RecognitionConfig.CREATOR.createFromParcel((Parcel)iRecognitionStatusCallback1);
            } else {
              iRecognitionStatusCallback1 = null;
            } 
            i = startRecognition(parcelUuid, iRecognitionStatusCallback2, (SoundTrigger.RecognitionConfig)iRecognitionStatusCallback1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 3:
            iRecognitionStatusCallback1.enforceInterface("com.android.internal.app.ISoundTriggerService");
            if (iRecognitionStatusCallback1.readInt() != 0) {
              ParcelUuid parcelUuid1 = (ParcelUuid)ParcelUuid.CREATOR.createFromParcel((Parcel)iRecognitionStatusCallback1);
            } else {
              iRecognitionStatusCallback1 = null;
            } 
            deleteSoundModel((ParcelUuid)iRecognitionStatusCallback1);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            iRecognitionStatusCallback1.enforceInterface("com.android.internal.app.ISoundTriggerService");
            if (iRecognitionStatusCallback1.readInt() != 0) {
              SoundTrigger.GenericSoundModel genericSoundModel1 = (SoundTrigger.GenericSoundModel)SoundTrigger.GenericSoundModel.CREATOR.createFromParcel((Parcel)iRecognitionStatusCallback1);
            } else {
              iRecognitionStatusCallback1 = null;
            } 
            updateSoundModel((SoundTrigger.GenericSoundModel)iRecognitionStatusCallback1);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        iRecognitionStatusCallback1.enforceInterface("com.android.internal.app.ISoundTriggerService");
        if (iRecognitionStatusCallback1.readInt() != 0) {
          ParcelUuid parcelUuid1 = (ParcelUuid)ParcelUuid.CREATOR.createFromParcel((Parcel)iRecognitionStatusCallback1);
        } else {
          iRecognitionStatusCallback1 = null;
        } 
        SoundTrigger.GenericSoundModel genericSoundModel = getSoundModel((ParcelUuid)iRecognitionStatusCallback1);
        param1Parcel2.writeNoException();
        if (genericSoundModel != null) {
          param1Parcel2.writeInt(1);
          genericSoundModel.writeToParcel(param1Parcel2, 1);
        } else {
          param1Parcel2.writeInt(0);
        } 
        return true;
      } 
      param1Parcel2.writeString("com.android.internal.app.ISoundTriggerService");
      return true;
    }
    
    private static class Proxy implements ISoundTriggerService {
      public static ISoundTriggerService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.app.ISoundTriggerService";
      }
      
      public SoundTrigger.GenericSoundModel getSoundModel(ParcelUuid param2ParcelUuid) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.ISoundTriggerService");
          if (param2ParcelUuid != null) {
            parcel1.writeInt(1);
            param2ParcelUuid.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ISoundTriggerService.Stub.getDefaultImpl() != null)
            return ISoundTriggerService.Stub.getDefaultImpl().getSoundModel(param2ParcelUuid); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            SoundTrigger.GenericSoundModel genericSoundModel = (SoundTrigger.GenericSoundModel)SoundTrigger.GenericSoundModel.CREATOR.createFromParcel(parcel2);
          } else {
            param2ParcelUuid = null;
          } 
          return (SoundTrigger.GenericSoundModel)param2ParcelUuid;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateSoundModel(SoundTrigger.GenericSoundModel param2GenericSoundModel) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.ISoundTriggerService");
          if (param2GenericSoundModel != null) {
            parcel1.writeInt(1);
            param2GenericSoundModel.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ISoundTriggerService.Stub.getDefaultImpl() != null) {
            ISoundTriggerService.Stub.getDefaultImpl().updateSoundModel(param2GenericSoundModel);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deleteSoundModel(ParcelUuid param2ParcelUuid) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.ISoundTriggerService");
          if (param2ParcelUuid != null) {
            parcel1.writeInt(1);
            param2ParcelUuid.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && ISoundTriggerService.Stub.getDefaultImpl() != null) {
            ISoundTriggerService.Stub.getDefaultImpl().deleteSoundModel(param2ParcelUuid);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int startRecognition(ParcelUuid param2ParcelUuid, IRecognitionStatusCallback param2IRecognitionStatusCallback, SoundTrigger.RecognitionConfig param2RecognitionConfig) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.app.ISoundTriggerService");
          if (param2ParcelUuid != null) {
            parcel1.writeInt(1);
            param2ParcelUuid.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2IRecognitionStatusCallback != null) {
            iBinder = param2IRecognitionStatusCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2RecognitionConfig != null) {
            parcel1.writeInt(1);
            param2RecognitionConfig.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && ISoundTriggerService.Stub.getDefaultImpl() != null)
            return ISoundTriggerService.Stub.getDefaultImpl().startRecognition(param2ParcelUuid, param2IRecognitionStatusCallback, param2RecognitionConfig); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int stopRecognition(ParcelUuid param2ParcelUuid, IRecognitionStatusCallback param2IRecognitionStatusCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.app.ISoundTriggerService");
          if (param2ParcelUuid != null) {
            parcel1.writeInt(1);
            param2ParcelUuid.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2IRecognitionStatusCallback != null) {
            iBinder = param2IRecognitionStatusCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && ISoundTriggerService.Stub.getDefaultImpl() != null)
            return ISoundTriggerService.Stub.getDefaultImpl().stopRecognition(param2ParcelUuid, param2IRecognitionStatusCallback); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int loadGenericSoundModel(SoundTrigger.GenericSoundModel param2GenericSoundModel) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.ISoundTriggerService");
          if (param2GenericSoundModel != null) {
            parcel1.writeInt(1);
            param2GenericSoundModel.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && ISoundTriggerService.Stub.getDefaultImpl() != null)
            return ISoundTriggerService.Stub.getDefaultImpl().loadGenericSoundModel(param2GenericSoundModel); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int loadKeyphraseSoundModel(SoundTrigger.KeyphraseSoundModel param2KeyphraseSoundModel) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.ISoundTriggerService");
          if (param2KeyphraseSoundModel != null) {
            parcel1.writeInt(1);
            param2KeyphraseSoundModel.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && ISoundTriggerService.Stub.getDefaultImpl() != null)
            return ISoundTriggerService.Stub.getDefaultImpl().loadKeyphraseSoundModel(param2KeyphraseSoundModel); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int startRecognitionForService(ParcelUuid param2ParcelUuid, Bundle param2Bundle, ComponentName param2ComponentName, SoundTrigger.RecognitionConfig param2RecognitionConfig) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.ISoundTriggerService");
          if (param2ParcelUuid != null) {
            parcel1.writeInt(1);
            param2ParcelUuid.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2RecognitionConfig != null) {
            parcel1.writeInt(1);
            param2RecognitionConfig.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && ISoundTriggerService.Stub.getDefaultImpl() != null)
            return ISoundTriggerService.Stub.getDefaultImpl().startRecognitionForService(param2ParcelUuid, param2Bundle, param2ComponentName, param2RecognitionConfig); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int stopRecognitionForService(ParcelUuid param2ParcelUuid) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.ISoundTriggerService");
          if (param2ParcelUuid != null) {
            parcel1.writeInt(1);
            param2ParcelUuid.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && ISoundTriggerService.Stub.getDefaultImpl() != null)
            return ISoundTriggerService.Stub.getDefaultImpl().stopRecognitionForService(param2ParcelUuid); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int unloadSoundModel(ParcelUuid param2ParcelUuid) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.ISoundTriggerService");
          if (param2ParcelUuid != null) {
            parcel1.writeInt(1);
            param2ParcelUuid.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && ISoundTriggerService.Stub.getDefaultImpl() != null)
            return ISoundTriggerService.Stub.getDefaultImpl().unloadSoundModel(param2ParcelUuid); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isRecognitionActive(ParcelUuid param2ParcelUuid) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.ISoundTriggerService");
          boolean bool1 = true;
          if (param2ParcelUuid != null) {
            parcel1.writeInt(1);
            param2ParcelUuid.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool2 && ISoundTriggerService.Stub.getDefaultImpl() != null) {
            bool1 = ISoundTriggerService.Stub.getDefaultImpl().isRecognitionActive(param2ParcelUuid);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getModelState(ParcelUuid param2ParcelUuid) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.ISoundTriggerService");
          if (param2ParcelUuid != null) {
            parcel1.writeInt(1);
            param2ParcelUuid.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && ISoundTriggerService.Stub.getDefaultImpl() != null)
            return ISoundTriggerService.Stub.getDefaultImpl().getModelState(param2ParcelUuid); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public SoundTrigger.ModuleProperties getModuleProperties() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          SoundTrigger.ModuleProperties moduleProperties;
          parcel1.writeInterfaceToken("com.android.internal.app.ISoundTriggerService");
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && ISoundTriggerService.Stub.getDefaultImpl() != null) {
            moduleProperties = ISoundTriggerService.Stub.getDefaultImpl().getModuleProperties();
            return moduleProperties;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            moduleProperties = (SoundTrigger.ModuleProperties)SoundTrigger.ModuleProperties.CREATOR.createFromParcel(parcel2);
          } else {
            moduleProperties = null;
          } 
          return moduleProperties;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setParameter(ParcelUuid param2ParcelUuid, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.ISoundTriggerService");
          if (param2ParcelUuid != null) {
            parcel1.writeInt(1);
            param2ParcelUuid.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && ISoundTriggerService.Stub.getDefaultImpl() != null) {
            param2Int1 = ISoundTriggerService.Stub.getDefaultImpl().setParameter(param2ParcelUuid, param2Int1, param2Int2);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getParameter(ParcelUuid param2ParcelUuid, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.ISoundTriggerService");
          if (param2ParcelUuid != null) {
            parcel1.writeInt(1);
            param2ParcelUuid.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && ISoundTriggerService.Stub.getDefaultImpl() != null) {
            param2Int = ISoundTriggerService.Stub.getDefaultImpl().getParameter(param2ParcelUuid, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public SoundTrigger.ModelParamRange queryParameter(ParcelUuid param2ParcelUuid, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.ISoundTriggerService");
          if (param2ParcelUuid != null) {
            parcel1.writeInt(1);
            param2ParcelUuid.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && ISoundTriggerService.Stub.getDefaultImpl() != null)
            return ISoundTriggerService.Stub.getDefaultImpl().queryParameter(param2ParcelUuid, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            SoundTrigger.ModelParamRange modelParamRange = (SoundTrigger.ModelParamRange)SoundTrigger.ModelParamRange.CREATOR.createFromParcel(parcel2);
          } else {
            param2ParcelUuid = null;
          } 
          return (SoundTrigger.ModelParamRange)param2ParcelUuid;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISoundTriggerService param1ISoundTriggerService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISoundTriggerService != null) {
          Proxy.sDefaultImpl = param1ISoundTriggerService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISoundTriggerService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
