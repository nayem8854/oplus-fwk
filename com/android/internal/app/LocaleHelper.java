package com.android.internal.app;

import android.icu.text.ListFormatter;
import android.icu.util.ULocale;
import android.os.LocaleList;
import android.text.TextUtils;
import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

public class LocaleHelper {
  public static String toSentenceCase(String paramString, Locale paramLocale) {
    if (paramString.isEmpty())
      return paramString; 
    int i = paramString.offsetByCodePoints(0, 1);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString.substring(0, i).toUpperCase(paramLocale));
    stringBuilder.append(paramString.substring(i));
    return stringBuilder.toString();
  }
  
  public static String normalizeForSearch(String paramString, Locale paramLocale) {
    return paramString.toUpperCase();
  }
  
  private static boolean shouldUseDialectName(Locale paramLocale) {
    String str = paramLocale.getLanguage();
    return ("fa".equals(str) || 
      "ro".equals(str) || 
      "zh".equals(str));
  }
  
  public static String getDisplayName(Locale paramLocale1, Locale paramLocale2, boolean paramBoolean) {
    String str;
    ULocale uLocale = ULocale.forLocale(paramLocale2);
    if (shouldUseDialectName(paramLocale1)) {
      str = ULocale.getDisplayNameWithDialect(paramLocale1.toLanguageTag(), uLocale);
    } else {
      str = ULocale.getDisplayName(str.toLanguageTag(), uLocale);
    } 
    if (paramBoolean)
      str = toSentenceCase(str, paramLocale2); 
    return str;
  }
  
  public static String getDisplayName(Locale paramLocale, boolean paramBoolean) {
    return getDisplayName(paramLocale, Locale.getDefault(), paramBoolean);
  }
  
  public static String getDisplayCountry(Locale paramLocale1, Locale paramLocale2) {
    String str3 = paramLocale1.toLanguageTag();
    ULocale uLocale = ULocale.forLocale(paramLocale2);
    String str2 = ULocale.getDisplayCountry(str3, uLocale);
    String str1 = paramLocale1.getUnicodeLocaleType("nu");
    if (str1 != null) {
      str1 = ULocale.getDisplayKeywordValue(str3, "numbers", uLocale);
      return String.format("%s (%s)", new Object[] { str2, str1 });
    } 
    return str2;
  }
  
  public static String getDisplayCountry(Locale paramLocale) {
    return ULocale.getDisplayCountry(paramLocale.toLanguageTag(), ULocale.getDefault());
  }
  
  public static String getDisplayLocaleList(LocaleList paramLocaleList, Locale paramLocale, int paramInt) {
    boolean bool;
    int i;
    if (paramLocale == null)
      paramLocale = Locale.getDefault(); 
    if (paramLocaleList.size() > paramInt) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool) {
      i = paramInt;
      j = paramInt + 1;
    } else {
      i = j = paramLocaleList.size();
    } 
    String[] arrayOfString = new String[j];
    for (int j = 0; j < i; j++)
      arrayOfString[j] = getDisplayName(paramLocaleList.get(j), paramLocale, false); 
    if (bool)
      arrayOfString[paramInt] = TextUtils.getEllipsisString(TextUtils.TruncateAt.END); 
    ListFormatter listFormatter = ListFormatter.getInstance(paramLocale);
    return listFormatter.format((Object[])arrayOfString);
  }
  
  public static Locale addLikelySubtags(Locale paramLocale) {
    return ULocale.addLikelySubtags(ULocale.forLocale(paramLocale)).toLocale();
  }
  
  public static final class LocaleInfoComparator implements Comparator<LocaleStore.LocaleInfo> {
    private static final String PREFIX_ARABIC = "ال";
    
    private final Collator mCollator;
    
    private final boolean mCountryMode;
    
    public LocaleInfoComparator(Locale param1Locale, boolean param1Boolean) {
      this.mCollator = Collator.getInstance(param1Locale);
      this.mCountryMode = param1Boolean;
    }
    
    private String removePrefixForCompare(Locale param1Locale, String param1String) {
      if ("ar".equals(param1Locale.getLanguage()) && param1String.startsWith("ال"))
        return param1String.substring("ال".length()); 
      return param1String;
    }
    
    public int compare(LocaleStore.LocaleInfo param1LocaleInfo1, LocaleStore.LocaleInfo param1LocaleInfo2) {
      String str;
      boolean bool;
      if (param1LocaleInfo1.isSuggested() == param1LocaleInfo2.isSuggested()) {
        Collator collator = this.mCollator;
        str = removePrefixForCompare(param1LocaleInfo1.getLocale(), param1LocaleInfo1.getLabel(this.mCountryMode));
        String str1 = removePrefixForCompare(param1LocaleInfo2.getLocale(), param1LocaleInfo2.getLabel(this.mCountryMode));
        return collator.compare(str, str1);
      } 
      if (str.isSuggested()) {
        bool = true;
      } else {
        bool = true;
      } 
      return bool;
    }
  }
}
