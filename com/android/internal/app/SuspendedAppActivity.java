package com.android.internal.app;

import android.app.AppGlobals;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.IPackageManager;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.SuspendDialogInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.UserHandle;
import android.util.Slog;
import com.android.internal.util.ArrayUtils;

public class SuspendedAppActivity extends AlertActivity implements DialogInterface.OnClickListener {
  public static final String EXTRA_ACTIVITY_OPTIONS = "com.android.internal.app.extra.ACTIVITY_OPTIONS";
  
  public static final String EXTRA_DIALOG_INFO = "com.android.internal.app.extra.DIALOG_INFO";
  
  public static final String EXTRA_SUSPENDED_PACKAGE = "com.android.internal.app.extra.SUSPENDED_PACKAGE";
  
  public static final String EXTRA_SUSPENDING_PACKAGE = "com.android.internal.app.extra.SUSPENDING_PACKAGE";
  
  public static final String EXTRA_UNSUSPEND_INTENT = "com.android.internal.app.extra.UNSUSPEND_INTENT";
  
  private static final String PACKAGE_NAME = "com.android.internal.app";
  
  private static final String TAG = SuspendedAppActivity.class.getSimpleName();
  
  private Intent mMoreDetailsIntent;
  
  private int mNeutralButtonAction;
  
  private IntentSender mOnUnsuspend;
  
  private Bundle mOptions;
  
  private PackageManager mPm;
  
  private SuspendDialogInfo mSuppliedDialogInfo;
  
  private String mSuspendedPackage;
  
  private Resources mSuspendingAppResources;
  
  private String mSuspendingPackage;
  
  private int mUserId;
  
  private CharSequence getAppLabel(String paramString) {
    try {
      return this.mPm.getApplicationInfoAsUser(paramString, 0, this.mUserId).loadLabel(this.mPm);
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Package ");
      stringBuilder.append(paramString);
      stringBuilder.append(" not found");
      Slog.e(str, stringBuilder.toString(), (Throwable)nameNotFoundException);
      return paramString;
    } 
  }
  
  private Intent getMoreDetailsActivity() {
    Intent intent1 = new Intent("android.intent.action.SHOW_SUSPENDED_APP_DETAILS");
    String str = this.mSuspendingPackage;
    Intent intent2 = intent1.setPackage(str);
    ResolveInfo resolveInfo = this.mPm.resolveActivityAsUser(intent2, 786432, this.mUserId);
    if (resolveInfo != null && resolveInfo.activityInfo != null) {
      String str1 = resolveInfo.activityInfo.permission;
      if ("android.permission.SEND_SHOW_SUSPENDED_APP_DETAILS".equals(str1)) {
        Intent intent = intent2.putExtra("android.intent.extra.PACKAGE_NAME", this.mSuspendedPackage);
        intent.setFlags(335544320);
        return intent2;
      } 
    } 
    return null;
  }
  
  private Drawable resolveIcon() {
    boolean bool;
    SuspendDialogInfo suspendDialogInfo = this.mSuppliedDialogInfo;
    if (suspendDialogInfo != null) {
      bool = suspendDialogInfo.getIconResId();
    } else {
      bool = false;
    } 
    if (bool) {
      Resources resources = this.mSuspendingAppResources;
      if (resources != null)
        try {
          return resources.getDrawable(bool, getTheme());
        } catch (android.content.res.Resources.NotFoundException notFoundException) {
          String str = TAG;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Could not resolve drawable resource id ");
          stringBuilder.append(bool);
          Slog.e(str, stringBuilder.toString());
        }  
    } 
    return null;
  }
  
  private String resolveTitle() {
    boolean bool;
    SuspendDialogInfo suspendDialogInfo = this.mSuppliedDialogInfo;
    if (suspendDialogInfo != null) {
      bool = suspendDialogInfo.getTitleResId();
    } else {
      bool = false;
    } 
    if (bool) {
      Resources resources = this.mSuspendingAppResources;
      if (resources != null)
        try {
          return resources.getString(bool);
        } catch (android.content.res.Resources.NotFoundException notFoundException) {
          String str = TAG;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Could not resolve string resource id ");
          stringBuilder.append(bool);
          Slog.e(str, stringBuilder.toString());
        }  
    } 
    return getString(17039664);
  }
  
  private String resolveDialogMessage() {
    // Byte code:
    //   0: aload_0
    //   1: aload_0
    //   2: getfield mSuspendedPackage : Ljava/lang/String;
    //   5: invokespecial getAppLabel : (Ljava/lang/String;)Ljava/lang/CharSequence;
    //   8: astore_1
    //   9: aload_0
    //   10: getfield mSuppliedDialogInfo : Landroid/content/pm/SuspendDialogInfo;
    //   13: astore_2
    //   14: aload_2
    //   15: ifnull -> 134
    //   18: aload_2
    //   19: invokevirtual getDialogMessageResId : ()I
    //   22: istore_3
    //   23: aload_0
    //   24: getfield mSuppliedDialogInfo : Landroid/content/pm/SuspendDialogInfo;
    //   27: invokevirtual getDialogMessage : ()Ljava/lang/String;
    //   30: astore_2
    //   31: iload_3
    //   32: ifeq -> 103
    //   35: aload_0
    //   36: getfield mSuspendingAppResources : Landroid/content/res/Resources;
    //   39: astore #4
    //   41: aload #4
    //   43: ifnull -> 103
    //   46: aload #4
    //   48: iload_3
    //   49: iconst_1
    //   50: anewarray java/lang/Object
    //   53: dup
    //   54: iconst_0
    //   55: aload_1
    //   56: aastore
    //   57: invokevirtual getString : (I[Ljava/lang/Object;)Ljava/lang/String;
    //   60: astore_2
    //   61: aload_2
    //   62: areturn
    //   63: astore_2
    //   64: getstatic com/android/internal/app/SuspendedAppActivity.TAG : Ljava/lang/String;
    //   67: astore #4
    //   69: new java/lang/StringBuilder
    //   72: dup
    //   73: invokespecial <init> : ()V
    //   76: astore_2
    //   77: aload_2
    //   78: ldc 'Could not resolve string resource id '
    //   80: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   83: pop
    //   84: aload_2
    //   85: iload_3
    //   86: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   89: pop
    //   90: aload #4
    //   92: aload_2
    //   93: invokevirtual toString : ()Ljava/lang/String;
    //   96: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   99: pop
    //   100: goto -> 134
    //   103: aload_2
    //   104: ifnull -> 134
    //   107: aload_0
    //   108: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   111: invokevirtual getConfiguration : ()Landroid/content/res/Configuration;
    //   114: invokevirtual getLocales : ()Landroid/os/LocaleList;
    //   117: iconst_0
    //   118: invokevirtual get : (I)Ljava/util/Locale;
    //   121: aload_2
    //   122: iconst_1
    //   123: anewarray java/lang/Object
    //   126: dup
    //   127: iconst_0
    //   128: aload_1
    //   129: aastore
    //   130: invokestatic format : (Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   133: areturn
    //   134: aload_0
    //   135: getfield mSuspendingPackage : Ljava/lang/String;
    //   138: astore_2
    //   139: aload_0
    //   140: aload_2
    //   141: invokespecial getAppLabel : (Ljava/lang/String;)Ljava/lang/CharSequence;
    //   144: astore_2
    //   145: aload_0
    //   146: ldc 17039662
    //   148: iconst_2
    //   149: anewarray java/lang/Object
    //   152: dup
    //   153: iconst_0
    //   154: aload_1
    //   155: aastore
    //   156: dup
    //   157: iconst_1
    //   158: aload_2
    //   159: aastore
    //   160: invokevirtual getString : (I[Ljava/lang/Object;)Ljava/lang/String;
    //   163: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #122	-> 0
    //   #123	-> 9
    //   #124	-> 18
    //   #125	-> 23
    //   #126	-> 31
    //   #128	-> 46
    //   #129	-> 63
    //   #130	-> 64
    //   #131	-> 100
    //   #132	-> 103
    //   #133	-> 107
    //   #137	-> 134
    //   #138	-> 139
    //   #137	-> 145
    // Exception table:
    //   from	to	target	type
    //   46	61	63	android/content/res/Resources$NotFoundException
  }
  
  private String resolveNeutralButtonText() {
    boolean bool;
    int i = this.mNeutralButtonAction;
    if (i != 0) {
      if (i != 1) {
        String str = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown neutral button action: ");
        stringBuilder.append(this.mNeutralButtonAction);
        Slog.w(str, stringBuilder.toString());
        return null;
      } 
      i = 17039665;
    } else {
      if (this.mMoreDetailsIntent == null)
        return null; 
      i = 17039663;
    } 
    SuspendDialogInfo suspendDialogInfo = this.mSuppliedDialogInfo;
    if (suspendDialogInfo != null) {
      bool = suspendDialogInfo.getNeutralButtonTextResId();
    } else {
      bool = false;
    } 
    if (bool) {
      Resources resources = this.mSuspendingAppResources;
      if (resources != null)
        try {
          return resources.getString(bool);
        } catch (android.content.res.Resources.NotFoundException notFoundException) {
          String str = TAG;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Could not resolve string resource id ");
          stringBuilder.append(bool);
          Slog.e(str, stringBuilder.toString());
        }  
    } 
    return getString(i);
  }
  
  public void onCreate(Bundle paramBundle) {
    StringBuilder stringBuilder;
    super.onCreate(paramBundle);
    this.mPm = getPackageManager();
    getWindow().setType(2008);
    Intent intent = getIntent();
    this.mOptions = intent.getBundleExtra("com.android.internal.app.extra.ACTIVITY_OPTIONS");
    int i = intent.getIntExtra("android.intent.extra.USER_ID", -1);
    if (i < 0) {
      String str = TAG;
      stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid user: ");
      stringBuilder.append(this.mUserId);
      Slog.wtf(str, stringBuilder.toString());
      finish();
      return;
    } 
    this.mSuspendedPackage = stringBuilder.getStringExtra("com.android.internal.app.extra.SUSPENDED_PACKAGE");
    this.mSuspendingPackage = stringBuilder.getStringExtra("com.android.internal.app.extra.SUSPENDING_PACKAGE");
    this.mSuppliedDialogInfo = (SuspendDialogInfo)stringBuilder.getParcelableExtra("com.android.internal.app.extra.DIALOG_INFO");
    this.mOnUnsuspend = (IntentSender)stringBuilder.getParcelableExtra("com.android.internal.app.extra.UNSUSPEND_INTENT");
    if (this.mSuppliedDialogInfo != null)
      try {
        this.mSuspendingAppResources = this.mPm.getResourcesForApplicationAsUser(this.mSuspendingPackage, this.mUserId);
      } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
        String str = TAG;
        stringBuilder = new StringBuilder();
        stringBuilder.append("Could not find resources for ");
        stringBuilder.append(this.mSuspendingPackage);
        Slog.e(str, stringBuilder.toString(), (Throwable)nameNotFoundException);
      }  
    SuspendDialogInfo suspendDialogInfo = this.mSuppliedDialogInfo;
    if (suspendDialogInfo != null) {
      i = suspendDialogInfo.getNeutralButtonAction();
    } else {
      i = 0;
    } 
    this.mNeutralButtonAction = i;
    if (i == 0) {
      Intent intent1 = getMoreDetailsActivity();
    } else {
      suspendDialogInfo = null;
    } 
    this.mMoreDetailsIntent = (Intent)suspendDialogInfo;
    AlertController.AlertParams alertParams = this.mAlertParams;
    alertParams.mIcon = resolveIcon();
    alertParams.mTitle = resolveTitle();
    alertParams.mMessage = resolveDialogMessage();
    alertParams.mPositiveButtonText = getString(17039370);
    alertParams.mNeutralButtonText = resolveNeutralButtonText();
    alertParams.mNeutralButtonListener = this;
    alertParams.mPositiveButtonListener = this;
    requestDismissKeyguardIfNeeded(alertParams.mMessage);
    setupAlert();
  }
  
  private void requestDismissKeyguardIfNeeded(CharSequence paramCharSequence) {
    KeyguardManager keyguardManager = (KeyguardManager)getSystemService(KeyguardManager.class);
    if (keyguardManager.isKeyguardLocked())
      keyguardManager.requestDismissKeyguard(this, paramCharSequence, (KeyguardManager.KeyguardDismissCallback)new Object(this)); 
  }
  
  public void onClick(DialogInterface paramDialogInterface, int paramInt) {
    if (paramInt == -3) {
      paramInt = this.mNeutralButtonAction;
      if (paramInt != 0) {
        if (paramInt != 1) {
          String str = TAG;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Unexpected action on neutral button: ");
          stringBuilder.append(this.mNeutralButtonAction);
          Slog.e(str, stringBuilder.toString());
        } else {
          IPackageManager iPackageManager = AppGlobals.getPackageManager();
          try {
            String str1 = this.mSuspendedPackage, str2 = this.mSuspendingPackage;
            paramInt = this.mUserId;
            String[] arrayOfString = iPackageManager.setPackagesSuspendedAsUser(new String[] { str1 }, false, null, null, null, str2, paramInt);
            if (ArrayUtils.contains((Object[])arrayOfString, this.mSuspendedPackage)) {
              String str = TAG;
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("Could not unsuspend ");
              stringBuilder.append(this.mSuspendedPackage);
              Slog.e(str, stringBuilder.toString());
            } else {
              Intent intent2 = new Intent();
              Intent intent3 = intent2.setAction("android.intent.action.PACKAGE_UNSUSPENDED_MANUALLY");
              String str3 = this.mSuspendedPackage;
              Intent intent1 = intent3.putExtra("android.intent.extra.PACKAGE_NAME", str3);
              String str4 = this.mSuspendingPackage;
              intent1 = intent1.setPackage(str4);
              intent1 = intent1.addFlags(16777216);
              sendBroadcastAsUser(intent1, UserHandle.of(this.mUserId));
              IntentSender intentSender = this.mOnUnsuspend;
              if (intentSender != null)
                try {
                  intentSender.sendIntent((Context)this, 0, null, null, null);
                } catch (android.content.IntentSender.SendIntentException sendIntentException) {
                  str2 = TAG;
                  StringBuilder stringBuilder = new StringBuilder();
                  stringBuilder.append("Error while starting intent ");
                  stringBuilder.append(this.mOnUnsuspend);
                  Slog.e(str2, stringBuilder.toString(), (Throwable)sendIntentException);
                }  
            } 
          } catch (RemoteException remoteException) {
            Slog.e(TAG, "Can't talk to system process", (Throwable)remoteException);
          } 
        } 
      } else {
        Intent intent = this.mMoreDetailsIntent;
        if (intent != null) {
          Bundle bundle = this.mOptions;
          paramInt = this.mUserId;
          UserHandle userHandle = UserHandle.of(paramInt);
          startActivityAsUser(intent, bundle, userHandle);
        } else {
          Slog.wtf(TAG, "Neutral button should not have existed!");
        } 
      } 
    } 
    finish();
  }
  
  public static Intent createSuspendedAppInterceptIntent(String paramString1, String paramString2, SuspendDialogInfo paramSuspendDialogInfo, Bundle paramBundle, IntentSender paramIntentSender, int paramInt) {
    Intent intent = new Intent();
    intent = intent.setClassName("android", SuspendedAppActivity.class.getName());
    null = intent.putExtra("com.android.internal.app.extra.SUSPENDED_PACKAGE", paramString1);
    null = null.putExtra("com.android.internal.app.extra.DIALOG_INFO", (Parcelable)paramSuspendDialogInfo);
    null = null.putExtra("com.android.internal.app.extra.SUSPENDING_PACKAGE", paramString2);
    null = null.putExtra("com.android.internal.app.extra.UNSUSPEND_INTENT", (Parcelable)paramIntentSender);
    null = null.putExtra("com.android.internal.app.extra.ACTIVITY_OPTIONS", paramBundle);
    null = null.putExtra("android.intent.extra.USER_ID", paramInt);
    return null.setFlags(276824064);
  }
}
