package com.android.internal.app;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.Property;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.SpinnerAdapter;
import android.widget.Toolbar;
import com.android.internal.R;
import com.android.internal.view.ActionBarPolicy;
import com.android.internal.view.menu.MenuBuilder;
import com.android.internal.view.menu.MenuPopupHelper;
import com.android.internal.view.menu.SubMenuBuilder;
import com.android.internal.widget.ActionBarContainer;
import com.android.internal.widget.ActionBarContextView;
import com.android.internal.widget.ActionBarOverlayLayout;
import com.android.internal.widget.DecorToolbar;
import com.android.internal.widget.ScrollingTabContainerView;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class WindowDecorActionBar extends ActionBar implements ActionBarOverlayLayout.ActionBarVisibilityCallback {
  private ArrayList<TabImpl> mTabs = new ArrayList<>();
  
  private int mSavedTabPosition = -1;
  
  private ArrayList<ActionBar.OnMenuVisibilityListener> mMenuVisibilityListeners = new ArrayList<>();
  
  private int mCurWindowVisibility = 0;
  
  private boolean mContentAnimations = true;
  
  private boolean mNowShowing = true;
  
  final Animator.AnimatorListener mHideListener = (Animator.AnimatorListener)new Object(this);
  
  final Animator.AnimatorListener mShowListener = (Animator.AnimatorListener)new Object(this);
  
  final ValueAnimator.AnimatorUpdateListener mUpdateListener = new ValueAnimator.AnimatorUpdateListener() {
      final WindowDecorActionBar this$0;
      
      public void onAnimationUpdate(ValueAnimator param1ValueAnimator) {
        ViewParent viewParent = WindowDecorActionBar.this.mContainerView.getParent();
        ((View)viewParent).invalidate();
      }
    };
  
  static final boolean $assertionsDisabled = false;
  
  private static final int CONTEXT_DISPLAY_NORMAL = 0;
  
  private static final int CONTEXT_DISPLAY_SPLIT = 1;
  
  private static final long FADE_IN_DURATION_MS = 200L;
  
  private static final long FADE_OUT_DURATION_MS = 100L;
  
  private static final int INVALID_POSITION = -1;
  
  private static final String TAG = "WindowDecorActionBar";
  
  ActionMode mActionMode;
  
  private Activity mActivity;
  
  private ActionBarContainer mContainerView;
  
  private View mContentView;
  
  private Context mContext;
  
  private int mContextDisplayMode;
  
  private ActionBarContextView mContextView;
  
  private Animator mCurrentShowAnim;
  
  private DecorToolbar mDecorToolbar;
  
  ActionMode mDeferredDestroyActionMode;
  
  ActionMode.Callback mDeferredModeDestroyCallback;
  
  private Dialog mDialog;
  
  private boolean mDisplayHomeAsUpSet;
  
  private boolean mHasEmbeddedTabs;
  
  private boolean mHiddenByApp;
  
  private boolean mHiddenBySystem;
  
  boolean mHideOnContentScroll;
  
  private boolean mLastMenuVisibility;
  
  private ActionBarOverlayLayout mOverlayLayout;
  
  private TabImpl mSelectedTab;
  
  private boolean mShowHideAnimationEnabled;
  
  private boolean mShowingForMode;
  
  private ActionBarContainer mSplitView;
  
  private ScrollingTabContainerView mTabScrollView;
  
  private Context mThemedContext;
  
  public WindowDecorActionBar(Activity paramActivity) {
    this.mActivity = paramActivity;
    Window window = paramActivity.getWindow();
    View view = window.getDecorView();
    boolean bool = this.mActivity.getWindow().hasFeature(9);
    init(view);
    if (!bool)
      this.mContentView = view.findViewById(16908290); 
  }
  
  public WindowDecorActionBar(Dialog paramDialog) {
    this.mDialog = paramDialog;
    init(paramDialog.getWindow().getDecorView());
  }
  
  public WindowDecorActionBar(View paramView) {
    init(paramView);
  }
  
  private void init(View paramView) {
    ActionBarOverlayLayout actionBarOverlayLayout = paramView.<ActionBarOverlayLayout>findViewById(16908917);
    if (actionBarOverlayLayout != null)
      actionBarOverlayLayout.setActionBarVisibilityCallback(this); 
    this.mDecorToolbar = getDecorToolbar(paramView.findViewById(16908709));
    this.mContextView = paramView.<ActionBarContextView>findViewById(16908714);
    this.mContainerView = paramView.<ActionBarContainer>findViewById(16908710);
    this.mSplitView = paramView.<ActionBarContainer>findViewById(16909466);
    DecorToolbar decorToolbar = this.mDecorToolbar;
    if (decorToolbar != null && this.mContextView != null && this.mContainerView != null) {
      boolean bool;
      this.mContext = decorToolbar.getContext();
      if (this.mDecorToolbar.isSplit()) {
        i = 1;
      } else {
        i = 0;
      } 
      this.mContextDisplayMode = i;
      int i = this.mDecorToolbar.getDisplayOptions();
      if ((i & 0x4) != 0) {
        i = 1;
      } else {
        i = 0;
      } 
      if (i != 0)
        this.mDisplayHomeAsUpSet = true; 
      ActionBarPolicy actionBarPolicy = ActionBarPolicy.get(this.mContext);
      if (actionBarPolicy.enableHomeButtonByDefault() || i != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      setHomeButtonEnabled(bool);
      setHasEmbeddedTabs(actionBarPolicy.hasEmbeddedTabs());
      TypedArray typedArray = this.mContext.obtainStyledAttributes(null, R.styleable.ActionBar, 16843470, 0);
      if (typedArray.getBoolean(21, false))
        setHideOnContentScrollEnabled(true); 
      i = typedArray.getDimensionPixelSize(20, 0);
      if (i != 0)
        setElevation(i); 
      typedArray.recycle();
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(getClass().getSimpleName());
    stringBuilder.append(" can only be used with a compatible window decor layout");
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  private DecorToolbar getDecorToolbar(View paramView) {
    if (paramView instanceof DecorToolbar)
      return (DecorToolbar)paramView; 
    if (paramView instanceof Toolbar)
      return ((Toolbar)paramView).getWrapper(); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Can't make a decor toolbar out of ");
    stringBuilder.append(paramView.getClass().getSimpleName());
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public void setElevation(float paramFloat) {
    this.mContainerView.setElevation(paramFloat);
    ActionBarContainer actionBarContainer = this.mSplitView;
    if (actionBarContainer != null)
      actionBarContainer.setElevation(paramFloat); 
  }
  
  public float getElevation() {
    return this.mContainerView.getElevation();
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration) {
    setHasEmbeddedTabs(ActionBarPolicy.get(this.mContext).hasEmbeddedTabs());
  }
  
  private void setHasEmbeddedTabs(boolean paramBoolean) {
    this.mHasEmbeddedTabs = paramBoolean;
    if (!paramBoolean) {
      this.mDecorToolbar.setEmbeddedTabView(null);
      this.mContainerView.setTabContainer(this.mTabScrollView);
    } else {
      this.mContainerView.setTabContainer(null);
      this.mDecorToolbar.setEmbeddedTabView(this.mTabScrollView);
    } 
    int i = getNavigationMode();
    boolean bool = true;
    if (i == 2) {
      i = 1;
    } else {
      i = 0;
    } 
    ScrollingTabContainerView scrollingTabContainerView = this.mTabScrollView;
    if (scrollingTabContainerView != null) {
      ActionBarOverlayLayout actionBarOverlayLayout1;
      if (i != 0) {
        scrollingTabContainerView.setVisibility(0);
        actionBarOverlayLayout1 = this.mOverlayLayout;
        if (actionBarOverlayLayout1 != null)
          actionBarOverlayLayout1.requestApplyInsets(); 
      } else {
        actionBarOverlayLayout1.setVisibility(8);
      } 
    } 
    DecorToolbar decorToolbar = this.mDecorToolbar;
    if (!this.mHasEmbeddedTabs && i != 0) {
      paramBoolean = true;
    } else {
      paramBoolean = false;
    } 
    decorToolbar.setCollapsible(paramBoolean);
    ActionBarOverlayLayout actionBarOverlayLayout = this.mOverlayLayout;
    if (!this.mHasEmbeddedTabs && i != 0) {
      paramBoolean = bool;
    } else {
      paramBoolean = false;
    } 
    actionBarOverlayLayout.setHasNonEmbeddedTabs(paramBoolean);
  }
  
  private void ensureTabsExist() {
    if (this.mTabScrollView != null)
      return; 
    ScrollingTabContainerView scrollingTabContainerView = new ScrollingTabContainerView(this.mContext);
    if (this.mHasEmbeddedTabs) {
      scrollingTabContainerView.setVisibility(0);
      this.mDecorToolbar.setEmbeddedTabView(scrollingTabContainerView);
    } else {
      if (getNavigationMode() == 2) {
        scrollingTabContainerView.setVisibility(0);
        ActionBarOverlayLayout actionBarOverlayLayout = this.mOverlayLayout;
        if (actionBarOverlayLayout != null)
          actionBarOverlayLayout.requestApplyInsets(); 
      } else {
        scrollingTabContainerView.setVisibility(8);
      } 
      this.mContainerView.setTabContainer(scrollingTabContainerView);
    } 
    this.mTabScrollView = scrollingTabContainerView;
  }
  
  void completeDeferredDestroyActionMode() {
    ActionMode.Callback callback = this.mDeferredModeDestroyCallback;
    if (callback != null) {
      callback.onDestroyActionMode(this.mDeferredDestroyActionMode);
      this.mDeferredDestroyActionMode = null;
      this.mDeferredModeDestroyCallback = null;
    } 
  }
  
  public void onWindowVisibilityChanged(int paramInt) {
    this.mCurWindowVisibility = paramInt;
  }
  
  public void setShowHideAnimationEnabled(boolean paramBoolean) {
    this.mShowHideAnimationEnabled = paramBoolean;
    if (!paramBoolean) {
      Animator animator = this.mCurrentShowAnim;
      if (animator != null)
        animator.end(); 
    } 
  }
  
  public void addOnMenuVisibilityListener(ActionBar.OnMenuVisibilityListener paramOnMenuVisibilityListener) {
    this.mMenuVisibilityListeners.add(paramOnMenuVisibilityListener);
  }
  
  public void removeOnMenuVisibilityListener(ActionBar.OnMenuVisibilityListener paramOnMenuVisibilityListener) {
    this.mMenuVisibilityListeners.remove(paramOnMenuVisibilityListener);
  }
  
  public void dispatchMenuVisibilityChanged(boolean paramBoolean) {
    if (paramBoolean == this.mLastMenuVisibility)
      return; 
    this.mLastMenuVisibility = paramBoolean;
    int i = this.mMenuVisibilityListeners.size();
    for (byte b = 0; b < i; b++)
      ((ActionBar.OnMenuVisibilityListener)this.mMenuVisibilityListeners.get(b)).onMenuVisibilityChanged(paramBoolean); 
  }
  
  public void setCustomView(int paramInt) {
    LayoutInflater layoutInflater = LayoutInflater.from(getThemedContext());
    DecorToolbar decorToolbar = this.mDecorToolbar;
    ViewGroup viewGroup = decorToolbar.getViewGroup();
    setCustomView(layoutInflater.inflate(paramInt, viewGroup, false));
  }
  
  public void setDisplayUseLogoEnabled(boolean paramBoolean) {
    setDisplayOptions(paramBoolean, 1);
  }
  
  public void setDisplayShowHomeEnabled(boolean paramBoolean) {
    boolean bool;
    if (paramBoolean) {
      bool = true;
    } else {
      bool = false;
    } 
    setDisplayOptions(bool, 2);
  }
  
  public void setDisplayHomeAsUpEnabled(boolean paramBoolean) {
    boolean bool;
    if (paramBoolean) {
      bool = true;
    } else {
      bool = false;
    } 
    setDisplayOptions(bool, 4);
  }
  
  public void setDisplayShowTitleEnabled(boolean paramBoolean) {
    boolean bool;
    if (paramBoolean) {
      bool = true;
    } else {
      bool = false;
    } 
    setDisplayOptions(bool, 8);
  }
  
  public void setDisplayShowCustomEnabled(boolean paramBoolean) {
    boolean bool;
    if (paramBoolean) {
      bool = true;
    } else {
      bool = false;
    } 
    setDisplayOptions(bool, 16);
  }
  
  public void setHomeButtonEnabled(boolean paramBoolean) {
    this.mDecorToolbar.setHomeButtonEnabled(paramBoolean);
  }
  
  public void setTitle(int paramInt) {
    setTitle(this.mContext.getString(paramInt));
  }
  
  public void setSubtitle(int paramInt) {
    setSubtitle(this.mContext.getString(paramInt));
  }
  
  public void setSelectedNavigationItem(int paramInt) {
    int i = this.mDecorToolbar.getNavigationMode();
    if (i != 1) {
      if (i == 2) {
        selectTab(this.mTabs.get(paramInt));
      } else {
        throw new IllegalStateException("setSelectedNavigationIndex not valid for current navigation mode");
      } 
    } else {
      this.mDecorToolbar.setDropdownSelectedPosition(paramInt);
    } 
  }
  
  public void removeAllTabs() {
    cleanupTabs();
  }
  
  private void cleanupTabs() {
    if (this.mSelectedTab != null)
      selectTab(null); 
    this.mTabs.clear();
    ScrollingTabContainerView scrollingTabContainerView = this.mTabScrollView;
    if (scrollingTabContainerView != null)
      scrollingTabContainerView.removeAllTabs(); 
    this.mSavedTabPosition = -1;
  }
  
  public void setTitle(CharSequence paramCharSequence) {
    this.mDecorToolbar.setTitle(paramCharSequence);
  }
  
  public void setWindowTitle(CharSequence paramCharSequence) {
    this.mDecorToolbar.setWindowTitle(paramCharSequence);
  }
  
  public void setSubtitle(CharSequence paramCharSequence) {
    this.mDecorToolbar.setSubtitle(paramCharSequence);
  }
  
  public void setDisplayOptions(int paramInt) {
    if ((paramInt & 0x4) != 0)
      this.mDisplayHomeAsUpSet = true; 
    this.mDecorToolbar.setDisplayOptions(paramInt);
  }
  
  public void setDisplayOptions(int paramInt1, int paramInt2) {
    int i = this.mDecorToolbar.getDisplayOptions();
    if ((paramInt2 & 0x4) != 0)
      this.mDisplayHomeAsUpSet = true; 
    this.mDecorToolbar.setDisplayOptions(paramInt1 & paramInt2 | (paramInt2 ^ 0xFFFFFFFF) & i);
  }
  
  public void setBackgroundDrawable(Drawable paramDrawable) {
    this.mContainerView.setPrimaryBackground(paramDrawable);
  }
  
  public void setStackedBackgroundDrawable(Drawable paramDrawable) {
    this.mContainerView.setStackedBackground(paramDrawable);
  }
  
  public void setSplitBackgroundDrawable(Drawable paramDrawable) {
    ActionBarContainer actionBarContainer = this.mSplitView;
    if (actionBarContainer != null)
      actionBarContainer.setSplitBackground(paramDrawable); 
  }
  
  public View getCustomView() {
    return this.mDecorToolbar.getCustomView();
  }
  
  public CharSequence getTitle() {
    return this.mDecorToolbar.getTitle();
  }
  
  public CharSequence getSubtitle() {
    return this.mDecorToolbar.getSubtitle();
  }
  
  public int getNavigationMode() {
    return this.mDecorToolbar.getNavigationMode();
  }
  
  public int getDisplayOptions() {
    return this.mDecorToolbar.getDisplayOptions();
  }
  
  public ActionMode startActionMode(ActionMode.Callback paramCallback) {
    ActionMode actionMode = this.mActionMode;
    if (actionMode != null)
      actionMode.finish(); 
    this.mOverlayLayout.setHideOnContentScrollEnabled(false);
    this.mContextView.killMode();
    ActionModeImpl actionModeImpl = new ActionModeImpl(this.mContextView.getContext(), paramCallback);
    if (actionModeImpl.dispatchOnCreate()) {
      this.mActionMode = actionModeImpl;
      actionModeImpl.invalidate();
      this.mContextView.initForMode(actionModeImpl);
      animateToMode(true);
      ActionBarContainer actionBarContainer = this.mSplitView;
      if (actionBarContainer != null && this.mContextDisplayMode == 1)
        if (actionBarContainer.getVisibility() != 0) {
          this.mSplitView.setVisibility(0);
          ActionBarOverlayLayout actionBarOverlayLayout = this.mOverlayLayout;
          if (actionBarOverlayLayout != null)
            actionBarOverlayLayout.requestApplyInsets(); 
        }  
      this.mContextView.sendAccessibilityEvent(32);
      return actionModeImpl;
    } 
    return null;
  }
  
  private void configureTab(ActionBar.Tab paramTab, int paramInt) {
    TabImpl tabImpl = (TabImpl)paramTab;
    ActionBar.TabListener tabListener = tabImpl.getCallback();
    if (tabListener != null) {
      tabImpl.setPosition(paramInt);
      this.mTabs.add(paramInt, tabImpl);
      int i = this.mTabs.size();
      for (; ++paramInt < i; paramInt++)
        ((TabImpl)this.mTabs.get(paramInt)).setPosition(paramInt); 
      return;
    } 
    throw new IllegalStateException("Action Bar Tab must have a Callback");
  }
  
  public void addTab(ActionBar.Tab paramTab) {
    addTab(paramTab, this.mTabs.isEmpty());
  }
  
  public void addTab(ActionBar.Tab paramTab, int paramInt) {
    addTab(paramTab, paramInt, this.mTabs.isEmpty());
  }
  
  public void addTab(ActionBar.Tab paramTab, boolean paramBoolean) {
    ensureTabsExist();
    this.mTabScrollView.addTab(paramTab, paramBoolean);
    configureTab(paramTab, this.mTabs.size());
    if (paramBoolean)
      selectTab(paramTab); 
  }
  
  public void addTab(ActionBar.Tab paramTab, int paramInt, boolean paramBoolean) {
    ensureTabsExist();
    this.mTabScrollView.addTab(paramTab, paramInt, paramBoolean);
    configureTab(paramTab, paramInt);
    if (paramBoolean)
      selectTab(paramTab); 
  }
  
  public ActionBar.Tab newTab() {
    return new TabImpl();
  }
  
  public void removeTab(ActionBar.Tab paramTab) {
    removeTabAt(paramTab.getPosition());
  }
  
  public void removeTabAt(int paramInt) {
    int i;
    if (this.mTabScrollView == null)
      return; 
    TabImpl tabImpl = this.mSelectedTab;
    if (tabImpl != null) {
      i = tabImpl.getPosition();
    } else {
      i = this.mSavedTabPosition;
    } 
    this.mTabScrollView.removeTabAt(paramInt);
    tabImpl = this.mTabs.remove(paramInt);
    if (tabImpl != null)
      tabImpl.setPosition(-1); 
    int j = this.mTabs.size();
    for (int k = paramInt; k < j; k++)
      ((TabImpl)this.mTabs.get(k)).setPosition(k); 
    if (i == paramInt) {
      ActionBar.Tab tab;
      if (this.mTabs.isEmpty()) {
        tabImpl = null;
      } else {
        tab = this.mTabs.get(Math.max(0, paramInt - 1));
      } 
      selectTab(tab);
    } 
  }
  
  public void selectTab(ActionBar.Tab paramTab) {
    FragmentTransaction fragmentTransaction;
    int i = getNavigationMode(), j = -1;
    if (i != 2) {
      if (paramTab != null)
        j = paramTab.getPosition(); 
      this.mSavedTabPosition = j;
      return;
    } 
    if (this.mDecorToolbar.getViewGroup().isInEditMode()) {
      fragmentTransaction = null;
    } else {
      fragmentTransaction = this.mActivity.getFragmentManager().beginTransaction().disallowAddToBackStack();
    } 
    TabImpl tabImpl = this.mSelectedTab;
    if (tabImpl == paramTab) {
      if (tabImpl != null) {
        tabImpl.getCallback().onTabReselected(this.mSelectedTab, fragmentTransaction);
        this.mTabScrollView.animateToTab(paramTab.getPosition());
      } 
    } else {
      ScrollingTabContainerView scrollingTabContainerView = this.mTabScrollView;
      if (paramTab != null)
        j = paramTab.getPosition(); 
      scrollingTabContainerView.setTabSelected(j);
      TabImpl tabImpl1 = this.mSelectedTab;
      if (tabImpl1 != null)
        tabImpl1.getCallback().onTabUnselected(this.mSelectedTab, fragmentTransaction); 
      this.mSelectedTab = (TabImpl)(paramTab = paramTab);
      if (paramTab != null)
        paramTab.getCallback().onTabSelected(this.mSelectedTab, fragmentTransaction); 
    } 
    if (fragmentTransaction != null && !fragmentTransaction.isEmpty())
      fragmentTransaction.commit(); 
  }
  
  public ActionBar.Tab getSelectedTab() {
    return this.mSelectedTab;
  }
  
  public int getHeight() {
    return this.mContainerView.getHeight();
  }
  
  public void enableContentAnimations(boolean paramBoolean) {
    this.mContentAnimations = paramBoolean;
  }
  
  public void show() {
    if (this.mHiddenByApp) {
      this.mHiddenByApp = false;
      updateVisibility(false);
    } 
  }
  
  private void showForActionMode() {
    if (!this.mShowingForMode) {
      this.mShowingForMode = true;
      ActionBarOverlayLayout actionBarOverlayLayout = this.mOverlayLayout;
      if (actionBarOverlayLayout != null)
        actionBarOverlayLayout.setShowingForActionMode(true); 
      updateVisibility(false);
    } 
  }
  
  public void showForSystem() {
    if (this.mHiddenBySystem) {
      this.mHiddenBySystem = false;
      updateVisibility(true);
    } 
  }
  
  public void hide() {
    if (!this.mHiddenByApp) {
      this.mHiddenByApp = true;
      updateVisibility(false);
    } 
  }
  
  private void hideForActionMode() {
    if (this.mShowingForMode) {
      this.mShowingForMode = false;
      ActionBarOverlayLayout actionBarOverlayLayout = this.mOverlayLayout;
      if (actionBarOverlayLayout != null)
        actionBarOverlayLayout.setShowingForActionMode(false); 
      updateVisibility(false);
    } 
  }
  
  public void hideForSystem() {
    if (!this.mHiddenBySystem) {
      this.mHiddenBySystem = true;
      updateVisibility(true);
    } 
  }
  
  public void setHideOnContentScrollEnabled(boolean paramBoolean) {
    if (!paramBoolean || this.mOverlayLayout.isInOverlayMode()) {
      this.mHideOnContentScroll = paramBoolean;
      this.mOverlayLayout.setHideOnContentScrollEnabled(paramBoolean);
      return;
    } 
    throw new IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll");
  }
  
  public boolean isHideOnContentScrollEnabled() {
    return this.mOverlayLayout.isHideOnContentScrollEnabled();
  }
  
  public int getHideOffset() {
    return this.mOverlayLayout.getActionBarHideOffset();
  }
  
  public void setHideOffset(int paramInt) {
    if (paramInt == 0 || this.mOverlayLayout.isInOverlayMode()) {
      this.mOverlayLayout.setActionBarHideOffset(paramInt);
      return;
    } 
    throw new IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to set a non-zero hide offset");
  }
  
  private static boolean checkShowingFlags(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3) {
    if (paramBoolean3)
      return true; 
    if (paramBoolean1 || paramBoolean2)
      return false; 
    return true;
  }
  
  private void updateVisibility(boolean paramBoolean) {
    boolean bool = checkShowingFlags(this.mHiddenByApp, this.mHiddenBySystem, this.mShowingForMode);
    if (bool) {
      if (!this.mNowShowing) {
        this.mNowShowing = true;
        doShow(paramBoolean);
      } 
    } else if (this.mNowShowing) {
      this.mNowShowing = false;
      doHide(paramBoolean);
    } 
  }
  
  public void doShow(boolean paramBoolean) {
    Animator animator = this.mCurrentShowAnim;
    if (animator != null)
      animator.end(); 
    this.mContainerView.setVisibility(0);
    if (this.mCurWindowVisibility == 0 && (this.mShowHideAnimationEnabled || paramBoolean)) {
      this.mContainerView.setTranslationY(0.0F);
      float f1 = -this.mContainerView.getHeight();
      float f2 = f1;
      if (paramBoolean) {
        int[] arrayOfInt = new int[2];
        arrayOfInt[0] = 0;
        arrayOfInt[1] = 0;
        this.mContainerView.getLocationInWindow(arrayOfInt);
        f2 = f1 - arrayOfInt[1];
      } 
      this.mContainerView.setTranslationY(f2);
      AnimatorSet animatorSet = new AnimatorSet();
      ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(this.mContainerView, View.TRANSLATION_Y, new float[] { 0.0F });
      objectAnimator.addUpdateListener(this.mUpdateListener);
      AnimatorSet.Builder builder = animatorSet.play((Animator)objectAnimator);
      if (this.mContentAnimations) {
        View view = this.mContentView;
        if (view != null)
          builder.with((Animator)ObjectAnimator.ofFloat(view, View.TRANSLATION_Y, new float[] { f2, 0.0F })); 
      } 
      ActionBarContainer actionBarContainer = this.mSplitView;
      if (actionBarContainer != null && this.mContextDisplayMode == 1) {
        actionBarContainer.setTranslationY(actionBarContainer.getHeight());
        this.mSplitView.setVisibility(0);
        builder.with((Animator)ObjectAnimator.ofFloat(this.mSplitView, View.TRANSLATION_Y, new float[] { 0.0F }));
      } 
      animatorSet.setInterpolator(AnimationUtils.loadInterpolator(this.mContext, 17563651));
      animatorSet.setDuration(250L);
      animatorSet.addListener(this.mShowListener);
      this.mCurrentShowAnim = (Animator)animatorSet;
      animatorSet.start();
    } else {
      this.mContainerView.setAlpha(1.0F);
      this.mContainerView.setTranslationY(0.0F);
      if (this.mContentAnimations) {
        View view = this.mContentView;
        if (view != null)
          view.setTranslationY(0.0F); 
      } 
      ActionBarContainer actionBarContainer = this.mSplitView;
      if (actionBarContainer != null && this.mContextDisplayMode == 1) {
        actionBarContainer.setAlpha(1.0F);
        this.mSplitView.setTranslationY(0.0F);
        this.mSplitView.setVisibility(0);
      } 
      this.mShowListener.onAnimationEnd(null);
    } 
    ActionBarOverlayLayout actionBarOverlayLayout = this.mOverlayLayout;
    if (actionBarOverlayLayout != null)
      actionBarOverlayLayout.requestApplyInsets(); 
  }
  
  public void doHide(boolean paramBoolean) {
    Animator animator = this.mCurrentShowAnim;
    if (animator != null)
      animator.end(); 
    if (this.mCurWindowVisibility == 0 && (this.mShowHideAnimationEnabled || paramBoolean)) {
      this.mContainerView.setAlpha(1.0F);
      this.mContainerView.setTransitioning(true);
      AnimatorSet animatorSet = new AnimatorSet();
      float f1 = -this.mContainerView.getHeight();
      float f2 = f1;
      if (paramBoolean) {
        int[] arrayOfInt = new int[2];
        arrayOfInt[0] = 0;
        arrayOfInt[1] = 0;
        this.mContainerView.getLocationInWindow(arrayOfInt);
        f2 = f1 - arrayOfInt[1];
      } 
      ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(this.mContainerView, View.TRANSLATION_Y, new float[] { f2 });
      objectAnimator.addUpdateListener(this.mUpdateListener);
      AnimatorSet.Builder builder = animatorSet.play((Animator)objectAnimator);
      if (this.mContentAnimations) {
        View view = this.mContentView;
        if (view != null)
          builder.with((Animator)ObjectAnimator.ofFloat(view, View.TRANSLATION_Y, new float[] { 0.0F, f2 })); 
      } 
      ActionBarContainer actionBarContainer = this.mSplitView;
      if (actionBarContainer != null && actionBarContainer.getVisibility() == 0) {
        this.mSplitView.setAlpha(1.0F);
        ActionBarContainer actionBarContainer1 = this.mSplitView;
        Property<View, Float> property = View.TRANSLATION_Y;
        actionBarContainer = this.mSplitView;
        f2 = actionBarContainer.getHeight();
        builder.with((Animator)ObjectAnimator.ofFloat(actionBarContainer1, property, new float[] { f2 }));
      } 
      animatorSet.setInterpolator(AnimationUtils.loadInterpolator(this.mContext, 17563650));
      animatorSet.setDuration(250L);
      animatorSet.addListener(this.mHideListener);
      this.mCurrentShowAnim = (Animator)animatorSet;
      animatorSet.start();
    } else {
      this.mHideListener.onAnimationEnd(null);
    } 
  }
  
  public boolean isShowing() {
    boolean bool;
    int i = getHeight();
    if (this.mNowShowing && (i == 0 || getHideOffset() < i)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  void animateToMode(boolean paramBoolean) {
    if (paramBoolean) {
      showForActionMode();
    } else {
      hideForActionMode();
    } 
    if (shouldAnimateContextView()) {
      Animator animator1, animator2;
      if (paramBoolean) {
        animator1 = this.mDecorToolbar.setupAnimatorToVisibility(8, 100L);
        animator2 = this.mContextView.setupAnimatorToVisibility(0, 200L);
      } else {
        animator2 = this.mDecorToolbar.setupAnimatorToVisibility(0, 200L);
        animator1 = this.mContextView.setupAnimatorToVisibility(8, 100L);
      } 
      AnimatorSet animatorSet = new AnimatorSet();
      animatorSet.playSequentially(new Animator[] { animator1, animator2 });
      animatorSet.start();
    } else if (paramBoolean) {
      this.mDecorToolbar.setVisibility(8);
      this.mContextView.setVisibility(0);
    } else {
      this.mDecorToolbar.setVisibility(0);
      this.mContextView.setVisibility(8);
    } 
  }
  
  private boolean shouldAnimateContextView() {
    return this.mContainerView.isLaidOut();
  }
  
  public Context getThemedContext() {
    if (this.mThemedContext == null) {
      TypedValue typedValue = new TypedValue();
      Resources.Theme theme = this.mContext.getTheme();
      theme.resolveAttribute(16843671, typedValue, true);
      int i = typedValue.resourceId;
      if (i != 0 && this.mContext.getThemeResId() != i) {
        this.mThemedContext = (Context)new ContextThemeWrapper(this.mContext, i);
      } else {
        this.mThemedContext = this.mContext;
      } 
    } 
    return this.mThemedContext;
  }
  
  public boolean isTitleTruncated() {
    boolean bool;
    DecorToolbar decorToolbar = this.mDecorToolbar;
    if (decorToolbar != null && decorToolbar.isTitleTruncated()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setHomeAsUpIndicator(Drawable paramDrawable) {
    this.mDecorToolbar.setNavigationIcon(paramDrawable);
  }
  
  public void setHomeAsUpIndicator(int paramInt) {
    this.mDecorToolbar.setNavigationIcon(paramInt);
  }
  
  public void setHomeActionContentDescription(CharSequence paramCharSequence) {
    this.mDecorToolbar.setNavigationContentDescription(paramCharSequence);
  }
  
  public void setHomeActionContentDescription(int paramInt) {
    this.mDecorToolbar.setNavigationContentDescription(paramInt);
  }
  
  public void onContentScrollStarted() {
    Animator animator = this.mCurrentShowAnim;
    if (animator != null) {
      animator.cancel();
      this.mCurrentShowAnim = null;
    } 
  }
  
  public void onContentScrollStopped() {}
  
  public boolean collapseActionView() {
    DecorToolbar decorToolbar = this.mDecorToolbar;
    if (decorToolbar != null && decorToolbar.hasExpandedActionView()) {
      this.mDecorToolbar.collapseActionView();
      return true;
    } 
    return false;
  }
  
  public class ActionModeImpl extends ActionMode implements MenuBuilder.Callback {
    private final Context mActionModeContext;
    
    private ActionMode.Callback mCallback;
    
    private WeakReference<View> mCustomView;
    
    private final MenuBuilder mMenu;
    
    final WindowDecorActionBar this$0;
    
    public ActionModeImpl(Context param1Context, ActionMode.Callback param1Callback) {
      this.mActionModeContext = param1Context;
      this.mCallback = param1Callback;
      MenuBuilder menuBuilder = new MenuBuilder(param1Context);
      this.mMenu = menuBuilder = menuBuilder.setDefaultShowAsAction(1);
      menuBuilder.setCallback(this);
    }
    
    public MenuInflater getMenuInflater() {
      return new MenuInflater(this.mActionModeContext);
    }
    
    public Menu getMenu() {
      return (Menu)this.mMenu;
    }
    
    public void finish() {
      if (WindowDecorActionBar.this.mActionMode != this)
        return; 
      if (!WindowDecorActionBar.checkShowingFlags(WindowDecorActionBar.this.mHiddenByApp, WindowDecorActionBar.this.mHiddenBySystem, false)) {
        WindowDecorActionBar.this.mDeferredDestroyActionMode = this;
        WindowDecorActionBar.this.mDeferredModeDestroyCallback = this.mCallback;
      } else {
        this.mCallback.onDestroyActionMode(this);
      } 
      this.mCallback = null;
      WindowDecorActionBar.this.animateToMode(false);
      WindowDecorActionBar.this.mContextView.closeMode();
      WindowDecorActionBar.this.mDecorToolbar.getViewGroup().sendAccessibilityEvent(32);
      WindowDecorActionBar.this.mOverlayLayout.setHideOnContentScrollEnabled(WindowDecorActionBar.this.mHideOnContentScroll);
      WindowDecorActionBar.this.mActionMode = null;
    }
    
    public void invalidate() {
      if (WindowDecorActionBar.this.mActionMode != this)
        return; 
      this.mMenu.stopDispatchingItemsChanged();
      try {
        this.mCallback.onPrepareActionMode(this, (Menu)this.mMenu);
        return;
      } finally {
        this.mMenu.startDispatchingItemsChanged();
      } 
    }
    
    public boolean dispatchOnCreate() {
      this.mMenu.stopDispatchingItemsChanged();
      try {
        return this.mCallback.onCreateActionMode(this, (Menu)this.mMenu);
      } finally {
        this.mMenu.startDispatchingItemsChanged();
      } 
    }
    
    public void setCustomView(View param1View) {
      WindowDecorActionBar.this.mContextView.setCustomView(param1View);
      this.mCustomView = new WeakReference<>(param1View);
    }
    
    public void setSubtitle(CharSequence param1CharSequence) {
      WindowDecorActionBar.this.mContextView.setSubtitle(param1CharSequence);
    }
    
    public void setTitle(CharSequence param1CharSequence) {
      WindowDecorActionBar.this.mContextView.setTitle(param1CharSequence);
    }
    
    public void setTitle(int param1Int) {
      setTitle(WindowDecorActionBar.this.mContext.getResources().getString(param1Int));
    }
    
    public void setSubtitle(int param1Int) {
      setSubtitle(WindowDecorActionBar.this.mContext.getResources().getString(param1Int));
    }
    
    public CharSequence getTitle() {
      return WindowDecorActionBar.this.mContextView.getTitle();
    }
    
    public CharSequence getSubtitle() {
      return WindowDecorActionBar.this.mContextView.getSubtitle();
    }
    
    public void setTitleOptionalHint(boolean param1Boolean) {
      super.setTitleOptionalHint(param1Boolean);
      WindowDecorActionBar.this.mContextView.setTitleOptional(param1Boolean);
    }
    
    public boolean isTitleOptional() {
      return WindowDecorActionBar.this.mContextView.isTitleOptional();
    }
    
    public View getCustomView() {
      WeakReference<View> weakReference = this.mCustomView;
      if (weakReference != null) {
        View view = weakReference.get();
      } else {
        weakReference = null;
      } 
      return (View)weakReference;
    }
    
    public boolean onMenuItemSelected(MenuBuilder param1MenuBuilder, MenuItem param1MenuItem) {
      ActionMode.Callback callback = this.mCallback;
      if (callback != null)
        return callback.onActionItemClicked(this, param1MenuItem); 
      return false;
    }
    
    public void onCloseMenu(MenuBuilder param1MenuBuilder, boolean param1Boolean) {}
    
    public boolean onSubMenuSelected(SubMenuBuilder param1SubMenuBuilder) {
      if (this.mCallback == null)
        return false; 
      if (!param1SubMenuBuilder.hasVisibleItems())
        return true; 
      (new MenuPopupHelper(WindowDecorActionBar.this.getThemedContext(), (MenuBuilder)param1SubMenuBuilder)).show();
      return true;
    }
    
    public void onCloseSubMenu(SubMenuBuilder param1SubMenuBuilder) {}
    
    public void onMenuModeChange(MenuBuilder param1MenuBuilder) {
      if (this.mCallback == null)
        return; 
      invalidate();
      WindowDecorActionBar.this.mContextView.showOverflowMenu();
    }
  }
  
  public class TabImpl extends ActionBar.Tab {
    private ActionBar.TabListener mCallback;
    
    private CharSequence mContentDesc;
    
    private View mCustomView;
    
    private Drawable mIcon;
    
    private int mPosition;
    
    private Object mTag;
    
    private CharSequence mText;
    
    final WindowDecorActionBar this$0;
    
    public TabImpl() {
      this.mPosition = -1;
    }
    
    public Object getTag() {
      return this.mTag;
    }
    
    public ActionBar.Tab setTag(Object param1Object) {
      this.mTag = param1Object;
      return this;
    }
    
    public ActionBar.TabListener getCallback() {
      return this.mCallback;
    }
    
    public ActionBar.Tab setTabListener(ActionBar.TabListener param1TabListener) {
      this.mCallback = param1TabListener;
      return this;
    }
    
    public View getCustomView() {
      return this.mCustomView;
    }
    
    public ActionBar.Tab setCustomView(View param1View) {
      this.mCustomView = param1View;
      if (this.mPosition >= 0)
        WindowDecorActionBar.this.mTabScrollView.updateTab(this.mPosition); 
      return this;
    }
    
    public ActionBar.Tab setCustomView(int param1Int) {
      LayoutInflater layoutInflater = LayoutInflater.from(WindowDecorActionBar.this.getThemedContext());
      View view = layoutInflater.inflate(param1Int, (ViewGroup)null);
      return setCustomView(view);
    }
    
    public Drawable getIcon() {
      return this.mIcon;
    }
    
    public int getPosition() {
      return this.mPosition;
    }
    
    public void setPosition(int param1Int) {
      this.mPosition = param1Int;
    }
    
    public CharSequence getText() {
      return this.mText;
    }
    
    public ActionBar.Tab setIcon(Drawable param1Drawable) {
      this.mIcon = param1Drawable;
      if (this.mPosition >= 0)
        WindowDecorActionBar.this.mTabScrollView.updateTab(this.mPosition); 
      return this;
    }
    
    public ActionBar.Tab setIcon(int param1Int) {
      return setIcon(WindowDecorActionBar.this.mContext.getDrawable(param1Int));
    }
    
    public ActionBar.Tab setText(CharSequence param1CharSequence) {
      this.mText = param1CharSequence;
      if (this.mPosition >= 0)
        WindowDecorActionBar.this.mTabScrollView.updateTab(this.mPosition); 
      return this;
    }
    
    public ActionBar.Tab setText(int param1Int) {
      return setText(WindowDecorActionBar.this.mContext.getResources().getText(param1Int));
    }
    
    public void select() {
      WindowDecorActionBar.this.selectTab(this);
    }
    
    public ActionBar.Tab setContentDescription(int param1Int) {
      return setContentDescription(WindowDecorActionBar.this.mContext.getResources().getText(param1Int));
    }
    
    public ActionBar.Tab setContentDescription(CharSequence param1CharSequence) {
      this.mContentDesc = param1CharSequence;
      if (this.mPosition >= 0)
        WindowDecorActionBar.this.mTabScrollView.updateTab(this.mPosition); 
      return this;
    }
    
    public CharSequence getContentDescription() {
      return this.mContentDesc;
    }
  }
  
  public void setCustomView(View paramView) {
    this.mDecorToolbar.setCustomView(paramView);
  }
  
  public void setCustomView(View paramView, ActionBar.LayoutParams paramLayoutParams) {
    paramView.setLayoutParams((ViewGroup.LayoutParams)paramLayoutParams);
    this.mDecorToolbar.setCustomView(paramView);
  }
  
  public void setListNavigationCallbacks(SpinnerAdapter paramSpinnerAdapter, ActionBar.OnNavigationListener paramOnNavigationListener) {
    this.mDecorToolbar.setDropdownParams(paramSpinnerAdapter, new NavItemSelectedListener(paramOnNavigationListener));
  }
  
  public int getSelectedNavigationIndex() {
    int i = this.mDecorToolbar.getNavigationMode();
    if (i != 1) {
      int j = -1;
      if (i != 2)
        return -1; 
      TabImpl tabImpl = this.mSelectedTab;
      if (tabImpl != null)
        j = tabImpl.getPosition(); 
      return j;
    } 
    return this.mDecorToolbar.getDropdownSelectedPosition();
  }
  
  public int getNavigationItemCount() {
    int i = this.mDecorToolbar.getNavigationMode();
    if (i != 1) {
      if (i != 2)
        return 0; 
      return this.mTabs.size();
    } 
    return this.mDecorToolbar.getDropdownItemCount();
  }
  
  public int getTabCount() {
    return this.mTabs.size();
  }
  
  public void setNavigationMode(int paramInt) {
    int i = this.mDecorToolbar.getNavigationMode();
    if (i == 2) {
      this.mSavedTabPosition = getSelectedNavigationIndex();
      selectTab(null);
      this.mTabScrollView.setVisibility(8);
    } 
    if (i != paramInt && !this.mHasEmbeddedTabs) {
      ActionBarOverlayLayout actionBarOverlayLayout1 = this.mOverlayLayout;
      if (actionBarOverlayLayout1 != null)
        actionBarOverlayLayout1.requestFitSystemWindows(); 
    } 
    this.mDecorToolbar.setNavigationMode(paramInt);
    boolean bool1 = false;
    if (paramInt == 2) {
      ensureTabsExist();
      this.mTabScrollView.setVisibility(0);
      i = this.mSavedTabPosition;
      if (i != -1) {
        setSelectedNavigationItem(i);
        this.mSavedTabPosition = -1;
      } 
    } 
    DecorToolbar decorToolbar = this.mDecorToolbar;
    if (paramInt == 2 && !this.mHasEmbeddedTabs) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    decorToolbar.setCollapsible(bool2);
    ActionBarOverlayLayout actionBarOverlayLayout = this.mOverlayLayout;
    boolean bool2 = bool1;
    if (paramInt == 2) {
      bool2 = bool1;
      if (!this.mHasEmbeddedTabs)
        bool2 = true; 
    } 
    actionBarOverlayLayout.setHasNonEmbeddedTabs(bool2);
  }
  
  public ActionBar.Tab getTabAt(int paramInt) {
    return this.mTabs.get(paramInt);
  }
  
  public void setIcon(int paramInt) {
    this.mDecorToolbar.setIcon(paramInt);
  }
  
  public void setIcon(Drawable paramDrawable) {
    this.mDecorToolbar.setIcon(paramDrawable);
  }
  
  public boolean hasIcon() {
    return this.mDecorToolbar.hasIcon();
  }
  
  public void setLogo(int paramInt) {
    this.mDecorToolbar.setLogo(paramInt);
  }
  
  public void setLogo(Drawable paramDrawable) {
    this.mDecorToolbar.setLogo(paramDrawable);
  }
  
  public boolean hasLogo() {
    return this.mDecorToolbar.hasLogo();
  }
  
  public void setDefaultDisplayHomeAsUpEnabled(boolean paramBoolean) {
    if (!this.mDisplayHomeAsUpSet)
      setDisplayHomeAsUpEnabled(paramBoolean); 
  }
}
