package com.android.internal.app;

import android.common.ColorFrameworkFactory;
import android.content.Context;
import android.os.UserHandle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.android.internal.widget.GridLayoutManager;
import com.android.internal.widget.RecyclerView;

public class ChooserMultiProfilePagerAdapter extends AbstractMultiProfilePagerAdapter {
  private static final int SINGLE_CELL_SPAN_SIZE = 1;
  
  private int mBottomOffset;
  
  private final boolean mIsSendAction;
  
  private final ChooserProfileDescriptor[] mItems;
  
  ChooserMultiProfilePagerAdapter(Context paramContext, ChooserActivity.ChooserGridAdapter paramChooserGridAdapter, UserHandle paramUserHandle1, UserHandle paramUserHandle2, boolean paramBoolean) {
    super(paramContext, 0, paramUserHandle1, paramUserHandle2);
    this.mItems = new ChooserProfileDescriptor[] { createProfileDescriptor(paramChooserGridAdapter) };
    this.mIsSendAction = paramBoolean;
  }
  
  ChooserMultiProfilePagerAdapter(Context paramContext, ChooserActivity.ChooserGridAdapter paramChooserGridAdapter1, ChooserActivity.ChooserGridAdapter paramChooserGridAdapter2, int paramInt, UserHandle paramUserHandle1, UserHandle paramUserHandle2, boolean paramBoolean) {
    super(paramContext, paramInt, paramUserHandle1, paramUserHandle2);
    ChooserProfileDescriptor chooserProfileDescriptor = createProfileDescriptor(paramChooserGridAdapter1);
    this.mItems = new ChooserProfileDescriptor[] { chooserProfileDescriptor, createProfileDescriptor(paramChooserGridAdapter2) };
    this.mIsSendAction = paramBoolean;
  }
  
  private ChooserProfileDescriptor createProfileDescriptor(ChooserActivity.ChooserGridAdapter paramChooserGridAdapter) {
    LayoutInflater layoutInflater = LayoutInflater.from(getContext());
    ViewGroup viewGroup = ((IOplusResolverManager)ColorFrameworkFactory.getInstance().getFeature(IOplusResolverManager.DEFAULT, new Object[0])).getChooserProfileDescriptor(layoutInflater);
    return new ChooserProfileDescriptor(viewGroup, paramChooserGridAdapter);
  }
  
  RecyclerView getListViewForIndex(int paramInt) {
    return (getItem(paramInt)).recyclerView;
  }
  
  ChooserProfileDescriptor getItem(int paramInt) {
    return this.mItems[paramInt];
  }
  
  int getItemCount() {
    return this.mItems.length;
  }
  
  public ChooserActivity.ChooserGridAdapter getAdapterForIndex(int paramInt) {
    return (this.mItems[paramInt]).chooserGridAdapter;
  }
  
  ChooserListAdapter getListAdapterForUserHandle(UserHandle paramUserHandle) {
    if (getActiveListAdapter().getUserHandle().equals(paramUserHandle))
      return getActiveListAdapter(); 
    if (getInactiveListAdapter() != null && 
      getInactiveListAdapter().getUserHandle().equals(paramUserHandle))
      return getInactiveListAdapter(); 
    return null;
  }
  
  void setupListAdapter(int paramInt) {
    RecyclerView recyclerView = (getItem(paramInt)).recyclerView;
    ChooserActivity.ChooserGridAdapter chooserGridAdapter = (getItem(paramInt)).chooserGridAdapter;
    GridLayoutManager gridLayoutManager = (GridLayoutManager)recyclerView.getLayoutManager();
    gridLayoutManager.setSpanCount(chooserGridAdapter.getMaxTargetsPerRow());
    gridLayoutManager.setSpanSizeLookup((GridLayoutManager.SpanSizeLookup)new Object(this, chooserGridAdapter, gridLayoutManager));
  }
  
  public ChooserListAdapter getActiveListAdapter() {
    return getAdapterForIndex(getCurrentPage()).getListAdapter();
  }
  
  public ChooserListAdapter getInactiveListAdapter() {
    if (getCount() == 1)
      return null; 
    return getAdapterForIndex(1 - getCurrentPage()).getListAdapter();
  }
  
  public ResolverListAdapter getPersonalListAdapter() {
    return getAdapterForIndex(0).getListAdapter();
  }
  
  public ResolverListAdapter getWorkListAdapter() {
    return getAdapterForIndex(1).getListAdapter();
  }
  
  ChooserActivity.ChooserGridAdapter getCurrentRootAdapter() {
    return getAdapterForIndex(getCurrentPage());
  }
  
  RecyclerView getActiveAdapterView() {
    return getListViewForIndex(getCurrentPage());
  }
  
  RecyclerView getInactiveAdapterView() {
    if (getCount() == 1)
      return null; 
    return getListViewForIndex(1 - getCurrentPage());
  }
  
  String getMetricsCategory() {
    return "intent_chooser";
  }
  
  protected void showWorkProfileOffEmptyState(ResolverListAdapter paramResolverListAdapter, View.OnClickListener paramOnClickListener) {
    showEmptyState(paramResolverListAdapter, 17302891, 17041187, 0, paramOnClickListener);
  }
  
  protected void showNoPersonalToWorkIntentsEmptyState(ResolverListAdapter paramResolverListAdapter) {
    if (this.mIsSendAction) {
      showEmptyState(paramResolverListAdapter, 17302824, 17041178, 17041179);
    } else {
      showEmptyState(paramResolverListAdapter, 17302824, 17041174, 17041175);
    } 
  }
  
  protected void showNoWorkToPersonalIntentsEmptyState(ResolverListAdapter paramResolverListAdapter) {
    if (this.mIsSendAction) {
      showEmptyState(paramResolverListAdapter, 17302824, 17041176, 17041177);
    } else {
      showEmptyState(paramResolverListAdapter, 17302824, 17041172, 17041173);
    } 
  }
  
  protected void showNoPersonalAppsAvailableEmptyState(ResolverListAdapter paramResolverListAdapter) {
    if (this.mIsSendAction) {
      showEmptyState(paramResolverListAdapter, 17302754, 17041181, 0);
    } else {
      showEmptyState(paramResolverListAdapter, 17302754, 17041180, 0);
    } 
  }
  
  protected void showNoWorkAppsAvailableEmptyState(ResolverListAdapter paramResolverListAdapter) {
    if (this.mIsSendAction) {
      showEmptyState(paramResolverListAdapter, 17302754, 17041183, 0);
    } else {
      showEmptyState(paramResolverListAdapter, 17302754, 17041182, 0);
    } 
  }
  
  void setEmptyStateBottomOffset(int paramInt) {
    this.mBottomOffset = paramInt;
  }
  
  protected void setupContainerPadding(View paramView) {
    int i = paramView.getPaddingLeft(), j = paramView.getPaddingTop();
    int k = paramView.getPaddingRight(), m = paramView.getPaddingBottom(), n = this.mBottomOffset;
    paramView.setPadding(i, j, k, m + n);
  }
  
  class ChooserProfileDescriptor extends AbstractMultiProfilePagerAdapter.ProfileDescriptor {
    private ChooserActivity.ChooserGridAdapter chooserGridAdapter;
    
    private RecyclerView recyclerView;
    
    final ChooserMultiProfilePagerAdapter this$0;
    
    ChooserProfileDescriptor(ViewGroup param1ViewGroup, ChooserActivity.ChooserGridAdapter param1ChooserGridAdapter) {
      super(param1ViewGroup);
      this.chooserGridAdapter = param1ChooserGridAdapter;
      this.recyclerView = param1ViewGroup.<RecyclerView>findViewById(16909358);
    }
  }
}
