package com.android.internal.app;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.util.Log;

public class ActivityTrigger {
  private static final String TAG = "ActivityTrigger";
  
  protected void finalize() {
    native_at_deinit();
  }
  
  public void activityStartTrigger(ApplicationInfo paramApplicationInfo, int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramApplicationInfo.packageName);
    stringBuilder.append("/");
    stringBuilder.append(paramApplicationInfo.processName);
    stringBuilder.append("/");
    stringBuilder.append(paramApplicationInfo.longVersionCode);
    stringBuilder.append("/");
    stringBuilder.append(paramInt);
    String str = stringBuilder.toString();
    native_at_startApp(str, 0);
  }
  
  public void activityResumeTrigger(Intent paramIntent, ActivityInfo paramActivityInfo, ApplicationInfo paramApplicationInfo, boolean paramBoolean) {
    String str;
    ComponentName componentName = paramIntent.getComponent();
    paramIntent = null;
    if (componentName != null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(componentName.flattenToString());
      stringBuilder.append("/");
      stringBuilder.append(paramApplicationInfo.versionCode);
      str = stringBuilder.toString();
    } 
    native_at_resumeActivity(str);
  }
  
  public void activityPauseTrigger(Intent paramIntent, ActivityInfo paramActivityInfo, ApplicationInfo paramApplicationInfo) {
    String str;
    ComponentName componentName = paramIntent.getComponent();
    paramActivityInfo = null;
    Log.d("ActivityTrigger", "ActivityTrigger activityPauseTrigger ");
    ActivityInfo activityInfo = paramActivityInfo;
    if (componentName != null) {
      activityInfo = paramActivityInfo;
      if (paramApplicationInfo != null) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(componentName.flattenToString());
        stringBuilder.append("/");
        stringBuilder.append(paramApplicationInfo.versionCode);
        str = stringBuilder.toString();
      } 
    } 
    native_at_pauseActivity(str);
  }
  
  public void activityStopTrigger(Intent paramIntent, ActivityInfo paramActivityInfo, ApplicationInfo paramApplicationInfo) {
    String str;
    ComponentName componentName = paramIntent.getComponent();
    paramActivityInfo = null;
    Log.d("ActivityTrigger", "ActivityTrigger activityStopTrigger ");
    ActivityInfo activityInfo = paramActivityInfo;
    if (componentName != null) {
      activityInfo = paramActivityInfo;
      if (paramApplicationInfo != null) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(componentName.flattenToString());
        stringBuilder.append("/");
        stringBuilder.append(paramApplicationInfo.versionCode);
        str = stringBuilder.toString();
      } 
    } 
    native_at_stopActivity(str);
  }
  
  public float activityMiscTrigger(int paramInt1, String paramString, int paramInt2, int paramInt3) {
    return native_at_miscActivity(paramInt1, paramString, paramInt2, paramInt3);
  }
  
  private native void native_at_deinit();
  
  private native float native_at_miscActivity(int paramInt1, String paramString, int paramInt2, int paramInt3);
  
  private native void native_at_pauseActivity(String paramString);
  
  private native void native_at_resumeActivity(String paramString);
  
  private native int native_at_startActivity(String paramString, int paramInt);
  
  private native int native_at_startApp(String paramString, int paramInt);
  
  private native void native_at_stopActivity(String paramString);
}
