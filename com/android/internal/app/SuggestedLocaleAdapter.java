package com.android.internal.app;

import android.content.Context;
import android.content.res.Configuration;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;
import java.util.Set;

public class SuggestedLocaleAdapter extends BaseAdapter implements Filterable {
  private int mSuggestionCount;
  
  private ArrayList<LocaleStore.LocaleInfo> mOriginalLocaleOptions;
  
  private ArrayList<LocaleStore.LocaleInfo> mLocaleOptions;
  
  private LayoutInflater mInflater;
  
  private Locale mDisplayLocale = null;
  
  private final boolean mCountryMode;
  
  private Context mContextOverride = null;
  
  private static final int TYPE_LOCALE = 2;
  
  private static final int TYPE_HEADER_SUGGESTED = 0;
  
  private static final int TYPE_HEADER_ALL_OTHERS = 1;
  
  private static final int MIN_REGIONS_FOR_SUGGESTIONS = 6;
  
  public SuggestedLocaleAdapter(Set<LocaleStore.LocaleInfo> paramSet, boolean paramBoolean) {
    this.mCountryMode = paramBoolean;
    this.mLocaleOptions = new ArrayList<>(paramSet.size());
    for (LocaleStore.LocaleInfo localeInfo : paramSet) {
      if (localeInfo.isSuggested())
        this.mSuggestionCount++; 
      this.mLocaleOptions.add(localeInfo);
    } 
  }
  
  public boolean areAllItemsEnabled() {
    return false;
  }
  
  public boolean isEnabled(int paramInt) {
    boolean bool;
    if (getItemViewType(paramInt) == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getItemViewType(int paramInt) {
    if (!showHeaders())
      return 2; 
    if (paramInt == 0)
      return 0; 
    if (paramInt == this.mSuggestionCount + 1)
      return 1; 
    return 2;
  }
  
  public int getViewTypeCount() {
    if (showHeaders())
      return 3; 
    return 1;
  }
  
  public int getCount() {
    if (showHeaders())
      return this.mLocaleOptions.size() + 2; 
    return this.mLocaleOptions.size();
  }
  
  public Object getItem(int paramInt) {
    byte b = 0;
    if (showHeaders())
      if (paramInt > this.mSuggestionCount) {
        b = -2;
      } else {
        b = -1;
      }  
    return this.mLocaleOptions.get(paramInt + b);
  }
  
  public long getItemId(int paramInt) {
    return paramInt;
  }
  
  public void setDisplayLocale(Context paramContext, Locale paramLocale) {
    if (paramLocale == null) {
      this.mDisplayLocale = null;
      this.mContextOverride = null;
    } else if (!paramLocale.equals(this.mDisplayLocale)) {
      this.mDisplayLocale = paramLocale;
      Configuration configuration = new Configuration();
      configuration.setLocale(paramLocale);
      this.mContextOverride = paramContext.createConfigurationContext(configuration);
    } 
  }
  
  private void setTextTo(TextView paramTextView, int paramInt) {
    Context context = this.mContextOverride;
    if (context == null) {
      paramTextView.setText(paramInt);
    } else {
      paramTextView.setText(context.getText(paramInt));
    } 
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup) {
    View view;
    TextView textView;
    if (paramView == null && this.mInflater == null)
      this.mInflater = LayoutInflater.from(paramViewGroup.getContext()); 
    int i = getItemViewType(paramInt);
    if (i != 0 && i != 1) {
      View view1 = paramView;
      if (!(paramView instanceof ViewGroup))
        view1 = this.mInflater.inflate(17367183, paramViewGroup, false); 
      textView = view1.<TextView>findViewById(16909134);
      LocaleStore.LocaleInfo localeInfo = (LocaleStore.LocaleInfo)getItem(paramInt);
      textView.setText(localeInfo.getLabel(this.mCountryMode));
      textView.setTextLocale(localeInfo.getLocale());
      textView.setContentDescription(localeInfo.getContentDescription(this.mCountryMode));
      paramView = view1;
      if (this.mCountryMode) {
        paramInt = TextUtils.getLayoutDirectionFromLocale(localeInfo.getParent());
        view1.setLayoutDirection(paramInt);
        if (paramInt == 1) {
          paramInt = 4;
        } else {
          paramInt = 3;
        } 
        textView.setTextDirection(paramInt);
        paramView = view1;
      } 
    } else {
      View view1 = paramView;
      if (!(paramView instanceof TextView))
        view1 = this.mInflater.inflate(17367184, (ViewGroup)textView, false); 
      textView = (TextView)view1;
      if (i == 0) {
        setTextTo(textView, 17040445);
      } else if (this.mCountryMode) {
        setTextTo(textView, 17041152);
      } else {
        setTextTo(textView, 17040444);
      } 
      Locale locale = this.mDisplayLocale;
      if (locale == null)
        locale = Locale.getDefault(); 
      textView.setTextLocale(locale);
      view = view1;
    } 
    return view;
  }
  
  private boolean showHeaders() {
    boolean bool = this.mCountryMode;
    boolean bool1 = false;
    if (bool && this.mLocaleOptions.size() < 6)
      return false; 
    int i = this.mSuggestionCount;
    bool = bool1;
    if (i != 0) {
      bool = bool1;
      if (i != this.mLocaleOptions.size())
        bool = true; 
    } 
    return bool;
  }
  
  public void sort(LocaleHelper.LocaleInfoComparator paramLocaleInfoComparator) {
    Collections.sort(this.mLocaleOptions, paramLocaleInfoComparator);
  }
  
  class FilterByNativeAndUiNames extends Filter {
    final SuggestedLocaleAdapter this$0;
    
    protected Filter.FilterResults performFiltering(CharSequence param1CharSequence) {
      Filter.FilterResults filterResults = new Filter.FilterResults();
      if (SuggestedLocaleAdapter.this.mOriginalLocaleOptions == null)
        SuggestedLocaleAdapter.access$002(SuggestedLocaleAdapter.this, new ArrayList(SuggestedLocaleAdapter.this.mLocaleOptions)); 
      ArrayList<LocaleStore.LocaleInfo> arrayList1 = new ArrayList(SuggestedLocaleAdapter.this.mOriginalLocaleOptions);
      if (param1CharSequence == null || param1CharSequence.length() == 0) {
        filterResults.values = arrayList1;
        filterResults.count = arrayList1.size();
        return filterResults;
      } 
      Locale locale = Locale.getDefault();
      param1CharSequence = LocaleHelper.normalizeForSearch(param1CharSequence.toString(), locale);
      int i = arrayList1.size();
      ArrayList<LocaleStore.LocaleInfo> arrayList2 = new ArrayList();
      for (byte b = 0; b < i; b++) {
        LocaleStore.LocaleInfo localeInfo = arrayList1.get(b);
        String str1 = localeInfo.getFullNameInUiLanguage();
        str1 = LocaleHelper.normalizeForSearch(str1, locale);
        String str2 = localeInfo.getFullNameNative();
        str2 = LocaleHelper.normalizeForSearch(str2, locale);
        if (wordMatches(str2, (String)param1CharSequence) || wordMatches(str1, (String)param1CharSequence))
          arrayList2.add(localeInfo); 
      } 
      filterResults.values = arrayList2;
      filterResults.count = arrayList2.size();
      return filterResults;
    }
    
    boolean wordMatches(String param1String1, String param1String2) {
      if (param1String1.startsWith(param1String2))
        return true; 
      for (String str : param1String1.split(" ")) {
        if (str.startsWith(param1String2))
          return true; 
      } 
      return false;
    }
    
    protected void publishResults(CharSequence param1CharSequence, Filter.FilterResults param1FilterResults) {
      SuggestedLocaleAdapter.access$102(SuggestedLocaleAdapter.this, (ArrayList)param1FilterResults.values);
      SuggestedLocaleAdapter.access$202(SuggestedLocaleAdapter.this, 0);
      for (LocaleStore.LocaleInfo localeInfo : SuggestedLocaleAdapter.this.mLocaleOptions) {
        if (localeInfo.isSuggested())
          SuggestedLocaleAdapter.access$208(SuggestedLocaleAdapter.this); 
      } 
      if (param1FilterResults.count > 0) {
        SuggestedLocaleAdapter.this.notifyDataSetChanged();
      } else {
        SuggestedLocaleAdapter.this.notifyDataSetInvalidated();
      } 
    }
  }
  
  public Filter getFilter() {
    return new FilterByNativeAndUiNames();
  }
}
