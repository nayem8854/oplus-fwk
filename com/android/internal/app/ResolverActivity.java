package com.android.internal.app;

import android.app.ActivityManager;
import android.app.ActivityTaskManager;
import android.app.ActivityThread;
import android.app.IActivityTaskManager;
import android.app.VoiceInteractor;
import android.app.admin.DevicePolicyEventLogger;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.PermissionChecker;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.UserInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Insets;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.StrictMode;
import android.os.UserHandle;
import android.os.UserManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.Slog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowInsets;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Space;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;
import com.android.internal.app.chooser.DisplayResolveInfo;
import com.android.internal.app.chooser.TargetInfo;
import com.android.internal.content.PackageMonitor;
import com.android.internal.logging.MetricsLogger;
import com.android.internal.widget.ResolverDrawerLayout;
import com.android.internal.widget.ViewPager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class ResolverActivity extends OplusBaseResolverActivity implements ResolverListAdapter.ResolverListCommunicator {
  private int mLastSelected = -1;
  
  private boolean mResolvingHome = false;
  
  private int mProfileSwitchMessageId = -1;
  
  protected final ArrayList<Intent> mIntents = new ArrayList<>();
  
  protected Insets mSystemWindowInsets = null;
  
  private Space mFooterSpacer = null;
  
  private boolean mWorkProfileHasBeenEnabled = false;
  
  public static boolean ENABLE_TABBED_VIEW = true;
  
  private static final boolean DEBUG = false;
  
  static final String EXTRA_CALLING_USER = "com.android.internal.app.ResolverActivity.EXTRA_CALLING_USER";
  
  private static final String EXTRA_FRAGMENT_ARG_KEY = ":settings:fragment_args_key";
  
  public static final String EXTRA_IS_AUDIO_CAPTURE_DEVICE = "is_audio_capture_device";
  
  static final String EXTRA_SELECTED_PROFILE = "com.android.internal.app.ResolverActivity.EXTRA_SELECTED_PROFILE";
  
  private static final String EXTRA_SHOW_FRAGMENT_ARGS = ":settings:show_fragment_args";
  
  private static final String LAST_SHOWN_TAB_KEY = "last_shown_tab_key";
  
  protected static final String METRICS_CATEGORY_CHOOSER = "intent_chooser";
  
  protected static final String METRICS_CATEGORY_RESOLVER = "intent_resolver";
  
  private static final String OPEN_LINKS_COMPONENT_KEY = "app_link_state";
  
  static final int PROFILE_PERSONAL = 0;
  
  static final int PROFILE_WORK = 1;
  
  private static final String TAB_TAG_PERSONAL = "personal";
  
  private static final String TAB_TAG_WORK = "work";
  
  private static final String TAG = "ResolverActivity";
  
  private Button mAlwaysButton;
  
  protected Bundle mBundle;
  
  private int mDefaultTitleResId;
  
  private UserHandle mHeaderCreatorUser;
  
  protected int mLaunchedFromUid;
  
  private int mLayoutId;
  
  protected AbstractMultiProfilePagerAdapter mMultiProfilePagerAdapter;
  
  private Button mOnceButton;
  
  private PackageMonitor mPersonalPackageMonitor;
  
  private PickTargetOptionRequest mPickOptionRequest;
  
  protected PackageManager mPm;
  
  protected View mProfileView;
  
  private String mReferrerPackage;
  
  private boolean mRegistered;
  
  protected ResolverDrawerLayout mResolverDrawerLayout;
  
  private boolean mRetainInOnStop;
  
  private boolean mSafeForwardingMode;
  
  protected boolean mSupportsAlwaysUseOption;
  
  private CharSequence mTitle;
  
  private PackageMonitor mWorkPackageMonitor;
  
  private BroadcastReceiver mWorkProfileStateReceiver;
  
  private UserHandle mWorkProfileUserHandle;
  
  public static int getLabelRes(String paramString) {
    return (ActionTitle.forAction(paramString)).labelRes;
  }
  
  class ActionTitle extends Enum<ActionTitle> {
    private static final ActionTitle[] $VALUES;
    
    public static final int BROWSABLE_APP_TITLE_RES = 17041532;
    
    public static final int BROWSABLE_HOST_APP_TITLE_RES = 17041530;
    
    public static final int BROWSABLE_HOST_TITLE_RES = 17041529;
    
    public static final int BROWSABLE_TITLE_RES = 17041531;
    
    public static final ActionTitle CAPTURE_IMAGE;
    
    public static final ActionTitle DEFAULT;
    
    public static final ActionTitle EDIT = new ActionTitle("EDIT", 1, "android.intent.action.EDIT", 17041517, 17041519, 17041518);
    
    public static final ActionTitle HOME;
    
    public static final ActionTitle SEND = new ActionTitle("SEND", 2, "android.intent.action.SEND", 17041533, 17041535, 17041534);
    
    public static final ActionTitle SENDTO = new ActionTitle("SENDTO", 3, "android.intent.action.SENDTO", 17041536, 17041538, 17041537);
    
    public static final ActionTitle SEND_MULTIPLE = new ActionTitle("SEND_MULTIPLE", 4, "android.intent.action.SEND_MULTIPLE", 17041533, 17041535, 17041534);
    
    public static ActionTitle valueOf(String param1String) {
      return Enum.<ActionTitle>valueOf(ActionTitle.class, param1String);
    }
    
    public static ActionTitle[] values() {
      return (ActionTitle[])$VALUES.clone();
    }
    
    public static final ActionTitle VIEW = new ActionTitle("VIEW", 0, "android.intent.action.VIEW", 17041539, 17041541, 17041540);
    
    public final String action;
    
    public final int labelRes;
    
    public final int namedTitleRes;
    
    public final int titleRes;
    
    static {
      CAPTURE_IMAGE = new ActionTitle("CAPTURE_IMAGE", 5, "android.media.action.IMAGE_CAPTURE", 17041526, 17041528, 17041527);
      DEFAULT = new ActionTitle("DEFAULT", 6, null, 17041514, 17041516, 17041515);
      ActionTitle actionTitle = new ActionTitle("HOME", 7, "android.intent.action.MAIN", 17041523, 17041525, 17041524);
      $VALUES = new ActionTitle[] { VIEW, EDIT, SEND, SENDTO, SEND_MULTIPLE, CAPTURE_IMAGE, DEFAULT, actionTitle };
    }
    
    private ActionTitle(ResolverActivity this$0, int param1Int1, String param1String1, int param1Int2, int param1Int3, int param1Int4) {
      super((String)this$0, param1Int1);
      this.action = param1String1;
      this.titleRes = param1Int2;
      this.namedTitleRes = param1Int3;
      this.labelRes = param1Int4;
    }
    
    public static ActionTitle forAction(String param1String) {
      for (ActionTitle actionTitle : values()) {
        if (actionTitle != HOME && param1String != null && param1String.equals(actionTitle.action))
          return actionTitle; 
      } 
      return DEFAULT;
    }
  }
  
  protected PackageMonitor createPackageMonitor(ResolverListAdapter paramResolverListAdapter) {
    return (PackageMonitor)new Object(this, paramResolverListAdapter);
  }
  
  private Intent makeMyIntent() {
    Intent intent = new Intent(getIntent());
    intent.setComponent(null);
    intent.setFlags(intent.getFlags() & 0xFF7FFFFF);
    return intent;
  }
  
  protected void onCreate(Bundle paramBundle) {
    Intent intent = makeMyIntent();
    Set set = intent.getCategories();
    if ("android.intent.action.MAIN".equals(intent.getAction()) && set != null)
      if (set.size() == 1 && 
        set.contains("android.intent.category.HOME"))
        this.mResolvingHome = true;  
    setSafeForwardingMode(true);
    onCreate(paramBundle, intent, (CharSequence)null, 0, (Intent[])null, (List<ResolveInfo>)null, true);
  }
  
  protected void onCreate(Bundle paramBundle, Intent paramIntent, CharSequence paramCharSequence, Intent[] paramArrayOfIntent, List<ResolveInfo> paramList, boolean paramBoolean) {
    onCreate(paramBundle, paramIntent, paramCharSequence, 0, paramArrayOfIntent, paramList, paramBoolean);
  }
  
  protected void onCreate(Bundle paramBundle, Intent paramIntent, CharSequence paramCharSequence, int paramInt, Intent[] paramArrayOfIntent, List<ResolveInfo> paramList, boolean paramBoolean) {
    setOriginTheme(appliedThemeResId());
    super.onCreate(paramBundle);
    setProfileSwitchMessageId(paramIntent.getContentUserHint());
    try {
      IActivityTaskManager iActivityTaskManager = ActivityTaskManager.getService();
      IBinder iBinder = getActivityToken();
      this.mLaunchedFromUid = iActivityTaskManager.getLaunchedFromUid(iBinder);
    } catch (RemoteException remoteException) {
      this.mLaunchedFromUid = -1;
    } 
    int i = this.mLaunchedFromUid;
    if (i < 0 || UserHandle.isIsolated(i)) {
      finish();
      return;
    } 
    this.mPm = getPackageManager();
    this.mReferrerPackage = getReferrerPackageName();
    this.mIntents.add(0, new Intent(paramIntent));
    this.mTitle = paramCharSequence;
    this.mDefaultTitleResId = paramInt;
    this.mSupportsAlwaysUseOption = paramBoolean;
    this.mWorkProfileUserHandle = fetchWorkProfileUserProfile();
    if (this.mSupportsAlwaysUseOption && !isVoiceInteraction() && 
      !shouldShowTabs()) {
      paramBoolean = true;
    } else {
      paramBoolean = false;
    } 
    this.mMultiProfilePagerAdapter = createMultiProfilePagerAdapter(paramArrayOfIntent, paramList, paramBoolean);
    if (configureContentView())
      return; 
    AbstractMultiProfilePagerAdapter abstractMultiProfilePagerAdapter = this.mMultiProfilePagerAdapter;
    ResolverListAdapter resolverListAdapter = abstractMultiProfilePagerAdapter.getPersonalListAdapter();
    PackageMonitor packageMonitor = createPackageMonitor(resolverListAdapter);
    Looper looper = getMainLooper();
    UserHandle userHandle = getPersonalProfileUserHandle();
    packageMonitor.register((Context)this, looper, userHandle, false);
    if (shouldShowTabs()) {
      AbstractMultiProfilePagerAdapter abstractMultiProfilePagerAdapter1 = this.mMultiProfilePagerAdapter;
      ResolverListAdapter resolverListAdapter1 = abstractMultiProfilePagerAdapter1.getWorkListAdapter();
      PackageMonitor packageMonitor1 = createPackageMonitor(resolverListAdapter1);
      packageMonitor1.register((Context)this, getMainLooper(), getWorkProfileUserHandle(), false);
    } 
    this.mRegistered = true;
    initPreferenceAndPinList();
    if (isOriginUi()) {
      ResolverDrawerLayout resolverDrawerLayout = (ResolverDrawerLayout)findViewById(16908864);
      if (resolverDrawerLayout != null) {
        resolverDrawerLayout.setOnDismissedListener((ResolverDrawerLayout.OnDismissedListener)new Object(this));
        PackageManager packageManager = getPackageManager();
        paramBoolean = packageManager.hasSystemFeature("android.hardware.touchscreen");
        if (isVoiceInteraction() || !paramBoolean)
          resolverDrawerLayout.setCollapsed(false); 
        resolverDrawerLayout.setSystemUiVisibility(768);
        resolverDrawerLayout.setOnApplyWindowInsetsListener(new _$$Lambda$yRChr_JGmMwuDQFg_BsC_mE_wmc(this));
        this.mResolverDrawerLayout = resolverDrawerLayout;
      } 
    } 
    View view = findViewById(16909321);
    if (view != null) {
      view.setOnClickListener(new _$$Lambda$fPZctSH683BQhFNSBKdl6Wz99qg(this));
      updateProfileViewButton();
    } 
    Set set = paramIntent.getCategories();
    if (this.mMultiProfilePagerAdapter.getActiveListAdapter().hasFilteredItem()) {
      paramInt = 451;
    } else {
      paramInt = 453;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramIntent.getAction());
    stringBuilder.append(":");
    stringBuilder.append(paramIntent.getType());
    stringBuilder.append(":");
    if (set != null) {
      str = Arrays.toString(set.toArray());
    } else {
      str = "";
    } 
    stringBuilder.append(str);
    String str = stringBuilder.toString();
    MetricsLogger.action((Context)this, paramInt, str);
  }
  
  private boolean isIntentPicker() {
    return getClass().equals(ResolverActivity.class);
  }
  
  protected AbstractMultiProfilePagerAdapter createMultiProfilePagerAdapter(Intent[] paramArrayOfIntent, List<ResolveInfo> paramList, boolean paramBoolean) {
    ResolverMultiProfilePagerAdapter resolverMultiProfilePagerAdapter;
    if (shouldShowTabs()) {
      resolverMultiProfilePagerAdapter = createResolverMultiProfilePagerAdapterForTwoProfiles(paramArrayOfIntent, paramList, paramBoolean);
    } else {
      resolverMultiProfilePagerAdapter = createResolverMultiProfilePagerAdapterForOneProfile((Intent[])resolverMultiProfilePagerAdapter, paramList, paramBoolean);
    } 
    return resolverMultiProfilePagerAdapter;
  }
  
  private ResolverMultiProfilePagerAdapter createResolverMultiProfilePagerAdapterForOneProfile(Intent[] paramArrayOfIntent, List<ResolveInfo> paramList, boolean paramBoolean) {
    ArrayList<Intent> arrayList = this.mIntents;
    UserHandle userHandle = UserHandle.of(UserHandle.myUserId());
    ResolverListAdapter resolverListAdapter = createResolverListAdapter((Context)this, arrayList, paramArrayOfIntent, paramList, paramBoolean, userHandle);
    return 

      
      new ResolverMultiProfilePagerAdapter((Context)this, resolverListAdapter, getPersonalProfileUserHandle(), null);
  }
  
  private ResolverMultiProfilePagerAdapter createResolverMultiProfilePagerAdapterForTwoProfiles(Intent[] paramArrayOfIntent, List<ResolveInfo> paramList, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual getCurrentProfile : ()I
    //   4: istore #4
    //   6: aload_0
    //   7: invokevirtual getIntent : ()Landroid/content/Intent;
    //   10: ldc 'com.android.internal.app.ResolverActivity.EXTRA_CALLING_USER'
    //   12: invokevirtual hasExtra : (Ljava/lang/String;)Z
    //   15: ifeq -> 35
    //   18: aload_0
    //   19: invokevirtual getIntent : ()Landroid/content/Intent;
    //   22: ldc 'com.android.internal.app.ResolverActivity.EXTRA_CALLING_USER'
    //   24: invokevirtual getParcelableExtra : (Ljava/lang/String;)Landroid/os/Parcelable;
    //   27: checkcast android/os/UserHandle
    //   30: astore #5
    //   32: goto -> 41
    //   35: aload_0
    //   36: invokevirtual getUser : ()Landroid/os/UserHandle;
    //   39: astore #5
    //   41: aload_0
    //   42: invokevirtual getUser : ()Landroid/os/UserHandle;
    //   45: aload #5
    //   47: invokevirtual equals : (Ljava/lang/Object;)Z
    //   50: ifne -> 89
    //   53: aload_0
    //   54: invokevirtual getPersonalProfileUserHandle : ()Landroid/os/UserHandle;
    //   57: aload #5
    //   59: invokevirtual equals : (Ljava/lang/Object;)Z
    //   62: ifeq -> 71
    //   65: iconst_0
    //   66: istore #6
    //   68: goto -> 108
    //   71: aload_0
    //   72: invokevirtual getWorkProfileUserHandle : ()Landroid/os/UserHandle;
    //   75: aload #5
    //   77: invokevirtual equals : (Ljava/lang/Object;)Z
    //   80: ifeq -> 104
    //   83: iconst_1
    //   84: istore #6
    //   86: goto -> 108
    //   89: aload_0
    //   90: invokevirtual getSelectedProfileExtra : ()I
    //   93: istore #6
    //   95: iload #6
    //   97: iconst_m1
    //   98: if_icmpeq -> 104
    //   101: goto -> 108
    //   104: iload #4
    //   106: istore #6
    //   108: aload_0
    //   109: getfield mIntents : Ljava/util/ArrayList;
    //   112: astore #7
    //   114: iload #6
    //   116: ifne -> 125
    //   119: aload_1
    //   120: astore #8
    //   122: goto -> 128
    //   125: aconst_null
    //   126: astore #8
    //   128: iload_3
    //   129: ifeq -> 155
    //   132: invokestatic myUserId : ()I
    //   135: istore #4
    //   137: iload #4
    //   139: aload_0
    //   140: invokevirtual getPersonalProfileUserHandle : ()Landroid/os/UserHandle;
    //   143: invokevirtual getIdentifier : ()I
    //   146: if_icmpne -> 155
    //   149: iconst_1
    //   150: istore #9
    //   152: goto -> 158
    //   155: iconst_0
    //   156: istore #9
    //   158: aload_0
    //   159: invokevirtual getPersonalProfileUserHandle : ()Landroid/os/UserHandle;
    //   162: astore #10
    //   164: aload_0
    //   165: aload_0
    //   166: aload #7
    //   168: aload #8
    //   170: aload_2
    //   171: iload #9
    //   173: aload #10
    //   175: invokevirtual createResolverListAdapter : (Landroid/content/Context;Ljava/util/List;[Landroid/content/Intent;Ljava/util/List;ZLandroid/os/UserHandle;)Lcom/android/internal/app/ResolverListAdapter;
    //   178: astore #8
    //   180: aload_0
    //   181: invokevirtual getWorkProfileUserHandle : ()Landroid/os/UserHandle;
    //   184: astore #7
    //   186: aload_0
    //   187: getfield mIntents : Ljava/util/ArrayList;
    //   190: astore #10
    //   192: iload #6
    //   194: iconst_1
    //   195: if_icmpne -> 201
    //   198: goto -> 203
    //   201: aconst_null
    //   202: astore_1
    //   203: iload_3
    //   204: ifeq -> 227
    //   207: invokestatic myUserId : ()I
    //   210: istore #4
    //   212: iload #4
    //   214: aload #7
    //   216: invokevirtual getIdentifier : ()I
    //   219: if_icmpne -> 227
    //   222: iconst_1
    //   223: istore_3
    //   224: goto -> 229
    //   227: iconst_0
    //   228: istore_3
    //   229: aload_0
    //   230: aload_0
    //   231: aload #10
    //   233: aload_1
    //   234: aload_2
    //   235: iload_3
    //   236: aload #7
    //   238: invokevirtual createResolverListAdapter : (Landroid/content/Context;Ljava/util/List;[Landroid/content/Intent;Ljava/util/List;ZLandroid/os/UserHandle;)Lcom/android/internal/app/ResolverListAdapter;
    //   241: astore #7
    //   243: aload_0
    //   244: invokevirtual getPersonalProfileUserHandle : ()Landroid/os/UserHandle;
    //   247: astore_1
    //   248: aload_0
    //   249: invokevirtual getWorkProfileUserHandle : ()Landroid/os/UserHandle;
    //   252: astore_2
    //   253: new com/android/internal/app/ResolverMultiProfilePagerAdapter
    //   256: dup
    //   257: aload_0
    //   258: aload #8
    //   260: aload #7
    //   262: iload #6
    //   264: aload_1
    //   265: aload_2
    //   266: aload_0
    //   267: invokevirtual getUser : ()Landroid/os/UserHandle;
    //   270: aload #5
    //   272: invokevirtual equals : (Ljava/lang/Object;)Z
    //   275: invokespecial <init> : (Landroid/content/Context;Lcom/android/internal/app/ResolverListAdapter;Lcom/android/internal/app/ResolverListAdapter;ILandroid/os/UserHandle;Landroid/os/UserHandle;Z)V
    //   278: astore_1
    //   279: aload_1
    //   280: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #515	-> 0
    //   #516	-> 6
    //   #517	-> 18
    //   #518	-> 35
    //   #519	-> 41
    //   #520	-> 53
    //   #521	-> 65
    //   #522	-> 71
    //   #523	-> 83
    //   #526	-> 89
    //   #527	-> 95
    //   #528	-> 101
    //   #534	-> 104
    //   #537	-> 114
    //   #539	-> 132
    //   #540	-> 137
    //   #541	-> 158
    //   #534	-> 164
    //   #542	-> 180
    //   #543	-> 186
    //   #546	-> 192
    //   #548	-> 207
    //   #549	-> 212
    //   #543	-> 229
    //   #551	-> 243
    //   #556	-> 243
    //   #557	-> 248
    //   #558	-> 253
    //   #551	-> 279
  }
  
  protected int appliedThemeResId() {
    return 16974836;
  }
  
  int getSelectedProfileExtra() {
    int i = -1;
    if (getIntent().hasExtra("com.android.internal.app.ResolverActivity.EXTRA_SELECTED_PROFILE")) {
      int j = getIntent().getIntExtra("com.android.internal.app.ResolverActivity.EXTRA_SELECTED_PROFILE", -1);
      i = j;
      if (j != 0)
        if (j == 1) {
          i = j;
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("com.android.internal.app.ResolverActivity.EXTRA_SELECTED_PROFILE has invalid value ");
          stringBuilder.append(j);
          stringBuilder.append(". Must be either ResolverActivity.PROFILE_PERSONAL or ResolverActivity.PROFILE_WORK.");
          throw new IllegalArgumentException(stringBuilder.toString());
        }  
    } 
    return i;
  }
  
  protected int getCurrentProfile() {
    boolean bool;
    if (UserHandle.myUserId() == 0) {
      bool = false;
    } else {
      bool = true;
    } 
    return bool;
  }
  
  protected UserHandle getPersonalProfileUserHandle() {
    return UserHandle.of(ActivityManager.getCurrentUser());
  }
  
  protected UserHandle getWorkProfileUserHandle() {
    return this.mWorkProfileUserHandle;
  }
  
  protected UserHandle fetchWorkProfileUserProfile() {
    this.mWorkProfileUserHandle = null;
    UserManager userManager = (UserManager)getSystemService(UserManager.class);
    for (UserInfo userInfo : userManager.getProfiles(ActivityManager.getCurrentUser())) {
      if (userInfo.isManagedProfile())
        this.mWorkProfileUserHandle = userInfo.getUserHandle(); 
    } 
    return this.mWorkProfileUserHandle;
  }
  
  private boolean hasWorkProfile() {
    boolean bool;
    if (getWorkProfileUserHandle() != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected boolean shouldShowTabs() {
    boolean bool;
    if (hasWorkProfile() && ENABLE_TABBED_VIEW) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected void onProfileClick(View paramView) {
    AbstractMultiProfilePagerAdapter abstractMultiProfilePagerAdapter = this.mMultiProfilePagerAdapter;
    DisplayResolveInfo displayResolveInfo = abstractMultiProfilePagerAdapter.getActiveListAdapter().getOtherProfile();
    if (displayResolveInfo == null)
      return; 
    this.mProfileSwitchMessageId = -1;
    onTargetSelected(displayResolveInfo, false);
    finish();
  }
  
  protected boolean shouldAddFooterView() {
    if (useLayoutWithDefault())
      return true; 
    View view = findViewById(16908811);
    if (view == null || view.getVisibility() == 8)
      return true; 
    return false;
  }
  
  protected void applyFooterView(int paramInt) {
    if (this.mFooterSpacer == null) {
      this.mFooterSpacer = new Space(getApplicationContext());
    } else {
      ResolverMultiProfilePagerAdapter resolverMultiProfilePagerAdapter1 = (ResolverMultiProfilePagerAdapter)this.mMultiProfilePagerAdapter;
      resolverMultiProfilePagerAdapter1.getActiveAdapterView().removeFooterView(this.mFooterSpacer);
    } 
    this.mFooterSpacer.setLayoutParams(new AbsListView.LayoutParams(-1, this.mSystemWindowInsets.bottom));
    ResolverMultiProfilePagerAdapter resolverMultiProfilePagerAdapter = (ResolverMultiProfilePagerAdapter)this.mMultiProfilePagerAdapter;
    resolverMultiProfilePagerAdapter.getActiveAdapterView().addFooterView(this.mFooterSpacer);
  }
  
  protected WindowInsets onApplyWindowInsets(View paramView, WindowInsets paramWindowInsets) {
    Insets insets = paramWindowInsets.getSystemWindowInsets();
    this.mResolverDrawerLayout.setPadding(insets.left, this.mSystemWindowInsets.top, this.mSystemWindowInsets.right, 0);
    resetButtonBar();
    if (shouldAddFooterView())
      applyFooterView(this.mSystemWindowInsets.bottom); 
    return paramWindowInsets.consumeSystemWindowInsets();
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration) {
    super.onConfigurationChanged(paramConfiguration);
    this.mMultiProfilePagerAdapter.getActiveListAdapter().handlePackagesChanged();
    if (isIntentPicker() && shouldShowTabs() && !useLayoutWithDefault())
      updateIntentPickerPaddings(); 
    Insets insets = this.mSystemWindowInsets;
    if (insets != null)
      this.mResolverDrawerLayout.setPadding(insets.left, this.mSystemWindowInsets.top, this.mSystemWindowInsets.right, 0); 
  }
  
  private void updateIntentPickerPaddings() {
    View view = findViewById(16909546);
    int i = view.getPaddingLeft();
    int j = view.getPaddingTop();
    int k = view.getPaddingRight();
    int m = getResources().getDimensionPixelSize(17105452);
    view.setPadding(i, j, k, m);
    view = findViewById(16908811);
    j = view.getPaddingLeft();
    m = getResources().getDimensionPixelSize(17105436);
    i = view.getPaddingRight();
    k = getResources().getDimensionPixelSize(17105436);
    view.setPadding(j, m, i, k);
    this.mMultiProfilePagerAdapter.updateAfterConfigChange();
  }
  
  public void sendVoiceChoicesIfNeeded() {
    if (!isVoiceInteraction())
      return; 
    int i = this.mMultiProfilePagerAdapter.getActiveListAdapter().getCount();
    VoiceInteractor.PickOptionRequest.Option[] arrayOfOption = new VoiceInteractor.PickOptionRequest.Option[i];
    int j;
    for (i = 0, j = arrayOfOption.length; i < j; i++) {
      TargetInfo targetInfo = this.mMultiProfilePagerAdapter.getActiveListAdapter().getItem(i);
      if (targetInfo == null)
        return; 
      arrayOfOption[i] = optionForChooserTarget(targetInfo, i);
    } 
    this.mPickOptionRequest = new PickTargetOptionRequest(new VoiceInteractor.Prompt(getTitle()), arrayOfOption, null);
    getVoiceInteractor().submitRequest((VoiceInteractor.Request)this.mPickOptionRequest);
  }
  
  VoiceInteractor.PickOptionRequest.Option optionForChooserTarget(TargetInfo paramTargetInfo, int paramInt) {
    return new VoiceInteractor.PickOptionRequest.Option(paramTargetInfo.getDisplayLabel(), paramInt);
  }
  
  protected final void setAdditionalTargets(Intent[] paramArrayOfIntent) {
    if (paramArrayOfIntent != null) {
      int i;
      byte b;
      for (i = paramArrayOfIntent.length, b = 0; b < i; ) {
        Intent intent = paramArrayOfIntent[b];
        this.mIntents.add(intent);
        b++;
      } 
    } 
  }
  
  public Intent getTargetIntent() {
    Intent intent;
    if (this.mIntents.isEmpty()) {
      intent = null;
    } else {
      intent = this.mIntents.get(0);
    } 
    return intent;
  }
  
  protected String getReferrerPackageName() {
    Uri uri = getReferrer();
    if (uri != null && "android-app".equals(uri.getScheme()))
      return uri.getHost(); 
    return null;
  }
  
  public int getLayoutResource() {
    return 17367274;
  }
  
  public void updateProfileViewButton() {
    if (this.mProfileView == null)
      return; 
    AbstractMultiProfilePagerAdapter abstractMultiProfilePagerAdapter = this.mMultiProfilePagerAdapter;
    DisplayResolveInfo displayResolveInfo = abstractMultiProfilePagerAdapter.getActiveListAdapter().getOtherProfile();
    if (displayResolveInfo != null && !shouldShowTabs()) {
      this.mProfileView.setVisibility(0);
      AbstractMultiProfilePagerAdapter abstractMultiProfilePagerAdapter1 = (AbstractMultiProfilePagerAdapter)this.mProfileView.findViewById(16909321);
      abstractMultiProfilePagerAdapter = abstractMultiProfilePagerAdapter1;
      if (!(abstractMultiProfilePagerAdapter1 instanceof TextView))
        abstractMultiProfilePagerAdapter = this.mProfileView.findViewById(16908308); 
      ((TextView)abstractMultiProfilePagerAdapter).setText(displayResolveInfo.getDisplayLabel());
    } else {
      this.mProfileView.setVisibility(8);
    } 
  }
  
  private void setProfileSwitchMessageId(int paramInt) {
    if (hasCustomFlag(512))
      return; 
    if (paramInt != -2 && 
      paramInt != UserHandle.myUserId()) {
      boolean bool;
      UserManager userManager = (UserManager)getSystemService("user");
      UserInfo userInfo = userManager.getUserInfo(paramInt);
      if (userInfo != null) {
        bool = userInfo.isManagedProfile();
      } else {
        bool = false;
      } 
      boolean bool1 = userManager.isManagedProfile();
      if (bool && !bool1) {
        this.mProfileSwitchMessageId = 17040261;
      } else if (!bool && bool1) {
        this.mProfileSwitchMessageId = 17040262;
      } 
    } 
  }
  
  public void setSafeForwardingMode(boolean paramBoolean) {
    this.mSafeForwardingMode = paramBoolean;
  }
  
  protected CharSequence getTitleForAction(Intent paramIntent, int paramInt) {
    ActionTitle actionTitle;
    CharSequence charSequence;
    boolean bool;
    if (this.mResolvingHome) {
      actionTitle = ActionTitle.HOME;
    } else {
      actionTitle = ActionTitle.forAction(actionTitle.getAction());
    } 
    AbstractMultiProfilePagerAdapter abstractMultiProfilePagerAdapter = this.mMultiProfilePagerAdapter;
    if (abstractMultiProfilePagerAdapter.getActiveListAdapter().getFilteredPosition() >= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    if (actionTitle == ActionTitle.DEFAULT && paramInt != 0)
      return getString(paramInt); 
    if (bool) {
      paramInt = actionTitle.namedTitleRes;
      AbstractMultiProfilePagerAdapter abstractMultiProfilePagerAdapter1 = this.mMultiProfilePagerAdapter;
      charSequence = abstractMultiProfilePagerAdapter1.getActiveListAdapter().getFilteredItem().getDisplayLabel();
      charSequence = getString(paramInt, new Object[] { charSequence });
    } else {
      charSequence = getString(((ActionTitle)charSequence).titleRes);
    } 
    return charSequence;
  }
  
  void dismiss() {
    if (!isFinishing())
      finish(); 
  }
  
  protected void onRestart() {
    super.onRestart();
    if (!this.mRegistered) {
      PackageMonitor packageMonitor = this.mPersonalPackageMonitor;
      Looper looper = getMainLooper();
      UserHandle userHandle = getPersonalProfileUserHandle();
      packageMonitor.register((Context)this, looper, userHandle, false);
      if (shouldShowTabs()) {
        if (this.mWorkPackageMonitor == null) {
          AbstractMultiProfilePagerAdapter abstractMultiProfilePagerAdapter = this.mMultiProfilePagerAdapter;
          ResolverListAdapter resolverListAdapter = abstractMultiProfilePagerAdapter.getWorkListAdapter();
          this.mWorkPackageMonitor = createPackageMonitor(resolverListAdapter);
        } 
        PackageMonitor packageMonitor1 = this.mWorkPackageMonitor;
        looper = getMainLooper();
        UserHandle userHandle1 = getWorkProfileUserHandle();
        packageMonitor1.register((Context)this, looper, userHandle1, false);
      } 
      this.mRegistered = true;
    } 
    if (shouldShowTabs() && this.mMultiProfilePagerAdapter.isWaitingToEnableWorkProfile() && 
      this.mMultiProfilePagerAdapter.isQuietModeEnabled(getWorkProfileUserHandle()))
      this.mMultiProfilePagerAdapter.markWorkProfileEnabledBroadcastReceived(); 
    this.mMultiProfilePagerAdapter.getActiveListAdapter().handlePackagesChanged();
    updateProfileViewButton();
  }
  
  protected void onStart() {
    super.onStart();
    if (shouldShowTabs()) {
      this.mWorkProfileStateReceiver = createWorkProfileStateReceiver();
      registerWorkProfileStateReceiver();
      this.mWorkProfileHasBeenEnabled = isWorkProfileEnabled();
    } 
  }
  
  private boolean isWorkProfileEnabled() {
    boolean bool;
    UserHandle userHandle = getWorkProfileUserHandle();
    UserManager userManager = (UserManager)getSystemService(UserManager.class);
    if (!userManager.isQuietModeEnabled(userHandle) && 
      userManager.isUserUnlocked(userHandle)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void registerWorkProfileStateReceiver() {
    IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction("android.intent.action.USER_UNLOCKED");
    intentFilter.addAction("android.intent.action.MANAGED_PROFILE_AVAILABLE");
    intentFilter.addAction("android.intent.action.MANAGED_PROFILE_UNAVAILABLE");
    registerReceiverAsUser(this.mWorkProfileStateReceiver, UserHandle.ALL, intentFilter, null, null);
  }
  
  protected void onStop() {
    super.onStop();
    if (this.mRegistered) {
      this.mPersonalPackageMonitor.unregister();
      PackageMonitor packageMonitor = this.mWorkPackageMonitor;
      if (packageMonitor != null)
        packageMonitor.unregister(); 
      this.mRegistered = false;
    } 
    Intent intent = getIntent();
    if ((intent.getFlags() & 0x10000000) != 0 && !isVoiceInteraction() && !this.mResolvingHome && !this.mRetainInOnStop)
      if (!isChangingConfigurations())
        finish();  
    if (this.mWorkPackageMonitor != null) {
      unregisterReceiver(this.mWorkProfileStateReceiver);
      this.mWorkPackageMonitor = null;
    } 
  }
  
  protected void onDestroy() {
    super.onDestroy();
    if (!isChangingConfigurations()) {
      PickTargetOptionRequest pickTargetOptionRequest = this.mPickOptionRequest;
      if (pickTargetOptionRequest != null)
        pickTargetOptionRequest.cancel(); 
    } 
    if (this.mMultiProfilePagerAdapter.getActiveListAdapter() != null)
      this.mMultiProfilePagerAdapter.getActiveListAdapter().onDestroy(); 
  }
  
  protected void onSaveInstanceState(Bundle paramBundle) {
    super.onSaveInstanceState(paramBundle);
    if (!isOriginUi()) {
      paramBundle.putInt("last_shown_tab_key", this.mMultiProfilePagerAdapter.getCurrentPage());
      return;
    } 
    ViewPager viewPager = (ViewPager)findViewById(16909322);
    paramBundle.putInt("last_shown_tab_key", viewPager.getCurrentItem());
  }
  
  protected void onRestoreInstanceState(Bundle paramBundle) {
    super.onRestoreInstanceState(paramBundle);
    resetButtonBar();
    if (!isOriginUi()) {
      restoreProfilePager(paramBundle.getInt("last_shown_tab_key"));
      this.mMultiProfilePagerAdapter.clearInactiveProfileCache();
      return;
    } 
    ViewPager viewPager = (ViewPager)findViewById(16909322);
    viewPager.setCurrentItem(paramBundle.getInt("last_shown_tab_key"));
    this.mMultiProfilePagerAdapter.clearInactiveProfileCache();
  }
  
  private boolean hasManagedProfile() {
    UserManager userManager = (UserManager)getSystemService("user");
    if (userManager == null)
      return false; 
    try {
      List list = userManager.getProfiles(getUserId());
      for (UserInfo userInfo : list) {
        if (userInfo != null) {
          boolean bool = userInfo.isManagedProfile();
          if (bool)
            return true; 
        } 
      } 
      return false;
    } catch (SecurityException securityException) {
      return false;
    } 
  }
  
  private boolean supportsManagedProfiles(ResolveInfo paramResolveInfo) {
    boolean bool = false;
    try {
      ApplicationInfo applicationInfo = getPackageManager().getApplicationInfo(paramResolveInfo.activityInfo.packageName, 0);
      int i = applicationInfo.targetSdkVersion;
      if (i >= 21)
        bool = true; 
      return bool;
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      return false;
    } 
  }
  
  private void setAlwaysButtonEnabled(boolean paramBoolean1, int paramInt, boolean paramBoolean2) {
    int i;
    ResolveInfo resolveInfo;
    if (!this.mMultiProfilePagerAdapter.getCurrentUserHandle().equals(getUser())) {
      this.mAlwaysButton.setEnabled(false);
      return;
    } 
    boolean bool = false;
    ResolverListAdapter resolverListAdapter = null;
    if (paramBoolean1) {
      resolverListAdapter = this.mMultiProfilePagerAdapter.getActiveListAdapter();
      resolveInfo = resolverListAdapter.resolveInfoForPosition(paramInt, paramBoolean2);
      if (resolveInfo == null) {
        Log.e("ResolverActivity", "Invalid position supplied to setAlwaysButtonEnabled");
        return;
      } 
      if (resolveInfo.targetUserId != -2) {
        Log.e("ResolverActivity", "Attempted to set selection to resolve info for another user");
        return;
      } 
      bool = true;
      Button button = this.mAlwaysButton;
      Resources resources = getResources();
      String str = resources.getString(17039599);
      button.setText(str);
    } 
    paramBoolean1 = bool;
    if (resolveInfo != null) {
      ActivityInfo activityInfo = resolveInfo.activityInfo;
      PackageManager packageManager = this.mPm;
      String str = activityInfo.packageName;
      if (packageManager.checkPermission("android.permission.RECORD_AUDIO", str) == 0) {
        paramInt = 1;
      } else {
        paramInt = 0;
      } 
      paramBoolean1 = bool;
      if (paramInt == 0) {
        paramBoolean1 = getIntent().getBooleanExtra("is_audio_capture_device", false);
        i = paramBoolean1 ^ true;
      } 
    } 
    this.mAlwaysButton.setEnabled(i);
  }
  
  public void onButtonClick(View paramView) {
    int j, i = paramView.getId();
    ListView listView = (ListView)this.mMultiProfilePagerAdapter.getActiveAdapterView();
    ResolverListAdapter resolverListAdapter = this.mMultiProfilePagerAdapter.getActiveListAdapter();
    if (resolverListAdapter.hasFilteredItem()) {
      j = resolverListAdapter.getFilteredPosition();
    } else {
      j = listView.getCheckedItemPosition();
    } 
    boolean bool = resolverListAdapter.hasFilteredItem();
    boolean bool1 = true;
    if (i != 16908810)
      bool1 = false; 
    startSelected(j, bool1, bool ^ true);
  }
  
  public void startSelected(int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    Toast toast;
    if (isFinishing())
      return; 
    ResolverListAdapter resolverListAdapter1 = this.mMultiProfilePagerAdapter.getActiveListAdapter();
    ResolveInfo resolveInfo = resolverListAdapter1.resolveInfoForPosition(paramInt, paramBoolean2);
    if (this.mResolvingHome && hasManagedProfile() && !supportsManagedProfiles(resolveInfo)) {
      String str2 = getResources().getString(17039601);
      ActivityInfo activityInfo = resolveInfo.activityInfo;
      String str1 = activityInfo.loadLabel(getPackageManager()).toString();
      toast = Toast.makeText((Context)this, String.format(str2, new Object[] { str1 }), 1);
      toast.show();
      return;
    } 
    ResolverListAdapter resolverListAdapter2 = this.mMultiProfilePagerAdapter.getActiveListAdapter();
    TargetInfo targetInfo = resolverListAdapter2.targetInfoForPosition(paramInt, paramBoolean2);
    if (targetInfo == null)
      return; 
    statisticsData((ResolveInfo)toast, paramInt);
    if (onTargetSelected(targetInfo, paramBoolean1)) {
      if (paramBoolean1 && this.mSupportsAlwaysUseOption) {
        MetricsLogger.action((Context)this, 455);
      } else if (this.mSupportsAlwaysUseOption) {
        MetricsLogger.action((Context)this, 456);
      } else {
        MetricsLogger.action((Context)this, 457);
      } 
      if (this.mMultiProfilePagerAdapter.getActiveListAdapter().hasFilteredItem()) {
        paramInt = 452;
      } else {
        paramInt = 454;
      } 
      MetricsLogger.action((Context)this, paramInt);
      finish();
    } 
  }
  
  public Intent getReplacementIntent(ActivityInfo paramActivityInfo, Intent paramIntent) {
    return paramIntent;
  }
  
  public final void onPostListReady(ResolverListAdapter paramResolverListAdapter, boolean paramBoolean1, boolean paramBoolean2) {
    if (isAutolaunching())
      return; 
    if (isIntentPicker()) {
      ResolverMultiProfilePagerAdapter resolverMultiProfilePagerAdapter = (ResolverMultiProfilePagerAdapter)this.mMultiProfilePagerAdapter;
      resolverMultiProfilePagerAdapter.setUseLayoutWithDefault(useLayoutWithDefault());
    } 
    if (this.mMultiProfilePagerAdapter.shouldShowEmptyStateScreen(paramResolverListAdapter)) {
      this.mMultiProfilePagerAdapter.showEmptyResolverListEmptyState(paramResolverListAdapter);
    } else {
      this.mMultiProfilePagerAdapter.showListView(paramResolverListAdapter);
    } 
    if (paramBoolean2 && maybeAutolaunchActivity())
      return; 
    if (paramBoolean1) {
      maybeCreateHeader(paramResolverListAdapter);
      resetButtonBar();
      onListRebuilt(paramResolverListAdapter);
    } 
  }
  
  protected void onListRebuilt(ResolverListAdapter paramResolverListAdapter) {
    setResolverContent();
    if (shouldShowTabs() && isIntentPicker()) {
      ResolverDrawerLayout resolverDrawerLayout = (ResolverDrawerLayout)findViewById(16908864);
      if (resolverDrawerLayout != null) {
        Resources resources = getResources();
        if (useLayoutWithDefault()) {
          i = 17105447;
        } else {
          i = 17105448;
        } 
        int i = resources.getDimensionPixelSize(i);
        resolverDrawerLayout.setMaxCollapsedHeight(i);
      } 
    } 
  }
  
  protected boolean onTargetSelected(TargetInfo paramTargetInfo, boolean paramBoolean) {
    // Byte code:
    //   0: aload_1
    //   1: invokeinterface getResolveInfo : ()Landroid/content/pm/ResolveInfo;
    //   6: astore_3
    //   7: aload_1
    //   8: ifnull -> 22
    //   11: aload_1
    //   12: invokeinterface getResolvedIntent : ()Landroid/content/Intent;
    //   17: astore #4
    //   19: goto -> 25
    //   22: aconst_null
    //   23: astore #4
    //   25: aload #4
    //   27: ifnull -> 945
    //   30: aload_0
    //   31: getfield mSupportsAlwaysUseOption : Z
    //   34: ifne -> 54
    //   37: aload_0
    //   38: getfield mMultiProfilePagerAdapter : Lcom/android/internal/app/AbstractMultiProfilePagerAdapter;
    //   41: astore #5
    //   43: aload #5
    //   45: invokevirtual getActiveListAdapter : ()Lcom/android/internal/app/ResolverListAdapter;
    //   48: invokevirtual hasFilteredItem : ()Z
    //   51: ifeq -> 945
    //   54: aload_0
    //   55: getfield mMultiProfilePagerAdapter : Lcom/android/internal/app/AbstractMultiProfilePagerAdapter;
    //   58: astore #5
    //   60: aload #5
    //   62: invokevirtual getActiveListAdapter : ()Lcom/android/internal/app/ResolverListAdapter;
    //   65: invokevirtual getUnfilteredResolveList : ()Ljava/util/List;
    //   68: ifnull -> 945
    //   71: new android/content/IntentFilter
    //   74: dup
    //   75: invokespecial <init> : ()V
    //   78: astore #6
    //   80: aload #4
    //   82: invokevirtual getSelector : ()Landroid/content/Intent;
    //   85: ifnull -> 98
    //   88: aload #4
    //   90: invokevirtual getSelector : ()Landroid/content/Intent;
    //   93: astore #5
    //   95: goto -> 102
    //   98: aload #4
    //   100: astore #5
    //   102: aload #5
    //   104: invokevirtual getAction : ()Ljava/lang/String;
    //   107: astore #7
    //   109: aload #7
    //   111: ifnull -> 121
    //   114: aload #6
    //   116: aload #7
    //   118: invokevirtual addAction : (Ljava/lang/String;)V
    //   121: aload #5
    //   123: invokevirtual getCategories : ()Ljava/util/Set;
    //   126: astore #8
    //   128: aload #8
    //   130: ifnull -> 174
    //   133: aload #8
    //   135: invokeinterface iterator : ()Ljava/util/Iterator;
    //   140: astore #9
    //   142: aload #9
    //   144: invokeinterface hasNext : ()Z
    //   149: ifeq -> 174
    //   152: aload #9
    //   154: invokeinterface next : ()Ljava/lang/Object;
    //   159: checkcast java/lang/String
    //   162: astore #10
    //   164: aload #6
    //   166: aload #10
    //   168: invokevirtual addCategory : (Ljava/lang/String;)V
    //   171: goto -> 142
    //   174: aload #6
    //   176: ldc_w 'android.intent.category.DEFAULT'
    //   179: invokevirtual addCategory : (Ljava/lang/String;)V
    //   182: ldc_w 268369920
    //   185: aload_3
    //   186: getfield match : I
    //   189: iand
    //   190: istore #11
    //   192: aload #5
    //   194: invokevirtual getData : ()Landroid/net/Uri;
    //   197: astore #9
    //   199: aload #6
    //   201: astore #10
    //   203: iload #11
    //   205: ldc_w 6291456
    //   208: if_icmpne -> 255
    //   211: aload #5
    //   213: aload_0
    //   214: invokevirtual resolveType : (Landroid/content/Context;)Ljava/lang/String;
    //   217: astore #12
    //   219: aload #6
    //   221: astore #10
    //   223: aload #12
    //   225: ifnull -> 255
    //   228: aload #6
    //   230: aload #12
    //   232: invokevirtual addDataType : (Ljava/lang/String;)V
    //   235: aload #6
    //   237: astore #10
    //   239: goto -> 255
    //   242: astore #10
    //   244: ldc 'ResolverActivity'
    //   246: aload #10
    //   248: invokestatic w : (Ljava/lang/String;Ljava/lang/Throwable;)I
    //   251: pop
    //   252: aconst_null
    //   253: astore #10
    //   255: aload #9
    //   257: ifnull -> 565
    //   260: aload #9
    //   262: invokevirtual getScheme : ()Ljava/lang/String;
    //   265: ifnull -> 565
    //   268: iload #11
    //   270: ldc_w 6291456
    //   273: if_icmpne -> 304
    //   276: ldc_w 'file'
    //   279: aload #9
    //   281: invokevirtual getScheme : ()Ljava/lang/String;
    //   284: invokevirtual equals : (Ljava/lang/Object;)Z
    //   287: ifne -> 565
    //   290: ldc_w 'content'
    //   293: aload #9
    //   295: invokevirtual getScheme : ()Ljava/lang/String;
    //   298: invokevirtual equals : (Ljava/lang/Object;)Z
    //   301: ifne -> 565
    //   304: aload #10
    //   306: aload #9
    //   308: invokevirtual getScheme : ()Ljava/lang/String;
    //   311: invokevirtual addDataScheme : (Ljava/lang/String;)V
    //   314: aload_3
    //   315: getfield filter : Landroid/content/IntentFilter;
    //   318: invokevirtual schemeSpecificPartsIterator : ()Ljava/util/Iterator;
    //   321: astore #12
    //   323: aload #12
    //   325: ifnull -> 393
    //   328: aload #9
    //   330: invokevirtual getSchemeSpecificPart : ()Ljava/lang/String;
    //   333: astore #13
    //   335: aload #13
    //   337: ifnull -> 393
    //   340: aload #12
    //   342: invokeinterface hasNext : ()Z
    //   347: ifeq -> 393
    //   350: aload #12
    //   352: invokeinterface next : ()Ljava/lang/Object;
    //   357: checkcast android/os/PatternMatcher
    //   360: astore #6
    //   362: aload #6
    //   364: aload #13
    //   366: invokevirtual match : (Ljava/lang/String;)Z
    //   369: ifeq -> 390
    //   372: aload #10
    //   374: aload #6
    //   376: invokevirtual getPath : ()Ljava/lang/String;
    //   379: aload #6
    //   381: invokevirtual getType : ()I
    //   384: invokevirtual addDataSchemeSpecificPart : (Ljava/lang/String;I)V
    //   387: goto -> 393
    //   390: goto -> 335
    //   393: aload_3
    //   394: getfield filter : Landroid/content/IntentFilter;
    //   397: invokevirtual authoritiesIterator : ()Ljava/util/Iterator;
    //   400: astore #6
    //   402: aload #6
    //   404: ifnull -> 486
    //   407: aload #6
    //   409: invokeinterface hasNext : ()Z
    //   414: ifeq -> 486
    //   417: aload #6
    //   419: invokeinterface next : ()Ljava/lang/Object;
    //   424: checkcast android/content/IntentFilter$AuthorityEntry
    //   427: astore #12
    //   429: aload #12
    //   431: aload #9
    //   433: invokevirtual match : (Landroid/net/Uri;)I
    //   436: iflt -> 483
    //   439: aload #12
    //   441: invokevirtual getPort : ()I
    //   444: istore #11
    //   446: aload #12
    //   448: invokevirtual getHost : ()Ljava/lang/String;
    //   451: astore #12
    //   453: iload #11
    //   455: iflt -> 468
    //   458: iload #11
    //   460: invokestatic toString : (I)Ljava/lang/String;
    //   463: astore #6
    //   465: goto -> 471
    //   468: aconst_null
    //   469: astore #6
    //   471: aload #10
    //   473: aload #12
    //   475: aload #6
    //   477: invokevirtual addDataAuthority : (Ljava/lang/String;Ljava/lang/String;)V
    //   480: goto -> 486
    //   483: goto -> 407
    //   486: aload_3
    //   487: getfield filter : Landroid/content/IntentFilter;
    //   490: invokevirtual pathsIterator : ()Ljava/util/Iterator;
    //   493: astore #6
    //   495: aload #6
    //   497: ifnull -> 565
    //   500: aload #9
    //   502: invokevirtual getPath : ()Ljava/lang/String;
    //   505: astore #9
    //   507: aload #9
    //   509: ifnull -> 565
    //   512: aload #6
    //   514: invokeinterface hasNext : ()Z
    //   519: ifeq -> 565
    //   522: aload #6
    //   524: invokeinterface next : ()Ljava/lang/Object;
    //   529: checkcast android/os/PatternMatcher
    //   532: astore #12
    //   534: aload #12
    //   536: aload #9
    //   538: invokevirtual match : (Ljava/lang/String;)Z
    //   541: ifeq -> 562
    //   544: aload #10
    //   546: aload #12
    //   548: invokevirtual getPath : ()Ljava/lang/String;
    //   551: aload #12
    //   553: invokevirtual getType : ()I
    //   556: invokevirtual addDataPath : (Ljava/lang/String;I)V
    //   559: goto -> 565
    //   562: goto -> 507
    //   565: aload #10
    //   567: ifnull -> 945
    //   570: aload_0
    //   571: getfield mMultiProfilePagerAdapter : Lcom/android/internal/app/AbstractMultiProfilePagerAdapter;
    //   574: invokevirtual getActiveListAdapter : ()Lcom/android/internal/app/ResolverListAdapter;
    //   577: astore #6
    //   579: aload #6
    //   581: invokevirtual getUnfilteredResolveList : ()Ljava/util/List;
    //   584: invokeinterface size : ()I
    //   589: istore #14
    //   591: aload_0
    //   592: getfield mMultiProfilePagerAdapter : Lcom/android/internal/app/AbstractMultiProfilePagerAdapter;
    //   595: astore #6
    //   597: aload #6
    //   599: invokevirtual getActiveListAdapter : ()Lcom/android/internal/app/ResolverListAdapter;
    //   602: invokevirtual getOtherProfile : ()Lcom/android/internal/app/chooser/DisplayResolveInfo;
    //   605: ifnull -> 614
    //   608: iconst_1
    //   609: istore #15
    //   611: goto -> 617
    //   614: iconst_0
    //   615: istore #15
    //   617: iload #15
    //   619: ifne -> 632
    //   622: iload #14
    //   624: anewarray android/content/ComponentName
    //   627: astore #6
    //   629: goto -> 641
    //   632: iload #14
    //   634: iconst_1
    //   635: iadd
    //   636: anewarray android/content/ComponentName
    //   639: astore #6
    //   641: iconst_0
    //   642: istore #11
    //   644: iconst_0
    //   645: istore #16
    //   647: iload #16
    //   649: iload #14
    //   651: if_icmpge -> 743
    //   654: aload_0
    //   655: getfield mMultiProfilePagerAdapter : Lcom/android/internal/app/AbstractMultiProfilePagerAdapter;
    //   658: invokevirtual getActiveListAdapter : ()Lcom/android/internal/app/ResolverListAdapter;
    //   661: astore #9
    //   663: aload #9
    //   665: invokevirtual getUnfilteredResolveList : ()Ljava/util/List;
    //   668: iload #16
    //   670: invokeinterface get : (I)Ljava/lang/Object;
    //   675: checkcast com/android/internal/app/ResolverActivity$ResolvedComponentInfo
    //   678: iconst_0
    //   679: invokevirtual getResolveInfoAt : (I)Landroid/content/pm/ResolveInfo;
    //   682: astore #9
    //   684: aload #6
    //   686: iload #16
    //   688: new android/content/ComponentName
    //   691: dup
    //   692: aload #9
    //   694: getfield activityInfo : Landroid/content/pm/ActivityInfo;
    //   697: getfield packageName : Ljava/lang/String;
    //   700: aload #9
    //   702: getfield activityInfo : Landroid/content/pm/ActivityInfo;
    //   705: getfield name : Ljava/lang/String;
    //   708: invokespecial <init> : (Ljava/lang/String;Ljava/lang/String;)V
    //   711: aastore
    //   712: iload #11
    //   714: istore #17
    //   716: aload #9
    //   718: getfield match : I
    //   721: iload #11
    //   723: if_icmple -> 733
    //   726: aload #9
    //   728: getfield match : I
    //   731: istore #17
    //   733: iinc #16, 1
    //   736: iload #17
    //   738: istore #11
    //   740: goto -> 647
    //   743: iload #15
    //   745: ifeq -> 806
    //   748: aload_0
    //   749: getfield mMultiProfilePagerAdapter : Lcom/android/internal/app/AbstractMultiProfilePagerAdapter;
    //   752: invokevirtual getActiveListAdapter : ()Lcom/android/internal/app/ResolverListAdapter;
    //   755: astore #5
    //   757: aload #6
    //   759: iload #14
    //   761: aload #5
    //   763: invokevirtual getOtherProfile : ()Lcom/android/internal/app/chooser/DisplayResolveInfo;
    //   766: invokevirtual getResolvedComponentName : ()Landroid/content/ComponentName;
    //   769: aastore
    //   770: aload_0
    //   771: getfield mMultiProfilePagerAdapter : Lcom/android/internal/app/AbstractMultiProfilePagerAdapter;
    //   774: invokevirtual getActiveListAdapter : ()Lcom/android/internal/app/ResolverListAdapter;
    //   777: astore #5
    //   779: aload #5
    //   781: invokevirtual getOtherProfile : ()Lcom/android/internal/app/chooser/DisplayResolveInfo;
    //   784: invokevirtual getResolveInfo : ()Landroid/content/pm/ResolveInfo;
    //   787: getfield match : I
    //   790: istore #15
    //   792: iload #15
    //   794: iload #11
    //   796: if_icmple -> 806
    //   799: iload #15
    //   801: istore #11
    //   803: goto -> 806
    //   806: iload_2
    //   807: ifeq -> 880
    //   810: aload_0
    //   811: invokevirtual getUserId : ()I
    //   814: istore #15
    //   816: aload_0
    //   817: invokevirtual getPackageManager : ()Landroid/content/pm/PackageManager;
    //   820: astore #5
    //   822: aload #5
    //   824: aload #10
    //   826: iload #11
    //   828: aload #6
    //   830: aload #4
    //   832: invokevirtual getComponent : ()Landroid/content/ComponentName;
    //   835: invokevirtual addPreferredActivity : (Landroid/content/IntentFilter;I[Landroid/content/ComponentName;Landroid/content/ComponentName;)V
    //   838: aload_3
    //   839: getfield handleAllWebDataURI : Z
    //   842: ifeq -> 877
    //   845: aload #5
    //   847: iload #15
    //   849: invokevirtual getDefaultBrowserPackageNameAsUser : (I)Ljava/lang/String;
    //   852: astore #10
    //   854: aload #10
    //   856: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   859: ifeq -> 877
    //   862: aload #5
    //   864: aload_3
    //   865: getfield activityInfo : Landroid/content/pm/ActivityInfo;
    //   868: getfield packageName : Ljava/lang/String;
    //   871: iload #15
    //   873: invokevirtual setDefaultBrowserPackageNameAsUser : (Ljava/lang/String;I)Z
    //   876: pop
    //   877: goto -> 945
    //   880: aload_0
    //   881: aload_0
    //   882: getfield mMultiProfilePagerAdapter : Lcom/android/internal/app/AbstractMultiProfilePagerAdapter;
    //   885: invokevirtual getActiveListAdapter : ()Lcom/android/internal/app/ResolverListAdapter;
    //   888: getfield mResolverListController : Lcom/android/internal/app/ResolverListController;
    //   891: aload #4
    //   893: aload #10
    //   895: iload #11
    //   897: invokevirtual setLastChosen : (Lcom/android/internal/app/ResolverListController;Landroid/content/Intent;Landroid/content/IntentFilter;I)V
    //   900: goto -> 945
    //   903: astore #5
    //   905: new java/lang/StringBuilder
    //   908: dup
    //   909: invokespecial <init> : ()V
    //   912: astore #10
    //   914: aload #10
    //   916: ldc_w 'Error calling setLastChosenActivity\\n'
    //   919: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   922: pop
    //   923: aload #10
    //   925: aload #5
    //   927: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   930: pop
    //   931: ldc 'ResolverActivity'
    //   933: aload #10
    //   935: invokevirtual toString : ()Ljava/lang/String;
    //   938: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   941: pop
    //   942: goto -> 945
    //   945: aload_1
    //   946: ifnull -> 1013
    //   949: aload #4
    //   951: ifnull -> 993
    //   954: aload_0
    //   955: invokespecial isLaunchingTargetInOtherProfile : ()Z
    //   958: ifeq -> 993
    //   961: aload_0
    //   962: sipush #512
    //   965: invokevirtual hasCustomFlag : (I)Z
    //   968: ifne -> 993
    //   971: invokestatic getInstance : ()Lcom/oplus/multiapp/OplusMultiAppManager;
    //   974: aload_0
    //   975: getfield mLaunchedFromUid : I
    //   978: invokestatic getUserId : (I)I
    //   981: invokevirtual isMultiAppUserId : (I)Z
    //   984: ifne -> 993
    //   987: aload_0
    //   988: aload #4
    //   990: invokespecial prepareIntentForCrossProfileLaunch : (Landroid/content/Intent;)V
    //   993: aload_0
    //   994: aload_1
    //   995: invokevirtual safelyStartActivity : (Lcom/android/internal/app/chooser/TargetInfo;)V
    //   998: aload_0
    //   999: invokevirtual performAnimation : ()V
    //   1002: aload_1
    //   1003: invokeinterface isSuspended : ()Z
    //   1008: ifeq -> 1013
    //   1011: iconst_0
    //   1012: ireturn
    //   1013: iconst_1
    //   1014: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1144	-> 0
    //   #1145	-> 7
    //   #1147	-> 25
    //   #1148	-> 43
    //   #1149	-> 60
    //   #1151	-> 71
    //   #1154	-> 80
    //   #1155	-> 88
    //   #1157	-> 98
    //   #1160	-> 102
    //   #1161	-> 109
    //   #1162	-> 114
    //   #1164	-> 121
    //   #1165	-> 128
    //   #1166	-> 133
    //   #1167	-> 164
    //   #1168	-> 171
    //   #1170	-> 174
    //   #1172	-> 182
    //   #1173	-> 192
    //   #1174	-> 199
    //   #1175	-> 211
    //   #1176	-> 219
    //   #1178	-> 228
    //   #1182	-> 235
    //   #1179	-> 242
    //   #1180	-> 244
    //   #1181	-> 252
    //   #1185	-> 255
    //   #1189	-> 268
    //   #1190	-> 276
    //   #1191	-> 290
    //   #1192	-> 304
    //   #1196	-> 314
    //   #1197	-> 323
    //   #1198	-> 328
    //   #1199	-> 335
    //   #1200	-> 350
    //   #1201	-> 362
    //   #1202	-> 372
    //   #1203	-> 387
    //   #1205	-> 390
    //   #1207	-> 393
    //   #1208	-> 402
    //   #1209	-> 407
    //   #1210	-> 417
    //   #1211	-> 429
    //   #1212	-> 439
    //   #1213	-> 446
    //   #1214	-> 453
    //   #1213	-> 471
    //   #1215	-> 480
    //   #1217	-> 483
    //   #1219	-> 486
    //   #1220	-> 495
    //   #1221	-> 500
    //   #1222	-> 507
    //   #1223	-> 522
    //   #1224	-> 534
    //   #1225	-> 544
    //   #1226	-> 559
    //   #1228	-> 562
    //   #1233	-> 565
    //   #1234	-> 570
    //   #1235	-> 579
    //   #1240	-> 591
    //   #1241	-> 597
    //   #1242	-> 617
    //   #1243	-> 622
    //   #1245	-> 632
    //   #1248	-> 641
    //   #1249	-> 644
    //   #1250	-> 654
    //   #1251	-> 663
    //   #1252	-> 684
    //   #1254	-> 712
    //   #1249	-> 733
    //   #1257	-> 743
    //   #1258	-> 748
    //   #1259	-> 757
    //   #1260	-> 770
    //   #1261	-> 779
    //   #1262	-> 792
    //   #1265	-> 806
    //   #1266	-> 810
    //   #1267	-> 816
    //   #1270	-> 822
    //   #1272	-> 838
    //   #1274	-> 845
    //   #1275	-> 854
    //   #1276	-> 862
    //   #1279	-> 877
    //   #1288	-> 880
    //   #1292	-> 900
    //   #1290	-> 903
    //   #1291	-> 905
    //   #1233	-> 945
    //   #1297	-> 945
    //   #1298	-> 949
    //   #1303	-> 961
    //   #1304	-> 971
    //   #1305	-> 987
    //   #1309	-> 993
    //   #1313	-> 998
    //   #1317	-> 1002
    //   #1318	-> 1011
    //   #1322	-> 1013
    // Exception table:
    //   from	to	target	type
    //   228	235	242	android/content/IntentFilter$MalformedMimeTypeException
    //   880	900	903	android/os/RemoteException
  }
  
  private void prepareIntentForCrossProfileLaunch(Intent paramIntent) {
    paramIntent.fixUris(UserHandle.myUserId());
  }
  
  private boolean isLaunchingTargetInOtherProfile() {
    boolean bool;
    int i = this.mMultiProfilePagerAdapter.getCurrentUserHandle().getIdentifier();
    if (i != UserHandle.myUserId()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void safelyStartActivity(TargetInfo paramTargetInfo) {
    StrictMode.disableDeathOnFileUriExposure();
    try {
      safelyStartActivityInternal(paramTargetInfo);
      return;
    } finally {
      StrictMode.enableDeathOnFileUriExposure();
    } 
  }
  
  private void safelyStartActivityInternal(TargetInfo paramTargetInfo) {
    if (!paramTargetInfo.isSuspended()) {
      PackageMonitor packageMonitor = this.mPersonalPackageMonitor;
      if (packageMonitor != null)
        packageMonitor.unregister(); 
      packageMonitor = this.mWorkPackageMonitor;
      if (packageMonitor != null)
        packageMonitor.unregister(); 
      this.mRegistered = false;
    } 
    int i = this.mProfileSwitchMessageId;
    if (i != -1)
      Toast.makeText((Context)this, getString(i), 1).show(); 
    UserHandle userHandle = this.mMultiProfilePagerAdapter.getCurrentUserHandle();
    if (!this.mSafeForwardingMode) {
      if (paramTargetInfo.startAsUser(this, this.mBundle, userHandle)) {
        onActivityStarted(paramTargetInfo);
        maybeLogCrossProfileTargetLaunch(paramTargetInfo, userHandle);
      } 
      return;
    } 
    try {
      if (paramTargetInfo.startAsCaller(this, this.mBundle, userHandle.getIdentifier())) {
        onActivityStarted(paramTargetInfo);
        maybeLogCrossProfileTargetLaunch(paramTargetInfo, userHandle);
      } 
    } catch (RuntimeException runtimeException) {
      try {
        IActivityTaskManager iActivityTaskManager = ActivityTaskManager.getService();
        IBinder iBinder = getActivityToken();
        str = iActivityTaskManager.getLaunchedFromPackage(iBinder);
      } catch (RemoteException remoteException) {
        str = "??";
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to launch as uid ");
      stringBuilder.append(this.mLaunchedFromUid);
      stringBuilder.append(" package ");
      stringBuilder.append(str);
      stringBuilder.append(", while running in ");
      stringBuilder.append(ActivityThread.currentProcessName());
      String str = stringBuilder.toString();
      Slog.wtf("ResolverActivity", str, runtimeException);
    } 
  }
  
  private void maybeLogCrossProfileTargetLaunch(TargetInfo paramTargetInfo, UserHandle paramUserHandle) {
    String str1;
    if (!hasWorkProfile() || paramUserHandle.equals(getUser()))
      return; 
    DevicePolicyEventLogger devicePolicyEventLogger2 = DevicePolicyEventLogger.createEvent(155);
    devicePolicyEventLogger2 = devicePolicyEventLogger2.setBoolean(paramUserHandle.equals(getPersonalProfileUserHandle()));
    String str2 = getMetricsCategory();
    if (paramTargetInfo instanceof com.android.internal.app.chooser.ChooserTargetInfo) {
      str1 = "direct_share";
    } else {
      str1 = "other_target";
    } 
    DevicePolicyEventLogger devicePolicyEventLogger1 = devicePolicyEventLogger2.setStrings(new String[] { str2, str1 });
    devicePolicyEventLogger1.write();
  }
  
  public boolean startAsCallerImpl(Intent paramIntent, Bundle paramBundle, boolean paramBoolean, int paramInt) {
    try {
      IActivityTaskManager iActivityTaskManager = ActivityTaskManager.getService();
      IBinder iBinder = iActivityTaskManager.requestStartActivityPermissionToken(getActivityToken());
      Intent intent = new Intent();
      this();
      String str1 = Resources.getSystem().getString(17039852);
      ComponentName componentName = ComponentName.unflattenFromString(str1);
      str1 = componentName.getPackageName();
      String str2 = componentName.getClassName();
      intent.setClassName(str1, str2);
      intent.putExtra("android.app.extra.PERMISSION_TOKEN", iBinder);
      intent.putExtra("android.intent.extra.INTENT", (Parcelable)paramIntent);
      intent.putExtra("android.app.extra.OPTIONS", paramBundle);
      intent.putExtra("android.app.extra.EXTRA_IGNORE_TARGET_SECURITY", paramBoolean);
      intent.putExtra("android.intent.extra.USER_ID", paramInt);
      intent.addFlags(50331648);
      startActivity(intent);
    } catch (RemoteException remoteException) {
      Log.e("ResolverActivity", remoteException.toString());
    } 
    return true;
  }
  
  public void onActivityStarted(TargetInfo paramTargetInfo) {}
  
  public boolean shouldGetActivityMetadata() {
    return false;
  }
  
  public boolean shouldAutoLaunchSingleChoice(TargetInfo paramTargetInfo) {
    return paramTargetInfo.isSuspended() ^ true;
  }
  
  void showTargetDetails(ResolveInfo paramResolveInfo) {
    Intent intent2 = (new Intent()).setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
    String str = paramResolveInfo.activityInfo.packageName;
    Intent intent1 = intent2.setData(Uri.fromParts("package", str, null));
    intent1 = intent1.addFlags(524288);
    startActivityAsUser(intent1, this.mMultiProfilePagerAdapter.getCurrentUserHandle());
    performAnimation();
  }
  
  protected ResolverListAdapter createResolverListAdapter(Context paramContext, List<Intent> paramList, Intent[] paramArrayOfIntent, List<ResolveInfo> paramList1, boolean paramBoolean, UserHandle paramUserHandle) {
    Intent intent = getIntent();
    boolean bool = intent.getBooleanExtra("is_audio_capture_device", false);
    return 
      new ResolverListAdapter(paramContext, paramList, paramArrayOfIntent, paramList1, paramBoolean, createListController(paramUserHandle), this, bool);
  }
  
  protected ResolverListController createListController(UserHandle paramUserHandle) {
    PackageManager packageManager = this.mPm;
    Intent intent = getTargetIntent();
    return new ResolverListController((Context)this, packageManager, intent, getReferrerPackageName(), this.mLaunchedFromUid, paramUserHandle);
  }
  
  private boolean configureContentView() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mMultiProfilePagerAdapter : Lcom/android/internal/app/AbstractMultiProfilePagerAdapter;
    //   4: invokevirtual getActiveListAdapter : ()Lcom/android/internal/app/ResolverListAdapter;
    //   7: ifnull -> 179
    //   10: aload_0
    //   11: getfield mMultiProfilePagerAdapter : Lcom/android/internal/app/AbstractMultiProfilePagerAdapter;
    //   14: astore_1
    //   15: iconst_1
    //   16: istore_2
    //   17: aload_1
    //   18: iconst_1
    //   19: invokevirtual rebuildActiveTab : (Z)Z
    //   22: ifne -> 48
    //   25: aload_0
    //   26: getfield mMultiProfilePagerAdapter : Lcom/android/internal/app/AbstractMultiProfilePagerAdapter;
    //   29: astore_1
    //   30: aload_1
    //   31: invokevirtual getActiveListAdapter : ()Lcom/android/internal/app/ResolverListAdapter;
    //   34: invokevirtual isTabLoaded : ()Z
    //   37: ifeq -> 43
    //   40: goto -> 48
    //   43: iconst_0
    //   44: istore_3
    //   45: goto -> 50
    //   48: iconst_1
    //   49: istore_3
    //   50: iload_3
    //   51: istore #4
    //   53: aload_0
    //   54: invokevirtual shouldShowTabs : ()Z
    //   57: ifeq -> 117
    //   60: aload_0
    //   61: getfield mMultiProfilePagerAdapter : Lcom/android/internal/app/AbstractMultiProfilePagerAdapter;
    //   64: iconst_0
    //   65: invokevirtual rebuildInactiveTab : (Z)Z
    //   68: ifne -> 95
    //   71: aload_0
    //   72: getfield mMultiProfilePagerAdapter : Lcom/android/internal/app/AbstractMultiProfilePagerAdapter;
    //   75: astore_1
    //   76: aload_1
    //   77: invokevirtual getInactiveListAdapter : ()Lcom/android/internal/app/ResolverListAdapter;
    //   80: invokevirtual isTabLoaded : ()Z
    //   83: ifeq -> 89
    //   86: goto -> 95
    //   89: iconst_0
    //   90: istore #5
    //   92: goto -> 98
    //   95: iconst_1
    //   96: istore #5
    //   98: iload_3
    //   99: ifeq -> 112
    //   102: iload #5
    //   104: ifeq -> 112
    //   107: iload_2
    //   108: istore_3
    //   109: goto -> 114
    //   112: iconst_0
    //   113: istore_3
    //   114: iload_3
    //   115: istore #4
    //   117: aload_0
    //   118: invokevirtual useLayoutWithDefault : ()Z
    //   121: ifeq -> 133
    //   124: aload_0
    //   125: ldc 17367276
    //   127: putfield mLayoutId : I
    //   130: goto -> 141
    //   133: aload_0
    //   134: aload_0
    //   135: invokevirtual getLayoutResource : ()I
    //   138: putfield mLayoutId : I
    //   141: aload_0
    //   142: aload_0
    //   143: getfield mLayoutId : I
    //   146: aload_0
    //   147: getfield mMultiProfilePagerAdapter : Lcom/android/internal/app/AbstractMultiProfilePagerAdapter;
    //   150: ldc_w 16909322
    //   153: invokevirtual setOriginContentView : (ILcom/android/internal/app/AbstractMultiProfilePagerAdapter;I)V
    //   156: aload_0
    //   157: invokevirtual initActionSend : ()V
    //   160: aload_0
    //   161: aload_0
    //   162: getfield mSafeForwardingMode : Z
    //   165: aload_0
    //   166: getfield mSupportsAlwaysUseOption : Z
    //   169: invokevirtual initView : (ZZ)V
    //   172: aload_0
    //   173: iload #4
    //   175: invokevirtual postRebuildList : (Z)Z
    //   178: ireturn
    //   179: new java/lang/IllegalStateException
    //   182: dup
    //   183: ldc_w 'mMultiProfilePagerAdapter.getCurrentListAdapter() cannot be null.'
    //   186: invokespecial <init> : (Ljava/lang/String;)V
    //   189: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1498	-> 0
    //   #1504	-> 10
    //   #1505	-> 30
    //   #1506	-> 50
    //   #1507	-> 60
    //   #1508	-> 76
    //   #1509	-> 98
    //   #1512	-> 117
    //   #1513	-> 124
    //   #1515	-> 133
    //   #1525	-> 141
    //   #1526	-> 156
    //   #1527	-> 160
    //   #1529	-> 172
    //   #1499	-> 179
  }
  
  protected boolean postRebuildList(boolean paramBoolean) {
    return postRebuildListInternal(paramBoolean);
  }
  
  final boolean postRebuildListInternal(boolean paramBoolean) {
    this.mMultiProfilePagerAdapter.getActiveListAdapter().getUnfilteredCount();
    if (paramBoolean && maybeAutolaunchActivity())
      return true; 
    if (!isOriginUi())
      return false; 
    setupViewVisibilities();
    if (shouldShowTabs())
      setupProfileTabs(); 
    return false;
  }
  
  private int isPermissionGranted(String paramString, int paramInt) {
    return ActivityManager.checkComponentPermission(paramString, paramInt, -1, true);
  }
  
  private boolean maybeAutolaunchActivity() {
    if (!addExtraOneAppFinish())
      return false; 
    int i = this.mMultiProfilePagerAdapter.getItemCount();
    if (i == 1 && maybeAutolaunchIfSingleTarget())
      return true; 
    if (i == 2) {
      AbstractMultiProfilePagerAdapter abstractMultiProfilePagerAdapter = this.mMultiProfilePagerAdapter;
      if (abstractMultiProfilePagerAdapter.getActiveListAdapter().isTabLoaded()) {
        abstractMultiProfilePagerAdapter = this.mMultiProfilePagerAdapter;
        if (abstractMultiProfilePagerAdapter.getInactiveListAdapter().isTabLoaded() && (
          maybeAutolaunchIfNoAppsOnInactiveTab() || 
          maybeAutolaunchIfCrossProfileSupported()))
          return true; 
      } 
    } 
    return false;
  }
  
  private boolean maybeAutolaunchIfSingleTarget() {
    int i = this.mMultiProfilePagerAdapter.getActiveListAdapter().getUnfilteredCount();
    if (i != 1)
      return false; 
    if (this.mMultiProfilePagerAdapter.getActiveListAdapter().getOtherProfile() != null)
      return false; 
    ResolverListAdapter resolverListAdapter = this.mMultiProfilePagerAdapter.getActiveListAdapter();
    TargetInfo targetInfo = resolverListAdapter.targetInfoForPosition(0, false);
    if (shouldAutoLaunchSingleChoice(targetInfo)) {
      safelyStartActivity(targetInfo);
      finish();
      return true;
    } 
    return false;
  }
  
  private boolean maybeAutolaunchIfNoAppsOnInactiveTab() {
    int i = this.mMultiProfilePagerAdapter.getActiveListAdapter().getUnfilteredCount();
    if (i != 1)
      return false; 
    AbstractMultiProfilePagerAdapter abstractMultiProfilePagerAdapter = this.mMultiProfilePagerAdapter;
    ResolverListAdapter resolverListAdapter = abstractMultiProfilePagerAdapter.getInactiveListAdapter();
    if (resolverListAdapter.getUnfilteredCount() != 0)
      return false; 
    resolverListAdapter = this.mMultiProfilePagerAdapter.getActiveListAdapter();
    TargetInfo targetInfo = resolverListAdapter.targetInfoForPosition(0, false);
    safelyStartActivity(targetInfo);
    finish();
    return true;
  }
  
  private boolean maybeAutolaunchIfCrossProfileSupported() {
    ResolverListAdapter resolverListAdapter1 = this.mMultiProfilePagerAdapter.getActiveListAdapter();
    int i = resolverListAdapter1.getUnfilteredCount();
    if (i != 1)
      return false; 
    AbstractMultiProfilePagerAdapter abstractMultiProfilePagerAdapter = this.mMultiProfilePagerAdapter;
    ResolverListAdapter resolverListAdapter2 = abstractMultiProfilePagerAdapter.getInactiveListAdapter();
    if (resolverListAdapter2.getUnfilteredCount() != 1)
      return false; 
    TargetInfo targetInfo1 = resolverListAdapter1.targetInfoForPosition(0, false);
    TargetInfo targetInfo2 = resolverListAdapter2.targetInfoForPosition(0, false);
    ComponentName componentName1 = targetInfo1.getResolvedComponentName();
    ComponentName componentName2 = targetInfo2.getResolvedComponentName();
    if (!Objects.equals(componentName1, componentName2))
      return false; 
    if (!shouldAutoLaunchSingleChoice(targetInfo1))
      return false; 
    String str = targetInfo1.getResolvedComponentName().getPackageName();
    if (!canAppInteractCrossProfiles(str))
      return false; 
    DevicePolicyEventLogger devicePolicyEventLogger2 = DevicePolicyEventLogger.createEvent(161);
    UserHandle userHandle = resolverListAdapter1.getUserHandle();
    boolean bool = userHandle.equals(getPersonalProfileUserHandle());
    DevicePolicyEventLogger devicePolicyEventLogger1 = devicePolicyEventLogger2.setBoolean(bool);
    devicePolicyEventLogger1 = devicePolicyEventLogger1.setStrings(new String[] { getMetricsCategory() });
    devicePolicyEventLogger1.write();
    safelyStartActivity(targetInfo1);
    finish();
    return true;
  }
  
  private boolean canAppInteractCrossProfiles(String paramString) {
    try {
      ApplicationInfo applicationInfo = getPackageManager().getApplicationInfo(paramString, 0);
      if (!applicationInfo.crossProfile)
        return false; 
      int i = applicationInfo.uid;
      if (isPermissionGranted("android.permission.INTERACT_ACROSS_USERS_FULL", i) == 0)
        return true; 
      if (isPermissionGranted("android.permission.INTERACT_ACROSS_USERS", i) == 0)
        return true; 
      if (PermissionChecker.checkPermissionForPreflight((Context)this, "android.permission.INTERACT_ACROSS_PROFILES", -1, i, paramString) == 0)
        return true; 
      return false;
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Package ");
      stringBuilder.append(paramString);
      stringBuilder.append(" does not exist on current user.");
      Log.e("ResolverActivity", stringBuilder.toString());
      return false;
    } 
  }
  
  private boolean isAutolaunching() {
    boolean bool;
    if (!this.mRegistered && isFinishing()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void setupProfileTabs() {
    maybeHideDivider();
    TabHost tabHost = (TabHost)findViewById(16909323);
    tabHost.setup();
    ViewPager viewPager = (ViewPager)findViewById(16909322);
    viewPager.setSaveEnabled(false);
    TabHost.TabSpec tabSpec = tabHost.newTabSpec("personal");
    tabSpec = tabSpec.setContent(16909322);
    tabSpec = tabSpec.setIndicator(getString(17041184));
    tabHost.addTab(tabSpec);
    tabSpec = tabHost.newTabSpec("work");
    tabSpec = tabSpec.setContent(16909322);
    tabSpec = tabSpec.setIndicator(getString(17041188));
    tabHost.addTab(tabSpec);
    TabWidget tabWidget = tabHost.getTabWidget();
    tabWidget.setVisibility(0);
    resetTabsHeaderStyle(tabWidget);
    updateActiveTabStyle(tabHost);
    tabHost.setOnTabChangedListener(new _$$Lambda$ResolverActivity$Qd_qc3_dQh5Hf5xmWHC3HV3_87E(this, tabWidget, tabHost, viewPager));
    viewPager.setVisibility(0);
    tabHost.setCurrentTab(this.mMultiProfilePagerAdapter.getCurrentPage());
    this.mMultiProfilePagerAdapter.setOnProfileSelectedListener((AbstractMultiProfilePagerAdapter.OnProfileSelectedListener)new Object(this, tabHost));
    this.mMultiProfilePagerAdapter.setOnSwitchOnWorkSelectedListener(new _$$Lambda$ResolverActivity$eYX_xfnxStZuzWl7sU7dSWSkNY4(tabHost));
    findViewById(16909359).setVisibility(0);
  }
  
  void onHorizontalSwipeStateChanged(int paramInt) {}
  
  private void maybeHideDivider() {
    if (!isIntentPicker())
      return; 
    View view = findViewById(16908933);
    if (view == null)
      return; 
    view.setVisibility(8);
  }
  
  protected void onProfileTabSelected() {}
  
  private void resetCheckedItem() {
    if (!isIntentPicker())
      return; 
    this.mLastSelected = -1;
    ListView listView = (ListView)this.mMultiProfilePagerAdapter.getInactiveAdapterView();
    if (listView.getCheckedItemCount() > 0)
      listView.setItemChecked(listView.getCheckedItemPosition(), false); 
  }
  
  private void resetTabsHeaderStyle(TabWidget paramTabWidget) {
    String str1 = getString(17041189);
    String str2 = getString(17041185);
    for (byte b = 0; b < paramTabWidget.getChildCount(); b++) {
      View view = paramTabWidget.getChildAt(b);
      TextView textView = view.<TextView>findViewById(16908310);
      textView.setTextAppearance(16974264);
      textView.setTextColor(getAttrColor((Context)this, 16843282));
      float f = getResources().getDimension(17105451);
      textView.setTextSize(0, f);
      if (textView.getText().equals(getString(17041184))) {
        view.setContentDescription(str2);
      } else if (textView.getText().equals(getString(17041188))) {
        view.setContentDescription(str1);
      } 
    } 
  }
  
  private static int getAttrColor(Context paramContext, int paramInt) {
    TypedArray typedArray = paramContext.obtainStyledAttributes(new int[] { paramInt });
    paramInt = typedArray.getColor(0, 0);
    typedArray.recycle();
    return paramInt;
  }
  
  private void updateActiveTabStyle(TabHost paramTabHost) {
    View view = paramTabHost.getTabWidget().getChildAt(paramTabHost.getCurrentTab());
    view = view.<TextView>findViewById(16908310);
    view.setTextColor(getAttrColor((Context)this, 16843829));
  }
  
  private void setupViewVisibilities() {
    ResolverListAdapter resolverListAdapter = this.mMultiProfilePagerAdapter.getActiveListAdapter();
    if (!this.mMultiProfilePagerAdapter.shouldShowEmptyStateScreen(resolverListAdapter))
      addUseDifferentAppLabelIfNecessary(resolverListAdapter); 
  }
  
  public void addUseDifferentAppLabelIfNecessary(ResolverListAdapter paramResolverListAdapter) {
    boolean bool = paramResolverListAdapter.hasFilteredItem();
    if (bool) {
      FrameLayout frameLayout = (FrameLayout)findViewById(16909483);
      frameLayout.setVisibility(0);
      TextView textView = (TextView)LayoutInflater.from((Context)this).inflate(17367272, (ViewGroup)null, false);
      if (shouldShowTabs())
        textView.setGravity(17); 
      frameLayout.addView(textView);
    } 
  }
  
  private void setupAdapterListView(ListView paramListView, ItemClickListener paramItemClickListener) {
    paramListView.setOnItemClickListener(paramItemClickListener);
    paramListView.setOnItemLongClickListener(paramItemClickListener);
    if (this.mSupportsAlwaysUseOption)
      paramListView.setChoiceMode(1); 
  }
  
  private void maybeCreateHeader(ResolverListAdapter paramResolverListAdapter) {
    if (!isOriginUi())
      return; 
    if (this.mHeaderCreatorUser != null && 
      !paramResolverListAdapter.getUserHandle().equals(this.mHeaderCreatorUser))
      return; 
    if (!shouldShowTabs() && 
      paramResolverListAdapter.getCount() == 0 && paramResolverListAdapter.getPlaceholderCount() == 0) {
      TextView textView = (TextView)findViewById(16908310);
      if (textView != null)
        textView.setVisibility(8); 
    } 
    CharSequence charSequence = this.mTitle;
    if (charSequence == null)
      charSequence = getTitleForAction(getTargetIntent(), this.mDefaultTitleResId); 
    if (!TextUtils.isEmpty(charSequence)) {
      TextView textView = (TextView)findViewById(16908310);
      if (textView != null)
        textView.setText(charSequence); 
      setTitle(charSequence);
    } 
    ImageView imageView = (ImageView)findViewById(16908294);
    if (imageView != null)
      paramResolverListAdapter.loadFilteredItemIconTaskAsync(imageView); 
    this.mHeaderCreatorUser = paramResolverListAdapter.getUserHandle();
  }
  
  protected void resetButtonBar() {
    if (!isOriginUi())
      return; 
    if (!this.mSupportsAlwaysUseOption)
      return; 
    ViewGroup viewGroup = (ViewGroup)findViewById(16908811);
    if (viewGroup == null) {
      Log.e("ResolverActivity", "Layout unexpectedly does not have a button bar");
      return;
    } 
    AbstractMultiProfilePagerAdapter abstractMultiProfilePagerAdapter = this.mMultiProfilePagerAdapter;
    ResolverListAdapter resolverListAdapter = abstractMultiProfilePagerAdapter.getActiveListAdapter();
    View view = findViewById(16909350);
    if (!useLayoutWithDefault()) {
      byte b;
      Insets insets = this.mSystemWindowInsets;
      if (insets != null) {
        b = insets.bottom;
      } else {
        b = 0;
      } 
      int i = viewGroup.getPaddingLeft(), j = viewGroup.getPaddingTop();
      int k = viewGroup.getPaddingRight(), m = getResources().getDimensionPixelSize(17105436);
      viewGroup.setPadding(i, j, k, m + b);
    } 
    if (resolverListAdapter.isTabLoaded()) {
      AbstractMultiProfilePagerAdapter abstractMultiProfilePagerAdapter1 = this.mMultiProfilePagerAdapter;
      if (abstractMultiProfilePagerAdapter1.shouldShowEmptyStateScreen(resolverListAdapter) && 
        !useLayoutWithDefault()) {
        viewGroup.setVisibility(4);
        if (view != null)
          view.setVisibility(4); 
        setButtonBarIgnoreOffset(false);
        return;
      } 
    } 
    if (view != null)
      view.setVisibility(0); 
    viewGroup.setVisibility(0);
    setButtonBarIgnoreOffset(true);
    this.mOnceButton = viewGroup.<Button>findViewById(16908813);
    this.mAlwaysButton = viewGroup.<Button>findViewById(16908810);
    resetAlwaysOrOnceButtonBar();
  }
  
  private void setButtonBarIgnoreOffset(boolean paramBoolean) {
    View view = findViewById(16908812);
    if (view != null) {
      ResolverDrawerLayout.LayoutParams layoutParams = (ResolverDrawerLayout.LayoutParams)view.getLayoutParams();
      layoutParams.ignoreOffset = paramBoolean;
      view.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
    } 
  }
  
  private void resetAlwaysOrOnceButtonBar() {
    setAlwaysButtonEnabled(false, -1, false);
    this.mOnceButton.setEnabled(false);
    ResolverListAdapter resolverListAdapter = this.mMultiProfilePagerAdapter.getActiveListAdapter();
    int i = resolverListAdapter.getFilteredPosition();
    if (useLayoutWithDefault() && i != -1) {
      setAlwaysButtonEnabled(true, i, false);
      this.mOnceButton.setEnabled(true);
      this.mOnceButton.requestFocus();
      return;
    } 
    ListView listView = (ListView)this.mMultiProfilePagerAdapter.getActiveAdapterView();
    if (listView != null && 
      listView.getCheckedItemPosition() != -1) {
      setAlwaysButtonEnabled(true, listView.getCheckedItemPosition(), true);
      this.mOnceButton.setEnabled(true);
    } 
  }
  
  public boolean useLayoutWithDefault() {
    boolean bool;
    int i = this.mMultiProfilePagerAdapter.getCurrentUserHandle().getIdentifier();
    if (i == UserHandle.myUserId()) {
      AbstractMultiProfilePagerAdapter abstractMultiProfilePagerAdapter = this.mMultiProfilePagerAdapter;
      bool = abstractMultiProfilePagerAdapter.getActiveListAdapter().hasFilteredItem();
    } else {
      AbstractMultiProfilePagerAdapter abstractMultiProfilePagerAdapter = this.mMultiProfilePagerAdapter;
      bool = abstractMultiProfilePagerAdapter.getInactiveListAdapter().hasFilteredItem();
    } 
    if (this.mSupportsAlwaysUseOption && bool) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected void setRetainInOnStop(boolean paramBoolean) {
    this.mRetainInOnStop = paramBoolean;
  }
  
  public boolean resolveInfoMatch(ResolveInfo paramResolveInfo1, ResolveInfo paramResolveInfo2) {
    boolean bool = true;
    if (paramResolveInfo1 == null) {
      if (paramResolveInfo2 != null)
        bool = false; 
    } else if (paramResolveInfo1.activityInfo == null) {
      if (paramResolveInfo2.activityInfo != null)
        bool = false; 
    } else {
      String str1 = paramResolveInfo1.activityInfo.name, str2 = paramResolveInfo2.activityInfo.name;
      if (Objects.equals(str1, str2)) {
        String str3 = paramResolveInfo1.activityInfo.packageName, str4 = paramResolveInfo2.activityInfo.packageName;
        if (Objects.equals(str3, str4))
          return bool; 
      } 
      bool = false;
    } 
    return bool;
  }
  
  protected String getMetricsCategory() {
    return "intent_resolver";
  }
  
  public void onHandlePackagesChanged(ResolverListAdapter paramResolverListAdapter) {
    if (paramResolverListAdapter == this.mMultiProfilePagerAdapter.getActiveListAdapter()) {
      if (paramResolverListAdapter.getUserHandle().equals(getWorkProfileUserHandle())) {
        AbstractMultiProfilePagerAdapter abstractMultiProfilePagerAdapter = this.mMultiProfilePagerAdapter;
        if (abstractMultiProfilePagerAdapter.isWaitingToEnableWorkProfile())
          return; 
      } 
      boolean bool = this.mMultiProfilePagerAdapter.rebuildActiveTab(true);
      if (bool) {
        AbstractMultiProfilePagerAdapter abstractMultiProfilePagerAdapter = this.mMultiProfilePagerAdapter;
        ResolverListAdapter resolverListAdapter = abstractMultiProfilePagerAdapter.getActiveListAdapter();
        resolverListAdapter.notifyDataSetChanged();
        if (resolverListAdapter.getCount() == 0 && !inactiveListAdapterHasItems())
          finish(); 
      } 
    } else {
      this.mMultiProfilePagerAdapter.clearInactiveProfileCache();
    } 
  }
  
  private boolean inactiveListAdapterHasItems() {
    boolean bool = shouldShowTabs();
    boolean bool1 = false;
    if (!bool)
      return false; 
    if (this.mMultiProfilePagerAdapter.getInactiveListAdapter().getCount() > 0)
      bool1 = true; 
    return bool1;
  }
  
  private BroadcastReceiver createWorkProfileStateReceiver() {
    return (BroadcastReceiver)new Object(this);
  }
  
  class ResolvedComponentInfo {
    private final List<Intent> mIntents = new ArrayList<>();
    
    private boolean mPinned;
    
    private final List<ResolveInfo> mResolveInfos = new ArrayList<>();
    
    public final ComponentName name;
    
    public ResolvedComponentInfo(ResolverActivity this$0, Intent param1Intent, ResolveInfo param1ResolveInfo) {
      this.name = (ComponentName)this$0;
      add(param1Intent, param1ResolveInfo);
    }
    
    public void add(Intent param1Intent, ResolveInfo param1ResolveInfo) {
      this.mIntents.add(param1Intent);
      this.mResolveInfos.add(param1ResolveInfo);
    }
    
    public int getCount() {
      return this.mIntents.size();
    }
    
    public Intent getIntentAt(int param1Int) {
      Intent intent;
      if (param1Int >= 0) {
        intent = this.mIntents.get(param1Int);
      } else {
        intent = null;
      } 
      return intent;
    }
    
    public ResolveInfo getResolveInfoAt(int param1Int) {
      ResolveInfo resolveInfo;
      if (param1Int >= 0) {
        resolveInfo = this.mResolveInfos.get(param1Int);
      } else {
        resolveInfo = null;
      } 
      return resolveInfo;
    }
    
    public int findIntent(Intent param1Intent) {
      byte b;
      int i;
      for (b = 0, i = this.mIntents.size(); b < i; b++) {
        if (param1Intent.equals(this.mIntents.get(b)))
          return b; 
      } 
      return -1;
    }
    
    public int findResolveInfo(ResolveInfo param1ResolveInfo) {
      byte b;
      int i;
      for (b = 0, i = this.mResolveInfos.size(); b < i; b++) {
        if (param1ResolveInfo.equals(this.mResolveInfos.get(b)))
          return b; 
      } 
      return -1;
    }
    
    public boolean isPinned() {
      return this.mPinned;
    }
    
    public void setPinned(boolean param1Boolean) {
      this.mPinned = param1Boolean;
    }
  }
  
  class ItemClickListener implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {
    final ResolverActivity this$0;
    
    public void onItemClick(AdapterView<?> param1AdapterView, View param1View, int param1Int, long param1Long) {
      // Byte code:
      //   0: aload_1
      //   1: instanceof android/widget/ListView
      //   4: ifeq -> 15
      //   7: aload_1
      //   8: checkcast android/widget/ListView
      //   11: astore_1
      //   12: goto -> 17
      //   15: aconst_null
      //   16: astore_1
      //   17: iload_3
      //   18: istore #6
      //   20: aload_1
      //   21: ifnull -> 32
      //   24: iload_3
      //   25: aload_1
      //   26: invokevirtual getHeaderViewsCount : ()I
      //   29: isub
      //   30: istore #6
      //   32: iload #6
      //   34: ifge -> 38
      //   37: return
      //   38: aload_0
      //   39: getfield this$0 : Lcom/android/internal/app/ResolverActivity;
      //   42: getfield mMultiProfilePagerAdapter : Lcom/android/internal/app/AbstractMultiProfilePagerAdapter;
      //   45: invokevirtual getActiveListAdapter : ()Lcom/android/internal/app/ResolverListAdapter;
      //   48: astore_1
      //   49: aload_1
      //   50: iload #6
      //   52: iconst_1
      //   53: invokevirtual resolveInfoForPosition : (IZ)Landroid/content/pm/ResolveInfo;
      //   56: ifnonnull -> 60
      //   59: return
      //   60: aload_0
      //   61: getfield this$0 : Lcom/android/internal/app/ResolverActivity;
      //   64: getfield mMultiProfilePagerAdapter : Lcom/android/internal/app/AbstractMultiProfilePagerAdapter;
      //   67: astore_1
      //   68: aload_1
      //   69: invokevirtual getActiveAdapterView : ()Landroid/view/ViewGroup;
      //   72: checkcast android/widget/ListView
      //   75: astore_1
      //   76: aload_1
      //   77: invokevirtual getCheckedItemPosition : ()I
      //   80: istore_3
      //   81: iload_3
      //   82: iconst_m1
      //   83: if_icmpeq -> 92
      //   86: iconst_1
      //   87: istore #7
      //   89: goto -> 95
      //   92: iconst_0
      //   93: istore #7
      //   95: aload_0
      //   96: getfield this$0 : Lcom/android/internal/app/ResolverActivity;
      //   99: invokevirtual useLayoutWithDefault : ()Z
      //   102: ifne -> 191
      //   105: iload #7
      //   107: ifeq -> 123
      //   110: aload_0
      //   111: getfield this$0 : Lcom/android/internal/app/ResolverActivity;
      //   114: astore_2
      //   115: aload_2
      //   116: invokestatic access$300 : (Lcom/android/internal/app/ResolverActivity;)I
      //   119: iload_3
      //   120: if_icmpeq -> 191
      //   123: aload_0
      //   124: getfield this$0 : Lcom/android/internal/app/ResolverActivity;
      //   127: astore_2
      //   128: aload_2
      //   129: invokestatic access$400 : (Lcom/android/internal/app/ResolverActivity;)Landroid/widget/Button;
      //   132: ifnull -> 191
      //   135: aload_0
      //   136: getfield this$0 : Lcom/android/internal/app/ResolverActivity;
      //   139: iload #7
      //   141: iload_3
      //   142: iconst_1
      //   143: invokestatic access$500 : (Lcom/android/internal/app/ResolverActivity;ZIZ)V
      //   146: aload_0
      //   147: getfield this$0 : Lcom/android/internal/app/ResolverActivity;
      //   150: invokestatic access$600 : (Lcom/android/internal/app/ResolverActivity;)Landroid/widget/Button;
      //   153: iload #7
      //   155: invokevirtual setEnabled : (Z)V
      //   158: iload #7
      //   160: ifeq -> 179
      //   163: aload_1
      //   164: iload_3
      //   165: invokevirtual smoothScrollToPosition : (I)V
      //   168: aload_0
      //   169: getfield this$0 : Lcom/android/internal/app/ResolverActivity;
      //   172: invokestatic access$600 : (Lcom/android/internal/app/ResolverActivity;)Landroid/widget/Button;
      //   175: invokevirtual requestFocus : ()Z
      //   178: pop
      //   179: aload_0
      //   180: getfield this$0 : Lcom/android/internal/app/ResolverActivity;
      //   183: iload_3
      //   184: invokestatic access$302 : (Lcom/android/internal/app/ResolverActivity;I)I
      //   187: pop
      //   188: goto -> 202
      //   191: aload_0
      //   192: getfield this$0 : Lcom/android/internal/app/ResolverActivity;
      //   195: iload #6
      //   197: iconst_0
      //   198: iconst_1
      //   199: invokevirtual startSelected : (IZZ)V
      //   202: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #2187	-> 0
      //   #2188	-> 17
      //   #2189	-> 24
      //   #2191	-> 32
      //   #2193	-> 37
      //   #2196	-> 38
      //   #2197	-> 49
      //   #2198	-> 59
      //   #2200	-> 60
      //   #2201	-> 68
      //   #2202	-> 76
      //   #2203	-> 81
      //   #2204	-> 95
      //   #2205	-> 115
      //   #2206	-> 128
      //   #2207	-> 135
      //   #2208	-> 146
      //   #2209	-> 158
      //   #2210	-> 163
      //   #2211	-> 168
      //   #2213	-> 179
      //   #2215	-> 191
      //   #2217	-> 202
    }
    
    public boolean onItemLongClick(AdapterView<?> param1AdapterView, View param1View, int param1Int, long param1Long) {
      if (param1AdapterView instanceof ListView) {
        param1AdapterView = param1AdapterView;
      } else {
        param1AdapterView = null;
      } 
      int i = param1Int;
      if (param1AdapterView != null)
        i = param1Int - param1AdapterView.getHeaderViewsCount(); 
      if (i < 0)
        return false; 
      ResolverListAdapter resolverListAdapter = ResolverActivity.this.mMultiProfilePagerAdapter.getActiveListAdapter();
      ResolveInfo resolveInfo = resolverListAdapter.resolveInfoForPosition(i, true);
      ResolverActivity.this.showTargetDetails(resolveInfo);
      return true;
    }
  }
  
  static final boolean isSpecificUriMatch(int paramInt) {
    boolean bool;
    paramInt &= 0xFFF0000;
    if (paramInt >= 3145728 && paramInt <= 5242880) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  class PickTargetOptionRequest extends VoiceInteractor.PickOptionRequest {
    public PickTargetOptionRequest(ResolverActivity this$0, VoiceInteractor.PickOptionRequest.Option[] param1ArrayOfOption, Bundle param1Bundle) {
      super((VoiceInteractor.Prompt)this$0, param1ArrayOfOption, param1Bundle);
    }
    
    public void onCancel() {
      super.onCancel();
      ResolverActivity resolverActivity = (ResolverActivity)getActivity();
      if (resolverActivity != null) {
        ResolverActivity.access$702(resolverActivity, (PickTargetOptionRequest)null);
        resolverActivity.finish();
      } 
    }
    
    public void onPickOptionResult(boolean param1Boolean, VoiceInteractor.PickOptionRequest.Option[] param1ArrayOfOption, Bundle param1Bundle) {
      super.onPickOptionResult(param1Boolean, param1ArrayOfOption, param1Bundle);
      if (param1ArrayOfOption.length != 1)
        return; 
      ResolverActivity resolverActivity = (ResolverActivity)getActivity();
      if (resolverActivity != null) {
        ResolverListAdapter resolverListAdapter = resolverActivity.mMultiProfilePagerAdapter.getActiveListAdapter();
        VoiceInteractor.PickOptionRequest.Option option = param1ArrayOfOption[0];
        TargetInfo targetInfo = resolverListAdapter.getItem(option.getIndex());
        if (resolverActivity.onTargetSelected(targetInfo, false)) {
          ResolverActivity.access$702(resolverActivity, (PickTargetOptionRequest)null);
          resolverActivity.finish();
        } 
      } 
    }
  }
  
  protected void maybeLogProfileChange() {}
  
  protected AbstractMultiProfilePagerAdapter getMultiProfilePagerAdapter() {
    return this.mMultiProfilePagerAdapter;
  }
  
  public int getLaunchedFromUid() {
    return this.mLaunchedFromUid;
  }
}
