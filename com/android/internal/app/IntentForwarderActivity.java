package com.android.internal.app;

import android.app.Activity;
import android.app.ActivityTaskManager;
import android.app.ActivityThread;
import android.app.AppGlobals;
import android.app.IActivityTaskManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.IPackageManager;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.UserInfo;
import android.metrics.LogMaker;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.UserHandle;
import android.os.UserManager;
import android.provider.Settings;
import android.util.Slog;
import android.widget.Toast;
import com.android.internal.logging.MetricsLogger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class IntentForwarderActivity extends Activity {
  private static final Set<String> ALLOWED_TEXT_MESSAGE_SCHEMES;
  
  public static String FORWARD_INTENT_TO_MANAGED_PROFILE;
  
  public static String FORWARD_INTENT_TO_PARENT;
  
  private static final ComponentName RESOLVER_COMPONENT_NAME;
  
  public static String TAG = "IntentForwarderActivity";
  
  private static final String TEL_SCHEME = "tel";
  
  protected ExecutorService mExecutorService;
  
  private Injector mInjector;
  
  private MetricsLogger mMetricsLogger;
  
  static {
    FORWARD_INTENT_TO_PARENT = "com.android.internal.app.ForwardIntentToParent";
    FORWARD_INTENT_TO_MANAGED_PROFILE = "com.android.internal.app.ForwardIntentToManagedProfile";
    ALLOWED_TEXT_MESSAGE_SCHEMES = new HashSet<>(Arrays.asList(new String[] { "sms", "smsto", "mms", "mmsto" }));
    RESOLVER_COMPONENT_NAME = new ComponentName("android", ResolverActivity.class.getName());
  }
  
  protected void onDestroy() {
    super.onDestroy();
    this.mExecutorService.shutdown();
  }
  
  protected void onCreate(Bundle paramBundle) {
    short s;
    byte b;
    super.onCreate(paramBundle);
    this.mInjector = createInjector();
    this.mExecutorService = Executors.newSingleThreadExecutor();
    Intent intent1 = getIntent();
    String str = intent1.getComponent().getClassName();
    if (str.equals(FORWARD_INTENT_TO_PARENT)) {
      s = getProfileParent();
      MetricsLogger metricsLogger = getMetricsLogger();
      LogMaker logMaker = new LogMaker(1661);
      logMaker = logMaker.setSubtype(1);
      metricsLogger.write(logMaker);
      b = 17040261;
    } else if (str.equals(FORWARD_INTENT_TO_MANAGED_PROFILE)) {
      s = getManagedProfile();
      MetricsLogger metricsLogger = getMetricsLogger();
      LogMaker logMaker = new LogMaker(1661);
      logMaker = logMaker.setSubtype(2);
      metricsLogger.write(logMaker);
      b = 17040262;
    } else {
      String str1 = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(IntentForwarderActivity.class.getName());
      stringBuilder.append(" cannot be called directly");
      Slog.wtf(str1, stringBuilder.toString());
      b = -1;
      s = -10000;
    } 
    if (s == -10000) {
      finish();
      return;
    } 
    if ("android.intent.action.CHOOSER".equals(intent1.getAction())) {
      launchChooserActivityWithCorrectTab(intent1, str);
      return;
    } 
    int i = getUserId();
    int j = getUserId();
    Injector injector2 = this.mInjector;
    IPackageManager iPackageManager = injector2.getIPackageManager();
    ContentResolver contentResolver = getContentResolver();
    Intent intent2 = canForward(intent1, j, s, iPackageManager, contentResolver);
    if (intent2 == null) {
      str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("the intent: ");
      stringBuilder.append(intent1);
      stringBuilder.append(" cannot be forwarded from user ");
      stringBuilder.append(i);
      stringBuilder.append(" to user ");
      stringBuilder.append(s);
      Slog.wtf(str, stringBuilder.toString());
      finish();
      return;
    } 
    intent2.prepareToLeaveUser(i);
    Injector injector1 = this.mInjector;
    CompletableFuture<ResolveInfo> completableFuture1 = injector1.resolveActivityAsUser(intent2, 65536, s);
    _$$Lambda$IntentForwarderActivity$qydOPmABrxKUPHVYcot2pr1dEhM _$$Lambda$IntentForwarderActivity$qydOPmABrxKUPHVYcot2pr1dEhM = new _$$Lambda$IntentForwarderActivity$qydOPmABrxKUPHVYcot2pr1dEhM(this, intent1, str, intent2, i, s);
    ExecutorService executorService = this.mExecutorService;
    CompletableFuture<?> completableFuture = completableFuture1.thenApplyAsync(_$$Lambda$IntentForwarderActivity$qydOPmABrxKUPHVYcot2pr1dEhM, executorService);
    _$$Lambda$IntentForwarderActivity$gWwZSR9fZYCIbpii7m4vcjL74HU _$$Lambda$IntentForwarderActivity$gWwZSR9fZYCIbpii7m4vcjL74HU = new _$$Lambda$IntentForwarderActivity$gWwZSR9fZYCIbpii7m4vcjL74HU(this, intent1, b);
    Executor executor = getApplicationContext().getMainExecutor();
    completableFuture.thenAcceptAsync(_$$Lambda$IntentForwarderActivity$gWwZSR9fZYCIbpii7m4vcjL74HU, executor);
  }
  
  private boolean isIntentForwarderResolveInfo(ResolveInfo paramResolveInfo) {
    null = false;
    if (paramResolveInfo == null)
      return false; 
    ActivityInfo activityInfo = paramResolveInfo.activityInfo;
    if (activityInfo == null)
      return false; 
    if (!"android".equals(activityInfo.packageName))
      return false; 
    if (!activityInfo.name.equals(FORWARD_INTENT_TO_PARENT)) {
      String str2 = activityInfo.name, str1 = FORWARD_INTENT_TO_MANAGED_PROFILE;
      return 
        str2.equals(str1) ? true : null;
    } 
    return true;
  }
  
  private boolean isResolverActivityResolveInfo(ResolveInfo paramResolveInfo) {
    if (paramResolveInfo != null && paramResolveInfo.activityInfo != null) {
      ComponentName componentName = RESOLVER_COMPONENT_NAME;
      ActivityInfo activityInfo = paramResolveInfo.activityInfo;
      if (componentName.equals(activityInfo.getComponentName()))
        return true; 
    } 
    return false;
  }
  
  private void maybeShowDisclosure(Intent paramIntent, ResolveInfo paramResolveInfo, int paramInt) {
    if (shouldShowDisclosure(paramResolveInfo, paramIntent))
      this.mInjector.showToast(paramInt, 1); 
  }
  
  private void startActivityAsCaller(Intent paramIntent, int paramInt) {
    try {
      startActivityAsCaller(paramIntent, null, null, false, paramInt);
    } catch (RuntimeException runtimeException) {
      int i = -1;
      String str1 = "?";
      paramInt = i;
      try {
        IActivityTaskManager iActivityTaskManager1 = ActivityTaskManager.getService();
        paramInt = i;
        IBinder iBinder = getActivityToken();
        paramInt = i;
        i = iActivityTaskManager1.getLaunchedFromUid(iBinder);
        paramInt = i;
        IActivityTaskManager iActivityTaskManager2 = ActivityTaskManager.getService();
        paramInt = i;
        String str = iActivityTaskManager2.getLaunchedFromPackage(getActivityToken());
        paramInt = i;
      } catch (RemoteException remoteException) {}
      String str2 = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to launch as UID ");
      stringBuilder.append(paramInt);
      stringBuilder.append(" package ");
      stringBuilder.append(str1);
      stringBuilder.append(", while running in ");
      stringBuilder.append(ActivityThread.currentProcessName());
      str1 = stringBuilder.toString();
      Slog.wtf(str2, str1, runtimeException);
    } 
  }
  
  private void launchChooserActivityWithCorrectTab(Intent paramIntent, String paramString) {
    int i = findSelectedProfile(paramString);
    sanitizeIntent(paramIntent);
    paramIntent.putExtra("com.android.internal.app.ResolverActivity.EXTRA_SELECTED_PROFILE", i);
    Intent intent = (Intent)paramIntent.getParcelableExtra("android.intent.extra.INTENT");
    if (intent == null) {
      Slog.wtf(TAG, "Cannot start a chooser intent with no extra android.intent.extra.INTENT");
      return;
    } 
    sanitizeIntent(intent);
    startActivityAsCaller(paramIntent, null, null, false, getUserId());
    finish();
  }
  
  private void launchResolverActivityWithCorrectTab(Intent paramIntent1, String paramString, Intent paramIntent2, int paramInt1, int paramInt2) {
    CompletableFuture<ResolveInfo> completableFuture = this.mInjector.resolveActivityAsUser(paramIntent2, 65536, paramInt1);
    ResolveInfo resolveInfo = completableFuture.join();
    if (!isIntentForwarderResolveInfo(resolveInfo))
      paramInt2 = paramInt1; 
    int i = findSelectedProfile(paramString);
    sanitizeIntent(paramIntent1);
    paramIntent1.putExtra("com.android.internal.app.ResolverActivity.EXTRA_SELECTED_PROFILE", i);
    paramIntent1.putExtra("com.android.internal.app.ResolverActivity.EXTRA_CALLING_USER", (Parcelable)UserHandle.of(paramInt1));
    startActivityAsCaller(paramIntent1, null, null, false, paramInt2);
    finish();
  }
  
  private int findSelectedProfile(String paramString) {
    if (paramString.equals(FORWARD_INTENT_TO_PARENT))
      return 0; 
    if (paramString.equals(FORWARD_INTENT_TO_MANAGED_PROFILE))
      return 1; 
    return -1;
  }
  
  private boolean shouldShowDisclosure(ResolveInfo paramResolveInfo, Intent paramIntent) {
    if (!isDeviceProvisioned())
      return false; 
    if (paramResolveInfo == null || paramResolveInfo.activityInfo == null)
      return true; 
    if (paramResolveInfo.activityInfo.applicationInfo.isSystemApp() && (
      isDialerIntent(paramIntent) || isTextMessageIntent(paramIntent)))
      return false; 
    return true ^ isTargetResolverOrChooserActivity(paramResolveInfo.activityInfo);
  }
  
  private boolean isDeviceProvisioned() {
    ContentResolver contentResolver = getContentResolver();
    boolean bool = false;
    if (Settings.Global.getInt(contentResolver, "device_provisioned", 0) != 0)
      bool = true; 
    return bool;
  }
  
  private boolean isTextMessageIntent(Intent paramIntent) {
    if ("android.intent.action.SENDTO".equals(paramIntent.getAction()) || isViewActionIntent(paramIntent)) {
      Set<String> set = ALLOWED_TEXT_MESSAGE_SCHEMES;
      if (set.contains(paramIntent.getScheme()))
        return true; 
    } 
    return false;
  }
  
  private boolean isDialerIntent(Intent paramIntent) {
    return ("android.intent.action.DIAL".equals(paramIntent.getAction()) || 
      "android.intent.action.CALL".equals(paramIntent.getAction()) || 
      "android.intent.action.CALL_PRIVILEGED".equals(paramIntent.getAction()) || 
      "android.intent.action.CALL_EMERGENCY".equals(paramIntent.getAction()) || (
      isViewActionIntent(paramIntent) && "tel".equals(paramIntent.getScheme())));
  }
  
  private boolean isViewActionIntent(Intent paramIntent) {
    boolean bool;
    if ("android.intent.action.VIEW".equals(paramIntent.getAction()) && 
      paramIntent.hasCategory("android.intent.category.BROWSABLE")) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isTargetResolverOrChooserActivity(ActivityInfo paramActivityInfo) {
    boolean bool = "android".equals(paramActivityInfo.packageName);
    boolean bool1 = false;
    if (!bool)
      return false; 
    if (ResolverActivity.class.getName().equals(paramActivityInfo.name) || 
      ChooserActivity.class.getName().equals(paramActivityInfo.name))
      bool1 = true; 
    return bool1;
  }
  
  static Intent canForward(Intent paramIntent, int paramInt1, int paramInt2, IPackageManager paramIPackageManager, ContentResolver paramContentResolver) {
    Intent intent = new Intent(paramIntent);
    intent.addFlags(50331648);
    sanitizeIntent(intent);
    paramIntent = intent;
    if ("android.intent.action.CHOOSER".equals(intent.getAction()))
      return null; 
    if (intent.getSelector() != null)
      paramIntent = intent.getSelector(); 
    String str = paramIntent.resolveTypeIfNeeded(paramContentResolver);
    sanitizeIntent(paramIntent);
    try {
      boolean bool = paramIPackageManager.canForwardTo(paramIntent, str, paramInt1, paramInt2);
      if (bool)
        return intent; 
    } catch (RemoteException remoteException) {
      Slog.e(TAG, "PackageManagerService is dead?");
    } 
    return null;
  }
  
  private int getManagedProfile() {
    List list = this.mInjector.getUserManager().getProfiles(UserHandle.myUserId());
    for (UserInfo userInfo : list) {
      if (userInfo.isManagedProfile())
        return userInfo.id; 
    } 
    String str = TAG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(FORWARD_INTENT_TO_MANAGED_PROFILE);
    stringBuilder.append(" has been called, but there is no managed profile");
    Slog.wtf(str, stringBuilder.toString());
    return -10000;
  }
  
  private int getProfileParent() {
    StringBuilder stringBuilder;
    UserInfo userInfo = this.mInjector.getUserManager().getProfileParent(UserHandle.myUserId());
    if (userInfo == null) {
      String str = TAG;
      stringBuilder = new StringBuilder();
      stringBuilder.append(FORWARD_INTENT_TO_PARENT);
      stringBuilder.append(" has been called, but there is no parent");
      Slog.wtf(str, stringBuilder.toString());
      return -10000;
    } 
    return ((UserInfo)stringBuilder).id;
  }
  
  private static void sanitizeIntent(Intent paramIntent) {
    paramIntent.setPackage(null);
    paramIntent.setComponent(null);
  }
  
  protected MetricsLogger getMetricsLogger() {
    if (this.mMetricsLogger == null)
      this.mMetricsLogger = new MetricsLogger(); 
    return this.mMetricsLogger;
  }
  
  protected Injector createInjector() {
    return new InjectorImpl();
  }
  
  class Injector {
    public abstract IPackageManager getIPackageManager();
    
    public abstract PackageManager getPackageManager();
    
    public abstract UserManager getUserManager();
    
    public abstract CompletableFuture<ResolveInfo> resolveActivityAsUser(Intent param1Intent, int param1Int1, int param1Int2);
    
    public abstract void showToast(int param1Int1, int param1Int2);
  }
  
  class InjectorImpl implements Injector {
    final IntentForwarderActivity this$0;
    
    private InjectorImpl() {}
    
    public IPackageManager getIPackageManager() {
      return AppGlobals.getPackageManager();
    }
    
    public UserManager getUserManager() {
      return (UserManager)IntentForwarderActivity.this.getSystemService(UserManager.class);
    }
    
    public PackageManager getPackageManager() {
      return IntentForwarderActivity.this.getPackageManager();
    }
    
    public CompletableFuture<ResolveInfo> resolveActivityAsUser(Intent param1Intent, int param1Int1, int param1Int2) {
      return CompletableFuture.supplyAsync(new _$$Lambda$IntentForwarderActivity$InjectorImpl$Mi1gUXOy2QrYPv_nmr6rZeOneQU(this, param1Intent, param1Int1, param1Int2));
    }
    
    public void showToast(int param1Int1, int param1Int2) {
      IntentForwarderActivity intentForwarderActivity = IntentForwarderActivity.this;
      Toast.makeText((Context)intentForwarderActivity, intentForwarderActivity.getString(param1Int1), param1Int2).show();
    }
  }
}
