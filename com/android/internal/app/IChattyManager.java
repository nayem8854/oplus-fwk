package com.android.internal.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IChattyManager extends IInterface {
  class Default implements IChattyManager {
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IChattyManager {
    private static final String DESCRIPTOR = "com.android.internal.app.IChattyManager";
    
    public Stub() {
      attachInterface(this, "com.android.internal.app.IChattyManager");
    }
    
    public static IChattyManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.app.IChattyManager");
      if (iInterface != null && iInterface instanceof IChattyManager)
        return (IChattyManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      return null;
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902)
        return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
      param1Parcel2.writeString("com.android.internal.app.IChattyManager");
      return true;
    }
    
    private static class Proxy implements IChattyManager {
      public static IChattyManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.app.IChattyManager";
      }
    }
    
    public static boolean setDefaultImpl(IChattyManager param1IChattyManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IChattyManager != null) {
          Proxy.sDefaultImpl = param1IChattyManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IChattyManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
