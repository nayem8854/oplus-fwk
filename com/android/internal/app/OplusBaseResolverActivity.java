package com.android.internal.app;

import android.app.Activity;
import android.common.ColorFrameworkFactory;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.OplusBaseIntent;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.UserHandle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.internal.app.chooser.TargetInfo;
import com.android.internal.widget.ViewPager;
import com.oplus.util.OplusTypeCastingHelper;
import java.util.List;

public abstract class OplusBaseResolverActivity extends Activity {
  protected IOplusResolverManager iOplusResolverManager = (IOplusResolverManager)ColorFrameworkFactory.getInstance().getFeature(IOplusResolverManager.DEFAULT, new Object[0]);
  
  private int mCustomFlag = 0;
  
  public int getLaunchedFromUid() {
    return -1;
  }
  
  public boolean hasCustomFlag(int paramInt) {
    boolean bool;
    if ((this.mCustomFlag & paramInt) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected void cacheCustomInfo(Intent paramIntent) {
    if (paramIntent != null) {
      OplusBaseIntent oplusBaseIntent = (OplusBaseIntent)OplusTypeCastingHelper.typeCasting(OplusBaseIntent.class, paramIntent);
      if (oplusBaseIntent != null)
        this.mCustomFlag = oplusBaseIntent.getOplusFlags(); 
    } 
  }
  
  public void finish() {
    super.finish();
    if (!isOriginUi())
      overridePendingTransition(201916421, 201916422); 
  }
  
  public void onMultiWindowModeChanged(boolean paramBoolean, Configuration paramConfiguration) {
    super.onMultiWindowModeChanged(paramBoolean, paramConfiguration);
    IOplusResolverManager iOplusResolverManager = this.iOplusResolverManager;
    if (iOplusResolverManager != null)
      iOplusResolverManager.onMultiWindowModeChanged(); 
  }
  
  protected void onCreate(Bundle paramBundle) {
    IOplusResolverManager iOplusResolverManager = this.iOplusResolverManager;
    if (iOplusResolverManager != null)
      iOplusResolverManager.onCreate(this); 
    cacheCustomInfo(getIntent());
    super.onCreate(paramBundle);
  }
  
  protected void onResume() {
    super.onResume();
    IOplusResolverManager iOplusResolverManager = this.iOplusResolverManager;
    if (iOplusResolverManager != null)
      iOplusResolverManager.onResume(); 
  }
  
  protected void onPause() {
    super.onPause();
    IOplusResolverManager iOplusResolverManager = this.iOplusResolverManager;
    if (iOplusResolverManager != null)
      iOplusResolverManager.onPause(); 
  }
  
  protected void onDestroy() {
    super.onDestroy();
    IOplusResolverManager iOplusResolverManager = this.iOplusResolverManager;
    if (iOplusResolverManager != null)
      iOplusResolverManager.onDestroy(); 
  }
  
  protected void setOriginTheme(int paramInt) {
    IOplusResolverManager iOplusResolverManager = this.iOplusResolverManager;
    if (iOplusResolverManager == null || iOplusResolverManager.isLoadTheme())
      setTheme(paramInt); 
  }
  
  protected void setOriginContentView(int paramInt1, AbstractMultiProfilePagerAdapter paramAbstractMultiProfilePagerAdapter, int paramInt2) {
    IOplusResolverManager iOplusResolverManager = this.iOplusResolverManager;
    if (iOplusResolverManager == null || iOplusResolverManager.isOriginUi()) {
      setContentView(paramInt1);
      paramAbstractMultiProfilePagerAdapter.setupViewPager((ViewPager)findViewById(paramInt2));
    } 
  }
  
  protected boolean isOriginUi() {
    IOplusResolverManager iOplusResolverManager = this.iOplusResolverManager;
    if (iOplusResolverManager != null)
      return iOplusResolverManager.isOriginUi(); 
    return true;
  }
  
  protected boolean addExtraOneAppFinish() {
    IOplusResolverManager iOplusResolverManager = this.iOplusResolverManager;
    if (iOplusResolverManager != null)
      return iOplusResolverManager.addExtraOneAppFinish(); 
    return true;
  }
  
  protected void setLastChosen(ResolverListController paramResolverListController, Intent paramIntent, IntentFilter paramIntentFilter, int paramInt) throws RemoteException {
    IOplusResolverManager iOplusResolverManager = this.iOplusResolverManager;
    if (iOplusResolverManager != null)
      iOplusResolverManager.setLastChosen(paramResolverListController, paramIntent, paramIntentFilter, paramInt); 
  }
  
  protected void initView(boolean paramBoolean1, boolean paramBoolean2) {
    IOplusResolverManager iOplusResolverManager = this.iOplusResolverManager;
    if (iOplusResolverManager != null)
      iOplusResolverManager.initView(paramBoolean1, paramBoolean2); 
  }
  
  protected void updateView(boolean paramBoolean1, boolean paramBoolean2) {
    IOplusResolverManager iOplusResolverManager = this.iOplusResolverManager;
    if (iOplusResolverManager != null)
      iOplusResolverManager.updateView(paramBoolean1, paramBoolean2); 
  }
  
  protected void initActionSend() {
    IOplusResolverManager iOplusResolverManager = this.iOplusResolverManager;
    if (iOplusResolverManager != null)
      iOplusResolverManager.initActionSend(); 
  }
  
  protected void statisticsData(ResolveInfo paramResolveInfo, int paramInt) {
    IOplusResolverManager iOplusResolverManager = this.iOplusResolverManager;
    if (iOplusResolverManager != null)
      iOplusResolverManager.statisticsData(paramResolveInfo, paramInt); 
  }
  
  public void setResolverContent() {
    IOplusResolverManager iOplusResolverManager = this.iOplusResolverManager;
    if (iOplusResolverManager != null)
      iOplusResolverManager.setResolverContent(); 
  }
  
  protected void initPreferenceAndPinList() {
    IOplusResolverManager iOplusResolverManager = this.iOplusResolverManager;
    if (iOplusResolverManager != null)
      iOplusResolverManager.initPreferenceAndPinList(); 
  }
  
  protected void performAnimation() {
    if (!isOriginUi())
      overridePendingTransition(201916431, 201916432); 
  }
  
  protected int getChooserPreFileSingleIconView(boolean paramBoolean, String paramString, Uri paramUri) {
    IOplusResolverManager iOplusResolverManager = this.iOplusResolverManager;
    if (iOplusResolverManager != null)
      return iOplusResolverManager.getChooserPreFileSingleIconView(paramBoolean, paramString, paramUri); 
    return -1;
  }
  
  protected ViewGroup getDisplayFileContentPreview(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup) {
    IOplusResolverManager iOplusResolverManager = this.iOplusResolverManager;
    if (iOplusResolverManager != null)
      return iOplusResolverManager.getDisplayFileContentPreview(paramLayoutInflater, paramViewGroup); 
    return null;
  }
  
  protected ViewGroup getDisplayTextContentPreview(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup) {
    IOplusResolverManager iOplusResolverManager = this.iOplusResolverManager;
    if (iOplusResolverManager != null)
      return iOplusResolverManager.getDisplayTextContentPreview(paramLayoutInflater, paramViewGroup); 
    return null;
  }
  
  protected ViewGroup getDisplayImageContentPreview(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup) {
    IOplusResolverManager iOplusResolverManager = this.iOplusResolverManager;
    if (iOplusResolverManager != null)
      return iOplusResolverManager.getDisplayImageContentPreview(paramLayoutInflater, paramViewGroup); 
    return null;
  }
  
  class OplusBaseResolveListAdapter extends BaseAdapter {
    final OplusBaseResolverActivity this$0;
    
    public void processSortedListWrapper(List<ResolverActivity.ResolvedComponentInfo> param1List) {}
  }
  
  protected ViewGroup createContentPreviewViewWrapper(ViewGroup paramViewGroup) {
    return null;
  }
  
  protected TargetInfo getNearbySharingTargetWrapper(Intent paramIntent) {
    return null;
  }
  
  protected void restoreProfilePager(int paramInt) {
    IOplusResolverManager iOplusResolverManager = this.iOplusResolverManager;
    if (iOplusResolverManager != null)
      iOplusResolverManager.restoreProfilePager(paramInt); 
  }
  
  protected boolean tryApkResourceName(Uri paramUri, ImageView paramImageView, TextView paramTextView) {
    IOplusResolverManager iOplusResolverManager = this.iOplusResolverManager;
    if (iOplusResolverManager != null)
      return iOplusResolverManager.tryApkResourceName(paramUri, paramImageView, paramTextView); 
    return false;
  }
  
  protected void displayTextAddActionButton(ViewGroup paramViewGroup, View.OnClickListener paramOnClickListener) {
    IOplusResolverManager iOplusResolverManager = this.iOplusResolverManager;
    if (iOplusResolverManager != null)
      iOplusResolverManager.displayTextAddActionButton(paramViewGroup, paramOnClickListener); 
  }
  
  protected void setCustomRoundImage(Paint paramPaint1, Paint paramPaint2, Paint paramPaint3) {
    IOplusResolverManager iOplusResolverManager = this.iOplusResolverManager;
    if (iOplusResolverManager != null)
      iOplusResolverManager.setCustomRoundImage(paramPaint1, paramPaint2, paramPaint3); 
  }
  
  protected int getCornerRadius() {
    return this.iOplusResolverManager.getCornerRadius((Context)this);
  }
  
  protected void addNearbyAction(ViewGroup paramViewGroup, Intent paramIntent) {
    IOplusResolverManager iOplusResolverManager = this.iOplusResolverManager;
    if (iOplusResolverManager != null)
      iOplusResolverManager.addNearbyAction(paramViewGroup, paramIntent); 
  }
  
  protected abstract AbstractMultiProfilePagerAdapter getMultiProfilePagerAdapter();
  
  protected abstract String getReferrerPackageName();
  
  protected abstract Intent getTargetIntent();
  
  protected abstract UserHandle getWorkProfileUserHandle();
  
  protected abstract void safelyStartActivity(TargetInfo paramTargetInfo);
  
  protected abstract void startSelected(int paramInt, boolean paramBoolean1, boolean paramBoolean2);
}
