package com.android.internal.app;

import android.app.AppGlobals;
import android.app.admin.DevicePolicyEventLogger;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.IPackageManager;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;
import android.os.UserHandle;
import android.os.UserManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.internal.widget.PagerAdapter;
import com.android.internal.widget.ViewPager;
import java.lang.annotation.Annotation;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public abstract class AbstractMultiProfilePagerAdapter extends PagerAdapter implements IOplusAbstractMultiProfilePagerAdapterEx {
  static final int PROFILE_PERSONAL = 0;
  
  static final int PROFILE_WORK = 1;
  
  private static final String TAG = "AbstractMultiProfilePagerAdapter";
  
  private final Context mContext;
  
  private int mCurrentPage;
  
  private Injector mInjector;
  
  private boolean mIsWaitingToEnableWorkProfile;
  
  private Set<Integer> mLoadedPages;
  
  private OnProfileSelectedListener mOnProfileSelectedListener;
  
  private OnSwitchOnWorkSelectedListener mOnSwitchOnWorkSelectedListener;
  
  private final UserHandle mPersonalProfileUserHandle;
  
  private final UserHandle mWorkProfileUserHandle;
  
  AbstractMultiProfilePagerAdapter(Context paramContext, int paramInt, UserHandle paramUserHandle1, UserHandle paramUserHandle2) {
    Objects.requireNonNull(paramContext);
    this.mContext = paramContext;
    this.mCurrentPage = paramInt;
    this.mLoadedPages = new HashSet<>();
    this.mPersonalProfileUserHandle = paramUserHandle1;
    this.mWorkProfileUserHandle = paramUserHandle2;
    final UserManager userManager = (UserManager)paramContext.getSystemService(UserManager.class);
    this.mInjector = new Injector() {
        final AbstractMultiProfilePagerAdapter this$0;
        
        final UserManager val$userManager;
        
        public boolean hasCrossProfileIntents(List<Intent> param1List, int param1Int1, int param1Int2) {
          AbstractMultiProfilePagerAdapter abstractMultiProfilePagerAdapter = AbstractMultiProfilePagerAdapter.this;
          return 
            abstractMultiProfilePagerAdapter.hasCrossProfileIntents(param1List, param1Int1, param1Int2);
        }
        
        public boolean isQuietModeEnabled(UserHandle param1UserHandle) {
          return userManager.isQuietModeEnabled(param1UserHandle);
        }
        
        public void requestQuietModeEnabled(boolean param1Boolean, UserHandle param1UserHandle) {
          AsyncTask.THREAD_POOL_EXECUTOR.execute(new _$$Lambda$AbstractMultiProfilePagerAdapter$1$iSo32hCiHlak2AHe_5mrZqhJq5E(userManager, param1Boolean, param1UserHandle));
          AbstractMultiProfilePagerAdapter.access$102(AbstractMultiProfilePagerAdapter.this, true);
        }
      };
  }
  
  protected void markWorkProfileEnabledBroadcastReceived() {
    this.mIsWaitingToEnableWorkProfile = false;
  }
  
  protected boolean isWaitingToEnableWorkProfile() {
    return this.mIsWaitingToEnableWorkProfile;
  }
  
  public void setInjector(Injector paramInjector) {
    this.mInjector = paramInjector;
  }
  
  protected boolean isQuietModeEnabled(UserHandle paramUserHandle) {
    return this.mInjector.isQuietModeEnabled(paramUserHandle);
  }
  
  void setOnProfileSelectedListener(OnProfileSelectedListener paramOnProfileSelectedListener) {
    this.mOnProfileSelectedListener = paramOnProfileSelectedListener;
  }
  
  void setOnSwitchOnWorkSelectedListener(OnSwitchOnWorkSelectedListener paramOnSwitchOnWorkSelectedListener) {
    this.mOnSwitchOnWorkSelectedListener = paramOnSwitchOnWorkSelectedListener;
  }
  
  Context getContext() {
    return this.mContext;
  }
  
  void setupViewPager(ViewPager paramViewPager) {
    paramViewPager.setOnPageChangeListener((ViewPager.OnPageChangeListener)new Object(this));
    paramViewPager.setAdapter(this);
    paramViewPager.setCurrentItem(this.mCurrentPage);
    this.mLoadedPages.add(Integer.valueOf(this.mCurrentPage));
  }
  
  void clearInactiveProfileCache() {
    if (this.mLoadedPages.size() == 1)
      return; 
    this.mLoadedPages.remove(Integer.valueOf(1 - this.mCurrentPage));
    ((OplusBaseResolverActivity)getContext()).iOplusResolverManager.clearInactiveProfileCache(this.mCurrentPage);
  }
  
  public ViewGroup instantiateItem(ViewGroup paramViewGroup, int paramInt) {
    ProfileDescriptor profileDescriptor = getItem(paramInt);
    paramViewGroup.addView(profileDescriptor.rootView);
    return profileDescriptor.rootView;
  }
  
  public void destroyItem(ViewGroup paramViewGroup, int paramInt, Object paramObject) {
    paramViewGroup.removeView((View)paramObject);
  }
  
  public int getCount() {
    return getItemCount();
  }
  
  protected int getCurrentPage() {
    return this.mCurrentPage;
  }
  
  public UserHandle getCurrentUserHandle() {
    return (getActiveListAdapter()).mResolverListController.getUserHandle();
  }
  
  public boolean isViewFromObject(View paramView, Object paramObject) {
    boolean bool;
    if (paramView == paramObject) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public CharSequence getPageTitle(int paramInt) {
    return null;
  }
  
  boolean rebuildActiveTab(boolean paramBoolean) {
    return rebuildTab(getActiveListAdapter(), paramBoolean);
  }
  
  boolean rebuildInactiveTab(boolean paramBoolean) {
    if (getItemCount() == 1)
      return false; 
    return rebuildTab(getInactiveListAdapter(), paramBoolean);
  }
  
  private int userHandleToPageIndex(UserHandle paramUserHandle) {
    if (paramUserHandle.equals((getPersonalListAdapter()).mResolverListController.getUserHandle()))
      return 0; 
    return 1;
  }
  
  private boolean rebuildTab(ResolverListAdapter paramResolverListAdapter, boolean paramBoolean) {
    if (shouldShowNoCrossProfileIntentsEmptyState(paramResolverListAdapter)) {
      paramResolverListAdapter.postListReadyRunnable(paramBoolean, true);
      return false;
    } 
    return paramResolverListAdapter.rebuildList(paramBoolean);
  }
  
  private boolean shouldShowNoCrossProfileIntentsEmptyState(ResolverListAdapter paramResolverListAdapter) {
    UserHandle userHandle = paramResolverListAdapter.getUserHandle();
    if (UserHandle.myUserId() != userHandle.getIdentifier() && 
      allowShowNoCrossProfileIntentsEmptyState()) {
      Injector injector = this.mInjector;
      List<Intent> list = paramResolverListAdapter.getIntents();
      int i = UserHandle.myUserId(), j = userHandle.getIdentifier();
      if (!injector.hasCrossProfileIntents(list, i, j))
        return true; 
    } 
    return false;
  }
  
  boolean allowShowNoCrossProfileIntentsEmptyState() {
    return true;
  }
  
  void updateAfterConfigChange() {
    for (byte b = 0; b < getItemCount(); b++) {
      ViewGroup viewGroup = getItem(b).getEmptyStateView();
      ImageView imageView = viewGroup.<ImageView>findViewById(16909354);
      updateIconVisibility(imageView, viewGroup);
    } 
  }
  
  private void updateIconVisibility(ImageView paramImageView, ViewGroup paramViewGroup) {
    if (isSpinnerShowing(paramViewGroup)) {
      paramImageView.setVisibility(4);
    } else if (this.mWorkProfileUserHandle != null && 
      !getContext().getResources().getBoolean(17891613)) {
      paramImageView.setVisibility(0);
    } else {
      paramImageView.setVisibility(8);
    } 
  }
  
  void showEmptyResolverListEmptyState(ResolverListAdapter paramResolverListAdapter) {
    if (maybeShowNoCrossProfileIntentsEmptyState(paramResolverListAdapter))
      return; 
    if (maybeShowWorkProfileOffEmptyState(paramResolverListAdapter))
      return; 
    maybeShowNoAppsAvailableEmptyState(paramResolverListAdapter);
  }
  
  private boolean maybeShowNoCrossProfileIntentsEmptyState(ResolverListAdapter paramResolverListAdapter) {
    if (!shouldShowNoCrossProfileIntentsEmptyState(paramResolverListAdapter))
      return false; 
    if (paramResolverListAdapter.getUserHandle().equals(this.mPersonalProfileUserHandle)) {
      DevicePolicyEventLogger devicePolicyEventLogger = DevicePolicyEventLogger.createEvent(158);
      devicePolicyEventLogger = devicePolicyEventLogger.setStrings(new String[] { getMetricsCategory() });
      devicePolicyEventLogger.write();
      ((OplusBaseResolverActivity)getContext()).iOplusResolverManager.showNoWorkToPersonalIntentsEmptyState(this, paramResolverListAdapter);
    } else {
      DevicePolicyEventLogger devicePolicyEventLogger = DevicePolicyEventLogger.createEvent(159);
      devicePolicyEventLogger = devicePolicyEventLogger.setStrings(new String[] { getMetricsCategory() });
      devicePolicyEventLogger.write();
      ((OplusBaseResolverActivity)getContext()).iOplusResolverManager.showNoPersonalToWorkIntentsEmptyState(this, paramResolverListAdapter);
    } 
    return true;
  }
  
  private boolean maybeShowWorkProfileOffEmptyState(ResolverListAdapter paramResolverListAdapter) {
    UserHandle userHandle = paramResolverListAdapter.getUserHandle();
    if (userHandle.equals(this.mWorkProfileUserHandle)) {
      Injector injector = this.mInjector;
      UserHandle userHandle1 = this.mWorkProfileUserHandle;
      if (injector.isQuietModeEnabled(userHandle1) && 
        paramResolverListAdapter.getCount() != 0) {
        DevicePolicyEventLogger devicePolicyEventLogger = DevicePolicyEventLogger.createEvent(157);
        devicePolicyEventLogger = devicePolicyEventLogger.setStrings(new String[] { getMetricsCategory() });
        devicePolicyEventLogger.write();
        ((OplusBaseResolverActivity)getContext()).iOplusResolverManager.showWorkProfileOffEmptyState(this, paramResolverListAdapter, new _$$Lambda$AbstractMultiProfilePagerAdapter$hQkaavOZ32b4_P9cXHNwBKZa760(this, paramResolverListAdapter));
        return true;
      } 
    } 
    return false;
  }
  
  private void maybeShowNoAppsAvailableEmptyState(ResolverListAdapter paramResolverListAdapter) {
    UserHandle userHandle = paramResolverListAdapter.getUserHandle();
    if (this.mWorkProfileUserHandle != null && (
      UserHandle.myUserId() == userHandle.getIdentifier() || 
      !hasAppsInOtherProfile(paramResolverListAdapter))) {
      DevicePolicyEventLogger devicePolicyEventLogger = DevicePolicyEventLogger.createEvent(160);
      boolean bool = true;
      devicePolicyEventLogger = devicePolicyEventLogger.setStrings(new String[] { getMetricsCategory() });
      if (userHandle != this.mPersonalProfileUserHandle)
        bool = false; 
      devicePolicyEventLogger = devicePolicyEventLogger.setBoolean(bool);
      devicePolicyEventLogger.write();
      if (userHandle == this.mPersonalProfileUserHandle) {
        ((OplusBaseResolverActivity)getContext()).iOplusResolverManager.showNoPersonalAppsAvailableEmptyState(this, paramResolverListAdapter);
      } else {
        ((OplusBaseResolverActivity)getContext()).iOplusResolverManager.showNoWorkAppsAvailableEmptyState(this, paramResolverListAdapter);
      } 
    } else if (this.mWorkProfileUserHandle == null) {
      showConsumerUserNoAppsAvailableEmptyState(paramResolverListAdapter);
    } 
  }
  
  protected void showEmptyState(ResolverListAdapter paramResolverListAdapter, int paramInt1, int paramInt2, int paramInt3) {
    showEmptyState(paramResolverListAdapter, paramInt1, paramInt2, paramInt3, null);
  }
  
  protected void showEmptyState(ResolverListAdapter paramResolverListAdapter, int paramInt1, int paramInt2, int paramInt3, View.OnClickListener paramOnClickListener) {
    int i = userHandleToPageIndex(paramResolverListAdapter.getUserHandle());
    ProfileDescriptor profileDescriptor = getItem(i);
    View view = (View)profileDescriptor.rootView.findViewById(16909358);
    i = 8;
    view.setVisibility(8);
    ViewGroup viewGroup = profileDescriptor.getEmptyStateView();
    resetViewVisibilitiesForWorkProfileEmptyState(viewGroup);
    viewGroup.setVisibility(0);
    view = viewGroup.findViewById(16909353);
    setupContainerPadding(view);
    view = viewGroup.<TextView>findViewById(16909357);
    view.setText(paramInt2);
    view = viewGroup.<TextView>findViewById(16909356);
    if (paramInt3 != 0) {
      view.setVisibility(0);
      view.setText(paramInt3);
    } else {
      view.setVisibility(8);
    } 
    view = viewGroup.<Button>findViewById(16909352);
    paramInt2 = i;
    if (paramOnClickListener != null)
      paramInt2 = 0; 
    view.setVisibility(paramInt2);
    view.setOnClickListener(paramOnClickListener);
    ImageView imageView = viewGroup.<ImageView>findViewById(16909354);
    imageView.setImageResource(paramInt1);
    updateIconVisibility(imageView, viewGroup);
    paramResolverListAdapter.markTabLoaded();
    ((OplusBaseResolverActivity)getContext()).iOplusResolverManager.showActiveEmptyViewState();
  }
  
  protected void setupContainerPadding(View paramView) {}
  
  private void showConsumerUserNoAppsAvailableEmptyState(ResolverListAdapter paramResolverListAdapter) {
    int i = userHandleToPageIndex(paramResolverListAdapter.getUserHandle());
    ProfileDescriptor profileDescriptor = getItem(i);
    profileDescriptor.rootView.<View>findViewById(16909358).setVisibility(8);
    ViewGroup viewGroup = profileDescriptor.getEmptyStateView();
    resetViewVisibilitiesForConsumerUserEmptyState(viewGroup);
    viewGroup.setVisibility(0);
    paramResolverListAdapter.markTabLoaded();
    ((OplusBaseResolverActivity)getContext()).iOplusResolverManager.showActiveEmptyViewState();
  }
  
  private boolean isSpinnerShowing(View paramView) {
    boolean bool;
    if (paramView.<View>findViewById(16909355).getVisibility() == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void showSpinner(View paramView) {
    paramView.<View>findViewById(16909354).setVisibility(4);
    paramView.<View>findViewById(16909357).setVisibility(4);
    paramView.<View>findViewById(16909352).setVisibility(4);
    paramView.<View>findViewById(16909355).setVisibility(0);
    paramView.<View>findViewById(16908292).setVisibility(8);
  }
  
  private void resetViewVisibilitiesForWorkProfileEmptyState(View paramView) {
    paramView.<View>findViewById(16909354).setVisibility(0);
    paramView.<View>findViewById(16909357).setVisibility(0);
    paramView.<View>findViewById(16909356).setVisibility(0);
    paramView.<View>findViewById(16909352).setVisibility(4);
    paramView.<View>findViewById(16909355).setVisibility(8);
    paramView.<View>findViewById(16908292).setVisibility(8);
  }
  
  private void resetViewVisibilitiesForConsumerUserEmptyState(View paramView) {
    boolean bool;
    View view = (View)paramView.findViewById(16909354);
    if (((OplusBaseResolverActivity)getContext()).iOplusResolverManager.isOriginUi()) {
      bool = true;
    } else {
      bool = false;
    } 
    view.setVisibility(bool);
    paramView.<View>findViewById(16909357).setVisibility(8);
    paramView.<View>findViewById(16909356).setVisibility(8);
    paramView.<View>findViewById(16909352).setVisibility(8);
    paramView.<View>findViewById(16909355).setVisibility(8);
    paramView.<View>findViewById(16908292).setVisibility(0);
  }
  
  protected void showListView(ResolverListAdapter paramResolverListAdapter) {
    int i = userHandleToPageIndex(paramResolverListAdapter.getUserHandle());
    ProfileDescriptor profileDescriptor = getItem(i);
    profileDescriptor.rootView.<View>findViewById(16909358).setVisibility(0);
    profileDescriptor = profileDescriptor.rootView.findViewById(16909351);
    profileDescriptor.setVisibility(8);
  }
  
  private boolean hasCrossProfileIntents(List<Intent> paramList, int paramInt1, int paramInt2) {
    IPackageManager iPackageManager = AppGlobals.getPackageManager();
    ContentResolver contentResolver = this.mContext.getContentResolver();
    for (Intent intent : paramList) {
      if (IntentForwarderActivity.canForward(intent, paramInt1, paramInt2, iPackageManager, contentResolver) != null)
        return true; 
    } 
    return false;
  }
  
  private boolean hasAppsInOtherProfile(ResolverListAdapter paramResolverListAdapter) {
    if (this.mWorkProfileUserHandle == null)
      return false; 
    List<ResolverActivity.ResolvedComponentInfo> list = paramResolverListAdapter.getResolversForUser(UserHandle.of(UserHandle.myUserId()));
    for (ResolverActivity.ResolvedComponentInfo resolvedComponentInfo : list) {
      ResolveInfo resolveInfo = resolvedComponentInfo.getResolveInfoAt(0);
      if (resolveInfo.targetUserId != -2)
        return true; 
    } 
    return false;
  }
  
  public void setCurrentPage(int paramInt) {
    this.mCurrentPage = paramInt;
  }
  
  boolean shouldShowEmptyStateScreen(ResolverListAdapter paramResolverListAdapter) {
    int i = paramResolverListAdapter.getUnfilteredCount();
    if (i != 0 || paramResolverListAdapter.getPlaceholderCount() != 0) {
      if (paramResolverListAdapter.getUserHandle().equals(this.mWorkProfileUserHandle)) {
        UserHandle userHandle = this.mWorkProfileUserHandle;
        if (isQuietModeEnabled(userHandle))
          return true; 
      } 
      return false;
    } 
    return true;
  }
  
  abstract ViewGroup getActiveAdapterView();
  
  public abstract ResolverListAdapter getActiveListAdapter();
  
  public abstract Object getAdapterForIndex(int paramInt);
  
  abstract Object getCurrentRootAdapter();
  
  abstract ViewGroup getInactiveAdapterView();
  
  public abstract ResolverListAdapter getInactiveListAdapter();
  
  abstract ProfileDescriptor getItem(int paramInt);
  
  abstract int getItemCount();
  
  abstract ResolverListAdapter getListAdapterForUserHandle(UserHandle paramUserHandle);
  
  abstract String getMetricsCategory();
  
  public abstract ResolverListAdapter getPersonalListAdapter();
  
  public abstract ResolverListAdapter getWorkListAdapter();
  
  abstract void setupListAdapter(int paramInt);
  
  protected abstract void showNoPersonalAppsAvailableEmptyState(ResolverListAdapter paramResolverListAdapter);
  
  protected abstract void showNoPersonalToWorkIntentsEmptyState(ResolverListAdapter paramResolverListAdapter);
  
  protected abstract void showNoWorkAppsAvailableEmptyState(ResolverListAdapter paramResolverListAdapter);
  
  protected abstract void showNoWorkToPersonalIntentsEmptyState(ResolverListAdapter paramResolverListAdapter);
  
  protected abstract void showWorkProfileOffEmptyState(ResolverListAdapter paramResolverListAdapter, View.OnClickListener paramOnClickListener);
  
  class Injector {
    public abstract boolean hasCrossProfileIntents(List<Intent> param1List, int param1Int1, int param1Int2);
    
    public abstract boolean isQuietModeEnabled(UserHandle param1UserHandle);
    
    public abstract void requestQuietModeEnabled(boolean param1Boolean, UserHandle param1UserHandle);
  }
  
  class OnProfileSelectedListener {
    public abstract void onProfilePageStateChanged(int param1Int);
    
    public abstract void onProfileSelected(int param1Int);
  }
  
  class OnSwitchOnWorkSelectedListener {
    public abstract void onSwitchOnWorkSelected();
  }
  
  class Profile implements Annotation {}
  
  class ProfileDescriptor {
    private final ViewGroup mEmptyStateView;
    
    final ViewGroup rootView;
    
    final AbstractMultiProfilePagerAdapter this$0;
    
    ProfileDescriptor(ViewGroup param1ViewGroup) {
      this.rootView = param1ViewGroup;
      this.mEmptyStateView = param1ViewGroup.<ViewGroup>findViewById(16909351);
    }
    
    protected ViewGroup getEmptyStateView() {
      return this.mEmptyStateView;
    }
  }
}
