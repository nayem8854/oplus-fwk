package com.android.internal.app;

import com.android.internal.logging.InstanceId;
import com.android.internal.logging.UiEventLogger;

public interface ChooserActivityLogger {
  default void logSharesheetTriggered() {
    log(SharesheetStandardEvent.SHARESHEET_TRIGGERED, getInstanceId());
  }
  
  default void logSharesheetAppLoadComplete() {
    log(SharesheetStandardEvent.SHARESHEET_APP_LOAD_COMPLETE, getInstanceId());
  }
  
  default void logSharesheetDirectLoadComplete() {
    log(SharesheetStandardEvent.SHARESHEET_DIRECT_LOAD_COMPLETE, getInstanceId());
  }
  
  default void logSharesheetDirectLoadTimeout() {
    log(SharesheetStandardEvent.SHARESHEET_DIRECT_LOAD_TIMEOUT, getInstanceId());
  }
  
  default void logShareheetProfileChanged() {
    log(SharesheetStandardEvent.SHARESHEET_PROFILE_CHANGED, getInstanceId());
  }
  
  default void logSharesheetExpansionChanged(boolean paramBoolean) {
    SharesheetStandardEvent sharesheetStandardEvent;
    if (paramBoolean) {
      sharesheetStandardEvent = SharesheetStandardEvent.SHARESHEET_COLLAPSED;
    } else {
      sharesheetStandardEvent = SharesheetStandardEvent.SHARESHEET_EXPANDED;
    } 
    InstanceId instanceId = getInstanceId();
    log(sharesheetStandardEvent, instanceId);
  }
  
  class SharesheetStartedEvent extends Enum<SharesheetStartedEvent> implements UiEventLogger.UiEventEnum {
    private static final SharesheetStartedEvent[] $VALUES;
    
    public static final SharesheetStartedEvent SHARE_STARTED;
    
    private final int mId;
    
    public static SharesheetStartedEvent valueOf(String param1String) {
      return Enum.<SharesheetStartedEvent>valueOf(SharesheetStartedEvent.class, param1String);
    }
    
    public static SharesheetStartedEvent[] values() {
      return (SharesheetStartedEvent[])$VALUES.clone();
    }
    
    static {
      SharesheetStartedEvent sharesheetStartedEvent = new SharesheetStartedEvent("SHARE_STARTED", 0, 228);
      $VALUES = new SharesheetStartedEvent[] { sharesheetStartedEvent };
    }
    
    private SharesheetStartedEvent(ChooserActivityLogger this$0, int param1Int1, int param1Int2) {
      super((String)this$0, param1Int1);
      this.mId = param1Int2;
    }
    
    public int getId() {
      return this.mId;
    }
  }
  
  class SharesheetTargetSelectedEvent extends Enum<SharesheetTargetSelectedEvent> implements UiEventLogger.UiEventEnum {
    private static final SharesheetTargetSelectedEvent[] $VALUES;
    
    public static SharesheetTargetSelectedEvent valueOf(String param1String) {
      return Enum.<SharesheetTargetSelectedEvent>valueOf(SharesheetTargetSelectedEvent.class, param1String);
    }
    
    public static SharesheetTargetSelectedEvent[] values() {
      return (SharesheetTargetSelectedEvent[])$VALUES.clone();
    }
    
    public static final SharesheetTargetSelectedEvent INVALID = new SharesheetTargetSelectedEvent("INVALID", 0, 0);
    
    public static final SharesheetTargetSelectedEvent SHARESHEET_APP_TARGET_SELECTED = new SharesheetTargetSelectedEvent("SHARESHEET_APP_TARGET_SELECTED", 2, 233);
    
    public static final SharesheetTargetSelectedEvent SHARESHEET_COPY_TARGET_SELECTED;
    
    public static final SharesheetTargetSelectedEvent SHARESHEET_SERVICE_TARGET_SELECTED = new SharesheetTargetSelectedEvent("SHARESHEET_SERVICE_TARGET_SELECTED", 1, 232);
    
    public static final SharesheetTargetSelectedEvent SHARESHEET_STANDARD_TARGET_SELECTED = new SharesheetTargetSelectedEvent("SHARESHEET_STANDARD_TARGET_SELECTED", 3, 234);
    
    private final int mId;
    
    static {
      SharesheetTargetSelectedEvent sharesheetTargetSelectedEvent = new SharesheetTargetSelectedEvent("SHARESHEET_COPY_TARGET_SELECTED", 4, 235);
      $VALUES = new SharesheetTargetSelectedEvent[] { INVALID, SHARESHEET_SERVICE_TARGET_SELECTED, SHARESHEET_APP_TARGET_SELECTED, SHARESHEET_STANDARD_TARGET_SELECTED, sharesheetTargetSelectedEvent };
    }
    
    private SharesheetTargetSelectedEvent(ChooserActivityLogger this$0, int param1Int1, int param1Int2) {
      super((String)this$0, param1Int1);
      this.mId = param1Int2;
    }
    
    public int getId() {
      return this.mId;
    }
    
    public static SharesheetTargetSelectedEvent fromTargetType(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return INVALID; 
            return SHARESHEET_COPY_TARGET_SELECTED;
          } 
          return SHARESHEET_STANDARD_TARGET_SELECTED;
        } 
        return SHARESHEET_APP_TARGET_SELECTED;
      } 
      return SHARESHEET_SERVICE_TARGET_SELECTED;
    }
  }
  
  class SharesheetStandardEvent extends Enum<SharesheetStandardEvent> implements UiEventLogger.UiEventEnum {
    private static final SharesheetStandardEvent[] $VALUES;
    
    public static SharesheetStandardEvent valueOf(String param1String) {
      return Enum.<SharesheetStandardEvent>valueOf(SharesheetStandardEvent.class, param1String);
    }
    
    public static SharesheetStandardEvent[] values() {
      return (SharesheetStandardEvent[])$VALUES.clone();
    }
    
    public static final SharesheetStandardEvent INVALID = new SharesheetStandardEvent("INVALID", 0, 0);
    
    public static final SharesheetStandardEvent SHARESHEET_APP_LOAD_COMPLETE;
    
    public static final SharesheetStandardEvent SHARESHEET_COLLAPSED;
    
    public static final SharesheetStandardEvent SHARESHEET_DIRECT_LOAD_COMPLETE;
    
    public static final SharesheetStandardEvent SHARESHEET_DIRECT_LOAD_TIMEOUT;
    
    public static final SharesheetStandardEvent SHARESHEET_EXPANDED;
    
    public static final SharesheetStandardEvent SHARESHEET_PROFILE_CHANGED = new SharesheetStandardEvent("SHARESHEET_PROFILE_CHANGED", 2, 229);
    
    public static final SharesheetStandardEvent SHARESHEET_TRIGGERED = new SharesheetStandardEvent("SHARESHEET_TRIGGERED", 1, 227);
    
    private final int mId;
    
    static {
      SHARESHEET_EXPANDED = new SharesheetStandardEvent("SHARESHEET_EXPANDED", 3, 230);
      SHARESHEET_COLLAPSED = new SharesheetStandardEvent("SHARESHEET_COLLAPSED", 4, 231);
      SHARESHEET_APP_LOAD_COMPLETE = new SharesheetStandardEvent("SHARESHEET_APP_LOAD_COMPLETE", 5, 322);
      SHARESHEET_DIRECT_LOAD_COMPLETE = new SharesheetStandardEvent("SHARESHEET_DIRECT_LOAD_COMPLETE", 6, 323);
      SharesheetStandardEvent sharesheetStandardEvent = new SharesheetStandardEvent("SHARESHEET_DIRECT_LOAD_TIMEOUT", 7, 324);
      $VALUES = new SharesheetStandardEvent[] { INVALID, SHARESHEET_TRIGGERED, SHARESHEET_PROFILE_CHANGED, SHARESHEET_EXPANDED, SHARESHEET_COLLAPSED, SHARESHEET_APP_LOAD_COMPLETE, SHARESHEET_DIRECT_LOAD_COMPLETE, sharesheetStandardEvent };
    }
    
    private SharesheetStandardEvent(ChooserActivityLogger this$0, int param1Int1, int param1Int2) {
      super((String)this$0, param1Int1);
      this.mId = param1Int2;
    }
    
    public int getId() {
      return this.mId;
    }
  }
  
  default int typeFromPreviewInt(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2)
        return 0; 
      return 2;
    } 
    return 1;
  }
  
  default int typeFromIntentString(String paramString) {
    if (paramString == null)
      return 0; 
    byte b = -1;
    switch (paramString.hashCode()) {
      case 2068787464:
        if (paramString.equals("android.intent.action.SENDTO"))
          b = 3; 
        break;
      case -58484670:
        if (paramString.equals("android.intent.action.SEND_MULTIPLE"))
          b = 4; 
        break;
      case -1173171990:
        if (paramString.equals("android.intent.action.VIEW"))
          b = 0; 
        break;
      case -1173264947:
        if (paramString.equals("android.intent.action.SEND"))
          b = 2; 
        break;
      case -1173447682:
        if (paramString.equals("android.intent.action.MAIN"))
          b = 6; 
        break;
      case -1173683121:
        if (paramString.equals("android.intent.action.EDIT"))
          b = 1; 
        break;
      case -1960745709:
        if (paramString.equals("android.media.action.IMAGE_CAPTURE"))
          b = 5; 
        break;
    } 
    switch (b) {
      default:
        return 0;
      case 6:
        return 7;
      case 5:
        return 6;
      case 4:
        return 5;
      case 3:
        return 4;
      case 2:
        return 3;
      case 1:
        return 2;
      case 0:
        break;
    } 
    return 1;
  }
  
  InstanceId getInstanceId();
  
  void log(UiEventLogger.UiEventEnum paramUiEventEnum, InstanceId paramInstanceId);
  
  void logShareStarted(int paramInt1, String paramString1, String paramString2, int paramInt2, int paramInt3, boolean paramBoolean, int paramInt4, String paramString3);
  
  void logShareTargetSelected(int paramInt1, String paramString, int paramInt2);
}
