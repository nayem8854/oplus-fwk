package com.android.internal.app;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.media.MediaRouter;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.SeekBar;

public class MediaRouteControllerDialog extends AlertDialog {
  private static final int VOLUME_UPDATE_DELAY_MILLIS = 250;
  
  private boolean mAttachedToWindow;
  
  private final MediaRouterCallback mCallback;
  
  private View mControlView;
  
  private boolean mCreated;
  
  private Drawable mCurrentIconDrawable;
  
  private Drawable mMediaRouteButtonDrawable;
  
  private int[] mMediaRouteConnectingState = new int[] { 16842912, 16842910 };
  
  private int[] mMediaRouteOnState = new int[] { 16843518, 16842910 };
  
  private final MediaRouter.RouteInfo mRoute;
  
  private final MediaRouter mRouter;
  
  private boolean mVolumeControlEnabled = true;
  
  private LinearLayout mVolumeLayout;
  
  private SeekBar mVolumeSlider;
  
  private boolean mVolumeSliderTouched;
  
  public MediaRouteControllerDialog(Context paramContext, int paramInt) {
    super(paramContext, paramInt);
    this.mRouter = (MediaRouter)paramContext.getSystemService("media_router");
    this.mCallback = new MediaRouterCallback();
    this.mRoute = this.mRouter.getSelectedRoute();
  }
  
  public MediaRouter.RouteInfo getRoute() {
    return this.mRoute;
  }
  
  public View onCreateMediaControlView(Bundle paramBundle) {
    return null;
  }
  
  public View getMediaControlView() {
    return this.mControlView;
  }
  
  public void setVolumeControlEnabled(boolean paramBoolean) {
    if (this.mVolumeControlEnabled != paramBoolean) {
      this.mVolumeControlEnabled = paramBoolean;
      if (this.mCreated)
        updateVolume(); 
    } 
  }
  
  public boolean isVolumeControlEnabled() {
    return this.mVolumeControlEnabled;
  }
  
  protected void onCreate(Bundle paramBundle) {
    setTitle(this.mRoute.getName());
    Resources resources = getContext().getResources();
    setButton(-2, resources.getString(17040535), (DialogInterface.OnClickListener)new Object(this));
    View view1 = getLayoutInflater().inflate(17367195, (ViewGroup)null);
    setView(view1, 0, 0, 0, 0);
    super.onCreate(paramBundle);
    View view2 = (View)getWindow().findViewById(16908901);
    if (view2 != null)
      view2.setMinimumHeight(0); 
    this.mVolumeLayout = view1.<LinearLayout>findViewById(16909158);
    this.mVolumeSlider = (SeekBar)(view2 = view1.<SeekBar>findViewById(16909159));
    view2.setOnSeekBarChangeListener((SeekBar.OnSeekBarChangeListener)new Object(this));
    this.mMediaRouteButtonDrawable = obtainMediaRouteButtonDrawable();
    this.mCreated = true;
    if (update()) {
      this.mControlView = onCreateMediaControlView(paramBundle);
      view1 = view1.<FrameLayout>findViewById(16909155);
      View view = this.mControlView;
      if (view != null) {
        view1.addView(view);
        view1.setVisibility(0);
      } else {
        view1.setVisibility(8);
      } 
    } 
  }
  
  public void onAttachedToWindow() {
    super.onAttachedToWindow();
    this.mAttachedToWindow = true;
    this.mRouter.addCallback(0, (MediaRouter.Callback)this.mCallback, 2);
    update();
  }
  
  public void onDetachedFromWindow() {
    this.mRouter.removeCallback((MediaRouter.Callback)this.mCallback);
    this.mAttachedToWindow = false;
    super.onDetachedFromWindow();
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
    MediaRouter.RouteInfo routeInfo;
    if (paramInt == 25 || paramInt == 24) {
      routeInfo = this.mRoute;
      if (paramInt == 25) {
        paramInt = -1;
      } else {
        paramInt = 1;
      } 
      routeInfo.requestUpdateVolume(paramInt);
      return true;
    } 
    return super.onKeyDown(paramInt, (KeyEvent)routeInfo);
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent) {
    if (paramInt == 25 || paramInt == 24)
      return true; 
    return super.onKeyUp(paramInt, paramKeyEvent);
  }
  
  private boolean update() {
    if (!this.mRoute.isSelected() || this.mRoute.isDefault()) {
      dismiss();
      return false;
    } 
    setTitle(this.mRoute.getName());
    updateVolume();
    Drawable drawable = getIconDrawable();
    if (drawable != this.mCurrentIconDrawable) {
      this.mCurrentIconDrawable = drawable;
      Drawable drawable1 = drawable;
      if (drawable instanceof AnimationDrawable) {
        AnimationDrawable animationDrawable = (AnimationDrawable)drawable;
        if (!this.mAttachedToWindow && !this.mRoute.isConnecting()) {
          if (animationDrawable.isRunning())
            animationDrawable.stop(); 
          drawable1 = animationDrawable.getFrame(animationDrawable.getNumberOfFrames() - 1);
        } else {
          drawable1 = drawable;
          if (!animationDrawable.isRunning()) {
            animationDrawable.start();
            drawable1 = drawable;
          } 
        } 
      } 
      setIcon(drawable1);
    } 
    return true;
  }
  
  private Drawable obtainMediaRouteButtonDrawable() {
    Context context = getContext();
    TypedValue typedValue = new TypedValue();
    if (!context.getTheme().resolveAttribute(16843693, typedValue, true))
      return null; 
    TypedArray typedArray = context.obtainStyledAttributes(typedValue.data, new int[] { 17956923 });
    Drawable drawable = typedArray.getDrawable(0);
    typedArray.recycle();
    return drawable;
  }
  
  private Drawable getIconDrawable() {
    Drawable drawable = this.mMediaRouteButtonDrawable;
    if (!(drawable instanceof StateListDrawable))
      return drawable; 
    if (this.mRoute.isConnecting()) {
      StateListDrawable stateListDrawable1 = (StateListDrawable)this.mMediaRouteButtonDrawable;
      stateListDrawable1.setState(this.mMediaRouteConnectingState);
      return stateListDrawable1.getCurrent();
    } 
    StateListDrawable stateListDrawable = (StateListDrawable)this.mMediaRouteButtonDrawable;
    stateListDrawable.setState(this.mMediaRouteOnState);
    return stateListDrawable.getCurrent();
  }
  
  private void updateVolume() {
    if (!this.mVolumeSliderTouched)
      if (isVolumeControlAvailable()) {
        this.mVolumeLayout.setVisibility(0);
        this.mVolumeSlider.setMax(this.mRoute.getVolumeMax());
        this.mVolumeSlider.setProgress(this.mRoute.getVolume());
      } else {
        this.mVolumeLayout.setVisibility(8);
      }  
  }
  
  private boolean isVolumeControlAvailable() {
    boolean bool = this.mVolumeControlEnabled;
    boolean bool1 = true;
    if (!bool || this.mRoute.getVolumeHandling() != 1)
      bool1 = false; 
    return bool1;
  }
  
  class MediaRouterCallback extends MediaRouter.SimpleCallback {
    final MediaRouteControllerDialog this$0;
    
    private MediaRouterCallback() {}
    
    public void onRouteUnselected(MediaRouter param1MediaRouter, int param1Int, MediaRouter.RouteInfo param1RouteInfo) {
      MediaRouteControllerDialog.this.update();
    }
    
    public void onRouteChanged(MediaRouter param1MediaRouter, MediaRouter.RouteInfo param1RouteInfo) {
      MediaRouteControllerDialog.this.update();
    }
    
    public void onRouteVolumeChanged(MediaRouter param1MediaRouter, MediaRouter.RouteInfo param1RouteInfo) {
      if (param1RouteInfo == MediaRouteControllerDialog.this.mRoute)
        MediaRouteControllerDialog.this.updateVolume(); 
    }
    
    public void onRouteGrouped(MediaRouter param1MediaRouter, MediaRouter.RouteInfo param1RouteInfo, MediaRouter.RouteGroup param1RouteGroup, int param1Int) {
      MediaRouteControllerDialog.this.update();
    }
    
    public void onRouteUngrouped(MediaRouter param1MediaRouter, MediaRouter.RouteInfo param1RouteInfo, MediaRouter.RouteGroup param1RouteGroup) {
      MediaRouteControllerDialog.this.update();
    }
  }
}
