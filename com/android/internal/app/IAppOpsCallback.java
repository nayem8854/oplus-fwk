package com.android.internal.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IAppOpsCallback extends IInterface {
  void opChanged(int paramInt1, int paramInt2, String paramString) throws RemoteException;
  
  class Default implements IAppOpsCallback {
    public void opChanged(int param1Int1, int param1Int2, String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAppOpsCallback {
    private static final String DESCRIPTOR = "com.android.internal.app.IAppOpsCallback";
    
    static final int TRANSACTION_opChanged = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.app.IAppOpsCallback");
    }
    
    public static IAppOpsCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.app.IAppOpsCallback");
      if (iInterface != null && iInterface instanceof IAppOpsCallback)
        return (IAppOpsCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "opChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.app.IAppOpsCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.app.IAppOpsCallback");
      param1Int2 = param1Parcel1.readInt();
      param1Int1 = param1Parcel1.readInt();
      String str = param1Parcel1.readString();
      opChanged(param1Int2, param1Int1, str);
      return true;
    }
    
    private static class Proxy implements IAppOpsCallback {
      public static IAppOpsCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.app.IAppOpsCallback";
      }
      
      public void opChanged(int param2Int1, int param2Int2, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.app.IAppOpsCallback");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IAppOpsCallback.Stub.getDefaultImpl() != null) {
            IAppOpsCallback.Stub.getDefaultImpl().opChanged(param2Int1, param2Int2, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAppOpsCallback param1IAppOpsCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAppOpsCallback != null) {
          Proxy.sDefaultImpl = param1IAppOpsCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAppOpsCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
