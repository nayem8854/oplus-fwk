package com.android.internal.app;

import android.util.ArrayMap;
import android.util.SparseArray;

public class ProcessMap<E> {
  final ArrayMap<String, SparseArray<E>> mMap = new ArrayMap<>();
  
  public E get(String paramString, int paramInt) {
    SparseArray<E> sparseArray = this.mMap.get(paramString);
    if (sparseArray == null)
      return null; 
    return sparseArray.get(paramInt);
  }
  
  public E put(String paramString, int paramInt, E paramE) {
    SparseArray<E> sparseArray1 = this.mMap.get(paramString);
    SparseArray<E> sparseArray2 = sparseArray1;
    if (sparseArray1 == null) {
      sparseArray2 = new SparseArray(2);
      this.mMap.put(paramString, sparseArray2);
    } 
    sparseArray2.put(paramInt, paramE);
    return paramE;
  }
  
  public E remove(String paramString, int paramInt) {
    SparseArray<Object> sparseArray = (SparseArray)this.mMap.get(paramString);
    if (sparseArray != null) {
      E e = (E)sparseArray.removeReturnOld(paramInt);
      if (sparseArray.size() == 0)
        this.mMap.remove(paramString); 
      return e;
    } 
    return null;
  }
  
  public ArrayMap<String, SparseArray<E>> getMap() {
    return this.mMap;
  }
  
  public int size() {
    return this.mMap.size();
  }
}
