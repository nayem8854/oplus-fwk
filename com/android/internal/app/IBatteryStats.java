package com.android.internal.app;

import android.bluetooth.BluetoothActivityEnergyInfo;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.WorkSource;
import android.os.connectivity.CellularBatteryStats;
import android.os.connectivity.GpsBatteryStats;
import android.os.connectivity.WifiActivityEnergyInfo;
import android.os.connectivity.WifiBatteryStats;
import android.os.health.HealthStatsParceler;
import android.telephony.ModemActivityInfo;
import android.telephony.SignalStrength;

public interface IBatteryStats extends IInterface {
  long computeBatteryTimeRemaining() throws RemoteException;
  
  long computeChargeTimeRemaining() throws RemoteException;
  
  long getAwakeTimeBattery() throws RemoteException;
  
  long getAwakeTimePlugged() throws RemoteException;
  
  CellularBatteryStats getCellularBatteryStats() throws RemoteException;
  
  GpsBatteryStats getGpsBatteryStats() throws RemoteException;
  
  byte[] getStatistics() throws RemoteException;
  
  ParcelFileDescriptor getStatisticsStream() throws RemoteException;
  
  WifiBatteryStats getWifiBatteryStats() throws RemoteException;
  
  boolean isCharging() throws RemoteException;
  
  void noteBleScanResults(WorkSource paramWorkSource, int paramInt) throws RemoteException;
  
  void noteBleScanStarted(WorkSource paramWorkSource, boolean paramBoolean) throws RemoteException;
  
  void noteBleScanStopped(WorkSource paramWorkSource, boolean paramBoolean) throws RemoteException;
  
  void noteBluetoothControllerActivity(BluetoothActivityEnergyInfo paramBluetoothActivityEnergyInfo) throws RemoteException;
  
  void noteChangeWakelockFromSource(WorkSource paramWorkSource1, int paramInt1, String paramString1, String paramString2, int paramInt2, WorkSource paramWorkSource2, int paramInt3, String paramString3, String paramString4, int paramInt4, boolean paramBoolean) throws RemoteException;
  
  void noteConnectivityChanged(int paramInt, String paramString) throws RemoteException;
  
  void noteDeviceIdleMode(int paramInt1, String paramString, int paramInt2) throws RemoteException;
  
  void noteEvent(int paramInt1, String paramString, int paramInt2) throws RemoteException;
  
  void noteFlashlightOff(int paramInt) throws RemoteException;
  
  void noteFlashlightOn(int paramInt) throws RemoteException;
  
  void noteFullWifiLockAcquired(int paramInt) throws RemoteException;
  
  void noteFullWifiLockAcquiredFromSource(WorkSource paramWorkSource) throws RemoteException;
  
  void noteFullWifiLockReleased(int paramInt) throws RemoteException;
  
  void noteFullWifiLockReleasedFromSource(WorkSource paramWorkSource) throws RemoteException;
  
  void noteGpsChanged(WorkSource paramWorkSource1, WorkSource paramWorkSource2) throws RemoteException;
  
  void noteGpsSignalQuality(int paramInt) throws RemoteException;
  
  void noteInteractive(boolean paramBoolean) throws RemoteException;
  
  void noteJobFinish(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void noteJobStart(String paramString, int paramInt) throws RemoteException;
  
  void noteLongPartialWakelockFinish(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  void noteLongPartialWakelockFinishFromSource(String paramString1, String paramString2, WorkSource paramWorkSource) throws RemoteException;
  
  void noteLongPartialWakelockStart(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  void noteLongPartialWakelockStartFromSource(String paramString1, String paramString2, WorkSource paramWorkSource) throws RemoteException;
  
  void noteMobileRadioPowerState(int paramInt1, long paramLong, int paramInt2) throws RemoteException;
  
  void noteModemControllerActivity(ModemActivityInfo paramModemActivityInfo) throws RemoteException;
  
  void noteNetworkInterfaceType(String paramString, int paramInt) throws RemoteException;
  
  void noteNetworkStatsEnabled() throws RemoteException;
  
  void notePhoneDataConnectionState(int paramInt1, boolean paramBoolean, int paramInt2) throws RemoteException;
  
  void notePhoneOff() throws RemoteException;
  
  void notePhoneOn() throws RemoteException;
  
  void notePhoneSignalStrength(SignalStrength paramSignalStrength) throws RemoteException;
  
  void notePhoneState(int paramInt) throws RemoteException;
  
  void noteResetAudio() throws RemoteException;
  
  void noteResetBleScan() throws RemoteException;
  
  void noteResetCamera() throws RemoteException;
  
  void noteResetFlashlight() throws RemoteException;
  
  void noteResetVideo() throws RemoteException;
  
  void noteScreenBrightness(int paramInt) throws RemoteException;
  
  void noteScreenState(int paramInt) throws RemoteException;
  
  void noteStartAudio(int paramInt) throws RemoteException;
  
  void noteStartCamera(int paramInt) throws RemoteException;
  
  void noteStartSensor(int paramInt1, int paramInt2) throws RemoteException;
  
  void noteStartVideo(int paramInt) throws RemoteException;
  
  void noteStartWakelock(int paramInt1, int paramInt2, String paramString1, String paramString2, int paramInt3, boolean paramBoolean) throws RemoteException;
  
  void noteStartWakelockFromSource(WorkSource paramWorkSource, int paramInt1, String paramString1, String paramString2, int paramInt2, boolean paramBoolean) throws RemoteException;
  
  void noteStopAudio(int paramInt) throws RemoteException;
  
  void noteStopCamera(int paramInt) throws RemoteException;
  
  void noteStopSensor(int paramInt1, int paramInt2) throws RemoteException;
  
  void noteStopVideo(int paramInt) throws RemoteException;
  
  void noteStopWakelock(int paramInt1, int paramInt2, String paramString1, String paramString2, int paramInt3) throws RemoteException;
  
  void noteStopWakelockFromSource(WorkSource paramWorkSource, int paramInt1, String paramString1, String paramString2, int paramInt2) throws RemoteException;
  
  void noteSyncFinish(String paramString, int paramInt) throws RemoteException;
  
  void noteSyncStart(String paramString, int paramInt) throws RemoteException;
  
  void noteUserActivity(int paramInt1, int paramInt2) throws RemoteException;
  
  void noteVibratorOff(int paramInt) throws RemoteException;
  
  void noteVibratorOn(int paramInt, long paramLong) throws RemoteException;
  
  void noteWakeUp(String paramString, int paramInt) throws RemoteException;
  
  void noteWifiBatchedScanStartedFromSource(WorkSource paramWorkSource, int paramInt) throws RemoteException;
  
  void noteWifiBatchedScanStoppedFromSource(WorkSource paramWorkSource) throws RemoteException;
  
  void noteWifiControllerActivity(WifiActivityEnergyInfo paramWifiActivityEnergyInfo) throws RemoteException;
  
  void noteWifiMulticastDisabled(int paramInt) throws RemoteException;
  
  void noteWifiMulticastEnabled(int paramInt) throws RemoteException;
  
  void noteWifiOff() throws RemoteException;
  
  void noteWifiOn() throws RemoteException;
  
  void noteWifiRadioPowerState(int paramInt1, long paramLong, int paramInt2) throws RemoteException;
  
  void noteWifiRssiChanged(int paramInt) throws RemoteException;
  
  void noteWifiRunning(WorkSource paramWorkSource) throws RemoteException;
  
  void noteWifiRunningChanged(WorkSource paramWorkSource1, WorkSource paramWorkSource2) throws RemoteException;
  
  void noteWifiScanStarted(int paramInt) throws RemoteException;
  
  void noteWifiScanStartedFromSource(WorkSource paramWorkSource) throws RemoteException;
  
  void noteWifiScanStopped(int paramInt) throws RemoteException;
  
  void noteWifiScanStoppedFromSource(WorkSource paramWorkSource) throws RemoteException;
  
  void noteWifiState(int paramInt, String paramString) throws RemoteException;
  
  void noteWifiStopped(WorkSource paramWorkSource) throws RemoteException;
  
  void noteWifiSupplicantStateChanged(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setBatteryState(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, long paramLong) throws RemoteException;
  
  boolean setChargingStateUpdateDelayMillis(int paramInt) throws RemoteException;
  
  HealthStatsParceler takeUidSnapshot(int paramInt) throws RemoteException;
  
  HealthStatsParceler[] takeUidSnapshots(int[] paramArrayOfint) throws RemoteException;
  
  class Default implements IBatteryStats {
    public void noteStartSensor(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void noteStopSensor(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void noteStartVideo(int param1Int) throws RemoteException {}
    
    public void noteStopVideo(int param1Int) throws RemoteException {}
    
    public void noteStartAudio(int param1Int) throws RemoteException {}
    
    public void noteStopAudio(int param1Int) throws RemoteException {}
    
    public void noteResetVideo() throws RemoteException {}
    
    public void noteResetAudio() throws RemoteException {}
    
    public void noteFlashlightOn(int param1Int) throws RemoteException {}
    
    public void noteFlashlightOff(int param1Int) throws RemoteException {}
    
    public void noteStartCamera(int param1Int) throws RemoteException {}
    
    public void noteStopCamera(int param1Int) throws RemoteException {}
    
    public void noteResetCamera() throws RemoteException {}
    
    public void noteResetFlashlight() throws RemoteException {}
    
    public byte[] getStatistics() throws RemoteException {
      return null;
    }
    
    public ParcelFileDescriptor getStatisticsStream() throws RemoteException {
      return null;
    }
    
    public boolean isCharging() throws RemoteException {
      return false;
    }
    
    public long computeBatteryTimeRemaining() throws RemoteException {
      return 0L;
    }
    
    public long computeChargeTimeRemaining() throws RemoteException {
      return 0L;
    }
    
    public void noteEvent(int param1Int1, String param1String, int param1Int2) throws RemoteException {}
    
    public void noteSyncStart(String param1String, int param1Int) throws RemoteException {}
    
    public void noteSyncFinish(String param1String, int param1Int) throws RemoteException {}
    
    public void noteJobStart(String param1String, int param1Int) throws RemoteException {}
    
    public void noteJobFinish(String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void noteStartWakelock(int param1Int1, int param1Int2, String param1String1, String param1String2, int param1Int3, boolean param1Boolean) throws RemoteException {}
    
    public void noteStopWakelock(int param1Int1, int param1Int2, String param1String1, String param1String2, int param1Int3) throws RemoteException {}
    
    public void noteStartWakelockFromSource(WorkSource param1WorkSource, int param1Int1, String param1String1, String param1String2, int param1Int2, boolean param1Boolean) throws RemoteException {}
    
    public void noteChangeWakelockFromSource(WorkSource param1WorkSource1, int param1Int1, String param1String1, String param1String2, int param1Int2, WorkSource param1WorkSource2, int param1Int3, String param1String3, String param1String4, int param1Int4, boolean param1Boolean) throws RemoteException {}
    
    public void noteStopWakelockFromSource(WorkSource param1WorkSource, int param1Int1, String param1String1, String param1String2, int param1Int2) throws RemoteException {}
    
    public void noteLongPartialWakelockStart(String param1String1, String param1String2, int param1Int) throws RemoteException {}
    
    public void noteLongPartialWakelockStartFromSource(String param1String1, String param1String2, WorkSource param1WorkSource) throws RemoteException {}
    
    public void noteLongPartialWakelockFinish(String param1String1, String param1String2, int param1Int) throws RemoteException {}
    
    public void noteLongPartialWakelockFinishFromSource(String param1String1, String param1String2, WorkSource param1WorkSource) throws RemoteException {}
    
    public void noteVibratorOn(int param1Int, long param1Long) throws RemoteException {}
    
    public void noteVibratorOff(int param1Int) throws RemoteException {}
    
    public void noteGpsChanged(WorkSource param1WorkSource1, WorkSource param1WorkSource2) throws RemoteException {}
    
    public void noteGpsSignalQuality(int param1Int) throws RemoteException {}
    
    public void noteScreenState(int param1Int) throws RemoteException {}
    
    public void noteScreenBrightness(int param1Int) throws RemoteException {}
    
    public void noteUserActivity(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void noteWakeUp(String param1String, int param1Int) throws RemoteException {}
    
    public void noteInteractive(boolean param1Boolean) throws RemoteException {}
    
    public void noteConnectivityChanged(int param1Int, String param1String) throws RemoteException {}
    
    public void noteMobileRadioPowerState(int param1Int1, long param1Long, int param1Int2) throws RemoteException {}
    
    public void notePhoneOn() throws RemoteException {}
    
    public void notePhoneOff() throws RemoteException {}
    
    public void notePhoneSignalStrength(SignalStrength param1SignalStrength) throws RemoteException {}
    
    public void notePhoneDataConnectionState(int param1Int1, boolean param1Boolean, int param1Int2) throws RemoteException {}
    
    public void notePhoneState(int param1Int) throws RemoteException {}
    
    public void noteWifiOn() throws RemoteException {}
    
    public void noteWifiOff() throws RemoteException {}
    
    public void noteWifiRunning(WorkSource param1WorkSource) throws RemoteException {}
    
    public void noteWifiRunningChanged(WorkSource param1WorkSource1, WorkSource param1WorkSource2) throws RemoteException {}
    
    public void noteWifiStopped(WorkSource param1WorkSource) throws RemoteException {}
    
    public void noteWifiState(int param1Int, String param1String) throws RemoteException {}
    
    public void noteWifiSupplicantStateChanged(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void noteWifiRssiChanged(int param1Int) throws RemoteException {}
    
    public void noteFullWifiLockAcquired(int param1Int) throws RemoteException {}
    
    public void noteFullWifiLockReleased(int param1Int) throws RemoteException {}
    
    public void noteWifiScanStarted(int param1Int) throws RemoteException {}
    
    public void noteWifiScanStopped(int param1Int) throws RemoteException {}
    
    public void noteWifiMulticastEnabled(int param1Int) throws RemoteException {}
    
    public void noteWifiMulticastDisabled(int param1Int) throws RemoteException {}
    
    public void noteFullWifiLockAcquiredFromSource(WorkSource param1WorkSource) throws RemoteException {}
    
    public void noteFullWifiLockReleasedFromSource(WorkSource param1WorkSource) throws RemoteException {}
    
    public void noteWifiScanStartedFromSource(WorkSource param1WorkSource) throws RemoteException {}
    
    public void noteWifiScanStoppedFromSource(WorkSource param1WorkSource) throws RemoteException {}
    
    public void noteWifiBatchedScanStartedFromSource(WorkSource param1WorkSource, int param1Int) throws RemoteException {}
    
    public void noteWifiBatchedScanStoppedFromSource(WorkSource param1WorkSource) throws RemoteException {}
    
    public void noteWifiRadioPowerState(int param1Int1, long param1Long, int param1Int2) throws RemoteException {}
    
    public void noteNetworkInterfaceType(String param1String, int param1Int) throws RemoteException {}
    
    public void noteNetworkStatsEnabled() throws RemoteException {}
    
    public void noteDeviceIdleMode(int param1Int1, String param1String, int param1Int2) throws RemoteException {}
    
    public void setBatteryState(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6, int param1Int7, int param1Int8, long param1Long) throws RemoteException {}
    
    public long getAwakeTimeBattery() throws RemoteException {
      return 0L;
    }
    
    public long getAwakeTimePlugged() throws RemoteException {
      return 0L;
    }
    
    public void noteBleScanStarted(WorkSource param1WorkSource, boolean param1Boolean) throws RemoteException {}
    
    public void noteBleScanStopped(WorkSource param1WorkSource, boolean param1Boolean) throws RemoteException {}
    
    public void noteResetBleScan() throws RemoteException {}
    
    public void noteBleScanResults(WorkSource param1WorkSource, int param1Int) throws RemoteException {}
    
    public CellularBatteryStats getCellularBatteryStats() throws RemoteException {
      return null;
    }
    
    public WifiBatteryStats getWifiBatteryStats() throws RemoteException {
      return null;
    }
    
    public GpsBatteryStats getGpsBatteryStats() throws RemoteException {
      return null;
    }
    
    public HealthStatsParceler takeUidSnapshot(int param1Int) throws RemoteException {
      return null;
    }
    
    public HealthStatsParceler[] takeUidSnapshots(int[] param1ArrayOfint) throws RemoteException {
      return null;
    }
    
    public void noteBluetoothControllerActivity(BluetoothActivityEnergyInfo param1BluetoothActivityEnergyInfo) throws RemoteException {}
    
    public void noteModemControllerActivity(ModemActivityInfo param1ModemActivityInfo) throws RemoteException {}
    
    public void noteWifiControllerActivity(WifiActivityEnergyInfo param1WifiActivityEnergyInfo) throws RemoteException {}
    
    public boolean setChargingStateUpdateDelayMillis(int param1Int) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IBatteryStats {
    private static final String DESCRIPTOR = "com.android.internal.app.IBatteryStats";
    
    static final int TRANSACTION_computeBatteryTimeRemaining = 18;
    
    static final int TRANSACTION_computeChargeTimeRemaining = 19;
    
    static final int TRANSACTION_getAwakeTimeBattery = 75;
    
    static final int TRANSACTION_getAwakeTimePlugged = 76;
    
    static final int TRANSACTION_getCellularBatteryStats = 81;
    
    static final int TRANSACTION_getGpsBatteryStats = 83;
    
    static final int TRANSACTION_getStatistics = 15;
    
    static final int TRANSACTION_getStatisticsStream = 16;
    
    static final int TRANSACTION_getWifiBatteryStats = 82;
    
    static final int TRANSACTION_isCharging = 17;
    
    static final int TRANSACTION_noteBleScanResults = 80;
    
    static final int TRANSACTION_noteBleScanStarted = 77;
    
    static final int TRANSACTION_noteBleScanStopped = 78;
    
    static final int TRANSACTION_noteBluetoothControllerActivity = 86;
    
    static final int TRANSACTION_noteChangeWakelockFromSource = 28;
    
    static final int TRANSACTION_noteConnectivityChanged = 43;
    
    static final int TRANSACTION_noteDeviceIdleMode = 73;
    
    static final int TRANSACTION_noteEvent = 20;
    
    static final int TRANSACTION_noteFlashlightOff = 10;
    
    static final int TRANSACTION_noteFlashlightOn = 9;
    
    static final int TRANSACTION_noteFullWifiLockAcquired = 58;
    
    static final int TRANSACTION_noteFullWifiLockAcquiredFromSource = 64;
    
    static final int TRANSACTION_noteFullWifiLockReleased = 59;
    
    static final int TRANSACTION_noteFullWifiLockReleasedFromSource = 65;
    
    static final int TRANSACTION_noteGpsChanged = 36;
    
    static final int TRANSACTION_noteGpsSignalQuality = 37;
    
    static final int TRANSACTION_noteInteractive = 42;
    
    static final int TRANSACTION_noteJobFinish = 24;
    
    static final int TRANSACTION_noteJobStart = 23;
    
    static final int TRANSACTION_noteLongPartialWakelockFinish = 32;
    
    static final int TRANSACTION_noteLongPartialWakelockFinishFromSource = 33;
    
    static final int TRANSACTION_noteLongPartialWakelockStart = 30;
    
    static final int TRANSACTION_noteLongPartialWakelockStartFromSource = 31;
    
    static final int TRANSACTION_noteMobileRadioPowerState = 44;
    
    static final int TRANSACTION_noteModemControllerActivity = 87;
    
    static final int TRANSACTION_noteNetworkInterfaceType = 71;
    
    static final int TRANSACTION_noteNetworkStatsEnabled = 72;
    
    static final int TRANSACTION_notePhoneDataConnectionState = 48;
    
    static final int TRANSACTION_notePhoneOff = 46;
    
    static final int TRANSACTION_notePhoneOn = 45;
    
    static final int TRANSACTION_notePhoneSignalStrength = 47;
    
    static final int TRANSACTION_notePhoneState = 49;
    
    static final int TRANSACTION_noteResetAudio = 8;
    
    static final int TRANSACTION_noteResetBleScan = 79;
    
    static final int TRANSACTION_noteResetCamera = 13;
    
    static final int TRANSACTION_noteResetFlashlight = 14;
    
    static final int TRANSACTION_noteResetVideo = 7;
    
    static final int TRANSACTION_noteScreenBrightness = 39;
    
    static final int TRANSACTION_noteScreenState = 38;
    
    static final int TRANSACTION_noteStartAudio = 5;
    
    static final int TRANSACTION_noteStartCamera = 11;
    
    static final int TRANSACTION_noteStartSensor = 1;
    
    static final int TRANSACTION_noteStartVideo = 3;
    
    static final int TRANSACTION_noteStartWakelock = 25;
    
    static final int TRANSACTION_noteStartWakelockFromSource = 27;
    
    static final int TRANSACTION_noteStopAudio = 6;
    
    static final int TRANSACTION_noteStopCamera = 12;
    
    static final int TRANSACTION_noteStopSensor = 2;
    
    static final int TRANSACTION_noteStopVideo = 4;
    
    static final int TRANSACTION_noteStopWakelock = 26;
    
    static final int TRANSACTION_noteStopWakelockFromSource = 29;
    
    static final int TRANSACTION_noteSyncFinish = 22;
    
    static final int TRANSACTION_noteSyncStart = 21;
    
    static final int TRANSACTION_noteUserActivity = 40;
    
    static final int TRANSACTION_noteVibratorOff = 35;
    
    static final int TRANSACTION_noteVibratorOn = 34;
    
    static final int TRANSACTION_noteWakeUp = 41;
    
    static final int TRANSACTION_noteWifiBatchedScanStartedFromSource = 68;
    
    static final int TRANSACTION_noteWifiBatchedScanStoppedFromSource = 69;
    
    static final int TRANSACTION_noteWifiControllerActivity = 88;
    
    static final int TRANSACTION_noteWifiMulticastDisabled = 63;
    
    static final int TRANSACTION_noteWifiMulticastEnabled = 62;
    
    static final int TRANSACTION_noteWifiOff = 51;
    
    static final int TRANSACTION_noteWifiOn = 50;
    
    static final int TRANSACTION_noteWifiRadioPowerState = 70;
    
    static final int TRANSACTION_noteWifiRssiChanged = 57;
    
    static final int TRANSACTION_noteWifiRunning = 52;
    
    static final int TRANSACTION_noteWifiRunningChanged = 53;
    
    static final int TRANSACTION_noteWifiScanStarted = 60;
    
    static final int TRANSACTION_noteWifiScanStartedFromSource = 66;
    
    static final int TRANSACTION_noteWifiScanStopped = 61;
    
    static final int TRANSACTION_noteWifiScanStoppedFromSource = 67;
    
    static final int TRANSACTION_noteWifiState = 55;
    
    static final int TRANSACTION_noteWifiStopped = 54;
    
    static final int TRANSACTION_noteWifiSupplicantStateChanged = 56;
    
    static final int TRANSACTION_setBatteryState = 74;
    
    static final int TRANSACTION_setChargingStateUpdateDelayMillis = 89;
    
    static final int TRANSACTION_takeUidSnapshot = 84;
    
    static final int TRANSACTION_takeUidSnapshots = 85;
    
    public Stub() {
      attachInterface(this, "com.android.internal.app.IBatteryStats");
    }
    
    public static IBatteryStats asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.app.IBatteryStats");
      if (iInterface != null && iInterface instanceof IBatteryStats)
        return (IBatteryStats)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 89:
          return "setChargingStateUpdateDelayMillis";
        case 88:
          return "noteWifiControllerActivity";
        case 87:
          return "noteModemControllerActivity";
        case 86:
          return "noteBluetoothControllerActivity";
        case 85:
          return "takeUidSnapshots";
        case 84:
          return "takeUidSnapshot";
        case 83:
          return "getGpsBatteryStats";
        case 82:
          return "getWifiBatteryStats";
        case 81:
          return "getCellularBatteryStats";
        case 80:
          return "noteBleScanResults";
        case 79:
          return "noteResetBleScan";
        case 78:
          return "noteBleScanStopped";
        case 77:
          return "noteBleScanStarted";
        case 76:
          return "getAwakeTimePlugged";
        case 75:
          return "getAwakeTimeBattery";
        case 74:
          return "setBatteryState";
        case 73:
          return "noteDeviceIdleMode";
        case 72:
          return "noteNetworkStatsEnabled";
        case 71:
          return "noteNetworkInterfaceType";
        case 70:
          return "noteWifiRadioPowerState";
        case 69:
          return "noteWifiBatchedScanStoppedFromSource";
        case 68:
          return "noteWifiBatchedScanStartedFromSource";
        case 67:
          return "noteWifiScanStoppedFromSource";
        case 66:
          return "noteWifiScanStartedFromSource";
        case 65:
          return "noteFullWifiLockReleasedFromSource";
        case 64:
          return "noteFullWifiLockAcquiredFromSource";
        case 63:
          return "noteWifiMulticastDisabled";
        case 62:
          return "noteWifiMulticastEnabled";
        case 61:
          return "noteWifiScanStopped";
        case 60:
          return "noteWifiScanStarted";
        case 59:
          return "noteFullWifiLockReleased";
        case 58:
          return "noteFullWifiLockAcquired";
        case 57:
          return "noteWifiRssiChanged";
        case 56:
          return "noteWifiSupplicantStateChanged";
        case 55:
          return "noteWifiState";
        case 54:
          return "noteWifiStopped";
        case 53:
          return "noteWifiRunningChanged";
        case 52:
          return "noteWifiRunning";
        case 51:
          return "noteWifiOff";
        case 50:
          return "noteWifiOn";
        case 49:
          return "notePhoneState";
        case 48:
          return "notePhoneDataConnectionState";
        case 47:
          return "notePhoneSignalStrength";
        case 46:
          return "notePhoneOff";
        case 45:
          return "notePhoneOn";
        case 44:
          return "noteMobileRadioPowerState";
        case 43:
          return "noteConnectivityChanged";
        case 42:
          return "noteInteractive";
        case 41:
          return "noteWakeUp";
        case 40:
          return "noteUserActivity";
        case 39:
          return "noteScreenBrightness";
        case 38:
          return "noteScreenState";
        case 37:
          return "noteGpsSignalQuality";
        case 36:
          return "noteGpsChanged";
        case 35:
          return "noteVibratorOff";
        case 34:
          return "noteVibratorOn";
        case 33:
          return "noteLongPartialWakelockFinishFromSource";
        case 32:
          return "noteLongPartialWakelockFinish";
        case 31:
          return "noteLongPartialWakelockStartFromSource";
        case 30:
          return "noteLongPartialWakelockStart";
        case 29:
          return "noteStopWakelockFromSource";
        case 28:
          return "noteChangeWakelockFromSource";
        case 27:
          return "noteStartWakelockFromSource";
        case 26:
          return "noteStopWakelock";
        case 25:
          return "noteStartWakelock";
        case 24:
          return "noteJobFinish";
        case 23:
          return "noteJobStart";
        case 22:
          return "noteSyncFinish";
        case 21:
          return "noteSyncStart";
        case 20:
          return "noteEvent";
        case 19:
          return "computeChargeTimeRemaining";
        case 18:
          return "computeBatteryTimeRemaining";
        case 17:
          return "isCharging";
        case 16:
          return "getStatisticsStream";
        case 15:
          return "getStatistics";
        case 14:
          return "noteResetFlashlight";
        case 13:
          return "noteResetCamera";
        case 12:
          return "noteStopCamera";
        case 11:
          return "noteStartCamera";
        case 10:
          return "noteFlashlightOff";
        case 9:
          return "noteFlashlightOn";
        case 8:
          return "noteResetAudio";
        case 7:
          return "noteResetVideo";
        case 6:
          return "noteStopAudio";
        case 5:
          return "noteStartAudio";
        case 4:
          return "noteStopVideo";
        case 3:
          return "noteStartVideo";
        case 2:
          return "noteStopSensor";
        case 1:
          break;
      } 
      return "noteStartSensor";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool2;
        int j;
        boolean bool1;
        int[] arrayOfInt;
        HealthStatsParceler arrayOfHealthStatsParceler[], healthStatsParceler;
        GpsBatteryStats gpsBatteryStats;
        WifiBatteryStats wifiBatteryStats;
        CellularBatteryStats cellularBatteryStats;
        String str1;
        ParcelFileDescriptor parcelFileDescriptor;
        byte[] arrayOfByte;
        WorkSource workSource;
        String str2;
        long l;
        int k, m, n, i1, i2, i3;
        String str3, str4, str5, str6, str7;
        boolean bool3 = false, bool4 = false, bool5 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 89:
            param1Parcel1.enforceInterface("com.android.internal.app.IBatteryStats");
            param1Int1 = param1Parcel1.readInt();
            bool2 = setChargingStateUpdateDelayMillis(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 88:
            param1Parcel1.enforceInterface("com.android.internal.app.IBatteryStats");
            if (param1Parcel1.readInt() != 0) {
              WifiActivityEnergyInfo wifiActivityEnergyInfo = (WifiActivityEnergyInfo)WifiActivityEnergyInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            noteWifiControllerActivity((WifiActivityEnergyInfo)param1Parcel1);
            return true;
          case 87:
            param1Parcel1.enforceInterface("com.android.internal.app.IBatteryStats");
            if (param1Parcel1.readInt() != 0) {
              ModemActivityInfo modemActivityInfo = (ModemActivityInfo)ModemActivityInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            noteModemControllerActivity((ModemActivityInfo)param1Parcel1);
            return true;
          case 86:
            param1Parcel1.enforceInterface("com.android.internal.app.IBatteryStats");
            if (param1Parcel1.readInt() != 0) {
              BluetoothActivityEnergyInfo bluetoothActivityEnergyInfo = (BluetoothActivityEnergyInfo)BluetoothActivityEnergyInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            noteBluetoothControllerActivity((BluetoothActivityEnergyInfo)param1Parcel1);
            return true;
          case 85:
            param1Parcel1.enforceInterface("com.android.internal.app.IBatteryStats");
            arrayOfInt = param1Parcel1.createIntArray();
            arrayOfHealthStatsParceler = takeUidSnapshots(arrayOfInt);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray((Parcelable[])arrayOfHealthStatsParceler, 1);
            return true;
          case 84:
            arrayOfHealthStatsParceler.enforceInterface("com.android.internal.app.IBatteryStats");
            j = arrayOfHealthStatsParceler.readInt();
            healthStatsParceler = takeUidSnapshot(j);
            param1Parcel2.writeNoException();
            if (healthStatsParceler != null) {
              param1Parcel2.writeInt(1);
              healthStatsParceler.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 83:
            healthStatsParceler.enforceInterface("com.android.internal.app.IBatteryStats");
            gpsBatteryStats = getGpsBatteryStats();
            param1Parcel2.writeNoException();
            if (gpsBatteryStats != null) {
              param1Parcel2.writeInt(1);
              gpsBatteryStats.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 82:
            gpsBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            wifiBatteryStats = getWifiBatteryStats();
            param1Parcel2.writeNoException();
            if (wifiBatteryStats != null) {
              param1Parcel2.writeInt(1);
              wifiBatteryStats.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 81:
            wifiBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            cellularBatteryStats = getCellularBatteryStats();
            param1Parcel2.writeNoException();
            if (cellularBatteryStats != null) {
              param1Parcel2.writeInt(1);
              cellularBatteryStats.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 80:
            cellularBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            if (cellularBatteryStats.readInt() != 0) {
              workSource = (WorkSource)WorkSource.CREATOR.createFromParcel((Parcel)cellularBatteryStats);
            } else {
              workSource = null;
            } 
            j = cellularBatteryStats.readInt();
            noteBleScanResults(workSource, j);
            param1Parcel2.writeNoException();
            return true;
          case 79:
            cellularBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            noteResetBleScan();
            param1Parcel2.writeNoException();
            return true;
          case 78:
            cellularBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            if (cellularBatteryStats.readInt() != 0) {
              workSource = (WorkSource)WorkSource.CREATOR.createFromParcel((Parcel)cellularBatteryStats);
            } else {
              workSource = null;
            } 
            bool4 = bool5;
            if (cellularBatteryStats.readInt() != 0)
              bool4 = true; 
            noteBleScanStopped(workSource, bool4);
            param1Parcel2.writeNoException();
            return true;
          case 77:
            cellularBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            if (cellularBatteryStats.readInt() != 0) {
              workSource = (WorkSource)WorkSource.CREATOR.createFromParcel((Parcel)cellularBatteryStats);
            } else {
              workSource = null;
            } 
            bool4 = bool3;
            if (cellularBatteryStats.readInt() != 0)
              bool4 = true; 
            noteBleScanStarted(workSource, bool4);
            param1Parcel2.writeNoException();
            return true;
          case 76:
            cellularBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            l = getAwakeTimePlugged();
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 75:
            cellularBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            l = getAwakeTimeBattery();
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 74:
            cellularBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            k = cellularBatteryStats.readInt();
            m = cellularBatteryStats.readInt();
            j = cellularBatteryStats.readInt();
            n = cellularBatteryStats.readInt();
            i1 = cellularBatteryStats.readInt();
            i2 = cellularBatteryStats.readInt();
            i3 = cellularBatteryStats.readInt();
            param1Int2 = cellularBatteryStats.readInt();
            l = cellularBatteryStats.readLong();
            setBatteryState(k, m, j, n, i1, i2, i3, param1Int2, l);
            param1Parcel2.writeNoException();
            return true;
          case 73:
            cellularBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            param1Int2 = cellularBatteryStats.readInt();
            str2 = cellularBatteryStats.readString();
            j = cellularBatteryStats.readInt();
            noteDeviceIdleMode(param1Int2, str2, j);
            param1Parcel2.writeNoException();
            return true;
          case 72:
            cellularBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            noteNetworkStatsEnabled();
            param1Parcel2.writeNoException();
            return true;
          case 71:
            cellularBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            str2 = cellularBatteryStats.readString();
            j = cellularBatteryStats.readInt();
            noteNetworkInterfaceType(str2, j);
            param1Parcel2.writeNoException();
            return true;
          case 70:
            cellularBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            j = cellularBatteryStats.readInt();
            l = cellularBatteryStats.readLong();
            param1Int2 = cellularBatteryStats.readInt();
            noteWifiRadioPowerState(j, l, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 69:
            cellularBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            if (cellularBatteryStats.readInt() != 0) {
              WorkSource workSource1 = (WorkSource)WorkSource.CREATOR.createFromParcel((Parcel)cellularBatteryStats);
            } else {
              cellularBatteryStats = null;
            } 
            noteWifiBatchedScanStoppedFromSource((WorkSource)cellularBatteryStats);
            param1Parcel2.writeNoException();
            return true;
          case 68:
            cellularBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            if (cellularBatteryStats.readInt() != 0) {
              WorkSource workSource1 = (WorkSource)WorkSource.CREATOR.createFromParcel((Parcel)cellularBatteryStats);
            } else {
              str2 = null;
            } 
            j = cellularBatteryStats.readInt();
            noteWifiBatchedScanStartedFromSource((WorkSource)str2, j);
            param1Parcel2.writeNoException();
            return true;
          case 67:
            cellularBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            if (cellularBatteryStats.readInt() != 0) {
              WorkSource workSource1 = (WorkSource)WorkSource.CREATOR.createFromParcel((Parcel)cellularBatteryStats);
            } else {
              cellularBatteryStats = null;
            } 
            noteWifiScanStoppedFromSource((WorkSource)cellularBatteryStats);
            param1Parcel2.writeNoException();
            return true;
          case 66:
            cellularBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            if (cellularBatteryStats.readInt() != 0) {
              WorkSource workSource1 = (WorkSource)WorkSource.CREATOR.createFromParcel((Parcel)cellularBatteryStats);
            } else {
              cellularBatteryStats = null;
            } 
            noteWifiScanStartedFromSource((WorkSource)cellularBatteryStats);
            param1Parcel2.writeNoException();
            return true;
          case 65:
            cellularBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            if (cellularBatteryStats.readInt() != 0) {
              WorkSource workSource1 = (WorkSource)WorkSource.CREATOR.createFromParcel((Parcel)cellularBatteryStats);
            } else {
              cellularBatteryStats = null;
            } 
            noteFullWifiLockReleasedFromSource((WorkSource)cellularBatteryStats);
            param1Parcel2.writeNoException();
            return true;
          case 64:
            cellularBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            if (cellularBatteryStats.readInt() != 0) {
              WorkSource workSource1 = (WorkSource)WorkSource.CREATOR.createFromParcel((Parcel)cellularBatteryStats);
            } else {
              cellularBatteryStats = null;
            } 
            noteFullWifiLockAcquiredFromSource((WorkSource)cellularBatteryStats);
            param1Parcel2.writeNoException();
            return true;
          case 63:
            cellularBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            j = cellularBatteryStats.readInt();
            noteWifiMulticastDisabled(j);
            param1Parcel2.writeNoException();
            return true;
          case 62:
            cellularBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            j = cellularBatteryStats.readInt();
            noteWifiMulticastEnabled(j);
            param1Parcel2.writeNoException();
            return true;
          case 61:
            cellularBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            j = cellularBatteryStats.readInt();
            noteWifiScanStopped(j);
            param1Parcel2.writeNoException();
            return true;
          case 60:
            cellularBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            j = cellularBatteryStats.readInt();
            noteWifiScanStarted(j);
            param1Parcel2.writeNoException();
            return true;
          case 59:
            cellularBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            j = cellularBatteryStats.readInt();
            noteFullWifiLockReleased(j);
            param1Parcel2.writeNoException();
            return true;
          case 58:
            cellularBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            j = cellularBatteryStats.readInt();
            noteFullWifiLockAcquired(j);
            param1Parcel2.writeNoException();
            return true;
          case 57:
            cellularBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            j = cellularBatteryStats.readInt();
            noteWifiRssiChanged(j);
            param1Parcel2.writeNoException();
            return true;
          case 56:
            cellularBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            j = cellularBatteryStats.readInt();
            if (cellularBatteryStats.readInt() != 0) {
              bool4 = true;
            } else {
              bool4 = false;
            } 
            noteWifiSupplicantStateChanged(j, bool4);
            param1Parcel2.writeNoException();
            return true;
          case 55:
            cellularBatteryStats.enforceInterface("com.android.internal.app.IBatteryStats");
            j = cellularBatteryStats.readInt();
            str1 = cellularBatteryStats.readString();
            noteWifiState(j, str1);
            param1Parcel2.writeNoException();
            return true;
          case 54:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            if (str1.readInt() != 0) {
              WorkSource workSource1 = (WorkSource)WorkSource.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            noteWifiStopped((WorkSource)str1);
            param1Parcel2.writeNoException();
            return true;
          case 53:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            if (str1.readInt() != 0) {
              WorkSource workSource1 = (WorkSource)WorkSource.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str2 = null;
            } 
            if (str1.readInt() != 0) {
              WorkSource workSource1 = (WorkSource)WorkSource.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            noteWifiRunningChanged((WorkSource)str2, (WorkSource)str1);
            param1Parcel2.writeNoException();
            return true;
          case 52:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            if (str1.readInt() != 0) {
              WorkSource workSource1 = (WorkSource)WorkSource.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            noteWifiRunning((WorkSource)str1);
            param1Parcel2.writeNoException();
            return true;
          case 51:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            noteWifiOff();
            param1Parcel2.writeNoException();
            return true;
          case 50:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            noteWifiOn();
            param1Parcel2.writeNoException();
            return true;
          case 49:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            j = str1.readInt();
            notePhoneState(j);
            param1Parcel2.writeNoException();
            return true;
          case 48:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            j = str1.readInt();
            if (str1.readInt() != 0) {
              bool4 = true;
            } else {
              bool4 = false;
            } 
            param1Int2 = str1.readInt();
            notePhoneDataConnectionState(j, bool4, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 47:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            if (str1.readInt() != 0) {
              SignalStrength signalStrength = (SignalStrength)SignalStrength.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            notePhoneSignalStrength((SignalStrength)str1);
            param1Parcel2.writeNoException();
            return true;
          case 46:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            notePhoneOff();
            param1Parcel2.writeNoException();
            return true;
          case 45:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            notePhoneOn();
            param1Parcel2.writeNoException();
            return true;
          case 44:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            param1Int2 = str1.readInt();
            l = str1.readLong();
            j = str1.readInt();
            noteMobileRadioPowerState(param1Int2, l, j);
            param1Parcel2.writeNoException();
            return true;
          case 43:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            j = str1.readInt();
            str1 = str1.readString();
            noteConnectivityChanged(j, str1);
            param1Parcel2.writeNoException();
            return true;
          case 42:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            if (str1.readInt() != 0) {
              bool4 = true;
            } else {
              bool4 = false;
            } 
            noteInteractive(bool4);
            param1Parcel2.writeNoException();
            return true;
          case 41:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            str2 = str1.readString();
            j = str1.readInt();
            noteWakeUp(str2, j);
            param1Parcel2.writeNoException();
            return true;
          case 40:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            param1Int2 = str1.readInt();
            j = str1.readInt();
            noteUserActivity(param1Int2, j);
            param1Parcel2.writeNoException();
            return true;
          case 39:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            j = str1.readInt();
            noteScreenBrightness(j);
            param1Parcel2.writeNoException();
            return true;
          case 38:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            j = str1.readInt();
            noteScreenState(j);
            param1Parcel2.writeNoException();
            return true;
          case 37:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            j = str1.readInt();
            noteGpsSignalQuality(j);
            param1Parcel2.writeNoException();
            return true;
          case 36:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            if (str1.readInt() != 0) {
              WorkSource workSource1 = (WorkSource)WorkSource.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str2 = null;
            } 
            if (str1.readInt() != 0) {
              WorkSource workSource1 = (WorkSource)WorkSource.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            noteGpsChanged((WorkSource)str2, (WorkSource)str1);
            param1Parcel2.writeNoException();
            return true;
          case 35:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            j = str1.readInt();
            noteVibratorOff(j);
            param1Parcel2.writeNoException();
            return true;
          case 34:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            j = str1.readInt();
            l = str1.readLong();
            noteVibratorOn(j, l);
            param1Parcel2.writeNoException();
            return true;
          case 33:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            str3 = str1.readString();
            str2 = str1.readString();
            if (str1.readInt() != 0) {
              WorkSource workSource1 = (WorkSource)WorkSource.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            noteLongPartialWakelockFinishFromSource(str3, str2, (WorkSource)str1);
            param1Parcel2.writeNoException();
            return true;
          case 32:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            str2 = str1.readString();
            str3 = str1.readString();
            j = str1.readInt();
            noteLongPartialWakelockFinish(str2, str3, j);
            param1Parcel2.writeNoException();
            return true;
          case 31:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            str3 = str1.readString();
            str2 = str1.readString();
            if (str1.readInt() != 0) {
              WorkSource workSource1 = (WorkSource)WorkSource.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            noteLongPartialWakelockStartFromSource(str3, str2, (WorkSource)str1);
            param1Parcel2.writeNoException();
            return true;
          case 30:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            str3 = str1.readString();
            str2 = str1.readString();
            j = str1.readInt();
            noteLongPartialWakelockStart(str3, str2, j);
            param1Parcel2.writeNoException();
            return true;
          case 29:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            if (str1.readInt() != 0) {
              WorkSource workSource1 = (WorkSource)WorkSource.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str2 = null;
            } 
            param1Int2 = str1.readInt();
            str3 = str1.readString();
            str4 = str1.readString();
            j = str1.readInt();
            noteStopWakelockFromSource((WorkSource)str2, param1Int2, str3, str4, j);
            param1Parcel2.writeNoException();
            return true;
          case 28:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            if (str1.readInt() != 0) {
              WorkSource workSource1 = (WorkSource)WorkSource.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str2 = null;
            } 
            j = str1.readInt();
            str4 = str1.readString();
            str5 = str1.readString();
            i1 = str1.readInt();
            if (str1.readInt() != 0) {
              WorkSource workSource1 = (WorkSource)WorkSource.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str3 = null;
            } 
            i2 = str1.readInt();
            str6 = str1.readString();
            str7 = str1.readString();
            param1Int2 = str1.readInt();
            if (str1.readInt() != 0)
              bool4 = true; 
            noteChangeWakelockFromSource((WorkSource)str2, j, str4, str5, i1, (WorkSource)str3, i2, str6, str7, param1Int2, bool4);
            param1Parcel2.writeNoException();
            return true;
          case 27:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            if (str1.readInt() != 0) {
              WorkSource workSource1 = (WorkSource)WorkSource.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str2 = null;
            } 
            j = str1.readInt();
            str3 = str1.readString();
            str4 = str1.readString();
            param1Int2 = str1.readInt();
            if (str1.readInt() != 0) {
              bool4 = true;
            } else {
              bool4 = false;
            } 
            noteStartWakelockFromSource((WorkSource)str2, j, str3, str4, param1Int2, bool4);
            param1Parcel2.writeNoException();
            return true;
          case 26:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            i2 = str1.readInt();
            j = str1.readInt();
            str2 = str1.readString();
            str3 = str1.readString();
            param1Int2 = str1.readInt();
            noteStopWakelock(i2, j, str2, str3, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 25:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            param1Int2 = str1.readInt();
            j = str1.readInt();
            str3 = str1.readString();
            str2 = str1.readString();
            i2 = str1.readInt();
            if (str1.readInt() != 0) {
              bool4 = true;
            } else {
              bool4 = false;
            } 
            noteStartWakelock(param1Int2, j, str3, str2, i2, bool4);
            param1Parcel2.writeNoException();
            return true;
          case 24:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            str2 = str1.readString();
            j = str1.readInt();
            param1Int2 = str1.readInt();
            noteJobFinish(str2, j, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            str2 = str1.readString();
            j = str1.readInt();
            noteJobStart(str2, j);
            param1Parcel2.writeNoException();
            return true;
          case 22:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            str2 = str1.readString();
            j = str1.readInt();
            noteSyncFinish(str2, j);
            param1Parcel2.writeNoException();
            return true;
          case 21:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            str2 = str1.readString();
            j = str1.readInt();
            noteSyncStart(str2, j);
            param1Parcel2.writeNoException();
            return true;
          case 20:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            j = str1.readInt();
            str2 = str1.readString();
            param1Int2 = str1.readInt();
            noteEvent(j, str2, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            l = computeChargeTimeRemaining();
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 18:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            l = computeBatteryTimeRemaining();
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 17:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            bool1 = isCharging();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 16:
            str1.enforceInterface("com.android.internal.app.IBatteryStats");
            parcelFileDescriptor = getStatisticsStream();
            param1Parcel2.writeNoException();
            if (parcelFileDescriptor != null) {
              param1Parcel2.writeInt(1);
              parcelFileDescriptor.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 15:
            parcelFileDescriptor.enforceInterface("com.android.internal.app.IBatteryStats");
            arrayOfByte = getStatistics();
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte);
            return true;
          case 14:
            arrayOfByte.enforceInterface("com.android.internal.app.IBatteryStats");
            noteResetFlashlight();
            param1Parcel2.writeNoException();
            return true;
          case 13:
            arrayOfByte.enforceInterface("com.android.internal.app.IBatteryStats");
            noteResetCamera();
            param1Parcel2.writeNoException();
            return true;
          case 12:
            arrayOfByte.enforceInterface("com.android.internal.app.IBatteryStats");
            i = arrayOfByte.readInt();
            noteStopCamera(i);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            arrayOfByte.enforceInterface("com.android.internal.app.IBatteryStats");
            i = arrayOfByte.readInt();
            noteStartCamera(i);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            arrayOfByte.enforceInterface("com.android.internal.app.IBatteryStats");
            i = arrayOfByte.readInt();
            noteFlashlightOff(i);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            arrayOfByte.enforceInterface("com.android.internal.app.IBatteryStats");
            i = arrayOfByte.readInt();
            noteFlashlightOn(i);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            arrayOfByte.enforceInterface("com.android.internal.app.IBatteryStats");
            noteResetAudio();
            param1Parcel2.writeNoException();
            return true;
          case 7:
            arrayOfByte.enforceInterface("com.android.internal.app.IBatteryStats");
            noteResetVideo();
            param1Parcel2.writeNoException();
            return true;
          case 6:
            arrayOfByte.enforceInterface("com.android.internal.app.IBatteryStats");
            i = arrayOfByte.readInt();
            noteStopAudio(i);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            arrayOfByte.enforceInterface("com.android.internal.app.IBatteryStats");
            i = arrayOfByte.readInt();
            noteStartAudio(i);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            arrayOfByte.enforceInterface("com.android.internal.app.IBatteryStats");
            i = arrayOfByte.readInt();
            noteStopVideo(i);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            arrayOfByte.enforceInterface("com.android.internal.app.IBatteryStats");
            i = arrayOfByte.readInt();
            noteStartVideo(i);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            arrayOfByte.enforceInterface("com.android.internal.app.IBatteryStats");
            param1Int2 = arrayOfByte.readInt();
            i = arrayOfByte.readInt();
            noteStopSensor(param1Int2, i);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        arrayOfByte.enforceInterface("com.android.internal.app.IBatteryStats");
        int i = arrayOfByte.readInt();
        param1Int2 = arrayOfByte.readInt();
        noteStartSensor(i, param1Int2);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("com.android.internal.app.IBatteryStats");
      return true;
    }
    
    private static class Proxy implements IBatteryStats {
      public static IBatteryStats sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.app.IBatteryStats";
      }
      
      public void noteStartSensor(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteStartSensor(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteStopSensor(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteStopSensor(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteStartVideo(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteStartVideo(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteStopVideo(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteStopVideo(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteStartAudio(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteStartAudio(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteStopAudio(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteStopAudio(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteResetVideo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteResetVideo();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteResetAudio() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteResetAudio();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteFlashlightOn(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteFlashlightOn(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteFlashlightOff(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteFlashlightOff(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteStartCamera(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteStartCamera(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteStopCamera(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteStopCamera(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteResetCamera() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteResetCamera();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteResetFlashlight() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteResetFlashlight();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getStatistics() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null)
            return IBatteryStats.Stub.getDefaultImpl().getStatistics(); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParcelFileDescriptor getStatisticsStream() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ParcelFileDescriptor parcelFileDescriptor;
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            parcelFileDescriptor = IBatteryStats.Stub.getDefaultImpl().getStatisticsStream();
            return parcelFileDescriptor;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(parcel2);
          } else {
            parcelFileDescriptor = null;
          } 
          return parcelFileDescriptor;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isCharging() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(17, parcel1, parcel2, 0);
          if (!bool2 && IBatteryStats.Stub.getDefaultImpl() != null) {
            bool1 = IBatteryStats.Stub.getDefaultImpl().isCharging();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long computeBatteryTimeRemaining() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null)
            return IBatteryStats.Stub.getDefaultImpl().computeBatteryTimeRemaining(); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long computeChargeTimeRemaining() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null)
            return IBatteryStats.Stub.getDefaultImpl().computeChargeTimeRemaining(); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteEvent(int param2Int1, String param2String, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteEvent(param2Int1, param2String, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteSyncStart(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteSyncStart(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteSyncFinish(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteSyncFinish(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteJobStart(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteJobStart(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteJobFinish(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteJobFinish(param2String, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteStartWakelock(int param2Int1, int param2Int2, String param2String1, String param2String2, int param2Int3, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeInt(param2Int2);
              try {
                parcel1.writeString(param2String1);
                try {
                  parcel1.writeString(param2String2);
                  try {
                    boolean bool;
                    parcel1.writeInt(param2Int3);
                    if (param2Boolean) {
                      bool = true;
                    } else {
                      bool = false;
                    } 
                    parcel1.writeInt(bool);
                    try {
                      boolean bool1 = this.mRemote.transact(25, parcel1, parcel2, 0);
                      if (!bool1 && IBatteryStats.Stub.getDefaultImpl() != null) {
                        IBatteryStats.Stub.getDefaultImpl().noteStartWakelock(param2Int1, param2Int2, param2String1, param2String2, param2Int3, param2Boolean);
                        parcel2.recycle();
                        parcel1.recycle();
                        return;
                      } 
                      parcel2.readException();
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void noteStopWakelock(int param2Int1, int param2Int2, String param2String1, String param2String2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteStopWakelock(param2Int1, param2Int2, param2String1, param2String2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteStartWakelockFromSource(WorkSource param2WorkSource, int param2Int1, String param2String1, String param2String2, int param2Int2, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          boolean bool = true;
          if (param2WorkSource != null) {
            parcel1.writeInt(1);
            param2WorkSource.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeString(param2String1);
              try {
                parcel1.writeString(param2String2);
                try {
                  parcel1.writeInt(param2Int2);
                  if (!param2Boolean)
                    bool = false; 
                  parcel1.writeInt(bool);
                  try {
                    boolean bool1 = this.mRemote.transact(27, parcel1, parcel2, 0);
                    if (!bool1 && IBatteryStats.Stub.getDefaultImpl() != null) {
                      IBatteryStats.Stub.getDefaultImpl().noteStartWakelockFromSource(param2WorkSource, param2Int1, param2String1, param2String2, param2Int2, param2Boolean);
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } 
                    parcel2.readException();
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2WorkSource;
      }
      
      public void noteChangeWakelockFromSource(WorkSource param2WorkSource1, int param2Int1, String param2String1, String param2String2, int param2Int2, WorkSource param2WorkSource2, int param2Int3, String param2String3, String param2String4, int param2Int4, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          boolean bool = true;
          if (param2WorkSource1 != null) {
            try {
              parcel1.writeInt(1);
              param2WorkSource1.writeToParcel(parcel1, 0);
            } finally {}
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int2);
          if (param2WorkSource2 != null) {
            parcel1.writeInt(1);
            param2WorkSource2.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int3);
          parcel1.writeString(param2String3);
          parcel1.writeString(param2String4);
          parcel1.writeInt(param2Int4);
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool1 && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats iBatteryStats = IBatteryStats.Stub.getDefaultImpl();
            try {
              iBatteryStats.noteChangeWakelockFromSource(param2WorkSource1, param2Int1, param2String1, param2String2, param2Int2, param2WorkSource2, param2Int3, param2String3, param2String4, param2Int4, param2Boolean);
              parcel2.recycle();
              parcel1.recycle();
              return;
            } finally {}
          } else {
            parcel2.readException();
            parcel2.recycle();
            parcel1.recycle();
            return;
          } 
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2WorkSource1;
      }
      
      public void noteStopWakelockFromSource(WorkSource param2WorkSource, int param2Int1, String param2String1, String param2String2, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          if (param2WorkSource != null) {
            parcel1.writeInt(1);
            param2WorkSource.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteStopWakelockFromSource(param2WorkSource, param2Int1, param2String1, param2String2, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteLongPartialWakelockStart(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteLongPartialWakelockStart(param2String1, param2String2, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteLongPartialWakelockStartFromSource(String param2String1, String param2String2, WorkSource param2WorkSource) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2WorkSource != null) {
            parcel1.writeInt(1);
            param2WorkSource.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteLongPartialWakelockStartFromSource(param2String1, param2String2, param2WorkSource);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteLongPartialWakelockFinish(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteLongPartialWakelockFinish(param2String1, param2String2, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteLongPartialWakelockFinishFromSource(String param2String1, String param2String2, WorkSource param2WorkSource) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2WorkSource != null) {
            parcel1.writeInt(1);
            param2WorkSource.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteLongPartialWakelockFinishFromSource(param2String1, param2String2, param2WorkSource);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteVibratorOn(int param2Int, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int);
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteVibratorOn(param2Int, param2Long);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteVibratorOff(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteVibratorOff(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteGpsChanged(WorkSource param2WorkSource1, WorkSource param2WorkSource2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          if (param2WorkSource1 != null) {
            parcel1.writeInt(1);
            param2WorkSource1.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2WorkSource2 != null) {
            parcel1.writeInt(1);
            param2WorkSource2.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteGpsChanged(param2WorkSource1, param2WorkSource2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteGpsSignalQuality(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteGpsSignalQuality(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteScreenState(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(38, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteScreenState(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteScreenBrightness(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteScreenBrightness(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteUserActivity(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(40, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteUserActivity(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteWakeUp(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(41, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteWakeUp(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteInteractive(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(42, parcel1, parcel2, 0);
          if (!bool1 && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteInteractive(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteConnectivityChanged(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(43, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteConnectivityChanged(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteMobileRadioPowerState(int param2Int1, long param2Long, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int1);
          parcel1.writeLong(param2Long);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(44, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteMobileRadioPowerState(param2Int1, param2Long, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notePhoneOn() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          boolean bool = this.mRemote.transact(45, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().notePhoneOn();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notePhoneOff() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          boolean bool = this.mRemote.transact(46, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().notePhoneOff();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notePhoneSignalStrength(SignalStrength param2SignalStrength) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          if (param2SignalStrength != null) {
            parcel1.writeInt(1);
            param2SignalStrength.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(47, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().notePhoneSignalStrength(param2SignalStrength);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notePhoneDataConnectionState(int param2Int1, boolean param2Boolean, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int1);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int2);
          boolean bool1 = this.mRemote.transact(48, parcel1, parcel2, 0);
          if (!bool1 && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().notePhoneDataConnectionState(param2Int1, param2Boolean, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notePhoneState(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(49, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().notePhoneState(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteWifiOn() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          boolean bool = this.mRemote.transact(50, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteWifiOn();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteWifiOff() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          boolean bool = this.mRemote.transact(51, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteWifiOff();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteWifiRunning(WorkSource param2WorkSource) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          if (param2WorkSource != null) {
            parcel1.writeInt(1);
            param2WorkSource.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(52, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteWifiRunning(param2WorkSource);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteWifiRunningChanged(WorkSource param2WorkSource1, WorkSource param2WorkSource2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          if (param2WorkSource1 != null) {
            parcel1.writeInt(1);
            param2WorkSource1.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2WorkSource2 != null) {
            parcel1.writeInt(1);
            param2WorkSource2.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(53, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteWifiRunningChanged(param2WorkSource1, param2WorkSource2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteWifiStopped(WorkSource param2WorkSource) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          if (param2WorkSource != null) {
            parcel1.writeInt(1);
            param2WorkSource.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(54, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteWifiStopped(param2WorkSource);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteWifiState(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(55, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteWifiState(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteWifiSupplicantStateChanged(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(56, parcel1, parcel2, 0);
          if (!bool1 && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteWifiSupplicantStateChanged(param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteWifiRssiChanged(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(57, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteWifiRssiChanged(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteFullWifiLockAcquired(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(58, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteFullWifiLockAcquired(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteFullWifiLockReleased(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(59, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteFullWifiLockReleased(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteWifiScanStarted(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(60, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteWifiScanStarted(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteWifiScanStopped(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(61, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteWifiScanStopped(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteWifiMulticastEnabled(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(62, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteWifiMulticastEnabled(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteWifiMulticastDisabled(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(63, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteWifiMulticastDisabled(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteFullWifiLockAcquiredFromSource(WorkSource param2WorkSource) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          if (param2WorkSource != null) {
            parcel1.writeInt(1);
            param2WorkSource.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(64, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteFullWifiLockAcquiredFromSource(param2WorkSource);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteFullWifiLockReleasedFromSource(WorkSource param2WorkSource) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          if (param2WorkSource != null) {
            parcel1.writeInt(1);
            param2WorkSource.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(65, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteFullWifiLockReleasedFromSource(param2WorkSource);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteWifiScanStartedFromSource(WorkSource param2WorkSource) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          if (param2WorkSource != null) {
            parcel1.writeInt(1);
            param2WorkSource.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(66, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteWifiScanStartedFromSource(param2WorkSource);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteWifiScanStoppedFromSource(WorkSource param2WorkSource) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          if (param2WorkSource != null) {
            parcel1.writeInt(1);
            param2WorkSource.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(67, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteWifiScanStoppedFromSource(param2WorkSource);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteWifiBatchedScanStartedFromSource(WorkSource param2WorkSource, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          if (param2WorkSource != null) {
            parcel1.writeInt(1);
            param2WorkSource.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(68, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteWifiBatchedScanStartedFromSource(param2WorkSource, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteWifiBatchedScanStoppedFromSource(WorkSource param2WorkSource) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          if (param2WorkSource != null) {
            parcel1.writeInt(1);
            param2WorkSource.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(69, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteWifiBatchedScanStoppedFromSource(param2WorkSource);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteWifiRadioPowerState(int param2Int1, long param2Long, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int1);
          parcel1.writeLong(param2Long);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(70, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteWifiRadioPowerState(param2Int1, param2Long, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteNetworkInterfaceType(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(71, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteNetworkInterfaceType(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteNetworkStatsEnabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          boolean bool = this.mRemote.transact(72, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteNetworkStatsEnabled();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteDeviceIdleMode(int param2Int1, String param2String, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(73, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteDeviceIdleMode(param2Int1, param2String, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setBatteryState(int param2Int1, int param2Int2, int param2Int3, int param2Int4, int param2Int5, int param2Int6, int param2Int7, int param2Int8, long param2Long) throws RemoteException {
        Exception exception;
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeInt(param2Int2);
              parcel1.writeInt(param2Int3);
              parcel1.writeInt(param2Int4);
              parcel1.writeInt(param2Int5);
              parcel1.writeInt(param2Int6);
              parcel1.writeInt(param2Int7);
              parcel1.writeInt(param2Int8);
              parcel1.writeLong(param2Long);
              boolean bool = this.mRemote.transact(74, parcel1, parcel2, 0);
              if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
                IBatteryStats.Stub.getDefaultImpl().setBatteryState(param2Int1, param2Int2, param2Int3, param2Int4, param2Int5, param2Int6, param2Int7, param2Int8, param2Long);
                parcel2.recycle();
                parcel1.recycle();
                return;
              } 
              parcel2.readException();
              parcel2.recycle();
              parcel1.recycle();
              return;
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw exception;
      }
      
      public long getAwakeTimeBattery() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          boolean bool = this.mRemote.transact(75, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null)
            return IBatteryStats.Stub.getDefaultImpl().getAwakeTimeBattery(); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getAwakeTimePlugged() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          boolean bool = this.mRemote.transact(76, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null)
            return IBatteryStats.Stub.getDefaultImpl().getAwakeTimePlugged(); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteBleScanStarted(WorkSource param2WorkSource, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          boolean bool = true;
          if (param2WorkSource != null) {
            parcel1.writeInt(1);
            param2WorkSource.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(77, parcel1, parcel2, 0);
          if (!bool1 && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteBleScanStarted(param2WorkSource, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteBleScanStopped(WorkSource param2WorkSource, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          boolean bool = true;
          if (param2WorkSource != null) {
            parcel1.writeInt(1);
            param2WorkSource.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(78, parcel1, parcel2, 0);
          if (!bool1 && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteBleScanStopped(param2WorkSource, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteResetBleScan() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          boolean bool = this.mRemote.transact(79, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteResetBleScan();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteBleScanResults(WorkSource param2WorkSource, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          if (param2WorkSource != null) {
            parcel1.writeInt(1);
            param2WorkSource.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(80, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteBleScanResults(param2WorkSource, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public CellularBatteryStats getCellularBatteryStats() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          CellularBatteryStats cellularBatteryStats;
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          boolean bool = this.mRemote.transact(81, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            cellularBatteryStats = IBatteryStats.Stub.getDefaultImpl().getCellularBatteryStats();
            return cellularBatteryStats;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            cellularBatteryStats = (CellularBatteryStats)CellularBatteryStats.CREATOR.createFromParcel(parcel2);
          } else {
            cellularBatteryStats = null;
          } 
          return cellularBatteryStats;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public WifiBatteryStats getWifiBatteryStats() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          WifiBatteryStats wifiBatteryStats;
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          boolean bool = this.mRemote.transact(82, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            wifiBatteryStats = IBatteryStats.Stub.getDefaultImpl().getWifiBatteryStats();
            return wifiBatteryStats;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            wifiBatteryStats = (WifiBatteryStats)WifiBatteryStats.CREATOR.createFromParcel(parcel2);
          } else {
            wifiBatteryStats = null;
          } 
          return wifiBatteryStats;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public GpsBatteryStats getGpsBatteryStats() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          GpsBatteryStats gpsBatteryStats;
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          boolean bool = this.mRemote.transact(83, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            gpsBatteryStats = IBatteryStats.Stub.getDefaultImpl().getGpsBatteryStats();
            return gpsBatteryStats;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            gpsBatteryStats = (GpsBatteryStats)GpsBatteryStats.CREATOR.createFromParcel(parcel2);
          } else {
            gpsBatteryStats = null;
          } 
          return gpsBatteryStats;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public HealthStatsParceler takeUidSnapshot(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          HealthStatsParceler healthStatsParceler;
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(84, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            healthStatsParceler = IBatteryStats.Stub.getDefaultImpl().takeUidSnapshot(param2Int);
            return healthStatsParceler;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            healthStatsParceler = (HealthStatsParceler)HealthStatsParceler.CREATOR.createFromParcel(parcel2);
          } else {
            healthStatsParceler = null;
          } 
          return healthStatsParceler;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public HealthStatsParceler[] takeUidSnapshots(int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(85, parcel1, parcel2, 0);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null)
            return IBatteryStats.Stub.getDefaultImpl().takeUidSnapshots(param2ArrayOfint); 
          parcel2.readException();
          return (HealthStatsParceler[])parcel2.createTypedArray(HealthStatsParceler.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteBluetoothControllerActivity(BluetoothActivityEnergyInfo param2BluetoothActivityEnergyInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          if (param2BluetoothActivityEnergyInfo != null) {
            parcel.writeInt(1);
            param2BluetoothActivityEnergyInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(86, parcel, null, 1);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteBluetoothControllerActivity(param2BluetoothActivityEnergyInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void noteModemControllerActivity(ModemActivityInfo param2ModemActivityInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          if (param2ModemActivityInfo != null) {
            parcel.writeInt(1);
            param2ModemActivityInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(87, parcel, null, 1);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteModemControllerActivity(param2ModemActivityInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void noteWifiControllerActivity(WifiActivityEnergyInfo param2WifiActivityEnergyInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          if (param2WifiActivityEnergyInfo != null) {
            parcel.writeInt(1);
            param2WifiActivityEnergyInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(88, parcel, null, 1);
          if (!bool && IBatteryStats.Stub.getDefaultImpl() != null) {
            IBatteryStats.Stub.getDefaultImpl().noteWifiControllerActivity(param2WifiActivityEnergyInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public boolean setChargingStateUpdateDelayMillis(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IBatteryStats");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(89, parcel1, parcel2, 0);
          if (!bool2 && IBatteryStats.Stub.getDefaultImpl() != null) {
            bool1 = IBatteryStats.Stub.getDefaultImpl().setChargingStateUpdateDelayMillis(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IBatteryStats param1IBatteryStats) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IBatteryStats != null) {
          Proxy.sDefaultImpl = param1IBatteryStats;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IBatteryStats getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
