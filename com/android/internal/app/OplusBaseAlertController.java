package com.android.internal.app;

import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.oplus.util.OplusContextUtil;

public abstract class OplusBaseAlertController {
  boolean mMessageNeedScroll = false;
  
  private int mDialogType = 0;
  
  public boolean isMessageNeedScroll() {
    return this.mMessageNeedScroll;
  }
  
  public void setMessageNeedScroll(boolean paramBoolean) {
    this.mMessageNeedScroll = paramBoolean;
  }
  
  public int getDialogType() {
    return this.mDialogType;
  }
  
  public void setDialogType(int paramInt) {
    this.mDialogType = paramInt;
  }
  
  public void setButtonIsBold(int paramInt1, int paramInt2, int paramInt3) {}
  
  public void setDeleteDialogOption(int paramInt) {}
  
  abstract Button getAlertControllerButtonNegative();
  
  abstract CharSequence getAlertControllerButtonNegativeText();
  
  abstract Button getAlertControllerButtonNeutral();
  
  abstract CharSequence getAlertControllerButtonNeutralText();
  
  abstract Button getAlertControllerButtonPositive();
  
  abstract CharSequence getAlertControllerButtonPositiveText();
  
  abstract CharSequence getAlertControllerTitle();
  
  protected abstract int selectContentViewWrapper();
  
  abstract void setupViewWrapper();
  
  public static abstract class BaseAlertParams {
    public int mDialogType;
    
    public boolean mMessageNeedScroll;
    
    public CharSequence[] mSummaries;
    
    public boolean needHookAdapter(boolean param1Boolean1, boolean param1Boolean2, CharSequence[] param1ArrayOfCharSequence) {
      if (!param1Boolean1 && !param1Boolean2 && OplusContextUtil.isOplusStyle(getContext())) {
        param1Boolean1 = true;
      } else {
        param1Boolean1 = false;
      } 
      return param1Boolean1;
    }
    
    public ListAdapter getHookAdapter(Context param1Context, CharSequence param1CharSequence1, CharSequence param1CharSequence2, CharSequence[] param1ArrayOfCharSequence1, CharSequence[] param1ArrayOfCharSequence2) {
      boolean bool1 = TextUtils.isEmpty(param1CharSequence1);
      boolean bool2 = TextUtils.isEmpty(param1CharSequence2);
      return new OplusBaseAlertController.SummaryAdapter(param1Context, bool1 ^ true, bool2 ^ true, param1ArrayOfCharSequence1, param1ArrayOfCharSequence2);
    }
    
    public void hookApply(OplusBaseAlertController param1OplusBaseAlertController) {
      if (OplusContextUtil.isOplusStyle(getContext())) {
        param1OplusBaseAlertController.mMessageNeedScroll = this.mMessageNeedScroll;
        param1OplusBaseAlertController.setDialogType(this.mDialogType);
      } 
    }
    
    public abstract Context getContext();
    
    public abstract void setItems(CharSequence[] param1ArrayOfCharSequence);
    
    public abstract void setOnClickListener(DialogInterface.OnClickListener param1OnClickListener);
  }
  
  class SummaryAdapter extends BaseAdapter {
    private static final int LAYOUT = 202440717;
    
    private Context mContext;
    
    private boolean mHasMessage;
    
    private boolean mHasTitle;
    
    private CharSequence[] mItems;
    
    private CharSequence[] mSummaries;
    
    public SummaryAdapter(OplusBaseAlertController this$0, boolean param1Boolean1, boolean param1Boolean2, CharSequence[] param1ArrayOfCharSequence1, CharSequence[] param1ArrayOfCharSequence2) {
      this.mHasTitle = param1Boolean1;
      this.mHasMessage = param1Boolean2;
      this.mContext = (Context)this$0;
      this.mItems = param1ArrayOfCharSequence1;
      this.mSummaries = param1ArrayOfCharSequence2;
    }
    
    public View getView(int param1Int, View param1View, ViewGroup param1ViewGroup) {
      View view = LayoutInflater.from(this.mContext).inflate(202440717, param1ViewGroup, false);
      TextView textView1 = view.<TextView>findViewById(16908308);
      TextView textView2 = view.<TextView>findViewById(201457869);
      CharSequence charSequence2 = getItem(param1Int);
      CharSequence charSequence1 = getSummary(param1Int);
      textView1.setText(charSequence2);
      if (TextUtils.isEmpty(charSequence1)) {
        textView2.setVisibility(8);
      } else {
        textView2.setVisibility(0);
        textView2.setText(charSequence1);
      } 
      resetPadding(param1Int, view);
      return view;
    }
    
    private void resetPadding(int param1Int, View param1View) {
      int i = this.mContext.getResources().getDimensionPixelSize(201654372);
      int j = param1View.getPaddingTop();
      int k = param1View.getPaddingLeft();
      int m = param1View.getPaddingBottom();
      int n = param1View.getPaddingRight();
      if (getCount() > 1)
        if (param1Int == getCount() - 1) {
          param1View.setPadding(k, j, n, m + i);
          param1View.setMinimumHeight(param1View.getMinimumHeight() + i);
        } else if (!this.mHasTitle && !this.mHasMessage) {
          if (param1Int == 0) {
            param1View.setPadding(k, j + i, n, m);
            param1View.setMinimumHeight(param1View.getMinimumHeight() + i);
          } else {
            param1View.setPadding(k, j, n, m);
          } 
        }  
    }
    
    public int getCount() {
      int i;
      CharSequence[] arrayOfCharSequence = this.mItems;
      if (arrayOfCharSequence == null) {
        i = 0;
      } else {
        i = arrayOfCharSequence.length;
      } 
      return i;
    }
    
    public CharSequence getItem(int param1Int) {
      CharSequence charSequence, arrayOfCharSequence[] = this.mItems;
      if (arrayOfCharSequence == null) {
        arrayOfCharSequence = null;
      } else {
        charSequence = arrayOfCharSequence[param1Int];
      } 
      return charSequence;
    }
    
    public CharSequence getSummary(int param1Int) {
      CharSequence[] arrayOfCharSequence = this.mSummaries;
      if (arrayOfCharSequence == null)
        return null; 
      if (param1Int >= arrayOfCharSequence.length)
        return null; 
      return arrayOfCharSequence[param1Int];
    }
    
    public long getItemId(int param1Int) {
      return param1Int;
    }
    
    public boolean hasStableIds() {
      return true;
    }
  }
}
