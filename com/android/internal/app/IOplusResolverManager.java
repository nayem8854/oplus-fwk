package com.android.internal.app;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Paint;
import android.net.Uri;
import android.os.RemoteException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public interface IOplusResolverManager extends IOplusCommonFeature {
  public static final IOplusResolverManager DEFAULT = (IOplusResolverManager)new Object();
  
  default IOplusResolverManager getDefault() {
    return DEFAULT;
  }
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusResolverManager;
  }
  
  default boolean isLoadTheme() {
    return true;
  }
  
  default boolean isOriginUi() {
    return true;
  }
  
  default void onCreate(OplusBaseResolverActivity paramOplusBaseResolverActivity) {}
  
  default void onResume() {}
  
  default void onPause() {}
  
  default void onDestroy() {}
  
  default void initActionSend() {}
  
  default boolean addExtraOneAppFinish() {
    return true;
  }
  
  default void setLastChosen(ResolverListController paramResolverListController, Intent paramIntent, IntentFilter paramIntentFilter, int paramInt) throws RemoteException {
    paramResolverListController.setLastChosen(paramIntent, paramIntentFilter, paramInt);
  }
  
  default void initView(boolean paramBoolean1, boolean paramBoolean2) {}
  
  default void updateView(boolean paramBoolean1, boolean paramBoolean2) {}
  
  default void onMultiWindowModeChanged() {}
  
  default void setResolverContent() {}
  
  default void initPreferenceAndPinList() {}
  
  default void statisticsData(ResolveInfo paramResolveInfo, int paramInt) {}
  
  default ResolveInfo getResolveInfo(Intent paramIntent, PackageManager paramPackageManager) {
    ActivityInfo activityInfo = paramIntent.resolveActivityInfo(paramPackageManager, 0);
    if (activityInfo == null)
      return null; 
    ResolveInfo resolveInfo = new ResolveInfo();
    resolveInfo.activityInfo = activityInfo;
    return resolveInfo;
  }
  
  default int getChooserPreFileSingleIconView(boolean paramBoolean, String paramString, Uri paramUri) {
    int i;
    if (paramBoolean) {
      i = 17302117;
    } else {
      i = 17302431;
    } 
    return i;
  }
  
  default ViewGroup getDisplayImageContentPreview(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup) {
    return (ViewGroup)paramLayoutInflater.inflate(17367130, paramViewGroup, false);
  }
  
  default ViewGroup getDisplayTextContentPreview(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup) {
    return (ViewGroup)paramLayoutInflater.inflate(17367131, paramViewGroup, false);
  }
  
  default ViewGroup getDisplayFileContentPreview(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup) {
    return (ViewGroup)paramLayoutInflater.inflate(17367129, paramViewGroup, false);
  }
  
  default void clearInactiveProfileCache(int paramInt) {}
  
  default void showActiveEmptyViewState() {}
  
  default void showWorkProfileOffEmptyState(AbstractMultiProfilePagerAdapter paramAbstractMultiProfilePagerAdapter, ResolverListAdapter paramResolverListAdapter, View.OnClickListener paramOnClickListener) {
    paramAbstractMultiProfilePagerAdapter.showWorkProfileOffEmptyState(paramResolverListAdapter, paramOnClickListener);
  }
  
  default void showNoPersonalToWorkIntentsEmptyState(AbstractMultiProfilePagerAdapter paramAbstractMultiProfilePagerAdapter, ResolverListAdapter paramResolverListAdapter) {
    paramAbstractMultiProfilePagerAdapter.showNoPersonalToWorkIntentsEmptyState(paramResolverListAdapter);
  }
  
  default void showNoPersonalAppsAvailableEmptyState(AbstractMultiProfilePagerAdapter paramAbstractMultiProfilePagerAdapter, ResolverListAdapter paramResolverListAdapter) {
    paramAbstractMultiProfilePagerAdapter.showNoPersonalAppsAvailableEmptyState(paramResolverListAdapter);
  }
  
  default void showNoWorkAppsAvailableEmptyState(AbstractMultiProfilePagerAdapter paramAbstractMultiProfilePagerAdapter, ResolverListAdapter paramResolverListAdapter) {
    paramAbstractMultiProfilePagerAdapter.showNoWorkAppsAvailableEmptyState(paramResolverListAdapter);
  }
  
  default void showNoWorkToPersonalIntentsEmptyState(AbstractMultiProfilePagerAdapter paramAbstractMultiProfilePagerAdapter, ResolverListAdapter paramResolverListAdapter) {
    paramAbstractMultiProfilePagerAdapter.showNoWorkToPersonalIntentsEmptyState(paramResolverListAdapter);
  }
  
  default void restoreProfilePager(int paramInt) {}
  
  default boolean tryApkResourceName(Uri paramUri, ImageView paramImageView, TextView paramTextView) {
    return false;
  }
  
  default ViewGroup getChooserProfileDescriptor(LayoutInflater paramLayoutInflater) {
    return (ViewGroup)paramLayoutInflater.inflate(17367132, (ViewGroup)null, false);
  }
  
  default ViewGroup getResolverProfileDescriptor(LayoutInflater paramLayoutInflater) {
    return (ViewGroup)paramLayoutInflater.inflate(17367275, (ViewGroup)null, false);
  }
  
  default void displayTextAddActionButton(ViewGroup paramViewGroup, View.OnClickListener paramOnClickListener) {}
  
  default void setCustomRoundImage(Paint paramPaint1, Paint paramPaint2, Paint paramPaint3) {}
  
  default int getCornerRadius(Context paramContext) {
    return paramContext.getResources().getDimensionPixelSize(17105032);
  }
  
  default void addNearbyAction(ViewGroup paramViewGroup, Intent paramIntent) {}
}
