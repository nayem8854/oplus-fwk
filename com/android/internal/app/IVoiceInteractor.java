package com.android.internal.app;

import android.app.VoiceInteractor;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ICancellationSignal;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public interface IVoiceInteractor extends IInterface {
  void notifyDirectActionsChanged(int paramInt, IBinder paramIBinder) throws RemoteException;
  
  void setKillCallback(ICancellationSignal paramICancellationSignal) throws RemoteException;
  
  IVoiceInteractorRequest startAbortVoice(String paramString, IVoiceInteractorCallback paramIVoiceInteractorCallback, VoiceInteractor.Prompt paramPrompt, Bundle paramBundle) throws RemoteException;
  
  IVoiceInteractorRequest startCommand(String paramString1, IVoiceInteractorCallback paramIVoiceInteractorCallback, String paramString2, Bundle paramBundle) throws RemoteException;
  
  IVoiceInteractorRequest startCompleteVoice(String paramString, IVoiceInteractorCallback paramIVoiceInteractorCallback, VoiceInteractor.Prompt paramPrompt, Bundle paramBundle) throws RemoteException;
  
  IVoiceInteractorRequest startConfirmation(String paramString, IVoiceInteractorCallback paramIVoiceInteractorCallback, VoiceInteractor.Prompt paramPrompt, Bundle paramBundle) throws RemoteException;
  
  IVoiceInteractorRequest startPickOption(String paramString, IVoiceInteractorCallback paramIVoiceInteractorCallback, VoiceInteractor.Prompt paramPrompt, VoiceInteractor.PickOptionRequest.Option[] paramArrayOfOption, Bundle paramBundle) throws RemoteException;
  
  boolean[] supportsCommands(String paramString, String[] paramArrayOfString) throws RemoteException;
  
  class Default implements IVoiceInteractor {
    public IVoiceInteractorRequest startConfirmation(String param1String, IVoiceInteractorCallback param1IVoiceInteractorCallback, VoiceInteractor.Prompt param1Prompt, Bundle param1Bundle) throws RemoteException {
      return null;
    }
    
    public IVoiceInteractorRequest startPickOption(String param1String, IVoiceInteractorCallback param1IVoiceInteractorCallback, VoiceInteractor.Prompt param1Prompt, VoiceInteractor.PickOptionRequest.Option[] param1ArrayOfOption, Bundle param1Bundle) throws RemoteException {
      return null;
    }
    
    public IVoiceInteractorRequest startCompleteVoice(String param1String, IVoiceInteractorCallback param1IVoiceInteractorCallback, VoiceInteractor.Prompt param1Prompt, Bundle param1Bundle) throws RemoteException {
      return null;
    }
    
    public IVoiceInteractorRequest startAbortVoice(String param1String, IVoiceInteractorCallback param1IVoiceInteractorCallback, VoiceInteractor.Prompt param1Prompt, Bundle param1Bundle) throws RemoteException {
      return null;
    }
    
    public IVoiceInteractorRequest startCommand(String param1String1, IVoiceInteractorCallback param1IVoiceInteractorCallback, String param1String2, Bundle param1Bundle) throws RemoteException {
      return null;
    }
    
    public boolean[] supportsCommands(String param1String, String[] param1ArrayOfString) throws RemoteException {
      return null;
    }
    
    public void notifyDirectActionsChanged(int param1Int, IBinder param1IBinder) throws RemoteException {}
    
    public void setKillCallback(ICancellationSignal param1ICancellationSignal) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IVoiceInteractor {
    private static final String DESCRIPTOR = "com.android.internal.app.IVoiceInteractor";
    
    static final int TRANSACTION_notifyDirectActionsChanged = 7;
    
    static final int TRANSACTION_setKillCallback = 8;
    
    static final int TRANSACTION_startAbortVoice = 4;
    
    static final int TRANSACTION_startCommand = 5;
    
    static final int TRANSACTION_startCompleteVoice = 3;
    
    static final int TRANSACTION_startConfirmation = 1;
    
    static final int TRANSACTION_startPickOption = 2;
    
    static final int TRANSACTION_supportsCommands = 6;
    
    public Stub() {
      attachInterface(this, "com.android.internal.app.IVoiceInteractor");
    }
    
    public static IVoiceInteractor asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.app.IVoiceInteractor");
      if (iInterface != null && iInterface instanceof IVoiceInteractor)
        return (IVoiceInteractor)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 8:
          return "setKillCallback";
        case 7:
          return "notifyDirectActionsChanged";
        case 6:
          return "supportsCommands";
        case 5:
          return "startCommand";
        case 4:
          return "startAbortVoice";
        case 3:
          return "startCompleteVoice";
        case 2:
          return "startPickOption";
        case 1:
          break;
      } 
      return "startConfirmation";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        ICancellationSignal iCancellationSignal;
        IBinder iBinder5;
        String[] arrayOfString;
        boolean[] arrayOfBoolean;
        String str2;
        IBinder iBinder4;
        String str1;
        IBinder iBinder3;
        IVoiceInteractorCallback iVoiceInteractorCallback2;
        IBinder iBinder2, iBinder1;
        VoiceInteractor.PickOptionRequest.Option[] arrayOfOption;
        IVoiceInteractorRequest iVoiceInteractorRequest1;
        String str5;
        IVoiceInteractorCallback iVoiceInteractorCallback4;
        String str3 = null;
        IBinder iBinder6 = null;
        String str6 = null;
        IVoiceInteractorCallback iVoiceInteractorCallback5 = null;
        String str7 = null;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 8:
            param1Parcel1.enforceInterface("com.android.internal.app.IVoiceInteractor");
            iCancellationSignal = ICancellationSignal.Stub.asInterface(param1Parcel1.readStrongBinder());
            setKillCallback(iCancellationSignal);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            iCancellationSignal.enforceInterface("com.android.internal.app.IVoiceInteractor");
            param1Int1 = iCancellationSignal.readInt();
            iBinder5 = iCancellationSignal.readStrongBinder();
            notifyDirectActionsChanged(param1Int1, iBinder5);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            iBinder5.enforceInterface("com.android.internal.app.IVoiceInteractor");
            str7 = iBinder5.readString();
            arrayOfString = iBinder5.createStringArray();
            arrayOfBoolean = supportsCommands(str7, arrayOfString);
            param1Parcel2.writeNoException();
            param1Parcel2.writeBooleanArray(arrayOfBoolean);
            return true;
          case 5:
            arrayOfBoolean.enforceInterface("com.android.internal.app.IVoiceInteractor");
            str6 = arrayOfBoolean.readString();
            iVoiceInteractorCallback5 = IVoiceInteractorCallback.Stub.asInterface(arrayOfBoolean.readStrongBinder());
            str3 = arrayOfBoolean.readString();
            if (arrayOfBoolean.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)arrayOfBoolean);
            } else {
              arrayOfBoolean = null;
            } 
            iVoiceInteractorRequest1 = startCommand(str6, iVoiceInteractorCallback5, str3, (Bundle)arrayOfBoolean);
            param1Parcel2.writeNoException();
            str2 = str7;
            if (iVoiceInteractorRequest1 != null)
              iBinder4 = iVoiceInteractorRequest1.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder4);
            return true;
          case 4:
            iBinder4.enforceInterface("com.android.internal.app.IVoiceInteractor");
            str5 = iBinder4.readString();
            iVoiceInteractorCallback5 = IVoiceInteractorCallback.Stub.asInterface(iBinder4.readStrongBinder());
            if (iBinder4.readInt() != 0) {
              VoiceInteractor.Prompt prompt = (VoiceInteractor.Prompt)VoiceInteractor.Prompt.CREATOR.createFromParcel((Parcel)iBinder4);
            } else {
              str7 = null;
            } 
            if (iBinder4.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iBinder4);
            } else {
              iBinder4 = null;
            } 
            iVoiceInteractorRequest2 = startAbortVoice(str5, iVoiceInteractorCallback5, (VoiceInteractor.Prompt)str7, (Bundle)iBinder4);
            param1Parcel2.writeNoException();
            str1 = str3;
            if (iVoiceInteractorRequest2 != null)
              iBinder3 = iVoiceInteractorRequest2.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder3);
            return true;
          case 3:
            iBinder3.enforceInterface("com.android.internal.app.IVoiceInteractor");
            str3 = iBinder3.readString();
            iVoiceInteractorCallback4 = IVoiceInteractorCallback.Stub.asInterface(iBinder3.readStrongBinder());
            if (iBinder3.readInt() != 0) {
              VoiceInteractor.Prompt prompt = (VoiceInteractor.Prompt)VoiceInteractor.Prompt.CREATOR.createFromParcel((Parcel)iBinder3);
            } else {
              iVoiceInteractorRequest2 = null;
            } 
            if (iBinder3.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iBinder3);
            } else {
              iBinder3 = null;
            } 
            iVoiceInteractorRequest2 = startCompleteVoice(str3, iVoiceInteractorCallback4, (VoiceInteractor.Prompt)iVoiceInteractorRequest2, (Bundle)iBinder3);
            param1Parcel2.writeNoException();
            iBinder3 = iBinder6;
            if (iVoiceInteractorRequest2 != null)
              iBinder3 = iVoiceInteractorRequest2.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder3);
            return true;
          case 2:
            iBinder3.enforceInterface("com.android.internal.app.IVoiceInteractor");
            str3 = iBinder3.readString();
            iVoiceInteractorCallback5 = IVoiceInteractorCallback.Stub.asInterface(iBinder3.readStrongBinder());
            if (iBinder3.readInt() != 0) {
              VoiceInteractor.Prompt prompt = (VoiceInteractor.Prompt)VoiceInteractor.Prompt.CREATOR.createFromParcel((Parcel)iBinder3);
            } else {
              iVoiceInteractorRequest2 = null;
            } 
            arrayOfOption = (VoiceInteractor.PickOptionRequest.Option[])iBinder3.createTypedArray(VoiceInteractor.PickOptionRequest.Option.CREATOR);
            if (iBinder3.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iBinder3);
            } else {
              iBinder3 = null;
            } 
            iVoiceInteractorRequest2 = startPickOption(str3, iVoiceInteractorCallback5, (VoiceInteractor.Prompt)iVoiceInteractorRequest2, arrayOfOption, (Bundle)iBinder3);
            param1Parcel2.writeNoException();
            iVoiceInteractorCallback2 = iVoiceInteractorCallback4;
            if (iVoiceInteractorRequest2 != null)
              iBinder2 = iVoiceInteractorRequest2.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder2);
            return true;
          case 1:
            break;
        } 
        iBinder2.enforceInterface("com.android.internal.app.IVoiceInteractor");
        String str4 = iBinder2.readString();
        IVoiceInteractorCallback iVoiceInteractorCallback3 = IVoiceInteractorCallback.Stub.asInterface(iBinder2.readStrongBinder());
        if (iBinder2.readInt() != 0) {
          VoiceInteractor.Prompt prompt = (VoiceInteractor.Prompt)VoiceInteractor.Prompt.CREATOR.createFromParcel((Parcel)iBinder2);
        } else {
          iVoiceInteractorRequest2 = null;
        } 
        if (iBinder2.readInt() != 0) {
          Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iBinder2);
        } else {
          iBinder2 = null;
        } 
        IVoiceInteractorRequest iVoiceInteractorRequest2 = startConfirmation(str4, iVoiceInteractorCallback3, (VoiceInteractor.Prompt)iVoiceInteractorRequest2, (Bundle)iBinder2);
        param1Parcel2.writeNoException();
        IVoiceInteractorCallback iVoiceInteractorCallback1 = iVoiceInteractorCallback5;
        if (iVoiceInteractorRequest2 != null)
          iBinder1 = iVoiceInteractorRequest2.asBinder(); 
        param1Parcel2.writeStrongBinder(iBinder1);
        return true;
      } 
      param1Parcel2.writeString("com.android.internal.app.IVoiceInteractor");
      return true;
    }
    
    private static class Proxy implements IVoiceInteractor {
      public static IVoiceInteractor sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.app.IVoiceInteractor";
      }
      
      public IVoiceInteractorRequest startConfirmation(String param2String, IVoiceInteractorCallback param2IVoiceInteractorCallback, VoiceInteractor.Prompt param2Prompt, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractor");
          parcel1.writeString(param2String);
          if (param2IVoiceInteractorCallback != null) {
            iBinder = param2IVoiceInteractorCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2Prompt != null) {
            parcel1.writeInt(1);
            param2Prompt.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractor.Stub.getDefaultImpl() != null)
            return IVoiceInteractor.Stub.getDefaultImpl().startConfirmation(param2String, param2IVoiceInteractorCallback, param2Prompt, param2Bundle); 
          parcel2.readException();
          return IVoiceInteractorRequest.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IVoiceInteractorRequest startPickOption(String param2String, IVoiceInteractorCallback param2IVoiceInteractorCallback, VoiceInteractor.Prompt param2Prompt, VoiceInteractor.PickOptionRequest.Option[] param2ArrayOfOption, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractor");
          parcel1.writeString(param2String);
          if (param2IVoiceInteractorCallback != null) {
            iBinder = param2IVoiceInteractorCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2Prompt != null) {
            parcel1.writeInt(1);
            param2Prompt.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeTypedArray((Parcelable[])param2ArrayOfOption, 0);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractor.Stub.getDefaultImpl() != null)
            return IVoiceInteractor.Stub.getDefaultImpl().startPickOption(param2String, param2IVoiceInteractorCallback, param2Prompt, param2ArrayOfOption, param2Bundle); 
          parcel2.readException();
          return IVoiceInteractorRequest.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IVoiceInteractorRequest startCompleteVoice(String param2String, IVoiceInteractorCallback param2IVoiceInteractorCallback, VoiceInteractor.Prompt param2Prompt, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractor");
          parcel1.writeString(param2String);
          if (param2IVoiceInteractorCallback != null) {
            iBinder = param2IVoiceInteractorCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2Prompt != null) {
            parcel1.writeInt(1);
            param2Prompt.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractor.Stub.getDefaultImpl() != null)
            return IVoiceInteractor.Stub.getDefaultImpl().startCompleteVoice(param2String, param2IVoiceInteractorCallback, param2Prompt, param2Bundle); 
          parcel2.readException();
          return IVoiceInteractorRequest.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IVoiceInteractorRequest startAbortVoice(String param2String, IVoiceInteractorCallback param2IVoiceInteractorCallback, VoiceInteractor.Prompt param2Prompt, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractor");
          parcel1.writeString(param2String);
          if (param2IVoiceInteractorCallback != null) {
            iBinder = param2IVoiceInteractorCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2Prompt != null) {
            parcel1.writeInt(1);
            param2Prompt.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractor.Stub.getDefaultImpl() != null)
            return IVoiceInteractor.Stub.getDefaultImpl().startAbortVoice(param2String, param2IVoiceInteractorCallback, param2Prompt, param2Bundle); 
          parcel2.readException();
          return IVoiceInteractorRequest.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IVoiceInteractorRequest startCommand(String param2String1, IVoiceInteractorCallback param2IVoiceInteractorCallback, String param2String2, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractor");
          parcel1.writeString(param2String1);
          if (param2IVoiceInteractorCallback != null) {
            iBinder = param2IVoiceInteractorCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String2);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractor.Stub.getDefaultImpl() != null)
            return IVoiceInteractor.Stub.getDefaultImpl().startCommand(param2String1, param2IVoiceInteractorCallback, param2String2, param2Bundle); 
          parcel2.readException();
          return IVoiceInteractorRequest.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean[] supportsCommands(String param2String, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractor");
          parcel1.writeString(param2String);
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractor.Stub.getDefaultImpl() != null)
            return IVoiceInteractor.Stub.getDefaultImpl().supportsCommands(param2String, param2ArrayOfString); 
          parcel2.readException();
          return parcel2.createBooleanArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyDirectActionsChanged(int param2Int, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractor");
          parcel1.writeInt(param2Int);
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractor.Stub.getDefaultImpl() != null) {
            IVoiceInteractor.Stub.getDefaultImpl().notifyDirectActionsChanged(param2Int, param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setKillCallback(ICancellationSignal param2ICancellationSignal) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.app.IVoiceInteractor");
          if (param2ICancellationSignal != null) {
            iBinder = param2ICancellationSignal.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IVoiceInteractor.Stub.getDefaultImpl() != null) {
            IVoiceInteractor.Stub.getDefaultImpl().setKillCallback(param2ICancellationSignal);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IVoiceInteractor param1IVoiceInteractor) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IVoiceInteractor != null) {
          Proxy.sDefaultImpl = param1IVoiceInteractor;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IVoiceInteractor getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
