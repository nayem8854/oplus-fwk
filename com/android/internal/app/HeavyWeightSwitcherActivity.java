package com.android.internal.app;

import android.app.Activity;
import android.content.IntentSender;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class HeavyWeightSwitcherActivity extends Activity {
  protected void onCreate(Bundle paramBundle) {
    super.onCreate(paramBundle);
    requestWindowFeature(1);
    this.mStartIntent = (IntentSender)getIntent().getParcelableExtra("intent");
    this.mHasResult = getIntent().getBooleanExtra("has_result", false);
    this.mCurApp = getIntent().getStringExtra("cur_app");
    this.mCurTask = getIntent().getIntExtra("cur_task", 0);
    this.mNewApp = getIntent().getStringExtra("new_app");
    setContentView(17367171);
    setIconAndText(16909253, 16909252, 0, this.mCurApp, this.mNewApp, 17040752, 0);
    setIconAndText(16909205, 16909203, 16909204, this.mNewApp, this.mCurApp, 17040689, 17040690);
    View view = findViewById(16909496);
    view.setOnClickListener(this.mSwitchOldListener);
    view = findViewById(16909495);
    view.setOnClickListener(this.mSwitchNewListener);
  }
  
  void setText(int paramInt, CharSequence paramCharSequence) {
    ((TextView)findViewById(paramInt)).setText(paramCharSequence);
  }
  
  void setDrawable(int paramInt, Drawable paramDrawable) {
    if (paramDrawable != null)
      ((ImageView)findViewById(paramInt)).setImageDrawable(paramDrawable); 
  }
  
  void setIconAndText(int paramInt1, int paramInt2, int paramInt3, String paramString1, String paramString2, int paramInt4, int paramInt5) {
    String str = paramString1;
    ApplicationInfo applicationInfo1 = null;
    CharSequence charSequence = str;
    ApplicationInfo applicationInfo2 = applicationInfo1;
    if (paramString1 != null) {
      charSequence = str;
      try {
        applicationInfo2 = getPackageManager().getApplicationInfo(paramString1, 0);
        charSequence = str;
        CharSequence charSequence1 = applicationInfo2.loadLabel(getPackageManager());
        charSequence = charSequence1;
        Drawable drawable = applicationInfo2.loadIcon(getPackageManager());
        charSequence = charSequence1;
      } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
        applicationInfo2 = applicationInfo1;
      } 
    } 
    setDrawable(paramInt1, (Drawable)applicationInfo2);
    setText(paramInt2, getString(paramInt4, new Object[] { charSequence }));
    if (paramInt3 != 0) {
      paramString1 = paramString2;
      charSequence = paramString1;
      if (paramString2 != null)
        try {
          ApplicationInfo applicationInfo = getPackageManager().getApplicationInfo(paramString2, 0);
          charSequence = applicationInfo.loadLabel(getPackageManager());
        } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
          charSequence = paramString1;
        }  
      setText(paramInt3, getString(paramInt5, new Object[] { charSequence }));
    } 
  }
  
  private View.OnClickListener mSwitchOldListener = (View.OnClickListener)new Object(this);
  
  private View.OnClickListener mSwitchNewListener = (View.OnClickListener)new Object(this);
  
  IntentSender mStartIntent;
  
  String mNewApp;
  
  boolean mHasResult;
  
  int mCurTask;
  
  String mCurApp;
  
  public static final String KEY_NEW_APP = "new_app";
  
  public static final String KEY_INTENT = "intent";
  
  public static final String KEY_HAS_RESULT = "has_result";
  
  public static final String KEY_CUR_TASK = "cur_task";
  
  public static final String KEY_CUR_APP = "cur_app";
}
