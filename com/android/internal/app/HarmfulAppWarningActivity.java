package com.android.internal.app;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class HarmfulAppWarningActivity extends AlertActivity implements DialogInterface.OnClickListener {
  private static final String EXTRA_HARMFUL_APP_WARNING = "harmful_app_warning";
  
  private static final String TAG = HarmfulAppWarningActivity.class.getSimpleName();
  
  private String mHarmfulAppWarning;
  
  private String mPackageName;
  
  private IntentSender mTarget;
  
  protected void onCreate(Bundle paramBundle) {
    super.onCreate(paramBundle);
    Intent intent = getIntent();
    this.mPackageName = intent.getStringExtra("android.intent.extra.PACKAGE_NAME");
    this.mTarget = (IntentSender)intent.getParcelableExtra("android.intent.extra.INTENT");
    String str = intent.getStringExtra("harmful_app_warning");
    if (this.mPackageName == null || this.mTarget == null || str == null) {
      String str1 = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid intent: ");
      stringBuilder.append(intent.toString());
      Log.wtf(str1, stringBuilder.toString());
      finish();
    } 
    try {
      ApplicationInfo applicationInfo = getPackageManager().getApplicationInfo(this.mPackageName, 0);
      AlertController.AlertParams alertParams = this.mAlertParams;
      alertParams.mTitle = getString(17040302);
      alertParams.mView = createView(applicationInfo);
      alertParams.mPositiveButtonText = getString(17040303);
      alertParams.mPositiveButtonListener = this;
      alertParams.mNegativeButtonText = getString(17040301);
      alertParams.mNegativeButtonListener = this;
      this.mAlert.installContent(this.mAlertParams);
      return;
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      Log.e(TAG, "Could not show warning because package does not exist ", (Throwable)nameNotFoundException);
      finish();
      return;
    } 
  }
  
  private View createView(ApplicationInfo paramApplicationInfo) {
    View view = getLayoutInflater().inflate(17367170, (ViewGroup)null);
    TextView textView = view.<TextView>findViewById(16908761);
    textView.setText(paramApplicationInfo.loadSafeLabel(getPackageManager(), 500.0F, 5));
    textView = view.<TextView>findViewById(16908299);
    String str = this.mHarmfulAppWarning;
    textView.setText(str);
    return view;
  }
  
  public void onClick(DialogInterface paramDialogInterface, int paramInt) {
    if (paramInt != -2) {
      if (paramInt == -1) {
        getPackageManager().deletePackage(this.mPackageName, null, 0);
        EventLogTags.writeHarmfulAppWarningUninstall(this.mPackageName);
        finish();
      } 
    } else {
      getPackageManager().setHarmfulAppWarning(this.mPackageName, null);
      IntentSender intentSender = (IntentSender)getIntent().getParcelableExtra("android.intent.extra.INTENT");
      try {
        startIntentSenderForResult(intentSender, -1, null, 0, 0, 0);
      } catch (android.content.IntentSender.SendIntentException sendIntentException) {
        Log.e(TAG, "Error while starting intent sender", (Throwable)sendIntentException);
      } 
      EventLogTags.writeHarmfulAppWarningLaunchAnyway(this.mPackageName);
      finish();
    } 
  }
  
  public static Intent createHarmfulAppWarningIntent(Context paramContext, String paramString, IntentSender paramIntentSender, CharSequence paramCharSequence) {
    Intent intent = new Intent();
    intent.setClass(paramContext, HarmfulAppWarningActivity.class);
    intent.putExtra("android.intent.extra.PACKAGE_NAME", paramString);
    intent.putExtra("android.intent.extra.INTENT", (Parcelable)paramIntentSender);
    intent.putExtra("harmful_app_warning", paramCharSequence);
    return intent;
  }
}
