package com.android.internal.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IMotorManager extends IInterface {
  void breathLedLoopEffect(int paramInt) throws RemoteException;
  
  int downMotorByPrivacyApp(String paramString, int paramInt, IBinder paramIBinder) throws RemoteException;
  
  int downMotorBySystemApp(String paramString, IBinder paramIBinder) throws RemoteException;
  
  int getMotorStateBySystemApp() throws RemoteException;
  
  int upMotorBySystemApp(String paramString, IBinder paramIBinder) throws RemoteException;
  
  class Default implements IMotorManager {
    public int getMotorStateBySystemApp() throws RemoteException {
      return 0;
    }
    
    public int downMotorBySystemApp(String param1String, IBinder param1IBinder) throws RemoteException {
      return 0;
    }
    
    public int upMotorBySystemApp(String param1String, IBinder param1IBinder) throws RemoteException {
      return 0;
    }
    
    public int downMotorByPrivacyApp(String param1String, int param1Int, IBinder param1IBinder) throws RemoteException {
      return 0;
    }
    
    public void breathLedLoopEffect(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMotorManager {
    private static final String DESCRIPTOR = "com.android.internal.app.IMotorManager";
    
    static final int TRANSACTION_breathLedLoopEffect = 5;
    
    static final int TRANSACTION_downMotorByPrivacyApp = 4;
    
    static final int TRANSACTION_downMotorBySystemApp = 2;
    
    static final int TRANSACTION_getMotorStateBySystemApp = 1;
    
    static final int TRANSACTION_upMotorBySystemApp = 3;
    
    public Stub() {
      attachInterface(this, "com.android.internal.app.IMotorManager");
    }
    
    public static IMotorManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.app.IMotorManager");
      if (iInterface != null && iInterface instanceof IMotorManager)
        return (IMotorManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "breathLedLoopEffect";
            } 
            return "downMotorByPrivacyApp";
          } 
          return "upMotorBySystemApp";
        } 
        return "downMotorBySystemApp";
      } 
      return "getMotorStateBySystemApp";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IBinder iBinder;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("com.android.internal.app.IMotorManager");
                return true;
              } 
              param1Parcel1.enforceInterface("com.android.internal.app.IMotorManager");
              param1Int1 = param1Parcel1.readInt();
              breathLedLoopEffect(param1Int1);
              param1Parcel2.writeNoException();
              return true;
            } 
            param1Parcel1.enforceInterface("com.android.internal.app.IMotorManager");
            String str2 = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            iBinder = param1Parcel1.readStrongBinder();
            param1Int1 = downMotorByPrivacyApp(str2, param1Int1, iBinder);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          } 
          iBinder.enforceInterface("com.android.internal.app.IMotorManager");
          String str1 = iBinder.readString();
          iBinder = iBinder.readStrongBinder();
          param1Int1 = upMotorBySystemApp(str1, iBinder);
          param1Parcel2.writeNoException();
          param1Parcel2.writeInt(param1Int1);
          return true;
        } 
        iBinder.enforceInterface("com.android.internal.app.IMotorManager");
        String str = iBinder.readString();
        iBinder = iBinder.readStrongBinder();
        param1Int1 = downMotorBySystemApp(str, iBinder);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(param1Int1);
        return true;
      } 
      iBinder.enforceInterface("com.android.internal.app.IMotorManager");
      param1Int1 = getMotorStateBySystemApp();
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(param1Int1);
      return true;
    }
    
    class Proxy implements IMotorManager {
      public static IMotorManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IMotorManager.Stub this$0) {
        this.mRemote = (IBinder)this$0;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.app.IMotorManager";
      }
      
      public int getMotorStateBySystemApp() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IMotorManager");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IMotorManager.Stub.getDefaultImpl() != null)
            return IMotorManager.Stub.getDefaultImpl().getMotorStateBySystemApp(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int downMotorBySystemApp(String param2String, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IMotorManager");
          parcel1.writeString(param2String);
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IMotorManager.Stub.getDefaultImpl() != null)
            return IMotorManager.Stub.getDefaultImpl().downMotorBySystemApp(param2String, param2IBinder); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int upMotorBySystemApp(String param2String, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IMotorManager");
          parcel1.writeString(param2String);
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IMotorManager.Stub.getDefaultImpl() != null)
            return IMotorManager.Stub.getDefaultImpl().upMotorBySystemApp(param2String, param2IBinder); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int downMotorByPrivacyApp(String param2String, int param2Int, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IMotorManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IMotorManager.Stub.getDefaultImpl() != null) {
            param2Int = IMotorManager.Stub.getDefaultImpl().downMotorByPrivacyApp(param2String, param2Int, param2IBinder);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void breathLedLoopEffect(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IMotorManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IMotorManager.Stub.getDefaultImpl() != null) {
            IMotorManager.Stub.getDefaultImpl().breathLedLoopEffect(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMotorManager param1IMotorManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMotorManager != null) {
          Proxy.sDefaultImpl = param1IMotorManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMotorManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
