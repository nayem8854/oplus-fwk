package com.android.internal.app;

import android.app.Activity;
import android.app.IUiModeManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;

public class DisableCarModeActivity extends Activity {
  private static final String TAG = "DisableCarModeActivity";
  
  protected void onCreate(Bundle paramBundle) {
    super.onCreate(paramBundle);
    try {
      IBinder iBinder = ServiceManager.getService("uimode");
      IUiModeManager iUiModeManager = IUiModeManager.Stub.asInterface(iBinder);
      String str = getOpPackageName();
      iUiModeManager.disableCarModeByCallingPackage(3, str);
    } catch (RemoteException remoteException) {
      Log.e("DisableCarModeActivity", "Failed to disable car mode", (Throwable)remoteException);
    } 
    finish();
  }
}
