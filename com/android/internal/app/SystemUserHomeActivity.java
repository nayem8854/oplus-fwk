package com.android.internal.app;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class SystemUserHomeActivity extends Activity {
  private static final String TAG = "SystemUserHome";
  
  protected void onCreate(Bundle paramBundle) {
    super.onCreate(paramBundle);
    Log.i("SystemUserHome", "onCreate");
    setContentView(17367317);
  }
  
  protected void onDestroy() {
    super.onDestroy();
    Log.i("SystemUserHome", "onDestroy");
  }
}
