package com.android.internal.app;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Slog;

public class BlockedAppActivity extends AlertActivity {
  private static final String EXTRA_BLOCKED_PACKAGE = "com.android.internal.app.extra.BLOCKED_PACKAGE";
  
  private static final String PACKAGE_NAME = "com.android.internal.app";
  
  private static final String TAG = "BlockedAppActivity";
  
  protected void onCreate(Bundle paramBundle) {
    StringBuilder stringBuilder;
    super.onCreate(paramBundle);
    Intent intent = getIntent();
    int i = intent.getIntExtra("android.intent.extra.USER_ID", -1);
    if (i < 0) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid user: ");
      stringBuilder.append(i);
      Slog.wtf("BlockedAppActivity", stringBuilder.toString());
      finish();
      return;
    } 
    String str = stringBuilder.getStringExtra("com.android.internal.app.extra.BLOCKED_PACKAGE");
    if (TextUtils.isEmpty(str)) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid package: ");
      stringBuilder.append(str);
      Slog.wtf("BlockedAppActivity", stringBuilder.toString());
      finish();
      return;
    } 
    CharSequence charSequence = getAppLabel(i, str);
    this.mAlertParams.mTitle = getString(17039649);
    this.mAlertParams.mMessage = getString(17039648, new Object[] { charSequence });
    this.mAlertParams.mPositiveButtonText = getString(17039370);
    setupAlert();
  }
  
  private CharSequence getAppLabel(int paramInt, String paramString) {
    PackageManager packageManager = getPackageManager();
    try {
      ApplicationInfo applicationInfo = packageManager.getApplicationInfoAsUser(paramString, 0, paramInt);
      return applicationInfo.loadLabel(packageManager);
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Package ");
      stringBuilder.append(paramString);
      stringBuilder.append(" not found");
      Slog.e("BlockedAppActivity", stringBuilder.toString(), (Throwable)nameNotFoundException);
      return paramString;
    } 
  }
  
  public static Intent createIntent(int paramInt, String paramString) {
    Intent intent = new Intent();
    intent = intent.setClassName("android", BlockedAppActivity.class.getName());
    intent = intent.putExtra("android.intent.extra.USER_ID", paramInt);
    return intent.putExtra("com.android.internal.app.extra.BLOCKED_PACKAGE", paramString);
  }
}
