package com.android.internal.app;

import android.app.prediction.AppPredictor;
import android.app.prediction.AppTarget;
import android.app.prediction.AppTargetEvent;
import android.app.prediction.AppTargetId;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Message;
import android.os.UserHandle;
import android.provider.DeviceConfig;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class AppPredictionServiceResolverComparator extends AbstractResolverComparator {
  private final Map<ComponentName, Integer> mTargetRanks = new HashMap<>();
  
  private final Map<ComponentName, Integer> mTargetScores = new HashMap<>();
  
  private boolean mAppendDirectShareEnabled = DeviceConfig.getBoolean("systemui", "append_direct_share_enabled", true);
  
  private static final String TAG = "APSResolverComparator";
  
  private final AppPredictor mAppPredictor;
  
  private final Context mContext;
  
  private final Intent mIntent;
  
  private final String mReferrerPackage;
  
  private ResolverRankerServiceResolverComparator mResolverRankerService;
  
  private final UserHandle mUser;
  
  AppPredictionServiceResolverComparator(Context paramContext, Intent paramIntent, String paramString, AppPredictor paramAppPredictor, UserHandle paramUserHandle) {
    super(paramContext, paramIntent);
    this.mContext = paramContext;
    this.mIntent = paramIntent;
    this.mAppPredictor = paramAppPredictor;
    this.mUser = paramUserHandle;
    this.mReferrerPackage = paramString;
  }
  
  int compare(ResolveInfo paramResolveInfo1, ResolveInfo paramResolveInfo2) {
    ResolverRankerServiceResolverComparator resolverRankerServiceResolverComparator = this.mResolverRankerService;
    if (resolverRankerServiceResolverComparator != null)
      return resolverRankerServiceResolverComparator.compare(paramResolveInfo1, paramResolveInfo2); 
    Integer integer1 = this.mTargetRanks.get(new ComponentName(paramResolveInfo1.activityInfo.packageName, paramResolveInfo1.activityInfo.name));
    Integer integer2 = this.mTargetRanks.get(new ComponentName(paramResolveInfo2.activityInfo.packageName, paramResolveInfo2.activityInfo.name));
    if (integer1 == null && integer2 == null)
      return 0; 
    if (integer1 == null)
      return -1; 
    if (integer2 == null)
      return 1; 
    return integer1.intValue() - integer2.intValue();
  }
  
  void doCompute(List<ResolverActivity.ResolvedComponentInfo> paramList) {
    if (paramList.isEmpty()) {
      this.mHandler.sendEmptyMessage(0);
      return;
    } 
    ArrayList<AppTarget> arrayList = new ArrayList();
    for (ResolverActivity.ResolvedComponentInfo resolvedComponentInfo : paramList) {
      ComponentName componentName2 = resolvedComponentInfo.name;
      AppTargetId appTargetId = new AppTargetId(componentName2.flattenToString());
      componentName2 = resolvedComponentInfo.name;
      AppTarget.Builder builder2 = new AppTarget.Builder(appTargetId, componentName2.getPackageName(), this.mUser);
      ComponentName componentName1 = resolvedComponentInfo.name;
      AppTarget.Builder builder1 = builder2.setClassName(componentName1.getClassName());
      AppTarget appTarget = builder1.build();
      arrayList.add(appTarget);
    } 
    this.mAppPredictor.sortTargets(arrayList, Executors.newSingleThreadExecutor(), new _$$Lambda$AppPredictionServiceResolverComparator$PQ_i16vesHTtkDyBgU_HkS0uF1A(this, paramList));
  }
  
  void handleResultMessage(Message paramMessage) {
    List<AppTarget> list;
    if (paramMessage.what == 0 && paramMessage.obj != null) {
      list = (List)paramMessage.obj;
      handleSortedAppTargets(list);
    } else if (((Message)list).obj == null && this.mResolverRankerService == null) {
      Log.e("APSResolverComparator", "Unexpected null result");
    } 
  }
  
  private void handleResult(List<AppTarget> paramList) {
    if (this.mHandler.hasMessages(1)) {
      handleSortedAppTargets(paramList);
      this.mHandler.removeMessages(1);
      afterCompute();
    } 
  }
  
  private void handleSortedAppTargets(List<AppTarget> paramList) {
    if (checkAppTargetRankValid(paramList))
      paramList.forEach(new _$$Lambda$AppPredictionServiceResolverComparator$2eH8BdPmQNaGw6ZzFc7QgsKf5CY(this)); 
    for (byte b = 0; b < paramList.size(); b++) {
      String str = ((AppTarget)paramList.get(b)).getPackageName();
      ComponentName componentName = new ComponentName(str, ((AppTarget)paramList.get(b)).getClassName());
      this.mTargetRanks.put(componentName, Integer.valueOf(b));
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("handleSortedAppTargets, sortedAppTargets #");
      stringBuilder.append(b);
      stringBuilder.append(": ");
      stringBuilder.append(componentName);
      Log.i("APSResolverComparator", stringBuilder.toString());
    } 
  }
  
  private boolean checkAppTargetRankValid(List<AppTarget> paramList) {
    for (AppTarget appTarget : paramList) {
      if (appTarget.getRank() != 0)
        return true; 
    } 
    return false;
  }
  
  float getScore(ComponentName paramComponentName) {
    ResolverRankerServiceResolverComparator resolverRankerServiceResolverComparator = this.mResolverRankerService;
    if (resolverRankerServiceResolverComparator != null)
      return resolverRankerServiceResolverComparator.getScore(paramComponentName); 
    if (this.mAppendDirectShareEnabled && !this.mTargetScores.isEmpty())
      return ((Integer)this.mTargetScores.get(paramComponentName)).intValue(); 
    Integer integer = this.mTargetRanks.get(paramComponentName);
    if (integer == null) {
      Log.w("APSResolverComparator", "Score requested for unknown component.");
      return 0.0F;
    } 
    int i = (this.mTargetRanks.size() - 1) * this.mTargetRanks.size() / 2;
    return 1.0F - integer.intValue() / i;
  }
  
  List<ComponentName> getTopComponentNames(int paramInt) {
    ResolverRankerServiceResolverComparator resolverRankerServiceResolverComparator = this.mResolverRankerService;
    if (resolverRankerServiceResolverComparator != null)
      return resolverRankerServiceResolverComparator.getTopComponentNames(paramInt); 
    Stream stream2 = this.mTargetRanks.entrySet().stream();
    stream2 = stream2.sorted(Map.Entry.comparingByValue());
    long l = paramInt;
    Stream stream3 = stream2.limit(l);
    -$.Lambda.vbUiOaY5W_R3oio_uxy-QbdJwl0 vbUiOaY5W_R3oio_uxy-QbdJwl0 = _$$Lambda$vbUiOaY5W_R3oio_uxy_QbdJwl0.INSTANCE;
    Stream stream1 = stream3.map((Function)vbUiOaY5W_R3oio_uxy-QbdJwl0);
    return (List)stream1.collect(Collectors.toList());
  }
  
  void updateModel(ComponentName paramComponentName) {
    ResolverRankerServiceResolverComparator resolverRankerServiceResolverComparator = this.mResolverRankerService;
    if (resolverRankerServiceResolverComparator != null) {
      resolverRankerServiceResolverComparator.updateModel(paramComponentName);
      return;
    } 
    AppPredictor appPredictor = this.mAppPredictor;
    AppTargetId appTargetId = new AppTargetId(paramComponentName.toString());
    AppTarget.Builder builder1 = new AppTarget.Builder(appTargetId, paramComponentName.getPackageName(), this.mUser);
    AppTargetEvent.Builder builder = new AppTargetEvent.Builder(builder1.setClassName(paramComponentName.getClassName()).build(), 1);
    AppTargetEvent appTargetEvent = builder.build();
    appPredictor.notifyAppTargetEvent(appTargetEvent);
  }
  
  void destroy() {
    ResolverRankerServiceResolverComparator resolverRankerServiceResolverComparator = this.mResolverRankerService;
    if (resolverRankerServiceResolverComparator != null) {
      resolverRankerServiceResolverComparator.destroy();
      this.mResolverRankerService = null;
    } 
  }
}
