package com.android.internal.app;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.Window;
import android.widget.ListView;

public interface IOplusAlertControllerEuclidManager extends IOplusCommonFeature {
  public static final IOplusAlertControllerEuclidManager DEFAULT = (IOplusAlertControllerEuclidManager)new Object();
  
  default IOplusAlertControllerEuclidManager getDefault() {
    return DEFAULT;
  }
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusAlertControllerEuclidManager;
  }
  
  default AlertController getAlertController(Context paramContext, DialogInterface paramDialogInterface, Window paramWindow) {
    return new AlertController(paramContext, paramDialogInterface, paramWindow);
  }
  
  default void setListStyle(ListView paramListView, boolean paramBoolean1, boolean paramBoolean2) {
    if (paramBoolean1) {
      paramListView.setChoiceMode(1);
    } else if (paramBoolean2) {
      paramListView.setChoiceMode(2);
    } 
  }
  
  default View getConvertView(View paramView, int paramInt1, int paramInt2) {
    return null;
  }
}
