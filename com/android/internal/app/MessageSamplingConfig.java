package com.android.internal.app;

import android.annotation.IntRange;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.AnnotationValidations;

public final class MessageSamplingConfig implements Parcelable {
  public MessageSamplingConfig(int paramInt1, int paramInt2, long paramLong) {
    this.mSampledOpCode = paramInt1;
    AnnotationValidations.validate(IntRange.class, null, paramInt1, "from", -1L, "to", 99L);
    this.mAcceptableLeftDistance = paramInt2;
    AnnotationValidations.validate(IntRange.class, null, paramInt2, "from", 0L, "to", 99L);
    this.mExpirationTimeSinceBootMillis = paramLong;
    AnnotationValidations.validate(IntRange.class, null, paramLong, "from", 0L);
  }
  
  public int getSampledOpCode() {
    return this.mSampledOpCode;
  }
  
  public int getAcceptableLeftDistance() {
    return this.mAcceptableLeftDistance;
  }
  
  public long getExpirationTimeSinceBootMillis() {
    return this.mExpirationTimeSinceBootMillis;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mSampledOpCode);
    paramParcel.writeInt(this.mAcceptableLeftDistance);
    paramParcel.writeLong(this.mExpirationTimeSinceBootMillis);
  }
  
  public int describeContents() {
    return 0;
  }
  
  MessageSamplingConfig(Parcel paramParcel) {
    int i = paramParcel.readInt();
    int j = paramParcel.readInt();
    long l = paramParcel.readLong();
    this.mSampledOpCode = i;
    AnnotationValidations.validate(IntRange.class, null, i, "from", -1L, "to", 99L);
    this.mAcceptableLeftDistance = j;
    AnnotationValidations.validate(IntRange.class, null, j, "from", 0L, "to", 99L);
    this.mExpirationTimeSinceBootMillis = l;
    AnnotationValidations.validate(IntRange.class, null, l, "from", 0L);
  }
  
  public static final Parcelable.Creator<MessageSamplingConfig> CREATOR = new Parcelable.Creator<MessageSamplingConfig>() {
      public MessageSamplingConfig[] newArray(int param1Int) {
        return new MessageSamplingConfig[param1Int];
      }
      
      public MessageSamplingConfig createFromParcel(Parcel param1Parcel) {
        return new MessageSamplingConfig(param1Parcel);
      }
    };
  
  private final int mAcceptableLeftDistance;
  
  private final long mExpirationTimeSinceBootMillis;
  
  private final int mSampledOpCode;
  
  @Deprecated
  private void __metadata() {}
}
