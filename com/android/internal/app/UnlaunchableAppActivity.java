package com.android.internal.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.common.IOplusCommonFeature;
import android.common.OplusFeatureCache;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.os.UserHandle;
import android.os.UserManager;
import android.util.Log;
import com.oplus.theme.IOplusThemeStyle;

public class UnlaunchableAppActivity extends Activity implements DialogInterface.OnDismissListener, DialogInterface.OnClickListener {
  private static final String EXTRA_UNLAUNCHABLE_REASON = "unlaunchable_reason";
  
  private static final String TAG = "UnlaunchableAppActivity";
  
  private static final int UNLAUNCHABLE_REASON_QUIET_MODE = 1;
  
  private int mReason;
  
  private IntentSender mTarget;
  
  private int mUserId;
  
  protected void onCreate(Bundle paramBundle) {
    super.onCreate(paramBundle);
    requestWindowFeature(1);
    Intent intent = getIntent();
    this.mReason = intent.getIntExtra("unlaunchable_reason", -1);
    this.mUserId = intent.getIntExtra("android.intent.extra.user_handle", -10000);
    this.mTarget = (IntentSender)intent.getParcelableExtra("android.intent.extra.INTENT");
    if (this.mUserId == -10000) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Invalid user id: ");
      stringBuilder1.append(this.mUserId);
      stringBuilder1.append(". Stopping.");
      Log.wtf("UnlaunchableAppActivity", stringBuilder1.toString());
      finish();
      return;
    } 
    if (this.mReason == 1) {
      String str2 = getResources().getString(17041587);
      String str1 = getResources().getString(17041586);
      AlertDialog.Builder builder2 = new AlertDialog.Builder((Context)this, ((IOplusThemeStyle)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusThemeStyle.DEFAULT, new Object[0])).getDialogThemeStyle(201523207));
      builder2 = builder2.setTitle(str2);
      AlertDialog.Builder builder1 = builder2.setMessage(str1);
      builder1 = builder1.setOnDismissListener(this);
      if (this.mReason == 1) {
        builder2 = builder1.setPositiveButton(17041588, this);
        builder2.setNegativeButton(17039360, null);
      } else {
        builder1.setPositiveButton(17039370, null);
      } 
      builder1.show();
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid unlaunchable type: ");
    stringBuilder.append(this.mReason);
    Log.wtf("UnlaunchableAppActivity", stringBuilder.toString());
    finish();
  }
  
  public void onDismiss(DialogInterface paramDialogInterface) {
    finish();
  }
  
  public void onClick(DialogInterface paramDialogInterface, int paramInt) {
    if (this.mReason == 1 && paramInt == -1) {
      UserManager userManager = UserManager.get((Context)this);
      (new Handler(Looper.getMainLooper())).post(new _$$Lambda$UnlaunchableAppActivity$2ENyH3JgWNnaEpcvk51IPFdyB3k(this, userManager));
    } 
  }
  
  private static final Intent createBaseIntent() {
    Intent intent = new Intent();
    intent.setComponent(new ComponentName("android", UnlaunchableAppActivity.class.getName()));
    intent.setFlags(276824064);
    return intent;
  }
  
  public static Intent createInQuietModeDialogIntent(int paramInt) {
    Intent intent = createBaseIntent();
    intent.putExtra("unlaunchable_reason", 1);
    intent.putExtra("android.intent.extra.user_handle", paramInt);
    return intent;
  }
  
  public static Intent createInQuietModeDialogIntent(int paramInt, IntentSender paramIntentSender) {
    Intent intent = createInQuietModeDialogIntent(paramInt);
    intent.putExtra("android.intent.extra.INTENT", (Parcelable)paramIntentSender);
    return intent;
  }
}
