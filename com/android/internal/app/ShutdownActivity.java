package com.android.internal.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Slog;

public class ShutdownActivity extends Activity {
  private static final String TAG = "ShutdownActivity";
  
  private boolean mConfirm;
  
  private boolean mReboot;
  
  private boolean mUserRequested;
  
  protected void onCreate(Bundle paramBundle) {
    String str;
    super.onCreate(paramBundle);
    Intent intent = getIntent();
    this.mReboot = "android.intent.action.REBOOT".equals(intent.getAction());
    this.mConfirm = intent.getBooleanExtra("android.intent.extra.KEY_CONFIRM", false);
    boolean bool = intent.getBooleanExtra("android.intent.extra.USER_REQUESTED_SHUTDOWN", false);
    if (bool) {
      str = "userrequested";
    } else {
      str = str.getStringExtra("android.intent.extra.REASON");
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("onCreate(): confirm=");
    stringBuilder.append(this.mConfirm);
    Slog.i("ShutdownActivity", stringBuilder.toString());
    Object object = new Object(this, "ShutdownActivity", str);
    object.start();
    finish();
    try {
      object.join();
    } catch (InterruptedException interruptedException) {}
  }
}
