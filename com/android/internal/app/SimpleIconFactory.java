package com.android.internal.app;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DrawFilter;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Xfermode;
import android.graphics.drawable.AdaptiveIconDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableWrapper;
import android.os.UserHandle;
import android.util.AttributeSet;
import android.util.Pools;
import android.util.TypedValue;
import java.util.Optional;
import org.xmlpull.v1.XmlPullParser;

@Deprecated
public class SimpleIconFactory {
  private static final int AMBIENT_SHADOW_ALPHA = 30;
  
  private static final float BLUR_FACTOR = 0.010416667F;
  
  private static final float CIRCLE_AREA_BY_RECT = 0.7853982F;
  
  private static final int DEFAULT_WRAPPER_BACKGROUND = -1;
  
  private static final int KEY_SHADOW_ALPHA = 61;
  
  private static final float KEY_SHADOW_DISTANCE = 0.020833334F;
  
  private static final float LINEAR_SCALE_SLOPE = 0.040449437F;
  
  private static final float MAX_CIRCLE_AREA_FACTOR = 0.6597222F;
  
  private static final float MAX_SQUARE_AREA_FACTOR = 0.6510417F;
  
  private static final int MIN_VISIBLE_ALPHA = 40;
  
  private static final float SCALE_NOT_INITIALIZED = 0.0F;
  
  private static final Pools.SynchronizedPool<SimpleIconFactory> sPool = new Pools.SynchronizedPool<>(Runtime.getRuntime().availableProcessors());
  
  private final Rect mAdaptiveIconBounds;
  
  private float mAdaptiveIconScale;
  
  private int mBadgeBitmapSize;
  
  private final Bitmap mBitmap;
  
  private Paint mBlurPaint;
  
  private final Rect mBounds;
  
  private Canvas mCanvas;
  
  private Context mContext;
  
  private BlurMaskFilter mDefaultBlurMaskFilter;
  
  private Paint mDrawPaint;
  
  private int mFillResIconDpi;
  
  private int mIconBitmapSize;
  
  private final float[] mLeftBorder;
  
  private final int mMaxSize;
  
  private final Rect mOldBounds = new Rect();
  
  private final byte[] mPixels;
  
  private PackageManager mPm;
  
  private final float[] mRightBorder;
  
  private final Canvas mScaleCheckCanvas;
  
  private int mWrapperBackgroundColor;
  
  private Drawable mWrapperIcon;
  
  @Deprecated
  public static SimpleIconFactory obtain(Context paramContext) {
    SimpleIconFactory simpleIconFactory1 = sPool.acquire();
    SimpleIconFactory simpleIconFactory2 = simpleIconFactory1;
    if (simpleIconFactory1 == null) {
      int i;
      ActivityManager activityManager = (ActivityManager)paramContext.getSystemService("activity");
      if (activityManager == null) {
        i = 0;
      } else {
        i = activityManager.getLauncherLargeIconDensity();
      } 
      int j = getIconSizeFromContext(paramContext);
      int k = getBadgeSizeFromContext(paramContext);
      simpleIconFactory2 = new SimpleIconFactory(paramContext, i, j, k);
      simpleIconFactory2.setWrapperBackgroundColor(-1);
    } 
    return simpleIconFactory2;
  }
  
  private static int getAttrDimFromContext(Context paramContext, int paramInt, String paramString) {
    Resources resources = paramContext.getResources();
    TypedValue typedValue = new TypedValue();
    if (paramContext.getTheme().resolveAttribute(paramInt, typedValue, true))
      return resources.getDimensionPixelSize(typedValue.resourceId); 
    throw new IllegalStateException(paramString);
  }
  
  private static int getIconSizeFromContext(Context paramContext) {
    return getAttrDimFromContext(paramContext, 17956951, "Expected theme to define iconfactoryIconSize.");
  }
  
  private static int getBadgeSizeFromContext(Context paramContext) {
    return getAttrDimFromContext(paramContext, 17956950, "Expected theme to define iconfactoryBadgeSize.");
  }
  
  @Deprecated
  public void recycle() {
    setWrapperBackgroundColor(-1);
    sPool.release(this);
  }
  
  @Deprecated
  void setWrapperBackgroundColor(int paramInt) {
    if (Color.alpha(paramInt) < 255)
      paramInt = -1; 
    this.mWrapperBackgroundColor = paramInt;
  }
  
  @Deprecated
  Bitmap createUserBadgedIconBitmap(Drawable paramDrawable, UserHandle paramUserHandle) {
    float[] arrayOfFloat = new float[1];
    Drawable drawable = paramDrawable;
    if (paramDrawable == null)
      drawable = getFullResDefaultActivityIcon(this.mFillResIconDpi); 
    drawable = normalizeAndWrapToAdaptiveIcon(drawable, null, arrayOfFloat);
    Bitmap bitmap = createIconBitmap(drawable, arrayOfFloat[0]);
    if (drawable instanceof AdaptiveIconDrawable) {
      this.mCanvas.setBitmap(bitmap);
      recreateIcon(Bitmap.createBitmap(bitmap), this.mCanvas);
      this.mCanvas.setBitmap(null);
    } 
    if (paramUserHandle != null) {
      FixedSizeBitmapDrawable fixedSizeBitmapDrawable = new FixedSizeBitmapDrawable(bitmap);
      Drawable drawable1 = this.mPm.getUserBadgedIcon((Drawable)fixedSizeBitmapDrawable, paramUserHandle);
      if (drawable1 instanceof BitmapDrawable) {
        bitmap = ((BitmapDrawable)drawable1).getBitmap();
      } else {
        bitmap = createIconBitmap((Drawable)bitmap, 1.0F);
      } 
    } 
    return bitmap;
  }
  
  @Deprecated
  public Bitmap createAppBadgedIconBitmap(Drawable paramDrawable, Bitmap paramBitmap) {
    Drawable drawable = paramDrawable;
    if (paramDrawable == null)
      drawable = getFullResDefaultActivityIcon(this.mFillResIconDpi); 
    int i = drawable.getIntrinsicWidth();
    int j = drawable.getIntrinsicHeight();
    float f1 = 1.0F;
    if (j > i && i > 0) {
      f2 = j / i;
    } else {
      f2 = f1;
      if (i > j) {
        f2 = f1;
        if (j > 0)
          f2 = i / j; 
      } 
    } 
    Bitmap bitmap2 = createIconBitmapNoInsetOrMask(drawable, f2);
    bitmap2 = maskBitmapToCircle(bitmap2);
    BitmapDrawable bitmapDrawable = new BitmapDrawable(this.mContext.getResources(), bitmap2);
    float f2 = getScale((Drawable)bitmapDrawable, null);
    Bitmap bitmap1 = createIconBitmap((Drawable)bitmapDrawable, f2);
    this.mCanvas.setBitmap(bitmap1);
    recreateIcon(Bitmap.createBitmap(bitmap1), this.mCanvas);
    if (paramBitmap != null) {
      i = this.mBadgeBitmapSize;
      paramBitmap = Bitmap.createScaledBitmap(paramBitmap, i, i, false);
      Canvas canvas = this.mCanvas;
      i = this.mIconBitmapSize;
      j = this.mBadgeBitmapSize;
      canvas.drawBitmap(paramBitmap, (i - j), (i - j), null);
    } 
    this.mCanvas.setBitmap(null);
    return bitmap1;
  }
  
  private Bitmap maskBitmapToCircle(Bitmap paramBitmap) {
    int i = paramBitmap.getWidth();
    int j = paramBitmap.getHeight();
    Bitmap.Config config = Bitmap.Config.ARGB_8888;
    Bitmap bitmap = Bitmap.createBitmap(i, j, config);
    Canvas canvas = new Canvas(bitmap);
    Paint paint = new Paint(7);
    i = paramBitmap.getWidth();
    i = Math.max((int)Math.ceil((i * 0.010416667F)), 1);
    paint.setColor(-1);
    canvas.drawARGB(0, 0, 0, 0);
    float f1 = paramBitmap.getWidth() / 2.0F;
    float f2 = paramBitmap.getHeight() / 2.0F;
    float f3 = paramBitmap.getWidth() / 2.0F, f4 = i;
    canvas.drawCircle(f1, f2, f3 - f4, paint);
    paint.setXfermode((Xfermode)new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
    Rect rect = new Rect(0, 0, paramBitmap.getWidth(), paramBitmap.getHeight());
    canvas.drawBitmap(paramBitmap, rect, rect, paint);
    return bitmap;
  }
  
  private static Drawable getFullResDefaultActivityIcon(int paramInt) {
    return Resources.getSystem().getDrawableForDensity(17629184, paramInt);
  }
  
  private Bitmap createIconBitmap(Drawable paramDrawable, float paramFloat) {
    return createIconBitmap(paramDrawable, paramFloat, this.mIconBitmapSize, true, false);
  }
  
  private Bitmap createIconBitmapNoInsetOrMask(Drawable paramDrawable, float paramFloat) {
    return createIconBitmap(paramDrawable, paramFloat, this.mIconBitmapSize, false, true);
  }
  
  private Bitmap createIconBitmap(Drawable paramDrawable, float paramFloat, int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    Bitmap bitmap = Bitmap.createBitmap(paramInt, paramInt, Bitmap.Config.ARGB_8888);
    this.mCanvas.setBitmap(bitmap);
    this.mOldBounds.set(paramDrawable.getBounds());
    if (paramDrawable instanceof AdaptiveIconDrawable) {
      AdaptiveIconDrawable adaptiveIconDrawable = (AdaptiveIconDrawable)paramDrawable;
      int i = Math.round(paramInt * (1.0F - paramFloat) / 2.0F);
      int j = i;
      if (paramBoolean1)
        j = Math.max((int)Math.ceil((paramInt * 0.010416667F)), i); 
      Rect rect = new Rect(j, j, paramInt - j, paramInt - j);
      if (paramBoolean2) {
        paramInt = rect.width() / 2;
        j = rect.height() / 2;
        paramFloat = 1.0F / (AdaptiveIconDrawable.getExtraInsetFraction() * 2.0F + 1.0F);
        i = (int)(rect.width() / paramFloat * 2.0F);
        int k = (int)(rect.height() / 2.0F * paramFloat);
        rect = new Rect(paramInt - i, j - k, paramInt + i, j + k);
        Optional.<Drawable>ofNullable(adaptiveIconDrawable.getBackground()).ifPresent(new _$$Lambda$SimpleIconFactory$fsx5rlUhrXI5RqytDEKaAnjCy8Q(this, rect));
        Optional.<Drawable>ofNullable(adaptiveIconDrawable.getForeground()).ifPresent(new _$$Lambda$SimpleIconFactory$PQ2vsLf_JFTvrQBtBGoNtaRuPYY(this, rect));
      } else {
        adaptiveIconDrawable.setBounds(rect);
        adaptiveIconDrawable.draw(this.mCanvas);
      } 
    } else {
      if (paramDrawable instanceof BitmapDrawable) {
        BitmapDrawable bitmapDrawable = (BitmapDrawable)paramDrawable;
        Bitmap bitmap1 = bitmapDrawable.getBitmap();
        if (bitmap != null && bitmap1.getDensity() == 0)
          bitmapDrawable.setTargetDensity(this.mContext.getResources().getDisplayMetrics()); 
      } 
      int m = paramInt;
      int k = paramInt;
      int n = paramDrawable.getIntrinsicWidth();
      int i1 = paramDrawable.getIntrinsicHeight();
      int i = m, j = k;
      if (n > 0) {
        i = m;
        j = k;
        if (i1 > 0) {
          float f = n / i1;
          if (n > i1) {
            j = (int)(m / f);
            i = m;
          } else {
            i = m;
            j = k;
            if (i1 > n) {
              i = (int)(k * f);
              j = k;
            } 
          } 
        } 
      } 
      m = (paramInt - i) / 2;
      k = (paramInt - j) / 2;
      paramDrawable.setBounds(m, k, m + i, k + j);
      this.mCanvas.save();
      this.mCanvas.scale(paramFloat, paramFloat, (paramInt / 2), (paramInt / 2));
      paramDrawable.draw(this.mCanvas);
      this.mCanvas.restore();
    } 
    paramDrawable.setBounds(this.mOldBounds);
    this.mCanvas.setBitmap(null);
    return bitmap;
  }
  
  private Drawable normalizeAndWrapToAdaptiveIcon(Drawable paramDrawable, RectF paramRectF, float[] paramArrayOffloat) {
    AdaptiveIconDrawable adaptiveIconDrawable1;
    if (this.mWrapperIcon == null) {
      Drawable drawable1 = this.mContext.getDrawable(17302895);
      this.mWrapperIcon = drawable1.mutate();
    } 
    AdaptiveIconDrawable adaptiveIconDrawable2 = (AdaptiveIconDrawable)this.mWrapperIcon;
    adaptiveIconDrawable2.setBounds(0, 0, 1, 1);
    float f1 = getScale(paramDrawable, paramRectF);
    float f2 = f1;
    Drawable drawable = paramDrawable;
    if (!(paramDrawable instanceof AdaptiveIconDrawable)) {
      FixedScaleDrawable fixedScaleDrawable = (FixedScaleDrawable)adaptiveIconDrawable2.getForeground();
      fixedScaleDrawable.setDrawable(paramDrawable);
      fixedScaleDrawable.setScale(f1);
      adaptiveIconDrawable1 = adaptiveIconDrawable2;
      f2 = getScale((Drawable)adaptiveIconDrawable1, paramRectF);
      ((ColorDrawable)adaptiveIconDrawable2.getBackground()).setColor(this.mWrapperBackgroundColor);
    } 
    paramArrayOffloat[0] = f2;
    return (Drawable)adaptiveIconDrawable1;
  }
  
  private float getScale(Drawable paramDrawable, RectF paramRectF) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_1
    //   3: instanceof android/graphics/drawable/AdaptiveIconDrawable
    //   6: ifeq -> 39
    //   9: aload_0
    //   10: getfield mAdaptiveIconScale : F
    //   13: fconst_0
    //   14: fcmpl
    //   15: ifeq -> 39
    //   18: aload_2
    //   19: ifnull -> 30
    //   22: aload_2
    //   23: aload_0
    //   24: getfield mAdaptiveIconBounds : Landroid/graphics/Rect;
    //   27: invokevirtual set : (Landroid/graphics/Rect;)V
    //   30: aload_0
    //   31: getfield mAdaptiveIconScale : F
    //   34: fstore_3
    //   35: aload_0
    //   36: monitorexit
    //   37: fload_3
    //   38: freturn
    //   39: aload_1
    //   40: invokevirtual getIntrinsicWidth : ()I
    //   43: istore #4
    //   45: aload_1
    //   46: invokevirtual getIntrinsicHeight : ()I
    //   49: istore #5
    //   51: iload #4
    //   53: ifle -> 126
    //   56: iload #5
    //   58: ifgt -> 64
    //   61: goto -> 126
    //   64: iload #4
    //   66: aload_0
    //   67: getfield mMaxSize : I
    //   70: if_icmpgt -> 90
    //   73: iload #4
    //   75: istore #6
    //   77: iload #5
    //   79: istore #7
    //   81: iload #5
    //   83: aload_0
    //   84: getfield mMaxSize : I
    //   87: if_icmple -> 190
    //   90: iload #4
    //   92: iload #5
    //   94: invokestatic max : (II)I
    //   97: istore #7
    //   99: aload_0
    //   100: getfield mMaxSize : I
    //   103: iload #4
    //   105: imul
    //   106: iload #7
    //   108: idiv
    //   109: istore #6
    //   111: aload_0
    //   112: getfield mMaxSize : I
    //   115: iload #5
    //   117: imul
    //   118: iload #7
    //   120: idiv
    //   121: istore #7
    //   123: goto -> 190
    //   126: iload #4
    //   128: ifle -> 146
    //   131: iload #4
    //   133: aload_0
    //   134: getfield mMaxSize : I
    //   137: if_icmple -> 143
    //   140: goto -> 146
    //   143: goto -> 152
    //   146: aload_0
    //   147: getfield mMaxSize : I
    //   150: istore #4
    //   152: iload #4
    //   154: istore #6
    //   156: iload #5
    //   158: ifle -> 180
    //   161: iload #5
    //   163: aload_0
    //   164: getfield mMaxSize : I
    //   167: if_icmple -> 173
    //   170: goto -> 180
    //   173: iload #5
    //   175: istore #4
    //   177: goto -> 186
    //   180: aload_0
    //   181: getfield mMaxSize : I
    //   184: istore #4
    //   186: iload #4
    //   188: istore #7
    //   190: aload_0
    //   191: getfield mBitmap : Landroid/graphics/Bitmap;
    //   194: iconst_0
    //   195: invokevirtual eraseColor : (I)V
    //   198: aload_1
    //   199: iconst_0
    //   200: iconst_0
    //   201: iload #6
    //   203: iload #7
    //   205: invokevirtual setBounds : (IIII)V
    //   208: aload_1
    //   209: aload_0
    //   210: getfield mScaleCheckCanvas : Landroid/graphics/Canvas;
    //   213: invokevirtual draw : (Landroid/graphics/Canvas;)V
    //   216: aload_0
    //   217: getfield mPixels : [B
    //   220: invokestatic wrap : ([B)Ljava/nio/ByteBuffer;
    //   223: astore #8
    //   225: aload #8
    //   227: invokevirtual rewind : ()Ljava/nio/Buffer;
    //   230: pop
    //   231: aload_0
    //   232: getfield mBitmap : Landroid/graphics/Bitmap;
    //   235: aload #8
    //   237: invokevirtual copyPixelsToBuffer : (Ljava/nio/Buffer;)V
    //   240: iconst_m1
    //   241: istore #9
    //   243: iconst_m1
    //   244: istore #10
    //   246: aload_0
    //   247: getfield mMaxSize : I
    //   250: iconst_1
    //   251: iadd
    //   252: istore #11
    //   254: iconst_m1
    //   255: istore #12
    //   257: iconst_0
    //   258: istore #13
    //   260: aload_0
    //   261: getfield mMaxSize : I
    //   264: istore #14
    //   266: iconst_0
    //   267: istore #4
    //   269: iload #4
    //   271: iload #7
    //   273: if_icmpge -> 465
    //   276: iconst_m1
    //   277: istore #15
    //   279: iconst_m1
    //   280: istore #16
    //   282: iconst_0
    //   283: istore #5
    //   285: iload #13
    //   287: istore #17
    //   289: iload #16
    //   291: istore #13
    //   293: iload #5
    //   295: iload #6
    //   297: if_icmpge -> 359
    //   300: iload #13
    //   302: istore #16
    //   304: aload_0
    //   305: getfield mPixels : [B
    //   308: iload #17
    //   310: baload
    //   311: sipush #255
    //   314: iand
    //   315: bipush #40
    //   317: if_icmple -> 346
    //   320: iload #13
    //   322: istore #15
    //   324: iload #13
    //   326: iconst_m1
    //   327: if_icmpne -> 334
    //   330: iload #5
    //   332: istore #15
    //   334: iload #5
    //   336: istore #13
    //   338: iload #15
    //   340: istore #16
    //   342: iload #13
    //   344: istore #15
    //   346: iinc #17, 1
    //   349: iinc #5, 1
    //   352: iload #16
    //   354: istore #13
    //   356: goto -> 293
    //   359: iload #17
    //   361: iload #14
    //   363: iload #6
    //   365: isub
    //   366: iadd
    //   367: istore #18
    //   369: aload_0
    //   370: getfield mLeftBorder : [F
    //   373: iload #4
    //   375: iload #13
    //   377: i2f
    //   378: fastore
    //   379: aload_0
    //   380: getfield mRightBorder : [F
    //   383: iload #4
    //   385: iload #15
    //   387: i2f
    //   388: fastore
    //   389: iload #9
    //   391: istore #5
    //   393: iload #11
    //   395: istore #16
    //   397: iload #12
    //   399: istore #17
    //   401: iload #13
    //   403: iconst_m1
    //   404: if_icmpeq -> 443
    //   407: iload #9
    //   409: istore #5
    //   411: iload #9
    //   413: iconst_m1
    //   414: if_icmpne -> 421
    //   417: iload #4
    //   419: istore #5
    //   421: iload #11
    //   423: iload #13
    //   425: invokestatic min : (II)I
    //   428: istore #16
    //   430: iload #12
    //   432: iload #15
    //   434: invokestatic max : (II)I
    //   437: istore #17
    //   439: iload #4
    //   441: istore #10
    //   443: iinc #4, 1
    //   446: iload #5
    //   448: istore #9
    //   450: iload #16
    //   452: istore #11
    //   454: iload #17
    //   456: istore #12
    //   458: iload #18
    //   460: istore #13
    //   462: goto -> 269
    //   465: iload #9
    //   467: iconst_m1
    //   468: if_icmpeq -> 775
    //   471: iload #12
    //   473: iconst_m1
    //   474: if_icmpne -> 480
    //   477: goto -> 775
    //   480: aload_0
    //   481: getfield mLeftBorder : [F
    //   484: iconst_1
    //   485: iload #9
    //   487: iload #10
    //   489: invokestatic convertToConvexArray : ([FIII)V
    //   492: aload_0
    //   493: getfield mRightBorder : [F
    //   496: iconst_m1
    //   497: iload #9
    //   499: iload #10
    //   501: invokestatic convertToConvexArray : ([FIII)V
    //   504: fconst_0
    //   505: fstore_3
    //   506: iconst_0
    //   507: istore #4
    //   509: iload #4
    //   511: iload #7
    //   513: if_icmpge -> 558
    //   516: aload_0
    //   517: getfield mLeftBorder : [F
    //   520: iload #4
    //   522: faload
    //   523: ldc -1.0
    //   525: fcmpg
    //   526: ifgt -> 532
    //   529: goto -> 552
    //   532: fload_3
    //   533: aload_0
    //   534: getfield mRightBorder : [F
    //   537: iload #4
    //   539: faload
    //   540: aload_0
    //   541: getfield mLeftBorder : [F
    //   544: iload #4
    //   546: faload
    //   547: fsub
    //   548: fconst_1
    //   549: fadd
    //   550: fadd
    //   551: fstore_3
    //   552: iinc #4, 1
    //   555: goto -> 509
    //   558: iload #10
    //   560: iconst_1
    //   561: iadd
    //   562: iload #9
    //   564: isub
    //   565: iload #12
    //   567: iconst_1
    //   568: iadd
    //   569: iload #11
    //   571: isub
    //   572: imul
    //   573: i2f
    //   574: fstore #19
    //   576: fload_3
    //   577: fload #19
    //   579: fdiv
    //   580: fstore #19
    //   582: fload #19
    //   584: ldc 0.7853982
    //   586: fcmpg
    //   587: ifge -> 597
    //   590: ldc 0.6597222
    //   592: fstore #19
    //   594: goto -> 609
    //   597: fconst_1
    //   598: fload #19
    //   600: fsub
    //   601: ldc 0.040449437
    //   603: fmul
    //   604: ldc 0.6510417
    //   606: fadd
    //   607: fstore #19
    //   609: aload_0
    //   610: getfield mBounds : Landroid/graphics/Rect;
    //   613: iload #11
    //   615: putfield left : I
    //   618: aload_0
    //   619: getfield mBounds : Landroid/graphics/Rect;
    //   622: iload #12
    //   624: putfield right : I
    //   627: aload_0
    //   628: getfield mBounds : Landroid/graphics/Rect;
    //   631: iload #9
    //   633: putfield top : I
    //   636: aload_0
    //   637: getfield mBounds : Landroid/graphics/Rect;
    //   640: iload #10
    //   642: putfield bottom : I
    //   645: aload_2
    //   646: ifnull -> 708
    //   649: aload_2
    //   650: aload_0
    //   651: getfield mBounds : Landroid/graphics/Rect;
    //   654: getfield left : I
    //   657: i2f
    //   658: iload #6
    //   660: i2f
    //   661: fdiv
    //   662: aload_0
    //   663: getfield mBounds : Landroid/graphics/Rect;
    //   666: getfield top : I
    //   669: i2f
    //   670: iload #7
    //   672: i2f
    //   673: fdiv
    //   674: fconst_1
    //   675: aload_0
    //   676: getfield mBounds : Landroid/graphics/Rect;
    //   679: getfield right : I
    //   682: i2f
    //   683: iload #6
    //   685: i2f
    //   686: fdiv
    //   687: fsub
    //   688: fconst_1
    //   689: aload_0
    //   690: getfield mBounds : Landroid/graphics/Rect;
    //   693: getfield bottom : I
    //   696: i2f
    //   697: iload #7
    //   699: i2f
    //   700: fdiv
    //   701: fsub
    //   702: invokevirtual set : (FFFF)V
    //   705: goto -> 708
    //   708: fload_3
    //   709: iload #6
    //   711: iload #7
    //   713: imul
    //   714: i2f
    //   715: fdiv
    //   716: fstore_3
    //   717: fload_3
    //   718: fload #19
    //   720: fcmpl
    //   721: ifle -> 737
    //   724: fload #19
    //   726: fload_3
    //   727: fdiv
    //   728: f2d
    //   729: invokestatic sqrt : (D)D
    //   732: d2f
    //   733: fstore_3
    //   734: goto -> 739
    //   737: fconst_1
    //   738: fstore_3
    //   739: aload_1
    //   740: instanceof android/graphics/drawable/AdaptiveIconDrawable
    //   743: ifeq -> 771
    //   746: aload_0
    //   747: getfield mAdaptiveIconScale : F
    //   750: fconst_0
    //   751: fcmpl
    //   752: ifne -> 771
    //   755: aload_0
    //   756: fload_3
    //   757: putfield mAdaptiveIconScale : F
    //   760: aload_0
    //   761: getfield mAdaptiveIconBounds : Landroid/graphics/Rect;
    //   764: aload_0
    //   765: getfield mBounds : Landroid/graphics/Rect;
    //   768: invokevirtual set : (Landroid/graphics/Rect;)V
    //   771: aload_0
    //   772: monitorexit
    //   773: fload_3
    //   774: freturn
    //   775: aload_0
    //   776: monitorexit
    //   777: fconst_1
    //   778: freturn
    //   779: astore_1
    //   780: aload_0
    //   781: monitorexit
    //   782: aload_1
    //   783: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #476	-> 2
    //   #477	-> 9
    //   #478	-> 18
    //   #479	-> 22
    //   #481	-> 30
    //   #484	-> 39
    //   #485	-> 45
    //   #486	-> 51
    //   #489	-> 64
    //   #490	-> 90
    //   #491	-> 99
    //   #492	-> 111
    //   #487	-> 126
    //   #488	-> 156
    //   #495	-> 190
    //   #496	-> 198
    //   #497	-> 208
    //   #499	-> 216
    //   #500	-> 225
    //   #501	-> 231
    //   #504	-> 240
    //   #505	-> 243
    //   #506	-> 246
    //   #507	-> 254
    //   #514	-> 257
    //   #516	-> 260
    //   #520	-> 266
    //   #521	-> 276
    //   #522	-> 276
    //   #523	-> 300
    //   #524	-> 320
    //   #525	-> 330
    //   #527	-> 334
    //   #529	-> 346
    //   #522	-> 349
    //   #531	-> 359
    //   #533	-> 369
    //   #534	-> 379
    //   #537	-> 389
    //   #538	-> 407
    //   #539	-> 407
    //   #540	-> 417
    //   #543	-> 421
    //   #544	-> 430
    //   #520	-> 443
    //   #548	-> 465
    //   #553	-> 480
    //   #554	-> 492
    //   #557	-> 504
    //   #558	-> 506
    //   #559	-> 516
    //   #560	-> 529
    //   #562	-> 532
    //   #558	-> 552
    //   #566	-> 558
    //   #567	-> 576
    //   #570	-> 582
    //   #571	-> 590
    //   #573	-> 597
    //   #575	-> 609
    //   #576	-> 618
    //   #578	-> 627
    //   #579	-> 636
    //   #581	-> 645
    //   #582	-> 649
    //   #581	-> 708
    //   #586	-> 708
    //   #588	-> 717
    //   #589	-> 739
    //   #590	-> 755
    //   #591	-> 760
    //   #593	-> 771
    //   #548	-> 775
    //   #550	-> 775
    //   #475	-> 779
    // Exception table:
    //   from	to	target	type
    //   2	9	779	finally
    //   9	18	779	finally
    //   22	30	779	finally
    //   30	35	779	finally
    //   39	45	779	finally
    //   45	51	779	finally
    //   64	73	779	finally
    //   81	90	779	finally
    //   90	99	779	finally
    //   99	111	779	finally
    //   111	123	779	finally
    //   131	140	779	finally
    //   146	152	779	finally
    //   161	170	779	finally
    //   180	186	779	finally
    //   190	198	779	finally
    //   198	208	779	finally
    //   208	216	779	finally
    //   216	225	779	finally
    //   225	231	779	finally
    //   231	240	779	finally
    //   246	254	779	finally
    //   260	266	779	finally
    //   304	320	779	finally
    //   369	379	779	finally
    //   379	389	779	finally
    //   421	430	779	finally
    //   430	439	779	finally
    //   480	492	779	finally
    //   492	504	779	finally
    //   516	529	779	finally
    //   532	552	779	finally
    //   609	618	779	finally
    //   618	627	779	finally
    //   627	636	779	finally
    //   636	645	779	finally
    //   649	705	779	finally
    //   724	734	779	finally
    //   739	755	779	finally
    //   755	760	779	finally
    //   760	771	779	finally
  }
  
  private static void convertToConvexArray(float[] paramArrayOffloat, int paramInt1, int paramInt2, int paramInt3) {
    int i = paramArrayOffloat.length;
    float[] arrayOfFloat = new float[i - 1];
    i = -1;
    float f = Float.MAX_VALUE;
    for (int j = paramInt2 + 1; j <= paramInt3; j++) {
      if (paramArrayOffloat[j] > -1.0F) {
        if (f == Float.MAX_VALUE) {
          i = paramInt2;
        } else {
          float f1 = (paramArrayOffloat[j] - paramArrayOffloat[i]) / (j - i);
          if ((f1 - f) * paramInt1 < 0.0F) {
            int m = i;
            while (true) {
              i = m;
              if (m > paramInt2) {
                i = m - 1;
                f = (paramArrayOffloat[j] - paramArrayOffloat[i]) / (j - i);
                m = i;
                if ((f - arrayOfFloat[i]) * paramInt1 >= 0.0F)
                  break; 
                continue;
              } 
              break;
            } 
          } 
        } 
        f = (paramArrayOffloat[j] - paramArrayOffloat[i]) / (j - i);
        for (int k = i; k < j; k++) {
          arrayOfFloat[k] = f;
          paramArrayOffloat[k] = paramArrayOffloat[i] + (k - i) * f;
        } 
        i = j;
      } 
    } 
  }
  
  @Deprecated
  private SimpleIconFactory(Context paramContext, int paramInt1, int paramInt2, int paramInt3) {
    this.mBlurPaint = new Paint(3);
    this.mDrawPaint = new Paint(3);
    this.mContext = paramContext = paramContext.getApplicationContext();
    this.mPm = paramContext.getPackageManager();
    this.mIconBitmapSize = paramInt2;
    this.mBadgeBitmapSize = paramInt3;
    this.mFillResIconDpi = paramInt1;
    Canvas canvas = new Canvas();
    canvas.setDrawFilter((DrawFilter)new PaintFlagsDrawFilter(4, 2));
    this.mMaxSize = paramInt1 = paramInt2 * 2;
    this.mBitmap = Bitmap.createBitmap(paramInt1, paramInt1, Bitmap.Config.ALPHA_8);
    this.mScaleCheckCanvas = new Canvas(this.mBitmap);
    paramInt1 = this.mMaxSize;
    this.mPixels = new byte[paramInt1 * paramInt1];
    this.mLeftBorder = new float[paramInt1];
    this.mRightBorder = new float[paramInt1];
    this.mBounds = new Rect();
    this.mAdaptiveIconBounds = new Rect();
    this.mAdaptiveIconScale = 0.0F;
    this.mDefaultBlurMaskFilter = new BlurMaskFilter(paramInt2 * 0.010416667F, BlurMaskFilter.Blur.NORMAL);
  }
  
  private void recreateIcon(Bitmap paramBitmap, Canvas paramCanvas) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: aload_0
    //   5: getfield mDefaultBlurMaskFilter : Landroid/graphics/BlurMaskFilter;
    //   8: bipush #30
    //   10: bipush #61
    //   12: aload_2
    //   13: invokespecial recreateIcon : (Landroid/graphics/Bitmap;Landroid/graphics/BlurMaskFilter;IILandroid/graphics/Canvas;)V
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: astore_1
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_1
    //   23: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #661	-> 2
    //   #662	-> 16
    //   #660	-> 19
    // Exception table:
    //   from	to	target	type
    //   2	16	19	finally
  }
  
  private void recreateIcon(Bitmap paramBitmap, BlurMaskFilter paramBlurMaskFilter, int paramInt1, int paramInt2, Canvas paramCanvas) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: iconst_2
    //   3: newarray int
    //   5: astore #6
    //   7: aload_0
    //   8: getfield mBlurPaint : Landroid/graphics/Paint;
    //   11: aload_2
    //   12: invokevirtual setMaskFilter : (Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;
    //   15: pop
    //   16: aload_1
    //   17: aload_0
    //   18: getfield mBlurPaint : Landroid/graphics/Paint;
    //   21: aload #6
    //   23: invokevirtual extractAlpha : (Landroid/graphics/Paint;[I)Landroid/graphics/Bitmap;
    //   26: astore_2
    //   27: aload_0
    //   28: getfield mDrawPaint : Landroid/graphics/Paint;
    //   31: iload_3
    //   32: invokevirtual setAlpha : (I)V
    //   35: aload #5
    //   37: aload_2
    //   38: aload #6
    //   40: iconst_0
    //   41: iaload
    //   42: i2f
    //   43: aload #6
    //   45: iconst_1
    //   46: iaload
    //   47: i2f
    //   48: aload_0
    //   49: getfield mDrawPaint : Landroid/graphics/Paint;
    //   52: invokevirtual drawBitmap : (Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
    //   55: aload_0
    //   56: getfield mDrawPaint : Landroid/graphics/Paint;
    //   59: iload #4
    //   61: invokevirtual setAlpha : (I)V
    //   64: aload #5
    //   66: aload_2
    //   67: aload #6
    //   69: iconst_0
    //   70: iaload
    //   71: i2f
    //   72: aload #6
    //   74: iconst_1
    //   75: iaload
    //   76: i2f
    //   77: aload_0
    //   78: getfield mIconBitmapSize : I
    //   81: i2f
    //   82: ldc 0.020833334
    //   84: fmul
    //   85: fadd
    //   86: aload_0
    //   87: getfield mDrawPaint : Landroid/graphics/Paint;
    //   90: invokevirtual drawBitmap : (Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
    //   93: aload_0
    //   94: getfield mDrawPaint : Landroid/graphics/Paint;
    //   97: sipush #255
    //   100: invokevirtual setAlpha : (I)V
    //   103: aload #5
    //   105: aload_1
    //   106: fconst_0
    //   107: fconst_0
    //   108: aload_0
    //   109: getfield mDrawPaint : Landroid/graphics/Paint;
    //   112: invokevirtual drawBitmap : (Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
    //   115: aload_0
    //   116: monitorexit
    //   117: return
    //   118: astore_1
    //   119: aload_0
    //   120: monitorexit
    //   121: aload_1
    //   122: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #666	-> 2
    //   #667	-> 7
    //   #668	-> 16
    //   #671	-> 27
    //   #672	-> 35
    //   #675	-> 55
    //   #676	-> 64
    //   #680	-> 93
    //   #681	-> 103
    //   #682	-> 115
    //   #665	-> 118
    // Exception table:
    //   from	to	target	type
    //   2	7	118	finally
    //   7	16	118	finally
    //   16	27	118	finally
    //   27	35	118	finally
    //   35	55	118	finally
    //   55	64	118	finally
    //   64	93	118	finally
    //   93	103	118	finally
    //   103	115	118	finally
  }
  
  class FixedScaleDrawable extends DrawableWrapper {
    private static final float LEGACY_ICON_SCALE = 0.46669F;
    
    private float mScaleX;
    
    private float mScaleY;
    
    public FixedScaleDrawable() {
      super((Drawable)new ColorDrawable());
      this.mScaleX = 0.46669F;
      this.mScaleY = 0.46669F;
    }
    
    public void draw(Canvas param1Canvas) {
      int i = param1Canvas.save();
      float f1 = this.mScaleX, f2 = this.mScaleY;
      float f3 = getBounds().exactCenterX(), f4 = getBounds().exactCenterY();
      param1Canvas.scale(f1, f2, f3, f4);
      super.draw(param1Canvas);
      param1Canvas.restoreToCount(i);
    }
    
    public void inflate(Resources param1Resources, XmlPullParser param1XmlPullParser, AttributeSet param1AttributeSet) {}
    
    public void inflate(Resources param1Resources, XmlPullParser param1XmlPullParser, AttributeSet param1AttributeSet, Resources.Theme param1Theme) {}
    
    public void setScale(float param1Float) {
      float f1 = getIntrinsicHeight();
      float f2 = getIntrinsicWidth();
      float f3 = param1Float * 0.46669F;
      this.mScaleY = 0.46669F * param1Float;
      if (f1 > f2 && f2 > 0.0F) {
        this.mScaleX = f3 * f2 / f1;
      } else if (f2 > f1 && f1 > 0.0F) {
        this.mScaleY *= f1 / f2;
      } 
    }
  }
  
  class FixedSizeBitmapDrawable extends BitmapDrawable {
    FixedSizeBitmapDrawable(SimpleIconFactory this$0) {
      super(null, (Bitmap)this$0);
    }
    
    public int getIntrinsicHeight() {
      return getBitmap().getWidth();
    }
    
    public int getIntrinsicWidth() {
      return getBitmap().getWidth();
    }
  }
}
