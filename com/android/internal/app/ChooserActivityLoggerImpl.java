package com.android.internal.app;

import com.android.internal.logging.InstanceId;
import com.android.internal.logging.InstanceIdSequence;
import com.android.internal.logging.UiEventLogger;
import com.android.internal.logging.UiEventLoggerImpl;
import com.android.internal.util.FrameworkStatsLog;

public class ChooserActivityLoggerImpl implements ChooserActivityLogger {
  private static final int SHARESHEET_INSTANCE_ID_MAX = 8192;
  
  private static InstanceIdSequence sInstanceIdSequence;
  
  private InstanceId mInstanceId;
  
  private UiEventLogger mUiEventLogger = new UiEventLoggerImpl();
  
  public void logShareStarted(int paramInt1, String paramString1, String paramString2, int paramInt2, int paramInt3, boolean paramBoolean, int paramInt4, String paramString3) {
    ChooserActivityLogger.SharesheetStartedEvent sharesheetStartedEvent = ChooserActivityLogger.SharesheetStartedEvent.SHARE_STARTED;
    paramInt1 = sharesheetStartedEvent.getId();
    int i = getInstanceId().getId();
    paramInt4 = typeFromPreviewInt(paramInt4);
    int j = typeFromIntentString(paramString3);
    FrameworkStatsLog.write(259, paramInt1, paramString1, i, paramString2, paramInt2, paramInt3, paramBoolean, paramInt4, j);
  }
  
  public void logShareTargetSelected(int paramInt1, String paramString, int paramInt2) {
    int i = ChooserActivityLogger.SharesheetTargetSelectedEvent.fromTargetType(paramInt1).getId();
    paramInt1 = getInstanceId().getId();
    FrameworkStatsLog.write(260, i, paramString, paramInt1, paramInt2);
  }
  
  public void log(UiEventLogger.UiEventEnum paramUiEventEnum, InstanceId paramInstanceId) {
    this.mUiEventLogger.logWithInstanceId(paramUiEventEnum, 0, null, paramInstanceId);
  }
  
  public InstanceId getInstanceId() {
    if (this.mInstanceId == null) {
      if (sInstanceIdSequence == null)
        sInstanceIdSequence = new InstanceIdSequence(8192); 
      this.mInstanceId = sInstanceIdSequence.newInstanceId();
    } 
    return this.mInstanceId;
  }
}
