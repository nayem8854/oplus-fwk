package com.android.internal.app;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.android.internal.widget.ViewPager;

public class ResolverViewPager extends ViewPager {
  private boolean mSwipingEnabled = true;
  
  public ResolverViewPager(Context paramContext) {
    super(paramContext);
  }
  
  public ResolverViewPager(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
  }
  
  public ResolverViewPager(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  public ResolverViewPager(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    super.onMeasure(paramInt1, paramInt2);
    if (View.MeasureSpec.getMode(paramInt2) != Integer.MIN_VALUE)
      return; 
    int i = View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824);
    int j = getMeasuredHeight();
    paramInt1 = 0;
    for (paramInt2 = 0; paramInt2 < getChildCount(); paramInt2++, paramInt1 = k) {
      View view = getChildAt(paramInt2);
      int k = View.MeasureSpec.makeMeasureSpec(j, -2147483648);
      view.measure(i, k);
      k = paramInt1;
      if (paramInt1 < view.getMeasuredHeight())
        k = view.getMeasuredHeight(); 
    } 
    paramInt2 = j;
    if (paramInt1 > 0)
      paramInt2 = paramInt1; 
    paramInt1 = View.MeasureSpec.makeMeasureSpec(paramInt2, 1073741824);
    super.onMeasure(i, paramInt1);
  }
  
  void setSwipingEnabled(boolean paramBoolean) {
    this.mSwipingEnabled = paramBoolean;
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent) {
    boolean bool;
    if (!isLayoutRtl() && this.mSwipingEnabled && super.onInterceptTouchEvent(paramMotionEvent)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
}
