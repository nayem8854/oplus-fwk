package com.android.internal.app;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ILMServiceManager extends IInterface {
  boolean enableBoost(int paramInt1, int paramInt2, int paramInt3, int paramInt4) throws RemoteException;
  
  boolean enableMobileBoost() throws RemoteException;
  
  String getLuckyMoneyInfo(int paramInt) throws RemoteException;
  
  Bundle getModeData(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  Bundle getModeEnableInfo(int paramInt1, int paramInt2) throws RemoteException;
  
  Bundle getSwitchInfo() throws RemoteException;
  
  boolean inDebugMode() throws RemoteException;
  
  boolean isInitialized() throws RemoteException;
  
  void writeDCS(Bundle paramBundle) throws RemoteException;
  
  class Default implements ILMServiceManager {
    public Bundle getModeEnableInfo(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public String getLuckyMoneyInfo(int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean enableBoost(int param1Int1, int param1Int2, int param1Int3, int param1Int4) throws RemoteException {
      return false;
    }
    
    public Bundle getModeData(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {
      return null;
    }
    
    public Bundle getSwitchInfo() throws RemoteException {
      return null;
    }
    
    public boolean enableMobileBoost() throws RemoteException {
      return false;
    }
    
    public void writeDCS(Bundle param1Bundle) throws RemoteException {}
    
    public boolean isInitialized() throws RemoteException {
      return false;
    }
    
    public boolean inDebugMode() throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ILMServiceManager {
    private static final String DESCRIPTOR = "com.android.internal.app.ILMServiceManager";
    
    static final int TRANSACTION_enableBoost = 3;
    
    static final int TRANSACTION_enableMobileBoost = 6;
    
    static final int TRANSACTION_getLuckyMoneyInfo = 2;
    
    static final int TRANSACTION_getModeData = 4;
    
    static final int TRANSACTION_getModeEnableInfo = 1;
    
    static final int TRANSACTION_getSwitchInfo = 5;
    
    static final int TRANSACTION_inDebugMode = 9;
    
    static final int TRANSACTION_isInitialized = 8;
    
    static final int TRANSACTION_writeDCS = 7;
    
    public Stub() {
      attachInterface(this, "com.android.internal.app.ILMServiceManager");
    }
    
    public static ILMServiceManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.app.ILMServiceManager");
      if (iInterface != null && iInterface instanceof ILMServiceManager)
        return (ILMServiceManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 9:
          return "inDebugMode";
        case 8:
          return "isInitialized";
        case 7:
          return "writeDCS";
        case 6:
          return "enableMobileBoost";
        case 5:
          return "getSwitchInfo";
        case 4:
          return "getModeData";
        case 3:
          return "enableBoost";
        case 2:
          return "getLuckyMoneyInfo";
        case 1:
          break;
      } 
      return "getModeEnableInfo";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool2;
        int j;
        boolean bool1;
        Bundle bundle2;
        String str;
        int k, m;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 9:
            param1Parcel1.enforceInterface("com.android.internal.app.ILMServiceManager");
            bool2 = inDebugMode();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 8:
            param1Parcel1.enforceInterface("com.android.internal.app.ILMServiceManager");
            bool2 = isInitialized();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 7:
            param1Parcel1.enforceInterface("com.android.internal.app.ILMServiceManager");
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            writeDCS((Bundle)param1Parcel1);
            param1Parcel2.writeNoException();
            if (param1Parcel1 != null) {
              param1Parcel2.writeInt(1);
              param1Parcel1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 6:
            param1Parcel1.enforceInterface("com.android.internal.app.ILMServiceManager");
            bool2 = enableMobileBoost();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 5:
            param1Parcel1.enforceInterface("com.android.internal.app.ILMServiceManager");
            bundle2 = getSwitchInfo();
            param1Parcel2.writeNoException();
            if (bundle2 != null) {
              param1Parcel2.writeInt(1);
              bundle2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 4:
            bundle2.enforceInterface("com.android.internal.app.ILMServiceManager");
            param1Int2 = bundle2.readInt();
            j = bundle2.readInt();
            k = bundle2.readInt();
            bundle2 = getModeData(param1Int2, j, k);
            param1Parcel2.writeNoException();
            if (bundle2 != null) {
              param1Parcel2.writeInt(1);
              bundle2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 3:
            bundle2.enforceInterface("com.android.internal.app.ILMServiceManager");
            m = bundle2.readInt();
            k = bundle2.readInt();
            j = bundle2.readInt();
            param1Int2 = bundle2.readInt();
            bool1 = enableBoost(m, k, j, param1Int2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 2:
            bundle2.enforceInterface("com.android.internal.app.ILMServiceManager");
            i = bundle2.readInt();
            str = getLuckyMoneyInfo(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str);
            return true;
          case 1:
            break;
        } 
        str.enforceInterface("com.android.internal.app.ILMServiceManager");
        int i = str.readInt();
        param1Int2 = str.readInt();
        Bundle bundle1 = getModeEnableInfo(i, param1Int2);
        param1Parcel2.writeNoException();
        if (bundle1 != null) {
          param1Parcel2.writeInt(1);
          bundle1.writeToParcel(param1Parcel2, 1);
        } else {
          param1Parcel2.writeInt(0);
        } 
        return true;
      } 
      param1Parcel2.writeString("com.android.internal.app.ILMServiceManager");
      return true;
    }
    
    private static class Proxy implements ILMServiceManager {
      public static ILMServiceManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.app.ILMServiceManager";
      }
      
      public Bundle getModeEnableInfo(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Bundle bundle;
          parcel1.writeInterfaceToken("com.android.internal.app.ILMServiceManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ILMServiceManager.Stub.getDefaultImpl() != null) {
            bundle = ILMServiceManager.Stub.getDefaultImpl().getModeEnableInfo(param2Int1, param2Int2);
            return bundle;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            bundle = (Bundle)Bundle.CREATOR.createFromParcel(parcel2);
          } else {
            bundle = null;
          } 
          return bundle;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getLuckyMoneyInfo(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.ILMServiceManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ILMServiceManager.Stub.getDefaultImpl() != null)
            return ILMServiceManager.Stub.getDefaultImpl().getLuckyMoneyInfo(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean enableBoost(int param2Int1, int param2Int2, int param2Int3, int param2Int4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.ILMServiceManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          parcel1.writeInt(param2Int4);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && ILMServiceManager.Stub.getDefaultImpl() != null) {
            bool1 = ILMServiceManager.Stub.getDefaultImpl().enableBoost(param2Int1, param2Int2, param2Int3, param2Int4);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bundle getModeData(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Bundle bundle;
          parcel1.writeInterfaceToken("com.android.internal.app.ILMServiceManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && ILMServiceManager.Stub.getDefaultImpl() != null) {
            bundle = ILMServiceManager.Stub.getDefaultImpl().getModeData(param2Int1, param2Int2, param2Int3);
            return bundle;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            bundle = (Bundle)Bundle.CREATOR.createFromParcel(parcel2);
          } else {
            bundle = null;
          } 
          return bundle;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bundle getSwitchInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Bundle bundle;
          parcel1.writeInterfaceToken("com.android.internal.app.ILMServiceManager");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && ILMServiceManager.Stub.getDefaultImpl() != null) {
            bundle = ILMServiceManager.Stub.getDefaultImpl().getSwitchInfo();
            return bundle;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            bundle = (Bundle)Bundle.CREATOR.createFromParcel(parcel2);
          } else {
            bundle = null;
          } 
          return bundle;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean enableMobileBoost() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.ILMServiceManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(6, parcel1, parcel2, 0);
          if (!bool2 && ILMServiceManager.Stub.getDefaultImpl() != null) {
            bool1 = ILMServiceManager.Stub.getDefaultImpl().enableMobileBoost();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void writeDCS(Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.ILMServiceManager");
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && ILMServiceManager.Stub.getDefaultImpl() != null) {
            ILMServiceManager.Stub.getDefaultImpl().writeDCS(param2Bundle);
            return;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0)
            param2Bundle.readFromParcel(parcel2); 
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isInitialized() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.ILMServiceManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(8, parcel1, parcel2, 0);
          if (!bool2 && ILMServiceManager.Stub.getDefaultImpl() != null) {
            bool1 = ILMServiceManager.Stub.getDefaultImpl().isInitialized();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean inDebugMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.ILMServiceManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(9, parcel1, parcel2, 0);
          if (!bool2 && ILMServiceManager.Stub.getDefaultImpl() != null) {
            bool1 = ILMServiceManager.Stub.getDefaultImpl().inDebugMode();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ILMServiceManager param1ILMServiceManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ILMServiceManager != null) {
          Proxy.sDefaultImpl = param1ILMServiceManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ILMServiceManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
