package com.android.internal.app;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.media.MediaRouter;
import android.util.Log;
import android.view.View;

public abstract class MediaRouteDialogPresenter {
  private static final String CHOOSER_FRAGMENT_TAG = "android.app.MediaRouteButton:MediaRouteChooserDialogFragment";
  
  private static final String CONTROLLER_FRAGMENT_TAG = "android.app.MediaRouteButton:MediaRouteControllerDialogFragment";
  
  private static final String TAG = "MediaRouter";
  
  public static DialogFragment showDialogFragment(Activity paramActivity, int paramInt, View.OnClickListener paramOnClickListener) {
    MediaRouter mediaRouter = (MediaRouter)paramActivity.getSystemService("media_router");
    FragmentManager fragmentManager = paramActivity.getFragmentManager();
    MediaRouter.RouteInfo routeInfo = mediaRouter.getSelectedRoute();
    if (routeInfo.isDefault() || !routeInfo.matchesTypes(paramInt)) {
      if (fragmentManager.findFragmentByTag("android.app.MediaRouteButton:MediaRouteChooserDialogFragment") != null) {
        Log.w("MediaRouter", "showDialog(): Route chooser dialog already showing!");
        return null;
      } 
      MediaRouteChooserDialogFragment mediaRouteChooserDialogFragment = new MediaRouteChooserDialogFragment();
      mediaRouteChooserDialogFragment.setRouteTypes(paramInt);
      mediaRouteChooserDialogFragment.setExtendedSettingsClickListener(paramOnClickListener);
      mediaRouteChooserDialogFragment.show(fragmentManager, "android.app.MediaRouteButton:MediaRouteChooserDialogFragment");
      return mediaRouteChooserDialogFragment;
    } 
    if (fragmentManager.findFragmentByTag("android.app.MediaRouteButton:MediaRouteControllerDialogFragment") != null) {
      Log.w("MediaRouter", "showDialog(): Route controller dialog already showing!");
      return null;
    } 
    MediaRouteControllerDialogFragment mediaRouteControllerDialogFragment = new MediaRouteControllerDialogFragment();
    mediaRouteControllerDialogFragment.show(fragmentManager, "android.app.MediaRouteButton:MediaRouteControllerDialogFragment");
    return mediaRouteControllerDialogFragment;
  }
  
  public static Dialog createDialog(Context paramContext, int paramInt, View.OnClickListener paramOnClickListener) {
    MediaRouteChooserDialog mediaRouteChooserDialog;
    int i;
    MediaRouter mediaRouter = (MediaRouter)paramContext.getSystemService("media_router");
    if (MediaRouteChooserDialog.isLightTheme(paramContext)) {
      i = 16974130;
    } else {
      i = 16974126;
    } 
    MediaRouter.RouteInfo routeInfo = mediaRouter.getSelectedRoute();
    if (routeInfo.isDefault() || !routeInfo.matchesTypes(paramInt)) {
      mediaRouteChooserDialog = new MediaRouteChooserDialog(paramContext, i);
      mediaRouteChooserDialog.setRouteTypes(paramInt);
      mediaRouteChooserDialog.setExtendedSettingsClickListener(paramOnClickListener);
      return mediaRouteChooserDialog;
    } 
    return (Dialog)new MediaRouteControllerDialog((Context)mediaRouteChooserDialog, i);
  }
}
