package com.android.internal.app;

import android.content.pm.PackageInfoLite;
import android.content.res.ObbInfo;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.android.internal.os.IParcelFileDescriptorFactory;

public interface IMediaContainerService extends IInterface {
  long calculateInstalledSize(String paramString1, String paramString2) throws RemoteException;
  
  int copyPackage(String paramString, IParcelFileDescriptorFactory paramIParcelFileDescriptorFactory) throws RemoteException;
  
  PackageInfoLite getMinimalPackageInfo(String paramString1, int paramInt, String paramString2) throws RemoteException;
  
  ObbInfo getObbInfo(String paramString) throws RemoteException;
  
  class Default implements IMediaContainerService {
    public int copyPackage(String param1String, IParcelFileDescriptorFactory param1IParcelFileDescriptorFactory) throws RemoteException {
      return 0;
    }
    
    public PackageInfoLite getMinimalPackageInfo(String param1String1, int param1Int, String param1String2) throws RemoteException {
      return null;
    }
    
    public ObbInfo getObbInfo(String param1String) throws RemoteException {
      return null;
    }
    
    public long calculateInstalledSize(String param1String1, String param1String2) throws RemoteException {
      return 0L;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMediaContainerService {
    private static final String DESCRIPTOR = "com.android.internal.app.IMediaContainerService";
    
    static final int TRANSACTION_calculateInstalledSize = 4;
    
    static final int TRANSACTION_copyPackage = 1;
    
    static final int TRANSACTION_getMinimalPackageInfo = 2;
    
    static final int TRANSACTION_getObbInfo = 3;
    
    public Stub() {
      attachInterface(this, "com.android.internal.app.IMediaContainerService");
    }
    
    public static IMediaContainerService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.app.IMediaContainerService");
      if (iInterface != null && iInterface instanceof IMediaContainerService)
        return (IMediaContainerService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "calculateInstalledSize";
          } 
          return "getObbInfo";
        } 
        return "getMinimalPackageInfo";
      } 
      return "copyPackage";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      PackageInfoLite packageInfoLite;
      if (param1Int1 != 1) {
        ObbInfo obbInfo;
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("com.android.internal.app.IMediaContainerService");
              return true;
            } 
            param1Parcel1.enforceInterface("com.android.internal.app.IMediaContainerService");
            String str4 = param1Parcel1.readString();
            str3 = param1Parcel1.readString();
            long l = calculateInstalledSize(str4, str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          } 
          str3.enforceInterface("com.android.internal.app.IMediaContainerService");
          String str3 = str3.readString();
          obbInfo = getObbInfo(str3);
          param1Parcel2.writeNoException();
          if (obbInfo != null) {
            param1Parcel2.writeInt(1);
            obbInfo.writeToParcel(param1Parcel2, 1);
          } else {
            param1Parcel2.writeInt(0);
          } 
          return true;
        } 
        obbInfo.enforceInterface("com.android.internal.app.IMediaContainerService");
        String str2 = obbInfo.readString();
        param1Int1 = obbInfo.readInt();
        String str1 = obbInfo.readString();
        packageInfoLite = getMinimalPackageInfo(str2, param1Int1, str1);
        param1Parcel2.writeNoException();
        if (packageInfoLite != null) {
          param1Parcel2.writeInt(1);
          packageInfoLite.writeToParcel(param1Parcel2, 1);
        } else {
          param1Parcel2.writeInt(0);
        } 
        return true;
      } 
      packageInfoLite.enforceInterface("com.android.internal.app.IMediaContainerService");
      String str = packageInfoLite.readString();
      IParcelFileDescriptorFactory iParcelFileDescriptorFactory = IParcelFileDescriptorFactory.Stub.asInterface(packageInfoLite.readStrongBinder());
      param1Int1 = copyPackage(str, iParcelFileDescriptorFactory);
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(param1Int1);
      return true;
    }
    
    private static class Proxy implements IMediaContainerService {
      public static IMediaContainerService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.app.IMediaContainerService";
      }
      
      public int copyPackage(String param2String, IParcelFileDescriptorFactory param2IParcelFileDescriptorFactory) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("com.android.internal.app.IMediaContainerService");
          parcel1.writeString(param2String);
          if (param2IParcelFileDescriptorFactory != null) {
            iBinder = param2IParcelFileDescriptorFactory.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IMediaContainerService.Stub.getDefaultImpl() != null)
            return IMediaContainerService.Stub.getDefaultImpl().copyPackage(param2String, param2IParcelFileDescriptorFactory); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public PackageInfoLite getMinimalPackageInfo(String param2String1, int param2Int, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IMediaContainerService");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IMediaContainerService.Stub.getDefaultImpl() != null)
            return IMediaContainerService.Stub.getDefaultImpl().getMinimalPackageInfo(param2String1, param2Int, param2String2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            PackageInfoLite packageInfoLite = (PackageInfoLite)PackageInfoLite.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (PackageInfoLite)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ObbInfo getObbInfo(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IMediaContainerService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IMediaContainerService.Stub.getDefaultImpl() != null)
            return IMediaContainerService.Stub.getDefaultImpl().getObbInfo(param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ObbInfo obbInfo = (ObbInfo)ObbInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (ObbInfo)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long calculateInstalledSize(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.app.IMediaContainerService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IMediaContainerService.Stub.getDefaultImpl() != null)
            return IMediaContainerService.Stub.getDefaultImpl().calculateInstalledSize(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMediaContainerService param1IMediaContainerService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMediaContainerService != null) {
          Proxy.sDefaultImpl = param1IMediaContainerService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMediaContainerService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
