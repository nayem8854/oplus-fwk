package com.android.internal.app;

import android.common.ColorFrameworkFactory;
import android.content.Context;
import android.content.res.Resources;
import android.os.UserHandle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class ResolverMultiProfilePagerAdapter extends AbstractMultiProfilePagerAdapter {
  private final ResolverProfileDescriptor[] mItems;
  
  private final boolean mShouldShowNoCrossProfileIntentsEmptyState;
  
  private boolean mUseLayoutWithDefault;
  
  ResolverMultiProfilePagerAdapter(Context paramContext, ResolverListAdapter paramResolverListAdapter, UserHandle paramUserHandle1, UserHandle paramUserHandle2) {
    super(paramContext, 0, paramUserHandle1, paramUserHandle2);
    this.mItems = new ResolverProfileDescriptor[] { createProfileDescriptor(paramResolverListAdapter) };
    this.mShouldShowNoCrossProfileIntentsEmptyState = true;
  }
  
  ResolverMultiProfilePagerAdapter(Context paramContext, ResolverListAdapter paramResolverListAdapter1, ResolverListAdapter paramResolverListAdapter2, int paramInt, UserHandle paramUserHandle1, UserHandle paramUserHandle2, boolean paramBoolean) {
    super(paramContext, paramInt, paramUserHandle1, paramUserHandle2);
    ResolverProfileDescriptor resolverProfileDescriptor = createProfileDescriptor(paramResolverListAdapter1);
    this.mItems = new ResolverProfileDescriptor[] { resolverProfileDescriptor, createProfileDescriptor(paramResolverListAdapter2) };
    this.mShouldShowNoCrossProfileIntentsEmptyState = paramBoolean;
  }
  
  void updateAfterConfigChange() {
    super.updateAfterConfigChange();
    for (ResolverProfileDescriptor resolverProfileDescriptor : this.mItems) {
      ViewGroup viewGroup = resolverProfileDescriptor.rootView;
      viewGroup = viewGroup.findViewById(16909353);
      Resources resources = getContext().getResources();
      int i = viewGroup.getPaddingLeft();
      int j = resources.getDimensionPixelSize(17105440);
      int k = viewGroup.getPaddingRight();
      int m = resources.getDimensionPixelSize(17105439);
      viewGroup.setPadding(i, j, k, m);
    } 
  }
  
  private ResolverProfileDescriptor createProfileDescriptor(ResolverListAdapter paramResolverListAdapter) {
    LayoutInflater layoutInflater = LayoutInflater.from(getContext());
    ViewGroup viewGroup = ((IOplusResolverManager)ColorFrameworkFactory.getInstance().getFeature(IOplusResolverManager.DEFAULT, new Object[0])).getResolverProfileDescriptor(layoutInflater);
    return new ResolverProfileDescriptor(viewGroup, paramResolverListAdapter);
  }
  
  ListView getListViewForIndex(int paramInt) {
    return (getItem(paramInt)).listView;
  }
  
  ResolverProfileDescriptor getItem(int paramInt) {
    return this.mItems[paramInt];
  }
  
  int getItemCount() {
    return this.mItems.length;
  }
  
  void setupListAdapter(int paramInt) {
    ListView listView = (getItem(paramInt)).listView;
    listView.setAdapter((getItem(paramInt)).resolverListAdapter);
  }
  
  public ResolverListAdapter getAdapterForIndex(int paramInt) {
    return (this.mItems[paramInt]).resolverListAdapter;
  }
  
  public ViewGroup instantiateItem(ViewGroup paramViewGroup, int paramInt) {
    setupListAdapter(paramInt);
    return super.instantiateItem(paramViewGroup, paramInt);
  }
  
  ResolverListAdapter getListAdapterForUserHandle(UserHandle paramUserHandle) {
    if (getActiveListAdapter().getUserHandle().equals(paramUserHandle))
      return getActiveListAdapter(); 
    if (getInactiveListAdapter() != null && 
      getInactiveListAdapter().getUserHandle().equals(paramUserHandle))
      return getInactiveListAdapter(); 
    return null;
  }
  
  public ResolverListAdapter getActiveListAdapter() {
    return getAdapterForIndex(getCurrentPage());
  }
  
  public ResolverListAdapter getInactiveListAdapter() {
    if (getCount() == 1)
      return null; 
    return getAdapterForIndex(1 - getCurrentPage());
  }
  
  public ResolverListAdapter getPersonalListAdapter() {
    return getAdapterForIndex(0);
  }
  
  public ResolverListAdapter getWorkListAdapter() {
    return getAdapterForIndex(1);
  }
  
  ResolverListAdapter getCurrentRootAdapter() {
    return getActiveListAdapter();
  }
  
  ListView getActiveAdapterView() {
    return getListViewForIndex(getCurrentPage());
  }
  
  ViewGroup getInactiveAdapterView() {
    if (getCount() == 1)
      return null; 
    return getListViewForIndex(1 - getCurrentPage());
  }
  
  String getMetricsCategory() {
    return "intent_resolver";
  }
  
  boolean allowShowNoCrossProfileIntentsEmptyState() {
    return this.mShouldShowNoCrossProfileIntentsEmptyState;
  }
  
  protected void showWorkProfileOffEmptyState(ResolverListAdapter paramResolverListAdapter, View.OnClickListener paramOnClickListener) {
    showEmptyState(paramResolverListAdapter, 17302891, 17041187, 0, paramOnClickListener);
  }
  
  protected void showNoPersonalToWorkIntentsEmptyState(ResolverListAdapter paramResolverListAdapter) {
    showEmptyState(paramResolverListAdapter, 17302824, 17041174, 17041175);
  }
  
  protected void showNoWorkToPersonalIntentsEmptyState(ResolverListAdapter paramResolverListAdapter) {
    showEmptyState(paramResolverListAdapter, 17302824, 17041172, 17041173);
  }
  
  protected void showNoPersonalAppsAvailableEmptyState(ResolverListAdapter paramResolverListAdapter) {
    showEmptyState(paramResolverListAdapter, 17302754, 17041180, 0);
  }
  
  protected void showNoWorkAppsAvailableEmptyState(ResolverListAdapter paramResolverListAdapter) {
    showEmptyState(paramResolverListAdapter, 17302754, 17041182, 0);
  }
  
  void setUseLayoutWithDefault(boolean paramBoolean) {
    this.mUseLayoutWithDefault = paramBoolean;
  }
  
  protected void setupContainerPadding(View paramView) {
    boolean bool;
    if (this.mUseLayoutWithDefault) {
      bool = paramView.getPaddingBottom();
    } else {
      bool = false;
    } 
    int i = paramView.getPaddingLeft(), j = paramView.getPaddingTop();
    int k = paramView.getPaddingRight();
    paramView.setPadding(i, j, k, bool);
  }
  
  class ResolverProfileDescriptor extends AbstractMultiProfilePagerAdapter.ProfileDescriptor {
    final ListView listView;
    
    private ResolverListAdapter resolverListAdapter;
    
    final ResolverMultiProfilePagerAdapter this$0;
    
    ResolverProfileDescriptor(ViewGroup param1ViewGroup, ResolverListAdapter param1ResolverListAdapter) {
      super(param1ViewGroup);
      this.resolverListAdapter = param1ResolverListAdapter;
      this.listView = param1ViewGroup.<ListView>findViewById(16909358);
    }
  }
}
