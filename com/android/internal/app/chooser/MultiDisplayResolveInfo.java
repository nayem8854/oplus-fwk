package com.android.internal.app.chooser;

import android.app.Activity;
import android.os.Bundle;
import android.os.UserHandle;
import com.android.internal.app.ResolverActivity;
import java.util.ArrayList;
import java.util.List;

public class MultiDisplayResolveInfo extends DisplayResolveInfo {
  final DisplayResolveInfo mBaseInfo;
  
  private int mSelected;
  
  List<DisplayResolveInfo> mTargetInfos;
  
  public MultiDisplayResolveInfo(String paramString, DisplayResolveInfo paramDisplayResolveInfo) {
    super(paramDisplayResolveInfo);
    ArrayList<DisplayResolveInfo> arrayList = new ArrayList();
    this.mSelected = -1;
    this.mBaseInfo = paramDisplayResolveInfo;
    arrayList.add(paramDisplayResolveInfo);
  }
  
  public CharSequence getExtendedInfo() {
    return null;
  }
  
  public void addTarget(DisplayResolveInfo paramDisplayResolveInfo) {
    this.mTargetInfos.add(paramDisplayResolveInfo);
  }
  
  public List<DisplayResolveInfo> getTargets() {
    return this.mTargetInfos;
  }
  
  public void setSelected(int paramInt) {
    this.mSelected = paramInt;
  }
  
  public DisplayResolveInfo getSelectedTarget() {
    DisplayResolveInfo displayResolveInfo;
    if (hasSelected()) {
      displayResolveInfo = this.mTargetInfos.get(this.mSelected);
    } else {
      displayResolveInfo = null;
    } 
    return displayResolveInfo;
  }
  
  public boolean hasSelected() {
    boolean bool;
    if (this.mSelected >= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean start(Activity paramActivity, Bundle paramBundle) {
    return ((DisplayResolveInfo)this.mTargetInfos.get(this.mSelected)).start(paramActivity, paramBundle);
  }
  
  public boolean startAsCaller(ResolverActivity paramResolverActivity, Bundle paramBundle, int paramInt) {
    return ((DisplayResolveInfo)this.mTargetInfos.get(this.mSelected)).startAsCaller(paramResolverActivity, paramBundle, paramInt);
  }
  
  public boolean startAsUser(Activity paramActivity, Bundle paramBundle, UserHandle paramUserHandle) {
    return ((DisplayResolveInfo)this.mTargetInfos.get(this.mSelected)).startAsUser(paramActivity, paramBundle, paramUserHandle);
  }
}
