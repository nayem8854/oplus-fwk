package com.android.internal.app.chooser;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.LauncherApps;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ShortcutInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.os.Bundle;
import android.os.UserHandle;
import android.service.chooser.ChooserTarget;
import android.text.SpannableStringBuilder;
import android.util.Log;
import com.android.internal.app.ResolverActivity;
import com.android.internal.app.ResolverListAdapter;
import com.android.internal.app.SimpleIconFactory;
import java.util.ArrayList;
import java.util.List;

public final class SelectableTargetInfo implements ChooserTargetInfo {
  private static final String TAG = "SelectableTargetInfo";
  
  private final ResolveInfo mBackupResolveInfo;
  
  private CharSequence mBadgeContentDescription;
  
  private Drawable mBadgeIcon = null;
  
  private final ChooserTarget mChooserTarget;
  
  private final Context mContext;
  
  private Drawable mDisplayIcon;
  
  private final String mDisplayLabel;
  
  private final int mFillInFlags;
  
  private final Intent mFillInIntent;
  
  private boolean mIsSuspended = false;
  
  private final float mModifiedScore;
  
  private final PackageManager mPm;
  
  private final SelectableTargetInfoCommunicator mSelectableTargetInfoCommunicator;
  
  private final DisplayResolveInfo mSourceInfo;
  
  public SelectableTargetInfo(Context paramContext, DisplayResolveInfo paramDisplayResolveInfo, ChooserTarget paramChooserTarget, float paramFloat, SelectableTargetInfoCommunicator paramSelectableTargetInfoCommunicator, ShortcutInfo paramShortcutInfo) {
    this.mContext = paramContext;
    this.mSourceInfo = paramDisplayResolveInfo;
    this.mChooserTarget = paramChooserTarget;
    this.mModifiedScore = paramFloat;
    this.mPm = paramContext.getPackageManager();
    this.mSelectableTargetInfoCommunicator = paramSelectableTargetInfoCommunicator;
    if (paramDisplayResolveInfo != null) {
      ResolveInfo resolveInfo = paramDisplayResolveInfo.getResolveInfo();
      if (resolveInfo != null) {
        ActivityInfo activityInfo = resolveInfo.activityInfo;
        if (activityInfo != null && activityInfo.applicationInfo != null) {
          boolean bool;
          PackageManager packageManager = this.mContext.getPackageManager();
          this.mBadgeIcon = packageManager.getApplicationIcon(activityInfo.applicationInfo);
          this.mBadgeContentDescription = packageManager.getApplicationLabel(activityInfo.applicationInfo);
          if ((activityInfo.applicationInfo.flags & 0x40000000) != 0) {
            bool = true;
          } else {
            bool = false;
          } 
          this.mIsSuspended = bool;
        } 
      } 
    } 
    this.mDisplayIcon = getChooserTargetIconDrawable(paramChooserTarget, paramShortcutInfo);
    if (paramDisplayResolveInfo != null) {
      this.mBackupResolveInfo = null;
    } else {
      paramContext = this.mContext;
      this.mBackupResolveInfo = paramContext.getPackageManager().resolveActivity(getResolvedIntent(), 0);
    } 
    this.mFillInIntent = null;
    this.mFillInFlags = 0;
    this.mDisplayLabel = sanitizeDisplayLabel(paramChooserTarget.getTitle());
  }
  
  private SelectableTargetInfo(SelectableTargetInfo paramSelectableTargetInfo, Intent paramIntent, int paramInt) {
    this.mContext = paramSelectableTargetInfo.mContext;
    this.mPm = paramSelectableTargetInfo.mPm;
    this.mSelectableTargetInfoCommunicator = paramSelectableTargetInfo.mSelectableTargetInfoCommunicator;
    this.mSourceInfo = paramSelectableTargetInfo.mSourceInfo;
    this.mBackupResolveInfo = paramSelectableTargetInfo.mBackupResolveInfo;
    ChooserTarget chooserTarget = paramSelectableTargetInfo.mChooserTarget;
    this.mBadgeIcon = paramSelectableTargetInfo.mBadgeIcon;
    this.mBadgeContentDescription = paramSelectableTargetInfo.mBadgeContentDescription;
    this.mDisplayIcon = paramSelectableTargetInfo.mDisplayIcon;
    this.mFillInIntent = paramIntent;
    this.mFillInFlags = paramInt;
    this.mModifiedScore = paramSelectableTargetInfo.mModifiedScore;
    this.mDisplayLabel = sanitizeDisplayLabel(chooserTarget.getTitle());
  }
  
  private String sanitizeDisplayLabel(CharSequence paramCharSequence) {
    paramCharSequence = new SpannableStringBuilder(paramCharSequence);
    paramCharSequence.clearSpans();
    return paramCharSequence.toString();
  }
  
  public boolean isSuspended() {
    return this.mIsSuspended;
  }
  
  public DisplayResolveInfo getDisplayResolveInfo() {
    return this.mSourceInfo;
  }
  
  private Drawable getChooserTargetIconDrawable(ChooserTarget paramChooserTarget, ShortcutInfo paramShortcutInfo) {
    ShortcutInfo shortcutInfo;
    Drawable drawable = null;
    Icon icon = paramChooserTarget.getIcon();
    if (icon != null) {
      drawable = icon.loadDrawable(this.mContext);
    } else if (paramShortcutInfo != null) {
      LauncherApps launcherApps = (LauncherApps)this.mContext.getSystemService("launcherapps");
      drawable = launcherApps.getShortcutIconDrawable(paramShortcutInfo, 0);
    } 
    if (drawable == null)
      return null; 
    paramShortcutInfo = null;
    try {
      ActivityInfo activityInfo = this.mPm.getActivityInfo(paramChooserTarget.getComponentName(), 0);
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      Log.e("SelectableTargetInfo", "Could not find activity associated with ChooserTarget");
      shortcutInfo = paramShortcutInfo;
    } 
    if (shortcutInfo == null)
      return null; 
    ResolverListAdapter.ActivityInfoPresentationGetter activityInfoPresentationGetter = this.mSelectableTargetInfoCommunicator.makePresentationGetter((ActivityInfo)shortcutInfo);
    Bitmap bitmap = activityInfoPresentationGetter.getIconBitmap(UserHandle.getUserHandleForUid(UserHandle.myUserId()));
    SimpleIconFactory simpleIconFactory = SimpleIconFactory.obtain(this.mContext);
    bitmap = simpleIconFactory.createAppBadgedIconBitmap(drawable, bitmap);
    simpleIconFactory.recycle();
    return (Drawable)new BitmapDrawable(this.mContext.getResources(), bitmap);
  }
  
  public float getModifiedScore() {
    return this.mModifiedScore;
  }
  
  public Intent getResolvedIntent() {
    DisplayResolveInfo displayResolveInfo = this.mSourceInfo;
    if (displayResolveInfo != null)
      return displayResolveInfo.getResolvedIntent(); 
    Intent intent = new Intent(this.mSelectableTargetInfoCommunicator.getTargetIntent());
    intent.setComponent(this.mChooserTarget.getComponentName());
    intent.putExtras(this.mChooserTarget.getIntentExtras());
    return intent;
  }
  
  public ComponentName getResolvedComponentName() {
    DisplayResolveInfo displayResolveInfo = this.mSourceInfo;
    if (displayResolveInfo != null)
      return displayResolveInfo.getResolvedComponentName(); 
    if (this.mBackupResolveInfo != null)
      return new ComponentName(this.mBackupResolveInfo.activityInfo.packageName, this.mBackupResolveInfo.activityInfo.name); 
    return null;
  }
  
  private Intent getBaseIntentToSend() {
    Intent intent = getResolvedIntent();
    if (intent == null) {
      Log.e("SelectableTargetInfo", "ChooserTargetInfo: no base intent available to send");
    } else {
      intent = new Intent(intent);
      Intent intent1 = this.mFillInIntent;
      if (intent1 != null)
        intent.fillIn(intent1, this.mFillInFlags); 
      intent.fillIn(this.mSelectableTargetInfoCommunicator.getReferrerFillInIntent(), 0);
    } 
    return intent;
  }
  
  public boolean start(Activity paramActivity, Bundle paramBundle) {
    throw new RuntimeException("ChooserTargets should be started as caller.");
  }
  
  public boolean startAsCaller(ResolverActivity paramResolverActivity, Bundle paramBundle, int paramInt) {
    Intent intent = getBaseIntentToSend();
    boolean bool = false;
    if (intent == null)
      return false; 
    intent.setComponent(this.mChooserTarget.getComponentName());
    intent.putExtras(this.mChooserTarget.getIntentExtras());
    DisplayResolveInfo displayResolveInfo = this.mSourceInfo;
    if (displayResolveInfo != null) {
      String str = displayResolveInfo.getResolvedComponentName().getPackageName();
      ChooserTarget chooserTarget = this.mChooserTarget;
      if (str.equals(chooserTarget.getComponentName().getPackageName()))
        bool = true; 
    } 
    return paramResolverActivity.startAsCallerImpl(intent, paramBundle, bool, paramInt);
  }
  
  public boolean startAsUser(Activity paramActivity, Bundle paramBundle, UserHandle paramUserHandle) {
    throw new RuntimeException("ChooserTargets should be started as caller.");
  }
  
  public ResolveInfo getResolveInfo() {
    ResolveInfo resolveInfo;
    DisplayResolveInfo displayResolveInfo = this.mSourceInfo;
    if (displayResolveInfo != null) {
      resolveInfo = displayResolveInfo.getResolveInfo();
    } else {
      resolveInfo = this.mBackupResolveInfo;
    } 
    return resolveInfo;
  }
  
  public CharSequence getDisplayLabel() {
    return this.mDisplayLabel;
  }
  
  public CharSequence getExtendedInfo() {
    return null;
  }
  
  public Drawable getDisplayIcon(Context paramContext) {
    return this.mDisplayIcon;
  }
  
  public ChooserTarget getChooserTarget() {
    return this.mChooserTarget;
  }
  
  public TargetInfo cloneFilledIn(Intent paramIntent, int paramInt) {
    return new SelectableTargetInfo(this, paramIntent, paramInt);
  }
  
  public List<Intent> getAllSourceIntents() {
    ArrayList<Intent> arrayList = new ArrayList();
    DisplayResolveInfo displayResolveInfo = this.mSourceInfo;
    if (displayResolveInfo != null)
      arrayList.add(displayResolveInfo.getAllSourceIntents().get(0)); 
    return arrayList;
  }
  
  public boolean isPinned() {
    boolean bool;
    DisplayResolveInfo displayResolveInfo = this.mSourceInfo;
    if (displayResolveInfo != null && displayResolveInfo.isPinned()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  class SelectableTargetInfoCommunicator {
    public abstract Intent getReferrerFillInIntent();
    
    public abstract Intent getTargetIntent();
    
    public abstract ResolverListAdapter.ActivityInfoPresentationGetter makePresentationGetter(ActivityInfo param1ActivityInfo);
  }
}
