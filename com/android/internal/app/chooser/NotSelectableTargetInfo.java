package com.android.internal.app.chooser;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.UserHandle;
import android.service.chooser.ChooserTarget;
import com.android.internal.app.ResolverActivity;
import java.util.List;

public abstract class NotSelectableTargetInfo implements ChooserTargetInfo {
  public Intent getResolvedIntent() {
    return null;
  }
  
  public ComponentName getResolvedComponentName() {
    return null;
  }
  
  public boolean start(Activity paramActivity, Bundle paramBundle) {
    return false;
  }
  
  public boolean startAsCaller(ResolverActivity paramResolverActivity, Bundle paramBundle, int paramInt) {
    return false;
  }
  
  public boolean startAsUser(Activity paramActivity, Bundle paramBundle, UserHandle paramUserHandle) {
    return false;
  }
  
  public ResolveInfo getResolveInfo() {
    return null;
  }
  
  public CharSequence getDisplayLabel() {
    return null;
  }
  
  public CharSequence getExtendedInfo() {
    return null;
  }
  
  public TargetInfo cloneFilledIn(Intent paramIntent, int paramInt) {
    return null;
  }
  
  public List<Intent> getAllSourceIntents() {
    return null;
  }
  
  public float getModifiedScore() {
    return -0.1F;
  }
  
  public ChooserTarget getChooserTarget() {
    return null;
  }
  
  public boolean isSuspended() {
    return false;
  }
  
  public boolean isPinned() {
    return false;
  }
}
