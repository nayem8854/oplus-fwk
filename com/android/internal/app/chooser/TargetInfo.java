package com.android.internal.app.chooser;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.UserHandle;
import com.android.internal.app.ResolverActivity;
import java.util.List;

public interface TargetInfo {
  TargetInfo cloneFilledIn(Intent paramIntent, int paramInt);
  
  List<Intent> getAllSourceIntents();
  
  Drawable getDisplayIcon(Context paramContext);
  
  CharSequence getDisplayLabel();
  
  CharSequence getExtendedInfo();
  
  ResolveInfo getResolveInfo();
  
  ComponentName getResolvedComponentName();
  
  Intent getResolvedIntent();
  
  boolean isPinned();
  
  boolean isSuspended();
  
  boolean start(Activity paramActivity, Bundle paramBundle);
  
  boolean startAsCaller(ResolverActivity paramResolverActivity, Bundle paramBundle, int paramInt);
  
  boolean startAsUser(Activity paramActivity, Bundle paramBundle, UserHandle paramUserHandle);
}
