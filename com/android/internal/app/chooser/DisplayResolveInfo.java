package com.android.internal.app.chooser;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.UserHandle;
import com.android.internal.app.ResolverActivity;
import com.android.internal.app.ResolverListAdapter;
import java.util.ArrayList;
import java.util.List;

public class DisplayResolveInfo extends OplusBaseDisplayResolveInfo implements TargetInfo {
  private static final boolean ENABLE_CHOOSER_DELEGATE = false;
  
  private Drawable mDisplayIcon;
  
  private CharSequence mDisplayLabel;
  
  private CharSequence mExtendedInfo;
  
  private boolean mIsSuspended;
  
  private boolean mPinned;
  
  private final ResolveInfo mResolveInfo;
  
  private ResolverListAdapter.ResolveInfoPresentationGetter mResolveInfoPresentationGetter;
  
  private final Intent mResolvedIntent;
  
  private final List<Intent> mSourceIntents;
  
  public DisplayResolveInfo(Intent paramIntent1, ResolveInfo paramResolveInfo, Intent paramIntent2, ResolverListAdapter.ResolveInfoPresentationGetter paramResolveInfoPresentationGetter) {
    this(paramIntent1, paramResolveInfo, null, null, paramIntent2, paramResolveInfoPresentationGetter);
  }
  
  public DisplayResolveInfo(Intent paramIntent1, ResolveInfo paramResolveInfo, CharSequence paramCharSequence1, CharSequence paramCharSequence2, Intent paramIntent2, ResolverListAdapter.ResolveInfoPresentationGetter paramResolveInfoPresentationGetter) {
    ArrayList<Intent> arrayList = new ArrayList();
    boolean bool = false;
    this.mPinned = false;
    arrayList.add(paramIntent1);
    this.mResolveInfo = paramResolveInfo;
    this.mDisplayLabel = paramCharSequence1;
    this.mExtendedInfo = paramCharSequence2;
    this.mResolveInfoPresentationGetter = paramResolveInfoPresentationGetter;
    paramIntent1 = new Intent(paramIntent2);
    paramIntent1.addFlags(50331648);
    ActivityInfo activityInfo = this.mResolveInfo.activityInfo;
    paramIntent1.setComponent(new ComponentName(activityInfo.applicationInfo.packageName, activityInfo.name));
    if ((activityInfo.applicationInfo.flags & 0x40000000) != 0)
      bool = true; 
    this.mIsSuspended = bool;
    this.mResolvedIntent = paramIntent1;
  }
  
  private DisplayResolveInfo(DisplayResolveInfo paramDisplayResolveInfo, Intent paramIntent, int paramInt, ResolverListAdapter.ResolveInfoPresentationGetter paramResolveInfoPresentationGetter) {
    ArrayList<Intent> arrayList = new ArrayList();
    this.mPinned = false;
    arrayList.addAll(paramDisplayResolveInfo.getAllSourceIntents());
    this.mResolveInfo = paramDisplayResolveInfo.mResolveInfo;
    this.mDisplayLabel = paramDisplayResolveInfo.mDisplayLabel;
    this.mDisplayIcon = paramDisplayResolveInfo.mDisplayIcon;
    this.mExtendedInfo = paramDisplayResolveInfo.mExtendedInfo;
    Intent intent = new Intent(paramDisplayResolveInfo.mResolvedIntent);
    intent.fillIn(paramIntent, paramInt);
    this.mResolveInfoPresentationGetter = paramResolveInfoPresentationGetter;
  }
  
  DisplayResolveInfo(DisplayResolveInfo paramDisplayResolveInfo) {
    ArrayList<Intent> arrayList = new ArrayList();
    this.mPinned = false;
    arrayList.addAll(paramDisplayResolveInfo.getAllSourceIntents());
    this.mResolveInfo = paramDisplayResolveInfo.mResolveInfo;
    this.mDisplayLabel = paramDisplayResolveInfo.mDisplayLabel;
    this.mDisplayIcon = paramDisplayResolveInfo.mDisplayIcon;
    this.mExtendedInfo = paramDisplayResolveInfo.mExtendedInfo;
    this.mResolvedIntent = paramDisplayResolveInfo.mResolvedIntent;
    this.mResolveInfoPresentationGetter = paramDisplayResolveInfo.mResolveInfoPresentationGetter;
  }
  
  public ResolveInfo getResolveInfo() {
    return this.mResolveInfo;
  }
  
  public CharSequence getDisplayLabel() {
    if (this.mDisplayLabel == null) {
      ResolverListAdapter.ResolveInfoPresentationGetter resolveInfoPresentationGetter = this.mResolveInfoPresentationGetter;
      if (resolveInfoPresentationGetter != null) {
        this.mDisplayLabel = resolveInfoPresentationGetter.getLabel();
        this.mExtendedInfo = this.mResolveInfoPresentationGetter.getSubLabel();
      } 
    } 
    return this.mDisplayLabel;
  }
  
  public boolean hasDisplayLabel() {
    boolean bool;
    if (this.mDisplayLabel != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setDisplayLabel(CharSequence paramCharSequence) {
    this.mDisplayLabel = paramCharSequence;
  }
  
  public void setExtendedInfo(CharSequence paramCharSequence) {
    this.mExtendedInfo = paramCharSequence;
  }
  
  public Drawable getDisplayIcon(Context paramContext) {
    return this.mDisplayIcon;
  }
  
  public TargetInfo cloneFilledIn(Intent paramIntent, int paramInt) {
    return new DisplayResolveInfo(this, paramIntent, paramInt, this.mResolveInfoPresentationGetter);
  }
  
  public List<Intent> getAllSourceIntents() {
    return this.mSourceIntents;
  }
  
  public void addAlternateSourceIntent(Intent paramIntent) {
    this.mSourceIntents.add(paramIntent);
  }
  
  public void setDisplayIcon(Drawable paramDrawable) {
    this.mDisplayIcon = paramDrawable;
  }
  
  public boolean hasDisplayIcon() {
    boolean bool;
    if (this.mDisplayIcon != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public CharSequence getExtendedInfo() {
    return this.mExtendedInfo;
  }
  
  public Intent getResolvedIntent() {
    return this.mResolvedIntent;
  }
  
  public ComponentName getResolvedComponentName() {
    return new ComponentName(this.mResolveInfo.activityInfo.packageName, this.mResolveInfo.activityInfo.name);
  }
  
  public boolean start(Activity paramActivity, Bundle paramBundle) {
    paramActivity.startActivity(this.mResolvedIntent, paramBundle);
    return true;
  }
  
  public boolean startAsCaller(ResolverActivity paramResolverActivity, Bundle paramBundle, int paramInt) {
    paramInt = changeUserIdIfNeed(paramResolverActivity, this.mResolvedIntent, paramInt);
    paramResolverActivity.startActivityAsCaller(this.mResolvedIntent, paramBundle, null, false, paramInt);
    return true;
  }
  
  public boolean startAsUser(Activity paramActivity, Bundle paramBundle, UserHandle paramUserHandle) {
    paramActivity.startActivityAsUser(this.mResolvedIntent, paramBundle, paramUserHandle);
    return false;
  }
  
  public boolean isSuspended() {
    return this.mIsSuspended;
  }
  
  public boolean isPinned() {
    return this.mPinned;
  }
  
  public void setPinned(boolean paramBoolean) {
    this.mPinned = paramBoolean;
  }
}
