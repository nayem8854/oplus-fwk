package com.android.internal.app.chooser;

import android.service.chooser.ChooserTarget;
import android.text.TextUtils;

public interface ChooserTargetInfo extends TargetInfo {
  ChooserTarget getChooserTarget();
  
  float getModifiedScore();
  
  default boolean isSimilar(ChooserTargetInfo paramChooserTargetInfo) {
    if (paramChooserTargetInfo == null)
      return false; 
    ChooserTarget chooserTarget1 = getChooserTarget();
    ChooserTarget chooserTarget2 = paramChooserTargetInfo.getChooserTarget();
    if (chooserTarget1 == null || chooserTarget2 == null)
      return false; 
    if (chooserTarget1.getComponentName().equals(chooserTarget2.getComponentName()) && 
      TextUtils.equals(getDisplayLabel(), paramChooserTargetInfo.getDisplayLabel()) && 
      TextUtils.equals(getExtendedInfo(), paramChooserTargetInfo.getExtendedInfo()))
      return true; 
    return false;
  }
}
