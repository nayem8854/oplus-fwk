package com.android.internal.app.chooser;

import android.content.Intent;
import android.content.OplusBaseIntent;
import android.os.UserHandle;
import android.util.Log;
import com.android.internal.app.OplusBaseResolverActivity;
import com.oplus.util.OplusTypeCastingHelper;

public class OplusBaseDisplayResolveInfo {
  private boolean mIsMultiApp = false;
  
  public boolean getIsMultiApp() {
    return this.mIsMultiApp;
  }
  
  public void setIsMultiApp(boolean paramBoolean) {
    this.mIsMultiApp = paramBoolean;
  }
  
  int changeUserIdIfNeed(OplusBaseResolverActivity paramOplusBaseResolverActivity, Intent paramIntent, int paramInt) {
    int i = paramInt;
    if (paramOplusBaseResolverActivity != null) {
      i = paramInt;
      if (paramOplusBaseResolverActivity.hasCustomFlag(512)) {
        i = paramInt;
        if (paramIntent != null) {
          if (getIsMultiApp()) {
            paramInt = 999;
            paramIntent.putExtra("android.intent.extra.USER_ID", 999);
          } else {
            paramInt = 0;
          } 
          if (-1 != paramOplusBaseResolverActivity.getLaunchedFromUid()) {
            i = UserHandle.getUserId(paramOplusBaseResolverActivity.getLaunchedFromUid());
            if ((i == 999 && paramInt == 0) || (i == 0 && paramInt == 999)) {
              paramIntent.prepareToLeaveUser(i);
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("CMAService fillMultiAppInfo for cross form:");
              stringBuilder.append(i);
              stringBuilder.append(" to:");
              stringBuilder.append(paramInt);
              stringBuilder.append(" intent=");
              stringBuilder.append(paramIntent.toString());
              Log.d("ChooseActivity", stringBuilder.toString());
            } 
          } 
          OplusBaseIntent oplusBaseIntent = (OplusBaseIntent)OplusTypeCastingHelper.typeCasting(OplusBaseIntent.class, paramIntent);
          i = paramInt;
          if (oplusBaseIntent != null) {
            oplusBaseIntent.addOplusFlags(2048);
            i = paramInt;
          } 
        } 
      } 
    } 
    return i;
  }
}
