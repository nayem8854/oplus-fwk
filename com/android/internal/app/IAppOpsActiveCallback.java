package com.android.internal.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IAppOpsActiveCallback extends IInterface {
  void opActiveChanged(int paramInt1, int paramInt2, String paramString, boolean paramBoolean) throws RemoteException;
  
  class Default implements IAppOpsActiveCallback {
    public void opActiveChanged(int param1Int1, int param1Int2, String param1String, boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAppOpsActiveCallback {
    private static final String DESCRIPTOR = "com.android.internal.app.IAppOpsActiveCallback";
    
    static final int TRANSACTION_opActiveChanged = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.app.IAppOpsActiveCallback");
    }
    
    public static IAppOpsActiveCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.app.IAppOpsActiveCallback");
      if (iInterface != null && iInterface instanceof IAppOpsActiveCallback)
        return (IAppOpsActiveCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "opActiveChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.app.IAppOpsActiveCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.app.IAppOpsActiveCallback");
      param1Int1 = param1Parcel1.readInt();
      param1Int2 = param1Parcel1.readInt();
      String str = param1Parcel1.readString();
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      opActiveChanged(param1Int1, param1Int2, str, bool);
      return true;
    }
    
    private static class Proxy implements IAppOpsActiveCallback {
      public static IAppOpsActiveCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.app.IAppOpsActiveCallback";
      }
      
      public void opActiveChanged(int param2Int1, int param2Int2, String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.app.IAppOpsActiveCallback");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, null, 1);
          if (!bool1 && IAppOpsActiveCallback.Stub.getDefaultImpl() != null) {
            IAppOpsActiveCallback.Stub.getDefaultImpl().opActiveChanged(param2Int1, param2Int2, param2String, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAppOpsActiveCallback param1IAppOpsActiveCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAppOpsActiveCallback != null) {
          Proxy.sDefaultImpl = param1IAppOpsActiveCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAppOpsActiveCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
