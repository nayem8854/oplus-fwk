package com.android.internal.app;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import com.oplus.util.OplusContextUtil;

public class OplusAlertControllerEuclidManger implements IOplusAlertControllerEuclidManager {
  private static volatile OplusAlertControllerEuclidManger sInstance;
  
  public static OplusAlertControllerEuclidManger getInstance() {
    // Byte code:
    //   0: getstatic com/android/internal/app/OplusAlertControllerEuclidManger.sInstance : Lcom/android/internal/app/OplusAlertControllerEuclidManger;
    //   3: ifnonnull -> 39
    //   6: ldc com/android/internal/app/OplusAlertControllerEuclidManger
    //   8: monitorenter
    //   9: getstatic com/android/internal/app/OplusAlertControllerEuclidManger.sInstance : Lcom/android/internal/app/OplusAlertControllerEuclidManger;
    //   12: ifnonnull -> 27
    //   15: new com/android/internal/app/OplusAlertControllerEuclidManger
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic com/android/internal/app/OplusAlertControllerEuclidManger.sInstance : Lcom/android/internal/app/OplusAlertControllerEuclidManger;
    //   27: ldc com/android/internal/app/OplusAlertControllerEuclidManger
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc com/android/internal/app/OplusAlertControllerEuclidManger
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic com/android/internal/app/OplusAlertControllerEuclidManger.sInstance : Lcom/android/internal/app/OplusAlertControllerEuclidManger;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #20	-> 0
    //   #21	-> 6
    //   #22	-> 9
    //   #23	-> 15
    //   #25	-> 27
    //   #27	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  public AlertController getAlertController(Context paramContext, DialogInterface paramDialogInterface, Window paramWindow) {
    if (OplusContextUtil.isOplusStyle(paramContext))
      return new OplusAlertController(paramContext, paramDialogInterface, paramWindow); 
    return new AlertController(paramContext, paramDialogInterface, paramWindow);
  }
  
  public void setListStyle(ListView paramListView, boolean paramBoolean1, boolean paramBoolean2) {
    boolean bool = OplusContextUtil.isOplusStyle(paramListView.getContext());
    if (paramBoolean1) {
      if (bool) {
        paramListView.setSelector(201851229);
        paramListView.setItemsCanFocus(false);
      } 
      paramListView.setChoiceMode(1);
    } else if (paramBoolean2) {
      if (bool) {
        paramListView.setSelector(201851229);
        paramListView.setItemsCanFocus(false);
      } 
      paramListView.setChoiceMode(2);
    } 
  }
  
  public View getConvertView(View paramView, int paramInt1, int paramInt2) {
    if (paramView == null || !OplusContextUtil.isOplusStyle(paramView.getContext()))
      return null; 
    Context context = paramView.getContext();
    int i = context.getResources().getDimensionPixelOffset(201654379);
    int j = context.getResources().getDimensionPixelOffset(201654380);
    int k = context.getResources().getDimensionPixelOffset(201654381);
    int m = context.getResources().getDimensionPixelOffset(201654372);
    int n = context.getResources().getDimensionPixelOffset(201654381);
    int i1 = context.getResources().getDimensionPixelOffset(201654373);
    if (paramInt1 == paramInt2 - 1) {
      paramView.setMinimumHeight(i1 + m);
      paramView.setPadding(i, n, j, k + m);
    } else {
      paramView.setMinimumHeight(i1);
      paramView.setPadding(i, n, j, k);
    } 
    return paramView;
  }
}
