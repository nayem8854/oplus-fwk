package com.android.internal.globalactions;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.List;
import java.util.function.BooleanSupplier;

public class ActionsAdapter extends BaseAdapter {
  private final Context mContext;
  
  private final BooleanSupplier mDeviceProvisioned;
  
  private final List<Action> mItems;
  
  private final BooleanSupplier mKeyguardShowing;
  
  public ActionsAdapter(Context paramContext, List<Action> paramList, BooleanSupplier paramBooleanSupplier1, BooleanSupplier paramBooleanSupplier2) {
    this.mContext = paramContext;
    this.mItems = paramList;
    this.mDeviceProvisioned = paramBooleanSupplier1;
    this.mKeyguardShowing = paramBooleanSupplier2;
  }
  
  public int getCount() {
    boolean bool1 = this.mKeyguardShowing.getAsBoolean();
    boolean bool2 = this.mDeviceProvisioned.getAsBoolean();
    byte b1 = 0;
    for (byte b2 = 0; b2 < this.mItems.size(); b2++) {
      Action action = this.mItems.get(b2);
      if (!bool1 || action.showDuringKeyguard())
        if (bool2 || action.showBeforeProvisioning())
          b1++;  
    } 
    return b1;
  }
  
  public boolean isEnabled(int paramInt) {
    return getItem(paramInt).isEnabled();
  }
  
  public boolean areAllItemsEnabled() {
    return false;
  }
  
  public Action getItem(int paramInt) {
    boolean bool1 = this.mKeyguardShowing.getAsBoolean();
    boolean bool2 = this.mDeviceProvisioned.getAsBoolean();
    int i = 0;
    for (byte b = 0; b < this.mItems.size(); b++) {
      Action action = this.mItems.get(b);
      if (!bool1 || action.showDuringKeyguard())
        if (bool2 || action.showBeforeProvisioning()) {
          if (i == paramInt)
            return action; 
          i++;
        }  
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("position ");
    stringBuilder.append(paramInt);
    stringBuilder.append(" out of range of showable actions, filtered count=");
    stringBuilder.append(getCount());
    stringBuilder.append(", keyguardshowing=");
    stringBuilder.append(bool1);
    stringBuilder.append(", provisioned=");
    stringBuilder.append(bool2);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public long getItemId(int paramInt) {
    return paramInt;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup) {
    Action action = getItem(paramInt);
    Context context = this.mContext;
    return action.create(context, paramView, paramViewGroup, LayoutInflater.from(context));
  }
}
