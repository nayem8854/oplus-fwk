package com.android.internal.globalactions;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public interface Action {
  View create(Context paramContext, View paramView, ViewGroup paramViewGroup, LayoutInflater paramLayoutInflater);
  
  CharSequence getLabelForAccessibility(Context paramContext);
  
  boolean isEnabled();
  
  void onPress();
  
  boolean showBeforeProvisioning();
  
  boolean showDuringKeyguard();
}
