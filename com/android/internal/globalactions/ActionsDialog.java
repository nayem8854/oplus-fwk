package com.android.internal.globalactions;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.accessibility.AccessibilityEvent;
import android.widget.ListView;
import com.android.internal.app.AlertController;

public final class ActionsDialog extends Dialog implements DialogInterface {
  private final ActionsAdapter mAdapter;
  
  private final AlertController mAlert;
  
  private final Context mContext;
  
  public ActionsDialog(Context paramContext, AlertController.AlertParams paramAlertParams) {
    super(paramContext, getDialogTheme(paramContext));
    this.mContext = paramContext = getContext();
    this.mAlert = AlertController.create(paramContext, this, getWindow());
    this.mAdapter = (ActionsAdapter)paramAlertParams.mAdapter;
    paramAlertParams.apply(this.mAlert);
  }
  
  private static int getDialogTheme(Context paramContext) {
    TypedValue typedValue = new TypedValue();
    paramContext.getTheme().resolveAttribute(16843529, typedValue, true);
    return typedValue.resourceId;
  }
  
  protected void onStart() {
    setCanceledOnTouchOutside(true);
    super.onStart();
  }
  
  public ListView getListView() {
    return this.mAlert.getListView();
  }
  
  protected void onCreate(Bundle paramBundle) {
    super.onCreate(paramBundle);
    this.mAlert.installContent();
  }
  
  public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent) {
    if (paramAccessibilityEvent.getEventType() == 32)
      for (byte b = 0; b < this.mAdapter.getCount(); b++) {
        ActionsAdapter actionsAdapter = this.mAdapter;
        CharSequence charSequence = actionsAdapter.getItem(b).getLabelForAccessibility(getContext());
        if (charSequence != null)
          paramAccessibilityEvent.getText().add(charSequence); 
      }  
    return super.dispatchPopulateAccessibilityEvent(paramAccessibilityEvent);
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
    if (this.mAlert.onKeyDown(paramInt, paramKeyEvent))
      return true; 
    return super.onKeyDown(paramInt, paramKeyEvent);
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent) {
    if (this.mAlert.onKeyUp(paramInt, paramKeyEvent))
      return true; 
    return super.onKeyUp(paramInt, paramKeyEvent);
  }
}
