package com.android.internal.globalactions;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public abstract class ToggleAction implements Action {
  private static final String TAG = "ToggleAction";
  
  protected int mDisabledIconResid;
  
  protected int mDisabledStatusMessageResId;
  
  protected int mEnabledIconResId;
  
  protected int mEnabledStatusMessageResId;
  
  protected int mMessageResId;
  
  class State extends Enum<State> {
    private static final State[] $VALUES;
    
    public static State valueOf(String param1String) {
      return Enum.<State>valueOf(State.class, param1String);
    }
    
    public static State[] values() {
      return (State[])$VALUES.clone();
    }
    
    public static final State Off = new State("Off", 0, false);
    
    public static final State On;
    
    public static final State TurningOff = new State("TurningOff", 2, true);
    
    public static final State TurningOn = new State("TurningOn", 1, true);
    
    private final boolean inTransition;
    
    static {
      State state = new State("On", 3, false);
      $VALUES = new State[] { Off, TurningOn, TurningOff, state };
    }
    
    private State(ToggleAction this$0, int param1Int, boolean param1Boolean) {
      super((String)this$0, param1Int);
      this.inTransition = param1Boolean;
    }
    
    public boolean inTransition() {
      return this.inTransition;
    }
  }
  
  protected State mState = State.Off;
  
  public ToggleAction(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    this.mEnabledIconResId = paramInt1;
    this.mDisabledIconResid = paramInt2;
    this.mMessageResId = paramInt3;
    this.mEnabledStatusMessageResId = paramInt4;
    this.mDisabledStatusMessageResId = paramInt5;
  }
  
  void willCreate() {}
  
  public CharSequence getLabelForAccessibility(Context paramContext) {
    return paramContext.getString(this.mMessageResId);
  }
  
  public View create(Context paramContext, View paramView, ViewGroup paramViewGroup, LayoutInflater paramLayoutInflater) {
    int i;
    willCreate();
    paramView = paramLayoutInflater.inflate(17367167, paramViewGroup, false);
    ImageView imageView = paramView.<ImageView>findViewById(16908294);
    TextView textView2 = paramView.<TextView>findViewById(16908299);
    TextView textView1 = paramView.<TextView>findViewById(16909480);
    boolean bool = isEnabled();
    if (textView2 != null) {
      textView2.setText(this.mMessageResId);
      textView2.setEnabled(bool);
    } 
    if (this.mState == State.On || this.mState == State.TurningOn) {
      i = 1;
    } else {
      i = 0;
    } 
    if (imageView != null) {
      int j;
      if (i) {
        j = this.mEnabledIconResId;
      } else {
        j = this.mDisabledIconResid;
      } 
      imageView.setImageDrawable(paramContext.getDrawable(j));
      imageView.setEnabled(bool);
    } 
    if (textView1 != null) {
      if (i) {
        i = this.mEnabledStatusMessageResId;
      } else {
        i = this.mDisabledStatusMessageResId;
      } 
      textView1.setText(i);
      textView1.setVisibility(0);
      textView1.setEnabled(bool);
    } 
    paramView.setEnabled(bool);
    return paramView;
  }
  
  public final void onPress() {
    boolean bool;
    if (this.mState.inTransition()) {
      Log.w("ToggleAction", "shouldn't be able to toggle when in transition");
      return;
    } 
    if (this.mState != State.On) {
      bool = true;
    } else {
      bool = false;
    } 
    onToggle(bool);
    changeStateFromPress(bool);
  }
  
  public boolean isEnabled() {
    return this.mState.inTransition() ^ true;
  }
  
  protected void changeStateFromPress(boolean paramBoolean) {
    State state;
    if (paramBoolean) {
      state = State.On;
    } else {
      state = State.Off;
    } 
    this.mState = state;
  }
  
  public void updateState(State paramState) {
    this.mState = paramState;
  }
  
  public abstract void onToggle(boolean paramBoolean);
}
