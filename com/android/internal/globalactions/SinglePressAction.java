package com.android.internal.globalactions;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public abstract class SinglePressAction implements Action {
  private final Drawable mIcon;
  
  private final int mIconResId;
  
  private final CharSequence mMessage;
  
  private final int mMessageResId;
  
  protected SinglePressAction(int paramInt1, int paramInt2) {
    this.mIconResId = paramInt1;
    this.mMessageResId = paramInt2;
    this.mMessage = null;
    this.mIcon = null;
  }
  
  protected SinglePressAction(int paramInt, Drawable paramDrawable, CharSequence paramCharSequence) {
    this.mIconResId = paramInt;
    this.mMessageResId = 0;
    this.mMessage = paramCharSequence;
    this.mIcon = paramDrawable;
  }
  
  public boolean isEnabled() {
    return true;
  }
  
  public String getStatus() {
    return null;
  }
  
  public CharSequence getLabelForAccessibility(Context paramContext) {
    CharSequence charSequence = this.mMessage;
    if (charSequence != null)
      return charSequence; 
    return paramContext.getString(this.mMessageResId);
  }
  
  public View create(Context paramContext, View paramView, ViewGroup paramViewGroup, LayoutInflater paramLayoutInflater) {
    View view = paramLayoutInflater.inflate(17367167, paramViewGroup, false);
    ImageView imageView = view.<ImageView>findViewById(16908294);
    paramView = view.<TextView>findViewById(16908299);
    TextView textView = view.<TextView>findViewById(16909480);
    String str = getStatus();
    if (textView != null)
      if (!TextUtils.isEmpty(str)) {
        textView.setText(str);
      } else {
        textView.setVisibility(8);
      }  
    if (imageView != null) {
      Drawable drawable = this.mIcon;
      if (drawable != null) {
        imageView.setImageDrawable(drawable);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
      } else {
        int i = this.mIconResId;
        if (i != 0)
          imageView.setImageDrawable(paramContext.getDrawable(i)); 
      } 
    } 
    if (paramView != null) {
      CharSequence charSequence = this.mMessage;
      if (charSequence != null) {
        paramView.setText(charSequence);
      } else {
        paramView.setText(this.mMessageResId);
      } 
    } 
    return view;
  }
  
  public abstract void onPress();
}
