package com.android.internal.graphics;

import android.animation.AnimationHandler;
import android.view.Choreographer;

public final class SfVsyncFrameCallbackProvider implements AnimationHandler.AnimationFrameCallbackProvider {
  private final Choreographer mChoreographer;
  
  public SfVsyncFrameCallbackProvider() {
    this.mChoreographer = Choreographer.getSfInstance();
  }
  
  public SfVsyncFrameCallbackProvider(Choreographer paramChoreographer) {
    this.mChoreographer = paramChoreographer;
  }
  
  public void postFrameCallback(Choreographer.FrameCallback paramFrameCallback) {
    this.mChoreographer.postFrameCallback(paramFrameCallback);
  }
  
  public void postCommitCallback(Runnable paramRunnable) {
    this.mChoreographer.postCallback(4, paramRunnable, null);
  }
  
  public long getFrameTime() {
    return this.mChoreographer.getFrameTime();
  }
  
  public long getFrameDelay() {
    return Choreographer.getFrameDelay();
  }
  
  public void setFrameDelay(long paramLong) {
    Choreographer.setFrameDelay(paramLong);
  }
}
