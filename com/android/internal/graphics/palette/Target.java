package com.android.internal.graphics.palette;

public final class Target {
  public static final Target DARK_MUTED;
  
  public static final Target DARK_VIBRANT;
  
  static final int INDEX_MAX = 2;
  
  static final int INDEX_MIN = 0;
  
  static final int INDEX_TARGET = 1;
  
  static final int INDEX_WEIGHT_LUMA = 1;
  
  static final int INDEX_WEIGHT_POP = 2;
  
  static final int INDEX_WEIGHT_SAT = 0;
  
  public static final Target LIGHT_MUTED;
  
  public static final Target LIGHT_VIBRANT;
  
  private static final float MAX_DARK_LUMA = 0.45F;
  
  private static final float MAX_MUTED_SATURATION = 0.4F;
  
  private static final float MAX_NORMAL_LUMA = 0.7F;
  
  private static final float MIN_LIGHT_LUMA = 0.55F;
  
  private static final float MIN_NORMAL_LUMA = 0.3F;
  
  private static final float MIN_VIBRANT_SATURATION = 0.35F;
  
  public static final Target MUTED;
  
  private static final float TARGET_DARK_LUMA = 0.26F;
  
  private static final float TARGET_LIGHT_LUMA = 0.74F;
  
  private static final float TARGET_MUTED_SATURATION = 0.3F;
  
  private static final float TARGET_NORMAL_LUMA = 0.5F;
  
  private static final float TARGET_VIBRANT_SATURATION = 1.0F;
  
  public static final Target VIBRANT;
  
  private static final float WEIGHT_LUMA = 0.52F;
  
  private static final float WEIGHT_POPULATION = 0.24F;
  
  private static final float WEIGHT_SATURATION = 0.24F;
  
  boolean mIsExclusive;
  
  final float[] mLightnessTargets;
  
  final float[] mSaturationTargets;
  
  final float[] mWeights;
  
  static {
    Target target = new Target();
    setDefaultLightLightnessValues(target);
    setDefaultVibrantSaturationValues(LIGHT_VIBRANT);
    VIBRANT = target = new Target();
    setDefaultNormalLightnessValues(target);
    setDefaultVibrantSaturationValues(VIBRANT);
    DARK_VIBRANT = target = new Target();
    setDefaultDarkLightnessValues(target);
    setDefaultVibrantSaturationValues(DARK_VIBRANT);
    LIGHT_MUTED = target = new Target();
    setDefaultLightLightnessValues(target);
    setDefaultMutedSaturationValues(LIGHT_MUTED);
    MUTED = target = new Target();
    setDefaultNormalLightnessValues(target);
    setDefaultMutedSaturationValues(MUTED);
    DARK_MUTED = target = new Target();
    setDefaultDarkLightnessValues(target);
    setDefaultMutedSaturationValues(DARK_MUTED);
  }
  
  Target() {
    float[] arrayOfFloat = new float[3];
    this.mLightnessTargets = new float[3];
    this.mWeights = new float[3];
    this.mIsExclusive = true;
    setTargetDefaultValues(arrayOfFloat);
    setTargetDefaultValues(this.mLightnessTargets);
    setDefaultWeights();
  }
  
  Target(Target paramTarget) {
    float[] arrayOfFloat2 = new float[3];
    this.mLightnessTargets = new float[3];
    this.mWeights = new float[3];
    this.mIsExclusive = true;
    System.arraycopy(paramTarget.mSaturationTargets, 0, arrayOfFloat2, 0, arrayOfFloat2.length);
    float[] arrayOfFloat3 = paramTarget.mLightnessTargets;
    arrayOfFloat2 = this.mLightnessTargets;
    System.arraycopy(arrayOfFloat3, 0, arrayOfFloat2, 0, arrayOfFloat2.length);
    float[] arrayOfFloat1 = paramTarget.mWeights;
    arrayOfFloat2 = this.mWeights;
    System.arraycopy(arrayOfFloat1, 0, arrayOfFloat2, 0, arrayOfFloat2.length);
  }
  
  public float getMinimumSaturation() {
    return this.mSaturationTargets[0];
  }
  
  public float getTargetSaturation() {
    return this.mSaturationTargets[1];
  }
  
  public float getMaximumSaturation() {
    return this.mSaturationTargets[2];
  }
  
  public float getMinimumLightness() {
    return this.mLightnessTargets[0];
  }
  
  public float getTargetLightness() {
    return this.mLightnessTargets[1];
  }
  
  public float getMaximumLightness() {
    return this.mLightnessTargets[2];
  }
  
  public float getSaturationWeight() {
    return this.mWeights[0];
  }
  
  public float getLightnessWeight() {
    return this.mWeights[1];
  }
  
  public float getPopulationWeight() {
    return this.mWeights[2];
  }
  
  public boolean isExclusive() {
    return this.mIsExclusive;
  }
  
  private static void setTargetDefaultValues(float[] paramArrayOffloat) {
    paramArrayOffloat[0] = 0.0F;
    paramArrayOffloat[1] = 0.5F;
    paramArrayOffloat[2] = 1.0F;
  }
  
  private void setDefaultWeights() {
    float[] arrayOfFloat = this.mWeights;
    arrayOfFloat[0] = 0.24F;
    arrayOfFloat[1] = 0.52F;
    arrayOfFloat[2] = 0.24F;
  }
  
  void normalizeWeights() {
    float f = 0.0F;
    byte b;
    int i;
    for (b = 0, i = this.mWeights.length; b < i; b++, f = f2) {
      float f1 = this.mWeights[b];
      float f2 = f;
      if (f1 > 0.0F)
        f2 = f + f1; 
    } 
    if (f != 0.0F)
      for (b = 0, i = this.mWeights.length; b < i; b++) {
        float[] arrayOfFloat = this.mWeights;
        if (arrayOfFloat[b] > 0.0F)
          arrayOfFloat[b] = arrayOfFloat[b] / f; 
      }  
  }
  
  private static void setDefaultDarkLightnessValues(Target paramTarget) {
    float[] arrayOfFloat = paramTarget.mLightnessTargets;
    arrayOfFloat[1] = 0.26F;
    arrayOfFloat[2] = 0.45F;
  }
  
  private static void setDefaultNormalLightnessValues(Target paramTarget) {
    float[] arrayOfFloat = paramTarget.mLightnessTargets;
    arrayOfFloat[0] = 0.3F;
    arrayOfFloat[1] = 0.5F;
    arrayOfFloat[2] = 0.7F;
  }
  
  private static void setDefaultLightLightnessValues(Target paramTarget) {
    float[] arrayOfFloat = paramTarget.mLightnessTargets;
    arrayOfFloat[0] = 0.55F;
    arrayOfFloat[1] = 0.74F;
  }
  
  private static void setDefaultVibrantSaturationValues(Target paramTarget) {
    float[] arrayOfFloat = paramTarget.mSaturationTargets;
    arrayOfFloat[0] = 0.35F;
    arrayOfFloat[1] = 1.0F;
  }
  
  private static void setDefaultMutedSaturationValues(Target paramTarget) {
    float[] arrayOfFloat = paramTarget.mSaturationTargets;
    arrayOfFloat[1] = 0.3F;
    arrayOfFloat[2] = 0.4F;
  }
  
  public static final class Builder {
    private final Target mTarget;
    
    public Builder() {
      this.mTarget = new Target();
    }
    
    public Builder(Target param1Target) {
      this.mTarget = new Target(param1Target);
    }
    
    public Builder setMinimumSaturation(float param1Float) {
      this.mTarget.mSaturationTargets[0] = param1Float;
      return this;
    }
    
    public Builder setTargetSaturation(float param1Float) {
      this.mTarget.mSaturationTargets[1] = param1Float;
      return this;
    }
    
    public Builder setMaximumSaturation(float param1Float) {
      this.mTarget.mSaturationTargets[2] = param1Float;
      return this;
    }
    
    public Builder setMinimumLightness(float param1Float) {
      this.mTarget.mLightnessTargets[0] = param1Float;
      return this;
    }
    
    public Builder setTargetLightness(float param1Float) {
      this.mTarget.mLightnessTargets[1] = param1Float;
      return this;
    }
    
    public Builder setMaximumLightness(float param1Float) {
      this.mTarget.mLightnessTargets[2] = param1Float;
      return this;
    }
    
    public Builder setSaturationWeight(float param1Float) {
      this.mTarget.mWeights[0] = param1Float;
      return this;
    }
    
    public Builder setLightnessWeight(float param1Float) {
      this.mTarget.mWeights[1] = param1Float;
      return this;
    }
    
    public Builder setPopulationWeight(float param1Float) {
      this.mTarget.mWeights[2] = param1Float;
      return this;
    }
    
    public Builder setExclusive(boolean param1Boolean) {
      this.mTarget.mIsExclusive = param1Boolean;
      return this;
    }
    
    public Target build() {
      return this.mTarget;
    }
  }
}
