package com.android.internal.graphics.palette;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.util.ArrayMap;
import android.util.SparseBooleanArray;
import com.android.internal.graphics.ColorUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

public final class Palette {
  static final int DEFAULT_CALCULATE_NUMBER_COLORS = 16;
  
  public static Builder from(Bitmap paramBitmap) {
    return new Builder(paramBitmap);
  }
  
  public static Palette from(List<Swatch> paramList) {
    return (new Builder(paramList)).generate();
  }
  
  @Deprecated
  public static Palette generate(Bitmap paramBitmap) {
    return from(paramBitmap).generate();
  }
  
  @Deprecated
  public static Palette generate(Bitmap paramBitmap, int paramInt) {
    return from(paramBitmap).maximumColorCount(paramInt).generate();
  }
  
  @Deprecated
  public static AsyncTask<Bitmap, Void, Palette> generateAsync(Bitmap paramBitmap, PaletteAsyncListener paramPaletteAsyncListener) {
    return from(paramBitmap).generate(paramPaletteAsyncListener);
  }
  
  @Deprecated
  public static AsyncTask<Bitmap, Void, Palette> generateAsync(Bitmap paramBitmap, int paramInt, PaletteAsyncListener paramPaletteAsyncListener) {
    return from(paramBitmap).maximumColorCount(paramInt).generate(paramPaletteAsyncListener);
  }
  
  Palette(List<Swatch> paramList, List<Target> paramList1) {
    this.mSwatches = paramList;
    this.mTargets = paramList1;
    this.mUsedColors = new SparseBooleanArray();
    this.mSelectedSwatches = new ArrayMap<>();
    this.mDominantSwatch = findDominantSwatch();
  }
  
  public List<Swatch> getSwatches() {
    return Collections.unmodifiableList(this.mSwatches);
  }
  
  public List<Target> getTargets() {
    return Collections.unmodifiableList(this.mTargets);
  }
  
  public Swatch getVibrantSwatch() {
    return getSwatchForTarget(Target.VIBRANT);
  }
  
  public Swatch getLightVibrantSwatch() {
    return getSwatchForTarget(Target.LIGHT_VIBRANT);
  }
  
  public Swatch getDarkVibrantSwatch() {
    return getSwatchForTarget(Target.DARK_VIBRANT);
  }
  
  public Swatch getMutedSwatch() {
    return getSwatchForTarget(Target.MUTED);
  }
  
  public Swatch getLightMutedSwatch() {
    return getSwatchForTarget(Target.LIGHT_MUTED);
  }
  
  public Swatch getDarkMutedSwatch() {
    return getSwatchForTarget(Target.DARK_MUTED);
  }
  
  public int getVibrantColor(int paramInt) {
    return getColorForTarget(Target.VIBRANT, paramInt);
  }
  
  public int getLightVibrantColor(int paramInt) {
    return getColorForTarget(Target.LIGHT_VIBRANT, paramInt);
  }
  
  public int getDarkVibrantColor(int paramInt) {
    return getColorForTarget(Target.DARK_VIBRANT, paramInt);
  }
  
  public int getMutedColor(int paramInt) {
    return getColorForTarget(Target.MUTED, paramInt);
  }
  
  public int getLightMutedColor(int paramInt) {
    return getColorForTarget(Target.LIGHT_MUTED, paramInt);
  }
  
  public int getDarkMutedColor(int paramInt) {
    return getColorForTarget(Target.DARK_MUTED, paramInt);
  }
  
  public Swatch getSwatchForTarget(Target paramTarget) {
    return this.mSelectedSwatches.get(paramTarget);
  }
  
  public int getColorForTarget(Target paramTarget, int paramInt) {
    Swatch swatch = getSwatchForTarget(paramTarget);
    if (swatch != null)
      paramInt = swatch.getRgb(); 
    return paramInt;
  }
  
  public Swatch getDominantSwatch() {
    return this.mDominantSwatch;
  }
  
  public int getDominantColor(int paramInt) {
    Swatch swatch = this.mDominantSwatch;
    if (swatch != null)
      paramInt = swatch.getRgb(); 
    return paramInt;
  }
  
  void generate() {
    byte b;
    int i;
    for (b = 0, i = this.mTargets.size(); b < i; b++) {
      Target target = this.mTargets.get(b);
      target.normalizeWeights();
      this.mSelectedSwatches.put(target, generateScoredTarget(target));
    } 
    this.mUsedColors.clear();
  }
  
  private Swatch generateScoredTarget(Target paramTarget) {
    Swatch swatch = getMaxScoredSwatchForTarget(paramTarget);
    if (swatch != null && paramTarget.isExclusive())
      this.mUsedColors.append(swatch.getRgb(), true); 
    return swatch;
  }
  
  private Swatch getMaxScoredSwatchForTarget(Target paramTarget) {
    // Byte code:
    //   0: fconst_0
    //   1: fstore_2
    //   2: aconst_null
    //   3: astore_3
    //   4: iconst_0
    //   5: istore #4
    //   7: aload_0
    //   8: getfield mSwatches : Ljava/util/List;
    //   11: invokeinterface size : ()I
    //   16: istore #5
    //   18: iload #4
    //   20: iload #5
    //   22: if_icmpge -> 103
    //   25: aload_0
    //   26: getfield mSwatches : Ljava/util/List;
    //   29: iload #4
    //   31: invokeinterface get : (I)Ljava/lang/Object;
    //   36: checkcast com/android/internal/graphics/palette/Palette$Swatch
    //   39: astore #6
    //   41: fload_2
    //   42: fstore #7
    //   44: aload_3
    //   45: astore #8
    //   47: aload_0
    //   48: aload #6
    //   50: aload_1
    //   51: invokespecial shouldBeScoredForTarget : (Lcom/android/internal/graphics/palette/Palette$Swatch;Lcom/android/internal/graphics/palette/Target;)Z
    //   54: ifeq -> 91
    //   57: aload_0
    //   58: aload #6
    //   60: aload_1
    //   61: invokespecial generateScore : (Lcom/android/internal/graphics/palette/Palette$Swatch;Lcom/android/internal/graphics/palette/Target;)F
    //   64: fstore #9
    //   66: aload_3
    //   67: ifnull -> 83
    //   70: fload_2
    //   71: fstore #7
    //   73: aload_3
    //   74: astore #8
    //   76: fload #9
    //   78: fload_2
    //   79: fcmpl
    //   80: ifle -> 91
    //   83: aload #6
    //   85: astore #8
    //   87: fload #9
    //   89: fstore #7
    //   91: iinc #4, 1
    //   94: fload #7
    //   96: fstore_2
    //   97: aload #8
    //   99: astore_3
    //   100: goto -> 18
    //   103: aload_3
    //   104: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #374	-> 0
    //   #375	-> 2
    //   #376	-> 4
    //   #377	-> 25
    //   #378	-> 41
    //   #379	-> 57
    //   #380	-> 66
    //   #381	-> 83
    //   #382	-> 87
    //   #376	-> 91
    //   #386	-> 103
  }
  
  private boolean shouldBeScoredForTarget(Swatch paramSwatch, Target paramTarget) {
    float[] arrayOfFloat = paramSwatch.getHsl();
    null = true;
    if (arrayOfFloat[1] >= paramTarget.getMinimumSaturation() && arrayOfFloat[1] <= paramTarget.getMaximumSaturation()) {
      float f = arrayOfFloat[2];
      if (f >= paramTarget.getMinimumLightness() && arrayOfFloat[2] <= paramTarget.getMaximumLightness()) {
        SparseBooleanArray sparseBooleanArray = this.mUsedColors;
        if (!sparseBooleanArray.get(paramSwatch.getRgb()))
          return null; 
      } 
    } 
    return false;
  }
  
  private float generateScore(Swatch paramSwatch, Target paramTarget) {
    boolean bool;
    float[] arrayOfFloat = paramSwatch.getHsl();
    float f1 = 0.0F;
    float f2 = 0.0F;
    float f3 = 0.0F;
    Swatch swatch = this.mDominantSwatch;
    if (swatch != null) {
      bool = swatch.getPopulation();
    } else {
      bool = true;
    } 
    if (paramTarget.getSaturationWeight() > 0.0F) {
      f1 = paramTarget.getSaturationWeight();
      float f = arrayOfFloat[1];
      f1 *= 1.0F - Math.abs(f - paramTarget.getTargetSaturation());
    } 
    if (paramTarget.getLightnessWeight() > 0.0F) {
      f2 = paramTarget.getLightnessWeight();
      float f = arrayOfFloat[2];
      f2 *= 1.0F - Math.abs(f - paramTarget.getTargetLightness());
    } 
    if (paramTarget.getPopulationWeight() > 0.0F) {
      f3 = paramTarget.getPopulationWeight();
      f3 *= paramSwatch.getPopulation() / bool;
    } 
    return f1 + f2 + f3;
  }
  
  private Swatch findDominantSwatch() {
    int i = Integer.MIN_VALUE;
    Swatch swatch = null;
    byte b;
    int j;
    for (b = 0, j = this.mSwatches.size(); b < j; b++, i = k) {
      Swatch swatch1 = this.mSwatches.get(b);
      int k = i;
      if (swatch1.getPopulation() > i) {
        swatch = swatch1;
        k = swatch1.getPopulation();
      } 
    } 
    return swatch;
  }
  
  private static float[] copyHslValues(Swatch paramSwatch) {
    float[] arrayOfFloat = new float[3];
    System.arraycopy(paramSwatch.getHsl(), 0, arrayOfFloat, 0, 3);
    return arrayOfFloat;
  }
  
  public static final class Swatch {
    private final int mBlue;
    
    private int mBodyTextColor;
    
    private boolean mGeneratedTextColors;
    
    private final int mGreen;
    
    private float[] mHsl;
    
    private final int mPopulation;
    
    private final int mRed;
    
    private final int mRgb;
    
    private int mTitleTextColor;
    
    public Swatch(int param1Int1, int param1Int2) {
      this.mRed = Color.red(param1Int1);
      this.mGreen = Color.green(param1Int1);
      this.mBlue = Color.blue(param1Int1);
      this.mRgb = param1Int1;
      this.mPopulation = param1Int2;
    }
    
    Swatch(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      this.mRed = param1Int1;
      this.mGreen = param1Int2;
      this.mBlue = param1Int3;
      this.mRgb = Color.rgb(param1Int1, param1Int2, param1Int3);
      this.mPopulation = param1Int4;
    }
    
    Swatch(float[] param1ArrayOffloat, int param1Int) {
      this(ColorUtils.HSLToColor(param1ArrayOffloat), param1Int);
      this.mHsl = param1ArrayOffloat;
    }
    
    public int getRgb() {
      return this.mRgb;
    }
    
    public float[] getHsl() {
      if (this.mHsl == null)
        this.mHsl = new float[3]; 
      ColorUtils.RGBToHSL(this.mRed, this.mGreen, this.mBlue, this.mHsl);
      return this.mHsl;
    }
    
    public int getPopulation() {
      return this.mPopulation;
    }
    
    public int getTitleTextColor() {
      ensureTextColorsGenerated();
      return this.mTitleTextColor;
    }
    
    public int getBodyTextColor() {
      ensureTextColorsGenerated();
      return this.mBodyTextColor;
    }
    
    private void ensureTextColorsGenerated() {
      if (!this.mGeneratedTextColors) {
        int i = ColorUtils.calculateMinimumAlpha(-1, this.mRgb, 4.5F);
        int j = ColorUtils.calculateMinimumAlpha(-1, this.mRgb, 3.0F);
        if (i != -1 && j != -1) {
          this.mBodyTextColor = ColorUtils.setAlphaComponent(-1, i);
          this.mTitleTextColor = ColorUtils.setAlphaComponent(-1, j);
          this.mGeneratedTextColors = true;
          return;
        } 
        int k = ColorUtils.calculateMinimumAlpha(-16777216, this.mRgb, 4.5F);
        int m = ColorUtils.calculateMinimumAlpha(-16777216, this.mRgb, 3.0F);
        if (k != -1 && m != -1) {
          this.mBodyTextColor = ColorUtils.setAlphaComponent(-16777216, k);
          this.mTitleTextColor = ColorUtils.setAlphaComponent(-16777216, m);
          this.mGeneratedTextColors = true;
          return;
        } 
        if (i != -1) {
          i = ColorUtils.setAlphaComponent(-1, i);
        } else {
          i = ColorUtils.setAlphaComponent(-16777216, k);
        } 
        this.mBodyTextColor = i;
        if (j != -1) {
          i = ColorUtils.setAlphaComponent(-1, j);
        } else {
          i = ColorUtils.setAlphaComponent(-16777216, m);
        } 
        this.mTitleTextColor = i;
        this.mGeneratedTextColors = true;
      } 
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder(getClass().getSimpleName());
      stringBuilder.append(" [RGB: #");
      stringBuilder.append(Integer.toHexString(getRgb()));
      stringBuilder.append(']');
      stringBuilder.append(" [HSL: ");
      stringBuilder.append(Arrays.toString(getHsl()));
      stringBuilder.append(']');
      stringBuilder.append(" [Population: ");
      stringBuilder.append(this.mPopulation);
      stringBuilder.append(']');
      stringBuilder.append(" [Title Text: #");
      stringBuilder.append(Integer.toHexString(getTitleTextColor()));
      stringBuilder.append(']');
      stringBuilder.append(" [Body Text: #");
      stringBuilder.append(Integer.toHexString(getBodyTextColor()));
      stringBuilder.append(']');
      return stringBuilder.toString();
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = true;
      if (this == param1Object)
        return true; 
      if (param1Object == null || getClass() != param1Object.getClass())
        return false; 
      param1Object = param1Object;
      if (this.mPopulation != ((Swatch)param1Object).mPopulation || this.mRgb != ((Swatch)param1Object).mRgb)
        bool = false; 
      return bool;
    }
    
    public int hashCode() {
      return this.mRgb * 31 + this.mPopulation;
    }
  }
  
  public static interface PaletteAsyncListener {
    void onGenerated(Palette param1Palette);
  }
  
  public static interface Filter {
    boolean isAllowed(int param1Int, float[] param1ArrayOffloat);
  }
  
  public static final class Builder {
    private final List<Target> mTargets = new ArrayList<>();
    
    private int mMaxColors = 16;
    
    private int mResizeArea = 12544;
    
    private int mResizeMaxDimension = -1;
    
    private final List<Palette.Filter> mFilters = new ArrayList<>();
    
    private final Bitmap mBitmap;
    
    private Quantizer mQuantizer;
    
    private Rect mRegion;
    
    private final List<Palette.Swatch> mSwatches;
    
    public Builder(Bitmap param1Bitmap) {
      if (param1Bitmap != null && !param1Bitmap.isRecycled()) {
        this.mFilters.add(Palette.DEFAULT_FILTER);
        this.mBitmap = param1Bitmap;
        this.mSwatches = null;
        this.mTargets.add(Target.LIGHT_VIBRANT);
        this.mTargets.add(Target.VIBRANT);
        this.mTargets.add(Target.DARK_VIBRANT);
        this.mTargets.add(Target.LIGHT_MUTED);
        this.mTargets.add(Target.MUTED);
        this.mTargets.add(Target.DARK_MUTED);
        return;
      } 
      throw new IllegalArgumentException("Bitmap is not valid");
    }
    
    public Builder(List<Palette.Swatch> param1List) {
      if (param1List != null && !param1List.isEmpty()) {
        this.mFilters.add(Palette.DEFAULT_FILTER);
        this.mSwatches = param1List;
        this.mBitmap = null;
        return;
      } 
      throw new IllegalArgumentException("List of Swatches is not valid");
    }
    
    public Builder maximumColorCount(int param1Int) {
      this.mMaxColors = param1Int;
      return this;
    }
    
    @Deprecated
    public Builder resizeBitmapSize(int param1Int) {
      this.mResizeMaxDimension = param1Int;
      this.mResizeArea = -1;
      return this;
    }
    
    public Builder resizeBitmapArea(int param1Int) {
      this.mResizeArea = param1Int;
      this.mResizeMaxDimension = -1;
      return this;
    }
    
    public Builder clearFilters() {
      this.mFilters.clear();
      return this;
    }
    
    public Builder addFilter(Palette.Filter param1Filter) {
      if (param1Filter != null)
        this.mFilters.add(param1Filter); 
      return this;
    }
    
    public Builder setQuantizer(Quantizer param1Quantizer) {
      this.mQuantizer = param1Quantizer;
      return this;
    }
    
    public Builder setRegion(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      if (this.mBitmap != null) {
        if (this.mRegion == null)
          this.mRegion = new Rect(); 
        this.mRegion.set(0, 0, this.mBitmap.getWidth(), this.mBitmap.getHeight());
        if (!this.mRegion.intersect(param1Int1, param1Int2, param1Int3, param1Int4))
          throw new IllegalArgumentException("The given region must intersect with the Bitmap's dimensions."); 
      } 
      return this;
    }
    
    public Builder clearRegion() {
      this.mRegion = null;
      return this;
    }
    
    public Builder addTarget(Target param1Target) {
      if (!this.mTargets.contains(param1Target))
        this.mTargets.add(param1Target); 
      return this;
    }
    
    public Builder clearTargets() {
      List<Target> list = this.mTargets;
      if (list != null)
        list.clear(); 
      return this;
    }
    
    public Palette generate() {
      List<Palette.Swatch> list;
      Bitmap bitmap = this.mBitmap;
      if (bitmap != null) {
        Palette.Filter[] arrayOfFilter;
        Bitmap bitmap1 = scaleBitmapDown(bitmap);
        if (false)
          throw new NullPointerException(); 
        Rect rect = this.mRegion;
        if (bitmap1 != this.mBitmap && rect != null) {
          double d = bitmap1.getWidth() / this.mBitmap.getWidth();
          rect.left = (int)Math.floor(rect.left * d);
          rect.top = (int)Math.floor(rect.top * d);
          int j = (int)Math.ceil(rect.right * d);
          int k = bitmap1.getWidth();
          rect.right = Math.min(j, k);
          j = (int)Math.ceil(rect.bottom * d);
          k = bitmap1.getHeight();
          rect.bottom = Math.min(j, k);
        } 
        if (this.mQuantizer == null)
          this.mQuantizer = new ColorCutQuantizer(); 
        Quantizer quantizer = this.mQuantizer;
        int arrayOfInt[] = getPixelsFromBitmap(bitmap1), i = this.mMaxColors;
        if (this.mFilters.isEmpty()) {
          rect = null;
        } else {
          List<Palette.Filter> list1 = this.mFilters;
          arrayOfFilter = list1.<Palette.Filter>toArray(new Palette.Filter[list1.size()]);
        } 
        quantizer.quantize(arrayOfInt, i, arrayOfFilter);
        if (bitmap1 != this.mBitmap)
          bitmap1.recycle(); 
        list = this.mQuantizer.getQuantizedColors();
        if (false)
          throw new NullPointerException(); 
      } else {
        list = this.mSwatches;
      } 
      Palette palette = new Palette(list, this.mTargets);
      palette.generate();
      if (false)
        throw new NullPointerException(); 
      return palette;
    }
    
    public AsyncTask<Bitmap, Void, Palette> generate(Palette.PaletteAsyncListener param1PaletteAsyncListener) {
      if (param1PaletteAsyncListener != null) {
        null = new Object(this, param1PaletteAsyncListener);
        Executor executor = AsyncTask.THREAD_POOL_EXECUTOR;
        Bitmap bitmap = this.mBitmap;
        return 













          
          null.executeOnExecutor(executor, (Object[])new Bitmap[] { bitmap });
      } 
      throw new IllegalArgumentException("listener can not be null");
    }
    
    private int[] getPixelsFromBitmap(Bitmap param1Bitmap) {
      int i = param1Bitmap.getWidth();
      int j = param1Bitmap.getHeight();
      int[] arrayOfInt2 = new int[i * j];
      param1Bitmap.getPixels(arrayOfInt2, 0, i, 0, 0, i, j);
      Rect rect = this.mRegion;
      if (rect == null)
        return arrayOfInt2; 
      int k = rect.width();
      int m = this.mRegion.height();
      int[] arrayOfInt1 = new int[k * m];
      for (j = 0; j < m; j++)
        System.arraycopy(arrayOfInt2, (this.mRegion.top + j) * i + this.mRegion.left, arrayOfInt1, j * k, k); 
      return arrayOfInt1;
    }
    
    private Bitmap scaleBitmapDown(Bitmap param1Bitmap) {
      double d2, d1 = -1.0D;
      if (this.mResizeArea > 0) {
        int k = param1Bitmap.getWidth() * param1Bitmap.getHeight();
        int m = this.mResizeArea;
        d2 = d1;
        if (k > m)
          d2 = Math.sqrt(m / k); 
      } else {
        d2 = d1;
        if (this.mResizeMaxDimension > 0) {
          int k = Math.max(param1Bitmap.getWidth(), param1Bitmap.getHeight());
          int m = this.mResizeMaxDimension;
          d2 = d1;
          if (k > m)
            d2 = m / k; 
        } 
      } 
      if (d2 <= 0.0D)
        return param1Bitmap; 
      int j = (int)Math.ceil(param1Bitmap.getWidth() * d2);
      int i = (int)Math.ceil(param1Bitmap.getHeight() * d2);
      return Bitmap.createScaledBitmap(param1Bitmap, j, i, false);
    }
  }
  
  static final Filter DEFAULT_FILTER = (Filter)new Object();
  
  static final int DEFAULT_RESIZE_BITMAP_AREA = 12544;
  
  static final String LOG_TAG = "Palette";
  
  static final boolean LOG_TIMINGS = false;
  
  static final float MIN_CONTRAST_BODY_TEXT = 4.5F;
  
  static final float MIN_CONTRAST_TITLE_TEXT = 3.0F;
  
  private final Swatch mDominantSwatch;
  
  private final Map<Target, Swatch> mSelectedSwatches;
  
  private final List<Swatch> mSwatches;
  
  private final List<Target> mTargets;
  
  private final SparseBooleanArray mUsedColors;
}
