package com.android.internal.graphics.palette;

import java.util.List;

public interface Quantizer {
  List<Palette.Swatch> getQuantizedColors();
  
  void quantize(int[] paramArrayOfint, int paramInt, Palette.Filter[] paramArrayOfFilter);
}
