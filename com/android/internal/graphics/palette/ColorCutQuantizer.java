package com.android.internal.graphics.palette;

import android.graphics.Color;
import android.util.TimingLogger;
import com.android.internal.graphics.ColorUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

final class ColorCutQuantizer implements Quantizer {
  TimingLogger mTimingLogger;
  
  private final float[] mTempHsl = new float[3];
  
  List<Palette.Swatch> mQuantizedColors;
  
  int[] mHistogram;
  
  Palette.Filter[] mFilters;
  
  int[] mColors;
  
  public void quantize(int[] paramArrayOfint, int paramInt, Palette.Filter[] paramArrayOfFilter) {
    boolean bool;
    this.mTimingLogger = null;
    this.mFilters = paramArrayOfFilter;
    int[] arrayOfInt = new int[32768];
    int i;
    for (i = 0; i < paramArrayOfint.length; i++) {
      int m = quantizeFromRgb888(paramArrayOfint[i]);
      paramArrayOfint[i] = m;
      arrayOfInt[m] = arrayOfInt[m] + 1;
    } 
    i = 0;
    int j = 0;
    while (true) {
      int m = arrayOfInt.length;
      bool = false;
      if (j < m) {
        if (arrayOfInt[j] > 0 && shouldIgnoreColor(j))
          arrayOfInt[j] = 0; 
        m = i;
        if (arrayOfInt[j] > 0)
          m = i + 1; 
        j++;
        i = m;
        continue;
      } 
      break;
    } 
    this.mColors = paramArrayOfint = new int[i];
    int k = 0;
    for (j = 0; j < arrayOfInt.length; j++, k = m) {
      int m = k;
      if (arrayOfInt[j] > 0) {
        paramArrayOfint[k] = j;
        m = k + 1;
      } 
    } 
    if (i <= paramInt) {
      this.mQuantizedColors = new ArrayList<>();
      for (i = paramArrayOfint.length, paramInt = bool; paramInt < i; ) {
        j = paramArrayOfint[paramInt];
        this.mQuantizedColors.add(new Palette.Swatch(approximateToRgb888(j), arrayOfInt[j]));
        paramInt++;
      } 
    } else {
      this.mQuantizedColors = quantizePixels(paramInt);
    } 
  }
  
  public List<Palette.Swatch> getQuantizedColors() {
    return this.mQuantizedColors;
  }
  
  private List<Palette.Swatch> quantizePixels(int paramInt) {
    PriorityQueue<Vbox> priorityQueue = new PriorityQueue<>(paramInt, VBOX_COMPARATOR_VOLUME);
    priorityQueue.offer(new Vbox(0, this.mColors.length - 1));
    splitBoxes(priorityQueue, paramInt);
    return generateAverageColors(priorityQueue);
  }
  
  private void splitBoxes(PriorityQueue<Vbox> paramPriorityQueue, int paramInt) {
    while (paramPriorityQueue.size() < paramInt) {
      Vbox vbox = paramPriorityQueue.poll();
      if (vbox != null && vbox.canSplit()) {
        paramPriorityQueue.offer(vbox.splitBox());
        paramPriorityQueue.offer(vbox);
        continue;
      } 
      return;
    } 
  }
  
  private List<Palette.Swatch> generateAverageColors(Collection<Vbox> paramCollection) {
    ArrayList<Palette.Swatch> arrayList = new ArrayList(paramCollection.size());
    for (Vbox vbox : paramCollection) {
      Palette.Swatch swatch = vbox.getAverageColor();
      if (!shouldIgnoreColor(swatch))
        arrayList.add(swatch); 
    } 
    return arrayList;
  }
  
  class Vbox {
    private int mLowerIndex;
    
    private int mMaxBlue;
    
    private int mMaxGreen;
    
    private int mMaxRed;
    
    private int mMinBlue;
    
    private int mMinGreen;
    
    private int mMinRed;
    
    private int mPopulation;
    
    private int mUpperIndex;
    
    final ColorCutQuantizer this$0;
    
    Vbox(int param1Int1, int param1Int2) {
      this.mLowerIndex = param1Int1;
      this.mUpperIndex = param1Int2;
      fitBox();
    }
    
    final int getVolume() {
      return (this.mMaxRed - this.mMinRed + 1) * (this.mMaxGreen - this.mMinGreen + 1) * (this.mMaxBlue - this.mMinBlue + 1);
    }
    
    final boolean canSplit() {
      int i = getColorCount();
      boolean bool = true;
      if (i <= 1)
        bool = false; 
      return bool;
    }
    
    final int getColorCount() {
      return this.mUpperIndex + 1 - this.mLowerIndex;
    }
    
    final void fitBox() {
      int[] arrayOfInt1 = ColorCutQuantizer.this.mColors;
      int[] arrayOfInt2 = ColorCutQuantizer.this.mHistogram;
      int i = Integer.MAX_VALUE, j = Integer.MAX_VALUE, k = Integer.MAX_VALUE;
      int m = Integer.MIN_VALUE, n = Integer.MIN_VALUE, i1 = Integer.MIN_VALUE;
      int i2 = 0;
      for (int i3 = this.mLowerIndex; i3 <= this.mUpperIndex; i3++, i = m, j = n, k = i1, m = i2, n = i7, i1 = i6, i2 = i5) {
        int i4 = arrayOfInt1[i3];
        int i5 = i2 + arrayOfInt2[i4];
        int i6 = ColorCutQuantizer.quantizedRed(i4);
        int i7 = ColorCutQuantizer.quantizedGreen(i4);
        i4 = ColorCutQuantizer.quantizedBlue(i4);
        i2 = m;
        if (i6 > m)
          i2 = i6; 
        m = i;
        if (i6 < i)
          m = i6; 
        i6 = i1;
        if (i7 > i1)
          i6 = i7; 
        i1 = k;
        if (i7 < k)
          i1 = i7; 
        i7 = n;
        if (i4 > n)
          i7 = i4; 
        n = j;
        if (i4 < j)
          n = i4; 
      } 
      this.mMinRed = i;
      this.mMaxRed = m;
      this.mMinGreen = k;
      this.mMaxGreen = i1;
      this.mMinBlue = j;
      this.mMaxBlue = n;
      this.mPopulation = i2;
    }
    
    final Vbox splitBox() {
      if (canSplit()) {
        int i = findSplitPoint();
        Vbox vbox = new Vbox(i + 1, this.mUpperIndex);
        this.mUpperIndex = i;
        fitBox();
        return vbox;
      } 
      throw new IllegalStateException("Can not split a box with only 1 color");
    }
    
    final int getLongestColorDimension() {
      int i = this.mMaxRed - this.mMinRed;
      int j = this.mMaxGreen - this.mMinGreen;
      int k = this.mMaxBlue - this.mMinBlue;
      if (i >= j && i >= k)
        return -3; 
      if (j >= i && j >= k)
        return -2; 
      return -1;
    }
    
    final int findSplitPoint() {
      int i = getLongestColorDimension();
      int[] arrayOfInt1 = ColorCutQuantizer.this.mColors;
      int[] arrayOfInt2 = ColorCutQuantizer.this.mHistogram;
      ColorCutQuantizer.modifySignificantOctet(arrayOfInt1, i, this.mLowerIndex, this.mUpperIndex);
      Arrays.sort(arrayOfInt1, this.mLowerIndex, this.mUpperIndex + 1);
      ColorCutQuantizer.modifySignificantOctet(arrayOfInt1, i, this.mLowerIndex, this.mUpperIndex);
      int j = this.mPopulation / 2;
      i = this.mLowerIndex;
      int k = 0;
      while (true) {
        int m = this.mUpperIndex;
        if (i <= m) {
          k += arrayOfInt2[arrayOfInt1[i]];
          if (k >= j)
            return Math.min(m - 1, i); 
          i++;
          continue;
        } 
        break;
      } 
      return this.mLowerIndex;
    }
    
    final Palette.Swatch getAverageColor() {
      int[] arrayOfInt1 = ColorCutQuantizer.this.mColors;
      int[] arrayOfInt2 = ColorCutQuantizer.this.mHistogram;
      int i = 0;
      int j = 0;
      int k = 0;
      int m = 0;
      int n;
      for (n = this.mLowerIndex; n <= this.mUpperIndex; n++) {
        int i1 = arrayOfInt1[n];
        int i2 = arrayOfInt2[i1];
        m += i2;
        i += ColorCutQuantizer.quantizedRed(i1) * i2;
        j += ColorCutQuantizer.quantizedGreen(i1) * i2;
        k += ColorCutQuantizer.quantizedBlue(i1) * i2;
      } 
      n = Math.round(i / m);
      j = Math.round(j / m);
      k = Math.round(k / m);
      return new Palette.Swatch(ColorCutQuantizer.approximateToRgb888(n, j, k), m);
    }
  }
  
  static void modifySignificantOctet(int[] paramArrayOfint, int paramInt1, int paramInt2, int paramInt3) {
    if (paramInt1 != -2) {
      if (paramInt1 == -1)
        for (paramInt1 = paramInt2; paramInt1 <= paramInt3; paramInt1++) {
          int i = paramArrayOfint[paramInt1];
          int j = quantizedBlue(i);
          paramInt2 = quantizedGreen(i);
          paramArrayOfint[paramInt1] = j << 10 | paramInt2 << 5 | quantizedRed(i);
        }  
    } else {
      for (paramInt1 = paramInt2; paramInt1 <= paramInt3; paramInt1++) {
        int i = paramArrayOfint[paramInt1];
        paramInt2 = quantizedGreen(i);
        int j = quantizedRed(i);
        paramArrayOfint[paramInt1] = paramInt2 << 10 | j << 5 | quantizedBlue(i);
      } 
    } 
  }
  
  private boolean shouldIgnoreColor(int paramInt) {
    paramInt = approximateToRgb888(paramInt);
    ColorUtils.colorToHSL(paramInt, this.mTempHsl);
    return shouldIgnoreColor(paramInt, this.mTempHsl);
  }
  
  private boolean shouldIgnoreColor(Palette.Swatch paramSwatch) {
    return shouldIgnoreColor(paramSwatch.getRgb(), paramSwatch.getHsl());
  }
  
  private boolean shouldIgnoreColor(int paramInt, float[] paramArrayOffloat) {
    Palette.Filter[] arrayOfFilter = this.mFilters;
    if (arrayOfFilter != null && arrayOfFilter.length > 0) {
      byte b;
      int i;
      for (b = 0, i = arrayOfFilter.length; b < i; b++) {
        if (!this.mFilters[b].isAllowed(paramInt, paramArrayOffloat))
          return true; 
      } 
    } 
    return false;
  }
  
  private static final Comparator<Vbox> VBOX_COMPARATOR_VOLUME = (Comparator<Vbox>)new Object();
  
  private static final int QUANTIZE_WORD_WIDTH = 5;
  
  private static final int QUANTIZE_WORD_MASK = 31;
  
  private static final boolean LOG_TIMINGS = false;
  
  private static final String LOG_TAG = "ColorCutQuantizer";
  
  static final int COMPONENT_RED = -3;
  
  static final int COMPONENT_GREEN = -2;
  
  static final int COMPONENT_BLUE = -1;
  
  private static int quantizeFromRgb888(int paramInt) {
    int i = modifyWordWidth(Color.red(paramInt), 8, 5);
    int j = modifyWordWidth(Color.green(paramInt), 8, 5);
    paramInt = modifyWordWidth(Color.blue(paramInt), 8, 5);
    return i << 10 | j << 5 | paramInt;
  }
  
  static int approximateToRgb888(int paramInt1, int paramInt2, int paramInt3) {
    paramInt1 = modifyWordWidth(paramInt1, 5, 8);
    paramInt2 = modifyWordWidth(paramInt2, 5, 8);
    paramInt3 = modifyWordWidth(paramInt3, 5, 8);
    return Color.rgb(paramInt1, paramInt2, paramInt3);
  }
  
  private static int approximateToRgb888(int paramInt) {
    return approximateToRgb888(quantizedRed(paramInt), quantizedGreen(paramInt), quantizedBlue(paramInt));
  }
  
  static int quantizedRed(int paramInt) {
    return paramInt >> 10 & 0x1F;
  }
  
  static int quantizedGreen(int paramInt) {
    return paramInt >> 5 & 0x1F;
  }
  
  static int quantizedBlue(int paramInt) {
    return paramInt & 0x1F;
  }
  
  private static int modifyWordWidth(int paramInt1, int paramInt2, int paramInt3) {
    if (paramInt3 > paramInt2) {
      paramInt1 <<= paramInt3 - paramInt2;
    } else {
      paramInt1 >>= paramInt2 - paramInt3;
    } 
    return paramInt1 & (1 << paramInt3) - 1;
  }
}
