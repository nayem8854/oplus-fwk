package com.android.internal.graphics.palette;

import com.android.internal.graphics.ColorUtils;
import com.android.internal.ml.clustering.KMeans;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class VariationalKMeansQuantizer implements Quantizer {
  private static final boolean DEBUG = false;
  
  private static final String TAG = "KMeansQuantizer";
  
  private final int mInitializations;
  
  private final KMeans mKMeans = new KMeans(new Random(0L), 30, 0.0F);
  
  private final float mMinClusterSqDistance;
  
  private List<Palette.Swatch> mQuantizedColors;
  
  public VariationalKMeansQuantizer() {
    this(0.25F);
  }
  
  public VariationalKMeansQuantizer(float paramFloat) {
    this(paramFloat, 1);
  }
  
  public VariationalKMeansQuantizer(float paramFloat, int paramInt) {
    this.mMinClusterSqDistance = paramFloat * paramFloat;
    this.mInitializations = paramInt;
  }
  
  public void quantize(int[] paramArrayOfint, int paramInt, Palette.Filter[] paramArrayOfFilter) {
    float[] arrayOfFloat2 = new float[3];
    arrayOfFloat2[0] = 0.0F;
    arrayOfFloat2[1] = 0.0F;
    arrayOfFloat2[2] = 0.0F;
    float[][] arrayOfFloat = new float[paramArrayOfint.length][3];
    byte b;
    for (b = 0; b < paramArrayOfint.length; b++) {
      ColorUtils.colorToHSL(paramArrayOfint[b], arrayOfFloat2);
      arrayOfFloat[b][0] = arrayOfFloat2[0] / 360.0F;
      arrayOfFloat[b][1] = arrayOfFloat2[1];
      arrayOfFloat[b][2] = arrayOfFloat2[2];
    } 
    List<KMeans.Mean> list = getOptimalKMeans(paramInt, arrayOfFloat);
    float[] arrayOfFloat1;
    for (b = 0, arrayOfFloat1 = arrayOfFloat2; b < list.size(); b++) {
      KMeans.Mean mean = list.get(b);
      float[] arrayOfFloat3 = mean.getCentroid();
      for (paramInt = b + 1; paramInt < list.size(); paramInt++) {
        KMeans.Mean mean1 = list.get(paramInt);
        float[] arrayOfFloat4 = mean1.getCentroid();
        float f = KMeans.sqDistance(arrayOfFloat3, arrayOfFloat4);
        if (f < this.mMinClusterSqDistance) {
          list.remove(mean1);
          mean.getItems().addAll(mean1.getItems());
          for (byte b1 = 0; b1 < arrayOfFloat3.length; b1++)
            arrayOfFloat3[b1] = (float)(arrayOfFloat3[b1] + (arrayOfFloat4[b1] - arrayOfFloat3[b1]) / 2.0D); 
          paramInt--;
        } 
      } 
    } 
    this.mQuantizedColors = new ArrayList<>();
    for (KMeans.Mean mean : list) {
      if (mean.getItems().size() == 0)
        continue; 
      float[] arrayOfFloat3 = mean.getCentroid();
      List<Palette.Swatch> list1 = this.mQuantizedColors;
      float f2 = arrayOfFloat3[0], f3 = arrayOfFloat3[1], f1 = arrayOfFloat3[2];
      paramInt = mean.getItems().size();
      Palette.Swatch swatch = new Palette.Swatch(new float[] { f2 * 360.0F, f3, f1 }, paramInt);
      list1.add(swatch);
    } 
  }
  
  private List<KMeans.Mean> getOptimalKMeans(int paramInt, float[][] paramArrayOffloat) {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: ldc2_w -1.7976931348623157E308
    //   5: dstore #4
    //   7: aload_0
    //   8: getfield mInitializations : I
    //   11: istore #6
    //   13: iload #6
    //   15: ifle -> 69
    //   18: aload_0
    //   19: getfield mKMeans : Lcom/android/internal/ml/clustering/KMeans;
    //   22: iload_1
    //   23: aload_2
    //   24: invokevirtual predict : (I[[F)Ljava/util/List;
    //   27: astore #7
    //   29: aload #7
    //   31: invokestatic score : (Ljava/util/List;)D
    //   34: dstore #8
    //   36: aload_3
    //   37: ifnull -> 52
    //   40: dload #4
    //   42: dstore #10
    //   44: dload #8
    //   46: dload #4
    //   48: dcmpl
    //   49: ifle -> 59
    //   52: dload #8
    //   54: dstore #10
    //   56: aload #7
    //   58: astore_3
    //   59: iinc #6, -1
    //   62: dload #10
    //   64: dstore #4
    //   66: goto -> 13
    //   69: aload_3
    //   70: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #128	-> 0
    //   #129	-> 2
    //   #130	-> 7
    //   #131	-> 13
    //   #135	-> 18
    //   #136	-> 29
    //   #137	-> 36
    //   #141	-> 52
    //   #142	-> 56
    //   #144	-> 59
    //   #145	-> 62
    //   #147	-> 69
  }
  
  public List<Palette.Swatch> getQuantizedColors() {
    return this.mQuantizedColors;
  }
}
