package com.android.internal.graphics.drawable;

import android.animation.ValueAnimator;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.util.AttributeSet;
import com.android.internal.R;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class AnimationScaleListDrawable extends DrawableContainer implements Animatable {
  private static final String TAG = "AnimationScaleListDrawable";
  
  private AnimationScaleListState mAnimationScaleListState;
  
  private boolean mMutated;
  
  public AnimationScaleListDrawable() {
    this((AnimationScaleListState)null, (Resources)null);
  }
  
  private AnimationScaleListDrawable(AnimationScaleListState paramAnimationScaleListState, Resources paramResources) {
    paramAnimationScaleListState = new AnimationScaleListState(paramAnimationScaleListState, this, paramResources);
    setConstantState(paramAnimationScaleListState);
    onStateChange(getState());
  }
  
  protected boolean onStateChange(int[] paramArrayOfint) {
    null = super.onStateChange(paramArrayOfint);
    int i = this.mAnimationScaleListState.getCurrentDrawableIndexBasedOnScale();
    return (selectDrawable(i) || null);
  }
  
  public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Resources.Theme paramTheme) throws XmlPullParserException, IOException {
    TypedArray typedArray = obtainAttributes(paramResources, paramTheme, paramAttributeSet, R.styleable.AnimationScaleListDrawable);
    updateDensity(paramResources);
    typedArray.recycle();
    inflateChildElements(paramResources, paramXmlPullParser, paramAttributeSet, paramTheme);
    onStateChange(getState());
  }
  
  private void inflateChildElements(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Resources.Theme paramTheme) throws XmlPullParserException, IOException {
    AnimationScaleListState animationScaleListState = this.mAnimationScaleListState;
    int i = paramXmlPullParser.getDepth() + 1;
    while (true) {
      int j = paramXmlPullParser.next();
      if (j != 1) {
        int k = paramXmlPullParser.getDepth();
        if (k >= i || j != 3) {
          if (j != 2)
            continue; 
          if (k > i || !paramXmlPullParser.getName().equals("item"))
            continue; 
          TypedArray typedArray = obtainAttributes(paramResources, paramTheme, paramAttributeSet, R.styleable.AnimationScaleListDrawableItem);
          Drawable drawable2 = typedArray.getDrawable(0);
          typedArray.recycle();
          Drawable drawable1 = drawable2;
          if (drawable2 == null) {
            while (true) {
              k = paramXmlPullParser.next();
              if (k == 4)
                continue; 
              break;
            } 
            if (k == 2) {
              drawable1 = Drawable.createFromXmlInner(paramResources, paramXmlPullParser, paramAttributeSet, paramTheme);
            } else {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append(paramXmlPullParser.getPositionDescription());
              stringBuilder.append(": <item> tag requires a 'drawable' attribute or child tag defining a drawable");
              throw new XmlPullParserException(stringBuilder.toString());
            } 
          } 
          animationScaleListState.addDrawable(drawable1);
          continue;
        } 
      } 
      break;
    } 
  }
  
  public Drawable mutate() {
    if (!this.mMutated && super.mutate() == this) {
      this.mAnimationScaleListState.mutate();
      this.mMutated = true;
    } 
    return (Drawable)this;
  }
  
  public void clearMutated() {
    super.clearMutated();
    this.mMutated = false;
  }
  
  public void start() {
    Drawable drawable = getCurrent();
    if (drawable != null && drawable instanceof Animatable)
      ((Animatable)drawable).start(); 
  }
  
  public void stop() {
    Drawable drawable = getCurrent();
    if (drawable != null && drawable instanceof Animatable)
      ((Animatable)drawable).stop(); 
  }
  
  public boolean isRunning() {
    boolean bool1 = false;
    Drawable drawable = getCurrent();
    boolean bool2 = bool1;
    if (drawable != null) {
      bool2 = bool1;
      if (drawable instanceof Animatable)
        bool2 = ((Animatable)drawable).isRunning(); 
    } 
    return bool2;
  }
  
  class AnimationScaleListState extends DrawableContainer.DrawableContainerState {
    int[] mThemeAttrs = null;
    
    int mStaticDrawableIndex = -1;
    
    int mAnimatableDrawableIndex = -1;
    
    AnimationScaleListState(AnimationScaleListDrawable this$0, AnimationScaleListDrawable param1AnimationScaleListDrawable, Resources param1Resources) {
      super((DrawableContainer.DrawableContainerState)this$0, param1AnimationScaleListDrawable, param1Resources);
      if (this$0 != null) {
        this.mThemeAttrs = ((AnimationScaleListState)this$0).mThemeAttrs;
        this.mStaticDrawableIndex = ((AnimationScaleListState)this$0).mStaticDrawableIndex;
        this.mAnimatableDrawableIndex = ((AnimationScaleListState)this$0).mAnimatableDrawableIndex;
      } 
    }
    
    void mutate() {
      int[] arrayOfInt = this.mThemeAttrs;
      if (arrayOfInt != null) {
        arrayOfInt = (int[])arrayOfInt.clone();
      } else {
        arrayOfInt = null;
      } 
      this.mThemeAttrs = arrayOfInt;
    }
    
    int addDrawable(Drawable param1Drawable) {
      int i = addChild(param1Drawable);
      if (param1Drawable instanceof Animatable) {
        this.mAnimatableDrawableIndex = i;
      } else {
        this.mStaticDrawableIndex = i;
      } 
      return i;
    }
    
    public Drawable newDrawable() {
      return (Drawable)new AnimationScaleListDrawable(this, null);
    }
    
    public Drawable newDrawable(Resources param1Resources) {
      return (Drawable)new AnimationScaleListDrawable(this, param1Resources);
    }
    
    public boolean canApplyTheme() {
      return (this.mThemeAttrs != null || super.canApplyTheme());
    }
    
    public int getCurrentDrawableIndexBasedOnScale() {
      if (ValueAnimator.getDurationScale() == 0.0F)
        return this.mStaticDrawableIndex; 
      return this.mAnimatableDrawableIndex;
    }
  }
  
  public void applyTheme(Resources.Theme paramTheme) {
    super.applyTheme(paramTheme);
    onStateChange(getState());
  }
  
  protected void setConstantState(DrawableContainer.DrawableContainerState paramDrawableContainerState) {
    super.setConstantState(paramDrawableContainerState);
    if (paramDrawableContainerState instanceof AnimationScaleListState)
      this.mAnimationScaleListState = (AnimationScaleListState)paramDrawableContainerState; 
  }
}
