package com.android.internal.midi;

import android.media.midi.MidiReceiver;
import java.io.IOException;

public class MidiFramer extends MidiReceiver {
  public String TAG = "MidiFramer";
  
  private byte[] mBuffer = new byte[3];
  
  private int mCount;
  
  private boolean mInSysEx;
  
  private int mNeeded;
  
  private MidiReceiver mReceiver;
  
  private byte mRunningStatus;
  
  public MidiFramer(MidiReceiver paramMidiReceiver) {
    this.mReceiver = paramMidiReceiver;
  }
  
  public static String formatMidiData(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("MIDI+");
    stringBuilder.append(paramInt1);
    stringBuilder.append(" : ");
    String str = stringBuilder.toString();
    for (byte b = 0; b < paramInt2; b++) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(str);
      stringBuilder1.append(String.format("0x%02X, ", new Object[] { Byte.valueOf(paramArrayOfbyte[paramInt1 + b]) }));
      str = stringBuilder1.toString();
    } 
    return str;
  }
  
  public void onSend(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, long paramLong) throws IOException {
    if (this.mInSysEx) {
      i = paramInt1;
    } else {
      i = -1;
    } 
    int i;
    int j;
    for (byte b = 0; b < paramInt2; b++, j = paramInt1) {
      byte b1 = paramArrayOfbyte[i];
      paramInt1 = b1 & 0xFF;
      if (paramInt1 >= 128) {
        if (paramInt1 < 240) {
          this.mRunningStatus = b1;
          this.mCount = 1;
          this.mNeeded = MidiConstants.getBytesPerMessage(b1) - 1;
          paramInt1 = j;
        } else if (paramInt1 < 248) {
          if (paramInt1 == 240) {
            this.mInSysEx = true;
            paramInt1 = i;
          } else if (paramInt1 == 247) {
            paramInt1 = j;
            if (this.mInSysEx) {
              this.mReceiver.send(paramArrayOfbyte, j, i - j + 1, paramLong);
              this.mInSysEx = false;
              paramInt1 = -1;
            } 
          } else {
            this.mBuffer[0] = b1;
            this.mRunningStatus = 0;
            this.mCount = 1;
            this.mNeeded = MidiConstants.getBytesPerMessage(b1) - 1;
            paramInt1 = j;
          } 
        } else {
          if (this.mInSysEx) {
            this.mReceiver.send(paramArrayOfbyte, j, i - j, paramLong);
            paramInt1 = i + 1;
          } else {
            paramInt1 = j;
          } 
          this.mReceiver.send(paramArrayOfbyte, i, 1, paramLong);
        } 
      } else {
        paramInt1 = j;
        if (!this.mInSysEx) {
          byte[] arrayOfByte = this.mBuffer;
          paramInt1 = this.mCount;
          this.mCount = paramInt1 + 1;
          arrayOfByte[paramInt1] = b1;
          int k = this.mNeeded - 1;
          paramInt1 = j;
          if (k == 0) {
            b1 = this.mRunningStatus;
            if (b1 != 0)
              arrayOfByte[0] = b1; 
            this.mReceiver.send(this.mBuffer, 0, this.mCount, paramLong);
            this.mNeeded = MidiConstants.getBytesPerMessage(this.mBuffer[0]) - 1;
            this.mCount = 1;
            paramInt1 = j;
          } 
        } 
      } 
      i++;
    } 
    if (j >= 0 && j < i)
      this.mReceiver.send(paramArrayOfbyte, j, i - j, paramLong); 
  }
}
