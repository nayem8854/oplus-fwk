package com.android.internal.midi;

import android.media.midi.MidiReceiver;
import java.io.IOException;

public class MidiEventScheduler extends EventScheduler {
  private static final int POOL_EVENT_SIZE = 16;
  
  private static final String TAG = "MidiEventScheduler";
  
  private MidiReceiver mReceiver = new SchedulingReceiver();
  
  private class SchedulingReceiver extends MidiReceiver {
    final MidiEventScheduler this$0;
    
    private SchedulingReceiver() {}
    
    public void onSend(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2, long param1Long) throws IOException {
      MidiEventScheduler.MidiEvent midiEvent = MidiEventScheduler.this.createScheduledEvent(param1ArrayOfbyte, param1Int1, param1Int2, param1Long);
      if (midiEvent != null)
        MidiEventScheduler.this.add(midiEvent); 
    }
    
    public void onFlush() {
      MidiEventScheduler.this.flush();
    }
  }
  
  public static class MidiEvent extends EventScheduler.SchedulableEvent {
    public int count = 0;
    
    public byte[] data;
    
    private MidiEvent(int param1Int) {
      super(0L);
      this.data = new byte[param1Int];
    }
    
    private MidiEvent(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2, long param1Long) {
      super(param1Long);
      byte[] arrayOfByte = new byte[param1Int2];
      System.arraycopy(param1ArrayOfbyte, param1Int1, arrayOfByte, 0, param1Int2);
      this.count = param1Int2;
    }
    
    public String toString() {
      String str = "Event: ";
      for (byte b = 0; b < this.count; b++) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(str);
        stringBuilder.append(this.data[b]);
        stringBuilder.append(", ");
        str = stringBuilder.toString();
      } 
      return str;
    }
  }
  
  private MidiEvent createScheduledEvent(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, long paramLong) {
    MidiEvent midiEvent;
    if (paramInt2 > 16) {
      midiEvent = new MidiEvent(paramArrayOfbyte, paramInt1, paramInt2, paramLong);
    } else {
      midiEvent = (MidiEvent)removeEventfromPool();
      if (midiEvent == null)
        midiEvent = new MidiEvent(16); 
      System.arraycopy(paramArrayOfbyte, paramInt1, midiEvent.data, 0, paramInt2);
      midiEvent.count = paramInt2;
      midiEvent.setTimestamp(paramLong);
    } 
    return midiEvent;
  }
  
  public void addEventToPool(EventScheduler.SchedulableEvent paramSchedulableEvent) {
    if (paramSchedulableEvent instanceof MidiEvent) {
      MidiEvent midiEvent = (MidiEvent)paramSchedulableEvent;
      if (midiEvent.data.length == 16)
        super.addEventToPool(paramSchedulableEvent); 
    } 
  }
  
  public MidiReceiver getReceiver() {
    return this.mReceiver;
  }
}
