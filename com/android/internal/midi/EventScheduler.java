package com.android.internal.midi;

import java.util.SortedMap;
import java.util.TreeMap;

public class EventScheduler {
  private final Object mLock = new Object();
  
  private FastEventQueue mEventPool = null;
  
  private int mMaxPoolSize = 200;
  
  private static final long NANOS_PER_MILLI = 1000000L;
  
  private boolean mClosed;
  
  private volatile SortedMap<Long, FastEventQueue> mEventBuffer;
  
  public EventScheduler() {
    this.mEventBuffer = new TreeMap<>();
  }
  
  private class FastEventQueue {
    volatile long mEventsAdded;
    
    volatile long mEventsRemoved;
    
    volatile EventScheduler.SchedulableEvent mFirst;
    
    volatile EventScheduler.SchedulableEvent mLast;
    
    final EventScheduler this$0;
    
    FastEventQueue(EventScheduler.SchedulableEvent param1SchedulableEvent) {
      this.mFirst = param1SchedulableEvent;
      this.mLast = this.mFirst;
      this.mEventsAdded = 1L;
      this.mEventsRemoved = 0L;
    }
    
    int size() {
      return (int)(this.mEventsAdded - this.mEventsRemoved);
    }
    
    public EventScheduler.SchedulableEvent remove() {
      this.mEventsRemoved++;
      EventScheduler.SchedulableEvent schedulableEvent = this.mFirst;
      this.mFirst = schedulableEvent.mNext;
      EventScheduler.SchedulableEvent.access$002(schedulableEvent, null);
      return schedulableEvent;
    }
    
    public void add(EventScheduler.SchedulableEvent param1SchedulableEvent) {
      EventScheduler.SchedulableEvent.access$002(param1SchedulableEvent, null);
      EventScheduler.SchedulableEvent.access$002(this.mLast, param1SchedulableEvent);
      this.mLast = param1SchedulableEvent;
      this.mEventsAdded++;
    }
  }
  
  public static class SchedulableEvent {
    private volatile SchedulableEvent mNext = null;
    
    private long mTimestamp;
    
    public SchedulableEvent(long param1Long) {
      this.mTimestamp = param1Long;
    }
    
    public long getTimestamp() {
      return this.mTimestamp;
    }
    
    public void setTimestamp(long param1Long) {
      this.mTimestamp = param1Long;
    }
  }
  
  public SchedulableEvent removeEventfromPool() {
    SchedulableEvent schedulableEvent1 = null;
    FastEventQueue fastEventQueue = this.mEventPool;
    SchedulableEvent schedulableEvent2 = schedulableEvent1;
    if (fastEventQueue != null) {
      schedulableEvent2 = schedulableEvent1;
      if (fastEventQueue.size() > 1)
        schedulableEvent2 = this.mEventPool.remove(); 
    } 
    return schedulableEvent2;
  }
  
  public void addEventToPool(SchedulableEvent paramSchedulableEvent) {
    FastEventQueue fastEventQueue = this.mEventPool;
    if (fastEventQueue == null) {
      this.mEventPool = new FastEventQueue(paramSchedulableEvent);
    } else if (fastEventQueue.size() < this.mMaxPoolSize) {
      this.mEventPool.add(paramSchedulableEvent);
    } 
  }
  
  public void add(SchedulableEvent paramSchedulableEvent) {
    synchronized (this.mLock) {
      FastEventQueue fastEventQueue = this.mEventBuffer.get(Long.valueOf(paramSchedulableEvent.getTimestamp()));
      if (fastEventQueue == null) {
        long l;
        if (this.mEventBuffer.isEmpty()) {
          l = Long.MAX_VALUE;
        } else {
          l = ((Long)this.mEventBuffer.firstKey()).longValue();
        } 
        fastEventQueue = new FastEventQueue();
        this(this, paramSchedulableEvent);
        this.mEventBuffer.put(Long.valueOf(paramSchedulableEvent.getTimestamp()), fastEventQueue);
        if (paramSchedulableEvent.getTimestamp() < l)
          this.mLock.notify(); 
      } else {
        fastEventQueue.add(paramSchedulableEvent);
      } 
      return;
    } 
  }
  
  private SchedulableEvent removeNextEventLocked(long paramLong) {
    FastEventQueue fastEventQueue = this.mEventBuffer.get(Long.valueOf(paramLong));
    if (fastEventQueue.size() == 1)
      this.mEventBuffer.remove(Long.valueOf(paramLong)); 
    return fastEventQueue.remove();
  }
  
  public SchedulableEvent getNextEvent(long paramLong) {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: aload_0
    //   3: getfield mLock : Ljava/lang/Object;
    //   6: astore #4
    //   8: aload #4
    //   10: monitorenter
    //   11: aload_3
    //   12: astore #5
    //   14: aload_0
    //   15: getfield mEventBuffer : Ljava/util/SortedMap;
    //   18: invokeinterface isEmpty : ()Z
    //   23: ifne -> 61
    //   26: aload_0
    //   27: getfield mEventBuffer : Ljava/util/SortedMap;
    //   30: invokeinterface firstKey : ()Ljava/lang/Object;
    //   35: checkcast java/lang/Long
    //   38: invokevirtual longValue : ()J
    //   41: lstore #6
    //   43: aload_3
    //   44: astore #5
    //   46: lload #6
    //   48: lload_1
    //   49: lcmp
    //   50: ifgt -> 61
    //   53: aload_0
    //   54: lload #6
    //   56: invokespecial removeNextEventLocked : (J)Lcom/android/internal/midi/EventScheduler$SchedulableEvent;
    //   59: astore #5
    //   61: aload #4
    //   63: monitorexit
    //   64: aload #5
    //   66: areturn
    //   67: astore #5
    //   69: aload #4
    //   71: monitorexit
    //   72: aload #5
    //   74: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #188	-> 0
    //   #189	-> 2
    //   #190	-> 11
    //   #191	-> 26
    //   #193	-> 43
    //   #194	-> 53
    //   #197	-> 61
    //   #199	-> 64
    //   #197	-> 67
    // Exception table:
    //   from	to	target	type
    //   14	26	67	finally
    //   26	43	67	finally
    //   53	61	67	finally
    //   61	64	67	finally
    //   69	72	67	finally
  }
  
  public SchedulableEvent waitNextEvent() throws InterruptedException {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: aload_0
    //   3: getfield mLock : Ljava/lang/Object;
    //   6: astore_2
    //   7: aload_2
    //   8: monitorenter
    //   9: aload_1
    //   10: astore_3
    //   11: aload_0
    //   12: getfield mClosed : Z
    //   15: ifne -> 120
    //   18: ldc2_w 2147483647
    //   21: lstore #4
    //   23: aload_0
    //   24: getfield mEventBuffer : Ljava/util/SortedMap;
    //   27: invokeinterface isEmpty : ()Z
    //   32: ifne -> 106
    //   35: invokestatic nanoTime : ()J
    //   38: lstore #6
    //   40: aload_0
    //   41: getfield mEventBuffer : Ljava/util/SortedMap;
    //   44: invokeinterface firstKey : ()Ljava/lang/Object;
    //   49: checkcast java/lang/Long
    //   52: invokevirtual longValue : ()J
    //   55: lstore #4
    //   57: lload #4
    //   59: lload #6
    //   61: lcmp
    //   62: ifgt -> 75
    //   65: aload_0
    //   66: lload #4
    //   68: invokespecial removeNextEventLocked : (J)Lcom/android/internal/midi/EventScheduler$SchedulableEvent;
    //   71: astore_3
    //   72: goto -> 120
    //   75: lload #4
    //   77: lload #6
    //   79: lsub
    //   80: ldc2_w 1000000
    //   83: ldiv
    //   84: lconst_1
    //   85: ladd
    //   86: lstore #6
    //   88: lload #6
    //   90: lstore #4
    //   92: lload #6
    //   94: ldc2_w 2147483647
    //   97: lcmp
    //   98: ifle -> 106
    //   101: ldc2_w 2147483647
    //   104: lstore #4
    //   106: aload_0
    //   107: getfield mLock : Ljava/lang/Object;
    //   110: lload #4
    //   112: l2i
    //   113: i2l
    //   114: invokevirtual wait : (J)V
    //   117: goto -> 9
    //   120: aload_2
    //   121: monitorexit
    //   122: aload_3
    //   123: areturn
    //   124: astore_3
    //   125: aload_2
    //   126: monitorexit
    //   127: aload_3
    //   128: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #211	-> 0
    //   #212	-> 2
    //   #213	-> 9
    //   #214	-> 18
    //   #215	-> 23
    //   #216	-> 35
    //   #217	-> 40
    //   #219	-> 57
    //   #220	-> 65
    //   #221	-> 72
    //   #224	-> 75
    //   #227	-> 75
    //   #229	-> 88
    //   #230	-> 101
    //   #234	-> 106
    //   #235	-> 117
    //   #236	-> 120
    //   #237	-> 122
    //   #236	-> 124
    // Exception table:
    //   from	to	target	type
    //   11	18	124	finally
    //   23	35	124	finally
    //   35	40	124	finally
    //   40	57	124	finally
    //   65	72	124	finally
    //   75	88	124	finally
    //   106	117	124	finally
    //   120	122	124	finally
    //   125	127	124	finally
  }
  
  protected void flush() {
    this.mEventBuffer = new TreeMap<>();
  }
  
  public void close() {
    synchronized (this.mLock) {
      this.mClosed = true;
      this.mLock.notify();
      return;
    } 
  }
}
