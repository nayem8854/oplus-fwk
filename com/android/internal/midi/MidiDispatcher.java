package com.android.internal.midi;

import android.media.midi.MidiReceiver;
import android.media.midi.MidiSender;
import java.io.IOException;
import java.util.concurrent.CopyOnWriteArrayList;

public final class MidiDispatcher extends MidiReceiver {
  private final MidiReceiverFailureHandler mFailureHandler;
  
  private final CopyOnWriteArrayList<MidiReceiver> mReceivers = new CopyOnWriteArrayList<>();
  
  private final MidiSender mSender = new MidiSender() {
      final MidiDispatcher this$0;
      
      public void onConnect(MidiReceiver param1MidiReceiver) {
        MidiDispatcher.this.mReceivers.add(param1MidiReceiver);
      }
      
      public void onDisconnect(MidiReceiver param1MidiReceiver) {
        MidiDispatcher.this.mReceivers.remove(param1MidiReceiver);
      }
    };
  
  public MidiDispatcher() {
    this(null);
  }
  
  public MidiDispatcher(MidiReceiverFailureHandler paramMidiReceiverFailureHandler) {
    this.mFailureHandler = paramMidiReceiverFailureHandler;
  }
  
  public int getReceiverCount() {
    return this.mReceivers.size();
  }
  
  public MidiSender getSender() {
    return this.mSender;
  }
  
  public void onSend(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, long paramLong) throws IOException {
    for (MidiReceiver midiReceiver : this.mReceivers) {
      try {
        midiReceiver.send(paramArrayOfbyte, paramInt1, paramInt2, paramLong);
      } catch (IOException iOException) {
        this.mReceivers.remove(midiReceiver);
        MidiReceiverFailureHandler midiReceiverFailureHandler = this.mFailureHandler;
        if (midiReceiverFailureHandler != null)
          midiReceiverFailureHandler.onReceiverFailure(midiReceiver, iOException); 
      } 
    } 
  }
  
  public void onFlush() throws IOException {
    for (MidiReceiver midiReceiver : this.mReceivers) {
      try {
        midiReceiver.flush();
      } catch (IOException iOException) {
        this.mReceivers.remove(midiReceiver);
        MidiReceiverFailureHandler midiReceiverFailureHandler = this.mFailureHandler;
        if (midiReceiverFailureHandler != null)
          midiReceiverFailureHandler.onReceiverFailure(midiReceiver, iOException); 
      } 
    } 
  }
  
  class MidiReceiverFailureHandler {
    public abstract void onReceiverFailure(MidiReceiver param1MidiReceiver, IOException param1IOException);
  }
}
