package com.android.internal.location;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.WorkSource;

public interface ILocationProvider extends IInterface {
  void sendExtraCommand(String paramString, Bundle paramBundle) throws RemoteException;
  
  void setLocationProviderManager(ILocationProviderManager paramILocationProviderManager) throws RemoteException;
  
  void setRequest(ProviderRequest paramProviderRequest, WorkSource paramWorkSource) throws RemoteException;
  
  class Default implements ILocationProvider {
    public void setLocationProviderManager(ILocationProviderManager param1ILocationProviderManager) throws RemoteException {}
    
    public void setRequest(ProviderRequest param1ProviderRequest, WorkSource param1WorkSource) throws RemoteException {}
    
    public void sendExtraCommand(String param1String, Bundle param1Bundle) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ILocationProvider {
    private static final String DESCRIPTOR = "com.android.internal.location.ILocationProvider";
    
    static final int TRANSACTION_sendExtraCommand = 3;
    
    static final int TRANSACTION_setLocationProviderManager = 1;
    
    static final int TRANSACTION_setRequest = 2;
    
    public Stub() {
      attachInterface(this, "com.android.internal.location.ILocationProvider");
    }
    
    public static ILocationProvider asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.location.ILocationProvider");
      if (iInterface != null && iInterface instanceof ILocationProvider)
        return (ILocationProvider)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "sendExtraCommand";
        } 
        return "setRequest";
      } 
      return "setLocationProviderManager";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("com.android.internal.location.ILocationProvider");
            return true;
          } 
          param1Parcel1.enforceInterface("com.android.internal.location.ILocationProvider");
          String str = param1Parcel1.readString();
          if (param1Parcel1.readInt() != 0) {
            Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel1 = null;
          } 
          sendExtraCommand(str, (Bundle)param1Parcel1);
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.internal.location.ILocationProvider");
        if (param1Parcel1.readInt() != 0) {
          ProviderRequest providerRequest = (ProviderRequest)ProviderRequest.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel2 = null;
        } 
        if (param1Parcel1.readInt() != 0) {
          WorkSource workSource = (WorkSource)WorkSource.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        setRequest((ProviderRequest)param1Parcel2, (WorkSource)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.location.ILocationProvider");
      ILocationProviderManager iLocationProviderManager = ILocationProviderManager.Stub.asInterface(param1Parcel1.readStrongBinder());
      setLocationProviderManager(iLocationProviderManager);
      return true;
    }
    
    private static class Proxy implements ILocationProvider {
      public static ILocationProvider sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.location.ILocationProvider";
      }
      
      public void setLocationProviderManager(ILocationProviderManager param2ILocationProviderManager) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.location.ILocationProvider");
          if (param2ILocationProviderManager != null) {
            iBinder = param2ILocationProviderManager.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && ILocationProvider.Stub.getDefaultImpl() != null) {
            ILocationProvider.Stub.getDefaultImpl().setLocationProviderManager(param2ILocationProviderManager);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setRequest(ProviderRequest param2ProviderRequest, WorkSource param2WorkSource) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.location.ILocationProvider");
          if (param2ProviderRequest != null) {
            parcel.writeInt(1);
            param2ProviderRequest.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2WorkSource != null) {
            parcel.writeInt(1);
            param2WorkSource.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && ILocationProvider.Stub.getDefaultImpl() != null) {
            ILocationProvider.Stub.getDefaultImpl().setRequest(param2ProviderRequest, param2WorkSource);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void sendExtraCommand(String param2String, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.location.ILocationProvider");
          parcel.writeString(param2String);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && ILocationProvider.Stub.getDefaultImpl() != null) {
            ILocationProvider.Stub.getDefaultImpl().sendExtraCommand(param2String, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ILocationProvider param1ILocationProvider) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ILocationProvider != null) {
          Proxy.sDefaultImpl = param1ILocationProvider;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ILocationProvider getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
