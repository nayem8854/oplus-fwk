package com.android.internal.location.gnssmetrics;

import android.app.StatsManager;
import android.content.Context;
import android.location.GnssStatus;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.connectivity.GpsBatteryStats;
import android.util.Base64;
import android.util.Log;
import android.util.StatsEvent;
import android.util.TimeUtils;
import com.android.internal.app.IBatteryStats;
import com.android.internal.location.nano.GnssLogsProto;
import com.android.internal.util.ConcurrentUtils;
import com.android.internal.util.FrameworkStatsLog;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class GnssMetrics {
  private static final int DEFAULT_TIME_BETWEEN_FIXES_MILLISECS = 1000;
  
  private static final int GPS_SIGNAL_QUALITY_GOOD = 1;
  
  private static final int GPS_SIGNAL_QUALITY_POOR = 0;
  
  private static final int GPS_SIGNAL_QUALITY_UNKNOWN = -1;
  
  private static final double HZ_PER_MHZ = 1000000.0D;
  
  private static final double L5_CARRIER_FREQ_RANGE_HIGH_HZ = 1.189E9D;
  
  private static final double L5_CARRIER_FREQ_RANGE_LOW_HZ = 1.164E9D;
  
  public static final int NUM_GPS_SIGNAL_QUALITY_LEVELS = 2;
  
  private static final String TAG = GnssMetrics.class.getSimpleName();
  
  private boolean[] mConstellationTypes;
  
  private GnssPowerMetrics mGnssPowerMetrics;
  
  private long mL5SvStatusReports;
  
  private long mL5SvStatusReportsUsedInFix;
  
  private Statistics mL5TopFourAverageCn0DbmHzReportsStatistics;
  
  private Statistics mLocationFailureReportsStatistics;
  
  private Statistics mLocationFailureStatistics;
  
  private String mLogStartInElapsedRealTime;
  
  private int mNumL5SvStatus;
  
  private int mNumL5SvStatusUsedInFix;
  
  private int mNumSvStatus;
  
  private int mNumSvStatusUsedInFix;
  
  private Statistics mPositionAccuracyMeterStatistics;
  
  private Statistics mPositionAccuracyMetersReportsStatistics;
  
  private StatsManager mStatsManager;
  
  private long mSvStatusReports;
  
  private long mSvStatusReportsUsedInFix;
  
  private Statistics mTimeToFirstFixMilliSReportsStatistics;
  
  private Statistics mTimeToFirstFixSecStatistics;
  
  private Statistics mTopFourAverageCn0DbmHzReportsStatistics;
  
  private Statistics mTopFourAverageCn0Statistics;
  
  private Statistics mTopFourAverageCn0StatisticsL5;
  
  public GnssMetrics(Context paramContext, IBatteryStats paramIBatteryStats) {
    this.mGnssPowerMetrics = new GnssPowerMetrics(paramIBatteryStats);
    this.mLocationFailureStatistics = new Statistics();
    this.mTimeToFirstFixSecStatistics = new Statistics();
    this.mPositionAccuracyMeterStatistics = new Statistics();
    this.mTopFourAverageCn0Statistics = new Statistics();
    this.mTopFourAverageCn0StatisticsL5 = new Statistics();
    reset();
    this.mLocationFailureReportsStatistics = new Statistics();
    this.mTimeToFirstFixMilliSReportsStatistics = new Statistics();
    this.mPositionAccuracyMetersReportsStatistics = new Statistics();
    this.mTopFourAverageCn0DbmHzReportsStatistics = new Statistics();
    this.mL5TopFourAverageCn0DbmHzReportsStatistics = new Statistics();
    this.mStatsManager = (StatsManager)paramContext.getSystemService("stats");
    registerGnssStats();
  }
  
  public void logReceivedLocationStatus(boolean paramBoolean) {
    if (!paramBoolean) {
      this.mLocationFailureStatistics.addItem(1.0D);
      this.mLocationFailureReportsStatistics.addItem(1.0D);
      return;
    } 
    this.mLocationFailureStatistics.addItem(0.0D);
    this.mLocationFailureReportsStatistics.addItem(0.0D);
  }
  
  public void logMissedReports(int paramInt1, int paramInt2) {
    paramInt2 = paramInt2 / Math.max(1000, paramInt1) - 1;
    if (paramInt2 > 0)
      for (paramInt1 = 0; paramInt1 < paramInt2; paramInt1++) {
        this.mLocationFailureStatistics.addItem(1.0D);
        this.mLocationFailureReportsStatistics.addItem(1.0D);
      }  
  }
  
  public void logTimeToFirstFixMilliSecs(int paramInt) {
    this.mTimeToFirstFixSecStatistics.addItem((paramInt / 1000));
    this.mTimeToFirstFixMilliSReportsStatistics.addItem(paramInt);
  }
  
  public void logPositionAccuracyMeters(float paramFloat) {
    this.mPositionAccuracyMeterStatistics.addItem(paramFloat);
    this.mPositionAccuracyMetersReportsStatistics.addItem(paramFloat);
  }
  
  public void logCn0(float[] paramArrayOffloat1, int paramInt, float[] paramArrayOffloat2) {
    logCn0L5(paramInt, paramArrayOffloat1, paramArrayOffloat2);
    if (paramInt == 0 || paramArrayOffloat1 == null || paramArrayOffloat1.length == 0 || paramArrayOffloat1.length < paramInt) {
      if (paramInt == 0)
        this.mGnssPowerMetrics.reportSignalQuality(null, 0); 
      return;
    } 
    paramArrayOffloat1 = Arrays.copyOf(paramArrayOffloat1, paramInt);
    Arrays.sort(paramArrayOffloat1);
    this.mGnssPowerMetrics.reportSignalQuality(paramArrayOffloat1, paramInt);
    if (paramInt < 4)
      return; 
    if (paramArrayOffloat1[paramInt - 4] > 0.0D) {
      double d = 0.0D;
      for (int i = paramInt - 4; i < paramInt; i++)
        d += paramArrayOffloat1[i]; 
      d /= 4.0D;
      this.mTopFourAverageCn0Statistics.addItem(d);
      this.mTopFourAverageCn0DbmHzReportsStatistics.addItem(1000.0D * d);
    } 
  }
  
  private static boolean isL5Sv(float paramFloat) {
    boolean bool;
    if (paramFloat >= 1.164E9D && paramFloat <= 1.189E9D) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void logSvStatus(GnssStatus paramGnssStatus) {
    for (byte b = 0; b < paramGnssStatus.getSatelliteCount(); b++) {
      if (paramGnssStatus.hasCarrierFrequencyHz(b)) {
        this.mNumSvStatus++;
        this.mSvStatusReports++;
        boolean bool = isL5Sv(paramGnssStatus.getCarrierFrequencyHz(b));
        if (bool) {
          this.mNumL5SvStatus++;
          this.mL5SvStatusReports++;
        } 
        if (paramGnssStatus.usedInFix(b)) {
          this.mNumSvStatusUsedInFix++;
          this.mSvStatusReportsUsedInFix++;
          if (bool) {
            this.mNumL5SvStatusUsedInFix++;
            this.mL5SvStatusReportsUsedInFix++;
          } 
        } 
      } 
    } 
  }
  
  private void logCn0L5(int paramInt, float[] paramArrayOffloat1, float[] paramArrayOffloat2) {
    if (paramInt == 0 || paramArrayOffloat1 == null || paramArrayOffloat1.length == 0 || paramArrayOffloat1.length < paramInt || paramArrayOffloat2 == null || paramArrayOffloat2.length == 0 || paramArrayOffloat2.length < paramInt)
      return; 
    ArrayList<Float> arrayList = new ArrayList();
    int i;
    for (i = 0; i < paramInt; i++) {
      if (isL5Sv(paramArrayOffloat2[i]))
        arrayList.add(Float.valueOf(paramArrayOffloat1[i])); 
    } 
    if (arrayList.size() == 0 || arrayList.size() < 4)
      return; 
    i = arrayList.size();
    Collections.sort(arrayList);
    if (((Float)arrayList.get(i - 4)).floatValue() > 0.0D) {
      double d = 0.0D;
      for (paramInt = i - 4; paramInt < i; paramInt++)
        d += ((Float)arrayList.get(paramInt)).floatValue(); 
      d /= 4.0D;
      this.mTopFourAverageCn0StatisticsL5.addItem(d);
      this.mL5TopFourAverageCn0DbmHzReportsStatistics.addItem(1000.0D * d);
    } 
  }
  
  public void logConstellationType(int paramInt) {
    String str;
    boolean[] arrayOfBoolean = this.mConstellationTypes;
    if (paramInt >= arrayOfBoolean.length) {
      str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Constellation type ");
      stringBuilder.append(paramInt);
      stringBuilder.append(" is not valid.");
      Log.e(str, stringBuilder.toString());
      return;
    } 
    str[paramInt] = '\001';
  }
  
  public String dumpGnssMetricsAsProtoString() {
    GnssLogsProto.GnssLog gnssLog = new GnssLogsProto.GnssLog();
    if (this.mLocationFailureStatistics.getCount() > 0) {
      gnssLog.numLocationReportProcessed = this.mLocationFailureStatistics.getCount();
      gnssLog.percentageLocationFailure = (int)(this.mLocationFailureStatistics.getMean() * 100.0D);
    } 
    if (this.mTimeToFirstFixSecStatistics.getCount() > 0) {
      gnssLog.numTimeToFirstFixProcessed = this.mTimeToFirstFixSecStatistics.getCount();
      gnssLog.meanTimeToFirstFixSecs = (int)this.mTimeToFirstFixSecStatistics.getMean();
      Statistics statistics = this.mTimeToFirstFixSecStatistics;
      gnssLog.standardDeviationTimeToFirstFixSecs = (int)statistics.getStandardDeviation();
    } 
    if (this.mPositionAccuracyMeterStatistics.getCount() > 0) {
      gnssLog.numPositionAccuracyProcessed = this.mPositionAccuracyMeterStatistics.getCount();
      gnssLog.meanPositionAccuracyMeters = (int)this.mPositionAccuracyMeterStatistics.getMean();
      Statistics statistics = this.mPositionAccuracyMeterStatistics;
      gnssLog.standardDeviationPositionAccuracyMeters = (int)statistics.getStandardDeviation();
    } 
    if (this.mTopFourAverageCn0Statistics.getCount() > 0) {
      gnssLog.numTopFourAverageCn0Processed = this.mTopFourAverageCn0Statistics.getCount();
      gnssLog.meanTopFourAverageCn0DbHz = this.mTopFourAverageCn0Statistics.getMean();
      Statistics statistics = this.mTopFourAverageCn0Statistics;
      gnssLog.standardDeviationTopFourAverageCn0DbHz = statistics.getStandardDeviation();
    } 
    int i = this.mNumSvStatus;
    if (i > 0)
      gnssLog.numSvStatusProcessed = i; 
    i = this.mNumL5SvStatus;
    if (i > 0)
      gnssLog.numL5SvStatusProcessed = i; 
    i = this.mNumSvStatusUsedInFix;
    if (i > 0)
      gnssLog.numSvStatusUsedInFix = i; 
    i = this.mNumL5SvStatusUsedInFix;
    if (i > 0)
      gnssLog.numL5SvStatusUsedInFix = i; 
    if (this.mTopFourAverageCn0StatisticsL5.getCount() > 0) {
      gnssLog.numL5TopFourAverageCn0Processed = this.mTopFourAverageCn0StatisticsL5.getCount();
      gnssLog.meanL5TopFourAverageCn0DbHz = this.mTopFourAverageCn0StatisticsL5.getMean();
      Statistics statistics = this.mTopFourAverageCn0StatisticsL5;
      gnssLog.standardDeviationL5TopFourAverageCn0DbHz = statistics.getStandardDeviation();
    } 
    gnssLog.powerMetrics = this.mGnssPowerMetrics.buildProto();
    gnssLog.hardwareRevision = SystemProperties.get("ro.boot.revision", "");
    String str = Base64.encodeToString(GnssLogsProto.GnssLog.toByteArray(gnssLog), 0);
    reset();
    return str;
  }
  
  public String dumpGnssMetricsAsText() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("GNSS_KPI_START");
    stringBuilder.append('\n');
    stringBuilder.append("  KPI logging start time: ");
    stringBuilder.append(this.mLogStartInElapsedRealTime);
    stringBuilder.append("\n");
    stringBuilder.append("  KPI logging end time: ");
    TimeUtils.formatDuration(SystemClock.elapsedRealtimeNanos() / 1000000L, stringBuilder);
    stringBuilder.append("\n");
    stringBuilder.append("  Number of location reports: ");
    Statistics statistics = this.mLocationFailureStatistics;
    int i = statistics.getCount();
    stringBuilder.append(i);
    stringBuilder.append("\n");
    if (this.mLocationFailureStatistics.getCount() > 0) {
      stringBuilder.append("  Percentage location failure: ");
      statistics = this.mLocationFailureStatistics;
      double d = statistics.getMean();
      stringBuilder.append(d * 100.0D);
      stringBuilder.append("\n");
    } 
    stringBuilder.append("  Number of TTFF reports: ");
    statistics = this.mTimeToFirstFixSecStatistics;
    i = statistics.getCount();
    stringBuilder.append(i);
    stringBuilder.append("\n");
    if (this.mTimeToFirstFixSecStatistics.getCount() > 0) {
      stringBuilder.append("  TTFF mean (sec): ");
      stringBuilder.append(this.mTimeToFirstFixSecStatistics.getMean());
      stringBuilder.append("\n");
      stringBuilder.append("  TTFF standard deviation (sec): ");
      statistics = this.mTimeToFirstFixSecStatistics;
      double d = statistics.getStandardDeviation();
      stringBuilder.append(d);
      stringBuilder.append("\n");
    } 
    stringBuilder.append("  Number of position accuracy reports: ");
    statistics = this.mPositionAccuracyMeterStatistics;
    i = statistics.getCount();
    stringBuilder.append(i);
    stringBuilder.append("\n");
    if (this.mPositionAccuracyMeterStatistics.getCount() > 0) {
      stringBuilder.append("  Position accuracy mean (m): ");
      statistics = this.mPositionAccuracyMeterStatistics;
      double d = statistics.getMean();
      stringBuilder.append(d);
      stringBuilder.append("\n");
      stringBuilder.append("  Position accuracy standard deviation (m): ");
      statistics = this.mPositionAccuracyMeterStatistics;
      d = statistics.getStandardDeviation();
      stringBuilder.append(d);
      stringBuilder.append("\n");
    } 
    stringBuilder.append("  Number of CN0 reports: ");
    statistics = this.mTopFourAverageCn0Statistics;
    i = statistics.getCount();
    stringBuilder.append(i);
    stringBuilder.append("\n");
    if (this.mTopFourAverageCn0Statistics.getCount() > 0) {
      stringBuilder.append("  Top 4 Avg CN0 mean (dB-Hz): ");
      statistics = this.mTopFourAverageCn0Statistics;
      double d = statistics.getMean();
      stringBuilder.append(d);
      stringBuilder.append("\n");
      stringBuilder.append("  Top 4 Avg CN0 standard deviation (dB-Hz): ");
      statistics = this.mTopFourAverageCn0Statistics;
      d = statistics.getStandardDeviation();
      stringBuilder.append(d);
      stringBuilder.append("\n");
    } 
    stringBuilder.append("  Total number of sv status messages processed: ");
    stringBuilder.append(this.mNumSvStatus);
    stringBuilder.append("\n");
    stringBuilder.append("  Total number of L5 sv status messages processed: ");
    stringBuilder.append(this.mNumL5SvStatus);
    stringBuilder.append("\n");
    stringBuilder.append("  Total number of sv status messages processed, where sv is used in fix: ");
    i = this.mNumSvStatusUsedInFix;
    stringBuilder.append(i);
    stringBuilder.append("\n");
    stringBuilder.append("  Total number of L5 sv status messages processed, where sv is used in fix: ");
    i = this.mNumL5SvStatusUsedInFix;
    stringBuilder.append(i);
    stringBuilder.append("\n");
    stringBuilder.append("  Number of L5 CN0 reports: ");
    statistics = this.mTopFourAverageCn0StatisticsL5;
    i = statistics.getCount();
    stringBuilder.append(i);
    stringBuilder.append("\n");
    if (this.mTopFourAverageCn0StatisticsL5.getCount() > 0) {
      stringBuilder.append("  L5 Top 4 Avg CN0 mean (dB-Hz): ");
      statistics = this.mTopFourAverageCn0StatisticsL5;
      double d = statistics.getMean();
      stringBuilder.append(d);
      stringBuilder.append("\n");
      stringBuilder.append("  L5 Top 4 Avg CN0 standard deviation (dB-Hz): ");
      statistics = this.mTopFourAverageCn0StatisticsL5;
      d = statistics.getStandardDeviation();
      stringBuilder.append(d);
      stringBuilder.append("\n");
    } 
    stringBuilder.append("  Used-in-fix constellation types: ");
    i = 0;
    while (true) {
      boolean[] arrayOfBoolean = this.mConstellationTypes;
      if (i < arrayOfBoolean.length) {
        if (arrayOfBoolean[i]) {
          stringBuilder.append(GnssStatus.constellationTypeToString(i));
          stringBuilder.append(" ");
        } 
        i++;
        continue;
      } 
      break;
    } 
    stringBuilder.append("\n");
    stringBuilder.append("GNSS_KPI_END");
    stringBuilder.append("\n");
    GpsBatteryStats gpsBatteryStats = this.mGnssPowerMetrics.getGpsBatteryStats();
    if (gpsBatteryStats != null) {
      stringBuilder.append("Power Metrics");
      stringBuilder.append("\n");
      stringBuilder.append("  Time on battery (min): ");
      double d = gpsBatteryStats.getLoggingDurationMs() / 60000.0D;
      stringBuilder.append(d);
      stringBuilder.append("\n");
      long[] arrayOfLong = gpsBatteryStats.getTimeInGpsSignalQualityLevel();
      if (arrayOfLong != null && arrayOfLong.length == 2) {
        stringBuilder.append("  Amount of time (while on battery) Top 4 Avg CN0 > 20.0 dB-Hz (min): ");
        d = arrayOfLong[1] / 60000.0D;
        stringBuilder.append(d);
        stringBuilder.append("\n");
        stringBuilder.append("  Amount of time (while on battery) Top 4 Avg CN0 <= 20.0 dB-Hz (min): ");
        d = arrayOfLong[0] / 60000.0D;
        stringBuilder.append(d);
        stringBuilder.append("\n");
      } 
      stringBuilder.append("  Energy consumed while on battery (mAh): ");
      d = gpsBatteryStats.getEnergyConsumedMaMs() / 3600000.0D;
      stringBuilder.append(d);
      stringBuilder.append("\n");
    } 
    stringBuilder.append("Hardware Version: ");
    stringBuilder.append(SystemProperties.get("ro.boot.revision", ""));
    stringBuilder.append("\n");
    return stringBuilder.toString();
  }
  
  private void reset() {
    StringBuilder stringBuilder = new StringBuilder();
    TimeUtils.formatDuration(SystemClock.elapsedRealtimeNanos() / 1000000L, stringBuilder);
    this.mLogStartInElapsedRealTime = stringBuilder.toString();
    this.mLocationFailureStatistics.reset();
    this.mTimeToFirstFixSecStatistics.reset();
    this.mPositionAccuracyMeterStatistics.reset();
    this.mTopFourAverageCn0Statistics.reset();
    resetConstellationTypes();
    this.mTopFourAverageCn0StatisticsL5.reset();
    this.mNumSvStatus = 0;
    this.mNumL5SvStatus = 0;
    this.mNumSvStatusUsedInFix = 0;
    this.mNumL5SvStatusUsedInFix = 0;
  }
  
  public void resetConstellationTypes() {
    this.mConstellationTypes = new boolean[8];
  }
  
  private static class Statistics {
    private int mCount;
    
    private long mLongSum;
    
    private double mSum;
    
    private double mSumSquare;
    
    private Statistics() {}
    
    public void reset() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: iconst_0
      //   4: putfield mCount : I
      //   7: aload_0
      //   8: dconst_0
      //   9: putfield mSum : D
      //   12: aload_0
      //   13: dconst_0
      //   14: putfield mSumSquare : D
      //   17: aload_0
      //   18: lconst_0
      //   19: putfield mLongSum : J
      //   22: aload_0
      //   23: monitorexit
      //   24: return
      //   25: astore_1
      //   26: aload_0
      //   27: monitorexit
      //   28: aload_1
      //   29: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #478	-> 2
      //   #479	-> 7
      //   #480	-> 12
      //   #481	-> 17
      //   #482	-> 22
      //   #477	-> 25
      // Exception table:
      //   from	to	target	type
      //   2	7	25	finally
      //   7	12	25	finally
      //   12	17	25	finally
      //   17	22	25	finally
    }
    
    public void addItem(double param1Double) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: aload_0
      //   4: getfield mCount : I
      //   7: iconst_1
      //   8: iadd
      //   9: putfield mCount : I
      //   12: aload_0
      //   13: aload_0
      //   14: getfield mSum : D
      //   17: dload_1
      //   18: dadd
      //   19: putfield mSum : D
      //   22: aload_0
      //   23: aload_0
      //   24: getfield mSumSquare : D
      //   27: dload_1
      //   28: dload_1
      //   29: dmul
      //   30: dadd
      //   31: putfield mSumSquare : D
      //   34: aload_0
      //   35: aload_0
      //   36: getfield mLongSum : J
      //   39: l2d
      //   40: dload_1
      //   41: dadd
      //   42: d2l
      //   43: putfield mLongSum : J
      //   46: aload_0
      //   47: monitorexit
      //   48: return
      //   49: astore_3
      //   50: aload_0
      //   51: monitorexit
      //   52: aload_3
      //   53: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #486	-> 2
      //   #487	-> 12
      //   #488	-> 22
      //   #489	-> 34
      //   #490	-> 46
      //   #485	-> 49
      // Exception table:
      //   from	to	target	type
      //   2	12	49	finally
      //   12	22	49	finally
      //   22	34	49	finally
      //   34	46	49	finally
    }
    
    public int getCount() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mCount : I
      //   6: istore_1
      //   7: aload_0
      //   8: monitorexit
      //   9: iload_1
      //   10: ireturn
      //   11: astore_2
      //   12: aload_0
      //   13: monitorexit
      //   14: aload_2
      //   15: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #494	-> 2
      //   #494	-> 11
      // Exception table:
      //   from	to	target	type
      //   2	7	11	finally
    }
    
    public double getMean() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mSum : D
      //   6: dstore_1
      //   7: aload_0
      //   8: getfield mCount : I
      //   11: istore_3
      //   12: dload_1
      //   13: iload_3
      //   14: i2d
      //   15: ddiv
      //   16: dstore_1
      //   17: aload_0
      //   18: monitorexit
      //   19: dload_1
      //   20: dreturn
      //   21: astore #4
      //   23: aload_0
      //   24: monitorexit
      //   25: aload #4
      //   27: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #499	-> 2
      //   #499	-> 21
      // Exception table:
      //   from	to	target	type
      //   2	12	21	finally
    }
    
    public double getStandardDeviation() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mSum : D
      //   6: aload_0
      //   7: getfield mCount : I
      //   10: i2d
      //   11: ddiv
      //   12: dstore_1
      //   13: dload_1
      //   14: dload_1
      //   15: dmul
      //   16: dstore_1
      //   17: aload_0
      //   18: getfield mSumSquare : D
      //   21: aload_0
      //   22: getfield mCount : I
      //   25: i2d
      //   26: ddiv
      //   27: dstore_3
      //   28: dload_3
      //   29: dload_1
      //   30: dcmpl
      //   31: ifle -> 45
      //   34: dload_3
      //   35: dload_1
      //   36: dsub
      //   37: invokestatic sqrt : (D)D
      //   40: dstore_1
      //   41: aload_0
      //   42: monitorexit
      //   43: dload_1
      //   44: dreturn
      //   45: aload_0
      //   46: monitorexit
      //   47: dconst_0
      //   48: dreturn
      //   49: astore #5
      //   51: aload_0
      //   52: monitorexit
      //   53: aload #5
      //   55: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #504	-> 2
      //   #505	-> 13
      //   #506	-> 17
      //   #507	-> 28
      //   #508	-> 34
      //   #510	-> 45
      //   #503	-> 49
      // Exception table:
      //   from	to	target	type
      //   2	13	49	finally
      //   17	28	49	finally
      //   34	41	49	finally
    }
    
    public long getLongSum() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mLongSum : J
      //   6: lstore_1
      //   7: aload_0
      //   8: monitorexit
      //   9: lload_1
      //   10: lreturn
      //   11: astore_3
      //   12: aload_0
      //   13: monitorexit
      //   14: aload_3
      //   15: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #515	-> 2
      //   #515	-> 11
      // Exception table:
      //   from	to	target	type
      //   2	7	11	finally
    }
  }
  
  private class GnssPowerMetrics {
    public static final double POOR_TOP_FOUR_AVG_CN0_THRESHOLD_DB_HZ = 20.0D;
    
    private static final double REPORTING_THRESHOLD_DB_HZ = 1.0D;
    
    private final IBatteryStats mBatteryStats;
    
    private double mLastAverageCn0;
    
    private int mLastSignalLevel;
    
    final GnssMetrics this$0;
    
    private GnssPowerMetrics(IBatteryStats param1IBatteryStats) {
      this.mBatteryStats = param1IBatteryStats;
      this.mLastAverageCn0 = -100.0D;
      this.mLastSignalLevel = -1;
    }
    
    public GnssLogsProto.PowerMetrics buildProto() {
      GnssLogsProto.PowerMetrics powerMetrics = new GnssLogsProto.PowerMetrics();
      GpsBatteryStats gpsBatteryStats = GnssMetrics.this.mGnssPowerMetrics.getGpsBatteryStats();
      if (gpsBatteryStats != null) {
        powerMetrics.loggingDurationMs = gpsBatteryStats.getLoggingDurationMs();
        powerMetrics.energyConsumedMah = gpsBatteryStats.getEnergyConsumedMaMs() / 3600000.0D;
        long[] arrayOfLong = gpsBatteryStats.getTimeInGpsSignalQualityLevel();
        powerMetrics.timeInSignalQualityLevelMs = new long[arrayOfLong.length];
        for (byte b = 0; b < arrayOfLong.length; b++)
          powerMetrics.timeInSignalQualityLevelMs[b] = arrayOfLong[b]; 
      } 
      return powerMetrics;
    }
    
    public GpsBatteryStats getGpsBatteryStats() {
      try {
        return this.mBatteryStats.getGpsBatteryStats();
      } catch (Exception exception) {
        Log.w(GnssMetrics.TAG, "Exception", exception);
        return null;
      } 
    }
    
    public void reportSignalQuality(float[] param1ArrayOffloat, int param1Int) {
      double d1 = 0.0D;
      double d2 = d1;
      if (param1Int > 0) {
        for (int i = Math.max(0, param1Int - 4); i < param1Int; i++)
          d1 += param1ArrayOffloat[i]; 
        d2 = d1 / Math.min(param1Int, 4);
      } 
      if (Math.abs(d2 - this.mLastAverageCn0) < 1.0D)
        return; 
      param1Int = getSignalLevel(d2);
      if (param1Int != this.mLastSignalLevel) {
        FrameworkStatsLog.write(69, param1Int);
        this.mLastSignalLevel = param1Int;
      } 
      try {
        this.mBatteryStats.noteGpsSignalQuality(param1Int);
        this.mLastAverageCn0 = d2;
      } catch (Exception exception) {
        Log.w(GnssMetrics.TAG, "Exception", exception);
      } 
    }
    
    private int getSignalLevel(double param1Double) {
      if (param1Double > 20.0D)
        return 1; 
      return 0;
    }
  }
  
  private void registerGnssStats() {
    StatsPullAtomCallbackImpl statsPullAtomCallbackImpl = new StatsPullAtomCallbackImpl();
    this.mStatsManager.setPullAtomCallback(10074, null, ConcurrentUtils.DIRECT_EXECUTOR, statsPullAtomCallbackImpl);
  }
  
  private class StatsPullAtomCallbackImpl implements StatsManager.StatsPullAtomCallback {
    final GnssMetrics this$0;
    
    private StatsPullAtomCallbackImpl() {}
    
    public int onPullAtom(int param1Int, List<StatsEvent> param1List) {
      if (param1Int == 10074) {
        StatsEvent.Builder builder4 = StatsEvent.newBuilder();
        builder4 = builder4.setAtomId(param1Int);
        GnssMetrics gnssMetrics7 = GnssMetrics.this;
        builder4 = builder4.writeLong(gnssMetrics7.mLocationFailureReportsStatistics.getCount());
        gnssMetrics7 = GnssMetrics.this;
        builder4 = builder4.writeLong(gnssMetrics7.mLocationFailureReportsStatistics.getLongSum());
        gnssMetrics7 = GnssMetrics.this;
        builder4 = builder4.writeLong(gnssMetrics7.mTimeToFirstFixMilliSReportsStatistics.getCount());
        gnssMetrics7 = GnssMetrics.this;
        StatsEvent.Builder builder7 = builder4.writeLong(gnssMetrics7.mTimeToFirstFixMilliSReportsStatistics.getLongSum());
        GnssMetrics gnssMetrics3 = GnssMetrics.this;
        StatsEvent.Builder builder3 = builder7.writeLong(gnssMetrics3.mPositionAccuracyMetersReportsStatistics.getCount());
        GnssMetrics gnssMetrics6 = GnssMetrics.this;
        StatsEvent.Builder builder6 = builder3.writeLong(gnssMetrics6.mPositionAccuracyMetersReportsStatistics.getLongSum());
        GnssMetrics gnssMetrics2 = GnssMetrics.this;
        StatsEvent.Builder builder2 = builder6.writeLong(gnssMetrics2.mTopFourAverageCn0DbmHzReportsStatistics.getCount());
        GnssMetrics gnssMetrics5 = GnssMetrics.this;
        builder2 = builder2.writeLong(gnssMetrics5.mTopFourAverageCn0DbmHzReportsStatistics.getLongSum());
        gnssMetrics5 = GnssMetrics.this;
        builder2 = builder2.writeLong(gnssMetrics5.mL5TopFourAverageCn0DbmHzReportsStatistics.getCount());
        gnssMetrics5 = GnssMetrics.this;
        builder2 = builder2.writeLong(gnssMetrics5.mL5TopFourAverageCn0DbmHzReportsStatistics.getLongSum());
        gnssMetrics5 = GnssMetrics.this;
        builder2 = builder2.writeLong(gnssMetrics5.mSvStatusReports);
        gnssMetrics5 = GnssMetrics.this;
        StatsEvent.Builder builder5 = builder2.writeLong(gnssMetrics5.mSvStatusReportsUsedInFix);
        GnssMetrics gnssMetrics1 = GnssMetrics.this;
        StatsEvent.Builder builder1 = builder5.writeLong(gnssMetrics1.mL5SvStatusReports);
        GnssMetrics gnssMetrics4 = GnssMetrics.this;
        builder1 = builder1.writeLong(gnssMetrics4.mL5SvStatusReportsUsedInFix);
        StatsEvent statsEvent = builder1.build();
        param1List.add(statsEvent);
        return 0;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unknown tagId = ");
      stringBuilder.append(param1Int);
      throw new UnsupportedOperationException(stringBuilder.toString());
    }
  }
}
