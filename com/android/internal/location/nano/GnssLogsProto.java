package com.android.internal.location.nano;

import com.android.framework.protobuf.nano.CodedInputByteBufferNano;
import com.android.framework.protobuf.nano.CodedOutputByteBufferNano;
import com.android.framework.protobuf.nano.InternalNano;
import com.android.framework.protobuf.nano.InvalidProtocolBufferNanoException;
import com.android.framework.protobuf.nano.MessageNano;
import com.android.framework.protobuf.nano.WireFormatNano;
import java.io.IOException;

public interface GnssLogsProto {
  class GnssLog extends MessageNano {
    private static volatile GnssLog[] _emptyArray;
    
    public String hardwareRevision;
    
    public double meanL5TopFourAverageCn0DbHz;
    
    public int meanPositionAccuracyMeters;
    
    public int meanTimeToFirstFixSecs;
    
    public double meanTopFourAverageCn0DbHz;
    
    public int numL5SvStatusProcessed;
    
    public int numL5SvStatusUsedInFix;
    
    public int numL5TopFourAverageCn0Processed;
    
    public int numLocationReportProcessed;
    
    public int numPositionAccuracyProcessed;
    
    public int numSvStatusProcessed;
    
    public int numSvStatusUsedInFix;
    
    public int numTimeToFirstFixProcessed;
    
    public int numTopFourAverageCn0Processed;
    
    public int percentageLocationFailure;
    
    public GnssLogsProto.PowerMetrics powerMetrics;
    
    public double standardDeviationL5TopFourAverageCn0DbHz;
    
    public int standardDeviationPositionAccuracyMeters;
    
    public int standardDeviationTimeToFirstFixSecs;
    
    public double standardDeviationTopFourAverageCn0DbHz;
    
    public static GnssLog[] emptyArray() {
      if (_emptyArray == null)
        synchronized (InternalNano.LAZY_INIT_LOCK) {
          if (_emptyArray == null)
            _emptyArray = new GnssLog[0]; 
        }  
      return _emptyArray;
    }
    
    public GnssLog() {
      clear();
    }
    
    public GnssLog clear() {
      this.numLocationReportProcessed = 0;
      this.percentageLocationFailure = 0;
      this.numTimeToFirstFixProcessed = 0;
      this.meanTimeToFirstFixSecs = 0;
      this.standardDeviationTimeToFirstFixSecs = 0;
      this.numPositionAccuracyProcessed = 0;
      this.meanPositionAccuracyMeters = 0;
      this.standardDeviationPositionAccuracyMeters = 0;
      this.numTopFourAverageCn0Processed = 0;
      this.meanTopFourAverageCn0DbHz = 0.0D;
      this.standardDeviationTopFourAverageCn0DbHz = 0.0D;
      this.powerMetrics = null;
      this.hardwareRevision = "";
      this.numSvStatusProcessed = 0;
      this.numL5SvStatusProcessed = 0;
      this.numSvStatusUsedInFix = 0;
      this.numL5SvStatusUsedInFix = 0;
      this.numL5TopFourAverageCn0Processed = 0;
      this.meanL5TopFourAverageCn0DbHz = 0.0D;
      this.standardDeviationL5TopFourAverageCn0DbHz = 0.0D;
      this.cachedSize = -1;
      return this;
    }
    
    public void writeTo(CodedOutputByteBufferNano param1CodedOutputByteBufferNano) throws IOException {
      int i = this.numLocationReportProcessed;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(1, i); 
      i = this.percentageLocationFailure;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(2, i); 
      i = this.numTimeToFirstFixProcessed;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(3, i); 
      i = this.meanTimeToFirstFixSecs;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(4, i); 
      i = this.standardDeviationTimeToFirstFixSecs;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(5, i); 
      i = this.numPositionAccuracyProcessed;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(6, i); 
      i = this.meanPositionAccuracyMeters;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(7, i); 
      i = this.standardDeviationPositionAccuracyMeters;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(8, i); 
      i = this.numTopFourAverageCn0Processed;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(9, i); 
      long l = Double.doubleToLongBits(this.meanTopFourAverageCn0DbHz);
      if (l != Double.doubleToLongBits(0.0D))
        param1CodedOutputByteBufferNano.writeDouble(10, this.meanTopFourAverageCn0DbHz); 
      l = Double.doubleToLongBits(this.standardDeviationTopFourAverageCn0DbHz);
      if (l != Double.doubleToLongBits(0.0D))
        param1CodedOutputByteBufferNano.writeDouble(11, this.standardDeviationTopFourAverageCn0DbHz); 
      GnssLogsProto.PowerMetrics powerMetrics = this.powerMetrics;
      if (powerMetrics != null)
        param1CodedOutputByteBufferNano.writeMessage(12, powerMetrics); 
      if (!this.hardwareRevision.equals(""))
        param1CodedOutputByteBufferNano.writeString(13, this.hardwareRevision); 
      i = this.numSvStatusProcessed;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(14, i); 
      i = this.numL5SvStatusProcessed;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(15, i); 
      i = this.numSvStatusUsedInFix;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(16, i); 
      i = this.numL5SvStatusUsedInFix;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(17, i); 
      i = this.numL5TopFourAverageCn0Processed;
      if (i != 0)
        param1CodedOutputByteBufferNano.writeInt32(18, i); 
      l = Double.doubleToLongBits(this.meanL5TopFourAverageCn0DbHz);
      if (l != Double.doubleToLongBits(0.0D))
        param1CodedOutputByteBufferNano.writeDouble(19, this.meanL5TopFourAverageCn0DbHz); 
      l = Double.doubleToLongBits(this.standardDeviationL5TopFourAverageCn0DbHz);
      if (l != Double.doubleToLongBits(0.0D))
        param1CodedOutputByteBufferNano.writeDouble(20, this.standardDeviationL5TopFourAverageCn0DbHz); 
      super.writeTo(param1CodedOutputByteBufferNano);
    }
    
    protected int computeSerializedSize() {
      int i = super.computeSerializedSize();
      int j = this.numLocationReportProcessed, k = i;
      if (j != 0)
        k = i + CodedOutputByteBufferNano.computeInt32Size(1, j); 
      j = this.percentageLocationFailure;
      i = k;
      if (j != 0)
        i = k + CodedOutputByteBufferNano.computeInt32Size(2, j); 
      j = this.numTimeToFirstFixProcessed;
      k = i;
      if (j != 0)
        k = i + CodedOutputByteBufferNano.computeInt32Size(3, j); 
      j = this.meanTimeToFirstFixSecs;
      i = k;
      if (j != 0)
        i = k + CodedOutputByteBufferNano.computeInt32Size(4, j); 
      j = this.standardDeviationTimeToFirstFixSecs;
      k = i;
      if (j != 0)
        k = i + CodedOutputByteBufferNano.computeInt32Size(5, j); 
      j = this.numPositionAccuracyProcessed;
      i = k;
      if (j != 0)
        i = k + CodedOutputByteBufferNano.computeInt32Size(6, j); 
      j = this.meanPositionAccuracyMeters;
      k = i;
      if (j != 0)
        k = i + CodedOutputByteBufferNano.computeInt32Size(7, j); 
      j = this.standardDeviationPositionAccuracyMeters;
      i = k;
      if (j != 0)
        i = k + CodedOutputByteBufferNano.computeInt32Size(8, j); 
      j = this.numTopFourAverageCn0Processed;
      k = i;
      if (j != 0)
        k = i + CodedOutputByteBufferNano.computeInt32Size(9, j); 
      long l = Double.doubleToLongBits(this.meanTopFourAverageCn0DbHz);
      i = k;
      if (l != Double.doubleToLongBits(0.0D)) {
        double d = this.meanTopFourAverageCn0DbHz;
        i = k + CodedOutputByteBufferNano.computeDoubleSize(10, d);
      } 
      l = Double.doubleToLongBits(this.standardDeviationTopFourAverageCn0DbHz);
      k = i;
      if (l != Double.doubleToLongBits(0.0D)) {
        double d = this.standardDeviationTopFourAverageCn0DbHz;
        k = i + CodedOutputByteBufferNano.computeDoubleSize(11, d);
      } 
      GnssLogsProto.PowerMetrics powerMetrics = this.powerMetrics;
      j = k;
      if (powerMetrics != null)
        j = k + CodedOutputByteBufferNano.computeMessageSize(12, powerMetrics); 
      i = j;
      if (!this.hardwareRevision.equals("")) {
        String str = this.hardwareRevision;
        i = j + CodedOutputByteBufferNano.computeStringSize(13, str);
      } 
      j = this.numSvStatusProcessed;
      k = i;
      if (j != 0)
        k = i + CodedOutputByteBufferNano.computeInt32Size(14, j); 
      j = this.numL5SvStatusProcessed;
      i = k;
      if (j != 0)
        i = k + CodedOutputByteBufferNano.computeInt32Size(15, j); 
      j = this.numSvStatusUsedInFix;
      k = i;
      if (j != 0)
        k = i + CodedOutputByteBufferNano.computeInt32Size(16, j); 
      j = this.numL5SvStatusUsedInFix;
      i = k;
      if (j != 0)
        i = k + CodedOutputByteBufferNano.computeInt32Size(17, j); 
      j = this.numL5TopFourAverageCn0Processed;
      k = i;
      if (j != 0)
        k = i + CodedOutputByteBufferNano.computeInt32Size(18, j); 
      l = Double.doubleToLongBits(this.meanL5TopFourAverageCn0DbHz);
      i = k;
      if (l != Double.doubleToLongBits(0.0D)) {
        double d = this.meanL5TopFourAverageCn0DbHz;
        i = k + CodedOutputByteBufferNano.computeDoubleSize(19, d);
      } 
      l = Double.doubleToLongBits(this.standardDeviationL5TopFourAverageCn0DbHz);
      k = i;
      if (l != Double.doubleToLongBits(0.0D)) {
        double d = this.standardDeviationL5TopFourAverageCn0DbHz;
        k = i + CodedOutputByteBufferNano.computeDoubleSize(20, d);
      } 
      return k;
    }
    
    public GnssLog mergeFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      while (true) {
        int i = param1CodedInputByteBufferNano.readTag();
        switch (i) {
          default:
            if (!WireFormatNano.parseUnknownField(param1CodedInputByteBufferNano, i))
              return this; 
            continue;
          case 161:
            this.standardDeviationL5TopFourAverageCn0DbHz = param1CodedInputByteBufferNano.readDouble();
            continue;
          case 153:
            this.meanL5TopFourAverageCn0DbHz = param1CodedInputByteBufferNano.readDouble();
            continue;
          case 144:
            this.numL5TopFourAverageCn0Processed = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 136:
            this.numL5SvStatusUsedInFix = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 128:
            this.numSvStatusUsedInFix = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 120:
            this.numL5SvStatusProcessed = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 112:
            this.numSvStatusProcessed = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 106:
            this.hardwareRevision = param1CodedInputByteBufferNano.readString();
            continue;
          case 98:
            if (this.powerMetrics == null)
              this.powerMetrics = new GnssLogsProto.PowerMetrics(); 
            param1CodedInputByteBufferNano.readMessage(this.powerMetrics);
            continue;
          case 89:
            this.standardDeviationTopFourAverageCn0DbHz = param1CodedInputByteBufferNano.readDouble();
            continue;
          case 81:
            this.meanTopFourAverageCn0DbHz = param1CodedInputByteBufferNano.readDouble();
            continue;
          case 72:
            this.numTopFourAverageCn0Processed = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 64:
            this.standardDeviationPositionAccuracyMeters = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 56:
            this.meanPositionAccuracyMeters = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 48:
            this.numPositionAccuracyProcessed = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 40:
            this.standardDeviationTimeToFirstFixSecs = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 32:
            this.meanTimeToFirstFixSecs = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 24:
            this.numTimeToFirstFixProcessed = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 16:
            this.percentageLocationFailure = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 8:
            this.numLocationReportProcessed = param1CodedInputByteBufferNano.readInt32();
            continue;
          case 0:
            break;
        } 
        break;
      } 
      return this;
    }
    
    public static GnssLog parseFrom(byte[] param1ArrayOfbyte) throws InvalidProtocolBufferNanoException {
      return MessageNano.<GnssLog>mergeFrom(new GnssLog(), param1ArrayOfbyte);
    }
    
    public static GnssLog parseFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      return (new GnssLog()).mergeFrom(param1CodedInputByteBufferNano);
    }
  }
  
  class PowerMetrics extends MessageNano {
    private static volatile PowerMetrics[] _emptyArray;
    
    public double energyConsumedMah;
    
    public long loggingDurationMs;
    
    public long[] timeInSignalQualityLevelMs;
    
    public static PowerMetrics[] emptyArray() {
      if (_emptyArray == null)
        synchronized (InternalNano.LAZY_INIT_LOCK) {
          if (_emptyArray == null)
            _emptyArray = new PowerMetrics[0]; 
        }  
      return _emptyArray;
    }
    
    public PowerMetrics() {
      clear();
    }
    
    public PowerMetrics clear() {
      this.loggingDurationMs = 0L;
      this.energyConsumedMah = 0.0D;
      this.timeInSignalQualityLevelMs = WireFormatNano.EMPTY_LONG_ARRAY;
      this.cachedSize = -1;
      return this;
    }
    
    public void writeTo(CodedOutputByteBufferNano param1CodedOutputByteBufferNano) throws IOException {
      long l = this.loggingDurationMs;
      if (l != 0L)
        param1CodedOutputByteBufferNano.writeInt64(1, l); 
      l = Double.doubleToLongBits(this.energyConsumedMah);
      if (l != Double.doubleToLongBits(0.0D))
        param1CodedOutputByteBufferNano.writeDouble(2, this.energyConsumedMah); 
      long[] arrayOfLong = this.timeInSignalQualityLevelMs;
      if (arrayOfLong != null && arrayOfLong.length > 0) {
        byte b = 0;
        while (true) {
          arrayOfLong = this.timeInSignalQualityLevelMs;
          if (b < arrayOfLong.length) {
            param1CodedOutputByteBufferNano.writeInt64(3, arrayOfLong[b]);
            b++;
            continue;
          } 
          break;
        } 
      } 
      super.writeTo(param1CodedOutputByteBufferNano);
    }
    
    protected int computeSerializedSize() {
      int i = super.computeSerializedSize();
      long l = this.loggingDurationMs;
      int j = i;
      if (l != 0L)
        j = i + CodedOutputByteBufferNano.computeInt64Size(1, l); 
      l = Double.doubleToLongBits(this.energyConsumedMah);
      i = j;
      if (l != Double.doubleToLongBits(0.0D)) {
        double d = this.energyConsumedMah;
        i = j + CodedOutputByteBufferNano.computeDoubleSize(2, d);
      } 
      long[] arrayOfLong = this.timeInSignalQualityLevelMs;
      j = i;
      if (arrayOfLong != null) {
        j = i;
        if (arrayOfLong.length > 0) {
          int k = 0;
          j = 0;
          while (true) {
            arrayOfLong = this.timeInSignalQualityLevelMs;
            if (j < arrayOfLong.length) {
              l = arrayOfLong[j];
              k += CodedOutputByteBufferNano.computeInt64SizeNoTag(l);
              j++;
              continue;
            } 
            break;
          } 
          j = i + k + arrayOfLong.length * 1;
        } 
      } 
      return j;
    }
    
    public PowerMetrics mergeFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      while (true) {
        int i = param1CodedInputByteBufferNano.readTag();
        if (i != 0) {
          if (i != 8) {
            if (i != 17) {
              if (i != 24) {
                if (i != 26) {
                  if (!WireFormatNano.parseUnknownField(param1CodedInputByteBufferNano, i))
                    return this; 
                  continue;
                } 
                i = param1CodedInputByteBufferNano.readRawVarint32();
                int k = param1CodedInputByteBufferNano.pushLimit(i);
                int m = 0;
                i = param1CodedInputByteBufferNano.getPosition();
                while (param1CodedInputByteBufferNano.getBytesUntilLimit() > 0) {
                  param1CodedInputByteBufferNano.readInt64();
                  m++;
                } 
                param1CodedInputByteBufferNano.rewindToPosition(i);
                long[] arrayOfLong1 = this.timeInSignalQualityLevelMs;
                if (arrayOfLong1 == null) {
                  i = 0;
                } else {
                  i = arrayOfLong1.length;
                } 
                arrayOfLong1 = new long[i + m];
                m = i;
                if (i != 0) {
                  System.arraycopy(this.timeInSignalQualityLevelMs, 0, arrayOfLong1, 0, i);
                  m = i;
                } 
                for (; m < arrayOfLong1.length; m++)
                  arrayOfLong1[m] = param1CodedInputByteBufferNano.readInt64(); 
                this.timeInSignalQualityLevelMs = arrayOfLong1;
                param1CodedInputByteBufferNano.popLimit(k);
                continue;
              } 
              int j = WireFormatNano.getRepeatedFieldArrayLength(param1CodedInputByteBufferNano, 24);
              long[] arrayOfLong = this.timeInSignalQualityLevelMs;
              if (arrayOfLong == null) {
                i = 0;
              } else {
                i = arrayOfLong.length;
              } 
              arrayOfLong = new long[i + j];
              j = i;
              if (i != 0) {
                System.arraycopy(this.timeInSignalQualityLevelMs, 0, arrayOfLong, 0, i);
                j = i;
              } 
              for (; j < arrayOfLong.length - 1; j++) {
                arrayOfLong[j] = param1CodedInputByteBufferNano.readInt64();
                param1CodedInputByteBufferNano.readTag();
              } 
              arrayOfLong[j] = param1CodedInputByteBufferNano.readInt64();
              this.timeInSignalQualityLevelMs = arrayOfLong;
              continue;
            } 
            this.energyConsumedMah = param1CodedInputByteBufferNano.readDouble();
            continue;
          } 
          this.loggingDurationMs = param1CodedInputByteBufferNano.readInt64();
          continue;
        } 
        break;
      } 
      return this;
    }
    
    public static PowerMetrics parseFrom(byte[] param1ArrayOfbyte) throws InvalidProtocolBufferNanoException {
      return MessageNano.<PowerMetrics>mergeFrom(new PowerMetrics(), param1ArrayOfbyte);
    }
    
    public static PowerMetrics parseFrom(CodedInputByteBufferNano param1CodedInputByteBufferNano) throws IOException {
      return (new PowerMetrics()).mergeFrom(param1CodedInputByteBufferNano);
    }
  }
}
