package com.android.internal.location;

import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.INetInitiatedListener;
import android.location.LocationManager;
import android.os.RemoteException;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.android.internal.app.NetInitiatedActivity;
import com.android.internal.telephony.GsmAlphabet;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.TimeUnit;

public class GpsNetInitiatedHandler {
  private static final boolean DEBUG = Log.isLoggable("GpsNetInitiatedHandler", 3);
  
  private boolean mPlaySounds = false;
  
  private boolean mPopupImmediately = true;
  
  private volatile boolean mIsLocationEnabled = false;
  
  private static boolean mIsHexInput = true;
  
  private volatile long mCallEndElapsedRealtimeMillis = 0L;
  
  private volatile long mEmergencyExtensionMillis = 0L;
  
  public static class GpsNiNotification {
    public int defaultResponse;
    
    public boolean needNotify;
    
    public boolean needVerify;
    
    public int niType;
    
    public int notificationId;
    
    public boolean privacyOverride;
    
    public String requestorId;
    
    public int requestorIdEncoding;
    
    public String text;
    
    public int textEncoding;
    
    public int timeout;
  }
  
  public static class GpsNiResponse {
    int userResponse;
  }
  
  private final BroadcastReceiver mBroadcastReciever = (BroadcastReceiver)new Object(this);
  
  public static final String ACTION_NI_VERIFY = "android.intent.action.NETWORK_INITIATED_VERIFY";
  
  public static final int GPS_ENC_NONE = 0;
  
  public static final int GPS_ENC_SUPL_GSM_DEFAULT = 1;
  
  public static final int GPS_ENC_SUPL_UCS2 = 3;
  
  public static final int GPS_ENC_SUPL_UTF8 = 2;
  
  public static final int GPS_ENC_UNKNOWN = -1;
  
  public static final int GPS_NI_NEED_NOTIFY = 1;
  
  public static final int GPS_NI_NEED_VERIFY = 2;
  
  public static final int GPS_NI_PRIVACY_OVERRIDE = 4;
  
  public static final int GPS_NI_RESPONSE_ACCEPT = 1;
  
  public static final int GPS_NI_RESPONSE_DENY = 2;
  
  public static final int GPS_NI_RESPONSE_IGNORE = 4;
  
  public static final int GPS_NI_RESPONSE_NORESP = 3;
  
  public static final int GPS_NI_TYPE_EMERGENCY_SUPL = 4;
  
  public static final int GPS_NI_TYPE_UMTS_CTRL_PLANE = 3;
  
  public static final int GPS_NI_TYPE_UMTS_SUPL = 2;
  
  public static final int GPS_NI_TYPE_VOICE = 1;
  
  public static final String NI_EXTRA_CMD_NOTIF_ID = "notif_id";
  
  public static final String NI_EXTRA_CMD_RESPONSE = "response";
  
  public static final String NI_INTENT_KEY_DEFAULT_RESPONSE = "default_resp";
  
  public static final String NI_INTENT_KEY_MESSAGE = "message";
  
  public static final String NI_INTENT_KEY_NOTIF_ID = "notif_id";
  
  public static final String NI_INTENT_KEY_TIMEOUT = "timeout";
  
  public static final String NI_INTENT_KEY_TITLE = "title";
  
  public static final String NI_RESPONSE_EXTRA_CMD = "send_ni_response";
  
  public static final String OPPO_ACTION_NEW_OUTGOING_CALL = "com.oppo.intent.action.CALL_STATE_OUTGOING";
  
  private static final String TAG = "GpsNetInitiatedHandler";
  
  private final Context mContext;
  
  private volatile boolean mIsInEmergencyCall;
  
  private volatile boolean mIsSuplEsEnabled;
  
  private final LocationManager mLocationManager;
  
  private final INetInitiatedListener mNetInitiatedListener;
  
  private Notification.Builder mNiNotificationBuilder;
  
  private final PhoneStateListener mPhoneStateListener;
  
  private final TelephonyManager mTelephonyManager;
  
  public GpsNetInitiatedHandler(Context paramContext, INetInitiatedListener paramINetInitiatedListener, boolean paramBoolean) {
    this.mContext = paramContext;
    if (paramINetInitiatedListener != null) {
      this.mNetInitiatedListener = paramINetInitiatedListener;
      setSuplEsEnabled(paramBoolean);
      this.mLocationManager = (LocationManager)paramContext.getSystemService("location");
      updateLocationMode();
      this.mTelephonyManager = (TelephonyManager)paramContext.getSystemService("phone");
      Object object = new Object(this);
      this.mTelephonyManager.listen((PhoneStateListener)object, 32);
      object = new IntentFilter();
      object.addAction("android.intent.action.NEW_OUTGOING_CALL");
      object.addAction("com.oppo.intent.action.CALL_STATE_OUTGOING");
      object.addAction("android.location.MODE_CHANGED");
      this.mContext.registerReceiver(this.mBroadcastReciever, (IntentFilter)object);
      return;
    } 
    throw new IllegalArgumentException("netInitiatedListener is null");
  }
  
  public void setSuplEsEnabled(boolean paramBoolean) {
    this.mIsSuplEsEnabled = paramBoolean;
  }
  
  public boolean getSuplEsEnabled() {
    return this.mIsSuplEsEnabled;
  }
  
  public void updateLocationMode() {
    this.mIsLocationEnabled = this.mLocationManager.isProviderEnabled("gps");
  }
  
  public boolean getLocationEnabled() {
    return this.mIsLocationEnabled;
  }
  
  public boolean getInEmergency() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mCallEndElapsedRealtimeMillis : J
    //   4: lstore_1
    //   5: iconst_1
    //   6: istore_3
    //   7: lload_1
    //   8: lconst_0
    //   9: lcmp
    //   10: ifle -> 35
    //   13: invokestatic elapsedRealtime : ()J
    //   16: aload_0
    //   17: getfield mCallEndElapsedRealtimeMillis : J
    //   20: lsub
    //   21: aload_0
    //   22: getfield mEmergencyExtensionMillis : J
    //   25: lcmp
    //   26: ifge -> 35
    //   29: iconst_1
    //   30: istore #4
    //   32: goto -> 38
    //   35: iconst_0
    //   36: istore #4
    //   38: aload_0
    //   39: getfield mTelephonyManager : Landroid/telephony/TelephonyManager;
    //   42: invokevirtual getEmergencyCallbackMode : ()Z
    //   45: istore #5
    //   47: aload_0
    //   48: getfield mTelephonyManager : Landroid/telephony/TelephonyManager;
    //   51: invokevirtual isInEmergencySmsMode : ()Z
    //   54: istore #6
    //   56: iload_3
    //   57: istore #7
    //   59: aload_0
    //   60: getfield mIsInEmergencyCall : Z
    //   63: ifne -> 96
    //   66: iload_3
    //   67: istore #7
    //   69: iload #5
    //   71: ifne -> 96
    //   74: iload_3
    //   75: istore #7
    //   77: iload #4
    //   79: ifne -> 96
    //   82: iload #6
    //   84: ifeq -> 93
    //   87: iload_3
    //   88: istore #7
    //   90: goto -> 96
    //   93: iconst_0
    //   94: istore #7
    //   96: iload #7
    //   98: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #273	-> 0
    //   #275	-> 13
    //   #277	-> 38
    //   #278	-> 47
    //   #279	-> 56
  }
  
  public void setEmergencyExtensionSeconds(int paramInt) {
    this.mEmergencyExtensionMillis = TimeUnit.SECONDS.toMillis(paramInt);
  }
  
  public void handleNiNotification(GpsNiNotification paramGpsNiNotification) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("in handleNiNotification () : notificationId: ");
      stringBuilder.append(paramGpsNiNotification.notificationId);
      stringBuilder.append(" requestorId: ");
      stringBuilder.append(paramGpsNiNotification.requestorId);
      stringBuilder.append(" text: ");
      stringBuilder.append(paramGpsNiNotification.text);
      stringBuilder.append(" mIsSuplEsEnabled");
      stringBuilder.append(getSuplEsEnabled());
      stringBuilder.append(" mIsLocationEnabled");
      stringBuilder.append(getLocationEnabled());
      String str = stringBuilder.toString();
      Log.d("GpsNetInitiatedHandler", str);
    } 
    if (getSuplEsEnabled()) {
      handleNiInEs(paramGpsNiNotification);
    } else {
      handleNi(paramGpsNiNotification);
    } 
  }
  
  private void handleNi(GpsNiNotification paramGpsNiNotification) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("in handleNi () : needNotify: ");
      stringBuilder.append(paramGpsNiNotification.needNotify);
      stringBuilder.append(" needVerify: ");
      stringBuilder.append(paramGpsNiNotification.needVerify);
      stringBuilder.append(" privacyOverride: ");
      stringBuilder.append(paramGpsNiNotification.privacyOverride);
      stringBuilder.append(" mPopupImmediately: ");
      stringBuilder.append(this.mPopupImmediately);
      stringBuilder.append(" mInEmergency: ");
      stringBuilder.append(getInEmergency());
      String str = stringBuilder.toString();
      Log.d("GpsNetInitiatedHandler", str);
    } 
    if (!getLocationEnabled() && !getInEmergency())
      try {
        this.mNetInitiatedListener.sendNiResponse(paramGpsNiNotification.notificationId, 4);
      } catch (RemoteException remoteException) {
        Log.e("GpsNetInitiatedHandler", "RemoteException in sendNiResponse");
      }  
    if (paramGpsNiNotification.needNotify)
      if (paramGpsNiNotification.needVerify && this.mPopupImmediately) {
        openNiDialog(paramGpsNiNotification);
      } else {
        setNiNotification(paramGpsNiNotification);
      }  
    if (!paramGpsNiNotification.needVerify || paramGpsNiNotification.privacyOverride)
      try {
        this.mNetInitiatedListener.sendNiResponse(paramGpsNiNotification.notificationId, 1);
      } catch (RemoteException remoteException) {
        Log.e("GpsNetInitiatedHandler", "RemoteException in sendNiResponse");
      }  
  }
  
  private void handleNiInEs(GpsNiNotification paramGpsNiNotification) {
    boolean bool;
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("in handleNiInEs () : niType: ");
      stringBuilder.append(paramGpsNiNotification.niType);
      stringBuilder.append(" notificationId: ");
      stringBuilder.append(paramGpsNiNotification.notificationId);
      Log.d("GpsNetInitiatedHandler", stringBuilder.toString());
    } 
    if (paramGpsNiNotification.niType == 4) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool != getInEmergency()) {
      try {
        this.mNetInitiatedListener.sendNiResponse(paramGpsNiNotification.notificationId, 4);
      } catch (RemoteException remoteException) {
        Log.e("GpsNetInitiatedHandler", "RemoteException in sendNiResponse");
      } 
    } else {
      handleNi((GpsNiNotification)remoteException);
    } 
  }
  
  private void setNiNotification(GpsNiNotification paramGpsNiNotification) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mContext : Landroid/content/Context;
    //   6: astore_2
    //   7: aload_2
    //   8: ldc_w 'notification'
    //   11: invokevirtual getSystemService : (Ljava/lang/String;)Ljava/lang/Object;
    //   14: checkcast android/app/NotificationManager
    //   17: astore_3
    //   18: aload_3
    //   19: ifnonnull -> 25
    //   22: aload_0
    //   23: monitorexit
    //   24: return
    //   25: aload_1
    //   26: aload_0
    //   27: getfield mContext : Landroid/content/Context;
    //   30: invokestatic getNotifTitle : (Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;Landroid/content/Context;)Ljava/lang/String;
    //   33: astore #4
    //   35: aload_1
    //   36: aload_0
    //   37: getfield mContext : Landroid/content/Context;
    //   40: invokestatic getNotifMessage : (Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;Landroid/content/Context;)Ljava/lang/String;
    //   43: astore_2
    //   44: getstatic com/android/internal/location/GpsNetInitiatedHandler.DEBUG : Z
    //   47: ifeq -> 123
    //   50: new java/lang/StringBuilder
    //   53: astore #5
    //   55: aload #5
    //   57: invokespecial <init> : ()V
    //   60: aload #5
    //   62: ldc_w 'setNiNotification, notifyId: '
    //   65: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   68: pop
    //   69: aload #5
    //   71: aload_1
    //   72: getfield notificationId : I
    //   75: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   78: pop
    //   79: aload #5
    //   81: ldc_w ', title: '
    //   84: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   87: pop
    //   88: aload #5
    //   90: aload #4
    //   92: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   95: pop
    //   96: aload #5
    //   98: ldc_w ', message: '
    //   101: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   104: pop
    //   105: aload #5
    //   107: aload_2
    //   108: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   111: pop
    //   112: ldc 'GpsNetInitiatedHandler'
    //   114: aload #5
    //   116: invokevirtual toString : ()Ljava/lang/String;
    //   119: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   122: pop
    //   123: aload_0
    //   124: getfield mNiNotificationBuilder : Landroid/app/Notification$Builder;
    //   127: ifnonnull -> 204
    //   130: new android/app/Notification$Builder
    //   133: astore #5
    //   135: aload #5
    //   137: aload_0
    //   138: getfield mContext : Landroid/content/Context;
    //   141: getstatic com/android/internal/notification/SystemNotificationChannels.NETWORK_ALERTS : Ljava/lang/String;
    //   144: invokespecial <init> : (Landroid/content/Context;Ljava/lang/String;)V
    //   147: aload #5
    //   149: ldc_w 17303605
    //   152: invokevirtual setSmallIcon : (I)Landroid/app/Notification$Builder;
    //   155: astore #5
    //   157: aload #5
    //   159: lconst_0
    //   160: invokevirtual setWhen : (J)Landroid/app/Notification$Builder;
    //   163: astore #5
    //   165: aload #5
    //   167: iconst_1
    //   168: invokevirtual setOngoing : (Z)Landroid/app/Notification$Builder;
    //   171: astore #5
    //   173: aload #5
    //   175: iconst_1
    //   176: invokevirtual setAutoCancel : (Z)Landroid/app/Notification$Builder;
    //   179: astore #6
    //   181: aload_0
    //   182: getfield mContext : Landroid/content/Context;
    //   185: astore #5
    //   187: aload_0
    //   188: aload #6
    //   190: aload #5
    //   192: ldc_w 17170460
    //   195: invokevirtual getColor : (I)I
    //   198: invokevirtual setColor : (I)Landroid/app/Notification$Builder;
    //   201: putfield mNiNotificationBuilder : Landroid/app/Notification$Builder;
    //   204: aload_0
    //   205: getfield mPlaySounds : Z
    //   208: ifeq -> 223
    //   211: aload_0
    //   212: getfield mNiNotificationBuilder : Landroid/app/Notification$Builder;
    //   215: iconst_1
    //   216: invokevirtual setDefaults : (I)Landroid/app/Notification$Builder;
    //   219: pop
    //   220: goto -> 232
    //   223: aload_0
    //   224: getfield mNiNotificationBuilder : Landroid/app/Notification$Builder;
    //   227: iconst_0
    //   228: invokevirtual setDefaults : (I)Landroid/app/Notification$Builder;
    //   231: pop
    //   232: aload_0
    //   233: getfield mNiNotificationBuilder : Landroid/app/Notification$Builder;
    //   236: aload_1
    //   237: aload_0
    //   238: getfield mContext : Landroid/content/Context;
    //   241: invokestatic getNotifTicker : (Lcom/android/internal/location/GpsNetInitiatedHandler$GpsNiNotification;Landroid/content/Context;)Ljava/lang/String;
    //   244: invokevirtual setTicker : (Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
    //   247: astore #5
    //   249: aload #5
    //   251: aload #4
    //   253: invokevirtual setContentTitle : (Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
    //   256: astore #4
    //   258: aload #4
    //   260: aload_2
    //   261: invokevirtual setContentText : (Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
    //   264: pop
    //   265: aload_3
    //   266: aconst_null
    //   267: aload_1
    //   268: getfield notificationId : I
    //   271: aload_0
    //   272: getfield mNiNotificationBuilder : Landroid/app/Notification$Builder;
    //   275: invokevirtual build : ()Landroid/app/Notification;
    //   278: getstatic android/os/UserHandle.ALL : Landroid/os/UserHandle;
    //   281: invokevirtual notifyAsUser : (Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V
    //   284: aload_0
    //   285: monitorexit
    //   286: return
    //   287: astore_1
    //   288: aload_0
    //   289: monitorexit
    //   290: aload_1
    //   291: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #392	-> 2
    //   #393	-> 7
    //   #394	-> 18
    //   #395	-> 22
    //   #398	-> 25
    //   #399	-> 35
    //   #401	-> 44
    //   #406	-> 123
    //   #407	-> 130
    //   #409	-> 147
    //   #410	-> 157
    //   #411	-> 165
    //   #412	-> 173
    //   #413	-> 187
    //   #417	-> 204
    //   #418	-> 211
    //   #420	-> 223
    //   #423	-> 232
    //   #424	-> 249
    //   #425	-> 258
    //   #427	-> 265
    //   #429	-> 284
    //   #391	-> 287
    // Exception table:
    //   from	to	target	type
    //   2	7	287	finally
    //   7	18	287	finally
    //   25	35	287	finally
    //   35	44	287	finally
    //   44	123	287	finally
    //   123	130	287	finally
    //   130	147	287	finally
    //   147	157	287	finally
    //   157	165	287	finally
    //   165	173	287	finally
    //   173	187	287	finally
    //   187	204	287	finally
    //   204	211	287	finally
    //   211	220	287	finally
    //   223	232	287	finally
    //   232	249	287	finally
    //   249	258	287	finally
    //   258	265	287	finally
    //   265	284	287	finally
  }
  
  private void openNiDialog(GpsNiNotification paramGpsNiNotification) {
    Intent intent = getDlgIntent(paramGpsNiNotification);
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("openNiDialog, notifyId: ");
      stringBuilder.append(paramGpsNiNotification.notificationId);
      stringBuilder.append(", requestorId: ");
      stringBuilder.append(paramGpsNiNotification.requestorId);
      stringBuilder.append(", text: ");
      stringBuilder.append(paramGpsNiNotification.text);
      Log.d("GpsNetInitiatedHandler", stringBuilder.toString());
    } 
    this.mContext.startActivity(intent);
  }
  
  private Intent getDlgIntent(GpsNiNotification paramGpsNiNotification) {
    Intent intent = new Intent();
    String str1 = getDialogTitle(paramGpsNiNotification, this.mContext);
    String str2 = getDialogMessage(paramGpsNiNotification, this.mContext);
    intent.setFlags(268468224);
    intent.setClass(this.mContext, NetInitiatedActivity.class);
    intent.putExtra("notif_id", paramGpsNiNotification.notificationId);
    intent.putExtra("title", str1);
    intent.putExtra("message", str2);
    intent.putExtra("timeout", paramGpsNiNotification.timeout);
    intent.putExtra("default_resp", paramGpsNiNotification.defaultResponse);
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("generateIntent, title: ");
      stringBuilder.append(str1);
      stringBuilder.append(", message: ");
      stringBuilder.append(str2);
      stringBuilder.append(", timeout: ");
      stringBuilder.append(paramGpsNiNotification.timeout);
      Log.d("GpsNetInitiatedHandler", stringBuilder.toString());
    } 
    return intent;
  }
  
  static byte[] stringToByteArray(String paramString, boolean paramBoolean) {
    int i = paramString.length(), j = i;
    if (paramBoolean)
      j = i / 2; 
    byte[] arrayOfByte = new byte[j];
    if (paramBoolean) {
      for (i = 0; i < j; i++)
        arrayOfByte[i] = (byte)Integer.parseInt(paramString.substring(i * 2, i * 2 + 2), 16); 
    } else {
      for (i = 0; i < j; i++)
        arrayOfByte[i] = (byte)paramString.charAt(i); 
    } 
    return arrayOfByte;
  }
  
  static String decodeGSMPackedString(byte[] paramArrayOfbyte) {
    int i = paramArrayOfbyte.length;
    int j = i * 8 / 7;
    int k = j;
    if (i % 7 == 0) {
      k = j;
      if (i > 0) {
        k = j;
        if (paramArrayOfbyte[i - 1] >> 1 == 0)
          k = j - 1; 
      } 
    } 
    String str2 = GsmAlphabet.gsm7BitPackedToString(paramArrayOfbyte, 0, k);
    String str1 = str2;
    if (str2 == null) {
      Log.e("GpsNetInitiatedHandler", "Decoding of GSM packed string failed");
      str1 = "";
    } 
    return str1;
  }
  
  static String decodeUTF8String(byte[] paramArrayOfbyte) {
    try {
      return new String(paramArrayOfbyte, "UTF-8");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      throw new AssertionError();
    } 
  }
  
  static String decodeUCS2String(byte[] paramArrayOfbyte) {
    try {
      return new String(paramArrayOfbyte, "UTF-16");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      throw new AssertionError();
    } 
  }
  
  private static String decodeString(String paramString, boolean paramBoolean, int paramInt) {
    StringBuilder stringBuilder;
    if (paramInt == 0 || paramInt == -1)
      return paramString; 
    byte[] arrayOfByte = stringToByteArray(paramString, paramBoolean);
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 3) {
          stringBuilder = new StringBuilder();
          stringBuilder.append("Unknown encoding ");
          stringBuilder.append(paramInt);
          stringBuilder.append(" for NI text ");
          stringBuilder.append(paramString);
          Log.e("GpsNetInitiatedHandler", stringBuilder.toString());
          return paramString;
        } 
        return decodeUCS2String((byte[])stringBuilder);
      } 
      return decodeUTF8String((byte[])stringBuilder);
    } 
    return decodeGSMPackedString((byte[])stringBuilder);
  }
  
  private static String getNotifTicker(GpsNiNotification paramGpsNiNotification, Context paramContext) {
    String str2 = paramContext.getString(17040288), str3 = paramGpsNiNotification.requestorId;
    boolean bool = mIsHexInput;
    int i = paramGpsNiNotification.requestorIdEncoding;
    str3 = decodeString(str3, bool, i);
    String str4 = paramGpsNiNotification.text;
    bool = mIsHexInput;
    i = paramGpsNiNotification.textEncoding;
    String str1 = decodeString(str4, bool, i);
    str1 = String.format(str2, new Object[] { str3, str1 });
    return str1;
  }
  
  private static String getNotifTitle(GpsNiNotification paramGpsNiNotification, Context paramContext) {
    return String.format(paramContext.getString(17040289), new Object[0]);
  }
  
  private static String getNotifMessage(GpsNiNotification paramGpsNiNotification, Context paramContext) {
    String str2 = paramContext.getString(17040287), str3 = paramGpsNiNotification.requestorId;
    boolean bool = mIsHexInput;
    int i = paramGpsNiNotification.requestorIdEncoding;
    str3 = decodeString(str3, bool, i);
    String str4 = paramGpsNiNotification.text;
    bool = mIsHexInput;
    i = paramGpsNiNotification.textEncoding;
    String str1 = decodeString(str4, bool, i);
    str1 = String.format(str2, new Object[] { str3, str1 });
    return str1;
  }
  
  public static String getDialogTitle(GpsNiNotification paramGpsNiNotification, Context paramContext) {
    return getNotifTitle(paramGpsNiNotification, paramContext);
  }
  
  private static String getDialogMessage(GpsNiNotification paramGpsNiNotification, Context paramContext) {
    return getNotifMessage(paramGpsNiNotification, paramContext);
  }
}
