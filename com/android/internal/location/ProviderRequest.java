package com.android.internal.location;

import android.location.LocationRequest;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.WorkSource;
import android.util.TimeUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public final class ProviderRequest implements Parcelable {
  public static final Parcelable.Creator<ProviderRequest> CREATOR;
  
  public static final ProviderRequest EMPTY_REQUEST = new ProviderRequest(false, Long.MAX_VALUE, false, false, Collections.emptyList(), new WorkSource());
  
  public final long interval;
  
  public final List<LocationRequest> locationRequests;
  
  public final boolean locationSettingsIgnored;
  
  public final boolean lowPowerMode;
  
  public final boolean reportLocation;
  
  public final WorkSource workSource;
  
  private ProviderRequest(boolean paramBoolean1, long paramLong, boolean paramBoolean2, boolean paramBoolean3, List<LocationRequest> paramList, WorkSource paramWorkSource) {
    this.reportLocation = paramBoolean1;
    this.interval = paramLong;
    this.lowPowerMode = paramBoolean2;
    this.locationSettingsIgnored = paramBoolean3;
    Objects.requireNonNull(paramList);
    this.locationRequests = paramList;
    Objects.requireNonNull(paramWorkSource);
    this.workSource = paramWorkSource;
  }
  
  static {
    CREATOR = new Parcelable.Creator<ProviderRequest>() {
        public ProviderRequest createFromParcel(Parcel param1Parcel) {
          boolean bool1 = param1Parcel.readBoolean();
          long l = param1Parcel.readLong();
          boolean bool2 = param1Parcel.readBoolean();
          boolean bool3 = param1Parcel.readBoolean();
          Parcelable.Creator creator1 = LocationRequest.CREATOR;
          ArrayList arrayList = param1Parcel.createTypedArrayList(creator1);
          Parcelable.Creator creator2 = WorkSource.CREATOR;
          return new ProviderRequest(bool1, l, bool2, bool3, arrayList, (WorkSource)param1Parcel.readTypedObject(creator2));
        }
        
        public ProviderRequest[] newArray(int param1Int) {
          return new ProviderRequest[param1Int];
        }
      };
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeBoolean(this.reportLocation);
    paramParcel.writeLong(this.interval);
    paramParcel.writeBoolean(this.lowPowerMode);
    paramParcel.writeBoolean(this.locationSettingsIgnored);
    paramParcel.writeTypedList(this.locationRequests);
    paramParcel.writeTypedObject((Parcelable)this.workSource, paramInt);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ProviderRequest[");
    if (this.reportLocation) {
      stringBuilder.append("interval=");
      TimeUtils.formatDuration(this.interval, stringBuilder);
      if (this.lowPowerMode)
        stringBuilder.append(", lowPowerMode"); 
      if (this.locationSettingsIgnored)
        stringBuilder.append(", locationSettingsIgnored"); 
    } else {
      stringBuilder.append("OFF");
    } 
    stringBuilder.append(']');
    return stringBuilder.toString();
  }
  
  class Builder {
    private long mInterval;
    
    private List<LocationRequest> mLocationRequests;
    
    private boolean mLocationSettingsIgnored;
    
    private boolean mLowPowerMode;
    
    private WorkSource mWorkSource;
    
    public Builder() {
      this.mInterval = Long.MAX_VALUE;
      this.mLocationRequests = Collections.emptyList();
      this.mWorkSource = new WorkSource();
    }
    
    public long getInterval() {
      return this.mInterval;
    }
    
    public Builder setInterval(long param1Long) {
      this.mInterval = param1Long;
      return this;
    }
    
    public boolean isLowPowerMode() {
      return this.mLowPowerMode;
    }
    
    public Builder setLowPowerMode(boolean param1Boolean) {
      this.mLowPowerMode = param1Boolean;
      return this;
    }
    
    public boolean isLocationSettingsIgnored() {
      return this.mLocationSettingsIgnored;
    }
    
    public Builder setLocationSettingsIgnored(boolean param1Boolean) {
      this.mLocationSettingsIgnored = param1Boolean;
      return this;
    }
    
    public List<LocationRequest> getLocationRequests() {
      return this.mLocationRequests;
    }
    
    public Builder setLocationRequests(List<LocationRequest> param1List) {
      Objects.requireNonNull(param1List);
      this.mLocationRequests = param1List;
      return this;
    }
    
    public WorkSource getWorkSource() {
      return this.mWorkSource;
    }
    
    public Builder setWorkSource(WorkSource param1WorkSource) {
      Objects.requireNonNull(param1WorkSource);
      this.mWorkSource = param1WorkSource;
      return this;
    }
    
    public ProviderRequest build() {
      if (this.mInterval == Long.MAX_VALUE)
        return ProviderRequest.EMPTY_REQUEST; 
      return new ProviderRequest(true, this.mInterval, this.mLowPowerMode, this.mLocationSettingsIgnored, this.mLocationRequests, this.mWorkSource);
    }
  }
}
