package com.android.internal.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class ProviderProperties implements Parcelable {
  public ProviderProperties(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, boolean paramBoolean7, int paramInt1, int paramInt2) {
    this.mRequiresNetwork = paramBoolean1;
    this.mRequiresSatellite = paramBoolean2;
    this.mRequiresCell = paramBoolean3;
    this.mHasMonetaryCost = paramBoolean4;
    this.mSupportsAltitude = paramBoolean5;
    this.mSupportsSpeed = paramBoolean6;
    this.mSupportsBearing = paramBoolean7;
    this.mPowerRequirement = Preconditions.checkArgumentInRange(paramInt1, 1, 3, "powerRequirement");
    this.mAccuracy = Preconditions.checkArgumentInRange(paramInt2, 1, 2, "accuracy");
  }
  
  public static final Parcelable.Creator<ProviderProperties> CREATOR = new Parcelable.Creator<ProviderProperties>() {
      public ProviderProperties createFromParcel(Parcel param1Parcel) {
        boolean bool1, bool2, bool3, bool4, bool5, bool6, bool7;
        if (param1Parcel.readInt() == 1) {
          bool1 = true;
        } else {
          bool1 = false;
        } 
        if (param1Parcel.readInt() == 1) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        if (param1Parcel.readInt() == 1) {
          bool3 = true;
        } else {
          bool3 = false;
        } 
        if (param1Parcel.readInt() == 1) {
          bool4 = true;
        } else {
          bool4 = false;
        } 
        if (param1Parcel.readInt() == 1) {
          bool5 = true;
        } else {
          bool5 = false;
        } 
        if (param1Parcel.readInt() == 1) {
          bool6 = true;
        } else {
          bool6 = false;
        } 
        if (param1Parcel.readInt() == 1) {
          bool7 = true;
        } else {
          bool7 = false;
        } 
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        return new ProviderProperties(bool1, bool2, bool3, bool4, bool5, bool6, bool7, i, j);
      }
      
      public ProviderProperties[] newArray(int param1Int) {
        return new ProviderProperties[param1Int];
      }
    };
  
  public final int mAccuracy;
  
  public final boolean mHasMonetaryCost;
  
  public final int mPowerRequirement;
  
  public final boolean mRequiresCell;
  
  public final boolean mRequiresNetwork;
  
  public final boolean mRequiresSatellite;
  
  public final boolean mSupportsAltitude;
  
  public final boolean mSupportsBearing;
  
  public final boolean mSupportsSpeed;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mRequiresNetwork);
    paramParcel.writeInt(this.mRequiresSatellite);
    paramParcel.writeInt(this.mRequiresCell);
    paramParcel.writeInt(this.mHasMonetaryCost);
    paramParcel.writeInt(this.mSupportsAltitude);
    paramParcel.writeInt(this.mSupportsSpeed);
    paramParcel.writeInt(this.mSupportsBearing);
    paramParcel.writeInt(this.mPowerRequirement);
    paramParcel.writeInt(this.mAccuracy);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("ProviderProperties[");
    stringBuilder.append("power=");
    stringBuilder.append(powerToString(this.mPowerRequirement));
    stringBuilder.append(", ");
    stringBuilder.append("accuracy=");
    stringBuilder.append(accuracyToString(this.mAccuracy));
    if (this.mRequiresNetwork || this.mRequiresSatellite || this.mRequiresCell) {
      stringBuilder.append(", requires=");
      if (this.mRequiresNetwork)
        stringBuilder.append("network,"); 
      if (this.mRequiresSatellite)
        stringBuilder.append("satellite,"); 
      if (this.mRequiresCell)
        stringBuilder.append("cell,"); 
      stringBuilder.setLength(stringBuilder.length() - 1);
    } 
    if (this.mHasMonetaryCost)
      stringBuilder.append(", hasMonetaryCost"); 
    if (this.mSupportsBearing || this.mSupportsSpeed || this.mSupportsAltitude) {
      stringBuilder.append(", supports=[");
      if (this.mSupportsBearing)
        stringBuilder.append("bearing, "); 
      if (this.mSupportsSpeed)
        stringBuilder.append("speed, "); 
      if (this.mSupportsAltitude)
        stringBuilder.append("altitude, "); 
      stringBuilder.setLength(stringBuilder.length() - 2);
      stringBuilder.append("]");
    } 
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  private static String powerToString(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 3)
          return "???"; 
        return "High";
      } 
      return "Medium";
    } 
    return "Low";
  }
  
  private static String accuracyToString(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2)
        return "???"; 
      return "Coarse";
    } 
    return "Fine";
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Accuracy implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class PowerRequirement implements Annotation {}
}
