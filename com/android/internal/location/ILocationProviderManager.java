package com.android.internal.location;

import android.location.Location;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface ILocationProviderManager extends IInterface {
  void onReportLocation(Location paramLocation) throws RemoteException;
  
  void onSetAdditionalProviderPackages(List<String> paramList) throws RemoteException;
  
  void onSetAllowed(boolean paramBoolean) throws RemoteException;
  
  void onSetProperties(ProviderProperties paramProviderProperties) throws RemoteException;
  
  class Default implements ILocationProviderManager {
    public void onSetAdditionalProviderPackages(List<String> param1List) throws RemoteException {}
    
    public void onSetAllowed(boolean param1Boolean) throws RemoteException {}
    
    public void onSetProperties(ProviderProperties param1ProviderProperties) throws RemoteException {}
    
    public void onReportLocation(Location param1Location) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ILocationProviderManager {
    private static final String DESCRIPTOR = "com.android.internal.location.ILocationProviderManager";
    
    static final int TRANSACTION_onReportLocation = 4;
    
    static final int TRANSACTION_onSetAdditionalProviderPackages = 1;
    
    static final int TRANSACTION_onSetAllowed = 2;
    
    static final int TRANSACTION_onSetProperties = 3;
    
    public Stub() {
      attachInterface(this, "com.android.internal.location.ILocationProviderManager");
    }
    
    public static ILocationProviderManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.location.ILocationProviderManager");
      if (iInterface != null && iInterface instanceof ILocationProviderManager)
        return (ILocationProviderManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "onReportLocation";
          } 
          return "onSetProperties";
        } 
        return "onSetAllowed";
      } 
      return "onSetAdditionalProviderPackages";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        boolean bool;
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("com.android.internal.location.ILocationProviderManager");
              return true;
            } 
            param1Parcel1.enforceInterface("com.android.internal.location.ILocationProviderManager");
            if (param1Parcel1.readInt() != 0) {
              Location location = (Location)Location.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onReportLocation((Location)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          } 
          param1Parcel1.enforceInterface("com.android.internal.location.ILocationProviderManager");
          if (param1Parcel1.readInt() != 0) {
            ProviderProperties providerProperties = (ProviderProperties)ProviderProperties.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel1 = null;
          } 
          onSetProperties((ProviderProperties)param1Parcel1);
          param1Parcel2.writeNoException();
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.internal.location.ILocationProviderManager");
        if (param1Parcel1.readInt() != 0) {
          bool = true;
        } else {
          bool = false;
        } 
        onSetAllowed(bool);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.location.ILocationProviderManager");
      ArrayList<String> arrayList = param1Parcel1.createStringArrayList();
      onSetAdditionalProviderPackages(arrayList);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements ILocationProviderManager {
      public static ILocationProviderManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.location.ILocationProviderManager";
      }
      
      public void onSetAdditionalProviderPackages(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.location.ILocationProviderManager");
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ILocationProviderManager.Stub.getDefaultImpl() != null) {
            ILocationProviderManager.Stub.getDefaultImpl().onSetAdditionalProviderPackages(param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onSetAllowed(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.location.ILocationProviderManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool1 && ILocationProviderManager.Stub.getDefaultImpl() != null) {
            ILocationProviderManager.Stub.getDefaultImpl().onSetAllowed(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onSetProperties(ProviderProperties param2ProviderProperties) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.location.ILocationProviderManager");
          if (param2ProviderProperties != null) {
            parcel1.writeInt(1);
            param2ProviderProperties.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && ILocationProviderManager.Stub.getDefaultImpl() != null) {
            ILocationProviderManager.Stub.getDefaultImpl().onSetProperties(param2ProviderProperties);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onReportLocation(Location param2Location) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.location.ILocationProviderManager");
          if (param2Location != null) {
            parcel1.writeInt(1);
            param2Location.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && ILocationProviderManager.Stub.getDefaultImpl() != null) {
            ILocationProviderManager.Stub.getDefaultImpl().onReportLocation(param2Location);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ILocationProviderManager param1ILocationProviderManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ILocationProviderManager != null) {
          Proxy.sDefaultImpl = param1ILocationProviderManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ILocationProviderManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
