package com.android.internal.util;

import android.os.SystemClock;
import android.util.SparseBooleanArray;
import android.util.SparseLongArray;
import java.io.PrintWriter;
import java.util.function.Supplier;

public class ProviderAccessStats {
  private final Object mLock = new Object();
  
  private final long mStartUptime = SystemClock.uptimeMillis();
  
  private final SparseBooleanArray mAllCallingUids = new SparseBooleanArray();
  
  private final SparseLongArray mQueryStats = new SparseLongArray(16);
  
  private final SparseLongArray mBatchStats = new SparseLongArray(0);
  
  private final SparseLongArray mInsertStats = new SparseLongArray(0);
  
  private final SparseLongArray mUpdateStats = new SparseLongArray(0);
  
  private final SparseLongArray mDeleteStats = new SparseLongArray(0);
  
  private final SparseLongArray mInsertInBatchStats = new SparseLongArray(0);
  
  private final SparseLongArray mUpdateInBatchStats = new SparseLongArray(0);
  
  private final SparseLongArray mDeleteInBatchStats = new SparseLongArray(0);
  
  private final SparseLongArray mOperationDurationMillis = new SparseLongArray(16);
  
  private final ThreadLocal<PerThreadData> mThreadLocal;
  
  private static class PerThreadData {
    public int nestCount;
    
    public long startUptimeMillis;
    
    private PerThreadData() {}
  }
  
  public ProviderAccessStats() {
    -$.Lambda.ProviderAccessStats.AhC6lKURctNKuYjVd-wu7jn6_c ahC6lKURctNKuYjVd-wu7jn6_c = _$$Lambda$ProviderAccessStats$9AhC6lKURctNKuYjVd_wu7jn6_c.INSTANCE;
    this.mThreadLocal = ThreadLocal.withInitial((Supplier<? extends PerThreadData>)ahC6lKURctNKuYjVd-wu7jn6_c);
  }
  
  private void incrementStats(int paramInt, SparseLongArray paramSparseLongArray) {
    synchronized (this.mLock) {
      paramSparseLongArray.put(paramInt, paramSparseLongArray.get(paramInt) + 1L);
      this.mAllCallingUids.put(paramInt, true);
      PerThreadData perThreadData = this.mThreadLocal.get();
      perThreadData.nestCount++;
      if (perThreadData.nestCount == 1)
        perThreadData.startUptimeMillis = SystemClock.uptimeMillis(); 
      return;
    } 
  }
  
  private void incrementStats(int paramInt, boolean paramBoolean, SparseLongArray paramSparseLongArray1, SparseLongArray paramSparseLongArray2) {
    if (paramBoolean)
      paramSparseLongArray1 = paramSparseLongArray2; 
    incrementStats(paramInt, paramSparseLongArray1);
  }
  
  public final void incrementInsertStats(int paramInt, boolean paramBoolean) {
    incrementStats(paramInt, paramBoolean, this.mInsertStats, this.mInsertInBatchStats);
  }
  
  public final void incrementUpdateStats(int paramInt, boolean paramBoolean) {
    incrementStats(paramInt, paramBoolean, this.mUpdateStats, this.mUpdateInBatchStats);
  }
  
  public final void incrementDeleteStats(int paramInt, boolean paramBoolean) {
    incrementStats(paramInt, paramBoolean, this.mDeleteStats, this.mDeleteInBatchStats);
  }
  
  public final void incrementQueryStats(int paramInt) {
    incrementStats(paramInt, this.mQueryStats);
  }
  
  public final void incrementBatchStats(int paramInt) {
    incrementStats(paramInt, this.mBatchStats);
  }
  
  public void finishOperation(int paramInt) {
    PerThreadData perThreadData = this.mThreadLocal.get();
    perThreadData.nestCount--;
    if (perThreadData.nestCount == 0) {
      long l = Math.max(1L, SystemClock.uptimeMillis() - perThreadData.startUptimeMillis);
      synchronized (this.mLock) {
        SparseLongArray sparseLongArray1 = this.mOperationDurationMillis, sparseLongArray2 = this.mOperationDurationMillis;
        long l1 = sparseLongArray2.get(paramInt);
        sparseLongArray1.put(paramInt, l1 + l);
      } 
    } 
  }
  
  public void dump(PrintWriter paramPrintWriter, String paramString) {
    synchronized (this.mLock) {
      paramPrintWriter.print("  Process uptime: ");
      paramPrintWriter.print((SystemClock.uptimeMillis() - this.mStartUptime) / 60000L);
      paramPrintWriter.println(" minutes");
      paramPrintWriter.println();
      paramPrintWriter.print(paramString);
      paramPrintWriter.println("Client activities:");
      paramPrintWriter.print(paramString);
      paramPrintWriter.println("  UID        Query  Insert Update Delete   Batch Insert Update Delete          Sec");
      for (byte b = 0; b < this.mAllCallingUids.size(); b++) {
        int i = this.mAllCallingUids.keyAt(b);
        paramPrintWriter.print(paramString);
        SparseLongArray sparseLongArray = this.mQueryStats;
        long l1 = sparseLongArray.get(i);
        sparseLongArray = this.mInsertStats;
        long l2 = sparseLongArray.get(i);
        sparseLongArray = this.mUpdateStats;
        long l3 = sparseLongArray.get(i);
        sparseLongArray = this.mDeleteStats;
        long l4 = sparseLongArray.get(i);
        sparseLongArray = this.mBatchStats;
        long l5 = sparseLongArray.get(i);
        sparseLongArray = this.mInsertInBatchStats;
        long l6 = sparseLongArray.get(i);
        sparseLongArray = this.mUpdateInBatchStats;
        long l7 = sparseLongArray.get(i);
        sparseLongArray = this.mDeleteInBatchStats;
        long l8 = sparseLongArray.get(i);
        sparseLongArray = this.mOperationDurationMillis;
        double d = sparseLongArray.get(i) / 1000.0D;
        paramPrintWriter.println(String.format("  %-9d %6d  %6d %6d %6d  %6d %6d %6d %6d %12.3f", new Object[] { Integer.valueOf(i), Long.valueOf(l1), Long.valueOf(l2), Long.valueOf(l3), Long.valueOf(l4), Long.valueOf(l5), Long.valueOf(l6), Long.valueOf(l7), Long.valueOf(l8), Double.valueOf(d) }));
      } 
      paramPrintWriter.println();
      return;
    } 
  }
}
