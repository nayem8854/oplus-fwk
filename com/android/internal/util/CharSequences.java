package com.android.internal.util;

public class CharSequences {
  public static CharSequence forAsciiBytes(final byte[] bytes) {
    return new CharSequence() {
        final byte[] val$bytes;
        
        public char charAt(int param1Int) {
          return (char)bytes[param1Int];
        }
        
        public int length() {
          return bytes.length;
        }
        
        public CharSequence subSequence(int param1Int1, int param1Int2) {
          return CharSequences.forAsciiBytes(bytes, param1Int1, param1Int2);
        }
        
        public String toString() {
          return new String(bytes);
        }
      };
  }
  
  public static CharSequence forAsciiBytes(final byte[] bytes, final int start, final int end) {
    validate(start, end, bytes.length);
    return new CharSequence() {
        final byte[] val$bytes;
        
        final int val$end;
        
        final int val$start;
        
        public char charAt(int param1Int) {
          return (char)bytes[start + param1Int];
        }
        
        public int length() {
          return end - start;
        }
        
        public CharSequence subSequence(int param1Int1, int param1Int2) {
          int i = start;
          param1Int1 -= i;
          param1Int2 -= i;
          CharSequences.validate(param1Int1, param1Int2, length());
          return CharSequences.forAsciiBytes(bytes, param1Int1, param1Int2);
        }
        
        public String toString() {
          return new String(bytes, start, length());
        }
      };
  }
  
  static void validate(int paramInt1, int paramInt2, int paramInt3) {
    if (paramInt1 >= 0) {
      if (paramInt2 >= 0) {
        if (paramInt2 <= paramInt3) {
          if (paramInt1 <= paramInt2)
            return; 
          throw new IndexOutOfBoundsException();
        } 
        throw new IndexOutOfBoundsException();
      } 
      throw new IndexOutOfBoundsException();
    } 
    throw new IndexOutOfBoundsException();
  }
  
  public static boolean equals(CharSequence paramCharSequence1, CharSequence paramCharSequence2) {
    if (paramCharSequence1.length() != paramCharSequence2.length())
      return false; 
    int i = paramCharSequence1.length();
    for (byte b = 0; b < i; b++) {
      if (paramCharSequence1.charAt(b) != paramCharSequence2.charAt(b))
        return false; 
    } 
    return true;
  }
  
  public static int compareToIgnoreCase(CharSequence paramCharSequence1, CharSequence paramCharSequence2) {
    int k, i = paramCharSequence1.length(), j = paramCharSequence2.length();
    byte b1 = 0, b2 = 0;
    if (i < j) {
      k = i;
    } else {
      k = j;
    } 
    while (b1 < k) {
      char c = Character.toLowerCase(paramCharSequence1.charAt(b1));
      int m = c - Character.toLowerCase(paramCharSequence2.charAt(b2));
      if (m != 0)
        return m; 
      b1++;
      b2++;
    } 
    return i - j;
  }
}
