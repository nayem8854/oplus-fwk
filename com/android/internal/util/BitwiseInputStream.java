package com.android.internal.util;

public class BitwiseInputStream {
  private byte[] mBuf;
  
  private int mEnd;
  
  private int mPos;
  
  public static class AccessException extends Exception {
    public AccessException(String param1String) {
      super(stringBuilder.toString());
    }
  }
  
  public BitwiseInputStream(byte[] paramArrayOfbyte) {
    this.mBuf = paramArrayOfbyte;
    this.mEnd = paramArrayOfbyte.length << 3;
    this.mPos = 0;
  }
  
  public int available() {
    return this.mEnd - this.mPos;
  }
  
  public int read(int paramInt) throws AccessException {
    int i = this.mPos, j = i >>> 3;
    int k = 16 - (i & 0x7) - paramInt;
    if (paramInt >= 0 && paramInt <= 8 && i + paramInt <= this.mEnd) {
      byte[] arrayOfByte = this.mBuf;
      int m = (arrayOfByte[j] & 0xFF) << 8;
      i = m;
      if (k < 8)
        i = m | arrayOfByte[j + 1] & 0xFF; 
      this.mPos += paramInt;
      return i >>> k & -1 >>> 32 - paramInt;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("illegal read (pos ");
    stringBuilder.append(this.mPos);
    stringBuilder.append(", end ");
    stringBuilder.append(this.mEnd);
    stringBuilder.append(", bits ");
    stringBuilder.append(paramInt);
    stringBuilder.append(")");
    throw new AccessException(stringBuilder.toString());
  }
  
  public byte[] readByteArray(int paramInt) throws AccessException {
    if ((paramInt & 0x7) > 0) {
      b = 1;
    } else {
      b = 0;
    } 
    int i = (paramInt >>> 3) + b;
    byte[] arrayOfByte = new byte[i];
    for (byte b = 0; b < i; b++) {
      int j = Math.min(8, paramInt - (b << 3));
      arrayOfByte[b] = (byte)(read(j) << 8 - j);
    } 
    return arrayOfByte;
  }
  
  public void skip(int paramInt) throws AccessException {
    int i = this.mPos;
    if (i + paramInt <= this.mEnd) {
      this.mPos = i + paramInt;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("illegal skip (pos ");
    stringBuilder.append(this.mPos);
    stringBuilder.append(", end ");
    stringBuilder.append(this.mEnd);
    stringBuilder.append(", bits ");
    stringBuilder.append(paramInt);
    stringBuilder.append(")");
    throw new AccessException(stringBuilder.toString());
  }
}
