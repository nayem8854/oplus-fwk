package com.android.internal.util;

import android.util.Log;
import android.util.SparseArray;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class MessageUtils {
  private static final boolean DBG = false;
  
  public static final String[] DEFAULT_PREFIXES;
  
  private static final String TAG = MessageUtils.class.getSimpleName();
  
  public static class DuplicateConstantError extends Error {
    private DuplicateConstantError() {}
    
    public DuplicateConstantError(String param1String1, String param1String2, int param1Int) {
      super(String.format("Duplicate constant value: both %s and %s = %d", new Object[] { param1String1, param1String2, Integer.valueOf(param1Int) }));
    }
  }
  
  public static SparseArray<String> findMessageNames(Class[] paramArrayOfClass, String[] paramArrayOfString) {
    SparseArray<String> sparseArray = new SparseArray();
    int i;
    byte b;
    for (i = paramArrayOfClass.length, b = 0; b < i; ) {
      Class clazz = paramArrayOfClass[b];
      String str = clazz.getName();
      try {
        for (Field field : clazz.getDeclaredFields()) {
          int j = field.getModifiers();
          if ((Modifier.isStatic(j) ^ true | Modifier.isFinal(j) ^ true) == 0) {
            String str1 = field.getName();
            for (int k = paramArrayOfString.length; j < k; ) {
              String str2 = paramArrayOfString[j];
              if (str1.startsWith(str2))
                try {
                  field.setAccessible(true);
                  try {
                    int m = field.getInt(null);
                    String str3 = (String)sparseArray.get(m);
                    if (str3 == null || str3.equals(str1)) {
                      sparseArray.put(m, str1);
                    } else {
                      DuplicateConstantError duplicateConstantError = new DuplicateConstantError();
                      this(str1, str3, m);
                      throw duplicateConstantError;
                    } 
                  } catch (IllegalArgumentException|ExceptionInInitializerError illegalArgumentException) {
                    break;
                  } 
                } catch (SecurityException|IllegalAccessException securityException) {} 
              j++;
            } 
          } 
        } 
      } catch (SecurityException securityException) {
        String str1 = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Can't list fields of class ");
        stringBuilder.append((String)illegalArgumentException);
        Log.e(str1, stringBuilder.toString());
      } 
      b++;
    } 
    return sparseArray;
  }
  
  static {
    DEFAULT_PREFIXES = new String[] { "CMD_", "EVENT_" };
  }
  
  public static SparseArray<String> findMessageNames(Class[] paramArrayOfClass) {
    return findMessageNames(paramArrayOfClass, DEFAULT_PREFIXES);
  }
}
