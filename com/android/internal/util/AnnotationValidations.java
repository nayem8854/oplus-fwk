package com.android.internal.util;

import android.annotation.AppIdInt;
import android.annotation.ColorInt;
import android.annotation.FloatRange;
import android.annotation.IntRange;
import android.annotation.NonNull;
import android.annotation.Size;
import android.annotation.UserIdInt;
import android.content.Intent;
import android.content.pm.PackageManager;
import java.lang.annotation.Annotation;

public class AnnotationValidations {
  public static void validate(Class<UserIdInt> paramClass, UserIdInt paramUserIdInt, int paramInt) {
    if ((paramInt != -10000 && paramInt < -3) || paramInt > 21474)
      invalid((Class)paramClass, Integer.valueOf(paramInt)); 
  }
  
  public static void validate(Class<AppIdInt> paramClass, AppIdInt paramAppIdInt, int paramInt) {
    if (paramInt / 100000 != 0 || paramInt < 0)
      invalid((Class)paramClass, Integer.valueOf(paramInt)); 
  }
  
  public static void validate(Class<IntRange> paramClass, IntRange paramIntRange, int paramInt, String paramString1, long paramLong1, String paramString2, long paramLong2) {
    validate(paramClass, paramIntRange, paramInt, paramString1, paramLong1);
    validate(paramClass, paramIntRange, paramInt, paramString2, paramLong2);
  }
  
  public static void validate(Class<IntRange> paramClass, IntRange paramIntRange, int paramInt, String paramString, long paramLong) {
    // Byte code:
    //   0: aload_3
    //   1: invokevirtual hashCode : ()I
    //   4: istore #6
    //   6: iload #6
    //   8: sipush #3707
    //   11: if_icmpeq -> 39
    //   14: iload #6
    //   16: ldc 3151786
    //   18: if_icmpeq -> 24
    //   21: goto -> 54
    //   24: aload_3
    //   25: ldc 'from'
    //   27: invokevirtual equals : (Ljava/lang/Object;)Z
    //   30: ifeq -> 21
    //   33: iconst_0
    //   34: istore #6
    //   36: goto -> 57
    //   39: aload_3
    //   40: ldc 'to'
    //   42: invokevirtual equals : (Ljava/lang/Object;)Z
    //   45: ifeq -> 21
    //   48: iconst_1
    //   49: istore #6
    //   51: goto -> 57
    //   54: iconst_m1
    //   55: istore #6
    //   57: iload #6
    //   59: ifeq -> 96
    //   62: iload #6
    //   64: iconst_1
    //   65: if_icmpeq -> 71
    //   68: goto -> 118
    //   71: iload_2
    //   72: i2l
    //   73: lload #4
    //   75: lcmp
    //   76: ifle -> 118
    //   79: aload_0
    //   80: iload_2
    //   81: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   84: aload_3
    //   85: lload #4
    //   87: invokestatic valueOf : (J)Ljava/lang/Long;
    //   90: invokestatic invalid : (Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V
    //   93: goto -> 118
    //   96: iload_2
    //   97: i2l
    //   98: lload #4
    //   100: lcmp
    //   101: ifge -> 118
    //   104: aload_0
    //   105: iload_2
    //   106: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   109: aload_3
    //   110: lload #4
    //   112: invokestatic valueOf : (J)Ljava/lang/Long;
    //   115: invokestatic invalid : (Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V
    //   118: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #71	-> 0
    //   #78	-> 71
    //   #79	-> 79
    //   #73	-> 96
    //   #74	-> 104
    //   #83	-> 118
  }
  
  public static void validate(Class<IntRange> paramClass, IntRange paramIntRange, long paramLong1, String paramString1, long paramLong2, String paramString2, long paramLong3) {
    validate(paramClass, paramIntRange, paramLong1, paramString1, paramLong2);
    validate(paramClass, paramIntRange, paramLong1, paramString2, paramLong3);
  }
  
  public static void validate(Class<IntRange> paramClass, IntRange paramIntRange, long paramLong1, String paramString, long paramLong2) {
    // Byte code:
    //   0: aload #4
    //   2: invokevirtual hashCode : ()I
    //   5: istore #7
    //   7: iload #7
    //   9: sipush #3707
    //   12: if_icmpeq -> 41
    //   15: iload #7
    //   17: ldc 3151786
    //   19: if_icmpeq -> 25
    //   22: goto -> 57
    //   25: aload #4
    //   27: ldc 'from'
    //   29: invokevirtual equals : (Ljava/lang/Object;)Z
    //   32: ifeq -> 22
    //   35: iconst_0
    //   36: istore #7
    //   38: goto -> 60
    //   41: aload #4
    //   43: ldc 'to'
    //   45: invokevirtual equals : (Ljava/lang/Object;)Z
    //   48: ifeq -> 22
    //   51: iconst_1
    //   52: istore #7
    //   54: goto -> 60
    //   57: iconst_m1
    //   58: istore #7
    //   60: iload #7
    //   62: ifeq -> 99
    //   65: iload #7
    //   67: iconst_1
    //   68: if_icmpeq -> 74
    //   71: goto -> 121
    //   74: lload_2
    //   75: lload #5
    //   77: lcmp
    //   78: ifle -> 121
    //   81: aload_0
    //   82: lload_2
    //   83: invokestatic valueOf : (J)Ljava/lang/Long;
    //   86: aload #4
    //   88: lload #5
    //   90: invokestatic valueOf : (J)Ljava/lang/Long;
    //   93: invokestatic invalid : (Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V
    //   96: goto -> 121
    //   99: lload_2
    //   100: lload #5
    //   102: lcmp
    //   103: ifge -> 121
    //   106: aload_0
    //   107: lload_2
    //   108: invokestatic valueOf : (J)Ljava/lang/Long;
    //   111: aload #4
    //   113: lload #5
    //   115: invokestatic valueOf : (J)Ljava/lang/Long;
    //   118: invokestatic invalid : (Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V
    //   121: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #99	-> 0
    //   #106	-> 74
    //   #107	-> 81
    //   #101	-> 99
    //   #102	-> 106
    //   #111	-> 121
  }
  
  public static void validate(Class<FloatRange> paramClass, FloatRange paramFloatRange, float paramFloat1, String paramString1, float paramFloat2, String paramString2, float paramFloat3) {
    validate(paramClass, paramFloatRange, paramFloat1, paramString1, paramFloat2);
    validate(paramClass, paramFloatRange, paramFloat1, paramString2, paramFloat3);
  }
  
  public static void validate(Class<FloatRange> paramClass, FloatRange paramFloatRange, float paramFloat1, String paramString, float paramFloat2) {
    // Byte code:
    //   0: aload_3
    //   1: invokevirtual hashCode : ()I
    //   4: istore #5
    //   6: iload #5
    //   8: sipush #3707
    //   11: if_icmpeq -> 39
    //   14: iload #5
    //   16: ldc 3151786
    //   18: if_icmpeq -> 24
    //   21: goto -> 54
    //   24: aload_3
    //   25: ldc 'from'
    //   27: invokevirtual equals : (Ljava/lang/Object;)Z
    //   30: ifeq -> 21
    //   33: iconst_0
    //   34: istore #5
    //   36: goto -> 57
    //   39: aload_3
    //   40: ldc 'to'
    //   42: invokevirtual equals : (Ljava/lang/Object;)Z
    //   45: ifeq -> 21
    //   48: iconst_1
    //   49: istore #5
    //   51: goto -> 57
    //   54: iconst_m1
    //   55: istore #5
    //   57: iload #5
    //   59: ifeq -> 95
    //   62: iload #5
    //   64: iconst_1
    //   65: if_icmpeq -> 71
    //   68: goto -> 116
    //   71: fload_2
    //   72: fload #4
    //   74: fcmpl
    //   75: ifle -> 116
    //   78: aload_0
    //   79: fload_2
    //   80: invokestatic valueOf : (F)Ljava/lang/Float;
    //   83: aload_3
    //   84: fload #4
    //   86: invokestatic valueOf : (F)Ljava/lang/Float;
    //   89: invokestatic invalid : (Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V
    //   92: goto -> 116
    //   95: fload_2
    //   96: fload #4
    //   98: fcmpg
    //   99: ifge -> 116
    //   102: aload_0
    //   103: fload_2
    //   104: invokestatic valueOf : (F)Ljava/lang/Float;
    //   107: aload_3
    //   108: fload #4
    //   110: invokestatic valueOf : (F)Ljava/lang/Float;
    //   113: invokestatic invalid : (Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V
    //   116: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #121	-> 0
    //   #123	-> 71
    //   #122	-> 95
    //   #125	-> 116
  }
  
  public static void validate(Class<NonNull> paramClass, NonNull paramNonNull, Object paramObject) {
    if (paramObject != null)
      return; 
    throw null;
  }
  
  public static void validate(Class<Size> paramClass, Size paramSize, int paramInt1, String paramString1, int paramInt2, String paramString2, int paramInt3) {
    validate(paramClass, paramSize, paramInt1, paramString1, paramInt2);
    validate(paramClass, paramSize, paramInt1, paramString2, paramInt3);
  }
  
  public static void validate(Class<Size> paramClass, Size paramSize, int paramInt1, String paramString, int paramInt2) {
    byte b;
    switch (paramString.hashCode()) {
      default:
        b = -1;
        break;
      case 653829648:
        if (paramString.equals("multiple")) {
          b = 3;
          break;
        } 
      case 111972721:
        if (paramString.equals("value")) {
          b = 0;
          break;
        } 
      case 108114:
        if (paramString.equals("min")) {
          b = 1;
          break;
        } 
      case 107876:
        if (paramString.equals("max")) {
          b = 2;
          break;
        } 
    } 
    if (b != 0) {
      if (b != 1) {
        if (b != 2) {
          if (b == 3)
            if (paramInt1 % paramInt2 != 0)
              invalid((Class)paramClass, Integer.valueOf(paramInt1), paramString, Integer.valueOf(paramInt2));  
        } else if (paramInt1 > paramInt2) {
          invalid((Class)paramClass, Integer.valueOf(paramInt1), paramString, Integer.valueOf(paramInt2));
        } 
      } else if (paramInt1 < paramInt2) {
        invalid((Class)paramClass, Integer.valueOf(paramInt1), paramString, Integer.valueOf(paramInt2));
      } 
    } else if (paramInt2 != -1 && paramInt1 != paramInt2) {
      invalid((Class)paramClass, Integer.valueOf(paramInt1), paramString, Integer.valueOf(paramInt2));
    } 
  }
  
  public static void validate(Class<PackageManager.PermissionResult> paramClass, PackageManager.PermissionResult paramPermissionResult, int paramInt) {
    validateIntEnum((Class)paramClass, paramInt, 0);
  }
  
  public static void validate(Class<PackageManager.PackageInfoFlags> paramClass, PackageManager.PackageInfoFlags paramPackageInfoFlags, int paramInt) {
    int i = BitUtils.flagsUpTo(536870912);
    validateIntFlags((Class)paramClass, paramInt, i);
  }
  
  public static void validate(Class<Intent.Flags> paramClass, Intent.Flags paramFlags, int paramInt) {
    validateIntFlags((Class)paramClass, paramInt, BitUtils.flagsUpTo(-2147483648));
  }
  
  @Deprecated
  public static void validate(Class<? extends Annotation> paramClass, Annotation paramAnnotation, Object paramObject, Object... paramVarArgs) {}
  
  @Deprecated
  public static void validate(Class<? extends Annotation> paramClass, Annotation paramAnnotation, Object paramObject) {}
  
  @Deprecated
  public static void validate(Class<? extends Annotation> paramClass, Annotation paramAnnotation, int paramInt, Object... paramVarArgs) {}
  
  public static void validate(Class<? extends Annotation> paramClass, Annotation paramAnnotation, int paramInt) {
    if ((("android.annotation".equals(paramClass.getPackageName$()) && 
      paramClass.getSimpleName().endsWith("Res")) || 
      ColorInt.class.equals(paramClass)) && 
      paramInt < 0)
      invalid(paramClass, Integer.valueOf(paramInt)); 
  }
  
  public static void validate(Class<? extends Annotation> paramClass, Annotation paramAnnotation, long paramLong) {
    if ("android.annotation".equals(paramClass.getPackageName$()) && 
      paramClass.getSimpleName().endsWith("Long") && 
      paramLong < 0L)
      invalid(paramClass, Long.valueOf(paramLong)); 
  }
  
  private static void validateIntEnum(Class<? extends Annotation> paramClass, int paramInt1, int paramInt2) {
    if (paramInt1 > paramInt2)
      invalid(paramClass, Integer.valueOf(paramInt1)); 
  }
  
  private static void validateIntFlags(Class<? extends Annotation> paramClass, int paramInt1, int paramInt2) {
    if ((paramInt2 & paramInt1) != paramInt2) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString(paramInt1));
      invalid(paramClass, stringBuilder.toString());
    } 
  }
  
  private static void invalid(Class<? extends Annotation> paramClass, Object paramObject) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("@");
    stringBuilder.append(paramClass.getSimpleName());
    invalid(stringBuilder.toString(), paramObject);
  }
  
  private static void invalid(Class<? extends Annotation> paramClass, Object paramObject1, String paramString, Object paramObject2) {
    if ("value".equals(paramString)) {
      paramString = "";
    } else {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString);
      stringBuilder1.append(" = ");
      paramString = stringBuilder1.toString();
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("@");
    stringBuilder.append(paramClass.getSimpleName());
    stringBuilder.append("(");
    stringBuilder.append(paramString);
    stringBuilder.append(paramObject2);
    stringBuilder.append(")");
    invalid(stringBuilder.toString(), paramObject1);
  }
  
  private static void invalid(String paramString, Object paramObject) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid ");
    stringBuilder.append(paramString);
    stringBuilder.append(": ");
    stringBuilder.append(paramObject);
    throw new IllegalStateException(stringBuilder.toString());
  }
}
