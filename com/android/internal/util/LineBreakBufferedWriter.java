package com.android.internal.util;

import java.io.PrintWriter;
import java.io.Writer;
import java.util.Arrays;

public class LineBreakBufferedWriter extends PrintWriter {
  private char[] buffer;
  
  private int bufferIndex;
  
  private final int bufferSize;
  
  private int lastNewline = -1;
  
  private final String lineSeparator;
  
  public LineBreakBufferedWriter(Writer paramWriter, int paramInt) {
    this(paramWriter, paramInt, 16);
  }
  
  public LineBreakBufferedWriter(Writer paramWriter, int paramInt1, int paramInt2) {
    super(paramWriter);
    this.buffer = new char[Math.min(paramInt2, paramInt1)];
    this.bufferIndex = 0;
    this.bufferSize = paramInt1;
    this.lineSeparator = System.getProperty("line.separator");
  }
  
  public void flush() {
    writeBuffer(this.bufferIndex);
    this.bufferIndex = 0;
    super.flush();
  }
  
  public void write(int paramInt) {
    int i = this.bufferIndex;
    char[] arrayOfChar = this.buffer;
    if (i < arrayOfChar.length) {
      arrayOfChar[i] = (char)paramInt;
      this.bufferIndex = ++i;
      if ((char)paramInt == '\n')
        this.lastNewline = i; 
    } else {
      write(new char[] { (char)paramInt }, 0, 1);
    } 
  }
  
  public void println() {
    write(this.lineSeparator);
  }
  
  public void write(char[] paramArrayOfchar, int paramInt1, int paramInt2) {
    int i = paramInt1;
    while (true) {
      int j = this.bufferIndex, k = this.bufferSize;
      if (j + paramInt2 > k) {
        int m = -1;
        for (paramInt1 = 0; paramInt1 < k - j; paramInt1++, m = n) {
          int n = m;
          if (paramArrayOfchar[i + paramInt1] == '\n')
            if (this.bufferIndex + paramInt1 < this.bufferSize) {
              n = paramInt1;
            } else {
              break;
            }  
        } 
        if (m != -1) {
          appendToBuffer(paramArrayOfchar, i, m);
          writeBuffer(this.bufferIndex);
          this.bufferIndex = 0;
          this.lastNewline = -1;
          paramInt1 = i + m + 1;
          paramInt2 -= m + 1;
        } else {
          paramInt1 = this.lastNewline;
          if (paramInt1 != -1) {
            writeBuffer(paramInt1);
            removeFromBuffer(this.lastNewline + 1);
            this.lastNewline = -1;
            paramInt1 = i;
          } else {
            m = this.bufferSize - this.bufferIndex;
            appendToBuffer(paramArrayOfchar, i, m);
            writeBuffer(this.bufferIndex);
            this.bufferIndex = 0;
            paramInt1 = i + m;
            paramInt2 -= m;
          } 
        } 
        i = paramInt1;
        continue;
      } 
      break;
    } 
    if (paramInt2 > 0) {
      appendToBuffer(paramArrayOfchar, i, paramInt2);
      for (paramInt1 = paramInt2 - 1; paramInt1 >= 0; paramInt1--) {
        if (paramArrayOfchar[i + paramInt1] == '\n') {
          this.lastNewline = this.bufferIndex - paramInt2 + paramInt1;
          break;
        } 
      } 
    } 
  }
  
  public void write(String paramString, int paramInt1, int paramInt2) {
    int i = paramInt2;
    paramInt2 = paramInt1;
    while (true) {
      int j = this.bufferIndex, k = this.bufferSize;
      if (j + i > k) {
        int m = -1;
        for (paramInt1 = 0; paramInt1 < k - j; paramInt1++, m = n) {
          int n = m;
          if (paramString.charAt(paramInt2 + paramInt1) == '\n')
            if (this.bufferIndex + paramInt1 < this.bufferSize) {
              n = paramInt1;
            } else {
              break;
            }  
        } 
        if (m != -1) {
          appendToBuffer(paramString, paramInt2, m);
          writeBuffer(this.bufferIndex);
          this.bufferIndex = 0;
          this.lastNewline = -1;
          paramInt2 += m + 1;
          paramInt1 = i - m + 1;
        } else {
          paramInt1 = this.lastNewline;
          if (paramInt1 != -1) {
            writeBuffer(paramInt1);
            removeFromBuffer(this.lastNewline + 1);
            this.lastNewline = -1;
            paramInt1 = i;
          } else {
            paramInt1 = this.bufferSize - this.bufferIndex;
            appendToBuffer(paramString, paramInt2, paramInt1);
            writeBuffer(this.bufferIndex);
            this.bufferIndex = 0;
            paramInt2 += paramInt1;
            paramInt1 = i - paramInt1;
          } 
        } 
        i = paramInt1;
        continue;
      } 
      break;
    } 
    if (i > 0) {
      appendToBuffer(paramString, paramInt2, i);
      for (paramInt1 = i - 1; paramInt1 >= 0; paramInt1--) {
        if (paramString.charAt(paramInt2 + paramInt1) == '\n') {
          this.lastNewline = this.bufferIndex - i + paramInt1;
          break;
        } 
      } 
    } 
  }
  
  private void appendToBuffer(char[] paramArrayOfchar, int paramInt1, int paramInt2) {
    int i = this.bufferIndex;
    if (i + paramInt2 > this.buffer.length)
      ensureCapacity(i + paramInt2); 
    System.arraycopy(paramArrayOfchar, paramInt1, this.buffer, this.bufferIndex, paramInt2);
    this.bufferIndex += paramInt2;
  }
  
  private void appendToBuffer(String paramString, int paramInt1, int paramInt2) {
    int i = this.bufferIndex;
    if (i + paramInt2 > this.buffer.length)
      ensureCapacity(i + paramInt2); 
    paramString.getChars(paramInt1, paramInt1 + paramInt2, this.buffer, this.bufferIndex);
    this.bufferIndex += paramInt2;
  }
  
  private void ensureCapacity(int paramInt) {
    int i = this.buffer.length * 2 + 2;
    int j = i;
    if (i < paramInt)
      j = paramInt; 
    this.buffer = Arrays.copyOf(this.buffer, j);
  }
  
  private void removeFromBuffer(int paramInt) {
    int i = this.bufferIndex;
    paramInt = i - paramInt;
    if (paramInt > 0) {
      char[] arrayOfChar = this.buffer;
      System.arraycopy(arrayOfChar, i - paramInt, arrayOfChar, 0, paramInt);
      this.bufferIndex = paramInt;
    } else {
      this.bufferIndex = 0;
    } 
  }
  
  private void writeBuffer(int paramInt) {
    if (paramInt > 0)
      super.write(this.buffer, 0, paramInt); 
  }
}
