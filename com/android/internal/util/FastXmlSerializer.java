package com.android.internal.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;
import org.xmlpull.v1.XmlSerializer;

public class FastXmlSerializer implements XmlSerializer {
  private static final String[] ESCAPE_TABLE = new String[] { 
      "&#0;", "&#1;", "&#2;", "&#3;", "&#4;", "&#5;", "&#6;", "&#7;", "&#8;", "&#9;", 
      "&#10;", "&#11;", "&#12;", "&#13;", "&#14;", "&#15;", "&#16;", "&#17;", "&#18;", "&#19;", 
      "&#20;", "&#21;", "&#22;", "&#23;", "&#24;", "&#25;", "&#26;", "&#27;", "&#28;", "&#29;", 
      "&#30;", "&#31;", null, null, "&quot;", null, null, null, "&amp;", null, 
      null, null, null, null, null, null, null, null, null, null, 
      null, null, null, null, null, null, null, null, null, null, 
      "&lt;", null, "&gt;", null };
  
  private static String sSpace = "                                                              ";
  
  private boolean mIndent = false;
  
  private int mNesting = 0;
  
  private boolean mLineStart = true;
  
  private static final int DEFAULT_BUFFER_LEN = 32768;
  
  private final int mBufferLen;
  
  private ByteBuffer mBytes;
  
  private CharsetEncoder mCharset;
  
  private boolean mInTag;
  
  private OutputStream mOutputStream;
  
  private int mPos;
  
  private final char[] mText;
  
  private Writer mWriter;
  
  public FastXmlSerializer() {
    this(32768);
  }
  
  public FastXmlSerializer(int paramInt) {
    if (paramInt <= 0)
      paramInt = 32768; 
    this.mBufferLen = paramInt;
    this.mText = new char[paramInt];
    this.mBytes = ByteBuffer.allocate(paramInt);
  }
  
  private void append(char paramChar) throws IOException {
    int i = this.mPos;
    int j = i;
    if (i >= this.mBufferLen - 1) {
      flush();
      j = this.mPos;
    } 
    this.mText[j] = paramChar;
    this.mPos = j + 1;
  }
  
  private void append(String paramString, int paramInt1, int paramInt2) throws IOException {
    int i = this.mBufferLen;
    if (paramInt2 > i) {
      int m = paramInt1 + paramInt2;
      while (paramInt1 < m) {
        paramInt2 = this.mBufferLen;
        int n = paramInt1 + paramInt2;
        if (n >= m)
          paramInt2 = m - paramInt1; 
        append(paramString, paramInt1, paramInt2);
        paramInt1 = n;
      } 
      return;
    } 
    int j = this.mPos;
    int k = j;
    if (j + paramInt2 > i) {
      flush();
      k = this.mPos;
    } 
    paramString.getChars(paramInt1, paramInt1 + paramInt2, this.mText, k);
    this.mPos = k + paramInt2;
  }
  
  private void append(char[] paramArrayOfchar, int paramInt1, int paramInt2) throws IOException {
    int i = this.mBufferLen;
    if (paramInt2 > i) {
      int m = paramInt1 + paramInt2;
      while (paramInt1 < m) {
        paramInt2 = this.mBufferLen;
        int n = paramInt1 + paramInt2;
        if (n >= m)
          paramInt2 = m - paramInt1; 
        append(paramArrayOfchar, paramInt1, paramInt2);
        paramInt1 = n;
      } 
      return;
    } 
    int j = this.mPos;
    int k = j;
    if (j + paramInt2 > i) {
      flush();
      k = this.mPos;
    } 
    System.arraycopy(paramArrayOfchar, paramInt1, this.mText, k, paramInt2);
    this.mPos = k + paramInt2;
  }
  
  private void append(String paramString) throws IOException {
    append(paramString, 0, paramString.length());
  }
  
  private void appendIndent(int paramInt) throws IOException {
    int i = paramInt * 4;
    paramInt = i;
    if (i > sSpace.length())
      paramInt = sSpace.length(); 
    append(sSpace, 0, paramInt);
  }
  
  private void escapeAndAppendString(String paramString) throws IOException {
    int i = paramString.length();
    char c = (char)ESCAPE_TABLE.length;
    String[] arrayOfString = ESCAPE_TABLE;
    int j = 0;
    byte b;
    for (b = 0; b < i; b++) {
      char c1 = paramString.charAt(b);
      if (c1 < c) {
        String str = arrayOfString[c1];
        if (str != null) {
          if (j < b)
            append(paramString, j, b - j); 
          j = b + 1;
          append(str);
        } 
      } 
    } 
    if (j < b)
      append(paramString, j, b - j); 
  }
  
  private void escapeAndAppendString(char[] paramArrayOfchar, int paramInt1, int paramInt2) throws IOException {
    char c = (char)ESCAPE_TABLE.length;
    String[] arrayOfString = ESCAPE_TABLE;
    int i = paramInt1;
    int j;
    for (j = paramInt1; j < paramInt1 + paramInt2; j++) {
      char c1 = paramArrayOfchar[j];
      if (c1 < c) {
        String str = arrayOfString[c1];
        if (str != null) {
          if (i < j)
            append(paramArrayOfchar, i, j - i); 
          i = j + 1;
          append(str);
        } 
      } 
    } 
    if (i < j)
      append(paramArrayOfchar, i, j - i); 
  }
  
  public XmlSerializer attribute(String paramString1, String paramString2, String paramString3) throws IOException, IllegalArgumentException, IllegalStateException {
    append(' ');
    if (paramString1 != null) {
      append(paramString1);
      append(':');
    } 
    append(paramString2);
    append("=\"");
    escapeAndAppendString(paramString3);
    append('"');
    this.mLineStart = false;
    return this;
  }
  
  public void cdsect(String paramString) throws IOException, IllegalArgumentException, IllegalStateException {
    throw new UnsupportedOperationException();
  }
  
  public void comment(String paramString) throws IOException, IllegalArgumentException, IllegalStateException {
    throw new UnsupportedOperationException();
  }
  
  public void docdecl(String paramString) throws IOException, IllegalArgumentException, IllegalStateException {
    throw new UnsupportedOperationException();
  }
  
  public void endDocument() throws IOException, IllegalArgumentException, IllegalStateException {
    flush();
  }
  
  public XmlSerializer endTag(String paramString1, String paramString2) throws IOException, IllegalArgumentException, IllegalStateException {
    int i = this.mNesting - 1;
    if (this.mInTag) {
      append(" />\n");
    } else {
      if (this.mIndent && this.mLineStart)
        appendIndent(i); 
      append("</");
      if (paramString1 != null) {
        append(paramString1);
        append(':');
      } 
      append(paramString2);
      append(">\n");
    } 
    this.mLineStart = true;
    this.mInTag = false;
    return this;
  }
  
  public void entityRef(String paramString) throws IOException, IllegalArgumentException, IllegalStateException {
    throw new UnsupportedOperationException();
  }
  
  private void flushBytes() throws IOException {
    int i = this.mBytes.position();
    if (i > 0) {
      this.mBytes.flip();
      this.mOutputStream.write(this.mBytes.array(), 0, i);
      this.mBytes.clear();
    } 
  }
  
  public void flush() throws IOException {
    int i = this.mPos;
    if (i > 0)
      if (this.mOutputStream != null) {
        CharBuffer charBuffer = CharBuffer.wrap(this.mText, 0, i);
        CoderResult coderResult = this.mCharset.encode(charBuffer, this.mBytes, true);
        while (true) {
          if (!coderResult.isError()) {
            if (coderResult.isOverflow()) {
              flushBytes();
              coderResult = this.mCharset.encode(charBuffer, this.mBytes, true);
              continue;
            } 
            flushBytes();
            this.mOutputStream.flush();
          } else {
            throw new IOException(coderResult.toString());
          } 
          this.mPos = 0;
        } 
      } else {
        this.mWriter.write(this.mText, 0, i);
        this.mWriter.flush();
        this.mPos = 0;
      }  
  }
  
  public int getDepth() {
    throw new UnsupportedOperationException();
  }
  
  public boolean getFeature(String paramString) {
    throw new UnsupportedOperationException();
  }
  
  public String getName() {
    throw new UnsupportedOperationException();
  }
  
  public String getNamespace() {
    throw new UnsupportedOperationException();
  }
  
  public String getPrefix(String paramString, boolean paramBoolean) throws IllegalArgumentException {
    throw new UnsupportedOperationException();
  }
  
  public Object getProperty(String paramString) {
    throw new UnsupportedOperationException();
  }
  
  public void ignorableWhitespace(String paramString) throws IOException, IllegalArgumentException, IllegalStateException {
    throw new UnsupportedOperationException();
  }
  
  public void processingInstruction(String paramString) throws IOException, IllegalArgumentException, IllegalStateException {
    throw new UnsupportedOperationException();
  }
  
  public void setFeature(String paramString, boolean paramBoolean) throws IllegalArgumentException, IllegalStateException {
    if (paramString.equals("http://xmlpull.org/v1/doc/features.html#indent-output")) {
      this.mIndent = true;
      return;
    } 
    throw new UnsupportedOperationException();
  }
  
  public void setOutput(OutputStream paramOutputStream, String paramString) throws IOException, IllegalArgumentException, IllegalStateException {
    if (paramOutputStream != null) {
      UnsupportedEncodingException unsupportedEncodingException;
      try {
        CharsetEncoder charsetEncoder1 = Charset.forName(paramString).newEncoder();
        CodingErrorAction codingErrorAction2 = CodingErrorAction.REPLACE;
        CharsetEncoder charsetEncoder2 = charsetEncoder1.onMalformedInput(codingErrorAction2);
        CodingErrorAction codingErrorAction1 = CodingErrorAction.REPLACE;
        this.mCharset = charsetEncoder2.onUnmappableCharacter(codingErrorAction1);
        this.mOutputStream = paramOutputStream;
        return;
      } catch (IllegalCharsetNameException illegalCharsetNameException) {
        unsupportedEncodingException = new UnsupportedEncodingException(paramString);
        throw (UnsupportedEncodingException)unsupportedEncodingException.initCause(illegalCharsetNameException);
      } catch (UnsupportedCharsetException unsupportedCharsetException) {
        unsupportedEncodingException = new UnsupportedEncodingException((String)unsupportedEncodingException);
        throw (UnsupportedEncodingException)unsupportedEncodingException.initCause(unsupportedCharsetException);
      } 
    } 
    throw new IllegalArgumentException();
  }
  
  public void setOutput(Writer paramWriter) throws IOException, IllegalArgumentException, IllegalStateException {
    this.mWriter = paramWriter;
  }
  
  public void setPrefix(String paramString1, String paramString2) throws IOException, IllegalArgumentException, IllegalStateException {
    throw new UnsupportedOperationException();
  }
  
  public void setProperty(String paramString, Object paramObject) throws IllegalArgumentException, IllegalStateException {
    throw new UnsupportedOperationException();
  }
  
  public void startDocument(String paramString, Boolean paramBoolean) throws IOException, IllegalArgumentException, IllegalStateException {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("<?xml version='1.0' encoding='utf-8' standalone='");
    if (paramBoolean.booleanValue()) {
      paramString = "yes";
    } else {
      paramString = "no";
    } 
    stringBuilder.append(paramString);
    stringBuilder.append("' ?>\n");
    paramString = stringBuilder.toString();
    append(paramString);
    this.mLineStart = true;
  }
  
  public XmlSerializer startTag(String paramString1, String paramString2) throws IOException, IllegalArgumentException, IllegalStateException {
    if (this.mInTag)
      append(">\n"); 
    if (this.mIndent)
      appendIndent(this.mNesting); 
    this.mNesting++;
    append('<');
    if (paramString1 != null) {
      append(paramString1);
      append(':');
    } 
    append(paramString2);
    this.mInTag = true;
    this.mLineStart = false;
    return this;
  }
  
  public XmlSerializer text(char[] paramArrayOfchar, int paramInt1, int paramInt2) throws IOException, IllegalArgumentException, IllegalStateException {
    boolean bool = this.mInTag;
    boolean bool1 = false;
    if (bool) {
      append(">");
      this.mInTag = false;
    } 
    escapeAndAppendString(paramArrayOfchar, paramInt1, paramInt2);
    if (this.mIndent) {
      if (paramArrayOfchar[paramInt1 + paramInt2 - 1] == '\n')
        bool1 = true; 
      this.mLineStart = bool1;
    } 
    return this;
  }
  
  public XmlSerializer text(String paramString) throws IOException, IllegalArgumentException, IllegalStateException {
    boolean bool = this.mInTag;
    boolean bool1 = false;
    if (bool) {
      append(">");
      this.mInTag = false;
    } 
    escapeAndAppendString(paramString);
    if (this.mIndent) {
      bool = bool1;
      if (paramString.length() > 0) {
        bool = bool1;
        if (paramString.charAt(paramString.length() - 1) == '\n')
          bool = true; 
      } 
      this.mLineStart = bool;
    } 
    return this;
  }
}
