package com.android.internal.util;

import java.util.Objects;

public class ObjectUtils {
  public static <T> T firstNotNull(T paramT1, T paramT2) {
    if (paramT1 == null) {
      Objects.requireNonNull(paramT2);
      paramT1 = paramT2;
    } 
    return paramT1;
  }
  
  public static <T extends Comparable> int compare(T paramT1, T paramT2) {
    boolean bool;
    if (paramT1 != null) {
      if (paramT2 != null) {
        bool = paramT1.compareTo(paramT2);
      } else {
        bool = true;
      } 
      return bool;
    } 
    if (paramT2 != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static <T> T getOrElse(T paramT1, T paramT2) {
    if (paramT1 == null)
      paramT1 = paramT2; 
    return paramT1;
  }
}
