package com.android.internal.util;

public class HexDump {
  private static final char[] HEX_DIGITS = new char[] { 
      '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
      'A', 'B', 'C', 'D', 'E', 'F' };
  
  private static final char[] HEX_LOWER_CASE_DIGITS = new char[] { 
      '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
      'a', 'b', 'c', 'd', 'e', 'f' };
  
  public static String dumpHexString(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte == null)
      return "(null)"; 
    return dumpHexString(paramArrayOfbyte, 0, paramArrayOfbyte.length);
  }
  
  public static String dumpHexString(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    if (paramArrayOfbyte == null)
      return "(null)"; 
    StringBuilder stringBuilder = new StringBuilder();
    byte[] arrayOfByte = new byte[16];
    int i = 0;
    stringBuilder.append("\n0x");
    stringBuilder.append(toHexString(paramInt1));
    for (int j = paramInt1; j < paramInt1 + paramInt2; j++, i = k + 1) {
      int k = i;
      if (i == 16) {
        stringBuilder.append(" ");
        for (i = 0; i < 16; i++) {
          if (arrayOfByte[i] > 32 && arrayOfByte[i] < 126) {
            stringBuilder.append(new String(arrayOfByte, i, 1));
          } else {
            stringBuilder.append(".");
          } 
        } 
        stringBuilder.append("\n0x");
        stringBuilder.append(toHexString(j));
        k = 0;
      } 
      byte b = paramArrayOfbyte[j];
      stringBuilder.append(" ");
      stringBuilder.append(HEX_DIGITS[b >>> 4 & 0xF]);
      stringBuilder.append(HEX_DIGITS[b & 0xF]);
      arrayOfByte[k] = b;
    } 
    if (i != 16) {
      for (paramInt1 = 0; paramInt1 < (16 - i) * 3 + 1; paramInt1++)
        stringBuilder.append(" "); 
      for (paramInt1 = 0; paramInt1 < i; paramInt1++) {
        if (arrayOfByte[paramInt1] > 32 && arrayOfByte[paramInt1] < 126) {
          stringBuilder.append(new String(arrayOfByte, paramInt1, 1));
        } else {
          stringBuilder.append(".");
        } 
      } 
    } 
    return stringBuilder.toString();
  }
  
  public static String toHexString(byte paramByte) {
    return toHexString(toByteArray(paramByte));
  }
  
  public static String toHexString(byte[] paramArrayOfbyte) {
    return toHexString(paramArrayOfbyte, 0, paramArrayOfbyte.length, true);
  }
  
  public static String toHexString(byte[] paramArrayOfbyte, boolean paramBoolean) {
    return toHexString(paramArrayOfbyte, 0, paramArrayOfbyte.length, paramBoolean);
  }
  
  public static String toHexString(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    return toHexString(paramArrayOfbyte, paramInt1, paramInt2, true);
  }
  
  public static String toHexString(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, boolean paramBoolean) {
    char[] arrayOfChar1;
    if (paramBoolean) {
      arrayOfChar1 = HEX_DIGITS;
    } else {
      arrayOfChar1 = HEX_LOWER_CASE_DIGITS;
    } 
    char[] arrayOfChar2 = new char[paramInt2 * 2];
    int i = 0;
    for (int j = paramInt1; j < paramInt1 + paramInt2; j++) {
      byte b = paramArrayOfbyte[j];
      int k = i + 1;
      arrayOfChar2[i] = arrayOfChar1[b >>> 4 & 0xF];
      i = k + 1;
      arrayOfChar2[k] = arrayOfChar1[b & 0xF];
    } 
    return new String(arrayOfChar2);
  }
  
  public static String toHexString(int paramInt) {
    return toHexString(toByteArray(paramInt));
  }
  
  public static byte[] toByteArray(byte paramByte) {
    return new byte[] { paramByte };
  }
  
  public static byte[] toByteArray(int paramInt) {
    byte b1 = (byte)(paramInt & 0xFF);
    byte b2 = (byte)(paramInt >> 8 & 0xFF);
    byte b3 = (byte)(paramInt >> 16 & 0xFF);
    byte b4 = (byte)(paramInt >> 24 & 0xFF);
    return new byte[] { b4, b3, b2, b1 };
  }
  
  private static int toByte(char paramChar) {
    if (paramChar >= '0' && paramChar <= '9')
      return paramChar - 48; 
    if (paramChar >= 'A' && paramChar <= 'F')
      return paramChar - 65 + 10; 
    if (paramChar >= 'a' && paramChar <= 'f')
      return paramChar - 97 + 10; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid hex char '");
    stringBuilder.append(paramChar);
    stringBuilder.append("'");
    throw new RuntimeException(stringBuilder.toString());
  }
  
  public static byte[] hexStringToByteArray(String paramString) {
    int i = paramString.length();
    byte[] arrayOfByte = new byte[i / 2];
    for (byte b = 0; b < i; b += 2)
      arrayOfByte[b / 2] = (byte)(toByte(paramString.charAt(b)) << 4 | toByte(paramString.charAt(b + 1))); 
    return arrayOfByte;
  }
  
  public static StringBuilder appendByteAsHex(StringBuilder paramStringBuilder, byte paramByte, boolean paramBoolean) {
    char[] arrayOfChar;
    if (paramBoolean) {
      arrayOfChar = HEX_DIGITS;
    } else {
      arrayOfChar = HEX_LOWER_CASE_DIGITS;
    } 
    paramStringBuilder.append(arrayOfChar[paramByte >> 4 & 0xF]);
    paramStringBuilder.append(arrayOfChar[paramByte & 0xF]);
    return paramStringBuilder;
  }
}
