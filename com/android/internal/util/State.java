package com.android.internal.util;

import android.os.Message;

public class State implements IState {
  public void enter() {}
  
  public void exit() {}
  
  public boolean processMessage(Message paramMessage) {
    return false;
  }
  
  public String getName() {
    String str = getClass().getName();
    int i = str.lastIndexOf('$');
    return str.substring(i + 1);
  }
}
