package com.android.internal.util;

import android.os.FileUtils;
import android.util.Slog;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import libcore.io.IoUtils;

public class FileRotator {
  private static final boolean LOGD = false;
  
  private static final String SUFFIX_BACKUP = ".backup";
  
  private static final String SUFFIX_NO_BACKUP = ".no_backup";
  
  private static final String TAG = "FileRotator";
  
  private final File mBasePath;
  
  private final long mDeleteAgeMillis;
  
  private final String mPrefix;
  
  private final long mRotateAgeMillis;
  
  public FileRotator(File paramFile, String paramString, long paramLong1, long paramLong2) {
    Objects.requireNonNull(paramFile);
    this.mBasePath = paramFile;
    Objects.requireNonNull(paramString);
    this.mPrefix = paramString;
    this.mRotateAgeMillis = paramLong1;
    this.mDeleteAgeMillis = paramLong2;
    this.mBasePath.mkdirs();
    for (String paramString : this.mBasePath.list()) {
      if (paramString.startsWith(this.mPrefix)) {
        File file;
        if (paramString.endsWith(".backup")) {
          File file1 = new File(this.mBasePath, paramString);
          File file2 = this.mBasePath;
          file = new File(file2, paramString.substring(0, paramString.length() - ".backup".length()));
          file1.renameTo(file);
        } else if (file.endsWith(".no_backup")) {
          File file1 = new File(this.mBasePath, (String)file);
          File file2 = this.mBasePath;
          file = new File(file2, file.substring(0, file.length() - ".no_backup".length()));
          file1.delete();
          file.delete();
        } 
      } 
    } 
  }
  
  public void deleteAll() {
    FileInfo fileInfo = new FileInfo(this.mPrefix);
    for (String str : this.mBasePath.list()) {
      if (fileInfo.parse(str))
        (new File(this.mBasePath, str)).delete(); 
    } 
  }
  
  public void dumpAll(OutputStream paramOutputStream) throws IOException {
    paramOutputStream = new ZipOutputStream(paramOutputStream);
    try {
      FileInfo fileInfo = new FileInfo();
      this(this.mPrefix);
      for (String str : this.mBasePath.list()) {
        if (fileInfo.parse(str)) {
          ZipEntry zipEntry = new ZipEntry();
          this(str);
          paramOutputStream.putNextEntry(zipEntry);
          File file = new File();
          this(this.mBasePath, str);
          FileInputStream fileInputStream = new FileInputStream();
          this(file);
        } 
      } 
      return;
    } finally {
      IoUtils.closeQuietly(paramOutputStream);
    } 
  }
  
  public void rewriteActive(Rewriter paramRewriter, long paramLong) throws IOException {
    try {
      String str = getActiveName(paramLong);
      rewriteSingle(paramRewriter, str);
    } catch (NullPointerException nullPointerException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("problem rewriteActive:");
      stringBuilder.append(nullPointerException);
      Slog.d("FileRotator", stringBuilder.toString());
    } 
  }
  
  @Deprecated
  public void combineActive(Reader paramReader, Writer paramWriter, long paramLong) throws IOException {
    rewriteActive((Rewriter)new Object(this, paramReader, paramWriter), paramLong);
  }
  
  public void rewriteAll(Rewriter paramRewriter) throws IOException {
    FileInfo fileInfo = new FileInfo(this.mPrefix);
    for (String str : this.mBasePath.list()) {
      if (fileInfo.parse(str))
        rewriteSingle(paramRewriter, str); 
    } 
  }
  
  private void rewriteSingle(Rewriter paramRewriter, String paramString) throws IOException {
    File file1, file2 = new File(this.mBasePath, paramString);
    paramRewriter.reset();
    if (file2.exists()) {
      readFile(file2, paramRewriter);
      if (!paramRewriter.shouldWrite())
        return; 
      File file = this.mBasePath;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append(".backup");
      file1 = new File(file, stringBuilder.toString());
      file2.renameTo(file1);
      try {
      
      } finally {
        paramRewriter = null;
        file2.delete();
        file1.renameTo(file2);
      } 
    } else {
      File file = this.mBasePath;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append((String)file1);
      stringBuilder.append(".no_backup");
      file1 = new File(file, stringBuilder.toString());
      file1.createNewFile();
      try {
        return;
      } finally {
        paramRewriter = null;
        file2.delete();
        file1.delete();
      } 
    } 
  }
  
  public void readMatching(Reader paramReader, long paramLong1, long paramLong2) throws IOException {
    FileInfo fileInfo = new FileInfo(this.mPrefix);
    for (String str : this.mBasePath.list()) {
      if (fileInfo.parse(str))
        if (fileInfo.startMillis <= paramLong2 && paramLong1 <= fileInfo.endMillis) {
          File file = new File(this.mBasePath, str);
          readFile(file, paramReader);
        }  
    } 
  }
  
  private String getActiveName(long paramLong) {
    String str = null;
    long l = Long.MAX_VALUE;
    FileInfo fileInfo = new FileInfo(this.mPrefix);
    String[] arrayOfString;
    int i;
    byte b;
    for (arrayOfString = this.mBasePath.list(), i = arrayOfString.length, b = 0; b < i; ) {
      String str2;
      long l1;
      String str1 = arrayOfString[b];
      if (!fileInfo.parse(str1)) {
        str2 = str;
        l1 = l;
      } else {
        str2 = str;
        l1 = l;
        if (fileInfo.isActive()) {
          str2 = str;
          l1 = l;
          if (fileInfo.startMillis < paramLong) {
            str2 = str;
            l1 = l;
            if (fileInfo.startMillis < l) {
              str2 = str1;
              l1 = fileInfo.startMillis;
            } 
          } 
        } 
      } 
      b++;
      str = str2;
      l = l1;
    } 
    if (str != null)
      return str; 
    fileInfo.startMillis = paramLong;
    fileInfo.endMillis = Long.MAX_VALUE;
    return fileInfo.build();
  }
  
  public void maybeRotate(long paramLong) {
    long l1 = this.mRotateAgeMillis;
    long l2 = this.mDeleteAgeMillis;
    FileInfo fileInfo = new FileInfo(this.mPrefix);
    String[] arrayOfString = this.mBasePath.list();
    if (arrayOfString == null)
      return; 
    int i;
    byte b;
    for (i = arrayOfString.length, b = 0; b < i; ) {
      String str = arrayOfString[b];
      if (fileInfo.parse(str)) {
        File file;
        if (fileInfo.isActive()) {
          if (fileInfo.startMillis <= paramLong - l1) {
            fileInfo.endMillis = paramLong;
            file = new File(this.mBasePath, str);
            File file1 = new File(this.mBasePath, fileInfo.build());
            file.renameTo(file1);
          } 
        } else if (fileInfo.endMillis <= paramLong - l2) {
          file = new File(this.mBasePath, (String)file);
          file.delete();
        } 
      } 
      b++;
    } 
  }
  
  private static void readFile(File paramFile, Reader paramReader) throws IOException {
    FileInputStream fileInputStream = new FileInputStream(paramFile);
    BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
    try {
      paramReader.read(bufferedInputStream);
      return;
    } finally {
      IoUtils.closeQuietly(bufferedInputStream);
    } 
  }
  
  private static void writeFile(File paramFile, Writer paramWriter) throws IOException {
    FileOutputStream fileOutputStream = new FileOutputStream(paramFile);
    BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
    try {
      paramWriter.write(bufferedOutputStream);
      bufferedOutputStream.flush();
      return;
    } finally {
      FileUtils.sync(fileOutputStream);
      IoUtils.closeQuietly(bufferedOutputStream);
    } 
  }
  
  private static IOException rethrowAsIoException(Throwable paramThrowable) throws IOException {
    if (paramThrowable instanceof IOException)
      throw (IOException)paramThrowable; 
    throw new IOException(paramThrowable.getMessage(), paramThrowable);
  }
  
  private static class FileInfo {
    public long endMillis;
    
    public final String prefix;
    
    public long startMillis;
    
    public FileInfo(String param1String) {
      Objects.requireNonNull(param1String);
      this.prefix = param1String;
    }
    
    public boolean parse(String param1String) {
      this.endMillis = -1L;
      this.startMillis = -1L;
      int i = param1String.lastIndexOf('.');
      int j = param1String.lastIndexOf('-');
      if (i == -1 || j == -1)
        return false; 
      if (!this.prefix.equals(param1String.substring(0, i)))
        return false; 
      try {
        this.startMillis = Long.parseLong(param1String.substring(i + 1, j));
        if (param1String.length() - j == 1) {
          this.endMillis = Long.MAX_VALUE;
        } else {
          this.endMillis = Long.parseLong(param1String.substring(j + 1));
        } 
        return true;
      } catch (NumberFormatException numberFormatException) {
        return false;
      } 
    }
    
    public String build() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(this.prefix);
      stringBuilder.append('.');
      stringBuilder.append(this.startMillis);
      stringBuilder.append('-');
      long l = this.endMillis;
      if (l != Long.MAX_VALUE)
        stringBuilder.append(l); 
      return stringBuilder.toString();
    }
    
    public boolean isActive() {
      boolean bool;
      if (this.endMillis == Long.MAX_VALUE) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
  }
  
  public static interface Reader {
    void read(InputStream param1InputStream) throws IOException;
  }
  
  class Rewriter implements Reader, Writer {
    public abstract void reset();
    
    public abstract boolean shouldWrite();
  }
  
  public static interface Writer {
    void write(OutputStream param1OutputStream) throws IOException;
  }
}
