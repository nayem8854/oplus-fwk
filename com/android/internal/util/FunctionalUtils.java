package com.android.internal.util;

import android.os.RemoteException;
import android.util.ExceptionUtils;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class FunctionalUtils {
  public static <T> Consumer<T> uncheckExceptions(ThrowingConsumer<T> paramThrowingConsumer) {
    return paramThrowingConsumer;
  }
  
  public static <I, O> Function<I, O> uncheckExceptions(ThrowingFunction<I, O> paramThrowingFunction) {
    return paramThrowingFunction;
  }
  
  public static Runnable uncheckExceptions(ThrowingRunnable paramThrowingRunnable) {
    return paramThrowingRunnable;
  }
  
  public static <A, B> BiConsumer<A, B> uncheckExceptions(ThrowingBiConsumer<A, B> paramThrowingBiConsumer) {
    return paramThrowingBiConsumer;
  }
  
  public static <T> Supplier<T> uncheckExceptions(ThrowingSupplier<T> paramThrowingSupplier) {
    return paramThrowingSupplier;
  }
  
  public static <T> Consumer<T> ignoreRemoteException(RemoteExceptionIgnoringConsumer<T> paramRemoteExceptionIgnoringConsumer) {
    return paramRemoteExceptionIgnoringConsumer;
  }
  
  public static Runnable handleExceptions(ThrowingRunnable paramThrowingRunnable, Consumer<Throwable> paramConsumer) {
    return new _$$Lambda$FunctionalUtils$koCSI8D7Nu5vOJTVTEj0m3leo_U(paramThrowingRunnable, paramConsumer);
  }
  
  @FunctionalInterface
  public static interface ThrowingRunnable extends Runnable {
    default void run() {
      try {
        runOrThrow();
        return;
      } catch (Exception exception) {
        throw ExceptionUtils.propagate(exception);
      } 
    }
    
    void runOrThrow() throws Exception;
  }
  
  @FunctionalInterface
  public static interface ThrowingSupplier<T> extends Supplier<T> {
    default T get() {
      try {
        return getOrThrow();
      } catch (Exception exception) {
        throw ExceptionUtils.propagate(exception);
      } 
    }
    
    T getOrThrow() throws Exception;
  }
  
  @FunctionalInterface
  public static interface ThrowingConsumer<T> extends Consumer<T> {
    default void accept(T param1T) {
      try {
        acceptOrThrow(param1T);
        return;
      } catch (Exception exception) {
        throw ExceptionUtils.propagate(exception);
      } 
    }
    
    void acceptOrThrow(T param1T) throws Exception;
  }
  
  @FunctionalInterface
  public static interface RemoteExceptionIgnoringConsumer<T> extends Consumer<T> {
    default void accept(T param1T) {
      try {
        acceptOrThrow(param1T);
      } catch (RemoteException remoteException) {}
    }
    
    void acceptOrThrow(T param1T) throws RemoteException;
  }
  
  @FunctionalInterface
  public static interface ThrowingFunction<T, R> extends Function<T, R> {
    default R apply(T param1T) {
      try {
        return applyOrThrow(param1T);
      } catch (Exception exception) {
        throw ExceptionUtils.propagate(exception);
      } 
    }
    
    R applyOrThrow(T param1T) throws Exception;
  }
  
  @FunctionalInterface
  public static interface ThrowingBiFunction<T, U, R> extends BiFunction<T, U, R> {
    default R apply(T param1T, U param1U) {
      try {
        return applyOrThrow(param1T, param1U);
      } catch (Exception exception) {
        throw ExceptionUtils.propagate(exception);
      } 
    }
    
    R applyOrThrow(T param1T, U param1U) throws Exception;
  }
  
  @FunctionalInterface
  public static interface ThrowingBiConsumer<A, B> extends BiConsumer<A, B> {
    default void accept(A param1A, B param1B) {
      try {
        acceptOrThrow(param1A, param1B);
        return;
      } catch (Exception exception) {
        throw ExceptionUtils.propagate(exception);
      } 
    }
    
    void acceptOrThrow(A param1A, B param1B) throws Exception;
  }
  
  public static String getLambdaName(Object paramObject) {
    paramObject = paramObject.toString();
    int i = paramObject.indexOf("-$$");
    if (i == -1)
      return (String)paramObject; 
    int j = paramObject.indexOf('$', i + 3);
    if (j == -1)
      return (String)paramObject; 
    int k = paramObject.indexOf('$', j + 1);
    if (k == -1) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramObject.substring(0, i - 1));
      stringBuilder1.append("$Lambda");
      return stringBuilder1.toString();
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramObject.substring(0, i));
    stringBuilder.append(paramObject.substring(j + 1, k));
    stringBuilder.append("$Lambda");
    return stringBuilder.toString();
  }
}
