package com.android.internal.util;

import android.os.Bundle;
import android.os.Parcelable;
import com.android.internal.os.IResultReceiver;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public final class SyncResultReceiver extends IResultReceiver.Stub {
  private static final String EXTRA = "EXTRA";
  
  private Bundle mBundle;
  
  private final CountDownLatch mLatch = new CountDownLatch(1);
  
  private int mResult;
  
  private final int mTimeoutMs;
  
  public SyncResultReceiver(int paramInt) {
    this.mTimeoutMs = paramInt;
  }
  
  private void waitResult() throws TimeoutException {
    try {
      if (this.mLatch.await(this.mTimeoutMs, TimeUnit.MILLISECONDS))
        return; 
      TimeoutException timeoutException = new TimeoutException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Not called in ");
      stringBuilder.append(this.mTimeoutMs);
      stringBuilder.append("ms");
      this(stringBuilder.toString());
      throw timeoutException;
    } catch (InterruptedException interruptedException) {
      Thread.currentThread().interrupt();
      throw new TimeoutException();
    } 
  }
  
  public int getIntResult() throws TimeoutException {
    waitResult();
    return this.mResult;
  }
  
  public String getStringResult() throws TimeoutException {
    String str;
    waitResult();
    Bundle bundle = this.mBundle;
    if (bundle == null) {
      bundle = null;
    } else {
      str = bundle.getString("EXTRA");
    } 
    return str;
  }
  
  public String[] getStringArrayResult() throws TimeoutException {
    String[] arrayOfString;
    waitResult();
    Bundle bundle = this.mBundle;
    if (bundle == null) {
      bundle = null;
    } else {
      arrayOfString = bundle.getStringArray("EXTRA");
    } 
    return arrayOfString;
  }
  
  public <P extends Parcelable> P getParcelableResult() throws TimeoutException {
    Parcelable parcelable;
    waitResult();
    Bundle bundle = this.mBundle;
    if (bundle == null) {
      bundle = null;
    } else {
      parcelable = bundle.getParcelable("EXTRA");
    } 
    return (P)parcelable;
  }
  
  public <P extends Parcelable> ArrayList<P> getParcelableListResult() throws TimeoutException {
    ArrayList<P> arrayList;
    waitResult();
    Bundle bundle = this.mBundle;
    if (bundle == null) {
      bundle = null;
    } else {
      arrayList = bundle.getParcelableArrayList("EXTRA");
    } 
    return arrayList;
  }
  
  public int getOptionalExtraIntResult(int paramInt) throws TimeoutException {
    waitResult();
    Bundle bundle = this.mBundle;
    if (bundle == null || !bundle.containsKey("EXTRA"))
      return paramInt; 
    return this.mBundle.getInt("EXTRA");
  }
  
  public void send(int paramInt, Bundle paramBundle) {
    this.mResult = paramInt;
    this.mBundle = paramBundle;
    this.mLatch.countDown();
  }
  
  public static Bundle bundleFor(String paramString) {
    Bundle bundle = new Bundle();
    bundle.putString("EXTRA", paramString);
    return bundle;
  }
  
  public static Bundle bundleFor(String[] paramArrayOfString) {
    Bundle bundle = new Bundle();
    bundle.putStringArray("EXTRA", paramArrayOfString);
    return bundle;
  }
  
  public static Bundle bundleFor(Parcelable paramParcelable) {
    Bundle bundle = new Bundle();
    bundle.putParcelable("EXTRA", paramParcelable);
    return bundle;
  }
  
  public static Bundle bundleFor(ArrayList<? extends Parcelable> paramArrayList) {
    Bundle bundle = new Bundle();
    bundle.putParcelableArrayList("EXTRA", paramArrayList);
    return bundle;
  }
  
  public static Bundle bundleFor(int paramInt) {
    Bundle bundle = new Bundle();
    bundle.putInt("EXTRA", paramInt);
    return bundle;
  }
  
  class TimeoutException extends Exception {
    private TimeoutException(SyncResultReceiver this$0) {
      super((String)this$0);
    }
  }
}
