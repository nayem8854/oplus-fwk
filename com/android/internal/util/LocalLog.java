package com.android.internal.util;

import android.util.Slog;
import android.util.proto.ProtoOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

public class LocalLog {
  private final String mTag;
  
  private final int mMaxLines = 20;
  
  private final ArrayList<String> mLines = new ArrayList<>(20);
  
  public LocalLog(String paramString) {
    this.mTag = paramString;
  }
  
  public void w(String paramString) {
    synchronized (this.mLines) {
      Slog.w(this.mTag, paramString);
      if (this.mLines.size() >= 20)
        this.mLines.remove(0); 
      this.mLines.add(paramString);
      return;
    } 
  }
  
  public boolean dump(PrintWriter paramPrintWriter, String paramString1, String paramString2) {
    synchronized (this.mLines) {
      if (this.mLines.size() <= 0)
        return false; 
      if (paramString1 != null)
        paramPrintWriter.println(paramString1); 
      for (byte b = 0; b < this.mLines.size(); b++) {
        if (paramString2 != null)
          paramPrintWriter.print(paramString2); 
        paramPrintWriter.println(this.mLines.get(b));
      } 
      return true;
    } 
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong) {
    // Byte code:
    //   0: aload_1
    //   1: lload_2
    //   2: invokevirtual start : (J)J
    //   5: lstore_2
    //   6: aload_0
    //   7: getfield mLines : Ljava/util/ArrayList;
    //   10: astore #4
    //   12: aload #4
    //   14: monitorenter
    //   15: iconst_0
    //   16: istore #5
    //   18: iload #5
    //   20: aload_0
    //   21: getfield mLines : Ljava/util/ArrayList;
    //   24: invokevirtual size : ()I
    //   27: if_icmpge -> 55
    //   30: aload_1
    //   31: ldc2_w 2237677961217
    //   34: aload_0
    //   35: getfield mLines : Ljava/util/ArrayList;
    //   38: iload #5
    //   40: invokevirtual get : (I)Ljava/lang/Object;
    //   43: checkcast java/lang/String
    //   46: invokevirtual write : (JLjava/lang/String;)V
    //   49: iinc #5, 1
    //   52: goto -> 18
    //   55: aload #4
    //   57: monitorexit
    //   58: aload_1
    //   59: lload_2
    //   60: invokevirtual end : (J)V
    //   63: return
    //   64: astore_1
    //   65: aload #4
    //   67: monitorexit
    //   68: aload_1
    //   69: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #69	-> 0
    //   #71	-> 6
    //   #72	-> 15
    //   #73	-> 30
    //   #72	-> 49
    //   #75	-> 55
    //   #77	-> 58
    //   #78	-> 63
    //   #75	-> 64
    // Exception table:
    //   from	to	target	type
    //   18	30	64	finally
    //   30	49	64	finally
    //   55	58	64	finally
    //   65	68	64	finally
  }
}
