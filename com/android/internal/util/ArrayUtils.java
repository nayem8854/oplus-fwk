package com.android.internal.util;

import android.util.ArraySet;
import dalvik.system.VMRuntime;
import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import libcore.util.EmptyArray;

public class ArrayUtils {
  private static final int CACHE_SIZE = 73;
  
  public static final File[] EMPTY_FILE;
  
  private static Object[] sCache = new Object[73];
  
  static {
    EMPTY_FILE = new File[0];
  }
  
  public static byte[] newUnpaddedByteArray(int paramInt) {
    return (byte[])VMRuntime.getRuntime().newUnpaddedArray(byte.class, paramInt);
  }
  
  public static char[] newUnpaddedCharArray(int paramInt) {
    return (char[])VMRuntime.getRuntime().newUnpaddedArray(char.class, paramInt);
  }
  
  public static int[] newUnpaddedIntArray(int paramInt) {
    return (int[])VMRuntime.getRuntime().newUnpaddedArray(int.class, paramInt);
  }
  
  public static boolean[] newUnpaddedBooleanArray(int paramInt) {
    return (boolean[])VMRuntime.getRuntime().newUnpaddedArray(boolean.class, paramInt);
  }
  
  public static long[] newUnpaddedLongArray(int paramInt) {
    return (long[])VMRuntime.getRuntime().newUnpaddedArray(long.class, paramInt);
  }
  
  public static float[] newUnpaddedFloatArray(int paramInt) {
    return (float[])VMRuntime.getRuntime().newUnpaddedArray(float.class, paramInt);
  }
  
  public static Object[] newUnpaddedObjectArray(int paramInt) {
    return (Object[])VMRuntime.getRuntime().newUnpaddedArray(Object.class, paramInt);
  }
  
  public static <T> T[] newUnpaddedArray(Class<T> paramClass, int paramInt) {
    return (T[])VMRuntime.getRuntime().newUnpaddedArray(paramClass, paramInt);
  }
  
  public static boolean equals(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, int paramInt) {
    if (paramInt >= 0) {
      if (paramArrayOfbyte1 == paramArrayOfbyte2)
        return true; 
      if (paramArrayOfbyte1 == null || paramArrayOfbyte2 == null || paramArrayOfbyte1.length < paramInt || paramArrayOfbyte2.length < paramInt)
        return false; 
      for (byte b = 0; b < paramInt; b++) {
        if (paramArrayOfbyte1[b] != paramArrayOfbyte2[b])
          return false; 
      } 
      return true;
    } 
    throw new IllegalArgumentException();
  }
  
  public static <T> T[] emptyArray(Class<T> paramClass) {
    if (paramClass == Object.class)
      return (T[])EmptyArray.OBJECT; 
    int i = (paramClass.hashCode() & Integer.MAX_VALUE) % 73;
    Object object1 = sCache[i];
    if (object1 != null) {
      Object object = object1;
      if (object1.getClass().getComponentType() != paramClass) {
        object = Array.newInstance(paramClass, 0);
        sCache[i] = object;
        return (T[])object;
      } 
      return (T[])object;
    } 
    Object object2 = Array.newInstance(paramClass, 0);
    sCache[i] = object2;
    return (T[])object2;
  }
  
  public static <T> T[] emptyIfNull(T[] paramArrayOfT, Class<T> paramClass) {
    if (paramArrayOfT == null)
      paramArrayOfT = emptyArray(paramClass); 
    return paramArrayOfT;
  }
  
  public static boolean isEmpty(Collection<?> paramCollection) {
    return (paramCollection == null || paramCollection.isEmpty());
  }
  
  public static boolean isEmpty(Map<?, ?> paramMap) {
    return (paramMap == null || paramMap.isEmpty());
  }
  
  public static <T> boolean isEmpty(T[] paramArrayOfT) {
    return (paramArrayOfT == null || paramArrayOfT.length == 0);
  }
  
  public static boolean isEmpty(int[] paramArrayOfint) {
    return (paramArrayOfint == null || paramArrayOfint.length == 0);
  }
  
  public static boolean isEmpty(long[] paramArrayOflong) {
    return (paramArrayOflong == null || paramArrayOflong.length == 0);
  }
  
  public static boolean isEmpty(byte[] paramArrayOfbyte) {
    return (paramArrayOfbyte == null || paramArrayOfbyte.length == 0);
  }
  
  public static boolean isEmpty(boolean[] paramArrayOfboolean) {
    return (paramArrayOfboolean == null || paramArrayOfboolean.length == 0);
  }
  
  public static int size(Object[] paramArrayOfObject) {
    int i;
    if (paramArrayOfObject == null) {
      i = 0;
    } else {
      i = paramArrayOfObject.length;
    } 
    return i;
  }
  
  public static int size(Collection<?> paramCollection) {
    int i;
    if (paramCollection == null) {
      i = 0;
    } else {
      i = paramCollection.size();
    } 
    return i;
  }
  
  public static <T> boolean contains(T[] paramArrayOfT, T paramT) {
    boolean bool;
    if (indexOf(paramArrayOfT, paramT) != -1) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static <T> int indexOf(T[] paramArrayOfT, T paramT) {
    if (paramArrayOfT == null)
      return -1; 
    for (byte b = 0; b < paramArrayOfT.length; b++) {
      if (Objects.equals(paramArrayOfT[b], paramT))
        return b; 
    } 
    return -1;
  }
  
  public static <T> boolean containsAll(T[] paramArrayOfT1, T[] paramArrayOfT2) {
    if (paramArrayOfT2 == null)
      return true; 
    int i;
    byte b;
    for (i = paramArrayOfT2.length, b = 0; b < i; ) {
      T t = paramArrayOfT2[b];
      if (!contains(paramArrayOfT1, t))
        return false; 
      b++;
    } 
    return true;
  }
  
  public static <T> boolean containsAny(T[] paramArrayOfT1, T[] paramArrayOfT2) {
    if (paramArrayOfT2 == null)
      return false; 
    int i;
    byte b;
    for (i = paramArrayOfT2.length, b = 0; b < i; ) {
      T t = paramArrayOfT2[b];
      if (contains(paramArrayOfT1, t))
        return true; 
      b++;
    } 
    return false;
  }
  
  public static boolean contains(int[] paramArrayOfint, int paramInt) {
    if (paramArrayOfint == null)
      return false; 
    int i;
    byte b;
    for (i = paramArrayOfint.length, b = 0; b < i; ) {
      int j = paramArrayOfint[b];
      if (j == paramInt)
        return true; 
      b++;
    } 
    return false;
  }
  
  public static boolean contains(long[] paramArrayOflong, long paramLong) {
    if (paramArrayOflong == null)
      return false; 
    int i;
    byte b;
    for (i = paramArrayOflong.length, b = 0; b < i; ) {
      long l = paramArrayOflong[b];
      if (l == paramLong)
        return true; 
      b++;
    } 
    return false;
  }
  
  public static boolean contains(char[] paramArrayOfchar, char paramChar) {
    if (paramArrayOfchar == null)
      return false; 
    int i;
    byte b;
    for (i = paramArrayOfchar.length, b = 0; b < i; ) {
      char c = paramArrayOfchar[b];
      if (c == paramChar)
        return true; 
      b++;
    } 
    return false;
  }
  
  public static <T> boolean containsAll(char[] paramArrayOfchar1, char[] paramArrayOfchar2) {
    if (paramArrayOfchar2 == null)
      return true; 
    int i;
    byte b;
    for (i = paramArrayOfchar2.length, b = 0; b < i; ) {
      char c = paramArrayOfchar2[b];
      if (!contains(paramArrayOfchar1, c))
        return false; 
      b++;
    } 
    return true;
  }
  
  public static long total(long[] paramArrayOflong) {
    long l1 = 0L;
    long l2 = l1;
    if (paramArrayOflong != null) {
      int i = paramArrayOflong.length;
      byte b = 0;
      while (true) {
        l2 = l1;
        if (b < i) {
          l2 = paramArrayOflong[b];
          l1 += l2;
          b++;
          continue;
        } 
        break;
      } 
    } 
    return l2;
  }
  
  public static int[] convertToIntArray(List<Integer> paramList) {
    int[] arrayOfInt = new int[paramList.size()];
    for (byte b = 0; b < paramList.size(); b++)
      arrayOfInt[b] = ((Integer)paramList.get(b)).intValue(); 
    return arrayOfInt;
  }
  
  public static long[] convertToLongArray(int[] paramArrayOfint) {
    if (paramArrayOfint == null)
      return null; 
    long[] arrayOfLong = new long[paramArrayOfint.length];
    for (byte b = 0; b < paramArrayOfint.length; b++)
      arrayOfLong[b] = paramArrayOfint[b]; 
    return arrayOfLong;
  }
  
  public static <T> T[] concatElements(Class<T> paramClass, T[]... paramVarArgs) {
    if (paramVarArgs == null || paramVarArgs.length == 0)
      return createEmptyArray(paramClass); 
    int i = 0;
    int j;
    byte b;
    for (j = paramVarArgs.length, b = 0; b < j; ) {
      T[] arrayOfT = paramVarArgs[b];
      if (arrayOfT != null)
        i += arrayOfT.length; 
      b++;
    } 
    if (i == 0)
      return createEmptyArray(paramClass); 
    Object[] arrayOfObject = (Object[])Array.newInstance(paramClass, i);
    j = 0;
    for (int k = paramVarArgs.length; b < k; ) {
      T[] arrayOfT = paramVarArgs[b];
      i = j;
      if (arrayOfT != null)
        if (arrayOfT.length == 0) {
          i = j;
        } else {
          System.arraycopy(arrayOfT, 0, arrayOfObject, j, arrayOfT.length);
          i = j + arrayOfT.length;
        }  
      b++;
      j = i;
    } 
    return (T[])arrayOfObject;
  }
  
  private static <T> T[] createEmptyArray(Class<T> paramClass) {
    if (paramClass == String.class)
      return (T[])EmptyArray.STRING; 
    if (paramClass == Object.class)
      return (T[])EmptyArray.OBJECT; 
    return (T[])Array.newInstance(paramClass, 0);
  }
  
  public static <T> T[] appendElement(Class<T> paramClass, T[] paramArrayOfT, T paramT) {
    return appendElement(paramClass, paramArrayOfT, paramT, false);
  }
  
  public static <T> T[] appendElement(Class<T> paramClass, T[] paramArrayOfT, T paramT, boolean paramBoolean) {
    Object[] arrayOfObject;
    boolean bool;
    if (paramArrayOfT != null) {
      if (!paramBoolean && contains(paramArrayOfT, paramT))
        return paramArrayOfT; 
      bool = paramArrayOfT.length;
      arrayOfObject = (Object[])Array.newInstance(paramClass, bool + 1);
      System.arraycopy(paramArrayOfT, 0, arrayOfObject, 0, bool);
    } else {
      bool = false;
      arrayOfObject = (Object[])Array.newInstance((Class<?>)arrayOfObject, 1);
    } 
    arrayOfObject[bool] = paramT;
    return (T[])arrayOfObject;
  }
  
  public static <T> T[] removeElement(Class<T> paramClass, T[] paramArrayOfT, T paramT) {
    if (paramArrayOfT != null) {
      if (!contains(paramArrayOfT, paramT))
        return paramArrayOfT; 
      int i = paramArrayOfT.length;
      for (byte b = 0; b < i; b++) {
        if (Objects.equals(paramArrayOfT[b], paramT)) {
          if (i == 1)
            return null; 
          Object[] arrayOfObject = (Object[])Array.newInstance(paramClass, i - 1);
          System.arraycopy(paramArrayOfT, 0, arrayOfObject, 0, b);
          System.arraycopy(paramArrayOfT, b + 1, arrayOfObject, b, i - b - 1);
          return (T[])arrayOfObject;
        } 
      } 
    } 
    return paramArrayOfT;
  }
  
  public static int[] appendInt(int[] paramArrayOfint, int paramInt, boolean paramBoolean) {
    if (paramArrayOfint == null)
      return new int[] { paramInt }; 
    int i = paramArrayOfint.length;
    if (!paramBoolean)
      for (byte b = 0; b < i; b++) {
        if (paramArrayOfint[b] == paramInt)
          return paramArrayOfint; 
      }  
    int[] arrayOfInt = new int[i + 1];
    System.arraycopy(paramArrayOfint, 0, arrayOfInt, 0, i);
    arrayOfInt[i] = paramInt;
    return arrayOfInt;
  }
  
  public static int[] appendInt(int[] paramArrayOfint, int paramInt) {
    return appendInt(paramArrayOfint, paramInt, false);
  }
  
  public static int[] removeInt(int[] paramArrayOfint, int paramInt) {
    if (paramArrayOfint == null)
      return null; 
    int i = paramArrayOfint.length;
    for (byte b = 0; b < i; b++) {
      if (paramArrayOfint[b] == paramInt) {
        int[] arrayOfInt = new int[i - 1];
        if (b > 0)
          System.arraycopy(paramArrayOfint, 0, arrayOfInt, 0, b); 
        if (b < i - 1)
          System.arraycopy(paramArrayOfint, b + 1, arrayOfInt, b, i - b - 1); 
        return arrayOfInt;
      } 
    } 
    return paramArrayOfint;
  }
  
  public static String[] removeString(String[] paramArrayOfString, String paramString) {
    if (paramArrayOfString == null)
      return null; 
    int i = paramArrayOfString.length;
    for (byte b = 0; b < i; b++) {
      if (Objects.equals(paramArrayOfString[b], paramString)) {
        String[] arrayOfString = new String[i - 1];
        if (b > 0)
          System.arraycopy(paramArrayOfString, 0, arrayOfString, 0, b); 
        if (b < i - 1)
          System.arraycopy(paramArrayOfString, b + 1, arrayOfString, b, i - b - 1); 
        return arrayOfString;
      } 
    } 
    return paramArrayOfString;
  }
  
  public static long[] appendLong(long[] paramArrayOflong, long paramLong, boolean paramBoolean) {
    if (paramArrayOflong == null)
      return new long[] { paramLong }; 
    int i = paramArrayOflong.length;
    if (!paramBoolean)
      for (byte b = 0; b < i; b++) {
        if (paramArrayOflong[b] == paramLong)
          return paramArrayOflong; 
      }  
    long[] arrayOfLong = new long[i + 1];
    System.arraycopy(paramArrayOflong, 0, arrayOfLong, 0, i);
    arrayOfLong[i] = paramLong;
    return arrayOfLong;
  }
  
  public static long[] appendLong(long[] paramArrayOflong, long paramLong) {
    return appendLong(paramArrayOflong, paramLong, false);
  }
  
  public static long[] removeLong(long[] paramArrayOflong, long paramLong) {
    if (paramArrayOflong == null)
      return null; 
    int i = paramArrayOflong.length;
    for (byte b = 0; b < i; b++) {
      if (paramArrayOflong[b] == paramLong) {
        long[] arrayOfLong = new long[i - 1];
        if (b > 0)
          System.arraycopy(paramArrayOflong, 0, arrayOfLong, 0, b); 
        if (b < i - 1)
          System.arraycopy(paramArrayOflong, b + 1, arrayOfLong, b, i - b - 1); 
        return arrayOfLong;
      } 
    } 
    return paramArrayOflong;
  }
  
  public static long[] cloneOrNull(long[] paramArrayOflong) {
    if (paramArrayOflong != null) {
      paramArrayOflong = (long[])paramArrayOflong.clone();
    } else {
      paramArrayOflong = null;
    } 
    return paramArrayOflong;
  }
  
  public static <T> T[] cloneOrNull(T[] paramArrayOfT) {
    if (paramArrayOfT != null) {
      paramArrayOfT = (T[])paramArrayOfT.clone();
    } else {
      paramArrayOfT = null;
    } 
    return paramArrayOfT;
  }
  
  public static <T> ArraySet<T> cloneOrNull(ArraySet<T> paramArraySet) {
    if (paramArraySet != null) {
      paramArraySet = new ArraySet(paramArraySet);
    } else {
      paramArraySet = null;
    } 
    return paramArraySet;
  }
  
  public static <T> ArraySet<T> add(ArraySet<T> paramArraySet, T paramT) {
    ArraySet<T> arraySet = paramArraySet;
    if (paramArraySet == null)
      arraySet = new ArraySet(); 
    arraySet.add(paramT);
    return arraySet;
  }
  
  public static <T> ArraySet<T> remove(ArraySet<T> paramArraySet, T paramT) {
    if (paramArraySet == null)
      return null; 
    paramArraySet.remove(paramT);
    if (paramArraySet.isEmpty())
      return null; 
    return paramArraySet;
  }
  
  public static <T> ArrayList<T> add(ArrayList<T> paramArrayList, T paramT) {
    ArrayList<T> arrayList = paramArrayList;
    if (paramArrayList == null)
      arrayList = new ArrayList<>(); 
    arrayList.add(paramT);
    return arrayList;
  }
  
  public static <T> ArrayList<T> add(ArrayList<T> paramArrayList, int paramInt, T paramT) {
    ArrayList<T> arrayList = paramArrayList;
    if (paramArrayList == null)
      arrayList = new ArrayList<>(); 
    arrayList.add(paramInt, paramT);
    return arrayList;
  }
  
  public static <T> ArrayList<T> remove(ArrayList<T> paramArrayList, T paramT) {
    if (paramArrayList == null)
      return null; 
    paramArrayList.remove(paramT);
    if (paramArrayList.isEmpty())
      return null; 
    return paramArrayList;
  }
  
  public static <T> boolean contains(Collection<T> paramCollection, T paramT) {
    boolean bool;
    if (paramCollection != null) {
      bool = paramCollection.contains(paramT);
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static <T> T[] trimToSize(T[] paramArrayOfT, int paramInt) {
    if (paramArrayOfT == null || paramInt == 0)
      return null; 
    if (paramArrayOfT.length == paramInt)
      return paramArrayOfT; 
    return Arrays.copyOf(paramArrayOfT, paramInt);
  }
  
  public static <T> boolean referenceEquals(ArrayList<T> paramArrayList1, ArrayList<T> paramArrayList2) {
    if (paramArrayList1 == paramArrayList2)
      return true; 
    int i = paramArrayList1.size();
    int j = paramArrayList2.size();
    if (paramArrayList1 == null || paramArrayList2 == null || i != j)
      return false; 
    int k = 0;
    for (j = 0; j < i && !k; j++) {
      byte b;
      if (paramArrayList1.get(j) != paramArrayList2.get(j)) {
        b = 1;
      } else {
        b = 0;
      } 
      k |= b;
    } 
    return k ^ 0x1;
  }
  
  public static <T> int unstableRemoveIf(ArrayList<T> paramArrayList, Predicate<T> paramPredicate) {
    int m;
    if (paramArrayList == null)
      return 0; 
    int i = paramArrayList.size();
    int j = 0;
    int k = i - 1;
    while (true) {
      m = j;
      if (j <= k) {
        while (true) {
          m = k;
          if (j < i) {
            m = k;
            if (!paramPredicate.test(paramArrayList.get(j))) {
              j++;
              continue;
            } 
          } 
          break;
        } 
        while (m > j && paramPredicate.test(paramArrayList.get(m)))
          m--; 
        if (j >= m) {
          m = j;
          break;
        } 
        Collections.swap(paramArrayList, j, m);
        j++;
        k = m - 1;
        continue;
      } 
      break;
    } 
    for (j = i - 1; j >= m; j--)
      paramArrayList.remove(j); 
    return i - m;
  }
  
  public static int[] defeatNullable(int[] paramArrayOfint) {
    if (paramArrayOfint == null)
      paramArrayOfint = EmptyArray.INT; 
    return paramArrayOfint;
  }
  
  public static String[] defeatNullable(String[] paramArrayOfString) {
    if (paramArrayOfString == null)
      paramArrayOfString = EmptyArray.STRING; 
    return paramArrayOfString;
  }
  
  public static File[] defeatNullable(File[] paramArrayOfFile) {
    if (paramArrayOfFile == null)
      paramArrayOfFile = EMPTY_FILE; 
    return paramArrayOfFile;
  }
  
  public static void checkBounds(int paramInt1, int paramInt2) {
    if (paramInt2 >= 0 && paramInt1 > paramInt2)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("length=");
    stringBuilder.append(paramInt1);
    stringBuilder.append("; index=");
    stringBuilder.append(paramInt2);
    throw new ArrayIndexOutOfBoundsException(stringBuilder.toString());
  }
  
  public static <T> T[] filterNotNull(T[] paramArrayOfT, IntFunction<T[]> paramIntFunction) {
    int i = 0;
    int j = size((Object[])paramArrayOfT);
    byte b;
    for (b = 0; b < j; b++, i = k) {
      int k = i;
      if (paramArrayOfT[b] == null)
        k = i + 1; 
    } 
    if (i == 0)
      return paramArrayOfT; 
    Object[] arrayOfObject = (Object[])paramIntFunction.apply(j - i);
    i = 0;
    for (b = 0; b < j; b++, i = k) {
      int k = i;
      if (paramArrayOfT[b] != null) {
        arrayOfObject[i] = paramArrayOfT[b];
        k = i + 1;
      } 
    } 
    return (T[])arrayOfObject;
  }
  
  public static <T> T[] filter(T[] paramArrayOfT, IntFunction<T[]> paramIntFunction, Predicate<T> paramPredicate) {
    if (isEmpty(paramArrayOfT))
      return paramArrayOfT; 
    int i = 0;
    int j = size((Object[])paramArrayOfT);
    byte b;
    for (b = 0; b < j; b++, i = m) {
      int m = i;
      if (paramPredicate.test(paramArrayOfT[b]))
        m = i + 1; 
    } 
    if (i == 0)
      return paramArrayOfT; 
    if (i == paramArrayOfT.length)
      return paramArrayOfT; 
    if (i == 0)
      return null; 
    Object[] arrayOfObject = (Object[])paramIntFunction.apply(i);
    int k = 0;
    for (b = 0; b < j; b++, k = i) {
      i = k;
      if (paramPredicate.test(paramArrayOfT[b])) {
        arrayOfObject[k] = paramArrayOfT[b];
        i = k + 1;
      } 
    } 
    return (T[])arrayOfObject;
  }
  
  public static boolean startsWith(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) {
    if (paramArrayOfbyte1 == null || paramArrayOfbyte2 == null)
      return false; 
    if (paramArrayOfbyte1.length < paramArrayOfbyte2.length)
      return false; 
    for (byte b = 0; b < paramArrayOfbyte2.length; b++) {
      if (paramArrayOfbyte1[b] != paramArrayOfbyte2[b])
        return false; 
    } 
    return true;
  }
  
  public static <T> T find(T[] paramArrayOfT, Predicate<T> paramPredicate) {
    if (isEmpty(paramArrayOfT))
      return null; 
    int i;
    byte b;
    for (i = paramArrayOfT.length, b = 0; b < i; ) {
      T t = paramArrayOfT[b];
      if (paramPredicate.test(t))
        return t; 
      b++;
    } 
    return null;
  }
  
  public static String deepToString(Object paramObject) {
    if (paramObject != null && paramObject.getClass().isArray()) {
      if (paramObject.getClass() == boolean[].class)
        return Arrays.toString((boolean[])paramObject); 
      if (paramObject.getClass() == byte[].class)
        return Arrays.toString((byte[])paramObject); 
      if (paramObject.getClass() == char[].class)
        return Arrays.toString((char[])paramObject); 
      if (paramObject.getClass() == double[].class)
        return Arrays.toString((double[])paramObject); 
      if (paramObject.getClass() == float[].class)
        return Arrays.toString((float[])paramObject); 
      if (paramObject.getClass() == int[].class)
        return Arrays.toString((int[])paramObject); 
      if (paramObject.getClass() == long[].class)
        return Arrays.toString((long[])paramObject); 
      if (paramObject.getClass() == short[].class)
        return Arrays.toString((short[])paramObject); 
      return Arrays.deepToString((Object[])paramObject);
    } 
    return String.valueOf(paramObject);
  }
  
  public static <T> T firstOrNull(T[] paramArrayOfT) {
    if (paramArrayOfT.length > 0) {
      T t = paramArrayOfT[0];
    } else {
      paramArrayOfT = null;
    } 
    return (T)paramArrayOfT;
  }
}
