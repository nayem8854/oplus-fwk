package com.android.internal.util;

import java.lang.reflect.Array;

public class RingBuffer<T> {
  private final T[] mBuffer;
  
  private long mCursor = 0L;
  
  public RingBuffer(Class<T> paramClass, int paramInt) {
    Preconditions.checkArgumentPositive(paramInt, "A RingBuffer cannot have 0 capacity");
    this.mBuffer = (T[])Array.newInstance(paramClass, paramInt);
  }
  
  public int size() {
    return (int)Math.min(this.mBuffer.length, this.mCursor);
  }
  
  public boolean isEmpty() {
    boolean bool;
    if (size() == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void clear() {
    for (byte b = 0; b < size(); b++)
      this.mBuffer[b] = null; 
    this.mCursor = 0L;
  }
  
  public void append(T paramT) {
    T[] arrayOfT = this.mBuffer;
    long l = this.mCursor;
    this.mCursor = 1L + l;
    arrayOfT[indexOf(l)] = paramT;
  }
  
  public T getNextSlot() {
    long l = this.mCursor;
    this.mCursor = 1L + l;
    int i = indexOf(l);
    T[] arrayOfT = this.mBuffer;
    if (arrayOfT[i] == null)
      arrayOfT[i] = createNewItem(); 
    return this.mBuffer[i];
  }
  
  protected T createNewItem() {
    try {
      return (T)this.mBuffer.getClass().getComponentType().newInstance();
    } catch (IllegalAccessException|InstantiationException illegalAccessException) {
      return null;
    } 
  }
  
  public T[] toArray() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mBuffer : [Ljava/lang/Object;
    //   4: aload_0
    //   5: invokevirtual size : ()I
    //   8: aload_0
    //   9: getfield mBuffer : [Ljava/lang/Object;
    //   12: invokevirtual getClass : ()Ljava/lang/Class;
    //   15: invokestatic copyOf : ([Ljava/lang/Object;ILjava/lang/Class;)[Ljava/lang/Object;
    //   18: astore_1
    //   19: aload_0
    //   20: getfield mCursor : J
    //   23: lconst_1
    //   24: lsub
    //   25: lstore_2
    //   26: aload_1
    //   27: arraylength
    //   28: iconst_1
    //   29: isub
    //   30: istore #4
    //   32: iload #4
    //   34: iflt -> 61
    //   37: aload_1
    //   38: iload #4
    //   40: aload_0
    //   41: getfield mBuffer : [Ljava/lang/Object;
    //   44: aload_0
    //   45: lload_2
    //   46: invokespecial indexOf : (J)I
    //   49: aaload
    //   50: aastore
    //   51: iinc #4, -1
    //   54: lload_2
    //   55: lconst_1
    //   56: lsub
    //   57: lstore_2
    //   58: goto -> 32
    //   61: aload_1
    //   62: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #89	-> 0
    //   #91	-> 19
    //   #92	-> 26
    //   #93	-> 32
    //   #94	-> 37
    //   #96	-> 61
  }
  
  private int indexOf(long paramLong) {
    return (int)Math.abs(paramLong % this.mBuffer.length);
  }
}
