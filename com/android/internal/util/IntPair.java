package com.android.internal.util;

public class IntPair {
  public static long of(int paramInt1, int paramInt2) {
    return paramInt1 << 32L | paramInt2 & 0xFFFFFFFFL;
  }
  
  public static int first(long paramLong) {
    return (int)(paramLong >> 32L);
  }
  
  public static int second(long paramLong) {
    return (int)paramLong;
  }
}
