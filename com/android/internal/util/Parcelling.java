package com.android.internal.util;

import android.os.Parcel;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.ArraySet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

public interface Parcelling<T> {
  void parcel(T paramT, Parcel paramParcel, int paramInt);
  
  T unparcel(Parcel paramParcel);
  
  public static class Cache {
    private static ArrayMap<Class, Parcelling> sCache = new ArrayMap();
    
    public static <P extends Parcelling<?>> P get(Class<P> param1Class) {
      return (P)sCache.get(param1Class);
    }
    
    public static <P extends Parcelling<?>> P put(P param1P) {
      sCache.put(param1P.getClass(), param1P);
      return param1P;
    }
    
    public static <P extends Parcelling<?>> P getOrCreate(Class<P> param1Class) {
      P p = (P)get((Class)param1Class);
      if (p != null)
        return p; 
      try {
        return (P)put((Parcelling)param1Class.newInstance());
      } catch (Exception exception) {
        throw new RuntimeException(exception);
      } 
    }
  }
  
  class ForInternedString implements Parcelling<String> {
    public void parcel(String param1String, Parcel param1Parcel, int param1Int) {
      param1Parcel.writeString(param1String);
    }
    
    public String unparcel(Parcel param1Parcel) {
      return TextUtils.safeIntern(param1Parcel.readString());
    }
  }
  
  class ForInternedStringArray implements Parcelling<String[]> {
    public void parcel(String[] param1ArrayOfString, Parcel param1Parcel, int param1Int) {
      param1Parcel.writeStringArray(param1ArrayOfString);
    }
    
    public String[] unparcel(Parcel param1Parcel) {
      String[] arrayOfString = param1Parcel.readStringArray();
      if (arrayOfString != null) {
        int i = ArrayUtils.size((Object[])arrayOfString);
        for (byte b = 0; b < i; b++)
          arrayOfString[b] = TextUtils.safeIntern(arrayOfString[b]); 
      } 
      return arrayOfString;
    }
  }
  
  class ForInternedStringList implements Parcelling<List<String>> {
    public void parcel(List<String> param1List, Parcel param1Parcel, int param1Int) {
      param1Parcel.writeStringList(param1List);
    }
    
    public List<String> unparcel(Parcel param1Parcel) {
      ArrayList<String> arrayList = param1Parcel.createStringArrayList();
      if (arrayList != null) {
        int i = arrayList.size();
        for (byte b = 0; b < i; b++)
          arrayList.set(b, ((String)arrayList.get(b)).intern()); 
      } 
      return CollectionUtils.emptyIfNull(arrayList);
    }
  }
  
  class ForInternedStringValueMap implements Parcelling<Map<String, String>> {
    public void parcel(Map<String, String> param1Map, Parcel param1Parcel, int param1Int) {
      param1Parcel.writeMap(param1Map);
    }
    
    public Map<String, String> unparcel(Parcel param1Parcel) {
      ArrayMap arrayMap = new ArrayMap();
      param1Parcel.readMap((Map)arrayMap, String.class.getClassLoader());
      for (byte b = 0; b < arrayMap.size(); b++)
        arrayMap.setValueAt(b, TextUtils.safeIntern((String)arrayMap.valueAt(b))); 
      return (Map<String, String>)arrayMap;
    }
  }
  
  class ForStringSet implements Parcelling<Set<String>> {
    public void parcel(Set<String> param1Set, Parcel param1Parcel, int param1Int) {
      if (param1Set == null) {
        param1Parcel.writeInt(-1);
      } else {
        param1Parcel.writeInt(param1Set.size());
        for (String str : param1Set)
          param1Parcel.writeString(str); 
      } 
    }
    
    public Set<String> unparcel(Parcel param1Parcel) {
      int i = param1Parcel.readInt();
      if (i < 0)
        return Collections.emptySet(); 
      ArraySet<String> arraySet = new ArraySet();
      for (byte b = 0; b < i; b++)
        arraySet.add(param1Parcel.readString()); 
      return (Set<String>)arraySet;
    }
  }
  
  class ForInternedStringSet implements Parcelling<Set<String>> {
    public void parcel(Set<String> param1Set, Parcel param1Parcel, int param1Int) {
      if (param1Set == null) {
        param1Parcel.writeInt(-1);
      } else {
        param1Parcel.writeInt(param1Set.size());
        for (String str : param1Set)
          param1Parcel.writeString(str); 
      } 
    }
    
    public Set<String> unparcel(Parcel param1Parcel) {
      int i = param1Parcel.readInt();
      if (i < 0)
        return Collections.emptySet(); 
      ArraySet<String> arraySet = new ArraySet();
      for (byte b = 0; b < i; b++)
        arraySet.add(TextUtils.safeIntern(param1Parcel.readString())); 
      return (Set<String>)arraySet;
    }
  }
  
  class ForInternedStringArraySet implements Parcelling<ArraySet<String>> {
    public void parcel(ArraySet<String> param1ArraySet, Parcel param1Parcel, int param1Int) {
      if (param1ArraySet == null) {
        param1Parcel.writeInt(-1);
      } else {
        param1Parcel.writeInt(param1ArraySet.size());
        for (String str : param1ArraySet)
          param1Parcel.writeString(str); 
      } 
    }
    
    public ArraySet<String> unparcel(Parcel param1Parcel) {
      int i = param1Parcel.readInt();
      if (i < 0)
        return null; 
      ArraySet<String> arraySet = new ArraySet();
      for (byte b = 0; b < i; b++)
        arraySet.add(TextUtils.safeIntern(param1Parcel.readString())); 
      return arraySet;
    }
  }
  
  public static interface BuiltIn {
    class ForInternedString implements Parcelling<String> {
      public void parcel(String param2String, Parcel param2Parcel, int param2Int) {
        param2Parcel.writeString(param2String);
      }
      
      public String unparcel(Parcel param2Parcel) {
        return TextUtils.safeIntern(param2Parcel.readString());
      }
    }
    
    class ForInternedStringArray implements Parcelling<String[]> {
      public void parcel(String[] param2ArrayOfString, Parcel param2Parcel, int param2Int) {
        param2Parcel.writeStringArray(param2ArrayOfString);
      }
      
      public String[] unparcel(Parcel param2Parcel) {
        String[] arrayOfString = param2Parcel.readStringArray();
        if (arrayOfString != null) {
          int i = ArrayUtils.size((Object[])arrayOfString);
          for (byte b = 0; b < i; b++)
            arrayOfString[b] = TextUtils.safeIntern(arrayOfString[b]); 
        } 
        return arrayOfString;
      }
    }
    
    class ForInternedStringList implements Parcelling<List<String>> {
      public void parcel(List<String> param2List, Parcel param2Parcel, int param2Int) {
        param2Parcel.writeStringList(param2List);
      }
      
      public List<String> unparcel(Parcel param2Parcel) {
        ArrayList<String> arrayList = param2Parcel.createStringArrayList();
        if (arrayList != null) {
          int i = arrayList.size();
          for (byte b = 0; b < i; b++)
            arrayList.set(b, ((String)arrayList.get(b)).intern()); 
        } 
        return CollectionUtils.emptyIfNull(arrayList);
      }
    }
    
    class ForInternedStringValueMap implements Parcelling<Map<String, String>> {
      public void parcel(Map<String, String> param2Map, Parcel param2Parcel, int param2Int) {
        param2Parcel.writeMap(param2Map);
      }
      
      public Map<String, String> unparcel(Parcel param2Parcel) {
        ArrayMap arrayMap = new ArrayMap();
        param2Parcel.readMap((Map)arrayMap, String.class.getClassLoader());
        for (byte b = 0; b < arrayMap.size(); b++)
          arrayMap.setValueAt(b, TextUtils.safeIntern((String)arrayMap.valueAt(b))); 
        return (Map<String, String>)arrayMap;
      }
    }
    
    class ForStringSet implements Parcelling<Set<String>> {
      public void parcel(Set<String> param2Set, Parcel param2Parcel, int param2Int) {
        if (param2Set == null) {
          param2Parcel.writeInt(-1);
        } else {
          param2Parcel.writeInt(param2Set.size());
          for (String str : param2Set)
            param2Parcel.writeString(str); 
        } 
      }
      
      public Set<String> unparcel(Parcel param2Parcel) {
        int i = param2Parcel.readInt();
        if (i < 0)
          return Collections.emptySet(); 
        ArraySet<String> arraySet = new ArraySet();
        for (byte b = 0; b < i; b++)
          arraySet.add(param2Parcel.readString()); 
        return (Set<String>)arraySet;
      }
    }
    
    class ForInternedStringSet implements Parcelling<Set<String>> {
      public void parcel(Set<String> param2Set, Parcel param2Parcel, int param2Int) {
        if (param2Set == null) {
          param2Parcel.writeInt(-1);
        } else {
          param2Parcel.writeInt(param2Set.size());
          for (String str : param2Set)
            param2Parcel.writeString(str); 
        } 
      }
      
      public Set<String> unparcel(Parcel param2Parcel) {
        int i = param2Parcel.readInt();
        if (i < 0)
          return Collections.emptySet(); 
        ArraySet<String> arraySet = new ArraySet();
        for (byte b = 0; b < i; b++)
          arraySet.add(TextUtils.safeIntern(param2Parcel.readString())); 
        return (Set<String>)arraySet;
      }
    }
    
    class ForInternedStringArraySet implements Parcelling<ArraySet<String>> {
      public void parcel(ArraySet<String> param2ArraySet, Parcel param2Parcel, int param2Int) {
        if (param2ArraySet == null) {
          param2Parcel.writeInt(-1);
        } else {
          param2Parcel.writeInt(param2ArraySet.size());
          for (String str : param2ArraySet)
            param2Parcel.writeString(str); 
        } 
      }
      
      public ArraySet<String> unparcel(Parcel param2Parcel) {
        int i = param2Parcel.readInt();
        if (i < 0)
          return null; 
        ArraySet<String> arraySet = new ArraySet();
        for (byte b = 0; b < i; b++)
          arraySet.add(TextUtils.safeIntern(param2Parcel.readString())); 
        return arraySet;
      }
    }
    
    class ForBoolean implements Parcelling<Boolean> {
      public void parcel(Boolean param2Boolean, Parcel param2Parcel, int param2Int) {
        if (param2Boolean == null) {
          param2Parcel.writeInt(1);
        } else if (!param2Boolean.booleanValue()) {
          param2Parcel.writeInt(0);
        } else {
          param2Parcel.writeInt(-1);
        } 
      }
      
      public Boolean unparcel(Parcel param2Parcel) {
        int i = param2Parcel.readInt();
        if (i != -1) {
          if (i != 0) {
            if (i == 1)
              return null; 
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Malformed Parcel reading Boolean: ");
            stringBuilder.append(param2Parcel);
            throw new IllegalStateException(stringBuilder.toString());
          } 
          return Boolean.FALSE;
        } 
        return Boolean.TRUE;
      }
    }
    
    class ForPattern implements Parcelling<Pattern> {
      public void parcel(Pattern param2Pattern, Parcel param2Parcel, int param2Int) {
        String str;
        if (param2Pattern == null) {
          param2Pattern = null;
        } else {
          str = param2Pattern.pattern();
        } 
        param2Parcel.writeString(str);
      }
      
      public Pattern unparcel(Parcel param2Parcel) {
        Pattern pattern;
        String str = param2Parcel.readString();
        if (str == null) {
          str = null;
        } else {
          pattern = Pattern.compile(str);
        } 
        return pattern;
      }
    }
  }
  
  class ForBoolean implements Parcelling<Boolean> {
    public void parcel(Boolean param1Boolean, Parcel param1Parcel, int param1Int) {
      if (param1Boolean == null) {
        param1Parcel.writeInt(1);
      } else if (!param1Boolean.booleanValue()) {
        param1Parcel.writeInt(0);
      } else {
        param1Parcel.writeInt(-1);
      } 
    }
    
    public Boolean unparcel(Parcel param1Parcel) {
      int i = param1Parcel.readInt();
      if (i != -1) {
        if (i != 0) {
          if (i == 1)
            return null; 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Malformed Parcel reading Boolean: ");
          stringBuilder.append(param1Parcel);
          throw new IllegalStateException(stringBuilder.toString());
        } 
        return Boolean.FALSE;
      } 
      return Boolean.TRUE;
    }
  }
  
  class ForPattern implements Parcelling<Pattern> {
    public void parcel(Pattern param1Pattern, Parcel param1Parcel, int param1Int) {
      String str;
      if (param1Pattern == null) {
        param1Pattern = null;
      } else {
        str = param1Pattern.pattern();
      } 
      param1Parcel.writeString(str);
    }
    
    public Pattern unparcel(Parcel param1Parcel) {
      Pattern pattern;
      String str = param1Parcel.readString();
      if (str == null) {
        str = null;
      } else {
        pattern = Pattern.compile(str);
      } 
      return pattern;
    }
  }
}
