package com.android.internal.util;

import java.io.IOException;
import java.io.InputStream;
import libcore.io.Streams;

public class SizedInputStream extends InputStream {
  private long mLength;
  
  private final InputStream mWrapped;
  
  public SizedInputStream(InputStream paramInputStream, long paramLong) {
    this.mWrapped = paramInputStream;
    this.mLength = paramLong;
  }
  
  public void close() throws IOException {
    super.close();
    this.mWrapped.close();
  }
  
  public int read() throws IOException {
    return Streams.readSingleByte(this);
  }
  
  public int read(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws IOException {
    long l = this.mLength;
    if (l <= 0L)
      return -1; 
    int i = paramInt2;
    if (paramInt2 > l)
      i = (int)l; 
    paramInt1 = this.mWrapped.read(paramArrayOfbyte, paramInt1, i);
    if (paramInt1 == -1) {
      if (this.mLength > 0L) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unexpected EOF; expected ");
        stringBuilder.append(this.mLength);
        stringBuilder.append(" more bytes");
        throw new IOException(stringBuilder.toString());
      } 
    } else {
      this.mLength -= paramInt1;
    } 
    return paramInt1;
  }
}
