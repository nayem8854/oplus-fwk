package com.android.internal.util;

import android.os.Process;
import android.util.Slog;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class ConcurrentUtils {
  public static final Executor DIRECT_EXECUTOR = new DirectExecutor();
  
  public static ExecutorService newFixedThreadPool(int paramInt1, final String poolName, final int linuxThreadPriority) {
    return Executors.newFixedThreadPool(paramInt1, new ThreadFactory() {
          private final AtomicInteger threadNum = new AtomicInteger(0);
          
          final int val$linuxThreadPriority;
          
          final String val$poolName;
          
          public Thread newThread(final Runnable r) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(poolName);
            stringBuilder.append(this.threadNum.incrementAndGet());
            return new Thread(stringBuilder.toString()) {
                final ConcurrentUtils.null this$0;
                
                final Runnable val$r;
                
                public void run() {
                  Process.setThreadPriority(linuxThreadPriority);
                  r.run();
                }
              };
          }
        });
  }
  
  public static <T> T waitForFutureNoInterrupt(Future<T> paramFuture, String paramString) {
    try {
      return paramFuture.get();
    } catch (InterruptedException interruptedException) {
      Thread.currentThread().interrupt();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append(" interrupted");
      throw new IllegalStateException(stringBuilder.toString());
    } catch (ExecutionException executionException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append(" failed");
      throw new RuntimeException(stringBuilder.toString(), executionException);
    } 
  }
  
  public static void waitForCountDownNoInterrupt(CountDownLatch paramCountDownLatch, long paramLong, String paramString) {
    try {
      if (paramCountDownLatch.await(paramLong, TimeUnit.MILLISECONDS))
        return; 
      IllegalStateException illegalStateException = new IllegalStateException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append(paramString);
      stringBuilder.append(" timed out.");
      this(stringBuilder.toString());
      throw illegalStateException;
    } catch (InterruptedException interruptedException) {
      Thread.currentThread().interrupt();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append(" interrupted.");
      throw new IllegalStateException(stringBuilder.toString());
    } 
  }
  
  public static void wtfIfLockHeld(String paramString, Object paramObject) {
    if (Thread.holdsLock(paramObject))
      Slog.wtf(paramString, "Lock mustn't be held"); 
  }
  
  public static void wtfIfLockNotHeld(String paramString, Object paramObject) {
    if (!Thread.holdsLock(paramObject))
      Slog.wtf(paramString, "Lock must be held"); 
  }
  
  private static class DirectExecutor implements Executor {
    private DirectExecutor() {}
    
    public void execute(Runnable param1Runnable) {
      param1Runnable.run();
    }
    
    public String toString() {
      return "DIRECT_EXECUTOR";
    }
  }
}
