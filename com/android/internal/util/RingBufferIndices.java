package com.android.internal.util;

public class RingBufferIndices {
  private final int mCapacity;
  
  private int mSize;
  
  private int mStart;
  
  public RingBufferIndices(int paramInt) {
    this.mCapacity = paramInt;
  }
  
  public int add() {
    int i = this.mSize, j = this.mCapacity;
    if (i < j) {
      j = this.mSize;
      this.mSize = i + 1;
      return j;
    } 
    i = this.mStart;
    int k = this.mStart + 1;
    if (k == j)
      this.mStart = 0; 
    return i;
  }
  
  public void clear() {
    this.mStart = 0;
    this.mSize = 0;
  }
  
  public int size() {
    return this.mSize;
  }
  
  public int indexOf(int paramInt) {
    int i = this.mStart + paramInt;
    int j = this.mCapacity;
    paramInt = i;
    if (i >= j)
      paramInt = i - j; 
    return paramInt;
  }
}
