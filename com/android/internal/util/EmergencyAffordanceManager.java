package com.android.internal.util;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.UserHandle;
import android.provider.Settings;

public class EmergencyAffordanceManager {
  private static final String EMERGENCY_CALL_NUMBER_SETTING = "emergency_affordance_number";
  
  public static final boolean ENABLED = true;
  
  private static final String FORCE_EMERGENCY_AFFORDANCE_SETTING = "force_emergency_affordance";
  
  private final Context mContext;
  
  public EmergencyAffordanceManager(Context paramContext) {
    this.mContext = paramContext;
  }
  
  public final void performEmergencyCall() {
    performEmergencyCall(this.mContext);
  }
  
  private static Uri getPhoneUri(Context paramContext) {
    String str1 = paramContext.getResources().getString(17039904);
    String str2 = str1;
    if (Build.IS_DEBUGGABLE) {
      ContentResolver contentResolver = paramContext.getContentResolver();
      String str = Settings.Global.getString(contentResolver, "emergency_affordance_number");
      str2 = str1;
      if (str != null)
        str2 = str; 
    } 
    return Uri.fromParts("tel", str2, null);
  }
  
  private static void performEmergencyCall(Context paramContext) {
    Intent intent = new Intent("android.intent.action.CALL_EMERGENCY");
    intent.setData(getPhoneUri(paramContext));
    intent.setFlags(268435456);
    paramContext.startActivityAsUser(intent, UserHandle.CURRENT);
  }
  
  public boolean needsEmergencyAffordance() {
    if (forceShowing())
      return true; 
    return isEmergencyAffordanceNeeded();
  }
  
  private boolean isEmergencyAffordanceNeeded() {
    ContentResolver contentResolver = this.mContext.getContentResolver();
    boolean bool = false;
    if (Settings.Global.getInt(contentResolver, "emergency_affordance_needed", 0) != 0)
      bool = true; 
    return bool;
  }
  
  private boolean forceShowing() {
    ContentResolver contentResolver = this.mContext.getContentResolver();
    boolean bool = false;
    if (Settings.Global.getInt(contentResolver, "force_emergency_affordance", 0) != 0)
      bool = true; 
    return bool;
  }
}
