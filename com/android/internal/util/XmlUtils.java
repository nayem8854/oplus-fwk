package com.android.internal.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Base64;
import android.util.Xml;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ProtocolException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import libcore.util.HexEncoding;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class XmlUtils {
  private static final String STRING_ARRAY_SEPARATOR = ":";
  
  public static void skipCurrentTag(XmlPullParser paramXmlPullParser) throws XmlPullParserException, IOException {
    int i = paramXmlPullParser.getDepth();
    while (true) {
      int j = paramXmlPullParser.next();
      if (j != 1) {
        if (j == 3) {
          if (paramXmlPullParser.getDepth() > i)
            continue; 
          break;
        } 
        continue;
      } 
      break;
    } 
  }
  
  public static final int convertValueToList(CharSequence paramCharSequence, String[] paramArrayOfString, int paramInt) {
    if (!TextUtils.isEmpty(paramCharSequence))
      for (byte b = 0; b < paramArrayOfString.length; b++) {
        if (paramCharSequence.equals(paramArrayOfString[b]))
          return b; 
      }  
    return paramInt;
  }
  
  public static final boolean convertValueToBoolean(CharSequence paramCharSequence, boolean paramBoolean) {
    boolean bool = false;
    if (TextUtils.isEmpty(paramCharSequence))
      return paramBoolean; 
    if (!paramCharSequence.equals("1") && 
      !paramCharSequence.equals("true")) {
      paramBoolean = bool;
      if (paramCharSequence.equals("TRUE")) {
        paramBoolean = true;
        return paramBoolean;
      } 
      return paramBoolean;
    } 
    paramBoolean = true;
    return paramBoolean;
  }
  
  public static final int convertValueToInt(CharSequence paramCharSequence, int paramInt) {
    if (TextUtils.isEmpty(paramCharSequence))
      return paramInt; 
    paramCharSequence = paramCharSequence.toString();
    byte b = 1;
    paramInt = 0;
    int i = paramCharSequence.length();
    int j = 10;
    if ('-' == paramCharSequence.charAt(0)) {
      b = -1;
      paramInt = 0 + 1;
    } 
    if ('0' == paramCharSequence.charAt(paramInt)) {
      if (paramInt == i - 1)
        return 0; 
      i = paramCharSequence.charAt(paramInt + 1);
      if (120 == i || 88 == i) {
        i = paramInt + 2;
        paramInt = 16;
      } else {
        i = paramInt + 1;
        paramInt = 8;
      } 
      j = paramInt;
    } else {
      i = paramInt;
      if ('#' == paramCharSequence.charAt(paramInt)) {
        i = paramInt + 1;
        j = 16;
      } 
    } 
    return Integer.parseInt(paramCharSequence.substring(i), j) * b;
  }
  
  public static int convertValueToUnsignedInt(String paramString, int paramInt) {
    if (TextUtils.isEmpty(paramString))
      return paramInt; 
    return parseUnsignedIntAttribute(paramString);
  }
  
  public static int parseUnsignedIntAttribute(CharSequence paramCharSequence) {
    paramCharSequence = paramCharSequence.toString();
    int i = 0;
    int j = paramCharSequence.length();
    byte b = 10;
    if ('0' == paramCharSequence.charAt(0)) {
      if (j - 1 == 0)
        return 0; 
      i = paramCharSequence.charAt(0 + 1);
      if (120 == i || 88 == i) {
        i = 0 + 2;
        b = 16;
        return (int)Long.parseLong(paramCharSequence.substring(i), b);
      } 
      i = 0 + 1;
      b = 8;
    } else if ('#' == paramCharSequence.charAt(0)) {
      i = 0 + 1;
      b = 16;
    } 
    return (int)Long.parseLong(paramCharSequence.substring(i), b);
  }
  
  public static final void writeMapXml(Map paramMap, OutputStream paramOutputStream) throws XmlPullParserException, IOException {
    FastXmlSerializer fastXmlSerializer = new FastXmlSerializer();
    fastXmlSerializer.setOutput(paramOutputStream, StandardCharsets.UTF_8.name());
    fastXmlSerializer.startDocument(null, Boolean.valueOf(true));
    fastXmlSerializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
    writeMapXml(paramMap, (String)null, fastXmlSerializer);
    fastXmlSerializer.endDocument();
  }
  
  public static final void writeListXml(List paramList, OutputStream paramOutputStream) throws XmlPullParserException, IOException {
    XmlSerializer xmlSerializer = Xml.newSerializer();
    xmlSerializer.setOutput(paramOutputStream, StandardCharsets.UTF_8.name());
    xmlSerializer.startDocument(null, Boolean.valueOf(true));
    xmlSerializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
    writeListXml(paramList, null, xmlSerializer);
    xmlSerializer.endDocument();
  }
  
  public static final void writeMapXml(Map paramMap, String paramString, XmlSerializer paramXmlSerializer) throws XmlPullParserException, IOException {
    writeMapXml(paramMap, paramString, paramXmlSerializer, null);
  }
  
  public static final void writeMapXml(Map paramMap, String paramString, XmlSerializer paramXmlSerializer, WriteMapCallback paramWriteMapCallback) throws XmlPullParserException, IOException {
    if (paramMap == null) {
      paramXmlSerializer.startTag(null, "null");
      paramXmlSerializer.endTag(null, "null");
      return;
    } 
    paramXmlSerializer.startTag(null, "map");
    if (paramString != null)
      paramXmlSerializer.attribute(null, "name", paramString); 
    writeMapXml(paramMap, paramXmlSerializer, paramWriteMapCallback);
    paramXmlSerializer.endTag(null, "map");
  }
  
  public static final void writeMapXml(Map paramMap, XmlSerializer paramXmlSerializer, WriteMapCallback paramWriteMapCallback) throws XmlPullParserException, IOException {
    if (paramMap == null)
      return; 
    Set set = paramMap.entrySet();
    Iterator<Map.Entry> iterator = set.iterator();
    while (iterator.hasNext()) {
      Map.Entry entry = iterator.next();
      writeValueXml(entry.getValue(), (String)entry.getKey(), paramXmlSerializer, paramWriteMapCallback);
    } 
  }
  
  public static final void writeListXml(List paramList, String paramString, XmlSerializer paramXmlSerializer) throws XmlPullParserException, IOException {
    if (paramList == null) {
      paramXmlSerializer.startTag(null, "null");
      paramXmlSerializer.endTag(null, "null");
      return;
    } 
    paramXmlSerializer.startTag(null, "list");
    if (paramString != null)
      paramXmlSerializer.attribute(null, "name", paramString); 
    int i = paramList.size();
    byte b = 0;
    while (b < i) {
      writeValueXml(paramList.get(b), null, paramXmlSerializer);
      b++;
    } 
    paramXmlSerializer.endTag(null, "list");
  }
  
  public static final void writeSetXml(Set paramSet, String paramString, XmlSerializer paramXmlSerializer) throws XmlPullParserException, IOException {
    if (paramSet == null) {
      paramXmlSerializer.startTag(null, "null");
      paramXmlSerializer.endTag(null, "null");
      return;
    } 
    paramXmlSerializer.startTag(null, "set");
    if (paramString != null)
      paramXmlSerializer.attribute(null, "name", paramString); 
    for (Set paramSet : paramSet)
      writeValueXml(paramSet, null, paramXmlSerializer); 
    paramXmlSerializer.endTag(null, "set");
  }
  
  public static final void writeByteArrayXml(byte[] paramArrayOfbyte, String paramString, XmlSerializer paramXmlSerializer) throws XmlPullParserException, IOException {
    if (paramArrayOfbyte == null) {
      paramXmlSerializer.startTag(null, "null");
      paramXmlSerializer.endTag(null, "null");
      return;
    } 
    paramXmlSerializer.startTag(null, "byte-array");
    if (paramString != null)
      paramXmlSerializer.attribute(null, "name", paramString); 
    int i = paramArrayOfbyte.length;
    paramXmlSerializer.attribute(null, "num", Integer.toString(i));
    paramXmlSerializer.text(HexEncoding.encodeToString(paramArrayOfbyte).toLowerCase());
    paramXmlSerializer.endTag(null, "byte-array");
  }
  
  public static final void writeIntArrayXml(int[] paramArrayOfint, String paramString, XmlSerializer paramXmlSerializer) throws XmlPullParserException, IOException {
    if (paramArrayOfint == null) {
      paramXmlSerializer.startTag(null, "null");
      paramXmlSerializer.endTag(null, "null");
      return;
    } 
    paramXmlSerializer.startTag(null, "int-array");
    if (paramString != null)
      paramXmlSerializer.attribute(null, "name", paramString); 
    int i = paramArrayOfint.length;
    paramXmlSerializer.attribute(null, "num", Integer.toString(i));
    for (byte b = 0; b < i; b++) {
      paramXmlSerializer.startTag(null, "item");
      paramXmlSerializer.attribute(null, "value", Integer.toString(paramArrayOfint[b]));
      paramXmlSerializer.endTag(null, "item");
    } 
    paramXmlSerializer.endTag(null, "int-array");
  }
  
  public static final void writeLongArrayXml(long[] paramArrayOflong, String paramString, XmlSerializer paramXmlSerializer) throws XmlPullParserException, IOException {
    if (paramArrayOflong == null) {
      paramXmlSerializer.startTag(null, "null");
      paramXmlSerializer.endTag(null, "null");
      return;
    } 
    paramXmlSerializer.startTag(null, "long-array");
    if (paramString != null)
      paramXmlSerializer.attribute(null, "name", paramString); 
    int i = paramArrayOflong.length;
    paramXmlSerializer.attribute(null, "num", Integer.toString(i));
    for (byte b = 0; b < i; b++) {
      paramXmlSerializer.startTag(null, "item");
      paramXmlSerializer.attribute(null, "value", Long.toString(paramArrayOflong[b]));
      paramXmlSerializer.endTag(null, "item");
    } 
    paramXmlSerializer.endTag(null, "long-array");
  }
  
  public static final void writeDoubleArrayXml(double[] paramArrayOfdouble, String paramString, XmlSerializer paramXmlSerializer) throws XmlPullParserException, IOException {
    if (paramArrayOfdouble == null) {
      paramXmlSerializer.startTag(null, "null");
      paramXmlSerializer.endTag(null, "null");
      return;
    } 
    paramXmlSerializer.startTag(null, "double-array");
    if (paramString != null)
      paramXmlSerializer.attribute(null, "name", paramString); 
    int i = paramArrayOfdouble.length;
    paramXmlSerializer.attribute(null, "num", Integer.toString(i));
    for (byte b = 0; b < i; b++) {
      paramXmlSerializer.startTag(null, "item");
      paramXmlSerializer.attribute(null, "value", Double.toString(paramArrayOfdouble[b]));
      paramXmlSerializer.endTag(null, "item");
    } 
    paramXmlSerializer.endTag(null, "double-array");
  }
  
  public static final void writeStringArrayXml(String[] paramArrayOfString, String paramString, XmlSerializer paramXmlSerializer) throws XmlPullParserException, IOException {
    if (paramArrayOfString == null) {
      paramXmlSerializer.startTag(null, "null");
      paramXmlSerializer.endTag(null, "null");
      return;
    } 
    paramXmlSerializer.startTag(null, "string-array");
    if (paramString != null)
      paramXmlSerializer.attribute(null, "name", paramString); 
    int i = paramArrayOfString.length;
    paramXmlSerializer.attribute(null, "num", Integer.toString(i));
    for (byte b = 0; b < i; b++) {
      paramXmlSerializer.startTag(null, "item");
      paramXmlSerializer.attribute(null, "value", paramArrayOfString[b]);
      paramXmlSerializer.endTag(null, "item");
    } 
    paramXmlSerializer.endTag(null, "string-array");
  }
  
  public static final void writeBooleanArrayXml(boolean[] paramArrayOfboolean, String paramString, XmlSerializer paramXmlSerializer) throws XmlPullParserException, IOException {
    if (paramArrayOfboolean == null) {
      paramXmlSerializer.startTag(null, "null");
      paramXmlSerializer.endTag(null, "null");
      return;
    } 
    paramXmlSerializer.startTag(null, "boolean-array");
    if (paramString != null)
      paramXmlSerializer.attribute(null, "name", paramString); 
    int i = paramArrayOfboolean.length;
    paramXmlSerializer.attribute(null, "num", Integer.toString(i));
    for (byte b = 0; b < i; b++) {
      paramXmlSerializer.startTag(null, "item");
      paramXmlSerializer.attribute(null, "value", Boolean.toString(paramArrayOfboolean[b]));
      paramXmlSerializer.endTag(null, "item");
    } 
    paramXmlSerializer.endTag(null, "boolean-array");
  }
  
  public static final void writeValueXml(Object paramObject, String paramString, XmlSerializer paramXmlSerializer) throws XmlPullParserException, IOException {
    writeValueXml(paramObject, paramString, paramXmlSerializer, null);
  }
  
  private static final void writeValueXml(Object paramObject, String paramString, XmlSerializer paramXmlSerializer, WriteMapCallback paramWriteMapCallback) throws XmlPullParserException, IOException {
    StringBuilder stringBuilder;
    String str;
    if (paramObject == null) {
      paramXmlSerializer.startTag(null, "null");
      if (paramString != null)
        paramXmlSerializer.attribute(null, "name", paramString); 
      paramXmlSerializer.endTag(null, "null");
      return;
    } 
    if (paramObject instanceof String) {
      paramXmlSerializer.startTag(null, "string");
      if (paramString != null)
        paramXmlSerializer.attribute(null, "name", paramString); 
      paramXmlSerializer.text(paramObject.toString());
      paramXmlSerializer.endTag(null, "string");
      return;
    } 
    if (paramObject instanceof Integer) {
      str = "int";
    } else if (paramObject instanceof Long) {
      str = "long";
    } else if (paramObject instanceof Float) {
      str = "float";
    } else if (paramObject instanceof Double) {
      str = "double";
    } else if (paramObject instanceof Boolean) {
      str = "boolean";
    } else {
      if (paramObject instanceof byte[]) {
        writeByteArrayXml((byte[])paramObject, paramString, paramXmlSerializer);
        return;
      } 
      if (paramObject instanceof int[]) {
        writeIntArrayXml((int[])paramObject, paramString, paramXmlSerializer);
        return;
      } 
      if (paramObject instanceof long[]) {
        writeLongArrayXml((long[])paramObject, paramString, paramXmlSerializer);
        return;
      } 
      if (paramObject instanceof double[]) {
        writeDoubleArrayXml((double[])paramObject, paramString, paramXmlSerializer);
        return;
      } 
      if (paramObject instanceof String[]) {
        writeStringArrayXml((String[])paramObject, paramString, paramXmlSerializer);
        return;
      } 
      if (paramObject instanceof boolean[]) {
        writeBooleanArrayXml((boolean[])paramObject, paramString, paramXmlSerializer);
        return;
      } 
      if (paramObject instanceof Map) {
        writeMapXml((Map)paramObject, paramString, paramXmlSerializer);
        return;
      } 
      if (paramObject instanceof List) {
        writeListXml((List)paramObject, paramString, paramXmlSerializer);
        return;
      } 
      if (paramObject instanceof Set) {
        writeSetXml((Set)paramObject, paramString, paramXmlSerializer);
        return;
      } 
      if (paramObject instanceof CharSequence) {
        paramXmlSerializer.startTag(null, "string");
        if (paramString != null)
          paramXmlSerializer.attribute(null, "name", paramString); 
        paramXmlSerializer.text(paramObject.toString());
        paramXmlSerializer.endTag(null, "string");
        return;
      } 
      if (str != null) {
        str.writeUnknownObject(paramObject, paramString, paramXmlSerializer);
        return;
      } 
      stringBuilder = new StringBuilder();
      stringBuilder.append("writeValueXml: unable to write value ");
      stringBuilder.append(paramObject);
      throw new RuntimeException(stringBuilder.toString());
    } 
    paramXmlSerializer.startTag(null, str);
    if (stringBuilder != null)
      paramXmlSerializer.attribute(null, "name", (String)stringBuilder); 
    paramXmlSerializer.attribute(null, "value", paramObject.toString());
    paramXmlSerializer.endTag(null, str);
  }
  
  public static final HashMap<String, ?> readMapXml(InputStream paramInputStream) throws XmlPullParserException, IOException {
    XmlPullParser xmlPullParser = Xml.newPullParser();
    xmlPullParser.setInput(paramInputStream, StandardCharsets.UTF_8.name());
    return (HashMap<String, ?>)readValueXml(xmlPullParser, new String[1]);
  }
  
  public static final ArrayList readListXml(InputStream paramInputStream) throws XmlPullParserException, IOException {
    XmlPullParser xmlPullParser = Xml.newPullParser();
    xmlPullParser.setInput(paramInputStream, StandardCharsets.UTF_8.name());
    return (ArrayList)readValueXml(xmlPullParser, new String[1]);
  }
  
  public static final HashSet readSetXml(InputStream paramInputStream) throws XmlPullParserException, IOException {
    XmlPullParser xmlPullParser = Xml.newPullParser();
    xmlPullParser.setInput(paramInputStream, null);
    return (HashSet)readValueXml(xmlPullParser, new String[1]);
  }
  
  public static final HashMap<String, ?> readThisMapXml(XmlPullParser paramXmlPullParser, String paramString, String[] paramArrayOfString) throws XmlPullParserException, IOException {
    return readThisMapXml(paramXmlPullParser, paramString, paramArrayOfString, null);
  }
  
  public static final HashMap<String, ?> readThisMapXml(XmlPullParser paramXmlPullParser, String paramString, String[] paramArrayOfString, ReadMapCallback paramReadMapCallback) throws XmlPullParserException, IOException {
    HashMap<Object, Object> hashMap = new HashMap<>();
    int i = paramXmlPullParser.getEventType();
    while (true) {
      if (i == 2) {
        Object object = readThisValueXml(paramXmlPullParser, paramArrayOfString, paramReadMapCallback, false);
        hashMap.put(paramArrayOfString[0], object);
      } else if (i == 3) {
        if (paramXmlPullParser.getName().equals(paramString))
          return (HashMap)hashMap; 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Expected ");
        stringBuilder1.append(paramString);
        stringBuilder1.append(" end tag at: ");
        stringBuilder1.append(paramXmlPullParser.getName());
        throw new XmlPullParserException(stringBuilder1.toString());
      } 
      i = paramXmlPullParser.next();
      if (i != 1)
        continue; 
      break;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Document ended before ");
    stringBuilder.append(paramString);
    stringBuilder.append(" end tag");
    throw new XmlPullParserException(stringBuilder.toString());
  }
  
  public static final ArrayMap<String, ?> readThisArrayMapXml(XmlPullParser paramXmlPullParser, String paramString, String[] paramArrayOfString, ReadMapCallback paramReadMapCallback) throws XmlPullParserException, IOException {
    ArrayMap<String, ?> arrayMap = new ArrayMap();
    int i = paramXmlPullParser.getEventType();
    while (true) {
      if (i == 2) {
        Object object = readThisValueXml(paramXmlPullParser, paramArrayOfString, paramReadMapCallback, true);
        arrayMap.put(paramArrayOfString[0], object);
      } else if (i == 3) {
        if (paramXmlPullParser.getName().equals(paramString))
          return arrayMap; 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Expected ");
        stringBuilder1.append(paramString);
        stringBuilder1.append(" end tag at: ");
        stringBuilder1.append(paramXmlPullParser.getName());
        throw new XmlPullParserException(stringBuilder1.toString());
      } 
      i = paramXmlPullParser.next();
      if (i != 1)
        continue; 
      break;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Document ended before ");
    stringBuilder.append(paramString);
    stringBuilder.append(" end tag");
    throw new XmlPullParserException(stringBuilder.toString());
  }
  
  public static final ArrayList readThisListXml(XmlPullParser paramXmlPullParser, String paramString, String[] paramArrayOfString) throws XmlPullParserException, IOException {
    return readThisListXml(paramXmlPullParser, paramString, paramArrayOfString, null, false);
  }
  
  private static final ArrayList readThisListXml(XmlPullParser paramXmlPullParser, String paramString, String[] paramArrayOfString, ReadMapCallback paramReadMapCallback, boolean paramBoolean) throws XmlPullParserException, IOException {
    ArrayList<Object> arrayList = new ArrayList();
    int i = paramXmlPullParser.getEventType();
    while (true) {
      if (i == 2) {
        Object object = readThisValueXml(paramXmlPullParser, paramArrayOfString, paramReadMapCallback, paramBoolean);
        arrayList.add(object);
      } else if (i == 3) {
        if (paramXmlPullParser.getName().equals(paramString))
          return arrayList; 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Expected ");
        stringBuilder1.append(paramString);
        stringBuilder1.append(" end tag at: ");
        stringBuilder1.append(paramXmlPullParser.getName());
        throw new XmlPullParserException(stringBuilder1.toString());
      } 
      i = paramXmlPullParser.next();
      if (i != 1)
        continue; 
      break;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Document ended before ");
    stringBuilder.append(paramString);
    stringBuilder.append(" end tag");
    throw new XmlPullParserException(stringBuilder.toString());
  }
  
  public static final HashSet readThisSetXml(XmlPullParser paramXmlPullParser, String paramString, String[] paramArrayOfString) throws XmlPullParserException, IOException {
    return readThisSetXml(paramXmlPullParser, paramString, paramArrayOfString, null, false);
  }
  
  private static final HashSet readThisSetXml(XmlPullParser paramXmlPullParser, String paramString, String[] paramArrayOfString, ReadMapCallback paramReadMapCallback, boolean paramBoolean) throws XmlPullParserException, IOException {
    HashSet<Object> hashSet = new HashSet();
    int i = paramXmlPullParser.getEventType();
    while (true) {
      if (i == 2) {
        Object object = readThisValueXml(paramXmlPullParser, paramArrayOfString, paramReadMapCallback, paramBoolean);
        hashSet.add(object);
      } else if (i == 3) {
        if (paramXmlPullParser.getName().equals(paramString))
          return hashSet; 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Expected ");
        stringBuilder1.append(paramString);
        stringBuilder1.append(" end tag at: ");
        stringBuilder1.append(paramXmlPullParser.getName());
        throw new XmlPullParserException(stringBuilder1.toString());
      } 
      i = paramXmlPullParser.next();
      if (i != 1)
        continue; 
      break;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Document ended before ");
    stringBuilder.append(paramString);
    stringBuilder.append(" end tag");
    throw new XmlPullParserException(stringBuilder.toString());
  }
  
  public static final byte[] readThisByteArrayXml(XmlPullParser paramXmlPullParser, String paramString, String[] paramArrayOfString) throws XmlPullParserException, IOException {
    try {
      int i = Integer.parseInt(paramXmlPullParser.getAttributeValue(null, "num"));
      byte[] arrayOfByte = new byte[0];
      int j = paramXmlPullParser.getEventType();
      while (true) {
        StringBuilder stringBuilder1;
        String str1, str2;
        if (j == 4) {
          byte[] arrayOfByte1 = arrayOfByte;
          if (i > 0) {
            str1 = paramXmlPullParser.getText();
            if (str1 != null && str1.length() == i * 2) {
              arrayOfByte1 = HexEncoding.decode(str1);
            } else {
              stringBuilder1 = new StringBuilder();
              stringBuilder1.append("Invalid value found in byte-array: ");
              stringBuilder1.append(str1);
              throw new XmlPullParserException(stringBuilder1.toString());
            } 
          } 
        } else {
          str2 = str1;
          if (j == 3) {
            if (stringBuilder1.getName().equals(paramString))
              return (byte[])str1; 
            StringBuilder stringBuilder2 = new StringBuilder();
            stringBuilder2.append("Expected ");
            stringBuilder2.append(paramString);
            stringBuilder2.append(" end tag at: ");
            stringBuilder2.append(stringBuilder1.getName());
            throw new XmlPullParserException(stringBuilder2.toString());
          } 
        } 
        j = stringBuilder1.next();
        if (j != 1) {
          str1 = str2;
          continue;
        } 
        break;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Document ended before ");
      stringBuilder.append(paramString);
      stringBuilder.append(" end tag");
      throw new XmlPullParserException(stringBuilder.toString());
    } catch (NullPointerException nullPointerException) {
      throw new XmlPullParserException("Need num attribute in byte-array");
    } catch (NumberFormatException numberFormatException) {
      throw new XmlPullParserException("Not a number in num attribute in byte-array");
    } 
  }
  
  public static final int[] readThisIntArrayXml(XmlPullParser paramXmlPullParser, String paramString, String[] paramArrayOfString) throws XmlPullParserException, IOException {
    try {
      StringBuilder stringBuilder2;
      int i = Integer.parseInt(paramXmlPullParser.getAttributeValue(null, "num"));
      paramXmlPullParser.next();
      int[] arrayOfInt = new int[i];
      i = 0;
      int j = paramXmlPullParser.getEventType();
      while (true) {
        int k;
        if (j == 2) {
          if (paramXmlPullParser.getName().equals("item")) {
            try {
              String str = paramXmlPullParser.getAttributeValue(null, "value");
              arrayOfInt[i] = Integer.parseInt(str);
              k = i;
            } catch (NullPointerException nullPointerException) {
              throw new XmlPullParserException("Need value attribute in item");
            } catch (NumberFormatException numberFormatException) {
              throw new XmlPullParserException("Not a number in value attribute in item");
            } 
          } else {
            stringBuilder2 = new StringBuilder();
            stringBuilder2.append("Expected item tag at: ");
            stringBuilder2.append(numberFormatException.getName());
            throw new XmlPullParserException(stringBuilder2.toString());
          } 
        } else {
          k = i;
          if (j == 3) {
            if (numberFormatException.getName().equals(stringBuilder2))
              return arrayOfInt; 
            if (numberFormatException.getName().equals("item")) {
              k = i + 1;
            } else {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Expected ");
              stringBuilder.append((String)stringBuilder2);
              stringBuilder.append(" end tag at: ");
              stringBuilder.append(numberFormatException.getName());
              throw new XmlPullParserException(stringBuilder.toString());
            } 
          } 
        } 
        j = numberFormatException.next();
        if (j != 1) {
          i = k;
          continue;
        } 
        break;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Document ended before ");
      stringBuilder1.append((String)stringBuilder2);
      stringBuilder1.append(" end tag");
      throw new XmlPullParserException(stringBuilder1.toString());
    } catch (NullPointerException nullPointerException) {
      throw new XmlPullParserException("Need num attribute in int-array");
    } catch (NumberFormatException numberFormatException) {
      throw new XmlPullParserException("Not a number in num attribute in int-array");
    } 
  }
  
  public static final long[] readThisLongArrayXml(XmlPullParser paramXmlPullParser, String paramString, String[] paramArrayOfString) throws XmlPullParserException, IOException {
    try {
      StringBuilder stringBuilder2;
      int i = Integer.parseInt(paramXmlPullParser.getAttributeValue(null, "num"));
      paramXmlPullParser.next();
      long[] arrayOfLong = new long[i];
      i = 0;
      int j = paramXmlPullParser.getEventType();
      while (true) {
        int k;
        if (j == 2) {
          if (paramXmlPullParser.getName().equals("item")) {
            try {
              arrayOfLong[i] = Long.parseLong(paramXmlPullParser.getAttributeValue(null, "value"));
              k = i;
            } catch (NullPointerException nullPointerException) {
              throw new XmlPullParserException("Need value attribute in item");
            } catch (NumberFormatException numberFormatException) {
              throw new XmlPullParserException("Not a number in value attribute in item");
            } 
          } else {
            stringBuilder2 = new StringBuilder();
            stringBuilder2.append("Expected item tag at: ");
            stringBuilder2.append(numberFormatException.getName());
            throw new XmlPullParserException(stringBuilder2.toString());
          } 
        } else {
          k = i;
          if (j == 3) {
            if (numberFormatException.getName().equals(stringBuilder2))
              return arrayOfLong; 
            if (numberFormatException.getName().equals("item")) {
              k = i + 1;
            } else {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Expected ");
              stringBuilder.append((String)stringBuilder2);
              stringBuilder.append(" end tag at: ");
              stringBuilder.append(numberFormatException.getName());
              throw new XmlPullParserException(stringBuilder.toString());
            } 
          } 
        } 
        j = numberFormatException.next();
        if (j != 1) {
          i = k;
          continue;
        } 
        break;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Document ended before ");
      stringBuilder1.append((String)stringBuilder2);
      stringBuilder1.append(" end tag");
      throw new XmlPullParserException(stringBuilder1.toString());
    } catch (NullPointerException nullPointerException) {
      throw new XmlPullParserException("Need num attribute in long-array");
    } catch (NumberFormatException numberFormatException) {
      throw new XmlPullParserException("Not a number in num attribute in long-array");
    } 
  }
  
  public static final double[] readThisDoubleArrayXml(XmlPullParser paramXmlPullParser, String paramString, String[] paramArrayOfString) throws XmlPullParserException, IOException {
    try {
      StringBuilder stringBuilder2;
      int i = Integer.parseInt(paramXmlPullParser.getAttributeValue(null, "num"));
      paramXmlPullParser.next();
      double[] arrayOfDouble = new double[i];
      i = 0;
      int j = paramXmlPullParser.getEventType();
      while (true) {
        int k;
        if (j == 2) {
          if (paramXmlPullParser.getName().equals("item")) {
            try {
              arrayOfDouble[i] = Double.parseDouble(paramXmlPullParser.getAttributeValue(null, "value"));
              k = i;
            } catch (NullPointerException nullPointerException) {
              throw new XmlPullParserException("Need value attribute in item");
            } catch (NumberFormatException numberFormatException) {
              throw new XmlPullParserException("Not a number in value attribute in item");
            } 
          } else {
            stringBuilder2 = new StringBuilder();
            stringBuilder2.append("Expected item tag at: ");
            stringBuilder2.append(numberFormatException.getName());
            throw new XmlPullParserException(stringBuilder2.toString());
          } 
        } else {
          k = i;
          if (j == 3) {
            if (numberFormatException.getName().equals(stringBuilder2))
              return arrayOfDouble; 
            if (numberFormatException.getName().equals("item")) {
              k = i + 1;
            } else {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Expected ");
              stringBuilder.append((String)stringBuilder2);
              stringBuilder.append(" end tag at: ");
              stringBuilder.append(numberFormatException.getName());
              throw new XmlPullParserException(stringBuilder.toString());
            } 
          } 
        } 
        j = numberFormatException.next();
        if (j != 1) {
          i = k;
          continue;
        } 
        break;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Document ended before ");
      stringBuilder1.append((String)stringBuilder2);
      stringBuilder1.append(" end tag");
      throw new XmlPullParserException(stringBuilder1.toString());
    } catch (NullPointerException nullPointerException) {
      throw new XmlPullParserException("Need num attribute in double-array");
    } catch (NumberFormatException numberFormatException) {
      throw new XmlPullParserException("Not a number in num attribute in double-array");
    } 
  }
  
  public static final String[] readThisStringArrayXml(XmlPullParser paramXmlPullParser, String paramString, String[] paramArrayOfString) throws XmlPullParserException, IOException {
    try {
      StringBuilder stringBuilder2;
      int i = Integer.parseInt(paramXmlPullParser.getAttributeValue(null, "num"));
      paramXmlPullParser.next();
      paramArrayOfString = new String[i];
      int j = 0;
      int k = paramXmlPullParser.getEventType();
      while (true) {
        if (k == 2) {
          if (paramXmlPullParser.getName().equals("item")) {
            try {
              paramArrayOfString[j] = paramXmlPullParser.getAttributeValue(null, "value");
              i = j;
            } catch (NullPointerException nullPointerException) {
              throw new XmlPullParserException("Need value attribute in item");
            } catch (NumberFormatException numberFormatException) {
              throw new XmlPullParserException("Not a number in value attribute in item");
            } 
          } else {
            stringBuilder2 = new StringBuilder();
            stringBuilder2.append("Expected item tag at: ");
            stringBuilder2.append(numberFormatException.getName());
            throw new XmlPullParserException(stringBuilder2.toString());
          } 
        } else {
          i = j;
          if (k == 3) {
            if (numberFormatException.getName().equals(stringBuilder2))
              return paramArrayOfString; 
            if (numberFormatException.getName().equals("item")) {
              i = j + 1;
            } else {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Expected ");
              stringBuilder.append((String)stringBuilder2);
              stringBuilder.append(" end tag at: ");
              stringBuilder.append(numberFormatException.getName());
              throw new XmlPullParserException(stringBuilder.toString());
            } 
          } 
        } 
        k = numberFormatException.next();
        if (k != 1) {
          j = i;
          continue;
        } 
        break;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Document ended before ");
      stringBuilder1.append((String)stringBuilder2);
      stringBuilder1.append(" end tag");
      throw new XmlPullParserException(stringBuilder1.toString());
    } catch (NullPointerException nullPointerException) {
      throw new XmlPullParserException("Need num attribute in string-array");
    } catch (NumberFormatException numberFormatException) {
      throw new XmlPullParserException("Not a number in num attribute in string-array");
    } 
  }
  
  public static final boolean[] readThisBooleanArrayXml(XmlPullParser paramXmlPullParser, String paramString, String[] paramArrayOfString) throws XmlPullParserException, IOException {
    try {
      StringBuilder stringBuilder2;
      int i = Integer.parseInt(paramXmlPullParser.getAttributeValue(null, "num"));
      paramXmlPullParser.next();
      boolean[] arrayOfBoolean = new boolean[i];
      int j = 0;
      int k = paramXmlPullParser.getEventType();
      while (true) {
        if (k == 2) {
          if (paramXmlPullParser.getName().equals("item")) {
            try {
              arrayOfBoolean[j] = Boolean.parseBoolean(paramXmlPullParser.getAttributeValue(null, "value"));
              i = j;
            } catch (NullPointerException nullPointerException) {
              throw new XmlPullParserException("Need value attribute in item");
            } catch (NumberFormatException numberFormatException) {
              throw new XmlPullParserException("Not a number in value attribute in item");
            } 
          } else {
            stringBuilder2 = new StringBuilder();
            stringBuilder2.append("Expected item tag at: ");
            stringBuilder2.append(numberFormatException.getName());
            throw new XmlPullParserException(stringBuilder2.toString());
          } 
        } else {
          i = j;
          if (k == 3) {
            if (numberFormatException.getName().equals(stringBuilder2))
              return arrayOfBoolean; 
            if (numberFormatException.getName().equals("item")) {
              i = j + 1;
            } else {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Expected ");
              stringBuilder.append((String)stringBuilder2);
              stringBuilder.append(" end tag at: ");
              stringBuilder.append(numberFormatException.getName());
              throw new XmlPullParserException(stringBuilder.toString());
            } 
          } 
        } 
        k = numberFormatException.next();
        if (k != 1) {
          j = i;
          continue;
        } 
        break;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Document ended before ");
      stringBuilder1.append((String)stringBuilder2);
      stringBuilder1.append(" end tag");
      throw new XmlPullParserException(stringBuilder1.toString());
    } catch (NullPointerException nullPointerException) {
      throw new XmlPullParserException("Need num attribute in string-array");
    } catch (NumberFormatException numberFormatException) {
      throw new XmlPullParserException("Not a number in num attribute in string-array");
    } 
  }
  
  public static final Object readValueXml(XmlPullParser paramXmlPullParser, String[] paramArrayOfString) throws XmlPullParserException, IOException {
    int i = paramXmlPullParser.getEventType();
    while (true) {
      if (i == 2)
        return readThisValueXml(paramXmlPullParser, paramArrayOfString, null, false); 
      if (i != 3) {
        if (i != 4) {
          i = paramXmlPullParser.next();
          if (i != 1)
            continue; 
          throw new XmlPullParserException("Unexpected end of document");
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Unexpected text: ");
        stringBuilder1.append(paramXmlPullParser.getText());
        throw new XmlPullParserException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unexpected end tag at: ");
      stringBuilder.append(paramXmlPullParser.getName());
      throw new XmlPullParserException(stringBuilder.toString());
    } 
  }
  
  private static final Object readThisValueXml(XmlPullParser paramXmlPullParser, String[] paramArrayOfString, ReadMapCallback paramReadMapCallback, boolean paramBoolean) throws XmlPullParserException, IOException {
    StringBuilder stringBuilder2;
    Object object;
    String str1 = paramXmlPullParser.getAttributeValue(null, "name");
    String str2 = paramXmlPullParser.getName();
    if (str2.equals("null")) {
      paramReadMapCallback = null;
    } else {
      if (str2.equals("string")) {
        object = "";
        while (true) {
          int i = paramXmlPullParser.next();
          if (i != 1) {
            if (i == 3) {
              if (paramXmlPullParser.getName().equals("string")) {
                paramArrayOfString[0] = str1;
                return object;
              } 
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Unexpected end tag in <string>: ");
              stringBuilder.append(paramXmlPullParser.getName());
              throw new XmlPullParserException(stringBuilder.toString());
            } 
            if (i == 4) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append((String)object);
              stringBuilder.append(paramXmlPullParser.getText());
              object = stringBuilder.toString();
              continue;
            } 
            if (i != 2)
              continue; 
            stringBuilder2 = new StringBuilder();
            stringBuilder2.append("Unexpected start tag in <string>: ");
            stringBuilder2.append(paramXmlPullParser.getName());
            throw new XmlPullParserException(stringBuilder2.toString());
          } 
          break;
        } 
        throw new XmlPullParserException("Unexpected end of document in <string>");
      } 
      Object object1 = readThisPrimitiveValueXml(paramXmlPullParser, str2);
      if (object1 != null) {
        object = object1;
      } else {
        byte[] arrayOfByte;
        int[] arrayOfInt;
        long[] arrayOfLong;
        double[] arrayOfDouble;
        String[] arrayOfString;
        boolean[] arrayOfBoolean;
        HashMap<String, ?> hashMap;
        ArrayList arrayList;
        HashSet hashSet;
        if (str2.equals("byte-array")) {
          arrayOfByte = readThisByteArrayXml(paramXmlPullParser, "byte-array", (String[])stringBuilder2);
          stringBuilder2[0] = (StringBuilder)str1;
          return arrayOfByte;
        } 
        if (str2.equals("int-array")) {
          arrayOfInt = readThisIntArrayXml((XmlPullParser)arrayOfByte, "int-array", (String[])stringBuilder2);
          stringBuilder2[0] = (StringBuilder)str1;
          return arrayOfInt;
        } 
        if (str2.equals("long-array")) {
          arrayOfLong = readThisLongArrayXml((XmlPullParser)arrayOfInt, "long-array", (String[])stringBuilder2);
          stringBuilder2[0] = (StringBuilder)str1;
          return arrayOfLong;
        } 
        if (str2.equals("double-array")) {
          arrayOfDouble = readThisDoubleArrayXml((XmlPullParser)arrayOfLong, "double-array", (String[])stringBuilder2);
          stringBuilder2[0] = (StringBuilder)str1;
          return arrayOfDouble;
        } 
        if (str2.equals("string-array")) {
          arrayOfString = readThisStringArrayXml((XmlPullParser)arrayOfDouble, "string-array", (String[])stringBuilder2);
          stringBuilder2[0] = (StringBuilder)str1;
          return arrayOfString;
        } 
        if (str2.equals("boolean-array")) {
          arrayOfBoolean = readThisBooleanArrayXml((XmlPullParser)arrayOfString, "boolean-array", (String[])stringBuilder2);
          stringBuilder2[0] = (StringBuilder)str1;
          return arrayOfBoolean;
        } 
        if (str2.equals("map")) {
          ArrayMap<String, ?> arrayMap;
          arrayOfBoolean.next();
          if (paramBoolean) {
            arrayMap = readThisArrayMapXml((XmlPullParser)arrayOfBoolean, "map", (String[])stringBuilder2, (ReadMapCallback)object);
          } else {
            hashMap = readThisMapXml((XmlPullParser)arrayMap, "map", (String[])stringBuilder2, (ReadMapCallback)object);
          } 
          stringBuilder2[0] = (StringBuilder)str1;
          return hashMap;
        } 
        if (str2.equals("list")) {
          hashMap.next();
          arrayList = readThisListXml((XmlPullParser)hashMap, "list", (String[])stringBuilder2, (ReadMapCallback)object, paramBoolean);
          stringBuilder2[0] = (StringBuilder)str1;
          return arrayList;
        } 
        if (str2.equals("set")) {
          arrayList.next();
          hashSet = readThisSetXml((XmlPullParser)arrayList, "set", (String[])stringBuilder2, (ReadMapCallback)object, paramBoolean);
          stringBuilder2[0] = (StringBuilder)str1;
          return hashSet;
        } 
        if (object != null) {
          Object object2 = object.readThisUnknownObjectXml((XmlPullParser)hashSet, str2);
          stringBuilder2[0] = (StringBuilder)str1;
          return object2;
        } 
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Unknown tag: ");
        stringBuilder1.append(str2);
        throw new XmlPullParserException(stringBuilder1.toString());
      } 
    } 
    while (true) {
      int i = stringBuilder1.next();
      if (i != 1) {
        if (i == 3) {
          if (stringBuilder1.getName().equals(str2)) {
            stringBuilder2[0] = (StringBuilder)str1;
            return object;
          } 
          stringBuilder2 = new StringBuilder();
          stringBuilder2.append("Unexpected end tag in <");
          stringBuilder2.append(str2);
          stringBuilder2.append(">: ");
          stringBuilder2.append(stringBuilder1.getName());
          throw new XmlPullParserException(stringBuilder2.toString());
        } 
        if (i != 4) {
          if (i != 2)
            continue; 
          stringBuilder2 = new StringBuilder();
          stringBuilder2.append("Unexpected start tag in <");
          stringBuilder2.append(str2);
          stringBuilder2.append(">: ");
          stringBuilder2.append(stringBuilder1.getName());
          throw new XmlPullParserException(stringBuilder2.toString());
        } 
        stringBuilder2 = new StringBuilder();
        stringBuilder2.append("Unexpected text in <");
        stringBuilder2.append(str2);
        stringBuilder2.append(">: ");
        stringBuilder2.append(stringBuilder1.getName());
        throw new XmlPullParserException(stringBuilder2.toString());
      } 
      break;
    } 
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("Unexpected end of document in <");
    stringBuilder1.append(str2);
    stringBuilder1.append(">");
    throw new XmlPullParserException(stringBuilder1.toString());
  }
  
  private static final Object readThisPrimitiveValueXml(XmlPullParser paramXmlPullParser, String paramString) throws XmlPullParserException, IOException {
    try {
      boolean bool = paramString.equals("int");
      if (bool)
        return Integer.valueOf(Integer.parseInt(paramXmlPullParser.getAttributeValue(null, "value"))); 
      if (paramString.equals("long"))
        return Long.valueOf(paramXmlPullParser.getAttributeValue(null, "value")); 
      if (paramString.equals("float"))
        return new Float(paramXmlPullParser.getAttributeValue(null, "value")); 
      if (paramString.equals("double"))
        return new Double(paramXmlPullParser.getAttributeValue(null, "value")); 
      if (paramString.equals("boolean"))
        return Boolean.valueOf(paramXmlPullParser.getAttributeValue(null, "value")); 
      return null;
    } catch (NullPointerException nullPointerException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Need value attribute in <");
      stringBuilder.append(paramString);
      stringBuilder.append(">");
      throw new XmlPullParserException(stringBuilder.toString());
    } catch (NumberFormatException numberFormatException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Not a number in value attribute in <");
      stringBuilder.append(paramString);
      stringBuilder.append(">");
      throw new XmlPullParserException(stringBuilder.toString());
    } 
  }
  
  public static final void beginDocument(XmlPullParser paramXmlPullParser, String paramString) throws XmlPullParserException, IOException {
    int i;
    while (true) {
      i = paramXmlPullParser.next();
      if (i != 2 && i != 1)
        continue; 
      break;
    } 
    if (i == 2) {
      if (paramXmlPullParser.getName().equals(paramString))
        return; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unexpected start tag: found ");
      stringBuilder.append(paramXmlPullParser.getName());
      stringBuilder.append(", expected ");
      stringBuilder.append(paramString);
      throw new XmlPullParserException(stringBuilder.toString());
    } 
    throw new XmlPullParserException("No start tag found");
  }
  
  public static final void nextElement(XmlPullParser paramXmlPullParser) throws XmlPullParserException, IOException {
    while (true) {
      int i = paramXmlPullParser.next();
      if (i != 2 && i != 1)
        continue; 
      break;
    } 
  }
  
  public static boolean nextElementWithin(XmlPullParser paramXmlPullParser, int paramInt) throws IOException, XmlPullParserException {
    while (true) {
      int i = paramXmlPullParser.next();
      if (i == 1 || (i == 3 && 
        paramXmlPullParser.getDepth() == paramInt))
        break; 
      if (i == 2 && 
        paramXmlPullParser.getDepth() == paramInt + 1)
        return true; 
    } 
    return false;
  }
  
  public static int readIntAttribute(XmlPullParser paramXmlPullParser, String paramString, int paramInt) {
    String str = paramXmlPullParser.getAttributeValue(null, paramString);
    if (TextUtils.isEmpty(str))
      return paramInt; 
    try {
      return Integer.parseInt(str);
    } catch (NumberFormatException numberFormatException) {
      return paramInt;
    } 
  }
  
  public static int readIntAttribute(XmlPullParser paramXmlPullParser, String paramString) throws IOException {
    String str = paramXmlPullParser.getAttributeValue(null, paramString);
    try {
      return Integer.parseInt(str);
    } catch (NumberFormatException numberFormatException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("problem parsing ");
      stringBuilder.append(paramString);
      stringBuilder.append("=");
      stringBuilder.append(str);
      stringBuilder.append(" as int");
      throw new ProtocolException(stringBuilder.toString());
    } 
  }
  
  public static void writeIntAttribute(XmlSerializer paramXmlSerializer, String paramString, int paramInt) throws IOException {
    paramXmlSerializer.attribute(null, paramString, Integer.toString(paramInt));
  }
  
  public static long readLongAttribute(XmlPullParser paramXmlPullParser, String paramString, long paramLong) {
    String str = paramXmlPullParser.getAttributeValue(null, paramString);
    if (TextUtils.isEmpty(str))
      return paramLong; 
    try {
      return Long.parseLong(str);
    } catch (NumberFormatException numberFormatException) {
      return paramLong;
    } 
  }
  
  public static long readLongAttribute(XmlPullParser paramXmlPullParser, String paramString) throws IOException {
    String str = paramXmlPullParser.getAttributeValue(null, paramString);
    try {
      return Long.parseLong(str);
    } catch (NumberFormatException numberFormatException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("problem parsing ");
      stringBuilder.append(paramString);
      stringBuilder.append("=");
      stringBuilder.append(str);
      stringBuilder.append(" as long");
      throw new ProtocolException(stringBuilder.toString());
    } 
  }
  
  public static void writeLongAttribute(XmlSerializer paramXmlSerializer, String paramString, long paramLong) throws IOException {
    paramXmlSerializer.attribute(null, paramString, Long.toString(paramLong));
  }
  
  public static float readFloatAttribute(XmlPullParser paramXmlPullParser, String paramString) throws IOException {
    String str = paramXmlPullParser.getAttributeValue(null, paramString);
    try {
      return Float.parseFloat(str);
    } catch (NumberFormatException numberFormatException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("problem parsing ");
      stringBuilder.append(paramString);
      stringBuilder.append("=");
      stringBuilder.append(str);
      stringBuilder.append(" as long");
      throw new ProtocolException(stringBuilder.toString());
    } 
  }
  
  public static void writeFloatAttribute(XmlSerializer paramXmlSerializer, String paramString, float paramFloat) throws IOException {
    paramXmlSerializer.attribute(null, paramString, Float.toString(paramFloat));
  }
  
  public static boolean readBooleanAttribute(XmlPullParser paramXmlPullParser, String paramString) {
    String str = paramXmlPullParser.getAttributeValue(null, paramString);
    return Boolean.parseBoolean(str);
  }
  
  public static boolean readBooleanAttribute(XmlPullParser paramXmlPullParser, String paramString, boolean paramBoolean) {
    String str = paramXmlPullParser.getAttributeValue(null, paramString);
    if (TextUtils.isEmpty(str))
      return paramBoolean; 
    return Boolean.parseBoolean(str);
  }
  
  public static void writeBooleanAttribute(XmlSerializer paramXmlSerializer, String paramString, boolean paramBoolean) throws IOException {
    paramXmlSerializer.attribute(null, paramString, Boolean.toString(paramBoolean));
  }
  
  public static Uri readUriAttribute(XmlPullParser paramXmlPullParser, String paramString) {
    Uri uri;
    XmlPullParser xmlPullParser = null;
    paramString = paramXmlPullParser.getAttributeValue(null, paramString);
    paramXmlPullParser = xmlPullParser;
    if (paramString != null)
      uri = Uri.parse(paramString); 
    return uri;
  }
  
  public static void writeUriAttribute(XmlSerializer paramXmlSerializer, String paramString, Uri paramUri) throws IOException {
    if (paramUri != null)
      paramXmlSerializer.attribute(null, paramString, paramUri.toString()); 
  }
  
  public static String readStringAttribute(XmlPullParser paramXmlPullParser, String paramString) {
    return paramXmlPullParser.getAttributeValue(null, paramString);
  }
  
  public static void writeStringAttribute(XmlSerializer paramXmlSerializer, String paramString, CharSequence paramCharSequence) throws IOException {
    if (paramCharSequence != null)
      paramXmlSerializer.attribute(null, paramString, paramCharSequence.toString()); 
  }
  
  public static byte[] readByteArrayAttribute(XmlPullParser paramXmlPullParser, String paramString) {
    String str = paramXmlPullParser.getAttributeValue(null, paramString);
    if (!TextUtils.isEmpty(str))
      return Base64.decode(str, 0); 
    return null;
  }
  
  public static void writeByteArrayAttribute(XmlSerializer paramXmlSerializer, String paramString, byte[] paramArrayOfbyte) throws IOException {
    if (paramArrayOfbyte != null)
      paramXmlSerializer.attribute(null, paramString, Base64.encodeToString(paramArrayOfbyte, 0)); 
  }
  
  public static Bitmap readBitmapAttribute(XmlPullParser paramXmlPullParser, String paramString) {
    byte[] arrayOfByte = readByteArrayAttribute(paramXmlPullParser, paramString);
    if (arrayOfByte != null)
      return BitmapFactory.decodeByteArray(arrayOfByte, 0, arrayOfByte.length); 
    return null;
  }
  
  @Deprecated
  public static void writeBitmapAttribute(XmlSerializer paramXmlSerializer, String paramString, Bitmap paramBitmap) throws IOException {
    if (paramBitmap != null) {
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      paramBitmap.compress(Bitmap.CompressFormat.PNG, 90, byteArrayOutputStream);
      writeByteArrayAttribute(paramXmlSerializer, paramString, byteArrayOutputStream.toByteArray());
    } 
  }
  
  public static interface ReadMapCallback {
    Object readThisUnknownObjectXml(XmlPullParser param1XmlPullParser, String param1String) throws XmlPullParserException, IOException;
  }
  
  public static interface WriteMapCallback {
    void writeUnknownObject(Object param1Object, String param1String, XmlSerializer param1XmlSerializer) throws XmlPullParserException, IOException;
  }
}
