package com.android.internal.util;

import java.io.PrintWriter;
import java.io.Writer;
import java.util.Arrays;

public class IndentingPrintWriter extends PrintWriter {
  private StringBuilder mIndentBuilder = new StringBuilder();
  
  private boolean mEmptyLine = true;
  
  private char[] mSingleChar = new char[1];
  
  private char[] mCurrentIndent;
  
  private int mCurrentLength;
  
  private final String mSingleIndent;
  
  private final int mWrapLength;
  
  public IndentingPrintWriter(Writer paramWriter, String paramString) {
    this(paramWriter, paramString, -1);
  }
  
  public IndentingPrintWriter(Writer paramWriter, String paramString, int paramInt) {
    super(paramWriter);
    this.mSingleIndent = paramString;
    this.mWrapLength = paramInt;
  }
  
  public IndentingPrintWriter setIndent(String paramString) {
    this.mIndentBuilder.setLength(0);
    this.mIndentBuilder.append(paramString);
    this.mCurrentIndent = null;
    return this;
  }
  
  public IndentingPrintWriter setIndent(int paramInt) {
    this.mIndentBuilder.setLength(0);
    for (byte b = 0; b < paramInt; b++)
      increaseIndent(); 
    return this;
  }
  
  public IndentingPrintWriter increaseIndent() {
    this.mIndentBuilder.append(this.mSingleIndent);
    this.mCurrentIndent = null;
    return this;
  }
  
  public IndentingPrintWriter decreaseIndent() {
    this.mIndentBuilder.delete(0, this.mSingleIndent.length());
    this.mCurrentIndent = null;
    return this;
  }
  
  public IndentingPrintWriter printPair(String paramString, Object paramObject) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append("=");
    stringBuilder.append(String.valueOf(paramObject));
    stringBuilder.append(" ");
    print(stringBuilder.toString());
    return this;
  }
  
  public IndentingPrintWriter printPair(String paramString, Object[] paramArrayOfObject) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append("=");
    stringBuilder.append(Arrays.toString(paramArrayOfObject));
    stringBuilder.append(" ");
    print(stringBuilder.toString());
    return this;
  }
  
  public IndentingPrintWriter printHexPair(String paramString, int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append("=0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    stringBuilder.append(" ");
    print(stringBuilder.toString());
    return this;
  }
  
  public void println() {
    write(10);
  }
  
  public void write(int paramInt) {
    char[] arrayOfChar = this.mSingleChar;
    arrayOfChar[0] = (char)paramInt;
    write(arrayOfChar, 0, 1);
  }
  
  public void write(String paramString, int paramInt1, int paramInt2) {
    char[] arrayOfChar = new char[paramInt2];
    paramString.getChars(paramInt1, paramInt2 - paramInt1, arrayOfChar, 0);
    write(arrayOfChar, 0, paramInt2);
  }
  
  public void write(char[] paramArrayOfchar, int paramInt1, int paramInt2) {
    int i = this.mIndentBuilder.length();
    int j = paramInt1;
    int k = paramInt1;
    while (k < paramInt1 + paramInt2) {
      int m = k + 1;
      char c = paramArrayOfchar[k];
      this.mCurrentLength++;
      k = j;
      if (c == '\n') {
        maybeWriteIndent();
        super.write(paramArrayOfchar, j, m - j);
        k = m;
        this.mEmptyLine = true;
        this.mCurrentLength = 0;
      } 
      int n = this.mWrapLength;
      j = k;
      if (n > 0) {
        j = k;
        if (this.mCurrentLength >= n - i)
          if (!this.mEmptyLine) {
            super.write(10);
            this.mEmptyLine = true;
            this.mCurrentLength = m - k;
            j = k;
          } else {
            maybeWriteIndent();
            super.write(paramArrayOfchar, k, m - k);
            super.write(10);
            this.mEmptyLine = true;
            j = m;
            this.mCurrentLength = 0;
          }  
      } 
      k = m;
    } 
    if (j != k) {
      maybeWriteIndent();
      super.write(paramArrayOfchar, j, k - j);
    } 
  }
  
  private void maybeWriteIndent() {
    if (this.mEmptyLine) {
      this.mEmptyLine = false;
      if (this.mIndentBuilder.length() != 0) {
        if (this.mCurrentIndent == null)
          this.mCurrentIndent = this.mIndentBuilder.toString().toCharArray(); 
        char[] arrayOfChar = this.mCurrentIndent;
        super.write(arrayOfChar, 0, arrayOfChar.length);
      } 
    } 
  }
}
