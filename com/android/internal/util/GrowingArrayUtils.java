package com.android.internal.util;

public final class GrowingArrayUtils {
  static final boolean $assertionsDisabled = false;
  
  public static <T> T[] append(T[] paramArrayOfT, int paramInt, T paramT) {
    // Byte code:
    //   0: aload_0
    //   1: astore_3
    //   2: iload_1
    //   3: iconst_1
    //   4: iadd
    //   5: aload_0
    //   6: arraylength
    //   7: if_icmple -> 39
    //   10: aload_0
    //   11: invokevirtual getClass : ()Ljava/lang/Class;
    //   14: invokevirtual getComponentType : ()Ljava/lang/Class;
    //   17: astore_3
    //   18: iload_1
    //   19: invokestatic growSize : (I)I
    //   22: istore #4
    //   24: aload_3
    //   25: iload #4
    //   27: invokestatic newUnpaddedArray : (Ljava/lang/Class;I)[Ljava/lang/Object;
    //   30: astore_3
    //   31: aload_0
    //   32: iconst_0
    //   33: aload_3
    //   34: iconst_0
    //   35: iload_1
    //   36: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   39: aload_3
    //   40: iload_1
    //   41: aload_2
    //   42: aastore
    //   43: aload_3
    //   44: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #44	-> 0
    //   #46	-> 0
    //   #48	-> 10
    //   #49	-> 10
    //   #48	-> 24
    //   #50	-> 31
    //   #51	-> 39
    //   #53	-> 39
    //   #54	-> 43
  }
  
  public static int[] append(int[] paramArrayOfint, int paramInt1, int paramInt2) {
    int[] arrayOfInt = paramArrayOfint;
    if (paramInt1 + 1 > paramArrayOfint.length) {
      arrayOfInt = ArrayUtils.newUnpaddedIntArray(growSize(paramInt1));
      System.arraycopy(paramArrayOfint, 0, arrayOfInt, 0, paramInt1);
    } 
    arrayOfInt[paramInt1] = paramInt2;
    return arrayOfInt;
  }
  
  public static long[] append(long[] paramArrayOflong, int paramInt, long paramLong) {
    long[] arrayOfLong = paramArrayOflong;
    if (paramInt + 1 > paramArrayOflong.length) {
      arrayOfLong = ArrayUtils.newUnpaddedLongArray(growSize(paramInt));
      System.arraycopy(paramArrayOflong, 0, arrayOfLong, 0, paramInt);
    } 
    arrayOfLong[paramInt] = paramLong;
    return arrayOfLong;
  }
  
  public static boolean[] append(boolean[] paramArrayOfboolean, int paramInt, boolean paramBoolean) {
    boolean[] arrayOfBoolean = paramArrayOfboolean;
    if (paramInt + 1 > paramArrayOfboolean.length) {
      arrayOfBoolean = ArrayUtils.newUnpaddedBooleanArray(growSize(paramInt));
      System.arraycopy(paramArrayOfboolean, 0, arrayOfBoolean, 0, paramInt);
    } 
    arrayOfBoolean[paramInt] = paramBoolean;
    return arrayOfBoolean;
  }
  
  public static float[] append(float[] paramArrayOffloat, int paramInt, float paramFloat) {
    float[] arrayOfFloat = paramArrayOffloat;
    if (paramInt + 1 > paramArrayOffloat.length) {
      arrayOfFloat = ArrayUtils.newUnpaddedFloatArray(growSize(paramInt));
      System.arraycopy(paramArrayOffloat, 0, arrayOfFloat, 0, paramInt);
    } 
    arrayOfFloat[paramInt] = paramFloat;
    return arrayOfFloat;
  }
  
  public static <T> T[] insert(T[] paramArrayOfT, int paramInt1, int paramInt2, T paramT) {
    // Byte code:
    //   0: iload_1
    //   1: iconst_1
    //   2: iadd
    //   3: aload_0
    //   4: arraylength
    //   5: if_icmpgt -> 26
    //   8: aload_0
    //   9: iload_2
    //   10: aload_0
    //   11: iload_2
    //   12: iconst_1
    //   13: iadd
    //   14: iload_1
    //   15: iload_2
    //   16: isub
    //   17: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   20: aload_0
    //   21: iload_2
    //   22: aload_3
    //   23: aastore
    //   24: aload_0
    //   25: areturn
    //   26: aload_0
    //   27: invokevirtual getClass : ()Ljava/lang/Class;
    //   30: invokevirtual getComponentType : ()Ljava/lang/Class;
    //   33: astore #4
    //   35: iload_1
    //   36: invokestatic growSize : (I)I
    //   39: istore_1
    //   40: aload #4
    //   42: iload_1
    //   43: invokestatic newUnpaddedArray : (Ljava/lang/Class;I)[Ljava/lang/Object;
    //   46: astore #4
    //   48: aload_0
    //   49: iconst_0
    //   50: aload #4
    //   52: iconst_0
    //   53: iload_2
    //   54: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   57: aload #4
    //   59: iload_2
    //   60: aload_3
    //   61: aastore
    //   62: aload_0
    //   63: iload_2
    //   64: aload #4
    //   66: iload_2
    //   67: iconst_1
    //   68: iadd
    //   69: aload_0
    //   70: arraylength
    //   71: iload_2
    //   72: isub
    //   73: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   76: aload #4
    //   78: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #130	-> 0
    //   #132	-> 0
    //   #133	-> 8
    //   #134	-> 20
    //   #135	-> 24
    //   #139	-> 26
    //   #140	-> 35
    //   #139	-> 40
    //   #141	-> 48
    //   #142	-> 57
    //   #143	-> 62
    //   #144	-> 76
  }
  
  public static int[] insert(int[] paramArrayOfint, int paramInt1, int paramInt2, int paramInt3) {
    if (paramInt1 + 1 <= paramArrayOfint.length) {
      System.arraycopy(paramArrayOfint, paramInt2, paramArrayOfint, paramInt2 + 1, paramInt1 - paramInt2);
      paramArrayOfint[paramInt2] = paramInt3;
      return paramArrayOfint;
    } 
    int[] arrayOfInt = ArrayUtils.newUnpaddedIntArray(growSize(paramInt1));
    System.arraycopy(paramArrayOfint, 0, arrayOfInt, 0, paramInt2);
    arrayOfInt[paramInt2] = paramInt3;
    System.arraycopy(paramArrayOfint, paramInt2, arrayOfInt, paramInt2 + 1, paramArrayOfint.length - paramInt2);
    return arrayOfInt;
  }
  
  public static long[] insert(long[] paramArrayOflong, int paramInt1, int paramInt2, long paramLong) {
    if (paramInt1 + 1 <= paramArrayOflong.length) {
      System.arraycopy(paramArrayOflong, paramInt2, paramArrayOflong, paramInt2 + 1, paramInt1 - paramInt2);
      paramArrayOflong[paramInt2] = paramLong;
      return paramArrayOflong;
    } 
    long[] arrayOfLong = ArrayUtils.newUnpaddedLongArray(growSize(paramInt1));
    System.arraycopy(paramArrayOflong, 0, arrayOfLong, 0, paramInt2);
    arrayOfLong[paramInt2] = paramLong;
    System.arraycopy(paramArrayOflong, paramInt2, arrayOfLong, paramInt2 + 1, paramArrayOflong.length - paramInt2);
    return arrayOfLong;
  }
  
  public static boolean[] insert(boolean[] paramArrayOfboolean, int paramInt1, int paramInt2, boolean paramBoolean) {
    if (paramInt1 + 1 <= paramArrayOfboolean.length) {
      System.arraycopy(paramArrayOfboolean, paramInt2, paramArrayOfboolean, paramInt2 + 1, paramInt1 - paramInt2);
      paramArrayOfboolean[paramInt2] = paramBoolean;
      return paramArrayOfboolean;
    } 
    boolean[] arrayOfBoolean = ArrayUtils.newUnpaddedBooleanArray(growSize(paramInt1));
    System.arraycopy(paramArrayOfboolean, 0, arrayOfBoolean, 0, paramInt2);
    arrayOfBoolean[paramInt2] = paramBoolean;
    System.arraycopy(paramArrayOfboolean, paramInt2, arrayOfBoolean, paramInt2 + 1, paramArrayOfboolean.length - paramInt2);
    return arrayOfBoolean;
  }
  
  public static int growSize(int paramInt) {
    if (paramInt <= 4) {
      paramInt = 8;
    } else {
      paramInt *= 2;
    } 
    return paramInt;
  }
}
