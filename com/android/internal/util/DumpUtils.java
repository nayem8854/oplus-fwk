package com.android.internal.util;

import android.app.AppOpsManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.Binder;
import android.os.Handler;
import android.text.TextUtils;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.function.Predicate;

public final class DumpUtils {
  public static final ComponentName[] CRITICAL_SECTION_COMPONENTS = new ComponentName[] { new ComponentName("com.android.systemui", "com.android.systemui.SystemUIService") };
  
  private static final boolean DEBUG = false;
  
  private static final String TAG = "DumpUtils";
  
  public static void dumpAsync(Handler paramHandler, final Dump dump, PrintWriter paramPrintWriter, final String prefix, long paramLong) {
    final StringWriter sw = new StringWriter();
    if (paramHandler.runWithScissors(new Runnable() {
          final DumpUtils.Dump val$dump;
          
          final String val$prefix;
          
          final StringWriter val$sw;
          
          public void run() {
            FastPrintWriter fastPrintWriter = new FastPrintWriter(sw);
            dump.dump(fastPrintWriter, prefix);
            fastPrintWriter.close();
          }
        }paramLong)) {
      paramPrintWriter.print(stringWriter.toString());
    } else {
      paramPrintWriter.println("... timed out");
    } 
  }
  
  private static void logMessage(PrintWriter paramPrintWriter, String paramString) {
    paramPrintWriter.println(paramString);
  }
  
  public static boolean checkDumpPermission(Context paramContext, String paramString, PrintWriter paramPrintWriter) {
    if (paramContext.checkCallingOrSelfPermission("android.permission.DUMP") != 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Permission Denial: can't dump ");
      stringBuilder.append(paramString);
      stringBuilder.append(" from from pid=");
      stringBuilder.append(Binder.getCallingPid());
      stringBuilder.append(", uid=");
      stringBuilder.append(Binder.getCallingUid());
      stringBuilder.append(" due to missing android.permission.DUMP permission");
      String str = stringBuilder.toString();
      logMessage(paramPrintWriter, str);
      return false;
    } 
    return true;
  }
  
  public static boolean checkUsageStatsPermission(Context paramContext, String paramString, PrintWriter paramPrintWriter) {
    int i = Binder.getCallingUid();
    if (i != 0 && i != 1000 && i != 1067 && i != 2000) {
      String str2;
      if (paramContext.checkCallingOrSelfPermission("android.permission.PACKAGE_USAGE_STATS") != 0) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Permission Denial: can't dump ");
        stringBuilder1.append(paramString);
        stringBuilder1.append(" from from pid=");
        stringBuilder1.append(Binder.getCallingPid());
        stringBuilder1.append(", uid=");
        stringBuilder1.append(Binder.getCallingUid());
        stringBuilder1.append(" due to missing android.permission.PACKAGE_USAGE_STATS permission");
        str2 = stringBuilder1.toString();
        logMessage(paramPrintWriter, str2);
        return false;
      } 
      AppOpsManager appOpsManager = (AppOpsManager)str2.getSystemService(AppOpsManager.class);
      String[] arrayOfString = str2.getPackageManager().getPackagesForUid(i);
      if (arrayOfString != null) {
        int j;
        byte b;
        for (j = arrayOfString.length, b = 0; b < j; ) {
          String str = arrayOfString[b];
          int k = appOpsManager.noteOpNoThrow(43, i, str);
          if (k != 0) {
            if (k != 3) {
              b++;
              continue;
            } 
            return true;
          } 
          return true;
        } 
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Permission Denial: can't dump ");
      stringBuilder.append(paramString);
      stringBuilder.append(" from from pid=");
      stringBuilder.append(Binder.getCallingPid());
      stringBuilder.append(", uid=");
      stringBuilder.append(Binder.getCallingUid());
      stringBuilder.append(" due to android:get_usage_stats app-op not allowed");
      String str1 = stringBuilder.toString();
      logMessage(paramPrintWriter, str1);
      return false;
    } 
    return true;
  }
  
  public static boolean checkDumpAndUsageStatsPermission(Context paramContext, String paramString, PrintWriter paramPrintWriter) {
    boolean bool;
    if (checkDumpPermission(paramContext, paramString, paramPrintWriter) && checkUsageStatsPermission(paramContext, paramString, paramPrintWriter)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isPlatformPackage(String paramString) {
    boolean bool;
    if (paramString != null && (
      paramString.equals("android") || 
      paramString.startsWith("android.") || 
      paramString.startsWith("com.android."))) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isPlatformPackage(ComponentName paramComponentName) {
    boolean bool;
    if (paramComponentName != null && isPlatformPackage(paramComponentName.getPackageName())) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isPlatformPackage(ComponentName.WithComponentName paramWithComponentName) {
    boolean bool;
    if (paramWithComponentName != null && isPlatformPackage(paramWithComponentName.getComponentName())) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isNonPlatformPackage(String paramString) {
    boolean bool;
    if (paramString != null && !isPlatformPackage(paramString)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isNonPlatformPackage(ComponentName paramComponentName) {
    boolean bool;
    if (paramComponentName != null && isNonPlatformPackage(paramComponentName.getPackageName())) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isNonPlatformPackage(ComponentName.WithComponentName paramWithComponentName) {
    boolean bool;
    if (paramWithComponentName != null && !isPlatformPackage(paramWithComponentName.getComponentName())) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static boolean isCriticalPackage(ComponentName paramComponentName) {
    if (paramComponentName == null)
      return false; 
    byte b = 0;
    while (true) {
      ComponentName[] arrayOfComponentName = CRITICAL_SECTION_COMPONENTS;
      if (b < arrayOfComponentName.length) {
        if (paramComponentName.equals(arrayOfComponentName[b]))
          return true; 
        b++;
        continue;
      } 
      break;
    } 
    return false;
  }
  
  public static boolean isPlatformCriticalPackage(ComponentName.WithComponentName paramWithComponentName) {
    boolean bool;
    if (paramWithComponentName != null && isPlatformPackage(paramWithComponentName.getComponentName()) && 
      isCriticalPackage(paramWithComponentName.getComponentName())) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isPlatformNonCriticalPackage(ComponentName.WithComponentName paramWithComponentName) {
    boolean bool;
    if (paramWithComponentName != null && isPlatformPackage(paramWithComponentName.getComponentName()) && 
      !isCriticalPackage(paramWithComponentName.getComponentName())) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static <TRec extends ComponentName.WithComponentName> Predicate<TRec> filterRecord(String paramString) {
    if (TextUtils.isEmpty(paramString))
      return (Predicate<TRec>)_$$Lambda$DumpUtils$D1OlZP6xIpu72ypnJd0fzx0wd6I.INSTANCE; 
    if ("all".equals(paramString))
      return (Predicate<TRec>)_$$Lambda$eRa1rlfDk6Og2yFeXGHqUGPzRF0.INSTANCE; 
    if ("all-platform".equals(paramString))
      return (Predicate<TRec>)_$$Lambda$kVylv1rl9MOSbHFZoVyK5dl1kfY.INSTANCE; 
    if ("all-non-platform".equals(paramString))
      return (Predicate<TRec>)_$$Lambda$JwOUSWW2_Jzu15y4Kn4JuPh8tWM.INSTANCE; 
    if ("all-platform-critical".equals(paramString))
      return (Predicate<TRec>)_$$Lambda$grRTg3idX3yJe9Zyx_tmLBiD1DM.INSTANCE; 
    if ("all-platform-non-critical".equals(paramString))
      return (Predicate<TRec>)_$$Lambda$TCbPpgWlKJUHZgFKCczglAvxLfw.INSTANCE; 
    ComponentName componentName = ComponentName.unflattenFromString(paramString);
    if (componentName != null)
      return new _$$Lambda$DumpUtils$X8irOs5hfloCKy89_l1HRA1QeG0(componentName); 
    int i = ParseUtils.parseIntWithBase(paramString, 16, -1);
    return new _$$Lambda$DumpUtils$vCLO_0ezRxkpSERUWCFrJ0ph5jg(i, paramString);
  }
  
  public static interface Dump {
    void dump(PrintWriter param1PrintWriter, String param1String);
  }
}
