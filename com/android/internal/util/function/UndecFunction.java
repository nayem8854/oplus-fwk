package com.android.internal.util.function;

public interface UndecFunction<A, B, C, D, E, F, G, H, I, J, K, R> {
  R apply(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH, I paramI, J paramJ, K paramK);
}
