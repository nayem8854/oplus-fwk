package com.android.internal.util.function;

public interface HeptConsumer<A, B, C, D, E, F, G> {
  void accept(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG);
}
