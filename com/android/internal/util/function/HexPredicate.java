package com.android.internal.util.function;

public interface HexPredicate<A, B, C, D, E, F> {
  boolean test(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF);
}
