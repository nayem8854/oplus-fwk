package com.android.internal.util.function;

public interface QuintFunction<A, B, C, D, E, R> {
  R apply(A paramA, B paramB, C paramC, D paramD, E paramE);
}
