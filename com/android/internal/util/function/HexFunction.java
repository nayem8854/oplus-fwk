package com.android.internal.util.function;

public interface HexFunction<A, B, C, D, E, F, R> {
  R apply(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF);
}
