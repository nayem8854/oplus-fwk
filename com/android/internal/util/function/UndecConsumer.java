package com.android.internal.util.function;

public interface UndecConsumer<A, B, C, D, E, F, G, H, I, J, K> {
  void accept(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH, I paramI, J paramJ, K paramK);
}
