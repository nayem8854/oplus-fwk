package com.android.internal.util.function;

public interface OctFunction<A, B, C, D, E, F, G, H, R> {
  R apply(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH);
}
