package com.android.internal.util.function;

public interface DecPredicate<A, B, C, D, E, F, G, H, I, J> {
  boolean test(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH, I paramI, J paramJ);
}
