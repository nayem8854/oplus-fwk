package com.android.internal.util.function;

public interface DecConsumer<A, B, C, D, E, F, G, H, I, J> {
  void accept(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH, I paramI, J paramJ);
}
