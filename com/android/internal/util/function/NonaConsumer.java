package com.android.internal.util.function;

public interface NonaConsumer<A, B, C, D, E, F, G, H, I> {
  void accept(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH, I paramI);
}
