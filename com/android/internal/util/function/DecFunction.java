package com.android.internal.util.function;

public interface DecFunction<A, B, C, D, E, F, G, H, I, J, R> {
  R apply(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH, I paramI, J paramJ);
}
