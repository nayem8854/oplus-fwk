package com.android.internal.util.function;

public interface TriPredicate<A, B, C> {
  boolean test(A paramA, B paramB, C paramC);
}
