package com.android.internal.util.function;

public interface QuintPredicate<A, B, C, D, E> {
  boolean test(A paramA, B paramB, C paramC, D paramD, E paramE);
}
