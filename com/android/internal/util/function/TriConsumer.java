package com.android.internal.util.function;

public interface TriConsumer<A, B, C> {
  void accept(A paramA, B paramB, C paramC);
}
