package com.android.internal.util.function;

public interface HeptFunction<A, B, C, D, E, F, G, R> {
  R apply(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG);
}
