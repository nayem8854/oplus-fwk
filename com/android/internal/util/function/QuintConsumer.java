package com.android.internal.util.function;

public interface QuintConsumer<A, B, C, D, E> {
  void accept(A paramA, B paramB, C paramC, D paramD, E paramE);
}
