package com.android.internal.util.function;

public interface HeptPredicate<A, B, C, D, E, F, G> {
  boolean test(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG);
}
