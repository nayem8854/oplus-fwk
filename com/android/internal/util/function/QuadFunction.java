package com.android.internal.util.function;

public interface QuadFunction<A, B, C, D, R> {
  R apply(A paramA, B paramB, C paramC, D paramD);
}
