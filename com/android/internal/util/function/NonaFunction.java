package com.android.internal.util.function;

public interface NonaFunction<A, B, C, D, E, F, G, H, I, R> {
  R apply(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH, I paramI);
}
