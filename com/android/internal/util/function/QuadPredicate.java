package com.android.internal.util.function;

public interface QuadPredicate<A, B, C, D> {
  boolean test(A paramA, B paramB, C paramC, D paramD);
}
