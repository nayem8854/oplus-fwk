package com.android.internal.util.function;

public interface HexConsumer<A, B, C, D, E, F> {
  void accept(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF);
}
