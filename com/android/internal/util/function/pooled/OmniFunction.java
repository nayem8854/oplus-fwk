package com.android.internal.util.function.pooled;

import com.android.internal.util.FunctionalUtils;
import com.android.internal.util.function.DecConsumer;
import com.android.internal.util.function.DecFunction;
import com.android.internal.util.function.HeptConsumer;
import com.android.internal.util.function.HeptFunction;
import com.android.internal.util.function.HexConsumer;
import com.android.internal.util.function.HexFunction;
import com.android.internal.util.function.NonaConsumer;
import com.android.internal.util.function.NonaFunction;
import com.android.internal.util.function.OctConsumer;
import com.android.internal.util.function.OctFunction;
import com.android.internal.util.function.QuadConsumer;
import com.android.internal.util.function.QuadFunction;
import com.android.internal.util.function.QuadPredicate;
import com.android.internal.util.function.QuintConsumer;
import com.android.internal.util.function.QuintFunction;
import com.android.internal.util.function.QuintPredicate;
import com.android.internal.util.function.TriConsumer;
import com.android.internal.util.function.TriFunction;
import com.android.internal.util.function.TriPredicate;
import com.android.internal.util.function.UndecConsumer;
import com.android.internal.util.function.UndecFunction;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;

abstract class OmniFunction<A, B, C, D, E, F, G, H, I, J, K, R> implements PooledFunction<A, R>, BiFunction<A, B, R>, TriFunction<A, B, C, R>, QuadFunction<A, B, C, D, R>, QuintFunction<A, B, C, D, E, R>, HexFunction<A, B, C, D, E, F, R>, HeptFunction<A, B, C, D, E, F, G, R>, OctFunction<A, B, C, D, E, F, G, H, R>, NonaFunction<A, B, C, D, E, F, G, H, I, R>, DecFunction<A, B, C, D, E, F, G, H, I, J, R>, UndecFunction<A, B, C, D, E, F, G, H, I, J, K, R>, PooledConsumer<A>, BiConsumer<A, B>, TriConsumer<A, B, C>, QuadConsumer<A, B, C, D>, QuintConsumer<A, B, C, D, E>, HexConsumer<A, B, C, D, E, F>, HeptConsumer<A, B, C, D, E, F, G>, OctConsumer<A, B, C, D, E, F, G, H>, NonaConsumer<A, B, C, D, E, F, G, H, I>, DecConsumer<A, B, C, D, E, F, G, H, I, J>, UndecConsumer<A, B, C, D, E, F, G, H, I, J, K>, PooledPredicate<A>, BiPredicate<A, B>, TriPredicate<A, B, C>, QuadPredicate<A, B, C, D>, QuintPredicate<A, B, C, D, E>, PooledSupplier<R>, PooledRunnable, FunctionalUtils.ThrowingRunnable, FunctionalUtils.ThrowingSupplier<R>, PooledSupplier.OfInt, PooledSupplier.OfLong, PooledSupplier.OfDouble {
  public R apply(A paramA, B paramB) {
    return invoke(paramA, paramB, null, null, null, null, null, null, null, null, null);
  }
  
  public R apply(A paramA) {
    return invoke(paramA, null, null, null, null, null, null, null, null, null, null);
  }
  
  public void accept(A paramA, B paramB) {
    invoke(paramA, paramB, null, null, null, null, null, null, null, null, null);
  }
  
  public void accept(A paramA) {
    invoke(paramA, null, null, null, null, null, null, null, null, null, null);
  }
  
  public void run() {
    invoke(null, null, null, null, null, null, null, null, null, null, null);
  }
  
  public R get() {
    return invoke(null, null, null, null, null, null, null, null, null, null, null);
  }
  
  public boolean test(A paramA, B paramB, C paramC, D paramD, E paramE) {
    return ((Boolean)invoke(paramA, paramB, paramC, paramD, paramE, null, null, null, null, null, null)).booleanValue();
  }
  
  public boolean test(A paramA, B paramB, C paramC, D paramD) {
    return ((Boolean)invoke(paramA, paramB, paramC, paramD, null, null, null, null, null, null, null)).booleanValue();
  }
  
  public boolean test(A paramA, B paramB, C paramC) {
    return ((Boolean)invoke(paramA, paramB, paramC, null, null, null, null, null, null, null, null)).booleanValue();
  }
  
  public boolean test(A paramA, B paramB) {
    return ((Boolean)invoke(paramA, paramB, null, null, null, null, null, null, null, null, null)).booleanValue();
  }
  
  public boolean test(A paramA) {
    return ((Boolean)invoke(paramA, null, null, null, null, null, null, null, null, null, null)).booleanValue();
  }
  
  public PooledRunnable asRunnable() {
    return this;
  }
  
  public PooledConsumer<A> asConsumer() {
    return this;
  }
  
  public R apply(A paramA, B paramB, C paramC) {
    return invoke(paramA, paramB, paramC, null, null, null, null, null, null, null, null);
  }
  
  public void accept(A paramA, B paramB, C paramC) {
    invoke(paramA, paramB, paramC, null, null, null, null, null, null, null, null);
  }
  
  public R apply(A paramA, B paramB, C paramC, D paramD) {
    return invoke(paramA, paramB, paramC, paramD, null, null, null, null, null, null, null);
  }
  
  public R apply(A paramA, B paramB, C paramC, D paramD, E paramE) {
    return invoke(paramA, paramB, paramC, paramD, paramE, null, null, null, null, null, null);
  }
  
  public R apply(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF) {
    return invoke(paramA, paramB, paramC, paramD, paramE, paramF, null, null, null, null, null);
  }
  
  public R apply(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG) {
    return invoke(paramA, paramB, paramC, paramD, paramE, paramF, paramG, null, null, null, null);
  }
  
  public R apply(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH) {
    return invoke(paramA, paramB, paramC, paramD, paramE, paramF, paramG, paramH, null, null, null);
  }
  
  public R apply(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH, I paramI) {
    return invoke(paramA, paramB, paramC, paramD, paramE, paramF, paramG, paramH, paramI, null, null);
  }
  
  public R apply(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH, I paramI, J paramJ) {
    return invoke(paramA, paramB, paramC, paramD, paramE, paramF, paramG, paramH, paramI, paramJ, null);
  }
  
  public R apply(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH, I paramI, J paramJ, K paramK) {
    return invoke(paramA, paramB, paramC, paramD, paramE, paramF, paramG, paramH, paramI, paramJ, paramK);
  }
  
  public void accept(A paramA, B paramB, C paramC, D paramD) {
    invoke(paramA, paramB, paramC, paramD, null, null, null, null, null, null, null);
  }
  
  public void accept(A paramA, B paramB, C paramC, D paramD, E paramE) {
    invoke(paramA, paramB, paramC, paramD, paramE, null, null, null, null, null, null);
  }
  
  public void accept(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF) {
    invoke(paramA, paramB, paramC, paramD, paramE, paramF, null, null, null, null, null);
  }
  
  public void accept(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG) {
    invoke(paramA, paramB, paramC, paramD, paramE, paramF, paramG, null, null, null, null);
  }
  
  public void accept(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH) {
    invoke(paramA, paramB, paramC, paramD, paramE, paramF, paramG, paramH, null, null, null);
  }
  
  public void accept(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH, I paramI) {
    invoke(paramA, paramB, paramC, paramD, paramE, paramF, paramG, paramH, paramI, null, null);
  }
  
  public void accept(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH, I paramI, J paramJ) {
    invoke(paramA, paramB, paramC, paramD, paramE, paramF, paramG, paramH, paramI, paramJ, null);
  }
  
  public void accept(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH, I paramI, J paramJ, K paramK) {
    invoke(paramA, paramB, paramC, paramD, paramE, paramF, paramG, paramH, paramI, paramJ, paramK);
  }
  
  public void runOrThrow() throws Exception {
    run();
  }
  
  public R getOrThrow() throws Exception {
    return get();
  }
  
  public abstract <V> OmniFunction<A, B, C, D, E, F, G, H, I, J, K, V> andThen(Function<? super R, ? extends V> paramFunction);
  
  abstract R invoke(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH, I paramI, J paramJ, K paramK);
  
  public abstract OmniFunction<A, B, C, D, E, F, G, H, I, J, K, R> negate();
  
  public abstract OmniFunction<A, B, C, D, E, F, G, H, I, J, K, R> recycleOnUse();
}
