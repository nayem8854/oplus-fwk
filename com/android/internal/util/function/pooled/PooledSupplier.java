package com.android.internal.util.function.pooled;

import com.android.internal.util.FunctionalUtils;
import java.util.function.DoubleSupplier;
import java.util.function.IntSupplier;
import java.util.function.LongSupplier;
import java.util.function.Supplier;

public interface PooledSupplier<T> extends PooledLambda, Supplier<T>, FunctionalUtils.ThrowingSupplier<T> {
  PooledRunnable asRunnable();
  
  PooledSupplier<T> recycleOnUse();
  
  public static interface OfInt extends IntSupplier, PooledLambda {
    OfInt recycleOnUse();
  }
  
  public static interface OfLong extends LongSupplier, PooledLambda {
    OfLong recycleOnUse();
  }
  
  public static interface OfDouble extends DoubleSupplier, PooledLambda {
    OfDouble recycleOnUse();
  }
}
