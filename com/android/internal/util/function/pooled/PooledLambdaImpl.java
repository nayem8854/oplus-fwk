package com.android.internal.util.function.pooled;

import android.os.Message;
import android.text.TextUtils;
import android.util.Pools;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.BitUtils;
import com.android.internal.util.FunctionalUtils;
import com.android.internal.util.function.DecConsumer;
import com.android.internal.util.function.DecFunction;
import com.android.internal.util.function.DecPredicate;
import com.android.internal.util.function.HeptConsumer;
import com.android.internal.util.function.HeptFunction;
import com.android.internal.util.function.HeptPredicate;
import com.android.internal.util.function.HexConsumer;
import com.android.internal.util.function.HexFunction;
import com.android.internal.util.function.HexPredicate;
import com.android.internal.util.function.NonaConsumer;
import com.android.internal.util.function.NonaFunction;
import com.android.internal.util.function.NonaPredicate;
import com.android.internal.util.function.OctConsumer;
import com.android.internal.util.function.OctFunction;
import com.android.internal.util.function.OctPredicate;
import com.android.internal.util.function.QuadConsumer;
import com.android.internal.util.function.QuadFunction;
import com.android.internal.util.function.QuadPredicate;
import com.android.internal.util.function.QuintConsumer;
import com.android.internal.util.function.QuintFunction;
import com.android.internal.util.function.QuintPredicate;
import com.android.internal.util.function.TriConsumer;
import com.android.internal.util.function.TriFunction;
import com.android.internal.util.function.TriPredicate;
import com.android.internal.util.function.UndecConsumer;
import com.android.internal.util.function.UndecFunction;
import com.android.internal.util.function.UndecPredicate;
import java.util.Arrays;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

final class PooledLambdaImpl<R> extends OmniFunction<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, R> {
  private static final boolean DEBUG = false;
  
  private static final int FLAG_ACQUIRED_FROM_MESSAGE_CALLBACKS_POOL = 8192;
  
  private static final int FLAG_RECYCLED = 2048;
  
  private static final int FLAG_RECYCLE_ON_USE = 4096;
  
  private static final String LOG_TAG = "PooledLambdaImpl";
  
  static final int MASK_EXPOSED_AS = 2080768;
  
  static final int MASK_FUNC_TYPE = 266338304;
  
  private static final int MAX_ARGS = 11;
  
  private static final int MAX_POOL_SIZE = 50;
  
  static final Pool sMessageCallbacksPool;
  
  static class Pool extends Pools.SynchronizedPool<PooledLambdaImpl> {
    public Pool(Object param1Object) {
      super(50, param1Object);
    }
  }
  
  static final Pool sPool = new Pool(new Object());
  
  static {
    sMessageCallbacksPool = new Pool(Message.sPoolSync);
  }
  
  Object[] mArgs = null;
  
  long mConstValue;
  
  int mFlags = 0;
  
  Object mFunc;
  
  public void recycle() {
    if (!isRecycled())
      doRecycle(); 
  }
  
  private void doRecycle() {
    Pool pool;
    if ((this.mFlags & 0x2000) != 0) {
      pool = sMessageCallbacksPool;
    } else {
      pool = sPool;
    } 
    this.mFunc = null;
    Object[] arrayOfObject = this.mArgs;
    if (arrayOfObject != null)
      Arrays.fill(arrayOfObject, (Object)null); 
    this.mFlags = 2048;
    this.mConstValue = 0L;
    pool.release(this);
  }
  
  R invoke(Object paramObject1, Object paramObject2, Object paramObject3, Object paramObject4, Object paramObject5, Object paramObject6, Object paramObject7, Object paramObject8, Object paramObject9, Object paramObject10, Object paramObject11) {
    checkNotRecycled();
    if (fillInArg(paramObject1) && fillInArg(paramObject2) && fillInArg(paramObject3) && fillInArg(paramObject4) && 
      fillInArg(paramObject5) && fillInArg(paramObject6) && fillInArg(paramObject7) && fillInArg(paramObject8) && 
      fillInArg(paramObject9) && fillInArg(paramObject10) && fillInArg(paramObject11)) {
      boolean bool = true;
    } else {
      boolean bool = false;
    } 
    int i = LambdaType.decodeArgCount(getFlags(266338304));
    if (i != 15)
      for (byte b = 0; b < i; ) {
        if (this.mArgs[b] != ArgumentPlaceholder.INSTANCE) {
          b++;
          continue;
        } 
        paramObject1 = new StringBuilder();
        paramObject1.append("Missing argument #");
        paramObject1.append(b);
        paramObject1.append(" among ");
        paramObject2 = this.mArgs;
        paramObject1.append(Arrays.toString((Object[])paramObject2));
        throw new IllegalStateException(paramObject1.toString());
      }  
    try {
      paramObject1 = doInvoke();
      return (R)paramObject1;
    } finally {
      if (!isRecycleOnUse()) {
        if (!isRecycled()) {
          i = ArrayUtils.size(this.mArgs);
          for (byte b = 0; b < i; b++)
            popArg(b); 
        } 
      } else {
        doRecycle();
      } 
    } 
  }
  
  private boolean fillInArg(Object paramObject) {
    int i = ArrayUtils.size(this.mArgs);
    for (byte b = 0; b < i; b++) {
      if (this.mArgs[b] == ArgumentPlaceholder.INSTANCE) {
        this.mArgs[b] = paramObject;
        this.mFlags = (int)(this.mFlags | BitUtils.bitAt(b));
        return true;
      } 
    } 
    if (paramObject == null || paramObject == ArgumentPlaceholder.INSTANCE)
      return false; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("No more arguments expected for provided arg ");
    stringBuilder.append(paramObject);
    stringBuilder.append(" among ");
    paramObject = this.mArgs;
    stringBuilder.append(Arrays.toString((Object[])paramObject));
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  private void checkNotRecycled() {
    if (!isRecycled())
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Instance is recycled: ");
    stringBuilder.append(this);
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  private R doInvoke() {
    int i = getFlags(266338304);
    int j = LambdaType.decodeArgCount(i);
    int k = LambdaType.decodeReturnType(i);
    if (j != 15) {
      StringBuilder stringBuilder;
      switch (j) {
        default:
          stringBuilder = new StringBuilder();
          stringBuilder.append("Unknown function type: ");
          stringBuilder.append(LambdaType.toString(i));
          throw new IllegalStateException(stringBuilder.toString());
        case 11:
          if (k != 1) {
            if (k != 2) {
              if (k == 3) {
                UndecFunction<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, R> undecFunction = (UndecFunction)this.mFunc;
                Object object1 = popArg(0), object2 = popArg(1);
                Object object3 = popArg(2), object4 = popArg(3), object5 = popArg(4), object6 = popArg(5);
                Object object7 = popArg(6), object8 = popArg(7), object9 = popArg(8), object10 = popArg(9), object11 = popArg(10);
                return undecFunction.apply(object1, object2, object3, object4, object5, object6, object7, object8, object9, object10, object11);
              } 
            } else {
              UndecPredicate<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object> undecPredicate = (UndecPredicate)this.mFunc;
              Object object3 = popArg(0);
              Object object2 = popArg(1), object1 = popArg(2), object7 = popArg(3), object10 = popArg(4);
              Object object9 = popArg(5), object4 = popArg(6), object6 = popArg(7), object5 = popArg(8), object11 = popArg(9), object8 = popArg(10);
              return (R)Boolean.valueOf(undecPredicate.test(object3, object2, object1, object7, object10, object9, object4, object6, object5, object11, object8));
            } 
          } else {
            UndecConsumer<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object> undecConsumer = (UndecConsumer)this.mFunc;
            Object object7 = popArg(0), object6 = popArg(1);
            Object object3 = popArg(2), object5 = popArg(3), object10 = popArg(4), object8 = popArg(5);
            Object object11 = popArg(6), object4 = popArg(7), object9 = popArg(8), object1 = popArg(9), object2 = popArg(10);
            undecConsumer.accept(object7, object6, object3, object5, object10, object8, object11, object4, object9, object1, object2);
            return null;
          } 
        case 10:
          if (k != 1) {
            if (k != 2) {
              if (k == 3) {
                DecFunction<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, R> decFunction = (DecFunction)this.mFunc;
                Object object1 = popArg(0), object9 = popArg(1);
                Object object5 = popArg(2), object10 = popArg(3), object2 = popArg(4), object6 = popArg(5);
                Object object4 = popArg(6), object3 = popArg(7), object8 = popArg(8), object7 = popArg(9);
                return decFunction.apply(object1, object9, object5, object10, object2, object6, object4, object3, object8, object7);
              } 
            } else {
              DecPredicate<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object> decPredicate = (DecPredicate)this.mFunc;
              Object object8 = popArg(0);
              Object object2 = popArg(1), object10 = popArg(2), object7 = popArg(3), object3 = popArg(4);
              Object object1 = popArg(5), object6 = popArg(6), object5 = popArg(7), object4 = popArg(8), object9 = popArg(9);
              return (R)Boolean.valueOf(decPredicate.test(object8, object2, object10, object7, object3, object1, object6, object5, object4, object9));
            } 
          } else {
            DecConsumer<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object> decConsumer = (DecConsumer)this.mFunc;
            Object object3 = popArg(0), object9 = popArg(1);
            Object object5 = popArg(2), object8 = popArg(3), object2 = popArg(4), object1 = popArg(5);
            Object object4 = popArg(6), object10 = popArg(7), object6 = popArg(8), object7 = popArg(9);
            decConsumer.accept(object3, object9, object5, object8, object2, object1, object4, object10, object6, object7);
            return null;
          } 
        case 9:
          if (k != 1) {
            if (k != 2) {
              if (k == 3) {
                NonaFunction<Object, Object, Object, Object, Object, Object, Object, Object, Object, R> nonaFunction = (NonaFunction)this.mFunc;
                Object object9 = popArg(0), object4 = popArg(1);
                Object object7 = popArg(2), object6 = popArg(3), object1 = popArg(4), object2 = popArg(5);
                Object object5 = popArg(6), object8 = popArg(7), object3 = popArg(8);
                return nonaFunction.apply(object9, object4, object7, object6, object1, object2, object5, object8, object3);
              } 
            } else {
              NonaPredicate<Object, Object, Object, Object, Object, Object, Object, Object, Object> nonaPredicate = (NonaPredicate)this.mFunc;
              Object object8 = popArg(0);
              Object object7 = popArg(1), object4 = popArg(2), object1 = popArg(3), object9 = popArg(4);
              Object object5 = popArg(5), object3 = popArg(6), object2 = popArg(7), object6 = popArg(8);
              return (R)Boolean.valueOf(nonaPredicate.test(object8, object7, object4, object1, object9, object5, object3, object2, object6));
            } 
          } else {
            NonaConsumer<Object, Object, Object, Object, Object, Object, Object, Object, Object> nonaConsumer = (NonaConsumer)this.mFunc;
            Object object8 = popArg(0), object9 = popArg(1);
            Object object3 = popArg(2), object6 = popArg(3), object1 = popArg(4), object7 = popArg(5);
            Object object4 = popArg(6), object2 = popArg(7), object5 = popArg(8);
            nonaConsumer.accept(object8, object9, object3, object6, object1, object7, object4, object2, object5);
            return null;
          } 
        case 8:
          if (k != 1) {
            if (k != 2) {
              if (k == 3) {
                OctFunction<Object, Object, Object, Object, Object, Object, Object, Object, R> octFunction = (OctFunction)this.mFunc;
                Object object5 = popArg(0), object1 = popArg(1);
                Object object2 = popArg(2), object8 = popArg(3), object7 = popArg(4);
                Object object4 = popArg(5), object6 = popArg(6), object3 = popArg(7);
                return octFunction.apply(object5, object1, object2, object8, object7, object4, object6, object3);
              } 
            } else {
              OctPredicate<Object, Object, Object, Object, Object, Object, Object, Object> octPredicate = (OctPredicate)this.mFunc;
              Object object4 = popArg(0);
              Object object8 = popArg(1), object5 = popArg(2), object2 = popArg(3);
              Object object6 = popArg(4), object7 = popArg(5), object1 = popArg(6), object3 = popArg(7);
              return (R)Boolean.valueOf(octPredicate.test(object4, object8, object5, object2, object6, object7, object1, object3));
            } 
          } else {
            OctConsumer<Object, Object, Object, Object, Object, Object, Object, Object> octConsumer = (OctConsumer)this.mFunc;
            Object object8 = popArg(0), object7 = popArg(1);
            Object object5 = popArg(2), object1 = popArg(3), object2 = popArg(4);
            Object object4 = popArg(5), object6 = popArg(6), object3 = popArg(7);
            octConsumer.accept(object8, object7, object5, object1, object2, object4, object6, object3);
            return null;
          } 
        case 7:
          if (k != 1) {
            if (k != 2) {
              if (k == 3) {
                HeptFunction<Object, Object, Object, Object, Object, Object, Object, R> heptFunction = (HeptFunction)this.mFunc;
                Object object1 = popArg(0), object4 = popArg(1);
                Object object6 = popArg(2), object5 = popArg(3), object3 = popArg(4);
                Object object2 = popArg(5), object7 = popArg(6);
                return heptFunction.apply(object1, object4, object6, object5, object3, object2, object7);
              } 
            } else {
              HeptPredicate<Object, Object, Object, Object, Object, Object, Object> heptPredicate = (HeptPredicate)this.mFunc;
              Object object4 = popArg(0);
              Object object7 = popArg(1), object3 = popArg(2), object1 = popArg(3);
              Object object6 = popArg(4), object2 = popArg(5), object5 = popArg(6);
              return (R)Boolean.valueOf(heptPredicate.test(object4, object7, object3, object1, object6, object2, object5));
            } 
          } else {
            HeptConsumer<Object, Object, Object, Object, Object, Object, Object> heptConsumer = (HeptConsumer)this.mFunc;
            Object object4 = popArg(0), object1 = popArg(1);
            Object object3 = popArg(2), object7 = popArg(3), object6 = popArg(4);
            Object object2 = popArg(5), object5 = popArg(6);
            heptConsumer.accept(object4, object1, object3, object7, object6, object2, object5);
            return null;
          } 
        case 6:
          if (k != 1) {
            if (k != 2) {
              if (k == 3) {
                HexFunction<Object, Object, Object, Object, Object, Object, R> hexFunction = (HexFunction)this.mFunc;
                Object object3 = popArg(0), object4 = popArg(1);
                Object object5 = popArg(2), object1 = popArg(3), object6 = popArg(4), object2 = popArg(5);
                return hexFunction.apply(object3, object4, object5, object1, object6, object2);
              } 
            } else {
              HexPredicate<Object, Object, Object, Object, Object, Object> hexPredicate = (HexPredicate)this.mFunc;
              Object object2 = popArg(0);
              Object object5 = popArg(1), object3 = popArg(2), object1 = popArg(3), object6 = popArg(4), object4 = popArg(5);
              return (R)Boolean.valueOf(hexPredicate.test(object2, object5, object3, object1, object6, object4));
            } 
          } else {
            HexConsumer<Object, Object, Object, Object, Object, Object> hexConsumer = (HexConsumer)this.mFunc;
            Object object5 = popArg(0), object1 = popArg(1);
            Object object4 = popArg(2), object3 = popArg(3), object6 = popArg(4), object2 = popArg(5);
            hexConsumer.accept(object5, object1, object4, object3, object6, object2);
            return null;
          } 
        case 5:
          if (k != 1) {
            if (k != 2) {
              if (k == 3) {
                QuintFunction<Object, Object, Object, Object, Object, R> quintFunction = (QuintFunction)this.mFunc;
                Object object5 = popArg(0), object4 = popArg(1), object3 = popArg(2), object1 = popArg(3), object2 = popArg(4);
                return quintFunction.apply(object5, object4, object3, object1, object2);
              } 
            } else {
              QuintPredicate<Object, Object, Object, Object, Object> quintPredicate = (QuintPredicate)this.mFunc;
              Object object4 = popArg(0), object3 = popArg(1), object2 = popArg(2), object1 = popArg(3), object5 = popArg(4);
              return (R)Boolean.valueOf(quintPredicate.test(object4, object3, object2, object1, object5));
            } 
          } else {
            QuintConsumer<Object, Object, Object, Object, Object> quintConsumer = (QuintConsumer)this.mFunc;
            Object object2 = popArg(0), object1 = popArg(1);
            Object object5 = popArg(2), object3 = popArg(3), object4 = popArg(4);
            quintConsumer.accept(object2, object1, object5, object3, object4);
            return null;
          } 
        case 4:
          if (k != 1) {
            if (k != 2) {
              if (k == 3) {
                QuadFunction<Object, Object, Object, Object, R> quadFunction = (QuadFunction)this.mFunc;
                Object object4 = popArg(0), object2 = popArg(1), object1 = popArg(2), object3 = popArg(3);
                return quadFunction.apply(object4, object2, object1, object3);
              } 
            } else {
              QuadPredicate<Object, Object, Object, Object> quadPredicate = (QuadPredicate)this.mFunc;
              Object object3 = popArg(0), object4 = popArg(1), object2 = popArg(2), object1 = popArg(3);
              return (R)Boolean.valueOf(quadPredicate.test(object3, object4, object2, object1));
            } 
          } else {
            ((QuadConsumer<Object, Object, Object, Object>)this.mFunc).accept(popArg(0), popArg(1), popArg(2), popArg(3));
            return null;
          } 
        case 3:
          if (k != 1) {
            if (k != 2) {
              if (k == 3)
                return ((TriFunction<Object, Object, Object, R>)this.mFunc).apply(popArg(0), popArg(1), popArg(2)); 
            } else {
              TriPredicate<Object, Object, Object> triPredicate = (TriPredicate)this.mFunc;
              Object object3 = popArg(0), object2 = popArg(1), object1 = popArg(2);
              return (R)Boolean.valueOf(triPredicate.test(object3, object2, object1));
            } 
          } else {
            ((TriConsumer<Object, Object, Object>)this.mFunc).accept(popArg(0), popArg(1), popArg(2));
            return null;
          } 
        case 2:
          if (k != 1) {
            if (k != 2) {
              if (k == 3)
                return ((BiFunction<Object, Object, R>)this.mFunc).apply(popArg(0), popArg(1)); 
            } else {
              return (R)Boolean.valueOf(((BiPredicate<Object, Object>)this.mFunc).test(popArg(0), popArg(1)));
            } 
          } else {
            ((BiConsumer<Object, Object>)this.mFunc).accept(popArg(0), popArg(1));
            return null;
          } 
        case 1:
          if (k != 1) {
            if (k != 2) {
              if (k == 3)
                return ((Function<Object, R>)this.mFunc).apply(popArg(0)); 
            } else {
              return (R)Boolean.valueOf(((Predicate<Object>)this.mFunc).test(popArg(0)));
            } 
          } else {
            ((Consumer<Object>)this.mFunc).accept(popArg(0));
            return null;
          } 
        case 0:
          break;
      } 
      if (k != 1)
        if (k == 2 || k == 3)
          return ((Supplier<R>)this.mFunc).get();  
      ((Runnable)this.mFunc).run();
      return null;
    } 
    if (k != 4) {
      if (k != 5) {
        if (k != 6)
          return (R)this.mFunc; 
        return (R)Double.valueOf(getAsDouble());
      } 
      return (R)Long.valueOf(getAsLong());
    } 
    return (R)Integer.valueOf(getAsInt());
  }
  
  private boolean isConstSupplier() {
    boolean bool;
    if (LambdaType.decodeArgCount(getFlags(266338304)) == 15) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private Object popArg(int paramInt) {
    Object object = this.mArgs[paramInt];
    if (isInvocationArgAtIndex(paramInt)) {
      this.mArgs[paramInt] = ArgumentPlaceholder.INSTANCE;
      this.mFlags = (int)(this.mFlags & (BitUtils.bitAt(paramInt) ^ 0xFFFFFFFFFFFFFFFFL));
    } 
    return object;
  }
  
  public String toString() {
    if (isRecycled()) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("<recycled PooledLambda@");
      stringBuilder1.append(hashCodeHex(this));
      stringBuilder1.append(">");
      return stringBuilder1.toString();
    } 
    StringBuilder stringBuilder = new StringBuilder();
    if (isConstSupplier()) {
      stringBuilder.append(getFuncTypeAsString());
      stringBuilder.append("(");
      stringBuilder.append(doInvoke());
      stringBuilder.append(")");
    } else {
      Object object = this.mFunc;
      if (object instanceof PooledLambdaImpl) {
        stringBuilder.append(object);
      } else {
        stringBuilder.append(getFuncTypeAsString());
        stringBuilder.append("@");
        stringBuilder.append(hashCodeHex(object));
      } 
      stringBuilder.append("(");
      object = this.mArgs;
      int i = LambdaType.decodeArgCount(getFlags(266338304));
      stringBuilder.append(commaSeparateFirstN((Object[])object, i));
      stringBuilder.append(")");
    } 
    return stringBuilder.toString();
  }
  
  private String commaSeparateFirstN(Object[] paramArrayOfObject, int paramInt) {
    if (paramArrayOfObject == null)
      return ""; 
    return TextUtils.join(",", Arrays.copyOf(paramArrayOfObject, paramInt));
  }
  
  private static String hashCodeHex(Object paramObject) {
    if (paramObject == null)
      return ""; 
    return Integer.toHexString(Objects.hashCode(paramObject));
  }
  
  private String getFuncTypeAsString() {
    if (isRecycled())
      return "<recycled>"; 
    if (isConstSupplier())
      return "supplier"; 
    String str = LambdaType.toString(getFlags(2080768));
    if (str.endsWith("Consumer"))
      return "consumer"; 
    if (str.endsWith("Function"))
      return "function"; 
    if (str.endsWith("Predicate"))
      return "predicate"; 
    if (str.endsWith("Supplier"))
      return "supplier"; 
    if (str.endsWith("Runnable"))
      return "runnable"; 
    return str;
  }
  
  static <E extends PooledLambda> E acquire(Pool paramPool, Object paramObject1, int paramInt1, int paramInt2, int paramInt3, Object paramObject2, Object paramObject3, Object paramObject4, Object paramObject5, Object paramObject6, Object paramObject7, Object paramObject8, Object paramObject9, Object paramObject10, Object paramObject11, Object paramObject12) {
    PooledLambdaImpl pooledLambdaImpl = acquire(paramPool);
    Objects.requireNonNull(paramObject1);
    pooledLambdaImpl.mFunc = paramObject1;
    pooledLambdaImpl.setFlags(266338304, LambdaType.encode(paramInt1, paramInt3));
    pooledLambdaImpl.setFlags(2080768, LambdaType.encode(paramInt2, paramInt3));
    if (ArrayUtils.size(pooledLambdaImpl.mArgs) < paramInt1)
      pooledLambdaImpl.mArgs = new Object[paramInt1]; 
    setIfInBounds(pooledLambdaImpl.mArgs, 0, paramObject2);
    setIfInBounds(pooledLambdaImpl.mArgs, 1, paramObject3);
    setIfInBounds(pooledLambdaImpl.mArgs, 2, paramObject4);
    setIfInBounds(pooledLambdaImpl.mArgs, 3, paramObject5);
    setIfInBounds(pooledLambdaImpl.mArgs, 4, paramObject6);
    setIfInBounds(pooledLambdaImpl.mArgs, 5, paramObject7);
    setIfInBounds(pooledLambdaImpl.mArgs, 6, paramObject8);
    setIfInBounds(pooledLambdaImpl.mArgs, 7, paramObject9);
    setIfInBounds(pooledLambdaImpl.mArgs, 8, paramObject10);
    setIfInBounds(pooledLambdaImpl.mArgs, 9, paramObject11);
    setIfInBounds(pooledLambdaImpl.mArgs, 10, paramObject12);
    return (E)pooledLambdaImpl;
  }
  
  static PooledLambdaImpl acquireConstSupplier(int paramInt) {
    PooledLambdaImpl pooledLambdaImpl = acquire(sPool);
    paramInt = LambdaType.encode(15, paramInt);
    pooledLambdaImpl.setFlags(266338304, paramInt);
    pooledLambdaImpl.setFlags(2080768, paramInt);
    return pooledLambdaImpl;
  }
  
  static PooledLambdaImpl acquire(Pool paramPool) {
    boolean bool;
    PooledLambdaImpl pooledLambdaImpl1 = (PooledLambdaImpl)paramPool.acquire();
    PooledLambdaImpl pooledLambdaImpl2 = pooledLambdaImpl1;
    if (pooledLambdaImpl1 == null)
      pooledLambdaImpl2 = new PooledLambdaImpl(); 
    pooledLambdaImpl2.mFlags &= 0xFFFFF7FF;
    if (paramPool == sMessageCallbacksPool) {
      bool = true;
    } else {
      bool = false;
    } 
    pooledLambdaImpl2.setFlags(8192, bool);
    return pooledLambdaImpl2;
  }
  
  private static void setIfInBounds(Object[] paramArrayOfObject, int paramInt, Object paramObject) {
    if (paramInt < ArrayUtils.size(paramArrayOfObject))
      paramArrayOfObject[paramInt] = paramObject; 
  }
  
  public OmniFunction<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, R> negate() {
    throw new UnsupportedOperationException();
  }
  
  public <V> OmniFunction<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, V> andThen(Function<? super R, ? extends V> paramFunction) {
    throw new UnsupportedOperationException();
  }
  
  public double getAsDouble() {
    return Double.longBitsToDouble(this.mConstValue);
  }
  
  public int getAsInt() {
    return (int)this.mConstValue;
  }
  
  public long getAsLong() {
    return this.mConstValue;
  }
  
  public OmniFunction<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, R> recycleOnUse() {
    this.mFlags |= 0x1000;
    return this;
  }
  
  public String getTraceName() {
    return FunctionalUtils.getLambdaName(this.mFunc);
  }
  
  private boolean isRecycled() {
    boolean bool;
    if ((this.mFlags & 0x800) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isRecycleOnUse() {
    boolean bool;
    if ((this.mFlags & 0x1000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isInvocationArgAtIndex(int paramInt) {
    int i = this.mFlags;
    boolean bool = true;
    if ((i & 1 << paramInt) == 0)
      bool = false; 
    return bool;
  }
  
  int getFlags(int paramInt) {
    return unmask(paramInt, this.mFlags);
  }
  
  void setFlags(int paramInt1, int paramInt2) {
    int i = this.mFlags & (paramInt1 ^ 0xFFFFFFFF);
    this.mFlags = i | mask(paramInt1, paramInt2);
  }
  
  private static int mask(int paramInt1, int paramInt2) {
    return paramInt2 << Integer.numberOfTrailingZeros(paramInt1) & paramInt1;
  }
  
  private static int unmask(int paramInt1, int paramInt2) {
    return (paramInt2 & paramInt1) / (1 << Integer.numberOfTrailingZeros(paramInt1));
  }
  
  class LambdaType {
    public static final int MASK = 127;
    
    public static final int MASK_ARG_COUNT = 15;
    
    public static final int MASK_BIT_COUNT = 7;
    
    public static final int MASK_RETURN_TYPE = 112;
    
    static int encode(int param1Int1, int param1Int2) {
      return PooledLambdaImpl.mask(15, param1Int1) | PooledLambdaImpl.mask(112, param1Int2);
    }
    
    static int decodeArgCount(int param1Int) {
      return param1Int & 0xF;
    }
    
    static int decodeReturnType(int param1Int) {
      return PooledLambdaImpl.unmask(112, param1Int);
    }
    
    static String toString(int param1Int) {
      int i = decodeArgCount(param1Int);
      param1Int = decodeReturnType(param1Int);
      if (i == 0) {
        if (param1Int == 1)
          return "Runnable"; 
        if (param1Int == 3 || param1Int == 2)
          return "Supplier"; 
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(argCountPrefix(i));
      stringBuilder.append(ReturnType.lambdaSuffix(param1Int));
      return stringBuilder.toString();
    }
    
    private static String argCountPrefix(int param1Int) {
      if (param1Int != 15) {
        StringBuilder stringBuilder;
        switch (param1Int) {
          default:
            stringBuilder = new StringBuilder();
            stringBuilder.append("");
            stringBuilder.append(param1Int);
            stringBuilder.append("arg");
            return stringBuilder.toString();
          case 11:
            return "Undec";
          case 10:
            return "Dec";
          case 9:
            return "Nona";
          case 8:
            return "Oct";
          case 7:
            return "Hept";
          case 6:
            return "Hex";
          case 5:
            return "Quint";
          case 4:
            return "Quad";
          case 3:
            return "Tri";
          case 2:
            return "Bi";
          case 1:
            return "";
          case 0:
            break;
        } 
        return "";
      } 
      return "";
    }
    
    static class ReturnType {
      public static final int BOOLEAN = 2;
      
      public static final int DOUBLE = 6;
      
      public static final int INT = 4;
      
      public static final int LONG = 5;
      
      public static final int OBJECT = 3;
      
      public static final int VOID = 1;
      
      static String toString(int param2Int) {
        StringBuilder stringBuilder;
        switch (param2Int) {
          default:
            stringBuilder = new StringBuilder();
            stringBuilder.append("");
            stringBuilder.append(param2Int);
            return stringBuilder.toString();
          case 6:
            return "DOUBLE";
          case 5:
            return "LONG";
          case 4:
            return "INT";
          case 3:
            return "OBJECT";
          case 2:
            return "BOOLEAN";
          case 1:
            break;
        } 
        return "VOID";
      }
      
      static String lambdaSuffix(int param2Int) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(prefix(param2Int));
        stringBuilder.append(suffix(param2Int));
        return stringBuilder.toString();
      }
      
      private static String prefix(int param2Int) {
        if (param2Int != 4) {
          if (param2Int != 5) {
            if (param2Int != 6)
              return ""; 
            return "Double";
          } 
          return "Long";
        } 
        return "Int";
      }
      
      private static String suffix(int param2Int) {
        if (param2Int != 1) {
          if (param2Int != 2) {
            if (param2Int != 3)
              return "Supplier"; 
            return "Function";
          } 
          return "Predicate";
        } 
        return "Consumer";
      }
    }
  }
}
