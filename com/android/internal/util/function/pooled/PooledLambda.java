package com.android.internal.util.function.pooled;

import android.os.Message;
import com.android.internal.util.function.DecConsumer;
import com.android.internal.util.function.DecFunction;
import com.android.internal.util.function.HeptConsumer;
import com.android.internal.util.function.HeptFunction;
import com.android.internal.util.function.HexConsumer;
import com.android.internal.util.function.HexFunction;
import com.android.internal.util.function.NonaConsumer;
import com.android.internal.util.function.NonaFunction;
import com.android.internal.util.function.OctConsumer;
import com.android.internal.util.function.OctFunction;
import com.android.internal.util.function.QuadConsumer;
import com.android.internal.util.function.QuadFunction;
import com.android.internal.util.function.QuadPredicate;
import com.android.internal.util.function.QuintConsumer;
import com.android.internal.util.function.QuintFunction;
import com.android.internal.util.function.QuintPredicate;
import com.android.internal.util.function.TriConsumer;
import com.android.internal.util.function.TriFunction;
import com.android.internal.util.function.TriPredicate;
import com.android.internal.util.function.UndecConsumer;
import com.android.internal.util.function.UndecFunction;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public interface PooledLambda {
  static <R> ArgumentPlaceholder<R> __() {
    return (ArgumentPlaceholder)ArgumentPlaceholder.INSTANCE;
  }
  
  static <R> ArgumentPlaceholder<R> __(Class<R> paramClass) {
    return __();
  }
  
  static <R> PooledSupplier<R> obtainSupplier(R paramR) {
    PooledLambdaImpl<R> pooledLambdaImpl = PooledLambdaImpl.acquireConstSupplier(3);
    pooledLambdaImpl.mFunc = paramR;
    return pooledLambdaImpl;
  }
  
  static PooledSupplier.OfInt obtainSupplier(int paramInt) {
    PooledLambdaImpl pooledLambdaImpl = PooledLambdaImpl.acquireConstSupplier(4);
    pooledLambdaImpl.mConstValue = paramInt;
    return pooledLambdaImpl;
  }
  
  static PooledSupplier.OfLong obtainSupplier(long paramLong) {
    PooledLambdaImpl pooledLambdaImpl = PooledLambdaImpl.acquireConstSupplier(5);
    pooledLambdaImpl.mConstValue = paramLong;
    return pooledLambdaImpl;
  }
  
  static PooledSupplier.OfDouble obtainSupplier(double paramDouble) {
    PooledLambdaImpl pooledLambdaImpl = PooledLambdaImpl.acquireConstSupplier(6);
    pooledLambdaImpl.mConstValue = Double.doubleToRawLongBits(paramDouble);
    return pooledLambdaImpl;
  }
  
  static <A> PooledRunnable obtainRunnable(Consumer<? super A> paramConsumer, A paramA) {
    return PooledLambdaImpl.<PooledRunnable>acquire(PooledLambdaImpl.sPool, paramConsumer, 1, 0, 1, paramA, null, null, null, null, null, null, null, null, null, null);
  }
  
  static <A> PooledSupplier<Boolean> obtainSupplier(Predicate<? super A> paramPredicate, A paramA) {
    return PooledLambdaImpl.<PooledSupplier<Boolean>>acquire(PooledLambdaImpl.sPool, paramPredicate, 1, 0, 2, paramA, null, null, null, null, null, null, null, null, null, null);
  }
  
  static <A, R> PooledSupplier<R> obtainSupplier(Function<? super A, ? extends R> paramFunction, A paramA) {
    return PooledLambdaImpl.<PooledSupplier<R>>acquire(PooledLambdaImpl.sPool, paramFunction, 1, 0, 3, paramA, null, null, null, null, null, null, null, null, null, null);
  }
  
  static <A> Message obtainMessage(Consumer<? super A> paramConsumer, A paramA) {
    synchronized (Message.sPoolSync) {
      PooledRunnable pooledRunnable = PooledLambdaImpl.<PooledRunnable>acquire(PooledLambdaImpl.sMessageCallbacksPool, paramConsumer, 1, 0, 1, paramA, null, null, null, null, null, null, null, null, null, null);
      return Message.obtain().setCallback(pooledRunnable.recycleOnUse());
    } 
  }
  
  static <A, B> PooledRunnable obtainRunnable(BiConsumer<? super A, ? super B> paramBiConsumer, A paramA, B paramB) {
    return PooledLambdaImpl.<PooledRunnable>acquire(PooledLambdaImpl.sPool, paramBiConsumer, 2, 0, 1, paramA, paramB, null, null, null, null, null, null, null, null, null);
  }
  
  static <A, B> PooledSupplier<Boolean> obtainSupplier(BiPredicate<? super A, ? super B> paramBiPredicate, A paramA, B paramB) {
    return PooledLambdaImpl.<PooledSupplier<Boolean>>acquire(PooledLambdaImpl.sPool, paramBiPredicate, 2, 0, 2, paramA, paramB, null, null, null, null, null, null, null, null, null);
  }
  
  static <A, B, R> PooledSupplier<R> obtainSupplier(BiFunction<? super A, ? super B, ? extends R> paramBiFunction, A paramA, B paramB) {
    return PooledLambdaImpl.<PooledSupplier<R>>acquire(PooledLambdaImpl.sPool, paramBiFunction, 2, 0, 3, paramA, paramB, null, null, null, null, null, null, null, null, null);
  }
  
  static <A, B> PooledConsumer<A> obtainConsumer(BiConsumer<? super A, ? super B> paramBiConsumer, ArgumentPlaceholder<A> paramArgumentPlaceholder, B paramB) {
    return PooledLambdaImpl.<PooledConsumer<A>>acquire(PooledLambdaImpl.sPool, paramBiConsumer, 2, 1, 1, paramArgumentPlaceholder, paramB, null, null, null, null, null, null, null, null, null);
  }
  
  static <A, B> PooledPredicate<A> obtainPredicate(BiPredicate<? super A, ? super B> paramBiPredicate, ArgumentPlaceholder<A> paramArgumentPlaceholder, B paramB) {
    return PooledLambdaImpl.<PooledPredicate<A>>acquire(PooledLambdaImpl.sPool, paramBiPredicate, 2, 1, 2, paramArgumentPlaceholder, paramB, null, null, null, null, null, null, null, null, null);
  }
  
  static <A, B, C> PooledPredicate<A> obtainPredicate(TriPredicate<? super A, ? super B, ? super C> paramTriPredicate, ArgumentPlaceholder<A> paramArgumentPlaceholder, B paramB, C paramC) {
    return PooledLambdaImpl.<PooledPredicate<A>>acquire(PooledLambdaImpl.sPool, paramTriPredicate, 3, 1, 2, paramArgumentPlaceholder, paramB, paramC, null, null, null, null, null, null, null, null);
  }
  
  static <A, B, C, D> PooledPredicate<A> obtainPredicate(QuadPredicate<? super A, ? super B, ? super C, ? super D> paramQuadPredicate, ArgumentPlaceholder<A> paramArgumentPlaceholder, B paramB, C paramC, D paramD) {
    return PooledLambdaImpl.<PooledPredicate<A>>acquire(PooledLambdaImpl.sPool, paramQuadPredicate, 4, 1, 2, paramArgumentPlaceholder, paramB, paramC, paramD, null, null, null, null, null, null, null);
  }
  
  static <A, B, C, D, E> PooledPredicate<A> obtainPredicate(QuintPredicate<? super A, ? super B, ? super C, ? super D, ? super E> paramQuintPredicate, ArgumentPlaceholder<A> paramArgumentPlaceholder, B paramB, C paramC, D paramD, E paramE) {
    return PooledLambdaImpl.<PooledPredicate<A>>acquire(PooledLambdaImpl.sPool, paramQuintPredicate, 5, 1, 2, paramArgumentPlaceholder, paramB, paramC, paramD, paramE, null, null, null, null, null, null);
  }
  
  static <A, B, R> PooledFunction<A, R> obtainFunction(BiFunction<? super A, ? super B, ? extends R> paramBiFunction, ArgumentPlaceholder<A> paramArgumentPlaceholder, B paramB) {
    return PooledLambdaImpl.<PooledFunction<A, R>>acquire(PooledLambdaImpl.sPool, paramBiFunction, 2, 1, 3, paramArgumentPlaceholder, paramB, null, null, null, null, null, null, null, null, null);
  }
  
  static <A, B> PooledConsumer<B> obtainConsumer(BiConsumer<? super A, ? super B> paramBiConsumer, A paramA, ArgumentPlaceholder<B> paramArgumentPlaceholder) {
    return PooledLambdaImpl.<PooledConsumer<B>>acquire(PooledLambdaImpl.sPool, paramBiConsumer, 2, 1, 1, paramA, paramArgumentPlaceholder, null, null, null, null, null, null, null, null, null);
  }
  
  static <A, B> PooledPredicate<B> obtainPredicate(BiPredicate<? super A, ? super B> paramBiPredicate, A paramA, ArgumentPlaceholder<B> paramArgumentPlaceholder) {
    return PooledLambdaImpl.<PooledPredicate<B>>acquire(PooledLambdaImpl.sPool, paramBiPredicate, 2, 1, 2, paramA, paramArgumentPlaceholder, null, null, null, null, null, null, null, null, null);
  }
  
  static <A, B, R> PooledFunction<B, R> obtainFunction(BiFunction<? super A, ? super B, ? extends R> paramBiFunction, A paramA, ArgumentPlaceholder<B> paramArgumentPlaceholder) {
    return PooledLambdaImpl.<PooledFunction<B, R>>acquire(PooledLambdaImpl.sPool, paramBiFunction, 2, 1, 3, paramA, paramArgumentPlaceholder, null, null, null, null, null, null, null, null, null);
  }
  
  static <A, B> Message obtainMessage(BiConsumer<? super A, ? super B> paramBiConsumer, A paramA, B paramB) {
    synchronized (Message.sPoolSync) {
      PooledRunnable pooledRunnable = PooledLambdaImpl.<PooledRunnable>acquire(PooledLambdaImpl.sMessageCallbacksPool, paramBiConsumer, 2, 0, 1, paramA, paramB, null, null, null, null, null, null, null, null, null);
      return Message.obtain().setCallback(pooledRunnable.recycleOnUse());
    } 
  }
  
  static <A, B, C> PooledRunnable obtainRunnable(TriConsumer<? super A, ? super B, ? super C> paramTriConsumer, A paramA, B paramB, C paramC) {
    return PooledLambdaImpl.<PooledRunnable>acquire(PooledLambdaImpl.sPool, paramTriConsumer, 3, 0, 1, paramA, paramB, paramC, null, null, null, null, null, null, null, null);
  }
  
  static <A, B, C, R> PooledSupplier<R> obtainSupplier(TriFunction<? super A, ? super B, ? super C, ? extends R> paramTriFunction, A paramA, B paramB, C paramC) {
    return PooledLambdaImpl.<PooledSupplier<R>>acquire(PooledLambdaImpl.sPool, paramTriFunction, 3, 0, 3, paramA, paramB, paramC, null, null, null, null, null, null, null, null);
  }
  
  static <A, B, C> PooledConsumer<A> obtainConsumer(TriConsumer<? super A, ? super B, ? super C> paramTriConsumer, ArgumentPlaceholder<A> paramArgumentPlaceholder, B paramB, C paramC) {
    return PooledLambdaImpl.<PooledConsumer<A>>acquire(PooledLambdaImpl.sPool, paramTriConsumer, 3, 1, 1, paramArgumentPlaceholder, paramB, paramC, null, null, null, null, null, null, null, null);
  }
  
  static <A, B, C, R> PooledFunction<A, R> obtainFunction(TriFunction<? super A, ? super B, ? super C, ? extends R> paramTriFunction, ArgumentPlaceholder<A> paramArgumentPlaceholder, B paramB, C paramC) {
    return PooledLambdaImpl.<PooledFunction<A, R>>acquire(PooledLambdaImpl.sPool, paramTriFunction, 3, 1, 3, paramArgumentPlaceholder, paramB, paramC, null, null, null, null, null, null, null, null);
  }
  
  static <A, B, C> PooledConsumer<B> obtainConsumer(TriConsumer<? super A, ? super B, ? super C> paramTriConsumer, A paramA, ArgumentPlaceholder<B> paramArgumentPlaceholder, C paramC) {
    return PooledLambdaImpl.<PooledConsumer<B>>acquire(PooledLambdaImpl.sPool, paramTriConsumer, 3, 1, 1, paramA, paramArgumentPlaceholder, paramC, null, null, null, null, null, null, null, null);
  }
  
  static <A, B, C, R> PooledFunction<B, R> obtainFunction(TriFunction<? super A, ? super B, ? super C, ? extends R> paramTriFunction, A paramA, ArgumentPlaceholder<B> paramArgumentPlaceholder, C paramC) {
    return PooledLambdaImpl.<PooledFunction<B, R>>acquire(PooledLambdaImpl.sPool, paramTriFunction, 3, 1, 3, paramA, paramArgumentPlaceholder, paramC, null, null, null, null, null, null, null, null);
  }
  
  static <A, B, C> PooledConsumer<C> obtainConsumer(TriConsumer<? super A, ? super B, ? super C> paramTriConsumer, A paramA, B paramB, ArgumentPlaceholder<C> paramArgumentPlaceholder) {
    return PooledLambdaImpl.<PooledConsumer<C>>acquire(PooledLambdaImpl.sPool, paramTriConsumer, 3, 1, 1, paramA, paramB, paramArgumentPlaceholder, null, null, null, null, null, null, null, null);
  }
  
  static <A, B, C, R> PooledFunction<C, R> obtainFunction(TriFunction<? super A, ? super B, ? super C, ? extends R> paramTriFunction, A paramA, B paramB, ArgumentPlaceholder<C> paramArgumentPlaceholder) {
    return PooledLambdaImpl.<PooledFunction<C, R>>acquire(PooledLambdaImpl.sPool, paramTriFunction, 3, 1, 3, paramA, paramB, paramArgumentPlaceholder, null, null, null, null, null, null, null, null);
  }
  
  static <A, B, C> Message obtainMessage(TriConsumer<? super A, ? super B, ? super C> paramTriConsumer, A paramA, B paramB, C paramC) {
    synchronized (Message.sPoolSync) {
      PooledRunnable pooledRunnable = PooledLambdaImpl.<PooledRunnable>acquire(PooledLambdaImpl.sMessageCallbacksPool, paramTriConsumer, 3, 0, 1, paramA, paramB, paramC, null, null, null, null, null, null, null, null);
      return Message.obtain().setCallback(pooledRunnable.recycleOnUse());
    } 
  }
  
  static <A, B, C, D> PooledRunnable obtainRunnable(QuadConsumer<? super A, ? super B, ? super C, ? super D> paramQuadConsumer, A paramA, B paramB, C paramC, D paramD) {
    return PooledLambdaImpl.<PooledRunnable>acquire(PooledLambdaImpl.sPool, paramQuadConsumer, 4, 0, 1, paramA, paramB, paramC, paramD, null, null, null, null, null, null, null);
  }
  
  static <A, B, C, D, R> PooledSupplier<R> obtainSupplier(QuadFunction<? super A, ? super B, ? super C, ? super D, ? extends R> paramQuadFunction, A paramA, B paramB, C paramC, D paramD) {
    return PooledLambdaImpl.<PooledSupplier<R>>acquire(PooledLambdaImpl.sPool, paramQuadFunction, 4, 0, 3, paramA, paramB, paramC, paramD, null, null, null, null, null, null, null);
  }
  
  static <A, B, C, D> PooledConsumer<A> obtainConsumer(QuadConsumer<? super A, ? super B, ? super C, ? super D> paramQuadConsumer, ArgumentPlaceholder<A> paramArgumentPlaceholder, B paramB, C paramC, D paramD) {
    return PooledLambdaImpl.<PooledConsumer<A>>acquire(PooledLambdaImpl.sPool, paramQuadConsumer, 4, 1, 1, paramArgumentPlaceholder, paramB, paramC, paramD, null, null, null, null, null, null, null);
  }
  
  static <A, B, C, D, R> PooledFunction<A, R> obtainFunction(QuadFunction<? super A, ? super B, ? super C, ? super D, ? extends R> paramQuadFunction, ArgumentPlaceholder<A> paramArgumentPlaceholder, B paramB, C paramC, D paramD) {
    return PooledLambdaImpl.<PooledFunction<A, R>>acquire(PooledLambdaImpl.sPool, paramQuadFunction, 4, 1, 3, paramArgumentPlaceholder, paramB, paramC, paramD, null, null, null, null, null, null, null);
  }
  
  static <A, B, C, D> PooledConsumer<B> obtainConsumer(QuadConsumer<? super A, ? super B, ? super C, ? super D> paramQuadConsumer, A paramA, ArgumentPlaceholder<B> paramArgumentPlaceholder, C paramC, D paramD) {
    return PooledLambdaImpl.<PooledConsumer<B>>acquire(PooledLambdaImpl.sPool, paramQuadConsumer, 4, 1, 1, paramA, paramArgumentPlaceholder, paramC, paramD, null, null, null, null, null, null, null);
  }
  
  static <A, B, C, D, R> PooledFunction<B, R> obtainFunction(QuadFunction<? super A, ? super B, ? super C, ? super D, ? extends R> paramQuadFunction, A paramA, ArgumentPlaceholder<B> paramArgumentPlaceholder, C paramC, D paramD) {
    return PooledLambdaImpl.<PooledFunction<B, R>>acquire(PooledLambdaImpl.sPool, paramQuadFunction, 4, 1, 3, paramA, paramArgumentPlaceholder, paramC, paramD, null, null, null, null, null, null, null);
  }
  
  static <A, B, C, D> PooledConsumer<C> obtainConsumer(QuadConsumer<? super A, ? super B, ? super C, ? super D> paramQuadConsumer, A paramA, B paramB, ArgumentPlaceholder<C> paramArgumentPlaceholder, D paramD) {
    return PooledLambdaImpl.<PooledConsumer<C>>acquire(PooledLambdaImpl.sPool, paramQuadConsumer, 4, 1, 1, paramA, paramB, paramArgumentPlaceholder, paramD, null, null, null, null, null, null, null);
  }
  
  static <A, B, C, D, R> PooledFunction<C, R> obtainFunction(QuadFunction<? super A, ? super B, ? super C, ? super D, ? extends R> paramQuadFunction, A paramA, B paramB, ArgumentPlaceholder<C> paramArgumentPlaceholder, D paramD) {
    return PooledLambdaImpl.<PooledFunction<C, R>>acquire(PooledLambdaImpl.sPool, paramQuadFunction, 4, 1, 3, paramA, paramB, paramArgumentPlaceholder, paramD, null, null, null, null, null, null, null);
  }
  
  static <A, B, C, D> PooledConsumer<D> obtainConsumer(QuadConsumer<? super A, ? super B, ? super C, ? super D> paramQuadConsumer, A paramA, B paramB, C paramC, ArgumentPlaceholder<D> paramArgumentPlaceholder) {
    return PooledLambdaImpl.<PooledConsumer<D>>acquire(PooledLambdaImpl.sPool, paramQuadConsumer, 4, 1, 1, paramA, paramB, paramC, paramArgumentPlaceholder, null, null, null, null, null, null, null);
  }
  
  static <A, B, C, D, R> PooledFunction<D, R> obtainFunction(QuadFunction<? super A, ? super B, ? super C, ? super D, ? extends R> paramQuadFunction, A paramA, B paramB, C paramC, ArgumentPlaceholder<D> paramArgumentPlaceholder) {
    return PooledLambdaImpl.<PooledFunction<D, R>>acquire(PooledLambdaImpl.sPool, paramQuadFunction, 4, 1, 3, paramA, paramB, paramC, paramArgumentPlaceholder, null, null, null, null, null, null, null);
  }
  
  static <A, B, C, D> Message obtainMessage(QuadConsumer<? super A, ? super B, ? super C, ? super D> paramQuadConsumer, A paramA, B paramB, C paramC, D paramD) {
    synchronized (Message.sPoolSync) {
      PooledRunnable pooledRunnable = PooledLambdaImpl.<PooledRunnable>acquire(PooledLambdaImpl.sMessageCallbacksPool, paramQuadConsumer, 4, 0, 1, paramA, paramB, paramC, paramD, null, null, null, null, null, null, null);
      return Message.obtain().setCallback(pooledRunnable.recycleOnUse());
    } 
  }
  
  static <A, B, C, D, E> PooledRunnable obtainRunnable(QuintConsumer<? super A, ? super B, ? super C, ? super D, ? super E> paramQuintConsumer, A paramA, B paramB, C paramC, D paramD, E paramE) {
    return PooledLambdaImpl.<PooledRunnable>acquire(PooledLambdaImpl.sPool, paramQuintConsumer, 5, 0, 1, paramA, paramB, paramC, paramD, paramE, null, null, null, null, null, null);
  }
  
  static <A, B, C, D, E, R> PooledSupplier<R> obtainSupplier(QuintFunction<? super A, ? super B, ? super C, ? super D, ? super E, ? extends R> paramQuintFunction, A paramA, B paramB, C paramC, D paramD, E paramE) {
    return PooledLambdaImpl.<PooledSupplier<R>>acquire(PooledLambdaImpl.sPool, paramQuintFunction, 5, 0, 3, paramA, paramB, paramC, paramD, paramE, null, null, null, null, null, null);
  }
  
  static <A, B, C, D, E> Message obtainMessage(QuintConsumer<? super A, ? super B, ? super C, ? super D, ? super E> paramQuintConsumer, A paramA, B paramB, C paramC, D paramD, E paramE) {
    synchronized (Message.sPoolSync) {
      PooledRunnable pooledRunnable = PooledLambdaImpl.<PooledRunnable>acquire(PooledLambdaImpl.sMessageCallbacksPool, paramQuintConsumer, 5, 0, 1, paramA, paramB, paramC, paramD, paramE, null, null, null, null, null, null);
      return Message.obtain().setCallback(pooledRunnable.recycleOnUse());
    } 
  }
  
  static <A, B, C, D, E, F> PooledRunnable obtainRunnable(HexConsumer<? super A, ? super B, ? super C, ? super D, ? super E, ? super F> paramHexConsumer, A paramA, B paramB, C paramC, D paramD, E paramE, F paramF) {
    return PooledLambdaImpl.<PooledRunnable>acquire(PooledLambdaImpl.sPool, paramHexConsumer, 6, 0, 1, paramA, paramB, paramC, paramD, paramE, paramF, null, null, null, null, null);
  }
  
  static <A, B, C, D, E, F, R> PooledSupplier<R> obtainSupplier(HexFunction<? super A, ? super B, ? super C, ? super D, ? super E, ? super F, ? extends R> paramHexFunction, A paramA, B paramB, C paramC, D paramD, E paramE, F paramF) {
    return PooledLambdaImpl.<PooledSupplier<R>>acquire(PooledLambdaImpl.sPool, paramHexFunction, 6, 0, 3, paramA, paramB, paramC, paramD, paramE, paramF, null, null, null, null, null);
  }
  
  static <A, B, C, D, E, F> Message obtainMessage(HexConsumer<? super A, ? super B, ? super C, ? super D, ? super E, ? super F> paramHexConsumer, A paramA, B paramB, C paramC, D paramD, E paramE, F paramF) {
    synchronized (Message.sPoolSync) {
      PooledRunnable pooledRunnable = PooledLambdaImpl.<PooledRunnable>acquire(PooledLambdaImpl.sMessageCallbacksPool, paramHexConsumer, 6, 0, 1, paramA, paramB, paramC, paramD, paramE, paramF, null, null, null, null, null);
      return Message.obtain().setCallback(pooledRunnable.recycleOnUse());
    } 
  }
  
  static <A, B, C, D, E, F, G> PooledRunnable obtainRunnable(HeptConsumer<? super A, ? super B, ? super C, ? super D, ? super E, ? super F, ? super G> paramHeptConsumer, A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG) {
    return PooledLambdaImpl.<PooledRunnable>acquire(PooledLambdaImpl.sPool, paramHeptConsumer, 7, 0, 1, paramA, paramB, paramC, paramD, paramE, paramF, paramG, null, null, null, null);
  }
  
  static <A, B, C, D, E, F, G, R> PooledSupplier<R> obtainSupplier(HeptFunction<? super A, ? super B, ? super C, ? super D, ? super E, ? super F, ? super G, ? extends R> paramHeptFunction, A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG) {
    return PooledLambdaImpl.<PooledSupplier<R>>acquire(PooledLambdaImpl.sPool, paramHeptFunction, 7, 0, 3, paramA, paramB, paramC, paramD, paramE, paramF, paramG, null, null, null, null);
  }
  
  static <A, B, C, D, E, F, G> Message obtainMessage(HeptConsumer<? super A, ? super B, ? super C, ? super D, ? super E, ? super F, ? super G> paramHeptConsumer, A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG) {
    synchronized (Message.sPoolSync) {
      PooledRunnable pooledRunnable = PooledLambdaImpl.<PooledRunnable>acquire(PooledLambdaImpl.sMessageCallbacksPool, paramHeptConsumer, 7, 0, 1, paramA, paramB, paramC, paramD, paramE, paramF, paramG, null, null, null, null);
      return Message.obtain().setCallback(pooledRunnable.recycleOnUse());
    } 
  }
  
  static <A, B, C, D, E, F, G, H> PooledRunnable obtainRunnable(OctConsumer<? super A, ? super B, ? super C, ? super D, ? super E, ? super F, ? super G, ? super H> paramOctConsumer, A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH) {
    return PooledLambdaImpl.<PooledRunnable>acquire(PooledLambdaImpl.sPool, paramOctConsumer, 8, 0, 1, paramA, paramB, paramC, paramD, paramE, paramF, paramG, paramH, null, null, null);
  }
  
  static <A, B, C, D, E, F, G, H, R> PooledSupplier<R> obtainSupplier(OctFunction<? super A, ? super B, ? super C, ? super D, ? super E, ? super F, ? super G, ? super H, ? extends R> paramOctFunction, A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH) {
    return PooledLambdaImpl.<PooledSupplier<R>>acquire(PooledLambdaImpl.sPool, paramOctFunction, 8, 0, 3, paramA, paramB, paramC, paramD, paramE, paramF, paramG, paramH, null, null, null);
  }
  
  static <A, B, C, D, E, F, G, H> Message obtainMessage(OctConsumer<? super A, ? super B, ? super C, ? super D, ? super E, ? super F, ? super G, ? super H> paramOctConsumer, A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH) {
    synchronized (Message.sPoolSync) {
      PooledRunnable pooledRunnable = PooledLambdaImpl.<PooledRunnable>acquire(PooledLambdaImpl.sMessageCallbacksPool, paramOctConsumer, 8, 0, 1, paramA, paramB, paramC, paramD, paramE, paramF, paramG, paramH, null, null, null);
      return Message.obtain().setCallback(pooledRunnable.recycleOnUse());
    } 
  }
  
  static <A, B, C, D, E, F, G, H, I> PooledRunnable obtainRunnable(NonaConsumer<? super A, ? super B, ? super C, ? super D, ? super E, ? super F, ? super G, ? super H, ? super I> paramNonaConsumer, A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH, I paramI) {
    return PooledLambdaImpl.<PooledRunnable>acquire(PooledLambdaImpl.sPool, paramNonaConsumer, 9, 0, 1, paramA, paramB, paramC, paramD, paramE, paramF, paramG, paramH, paramI, null, null);
  }
  
  static <A, B, C, D, E, F, G, H, I, R> PooledSupplier<R> obtainSupplier(NonaFunction<? super A, ? super B, ? super C, ? super D, ? super E, ? super F, ? super G, ? super H, ? super I, ? extends R> paramNonaFunction, A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH, I paramI) {
    return PooledLambdaImpl.<PooledSupplier<R>>acquire(PooledLambdaImpl.sPool, paramNonaFunction, 9, 0, 3, paramA, paramB, paramC, paramD, paramE, paramF, paramG, paramH, paramI, null, null);
  }
  
  static <A, B, C, D, E, F, G, H, I> Message obtainMessage(NonaConsumer<? super A, ? super B, ? super C, ? super D, ? super E, ? super F, ? super G, ? super H, ? super I> paramNonaConsumer, A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH, I paramI) {
    synchronized (Message.sPoolSync) {
      PooledRunnable pooledRunnable = PooledLambdaImpl.<PooledRunnable>acquire(PooledLambdaImpl.sMessageCallbacksPool, paramNonaConsumer, 9, 0, 1, paramA, paramB, paramC, paramD, paramE, paramF, paramG, paramH, paramI, null, null);
      return Message.obtain().setCallback(pooledRunnable.recycleOnUse());
    } 
  }
  
  static <A, B, C, D, E, F, G, H, I, J> PooledRunnable obtainRunnable(DecConsumer<? super A, ? super B, ? super C, ? super D, ? super E, ? super F, ? super G, ? super H, ? super I, ? super J> paramDecConsumer, A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH, I paramI, J paramJ) {
    return PooledLambdaImpl.<PooledRunnable>acquire(PooledLambdaImpl.sPool, paramDecConsumer, 10, 0, 1, paramA, paramB, paramC, paramD, paramE, paramF, paramG, paramH, paramI, paramJ, null);
  }
  
  static <A, B, C, D, E, F, G, H, I, J, R> PooledSupplier<R> obtainSupplier(DecFunction<? super A, ? super B, ? super C, ? super D, ? super E, ? super F, ? super G, ? super H, ? super I, ? super J, ? extends R> paramDecFunction, A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH, I paramI, J paramJ) {
    return PooledLambdaImpl.<PooledSupplier<R>>acquire(PooledLambdaImpl.sPool, paramDecFunction, 10, 0, 3, paramA, paramB, paramC, paramD, paramE, paramF, paramG, paramH, paramI, paramJ, null);
  }
  
  static <A, B, C, D, E, F, G, H, I, J> Message obtainMessage(DecConsumer<? super A, ? super B, ? super C, ? super D, ? super E, ? super F, ? super G, ? super H, ? super I, ? super J> paramDecConsumer, A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH, I paramI, J paramJ) {
    synchronized (Message.sPoolSync) {
      PooledRunnable pooledRunnable = PooledLambdaImpl.<PooledRunnable>acquire(PooledLambdaImpl.sMessageCallbacksPool, paramDecConsumer, 10, 0, 1, paramA, paramB, paramC, paramD, paramE, paramF, paramG, paramH, paramI, paramJ, null);
      return Message.obtain().setCallback(pooledRunnable.recycleOnUse());
    } 
  }
  
  static <A, B, C, D, E, F, G, H, I, J, K> PooledRunnable obtainRunnable(UndecConsumer<? super A, ? super B, ? super C, ? super D, ? super E, ? super F, ? super G, ? super H, ? super I, ? super J, ? super K> paramUndecConsumer, A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH, I paramI, J paramJ, K paramK) {
    return PooledLambdaImpl.<PooledRunnable>acquire(PooledLambdaImpl.sPool, paramUndecConsumer, 11, 0, 1, paramA, paramB, paramC, paramD, paramE, paramF, paramG, paramH, paramI, paramJ, paramK);
  }
  
  static <A, B, C, D, E, F, G, H, I, J, K, R> PooledSupplier<R> obtainSupplier(UndecFunction<? super A, ? super B, ? super C, ? super D, ? super E, ? super F, ? super G, ? super H, ? super I, ? super J, ? super K, ? extends R> paramUndecFunction, A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH, I paramI, J paramJ, K paramK) {
    return PooledLambdaImpl.<PooledSupplier<R>>acquire(PooledLambdaImpl.sPool, paramUndecFunction, 11, 0, 3, paramA, paramB, paramC, paramD, paramE, paramF, paramG, paramH, paramI, paramJ, paramK);
  }
  
  static <A, B, C, D, E, F, G, H, I, J, K> Message obtainMessage(UndecConsumer<? super A, ? super B, ? super C, ? super D, ? super E, ? super F, ? super G, ? super H, ? super I, ? super J, ? super K> paramUndecConsumer, A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH, I paramI, J paramJ, K paramK) {
    synchronized (Message.sPoolSync) {
      PooledRunnable pooledRunnable = PooledLambdaImpl.<PooledRunnable>acquire(PooledLambdaImpl.sMessageCallbacksPool, paramUndecConsumer, 11, 0, 1, paramA, paramB, paramC, paramD, paramE, paramF, paramG, paramH, paramI, paramJ, paramK);
      return Message.obtain().setCallback(pooledRunnable.recycleOnUse());
    } 
  }
  
  void recycle();
  
  PooledLambda recycleOnUse();
}
