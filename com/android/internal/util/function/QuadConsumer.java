package com.android.internal.util.function;

public interface QuadConsumer<A, B, C, D> {
  void accept(A paramA, B paramB, C paramC, D paramD);
}
