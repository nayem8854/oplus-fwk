package com.android.internal.util.function;

public interface UndecPredicate<A, B, C, D, E, F, G, H, I, J, K> {
  boolean test(A paramA, B paramB, C paramC, D paramD, E paramE, F paramF, G paramG, H paramH, I paramI, J paramJ, K paramK);
}
