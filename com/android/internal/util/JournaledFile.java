package com.android.internal.util;

import java.io.File;
import java.io.IOException;

@Deprecated
public class JournaledFile {
  File mReal;
  
  File mTemp;
  
  boolean mWriting;
  
  public JournaledFile(File paramFile1, File paramFile2) {
    this.mReal = paramFile1;
    this.mTemp = paramFile2;
  }
  
  public File chooseForRead() {
    File file;
    if (this.mReal.exists()) {
      File file1 = this.mReal;
      file = file1;
      if (this.mTemp.exists()) {
        this.mTemp.delete();
        file = file1;
      } 
    } else {
      if (this.mTemp.exists()) {
        file = this.mTemp;
        this.mTemp.renameTo(this.mReal);
        return file;
      } 
      return this.mReal;
    } 
    return file;
  }
  
  public File chooseForWrite() {
    if (!this.mWriting) {
      if (!this.mReal.exists())
        try {
          this.mReal.createNewFile();
        } catch (IOException iOException) {} 
      if (this.mTemp.exists())
        this.mTemp.delete(); 
      this.mWriting = true;
      return this.mTemp;
    } 
    throw new IllegalStateException("uncommitted write already in progress");
  }
  
  public void commit() {
    if (this.mWriting) {
      this.mWriting = false;
      this.mTemp.renameTo(this.mReal);
      return;
    } 
    throw new IllegalStateException("no file to commit");
  }
  
  public void rollback() {
    if (this.mWriting) {
      this.mWriting = false;
      this.mTemp.delete();
      return;
    } 
    throw new IllegalStateException("no file to roll back");
  }
}
