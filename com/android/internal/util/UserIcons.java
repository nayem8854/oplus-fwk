package com.android.internal.util;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;

public class UserIcons {
  private static final int[] USER_ICON_COLORS = new int[] { 17171012, 17171013, 17171014, 17171015, 17171016, 17171017, 17171018, 17171019 };
  
  public static Bitmap convertToBitmap(Drawable paramDrawable) {
    if (paramDrawable == null)
      return null; 
    int i = paramDrawable.getIntrinsicWidth();
    int j = paramDrawable.getIntrinsicHeight();
    Bitmap bitmap = Bitmap.createBitmap(i, j, Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(bitmap);
    paramDrawable.setBounds(0, 0, i, j);
    paramDrawable.draw(canvas);
    return bitmap;
  }
  
  public static Drawable getDefaultUserIcon(Resources paramResources, int paramInt, boolean paramBoolean) {
    int i;
    if (paramBoolean) {
      i = 17171021;
    } else {
      i = 17171020;
    } 
    if (paramInt != -10000) {
      int[] arrayOfInt = USER_ICON_COLORS;
      i = arrayOfInt[paramInt % arrayOfInt.length];
    } 
    Drawable drawable = paramResources.getDrawable(201850910, null).mutate();
    drawable.setColorFilter(paramResources.getColor(i, null), PorterDuff.Mode.XOR);
    drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
    return drawable;
  }
}
