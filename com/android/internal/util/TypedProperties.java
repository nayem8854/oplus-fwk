package com.android.internal.util;

import java.io.IOException;
import java.io.Reader;
import java.io.StreamTokenizer;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class TypedProperties extends HashMap<String, Object> {
  static StreamTokenizer initTokenizer(Reader paramReader) {
    StreamTokenizer streamTokenizer = new StreamTokenizer(paramReader);
    streamTokenizer.resetSyntax();
    streamTokenizer.wordChars(48, 57);
    streamTokenizer.wordChars(65, 90);
    streamTokenizer.wordChars(97, 122);
    streamTokenizer.wordChars(95, 95);
    streamTokenizer.wordChars(36, 36);
    streamTokenizer.wordChars(46, 46);
    streamTokenizer.wordChars(45, 45);
    streamTokenizer.wordChars(43, 43);
    streamTokenizer.ordinaryChar(61);
    streamTokenizer.whitespaceChars(32, 32);
    streamTokenizer.whitespaceChars(9, 9);
    streamTokenizer.whitespaceChars(10, 10);
    streamTokenizer.whitespaceChars(13, 13);
    streamTokenizer.quoteChar(34);
    streamTokenizer.slashStarComments(true);
    streamTokenizer.slashSlashComments(true);
    return streamTokenizer;
  }
  
  public static class ParseException extends IllegalArgumentException {
    ParseException(StreamTokenizer param1StreamTokenizer, String param1String) {
      super(stringBuilder.toString());
    }
  }
  
  static final String NULL_STRING = new String("<TypedProperties:NULL_STRING>");
  
  public static final int STRING_NOT_SET = -1;
  
  public static final int STRING_NULL = 0;
  
  public static final int STRING_SET = 1;
  
  public static final int STRING_TYPE_MISMATCH = -2;
  
  static final int TYPE_BOOLEAN = 90;
  
  static final int TYPE_BYTE = 329;
  
  static final int TYPE_DOUBLE = 2118;
  
  static final int TYPE_ERROR = -1;
  
  static final int TYPE_FLOAT = 1094;
  
  static final int TYPE_INT = 1097;
  
  static final int TYPE_LONG = 2121;
  
  static final int TYPE_SHORT = 585;
  
  static final int TYPE_STRING = 29516;
  
  static final int TYPE_UNSET = 120;
  
  static int interpretType(String paramString) {
    if ("unset".equals(paramString))
      return 120; 
    if ("boolean".equals(paramString))
      return 90; 
    if ("byte".equals(paramString))
      return 329; 
    if ("short".equals(paramString))
      return 585; 
    if ("int".equals(paramString))
      return 1097; 
    if ("long".equals(paramString))
      return 2121; 
    if ("float".equals(paramString))
      return 1094; 
    if ("double".equals(paramString))
      return 2118; 
    if ("String".equals(paramString))
      return 29516; 
    return -1;
  }
  
  static void parse(Reader paramReader, Map<String, Object> paramMap) throws ParseException, IOException {
    StreamTokenizer streamTokenizer = initTokenizer(paramReader);
    Pattern pattern = Pattern.compile("([a-zA-Z_$][0-9a-zA-Z_$]*\\.)*[a-zA-Z_$][0-9a-zA-Z_$]*");
    while (true) {
      int i = streamTokenizer.nextToken();
      if (i == -1)
        return; 
      if (i == -3) {
        i = interpretType(streamTokenizer.sval);
        if (i != -1) {
          streamTokenizer.sval = null;
          if (i == 120) {
            int k = streamTokenizer.nextToken();
            if (k != 40)
              throw new ParseException(streamTokenizer, "'('"); 
          } 
          int j = streamTokenizer.nextToken();
          if (j == -3) {
            String str = streamTokenizer.sval;
            if (pattern.matcher(str).matches()) {
              streamTokenizer.sval = null;
              if (i == 120) {
                i = streamTokenizer.nextToken();
                if (i == 41) {
                  paramMap.remove(str);
                } else {
                  throw new ParseException(streamTokenizer, "')'");
                } 
              } else {
                j = streamTokenizer.nextToken();
                if (j == 61) {
                  Object object1 = parseValue(streamTokenizer, i);
                  Object object2 = paramMap.remove(str);
                  if (object2 != null)
                    if (object1.getClass() != object2.getClass())
                      throw new ParseException(streamTokenizer, "(property previously declared as a different type)");  
                  paramMap.put(str, object1);
                } else {
                  throw new ParseException(streamTokenizer, "'='");
                } 
              } 
              i = streamTokenizer.nextToken();
              if (i == 59)
                continue; 
              throw new ParseException(streamTokenizer, "';'");
            } 
            throw new ParseException(streamTokenizer, "valid property name");
          } 
          throw new ParseException(streamTokenizer, "property name");
        } 
        throw new ParseException(streamTokenizer, "valid type name");
      } 
      throw new ParseException(streamTokenizer, "type name");
    } 
  }
  
  static Object parseValue(StreamTokenizer paramStreamTokenizer, int paramInt) throws IOException {
    int i = paramStreamTokenizer.nextToken();
    if (paramInt == 90) {
      if (i == -3) {
        if ("true".equals(paramStreamTokenizer.sval))
          return Boolean.TRUE; 
        if ("false".equals(paramStreamTokenizer.sval))
          return Boolean.FALSE; 
        throw new ParseException(paramStreamTokenizer, "boolean constant");
      } 
      throw new ParseException(paramStreamTokenizer, "boolean constant");
    } 
    if ((paramInt & 0xFF) == 73) {
      if (i == -3)
        try {
          long l = Long.decode(paramStreamTokenizer.sval).longValue();
          paramInt = paramInt >> 8 & 0xFF;
          if (paramInt != 1) {
            if (paramInt != 2) {
              if (paramInt != 4) {
                if (paramInt == 8) {
                  if (l >= Long.MIN_VALUE && l <= Long.MAX_VALUE)
                    return new Long(l); 
                  throw new ParseException(paramStreamTokenizer, "64-bit integer constant");
                } 
                stringBuilder = new StringBuilder();
                stringBuilder.append("Internal error; unexpected integer type width ");
                stringBuilder.append(paramInt);
                throw new IllegalStateException(stringBuilder.toString());
              } 
              if (l >= -2147483648L && l <= 2147483647L)
                return new Integer((int)l); 
              throw new ParseException(stringBuilder, "32-bit integer constant");
            } 
            if (l >= -32768L && l <= 32767L)
              return new Short((short)(int)l); 
            throw new ParseException(stringBuilder, "16-bit integer constant");
          } 
          if (l >= -128L && l <= 127L)
            return new Byte((byte)(int)l); 
          throw new ParseException(stringBuilder, "8-bit integer constant");
        } catch (NumberFormatException numberFormatException) {
          throw new ParseException(stringBuilder, "integer constant");
        }  
      throw new ParseException(stringBuilder, "integer constant");
    } 
    if ((paramInt & 0xFF) == 70) {
      if (i == -3)
        try {
          double d = Double.parseDouble(((StreamTokenizer)stringBuilder).sval);
          if ((paramInt >> 8 & 0xFF) == 4) {
            double d1 = Math.abs(d);
            if (d1 == 0.0D || Double.isInfinite(d) || Double.isNaN(d) || (
              d1 >= 1.401298464324817E-45D && d1 <= 3.4028234663852886E38D))
              return new Float((float)d); 
            throw new ParseException(stringBuilder, "32-bit float constant");
          } 
          return new Double(d);
        } catch (NumberFormatException numberFormatException) {
          throw new ParseException(stringBuilder, "float constant");
        }  
      throw new ParseException(stringBuilder, "float constant");
    } 
    if (paramInt == 29516) {
      if (i == 34)
        return ((StreamTokenizer)stringBuilder).sval; 
      if (i == -3 && "null".equals(((StreamTokenizer)stringBuilder).sval))
        return NULL_STRING; 
      throw new ParseException(stringBuilder, "double-quoted string or 'null'");
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Internal error; unknown type ");
    stringBuilder.append(paramInt);
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public void load(Reader paramReader) throws IOException {
    parse(paramReader, this);
  }
  
  public Object get(Object paramObject) {
    paramObject = super.get(paramObject);
    if (paramObject == NULL_STRING)
      return null; 
    return paramObject;
  }
  
  public static class TypeException extends IllegalArgumentException {
    TypeException(String param1String1, Object param1Object, String param1String2) {
      super(stringBuilder.toString());
    }
  }
  
  public boolean getBoolean(String paramString, boolean paramBoolean) {
    Object object = super.get(paramString);
    if (object == null)
      return paramBoolean; 
    if (object instanceof Boolean)
      return ((Boolean)object).booleanValue(); 
    throw new TypeException(paramString, object, "boolean");
  }
  
  public byte getByte(String paramString, byte paramByte) {
    Object object = super.get(paramString);
    if (object == null)
      return paramByte; 
    if (object instanceof Byte)
      return ((Byte)object).byteValue(); 
    throw new TypeException(paramString, object, "byte");
  }
  
  public short getShort(String paramString, short paramShort) {
    Object object = super.get(paramString);
    if (object == null)
      return paramShort; 
    if (object instanceof Short)
      return ((Short)object).shortValue(); 
    throw new TypeException(paramString, object, "short");
  }
  
  public int getInt(String paramString, int paramInt) {
    Object object = super.get(paramString);
    if (object == null)
      return paramInt; 
    if (object instanceof Integer)
      return ((Integer)object).intValue(); 
    throw new TypeException(paramString, object, "int");
  }
  
  public long getLong(String paramString, long paramLong) {
    Object object = super.get(paramString);
    if (object == null)
      return paramLong; 
    if (object instanceof Long)
      return ((Long)object).longValue(); 
    throw new TypeException(paramString, object, "long");
  }
  
  public float getFloat(String paramString, float paramFloat) {
    Object object = super.get(paramString);
    if (object == null)
      return paramFloat; 
    if (object instanceof Float)
      return ((Float)object).floatValue(); 
    throw new TypeException(paramString, object, "float");
  }
  
  public double getDouble(String paramString, double paramDouble) {
    Object object = super.get(paramString);
    if (object == null)
      return paramDouble; 
    if (object instanceof Double)
      return ((Double)object).doubleValue(); 
    throw new TypeException(paramString, object, "double");
  }
  
  public String getString(String paramString1, String paramString2) {
    Object object = super.get(paramString1);
    if (object == null)
      return paramString2; 
    if (object == NULL_STRING)
      return null; 
    if (object instanceof String)
      return (String)object; 
    throw new TypeException(paramString1, object, "string");
  }
  
  public boolean getBoolean(String paramString) {
    return getBoolean(paramString, false);
  }
  
  public byte getByte(String paramString) {
    return getByte(paramString, (byte)0);
  }
  
  public short getShort(String paramString) {
    return getShort(paramString, (short)0);
  }
  
  public int getInt(String paramString) {
    return getInt(paramString, 0);
  }
  
  public long getLong(String paramString) {
    return getLong(paramString, 0L);
  }
  
  public float getFloat(String paramString) {
    return getFloat(paramString, 0.0F);
  }
  
  public double getDouble(String paramString) {
    return getDouble(paramString, 0.0D);
  }
  
  public String getString(String paramString) {
    return getString(paramString, "");
  }
  
  public int getStringInfo(String paramString) {
    paramString = (String)super.get(paramString);
    if (paramString == null)
      return -1; 
    if (paramString == NULL_STRING)
      return 0; 
    if (paramString instanceof String)
      return 1; 
    return -2;
  }
}
