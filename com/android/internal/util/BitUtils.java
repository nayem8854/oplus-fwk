package com.android.internal.util;

import android.text.TextUtils;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Objects;
import java.util.UUID;
import java.util.function.IntFunction;

public final class BitUtils {
  public static boolean maskedEquals(long paramLong1, long paramLong2, long paramLong3) {
    boolean bool;
    if ((paramLong1 & paramLong3) == (paramLong2 & paramLong3)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean maskedEquals(byte paramByte1, byte paramByte2, byte paramByte3) {
    boolean bool;
    if ((paramByte1 & paramByte3) == (paramByte2 & paramByte3)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean maskedEquals(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, byte[] paramArrayOfbyte3) {
    boolean bool = false;
    if (paramArrayOfbyte1 == null || paramArrayOfbyte2 == null) {
      if (paramArrayOfbyte1 == paramArrayOfbyte2)
        bool = true; 
      return bool;
    } 
    if (paramArrayOfbyte1.length == paramArrayOfbyte2.length) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "Inputs must be of same size");
    if (paramArrayOfbyte3 == null)
      return Arrays.equals(paramArrayOfbyte1, paramArrayOfbyte2); 
    if (paramArrayOfbyte1.length == paramArrayOfbyte3.length) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "Mask must be of same size as inputs");
    for (byte b = 0; b < paramArrayOfbyte3.length; b++) {
      if (!maskedEquals(paramArrayOfbyte1[b], paramArrayOfbyte2[b], paramArrayOfbyte3[b]))
        return false; 
    } 
    return true;
  }
  
  public static boolean maskedEquals(UUID paramUUID1, UUID paramUUID2, UUID paramUUID3) {
    if (paramUUID3 == null)
      return Objects.equals(paramUUID1, paramUUID2); 
    long l1 = paramUUID1.getLeastSignificantBits(), l2 = paramUUID2.getLeastSignificantBits();
    long l3 = paramUUID3.getLeastSignificantBits();
    if (maskedEquals(l1, l2, l3)) {
      l1 = paramUUID1.getMostSignificantBits();
      l3 = paramUUID2.getMostSignificantBits();
      l2 = paramUUID3.getMostSignificantBits();
      if (maskedEquals(l1, l3, l2))
        return true; 
    } 
    return false;
  }
  
  public static int[] unpackBits(long paramLong) {
    int i = Long.bitCount(paramLong);
    int[] arrayOfInt = new int[i];
    int j = 0;
    i = 0;
    while (paramLong != 0L) {
      int k = j;
      if ((paramLong & 0x1L) == 1L) {
        arrayOfInt[j] = i;
        k = j + 1;
      } 
      paramLong >>>= 1L;
      i++;
      j = k;
    } 
    return arrayOfInt;
  }
  
  public static long packBits(int[] paramArrayOfint) {
    long l = 0L;
    int i;
    byte b;
    for (i = paramArrayOfint.length, b = 0; b < i; ) {
      int j = paramArrayOfint[b];
      l |= 1L << j;
      b++;
    } 
    return l;
  }
  
  public static int uint8(byte paramByte) {
    return paramByte & 0xFF;
  }
  
  public static int uint16(short paramShort) {
    return 0xFFFF & paramShort;
  }
  
  public static int uint16(byte paramByte1, byte paramByte2) {
    return (paramByte1 & 0xFF) << 8 | paramByte2 & 0xFF;
  }
  
  public static long uint32(int paramInt) {
    return paramInt & 0xFFFFFFFFL;
  }
  
  public static int bytesToBEInt(byte[] paramArrayOfbyte) {
    int i = uint8(paramArrayOfbyte[0]);
    byte b = paramArrayOfbyte[1];
    int j = uint8(b);
    b = paramArrayOfbyte[2];
    int k = uint8(b);
    b = paramArrayOfbyte[3];
    int m = uint8(b);
    return (i << 24) + (j << 16) + (k << 8) + m;
  }
  
  public static int bytesToLEInt(byte[] paramArrayOfbyte) {
    return Integer.reverseBytes(bytesToBEInt(paramArrayOfbyte));
  }
  
  public static int getUint8(ByteBuffer paramByteBuffer, int paramInt) {
    return uint8(paramByteBuffer.get(paramInt));
  }
  
  public static int getUint16(ByteBuffer paramByteBuffer, int paramInt) {
    return uint16(paramByteBuffer.getShort(paramInt));
  }
  
  public static long getUint32(ByteBuffer paramByteBuffer, int paramInt) {
    return uint32(paramByteBuffer.getInt(paramInt));
  }
  
  public static void put(ByteBuffer paramByteBuffer, int paramInt, byte[] paramArrayOfbyte) {
    int i = paramByteBuffer.position();
    paramByteBuffer.position(paramInt);
    paramByteBuffer.put(paramArrayOfbyte);
    paramByteBuffer.position(i);
  }
  
  public static boolean isBitSet(long paramLong, int paramInt) {
    boolean bool;
    if ((bitAt(paramInt) & paramLong) != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static long bitAt(int paramInt) {
    return 1L << paramInt;
  }
  
  public static String flagsToString(int paramInt, IntFunction<String> paramIntFunction) {
    StringBuilder stringBuilder = new StringBuilder();
    int i = 0, j = paramInt;
    paramInt = i;
    while (j != 0) {
      i = 1 << Integer.numberOfTrailingZeros(j);
      j &= i ^ 0xFFFFFFFF;
      if (paramInt > 0)
        stringBuilder.append(", "); 
      stringBuilder.append(paramIntFunction.apply(i));
      paramInt++;
    } 
    TextUtils.wrap(stringBuilder, "[", "]");
    return stringBuilder.toString();
  }
  
  public static byte[] toBytes(long paramLong) {
    return ByteBuffer.allocate(8).putLong(paramLong).array();
  }
  
  public static int flagsUpTo(int paramInt) {
    if (paramInt <= 0) {
      paramInt = 0;
    } else {
      paramInt = flagsUpTo(paramInt >> 1) | paramInt;
    } 
    return paramInt;
  }
  
  public static int flagsWithin(int paramInt1, int paramInt2) {
    return flagsUpTo(paramInt2) & (flagsUpTo(paramInt1) ^ 0xFFFFFFFF) | paramInt1;
  }
}
