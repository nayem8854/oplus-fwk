package com.android.internal.util;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import java.lang.reflect.Method;
import java.util.Objects;
import java.util.Stack;

public class AsyncChannel {
  private static final int BASE = 69632;
  
  public static final int CMD_CHANNEL_DISCONNECT = 69635;
  
  public static final int CMD_CHANNEL_DISCONNECTED = 69636;
  
  public static final int CMD_CHANNEL_FULLY_CONNECTED = 69634;
  
  public static final int CMD_CHANNEL_FULL_CONNECTION = 69633;
  
  public static final int CMD_CHANNEL_HALF_CONNECTED = 69632;
  
  private static final int CMD_TO_STRING_COUNT = 5;
  
  private static final boolean DBG = false;
  
  public static final int STATUS_BINDING_UNSUCCESSFUL = 1;
  
  public static final int STATUS_FULL_CONNECTION_REFUSED_ALREADY_CONNECTED = 3;
  
  public static final int STATUS_REMOTE_DISCONNECTION = 4;
  
  public static final int STATUS_SEND_UNSUCCESSFUL = 2;
  
  public static final int STATUS_SUCCESSFUL = 0;
  
  private static final String TAG = "AsyncChannel";
  
  private static String[] sCmdToString;
  
  private AsyncChannelConnection mConnection;
  
  private DeathMonitor mDeathMonitor;
  
  private Messenger mDstMessenger;
  
  private Context mSrcContext;
  
  private Handler mSrcHandler;
  
  private Messenger mSrcMessenger;
  
  static {
    String[] arrayOfString = new String[5];
    arrayOfString[0] = "CMD_CHANNEL_HALF_CONNECTED";
    arrayOfString[1] = "CMD_CHANNEL_FULL_CONNECTION";
    arrayOfString[2] = "CMD_CHANNEL_FULLY_CONNECTED";
    arrayOfString[3] = "CMD_CHANNEL_DISCONNECT";
    arrayOfString[4] = "CMD_CHANNEL_DISCONNECTED";
  }
  
  protected static String cmdToString(int paramInt) {
    paramInt -= 69632;
    if (paramInt >= 0) {
      String[] arrayOfString = sCmdToString;
      if (paramInt < arrayOfString.length)
        return arrayOfString[paramInt]; 
    } 
    return null;
  }
  
  public int connectSrcHandlerToPackageSync(Context paramContext, Handler paramHandler, String paramString1, String paramString2) {
    this.mConnection = new AsyncChannelConnection();
    this.mSrcContext = paramContext;
    this.mSrcHandler = paramHandler;
    this.mSrcMessenger = new Messenger(paramHandler);
    this.mDstMessenger = null;
    Intent intent = new Intent("android.intent.action.MAIN");
    intent.setClassName(paramString1, paramString2);
    boolean bool = paramContext.bindService(intent, this.mConnection, 1);
    return bool ^ true;
  }
  
  public int connectSync(Context paramContext, Handler paramHandler, Messenger paramMessenger) {
    connected(paramContext, paramHandler, paramMessenger);
    return 0;
  }
  
  public int connectSync(Context paramContext, Handler paramHandler1, Handler paramHandler2) {
    return connectSync(paramContext, paramHandler1, new Messenger(paramHandler2));
  }
  
  public int fullyConnectSync(Context paramContext, Handler paramHandler1, Handler paramHandler2) {
    int i = connectSync(paramContext, paramHandler1, paramHandler2);
    int j = i;
    if (i == 0) {
      Message message = sendMessageSynchronously(69633);
      j = message.arg1;
    } 
    return j;
  }
  
  public void connect(Context paramContext, Handler paramHandler, String paramString1, String paramString2) {
    final class ConnectAsync implements Runnable {
      String mDstClassName;
      
      String mDstPackageName;
      
      Context mSrcCtx;
      
      Handler mSrcHdlr;
      
      final AsyncChannel this$0;
      
      ConnectAsync(Context param1Context, Handler param1Handler, String param1String1, String param1String2) {
        this.mSrcCtx = param1Context;
        this.mSrcHdlr = param1Handler;
        this.mDstPackageName = param1String1;
        this.mDstClassName = param1String2;
      }
      
      public void run() {
        int i = AsyncChannel.this.connectSrcHandlerToPackageSync(this.mSrcCtx, this.mSrcHdlr, this.mDstPackageName, this.mDstClassName);
        AsyncChannel.this.replyHalfConnected(i);
      }
    };
    ConnectAsync connectAsync = new ConnectAsync(paramContext, paramHandler, paramString1, paramString2);
    (new Thread(connectAsync)).start();
  }
  
  public void connect(Context paramContext, Handler paramHandler, Class<?> paramClass) {
    connect(paramContext, paramHandler, paramClass.getPackage().getName(), paramClass.getName());
  }
  
  public void connect(Context paramContext, Handler paramHandler, Messenger paramMessenger) {
    connected(paramContext, paramHandler, paramMessenger);
    replyHalfConnected(0);
  }
  
  public void connected(Context paramContext, Handler paramHandler, Messenger paramMessenger) {
    this.mSrcContext = paramContext;
    this.mSrcHandler = paramHandler;
    this.mSrcMessenger = new Messenger(this.mSrcHandler);
    this.mDstMessenger = paramMessenger;
  }
  
  public void connect(Context paramContext, Handler paramHandler1, Handler paramHandler2) {
    connect(paramContext, paramHandler1, new Messenger(paramHandler2));
  }
  
  public void connect(AsyncService paramAsyncService, Messenger paramMessenger) {
    connect((Context)paramAsyncService, paramAsyncService.getHandler(), paramMessenger);
  }
  
  public void disconnected() {
    this.mSrcContext = null;
    this.mSrcHandler = null;
    this.mSrcMessenger = null;
    this.mDstMessenger = null;
    this.mDeathMonitor = null;
    this.mConnection = null;
  }
  
  public void disconnect() {
    AsyncChannelConnection asyncChannelConnection = this.mConnection;
    if (asyncChannelConnection != null) {
      Context context = this.mSrcContext;
      if (context != null) {
        context.unbindService(asyncChannelConnection);
        this.mConnection = null;
      } 
    } 
    try {
      Message message = Message.obtain();
      message.what = 69636;
      message.replyTo = this.mSrcMessenger;
      this.mDstMessenger.send(message);
    } catch (Exception exception) {}
    replyDisconnected(0);
    this.mSrcHandler = null;
    if (this.mConnection == null) {
      Messenger messenger = this.mDstMessenger;
      if (messenger != null && this.mDeathMonitor != null) {
        messenger.getBinder().unlinkToDeath(this.mDeathMonitor, 0);
        this.mDeathMonitor = null;
      } 
    } 
  }
  
  public void sendMessage(Message paramMessage) {
    paramMessage.replyTo = this.mSrcMessenger;
    try {
      this.mDstMessenger.send(paramMessage);
    } catch (RemoteException remoteException) {
      Handler handler = this.mSrcHandler;
      if (handler != null && handler.toString().contains("DcNetworkAgent") && 528386 == paramMessage.what) {
        remoteException.printStackTrace();
        broadcastNoDataIconError(0, 983201, 0, "binder error");
        return;
      } 
      replyDisconnected(2);
    } 
  }
  
  public void broadcastNoDataIconError(int paramInt1, int paramInt2, int paramInt3, String paramString) {
    try {
      if (this.mSrcContext == null)
        return; 
      Class<?> clazz1 = Class.forName("com.oplus.nec.OplusNecManager");
      Method method2 = clazz1.getMethod("getInstance", new Class[] { Context.class });
      Object object = method2.invoke(clazz1, new Object[] { this.mSrcContext });
      Class<?> clazz2 = object.getClass();
      Method method1 = clazz2.getDeclaredMethod("broadcastNoDataIconError", new Class[] { int.class, int.class, int.class, String.class });
      method1.invoke(object, new Object[] { Integer.valueOf(paramInt1), Integer.valueOf(paramInt2), Integer.valueOf(paramInt3), paramString });
    } catch (Exception exception) {}
  }
  
  public void sendMessage(int paramInt) {
    Message message = Message.obtain();
    message.what = paramInt;
    sendMessage(message);
  }
  
  public void sendMessage(int paramInt1, int paramInt2) {
    Message message = Message.obtain();
    message.what = paramInt1;
    message.arg1 = paramInt2;
    sendMessage(message);
  }
  
  public void sendMessage(int paramInt1, int paramInt2, int paramInt3) {
    Message message = Message.obtain();
    message.what = paramInt1;
    message.arg1 = paramInt2;
    message.arg2 = paramInt3;
    sendMessage(message);
  }
  
  public void sendMessage(int paramInt1, int paramInt2, int paramInt3, Object paramObject) {
    Message message = Message.obtain();
    message.what = paramInt1;
    message.arg1 = paramInt2;
    message.arg2 = paramInt3;
    message.obj = paramObject;
    sendMessage(message);
  }
  
  public void sendMessage(int paramInt, Object paramObject) {
    Message message = Message.obtain();
    message.what = paramInt;
    message.obj = paramObject;
    sendMessage(message);
  }
  
  public void replyToMessage(Message paramMessage1, Message paramMessage2) {
    try {
      paramMessage2.replyTo = this.mSrcMessenger;
      paramMessage1.replyTo.send(paramMessage2);
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("TODO: handle replyToMessage RemoteException");
      stringBuilder.append(remoteException);
      log(stringBuilder.toString());
      remoteException.printStackTrace();
    } 
  }
  
  public void replyToMessage(Message paramMessage, int paramInt) {
    Message message = Message.obtain();
    message.what = paramInt;
    replyToMessage(paramMessage, message);
  }
  
  public void replyToMessage(Message paramMessage, int paramInt1, int paramInt2) {
    Message message = Message.obtain();
    message.what = paramInt1;
    message.arg1 = paramInt2;
    replyToMessage(paramMessage, message);
  }
  
  public void replyToMessage(Message paramMessage, int paramInt1, int paramInt2, int paramInt3) {
    Message message = Message.obtain();
    message.what = paramInt1;
    message.arg1 = paramInt2;
    message.arg2 = paramInt3;
    replyToMessage(paramMessage, message);
  }
  
  public void replyToMessage(Message paramMessage, int paramInt1, int paramInt2, int paramInt3, Object paramObject) {
    Message message = Message.obtain();
    message.what = paramInt1;
    message.arg1 = paramInt2;
    message.arg2 = paramInt3;
    message.obj = paramObject;
    replyToMessage(paramMessage, message);
  }
  
  public void replyToMessage(Message paramMessage, int paramInt, Object paramObject) {
    Message message = Message.obtain();
    message.what = paramInt;
    message.obj = paramObject;
    replyToMessage(paramMessage, message);
  }
  
  public Message sendMessageSynchronously(Message paramMessage) {
    paramMessage = SyncMessenger.sendMessageSynchronously(this.mDstMessenger, paramMessage);
    return paramMessage;
  }
  
  public Message sendMessageSynchronously(int paramInt) {
    Message message = Message.obtain();
    message.what = paramInt;
    message = sendMessageSynchronously(message);
    return message;
  }
  
  public Message sendMessageSynchronously(int paramInt1, int paramInt2) {
    Message message = Message.obtain();
    message.what = paramInt1;
    message.arg1 = paramInt2;
    message = sendMessageSynchronously(message);
    return message;
  }
  
  public Message sendMessageSynchronously(int paramInt1, int paramInt2, int paramInt3) {
    Message message = Message.obtain();
    message.what = paramInt1;
    message.arg1 = paramInt2;
    message.arg2 = paramInt3;
    message = sendMessageSynchronously(message);
    return message;
  }
  
  public Message sendMessageSynchronously(int paramInt1, int paramInt2, int paramInt3, Object paramObject) {
    Message message = Message.obtain();
    message.what = paramInt1;
    message.arg1 = paramInt2;
    message.arg2 = paramInt3;
    message.obj = paramObject;
    paramObject = sendMessageSynchronously(message);
    return (Message)paramObject;
  }
  
  public Message sendMessageSynchronously(int paramInt, Object paramObject) {
    Message message = Message.obtain();
    message.what = paramInt;
    message.obj = paramObject;
    paramObject = sendMessageSynchronously(message);
    return (Message)paramObject;
  }
  
  private static class SyncMessenger {
    private static int sCount = 0;
    
    private static Stack<SyncMessenger> sStack = new Stack<>();
    
    private SyncHandler mHandler;
    
    private HandlerThread mHandlerThread;
    
    private Messenger mMessenger;
    
    static {
    
    }
    
    class SyncHandler extends Handler {
      private Object mLockObject = new Object();
      
      private Message mResultMsg;
      
      final AsyncChannel.SyncMessenger this$0;
      
      private SyncHandler(Looper param2Looper) {
        super(param2Looper);
      }
      
      public void handleMessage(Message param2Message) {
        null = Message.obtain();
        null.copyFrom(param2Message);
        synchronized (this.mLockObject) {
          this.mResultMsg = null;
          this.mLockObject.notify();
          return;
        } 
      }
    }
    
    private static SyncMessenger obtain() {
      synchronized (sStack) {
        SyncMessenger syncMessenger;
        if (sStack.isEmpty()) {
          syncMessenger = new SyncMessenger();
          this();
          HandlerThread handlerThread = new HandlerThread();
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("SyncHandler-");
          int i = sCount;
          sCount = i + 1;
          stringBuilder.append(i);
          this(stringBuilder.toString());
          syncMessenger.mHandlerThread = handlerThread;
          handlerThread.start();
          SyncHandler syncHandler = new SyncHandler();
          Objects.requireNonNull(syncMessenger);
          this(syncMessenger, syncMessenger.mHandlerThread.getLooper());
          syncMessenger.mHandler = syncHandler;
          Messenger messenger = new Messenger();
          this(syncMessenger.mHandler);
          syncMessenger.mMessenger = messenger;
        } else {
          syncMessenger = sStack.pop();
        } 
        return syncMessenger;
      } 
    }
    
    private void recycle() {
      synchronized (sStack) {
        sStack.push(this);
        return;
      } 
    }
    
    private static Message sendMessageSynchronously(Messenger param1Messenger, Message param1Message) {
      // Byte code:
      //   0: invokestatic obtain : ()Lcom/android/internal/util/AsyncChannel$SyncMessenger;
      //   3: astore_2
      //   4: aconst_null
      //   5: astore_3
      //   6: aconst_null
      //   7: astore #4
      //   9: aconst_null
      //   10: astore #5
      //   12: aconst_null
      //   13: astore #6
      //   15: aload #5
      //   17: astore #7
      //   19: aload_0
      //   20: ifnull -> 207
      //   23: aload #5
      //   25: astore #7
      //   27: aload_1
      //   28: ifnull -> 207
      //   31: aload_3
      //   32: astore #5
      //   34: aload #4
      //   36: astore #7
      //   38: aload_1
      //   39: aload_2
      //   40: getfield mMessenger : Landroid/os/Messenger;
      //   43: putfield replyTo : Landroid/os/Messenger;
      //   46: aload_3
      //   47: astore #5
      //   49: aload #4
      //   51: astore #7
      //   53: aload_2
      //   54: getfield mHandler : Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;
      //   57: invokestatic access$300 : (Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;)Ljava/lang/Object;
      //   60: astore #8
      //   62: aload_3
      //   63: astore #5
      //   65: aload #4
      //   67: astore #7
      //   69: aload #8
      //   71: monitorenter
      //   72: aload #6
      //   74: astore #7
      //   76: aload_2
      //   77: getfield mHandler : Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;
      //   80: invokestatic access$400 : (Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;)Landroid/os/Message;
      //   83: ifnull -> 111
      //   86: aload #6
      //   88: astore #7
      //   90: ldc 'AsyncChannel'
      //   92: ldc 'mResultMsg should be null here'
      //   94: invokestatic wtf : (Ljava/lang/String;Ljava/lang/String;)I
      //   97: pop
      //   98: aload #6
      //   100: astore #7
      //   102: aload_2
      //   103: getfield mHandler : Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;
      //   106: aconst_null
      //   107: invokestatic access$402 : (Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;Landroid/os/Message;)Landroid/os/Message;
      //   110: pop
      //   111: aload #6
      //   113: astore #7
      //   115: aload_0
      //   116: aload_1
      //   117: invokevirtual send : (Landroid/os/Message;)V
      //   120: aload #6
      //   122: astore #7
      //   124: aload_2
      //   125: getfield mHandler : Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;
      //   128: invokestatic access$300 : (Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;)Ljava/lang/Object;
      //   131: invokevirtual wait : ()V
      //   134: aload #6
      //   136: astore #7
      //   138: aload_2
      //   139: getfield mHandler : Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;
      //   142: invokestatic access$400 : (Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;)Landroid/os/Message;
      //   145: astore_0
      //   146: aload_0
      //   147: astore #7
      //   149: aload_2
      //   150: getfield mHandler : Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;
      //   153: aconst_null
      //   154: invokestatic access$402 : (Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;Landroid/os/Message;)Landroid/os/Message;
      //   157: pop
      //   158: aload_0
      //   159: astore #7
      //   161: aload #8
      //   163: monitorexit
      //   164: aload_0
      //   165: astore #7
      //   167: goto -> 207
      //   170: astore_0
      //   171: aload #8
      //   173: monitorexit
      //   174: aload #7
      //   176: astore #5
      //   178: aload_0
      //   179: athrow
      //   180: astore_0
      //   181: ldc 'AsyncChannel'
      //   183: ldc 'error in sendMessageSynchronously'
      //   185: aload_0
      //   186: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   189: pop
      //   190: aload #5
      //   192: astore #7
      //   194: goto -> 207
      //   197: astore_0
      //   198: ldc 'AsyncChannel'
      //   200: ldc 'error in sendMessageSynchronously'
      //   202: aload_0
      //   203: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   206: pop
      //   207: aload_2
      //   208: invokespecial recycle : ()V
      //   211: aload #7
      //   213: areturn
      // Line number table:
      //   Java source line number -> byte code offset
      //   #872	-> 0
      //   #873	-> 4
      //   #875	-> 15
      //   #876	-> 31
      //   #877	-> 46
      //   #878	-> 72
      //   #879	-> 86
      //   #880	-> 98
      //   #882	-> 111
      //   #883	-> 120
      //   #884	-> 134
      //   #885	-> 146
      //   #886	-> 158
      //   #890	-> 180
      //   #891	-> 181
      //   #888	-> 197
      //   #889	-> 198
      //   #892	-> 207
      //   #893	-> 207
      //   #894	-> 211
      // Exception table:
      //   from	to	target	type
      //   38	46	197	java/lang/InterruptedException
      //   38	46	180	android/os/RemoteException
      //   53	62	197	java/lang/InterruptedException
      //   53	62	180	android/os/RemoteException
      //   69	72	197	java/lang/InterruptedException
      //   69	72	180	android/os/RemoteException
      //   76	86	170	finally
      //   90	98	170	finally
      //   102	111	170	finally
      //   115	120	170	finally
      //   124	134	170	finally
      //   138	146	170	finally
      //   149	158	170	finally
      //   161	164	170	finally
      //   171	174	170	finally
      //   178	180	197	java/lang/InterruptedException
      //   178	180	180	android/os/RemoteException
    }
  }
  
  class SyncHandler extends Handler {
    private Object mLockObject = new Object();
    
    private Message mResultMsg;
    
    final AsyncChannel.SyncMessenger this$0;
    
    private SyncHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      null = Message.obtain();
      null.copyFrom(param1Message);
      synchronized (this.mLockObject) {
        this.mResultMsg = null;
        this.mLockObject.notify();
        return;
      } 
    }
  }
  
  private void replyHalfConnected(int paramInt) {
    Message message = this.mSrcHandler.obtainMessage(69632);
    message.arg1 = paramInt;
    message.obj = this;
    message.replyTo = this.mDstMessenger;
    if (!linkToDeathMonitor())
      message.arg1 = 1; 
    this.mSrcHandler.sendMessage(message);
  }
  
  private boolean linkToDeathMonitor() {
    if (this.mConnection == null && this.mDeathMonitor == null) {
      this.mDeathMonitor = new DeathMonitor();
      try {
        this.mDstMessenger.getBinder().linkToDeath(this.mDeathMonitor, 0);
      } catch (RemoteException remoteException) {
        this.mDeathMonitor = null;
        return false;
      } 
    } 
    return true;
  }
  
  private void replyDisconnected(int paramInt) {
    Handler handler = this.mSrcHandler;
    if (handler == null)
      return; 
    Message message = handler.obtainMessage(69636);
    message.arg1 = paramInt;
    message.obj = this;
    message.replyTo = this.mDstMessenger;
    this.mSrcHandler.sendMessage(message);
  }
  
  class AsyncChannelConnection implements ServiceConnection {
    final AsyncChannel this$0;
    
    public void onServiceConnected(ComponentName param1ComponentName, IBinder param1IBinder) {
      AsyncChannel.access$502(AsyncChannel.this, new Messenger(param1IBinder));
      AsyncChannel.this.replyHalfConnected(0);
    }
    
    public void onServiceDisconnected(ComponentName param1ComponentName) {
      AsyncChannel.this.replyDisconnected(0);
    }
  }
  
  private static void log(String paramString) {
    Log.d("AsyncChannel", paramString);
  }
  
  class DeathMonitor implements IBinder.DeathRecipient {
    final AsyncChannel this$0;
    
    public void binderDied() {
      AsyncChannel.this.replyDisconnected(4);
    }
  }
}
