package com.android.internal.util;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.net.ProtocolException;
import java.nio.charset.StandardCharsets;

public class ProcFileReader implements Closeable {
  private final byte[] mBuffer;
  
  private boolean mLineFinished;
  
  private final InputStream mStream;
  
  private int mTail;
  
  public ProcFileReader(InputStream paramInputStream) throws IOException {
    this(paramInputStream, 4096);
  }
  
  public ProcFileReader(InputStream paramInputStream, int paramInt) throws IOException {
    this.mStream = paramInputStream;
    this.mBuffer = new byte[paramInt];
    fillBuf();
  }
  
  private int fillBuf() throws IOException {
    byte[] arrayOfByte = this.mBuffer;
    int i = arrayOfByte.length, j = this.mTail;
    i -= j;
    if (i != 0) {
      j = this.mStream.read(arrayOfByte, j, i);
      if (j != -1)
        this.mTail += j; 
      return j;
    } 
    throw new IOException("attempting to fill already-full buffer");
  }
  
  private void consumeBuf(int paramInt) throws IOException {
    byte[] arrayOfByte = this.mBuffer;
    System.arraycopy(arrayOfByte, paramInt, arrayOfByte, 0, this.mTail - paramInt);
    this.mTail = paramInt = this.mTail - paramInt;
    if (paramInt == 0)
      fillBuf(); 
  }
  
  private int nextTokenIndex() throws IOException {
    if (this.mLineFinished)
      return -1; 
    byte b = 0;
    while (true) {
      for (; b < this.mTail; b++) {
        byte b1 = this.mBuffer[b];
        if (b1 == 10) {
          this.mLineFinished = true;
          return b;
        } 
        if (b1 == 32)
          return b; 
      } 
      if (fillBuf() > 0)
        continue; 
      break;
    } 
    throw new ProtocolException("End of stream while looking for token boundary");
  }
  
  public boolean hasMoreData() {
    boolean bool;
    if (this.mTail > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void finishLine() throws IOException {
    if (this.mLineFinished) {
      this.mLineFinished = false;
      return;
    } 
    byte b = 0;
    while (true) {
      for (; b < this.mTail; b++) {
        if (this.mBuffer[b] == 10) {
          consumeBuf(b + 1);
          return;
        } 
      } 
      if (fillBuf() > 0)
        continue; 
      break;
    } 
    throw new ProtocolException("End of stream while looking for line boundary");
  }
  
  public String nextString() throws IOException {
    int i = nextTokenIndex();
    if (i != -1)
      return parseAndConsumeString(i); 
    throw new ProtocolException("Missing required string");
  }
  
  public long nextLong() throws IOException {
    int i = nextTokenIndex();
    if (i != -1)
      return parseAndConsumeLong(i); 
    throw new ProtocolException("Missing required long");
  }
  
  public long nextOptionalLong(long paramLong) throws IOException {
    int i = nextTokenIndex();
    if (i == -1)
      return paramLong; 
    return parseAndConsumeLong(i);
  }
  
  private String parseAndConsumeString(int paramInt) throws IOException {
    String str = new String(this.mBuffer, 0, paramInt, StandardCharsets.US_ASCII);
    consumeBuf(paramInt + 1);
    return str;
  }
  
  private long parseAndConsumeLong(int paramInt) throws IOException {
    boolean bool;
    byte[] arrayOfByte = this.mBuffer;
    byte b = 0;
    if (arrayOfByte[0] == 45) {
      bool = true;
    } else {
      bool = false;
    } 
    long l1 = 0L;
    long l2 = l1;
    if (bool) {
      b = 1;
      l2 = l1;
    } 
    while (b < paramInt) {
      int i = this.mBuffer[b] - 48;
      if (i >= 0 && i <= 9) {
        l1 = 10L * l2 - i;
        if (l1 <= l2) {
          l2 = l1;
          b++;
        } 
        throw invalidLong(paramInt);
      } 
      throw invalidLong(paramInt);
    } 
    consumeBuf(paramInt + 1);
    if (!bool)
      l2 = -l2; 
    return l2;
  }
  
  private NumberFormatException invalidLong(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("invalid long: ");
    stringBuilder.append(new String(this.mBuffer, 0, paramInt, StandardCharsets.US_ASCII));
    return new NumberFormatException(stringBuilder.toString());
  }
  
  public int nextInt() throws IOException {
    long l = nextLong();
    if (l <= 2147483647L && l >= -2147483648L)
      return (int)l; 
    throw new NumberFormatException("parsed value larger than integer");
  }
  
  public void close() throws IOException {
    this.mStream.close();
  }
}
