package com.android.internal.util;

public final class VirtualRefBasePtr {
  private long mNativePtr;
  
  public VirtualRefBasePtr(long paramLong) {
    this.mNativePtr = paramLong;
    nIncStrong(paramLong);
  }
  
  public long get() {
    return this.mNativePtr;
  }
  
  public void release() {
    long l = this.mNativePtr;
    if (l != 0L) {
      nDecStrong(l);
      this.mNativePtr = 0L;
    } 
  }
  
  protected void finalize() throws Throwable {
    try {
      release();
      return;
    } finally {
      super.finalize();
    } 
  }
  
  private static native void nDecStrong(long paramLong);
  
  private static native void nIncStrong(long paramLong);
}
