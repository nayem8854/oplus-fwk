package com.android.internal.util;

public class FastMath {
  public static int round(float paramFloat) {
    long l = (long)(1.6777216E7F * paramFloat);
    return (int)(8388608L + l >> 24L);
  }
}
