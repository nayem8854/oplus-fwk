package com.android.internal.util;

import android.os.SystemClock;
import android.util.Slog;
import android.util.proto.ProtoOutputStream;
import java.io.PrintWriter;

public class StatLogger {
  private static final String TAG = "StatLogger";
  
  private final int SIZE;
  
  private final int[] mCallsPerSecond;
  
  private final int[] mCountStats;
  
  private final long[] mDurationPerSecond;
  
  private final long[] mDurationStats;
  
  private final String[] mLabels;
  
  private final Object mLock = new Object();
  
  private final int[] mMaxCallsPerSecond;
  
  private final long[] mMaxDurationPerSecond;
  
  private final long[] mMaxDurationStats;
  
  private long mNextTickTime = SystemClock.elapsedRealtime() + 1000L;
  
  public StatLogger(String[] paramArrayOfString) {
    int i = paramArrayOfString.length;
    this.mCountStats = new int[i];
    this.mDurationStats = new long[i];
    this.mCallsPerSecond = new int[i];
    this.mMaxCallsPerSecond = new int[i];
    this.mDurationPerSecond = new long[i];
    this.mMaxDurationPerSecond = new long[i];
    this.mMaxDurationStats = new long[i];
    this.mLabels = paramArrayOfString;
  }
  
  public long getTime() {
    return SystemClock.elapsedRealtimeNanos() / 1000L;
  }
  
  public long logDurationStat(int paramInt, long paramLong) {
    synchronized (this.mLock) {
      long l = getTime() - paramLong;
      if (paramInt >= 0 && paramInt < this.SIZE) {
        int[] arrayOfInt2 = this.mCountStats;
        arrayOfInt2[paramInt] = arrayOfInt2[paramInt] + 1;
        long[] arrayOfLong2 = this.mDurationStats;
        arrayOfLong2[paramInt] = arrayOfLong2[paramInt] + l;
        if (this.mMaxDurationStats[paramInt] < l)
          this.mMaxDurationStats[paramInt] = l; 
        paramLong = SystemClock.elapsedRealtime();
        if (paramLong > this.mNextTickTime) {
          if (this.mMaxCallsPerSecond[paramInt] < this.mCallsPerSecond[paramInt])
            this.mMaxCallsPerSecond[paramInt] = this.mCallsPerSecond[paramInt]; 
          if (this.mMaxDurationPerSecond[paramInt] < this.mDurationPerSecond[paramInt])
            this.mMaxDurationPerSecond[paramInt] = this.mDurationPerSecond[paramInt]; 
          this.mCallsPerSecond[paramInt] = 0;
          this.mDurationPerSecond[paramInt] = 0L;
          this.mNextTickTime = 1000L + paramLong;
        } 
        int[] arrayOfInt1 = this.mCallsPerSecond;
        arrayOfInt1[paramInt] = arrayOfInt1[paramInt] + 1;
        long[] arrayOfLong1 = this.mDurationPerSecond;
        arrayOfLong1[paramInt] = arrayOfLong1[paramInt] + l;
        return l;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Invalid event ID: ");
      stringBuilder.append(paramInt);
      Slog.wtf("StatLogger", stringBuilder.toString());
      return l;
    } 
  }
  
  public void dump(PrintWriter paramPrintWriter, String paramString) {
    dump((new IndentingPrintWriter(paramPrintWriter, "  ")).setIndent(paramString));
  }
  
  public void dump(IndentingPrintWriter paramIndentingPrintWriter) {
    synchronized (this.mLock) {
      paramIndentingPrintWriter.println("Stats:");
      paramIndentingPrintWriter.increaseIndent();
      for (byte b = 0; b < this.SIZE; b++) {
        double d2;
        int i = this.mCountStats[b];
        double d1 = this.mDurationStats[b] / 1000.0D;
        String str = this.mLabels[b];
        if (i == 0) {
          d2 = 0.0D;
        } else {
          d2 = d1 / i;
        } 
        int j = this.mMaxCallsPerSecond[b];
        double d3 = this.mMaxDurationPerSecond[b] / 1000.0D, d4 = this.mMaxDurationStats[b] / 1000.0D;
        paramIndentingPrintWriter.println(String.format("%s: count=%d, total=%.1fms, avg=%.3fms, max calls/s=%d max dur/s=%.1fms max time=%.1fms", new Object[] { str, Integer.valueOf(i), Double.valueOf(d1), Double.valueOf(d2), Integer.valueOf(j), Double.valueOf(d3), Double.valueOf(d4) }));
      } 
      paramIndentingPrintWriter.decreaseIndent();
      return;
    } 
  }
  
  public void dumpProto(ProtoOutputStream paramProtoOutputStream, long paramLong) {
    synchronized (this.mLock) {
      long l = paramProtoOutputStream.start(paramLong);
      for (byte b = 0; b < this.mLabels.length; b++) {
        paramLong = paramProtoOutputStream.start(2246267895809L);
        paramProtoOutputStream.write(1120986464257L, b);
        paramProtoOutputStream.write(1138166333442L, this.mLabels[b]);
        paramProtoOutputStream.write(1120986464259L, this.mCountStats[b]);
        paramProtoOutputStream.write(1112396529668L, this.mDurationStats[b]);
        paramProtoOutputStream.write(1120986464261L, this.mMaxCallsPerSecond[b]);
        paramProtoOutputStream.write(1112396529670L, this.mMaxDurationPerSecond[b]);
        paramProtoOutputStream.write(1112396529671L, this.mMaxDurationStats[b]);
        paramProtoOutputStream.end(paramLong);
      } 
      paramProtoOutputStream.end(l);
      return;
    } 
  }
}
