package com.android.internal.util;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

public abstract class AsyncService extends Service {
  public static final int CMD_ASYNC_SERVICE_DESTROY = 16777216;
  
  public static final int CMD_ASYNC_SERVICE_ON_START_INTENT = 16777215;
  
  protected static final boolean DBG = true;
  
  private static final String TAG = "AsyncService";
  
  AsyncServiceInfo mAsyncServiceInfo;
  
  Handler mHandler;
  
  protected Messenger mMessenger;
  
  class AsyncServiceInfo {
    public Handler mHandler;
    
    public int mRestartFlags;
  }
  
  public Handler getHandler() {
    return this.mHandler;
  }
  
  public void onCreate() {
    super.onCreate();
    AsyncServiceInfo asyncServiceInfo = createHandler();
    this.mHandler = asyncServiceInfo.mHandler;
    this.mMessenger = new Messenger(this.mHandler);
  }
  
  public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2) {
    Log.d("AsyncService", "onStartCommand");
    Message message = this.mHandler.obtainMessage();
    message.what = 16777215;
    message.arg1 = paramInt1;
    message.arg2 = paramInt2;
    message.obj = paramIntent;
    this.mHandler.sendMessage(message);
    return this.mAsyncServiceInfo.mRestartFlags;
  }
  
  public void onDestroy() {
    Log.d("AsyncService", "onDestroy");
    Message message = this.mHandler.obtainMessage();
    message.what = 16777216;
    this.mHandler.sendMessage(message);
  }
  
  public IBinder onBind(Intent paramIntent) {
    return this.mMessenger.getBinder();
  }
  
  public abstract AsyncServiceInfo createHandler();
}
