package com.android.internal.util;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Optional;
import java.util.Vector;
import java.util.stream.Stream;

public class StateMachine {
  public static final boolean HANDLED = true;
  
  public static final boolean NOT_HANDLED = false;
  
  private static final int SM_INIT_CMD = -2;
  
  private static final int SM_QUIT_CMD = -1;
  
  private String mName;
  
  private SmHandler mSmHandler;
  
  private HandlerThread mSmThread;
  
  public static class LogRec {
    private IState mDstState;
    
    private String mInfo;
    
    private IState mOrgState;
    
    private StateMachine mSm;
    
    private IState mState;
    
    private long mTime;
    
    private int mWhat;
    
    LogRec(StateMachine param1StateMachine, Message param1Message, String param1String, IState param1IState1, IState param1IState2, IState param1IState3) {
      update(param1StateMachine, param1Message, param1String, param1IState1, param1IState2, param1IState3);
    }
    
    public void update(StateMachine param1StateMachine, Message param1Message, String param1String, IState param1IState1, IState param1IState2, IState param1IState3) {
      boolean bool;
      this.mSm = param1StateMachine;
      this.mTime = System.currentTimeMillis();
      if (param1Message != null) {
        bool = param1Message.what;
      } else {
        bool = false;
      } 
      this.mWhat = bool;
      this.mInfo = param1String;
      this.mState = param1IState1;
      this.mOrgState = param1IState2;
      this.mDstState = param1IState3;
    }
    
    public long getTime() {
      return this.mTime;
    }
    
    public long getWhat() {
      return this.mWhat;
    }
    
    public String getInfo() {
      return this.mInfo;
    }
    
    public IState getState() {
      return this.mState;
    }
    
    public IState getDestState() {
      return this.mDstState;
    }
    
    public IState getOriginalState() {
      return this.mOrgState;
    }
    
    public String toString() {
      String str4, str3, str2, str1;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("time=");
      Calendar calendar = Calendar.getInstance();
      calendar.setTimeInMillis(this.mTime);
      stringBuilder.append(String.format("%tm-%td %tH:%tM:%tS.%tL", new Object[] { calendar, calendar, calendar, calendar, calendar, calendar }));
      stringBuilder.append(" processed=");
      IState iState3 = this.mState;
      String str5 = "<null>";
      if (iState3 == null) {
        str4 = "<null>";
      } else {
        str4 = str4.getName();
      } 
      stringBuilder.append(str4);
      stringBuilder.append(" org=");
      IState iState2 = this.mOrgState;
      if (iState2 == null) {
        str3 = "<null>";
      } else {
        str3 = str3.getName();
      } 
      stringBuilder.append(str3);
      stringBuilder.append(" dest=");
      IState iState1 = this.mDstState;
      if (iState1 == null) {
        str2 = str5;
      } else {
        str2 = str2.getName();
      } 
      stringBuilder.append(str2);
      stringBuilder.append(" what=");
      StateMachine stateMachine = this.mSm;
      if (stateMachine != null) {
        str1 = stateMachine.getWhatToString(this.mWhat);
      } else {
        str1 = "";
      } 
      if (TextUtils.isEmpty(str1)) {
        stringBuilder.append(this.mWhat);
        stringBuilder.append("(0x");
        stringBuilder.append(Integer.toHexString(this.mWhat));
        stringBuilder.append(")");
      } else {
        stringBuilder.append(str1);
      } 
      if (!TextUtils.isEmpty(this.mInfo)) {
        stringBuilder.append(" ");
        stringBuilder.append(this.mInfo);
      } 
      return stringBuilder.toString();
    }
  }
  
  private static class LogRecords {
    private Vector<StateMachine.LogRec> mLogRecVector = new Vector<>();
    
    private int mMaxSize = 20;
    
    private int mOldestIndex = 0;
    
    private int mCount = 0;
    
    private boolean mLogOnlyTransitions = false;
    
    private static final int DEFAULT_SIZE = 20;
    
    void setSize(int param1Int) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: iload_1
      //   4: putfield mMaxSize : I
      //   7: aload_0
      //   8: iconst_0
      //   9: putfield mOldestIndex : I
      //   12: aload_0
      //   13: iconst_0
      //   14: putfield mCount : I
      //   17: aload_0
      //   18: getfield mLogRecVector : Ljava/util/Vector;
      //   21: invokevirtual clear : ()V
      //   24: aload_0
      //   25: monitorexit
      //   26: return
      //   27: astore_2
      //   28: aload_0
      //   29: monitorexit
      //   30: aload_2
      //   31: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #601	-> 2
      //   #602	-> 7
      //   #603	-> 12
      //   #604	-> 17
      //   #605	-> 24
      //   #600	-> 27
      // Exception table:
      //   from	to	target	type
      //   2	7	27	finally
      //   7	12	27	finally
      //   12	17	27	finally
      //   17	24	27	finally
    }
    
    void setLogOnlyTransitions(boolean param1Boolean) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: iload_1
      //   4: putfield mLogOnlyTransitions : Z
      //   7: aload_0
      //   8: monitorexit
      //   9: return
      //   10: astore_2
      //   11: aload_0
      //   12: monitorexit
      //   13: aload_2
      //   14: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #608	-> 2
      //   #609	-> 7
      //   #607	-> 10
      // Exception table:
      //   from	to	target	type
      //   2	7	10	finally
    }
    
    boolean logOnlyTransitions() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mLogOnlyTransitions : Z
      //   6: istore_1
      //   7: aload_0
      //   8: monitorexit
      //   9: iload_1
      //   10: ireturn
      //   11: astore_2
      //   12: aload_0
      //   13: monitorexit
      //   14: aload_2
      //   15: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #612	-> 2
      //   #612	-> 11
      // Exception table:
      //   from	to	target	type
      //   2	7	11	finally
    }
    
    int size() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mLogRecVector : Ljava/util/Vector;
      //   6: invokevirtual size : ()I
      //   9: istore_1
      //   10: aload_0
      //   11: monitorexit
      //   12: iload_1
      //   13: ireturn
      //   14: astore_2
      //   15: aload_0
      //   16: monitorexit
      //   17: aload_2
      //   18: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #619	-> 2
      //   #619	-> 14
      // Exception table:
      //   from	to	target	type
      //   2	10	14	finally
    }
    
    int count() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mCount : I
      //   6: istore_1
      //   7: aload_0
      //   8: monitorexit
      //   9: iload_1
      //   10: ireturn
      //   11: astore_2
      //   12: aload_0
      //   13: monitorexit
      //   14: aload_2
      //   15: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #626	-> 2
      //   #626	-> 11
      // Exception table:
      //   from	to	target	type
      //   2	7	11	finally
    }
    
    void cleanup() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mLogRecVector : Ljava/util/Vector;
      //   6: invokevirtual clear : ()V
      //   9: aload_0
      //   10: monitorexit
      //   11: return
      //   12: astore_1
      //   13: aload_0
      //   14: monitorexit
      //   15: aload_1
      //   16: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #633	-> 2
      //   #634	-> 9
      //   #632	-> 12
      // Exception table:
      //   from	to	target	type
      //   2	9	12	finally
    }
    
    StateMachine.LogRec get(int param1Int) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mOldestIndex : I
      //   6: iload_1
      //   7: iadd
      //   8: istore_2
      //   9: iload_2
      //   10: istore_1
      //   11: iload_2
      //   12: aload_0
      //   13: getfield mMaxSize : I
      //   16: if_icmplt -> 26
      //   19: iload_2
      //   20: aload_0
      //   21: getfield mMaxSize : I
      //   24: isub
      //   25: istore_1
      //   26: aload_0
      //   27: invokevirtual size : ()I
      //   30: istore_2
      //   31: iload_1
      //   32: iload_2
      //   33: if_icmplt -> 40
      //   36: aload_0
      //   37: monitorexit
      //   38: aconst_null
      //   39: areturn
      //   40: aload_0
      //   41: getfield mLogRecVector : Ljava/util/Vector;
      //   44: iload_1
      //   45: invokevirtual get : (I)Ljava/lang/Object;
      //   48: checkcast com/android/internal/util/StateMachine$LogRec
      //   51: astore_3
      //   52: aload_0
      //   53: monitorexit
      //   54: aload_3
      //   55: areturn
      //   56: astore_3
      //   57: aload_0
      //   58: monitorexit
      //   59: aload_3
      //   60: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #642	-> 2
      //   #643	-> 9
      //   #644	-> 19
      //   #646	-> 26
      //   #647	-> 36
      //   #649	-> 40
      //   #641	-> 56
      // Exception table:
      //   from	to	target	type
      //   2	9	56	finally
      //   11	19	56	finally
      //   19	26	56	finally
      //   26	31	56	finally
      //   40	52	56	finally
    }
    
    void add(StateMachine param1StateMachine, Message param1Message, String param1String, IState param1IState1, IState param1IState2, IState param1IState3) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: aload_0
      //   4: getfield mCount : I
      //   7: iconst_1
      //   8: iadd
      //   9: putfield mCount : I
      //   12: aload_0
      //   13: getfield mLogRecVector : Ljava/util/Vector;
      //   16: invokevirtual size : ()I
      //   19: aload_0
      //   20: getfield mMaxSize : I
      //   23: if_icmpge -> 62
      //   26: aload_0
      //   27: getfield mLogRecVector : Ljava/util/Vector;
      //   30: astore #7
      //   32: new com/android/internal/util/StateMachine$LogRec
      //   35: astore #8
      //   37: aload #8
      //   39: aload_1
      //   40: aload_2
      //   41: aload_3
      //   42: aload #4
      //   44: aload #5
      //   46: aload #6
      //   48: invokespecial <init> : (Lcom/android/internal/util/StateMachine;Landroid/os/Message;Ljava/lang/String;Lcom/android/internal/util/IState;Lcom/android/internal/util/IState;Lcom/android/internal/util/IState;)V
      //   51: aload #7
      //   53: aload #8
      //   55: invokevirtual add : (Ljava/lang/Object;)Z
      //   58: pop
      //   59: goto -> 120
      //   62: aload_0
      //   63: getfield mLogRecVector : Ljava/util/Vector;
      //   66: aload_0
      //   67: getfield mOldestIndex : I
      //   70: invokevirtual get : (I)Ljava/lang/Object;
      //   73: checkcast com/android/internal/util/StateMachine$LogRec
      //   76: astore #7
      //   78: aload_0
      //   79: getfield mOldestIndex : I
      //   82: iconst_1
      //   83: iadd
      //   84: istore #9
      //   86: aload_0
      //   87: iload #9
      //   89: putfield mOldestIndex : I
      //   92: iload #9
      //   94: aload_0
      //   95: getfield mMaxSize : I
      //   98: if_icmplt -> 106
      //   101: aload_0
      //   102: iconst_0
      //   103: putfield mOldestIndex : I
      //   106: aload #7
      //   108: aload_1
      //   109: aload_2
      //   110: aload_3
      //   111: aload #4
      //   113: aload #5
      //   115: aload #6
      //   117: invokevirtual update : (Lcom/android/internal/util/StateMachine;Landroid/os/Message;Ljava/lang/String;Lcom/android/internal/util/IState;Lcom/android/internal/util/IState;Lcom/android/internal/util/IState;)V
      //   120: aload_0
      //   121: monitorexit
      //   122: return
      //   123: astore_1
      //   124: aload_0
      //   125: monitorexit
      //   126: aload_1
      //   127: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #667	-> 2
      //   #668	-> 12
      //   #669	-> 26
      //   #671	-> 62
      //   #672	-> 78
      //   #673	-> 92
      //   #674	-> 101
      //   #676	-> 106
      //   #678	-> 120
      //   #666	-> 123
      // Exception table:
      //   from	to	target	type
      //   2	12	123	finally
      //   12	26	123	finally
      //   26	59	123	finally
      //   62	78	123	finally
      //   78	92	123	finally
      //   92	101	123	finally
      //   101	106	123	finally
      //   106	120	123	finally
    }
    
    private LogRecords() {}
  }
  
  class SmHandler extends Handler {
    private boolean mHasQuit = false;
    
    private boolean mDbg = false;
    
    private static final Object mSmHandlerObj = new Object();
    
    private StateMachine.LogRecords mLogRecords = new StateMachine.LogRecords();
    
    private int mStateStackTopIndex = -1;
    
    private HaltingState mHaltingState = new HaltingState();
    
    private QuittingState mQuittingState = new QuittingState();
    
    class StateInfo {
      boolean active;
      
      StateInfo parentStateInfo;
      
      State state;
      
      final StateMachine.SmHandler this$0;
      
      private StateInfo() {}
      
      public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("state=");
        stringBuilder.append(this.state.getName());
        stringBuilder.append(",active=");
        stringBuilder.append(this.active);
        stringBuilder.append(",parent=");
        StateInfo stateInfo = this.parentStateInfo;
        if (stateInfo == null) {
          null = "null";
        } else {
          null = ((StateInfo)null).state.getName();
        } 
        stringBuilder.append(null);
        return stringBuilder.toString();
      }
    }
    
    private HashMap<State, StateInfo> mStateInfo = new HashMap<>();
    
    private boolean mTransitionInProgress = false;
    
    private ArrayList<Message> mDeferredMessages = new ArrayList<>();
    
    private State mDestState;
    
    private State mInitialState;
    
    private boolean mIsConstructionCompleted;
    
    private Message mMsg;
    
    private StateMachine mSm;
    
    private StateInfo[] mStateStack;
    
    private StateInfo[] mTempStateStack;
    
    private int mTempStateStackCount;
    
    class HaltingState extends State {
      final StateMachine.SmHandler this$0;
      
      private HaltingState() {}
      
      public boolean processMessage(Message param2Message) {
        StateMachine.SmHandler.this.mSm.haltedProcessMessage(param2Message);
        return true;
      }
    }
    
    class QuittingState extends State {
      final StateMachine.SmHandler this$0;
      
      private QuittingState() {}
      
      public boolean processMessage(Message param2Message) {
        return false;
      }
    }
    
    public final void handleMessage(Message param1Message) {
      if (!this.mHasQuit) {
        StringBuilder stringBuilder;
        if (this.mSm != null && param1Message.what != -2 && param1Message.what != -1)
          this.mSm.onPreHandleMessage(param1Message); 
        if (this.mDbg) {
          StateMachine stateMachine = this.mSm;
          stringBuilder = new StringBuilder();
          stringBuilder.append("handleMessage: E msg.what=");
          stringBuilder.append(param1Message.what);
          stateMachine.log(stringBuilder.toString());
        } 
        this.mMsg = param1Message;
        State state = null;
        if (this.mIsConstructionCompleted || param1Message.what == -1) {
          state = processMsg(param1Message);
        } else if (!this.mIsConstructionCompleted && this.mMsg.what == -2 && this.mMsg.obj == mSmHandlerObj) {
          this.mIsConstructionCompleted = true;
          invokeEnterMethods(0);
        } else {
          stringBuilder = new StringBuilder();
          stringBuilder.append("StateMachine.handleMessage: The start method not called, received msg: ");
          stringBuilder.append(param1Message);
          throw new RuntimeException(stringBuilder.toString());
        } 
        performTransitions((State)stringBuilder, param1Message);
        if (this.mDbg) {
          StateMachine stateMachine = this.mSm;
          if (stateMachine != null)
            stateMachine.log("handleMessage: X"); 
        } 
        if (this.mSm != null && param1Message.what != -2 && param1Message.what != -1)
          this.mSm.onPostHandleMessage(param1Message); 
      } 
    }
    
    private void performTransitions(State param1State, Message param1Message) {
      int i;
      State state2 = (this.mStateStack[this.mStateStackTopIndex]).state;
      if (this.mSm.recordLogRec(this.mMsg) && param1Message.obj != mSmHandlerObj) {
        i = 1;
      } else {
        i = 0;
      } 
      if (this.mLogRecords.logOnlyTransitions()) {
        if (this.mDestState != null) {
          StateMachine.LogRecords logRecords = this.mLogRecords;
          StateMachine stateMachine = this.mSm;
          Message message = this.mMsg;
          logRecords.add(stateMachine, message, stateMachine.getLogRecString(message), param1State, state2, this.mDestState);
        } 
      } else if (i) {
        StateMachine.LogRecords logRecords = this.mLogRecords;
        StateMachine stateMachine = this.mSm;
        Message message = this.mMsg;
        logRecords.add(stateMachine, message, stateMachine.getLogRecString(message), param1State, state2, this.mDestState);
      } 
      param1State = this.mDestState;
      State state1 = param1State;
      if (param1State != null) {
        while (true) {
          if (this.mDbg)
            this.mSm.log("handleMessage: new destination call exit/enter"); 
          StateInfo stateInfo = setupTempStateStackWithStatesToEnter(param1State);
          this.mTransitionInProgress = true;
          invokeExitMethods(stateInfo);
          i = moveTempStateStackToStateStack();
          invokeEnterMethods(i);
          moveDeferredMessageAtFrontOfQueue();
          if (param1State != this.mDestState) {
            param1State = this.mDestState;
            continue;
          } 
          break;
        } 
        this.mDestState = null;
        state1 = param1State;
      } 
      if (state1 != null)
        if (state1 == this.mQuittingState) {
          this.mSm.onQuitting();
          cleanupAfterQuitting();
        } else if (state1 == this.mHaltingState) {
          this.mSm.onHalting();
        }  
    }
    
    private final void cleanupAfterQuitting() {
      if (this.mSm.mSmThread != null) {
        getLooper().quit();
        StateMachine.access$402(this.mSm, null);
      } 
      StateMachine.access$502(this.mSm, null);
      this.mSm = null;
      this.mMsg = null;
      this.mLogRecords.cleanup();
      this.mStateStack = null;
      this.mTempStateStack = null;
      this.mStateInfo.clear();
      this.mInitialState = null;
      this.mDestState = null;
      this.mDeferredMessages.clear();
      this.mHasQuit = true;
    }
    
    private final void completeConstruction() {
      if (this.mDbg)
        this.mSm.log("completeConstruction: E"); 
      byte b = 0;
      for (StateInfo stateInfo : this.mStateInfo.values()) {
        byte b1 = 0;
        for (; stateInfo != null; b1++)
          stateInfo = stateInfo.parentStateInfo; 
        byte b2 = b;
        if (b < b1)
          b2 = b1; 
        b = b2;
      } 
      if (this.mDbg) {
        StateMachine stateMachine = this.mSm;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("completeConstruction: maxDepth=");
        stringBuilder.append(b);
        stateMachine.log(stringBuilder.toString());
      } 
      this.mStateStack = new StateInfo[b];
      this.mTempStateStack = new StateInfo[b];
      setupInitialStateStack();
      sendMessageAtFrontOfQueue(obtainMessage(-2, mSmHandlerObj));
      if (this.mDbg)
        this.mSm.log("completeConstruction: X"); 
    }
    
    private final State processMsg(Message param1Message) {
      StateInfo stateInfo1 = this.mStateStack[this.mStateStackTopIndex];
      if (this.mDbg) {
        StateMachine stateMachine = this.mSm;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("processMsg: ");
        stringBuilder.append(stateInfo1.state.getName());
        stateMachine.log(stringBuilder.toString());
      } 
      StateInfo stateInfo2 = stateInfo1;
      if (isQuit(param1Message)) {
        transitionTo(this.mQuittingState);
      } else {
        while (true) {
          stateInfo1 = stateInfo2;
          if (!stateInfo2.state.processMessage(param1Message)) {
            stateInfo1 = stateInfo2.parentStateInfo;
            if (stateInfo1 == null) {
              this.mSm.unhandledMessage(param1Message);
              break;
            } 
            stateInfo2 = stateInfo1;
            if (this.mDbg) {
              StateMachine stateMachine = this.mSm;
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("processMsg: ");
              stringBuilder.append(stateInfo1.state.getName());
              stateMachine.log(stringBuilder.toString());
              StateInfo stateInfo = stateInfo1;
            } 
            continue;
          } 
          break;
        } 
      } 
      if (stateInfo1 != null) {
        State state = stateInfo1.state;
      } else {
        param1Message = null;
      } 
      return (State)param1Message;
    }
    
    private final void invokeExitMethods(StateInfo param1StateInfo) {
      while (true) {
        int i = this.mStateStackTopIndex;
        if (i >= 0) {
          StateInfo[] arrayOfStateInfo = this.mStateStack;
          if (arrayOfStateInfo[i] != param1StateInfo) {
            State state = (arrayOfStateInfo[i]).state;
            if (this.mDbg) {
              StateMachine stateMachine = this.mSm;
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("invokeExitMethods: ");
              stringBuilder.append(state.getName());
              stateMachine.log(stringBuilder.toString());
            } 
            state.exit();
            (this.mStateStack[this.mStateStackTopIndex]).active = false;
            this.mStateStackTopIndex--;
            continue;
          } 
        } 
        break;
      } 
    }
    
    private final void invokeEnterMethods(int param1Int) {
      int i = param1Int;
      while (true) {
        int j = this.mStateStackTopIndex;
        if (i <= j) {
          if (param1Int == j)
            this.mTransitionInProgress = false; 
          if (this.mDbg) {
            StateMachine stateMachine = this.mSm;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("invokeEnterMethods: ");
            stringBuilder.append((this.mStateStack[i]).state.getName());
            stateMachine.log(stringBuilder.toString());
          } 
          (this.mStateStack[i]).state.enter();
          (this.mStateStack[i]).active = true;
          i++;
          continue;
        } 
        break;
      } 
      this.mTransitionInProgress = false;
    }
    
    private final void moveDeferredMessageAtFrontOfQueue() {
      for (int i = this.mDeferredMessages.size() - 1; i >= 0; i--) {
        Message message = this.mDeferredMessages.get(i);
        if (this.mDbg) {
          StateMachine stateMachine = this.mSm;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("moveDeferredMessageAtFrontOfQueue; what=");
          stringBuilder.append(message.what);
          stateMachine.log(stringBuilder.toString());
        } 
        sendMessageAtFrontOfQueue(message);
      } 
      this.mDeferredMessages.clear();
    }
    
    private final int moveTempStateStackToStateStack() {
      int i = this.mStateStackTopIndex + 1;
      int j = this.mTempStateStackCount - 1;
      int k = i;
      while (j >= 0) {
        if (this.mDbg) {
          StateMachine stateMachine = this.mSm;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("moveTempStackToStateStack: i=");
          stringBuilder.append(j);
          stringBuilder.append(",j=");
          stringBuilder.append(k);
          stateMachine.log(stringBuilder.toString());
        } 
        this.mStateStack[k] = this.mTempStateStack[j];
        k++;
        j--;
      } 
      this.mStateStackTopIndex = k - 1;
      if (this.mDbg) {
        StateMachine stateMachine = this.mSm;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("moveTempStackToStateStack: X mStateStackTop=");
        stringBuilder.append(this.mStateStackTopIndex);
        stringBuilder.append(",startingIndex=");
        stringBuilder.append(i);
        stringBuilder.append(",Top=");
        State state = (this.mStateStack[this.mStateStackTopIndex]).state;
        stringBuilder.append(state.getName());
        String str = stringBuilder.toString();
        stateMachine.log(str);
      } 
      return i;
    }
    
    private final StateInfo setupTempStateStackWithStatesToEnter(State param1State) {
      StateInfo stateInfo2;
      this.mTempStateStackCount = 0;
      StateInfo stateInfo1 = this.mStateInfo.get(param1State);
      while (true) {
        StateInfo[] arrayOfStateInfo = this.mTempStateStack;
        int i = this.mTempStateStackCount;
        this.mTempStateStackCount = i + 1;
        arrayOfStateInfo[i] = stateInfo1;
        stateInfo2 = stateInfo1.parentStateInfo;
        if (stateInfo2 != null) {
          stateInfo1 = stateInfo2;
          if (stateInfo2.active)
            break; 
          continue;
        } 
        break;
      } 
      if (this.mDbg) {
        StateMachine stateMachine = this.mSm;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("setupTempStateStackWithStatesToEnter: X mTempStateStackCount=");
        stringBuilder.append(this.mTempStateStackCount);
        stringBuilder.append(",curStateInfo: ");
        stringBuilder.append(stateInfo2);
        stateMachine.log(stringBuilder.toString());
      } 
      return stateInfo2;
    }
    
    private final void setupInitialStateStack() {
      if (this.mDbg) {
        StateMachine stateMachine = this.mSm;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("setupInitialStateStack: E mInitialState=");
        stringBuilder.append(this.mInitialState.getName());
        stateMachine.log(stringBuilder.toString());
      } 
      StateInfo stateInfo = this.mStateInfo.get(this.mInitialState);
      int i = 0;
      while (true) {
        this.mTempStateStackCount = i;
        if (stateInfo != null) {
          this.mTempStateStack[this.mTempStateStackCount] = stateInfo;
          stateInfo = stateInfo.parentStateInfo;
          i = this.mTempStateStackCount + 1;
          continue;
        } 
        break;
      } 
      this.mStateStackTopIndex = -1;
      moveTempStateStackToStateStack();
    }
    
    private final Message getCurrentMessage() {
      return this.mMsg;
    }
    
    private final IState getCurrentState() {
      int i = this.mStateStackTopIndex;
      if (i < 0) {
        StateMachine stateMachine = this.mSm;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("tyl tmpIndex = ");
        stringBuilder.append(i);
        stringBuilder.append(" index out of bound!!");
        stateMachine.log(stringBuilder.toString());
        return (this.mStateStack[0]).state;
      } 
      return (this.mStateStack[i]).state;
    }
    
    private final StateInfo addState(State param1State1, State param1State2) {
      if (this.mDbg) {
        StateMachine stateMachine = this.mSm;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("addStateInternal: E state=");
        stringBuilder.append(param1State1.getName());
        stringBuilder.append(",parent=");
        if (param1State2 == null) {
          str = "";
        } else {
          str = param1State2.getName();
        } 
        stringBuilder.append(str);
        String str = stringBuilder.toString();
        stateMachine.log(str);
      } 
      StateInfo stateInfo3 = null;
      if (param1State2 != null) {
        StateInfo stateInfo = this.mStateInfo.get(param1State2);
        stateInfo3 = stateInfo;
        if (stateInfo == null)
          stateInfo3 = addState(param1State2, (State)null); 
      } 
      StateInfo stateInfo2 = this.mStateInfo.get(param1State1);
      StateInfo stateInfo1 = stateInfo2;
      if (stateInfo2 == null) {
        stateInfo1 = new StateInfo();
        this.mStateInfo.put(param1State1, stateInfo1);
      } 
      if (stateInfo1.parentStateInfo == null || stateInfo1.parentStateInfo == stateInfo3) {
        stateInfo1.state = param1State1;
        stateInfo1.parentStateInfo = stateInfo3;
        stateInfo1.active = false;
        if (this.mDbg) {
          StateMachine stateMachine = this.mSm;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("addStateInternal: X stateInfo: ");
          stringBuilder.append(stateInfo1);
          stateMachine.log(stringBuilder.toString());
        } 
        return stateInfo1;
      } 
      throw new RuntimeException("state already added");
    }
    
    private void removeState(State param1State) {
      StateInfo stateInfo = this.mStateInfo.get(param1State);
      if (stateInfo == null || stateInfo.active)
        return; 
      Stream stream = this.mStateInfo.values().stream();
      _$$Lambda$StateMachine$SmHandler$KkPO7NIVuI9r_FPEGrY6ux6a5Ks _$$Lambda$StateMachine$SmHandler$KkPO7NIVuI9r_FPEGrY6ux6a5Ks = new _$$Lambda$StateMachine$SmHandler$KkPO7NIVuI9r_FPEGrY6ux6a5Ks(stateInfo);
      stream = stream.filter(_$$Lambda$StateMachine$SmHandler$KkPO7NIVuI9r_FPEGrY6ux6a5Ks);
      Optional optional = stream.findAny();
      boolean bool = optional.isPresent();
      if (bool)
        return; 
      this.mStateInfo.remove(param1State);
    }
    
    private SmHandler(StateMachine this$0, StateMachine param1StateMachine) {
      super((Looper)this$0);
      this.mSm = param1StateMachine;
      addState(this.mHaltingState, (State)null);
      addState(this.mQuittingState, (State)null);
    }
    
    private final void setInitialState(State param1State) {
      if (this.mDbg) {
        StateMachine stateMachine = this.mSm;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("setInitialState: initialState=");
        stringBuilder.append(param1State.getName());
        stateMachine.log(stringBuilder.toString());
      } 
      this.mInitialState = param1State;
    }
    
    private final void transitionTo(IState param1IState) {
      if (this.mTransitionInProgress) {
        String str = this.mSm.mName;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("transitionTo called while transition already in progress to ");
        stringBuilder.append(this.mDestState);
        stringBuilder.append(", new target state=");
        stringBuilder.append(param1IState);
        Log.wtf(str, stringBuilder.toString());
      } 
      this.mDestState = (State)param1IState;
      if (this.mDbg) {
        StateMachine stateMachine = this.mSm;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("transitionTo: destState=");
        stringBuilder.append(this.mDestState.getName());
        stateMachine.log(stringBuilder.toString());
      } 
    }
    
    private final void deferMessage(Message param1Message) {
      if (this.mDbg) {
        StateMachine stateMachine = this.mSm;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("deferMessage: msg=");
        stringBuilder.append(param1Message.what);
        stateMachine.log(stringBuilder.toString());
      } 
      Message message = obtainMessage();
      message.copyFrom(param1Message);
      this.mDeferredMessages.add(message);
    }
    
    private final void quit() {
      if (this.mDbg)
        this.mSm.log("quit:"); 
      sendMessage(obtainMessage(-1, mSmHandlerObj));
    }
    
    private final void quitNow() {
      if (this.mDbg)
        this.mSm.log("quitNow:"); 
      sendMessageAtFrontOfQueue(obtainMessage(-1, mSmHandlerObj));
    }
    
    private final boolean isQuit(Message param1Message) {
      boolean bool;
      if (param1Message.what == -1 && param1Message.obj == mSmHandlerObj) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    private final boolean isDbg() {
      return this.mDbg;
    }
    
    private final void setDbg(boolean param1Boolean) {
      this.mDbg = param1Boolean;
    }
  }
  
  private class StateInfo {
    boolean active;
    
    StateInfo parentStateInfo;
    
    State state;
    
    final StateMachine.SmHandler this$0;
    
    private StateInfo() {}
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("state=");
      stringBuilder.append(this.state.getName());
      stringBuilder.append(",active=");
      stringBuilder.append(this.active);
      stringBuilder.append(",parent=");
      StateInfo stateInfo = this.parentStateInfo;
      if (stateInfo == null) {
        null = "null";
      } else {
        null = ((StateInfo)null).state.getName();
      } 
      stringBuilder.append(null);
      return stringBuilder.toString();
    }
  }
  
  private void initStateMachine(String paramString, Looper paramLooper) {
    this.mName = paramString;
    this.mSmHandler = new SmHandler(this);
  }
  
  protected StateMachine(String paramString) {
    HandlerThread handlerThread = new HandlerThread(paramString);
    handlerThread.start();
    Looper looper = this.mSmThread.getLooper();
    initStateMachine(paramString, looper);
  }
  
  protected StateMachine(String paramString, Looper paramLooper) {
    initStateMachine(paramString, paramLooper);
  }
  
  protected StateMachine(String paramString, Handler paramHandler) {
    initStateMachine(paramString, paramHandler.getLooper());
  }
  
  protected void onPreHandleMessage(Message paramMessage) {}
  
  protected void onPostHandleMessage(Message paramMessage) {}
  
  public final void addState(State paramState1, State paramState2) {
    this.mSmHandler.addState(paramState1, paramState2);
  }
  
  public final void addState(State paramState) {
    this.mSmHandler.addState(paramState, (State)null);
  }
  
  public final void removeState(State paramState) {
    this.mSmHandler.removeState(paramState);
  }
  
  public final void setInitialState(State paramState) {
    this.mSmHandler.setInitialState(paramState);
  }
  
  public final Message getCurrentMessage() {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return null; 
    return smHandler.getCurrentMessage();
  }
  
  public final IState getCurrentState() {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return null; 
    return smHandler.getCurrentState();
  }
  
  public final void transitionTo(IState paramIState) {
    this.mSmHandler.transitionTo(paramIState);
  }
  
  public final void transitionToHaltingState() {
    SmHandler smHandler = this.mSmHandler;
    smHandler.transitionTo(smHandler.mHaltingState);
  }
  
  public final void deferMessage(Message paramMessage) {
    this.mSmHandler.deferMessage(paramMessage);
  }
  
  protected void unhandledMessage(Message paramMessage) {
    if (this.mSmHandler.mDbg) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(" - unhandledMessage: msg.what=");
      stringBuilder.append(paramMessage.what);
      loge(stringBuilder.toString());
    } 
  }
  
  protected void haltedProcessMessage(Message paramMessage) {}
  
  protected void onHalting() {}
  
  protected void onQuitting() {}
  
  public final String getName() {
    return this.mName;
  }
  
  public final void setLogRecSize(int paramInt) {
    this.mSmHandler.mLogRecords.setSize(paramInt);
  }
  
  public final void setLogOnlyTransitions(boolean paramBoolean) {
    this.mSmHandler.mLogRecords.setLogOnlyTransitions(paramBoolean);
  }
  
  public final int getLogRecSize() {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return 0; 
    return smHandler.mLogRecords.size();
  }
  
  public final int getLogRecMaxSize() {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return 0; 
    return smHandler.mLogRecords.mMaxSize;
  }
  
  public final int getLogRecCount() {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return 0; 
    return smHandler.mLogRecords.count();
  }
  
  public final LogRec getLogRec(int paramInt) {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return null; 
    return smHandler.mLogRecords.get(paramInt);
  }
  
  public final Collection<LogRec> copyLogRecs() {
    Vector<LogRec> vector = new Vector();
    SmHandler smHandler = this.mSmHandler;
    if (smHandler != null)
      for (LogRec logRec : smHandler.mLogRecords.mLogRecVector)
        vector.add(logRec);  
    return vector;
  }
  
  public void addLogRec(String paramString) {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return; 
    LogRecords logRecords = smHandler.mLogRecords;
    Message message = smHandler.getCurrentMessage();
    IState iState = smHandler.getCurrentState();
    State state2 = (smHandler.mStateStack[smHandler.mStateStackTopIndex]).state, state1 = smHandler.mDestState;
    logRecords.add(this, message, paramString, iState, state2, state1);
  }
  
  protected boolean recordLogRec(Message paramMessage) {
    return true;
  }
  
  protected String getLogRecString(Message paramMessage) {
    return "";
  }
  
  protected String getWhatToString(int paramInt) {
    return null;
  }
  
  public final Handler getHandler() {
    return this.mSmHandler;
  }
  
  public final Message obtainMessage() {
    return Message.obtain(this.mSmHandler);
  }
  
  public final Message obtainMessage(int paramInt) {
    return Message.obtain(this.mSmHandler, paramInt);
  }
  
  public final Message obtainMessage(int paramInt, Object paramObject) {
    return Message.obtain(this.mSmHandler, paramInt, paramObject);
  }
  
  public final Message obtainMessage(int paramInt1, int paramInt2) {
    return Message.obtain(this.mSmHandler, paramInt1, paramInt2, 0);
  }
  
  public final Message obtainMessage(int paramInt1, int paramInt2, int paramInt3) {
    return Message.obtain(this.mSmHandler, paramInt1, paramInt2, paramInt3);
  }
  
  public final Message obtainMessage(int paramInt1, int paramInt2, int paramInt3, Object paramObject) {
    return Message.obtain(this.mSmHandler, paramInt1, paramInt2, paramInt3, paramObject);
  }
  
  public void sendMessage(int paramInt) {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return; 
    smHandler.sendMessage(obtainMessage(paramInt));
  }
  
  public void sendMessage(int paramInt, Object paramObject) {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return; 
    smHandler.sendMessage(obtainMessage(paramInt, paramObject));
  }
  
  public void sendMessage(int paramInt1, int paramInt2) {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return; 
    smHandler.sendMessage(obtainMessage(paramInt1, paramInt2));
  }
  
  public void sendMessage(int paramInt1, int paramInt2, int paramInt3) {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return; 
    smHandler.sendMessage(obtainMessage(paramInt1, paramInt2, paramInt3));
  }
  
  public void sendMessage(int paramInt1, int paramInt2, int paramInt3, Object paramObject) {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return; 
    smHandler.sendMessage(obtainMessage(paramInt1, paramInt2, paramInt3, paramObject));
  }
  
  public void sendMessage(Message paramMessage) {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return; 
    smHandler.sendMessage(paramMessage);
  }
  
  public void sendMessageDelayed(int paramInt, long paramLong) {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return; 
    smHandler.sendMessageDelayed(obtainMessage(paramInt), paramLong);
  }
  
  public void sendMessageDelayed(int paramInt, Object paramObject, long paramLong) {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return; 
    smHandler.sendMessageDelayed(obtainMessage(paramInt, paramObject), paramLong);
  }
  
  public void sendMessageDelayed(int paramInt1, int paramInt2, long paramLong) {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return; 
    smHandler.sendMessageDelayed(obtainMessage(paramInt1, paramInt2), paramLong);
  }
  
  public void sendMessageDelayed(int paramInt1, int paramInt2, int paramInt3, long paramLong) {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return; 
    smHandler.sendMessageDelayed(obtainMessage(paramInt1, paramInt2, paramInt3), paramLong);
  }
  
  public void sendMessageDelayed(int paramInt1, int paramInt2, int paramInt3, Object paramObject, long paramLong) {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return; 
    smHandler.sendMessageDelayed(obtainMessage(paramInt1, paramInt2, paramInt3, paramObject), paramLong);
  }
  
  public void sendMessageDelayed(Message paramMessage, long paramLong) {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return; 
    smHandler.sendMessageDelayed(paramMessage, paramLong);
  }
  
  protected final void sendMessageAtFrontOfQueue(int paramInt) {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return; 
    smHandler.sendMessageAtFrontOfQueue(obtainMessage(paramInt));
  }
  
  protected final void sendMessageAtFrontOfQueue(int paramInt, Object paramObject) {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return; 
    smHandler.sendMessageAtFrontOfQueue(obtainMessage(paramInt, paramObject));
  }
  
  protected final void sendMessageAtFrontOfQueue(int paramInt1, int paramInt2) {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return; 
    smHandler.sendMessageAtFrontOfQueue(obtainMessage(paramInt1, paramInt2));
  }
  
  protected final void sendMessageAtFrontOfQueue(int paramInt1, int paramInt2, int paramInt3) {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return; 
    smHandler.sendMessageAtFrontOfQueue(obtainMessage(paramInt1, paramInt2, paramInt3));
  }
  
  protected final void sendMessageAtFrontOfQueue(int paramInt1, int paramInt2, int paramInt3, Object paramObject) {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return; 
    smHandler.sendMessageAtFrontOfQueue(obtainMessage(paramInt1, paramInt2, paramInt3, paramObject));
  }
  
  protected final void sendMessageAtFrontOfQueue(Message paramMessage) {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return; 
    smHandler.sendMessageAtFrontOfQueue(paramMessage);
  }
  
  protected final void removeMessages(int paramInt) {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return; 
    smHandler.removeMessages(paramInt);
  }
  
  protected final void removeDeferredMessages(int paramInt) {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return; 
    Iterator<Message> iterator = smHandler.mDeferredMessages.iterator();
    while (iterator.hasNext()) {
      Message message = iterator.next();
      if (message.what == paramInt)
        iterator.remove(); 
    } 
  }
  
  protected final boolean hasDeferredMessages(int paramInt) {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return false; 
    Iterator<Message> iterator = smHandler.mDeferredMessages.iterator();
    while (iterator.hasNext()) {
      Message message = iterator.next();
      if (message.what == paramInt)
        return true; 
    } 
    return false;
  }
  
  protected final boolean hasMessages(int paramInt) {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return false; 
    return smHandler.hasMessages(paramInt);
  }
  
  protected final boolean isQuit(Message paramMessage) {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null) {
      boolean bool;
      if (paramMessage.what == -1) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    } 
    return smHandler.isQuit(paramMessage);
  }
  
  public final void quit() {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return; 
    smHandler.quit();
  }
  
  public final void quitNow() {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return; 
    smHandler.quitNow();
  }
  
  public boolean isDbg() {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return false; 
    return smHandler.isDbg();
  }
  
  public void setDbg(boolean paramBoolean) {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return; 
    smHandler.setDbg(paramBoolean);
  }
  
  public void start() {
    SmHandler smHandler = this.mSmHandler;
    if (smHandler == null)
      return; 
    smHandler.completeConstruction();
  }
  
  public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(getName());
    stringBuilder.append(":");
    paramPrintWriter.println(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append(" total records=");
    stringBuilder.append(getLogRecCount());
    paramPrintWriter.println(stringBuilder.toString());
    for (byte b = 0; b < getLogRecSize(); b++) {
      stringBuilder = new StringBuilder();
      stringBuilder.append(" rec[");
      stringBuilder.append(b);
      stringBuilder.append("]: ");
      stringBuilder.append(getLogRec(b).toString());
      paramPrintWriter.println(stringBuilder.toString());
      paramPrintWriter.flush();
    } 
    IState iState = getCurrentState();
    if (iState != null) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("curState=");
      stringBuilder1.append(iState.getName());
      paramPrintWriter.println(stringBuilder1.toString());
    } else {
      paramPrintWriter.println("curState=null");
    } 
  }
  
  public String toString() {
    String str3, str1 = "(null)";
    String str2 = "(null)";
    try {
      str3 = this.mName.toString();
      str1 = str3;
      String str = this.mSmHandler.getCurrentState().getName().toString();
      str1 = str3;
      str3 = str2;
    } catch (NullPointerException|ArrayIndexOutOfBoundsException nullPointerException) {
      str3 = str2;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("name=");
    stringBuilder.append(str1);
    stringBuilder.append(" state=");
    stringBuilder.append(str3);
    return stringBuilder.toString();
  }
  
  protected void logAndAddLogRec(String paramString) {
    addLogRec(paramString);
    log(paramString);
  }
  
  protected void log(String paramString) {
    Log.d(this.mName, paramString);
  }
  
  protected void logd(String paramString) {
    Log.d(this.mName, paramString);
  }
  
  protected void logv(String paramString) {
    Log.v(this.mName, paramString);
  }
  
  protected void logi(String paramString) {
    Log.i(this.mName, paramString);
  }
  
  protected void logw(String paramString) {
    Log.w(this.mName, paramString);
  }
  
  protected void loge(String paramString) {
    Log.e(this.mName, paramString);
  }
  
  protected void loge(String paramString, Throwable paramThrowable) {
    Log.e(this.mName, paramString, paramThrowable);
  }
}
