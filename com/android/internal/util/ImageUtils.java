package com.android.internal.util;

import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ImageDecoder;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Size;
import java.io.IOException;

public class ImageUtils {
  private static final int ALPHA_TOLERANCE = 50;
  
  private static final int COMPACT_BITMAP_SIZE = 64;
  
  private static final int TOLERANCE = 20;
  
  private int[] mTempBuffer;
  
  private Bitmap mTempCompactBitmap;
  
  private Canvas mTempCompactBitmapCanvas;
  
  private Paint mTempCompactBitmapPaint;
  
  private final Matrix mTempMatrix = new Matrix();
  
  public boolean isGrayscale(Bitmap paramBitmap) {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual getHeight : ()I
    //   4: istore_2
    //   5: aload_1
    //   6: invokevirtual getWidth : ()I
    //   9: istore_3
    //   10: iload_2
    //   11: bipush #64
    //   13: if_icmpgt -> 31
    //   16: iload_2
    //   17: istore #4
    //   19: iload_3
    //   20: istore #5
    //   22: aload_1
    //   23: astore #6
    //   25: iload_3
    //   26: bipush #64
    //   28: if_icmple -> 156
    //   31: aload_0
    //   32: getfield mTempCompactBitmap : Landroid/graphics/Bitmap;
    //   35: ifnonnull -> 89
    //   38: aload_0
    //   39: bipush #64
    //   41: bipush #64
    //   43: getstatic android/graphics/Bitmap$Config.ARGB_8888 : Landroid/graphics/Bitmap$Config;
    //   46: invokestatic createBitmap : (IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    //   49: putfield mTempCompactBitmap : Landroid/graphics/Bitmap;
    //   52: aload_0
    //   53: new android/graphics/Canvas
    //   56: dup
    //   57: aload_0
    //   58: getfield mTempCompactBitmap : Landroid/graphics/Bitmap;
    //   61: invokespecial <init> : (Landroid/graphics/Bitmap;)V
    //   64: putfield mTempCompactBitmapCanvas : Landroid/graphics/Canvas;
    //   67: new android/graphics/Paint
    //   70: dup
    //   71: iconst_1
    //   72: invokespecial <init> : (I)V
    //   75: astore #6
    //   77: aload_0
    //   78: aload #6
    //   80: putfield mTempCompactBitmapPaint : Landroid/graphics/Paint;
    //   83: aload #6
    //   85: iconst_1
    //   86: invokevirtual setFilterBitmap : (Z)V
    //   89: aload_0
    //   90: getfield mTempMatrix : Landroid/graphics/Matrix;
    //   93: invokevirtual reset : ()V
    //   96: aload_0
    //   97: getfield mTempMatrix : Landroid/graphics/Matrix;
    //   100: ldc 64.0
    //   102: iload_3
    //   103: i2f
    //   104: fdiv
    //   105: ldc 64.0
    //   107: iload_2
    //   108: i2f
    //   109: fdiv
    //   110: fconst_0
    //   111: fconst_0
    //   112: invokevirtual setScale : (FFFF)V
    //   115: aload_0
    //   116: getfield mTempCompactBitmapCanvas : Landroid/graphics/Canvas;
    //   119: iconst_0
    //   120: getstatic android/graphics/PorterDuff$Mode.SRC : Landroid/graphics/PorterDuff$Mode;
    //   123: invokevirtual drawColor : (ILandroid/graphics/PorterDuff$Mode;)V
    //   126: aload_0
    //   127: getfield mTempCompactBitmapCanvas : Landroid/graphics/Canvas;
    //   130: aload_1
    //   131: aload_0
    //   132: getfield mTempMatrix : Landroid/graphics/Matrix;
    //   135: aload_0
    //   136: getfield mTempCompactBitmapPaint : Landroid/graphics/Paint;
    //   139: invokevirtual drawBitmap : (Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V
    //   142: aload_0
    //   143: getfield mTempCompactBitmap : Landroid/graphics/Bitmap;
    //   146: astore #6
    //   148: bipush #64
    //   150: istore #4
    //   152: bipush #64
    //   154: istore #5
    //   156: iload #4
    //   158: iload #5
    //   160: imul
    //   161: istore_2
    //   162: aload_0
    //   163: iload_2
    //   164: invokespecial ensureBufferSize : (I)V
    //   167: aload #6
    //   169: aload_0
    //   170: getfield mTempBuffer : [I
    //   173: iconst_0
    //   174: iload #5
    //   176: iconst_0
    //   177: iconst_0
    //   178: iload #5
    //   180: iload #4
    //   182: invokevirtual getPixels : ([IIIIIII)V
    //   185: iconst_0
    //   186: istore #5
    //   188: iload #5
    //   190: iload_2
    //   191: if_icmpge -> 215
    //   194: aload_0
    //   195: getfield mTempBuffer : [I
    //   198: iload #5
    //   200: iaload
    //   201: invokestatic isGrayscale : (I)Z
    //   204: ifne -> 209
    //   207: iconst_0
    //   208: ireturn
    //   209: iinc #5, 1
    //   212: goto -> 188
    //   215: iconst_1
    //   216: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #70	-> 0
    //   #71	-> 5
    //   #74	-> 10
    //   #75	-> 31
    //   #76	-> 38
    //   #79	-> 52
    //   #80	-> 67
    //   #81	-> 83
    //   #83	-> 89
    //   #84	-> 96
    //   #88	-> 115
    //   #89	-> 126
    //   #90	-> 142
    //   #91	-> 148
    //   #94	-> 156
    //   #95	-> 162
    //   #96	-> 167
    //   #97	-> 185
    //   #98	-> 194
    //   #99	-> 207
    //   #97	-> 209
    //   #102	-> 215
  }
  
  private void ensureBufferSize(int paramInt) {
    int[] arrayOfInt = this.mTempBuffer;
    if (arrayOfInt == null || arrayOfInt.length < paramInt)
      this.mTempBuffer = new int[paramInt]; 
  }
  
  public static boolean isGrayscale(int paramInt) {
    boolean bool = true;
    if ((paramInt >> 24 & 0xFF) < 50)
      return true; 
    int i = paramInt >> 16 & 0xFF;
    int j = paramInt >> 8 & 0xFF;
    paramInt &= 0xFF;
    if (Math.abs(i - j) >= 20 || 
      Math.abs(i - paramInt) >= 20 || 
      Math.abs(j - paramInt) >= 20)
      bool = false; 
    return bool;
  }
  
  public static Bitmap buildScaledBitmap(Drawable paramDrawable, int paramInt1, int paramInt2) {
    if (paramDrawable == null)
      return null; 
    int i = paramDrawable.getIntrinsicWidth();
    int j = paramDrawable.getIntrinsicHeight();
    if (i <= paramInt1 && j <= paramInt2 && paramDrawable instanceof BitmapDrawable)
      return ((BitmapDrawable)paramDrawable).getBitmap(); 
    if (j <= 0 || i <= 0)
      return null; 
    float f = Math.min(paramInt1 / i, paramInt2 / j);
    f = Math.min(1.0F, f);
    paramInt1 = (int)(i * f);
    paramInt2 = (int)(j * f);
    Bitmap bitmap = Bitmap.createBitmap(paramInt1, paramInt2, Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(bitmap);
    paramDrawable.setBounds(0, 0, paramInt1, paramInt2);
    paramDrawable.draw(canvas);
    return bitmap;
  }
  
  public static int calculateSampleSize(Size paramSize1, Size paramSize2) {
    int i = 1, j = 1;
    if (paramSize1.getHeight() > paramSize2.getHeight() || 
      paramSize1.getWidth() > paramSize2.getWidth()) {
      int k = paramSize1.getHeight() / 2;
      int m = paramSize1.getWidth() / 2;
      while (true) {
        i = j;
        if (k / j >= paramSize2.getHeight()) {
          int n = m / j;
          i = j;
          if (n >= paramSize2.getWidth()) {
            j *= 2;
            continue;
          } 
        } 
        break;
      } 
    } 
    return i;
  }
  
  public static Bitmap loadThumbnail(ContentResolver paramContentResolver, Uri paramUri, Size paramSize) throws IOException {
    ContentProviderClient contentProviderClient = paramContentResolver.acquireContentProviderClient(paramUri);
    try {
      Bundle bundle = new Bundle();
      this();
      bundle.putParcelable("android.content.extra.SIZE", (Parcelable)Point.convert(paramSize));
      _$$Lambda$ImageUtils$UJyN8OeHYbkY_xJzm1U3D7W4PNY _$$Lambda$ImageUtils$UJyN8OeHYbkY_xJzm1U3D7W4PNY = new _$$Lambda$ImageUtils$UJyN8OeHYbkY_xJzm1U3D7W4PNY();
      this(contentProviderClient, paramUri, bundle);
      ImageDecoder.Source source = ImageDecoder.createSource(_$$Lambda$ImageUtils$UJyN8OeHYbkY_xJzm1U3D7W4PNY);
      _$$Lambda$ImageUtils$rnRZcgsdC1BtH9FpHTN2Kf_FXwE _$$Lambda$ImageUtils$rnRZcgsdC1BtH9FpHTN2Kf_FXwE = new _$$Lambda$ImageUtils$rnRZcgsdC1BtH9FpHTN2Kf_FXwE();
      this(paramSize);
      return ImageDecoder.decodeBitmap(source, _$$Lambda$ImageUtils$rnRZcgsdC1BtH9FpHTN2Kf_FXwE);
    } finally {
      if (contentProviderClient != null)
        try {
          contentProviderClient.close();
        } finally {
          contentProviderClient = null;
        }  
    } 
  }
}
