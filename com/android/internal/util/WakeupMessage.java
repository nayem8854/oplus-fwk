package com.android.internal.util;

import android.app.AlarmManager;
import android.content.Context;
import android.os.Handler;

public class WakeupMessage implements AlarmManager.OnAlarmListener {
  private final AlarmManager mAlarmManager;
  
  protected final int mArg1;
  
  protected final int mArg2;
  
  protected final int mCmd;
  
  protected final String mCmdName;
  
  protected final Handler mHandler;
  
  protected final Object mObj;
  
  private final Runnable mRunnable;
  
  private boolean mScheduled;
  
  public WakeupMessage(Context paramContext, Handler paramHandler, String paramString, int paramInt1, int paramInt2, int paramInt3, Object paramObject) {
    this.mAlarmManager = getAlarmManager(paramContext);
    this.mHandler = paramHandler;
    this.mCmdName = paramString;
    this.mCmd = paramInt1;
    this.mArg1 = paramInt2;
    this.mArg2 = paramInt3;
    this.mObj = paramObject;
    this.mRunnable = null;
  }
  
  public WakeupMessage(Context paramContext, Handler paramHandler, String paramString, int paramInt1, int paramInt2) {
    this(paramContext, paramHandler, paramString, paramInt1, paramInt2, 0, null);
  }
  
  public WakeupMessage(Context paramContext, Handler paramHandler, String paramString, int paramInt1, int paramInt2, int paramInt3) {
    this(paramContext, paramHandler, paramString, paramInt1, paramInt2, paramInt3, null);
  }
  
  public WakeupMessage(Context paramContext, Handler paramHandler, String paramString, int paramInt) {
    this(paramContext, paramHandler, paramString, paramInt, 0, 0, null);
  }
  
  public WakeupMessage(Context paramContext, Handler paramHandler, String paramString, Runnable paramRunnable) {
    this.mAlarmManager = getAlarmManager(paramContext);
    this.mHandler = paramHandler;
    this.mCmdName = paramString;
    this.mCmd = 0;
    this.mArg1 = 0;
    this.mArg2 = 0;
    this.mObj = null;
    this.mRunnable = paramRunnable;
  }
  
  private static AlarmManager getAlarmManager(Context paramContext) {
    return (AlarmManager)paramContext.getSystemService("alarm");
  }
  
  public void schedule(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mAlarmManager : Landroid/app/AlarmManager;
    //   6: iconst_2
    //   7: lload_1
    //   8: aload_0
    //   9: getfield mCmdName : Ljava/lang/String;
    //   12: aload_0
    //   13: aload_0
    //   14: getfield mHandler : Landroid/os/Handler;
    //   17: invokevirtual setExact : (IJLjava/lang/String;Landroid/app/AlarmManager$OnAlarmListener;Landroid/os/Handler;)V
    //   20: aload_0
    //   21: iconst_1
    //   22: putfield mScheduled : Z
    //   25: aload_0
    //   26: monitorexit
    //   27: return
    //   28: astore_3
    //   29: aload_0
    //   30: monitorexit
    //   31: aload_3
    //   32: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #100	-> 2
    //   #102	-> 20
    //   #103	-> 25
    //   #99	-> 28
    // Exception table:
    //   from	to	target	type
    //   2	20	28	finally
    //   20	25	28	finally
  }
  
  public void cancel() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mScheduled : Z
    //   6: ifeq -> 22
    //   9: aload_0
    //   10: getfield mAlarmManager : Landroid/app/AlarmManager;
    //   13: aload_0
    //   14: invokevirtual cancel : (Landroid/app/AlarmManager$OnAlarmListener;)V
    //   17: aload_0
    //   18: iconst_0
    //   19: putfield mScheduled : Z
    //   22: aload_0
    //   23: monitorexit
    //   24: return
    //   25: astore_1
    //   26: aload_0
    //   27: monitorexit
    //   28: aload_1
    //   29: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #110	-> 2
    //   #111	-> 9
    //   #112	-> 17
    //   #114	-> 22
    //   #109	-> 25
    // Exception table:
    //   from	to	target	type
    //   2	9	25	finally
    //   9	17	25	finally
    //   17	22	25	finally
  }
  
  public void onAlarm() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mScheduled : Z
    //   6: istore_1
    //   7: aload_0
    //   8: iconst_0
    //   9: putfield mScheduled : Z
    //   12: aload_0
    //   13: monitorexit
    //   14: iload_1
    //   15: ifeq -> 75
    //   18: aload_0
    //   19: getfield mRunnable : Ljava/lang/Runnable;
    //   22: astore_2
    //   23: aload_2
    //   24: ifnonnull -> 54
    //   27: aload_0
    //   28: getfield mHandler : Landroid/os/Handler;
    //   31: aload_0
    //   32: getfield mCmd : I
    //   35: aload_0
    //   36: getfield mArg1 : I
    //   39: aload_0
    //   40: getfield mArg2 : I
    //   43: aload_0
    //   44: getfield mObj : Ljava/lang/Object;
    //   47: invokevirtual obtainMessage : (IIILjava/lang/Object;)Landroid/os/Message;
    //   50: astore_2
    //   51: goto -> 63
    //   54: aload_0
    //   55: getfield mHandler : Landroid/os/Handler;
    //   58: aload_2
    //   59: invokestatic obtain : (Landroid/os/Handler;Ljava/lang/Runnable;)Landroid/os/Message;
    //   62: astore_2
    //   63: aload_0
    //   64: getfield mHandler : Landroid/os/Handler;
    //   67: aload_2
    //   68: invokevirtual dispatchMessage : (Landroid/os/Message;)V
    //   71: aload_2
    //   72: invokevirtual recycle : ()V
    //   75: return
    //   76: astore_2
    //   77: aload_0
    //   78: monitorexit
    //   79: aload_2
    //   80: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #122	-> 0
    //   #123	-> 2
    //   #124	-> 7
    //   #125	-> 12
    //   #126	-> 14
    //   #128	-> 18
    //   #129	-> 27
    //   #131	-> 54
    //   #133	-> 63
    //   #134	-> 71
    //   #136	-> 75
    //   #125	-> 76
    // Exception table:
    //   from	to	target	type
    //   2	7	76	finally
    //   7	12	76	finally
    //   12	14	76	finally
    //   77	79	76	finally
  }
}
