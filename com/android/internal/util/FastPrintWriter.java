package com.android.internal.util;

import android.util.Log;
import android.util.Printer;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;
import java.nio.charset.CodingErrorAction;

public class FastPrintWriter extends PrintWriter {
  private final boolean mAutoFlush;
  
  private final int mBufferLen;
  
  private final ByteBuffer mBytes;
  
  private CharsetEncoder mCharset;
  
  private boolean mIoError;
  
  private final OutputStream mOutputStream;
  
  private int mPos;
  
  private final Printer mPrinter;
  
  private final String mSeparator;
  
  private final char[] mText;
  
  private final Writer mWriter;
  
  private static class DummyWriter extends Writer {
    private DummyWriter() {}
    
    public void close() throws IOException {
      UnsupportedOperationException unsupportedOperationException = new UnsupportedOperationException("Shouldn't be here");
      throw unsupportedOperationException;
    }
    
    public void flush() throws IOException {
      close();
    }
    
    public void write(char[] param1ArrayOfchar, int param1Int1, int param1Int2) throws IOException {
      close();
    }
  }
  
  public FastPrintWriter(OutputStream paramOutputStream) {
    this(paramOutputStream, false, 8192);
  }
  
  public FastPrintWriter(OutputStream paramOutputStream, boolean paramBoolean) {
    this(paramOutputStream, paramBoolean, 8192);
  }
  
  public FastPrintWriter(OutputStream paramOutputStream, boolean paramBoolean, int paramInt) {
    super(new DummyWriter(null), paramBoolean);
    if (paramOutputStream != null) {
      this.mBufferLen = paramInt;
      this.mText = new char[paramInt];
      this.mBytes = ByteBuffer.allocate(paramInt);
      this.mOutputStream = paramOutputStream;
      this.mWriter = null;
      this.mPrinter = null;
      this.mAutoFlush = paramBoolean;
      this.mSeparator = System.lineSeparator();
      initDefaultEncoder();
      return;
    } 
    throw new NullPointerException("out is null");
  }
  
  public FastPrintWriter(Writer paramWriter) {
    this(paramWriter, false, 8192);
  }
  
  public FastPrintWriter(Writer paramWriter, boolean paramBoolean) {
    this(paramWriter, paramBoolean, 8192);
  }
  
  public FastPrintWriter(Writer paramWriter, boolean paramBoolean, int paramInt) {
    super(new DummyWriter(null), paramBoolean);
    if (paramWriter != null) {
      this.mBufferLen = paramInt;
      this.mText = new char[paramInt];
      this.mBytes = null;
      this.mOutputStream = null;
      this.mWriter = paramWriter;
      this.mPrinter = null;
      this.mAutoFlush = paramBoolean;
      this.mSeparator = System.lineSeparator();
      initDefaultEncoder();
      return;
    } 
    throw new NullPointerException("wr is null");
  }
  
  public FastPrintWriter(Printer paramPrinter) {
    this(paramPrinter, 512);
  }
  
  public FastPrintWriter(Printer paramPrinter, int paramInt) {
    super(new DummyWriter(null), true);
    if (paramPrinter != null) {
      this.mBufferLen = paramInt;
      this.mText = new char[paramInt];
      this.mBytes = null;
      this.mOutputStream = null;
      this.mWriter = null;
      this.mPrinter = paramPrinter;
      this.mAutoFlush = true;
      this.mSeparator = System.lineSeparator();
      initDefaultEncoder();
      return;
    } 
    throw new NullPointerException("pr is null");
  }
  
  private final void initEncoder(String paramString) throws UnsupportedEncodingException {
    try {
      CharsetEncoder charsetEncoder = Charset.forName(paramString).newEncoder();
      charsetEncoder.onMalformedInput(CodingErrorAction.REPLACE);
      this.mCharset.onUnmappableCharacter(CodingErrorAction.REPLACE);
      return;
    } catch (Exception exception) {
      throw new UnsupportedEncodingException(paramString);
    } 
  }
  
  public boolean checkError() {
    flush();
    synchronized (this.lock) {
      return this.mIoError;
    } 
  }
  
  protected void clearError() {
    synchronized (this.lock) {
      this.mIoError = false;
      return;
    } 
  }
  
  protected void setError() {
    synchronized (this.lock) {
      this.mIoError = true;
      return;
    } 
  }
  
  private final void initDefaultEncoder() {
    CharsetEncoder charsetEncoder = Charset.defaultCharset().newEncoder();
    charsetEncoder.onMalformedInput(CodingErrorAction.REPLACE);
    this.mCharset.onUnmappableCharacter(CodingErrorAction.REPLACE);
  }
  
  private void appendLocked(char paramChar) throws IOException {
    int i = this.mPos;
    int j = i;
    if (i >= this.mBufferLen - 1) {
      flushLocked();
      j = this.mPos;
    } 
    this.mText[j] = paramChar;
    this.mPos = j + 1;
  }
  
  private void appendLocked(String paramString, int paramInt1, int paramInt2) throws IOException {
    int i = this.mBufferLen;
    if (paramInt2 > i) {
      int m = paramInt1 + paramInt2;
      while (paramInt1 < m) {
        int n = paramInt1 + i;
        if (n < m) {
          paramInt2 = i;
        } else {
          paramInt2 = m - paramInt1;
        } 
        appendLocked(paramString, paramInt1, paramInt2);
        paramInt1 = n;
      } 
      return;
    } 
    int j = this.mPos;
    int k = j;
    if (j + paramInt2 > i) {
      flushLocked();
      k = this.mPos;
    } 
    paramString.getChars(paramInt1, paramInt1 + paramInt2, this.mText, k);
    this.mPos = k + paramInt2;
  }
  
  private void appendLocked(char[] paramArrayOfchar, int paramInt1, int paramInt2) throws IOException {
    int i = this.mBufferLen;
    if (paramInt2 > i) {
      int m = paramInt1 + paramInt2;
      while (paramInt1 < m) {
        int n = paramInt1 + i;
        if (n < m) {
          paramInt2 = i;
        } else {
          paramInt2 = m - paramInt1;
        } 
        appendLocked(paramArrayOfchar, paramInt1, paramInt2);
        paramInt1 = n;
      } 
      return;
    } 
    int j = this.mPos;
    int k = j;
    if (j + paramInt2 > i) {
      flushLocked();
      k = this.mPos;
    } 
    System.arraycopy(paramArrayOfchar, paramInt1, this.mText, k, paramInt2);
    this.mPos = k + paramInt2;
  }
  
  private void flushBytesLocked() throws IOException {
    if (!this.mIoError) {
      int i = this.mBytes.position();
      if (i > 0) {
        this.mBytes.flip();
        this.mOutputStream.write(this.mBytes.array(), 0, i);
        this.mBytes.clear();
      } 
    } 
  }
  
  private void flushLocked() throws IOException {
    int i = this.mPos;
    if (i > 0) {
      if (this.mOutputStream != null) {
        CharBuffer charBuffer = CharBuffer.wrap(this.mText, 0, i);
        CoderResult coderResult = this.mCharset.encode(charBuffer, this.mBytes, true);
        while (!this.mIoError) {
          if (!coderResult.isError()) {
            if (coderResult.isOverflow()) {
              flushBytesLocked();
              coderResult = this.mCharset.encode(charBuffer, this.mBytes, true);
              continue;
            } 
            break;
          } 
          throw new IOException(coderResult.toString());
        } 
        if (!this.mIoError) {
          flushBytesLocked();
          this.mOutputStream.flush();
        } 
      } else {
        Writer writer = this.mWriter;
        if (writer != null) {
          if (!this.mIoError) {
            writer.write(this.mText, 0, i);
            this.mWriter.flush();
          } 
        } else {
          char c1 = Character.MIN_VALUE;
          int j = this.mSeparator.length();
          int k = this.mPos;
          char c2 = c1;
          i = k;
          if (j < k) {
            i = j;
            c2 = c1;
          } 
          while (c2 < i) {
            c1 = this.mText[this.mPos - 1 - c2];
            String str = this.mSeparator;
            if (c1 == str.charAt(str.length() - 1 - c2))
              c2++; 
          } 
          if (c2 >= this.mPos) {
            this.mPrinter.println("");
          } else {
            this.mPrinter.println(new String(this.mText, 0, this.mPos - c2));
          } 
        } 
      } 
      this.mPos = 0;
    } 
  }
  
  public void flush() {
    Object object = this.lock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    try {
      flushLocked();
      if (!this.mIoError)
        if (this.mOutputStream != null) {
          this.mOutputStream.flush();
        } else if (this.mWriter != null) {
          this.mWriter.flush();
        }  
    } catch (IOException iOException) {
      Log.w("FastPrintWriter", "Write failure", iOException);
      setError();
    } finally {
      Exception exception;
    } 
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
  }
  
  public void close() {
    Object object = this.lock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    try {
      flushLocked();
      if (this.mOutputStream != null) {
        this.mOutputStream.close();
      } else if (this.mWriter != null) {
        this.mWriter.close();
      } 
    } catch (IOException iOException) {
      Log.w("FastPrintWriter", "Write failure", iOException);
      setError();
    } finally {
      Exception exception;
    } 
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
  }
  
  public void print(char[] paramArrayOfchar) {
    Object object = this.lock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    try {
      appendLocked(paramArrayOfchar, 0, paramArrayOfchar.length);
    } catch (IOException iOException) {
      Log.w("FastPrintWriter", "Write failure", iOException);
      setError();
    } finally {}
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
  }
  
  public void print(char paramChar) {
    Object object = this.lock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    try {
      appendLocked(paramChar);
    } catch (IOException iOException) {
      Log.w("FastPrintWriter", "Write failure", iOException);
      setError();
    } finally {
      Exception exception;
    } 
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
  }
  
  public void print(String paramString) {
    String str = paramString;
    if (paramString == null)
      str = String.valueOf((Object)null); 
    Object object = this.lock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    try {
      appendLocked(str, 0, str.length());
    } catch (IOException iOException) {
      Log.w("FastPrintWriter", "Write failure", iOException);
      setError();
    } finally {}
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
  }
  
  public void print(int paramInt) {
    if (paramInt == 0) {
      print("0");
    } else {
      super.print(paramInt);
    } 
  }
  
  public void print(long paramLong) {
    if (paramLong == 0L) {
      print("0");
    } else {
      super.print(paramLong);
    } 
  }
  
  public void println() {
    Object object = this.lock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    try {
      appendLocked(this.mSeparator, 0, this.mSeparator.length());
      if (this.mAutoFlush)
        flushLocked(); 
    } catch (IOException iOException) {
      Log.w("FastPrintWriter", "Write failure", iOException);
      setError();
    } finally {
      Exception exception;
    } 
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
  }
  
  public void println(int paramInt) {
    if (paramInt == 0) {
      println("0");
    } else {
      super.println(paramInt);
    } 
  }
  
  public void println(long paramLong) {
    if (paramLong == 0L) {
      println("0");
    } else {
      super.println(paramLong);
    } 
  }
  
  public void println(char[] paramArrayOfchar) {
    print(paramArrayOfchar);
    println();
  }
  
  public void println(char paramChar) {
    print(paramChar);
    println();
  }
  
  public void write(char[] paramArrayOfchar, int paramInt1, int paramInt2) {
    Object object = this.lock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    try {
      appendLocked(paramArrayOfchar, paramInt1, paramInt2);
    } catch (IOException iOException) {
      Log.w("FastPrintWriter", "Write failure", iOException);
      setError();
    } finally {}
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
  }
  
  public void write(int paramInt) {
    Object object = this.lock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    char c = (char)paramInt;
    try {
      appendLocked(c);
    } catch (IOException iOException) {
      Log.w("FastPrintWriter", "Write failure", iOException);
      setError();
    } finally {
      Exception exception;
    } 
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
  }
  
  public void write(String paramString) {
    Object object = this.lock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    try {
      appendLocked(paramString, 0, paramString.length());
    } catch (IOException iOException) {
      Log.w("FastPrintWriter", "Write failure", iOException);
      setError();
    } finally {}
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
  }
  
  public void write(String paramString, int paramInt1, int paramInt2) {
    Object object = this.lock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    try {
      appendLocked(paramString, paramInt1, paramInt2);
    } catch (IOException iOException) {
      Log.w("FastPrintWriter", "Write failure", iOException);
      setError();
    } finally {}
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
  }
  
  public PrintWriter append(CharSequence paramCharSequence, int paramInt1, int paramInt2) {
    CharSequence charSequence = paramCharSequence;
    if (paramCharSequence == null)
      charSequence = "null"; 
    paramCharSequence = charSequence.subSequence(paramInt1, paramInt2).toString();
    write((String)paramCharSequence, 0, paramCharSequence.length());
    return this;
  }
}
