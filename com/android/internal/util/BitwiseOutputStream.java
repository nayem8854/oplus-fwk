package com.android.internal.util;

public class BitwiseOutputStream {
  private byte[] mBuf;
  
  private int mEnd;
  
  private int mPos;
  
  public static class AccessException extends Exception {
    public AccessException(String param1String) {
      super(stringBuilder.toString());
    }
  }
  
  public BitwiseOutputStream(int paramInt) {
    this.mBuf = new byte[paramInt];
    this.mEnd = paramInt << 3;
    this.mPos = 0;
  }
  
  public byte[] toByteArray() {
    int i = this.mPos;
    if ((i & 0x7) > 0) {
      j = 1;
    } else {
      j = 0;
    } 
    int j = (i >>> 3) + j;
    byte[] arrayOfByte = new byte[j];
    System.arraycopy(this.mBuf, 0, arrayOfByte, 0, j);
    return arrayOfByte;
  }
  
  private void possExpand(int paramInt) {
    int i = this.mPos, j = this.mEnd;
    if (i + paramInt < j)
      return; 
    byte[] arrayOfByte = new byte[i + paramInt >>> 2];
    System.arraycopy(this.mBuf, 0, arrayOfByte, 0, j >>> 3);
    this.mBuf = arrayOfByte;
    this.mEnd = arrayOfByte.length << 3;
  }
  
  public void write(int paramInt1, int paramInt2) throws AccessException {
    if (paramInt1 >= 0 && paramInt1 <= 8) {
      possExpand(paramInt1);
      int i = this.mPos, j = i >>> 3;
      int k = 16 - (i & 0x7) - paramInt1;
      paramInt2 = (paramInt2 & -1 >>> 32 - paramInt1) << k;
      this.mPos = i + paramInt1;
      byte[] arrayOfByte = this.mBuf;
      arrayOfByte[j] = (byte)(arrayOfByte[j] | paramInt2 >>> 8);
      if (k < 8) {
        paramInt1 = j + 1;
        arrayOfByte[paramInt1] = (byte)(arrayOfByte[paramInt1] | paramInt2 & 0xFF);
      } 
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("illegal write (");
    stringBuilder.append(paramInt1);
    stringBuilder.append(" bits)");
    throw new AccessException(stringBuilder.toString());
  }
  
  public void writeByteArray(int paramInt, byte[] paramArrayOfbyte) throws AccessException {
    for (byte b = 0; b < paramArrayOfbyte.length; b++) {
      int i = Math.min(8, paramInt - (b << 3));
      if (i > 0)
        write(i, (byte)(paramArrayOfbyte[b] >>> 8 - i)); 
    } 
  }
  
  public void skip(int paramInt) {
    possExpand(paramInt);
    this.mPos += paramInt;
  }
}
