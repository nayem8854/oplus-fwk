package com.android.internal.util;

import android.os.Debug;
import android.os.StrictMode;

public final class MemInfoReader {
  final long[] mInfos = new long[18];
  
  public void readMemInfo() {
    StrictMode.ThreadPolicy threadPolicy = StrictMode.allowThreadDiskReads();
    try {
      Debug.getMemInfo(this.mInfos);
      return;
    } finally {
      StrictMode.setThreadPolicy(threadPolicy);
    } 
  }
  
  public long getTotalSize() {
    return this.mInfos[0] * 1024L;
  }
  
  public long getFreeSize() {
    return this.mInfos[1] * 1024L;
  }
  
  public long getCachedSize() {
    return getCachedSizeKb() * 1024L;
  }
  
  public long getKernelUsedSize() {
    return getKernelUsedSizeKb() * 1024L;
  }
  
  public long getTotalSizeKb() {
    return this.mInfos[0];
  }
  
  public long getFreeSizeKb() {
    return this.mInfos[1];
  }
  
  public long getCachedSizeKb() {
    long arrayOfLong[] = this.mInfos, l1 = arrayOfLong[15];
    long l2 = l1;
    if (l1 == 0L)
      l2 = arrayOfLong[6]; 
    arrayOfLong = this.mInfos;
    return arrayOfLong[2] + l2 + arrayOfLong[3] - arrayOfLong[11];
  }
  
  public long getKernelUsedSizeKb() {
    long arrayOfLong[] = this.mInfos, l1 = arrayOfLong[4] + arrayOfLong[7] + arrayOfLong[12] + arrayOfLong[13];
    long l2 = l1;
    if (!Debug.isVmapStack())
      l2 = l1 + this.mInfos[14]; 
    return l2;
  }
  
  public long getSwapTotalSizeKb() {
    return this.mInfos[8];
  }
  
  public long getSwapFreeSizeKb() {
    return this.mInfos[9];
  }
  
  public long getZramTotalSizeKb() {
    return this.mInfos[10];
  }
  
  public long[] getRawInfo() {
    return this.mInfos;
  }
  
  public long getIonTotalCachedKb() {
    return this.mInfos[16];
  }
}
