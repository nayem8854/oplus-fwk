package com.android.internal.util;

import android.util.Log;
import java.util.Arrays;

public class ExponentiallyBucketedHistogram {
  private final int[] mData;
  
  public ExponentiallyBucketedHistogram(int paramInt) {
    paramInt = Preconditions.checkArgumentInRange(paramInt, 1, 31, "numBuckets");
    this.mData = new int[paramInt];
  }
  
  public void add(int paramInt) {
    if (paramInt <= 0) {
      int[] arrayOfInt = this.mData;
      arrayOfInt[0] = arrayOfInt[0] + 1;
    } else {
      int[] arrayOfInt = this.mData;
      paramInt = Math.min(arrayOfInt.length - 1, 32 - Integer.numberOfLeadingZeros(paramInt));
      arrayOfInt[paramInt] = arrayOfInt[paramInt] + 1;
    } 
  }
  
  public void reset() {
    Arrays.fill(this.mData, 0);
  }
  
  public void log(String paramString, CharSequence paramCharSequence) {
    paramCharSequence = new StringBuilder(paramCharSequence);
    paramCharSequence.append('[');
    for (byte b = 0; b < this.mData.length; b++) {
      if (b != 0)
        paramCharSequence.append(", "); 
      if (b < this.mData.length - 1) {
        paramCharSequence.append("<");
        paramCharSequence.append(1 << b);
      } else {
        paramCharSequence.append(">=");
        paramCharSequence.append(1 << b - 1);
      } 
      paramCharSequence.append(": ");
      paramCharSequence.append(this.mData[b]);
    } 
    paramCharSequence.append("]");
    Log.d(paramString, paramCharSequence.toString());
  }
}
