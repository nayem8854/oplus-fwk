package com.android.internal.util;

import android.util.ArrayMap;
import android.util.ArraySet;
import android.util.ExceptionUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class CollectionUtils {
  public static <T> List<T> filter(List<T> paramList, Predicate<? super T> paramPredicate) {
    ArrayList<T> arrayList = null;
    for (byte b = 0; b < size(paramList); b++, arrayList = arrayList1) {
      T t = paramList.get(b);
      ArrayList<T> arrayList1 = arrayList;
      if (paramPredicate.test(t))
        arrayList1 = ArrayUtils.add(arrayList, t); 
    } 
    return emptyIfNull(arrayList);
  }
  
  public static <T> Set<T> filter(Set<T> paramSet, Predicate<? super T> paramPredicate) {
    ArraySet<T> arraySet1, arraySet3;
    if (paramSet == null || paramSet.size() == 0)
      return Collections.emptySet(); 
    ArraySet<T> arraySet2 = null;
    Set<T> set = null;
    if (paramSet instanceof ArraySet) {
      arraySet2 = (ArraySet)paramSet;
      int i = arraySet2.size();
      for (byte b = 0; b < i; b++, arraySet1 = arraySet) {
        ArraySet<T> arraySet;
        Object object = arraySet2.valueAt(b);
        set = paramSet;
        if (paramPredicate.test((T)object))
          arraySet = ArrayUtils.add((ArraySet<T>)paramSet, (T)object); 
      } 
      arraySet3 = arraySet1;
    } else {
      Iterator<T> iterator = arraySet1.iterator();
      arraySet1 = arraySet2;
      while (true) {
        arraySet3 = arraySet1;
        if (iterator.hasNext()) {
          arraySet2 = (ArraySet<T>)iterator.next();
          arraySet3 = arraySet1;
          if (paramPredicate.test((T)arraySet2))
            arraySet3 = ArrayUtils.add(arraySet1, (T)arraySet2); 
          arraySet1 = arraySet3;
          continue;
        } 
        break;
      } 
    } 
    return emptyIfNull((Set<T>)arraySet3);
  }
  
  public static <T> void addIf(List<T> paramList, Collection<? super T> paramCollection, Predicate<? super T> paramPredicate) {
    for (byte b = 0; b < size(paramList); b++) {
      T t = paramList.get(b);
      if (paramPredicate.test(t))
        paramCollection.add(t); 
    } 
  }
  
  public static <I, O> List<O> map(List<I> paramList, Function<? super I, ? extends O> paramFunction) {
    if (isEmpty(paramList))
      return Collections.emptyList(); 
    ArrayList<O> arrayList = new ArrayList();
    for (byte b = 0; b < paramList.size(); b++)
      arrayList.add(paramFunction.apply(paramList.get(b))); 
    return arrayList;
  }
  
  public static <I, O> Set<O> map(Set<I> paramSet, Function<? super I, ? extends O> paramFunction) {
    if (isEmpty(paramSet))
      return Collections.emptySet(); 
    ArraySet arraySet2 = new ArraySet();
    if (paramSet instanceof ArraySet) {
      arraySet1 = (ArraySet)paramSet;
      int i = arraySet1.size();
      for (byte b = 0; b < i; b++)
        arraySet2.add(paramFunction.apply((I)arraySet1.valueAt(b))); 
    } else {
      for (ArraySet arraySet1 : arraySet1)
        arraySet2.add(paramFunction.apply((I)arraySet1)); 
    } 
    return (Set<O>)arraySet2;
  }
  
  public static <I, O> List<O> mapNotNull(List<I> paramList, Function<? super I, ? extends O> paramFunction) {
    if (isEmpty(paramList))
      return Collections.emptyList(); 
    List<O> list = null;
    for (byte b = 0; b < paramList.size(); b++, list = list1) {
      O o = paramFunction.apply(paramList.get(b));
      List<O> list1 = list;
      if (o != null)
        list1 = add(list, o); 
    } 
    return emptyIfNull(list);
  }
  
  public static <T> List<T> emptyIfNull(List<T> paramList) {
    if (paramList == null)
      paramList = Collections.emptyList(); 
    return paramList;
  }
  
  public static <T> Set<T> emptyIfNull(Set<T> paramSet) {
    if (paramSet == null)
      paramSet = Collections.emptySet(); 
    return paramSet;
  }
  
  public static <K, V> Map<K, V> emptyIfNull(Map<K, V> paramMap) {
    if (paramMap == null)
      paramMap = Collections.emptyMap(); 
    return paramMap;
  }
  
  public static int size(Collection<?> paramCollection) {
    boolean bool;
    if (paramCollection != null) {
      bool = paramCollection.size();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static int size(Map<?, ?> paramMap) {
    boolean bool;
    if (paramMap != null) {
      bool = paramMap.size();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isEmpty(Collection<?> paramCollection) {
    boolean bool;
    if (size(paramCollection) == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static <T> List<T> filter(List<?> paramList, Class<T> paramClass) {
    if (isEmpty(paramList))
      return Collections.emptyList(); 
    ArrayList<T> arrayList = null;
    for (byte b = 0; b < paramList.size(); b++, arrayList = arrayList1) {
      Object object = paramList.get(b);
      ArrayList<T> arrayList1 = arrayList;
      if (paramClass.isInstance(object))
        arrayList1 = ArrayUtils.add(arrayList, object); 
    } 
    return emptyIfNull(arrayList);
  }
  
  public static <T> boolean any(List<T> paramList, Predicate<T> paramPredicate) {
    boolean bool;
    if (find(paramList, paramPredicate) != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static <T> boolean any(Set<T> paramSet, Predicate<T> paramPredicate) {
    boolean bool;
    if (find(paramSet, paramPredicate) != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static <T> T find(List<T> paramList, Predicate<T> paramPredicate) {
    if (isEmpty(paramList))
      return null; 
    for (byte b = 0; b < paramList.size(); b++) {
      T t = paramList.get(b);
      if (paramPredicate.test(t))
        return t; 
    } 
    return null;
  }
  
  public static <T> T find(Set<T> paramSet, Predicate<T> paramPredicate) {
    if (paramSet == null || paramPredicate == null)
      return null; 
    int i = paramSet.size();
    if (i == 0)
      return null; 
    try {
      if (paramSet instanceof ArraySet) {
        ArraySet arraySet = (ArraySet)paramSet;
        for (byte b = 0; b < i; b++) {
          object = arraySet.valueAt(b);
          if (paramPredicate.test((T)object))
            return (T)object; 
        } 
      } else {
        for (Object object : object) {
          boolean bool = paramPredicate.test((T)object);
          if (bool)
            return (T)object; 
        } 
      } 
      return null;
    } catch (Exception exception) {
      throw ExceptionUtils.propagate(exception);
    } 
  }
  
  public static <T> List<T> add(List<T> paramList, T paramT) {
    if (paramList != null) {
      List<T> list = paramList;
      if (paramList == Collections.emptyList()) {
        list = new ArrayList<>();
        list.add(paramT);
        return list;
      } 
      list.add(paramT);
      return list;
    } 
    ArrayList<T> arrayList = new ArrayList();
    arrayList.add(paramT);
    return arrayList;
  }
  
  public static <T> List<T> add(List<T> paramList, int paramInt, T paramT) {
    if (paramList != null) {
      List<T> list = paramList;
      if (paramList == Collections.emptyList()) {
        list = new ArrayList<>();
        list.add(paramInt, paramT);
        return list;
      } 
      list.add(paramInt, paramT);
      return list;
    } 
    ArrayList<T> arrayList = new ArrayList();
    arrayList.add(paramInt, paramT);
    return arrayList;
  }
  
  public static <T> Set<T> addAll(Set<T> paramSet, Collection<T> paramCollection) {
    if (isEmpty(paramCollection)) {
      if (paramSet == null)
        paramSet = Collections.emptySet(); 
      return paramSet;
    } 
    if (paramSet != null) {
      ArraySet<T> arraySet1;
      Set<T> set = paramSet;
      if (paramSet == Collections.emptySet()) {
        arraySet1 = new ArraySet();
        arraySet1.addAll(paramCollection);
        return (Set<T>)arraySet1;
      } 
      arraySet1.addAll(paramCollection);
      return (Set<T>)arraySet1;
    } 
    ArraySet<T> arraySet = new ArraySet();
    arraySet.addAll(paramCollection);
    return (Set<T>)arraySet;
  }
  
  public static <T> Set<T> add(Set<T> paramSet, T paramT) {
    if (paramSet != null) {
      ArraySet<T> arraySet1;
      Set<T> set = paramSet;
      if (paramSet == Collections.emptySet()) {
        arraySet1 = new ArraySet();
        arraySet1.add(paramT);
        return (Set<T>)arraySet1;
      } 
      arraySet1.add(paramT);
      return (Set<T>)arraySet1;
    } 
    ArraySet<T> arraySet = new ArraySet();
    arraySet.add(paramT);
    return (Set<T>)arraySet;
  }
  
  public static <K, V> Map<K, V> add(Map<K, V> paramMap, K paramK, V paramV) {
    if (paramMap != null) {
      ArrayMap<K, V> arrayMap1;
      Map<K, V> map = paramMap;
      if (paramMap == Collections.emptyMap()) {
        arrayMap1 = new ArrayMap();
        arrayMap1.put(paramK, paramV);
        return (Map<K, V>)arrayMap1;
      } 
      arrayMap1.put(paramK, paramV);
      return (Map<K, V>)arrayMap1;
    } 
    ArrayMap<K, V> arrayMap = new ArrayMap();
    arrayMap.put(paramK, paramV);
    return (Map<K, V>)arrayMap;
  }
  
  public static <T> List<T> remove(List<T> paramList, T paramT) {
    if (isEmpty(paramList))
      return emptyIfNull(paramList); 
    paramList.remove(paramT);
    return paramList;
  }
  
  public static <T> Set<T> remove(Set<T> paramSet, T paramT) {
    if (isEmpty(paramSet))
      return emptyIfNull(paramSet); 
    paramSet.remove(paramT);
    return paramSet;
  }
  
  public static <T> List<T> copyOf(List<T> paramList) {
    if (isEmpty(paramList)) {
      paramList = Collections.emptyList();
    } else {
      paramList = new ArrayList<>(paramList);
    } 
    return paramList;
  }
  
  public static <T> Set<T> copyOf(Set<T> paramSet) {
    ArraySet arraySet;
    if (isEmpty(paramSet)) {
      paramSet = Collections.emptySet();
    } else {
      arraySet = new ArraySet(paramSet);
    } 
    return (Set<T>)arraySet;
  }
  
  public static <T> Set<T> toSet(Collection<T> paramCollection) {
    ArraySet arraySet;
    if (isEmpty(paramCollection)) {
      paramCollection = Collections.emptySet();
    } else {
      arraySet = new ArraySet(paramCollection);
    } 
    return (Set<T>)arraySet;
  }
  
  public static <T> void forEach(Set<T> paramSet, FunctionalUtils.ThrowingConsumer<T> paramThrowingConsumer) {
    if (paramSet == null || paramThrowingConsumer == null)
      return; 
    int i = paramSet.size();
    if (i == 0)
      return; 
    try {
      ArraySet arraySet;
      if (paramSet instanceof ArraySet) {
        arraySet = (ArraySet)paramSet;
        for (byte b = 0; b < i; b++)
          paramThrowingConsumer.acceptOrThrow((T)arraySet.valueAt(b)); 
      } else {
        for (T t : arraySet)
          paramThrowingConsumer.acceptOrThrow(t); 
      } 
      return;
    } catch (Exception exception) {
      throw ExceptionUtils.propagate(exception);
    } 
  }
  
  public static <K, V> void forEach(Map<K, V> paramMap, BiConsumer<K, V> paramBiConsumer) {
    ArrayMap arrayMap;
    if (paramMap == null || paramBiConsumer == null)
      return; 
    int i = paramMap.size();
    if (i == 0)
      return; 
    if (paramMap instanceof ArrayMap) {
      arrayMap = (ArrayMap)paramMap;
      for (byte b = 0; b < i; b++)
        paramBiConsumer.accept((K)arrayMap.keyAt(b), (V)arrayMap.valueAt(b)); 
    } else {
      for (K k : arrayMap.keySet())
        paramBiConsumer.accept(k, (V)arrayMap.get(k)); 
    } 
  }
  
  public static <T> T firstOrNull(List<T> paramList) {
    if (isEmpty(paramList)) {
      paramList = null;
    } else {
      paramList = (List<T>)paramList.get(0);
    } 
    return (T)paramList;
  }
  
  public static <T> T firstOrNull(Collection<T> paramCollection) {
    if (isEmpty(paramCollection)) {
      paramCollection = null;
    } else {
      paramCollection = paramCollection.iterator().next();
    } 
    return (T)paramCollection;
  }
  
  public static <T> List<T> singletonOrEmpty(T paramT) {
    List<?> list;
    if (paramT == null) {
      list = Collections.emptyList();
    } else {
      list = Collections.singletonList(list);
    } 
    return (List)list;
  }
}
