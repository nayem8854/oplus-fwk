package com.android.internal.util;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

public class ScreenRecordHelper {
  private final Context mContext;
  
  public ScreenRecordHelper(Context paramContext) {
    this.mContext = paramContext;
  }
  
  public void launchRecordPrompt() {
    Context context = this.mContext;
    String str = context.getResources().getString(17039958);
    ComponentName componentName = ComponentName.unflattenFromString(str);
    Intent intent = new Intent();
    intent.setComponent(componentName);
    intent.setFlags(268435456);
    this.mContext.startActivity(intent);
  }
}
