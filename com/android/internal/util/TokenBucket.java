package com.android.internal.util;

import android.os.SystemClock;

public class TokenBucket {
  private int mAvailable;
  
  private final int mCapacity;
  
  private final int mFillDelta;
  
  private long mLastFill;
  
  public TokenBucket(int paramInt1, int paramInt2, int paramInt3) {
    this.mFillDelta = Preconditions.checkArgumentPositive(paramInt1, "deltaMs must be strictly positive");
    this.mCapacity = Preconditions.checkArgumentPositive(paramInt2, "capacity must be strictly positive");
    this.mAvailable = Math.min(Preconditions.checkArgumentNonnegative(paramInt3), this.mCapacity);
    this.mLastFill = scaledTime();
  }
  
  public TokenBucket(int paramInt1, int paramInt2) {
    this(paramInt1, paramInt2, paramInt2);
  }
  
  public void reset(int paramInt) {
    Preconditions.checkArgumentNonnegative(paramInt);
    this.mAvailable = Math.min(paramInt, this.mCapacity);
    this.mLastFill = scaledTime();
  }
  
  public int capacity() {
    return this.mCapacity;
  }
  
  public int available() {
    fill();
    return this.mAvailable;
  }
  
  public boolean has() {
    boolean bool;
    fill();
    if (this.mAvailable > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean get() {
    boolean bool = true;
    if (get(1) != 1)
      bool = false; 
    return bool;
  }
  
  public int get(int paramInt) {
    fill();
    if (paramInt <= 0)
      return 0; 
    int i = this.mAvailable;
    if (paramInt > i) {
      paramInt = this.mAvailable;
      this.mAvailable = 0;
      return paramInt;
    } 
    this.mAvailable = i - paramInt;
    return paramInt;
  }
  
  private void fill() {
    long l = scaledTime();
    int i = (int)(l - this.mLastFill);
    this.mAvailable = Math.min(this.mCapacity, this.mAvailable + i);
    this.mLastFill = l;
  }
  
  private long scaledTime() {
    return SystemClock.elapsedRealtime() / this.mFillDelta;
  }
}
