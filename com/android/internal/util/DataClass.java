package com.android.internal.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.TYPE})
public @interface DataClass {
  boolean genAidl() default false;
  
  boolean genBuilder() default false;
  
  boolean genConstDefs() default true;
  
  boolean genConstructor() default true;
  
  boolean genCopyConstructor() default false;
  
  boolean genEqualsHashCode() default false;
  
  boolean genForEachField() default false;
  
  boolean genGetters() default true;
  
  boolean genHiddenBuilder() default false;
  
  boolean genHiddenConstDefs() default false;
  
  boolean genHiddenConstructor() default false;
  
  boolean genHiddenCopyConstructor() default false;
  
  boolean genHiddenGetters() default false;
  
  boolean genHiddenSetters() default false;
  
  boolean genParcelable() default false;
  
  boolean genSetters() default false;
  
  boolean genToString() default false;
  
  @Retention(RetentionPolicy.SOURCE)
  @Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.LOCAL_VARIABLE})
  public static @interface Each {}
  
  @Deprecated
  @Retention(RetentionPolicy.SOURCE)
  @Target({ElementType.METHOD})
  public static @interface Generated {
    String codegenVersion();
    
    String inputSignatures() default "";
    
    String sourceFile();
    
    long time();
    
    @Deprecated
    @Retention(RetentionPolicy.SOURCE)
    @Target({ElementType.FIELD, ElementType.METHOD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.TYPE})
    public static @interface Member {}
  }
  
  @Deprecated
  @Retention(RetentionPolicy.SOURCE)
  @Target({ElementType.FIELD, ElementType.METHOD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.TYPE})
  public static @interface Member {}
  
  @Retention(RetentionPolicy.SOURCE)
  @Target({ElementType.FIELD})
  public static @interface MaySetToNull {}
  
  @Retention(RetentionPolicy.SOURCE)
  @Target({ElementType.FIELD})
  public static @interface ParcelWith {
    Class<? extends Parcelling> value();
  }
  
  public static interface PerIntFieldAction<THIS> {
    void acceptInt(THIS param1THIS, String param1String, int param1Int);
  }
  
  public static interface PerObjectFieldAction<THIS> {
    void acceptObject(THIS param1THIS, String param1String, Object param1Object);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  @Target({ElementType.FIELD})
  public static @interface PluralOf {
    String value();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  @Target({ElementType.TYPE})
  public static @interface Suppress {
    String[] value();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  @Target({ElementType.FIELD})
  public static @interface SuppressConstDefsGeneration {}
}
