package com.android.internal.util;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.CharacterStyle;
import android.text.style.ForegroundColorSpan;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.util.Pair;
import java.util.Arrays;
import java.util.WeakHashMap;

public class ContrastColorUtil {
  private static final Object sLock = new Object();
  
  private final ImageUtils mImageUtils = new ImageUtils();
  
  private final WeakHashMap<Bitmap, Pair<Boolean, Integer>> mGrayscaleBitmapCache = new WeakHashMap<>();
  
  private static final boolean DEBUG = false;
  
  private static final String TAG = "ContrastColorUtil";
  
  private static ContrastColorUtil sInstance;
  
  private final int mGrayscaleIconMaxSize;
  
  public static ContrastColorUtil getInstance(Context paramContext) {
    synchronized (sLock) {
      if (sInstance == null) {
        ContrastColorUtil contrastColorUtil = new ContrastColorUtil();
        this(paramContext);
        sInstance = contrastColorUtil;
      } 
      return sInstance;
    } 
  }
  
  private ContrastColorUtil(Context paramContext) {
    this.mGrayscaleIconMaxSize = paramContext.getResources().getDimensionPixelSize(17104901);
  }
  
  public boolean isGrayscaleIcon(Bitmap paramBitmap) {
    if (paramBitmap.getWidth() > this.mGrayscaleIconMaxSize || 
      paramBitmap.getHeight() > this.mGrayscaleIconMaxSize)
      return false; 
    synchronized (sLock) {
      Pair pair = this.mGrayscaleBitmapCache.get(paramBitmap);
      if (pair != null && (
        (Integer)pair.second).intValue() == paramBitmap.getGenerationId())
        return ((Boolean)pair.first).booleanValue(); 
      synchronized (this.mImageUtils) {
        boolean bool = this.mImageUtils.isGrayscale(paramBitmap);
        int i = paramBitmap.getGenerationId();
        synchronized (sLock) {
          this.mGrayscaleBitmapCache.put(paramBitmap, Pair.create(Boolean.valueOf(bool), Integer.valueOf(i)));
          return bool;
        } 
      } 
    } 
  }
  
  public boolean isGrayscaleIcon(Drawable paramDrawable) {
    BitmapDrawable bitmapDrawable;
    AnimationDrawable animationDrawable;
    boolean bool1 = false, bool2 = false;
    if (paramDrawable == null)
      return false; 
    if (paramDrawable instanceof BitmapDrawable) {
      bitmapDrawable = (BitmapDrawable)paramDrawable;
      boolean bool = bool2;
      if (bitmapDrawable.getBitmap() != null) {
        bool = bool2;
        if (isGrayscaleIcon(bitmapDrawable.getBitmap()))
          bool = true; 
      } 
      return bool;
    } 
    if (bitmapDrawable instanceof AnimationDrawable) {
      animationDrawable = (AnimationDrawable)bitmapDrawable;
      int i = animationDrawable.getNumberOfFrames();
      boolean bool = bool1;
      if (i > 0) {
        bool = bool1;
        if (isGrayscaleIcon(animationDrawable.getFrame(0)))
          bool = true; 
      } 
      return bool;
    } 
    if (animationDrawable instanceof android.graphics.drawable.VectorDrawable)
      return true; 
    return false;
  }
  
  public boolean isGrayscaleIcon(Context paramContext, Icon paramIcon) {
    if (paramIcon == null)
      return false; 
    int i = paramIcon.getType();
    if (i != 1) {
      if (i != 2)
        return false; 
      return isGrayscaleIcon(paramContext, paramIcon.getResId());
    } 
    return isGrayscaleIcon(paramIcon.getBitmap());
  }
  
  public boolean isGrayscaleIcon(Context paramContext, int paramInt) {
    if (paramInt != 0)
      try {
        return isGrayscaleIcon(paramContext.getDrawable(paramInt));
      } catch (android.content.res.Resources.NotFoundException notFoundException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Drawable not found: ");
        stringBuilder.append(paramInt);
        Log.e("ContrastColorUtil", stringBuilder.toString());
        return false;
      }  
    return false;
  }
  
  public CharSequence invertCharSequenceColors(CharSequence paramCharSequence) {
    Object object;
    if (paramCharSequence instanceof Spanned) {
      Spanned spanned = (Spanned)paramCharSequence;
      int i = spanned.length();
      byte b = 0;
      Object[] arrayOfObject = spanned.getSpans(0, i, Object.class);
      SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(spanned.toString());
      for (i = arrayOfObject.length; b < i; ) {
        object = arrayOfObject[b];
        Object object1 = object;
        Object object2 = object1;
        if (object1 instanceof CharacterStyle)
          object2 = ((CharacterStyle)object).getUnderlying(); 
        if (object2 instanceof TextAppearanceSpan) {
          object1 = processTextAppearanceSpan((TextAppearanceSpan)object);
          if (object1 != object2) {
            object2 = object1;
          } else {
            object2 = object;
          } 
        } else if (object2 instanceof ForegroundColorSpan) {
          object2 = object2;
          int n = object2.getForegroundColor();
          object2 = new ForegroundColorSpan(processColor(n));
        } else {
          object2 = object;
        } 
        int j = spanned.getSpanStart(object), k = spanned.getSpanEnd(object);
        int m = spanned.getSpanFlags(object);
        spannableStringBuilder.setSpan(object2, j, k, m);
        b++;
      } 
      return (CharSequence)spannableStringBuilder;
    } 
    return (CharSequence)object;
  }
  
  private TextAppearanceSpan processTextAppearanceSpan(TextAppearanceSpan paramTextAppearanceSpan) {
    ColorStateList colorStateList = paramTextAppearanceSpan.getTextColor();
    if (colorStateList != null) {
      int[] arrayOfInt = colorStateList.getColors();
      int i = 0;
      int j;
      for (j = 0; j < arrayOfInt.length; j++, arrayOfInt = arrayOfInt1, i = k) {
        int arrayOfInt1[] = arrayOfInt, k = i;
        if (ImageUtils.isGrayscale(arrayOfInt[j])) {
          arrayOfInt1 = arrayOfInt;
          if (!i)
            arrayOfInt1 = Arrays.copyOf(arrayOfInt, arrayOfInt.length); 
          arrayOfInt1[j] = processColor(arrayOfInt1[j]);
          k = 1;
        } 
      } 
      if (i) {
        String str = paramTextAppearanceSpan.getFamily();
        j = paramTextAppearanceSpan.getTextStyle();
        i = paramTextAppearanceSpan.getTextSize();
        ColorStateList colorStateList1 = new ColorStateList(colorStateList.getStates(), arrayOfInt);
        return new TextAppearanceSpan(str, j, i, colorStateList1, paramTextAppearanceSpan.getLinkTextColor());
      } 
    } 
    return paramTextAppearanceSpan;
  }
  
  public static CharSequence clearColorSpans(CharSequence paramCharSequence) {
    Object object;
    if (paramCharSequence instanceof Spanned) {
      Spanned spanned = (Spanned)paramCharSequence;
      int i = spanned.length();
      byte b = 0;
      Object[] arrayOfObject = spanned.getSpans(0, i, Object.class);
      SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(spanned.toString());
      for (i = arrayOfObject.length; b < i; ) {
        Object object1 = arrayOfObject[b];
        Object object2 = object1;
        object = object2;
        if (object2 instanceof CharacterStyle)
          object = ((CharacterStyle)object1).getUnderlying(); 
        if (object instanceof TextAppearanceSpan) {
          object2 = object;
          if (object2.getTextColor() != null) {
            object = object2.getFamily();
            int n = object2.getTextStyle();
            int i1 = object2.getTextSize();
            object = new TextAppearanceSpan((String)object, n, i1, null, object2.getLinkTextColor());
          } 
        } else {
          if (object instanceof ForegroundColorSpan || object instanceof android.text.style.BackgroundColorSpan)
            continue; 
          object = object1;
        } 
        int k = spanned.getSpanStart(object1), m = spanned.getSpanEnd(object1);
        int j = spanned.getSpanFlags(object1);
        spannableStringBuilder.setSpan(object, k, m, j);
        continue;
        b++;
      } 
      return (CharSequence)spannableStringBuilder;
    } 
    return (CharSequence)object;
  }
  
  private int processColor(int paramInt) {
    int i = Color.alpha(paramInt);
    int j = Color.red(paramInt);
    int k = Color.green(paramInt);
    paramInt = Color.blue(paramInt);
    return Color.argb(i, 255 - j, 255 - k, 255 - paramInt);
  }
  
  public static int findContrastColor(int paramInt1, int paramInt2, boolean paramBoolean, double paramDouble) {
    int i;
    if (paramBoolean) {
      i = paramInt1;
    } else {
      i = paramInt2;
    } 
    if (!paramBoolean)
      paramInt2 = paramInt1; 
    if (ColorUtilsFromCompat.calculateContrast(i, paramInt2) >= paramDouble)
      return paramInt1; 
    double[] arrayOfDouble = new double[3];
    if (paramBoolean) {
      paramInt1 = i;
    } else {
      paramInt1 = paramInt2;
    } 
    ColorUtilsFromCompat.colorToLAB(paramInt1, arrayOfDouble);
    double d1 = 0.0D, d2 = arrayOfDouble[0];
    double d3 = arrayOfDouble[1], d4 = arrayOfDouble[2];
    for (paramInt1 = 0; paramInt1 < 15 && d2 - d1 > 1.0E-5D; paramInt1++) {
      double d = (d1 + d2) / 2.0D;
      if (paramBoolean) {
        i = ColorUtilsFromCompat.LABToColor(d, d3, d4);
      } else {
        paramInt2 = ColorUtilsFromCompat.LABToColor(d, d3, d4);
      } 
      if (ColorUtilsFromCompat.calculateContrast(i, paramInt2) > paramDouble) {
        d1 = d;
      } else {
        d2 = d;
      } 
    } 
    return ColorUtilsFromCompat.LABToColor(d1, d3, d4);
  }
  
  public static int findAlphaToMeetContrast(int paramInt1, int paramInt2, double paramDouble) {
    if (ColorUtilsFromCompat.calculateContrast(paramInt1, paramInt2) >= paramDouble)
      return paramInt1; 
    int i = Color.alpha(paramInt1);
    int j = Color.red(paramInt1);
    int k = Color.green(paramInt1);
    int m = Color.blue(paramInt1);
    int n = 255;
    for (paramInt1 = 0; paramInt1 < 15 && n - i > 0; paramInt1++) {
      int i1 = (i + n) / 2;
      int i2 = Color.argb(i1, j, k, m);
      if (ColorUtilsFromCompat.calculateContrast(i2, paramInt2) > paramDouble) {
        n = i1;
      } else {
        i = i1;
      } 
    } 
    return Color.argb(n, j, k, m);
  }
  
  public static int findContrastColorAgainstDark(int paramInt1, int paramInt2, boolean paramBoolean, double paramDouble) {
    int i;
    if (paramBoolean) {
      i = paramInt1;
    } else {
      i = paramInt2;
    } 
    if (!paramBoolean)
      paramInt2 = paramInt1; 
    if (ColorUtilsFromCompat.calculateContrast(i, paramInt2) >= paramDouble)
      return paramInt1; 
    float[] arrayOfFloat = new float[3];
    if (paramBoolean) {
      paramInt1 = i;
    } else {
      paramInt1 = paramInt2;
    } 
    ColorUtilsFromCompat.colorToHSL(paramInt1, arrayOfFloat);
    float f1 = arrayOfFloat[2], f2 = 1.0F;
    for (paramInt1 = 0; paramInt1 < 15 && (f2 - f1) > 1.0E-5D; paramInt1++) {
      float f = (f1 + f2) / 2.0F;
      arrayOfFloat[2] = f;
      if (paramBoolean) {
        i = ColorUtilsFromCompat.HSLToColor(arrayOfFloat);
      } else {
        paramInt2 = ColorUtilsFromCompat.HSLToColor(arrayOfFloat);
      } 
      if (ColorUtilsFromCompat.calculateContrast(i, paramInt2) > paramDouble) {
        f2 = f;
      } else {
        f1 = f;
      } 
    } 
    if (paramBoolean)
      paramInt2 = i; 
    return paramInt2;
  }
  
  public static int ensureTextContrastOnBlack(int paramInt) {
    return findContrastColorAgainstDark(paramInt, -16777216, true, 12.0D);
  }
  
  public static int ensureLargeTextContrast(int paramInt1, int paramInt2, boolean paramBoolean) {
    if (paramBoolean) {
      paramInt1 = findContrastColorAgainstDark(paramInt1, paramInt2, true, 3.0D);
    } else {
      paramInt1 = findContrastColor(paramInt1, paramInt2, true, 3.0D);
    } 
    return paramInt1;
  }
  
  public static int ensureTextContrast(int paramInt1, int paramInt2, boolean paramBoolean) {
    return ensureContrast(paramInt1, paramInt2, paramBoolean, 4.5D);
  }
  
  public static int ensureContrast(int paramInt1, int paramInt2, boolean paramBoolean, double paramDouble) {
    if (paramBoolean) {
      paramInt1 = findContrastColorAgainstDark(paramInt1, paramInt2, true, paramDouble);
    } else {
      paramInt1 = findContrastColor(paramInt1, paramInt2, true, paramDouble);
    } 
    return paramInt1;
  }
  
  public static int ensureTextBackgroundColor(int paramInt1, int paramInt2, int paramInt3) {
    paramInt1 = findContrastColor(paramInt1, paramInt3, false, 3.0D);
    return findContrastColor(paramInt1, paramInt2, false, 4.5D);
  }
  
  private static String contrastChange(int paramInt1, int paramInt2, int paramInt3) {
    double d1 = ColorUtilsFromCompat.calculateContrast(paramInt1, paramInt3);
    double d2 = ColorUtilsFromCompat.calculateContrast(paramInt2, paramInt3);
    return String.format("from %.2f:1 to %.2f:1", new Object[] { Double.valueOf(d1), Double.valueOf(d2) });
  }
  
  public static int resolveColor(Context paramContext, int paramInt, boolean paramBoolean) {
    if (paramInt == 0) {
      if (paramBoolean) {
        paramInt = 17170886;
      } else {
        paramInt = 17170887;
      } 
      return paramContext.getColor(paramInt);
    } 
    return paramInt;
  }
  
  public static int resolveContrastColor(Context paramContext, int paramInt1, int paramInt2) {
    return resolveContrastColor(paramContext, paramInt1, paramInt2, false);
  }
  
  public static int resolveContrastColor(Context paramContext, int paramInt1, int paramInt2, boolean paramBoolean) {
    paramInt1 = resolveColor(paramContext, paramInt1, paramBoolean);
    paramInt1 = ensureTextContrast(paramInt1, paramInt2, paramBoolean);
    return paramInt1;
  }
  
  public static int changeColorLightness(int paramInt1, int paramInt2) {
    double[] arrayOfDouble = ColorUtilsFromCompat.getTempDouble3Array();
    ColorUtilsFromCompat.colorToLAB(paramInt1, arrayOfDouble);
    arrayOfDouble[0] = Math.max(Math.min(100.0D, arrayOfDouble[0] + paramInt2), 0.0D);
    return ColorUtilsFromCompat.LABToColor(arrayOfDouble[0], arrayOfDouble[1], arrayOfDouble[2]);
  }
  
  public static int resolvePrimaryColor(Context paramContext, int paramInt, boolean paramBoolean) {
    paramBoolean = shouldUseDark(paramInt, paramBoolean);
    if (paramBoolean)
      return paramContext.getColor(17170890); 
    return paramContext.getColor(17170889);
  }
  
  public static int resolveSecondaryColor(Context paramContext, int paramInt, boolean paramBoolean) {
    paramBoolean = shouldUseDark(paramInt, paramBoolean);
    if (paramBoolean)
      return paramContext.getColor(17170893); 
    return paramContext.getColor(17170892);
  }
  
  public static int resolveDefaultColor(Context paramContext, int paramInt, boolean paramBoolean) {
    paramBoolean = shouldUseDark(paramInt, paramBoolean);
    if (paramBoolean)
      return paramContext.getColor(17170887); 
    return paramContext.getColor(17170886);
  }
  
  public static int getShiftedColor(int paramInt1, int paramInt2) {
    double[] arrayOfDouble = ColorUtilsFromCompat.getTempDouble3Array();
    ColorUtilsFromCompat.colorToLAB(paramInt1, arrayOfDouble);
    if (arrayOfDouble[0] >= 4.0D) {
      arrayOfDouble[0] = Math.max(0.0D, arrayOfDouble[0] - paramInt2);
    } else {
      arrayOfDouble[0] = Math.min(100.0D, arrayOfDouble[0] + paramInt2);
    } 
    return ColorUtilsFromCompat.LABToColor(arrayOfDouble[0], arrayOfDouble[1], arrayOfDouble[2]);
  }
  
  public static int getMutedColor(int paramInt, float paramFloat) {
    int i = ColorUtilsFromCompat.setAlphaComponent(-1, (int)(255.0F * paramFloat));
    return compositeColors(i, paramInt);
  }
  
  private static boolean shouldUseDark(int paramInt, boolean paramBoolean) {
    if (paramInt == 0)
      return paramBoolean ^ true; 
    if (ColorUtilsFromCompat.calculateLuminance(paramInt) > 0.5D) {
      paramBoolean = true;
    } else {
      paramBoolean = false;
    } 
    return paramBoolean;
  }
  
  public static double calculateLuminance(int paramInt) {
    return ColorUtilsFromCompat.calculateLuminance(paramInt);
  }
  
  public static double calculateContrast(int paramInt1, int paramInt2) {
    return ColorUtilsFromCompat.calculateContrast(paramInt1, paramInt2);
  }
  
  public static boolean satisfiesTextContrast(int paramInt1, int paramInt2) {
    boolean bool;
    if (calculateContrast(paramInt2, paramInt1) >= 4.5D) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static int compositeColors(int paramInt1, int paramInt2) {
    return ColorUtilsFromCompat.compositeColors(paramInt1, paramInt2);
  }
  
  public static boolean isColorLight(int paramInt) {
    boolean bool;
    if (calculateLuminance(paramInt) > 0.5D) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static class ColorUtilsFromCompat {
    private static final int MIN_ALPHA_SEARCH_MAX_ITERATIONS = 10;
    
    private static final int MIN_ALPHA_SEARCH_PRECISION = 1;
    
    private static final ThreadLocal<double[]> TEMP_ARRAY = (ThreadLocal)new ThreadLocal<>();
    
    private static final double XYZ_EPSILON = 0.008856D;
    
    private static final double XYZ_KAPPA = 903.3D;
    
    private static final double XYZ_WHITE_REFERENCE_X = 95.047D;
    
    private static final double XYZ_WHITE_REFERENCE_Y = 100.0D;
    
    private static final double XYZ_WHITE_REFERENCE_Z = 108.883D;
    
    public static int compositeColors(int param1Int1, int param1Int2) {
      int i = Color.alpha(param1Int2);
      int j = Color.alpha(param1Int1);
      int k = compositeAlpha(j, i);
      int m = Color.red(param1Int1);
      int n = Color.red(param1Int2);
      n = compositeComponent(m, j, n, i, k);
      int i1 = Color.green(param1Int1);
      m = Color.green(param1Int2);
      m = compositeComponent(i1, j, m, i, k);
      param1Int1 = Color.blue(param1Int1);
      param1Int2 = Color.blue(param1Int2);
      param1Int1 = compositeComponent(param1Int1, j, param1Int2, i, k);
      return Color.argb(k, n, m, param1Int1);
    }
    
    private static int compositeAlpha(int param1Int1, int param1Int2) {
      return 255 - (255 - param1Int2) * (255 - param1Int1) / 255;
    }
    
    private static int compositeComponent(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5) {
      if (param1Int5 == 0)
        return 0; 
      return (param1Int1 * 255 * param1Int2 + param1Int3 * param1Int4 * (255 - param1Int2)) / param1Int5 * 255;
    }
    
    public static int setAlphaComponent(int param1Int1, int param1Int2) {
      if (param1Int2 >= 0 && param1Int2 <= 255)
        return 0xFFFFFF & param1Int1 | param1Int2 << 24; 
      throw new IllegalArgumentException("alpha must be between 0 and 255.");
    }
    
    public static double calculateLuminance(int param1Int) {
      double[] arrayOfDouble = getTempDouble3Array();
      colorToXYZ(param1Int, arrayOfDouble);
      return arrayOfDouble[1] / 100.0D;
    }
    
    public static double calculateContrast(int param1Int1, int param1Int2) {
      if (Color.alpha(param1Int2) != 255) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("background can not be translucent: #");
        stringBuilder.append(Integer.toHexString(param1Int2));
        String str = stringBuilder.toString();
        Log.wtf("ContrastColorUtil", str);
      } 
      int i = param1Int1;
      if (Color.alpha(param1Int1) < 255)
        i = compositeColors(param1Int1, param1Int2); 
      double d1 = calculateLuminance(i) + 0.05D;
      double d2 = calculateLuminance(param1Int2) + 0.05D;
      return Math.max(d1, d2) / Math.min(d1, d2);
    }
    
    public static void colorToLAB(int param1Int, double[] param1ArrayOfdouble) {
      RGBToLAB(Color.red(param1Int), Color.green(param1Int), Color.blue(param1Int), param1ArrayOfdouble);
    }
    
    public static void RGBToLAB(int param1Int1, int param1Int2, int param1Int3, double[] param1ArrayOfdouble) {
      RGBToXYZ(param1Int1, param1Int2, param1Int3, param1ArrayOfdouble);
      XYZToLAB(param1ArrayOfdouble[0], param1ArrayOfdouble[1], param1ArrayOfdouble[2], param1ArrayOfdouble);
    }
    
    public static void colorToXYZ(int param1Int, double[] param1ArrayOfdouble) {
      RGBToXYZ(Color.red(param1Int), Color.green(param1Int), Color.blue(param1Int), param1ArrayOfdouble);
    }
    
    public static void RGBToXYZ(int param1Int1, int param1Int2, int param1Int3, double[] param1ArrayOfdouble) {
      if (param1ArrayOfdouble.length == 3) {
        double d1 = param1Int1 / 255.0D;
        if (d1 < 0.04045D) {
          d1 /= 12.92D;
        } else {
          d1 = Math.pow((d1 + 0.055D) / 1.055D, 2.4D);
        } 
        double d2 = param1Int2 / 255.0D;
        if (d2 < 0.04045D) {
          d2 /= 12.92D;
        } else {
          d2 = Math.pow((d2 + 0.055D) / 1.055D, 2.4D);
        } 
        double d3 = param1Int3 / 255.0D;
        if (d3 < 0.04045D) {
          d3 /= 12.92D;
        } else {
          d3 = Math.pow((0.055D + d3) / 1.055D, 2.4D);
        } 
        param1ArrayOfdouble[0] = (0.4124D * d1 + 0.3576D * d2 + 0.1805D * d3) * 100.0D;
        param1ArrayOfdouble[1] = (0.2126D * d1 + 0.7152D * d2 + 0.0722D * d3) * 100.0D;
        param1ArrayOfdouble[2] = (0.0193D * d1 + 0.1192D * d2 + 0.9505D * d3) * 100.0D;
        return;
      } 
      throw new IllegalArgumentException("outXyz must have a length of 3.");
    }
    
    public static void XYZToLAB(double param1Double1, double param1Double2, double param1Double3, double[] param1ArrayOfdouble) {
      if (param1ArrayOfdouble.length == 3) {
        param1Double1 = pivotXyzComponent(param1Double1 / 95.047D);
        param1Double2 = pivotXyzComponent(param1Double2 / 100.0D);
        param1Double3 = pivotXyzComponent(param1Double3 / 108.883D);
        param1ArrayOfdouble[0] = Math.max(0.0D, 116.0D * param1Double2 - 16.0D);
        param1ArrayOfdouble[1] = (param1Double1 - param1Double2) * 500.0D;
        param1ArrayOfdouble[2] = (param1Double2 - param1Double3) * 200.0D;
        return;
      } 
      throw new IllegalArgumentException("outLab must have a length of 3.");
    }
    
    public static void LABToXYZ(double param1Double1, double param1Double2, double param1Double3, double[] param1ArrayOfdouble) {
      double d1 = (param1Double1 + 16.0D) / 116.0D;
      double d2 = param1Double2 / 500.0D + d1;
      double d3 = d1 - param1Double3 / 200.0D;
      param1Double2 = Math.pow(d2, 3.0D);
      if (param1Double2 <= 0.008856D)
        param1Double2 = (d2 * 116.0D - 16.0D) / 903.3D; 
      if (param1Double1 > 7.9996247999999985D) {
        param1Double1 = Math.pow(d1, 3.0D);
      } else {
        param1Double1 /= 903.3D;
      } 
      param1Double3 = Math.pow(d3, 3.0D);
      if (param1Double3 <= 0.008856D)
        param1Double3 = (116.0D * d3 - 16.0D) / 903.3D; 
      param1ArrayOfdouble[0] = 95.047D * param1Double2;
      param1ArrayOfdouble[1] = 100.0D * param1Double1;
      param1ArrayOfdouble[2] = 108.883D * param1Double3;
    }
    
    public static int XYZToColor(double param1Double1, double param1Double2, double param1Double3) {
      double d1 = (3.2406D * param1Double1 + -1.5372D * param1Double2 + -0.4986D * param1Double3) / 100.0D;
      double d2 = (-0.9689D * param1Double1 + 1.8758D * param1Double2 + 0.0415D * param1Double3) / 100.0D;
      param1Double3 = (0.0557D * param1Double1 + -0.204D * param1Double2 + 1.057D * param1Double3) / 100.0D;
      if (d1 > 0.0031308D) {
        param1Double1 = Math.pow(d1, 0.4166666666666667D) * 1.055D - 0.055D;
      } else {
        param1Double1 = d1 * 12.92D;
      } 
      if (d2 > 0.0031308D) {
        param1Double2 = Math.pow(d2, 0.4166666666666667D) * 1.055D - 0.055D;
      } else {
        param1Double2 = d2 * 12.92D;
      } 
      if (param1Double3 > 0.0031308D) {
        param1Double3 = Math.pow(param1Double3, 0.4166666666666667D) * 1.055D - 0.055D;
      } else {
        param1Double3 *= 12.92D;
      } 
      int i = constrain((int)Math.round(param1Double1 * 255.0D), 0, 255);
      int j = constrain((int)Math.round(param1Double2 * 255.0D), 0, 255);
      int k = constrain((int)Math.round(255.0D * param1Double3), 0, 255);
      return Color.rgb(i, j, k);
    }
    
    public static int LABToColor(double param1Double1, double param1Double2, double param1Double3) {
      double[] arrayOfDouble = getTempDouble3Array();
      LABToXYZ(param1Double1, param1Double2, param1Double3, arrayOfDouble);
      return XYZToColor(arrayOfDouble[0], arrayOfDouble[1], arrayOfDouble[2]);
    }
    
    private static int constrain(int param1Int1, int param1Int2, int param1Int3) {
      if (param1Int1 < param1Int2) {
        param1Int1 = param1Int2;
      } else if (param1Int1 > param1Int3) {
        param1Int1 = param1Int3;
      } 
      return param1Int1;
    }
    
    private static float constrain(float param1Float1, float param1Float2, float param1Float3) {
      if (param1Float1 < param1Float2) {
        param1Float1 = param1Float2;
      } else if (param1Float1 > param1Float3) {
        param1Float1 = param1Float3;
      } 
      return param1Float1;
    }
    
    private static double pivotXyzComponent(double param1Double) {
      if (param1Double > 0.008856D) {
        param1Double = Math.pow(param1Double, 0.3333333333333333D);
      } else {
        param1Double = (903.3D * param1Double + 16.0D) / 116.0D;
      } 
      return param1Double;
    }
    
    public static double[] getTempDouble3Array() {
      double[] arrayOfDouble1 = TEMP_ARRAY.get();
      double[] arrayOfDouble2 = arrayOfDouble1;
      if (arrayOfDouble1 == null) {
        arrayOfDouble2 = new double[3];
        TEMP_ARRAY.set(arrayOfDouble2);
      } 
      return arrayOfDouble2;
    }
    
    public static int HSLToColor(float[] param1ArrayOffloat) {
      float f1 = param1ArrayOffloat[0];
      float f2 = param1ArrayOffloat[1];
      float f3 = param1ArrayOffloat[2];
      f2 = (1.0F - Math.abs(f3 * 2.0F - 1.0F)) * f2;
      f3 -= 0.5F * f2;
      float f4 = (1.0F - Math.abs(f1 / 60.0F % 2.0F - 1.0F)) * f2;
      int i = (int)f1 / 60;
      int j = 0, k = 0, m = 0;
      switch (i) {
        default:
          j = constrain(j, 0, 255);
          k = constrain(k, 0, 255);
          m = constrain(m, 0, 255);
          return Color.rgb(j, k, m);
        case 5:
        case 6:
          j = Math.round((f2 + f3) * 255.0F);
          k = Math.round(f3 * 255.0F);
          m = Math.round((f4 + f3) * 255.0F);
        case 4:
          j = Math.round((f4 + f3) * 255.0F);
          k = Math.round(f3 * 255.0F);
          m = Math.round((f2 + f3) * 255.0F);
        case 3:
          j = Math.round(f3 * 255.0F);
          k = Math.round((f4 + f3) * 255.0F);
          m = Math.round((f2 + f3) * 255.0F);
        case 2:
          j = Math.round(f3 * 255.0F);
          k = Math.round((f2 + f3) * 255.0F);
          m = Math.round((f4 + f3) * 255.0F);
        case 1:
          j = Math.round((f4 + f3) * 255.0F);
          k = Math.round((f2 + f3) * 255.0F);
          m = Math.round(255.0F * f3);
        case 0:
          break;
      } 
      j = Math.round((f2 + f3) * 255.0F);
      k = Math.round((f4 + f3) * 255.0F);
      m = Math.round(255.0F * f3);
    }
    
    public static void colorToHSL(int param1Int, float[] param1ArrayOffloat) {
      RGBToHSL(Color.red(param1Int), Color.green(param1Int), Color.blue(param1Int), param1ArrayOffloat);
    }
    
    public static void RGBToHSL(int param1Int1, int param1Int2, int param1Int3, float[] param1ArrayOffloat) {
      float f1 = param1Int1 / 255.0F;
      float f2 = param1Int2 / 255.0F;
      float f3 = param1Int3 / 255.0F;
      float f4 = Math.max(f1, Math.max(f2, f3));
      float f5 = Math.min(f1, Math.min(f2, f3));
      float f6 = f4 - f5;
      float f7 = (f4 + f5) / 2.0F;
      if (f4 == f5) {
        f5 = 0.0F;
        f6 = 0.0F;
      } else {
        if (f4 == f1) {
          f5 = (f2 - f3) / f6 % 6.0F;
        } else if (f4 == f2) {
          f5 = (f3 - f1) / f6 + 2.0F;
        } else {
          f5 = (f1 - f2) / f6 + 4.0F;
        } 
        f2 = f6 / (1.0F - Math.abs(2.0F * f7 - 1.0F));
        f6 = f5;
        f5 = f2;
      } 
      f2 = 60.0F * f6 % 360.0F;
      f6 = f2;
      if (f2 < 0.0F)
        f6 = f2 + 360.0F; 
      param1ArrayOffloat[0] = constrain(f6, 0.0F, 360.0F);
      param1ArrayOffloat[1] = constrain(f5, 0.0F, 1.0F);
      param1ArrayOffloat[2] = constrain(f7, 0.0F, 1.0F);
    }
  }
}
