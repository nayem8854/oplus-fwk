package com.android.internal.util;

import android.app.Notification;
import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.service.notification.StatusBarNotification;
import android.util.ArrayMap;
import java.util.Objects;

public class NotificationMessagingUtil {
  private static final String DEFAULT_SMS_APP_SETTING = "sms_default_application";
  
  private final Context mContext;
  
  private ArrayMap<Integer, String> mDefaultSmsApp = new ArrayMap();
  
  private final ContentObserver mSmsContentObserver;
  
  public boolean isImportantMessaging(StatusBarNotification paramStatusBarNotification, int paramInt) {
    boolean bool = false;
    if (paramInt < 2)
      return false; 
    if (!hasMessagingStyle(paramStatusBarNotification)) {
      boolean bool1 = bool;
      if (isCategoryMessage(paramStatusBarNotification)) {
        bool1 = bool;
        if (isDefaultMessagingApp(paramStatusBarNotification))
          return true; 
      } 
      return bool1;
    } 
    return true;
  }
  
  public boolean isMessaging(StatusBarNotification paramStatusBarNotification) {
    return (hasMessagingStyle(paramStatusBarNotification) || isDefaultMessagingApp(paramStatusBarNotification) || isCategoryMessage(paramStatusBarNotification));
  }
  
  private boolean isDefaultMessagingApp(StatusBarNotification paramStatusBarNotification) {
    int i = paramStatusBarNotification.getUserId();
    if (i == -10000 || i == -1)
      return false; 
    if (this.mDefaultSmsApp.get(Integer.valueOf(i)) == null)
      cacheDefaultSmsApp(i); 
    return Objects.equals(this.mDefaultSmsApp.get(Integer.valueOf(i)), paramStatusBarNotification.getPackageName());
  }
  
  private void cacheDefaultSmsApp(int paramInt) {
    ArrayMap<Integer, String> arrayMap = this.mDefaultSmsApp;
    Context context = this.mContext;
    ContentResolver contentResolver = context.getContentResolver();
    arrayMap.put(Integer.valueOf(paramInt), Settings.Secure.getStringForUser(contentResolver, "sms_default_application", paramInt));
  }
  
  public NotificationMessagingUtil(Context paramContext) {
    this.mSmsContentObserver = (ContentObserver)new Object(this, new Handler(Looper.getMainLooper()));
    this.mContext = paramContext;
    ContentResolver contentResolver = paramContext.getContentResolver();
    Uri uri = Settings.Secure.getUriFor("sms_default_application");
    ContentObserver contentObserver = this.mSmsContentObserver;
    contentResolver.registerContentObserver(uri, false, contentObserver);
  }
  
  private boolean hasMessagingStyle(StatusBarNotification paramStatusBarNotification) {
    Class clazz = paramStatusBarNotification.getNotification().getNotificationStyle();
    return Notification.MessagingStyle.class.equals(clazz);
  }
  
  private boolean isCategoryMessage(StatusBarNotification paramStatusBarNotification) {
    return "msg".equals((paramStatusBarNotification.getNotification()).category);
  }
}
