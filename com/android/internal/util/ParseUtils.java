package com.android.internal.util;

public final class ParseUtils {
  public static int parseInt(String paramString, int paramInt) {
    return parseIntWithBase(paramString, 10, paramInt);
  }
  
  public static int parseIntWithBase(String paramString, int paramInt1, int paramInt2) {
    if (paramString == null)
      return paramInt2; 
    try {
      return Integer.parseInt(paramString, paramInt1);
    } catch (NumberFormatException numberFormatException) {
      return paramInt2;
    } 
  }
  
  public static long parseLong(String paramString, long paramLong) {
    return parseLongWithBase(paramString, 10, paramLong);
  }
  
  public static long parseLongWithBase(String paramString, int paramInt, long paramLong) {
    if (paramString == null)
      return paramLong; 
    try {
      return Long.parseLong(paramString, paramInt);
    } catch (NumberFormatException numberFormatException) {
      return paramLong;
    } 
  }
  
  public static float parseFloat(String paramString, float paramFloat) {
    if (paramString == null)
      return paramFloat; 
    try {
      return Float.parseFloat(paramString);
    } catch (NumberFormatException numberFormatException) {
      return paramFloat;
    } 
  }
  
  public static double parseDouble(String paramString, double paramDouble) {
    if (paramString == null)
      return paramDouble; 
    try {
      return Double.parseDouble(paramString);
    } catch (NumberFormatException numberFormatException) {
      return paramDouble;
    } 
  }
  
  public static boolean parseBoolean(String paramString, boolean paramBoolean) {
    boolean bool = "true".equals(paramString);
    boolean bool1 = true;
    if (bool)
      return true; 
    if ("false".equals(paramString))
      return false; 
    if (parseInt(paramString, paramBoolean) == 0)
      bool1 = false; 
    return bool1;
  }
}
