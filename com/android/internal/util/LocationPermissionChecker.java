package com.android.internal.util;

import android.app.ActivityManager;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Binder;
import android.os.UserHandle;
import android.util.Log;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class LocationPermissionChecker {
  public static final int ERROR_LOCATION_MODE_OFF = 1;
  
  public static final int ERROR_LOCATION_PERMISSION_MISSING = 2;
  
  public static final int SUCCEEDED = 0;
  
  private static final String TAG = "LocationPermissionChecker";
  
  private final AppOpsManager mAppOpsManager;
  
  private final Context mContext;
  
  public LocationPermissionChecker(Context paramContext) {
    this.mContext = paramContext;
    this.mAppOpsManager = (AppOpsManager)paramContext.getSystemService("appops");
  }
  
  public boolean checkLocationPermission(String paramString1, String paramString2, int paramInt, String paramString3) {
    boolean bool;
    if (checkLocationPermissionInternal(paramString1, paramString2, paramInt, paramString3) == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int checkLocationPermissionWithDetailInfo(String paramString1, String paramString2, int paramInt, String paramString3) {
    int i = checkLocationPermissionInternal(paramString1, paramString2, paramInt, paramString3);
    if (i != 1) {
      if (i == 2) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("UID ");
        stringBuilder.append(paramInt);
        stringBuilder.append(" has no location permission");
        Log.e("LocationPermissionChecker", stringBuilder.toString());
      } 
    } else {
      Log.e("LocationPermissionChecker", "Location mode is disabled for the device");
    } 
    return i;
  }
  
  public void enforceLocationPermission(String paramString1, String paramString2, int paramInt, String paramString3) throws SecurityException {
    int i = checkLocationPermissionInternal(paramString1, paramString2, paramInt, paramString3);
    if (i != 1) {
      if (i != 2)
        return; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("UID ");
      stringBuilder.append(paramInt);
      stringBuilder.append(" has no location permission");
      throw new SecurityException(stringBuilder.toString());
    } 
    throw new SecurityException("Location mode is disabled for the device");
  }
  
  private int checkLocationPermissionInternal(String paramString1, String paramString2, int paramInt, String paramString3) {
    checkPackage(paramInt, paramString1);
    if (!isLocationModeEnabled())
      return 1; 
    if (!checkCallersLocationPermission(paramString1, paramString2, paramInt, true, paramString3))
      return 2; 
    return 0;
  }
  
  public boolean checkCallersLocationPermission(String paramString1, String paramString2, int paramInt, boolean paramBoolean, String paramString3) {
    String str;
    boolean bool1 = isTargetSdkLessThan(paramString1, 29, paramInt);
    if (paramBoolean && bool1) {
      str = "android.permission.ACCESS_COARSE_LOCATION";
    } else {
      str = "android.permission.ACCESS_FINE_LOCATION";
    } 
    if (getUidPermission(str, paramInt) == -1)
      return false; 
    boolean bool2 = noteAppOpAllowed("android:fine_location", paramString1, paramString2, paramInt, paramString3);
    if (bool2)
      return true; 
    if (paramBoolean && bool1)
      return noteAppOpAllowed("android:coarse_location", paramString1, paramString2, paramInt, paramString3); 
    return false;
  }
  
  public boolean isLocationModeEnabled() {
    Context context = this.mContext;
    LocationManager locationManager = (LocationManager)context.getSystemService("location");
    try {
      int i = getCurrentUser();
      return locationManager.isLocationEnabledForUser(UserHandle.of(i));
    } catch (Exception exception) {
      Log.e("LocationPermissionChecker", "Failure to get location mode via API, falling back to settings", exception);
      return false;
    } 
  }
  
  private boolean isTargetSdkLessThan(String paramString, int paramInt1, int paramInt2) {
    long l = Binder.clearCallingIdentity();
    try {
      PackageManager packageManager = this.mContext.getPackageManager();
      UserHandle userHandle = UserHandle.getUserHandleForUid(paramInt2);
      paramInt2 = (packageManager.getApplicationInfoAsUser(paramString, 0, userHandle)).targetSdkVersion;
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
    
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
    Binder.restoreCallingIdentity(l);
    return false;
  }
  
  private boolean noteAppOpAllowed(String paramString1, String paramString2, String paramString3, int paramInt, String paramString4) {
    boolean bool;
    if (this.mAppOpsManager.noteOp(paramString1, paramInt, paramString2, paramString3, paramString4) == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void checkPackage(int paramInt, String paramString) throws SecurityException {
    if (paramString != null) {
      this.mAppOpsManager.checkPackage(paramInt, paramString);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Checking UID ");
    stringBuilder.append(paramInt);
    stringBuilder.append(" but Package Name is Null");
    throw new SecurityException(stringBuilder.toString());
  }
  
  protected int getCurrentUser() {
    return ActivityManager.getCurrentUser();
  }
  
  private int getUidPermission(String paramString, int paramInt) {
    return this.mContext.checkPermission(paramString, -1, paramInt);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface LocationPermissionCheckStatus {}
}
