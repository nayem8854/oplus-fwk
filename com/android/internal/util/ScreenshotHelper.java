package com.android.internal.util;

import android.common.OplusFeatureCache;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Insets;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.UserHandle;
import android.util.Log;
import com.oplus.screenshot.IOplusScreenShotEuclidManager;
import java.util.function.Consumer;

public class ScreenshotHelper {
  class ScreenshotRequest implements Parcelable {
    ScreenshotRequest(ScreenshotHelper this$0, boolean param1Boolean1, boolean param1Boolean2) {
      this.mSource = this$0;
      this.mHasStatusBar = param1Boolean1;
      this.mHasNavBar = param1Boolean2;
    }
    
    ScreenshotRequest(ScreenshotHelper this$0, Bundle param1Bundle, Rect param1Rect, Insets param1Insets, int param1Int1, int param1Int2, ComponentName param1ComponentName) {
      this.mSource = this$0;
      this.mBitmapBundle = param1Bundle;
      this.mBoundsInScreen = param1Rect;
      this.mInsets = param1Insets;
      this.mTaskId = param1Int1;
      this.mUserId = param1Int2;
      this.mTopComponent = param1ComponentName;
    }
    
    ScreenshotRequest(ScreenshotHelper this$0) {
      this.mSource = this$0.readInt();
      this.mHasStatusBar = this$0.readBoolean();
      this.mHasNavBar = this$0.readBoolean();
      if (this$0.readInt() == 1) {
        this.mBitmapBundle = this$0.readBundle(getClass().getClassLoader());
        this.mBoundsInScreen = (Rect)this$0.readParcelable(Rect.class.getClassLoader());
        this.mInsets = (Insets)this$0.readParcelable(Insets.class.getClassLoader());
        this.mTaskId = this$0.readInt();
        this.mUserId = this$0.readInt();
        this.mTopComponent = (ComponentName)this$0.readParcelable(ComponentName.class.getClassLoader());
      } 
    }
    
    public int getSource() {
      return this.mSource;
    }
    
    public boolean getHasStatusBar() {
      return this.mHasStatusBar;
    }
    
    public boolean getHasNavBar() {
      return this.mHasNavBar;
    }
    
    public Bundle getBitmapBundle() {
      return this.mBitmapBundle;
    }
    
    public Rect getBoundsInScreen() {
      return this.mBoundsInScreen;
    }
    
    public Insets getInsets() {
      return this.mInsets;
    }
    
    public int getTaskId() {
      return this.mTaskId;
    }
    
    public int getUserId() {
      return this.mUserId;
    }
    
    public ComponentName getTopComponent() {
      return this.mTopComponent;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.mSource);
      param1Parcel.writeBoolean(this.mHasStatusBar);
      param1Parcel.writeBoolean(this.mHasNavBar);
      if (this.mBitmapBundle == null) {
        param1Parcel.writeInt(0);
      } else {
        param1Parcel.writeInt(1);
        param1Parcel.writeBundle(this.mBitmapBundle);
        param1Parcel.writeParcelable((Parcelable)this.mBoundsInScreen, 0);
        param1Parcel.writeParcelable((Parcelable)this.mInsets, 0);
        param1Parcel.writeInt(this.mTaskId);
        param1Parcel.writeInt(this.mUserId);
        param1Parcel.writeParcelable((Parcelable)this.mTopComponent, 0);
      } 
    }
    
    public static final Parcelable.Creator<ScreenshotRequest> CREATOR = new Parcelable.Creator<ScreenshotRequest>() {
        public ScreenshotHelper.ScreenshotRequest createFromParcel(Parcel param2Parcel) {
          return new ScreenshotHelper.ScreenshotRequest(param2Parcel);
        }
        
        public ScreenshotHelper.ScreenshotRequest[] newArray(int param2Int) {
          return new ScreenshotHelper.ScreenshotRequest[param2Int];
        }
      };
    
    private Bundle mBitmapBundle;
    
    private Rect mBoundsInScreen;
    
    private boolean mHasNavBar;
    
    private boolean mHasStatusBar;
    
    private Insets mInsets;
    
    private int mSource;
    
    private int mTaskId;
    
    private ComponentName mTopComponent;
    
    private int mUserId;
  }
  
  private final int SCREENSHOT_TIMEOUT_MS = 10000;
  
  private final Object mScreenshotLock = new Object();
  
  private IBinder mScreenshotService = null;
  
  private ServiceConnection mScreenshotConnection = null;
  
  private final BroadcastReceiver mBroadcastReceiver = (BroadcastReceiver)new Object(this);
  
  public static final int SCREENSHOT_MSG_PROCESS_COMPLETE = 2;
  
  public static final int SCREENSHOT_MSG_URI = 1;
  
  private static final String TAG = "ScreenshotHelper";
  
  private final Context mContext;
  
  public ScreenshotHelper(Context paramContext) {
    this.mContext = paramContext;
    IntentFilter intentFilter = new IntentFilter("android.intent.action.USER_SWITCHED");
    this.mContext.registerReceiver(this.mBroadcastReceiver, intentFilter);
  }
  
  public void takeScreenshot(int paramInt1, boolean paramBoolean1, boolean paramBoolean2, int paramInt2, Handler paramHandler, Consumer<Uri> paramConsumer) {
    ScreenshotRequest screenshotRequest = new ScreenshotRequest(paramInt2, paramBoolean1, paramBoolean2);
    takeScreenshot(paramInt1, 10000L, paramHandler, screenshotRequest, paramConsumer);
  }
  
  public void takeScreenshot(int paramInt, boolean paramBoolean1, boolean paramBoolean2, Handler paramHandler, Consumer<Uri> paramConsumer) {
    takeScreenshot(paramInt, paramBoolean1, paramBoolean2, 10000, paramHandler, paramConsumer);
  }
  
  public void takeScreenshot(int paramInt, boolean paramBoolean1, boolean paramBoolean2, long paramLong, Handler paramHandler, Consumer<Uri> paramConsumer) {
    ScreenshotRequest screenshotRequest = new ScreenshotRequest(5, paramBoolean1, paramBoolean2);
    takeScreenshot(paramInt, paramLong, paramHandler, screenshotRequest, paramConsumer);
  }
  
  public void provideScreenshot(Bundle paramBundle, Rect paramRect, Insets paramInsets, int paramInt1, int paramInt2, ComponentName paramComponentName, int paramInt3, Handler paramHandler, Consumer<Uri> paramConsumer) {
    ScreenshotRequest screenshotRequest = new ScreenshotRequest(paramInt3, paramBundle, paramRect, paramInsets, paramInt1, paramInt2, paramComponentName);
    takeScreenshot(3, 10000L, paramHandler, screenshotRequest, paramConsumer);
  }
  
  private void takeScreenshot(int paramInt, long paramLong, Handler paramHandler, ScreenshotRequest paramScreenshotRequest, Consumer<Uri> paramConsumer) {
    synchronized (this.mScreenshotLock) {
      IOplusScreenShotEuclidManager iOplusScreenShotEuclidManager = (IOplusScreenShotEuclidManager)OplusFeatureCache.getOrCreate(IOplusScreenShotEuclidManager.DEFAULT, new Object[0]);
      Context context = this.mContext;
      if (iOplusScreenShotEuclidManager.takeScreenshot(context, paramInt, paramScreenshotRequest.getHasStatusBar(), paramScreenshotRequest.getHasNavBar(), paramHandler))
        return; 
      _$$Lambda$ScreenshotHelper$r9rp933Jt_vm9lU4BZ_gj9VY0YE _$$Lambda$ScreenshotHelper$r9rp933Jt_vm9lU4BZ_gj9VY0YE = new _$$Lambda$ScreenshotHelper$r9rp933Jt_vm9lU4BZ_gj9VY0YE();
      this(this, paramConsumer);
      Message message = Message.obtain(null, paramInt, paramScreenshotRequest);
      ServiceConnection serviceConnection = this.mScreenshotConnection;
      Object object = new Object();
      super(this, paramHandler.getLooper(), paramConsumer, paramHandler, _$$Lambda$ScreenshotHelper$r9rp933Jt_vm9lU4BZ_gj9VY0YE, serviceConnection);
      Messenger messenger = new Messenger();
      this((Handler)object);
      message.replyTo = messenger;
      if (this.mScreenshotConnection == null || this.mScreenshotService == null) {
        object = this.mContext;
        object = object.getResources().getString(17039960);
        ComponentName componentName = ComponentName.unflattenFromString((String)object);
        object = new Intent();
        super();
        object.setComponent(componentName);
        Object object1 = new Object();
        super(this, message, paramConsumer, paramHandler, _$$Lambda$ScreenshotHelper$r9rp933Jt_vm9lU4BZ_gj9VY0YE);
        if (this.mContext.bindServiceAsUser((Intent)object, (ServiceConnection)object1, 67108865, UserHandle.CURRENT)) {
          this.mScreenshotConnection = (ServiceConnection)object1;
          paramHandler.postDelayed(_$$Lambda$ScreenshotHelper$r9rp933Jt_vm9lU4BZ_gj9VY0YE, paramLong);
        } 
      } else {
        object = new Messenger();
        super(this.mScreenshotService);
        try {
          object.send(message);
        } catch (RemoteException remoteException) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Couldn't take screenshot: ");
          stringBuilder.append(remoteException);
          Log.e("ScreenshotHelper", stringBuilder.toString());
          if (paramConsumer != null)
            paramConsumer.accept(null); 
        } 
        paramHandler.postDelayed(_$$Lambda$ScreenshotHelper$r9rp933Jt_vm9lU4BZ_gj9VY0YE, paramLong);
      } 
      return;
    } 
  }
  
  private void resetConnection() {
    ServiceConnection serviceConnection = this.mScreenshotConnection;
    if (serviceConnection != null) {
      this.mContext.unbindService(serviceConnection);
      this.mScreenshotConnection = null;
      this.mScreenshotService = null;
    } 
  }
  
  private void notifyScreenshotError() {
    Context context = this.mContext;
    String str = context.getResources().getString(17039959);
    ComponentName componentName = ComponentName.unflattenFromString(str);
    Intent intent = new Intent("android.intent.action.USER_PRESENT");
    intent.setComponent(componentName);
    intent.addFlags(335544320);
    this.mContext.sendBroadcastAsUser(intent, UserHandle.CURRENT);
  }
}
