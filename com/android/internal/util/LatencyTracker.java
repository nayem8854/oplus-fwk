package com.android.internal.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Build;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.Trace;
import android.util.EventLog;
import android.util.Log;
import android.util.SparseLongArray;

public class LatencyTracker {
  public static final int ACTION_CHECK_CREDENTIAL = 3;
  
  public static final int ACTION_CHECK_CREDENTIAL_UNLOCKED = 4;
  
  public static final int ACTION_EXPAND_PANEL = 0;
  
  public static final int ACTION_FACE_WAKE_AND_UNLOCK = 6;
  
  public static final int ACTION_FINGERPRINT_WAKE_AND_UNLOCK = 2;
  
  private static final String ACTION_RELOAD_PROPERTY = "com.android.systemui.RELOAD_LATENCY_TRACKER_PROPERTY";
  
  public static final int ACTION_ROTATE_SCREEN = 6;
  
  public static final int ACTION_TOGGLE_RECENTS = 1;
  
  public static final int ACTION_TURN_ON_SCREEN = 5;
  
  private static final String[] NAMES = new String[] { "expand panel", "toggle recents", "fingerprint wake-and-unlock", "check credential", "check credential unlocked", "turn on screen", "rotate the screen", "face wake-and-unlock" };
  
  private static final String TAG = "LatencyTracker";
  
  private static LatencyTracker sLatencyTracker;
  
  private boolean mEnabled;
  
  private final SparseLongArray mStartRtc = new SparseLongArray();
  
  public static LatencyTracker getInstance(Context paramContext) {
    if (sLatencyTracker == null)
      sLatencyTracker = new LatencyTracker(paramContext.getApplicationContext()); 
    return sLatencyTracker;
  }
  
  private LatencyTracker(Context paramContext) {
    paramContext.registerReceiver((BroadcastReceiver)new Object(this), new IntentFilter("com.android.systemui.RELOAD_LATENCY_TRACKER_PROPERTY"));
    reloadProperty();
  }
  
  private void reloadProperty() {
    this.mEnabled = SystemProperties.getBoolean("debug.systemui.latency_tracking", false);
  }
  
  public static boolean isEnabled(Context paramContext) {
    return getInstance(paramContext).isEnabled();
  }
  
  public boolean isEnabled() {
    boolean bool;
    if (Build.IS_DEBUGGABLE && this.mEnabled) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void onActionStart(int paramInt) {
    if (!this.mEnabled)
      return; 
    Trace.asyncTraceBegin(4096L, NAMES[paramInt], 0);
    this.mStartRtc.put(paramInt, SystemClock.elapsedRealtime());
  }
  
  public void onActionEnd(int paramInt) {
    if (!this.mEnabled)
      return; 
    long l1 = SystemClock.elapsedRealtime();
    long l2 = this.mStartRtc.get(paramInt, -1L);
    if (l2 == -1L)
      return; 
    this.mStartRtc.delete(paramInt);
    Trace.asyncTraceEnd(4096L, NAMES[paramInt], 0);
    logAction(paramInt, (int)(l1 - l2));
  }
  
  public static void logAction(int paramInt1, int paramInt2) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("action=");
    stringBuilder.append(paramInt1);
    stringBuilder.append(" latency=");
    stringBuilder.append(paramInt2);
    Log.i("LatencyTracker", stringBuilder.toString());
    EventLog.writeEvent(36070, new Object[] { Integer.valueOf(paramInt1), Integer.valueOf(paramInt2) });
  }
}
