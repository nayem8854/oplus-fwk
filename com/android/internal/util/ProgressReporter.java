package com.android.internal.util;

import android.os.Bundle;
import android.os.IProgressListener;
import android.os.RemoteCallbackList;
import android.os.RemoteException;

public class ProgressReporter {
  private final RemoteCallbackList<IProgressListener> mListeners = new RemoteCallbackList();
  
  private int mState = 0;
  
  private int mProgress = 0;
  
  private Bundle mExtras = new Bundle();
  
  private int[] mSegmentRange = new int[] { 0, 100 };
  
  private static final int STATE_FINISHED = 2;
  
  private static final int STATE_INIT = 0;
  
  private static final int STATE_STARTED = 1;
  
  private final int mId;
  
  public ProgressReporter(int paramInt) {
    this.mId = paramInt;
  }
  
  public void addListener(IProgressListener paramIProgressListener) {
    // Byte code:
    //   0: aload_1
    //   1: ifnonnull -> 5
    //   4: return
    //   5: aload_0
    //   6: monitorenter
    //   7: aload_0
    //   8: getfield mListeners : Landroid/os/RemoteCallbackList;
    //   11: aload_1
    //   12: invokevirtual register : (Landroid/os/IInterface;)Z
    //   15: pop
    //   16: aload_0
    //   17: getfield mState : I
    //   20: istore_2
    //   21: iload_2
    //   22: iconst_1
    //   23: if_icmpeq -> 52
    //   26: iload_2
    //   27: iconst_2
    //   28: if_icmpeq -> 34
    //   31: goto -> 85
    //   34: aload_1
    //   35: aload_0
    //   36: getfield mId : I
    //   39: aconst_null
    //   40: invokeinterface onFinished : (ILandroid/os/Bundle;)V
    //   45: goto -> 85
    //   48: astore_1
    //   49: goto -> 85
    //   52: aload_1
    //   53: aload_0
    //   54: getfield mId : I
    //   57: aconst_null
    //   58: invokeinterface onStarted : (ILandroid/os/Bundle;)V
    //   63: aload_1
    //   64: aload_0
    //   65: getfield mId : I
    //   68: aload_0
    //   69: getfield mProgress : I
    //   72: aload_0
    //   73: getfield mExtras : Landroid/os/Bundle;
    //   76: invokeinterface onProgress : (IILandroid/os/Bundle;)V
    //   81: goto -> 85
    //   84: astore_1
    //   85: aload_0
    //   86: monitorexit
    //   87: return
    //   88: astore_1
    //   89: aload_0
    //   90: monitorexit
    //   91: aload_1
    //   92: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #89	-> 0
    //   #90	-> 5
    //   #91	-> 7
    //   #92	-> 16
    //   #105	-> 34
    //   #107	-> 45
    //   #106	-> 48
    //   #98	-> 52
    //   #99	-> 63
    //   #101	-> 81
    //   #100	-> 84
    //   #102	-> 85
    //   #110	-> 85
    //   #111	-> 87
    //   #110	-> 88
    // Exception table:
    //   from	to	target	type
    //   7	16	88	finally
    //   16	21	88	finally
    //   34	45	48	android/os/RemoteException
    //   34	45	88	finally
    //   52	63	84	android/os/RemoteException
    //   52	63	88	finally
    //   63	81	84	android/os/RemoteException
    //   63	81	88	finally
    //   85	87	88	finally
    //   89	91	88	finally
  }
  
  public void setProgress(int paramInt) {
    setProgress(paramInt, 100, null);
  }
  
  public void setProgress(int paramInt, CharSequence paramCharSequence) {
    setProgress(paramInt, 100, paramCharSequence);
  }
  
  public void setProgress(int paramInt1, int paramInt2) {
    setProgress(paramInt1, paramInt2, null);
  }
  
  public void setProgress(int paramInt1, int paramInt2, CharSequence paramCharSequence) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mState : I
    //   6: iconst_1
    //   7: if_icmpne -> 82
    //   10: aload_0
    //   11: getfield mSegmentRange : [I
    //   14: iconst_0
    //   15: iaload
    //   16: istore #4
    //   18: aload_0
    //   19: getfield mSegmentRange : [I
    //   22: iconst_1
    //   23: iaload
    //   24: iload_1
    //   25: imul
    //   26: iload_2
    //   27: idiv
    //   28: istore_1
    //   29: aload_0
    //   30: getfield mSegmentRange : [I
    //   33: iconst_1
    //   34: iaload
    //   35: istore_2
    //   36: aload_0
    //   37: iload #4
    //   39: iload_1
    //   40: iconst_0
    //   41: iload_2
    //   42: invokestatic constrain : (III)I
    //   45: iadd
    //   46: putfield mProgress : I
    //   49: aload_3
    //   50: ifnull -> 63
    //   53: aload_0
    //   54: getfield mExtras : Landroid/os/Bundle;
    //   57: ldc 'android.intent.extra.TITLE'
    //   59: aload_3
    //   60: invokevirtual putCharSequence : (Ljava/lang/String;Ljava/lang/CharSequence;)V
    //   63: aload_0
    //   64: aload_0
    //   65: getfield mId : I
    //   68: aload_0
    //   69: getfield mProgress : I
    //   72: aload_0
    //   73: getfield mExtras : Landroid/os/Bundle;
    //   76: invokespecial notifyProgress : (IILandroid/os/Bundle;)V
    //   79: aload_0
    //   80: monitorexit
    //   81: return
    //   82: new java/lang/IllegalStateException
    //   85: astore_3
    //   86: aload_3
    //   87: ldc 'Must be started to change progress'
    //   89: invokespecial <init> : (Ljava/lang/String;)V
    //   92: aload_3
    //   93: athrow
    //   94: astore_3
    //   95: aload_0
    //   96: monitorexit
    //   97: aload_3
    //   98: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #142	-> 0
    //   #143	-> 2
    //   #146	-> 10
    //   #147	-> 36
    //   #148	-> 49
    //   #149	-> 53
    //   #151	-> 63
    //   #152	-> 79
    //   #153	-> 81
    //   #144	-> 82
    //   #152	-> 94
    // Exception table:
    //   from	to	target	type
    //   2	10	94	finally
    //   10	36	94	finally
    //   36	49	94	finally
    //   53	63	94	finally
    //   63	79	94	finally
    //   79	81	94	finally
    //   82	94	94	finally
    //   95	97	94	finally
  }
  
  public int[] startSegment(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mSegmentRange : [I
    //   6: astore_2
    //   7: aload_0
    //   8: iconst_2
    //   9: newarray int
    //   11: dup
    //   12: iconst_0
    //   13: aload_0
    //   14: getfield mProgress : I
    //   17: iastore
    //   18: dup
    //   19: iconst_1
    //   20: aload_0
    //   21: getfield mSegmentRange : [I
    //   24: iconst_1
    //   25: iaload
    //   26: iload_1
    //   27: imul
    //   28: bipush #100
    //   30: idiv
    //   31: iastore
    //   32: putfield mSegmentRange : [I
    //   35: aload_0
    //   36: monitorexit
    //   37: aload_2
    //   38: areturn
    //   39: astore_2
    //   40: aload_0
    //   41: monitorexit
    //   42: aload_2
    //   43: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #161	-> 0
    //   #162	-> 2
    //   #163	-> 7
    //   #164	-> 35
    //   #165	-> 39
    // Exception table:
    //   from	to	target	type
    //   2	7	39	finally
    //   7	35	39	finally
    //   35	37	39	finally
    //   40	42	39	finally
  }
  
  public void endSegment(int[] paramArrayOfint) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_0
    //   4: getfield mSegmentRange : [I
    //   7: iconst_0
    //   8: iaload
    //   9: aload_0
    //   10: getfield mSegmentRange : [I
    //   13: iconst_1
    //   14: iaload
    //   15: iadd
    //   16: putfield mProgress : I
    //   19: aload_0
    //   20: aload_1
    //   21: putfield mSegmentRange : [I
    //   24: aload_0
    //   25: monitorexit
    //   26: return
    //   27: astore_1
    //   28: aload_0
    //   29: monitorexit
    //   30: aload_1
    //   31: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #172	-> 0
    //   #173	-> 2
    //   #174	-> 19
    //   #175	-> 24
    //   #176	-> 26
    //   #175	-> 27
    // Exception table:
    //   from	to	target	type
    //   2	19	27	finally
    //   19	24	27	finally
    //   24	26	27	finally
    //   28	30	27	finally
  }
  
  int getProgress() {
    return this.mProgress;
  }
  
  int[] getSegmentRange() {
    return this.mSegmentRange;
  }
  
  public void start() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iconst_1
    //   4: putfield mState : I
    //   7: aload_0
    //   8: aload_0
    //   9: getfield mId : I
    //   12: aconst_null
    //   13: invokespecial notifyStarted : (ILandroid/os/Bundle;)V
    //   16: aload_0
    //   17: aload_0
    //   18: getfield mId : I
    //   21: aload_0
    //   22: getfield mProgress : I
    //   25: aload_0
    //   26: getfield mExtras : Landroid/os/Bundle;
    //   29: invokespecial notifyProgress : (IILandroid/os/Bundle;)V
    //   32: aload_0
    //   33: monitorexit
    //   34: return
    //   35: astore_1
    //   36: aload_0
    //   37: monitorexit
    //   38: aload_1
    //   39: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #190	-> 0
    //   #191	-> 2
    //   #192	-> 7
    //   #193	-> 16
    //   #194	-> 32
    //   #195	-> 34
    //   #194	-> 35
    // Exception table:
    //   from	to	target	type
    //   2	7	35	finally
    //   7	16	35	finally
    //   16	32	35	finally
    //   32	34	35	finally
    //   36	38	35	finally
  }
  
  public void finish() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iconst_2
    //   4: putfield mState : I
    //   7: aload_0
    //   8: aload_0
    //   9: getfield mId : I
    //   12: aconst_null
    //   13: invokespecial notifyFinished : (ILandroid/os/Bundle;)V
    //   16: aload_0
    //   17: getfield mListeners : Landroid/os/RemoteCallbackList;
    //   20: invokevirtual kill : ()V
    //   23: aload_0
    //   24: monitorexit
    //   25: return
    //   26: astore_1
    //   27: aload_0
    //   28: monitorexit
    //   29: aload_1
    //   30: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #201	-> 0
    //   #202	-> 2
    //   #203	-> 7
    //   #204	-> 16
    //   #205	-> 23
    //   #206	-> 25
    //   #205	-> 26
    // Exception table:
    //   from	to	target	type
    //   2	7	26	finally
    //   7	16	26	finally
    //   16	23	26	finally
    //   23	25	26	finally
    //   27	29	26	finally
  }
  
  private void notifyStarted(int paramInt, Bundle paramBundle) {
    for (int i = this.mListeners.beginBroadcast() - 1; i >= 0; i--) {
      try {
        ((IProgressListener)this.mListeners.getBroadcastItem(i)).onStarted(paramInt, paramBundle);
      } catch (RemoteException remoteException) {}
    } 
    this.mListeners.finishBroadcast();
  }
  
  private void notifyProgress(int paramInt1, int paramInt2, Bundle paramBundle) {
    for (int i = this.mListeners.beginBroadcast() - 1; i >= 0; i--) {
      try {
        ((IProgressListener)this.mListeners.getBroadcastItem(i)).onProgress(paramInt1, paramInt2, paramBundle);
      } catch (RemoteException remoteException) {}
    } 
    this.mListeners.finishBroadcast();
  }
  
  private void notifyFinished(int paramInt, Bundle paramBundle) {
    for (int i = this.mListeners.beginBroadcast() - 1; i >= 0; i--) {
      try {
        ((IProgressListener)this.mListeners.getBroadcastItem(i)).onFinished(paramInt, paramBundle);
      } catch (RemoteException remoteException) {}
    } 
    this.mListeners.finishBroadcast();
  }
}
