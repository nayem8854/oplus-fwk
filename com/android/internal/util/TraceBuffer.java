package com.android.internal.util;

import android.util.proto.ProtoOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Queue;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class TraceBuffer<P, S extends P, T extends P> {
  private final ProtoProvider<P, S, T> mProtoProvider;
  
  private final Consumer mProtoDequeuedCallback;
  
  private int mBufferUsedSize;
  
  private final Object mBufferLock = new Object();
  
  private int mBufferCapacity;
  
  private final Queue<T> mBuffer = new ArrayDeque<>();
  
  class ProtoOutputStreamProvider implements ProtoProvider<ProtoOutputStream, ProtoOutputStream, ProtoOutputStream> {
    private ProtoOutputStreamProvider() {}
    
    public int getItemSize(ProtoOutputStream param1ProtoOutputStream) {
      return param1ProtoOutputStream.getRawSize();
    }
    
    public byte[] getBytes(ProtoOutputStream param1ProtoOutputStream) {
      return param1ProtoOutputStream.getBytes();
    }
    
    public void write(ProtoOutputStream param1ProtoOutputStream, Queue<ProtoOutputStream> param1Queue, OutputStream param1OutputStream) throws IOException {
      param1OutputStream.write(param1ProtoOutputStream.getBytes());
      for (ProtoOutputStream protoOutputStream : param1Queue) {
        byte[] arrayOfByte = protoOutputStream.getBytes();
        param1OutputStream.write(arrayOfByte);
      } 
    }
  }
  
  public TraceBuffer(int paramInt) {
    this(paramInt, new ProtoOutputStreamProvider(null), null);
  }
  
  public TraceBuffer(int paramInt, ProtoProvider<P, S, T> paramProtoProvider, Consumer<T> paramConsumer) {
    this.mBufferCapacity = paramInt;
    this.mProtoProvider = paramProtoProvider;
    this.mProtoDequeuedCallback = paramConsumer;
    resetBuffer();
  }
  
  public int getAvailableSpace() {
    return this.mBufferCapacity - this.mBufferUsedSize;
  }
  
  public int size() {
    return this.mBuffer.size();
  }
  
  public void setCapacity(int paramInt) {
    this.mBufferCapacity = paramInt;
  }
  
  public void add(T paramT) {
    int i = this.mProtoProvider.getItemSize((P)paramT);
    if (i <= this.mBufferCapacity)
      synchronized (this.mBufferLock) {
        discardOldest(i);
        this.mBuffer.add(paramT);
        this.mBufferUsedSize += i;
        this.mBufferLock.notify();
        return;
      }  
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Trace object too large for the buffer. Buffer size:");
    stringBuilder.append(this.mBufferCapacity);
    stringBuilder.append(" Object size: ");
    stringBuilder.append(i);
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public boolean contains(byte[] paramArrayOfbyte) {
    Stream<T> stream = this.mBuffer.stream();
    _$$Lambda$TraceBuffer$BDCkdpEKAlh5YuuB_2MOTxgQ3w4 _$$Lambda$TraceBuffer$BDCkdpEKAlh5YuuB_2MOTxgQ3w4 = new _$$Lambda$TraceBuffer$BDCkdpEKAlh5YuuB_2MOTxgQ3w4(this, paramArrayOfbyte);
    return 
      stream.anyMatch(_$$Lambda$TraceBuffer$BDCkdpEKAlh5YuuB_2MOTxgQ3w4);
  }
  
  public void writeTraceToFile(File paramFile, S paramS) throws IOException {
    synchronized (this.mBufferLock) {
      paramFile.delete();
      FileOutputStream fileOutputStream = new FileOutputStream();
      this(paramFile);
      try {
        paramFile.setReadable(true, false);
        this.mProtoProvider.write(paramS, this.mBuffer, fileOutputStream);
        fileOutputStream.flush();
        return;
      } finally {
        try {
          fileOutputStream.close();
        } finally {
          paramS = null;
        } 
      } 
    } 
  }
  
  private void discardOldest(int paramInt) {
    long l = getAvailableSpace();
    while (l < paramInt) {
      T t = this.mBuffer.poll();
      if (t != null) {
        this.mBufferUsedSize -= this.mProtoProvider.getItemSize((P)t);
        l = getAvailableSpace();
        Consumer<T> consumer = this.mProtoDequeuedCallback;
        if (consumer != null)
          consumer.accept(t); 
        continue;
      } 
      throw new IllegalStateException("No element to discard from buffer");
    } 
  }
  
  public void resetBuffer() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mBufferLock : Ljava/lang/Object;
    //   4: astore_1
    //   5: aload_1
    //   6: monitorenter
    //   7: aload_0
    //   8: getfield mProtoDequeuedCallback : Ljava/util/function/Consumer;
    //   11: ifnull -> 53
    //   14: aload_0
    //   15: getfield mBuffer : Ljava/util/Queue;
    //   18: invokeinterface iterator : ()Ljava/util/Iterator;
    //   23: astore_2
    //   24: aload_2
    //   25: invokeinterface hasNext : ()Z
    //   30: ifeq -> 53
    //   33: aload_2
    //   34: invokeinterface next : ()Ljava/lang/Object;
    //   39: astore_3
    //   40: aload_0
    //   41: getfield mProtoDequeuedCallback : Ljava/util/function/Consumer;
    //   44: aload_3
    //   45: invokeinterface accept : (Ljava/lang/Object;)V
    //   50: goto -> 24
    //   53: aload_0
    //   54: getfield mBuffer : Ljava/util/Queue;
    //   57: invokeinterface clear : ()V
    //   62: aload_0
    //   63: iconst_0
    //   64: putfield mBufferUsedSize : I
    //   67: aload_1
    //   68: monitorexit
    //   69: return
    //   70: astore_2
    //   71: aload_1
    //   72: monitorexit
    //   73: aload_2
    //   74: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #199	-> 0
    //   #200	-> 7
    //   #201	-> 14
    //   #202	-> 40
    //   #203	-> 50
    //   #205	-> 53
    //   #206	-> 62
    //   #207	-> 67
    //   #208	-> 69
    //   #207	-> 70
    // Exception table:
    //   from	to	target	type
    //   7	14	70	finally
    //   14	24	70	finally
    //   24	40	70	finally
    //   40	50	70	finally
    //   53	62	70	finally
    //   62	67	70	finally
    //   67	69	70	finally
    //   71	73	70	finally
  }
  
  public int getBufferSize() {
    return this.mBufferUsedSize;
  }
  
  public String getStatus() {
    synchronized (this.mBufferLock) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Buffer size: ");
      stringBuilder.append(this.mBufferCapacity);
      stringBuilder.append(" bytes\nBuffer usage: ");
      stringBuilder.append(this.mBufferUsedSize);
      stringBuilder.append(" bytes\nElements in the buffer: ");
      Queue<T> queue = this.mBuffer;
      stringBuilder.append(queue.size());
      return stringBuilder.toString();
    } 
  }
  
  public static interface ProtoProvider<P, S extends P, T extends P> {
    byte[] getBytes(P param1P);
    
    int getItemSize(P param1P);
    
    void write(S param1S, Queue<T> param1Queue, OutputStream param1OutputStream) throws IOException;
  }
}
