package com.android.internal.util;

import android.content.ContentResolver;
import android.content.res.Resources;
import android.graphics.drawable.Icon;
import android.text.TextUtils;
import android.util.ArrayMap;
import java.util.Locale;
import java.util.Objects;
import libcore.content.type.MimeMap;

public class MimeIconUtils {
  private static final ArrayMap<String, ContentResolver.MimeTypeInfo> sCache = new ArrayMap();
  
  private static ContentResolver.MimeTypeInfo buildTypeInfo(String paramString, int paramInt1, int paramInt2, int paramInt3) {
    Resources resources = Resources.getSystem();
    paramString = MimeMap.getDefault().guessExtensionFromMimeType(paramString);
    if (!TextUtils.isEmpty(paramString) && paramInt3 != -1) {
      paramString = resources.getString(paramInt3, new Object[] { paramString.toUpperCase(Locale.US) });
    } else {
      paramString = resources.getString(paramInt2);
    } 
    return new ContentResolver.MimeTypeInfo(Icon.createWithResource(resources, paramInt1), paramString, paramString);
  }
  
  private static ContentResolver.MimeTypeInfo buildTypeInfo(String paramString) {
    byte b;
    switch (paramString.hashCode()) {
      default:
        b = -1;
        break;
      case 2132236175:
        if (paramString.equals("text/javascript")) {
          b = 39;
          break;
        } 
      case 2062095256:
        if (paramString.equals("text/x-c++src")) {
          b = 21;
          break;
        } 
      case 2062084266:
        if (paramString.equals("text/x-c++hdr")) {
          b = 20;
          break;
        } 
      case 2041423923:
        if (paramString.equals("application/x-x509-user-cert")) {
          b = 9;
          break;
        } 
      case 1993842850:
        if (paramString.equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
          b = 106;
          break;
        } 
      case 1969663169:
        if (paramString.equals("application/rdf+xml")) {
          b = 13;
          break;
        } 
      case 1948418893:
        if (paramString.equals("application/mac-binhex40")) {
          b = 41;
          break;
        } 
      case 1868769095:
        if (paramString.equals("application/x-quicktimeplayer")) {
          b = 100;
          break;
        } 
      case 1851895234:
        if (paramString.equals("application/x-webarchive")) {
          b = 53;
          break;
        } 
      case 1709755138:
        if (paramString.equals("application/x-font-woff")) {
          b = 65;
          break;
        } 
      case 1673742401:
        if (paramString.equals("application/vnd.stardivision.writer")) {
          b = 92;
          break;
        } 
      case 1643664935:
        if (paramString.equals("application/vnd.oasis.opendocument.spreadsheet")) {
          b = 81;
          break;
        } 
      case 1637222218:
        if (paramString.equals("application/x-kspread")) {
          b = 86;
          break;
        } 
      case 1577426419:
        if (paramString.equals("application/vnd.openxmlformats-officedocument.presentationml.slideshow")) {
          b = 111;
          break;
        } 
      case 1573656544:
        if (paramString.equals("application/x-pkcs12")) {
          b = 5;
          break;
        } 
      case 1536912403:
        if (paramString.equals("application/x-object")) {
          b = 15;
          break;
        } 
      case 1502452311:
        if (paramString.equals("application/font-woff")) {
          b = 64;
          break;
        } 
      case 1461751133:
        if (paramString.equals("application/vnd.oasis.opendocument.text-master")) {
          b = 89;
          break;
        } 
      case 1454024983:
        if (paramString.equals("application/x-7z-compressed")) {
          b = 56;
          break;
        } 
      case 1440428940:
        if (paramString.equals("application/javascript")) {
          b = 37;
          break;
        } 
      case 1436962847:
        if (paramString.equals("application/vnd.oasis.opendocument.presentation")) {
          b = 79;
          break;
        } 
      case 1432260414:
        if (paramString.equals("application/x-latex")) {
          b = 32;
          break;
        } 
      case 1431987873:
        if (paramString.equals("application/x-kword")) {
          b = 98;
          break;
        } 
      case 1383977381:
        if (paramString.equals("application/vnd.sun.xml.impress")) {
          b = 76;
          break;
        } 
      case 1377360791:
        if (paramString.equals("application/vnd.oasis.opendocument.graphics-template")) {
          b = 68;
          break;
        } 
      case 1305955842:
        if (paramString.equals("application/x-debian-package")) {
          b = 45;
          break;
        } 
      case 1283455191:
        if (paramString.equals("application/x-apple-diskimage")) {
          b = 44;
          break;
        } 
      case 1255211837:
        if (paramString.equals("text/x-haskell")) {
          b = 26;
          break;
        } 
      case 1239557416:
        if (paramString.equals("application/x-pkcs7-crl")) {
          b = 7;
          break;
        } 
      case 1154449330:
        if (paramString.equals("application/x-gtar")) {
          b = 46;
          break;
        } 
      case 1154415139:
        if (paramString.equals("application/x-font")) {
          b = 63;
          break;
        } 
      case 1060806194:
        if (paramString.equals("application/vnd.openxmlformats-officedocument.wordprocessingml.template")) {
          b = 104;
          break;
        } 
      case 1043583697:
        if (paramString.equals("application/x-pkcs7-certificates")) {
          b = 10;
          break;
        } 
      case 904647503:
        if (paramString.equals("application/msword")) {
          b = 102;
          break;
        } 
      case 859118878:
        if (paramString.equals("application/x-abiword")) {
          b = 97;
          break;
        } 
      case 822865392:
        if (paramString.equals("text/x-tex")) {
          b = 31;
          break;
        } 
      case 822865318:
        if (paramString.equals("text/x-tcl")) {
          b = 30;
          break;
        } 
      case 822849473:
        if (paramString.equals("text/x-csh")) {
          b = 25;
          break;
        } 
      case 822609188:
        if (paramString.equals("text/vcard")) {
          b = 60;
          break;
        } 
      case 717553764:
        if (paramString.equals("application/vnd.google-apps.document")) {
          b = 99;
          break;
        } 
      case 694663701:
        if (paramString.equals("application/vnd.openxmlformats-officedocument.presentationml.template")) {
          b = 110;
          break;
        } 
      case 669516689:
        if (paramString.equals("application/vnd.stardivision.impress")) {
          b = 75;
          break;
        } 
      case 641141505:
        if (paramString.equals("application/x-texinfo")) {
          b = 33;
          break;
        } 
      case 603849904:
        if (paramString.equals("application/xhtml+xml")) {
          b = 16;
          break;
        } 
      case 571050671:
        if (paramString.equals("application/vnd.stardivision.writer-global")) {
          b = 93;
          break;
        } 
      case 501428239:
        if (paramString.equals("text/x-vcard")) {
          b = 59;
          break;
        } 
      case 428819984:
        if (paramString.equals("application/vnd.oasis.opendocument.graphics")) {
          b = 67;
          break;
        } 
      case 394650567:
        if (paramString.equals("application/pgp-keys")) {
          b = 3;
          break;
        } 
      case 363965503:
        if (paramString.equals("application/x-rar-compressed")) {
          b = 58;
          break;
        } 
      case 302663708:
        if (paramString.equals("application/ecmascript")) {
          b = 35;
          break;
        } 
      case 302189274:
        if (paramString.equals("vnd.android.document/directory")) {
          b = 1;
          break;
        } 
      case 262346941:
        if (paramString.equals("text/x-vcalendar")) {
          b = 62;
          break;
        } 
      case 245790645:
        if (paramString.equals("application/vnd.google-apps.drawing")) {
          b = 73;
          break;
        } 
      case 180207563:
        if (paramString.equals("application/x-stuffit")) {
          b = 51;
          break;
        } 
      case 163679941:
        if (paramString.equals("application/pgp-signature")) {
          b = 4;
          break;
        } 
      case 81142075:
        if (paramString.equals("application/vnd.android.package-archive")) {
          b = 2;
          break;
        } 
      case 26919318:
        if (paramString.equals("application/x-iso9660-image")) {
          b = 47;
          break;
        } 
      case -43840953:
        if (paramString.equals("application/json")) {
          b = 36;
          break;
        } 
      case -43923783:
        if (paramString.equals("application/gzip")) {
          b = 55;
          break;
        } 
      case -109382304:
        if (paramString.equals("application/vnd.oasis.opendocument.spreadsheet-template")) {
          b = 82;
          break;
        } 
      case -221944004:
        if (paramString.equals("application/x-font-ttf")) {
          b = 66;
          break;
        } 
      case -228136375:
        if (paramString.equals("application/x-pkcs7-mime")) {
          b = 11;
          break;
        } 
      case -261278343:
        if (paramString.equals("text/x-java")) {
          b = 27;
          break;
        } 
      case -261439913:
        if (paramString.equals("text/x-dsrc")) {
          b = 24;
          break;
        } 
      case -261469704:
        if (paramString.equals("text/x-csrc")) {
          b = 23;
          break;
        } 
      case -261480694:
        if (paramString.equals("text/x-chdr")) {
          b = 22;
          break;
        } 
      case -366307023:
        if (paramString.equals("application/vnd.ms-excel")) {
          b = 105;
          break;
        } 
      case -396757341:
        if (paramString.equals("application/vnd.sun.xml.impress.template")) {
          b = 77;
          break;
        } 
      case -427343476:
        if (paramString.equals("application/x-webarchive-xml")) {
          b = 54;
          break;
        } 
      case -479218428:
        if (paramString.equals("application/vnd.sun.xml.writer.global")) {
          b = 95;
          break;
        } 
      case -676675015:
        if (paramString.equals("application/vnd.oasis.opendocument.text-web")) {
          b = 91;
          break;
        } 
      case -723118015:
        if (paramString.equals("application/x-javascript")) {
          b = 40;
          break;
        } 
      case -779913474:
        if (paramString.equals("application/vnd.sun.xml.draw")) {
          b = 71;
          break;
        } 
      case -779959281:
        if (paramString.equals("application/vnd.sun.xml.calc")) {
          b = 84;
          break;
        } 
      case -951557661:
        if (paramString.equals("application/vnd.google-apps.presentation")) {
          b = 80;
          break;
        } 
      case -958424608:
        if (paramString.equals("text/calendar")) {
          b = 61;
          break;
        } 
      case -1004727243:
        if (paramString.equals("text/xml")) {
          b = 19;
          break;
        } 
      case -1004747231:
        if (paramString.equals("text/css")) {
          b = 17;
          break;
        } 
      case -1033484950:
        if (paramString.equals("application/vnd.sun.xml.draw.template")) {
          b = 72;
          break;
        } 
      case -1050893613:
        if (paramString.equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")) {
          b = 103;
          break;
        } 
      case -1071817359:
        if (paramString.equals("application/vnd.ms-powerpoint")) {
          b = 108;
          break;
        } 
      case -1073633483:
        if (paramString.equals("application/vnd.openxmlformats-officedocument.presentationml.presentation")) {
          b = 109;
          break;
        } 
      case -1082243251:
        if (paramString.equals("text/html")) {
          b = 18;
          break;
        } 
      case -1143717099:
        if (paramString.equals("application/x-pkcs7-certreqresp")) {
          b = 6;
          break;
        } 
      case -1190438973:
        if (paramString.equals("application/x-pkcs7-signature")) {
          b = 12;
          break;
        } 
      case -1248325150:
        if (paramString.equals("application/zip")) {
          b = 43;
          break;
        } 
      case -1248326952:
        if (paramString.equals("application/xml")) {
          b = 38;
          break;
        } 
      case -1248333084:
        if (paramString.equals("application/rar")) {
          b = 42;
          break;
        } 
      case -1248334925:
        if (paramString.equals("application/pdf")) {
          b = 74;
          break;
        } 
      case -1294595255:
        if (paramString.equals("inode/directory")) {
          b = 0;
          break;
        } 
      case -1296467268:
        if (paramString.equals("application/atom+xml")) {
          b = 34;
          break;
        } 
      case -1316922187:
        if (paramString.equals("application/vnd.oasis.opendocument.text-template")) {
          b = 90;
          break;
        } 
      case -1326989846:
        if (paramString.equals("application/x-shockwave-flash")) {
          b = 101;
          break;
        } 
      case -1348221103:
        if (paramString.equals("application/x-tar")) {
          b = 52;
          break;
        } 
      case -1348228010:
        if (paramString.equals("application/x-lzx")) {
          b = 50;
          break;
        } 
      case -1348228026:
        if (paramString.equals("application/x-lzh")) {
          b = 49;
          break;
        } 
      case -1348228591:
        if (paramString.equals("application/x-lha")) {
          b = 48;
          break;
        } 
      case -1348236371:
        if (paramString.equals("application/x-deb")) {
          b = 57;
          break;
        } 
      case -1386165903:
        if (paramString.equals("application/x-kpresenter")) {
          b = 78;
          break;
        } 
      case -1506009513:
        if (paramString.equals("application/vnd.openxmlformats-officedocument.spreadsheetml.template")) {
          b = 107;
          break;
        } 
      case -1590813831:
        if (paramString.equals("application/vnd.sun.xml.calc.template")) {
          b = 85;
          break;
        } 
      case -1628346451:
        if (paramString.equals("application/vnd.sun.xml.writer")) {
          b = 94;
          break;
        } 
      case -1719571662:
        if (paramString.equals("application/vnd.oasis.opendocument.text")) {
          b = 88;
          break;
        } 
      case -1747277413:
        if (paramString.equals("application/vnd.sun.xml.writer.template")) {
          b = 96;
          break;
        } 
      case -1777056778:
        if (paramString.equals("application/vnd.oasis.opendocument.image")) {
          b = 69;
          break;
        } 
      case -1808693885:
        if (paramString.equals("text/x-pascal")) {
          b = 29;
          break;
        } 
      case -1883861089:
        if (paramString.equals("application/rss+xml")) {
          b = 14;
          break;
        } 
      case -1917350260:
        if (paramString.equals("text/x-literate-haskell")) {
          b = 28;
          break;
        } 
      case -1988437312:
        if (paramString.equals("application/x-x509-ca-cert")) {
          b = 8;
          break;
        } 
      case -2035614749:
        if (paramString.equals("application/vnd.google-apps.spreadsheet")) {
          b = 87;
          break;
        } 
      case -2135135086:
        if (paramString.equals("application/vnd.stardivision.draw")) {
          b = 70;
          break;
        } 
      case -2135180893:
        if (paramString.equals("application/vnd.stardivision.calc")) {
          b = 83;
          break;
        } 
    } 
    switch (b) {
      default:
        return buildGenericTypeInfo(paramString);
      case 108:
      case 109:
      case 110:
      case 111:
        return buildTypeInfo(paramString, 17302416, 17040648, 17040649);
      case 105:
      case 106:
      case 107:
        return buildTypeInfo(paramString, 17302410, 17040650, 17040651);
      case 102:
      case 103:
      case 104:
        return buildTypeInfo(paramString, 17302421, 17040641, 17040642);
      case 100:
      case 101:
        return buildTypeInfo(paramString, 17302420, 17040652, 17040653);
      case 88:
      case 89:
      case 90:
      case 91:
      case 92:
      case 93:
      case 94:
      case 95:
      case 96:
      case 97:
      case 98:
      case 99:
        return buildTypeInfo(paramString, 17302408, 17040641, 17040642);
      case 81:
      case 82:
      case 83:
      case 84:
      case 85:
      case 86:
      case 87:
        return buildTypeInfo(paramString, 17302418, 17040650, 17040651);
      case 75:
      case 76:
      case 77:
      case 78:
      case 79:
      case 80:
        return buildTypeInfo(paramString, 17302417, 17040648, 17040649);
      case 74:
        return buildTypeInfo(paramString, 17302415, 17040641, 17040642);
      case 67:
      case 68:
      case 69:
      case 70:
      case 71:
      case 72:
      case 73:
        return buildTypeInfo(paramString, 17302414, 17040646, 17040647);
      case 63:
      case 64:
      case 65:
      case 66:
        return buildTypeInfo(paramString, 17302412, 17040644, 17040645);
      case 61:
      case 62:
        return buildTypeInfo(paramString, 17302409, 17040644, 17040645);
      case 59:
      case 60:
        return buildTypeInfo(paramString, 17302407, 17040644, 17040645);
      case 41:
      case 42:
      case 43:
      case 44:
      case 45:
      case 46:
      case 47:
      case 48:
      case 49:
      case 50:
      case 51:
      case 52:
      case 53:
      case 54:
      case 55:
      case 56:
      case 57:
      case 58:
        return buildTypeInfo(paramString, 17302406, 17040639, 17040640);
      case 13:
      case 14:
      case 15:
      case 16:
      case 17:
      case 18:
      case 19:
      case 20:
      case 21:
      case 22:
      case 23:
      case 24:
      case 25:
      case 26:
      case 27:
      case 28:
      case 29:
      case 30:
      case 31:
      case 32:
      case 33:
      case 34:
      case 35:
      case 36:
      case 37:
      case 38:
      case 39:
      case 40:
        return buildTypeInfo(paramString, 17302405, 17040641, 17040642);
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 10:
      case 11:
      case 12:
        return buildTypeInfo(paramString, 17302404, 17040644, 17040645);
      case 2:
        return buildTypeInfo(paramString, 17302402, 17040636, -1);
      case 0:
      case 1:
        break;
    } 
    return buildTypeInfo(paramString, 17302411, 17040643, -1);
  }
  
  private static ContentResolver.MimeTypeInfo buildGenericTypeInfo(String paramString) {
    if (paramString.startsWith("audio/"))
      return buildTypeInfo(paramString, 17302403, 17040637, 17040638); 
    if (paramString.startsWith("video/"))
      return buildTypeInfo(paramString, 17302420, 17040652, 17040653); 
    if (paramString.startsWith("image/"))
      return buildTypeInfo(paramString, 17302414, 17040646, 17040647); 
    if (paramString.startsWith("text/"))
      return buildTypeInfo(paramString, 17302419, 17040641, 17040642); 
    MimeMap mimeMap = MimeMap.getDefault();
    String str = mimeMap.guessMimeTypeFromExtension(mimeMap.guessExtensionFromMimeType(paramString));
    if (str != null && !Objects.equals(paramString, str))
      return buildTypeInfo(str); 
    return buildTypeInfo(paramString, 17302413, 17040644, 17040645);
  }
  
  public static ContentResolver.MimeTypeInfo getTypeInfo(String paramString) {
    String str = paramString.toLowerCase(Locale.US);
    synchronized (sCache) {
      ContentResolver.MimeTypeInfo mimeTypeInfo2 = (ContentResolver.MimeTypeInfo)sCache.get(str);
      ContentResolver.MimeTypeInfo mimeTypeInfo1 = mimeTypeInfo2;
      if (mimeTypeInfo2 == null) {
        mimeTypeInfo1 = buildTypeInfo(str);
        sCache.put(str, mimeTypeInfo1);
      } 
      return mimeTypeInfo1;
    } 
  }
}
