package com.android.internal.util;

import android.os.WorkSource;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.util.SparseLongArray;
import android.util.StatsEvent;
import android.util.StatsLog;
import java.util.List;

public class FrameworkStatsLog {
  public static final int ACCESSIBILITY_SHORTCUT_REPORTED = 266;
  
  public static final int ACCESSIBILITY_SHORTCUT_REPORTED__SERVICE_STATUS__DISABLED = 2;
  
  public static final int ACCESSIBILITY_SHORTCUT_REPORTED__SERVICE_STATUS__ENABLED = 1;
  
  public static final int ACCESSIBILITY_SHORTCUT_REPORTED__SERVICE_STATUS__UNKNOWN = 0;
  
  public static final int ACCESSIBILITY_SHORTCUT_REPORTED__SHORTCUT_TYPE__A11Y_BUTTON = 1;
  
  public static final int ACCESSIBILITY_SHORTCUT_REPORTED__SHORTCUT_TYPE__A11Y_BUTTON_LONG_PRESS = 4;
  
  public static final int ACCESSIBILITY_SHORTCUT_REPORTED__SHORTCUT_TYPE__TRIPLE_TAP = 3;
  
  public static final int ACCESSIBILITY_SHORTCUT_REPORTED__SHORTCUT_TYPE__UNKNOWN_TYPE = 0;
  
  public static final int ACCESSIBILITY_SHORTCUT_REPORTED__SHORTCUT_TYPE__VOLUME_KEY = 2;
  
  public static final int ACTIVITY_FOREGROUND_STATE_CHANGED = 42;
  
  public static final int ACTIVITY_FOREGROUND_STATE_CHANGED__STATE__BACKGROUND = 0;
  
  public static final int ACTIVITY_FOREGROUND_STATE_CHANGED__STATE__FOREGROUND = 1;
  
  public static final int ACTIVITY_MANAGER_SLEEP_STATE_CHANGED = 14;
  
  public static final int ACTIVITY_MANAGER_SLEEP_STATE_CHANGED__STATE__ASLEEP = 1;
  
  public static final int ACTIVITY_MANAGER_SLEEP_STATE_CHANGED__STATE__AWAKE = 2;
  
  public static final int ACTIVITY_MANAGER_SLEEP_STATE_CHANGED__STATE__UNKNOWN = 0;
  
  public static final int ADB_CONNECTION_CHANGED = 144;
  
  public static final int ADB_CONNECTION_CHANGED__STATE__AUTOMATICALLY_ALLOWED = 4;
  
  public static final int ADB_CONNECTION_CHANGED__STATE__AWAITING_USER_APPROVAL = 1;
  
  public static final int ADB_CONNECTION_CHANGED__STATE__DENIED_INVALID_KEY = 5;
  
  public static final int ADB_CONNECTION_CHANGED__STATE__DENIED_VOLD_DECRYPT = 6;
  
  public static final int ADB_CONNECTION_CHANGED__STATE__DISCONNECTED = 7;
  
  public static final int ADB_CONNECTION_CHANGED__STATE__UNKNOWN = 0;
  
  public static final int ADB_CONNECTION_CHANGED__STATE__USER_ALLOWED = 2;
  
  public static final int ADB_CONNECTION_CHANGED__STATE__USER_DENIED = 3;
  
  public static final byte ANNOTATION_ID_EXCLUSIVE_STATE = 4;
  
  public static final byte ANNOTATION_ID_IS_UID = 1;
  
  public static final byte ANNOTATION_ID_PRIMARY_FIELD = 3;
  
  public static final byte ANNOTATION_ID_PRIMARY_FIELD_FIRST_UID = 5;
  
  public static final byte ANNOTATION_ID_STATE_NESTED = 8;
  
  public static final byte ANNOTATION_ID_TRIGGER_STATE_RESET = 7;
  
  public static final byte ANNOTATION_ID_TRUNCATE_TIMESTAMP = 2;
  
  public static final int ANROCCURRED__ERROR_SOURCE__DATA_APP = 1;
  
  public static final int ANROCCURRED__ERROR_SOURCE__ERROR_SOURCE_UNKNOWN = 0;
  
  public static final int ANROCCURRED__ERROR_SOURCE__SYSTEM_APP = 2;
  
  public static final int ANROCCURRED__ERROR_SOURCE__SYSTEM_SERVER = 3;
  
  public static final int ANROCCURRED__FOREGROUND_STATE__BACKGROUND = 1;
  
  public static final int ANROCCURRED__FOREGROUND_STATE__FOREGROUND = 2;
  
  public static final int ANROCCURRED__FOREGROUND_STATE__UNKNOWN = 0;
  
  public static final int ANROCCURRED__IS_INSTANT_APP__FALSE = 1;
  
  public static final int ANROCCURRED__IS_INSTANT_APP__TRUE = 2;
  
  public static final int ANROCCURRED__IS_INSTANT_APP__UNAVAILABLE = 0;
  
  public static final int ANR_OCCURRED = 79;
  
  public static final int APPS_ON_EXTERNAL_STORAGE_INFO = 10057;
  
  public static final int APPS_ON_EXTERNAL_STORAGE_INFO__EXTERNAL_STORAGE_TYPE__OTHER = 3;
  
  public static final int APPS_ON_EXTERNAL_STORAGE_INFO__EXTERNAL_STORAGE_TYPE__SD_CARD = 1;
  
  public static final int APPS_ON_EXTERNAL_STORAGE_INFO__EXTERNAL_STORAGE_TYPE__UNKNOWN = 0;
  
  public static final int APPS_ON_EXTERNAL_STORAGE_INFO__EXTERNAL_STORAGE_TYPE__USB = 2;
  
  public static final int APP_COMPACTED = 115;
  
  public static final int APP_COMPACTED__ACTION__BFGS = 4;
  
  public static final int APP_COMPACTED__ACTION__FULL = 2;
  
  public static final int APP_COMPACTED__ACTION__PERSISTENT = 3;
  
  public static final int APP_COMPACTED__ACTION__SOME = 1;
  
  public static final int APP_COMPACTED__ACTION__UNKNOWN = 0;
  
  public static final int APP_COMPACTED__LAST_ACTION__BFGS = 4;
  
  public static final int APP_COMPACTED__LAST_ACTION__FULL = 2;
  
  public static final int APP_COMPACTED__LAST_ACTION__PERSISTENT = 3;
  
  public static final int APP_COMPACTED__LAST_ACTION__SOME = 1;
  
  public static final int APP_COMPACTED__LAST_ACTION__UNKNOWN = 0;
  
  public static final int APP_COMPACTED__PROCESS_STATE__PROCESS_STATE_BACKUP = 1008;
  
  public static final int APP_COMPACTED__PROCESS_STATE__PROCESS_STATE_BOUND_FOREGROUND_SERVICE = 1004;
  
  public static final int APP_COMPACTED__PROCESS_STATE__PROCESS_STATE_BOUND_TOP = 1020;
  
  public static final int APP_COMPACTED__PROCESS_STATE__PROCESS_STATE_CACHED_ACTIVITY = 1015;
  
  public static final int APP_COMPACTED__PROCESS_STATE__PROCESS_STATE_CACHED_ACTIVITY_CLIENT = 1016;
  
  public static final int APP_COMPACTED__PROCESS_STATE__PROCESS_STATE_CACHED_EMPTY = 1018;
  
  public static final int APP_COMPACTED__PROCESS_STATE__PROCESS_STATE_CACHED_RECENT = 1017;
  
  public static final int APP_COMPACTED__PROCESS_STATE__PROCESS_STATE_FOREGROUND_SERVICE = 1003;
  
  public static final int APP_COMPACTED__PROCESS_STATE__PROCESS_STATE_HEAVY_WEIGHT = 1012;
  
  public static final int APP_COMPACTED__PROCESS_STATE__PROCESS_STATE_HOME = 1013;
  
  public static final int APP_COMPACTED__PROCESS_STATE__PROCESS_STATE_IMPORTANT_BACKGROUND = 1006;
  
  public static final int APP_COMPACTED__PROCESS_STATE__PROCESS_STATE_IMPORTANT_FOREGROUND = 1005;
  
  public static final int APP_COMPACTED__PROCESS_STATE__PROCESS_STATE_LAST_ACTIVITY = 1014;
  
  public static final int APP_COMPACTED__PROCESS_STATE__PROCESS_STATE_NONEXISTENT = 1019;
  
  public static final int APP_COMPACTED__PROCESS_STATE__PROCESS_STATE_PERSISTENT = 1000;
  
  public static final int APP_COMPACTED__PROCESS_STATE__PROCESS_STATE_PERSISTENT_UI = 1001;
  
  public static final int APP_COMPACTED__PROCESS_STATE__PROCESS_STATE_RECEIVER = 1010;
  
  public static final int APP_COMPACTED__PROCESS_STATE__PROCESS_STATE_SERVICE = 1009;
  
  public static final int APP_COMPACTED__PROCESS_STATE__PROCESS_STATE_TOP = 1002;
  
  public static final int APP_COMPACTED__PROCESS_STATE__PROCESS_STATE_TOP_SLEEPING = 1011;
  
  public static final int APP_COMPACTED__PROCESS_STATE__PROCESS_STATE_TRANSIENT_BACKGROUND = 1007;
  
  public static final int APP_COMPACTED__PROCESS_STATE__PROCESS_STATE_UNKNOWN = 999;
  
  public static final int APP_COMPACTED__PROCESS_STATE__PROCESS_STATE_UNKNOWN_TO_PROTO = 998;
  
  public static final int APP_COMPATIBILITY_CHANGE_REPORTED = 228;
  
  public static final int APP_COMPATIBILITY_CHANGE_REPORTED__SOURCE__APP_PROCESS = 1;
  
  public static final int APP_COMPATIBILITY_CHANGE_REPORTED__SOURCE__SYSTEM_SERVER = 2;
  
  public static final int APP_COMPATIBILITY_CHANGE_REPORTED__SOURCE__UNKNOWN_SOURCE = 0;
  
  public static final int APP_COMPATIBILITY_CHANGE_REPORTED__STATE__DISABLED = 2;
  
  public static final int APP_COMPATIBILITY_CHANGE_REPORTED__STATE__ENABLED = 1;
  
  public static final int APP_COMPATIBILITY_CHANGE_REPORTED__STATE__LOGGED = 3;
  
  public static final int APP_COMPATIBILITY_CHANGE_REPORTED__STATE__UNKNOWN_STATE = 0;
  
  public static final int APP_CRASH_OCCURRED = 78;
  
  public static final int APP_CRASH_OCCURRED__ERROR_SOURCE__DATA_APP = 1;
  
  public static final int APP_CRASH_OCCURRED__ERROR_SOURCE__ERROR_SOURCE_UNKNOWN = 0;
  
  public static final int APP_CRASH_OCCURRED__ERROR_SOURCE__SYSTEM_APP = 2;
  
  public static final int APP_CRASH_OCCURRED__ERROR_SOURCE__SYSTEM_SERVER = 3;
  
  public static final int APP_CRASH_OCCURRED__FOREGROUND_STATE__BACKGROUND = 1;
  
  public static final int APP_CRASH_OCCURRED__FOREGROUND_STATE__FOREGROUND = 2;
  
  public static final int APP_CRASH_OCCURRED__FOREGROUND_STATE__UNKNOWN = 0;
  
  public static final int APP_CRASH_OCCURRED__IS_INSTANT_APP__FALSE = 1;
  
  public static final int APP_CRASH_OCCURRED__IS_INSTANT_APP__TRUE = 2;
  
  public static final int APP_CRASH_OCCURRED__IS_INSTANT_APP__UNAVAILABLE = 0;
  
  public static final int APP_DIED = 65;
  
  public static final int APP_DOWNGRADED = 128;
  
  public static final int APP_FREEZE_CHANGED = 254;
  
  public static final int APP_FREEZE_CHANGED__ACTION__FREEZE_APP = 1;
  
  public static final int APP_FREEZE_CHANGED__ACTION__UNFREEZE_APP = 2;
  
  public static final int APP_FREEZE_CHANGED__ACTION__UNKNOWN = 0;
  
  public static final int APP_INSTALL_ON_EXTERNAL_STORAGE_REPORTED = 181;
  
  public static final int APP_INSTALL_ON_EXTERNAL_STORAGE_REPORTED__STORAGE_TYPE__OTHER = 3;
  
  public static final int APP_INSTALL_ON_EXTERNAL_STORAGE_REPORTED__STORAGE_TYPE__SD_CARD = 1;
  
  public static final int APP_INSTALL_ON_EXTERNAL_STORAGE_REPORTED__STORAGE_TYPE__UNKNOWN = 0;
  
  public static final int APP_INSTALL_ON_EXTERNAL_STORAGE_REPORTED__STORAGE_TYPE__USB = 2;
  
  public static final int APP_MOVED_STORAGE_REPORTED = 183;
  
  public static final int APP_MOVED_STORAGE_REPORTED__EXTERNAL_STORAGE_TYPE__OTHER = 3;
  
  public static final int APP_MOVED_STORAGE_REPORTED__EXTERNAL_STORAGE_TYPE__SD_CARD = 1;
  
  public static final int APP_MOVED_STORAGE_REPORTED__EXTERNAL_STORAGE_TYPE__UNKNOWN = 0;
  
  public static final int APP_MOVED_STORAGE_REPORTED__EXTERNAL_STORAGE_TYPE__USB = 2;
  
  public static final int APP_MOVED_STORAGE_REPORTED__MOVE_TYPE__TO_EXTERNAL = 1;
  
  public static final int APP_MOVED_STORAGE_REPORTED__MOVE_TYPE__TO_INTERNAL = 2;
  
  public static final int APP_MOVED_STORAGE_REPORTED__MOVE_TYPE__UNKNOWN = 0;
  
  public static final int APP_OPS = 10060;
  
  public static final int APP_OPS__OP_ID__APP_OP_ACCEPT_HANDOVER = 74;
  
  public static final int APP_OPS__OP_ID__APP_OP_ACCESS_ACCESSIBILITY = 88;
  
  public static final int APP_OPS__OP_ID__APP_OP_ACCESS_MEDIA_LOCATION = 90;
  
  public static final int APP_OPS__OP_ID__APP_OP_ACCESS_NOTIFICATIONS = 25;
  
  public static final int APP_OPS__OP_ID__APP_OP_ACTIVATE_PLATFORM_VPN = 94;
  
  public static final int APP_OPS__OP_ID__APP_OP_ACTIVATE_VPN = 47;
  
  public static final int APP_OPS__OP_ID__APP_OP_ACTIVITY_RECOGNITION = 79;
  
  public static final int APP_OPS__OP_ID__APP_OP_ADD_VOICEMAIL = 52;
  
  public static final int APP_OPS__OP_ID__APP_OP_ANSWER_PHONE_CALLS = 69;
  
  public static final int APP_OPS__OP_ID__APP_OP_ASSIST_SCREENSHOT = 50;
  
  public static final int APP_OPS__OP_ID__APP_OP_ASSIST_STRUCTURE = 49;
  
  public static final int APP_OPS__OP_ID__APP_OP_AUDIO_ACCESSIBILITY_VOLUME = 64;
  
  public static final int APP_OPS__OP_ID__APP_OP_AUDIO_ALARM_VOLUME = 37;
  
  public static final int APP_OPS__OP_ID__APP_OP_AUDIO_BLUETOOTH_VOLUME = 39;
  
  public static final int APP_OPS__OP_ID__APP_OP_AUDIO_MASTER_VOLUME = 33;
  
  public static final int APP_OPS__OP_ID__APP_OP_AUDIO_MEDIA_VOLUME = 36;
  
  public static final int APP_OPS__OP_ID__APP_OP_AUDIO_NOTIFICATION_VOLUME = 38;
  
  public static final int APP_OPS__OP_ID__APP_OP_AUDIO_RING_VOLUME = 35;
  
  public static final int APP_OPS__OP_ID__APP_OP_AUDIO_VOICE_VOLUME = 34;
  
  public static final int APP_OPS__OP_ID__APP_OP_AUTO_REVOKE_MANAGED_BY_INSTALLER = 98;
  
  public static final int APP_OPS__OP_ID__APP_OP_AUTO_REVOKE_PERMISSIONS_IF_UNUSED = 97;
  
  public static final int APP_OPS__OP_ID__APP_OP_BIND_ACCESSIBILITY_SERVICE = 73;
  
  public static final int APP_OPS__OP_ID__APP_OP_BLUETOOTH_SCAN = 77;
  
  public static final int APP_OPS__OP_ID__APP_OP_BODY_SENSORS = 56;
  
  public static final int APP_OPS__OP_ID__APP_OP_CALL_PHONE = 13;
  
  public static final int APP_OPS__OP_ID__APP_OP_CAMERA = 26;
  
  public static final int APP_OPS__OP_ID__APP_OP_CHANGE_WIFI_STATE = 71;
  
  public static final int APP_OPS__OP_ID__APP_OP_COARSE_LOCATION = 0;
  
  public static final int APP_OPS__OP_ID__APP_OP_DEPRECATED_1 = 96;
  
  public static final int APP_OPS__OP_ID__APP_OP_FINE_LOCATION = 1;
  
  public static final int APP_OPS__OP_ID__APP_OP_GET_ACCOUNTS = 62;
  
  public static final int APP_OPS__OP_ID__APP_OP_GET_USAGE_STATS = 43;
  
  public static final int APP_OPS__OP_ID__APP_OP_GPS = 2;
  
  public static final int APP_OPS__OP_ID__APP_OP_INSTANT_APP_START_FOREGROUND = 68;
  
  public static final int APP_OPS__OP_ID__APP_OP_INTERACT_ACROSS_PROFILES = 93;
  
  public static final int APP_OPS__OP_ID__APP_OP_LEGACY_STORAGE = 87;
  
  public static final int APP_OPS__OP_ID__APP_OP_LOADER_USAGE_STATS = 95;
  
  public static final int APP_OPS__OP_ID__APP_OP_MANAGE_EXTERNAL_STORAGE = 92;
  
  public static final int APP_OPS__OP_ID__APP_OP_MANAGE_IPSEC_TUNNELS = 75;
  
  public static final int APP_OPS__OP_ID__APP_OP_MOCK_LOCATION = 58;
  
  public static final int APP_OPS__OP_ID__APP_OP_MONITOR_HIGH_POWER_LOCATION = 42;
  
  public static final int APP_OPS__OP_ID__APP_OP_MONITOR_LOCATION = 41;
  
  public static final int APP_OPS__OP_ID__APP_OP_MUTE_MICROPHONE = 44;
  
  public static final int APP_OPS__OP_ID__APP_OP_NEIGHBORING_CELLS = 12;
  
  public static final int APP_OPS__OP_ID__APP_OP_NONE = -1;
  
  public static final int APP_OPS__OP_ID__APP_OP_NO_ISOLATED_STORAGE = 99;
  
  public static final int APP_OPS__OP_ID__APP_OP_PICTURE_IN_PICTURE = 67;
  
  public static final int APP_OPS__OP_ID__APP_OP_PLAY_AUDIO = 28;
  
  public static final int APP_OPS__OP_ID__APP_OP_POST_NOTIFICATION = 11;
  
  public static final int APP_OPS__OP_ID__APP_OP_PROCESS_OUTGOING_CALLS = 54;
  
  public static final int APP_OPS__OP_ID__APP_OP_PROJECT_MEDIA = 46;
  
  public static final int APP_OPS__OP_ID__APP_OP_QUERY_ALL_PACKAGES = 91;
  
  public static final int APP_OPS__OP_ID__APP_OP_READ_CALENDAR = 8;
  
  public static final int APP_OPS__OP_ID__APP_OP_READ_CALL_LOG = 6;
  
  public static final int APP_OPS__OP_ID__APP_OP_READ_CELL_BROADCASTS = 57;
  
  public static final int APP_OPS__OP_ID__APP_OP_READ_CLIPBOARD = 29;
  
  public static final int APP_OPS__OP_ID__APP_OP_READ_CONTACTS = 4;
  
  public static final int APP_OPS__OP_ID__APP_OP_READ_DEVICE_IDENTIFIERS = 89;
  
  public static final int APP_OPS__OP_ID__APP_OP_READ_EXTERNAL_STORAGE = 59;
  
  public static final int APP_OPS__OP_ID__APP_OP_READ_ICC_SMS = 21;
  
  public static final int APP_OPS__OP_ID__APP_OP_READ_MEDIA_AUDIO = 81;
  
  public static final int APP_OPS__OP_ID__APP_OP_READ_MEDIA_IMAGES = 85;
  
  public static final int APP_OPS__OP_ID__APP_OP_READ_MEDIA_VIDEO = 83;
  
  public static final int APP_OPS__OP_ID__APP_OP_READ_PHONE_NUMBERS = 65;
  
  public static final int APP_OPS__OP_ID__APP_OP_READ_PHONE_STATE = 51;
  
  public static final int APP_OPS__OP_ID__APP_OP_READ_SMS = 14;
  
  public static final int APP_OPS__OP_ID__APP_OP_RECEIVE_EMERGENCY_SMS = 17;
  
  public static final int APP_OPS__OP_ID__APP_OP_RECEIVE_MMS = 18;
  
  public static final int APP_OPS__OP_ID__APP_OP_RECEIVE_SMS = 16;
  
  public static final int APP_OPS__OP_ID__APP_OP_RECEIVE_WAP_PUSH = 19;
  
  public static final int APP_OPS__OP_ID__APP_OP_RECORD_AUDIO = 27;
  
  public static final int APP_OPS__OP_ID__APP_OP_REQUEST_DELETE_PACKAGES = 72;
  
  public static final int APP_OPS__OP_ID__APP_OP_REQUEST_INSTALL_PACKAGES = 66;
  
  public static final int APP_OPS__OP_ID__APP_OP_RUN_ANY_IN_BACKGROUND = 70;
  
  public static final int APP_OPS__OP_ID__APP_OP_RUN_IN_BACKGROUND = 63;
  
  public static final int APP_OPS__OP_ID__APP_OP_SEND_SMS = 20;
  
  public static final int APP_OPS__OP_ID__APP_OP_SMS_FINANCIAL_TRANSACTIONS = 80;
  
  public static final int APP_OPS__OP_ID__APP_OP_START_FOREGROUND = 76;
  
  public static final int APP_OPS__OP_ID__APP_OP_SYSTEM_ALERT_WINDOW = 24;
  
  public static final int APP_OPS__OP_ID__APP_OP_TAKE_AUDIO_FOCUS = 32;
  
  public static final int APP_OPS__OP_ID__APP_OP_TAKE_MEDIA_BUTTONS = 31;
  
  public static final int APP_OPS__OP_ID__APP_OP_TOAST_WINDOW = 45;
  
  public static final int APP_OPS__OP_ID__APP_OP_TURN_SCREEN_ON = 61;
  
  public static final int APP_OPS__OP_ID__APP_OP_USE_BIOMETRIC = 78;
  
  public static final int APP_OPS__OP_ID__APP_OP_USE_FINGERPRINT = 55;
  
  public static final int APP_OPS__OP_ID__APP_OP_USE_SIP = 53;
  
  public static final int APP_OPS__OP_ID__APP_OP_VIBRATE = 3;
  
  public static final int APP_OPS__OP_ID__APP_OP_WAKE_LOCK = 40;
  
  public static final int APP_OPS__OP_ID__APP_OP_WIFI_SCAN = 10;
  
  public static final int APP_OPS__OP_ID__APP_OP_WRITE_CALENDAR = 9;
  
  public static final int APP_OPS__OP_ID__APP_OP_WRITE_CALL_LOG = 7;
  
  public static final int APP_OPS__OP_ID__APP_OP_WRITE_CLIPBOARD = 30;
  
  public static final int APP_OPS__OP_ID__APP_OP_WRITE_CONTACTS = 5;
  
  public static final int APP_OPS__OP_ID__APP_OP_WRITE_EXTERNAL_STORAGE = 60;
  
  public static final int APP_OPS__OP_ID__APP_OP_WRITE_ICC_SMS = 22;
  
  public static final int APP_OPS__OP_ID__APP_OP_WRITE_MEDIA_AUDIO = 82;
  
  public static final int APP_OPS__OP_ID__APP_OP_WRITE_MEDIA_IMAGES = 86;
  
  public static final int APP_OPS__OP_ID__APP_OP_WRITE_MEDIA_VIDEO = 84;
  
  public static final int APP_OPS__OP_ID__APP_OP_WRITE_SETTINGS = 23;
  
  public static final int APP_OPS__OP_ID__APP_OP_WRITE_SMS = 15;
  
  public static final int APP_OPS__OP_ID__APP_OP_WRITE_WALLPAPER = 48;
  
  public static final int APP_SIZE = 10027;
  
  public static final int APP_STANDBY_BUCKET_CHANGED = 258;
  
  public static final int APP_STANDBY_BUCKET_CHANGED__BUCKET__BUCKET_ACTIVE = 10;
  
  public static final int APP_STANDBY_BUCKET_CHANGED__BUCKET__BUCKET_EXEMPTED = 5;
  
  public static final int APP_STANDBY_BUCKET_CHANGED__BUCKET__BUCKET_FREQUENT = 30;
  
  public static final int APP_STANDBY_BUCKET_CHANGED__BUCKET__BUCKET_NEVER = 50;
  
  public static final int APP_STANDBY_BUCKET_CHANGED__BUCKET__BUCKET_RARE = 40;
  
  public static final int APP_STANDBY_BUCKET_CHANGED__BUCKET__BUCKET_RESTRICTED = 45;
  
  public static final int APP_STANDBY_BUCKET_CHANGED__BUCKET__BUCKET_UNKNOWN = 0;
  
  public static final int APP_STANDBY_BUCKET_CHANGED__BUCKET__BUCKET_WORKING_SET = 20;
  
  public static final int APP_STANDBY_BUCKET_CHANGED__MAIN_REASON__MAIN_DEFAULT = 256;
  
  public static final int APP_STANDBY_BUCKET_CHANGED__MAIN_REASON__MAIN_FORCED_BY_SYSTEM = 1536;
  
  public static final int APP_STANDBY_BUCKET_CHANGED__MAIN_REASON__MAIN_FORCED_BY_USER = 1024;
  
  public static final int APP_STANDBY_BUCKET_CHANGED__MAIN_REASON__MAIN_PREDICTED = 1280;
  
  public static final int APP_STANDBY_BUCKET_CHANGED__MAIN_REASON__MAIN_TIMEOUT = 512;
  
  public static final int APP_STANDBY_BUCKET_CHANGED__MAIN_REASON__MAIN_UNKNOWN = 0;
  
  public static final int APP_STANDBY_BUCKET_CHANGED__MAIN_REASON__MAIN_USAGE = 768;
  
  public static final int APP_START_CANCELED = 49;
  
  public static final int APP_START_CANCELED__TYPE__COLD = 3;
  
  public static final int APP_START_CANCELED__TYPE__HOT = 2;
  
  public static final int APP_START_CANCELED__TYPE__UNKNOWN = 0;
  
  public static final int APP_START_CANCELED__TYPE__WARM = 1;
  
  public static final int APP_START_FULLY_DRAWN = 50;
  
  public static final int APP_START_FULLY_DRAWN__TYPE__UNKNOWN = 0;
  
  public static final int APP_START_FULLY_DRAWN__TYPE__WITHOUT_BUNDLE = 2;
  
  public static final int APP_START_FULLY_DRAWN__TYPE__WITH_BUNDLE = 1;
  
  public static final int APP_START_MEMORY_STATE_CAPTURED = 55;
  
  public static final int APP_START_OCCURRED = 48;
  
  public static final int APP_START_OCCURRED__REASON__APP_TRANSITION_REASON_UNKNOWN = 0;
  
  public static final int APP_START_OCCURRED__REASON__APP_TRANSITION_RECENTS_ANIM = 5;
  
  public static final int APP_START_OCCURRED__REASON__APP_TRANSITION_SNAPSHOT = 4;
  
  public static final int APP_START_OCCURRED__REASON__APP_TRANSITION_SPLASH_SCREEN = 1;
  
  public static final int APP_START_OCCURRED__REASON__APP_TRANSITION_TIMEOUT = 3;
  
  public static final int APP_START_OCCURRED__REASON__APP_TRANSITION_WINDOWS_DRAWN = 2;
  
  public static final int APP_START_OCCURRED__TYPE__COLD = 3;
  
  public static final int APP_START_OCCURRED__TYPE__HOT = 2;
  
  public static final int APP_START_OCCURRED__TYPE__UNKNOWN = 0;
  
  public static final int APP_START_OCCURRED__TYPE__WARM = 1;
  
  public static final int APP_USAGE_EVENT_OCCURRED = 269;
  
  public static final int APP_USAGE_EVENT_OCCURRED__EVENT_TYPE__MOVE_TO_BACKGROUND = 2;
  
  public static final int APP_USAGE_EVENT_OCCURRED__EVENT_TYPE__MOVE_TO_FOREGROUND = 1;
  
  public static final int APP_USAGE_EVENT_OCCURRED__EVENT_TYPE__NONE = 0;
  
  public static final int ASSISTANT_INVOCATION_REPORTED = 281;
  
  public static final int ASSISTANT_INVOCATION_REPORTED__DEVICE_STATE__AOD1 = 1;
  
  public static final int ASSISTANT_INVOCATION_REPORTED__DEVICE_STATE__AOD2 = 2;
  
  public static final int ASSISTANT_INVOCATION_REPORTED__DEVICE_STATE__APP_DEFAULT = 8;
  
  public static final int ASSISTANT_INVOCATION_REPORTED__DEVICE_STATE__APP_FULLSCREEN = 10;
  
  public static final int ASSISTANT_INVOCATION_REPORTED__DEVICE_STATE__APP_IMMERSIVE = 9;
  
  public static final int ASSISTANT_INVOCATION_REPORTED__DEVICE_STATE__BOUNCER = 3;
  
  public static final int ASSISTANT_INVOCATION_REPORTED__DEVICE_STATE__LAUNCHER_ALL_APPS = 7;
  
  public static final int ASSISTANT_INVOCATION_REPORTED__DEVICE_STATE__LAUNCHER_HOME = 5;
  
  public static final int ASSISTANT_INVOCATION_REPORTED__DEVICE_STATE__LAUNCHER_OVERVIEW = 6;
  
  public static final int ASSISTANT_INVOCATION_REPORTED__DEVICE_STATE__UNKNOWN_DEVICE_STATE = 0;
  
  public static final int ASSISTANT_INVOCATION_REPORTED__DEVICE_STATE__UNLOCKED_LOCKSCREEN = 4;
  
  public static final int ATTENTION_MANAGER_SERVICE_RESULT_REPORTED = 143;
  
  public static final int ATTENTION_MANAGER_SERVICE_RESULT_REPORTED__ATTENTION_CHECK_RESULT__ATTENTION_FAILURE_CAMERA_PERMISSION_ABSENT = 6;
  
  public static final int ATTENTION_MANAGER_SERVICE_RESULT_REPORTED__ATTENTION_CHECK_RESULT__ATTENTION_FAILURE_CANCELLED = 3;
  
  public static final int ATTENTION_MANAGER_SERVICE_RESULT_REPORTED__ATTENTION_CHECK_RESULT__ATTENTION_FAILURE_PREEMPTED = 4;
  
  public static final int ATTENTION_MANAGER_SERVICE_RESULT_REPORTED__ATTENTION_CHECK_RESULT__ATTENTION_FAILURE_TIMED_OUT = 5;
  
  public static final int ATTENTION_MANAGER_SERVICE_RESULT_REPORTED__ATTENTION_CHECK_RESULT__ATTENTION_FAILURE_UNKNOWN = 2;
  
  public static final int ATTENTION_MANAGER_SERVICE_RESULT_REPORTED__ATTENTION_CHECK_RESULT__ATTENTION_SUCCESS_ABSENT = 0;
  
  public static final int ATTENTION_MANAGER_SERVICE_RESULT_REPORTED__ATTENTION_CHECK_RESULT__ATTENTION_SUCCESS_PRESENT = 1;
  
  public static final int ATTENTION_MANAGER_SERVICE_RESULT_REPORTED__ATTENTION_CHECK_RESULT__UNKNOWN = 20;
  
  public static final int ATTRIBUTED_APP_OPS = 10075;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_ACCEPT_HANDOVER = 74;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_ACCESS_ACCESSIBILITY = 88;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_ACCESS_MEDIA_LOCATION = 90;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_ACCESS_NOTIFICATIONS = 25;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_ACTIVATE_PLATFORM_VPN = 94;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_ACTIVATE_VPN = 47;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_ACTIVITY_RECOGNITION = 79;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_ADD_VOICEMAIL = 52;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_ANSWER_PHONE_CALLS = 69;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_ASSIST_SCREENSHOT = 50;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_ASSIST_STRUCTURE = 49;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_AUDIO_ACCESSIBILITY_VOLUME = 64;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_AUDIO_ALARM_VOLUME = 37;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_AUDIO_BLUETOOTH_VOLUME = 39;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_AUDIO_MASTER_VOLUME = 33;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_AUDIO_MEDIA_VOLUME = 36;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_AUDIO_NOTIFICATION_VOLUME = 38;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_AUDIO_RING_VOLUME = 35;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_AUDIO_VOICE_VOLUME = 34;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_AUTO_REVOKE_MANAGED_BY_INSTALLER = 98;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_AUTO_REVOKE_PERMISSIONS_IF_UNUSED = 97;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_BIND_ACCESSIBILITY_SERVICE = 73;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_BLUETOOTH_SCAN = 77;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_BODY_SENSORS = 56;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_CALL_PHONE = 13;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_CAMERA = 26;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_CHANGE_WIFI_STATE = 71;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_COARSE_LOCATION = 0;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_DEPRECATED_1 = 96;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_FINE_LOCATION = 1;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_GET_ACCOUNTS = 62;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_GET_USAGE_STATS = 43;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_GPS = 2;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_INSTANT_APP_START_FOREGROUND = 68;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_INTERACT_ACROSS_PROFILES = 93;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_LEGACY_STORAGE = 87;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_LOADER_USAGE_STATS = 95;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_MANAGE_EXTERNAL_STORAGE = 92;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_MANAGE_IPSEC_TUNNELS = 75;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_MOCK_LOCATION = 58;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_MONITOR_HIGH_POWER_LOCATION = 42;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_MONITOR_LOCATION = 41;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_MUTE_MICROPHONE = 44;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_NEIGHBORING_CELLS = 12;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_NONE = -1;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_NO_ISOLATED_STORAGE = 99;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_PICTURE_IN_PICTURE = 67;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_PLAY_AUDIO = 28;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_POST_NOTIFICATION = 11;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_PROCESS_OUTGOING_CALLS = 54;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_PROJECT_MEDIA = 46;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_QUERY_ALL_PACKAGES = 91;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_READ_CALENDAR = 8;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_READ_CALL_LOG = 6;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_READ_CELL_BROADCASTS = 57;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_READ_CLIPBOARD = 29;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_READ_CONTACTS = 4;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_READ_DEVICE_IDENTIFIERS = 89;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_READ_EXTERNAL_STORAGE = 59;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_READ_ICC_SMS = 21;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_READ_MEDIA_AUDIO = 81;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_READ_MEDIA_IMAGES = 85;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_READ_MEDIA_VIDEO = 83;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_READ_PHONE_NUMBERS = 65;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_READ_PHONE_STATE = 51;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_READ_SMS = 14;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_RECEIVE_EMERGENCY_SMS = 17;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_RECEIVE_MMS = 18;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_RECEIVE_SMS = 16;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_RECEIVE_WAP_PUSH = 19;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_RECORD_AUDIO = 27;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_REQUEST_DELETE_PACKAGES = 72;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_REQUEST_INSTALL_PACKAGES = 66;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_RUN_ANY_IN_BACKGROUND = 70;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_RUN_IN_BACKGROUND = 63;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_SEND_SMS = 20;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_SMS_FINANCIAL_TRANSACTIONS = 80;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_START_FOREGROUND = 76;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_SYSTEM_ALERT_WINDOW = 24;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_TAKE_AUDIO_FOCUS = 32;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_TAKE_MEDIA_BUTTONS = 31;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_TOAST_WINDOW = 45;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_TURN_SCREEN_ON = 61;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_USE_BIOMETRIC = 78;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_USE_FINGERPRINT = 55;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_USE_SIP = 53;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_VIBRATE = 3;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_WAKE_LOCK = 40;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_WIFI_SCAN = 10;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_WRITE_CALENDAR = 9;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_WRITE_CALL_LOG = 7;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_WRITE_CLIPBOARD = 30;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_WRITE_CONTACTS = 5;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_WRITE_EXTERNAL_STORAGE = 60;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_WRITE_ICC_SMS = 22;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_WRITE_MEDIA_AUDIO = 82;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_WRITE_MEDIA_IMAGES = 86;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_WRITE_MEDIA_VIDEO = 84;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_WRITE_SETTINGS = 23;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_WRITE_SMS = 15;
  
  public static final int ATTRIBUTED_APP_OPS__OP__APP_OP_WRITE_WALLPAPER = 48;
  
  public static final int AUDIO_STATE_CHANGED = 23;
  
  public static final int AUDIO_STATE_CHANGED__STATE__OFF = 0;
  
  public static final int AUDIO_STATE_CHANGED__STATE__ON = 1;
  
  public static final int AUDIO_STATE_CHANGED__STATE__RESET = 2;
  
  public static final int BATTERY_CYCLE_COUNT = 10045;
  
  public static final int BATTERY_LEVEL = 10043;
  
  public static final int BATTERY_LEVEL_CHANGED = 30;
  
  public static final int BATTERY_SAVER_MODE_STATE_CHANGED = 20;
  
  public static final int BATTERY_SAVER_MODE_STATE_CHANGED__STATE__OFF = 0;
  
  public static final int BATTERY_SAVER_MODE_STATE_CHANGED__STATE__ON = 1;
  
  public static final int BATTERY_VOLTAGE = 10030;
  
  public static final int BINDER_CALLS = 10022;
  
  public static final int BINDER_CALLS_EXCEPTIONS = 10023;
  
  public static final int BIOMETRIC_ACQUIRED = 87;
  
  public static final int BIOMETRIC_ACQUIRED__ACTION__ACTION_AUTHENTICATE = 2;
  
  public static final int BIOMETRIC_ACQUIRED__ACTION__ACTION_ENGINEER = 6;
  
  public static final int BIOMETRIC_ACQUIRED__ACTION__ACTION_ENROLL = 1;
  
  public static final int BIOMETRIC_ACQUIRED__ACTION__ACTION_ENUMERATE = 3;
  
  public static final int BIOMETRIC_ACQUIRED__ACTION__ACTION_REMOVE = 4;
  
  public static final int BIOMETRIC_ACQUIRED__ACTION__ACTION_TOUCH = 5;
  
  public static final int BIOMETRIC_ACQUIRED__ACTION__ACTION_UNKNOWN = 0;
  
  public static final int BIOMETRIC_ACQUIRED__CLIENT__CLIENT_BIOMETRIC_PROMPT = 2;
  
  public static final int BIOMETRIC_ACQUIRED__CLIENT__CLIENT_FINGERPRINT_MANAGER = 3;
  
  public static final int BIOMETRIC_ACQUIRED__CLIENT__CLIENT_KEYGUARD = 1;
  
  public static final int BIOMETRIC_ACQUIRED__CLIENT__CLIENT_UNKNOWN = 0;
  
  public static final int BIOMETRIC_ACQUIRED__MODALITY__MODALITY_FACE = 4;
  
  public static final int BIOMETRIC_ACQUIRED__MODALITY__MODALITY_FINGERPRINT = 1;
  
  public static final int BIOMETRIC_ACQUIRED__MODALITY__MODALITY_IRIS = 2;
  
  public static final int BIOMETRIC_ACQUIRED__MODALITY__MODALITY_UNKNOWN = 0;
  
  public static final int BIOMETRIC_AUTHENTICATED = 88;
  
  public static final int BIOMETRIC_AUTHENTICATED__CLIENT__CLIENT_BIOMETRIC_PROMPT = 2;
  
  public static final int BIOMETRIC_AUTHENTICATED__CLIENT__CLIENT_FINGERPRINT_MANAGER = 3;
  
  public static final int BIOMETRIC_AUTHENTICATED__CLIENT__CLIENT_KEYGUARD = 1;
  
  public static final int BIOMETRIC_AUTHENTICATED__CLIENT__CLIENT_UNKNOWN = 0;
  
  public static final int BIOMETRIC_AUTHENTICATED__MODALITY__MODALITY_FACE = 4;
  
  public static final int BIOMETRIC_AUTHENTICATED__MODALITY__MODALITY_FINGERPRINT = 1;
  
  public static final int BIOMETRIC_AUTHENTICATED__MODALITY__MODALITY_IRIS = 2;
  
  public static final int BIOMETRIC_AUTHENTICATED__MODALITY__MODALITY_UNKNOWN = 0;
  
  public static final int BIOMETRIC_AUTHENTICATED__STATE__CONFIRMED = 3;
  
  public static final int BIOMETRIC_AUTHENTICATED__STATE__PENDING_CONFIRMATION = 2;
  
  public static final int BIOMETRIC_AUTHENTICATED__STATE__REJECTED = 1;
  
  public static final int BIOMETRIC_AUTHENTICATED__STATE__UNKNOWN = 0;
  
  public static final int BIOMETRIC_ENROLLED = 184;
  
  public static final int BIOMETRIC_ENROLLED__MODALITY__MODALITY_FACE = 4;
  
  public static final int BIOMETRIC_ENROLLED__MODALITY__MODALITY_FINGERPRINT = 1;
  
  public static final int BIOMETRIC_ENROLLED__MODALITY__MODALITY_IRIS = 2;
  
  public static final int BIOMETRIC_ENROLLED__MODALITY__MODALITY_UNKNOWN = 0;
  
  public static final int BIOMETRIC_ERROR_OCCURRED = 89;
  
  public static final int BIOMETRIC_ERROR_OCCURRED__ACTION__ACTION_AUTHENTICATE = 2;
  
  public static final int BIOMETRIC_ERROR_OCCURRED__ACTION__ACTION_ENGINEER = 6;
  
  public static final int BIOMETRIC_ERROR_OCCURRED__ACTION__ACTION_ENROLL = 1;
  
  public static final int BIOMETRIC_ERROR_OCCURRED__ACTION__ACTION_ENUMERATE = 3;
  
  public static final int BIOMETRIC_ERROR_OCCURRED__ACTION__ACTION_REMOVE = 4;
  
  public static final int BIOMETRIC_ERROR_OCCURRED__ACTION__ACTION_TOUCH = 5;
  
  public static final int BIOMETRIC_ERROR_OCCURRED__ACTION__ACTION_UNKNOWN = 0;
  
  public static final int BIOMETRIC_ERROR_OCCURRED__CLIENT__CLIENT_BIOMETRIC_PROMPT = 2;
  
  public static final int BIOMETRIC_ERROR_OCCURRED__CLIENT__CLIENT_FINGERPRINT_MANAGER = 3;
  
  public static final int BIOMETRIC_ERROR_OCCURRED__CLIENT__CLIENT_KEYGUARD = 1;
  
  public static final int BIOMETRIC_ERROR_OCCURRED__CLIENT__CLIENT_UNKNOWN = 0;
  
  public static final int BIOMETRIC_ERROR_OCCURRED__MODALITY__MODALITY_FACE = 4;
  
  public static final int BIOMETRIC_ERROR_OCCURRED__MODALITY__MODALITY_FINGERPRINT = 1;
  
  public static final int BIOMETRIC_ERROR_OCCURRED__MODALITY__MODALITY_IRIS = 2;
  
  public static final int BIOMETRIC_ERROR_OCCURRED__MODALITY__MODALITY_UNKNOWN = 0;
  
  public static final int BIOMETRIC_SYSTEM_HEALTH_ISSUE_DETECTED = 148;
  
  public static final int BIOMETRIC_SYSTEM_HEALTH_ISSUE_DETECTED__ISSUE__ISSUE_CANCEL_TIMED_OUT = 4;
  
  public static final int BIOMETRIC_SYSTEM_HEALTH_ISSUE_DETECTED__ISSUE__ISSUE_HAL_DEATH = 1;
  
  public static final int BIOMETRIC_SYSTEM_HEALTH_ISSUE_DETECTED__ISSUE__ISSUE_UNKNOWN = 0;
  
  public static final int BIOMETRIC_SYSTEM_HEALTH_ISSUE_DETECTED__ISSUE__ISSUE_UNKNOWN_TEMPLATE_ENROLLED_FRAMEWORK = 2;
  
  public static final int BIOMETRIC_SYSTEM_HEALTH_ISSUE_DETECTED__ISSUE__ISSUE_UNKNOWN_TEMPLATE_ENROLLED_HAL = 3;
  
  public static final int BIOMETRIC_SYSTEM_HEALTH_ISSUE_DETECTED__MODALITY__MODALITY_FACE = 4;
  
  public static final int BIOMETRIC_SYSTEM_HEALTH_ISSUE_DETECTED__MODALITY__MODALITY_FINGERPRINT = 1;
  
  public static final int BIOMETRIC_SYSTEM_HEALTH_ISSUE_DETECTED__MODALITY__MODALITY_IRIS = 2;
  
  public static final int BIOMETRIC_SYSTEM_HEALTH_ISSUE_DETECTED__MODALITY__MODALITY_UNKNOWN = 0;
  
  public static final int BLOB_COMMITTED = 298;
  
  public static final int BLOB_COMMITTED__RESULT__COUNT_LIMIT_EXCEEDED = 4;
  
  public static final int BLOB_COMMITTED__RESULT__DIGEST_MISMATCH = 3;
  
  public static final int BLOB_COMMITTED__RESULT__ERROR_DURING_COMMIT = 2;
  
  public static final int BLOB_COMMITTED__RESULT__SUCCESS = 1;
  
  public static final int BLOB_COMMITTED__RESULT__UNKNOWN = 0;
  
  public static final int BLOB_INFO = 10081;
  
  public static final int BLOB_LEASED = 299;
  
  public static final int BLOB_LEASED__RESULT__ACCESS_NOT_ALLOWED = 3;
  
  public static final int BLOB_LEASED__RESULT__BLOB_DNE = 2;
  
  public static final int BLOB_LEASED__RESULT__COUNT_LIMIT_EXCEEDED = 6;
  
  public static final int BLOB_LEASED__RESULT__DATA_SIZE_LIMIT_EXCEEDED = 5;
  
  public static final int BLOB_LEASED__RESULT__LEASE_EXPIRY_INVALID = 4;
  
  public static final int BLOB_LEASED__RESULT__SUCCESS = 1;
  
  public static final int BLOB_LEASED__RESULT__UNKNOWN = 0;
  
  public static final int BLOB_OPENED = 300;
  
  public static final int BLOB_OPENED__RESULT__ACCESS_NOT_ALLOWED = 3;
  
  public static final int BLOB_OPENED__RESULT__BLOB_DNE = 2;
  
  public static final int BLOB_OPENED__RESULT__SUCCESS = 1;
  
  public static final int BLOB_OPENED__RESULT__UNKNOWN = 0;
  
  public static final int BLUETOOTH_ACTIVITY_INFO = 10007;
  
  public static final int BLUETOOTH_BYTES_TRANSFER = 10006;
  
  public static final int BLUETOOTH_ENABLED_STATE_CHANGED = 67;
  
  public static final int BLUETOOTH_ENABLED_STATE_CHANGED__REASON__ENABLE_DISABLE_REASON_AIRPLANE_MODE = 2;
  
  public static final int BLUETOOTH_ENABLED_STATE_CHANGED__REASON__ENABLE_DISABLE_REASON_APPLICATION_REQUEST = 1;
  
  public static final int BLUETOOTH_ENABLED_STATE_CHANGED__REASON__ENABLE_DISABLE_REASON_CRASH = 7;
  
  public static final int BLUETOOTH_ENABLED_STATE_CHANGED__REASON__ENABLE_DISABLE_REASON_DISALLOWED = 3;
  
  public static final int BLUETOOTH_ENABLED_STATE_CHANGED__REASON__ENABLE_DISABLE_REASON_FACTORY_RESET = 10;
  
  public static final int BLUETOOTH_ENABLED_STATE_CHANGED__REASON__ENABLE_DISABLE_REASON_RESTARTED = 4;
  
  public static final int BLUETOOTH_ENABLED_STATE_CHANGED__REASON__ENABLE_DISABLE_REASON_RESTORE_USER_SETTING = 9;
  
  public static final int BLUETOOTH_ENABLED_STATE_CHANGED__REASON__ENABLE_DISABLE_REASON_START_ERROR = 5;
  
  public static final int BLUETOOTH_ENABLED_STATE_CHANGED__REASON__ENABLE_DISABLE_REASON_SYSTEM_BOOT = 6;
  
  public static final int BLUETOOTH_ENABLED_STATE_CHANGED__REASON__ENABLE_DISABLE_REASON_UNSPECIFIED = 0;
  
  public static final int BLUETOOTH_ENABLED_STATE_CHANGED__REASON__ENABLE_DISABLE_REASON_USER_SWITCH = 8;
  
  public static final int BLUETOOTH_ENABLED_STATE_CHANGED__STATE__DISABLED = 2;
  
  public static final int BLUETOOTH_ENABLED_STATE_CHANGED__STATE__ENABLED = 1;
  
  public static final int BLUETOOTH_ENABLED_STATE_CHANGED__STATE__UNKNOWN = 0;
  
  public static final int BOOT_TIME_EVENT_DURATION_REPORTED = 239;
  
  public static final int BOOT_TIME_EVENT_DURATION__EVENT__ABSOLUTE_BOOT_TIME = 1;
  
  public static final int BOOT_TIME_EVENT_DURATION__EVENT__ANDROID_INIT_STAGE_1 = 19;
  
  public static final int BOOT_TIME_EVENT_DURATION__EVENT__BOOTLOADER_FIRST_STAGE_EXEC = 2;
  
  public static final int BOOT_TIME_EVENT_DURATION__EVENT__BOOTLOADER_FIRST_STAGE_LOAD = 3;
  
  public static final int BOOT_TIME_EVENT_DURATION__EVENT__BOOTLOADER_KERNEL_LOAD = 4;
  
  public static final int BOOT_TIME_EVENT_DURATION__EVENT__BOOTLOADER_SECOND_STAGE_EXEC = 5;
  
  public static final int BOOT_TIME_EVENT_DURATION__EVENT__BOOTLOADER_SECOND_STAGE_LOAD = 6;
  
  public static final int BOOT_TIME_EVENT_DURATION__EVENT__BOOTLOADER_TOTAL = 8;
  
  public static final int BOOT_TIME_EVENT_DURATION__EVENT__BOOTLOADER_UI_WAIT = 7;
  
  public static final int BOOT_TIME_EVENT_DURATION__EVENT__COLDBOOT_WAIT = 16;
  
  public static final int BOOT_TIME_EVENT_DURATION__EVENT__FACTORY_RESET_TIME_SINCE_RESET = 18;
  
  public static final int BOOT_TIME_EVENT_DURATION__EVENT__MOUNT_DEFAULT_DURATION = 10;
  
  public static final int BOOT_TIME_EVENT_DURATION__EVENT__MOUNT_EARLY_DURATION = 11;
  
  public static final int BOOT_TIME_EVENT_DURATION__EVENT__MOUNT_LATE_DURATION = 12;
  
  public static final int BOOT_TIME_EVENT_DURATION__EVENT__OTA_PACKAGE_MANAGER_DATA_APP_AVG_SCAN_TIME = 14;
  
  public static final int BOOT_TIME_EVENT_DURATION__EVENT__OTA_PACKAGE_MANAGER_INIT_TIME = 13;
  
  public static final int BOOT_TIME_EVENT_DURATION__EVENT__OTA_PACKAGE_MANAGER_SYSTEM_APP_AVG_SCAN_TIME = 15;
  
  public static final int BOOT_TIME_EVENT_DURATION__EVENT__SELINUX_INIT = 17;
  
  public static final int BOOT_TIME_EVENT_DURATION__EVENT__SHUTDOWN_DURATION = 9;
  
  public static final int BOOT_TIME_EVENT_DURATION__EVENT__UNKNOWN = 0;
  
  public static final int BOOT_TIME_EVENT_ELAPSED_TIME_REPORTED = 240;
  
  public static final int BOOT_TIME_EVENT_ELAPSED_TIME__EVENT__ANDROID_INIT_STAGE_1 = 1;
  
  public static final int BOOT_TIME_EVENT_ELAPSED_TIME__EVENT__BOOT_COMPLETE = 2;
  
  public static final int BOOT_TIME_EVENT_ELAPSED_TIME__EVENT__BOOT_COMPLETE_ENCRYPTION = 3;
  
  public static final int BOOT_TIME_EVENT_ELAPSED_TIME__EVENT__BOOT_COMPLETE_NO_ENCRYPTION = 4;
  
  public static final int BOOT_TIME_EVENT_ELAPSED_TIME__EVENT__BOOT_COMPLETE_POST_DECRYPT = 5;
  
  public static final int BOOT_TIME_EVENT_ELAPSED_TIME__EVENT__FACTORY_RESET_BOOT_COMPLETE = 6;
  
  public static final int BOOT_TIME_EVENT_ELAPSED_TIME__EVENT__FACTORY_RESET_BOOT_COMPLETE_NO_ENCRYPTION = 7;
  
  public static final int BOOT_TIME_EVENT_ELAPSED_TIME__EVENT__FACTORY_RESET_BOOT_COMPLETE_POST_DECRYPT = 8;
  
  public static final int BOOT_TIME_EVENT_ELAPSED_TIME__EVENT__FRAMEWORK_BOOT_COMPLETED = 13;
  
  public static final int BOOT_TIME_EVENT_ELAPSED_TIME__EVENT__FRAMEWORK_LOCKED_BOOT_COMPLETED = 12;
  
  public static final int BOOT_TIME_EVENT_ELAPSED_TIME__EVENT__LAUNCHER_SHOWN = 22;
  
  public static final int BOOT_TIME_EVENT_ELAPSED_TIME__EVENT__LAUNCHER_START = 21;
  
  public static final int BOOT_TIME_EVENT_ELAPSED_TIME__EVENT__OTA_BOOT_COMPLETE = 9;
  
  public static final int BOOT_TIME_EVENT_ELAPSED_TIME__EVENT__OTA_BOOT_COMPLETE_NO_ENCRYPTION = 10;
  
  public static final int BOOT_TIME_EVENT_ELAPSED_TIME__EVENT__OTA_BOOT_COMPLETE_POST_DECRYPT = 11;
  
  public static final int BOOT_TIME_EVENT_ELAPSED_TIME__EVENT__PACKAGE_MANAGER_INIT_READY = 15;
  
  public static final int BOOT_TIME_EVENT_ELAPSED_TIME__EVENT__PACKAGE_MANAGER_INIT_START = 14;
  
  public static final int BOOT_TIME_EVENT_ELAPSED_TIME__EVENT__POST_DECRYPT = 16;
  
  public static final int BOOT_TIME_EVENT_ELAPSED_TIME__EVENT__SECONDARY_ZYGOTE_INIT_START = 18;
  
  public static final int BOOT_TIME_EVENT_ELAPSED_TIME__EVENT__SYSTEM_SERVER_INIT_START = 19;
  
  public static final int BOOT_TIME_EVENT_ELAPSED_TIME__EVENT__SYSTEM_SERVER_READY = 20;
  
  public static final int BOOT_TIME_EVENT_ELAPSED_TIME__EVENT__UNKNOWN = 0;
  
  public static final int BOOT_TIME_EVENT_ELAPSED_TIME__EVENT__ZYGOTE_INIT_START = 17;
  
  public static final int BOOT_TIME_EVENT_ERROR_CODE_REPORTED = 242;
  
  public static final int BOOT_TIME_EVENT_ERROR_CODE__EVENT__FACTORY_RESET_CURRENT_TIME_FAILURE = 1;
  
  public static final int BOOT_TIME_EVENT_ERROR_CODE__EVENT__FS_MGR_FS_STAT_DATA_PARTITION = 3;
  
  public static final int BOOT_TIME_EVENT_ERROR_CODE__EVENT__SHUTDOWN_UMOUNT_STAT = 2;
  
  public static final int BOOT_TIME_EVENT_ERROR_CODE__EVENT__UNKNOWN = 0;
  
  public static final int BROADCAST_DISPATCH_LATENCY_REPORTED = 142;
  
  public static final int BUBBLE_DEVELOPER_ERROR_REPORTED = 173;
  
  public static final int BUBBLE_DEVELOPER_ERROR_REPORTED__ERROR__ACTIVITY_INFO_MISSING = 1;
  
  public static final int BUBBLE_DEVELOPER_ERROR_REPORTED__ERROR__ACTIVITY_INFO_NOT_RESIZABLE = 2;
  
  public static final int BUBBLE_DEVELOPER_ERROR_REPORTED__ERROR__DOCUMENT_LAUNCH_NOT_ALWAYS = 3;
  
  public static final int BUBBLE_DEVELOPER_ERROR_REPORTED__ERROR__UNKNOWN = 0;
  
  public static final int BUILD_INFORMATION = 10044;
  
  public static final int BYTES_TRANSFER_BY_TAG_AND_METERED = 10083;
  
  public static final int CACHED_KILL_REPORTED = 17;
  
  public static final int CAMERA_ACTION_EVENT = 227;
  
  public static final int CAMERA_ACTION_EVENT__FACING__BACK = 1;
  
  public static final int CAMERA_ACTION_EVENT__FACING__EXTERNAL = 3;
  
  public static final int CAMERA_ACTION_EVENT__FACING__FRONT = 2;
  
  public static final int CAMERA_ACTION_EVENT__FACING__UNKNOWN = 0;
  
  public static final int CAMERA_STATE_CHANGED = 25;
  
  public static final int CAMERA_STATE_CHANGED__STATE__OFF = 0;
  
  public static final int CAMERA_STATE_CHANGED__STATE__ON = 1;
  
  public static final int CAMERA_STATE_CHANGED__STATE__RESET = 2;
  
  public static final int CATEGORY_SIZE = 10028;
  
  public static final int CATEGORY_SIZE__CATEGORY__APP_CACHE_SIZE = 3;
  
  public static final int CATEGORY_SIZE__CATEGORY__APP_DATA_SIZE = 2;
  
  public static final int CATEGORY_SIZE__CATEGORY__APP_SIZE = 1;
  
  public static final int CATEGORY_SIZE__CATEGORY__AUDIO = 6;
  
  public static final int CATEGORY_SIZE__CATEGORY__DOWNLOADS = 7;
  
  public static final int CATEGORY_SIZE__CATEGORY__OTHER = 9;
  
  public static final int CATEGORY_SIZE__CATEGORY__PHOTOS = 4;
  
  public static final int CATEGORY_SIZE__CATEGORY__SYSTEM = 8;
  
  public static final int CATEGORY_SIZE__CATEGORY__UNKNOWN = 0;
  
  public static final int CATEGORY_SIZE__CATEGORY__VIDEOS = 5;
  
  public static final int CHARGING_STATE_CHANGED = 31;
  
  public static final int CHARGING_STATE_CHANGED__STATE__BATTERY_STATUS_CHARGING = 2;
  
  public static final int CHARGING_STATE_CHANGED__STATE__BATTERY_STATUS_DISCHARGING = 3;
  
  public static final int CHARGING_STATE_CHANGED__STATE__BATTERY_STATUS_FULL = 5;
  
  public static final int CHARGING_STATE_CHANGED__STATE__BATTERY_STATUS_INVALID = 0;
  
  public static final int CHARGING_STATE_CHANGED__STATE__BATTERY_STATUS_NOT_CHARGING = 4;
  
  public static final int CHARGING_STATE_CHANGED__STATE__BATTERY_STATUS_UNKNOWN = 1;
  
  public static final int CONNECTIVITY_STATE_CHANGED = 98;
  
  public static final int CONNECTIVITY_STATE_CHANGED__STATE__CONNECTED = 1;
  
  public static final int CONNECTIVITY_STATE_CHANGED__STATE__DISCONNECTED = 2;
  
  public static final int CONNECTIVITY_STATE_CHANGED__STATE__UNKNOWN = 0;
  
  public static final int CONTENT_CAPTURE_CALLER_MISMATCH_REPORTED = 206;
  
  public static final int CONTENT_CAPTURE_FLUSHED = 209;
  
  public static final int CONTENT_CAPTURE_SERVICE_EVENTS = 207;
  
  public static final int CONTENT_CAPTURE_SERVICE_EVENTS__EVENT__ACCEPT_DATA_SHARE_REQUEST = 7;
  
  public static final int CONTENT_CAPTURE_SERVICE_EVENTS__EVENT__DATA_SHARE_ERROR_CLIENT_PIPE_FAIL = 12;
  
  public static final int CONTENT_CAPTURE_SERVICE_EVENTS__EVENT__DATA_SHARE_ERROR_CONCURRENT_REQUEST = 14;
  
  public static final int CONTENT_CAPTURE_SERVICE_EVENTS__EVENT__DATA_SHARE_ERROR_EMPTY_DATA = 11;
  
  public static final int CONTENT_CAPTURE_SERVICE_EVENTS__EVENT__DATA_SHARE_ERROR_IOEXCEPTION = 10;
  
  public static final int CONTENT_CAPTURE_SERVICE_EVENTS__EVENT__DATA_SHARE_ERROR_SERVICE_PIPE_FAIL = 13;
  
  public static final int CONTENT_CAPTURE_SERVICE_EVENTS__EVENT__DATA_SHARE_ERROR_TIMEOUT_INTERRUPTED = 15;
  
  public static final int CONTENT_CAPTURE_SERVICE_EVENTS__EVENT__DATA_SHARE_WRITE_FINISHED = 9;
  
  public static final int CONTENT_CAPTURE_SERVICE_EVENTS__EVENT__ON_CONNECTED = 1;
  
  public static final int CONTENT_CAPTURE_SERVICE_EVENTS__EVENT__ON_DATA_SHARE_REQUEST = 6;
  
  public static final int CONTENT_CAPTURE_SERVICE_EVENTS__EVENT__ON_DISCONNECTED = 2;
  
  public static final int CONTENT_CAPTURE_SERVICE_EVENTS__EVENT__ON_USER_DATA_REMOVED = 5;
  
  public static final int CONTENT_CAPTURE_SERVICE_EVENTS__EVENT__REJECT_DATA_SHARE_REQUEST = 8;
  
  public static final int CONTENT_CAPTURE_SERVICE_EVENTS__EVENT__SET_DISABLED = 4;
  
  public static final int CONTENT_CAPTURE_SERVICE_EVENTS__EVENT__SET_WHITELIST = 3;
  
  public static final int CONTENT_CAPTURE_SERVICE_EVENTS__EVENT__UNKNOWN = 0;
  
  public static final int CONTENT_CAPTURE_SESSION_EVENTS = 208;
  
  public static final int CONTENT_CAPTURE_SESSION_EVENTS__EVENT__ON_SESSION_FINISHED = 2;
  
  public static final int CONTENT_CAPTURE_SESSION_EVENTS__EVENT__ON_SESSION_STARTED = 1;
  
  public static final int CONTENT_CAPTURE_SESSION_EVENTS__EVENT__SESSION_NOT_CREATED = 3;
  
  public static final int CONTENT_CAPTURE_SESSION_EVENTS__EVENT__UNKNOWN = 0;
  
  public static final int COOLING_DEVICE = 10059;
  
  public static final int COOLING_DEVICE__DEVICE_LOCATION__BATTERY = 1;
  
  public static final int COOLING_DEVICE__DEVICE_LOCATION__COMPONENT = 6;
  
  public static final int COOLING_DEVICE__DEVICE_LOCATION__CPU = 2;
  
  public static final int COOLING_DEVICE__DEVICE_LOCATION__FAN = 0;
  
  public static final int COOLING_DEVICE__DEVICE_LOCATION__GPU = 3;
  
  public static final int COOLING_DEVICE__DEVICE_LOCATION__MODEM = 4;
  
  public static final int COOLING_DEVICE__DEVICE_LOCATION__NPU = 5;
  
  public static final int CPU_ACTIVE_TIME = 10016;
  
  public static final int CPU_CLUSTER_TIME = 10017;
  
  public static final int CPU_TIME_PER_FREQ = 10008;
  
  public static final int CPU_TIME_PER_THREAD_FREQ = 10037;
  
  public static final int CPU_TIME_PER_UID = 10009;
  
  public static final int CPU_TIME_PER_UID_FREQ = 10010;
  
  public static final int DANGEROUS_PERMISSION_STATE = 10050;
  
  public static final int DANGEROUS_PERMISSION_STATE_SAMPLED = 10067;
  
  public static final int DATA_USAGE_BYTES_TRANSFER = 10082;
  
  public static final int DATA_USAGE_BYTES_TRANSFER__OPPORTUNISTIC_DATA_SUB__ALL = 1;
  
  public static final int DATA_USAGE_BYTES_TRANSFER__OPPORTUNISTIC_DATA_SUB__NOT_OPPORTUNISTIC = 3;
  
  public static final int DATA_USAGE_BYTES_TRANSFER__OPPORTUNISTIC_DATA_SUB__OPPORTUNISTIC = 2;
  
  public static final int DATA_USAGE_BYTES_TRANSFER__OPPORTUNISTIC_DATA_SUB__UNKNOWN = 0;
  
  public static final int DEBUG_ELAPSED_CLOCK = 10046;
  
  public static final int DEBUG_ELAPSED_CLOCK__TYPE__ALWAYS_PRESENT = 1;
  
  public static final int DEBUG_ELAPSED_CLOCK__TYPE__PRESENT_ON_ODD_PULLS = 2;
  
  public static final int DEBUG_ELAPSED_CLOCK__TYPE__TYPE_UNKNOWN = 0;
  
  public static final int DEBUG_FAILING_ELAPSED_CLOCK = 10047;
  
  public static final int DEFERRED_JOB_STATS_REPORTED = 85;
  
  public static final int DEVICE_CALCULATED_POWER_BLAME_OTHER = 10041;
  
  public static final int DEVICE_CALCULATED_POWER_BLAME_OTHER__DRAIN_TYPE__AMBIENT_DISPLAY = 0;
  
  public static final int DEVICE_CALCULATED_POWER_BLAME_OTHER__DRAIN_TYPE__BLUETOOTH = 2;
  
  public static final int DEVICE_CALCULATED_POWER_BLAME_OTHER__DRAIN_TYPE__CAMERA = 3;
  
  public static final int DEVICE_CALCULATED_POWER_BLAME_OTHER__DRAIN_TYPE__CELL = 4;
  
  public static final int DEVICE_CALCULATED_POWER_BLAME_OTHER__DRAIN_TYPE__FLASHLIGHT = 5;
  
  public static final int DEVICE_CALCULATED_POWER_BLAME_OTHER__DRAIN_TYPE__IDLE = 6;
  
  public static final int DEVICE_CALCULATED_POWER_BLAME_OTHER__DRAIN_TYPE__MEMORY = 7;
  
  public static final int DEVICE_CALCULATED_POWER_BLAME_OTHER__DRAIN_TYPE__OVERCOUNTED = 8;
  
  public static final int DEVICE_CALCULATED_POWER_BLAME_OTHER__DRAIN_TYPE__PHONE = 9;
  
  public static final int DEVICE_CALCULATED_POWER_BLAME_OTHER__DRAIN_TYPE__SCREEN = 10;
  
  public static final int DEVICE_CALCULATED_POWER_BLAME_OTHER__DRAIN_TYPE__UNACCOUNTED = 11;
  
  public static final int DEVICE_CALCULATED_POWER_BLAME_OTHER__DRAIN_TYPE__WIFI = 13;
  
  public static final int DEVICE_CALCULATED_POWER_BLAME_UID = 10040;
  
  public static final int DEVICE_CALCULATED_POWER_USE = 10039;
  
  public static final int DEVICE_IDLE_MODE_STATE_CHANGED = 21;
  
  public static final int DEVICE_IDLE_MODE_STATE_CHANGED__STATE__DEVICE_IDLE_MODE_DEEP = 2;
  
  public static final int DEVICE_IDLE_MODE_STATE_CHANGED__STATE__DEVICE_IDLE_MODE_LIGHT = 1;
  
  public static final int DEVICE_IDLE_MODE_STATE_CHANGED__STATE__DEVICE_IDLE_MODE_OFF = 0;
  
  public static final int DEVICE_IDLING_MODE_STATE_CHANGED = 22;
  
  public static final int DEVICE_IDLING_MODE_STATE_CHANGED__STATE__DEVICE_IDLE_MODE_DEEP = 2;
  
  public static final int DEVICE_IDLING_MODE_STATE_CHANGED__STATE__DEVICE_IDLE_MODE_LIGHT = 1;
  
  public static final int DEVICE_IDLING_MODE_STATE_CHANGED__STATE__DEVICE_IDLE_MODE_OFF = 0;
  
  public static final int DEVICE_POLICY_EVENT = 103;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__ADD_CROSS_PROFILE_INTENT_FILTER = 48;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__ADD_CROSS_PROFILE_WIDGET_PROVIDER = 49;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__ADD_PERSISTENT_PREFERRED_ACTIVITY = 52;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__ADD_USER_RESTRICTION = 12;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__ALLOW_MODIFICATION_OF_ADMIN_CONFIGURED_NETWORKS = 132;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__BIND_CROSS_PROFILE_SERVICE = 151;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__CAN_INTERACT_ACROSS_PROFILES_FALSE_NO_PROFILES = 147;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__CAN_INTERACT_ACROSS_PROFILES_FALSE_PERMISSION = 146;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__CAN_INTERACT_ACROSS_PROFILES_TRUE = 145;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__CAN_REQUEST_INTERACT_ACROSS_PROFILES_FALSE_NO_PROFILES = 142;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__CAN_REQUEST_INTERACT_ACROSS_PROFILES_FALSE_PERMISSION = 144;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__CAN_REQUEST_INTERACT_ACROSS_PROFILES_FALSE_WHITELIST = 143;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__CAN_REQUEST_INTERACT_ACROSS_PROFILES_TRUE = 141;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__CHOOSE_PRIVATE_KEY_ALIAS = 22;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__COMP_TO_ORG_OWNED_PO_MIGRATED = 137;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__CREATE_CROSS_PROFILE_INTENT = 148;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__CROSS_PROFILE_APPS_GET_TARGET_USER_PROFILES = 125;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__CROSS_PROFILE_APPS_START_ACTIVITY_AS_USER = 126;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__CROSS_PROFILE_SETTINGS_PAGE_ADMIN_RESTRICTED = 164;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__CROSS_PROFILE_SETTINGS_PAGE_INSTALL_BANNER_CLICKED = 168;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__CROSS_PROFILE_SETTINGS_PAGE_INSTALL_BANNER_NO_INTENT_CLICKED = 169;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__CROSS_PROFILE_SETTINGS_PAGE_LAUNCHED_FROM_APP = 162;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__CROSS_PROFILE_SETTINGS_PAGE_LAUNCHED_FROM_SETTINGS = 163;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__CROSS_PROFILE_SETTINGS_PAGE_MISSING_INSTALL_BANNER_INTENT = 167;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__CROSS_PROFILE_SETTINGS_PAGE_MISSING_PERSONAL_APP = 166;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__CROSS_PROFILE_SETTINGS_PAGE_MISSING_WORK_APP = 165;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__CROSS_PROFILE_SETTINGS_PAGE_PERMISSION_REVOKED = 172;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__CROSS_PROFILE_SETTINGS_PAGE_USER_CONSENTED = 170;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__CROSS_PROFILE_SETTINGS_PAGE_USER_DECLINED_CONSENT = 171;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__DOCSUI_EMPTY_STATE_NO_PERMISSION = 173;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__DOCSUI_EMPTY_STATE_QUIET_MODE = 174;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__DOCSUI_LAUNCH_OTHER_APP = 175;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__DOCSUI_PICK_RESULT = 176;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__DO_USER_INFO_CLICKED = 57;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__ENABLE_SYSTEM_APP = 64;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__ENABLE_SYSTEM_APP_WITH_INTENT = 65;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__ESTABLISH_VPN = 118;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__GENERATE_KEY_PAIR = 59;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__GET_CROSS_PROFILE_PACKAGES = 140;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__GET_USER_PASSWORD_COMPLEXITY_LEVEL = 72;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__GET_WIFI_MAC_ADDRESS = 54;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__INSTALL_CA_CERT = 21;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__INSTALL_EXISTING_PACKAGE = 66;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__INSTALL_KEY_PAIR = 20;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__INSTALL_PACKAGE = 112;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__INSTALL_SYSTEM_UPDATE = 73;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__INSTALL_SYSTEM_UPDATE_ERROR = 74;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__IS_MANAGED_KIOSK = 75;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__IS_MANAGED_PROFILE = 149;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__IS_UNATTENDED_MANAGED_KIOSK = 76;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__LOCK_NOW = 10;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__ON_LOCK_TASK_MODE_ENTERING = 69;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_ACTION = 94;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_CANCELLED = 101;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_COPY_ACCOUNT_STATUS = 103;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_COPY_ACCOUNT_TASK_MS = 96;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_CREATE_PROFILE_TASK_MS = 97;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_DOWNLOAD_PACKAGE_TASK_MS = 99;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_DPC_INSTALLED_BY_PACKAGE = 85;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_DPC_PACKAGE_NAME = 84;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_DPC_SETUP_COMPLETED = 153;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_DPC_SETUP_STARTED = 152;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_ENCRYPT_DEVICE_ACTIVITY_TIME_MS = 88;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_ENTRY_POINT_ADB = 82;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_ENTRY_POINT_CLOUD_ENROLLMENT = 81;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_ENTRY_POINT_NFC = 79;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_ENTRY_POINT_QR_CODE = 80;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_ENTRY_POINT_TRUSTED_SOURCE = 83;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_ERROR = 102;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_EXTRAS = 95;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_FINALIZATION_ACTIVITY_TIME_MS = 92;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_FLOW_TYPE = 124;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_INSTALL_PACKAGE_TASK_MS = 100;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_MANAGED_PROFILE_ON_FULLY_MANAGED_DEVICE = 77;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_NETWORK_TYPE = 93;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_ORGANIZATION_OWNED_MANAGED_PROFILE = 154;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_PERSISTENT_DEVICE_OWNER = 78;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_POST_ENCRYPTION_ACTIVITY_TIME_MS = 91;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_PREPARE_COMPLETED = 123;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_PREPARE_STARTED = 122;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_PREPARE_TOTAL_TIME_MS = 121;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_PREPROVISIONING_ACTIVITY_TIME_MS = 87;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_PROVISIONING_ACTIVITY_TIME_MS = 86;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_SESSION_COMPLETED = 106;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_SESSION_STARTED = 105;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_START_PROFILE_TASK_MS = 98;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_TERMS_ACTIVITY_TIME_MS = 107;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_TERMS_COUNT = 108;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_TERMS_READ = 109;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_TOTAL_TASK_TIME_MS = 104;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_TRAMPOLINE_ACTIVITY_TIME_MS = 90;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__PROVISIONING_WEB_ACTIVITY_TIME_MS = 89;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__QUERY_DETAILS = 33;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__QUERY_SUMMARY = 32;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__QUERY_SUMMARY_FOR_DEVICE = 116;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__QUERY_SUMMARY_FOR_USER = 31;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__REBOOT = 34;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__REMOVE_CROSS_PROFILE_WIDGET_PROVIDER = 117;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__REMOVE_KEY_PAIR = 23;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__REMOVE_USER_RESTRICTION = 13;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__REQUEST_BUGREPORT = 53;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__REQUEST_QUIET_MODE_ENABLED = 55;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__RESOLVER_AUTOLAUNCH_CROSS_PROFILE_TARGET = 161;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__RESOLVER_CROSS_PROFILE_TARGET_OPENED = 155;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__RESOLVER_EMPTY_STATE_NO_APPS_RESOLVED = 160;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__RESOLVER_EMPTY_STATE_NO_SHARING_TO_PERSONAL = 158;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__RESOLVER_EMPTY_STATE_NO_SHARING_TO_WORK = 159;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__RESOLVER_EMPTY_STATE_WORK_APPS_DISABLED = 157;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__RESOLVER_SWITCH_TABS = 156;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__RETRIEVE_NETWORK_LOGS = 120;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__RETRIEVE_PRE_REBOOT_SECURITY_LOGS = 17;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__RETRIEVE_SECURITY_LOGS = 16;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SEPARATE_PROFILE_CHALLENGE_CHANGED = 110;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_ALWAYS_ON_VPN_PACKAGE = 26;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_APPLICATION_HIDDEN = 63;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_APPLICATION_RESTRICTIONS = 62;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_AUTO_TIME = 127;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_AUTO_TIME_REQUIRED = 36;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_AUTO_TIME_ZONE = 128;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_BLUETOOTH_CONTACT_SHARING_DISABLED = 47;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_CAMERA_DISABLED = 30;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_CERT_INSTALLER_PACKAGE = 25;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_COMMON_CRITERIA_MODE = 131;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_CROSS_PROFILE_CALENDAR_PACKAGES = 70;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_CROSS_PROFILE_CALLER_ID_DISABLED = 46;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_CROSS_PROFILE_CONTACTS_SEARCH_DISABLED = 45;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_CROSS_PROFILE_PACKAGES = 138;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_DEVICE_OWNER_LOCK_SCREEN_INFO = 42;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_FACTORY_RESET_PROTECTION = 130;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_GLOBAL_SETTING = 111;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_INTERACT_ACROSS_PROFILES_APP_OP = 139;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_KEEP_UNINSTALLED_PACKAGES = 61;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_KEYGUARD_DISABLED = 37;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_KEYGUARD_DISABLED_FEATURES = 9;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_KEY_PAIR_CERTIFICATE = 60;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_LOCKTASK_MODE_ENABLED = 51;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_LONG_SUPPORT_MESSAGE = 44;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_MANAGED_PROFILE_MAXIMUM_TIME_OFF = 136;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_MASTER_VOLUME_MUTED = 35;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_NETWORK_LOGGING_ENABLED = 119;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_ORGANIZATION_COLOR = 39;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_PACKAGES_SUSPENDED = 68;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_PASSWORD_MINIMUM_LENGTH = 2;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_PASSWORD_MINIMUM_LETTERS = 5;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_PASSWORD_MINIMUM_LOWER_CASE = 6;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_PASSWORD_MINIMUM_NON_LETTER = 4;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_PASSWORD_MINIMUM_NUMERIC = 3;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_PASSWORD_MINIMUM_SYMBOLS = 8;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_PASSWORD_MINIMUM_UPPER_CASE = 7;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_PASSWORD_QUALITY = 1;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_PERMISSION_GRANT_STATE = 19;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_PERMISSION_POLICY = 18;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_PERMITTED_ACCESSIBILITY_SERVICES = 28;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_PERMITTED_INPUT_METHODS = 27;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_PERSONAL_APPS_SUSPENDED = 135;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_PROFILE_NAME = 40;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_SCREEN_CAPTURE_DISABLED = 29;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_SECURE_SETTING = 14;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_SECURITY_LOGGING_ENABLED = 15;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_SHORT_SUPPORT_MESSAGE = 43;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_STATUS_BAR_DISABLED = 38;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_SYSTEM_UPDATE_POLICY = 50;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_TIME = 133;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_TIME_ZONE = 134;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_UNINSTALL_BLOCKED = 67;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_USER_CONTROL_DISABLED_PACKAGES = 129;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__SET_USER_ICON = 41;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__START_ACTIVITY_BY_INTENT = 150;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__TRANSFER_OWNERSHIP = 58;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__UNINSTALL_CA_CERTS = 24;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__UNINSTALL_PACKAGE = 113;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__WIFI_SERVICE_ADD_NETWORK_SUGGESTIONS = 114;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__WIFI_SERVICE_ADD_OR_UPDATE_NETWORK = 115;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__WIPE_DATA_WITH_REASON = 11;
  
  public static final int DEVICE_POLICY_EVENT__EVENT_ID__WORK_PROFILE_LOCATION_CHANGED = 56;
  
  public static final int DIRECTORY_USAGE = 10026;
  
  public static final int DIRECTORY_USAGE__DIRECTORY__CACHE = 2;
  
  public static final int DIRECTORY_USAGE__DIRECTORY__DATA = 1;
  
  public static final int DIRECTORY_USAGE__DIRECTORY__SYSTEM = 3;
  
  public static final int DIRECTORY_USAGE__DIRECTORY__UNKNOWN = 0;
  
  public static final int DISK_IO = 10032;
  
  public static final int DISK_STATS = 10025;
  
  public static final int DISPLAY_WAKE_REPORTED = 282;
  
  public static final int DNDMODE_PROTO__ZEN_MODE__ROOT_CONFIG = -1;
  
  public static final int DNDMODE_PROTO__ZEN_MODE__ZEN_MODE_ALARMS = 3;
  
  public static final int DNDMODE_PROTO__ZEN_MODE__ZEN_MODE_IMPORTANT_INTERRUPTIONS = 1;
  
  public static final int DNDMODE_PROTO__ZEN_MODE__ZEN_MODE_NO_INTERRUPTIONS = 2;
  
  public static final int DNDMODE_PROTO__ZEN_MODE__ZEN_MODE_OFF = 0;
  
  public static final int DND_MODE_RULE = 10084;
  
  public static final int EXCESSIVE_CPU_USAGE_REPORTED = 16;
  
  public static final int EXCLUSION_RECT_STATE_CHANGED = 223;
  
  public static final int EXCLUSION_RECT_STATE_CHANGED__X_LOCATION__DEFAULT_LOCATION = 0;
  
  public static final int EXCLUSION_RECT_STATE_CHANGED__X_LOCATION__LEFT = 1;
  
  public static final int EXCLUSION_RECT_STATE_CHANGED__X_LOCATION__RIGHT = 2;
  
  public static final int EXTERNAL_STORAGE_INFO = 10053;
  
  public static final int EXTERNAL_STORAGE_INFO__STORAGE_TYPE__OTHER = 3;
  
  public static final int EXTERNAL_STORAGE_INFO__STORAGE_TYPE__SD_CARD = 1;
  
  public static final int EXTERNAL_STORAGE_INFO__STORAGE_TYPE__UNKNOWN = 0;
  
  public static final int EXTERNAL_STORAGE_INFO__STORAGE_TYPE__USB = 2;
  
  public static final int EXTERNAL_STORAGE_INFO__VOLUME_TYPE__OTHER = 3;
  
  public static final int EXTERNAL_STORAGE_INFO__VOLUME_TYPE__PRIVATE = 2;
  
  public static final int EXTERNAL_STORAGE_INFO__VOLUME_TYPE__PUBLIC = 1;
  
  public static final int EXTERNAL_STORAGE_INFO__VOLUME_TYPE__UNKNOWN = 0;
  
  public static final int FACE_SETTINGS = 10058;
  
  public static final int FLAG_FLIP_UPDATE_OCCURRED = 101;
  
  public static final int FLASHLIGHT_STATE_CHANGED = 26;
  
  public static final int FLASHLIGHT_STATE_CHANGED__STATE__OFF = 0;
  
  public static final int FLASHLIGHT_STATE_CHANGED__STATE__ON = 1;
  
  public static final int FLASHLIGHT_STATE_CHANGED__STATE__RESET = 2;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED = 256;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_MODE__MODE_ALLOWED = 1;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_MODE__MODE_FOREGROUND = 3;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_MODE__MODE_IGNORED = 2;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_MODE__MODE_UNKNOWN = 0;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_ACCEPT_HANDOVER = 74;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_ACCESS_ACCESSIBILITY = 88;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_ACCESS_MEDIA_LOCATION = 90;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_ACCESS_NOTIFICATIONS = 25;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_ACTIVATE_PLATFORM_VPN = 94;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_ACTIVATE_VPN = 47;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_ACTIVITY_RECOGNITION = 79;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_ADD_VOICEMAIL = 52;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_ANSWER_PHONE_CALLS = 69;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_ASSIST_SCREENSHOT = 50;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_ASSIST_STRUCTURE = 49;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_AUDIO_ACCESSIBILITY_VOLUME = 64;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_AUDIO_ALARM_VOLUME = 37;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_AUDIO_BLUETOOTH_VOLUME = 39;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_AUDIO_MASTER_VOLUME = 33;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_AUDIO_MEDIA_VOLUME = 36;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_AUDIO_NOTIFICATION_VOLUME = 38;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_AUDIO_RING_VOLUME = 35;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_AUDIO_VOICE_VOLUME = 34;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_AUTO_REVOKE_MANAGED_BY_INSTALLER = 98;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_AUTO_REVOKE_PERMISSIONS_IF_UNUSED = 97;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_BIND_ACCESSIBILITY_SERVICE = 73;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_BLUETOOTH_SCAN = 77;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_BODY_SENSORS = 56;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_CALL_PHONE = 13;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_CAMERA = 26;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_CHANGE_WIFI_STATE = 71;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_COARSE_LOCATION = 0;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_DEPRECATED_1 = 96;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_FINE_LOCATION = 1;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_GET_ACCOUNTS = 62;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_GET_USAGE_STATS = 43;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_GPS = 2;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_INSTANT_APP_START_FOREGROUND = 68;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_INTERACT_ACROSS_PROFILES = 93;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_LEGACY_STORAGE = 87;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_LOADER_USAGE_STATS = 95;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_MANAGE_EXTERNAL_STORAGE = 92;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_MANAGE_IPSEC_TUNNELS = 75;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_MOCK_LOCATION = 58;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_MONITOR_HIGH_POWER_LOCATION = 42;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_MONITOR_LOCATION = 41;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_MUTE_MICROPHONE = 44;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_NEIGHBORING_CELLS = 12;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_NONE = -1;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_NO_ISOLATED_STORAGE = 99;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_PICTURE_IN_PICTURE = 67;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_PLAY_AUDIO = 28;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_POST_NOTIFICATION = 11;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_PROCESS_OUTGOING_CALLS = 54;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_PROJECT_MEDIA = 46;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_QUERY_ALL_PACKAGES = 91;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_READ_CALENDAR = 8;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_READ_CALL_LOG = 6;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_READ_CELL_BROADCASTS = 57;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_READ_CLIPBOARD = 29;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_READ_CONTACTS = 4;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_READ_DEVICE_IDENTIFIERS = 89;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_READ_EXTERNAL_STORAGE = 59;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_READ_ICC_SMS = 21;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_READ_MEDIA_AUDIO = 81;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_READ_MEDIA_IMAGES = 85;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_READ_MEDIA_VIDEO = 83;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_READ_PHONE_NUMBERS = 65;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_READ_PHONE_STATE = 51;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_READ_SMS = 14;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_RECEIVE_EMERGENCY_SMS = 17;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_RECEIVE_MMS = 18;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_RECEIVE_SMS = 16;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_RECEIVE_WAP_PUSH = 19;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_RECORD_AUDIO = 27;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_REQUEST_DELETE_PACKAGES = 72;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_REQUEST_INSTALL_PACKAGES = 66;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_RUN_ANY_IN_BACKGROUND = 70;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_RUN_IN_BACKGROUND = 63;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_SEND_SMS = 20;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_SMS_FINANCIAL_TRANSACTIONS = 80;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_START_FOREGROUND = 76;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_SYSTEM_ALERT_WINDOW = 24;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_TAKE_AUDIO_FOCUS = 32;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_TAKE_MEDIA_BUTTONS = 31;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_TOAST_WINDOW = 45;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_TURN_SCREEN_ON = 61;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_USE_BIOMETRIC = 78;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_USE_FINGERPRINT = 55;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_USE_SIP = 53;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_VIBRATE = 3;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_WAKE_LOCK = 40;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_WIFI_SCAN = 10;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_WRITE_CALENDAR = 9;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_WRITE_CALL_LOG = 7;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_WRITE_CLIPBOARD = 30;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_WRITE_CONTACTS = 5;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_WRITE_EXTERNAL_STORAGE = 60;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_WRITE_ICC_SMS = 22;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_WRITE_MEDIA_AUDIO = 82;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_WRITE_MEDIA_IMAGES = 86;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_WRITE_MEDIA_VIDEO = 84;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_WRITE_SETTINGS = 23;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_WRITE_SMS = 15;
  
  public static final int FOREGROUND_SERVICE_APP_OP_SESSION_ENDED__APP_OP_NAME__APP_OP_WRITE_WALLPAPER = 48;
  
  public static final int FOREGROUND_SERVICE_STATE_CHANGED = 60;
  
  public static final int FOREGROUND_SERVICE_STATE_CHANGED__STATE__ENTER = 1;
  
  public static final int FOREGROUND_SERVICE_STATE_CHANGED__STATE__EXIT = 2;
  
  public static final int FULL_BATTERY_CAPACITY = 10020;
  
  public static final int GNSS_CONFIGURATION_REPORTED = 132;
  
  public static final int GNSS_CONFIGURATION_REPORTED__A_GLONASS_POS_PROTOCOL_SELECT__LPP_UPLANE = 4;
  
  public static final int GNSS_CONFIGURATION_REPORTED__A_GLONASS_POS_PROTOCOL_SELECT__RRC_CPLANE = 1;
  
  public static final int GNSS_CONFIGURATION_REPORTED__A_GLONASS_POS_PROTOCOL_SELECT__RRLP_CPLANE = 2;
  
  public static final int GNSS_CONFIGURATION_REPORTED__GPS_LOCK__MO = 1;
  
  public static final int GNSS_CONFIGURATION_REPORTED__GPS_LOCK__NI = 2;
  
  public static final int GNSS_CONFIGURATION_REPORTED__LPP_PROFILE__CONTROL_PLANE = 2;
  
  public static final int GNSS_CONFIGURATION_REPORTED__LPP_PROFILE__USER_PLANE = 1;
  
  public static final int GNSS_CONFIGURATION_REPORTED__SUPL_MODE__MSA = 2;
  
  public static final int GNSS_CONFIGURATION_REPORTED__SUPL_MODE__MSB = 1;
  
  public static final int GNSS_NFW_NOTIFICATION_REPORTED = 131;
  
  public static final int GNSS_NFW_NOTIFICATION_REPORTED__PROTOCOL_STACK__CTRL_PLANE = 0;
  
  public static final int GNSS_NFW_NOTIFICATION_REPORTED__PROTOCOL_STACK__IMS = 10;
  
  public static final int GNSS_NFW_NOTIFICATION_REPORTED__PROTOCOL_STACK__OTHER_PROTOCOL_STACK = 100;
  
  public static final int GNSS_NFW_NOTIFICATION_REPORTED__PROTOCOL_STACK__SIM = 11;
  
  public static final int GNSS_NFW_NOTIFICATION_REPORTED__PROTOCOL_STACK__SUPL = 1;
  
  public static final int GNSS_NFW_NOTIFICATION_REPORTED__REQUESTOR__AUTOMOBILE_CLIENT = 20;
  
  public static final int GNSS_NFW_NOTIFICATION_REPORTED__REQUESTOR__CARRIER = 0;
  
  public static final int GNSS_NFW_NOTIFICATION_REPORTED__REQUESTOR__GNSS_CHIPSET_VENDOR = 12;
  
  public static final int GNSS_NFW_NOTIFICATION_REPORTED__REQUESTOR__MODEM_CHIPSET_VENDOR = 11;
  
  public static final int GNSS_NFW_NOTIFICATION_REPORTED__REQUESTOR__OEM = 10;
  
  public static final int GNSS_NFW_NOTIFICATION_REPORTED__REQUESTOR__OTHER_CHIPSET_VENDOR = 13;
  
  public static final int GNSS_NFW_NOTIFICATION_REPORTED__REQUESTOR__OTHER_REQUESTOR = 100;
  
  public static final int GNSS_NFW_NOTIFICATION_REPORTED__RESPONSE_TYPE__ACCEPTED_LOCATION_PROVIDED = 2;
  
  public static final int GNSS_NFW_NOTIFICATION_REPORTED__RESPONSE_TYPE__ACCEPTED_NO_LOCATION_PROVIDED = 1;
  
  public static final int GNSS_NFW_NOTIFICATION_REPORTED__RESPONSE_TYPE__REJECTED = 0;
  
  public static final int GNSS_NI_EVENT_REPORTED = 124;
  
  public static final int GNSS_NI_EVENT_REPORTED__DEFAULT_RESPONSE__RESPONSE_ACCEPT = 1;
  
  public static final int GNSS_NI_EVENT_REPORTED__DEFAULT_RESPONSE__RESPONSE_DENY = 2;
  
  public static final int GNSS_NI_EVENT_REPORTED__DEFAULT_RESPONSE__RESPONSE_NORESP = 3;
  
  public static final int GNSS_NI_EVENT_REPORTED__EVENT_TYPE__NI_REQUEST = 1;
  
  public static final int GNSS_NI_EVENT_REPORTED__EVENT_TYPE__NI_RESPONSE = 2;
  
  public static final int GNSS_NI_EVENT_REPORTED__EVENT_TYPE__UNKNOWN = 0;
  
  public static final int GNSS_NI_EVENT_REPORTED__NI_TYPE__EMERGENCY_SUPL = 4;
  
  public static final int GNSS_NI_EVENT_REPORTED__NI_TYPE__UMTS_CTRL_PLANE = 3;
  
  public static final int GNSS_NI_EVENT_REPORTED__NI_TYPE__UMTS_SUPL = 2;
  
  public static final int GNSS_NI_EVENT_REPORTED__NI_TYPE__VOICE = 1;
  
  public static final int GNSS_NI_EVENT_REPORTED__REQUESTOR_ID_ENCODING__ENC_NONE = 0;
  
  public static final int GNSS_NI_EVENT_REPORTED__REQUESTOR_ID_ENCODING__ENC_SUPL_GSM_DEFAULT = 1;
  
  public static final int GNSS_NI_EVENT_REPORTED__REQUESTOR_ID_ENCODING__ENC_SUPL_UCS2 = 3;
  
  public static final int GNSS_NI_EVENT_REPORTED__REQUESTOR_ID_ENCODING__ENC_SUPL_UTF8 = 2;
  
  public static final int GNSS_NI_EVENT_REPORTED__REQUESTOR_ID_ENCODING__ENC_UNKNOWN = -1;
  
  public static final int GNSS_NI_EVENT_REPORTED__TEXT_ENCODING__ENC_NONE = 0;
  
  public static final int GNSS_NI_EVENT_REPORTED__TEXT_ENCODING__ENC_SUPL_GSM_DEFAULT = 1;
  
  public static final int GNSS_NI_EVENT_REPORTED__TEXT_ENCODING__ENC_SUPL_UCS2 = 3;
  
  public static final int GNSS_NI_EVENT_REPORTED__TEXT_ENCODING__ENC_SUPL_UTF8 = 2;
  
  public static final int GNSS_NI_EVENT_REPORTED__TEXT_ENCODING__ENC_UNKNOWN = -1;
  
  public static final int GNSS_NI_EVENT_REPORTED__USER_RESPONSE__RESPONSE_ACCEPT = 1;
  
  public static final int GNSS_NI_EVENT_REPORTED__USER_RESPONSE__RESPONSE_DENY = 2;
  
  public static final int GNSS_NI_EVENT_REPORTED__USER_RESPONSE__RESPONSE_NORESP = 3;
  
  public static final int GNSS_STATS = 10074;
  
  public static final int GPS_SCAN_STATE_CHANGED = 6;
  
  public static final int GPS_SCAN_STATE_CHANGED__STATE__OFF = 0;
  
  public static final int GPS_SCAN_STATE_CHANGED__STATE__ON = 1;
  
  public static final int GPS_SIGNAL_QUALITY_CHANGED = 69;
  
  public static final int GPS_SIGNAL_QUALITY_CHANGED__LEVEL__GPS_SIGNAL_QUALITY_GOOD = 1;
  
  public static final int GPS_SIGNAL_QUALITY_CHANGED__LEVEL__GPS_SIGNAL_QUALITY_POOR = 0;
  
  public static final int GPS_SIGNAL_QUALITY_CHANGED__LEVEL__GPS_SIGNAL_QUALITY_UNKNOWN = -1;
  
  public static final int HIDDEN_API_USED = 178;
  
  public static final int HIDDEN_API_USED__ACCESS_METHOD__JNI = 2;
  
  public static final int HIDDEN_API_USED__ACCESS_METHOD__LINKING = 3;
  
  public static final int HIDDEN_API_USED__ACCESS_METHOD__NONE = 0;
  
  public static final int HIDDEN_API_USED__ACCESS_METHOD__REFLECTION = 1;
  
  public static final int INTEGRITY_CHECK_RESULT_REPORTED = 247;
  
  public static final int INTEGRITY_CHECK_RESULT_REPORTED__RESPONSE__ALLOWED = 1;
  
  public static final int INTEGRITY_CHECK_RESULT_REPORTED__RESPONSE__FORCE_ALLOWED = 3;
  
  public static final int INTEGRITY_CHECK_RESULT_REPORTED__RESPONSE__REJECTED = 2;
  
  public static final int INTEGRITY_CHECK_RESULT_REPORTED__RESPONSE__UNKNOWN = 0;
  
  public static final int INTEGRITY_RULES_PUSHED = 248;
  
  public static final int INTERACTIVE_STATE_CHANGED = 33;
  
  public static final int INTERACTIVE_STATE_CHANGED__STATE__OFF = 0;
  
  public static final int INTERACTIVE_STATE_CHANGED__STATE__ON = 1;
  
  public static final int ION_HEAP_SIZE = 10070;
  
  public static final int ISOLATED_UID_CHANGED = 43;
  
  public static final int ISOLATED_UID_CHANGED__EVENT__CREATED = 1;
  
  public static final int ISOLATED_UID_CHANGED__EVENT__REMOVED = 0;
  
  public static final int KERNEL_WAKELOCK = 10004;
  
  public static final int KERNEL_WAKEUP_REPORTED = 36;
  
  public static final int KEY_VALUE_PAIRS_ATOM = 83;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED = 210;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__ACTIVIY_IMPORTANCE__IMPORTANCE_BACKGROUND = 3;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__ACTIVIY_IMPORTANCE__IMPORTANCE_FORGROUND_SERVICE = 2;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__ACTIVIY_IMPORTANCE__IMPORTANCE_TOP = 1;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__ACTIVIY_IMPORTANCE__IMPORTANCE_UNKNOWN = 0;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__API_IN_USE__API_ADD_GNSS_MEASUREMENTS_LISTENER = 2;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__API_IN_USE__API_REGISTER_GNSS_STATUS_CALLBACK = 3;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__API_IN_USE__API_REQUEST_GEOFENCE = 4;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__API_IN_USE__API_REQUEST_LOCATION_UPDATES = 1;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__API_IN_USE__API_SEND_EXTRA_COMMAND = 5;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__API_IN_USE__API_UNKNOWN = 0;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__BUCKETIZED_EXPIRE_IN__EXPIRATION_BETWEEN_0_AND_20_SEC = 1;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__BUCKETIZED_EXPIRE_IN__EXPIRATION_BETWEEN_10_MIN_AND_1_HOUR = 4;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__BUCKETIZED_EXPIRE_IN__EXPIRATION_BETWEEN_1_MIN_AND_10_MIN = 3;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__BUCKETIZED_EXPIRE_IN__EXPIRATION_BETWEEN_20_SEC_AND_1_MIN = 2;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__BUCKETIZED_EXPIRE_IN__EXPIRATION_LARGER_THAN_1_HOUR = 5;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__BUCKETIZED_EXPIRE_IN__EXPIRATION_NO_EXPIRY = 6;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__BUCKETIZED_EXPIRE_IN__EXPIRATION_UNKNOWN = 0;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__BUCKETIZED_INTERVAL__INTERVAL_BETWEEN_0_SEC_AND_1_SEC = 1;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__BUCKETIZED_INTERVAL__INTERVAL_BETWEEN_10_MIN_AND_1_HOUR = 5;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__BUCKETIZED_INTERVAL__INTERVAL_BETWEEN_1_MIN_AND_10_MIN = 4;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__BUCKETIZED_INTERVAL__INTERVAL_BETWEEN_1_SEC_AND_5_SEC = 2;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__BUCKETIZED_INTERVAL__INTERVAL_BETWEEN_5_SEC_AND_1_MIN = 3;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__BUCKETIZED_INTERVAL__INTERVAL_LARGER_THAN_1_HOUR = 6;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__BUCKETIZED_INTERVAL__INTERVAL_UNKNOWN = 0;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__BUCKETIZED_RADIUS__RADIUS_BETWEEN_0_AND_100 = 1;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__BUCKETIZED_RADIUS__RADIUS_BETWEEN_1000_AND_10000 = 5;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__BUCKETIZED_RADIUS__RADIUS_BETWEEN_100_AND_200 = 2;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__BUCKETIZED_RADIUS__RADIUS_BETWEEN_200_AND_300 = 3;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__BUCKETIZED_RADIUS__RADIUS_BETWEEN_300_AND_1000 = 4;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__BUCKETIZED_RADIUS__RADIUS_LARGER_THAN_100000 = 6;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__BUCKETIZED_RADIUS__RADIUS_NEGATIVE = 7;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__BUCKETIZED_RADIUS__RADIUS_UNKNOWN = 0;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__BUCKETIZED_SMALLEST_DISPLACEMENT__DISTANCE_BETWEEN_0_AND_100 = 2;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__BUCKETIZED_SMALLEST_DISPLACEMENT__DISTANCE_LARGER_THAN_100 = 3;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__BUCKETIZED_SMALLEST_DISPLACEMENT__DISTANCE_UNKNOWN = 0;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__BUCKETIZED_SMALLEST_DISPLACEMENT__DISTANCE_ZERO = 1;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__CALLBACK_TYPE__CALLBACK_LISTENER = 2;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__CALLBACK_TYPE__CALLBACK_NOT_APPLICABLE = 1;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__CALLBACK_TYPE__CALLBACK_PENDING_INTENT = 3;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__CALLBACK_TYPE__CALLBACK_UNKNOWN = 0;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__PROVIDER__PROVIDER_FUSED = 4;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__PROVIDER__PROVIDER_GPS = 2;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__PROVIDER__PROVIDER_NETWORK = 1;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__PROVIDER__PROVIDER_PASSIVE = 3;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__PROVIDER__PROVIDER_UNKNOWN = 0;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__QUALITY__ACCURACY_BLOCK = 102;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__QUALITY__ACCURACY_CITY = 104;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__QUALITY__ACCURACY_FINE = 100;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__QUALITY__POWER_HIGH = 203;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__QUALITY__POWER_LOW = 201;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__QUALITY__POWER_NONE = 200;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__QUALITY__QUALITY_UNKNOWN = 0;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__STATE__USAGE_ENDED = 1;
  
  public static final int LOCATION_MANAGER_API_USAGE_REPORTED__STATE__USAGE_STARTED = 0;
  
  public static final int LONG_PARTIAL_WAKELOCK_STATE_CHANGED = 11;
  
  public static final int LONG_PARTIAL_WAKELOCK_STATE_CHANGED__STATE__OFF = 0;
  
  public static final int LONG_PARTIAL_WAKELOCK_STATE_CHANGED__STATE__ON = 1;
  
  public static final int LOOPER_STATS = 10024;
  
  public static final int LOW_MEM_REPORTED = 81;
  
  public static final int LOW_STORAGE_STATE_CHANGED = 130;
  
  public static final int LOW_STORAGE_STATE_CHANGED__STATE__OFF = 1;
  
  public static final int LOW_STORAGE_STATE_CHANGED__STATE__ON = 2;
  
  public static final int LOW_STORAGE_STATE_CHANGED__STATE__UNKNOWN = 0;
  
  public static final int MEDIA_CODEC_STATE_CHANGED = 24;
  
  public static final int MEDIA_CODEC_STATE_CHANGED__STATE__OFF = 0;
  
  public static final int MEDIA_CODEC_STATE_CHANGED__STATE__ON = 1;
  
  public static final int MEDIA_CODEC_STATE_CHANGED__STATE__RESET = 2;
  
  public static final int MEMORY_FACTOR_STATE_CHANGED = 15;
  
  public static final int MEMORY_FACTOR_STATE_CHANGED__FACTOR__CRITICAL = 4;
  
  public static final int MEMORY_FACTOR_STATE_CHANGED__FACTOR__LOW = 3;
  
  public static final int MEMORY_FACTOR_STATE_CHANGED__FACTOR__MEMORY_UNKNOWN = 0;
  
  public static final int MEMORY_FACTOR_STATE_CHANGED__FACTOR__MODERATE = 2;
  
  public static final int MEMORY_FACTOR_STATE_CHANGED__FACTOR__NORMAL = 1;
  
  public static final int MOBILE_BYTES_TRANSFER = 10002;
  
  public static final int MOBILE_BYTES_TRANSFER_BY_FG_BG = 10003;
  
  public static final int MOBILE_RADIO_POWER_STATE_CHANGED = 12;
  
  public static final int MOBILE_RADIO_POWER_STATE_CHANGED__STATE__DATA_CONNECTION_POWER_STATE_HIGH = 3;
  
  public static final int MOBILE_RADIO_POWER_STATE_CHANGED__STATE__DATA_CONNECTION_POWER_STATE_LOW = 1;
  
  public static final int MOBILE_RADIO_POWER_STATE_CHANGED__STATE__DATA_CONNECTION_POWER_STATE_MEDIUM = 2;
  
  public static final int MOBILE_RADIO_POWER_STATE_CHANGED__STATE__DATA_CONNECTION_POWER_STATE_UNKNOWN = 2147483647;
  
  public static final int MODEM_ACTIVITY_INFO = 10012;
  
  public static final int NOTIFICATION_CHANNEL_MODIFIED = 246;
  
  public static final int NOTIFICATION_CHANNEL_MODIFIED__IMPORTANCE__IMPORTANCE_DEFAULT = 3;
  
  public static final int NOTIFICATION_CHANNEL_MODIFIED__IMPORTANCE__IMPORTANCE_HIGH = 4;
  
  public static final int NOTIFICATION_CHANNEL_MODIFIED__IMPORTANCE__IMPORTANCE_IMPORTANT_CONVERSATION = 5;
  
  public static final int NOTIFICATION_CHANNEL_MODIFIED__IMPORTANCE__IMPORTANCE_LOW = 2;
  
  public static final int NOTIFICATION_CHANNEL_MODIFIED__IMPORTANCE__IMPORTANCE_MIN = 1;
  
  public static final int NOTIFICATION_CHANNEL_MODIFIED__IMPORTANCE__IMPORTANCE_NONE = 0;
  
  public static final int NOTIFICATION_CHANNEL_MODIFIED__IMPORTANCE__IMPORTANCE_UNSPECIFIED = -1000;
  
  public static final int NOTIFICATION_CHANNEL_MODIFIED__OLD_IMPORTANCE__IMPORTANCE_DEFAULT = 3;
  
  public static final int NOTIFICATION_CHANNEL_MODIFIED__OLD_IMPORTANCE__IMPORTANCE_HIGH = 4;
  
  public static final int NOTIFICATION_CHANNEL_MODIFIED__OLD_IMPORTANCE__IMPORTANCE_IMPORTANT_CONVERSATION = 5;
  
  public static final int NOTIFICATION_CHANNEL_MODIFIED__OLD_IMPORTANCE__IMPORTANCE_LOW = 2;
  
  public static final int NOTIFICATION_CHANNEL_MODIFIED__OLD_IMPORTANCE__IMPORTANCE_MIN = 1;
  
  public static final int NOTIFICATION_CHANNEL_MODIFIED__OLD_IMPORTANCE__IMPORTANCE_NONE = 0;
  
  public static final int NOTIFICATION_CHANNEL_MODIFIED__OLD_IMPORTANCE__IMPORTANCE_UNSPECIFIED = -1000;
  
  public static final int NOTIFICATION_REMOTE_VIEWS = 10066;
  
  public static final int NOTIFICATION_REPORTED = 244;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE_ASST__IMPORTANCE_DEFAULT = 3;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE_ASST__IMPORTANCE_HIGH = 4;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE_ASST__IMPORTANCE_IMPORTANT_CONVERSATION = 5;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE_ASST__IMPORTANCE_LOW = 2;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE_ASST__IMPORTANCE_MIN = 1;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE_ASST__IMPORTANCE_NONE = 0;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE_ASST__IMPORTANCE_UNSPECIFIED = -1000;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE_INITIAL_SOURCE__IMPORTANCE_EXPLANATION_APP = 1;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE_INITIAL_SOURCE__IMPORTANCE_EXPLANATION_APP_PRE_CHANNELS = 5;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE_INITIAL_SOURCE__IMPORTANCE_EXPLANATION_ASST = 3;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE_INITIAL_SOURCE__IMPORTANCE_EXPLANATION_SYSTEM = 4;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE_INITIAL_SOURCE__IMPORTANCE_EXPLANATION_UNKNOWN = 0;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE_INITIAL_SOURCE__IMPORTANCE_EXPLANATION_USER = 2;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE_INITIAL__IMPORTANCE_DEFAULT = 3;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE_INITIAL__IMPORTANCE_HIGH = 4;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE_INITIAL__IMPORTANCE_IMPORTANT_CONVERSATION = 5;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE_INITIAL__IMPORTANCE_LOW = 2;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE_INITIAL__IMPORTANCE_MIN = 1;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE_INITIAL__IMPORTANCE_NONE = 0;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE_INITIAL__IMPORTANCE_UNSPECIFIED = -1000;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE_SOURCE__IMPORTANCE_EXPLANATION_APP = 1;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE_SOURCE__IMPORTANCE_EXPLANATION_APP_PRE_CHANNELS = 5;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE_SOURCE__IMPORTANCE_EXPLANATION_ASST = 3;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE_SOURCE__IMPORTANCE_EXPLANATION_SYSTEM = 4;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE_SOURCE__IMPORTANCE_EXPLANATION_UNKNOWN = 0;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE_SOURCE__IMPORTANCE_EXPLANATION_USER = 2;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE__IMPORTANCE_DEFAULT = 3;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE__IMPORTANCE_HIGH = 4;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE__IMPORTANCE_IMPORTANT_CONVERSATION = 5;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE__IMPORTANCE_LOW = 2;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE__IMPORTANCE_MIN = 1;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE__IMPORTANCE_NONE = 0;
  
  public static final int NOTIFICATION_REPORTED__IMPORTANCE__IMPORTANCE_UNSPECIFIED = -1000;
  
  public static final int NUM_FACES_ENROLLED = 10048;
  
  public static final int NUM_FINGERPRINTS_ENROLLED = 10031;
  
  public static final int OVERLAY_STATE_CHANGED = 59;
  
  public static final int OVERLAY_STATE_CHANGED__STATE__ENTERED = 1;
  
  public static final int OVERLAY_STATE_CHANGED__STATE__EXITED = 2;
  
  public static final int PACKAGE_INSTALLER_V2_REPORTED = 263;
  
  public static final int PACKAGE_NOTIFICATION_CHANNEL_GROUP_PREFERENCES = 10073;
  
  public static final int PACKAGE_NOTIFICATION_CHANNEL_PREFERENCES = 10072;
  
  public static final int PACKAGE_NOTIFICATION_PREFERENCES = 10071;
  
  public static final int PACKET_WAKEUP_OCCURRED = 44;
  
  public static final int PHONE_SERVICE_STATE_CHANGED = 94;
  
  public static final int PHONE_SERVICE_STATE_CHANGED__SIGNAL_STRENGTH__SIGNAL_STRENGTH_GOOD = 3;
  
  public static final int PHONE_SERVICE_STATE_CHANGED__SIGNAL_STRENGTH__SIGNAL_STRENGTH_GREAT = 4;
  
  public static final int PHONE_SERVICE_STATE_CHANGED__SIGNAL_STRENGTH__SIGNAL_STRENGTH_MODERATE = 2;
  
  public static final int PHONE_SERVICE_STATE_CHANGED__SIGNAL_STRENGTH__SIGNAL_STRENGTH_NONE_OR_UNKNOWN = 0;
  
  public static final int PHONE_SERVICE_STATE_CHANGED__SIGNAL_STRENGTH__SIGNAL_STRENGTH_POOR = 1;
  
  public static final int PHONE_SERVICE_STATE_CHANGED__SIM_STATE__SIM_STATE_ABSENT = 1;
  
  public static final int PHONE_SERVICE_STATE_CHANGED__SIM_STATE__SIM_STATE_CARD_IO_ERROR = 8;
  
  public static final int PHONE_SERVICE_STATE_CHANGED__SIM_STATE__SIM_STATE_CARD_RESTRICTED = 9;
  
  public static final int PHONE_SERVICE_STATE_CHANGED__SIM_STATE__SIM_STATE_LOADED = 10;
  
  public static final int PHONE_SERVICE_STATE_CHANGED__SIM_STATE__SIM_STATE_NETWORK_LOCKED = 4;
  
  public static final int PHONE_SERVICE_STATE_CHANGED__SIM_STATE__SIM_STATE_NOT_READY = 6;
  
  public static final int PHONE_SERVICE_STATE_CHANGED__SIM_STATE__SIM_STATE_PERM_DISABLED = 7;
  
  public static final int PHONE_SERVICE_STATE_CHANGED__SIM_STATE__SIM_STATE_PIN_REQUIRED = 2;
  
  public static final int PHONE_SERVICE_STATE_CHANGED__SIM_STATE__SIM_STATE_PRESENT = 11;
  
  public static final int PHONE_SERVICE_STATE_CHANGED__SIM_STATE__SIM_STATE_PUK_REQUIRED = 3;
  
  public static final int PHONE_SERVICE_STATE_CHANGED__SIM_STATE__SIM_STATE_READY = 5;
  
  public static final int PHONE_SERVICE_STATE_CHANGED__SIM_STATE__SIM_STATE_UNKNOWN = 0;
  
  public static final int PHONE_SERVICE_STATE_CHANGED__STATE__SERVICE_STATE_EMERGENCY_ONLY = 2;
  
  public static final int PHONE_SERVICE_STATE_CHANGED__STATE__SERVICE_STATE_IN_SERVICE = 0;
  
  public static final int PHONE_SERVICE_STATE_CHANGED__STATE__SERVICE_STATE_OUT_OF_SERVICE = 1;
  
  public static final int PHONE_SERVICE_STATE_CHANGED__STATE__SERVICE_STATE_POWER_OFF = 3;
  
  public static final int PHONE_SIGNAL_STRENGTH_CHANGED = 40;
  
  public static final int PHONE_SIGNAL_STRENGTH_CHANGED__SIGNAL_STRENGTH__SIGNAL_STRENGTH_GOOD = 3;
  
  public static final int PHONE_SIGNAL_STRENGTH_CHANGED__SIGNAL_STRENGTH__SIGNAL_STRENGTH_GREAT = 4;
  
  public static final int PHONE_SIGNAL_STRENGTH_CHANGED__SIGNAL_STRENGTH__SIGNAL_STRENGTH_MODERATE = 2;
  
  public static final int PHONE_SIGNAL_STRENGTH_CHANGED__SIGNAL_STRENGTH__SIGNAL_STRENGTH_NONE_OR_UNKNOWN = 0;
  
  public static final int PHONE_SIGNAL_STRENGTH_CHANGED__SIGNAL_STRENGTH__SIGNAL_STRENGTH_POOR = 1;
  
  public static final int PHONE_STATE_CHANGED = 95;
  
  public static final int PHONE_STATE_CHANGED__STATE__OFF = 0;
  
  public static final int PHONE_STATE_CHANGED__STATE__ON = 1;
  
  public static final int PICTURE_IN_PICTURE_STATE_CHANGED = 52;
  
  public static final int PICTURE_IN_PICTURE_STATE_CHANGED__STATE__DISMISSED = 4;
  
  public static final int PICTURE_IN_PICTURE_STATE_CHANGED__STATE__ENTERED = 1;
  
  public static final int PICTURE_IN_PICTURE_STATE_CHANGED__STATE__EXPANDED_TO_FULL_SCREEN = 2;
  
  public static final int PICTURE_IN_PICTURE_STATE_CHANGED__STATE__MINIMIZED = 3;
  
  public static final int PLUGGED_STATE_CHANGED = 32;
  
  public static final int PLUGGED_STATE_CHANGED__STATE__BATTERY_PLUGGED_AC = 1;
  
  public static final int PLUGGED_STATE_CHANGED__STATE__BATTERY_PLUGGED_NONE = 0;
  
  public static final int PLUGGED_STATE_CHANGED__STATE__BATTERY_PLUGGED_USB = 2;
  
  public static final int PLUGGED_STATE_CHANGED__STATE__BATTERY_PLUGGED_WIRELESS = 4;
  
  public static final int POWER_PROFILE = 10033;
  
  public static final int PROCESS_CPU_TIME = 10035;
  
  public static final int PROCESS_LIFE_CYCLE_STATE_CHANGED = 28;
  
  public static final int PROCESS_LIFE_CYCLE_STATE_CHANGED__STATE__CRASHED = 2;
  
  public static final int PROCESS_LIFE_CYCLE_STATE_CHANGED__STATE__FINISHED = 0;
  
  public static final int PROCESS_LIFE_CYCLE_STATE_CHANGED__STATE__STARTED = 1;
  
  public static final int PROCESS_MEMORY_HIGH_WATER_MARK = 10042;
  
  public static final int PROCESS_MEMORY_SNAPSHOT = 10064;
  
  public static final int PROCESS_MEMORY_STATE = 10013;
  
  public static final int PROCESS_MEMORY_STAT_REPORTED = 18;
  
  public static final int PROCESS_MEMORY_STAT_REPORTED__TYPE__ADD_PSS_EXTERNAL = 3;
  
  public static final int PROCESS_MEMORY_STAT_REPORTED__TYPE__ADD_PSS_EXTERNAL_SLOW = 4;
  
  public static final int PROCESS_MEMORY_STAT_REPORTED__TYPE__ADD_PSS_INTERNAL_ALL_MEM = 1;
  
  public static final int PROCESS_MEMORY_STAT_REPORTED__TYPE__ADD_PSS_INTERNAL_ALL_POLL = 2;
  
  public static final int PROCESS_MEMORY_STAT_REPORTED__TYPE__ADD_PSS_INTERNAL_SINGLE = 0;
  
  public static final int PROCESS_START_TIME = 169;
  
  public static final int PROCESS_START_TIME__TYPE__COLD = 3;
  
  public static final int PROCESS_START_TIME__TYPE__HOT = 2;
  
  public static final int PROCESS_START_TIME__TYPE__UNKNOWN = 0;
  
  public static final int PROCESS_START_TIME__TYPE__WARM = 1;
  
  public static final int PROCESS_STATE_CHANGED = 3;
  
  public static final int PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_BACKUP = 1008;
  
  public static final int PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_BOUND_FOREGROUND_SERVICE = 1004;
  
  public static final int PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_BOUND_TOP = 1020;
  
  public static final int PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_CACHED_ACTIVITY = 1015;
  
  public static final int PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_CACHED_ACTIVITY_CLIENT = 1016;
  
  public static final int PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_CACHED_EMPTY = 1018;
  
  public static final int PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_CACHED_RECENT = 1017;
  
  public static final int PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_FOREGROUND_SERVICE = 1003;
  
  public static final int PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_HEAVY_WEIGHT = 1012;
  
  public static final int PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_HOME = 1013;
  
  public static final int PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_IMPORTANT_BACKGROUND = 1006;
  
  public static final int PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_IMPORTANT_FOREGROUND = 1005;
  
  public static final int PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_LAST_ACTIVITY = 1014;
  
  public static final int PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_NONEXISTENT = 1019;
  
  public static final int PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_PERSISTENT = 1000;
  
  public static final int PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_PERSISTENT_UI = 1001;
  
  public static final int PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_RECEIVER = 1010;
  
  public static final int PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_SERVICE = 1009;
  
  public static final int PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_TOP = 1002;
  
  public static final int PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_TOP_SLEEPING = 1011;
  
  public static final int PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_TRANSIENT_BACKGROUND = 1007;
  
  public static final int PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_UNKNOWN = 999;
  
  public static final int PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_UNKNOWN_TO_PROTO = 998;
  
  public static final int PROCESS_SYSTEM_ION_HEAP_SIZE = 10061;
  
  public static final int PROC_STATS = 10029;
  
  public static final int PROC_STATS_PKG_PROC = 10034;
  
  public static final int RANKING_SELECTED = 260;
  
  public static final int REBOOT_ESCROW_RECOVERY_REPORTED = 238;
  
  public static final int REMAINING_BATTERY_CAPACITY = 10019;
  
  public static final int RESCUE_PARTY_RESET_REPORTED = 122;
  
  public static final int RESOURCE_CONFIGURATION_CHANGED = 66;
  
  public static final int ROLE_HOLDER = 10049;
  
  public static final int RUNTIME_APP_OP_ACCESS = 10069;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_ACCEPT_HANDOVER = 74;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_ACCESS_ACCESSIBILITY = 88;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_ACCESS_MEDIA_LOCATION = 90;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_ACCESS_NOTIFICATIONS = 25;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_ACTIVATE_PLATFORM_VPN = 94;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_ACTIVATE_VPN = 47;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_ACTIVITY_RECOGNITION = 79;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_ADD_VOICEMAIL = 52;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_ANSWER_PHONE_CALLS = 69;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_ASSIST_SCREENSHOT = 50;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_ASSIST_STRUCTURE = 49;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_AUDIO_ACCESSIBILITY_VOLUME = 64;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_AUDIO_ALARM_VOLUME = 37;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_AUDIO_BLUETOOTH_VOLUME = 39;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_AUDIO_MASTER_VOLUME = 33;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_AUDIO_MEDIA_VOLUME = 36;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_AUDIO_NOTIFICATION_VOLUME = 38;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_AUDIO_RING_VOLUME = 35;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_AUDIO_VOICE_VOLUME = 34;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_AUTO_REVOKE_MANAGED_BY_INSTALLER = 98;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_AUTO_REVOKE_PERMISSIONS_IF_UNUSED = 97;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_BIND_ACCESSIBILITY_SERVICE = 73;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_BLUETOOTH_SCAN = 77;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_BODY_SENSORS = 56;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_CALL_PHONE = 13;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_CAMERA = 26;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_CHANGE_WIFI_STATE = 71;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_COARSE_LOCATION = 0;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_DEPRECATED_1 = 96;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_FINE_LOCATION = 1;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_GET_ACCOUNTS = 62;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_GET_USAGE_STATS = 43;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_GPS = 2;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_INSTANT_APP_START_FOREGROUND = 68;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_INTERACT_ACROSS_PROFILES = 93;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_LEGACY_STORAGE = 87;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_LOADER_USAGE_STATS = 95;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_MANAGE_EXTERNAL_STORAGE = 92;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_MANAGE_IPSEC_TUNNELS = 75;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_MOCK_LOCATION = 58;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_MONITOR_HIGH_POWER_LOCATION = 42;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_MONITOR_LOCATION = 41;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_MUTE_MICROPHONE = 44;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_NEIGHBORING_CELLS = 12;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_NONE = -1;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_NO_ISOLATED_STORAGE = 99;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_PICTURE_IN_PICTURE = 67;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_PLAY_AUDIO = 28;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_POST_NOTIFICATION = 11;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_PROCESS_OUTGOING_CALLS = 54;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_PROJECT_MEDIA = 46;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_QUERY_ALL_PACKAGES = 91;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_READ_CALENDAR = 8;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_READ_CALL_LOG = 6;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_READ_CELL_BROADCASTS = 57;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_READ_CLIPBOARD = 29;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_READ_CONTACTS = 4;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_READ_DEVICE_IDENTIFIERS = 89;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_READ_EXTERNAL_STORAGE = 59;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_READ_ICC_SMS = 21;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_READ_MEDIA_AUDIO = 81;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_READ_MEDIA_IMAGES = 85;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_READ_MEDIA_VIDEO = 83;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_READ_PHONE_NUMBERS = 65;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_READ_PHONE_STATE = 51;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_READ_SMS = 14;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_RECEIVE_EMERGENCY_SMS = 17;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_RECEIVE_MMS = 18;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_RECEIVE_SMS = 16;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_RECEIVE_WAP_PUSH = 19;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_RECORD_AUDIO = 27;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_REQUEST_DELETE_PACKAGES = 72;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_REQUEST_INSTALL_PACKAGES = 66;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_RUN_ANY_IN_BACKGROUND = 70;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_RUN_IN_BACKGROUND = 63;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_SEND_SMS = 20;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_SMS_FINANCIAL_TRANSACTIONS = 80;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_START_FOREGROUND = 76;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_SYSTEM_ALERT_WINDOW = 24;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_TAKE_AUDIO_FOCUS = 32;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_TAKE_MEDIA_BUTTONS = 31;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_TOAST_WINDOW = 45;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_TURN_SCREEN_ON = 61;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_USE_BIOMETRIC = 78;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_USE_FINGERPRINT = 55;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_USE_SIP = 53;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_VIBRATE = 3;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_WAKE_LOCK = 40;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_WIFI_SCAN = 10;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_WRITE_CALENDAR = 9;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_WRITE_CALL_LOG = 7;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_WRITE_CLIPBOARD = 30;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_WRITE_CONTACTS = 5;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_WRITE_EXTERNAL_STORAGE = 60;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_WRITE_ICC_SMS = 22;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_WRITE_MEDIA_AUDIO = 82;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_WRITE_MEDIA_IMAGES = 86;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_WRITE_MEDIA_VIDEO = 84;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_WRITE_SETTINGS = 23;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_WRITE_SMS = 15;
  
  public static final int RUNTIME_APP_OP_ACCESS__OP__APP_OP_WRITE_WALLPAPER = 48;
  
  public static final int RUNTIME_APP_OP_ACCESS__SAMPLING_STRATEGY__BOOT_TIME_SAMPLING = 3;
  
  public static final int RUNTIME_APP_OP_ACCESS__SAMPLING_STRATEGY__DEFAULT = 0;
  
  public static final int RUNTIME_APP_OP_ACCESS__SAMPLING_STRATEGY__RARELY_USED = 2;
  
  public static final int RUNTIME_APP_OP_ACCESS__SAMPLING_STRATEGY__UNIFORM = 1;
  
  public static final int RUNTIME_APP_OP_ACCESS__SAMPLING_STRATEGY__UNIFORM_OPS = 4;
  
  public static final int SCHEDULED_JOB_CONSTRAINT_CHANGED = 150;
  
  public static final int SCHEDULED_JOB_CONSTRAINT_CHANGED__CONSTRAINT__CONSTRAINT_BACKGROUND_NOT_RESTRICTED = 11;
  
  public static final int SCHEDULED_JOB_CONSTRAINT_CHANGED__CONSTRAINT__CONSTRAINT_BATTERY_NOT_LOW = 2;
  
  public static final int SCHEDULED_JOB_CONSTRAINT_CHANGED__CONSTRAINT__CONSTRAINT_CHARGING = 1;
  
  public static final int SCHEDULED_JOB_CONSTRAINT_CHANGED__CONSTRAINT__CONSTRAINT_CONNECTIVITY = 7;
  
  public static final int SCHEDULED_JOB_CONSTRAINT_CHANGED__CONSTRAINT__CONSTRAINT_CONTENT_TRIGGER = 8;
  
  public static final int SCHEDULED_JOB_CONSTRAINT_CHANGED__CONSTRAINT__CONSTRAINT_DEADLINE = 5;
  
  public static final int SCHEDULED_JOB_CONSTRAINT_CHANGED__CONSTRAINT__CONSTRAINT_DEVICE_NOT_DOZING = 9;
  
  public static final int SCHEDULED_JOB_CONSTRAINT_CHANGED__CONSTRAINT__CONSTRAINT_IDLE = 6;
  
  public static final int SCHEDULED_JOB_CONSTRAINT_CHANGED__CONSTRAINT__CONSTRAINT_STORAGE_NOT_LOW = 3;
  
  public static final int SCHEDULED_JOB_CONSTRAINT_CHANGED__CONSTRAINT__CONSTRAINT_TIMING_DELAY = 4;
  
  public static final int SCHEDULED_JOB_CONSTRAINT_CHANGED__CONSTRAINT__CONSTRAINT_UNKNOWN = 0;
  
  public static final int SCHEDULED_JOB_CONSTRAINT_CHANGED__CONSTRAINT__CONSTRAINT_WITHIN_QUOTA = 10;
  
  public static final int SCHEDULED_JOB_CONSTRAINT_CHANGED__STATE__SATISFIED = 2;
  
  public static final int SCHEDULED_JOB_CONSTRAINT_CHANGED__STATE__UNKNOWN = 0;
  
  public static final int SCHEDULED_JOB_CONSTRAINT_CHANGED__STATE__UNSATISFIED = 1;
  
  public static final int SCHEDULED_JOB_STATE_CHANGED = 8;
  
  public static final int SCHEDULED_JOB_STATE_CHANGED__STANDBY_BUCKET__ACTIVE = 0;
  
  public static final int SCHEDULED_JOB_STATE_CHANGED__STANDBY_BUCKET__FREQUENT = 2;
  
  public static final int SCHEDULED_JOB_STATE_CHANGED__STANDBY_BUCKET__NEVER = 4;
  
  public static final int SCHEDULED_JOB_STATE_CHANGED__STANDBY_BUCKET__RARE = 3;
  
  public static final int SCHEDULED_JOB_STATE_CHANGED__STANDBY_BUCKET__RESTRICTED = 5;
  
  public static final int SCHEDULED_JOB_STATE_CHANGED__STANDBY_BUCKET__UNKNOWN = -1;
  
  public static final int SCHEDULED_JOB_STATE_CHANGED__STANDBY_BUCKET__WORKING_SET = 1;
  
  public static final int SCHEDULED_JOB_STATE_CHANGED__STATE__FINISHED = 0;
  
  public static final int SCHEDULED_JOB_STATE_CHANGED__STATE__SCHEDULED = 2;
  
  public static final int SCHEDULED_JOB_STATE_CHANGED__STATE__STARTED = 1;
  
  public static final int SCHEDULED_JOB_STATE_CHANGED__STOP_REASON__STOP_REASON_CANCELLED = 0;
  
  public static final int SCHEDULED_JOB_STATE_CHANGED__STOP_REASON__STOP_REASON_CONSTRAINTS_NOT_SATISFIED = 1;
  
  public static final int SCHEDULED_JOB_STATE_CHANGED__STOP_REASON__STOP_REASON_DEVICE_IDLE = 4;
  
  public static final int SCHEDULED_JOB_STATE_CHANGED__STOP_REASON__STOP_REASON_DEVICE_THERMAL = 5;
  
  public static final int SCHEDULED_JOB_STATE_CHANGED__STOP_REASON__STOP_REASON_PREEMPT = 2;
  
  public static final int SCHEDULED_JOB_STATE_CHANGED__STOP_REASON__STOP_REASON_RESTRICTED_BUCKET = 6;
  
  public static final int SCHEDULED_JOB_STATE_CHANGED__STOP_REASON__STOP_REASON_TIMEOUT = 3;
  
  public static final int SCHEDULED_JOB_STATE_CHANGED__STOP_REASON__STOP_REASON_UNKNOWN = -1;
  
  public static final int SCREEN_BRIGHTNESS_CHANGED = 9;
  
  public static final int SCREEN_STATE_CHANGED = 29;
  
  public static final int SCREEN_STATE_CHANGED__STATE__DISPLAY_STATE_DOZE = 3;
  
  public static final int SCREEN_STATE_CHANGED__STATE__DISPLAY_STATE_DOZE_SUSPEND = 4;
  
  public static final int SCREEN_STATE_CHANGED__STATE__DISPLAY_STATE_OFF = 1;
  
  public static final int SCREEN_STATE_CHANGED__STATE__DISPLAY_STATE_ON = 2;
  
  public static final int SCREEN_STATE_CHANGED__STATE__DISPLAY_STATE_ON_SUSPEND = 6;
  
  public static final int SCREEN_STATE_CHANGED__STATE__DISPLAY_STATE_UNKNOWN = 0;
  
  public static final int SCREEN_STATE_CHANGED__STATE__DISPLAY_STATE_VR = 5;
  
  public static final int SCREEN_TIMEOUT_EXTENSION_REPORTED = 168;
  
  public static final int SENSOR_STATE_CHANGED = 5;
  
  public static final int SENSOR_STATE_CHANGED__STATE__OFF = 0;
  
  public static final int SENSOR_STATE_CHANGED__STATE__ON = 1;
  
  public static final int SERVICE_LAUNCH_REPORTED = 100;
  
  public static final int SERVICE_STATE_CHANGED = 99;
  
  public static final int SERVICE_STATE_CHANGED__STATE__START = 1;
  
  public static final int SERVICE_STATE_CHANGED__STATE__STOP = 2;
  
  public static final int SETTING_CHANGED = 41;
  
  public static final int SETTING_CHANGED__REASON__DELETED = 2;
  
  public static final int SETTING_CHANGED__REASON__UPDATED = 1;
  
  public static final int SETTING_SNAPSHOT = 10080;
  
  public static final int SETTING_SNAPSHOT__TYPE__ASSIGNED_BOOL_TYPE = 1;
  
  public static final int SETTING_SNAPSHOT__TYPE__ASSIGNED_FLOAT_TYPE = 3;
  
  public static final int SETTING_SNAPSHOT__TYPE__ASSIGNED_INT_TYPE = 2;
  
  public static final int SETTING_SNAPSHOT__TYPE__ASSIGNED_STRING_TYPE = 4;
  
  public static final int SETTING_SNAPSHOT__TYPE__NOTASSIGNED = 0;
  
  public static final int SHARESHEET_STARTED = 259;
  
  public static final int SHARESHEET_STARTED__INTENT_TYPE__INTENT_ACTION_EDIT = 2;
  
  public static final int SHARESHEET_STARTED__INTENT_TYPE__INTENT_ACTION_IMAGE_CAPTURE = 6;
  
  public static final int SHARESHEET_STARTED__INTENT_TYPE__INTENT_ACTION_MAIN = 7;
  
  public static final int SHARESHEET_STARTED__INTENT_TYPE__INTENT_ACTION_SEND = 3;
  
  public static final int SHARESHEET_STARTED__INTENT_TYPE__INTENT_ACTION_SENDTO = 4;
  
  public static final int SHARESHEET_STARTED__INTENT_TYPE__INTENT_ACTION_SEND_MULTIPLE = 5;
  
  public static final int SHARESHEET_STARTED__INTENT_TYPE__INTENT_ACTION_VIEW = 1;
  
  public static final int SHARESHEET_STARTED__INTENT_TYPE__INTENT_DEFAULT = 0;
  
  public static final int SHARESHEET_STARTED__PREVIEW_TYPE__CONTENT_PREVIEW_FILE = 2;
  
  public static final int SHARESHEET_STARTED__PREVIEW_TYPE__CONTENT_PREVIEW_IMAGE = 1;
  
  public static final int SHARESHEET_STARTED__PREVIEW_TYPE__CONTENT_PREVIEW_TEXT = 3;
  
  public static final int SHARESHEET_STARTED__PREVIEW_TYPE__CONTENT_PREVIEW_TYPE_UNKNOWN = 0;
  
  public static final int SHUTDOWN_SEQUENCE_REPORTED = 56;
  
  public static final int SIGNED_CONFIG_REPORTED = 123;
  
  public static final int SIGNED_CONFIG_REPORTED__STATUS__APPLIED = 1;
  
  public static final int SIGNED_CONFIG_REPORTED__STATUS__BASE64_FAILURE_CONFIG = 2;
  
  public static final int SIGNED_CONFIG_REPORTED__STATUS__BASE64_FAILURE_SIGNATURE = 3;
  
  public static final int SIGNED_CONFIG_REPORTED__STATUS__INVALID_CONFIG = 5;
  
  public static final int SIGNED_CONFIG_REPORTED__STATUS__NOT_APPLICABLE = 8;
  
  public static final int SIGNED_CONFIG_REPORTED__STATUS__OLD_CONFIG = 6;
  
  public static final int SIGNED_CONFIG_REPORTED__STATUS__SECURITY_EXCEPTION = 4;
  
  public static final int SIGNED_CONFIG_REPORTED__STATUS__SIGNATURE_CHECK_FAILED = 7;
  
  public static final int SIGNED_CONFIG_REPORTED__STATUS__SIGNATURE_CHECK_FAILED_PROD_KEY_ABSENT = 9;
  
  public static final int SIGNED_CONFIG_REPORTED__STATUS__UNKNOWN_STATUS = 0;
  
  public static final int SIGNED_CONFIG_REPORTED__TYPE__GLOBAL_SETTINGS = 1;
  
  public static final int SIGNED_CONFIG_REPORTED__TYPE__UNKNOWN_TYPE = 0;
  
  public static final int SIGNED_CONFIG_REPORTED__VERIFIED_WITH__DEBUG = 1;
  
  public static final int SIGNED_CONFIG_REPORTED__VERIFIED_WITH__NO_KEY = 0;
  
  public static final int SIGNED_CONFIG_REPORTED__VERIFIED_WITH__PRODUCTION = 2;
  
  public static final int SYNC_STATE_CHANGED = 7;
  
  public static final int SYNC_STATE_CHANGED__STATE__OFF = 0;
  
  public static final int SYNC_STATE_CHANGED__STATE__ON = 1;
  
  public static final int SYSTEM_ELAPSED_REALTIME = 10014;
  
  public static final int SYSTEM_ION_HEAP_SIZE = 10056;
  
  public static final int SYSTEM_SERVER_WATCHDOG_OCCURRED = 185;
  
  public static final int SYSTEM_UPTIME = 10015;
  
  public static final int TEMPERATURE = 10021;
  
  public static final int TEMPERATURE__SENSOR_LOCATION__TEMPERATURE_TYPE_BATTERY = 2;
  
  public static final int TEMPERATURE__SENSOR_LOCATION__TEMPERATURE_TYPE_BCL_CURRENT = 7;
  
  public static final int TEMPERATURE__SENSOR_LOCATION__TEMPERATURE_TYPE_BCL_PERCENTAGE = 8;
  
  public static final int TEMPERATURE__SENSOR_LOCATION__TEMPERATURE_TYPE_BCL_VOLTAGE = 6;
  
  public static final int TEMPERATURE__SENSOR_LOCATION__TEMPERATURE_TYPE_CPU = 0;
  
  public static final int TEMPERATURE__SENSOR_LOCATION__TEMPERATURE_TYPE_GPU = 1;
  
  public static final int TEMPERATURE__SENSOR_LOCATION__TEMPERATURE_TYPE_NPU = 9;
  
  public static final int TEMPERATURE__SENSOR_LOCATION__TEMPERATURE_TYPE_POWER_AMPLIFIER = 5;
  
  public static final int TEMPERATURE__SENSOR_LOCATION__TEMPERATURE_TYPE_SKIN = 3;
  
  public static final int TEMPERATURE__SENSOR_LOCATION__TEMPERATURE_TYPE_UNKNOWN = -1;
  
  public static final int TEMPERATURE__SENSOR_LOCATION__TEMPERATURE_TYPE_USB_PORT = 4;
  
  public static final int TEMPERATURE__SEVERITY__CRITICAL = 4;
  
  public static final int TEMPERATURE__SEVERITY__EMERGENCY = 5;
  
  public static final int TEMPERATURE__SEVERITY__LIGHT = 1;
  
  public static final int TEMPERATURE__SEVERITY__MODERATE = 2;
  
  public static final int TEMPERATURE__SEVERITY__NONE = 0;
  
  public static final int TEMPERATURE__SEVERITY__SEVERE = 3;
  
  public static final int TEMPERATURE__SEVERITY__SHUTDOWN = 6;
  
  public static final int THERMAL_THROTTLING_SEVERITY_STATE_CHANGED = 189;
  
  public static final int THERMAL_THROTTLING_SEVERITY_STATE_CHANGED__SENSOR_TYPE__TEMPERATURE_TYPE_BATTERY = 2;
  
  public static final int THERMAL_THROTTLING_SEVERITY_STATE_CHANGED__SENSOR_TYPE__TEMPERATURE_TYPE_BCL_CURRENT = 7;
  
  public static final int THERMAL_THROTTLING_SEVERITY_STATE_CHANGED__SENSOR_TYPE__TEMPERATURE_TYPE_BCL_PERCENTAGE = 8;
  
  public static final int THERMAL_THROTTLING_SEVERITY_STATE_CHANGED__SENSOR_TYPE__TEMPERATURE_TYPE_BCL_VOLTAGE = 6;
  
  public static final int THERMAL_THROTTLING_SEVERITY_STATE_CHANGED__SENSOR_TYPE__TEMPERATURE_TYPE_CPU = 0;
  
  public static final int THERMAL_THROTTLING_SEVERITY_STATE_CHANGED__SENSOR_TYPE__TEMPERATURE_TYPE_GPU = 1;
  
  public static final int THERMAL_THROTTLING_SEVERITY_STATE_CHANGED__SENSOR_TYPE__TEMPERATURE_TYPE_NPU = 9;
  
  public static final int THERMAL_THROTTLING_SEVERITY_STATE_CHANGED__SENSOR_TYPE__TEMPERATURE_TYPE_POWER_AMPLIFIER = 5;
  
  public static final int THERMAL_THROTTLING_SEVERITY_STATE_CHANGED__SENSOR_TYPE__TEMPERATURE_TYPE_SKIN = 3;
  
  public static final int THERMAL_THROTTLING_SEVERITY_STATE_CHANGED__SENSOR_TYPE__TEMPERATURE_TYPE_UNKNOWN = -1;
  
  public static final int THERMAL_THROTTLING_SEVERITY_STATE_CHANGED__SENSOR_TYPE__TEMPERATURE_TYPE_USB_PORT = 4;
  
  public static final int THERMAL_THROTTLING_SEVERITY_STATE_CHANGED__SEVERITY__CRITICAL = 4;
  
  public static final int THERMAL_THROTTLING_SEVERITY_STATE_CHANGED__SEVERITY__EMERGENCY = 5;
  
  public static final int THERMAL_THROTTLING_SEVERITY_STATE_CHANGED__SEVERITY__LIGHT = 1;
  
  public static final int THERMAL_THROTTLING_SEVERITY_STATE_CHANGED__SEVERITY__MODERATE = 2;
  
  public static final int THERMAL_THROTTLING_SEVERITY_STATE_CHANGED__SEVERITY__NONE = 0;
  
  public static final int THERMAL_THROTTLING_SEVERITY_STATE_CHANGED__SEVERITY__SEVERE = 3;
  
  public static final int THERMAL_THROTTLING_SEVERITY_STATE_CHANGED__SEVERITY__SHUTDOWN = 6;
  
  public static final int TIME_ZONE_DATA_INFO = 10052;
  
  public static final int TOMB_STONE_OCCURRED = 186;
  
  public static final int TOUCH_GESTURE_CLASSIFIED = 177;
  
  public static final int TOUCH_GESTURE_CLASSIFIED__CLASSIFICATION__DEEP_PRESS = 4;
  
  public static final int TOUCH_GESTURE_CLASSIFIED__CLASSIFICATION__DOUBLE_TAP = 2;
  
  public static final int TOUCH_GESTURE_CLASSIFIED__CLASSIFICATION__LONG_PRESS = 3;
  
  public static final int TOUCH_GESTURE_CLASSIFIED__CLASSIFICATION__SCROLL = 5;
  
  public static final int TOUCH_GESTURE_CLASSIFIED__CLASSIFICATION__SINGLE_TAP = 1;
  
  public static final int TOUCH_GESTURE_CLASSIFIED__CLASSIFICATION__UNKNOWN_CLASSIFICATION = 0;
  
  public static final int TV_CAS_SESSION_OPEN_STATUS = 280;
  
  public static final int TV_CAS_SESSION_OPEN_STATUS__STATE__FAILED = 2;
  
  public static final int TV_CAS_SESSION_OPEN_STATUS__STATE__SUCCEEDED = 1;
  
  public static final int TV_CAS_SESSION_OPEN_STATUS__STATE__UNKNOWN = 0;
  
  public static final int TV_TUNER_DVR_STATUS = 279;
  
  public static final int TV_TUNER_DVR_STATUS__STATE__STARTED = 1;
  
  public static final int TV_TUNER_DVR_STATUS__STATE__STOPPED = 2;
  
  public static final int TV_TUNER_DVR_STATUS__STATE__UNKNOWN_STATE = 0;
  
  public static final int TV_TUNER_DVR_STATUS__TYPE__PLAYBACK = 1;
  
  public static final int TV_TUNER_DVR_STATUS__TYPE__RECORD = 2;
  
  public static final int TV_TUNER_DVR_STATUS__TYPE__UNKNOWN_TYPE = 0;
  
  public static final int TV_TUNER_STATE_CHANGED = 276;
  
  public static final int TV_TUNER_STATE_CHANGED__STATE__LOCKED = 2;
  
  public static final int TV_TUNER_STATE_CHANGED__STATE__NOT_LOCKED = 3;
  
  public static final int TV_TUNER_STATE_CHANGED__STATE__SCANNING = 5;
  
  public static final int TV_TUNER_STATE_CHANGED__STATE__SCAN_STOPPED = 6;
  
  public static final int TV_TUNER_STATE_CHANGED__STATE__SIGNAL_LOST = 4;
  
  public static final int TV_TUNER_STATE_CHANGED__STATE__TUNING = 1;
  
  public static final int TV_TUNER_STATE_CHANGED__STATE__UNKNOWN = 0;
  
  public static final int UID_PROCESS_STATE_CHANGED = 27;
  
  public static final int UID_PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_BACKUP = 1008;
  
  public static final int UID_PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_BOUND_FOREGROUND_SERVICE = 1004;
  
  public static final int UID_PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_BOUND_TOP = 1020;
  
  public static final int UID_PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_CACHED_ACTIVITY = 1015;
  
  public static final int UID_PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_CACHED_ACTIVITY_CLIENT = 1016;
  
  public static final int UID_PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_CACHED_EMPTY = 1018;
  
  public static final int UID_PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_CACHED_RECENT = 1017;
  
  public static final int UID_PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_FOREGROUND_SERVICE = 1003;
  
  public static final int UID_PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_HEAVY_WEIGHT = 1012;
  
  public static final int UID_PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_HOME = 1013;
  
  public static final int UID_PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_IMPORTANT_BACKGROUND = 1006;
  
  public static final int UID_PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_IMPORTANT_FOREGROUND = 1005;
  
  public static final int UID_PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_LAST_ACTIVITY = 1014;
  
  public static final int UID_PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_NONEXISTENT = 1019;
  
  public static final int UID_PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_PERSISTENT = 1000;
  
  public static final int UID_PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_PERSISTENT_UI = 1001;
  
  public static final int UID_PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_RECEIVER = 1010;
  
  public static final int UID_PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_SERVICE = 1009;
  
  public static final int UID_PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_TOP = 1002;
  
  public static final int UID_PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_TOP_SLEEPING = 1011;
  
  public static final int UID_PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_TRANSIENT_BACKGROUND = 1007;
  
  public static final int UID_PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_UNKNOWN = 999;
  
  public static final int UID_PROCESS_STATE_CHANGED__STATE__PROCESS_STATE_UNKNOWN_TO_PROTO = 998;
  
  public static final int UI_EVENT_REPORTED = 90;
  
  public static final int USB_CONNECTOR_STATE_CHANGED = 70;
  
  public static final int USB_CONNECTOR_STATE_CHANGED__STATE__STATE_CONNECTED = 1;
  
  public static final int USB_CONNECTOR_STATE_CHANGED__STATE__STATE_DISCONNECTED = 0;
  
  public static final int USB_CONTAMINANT_REPORTED = 146;
  
  public static final int USB_CONTAMINANT_REPORTED__STATUS__CONTAMINANT_STATUS_DETECTED = 4;
  
  public static final int USB_CONTAMINANT_REPORTED__STATUS__CONTAMINANT_STATUS_DISABLED = 2;
  
  public static final int USB_CONTAMINANT_REPORTED__STATUS__CONTAMINANT_STATUS_NOT_DETECTED = 3;
  
  public static final int USB_CONTAMINANT_REPORTED__STATUS__CONTAMINANT_STATUS_NOT_SUPPORTED = 1;
  
  public static final int USB_CONTAMINANT_REPORTED__STATUS__CONTAMINANT_STATUS_UNKNOWN = 0;
  
  public static final int USB_DEVICE_ATTACHED = 77;
  
  public static final int USB_DEVICE_ATTACHED__STATE__STATE_CONNECTED = 1;
  
  public static final int USB_DEVICE_ATTACHED__STATE__STATE_DISCONNECTED = 0;
  
  public static final int USERSPACE_REBOOT_REPORTED = 243;
  
  public static final int USERSPACE_REBOOT_REPORTED__OUTCOME__FAILED_SHUTDOWN_SEQUENCE_ABORTED = 2;
  
  public static final int USERSPACE_REBOOT_REPORTED__OUTCOME__FAILED_USERDATA_REMOUNT = 3;
  
  public static final int USERSPACE_REBOOT_REPORTED__OUTCOME__FAILED_USERSPACE_REBOOT_WATCHDOG_TRIGGERED = 4;
  
  public static final int USERSPACE_REBOOT_REPORTED__OUTCOME__OUTCOME_UNKNOWN = 0;
  
  public static final int USERSPACE_REBOOT_REPORTED__OUTCOME__SUCCESS = 1;
  
  public static final int USERSPACE_REBOOT_REPORTED__USER_ENCRYPTION_STATE__LOCKED = 2;
  
  public static final int USERSPACE_REBOOT_REPORTED__USER_ENCRYPTION_STATE__UNLOCKED = 1;
  
  public static final int USERSPACE_REBOOT_REPORTED__USER_ENCRYPTION_STATE__USER_ENCRYPTION_STATE_UNKNOWN = 0;
  
  public static final int USER_LIFECYCLE_EVENT_OCCURRED = 265;
  
  public static final int USER_LIFECYCLE_EVENT_OCCURRED__EVENT__CREATE_USER = 3;
  
  public static final int USER_LIFECYCLE_EVENT_OCCURRED__EVENT__START_USER = 2;
  
  public static final int USER_LIFECYCLE_EVENT_OCCURRED__EVENT__SWITCH_USER = 1;
  
  public static final int USER_LIFECYCLE_EVENT_OCCURRED__EVENT__UNKNOWN = 0;
  
  public static final int USER_LIFECYCLE_EVENT_OCCURRED__EVENT__UNLOCKED_USER = 6;
  
  public static final int USER_LIFECYCLE_EVENT_OCCURRED__EVENT__UNLOCKING_USER = 5;
  
  public static final int USER_LIFECYCLE_EVENT_OCCURRED__EVENT__USER_RUNNING_LOCKED = 4;
  
  public static final int USER_LIFECYCLE_EVENT_OCCURRED__STATE__BEGIN = 1;
  
  public static final int USER_LIFECYCLE_EVENT_OCCURRED__STATE__FINISH = 2;
  
  public static final int USER_LIFECYCLE_EVENT_OCCURRED__STATE__NONE = 0;
  
  public static final int USER_LIFECYCLE_JOURNEY_REPORTED = 264;
  
  public static final int USER_LIFECYCLE_JOURNEY_REPORTED__JOURNEY__UNKNOWN = 0;
  
  public static final int USER_LIFECYCLE_JOURNEY_REPORTED__JOURNEY__USER_CREATE = 4;
  
  public static final int USER_LIFECYCLE_JOURNEY_REPORTED__JOURNEY__USER_START = 3;
  
  public static final int USER_LIFECYCLE_JOURNEY_REPORTED__JOURNEY__USER_SWITCH_FG = 2;
  
  public static final int USER_LIFECYCLE_JOURNEY_REPORTED__JOURNEY__USER_SWITCH_UI = 1;
  
  public static final int USER_LIFECYCLE_JOURNEY_REPORTED__USER_TYPE__FULL_DEMO = 4;
  
  public static final int USER_LIFECYCLE_JOURNEY_REPORTED__USER_TYPE__FULL_GUEST = 3;
  
  public static final int USER_LIFECYCLE_JOURNEY_REPORTED__USER_TYPE__FULL_RESTRICTED = 5;
  
  public static final int USER_LIFECYCLE_JOURNEY_REPORTED__USER_TYPE__FULL_SECONDARY = 2;
  
  public static final int USER_LIFECYCLE_JOURNEY_REPORTED__USER_TYPE__FULL_SYSTEM = 1;
  
  public static final int USER_LIFECYCLE_JOURNEY_REPORTED__USER_TYPE__PROFILE_MANAGED = 6;
  
  public static final int USER_LIFECYCLE_JOURNEY_REPORTED__USER_TYPE__SYSTEM_HEADLESS = 7;
  
  public static final int USER_LIFECYCLE_JOURNEY_REPORTED__USER_TYPE__TYPE_UNKNOWN = 0;
  
  public static final int VIBRATOR_STATE_CHANGED = 84;
  
  public static final int VIBRATOR_STATE_CHANGED__STATE__OFF = 0;
  
  public static final int VIBRATOR_STATE_CHANGED__STATE__ON = 1;
  
  public static final int WAKELOCK_STATE_CHANGED = 10;
  
  public static final int WAKELOCK_STATE_CHANGED__STATE__ACQUIRE = 1;
  
  public static final int WAKELOCK_STATE_CHANGED__STATE__CHANGE_ACQUIRE = 3;
  
  public static final int WAKELOCK_STATE_CHANGED__STATE__CHANGE_RELEASE = 2;
  
  public static final int WAKELOCK_STATE_CHANGED__STATE__RELEASE = 0;
  
  public static final int WAKELOCK_STATE_CHANGED__TYPE__DOZE_WAKE_LOCK = 64;
  
  public static final int WAKELOCK_STATE_CHANGED__TYPE__DRAW_WAKE_LOCK = 128;
  
  public static final int WAKELOCK_STATE_CHANGED__TYPE__FULL_WAKE_LOCK = 26;
  
  public static final int WAKELOCK_STATE_CHANGED__TYPE__PARTIAL_WAKE_LOCK = 1;
  
  public static final int WAKELOCK_STATE_CHANGED__TYPE__PROXIMITY_SCREEN_OFF_WAKE_LOCK = 32;
  
  public static final int WAKELOCK_STATE_CHANGED__TYPE__SCREEN_BRIGHT_WAKE_LOCK = 10;
  
  public static final int WAKELOCK_STATE_CHANGED__TYPE__SCREEN_DIM_WAKE_LOCK = 6;
  
  public static final int WAKEUP_ALARM_OCCURRED = 35;
  
  public static final int WAKEUP_ALARM_OCCURRED__APP_STANDBY_BUCKET__BUCKET_ACTIVE = 10;
  
  public static final int WAKEUP_ALARM_OCCURRED__APP_STANDBY_BUCKET__BUCKET_EXEMPTED = 5;
  
  public static final int WAKEUP_ALARM_OCCURRED__APP_STANDBY_BUCKET__BUCKET_FREQUENT = 30;
  
  public static final int WAKEUP_ALARM_OCCURRED__APP_STANDBY_BUCKET__BUCKET_NEVER = 50;
  
  public static final int WAKEUP_ALARM_OCCURRED__APP_STANDBY_BUCKET__BUCKET_RARE = 40;
  
  public static final int WAKEUP_ALARM_OCCURRED__APP_STANDBY_BUCKET__BUCKET_RESTRICTED = 45;
  
  public static final int WAKEUP_ALARM_OCCURRED__APP_STANDBY_BUCKET__BUCKET_UNKNOWN = 0;
  
  public static final int WAKEUP_ALARM_OCCURRED__APP_STANDBY_BUCKET__BUCKET_WORKING_SET = 20;
  
  public static final int WALL_CLOCK_TIME_SHIFTED = 45;
  
  public static final int WATCHDOG_ROLLBACK_OCCURRED = 147;
  
  public static final int WATCHDOG_ROLLBACK_OCCURRED__ROLLBACK_REASON__REASON_APP_CRASH = 3;
  
  public static final int WATCHDOG_ROLLBACK_OCCURRED__ROLLBACK_REASON__REASON_APP_NOT_RESPONDING = 4;
  
  public static final int WATCHDOG_ROLLBACK_OCCURRED__ROLLBACK_REASON__REASON_EXPLICIT_HEALTH_CHECK = 2;
  
  public static final int WATCHDOG_ROLLBACK_OCCURRED__ROLLBACK_REASON__REASON_NATIVE_CRASH = 1;
  
  public static final int WATCHDOG_ROLLBACK_OCCURRED__ROLLBACK_REASON__REASON_NATIVE_CRASH_DURING_BOOT = 5;
  
  public static final int WATCHDOG_ROLLBACK_OCCURRED__ROLLBACK_REASON__REASON_UNKNOWN = 0;
  
  public static final int WATCHDOG_ROLLBACK_OCCURRED__ROLLBACK_TYPE__ROLLBACK_BOOT_TRIGGERED = 4;
  
  public static final int WATCHDOG_ROLLBACK_OCCURRED__ROLLBACK_TYPE__ROLLBACK_FAILURE = 3;
  
  public static final int WATCHDOG_ROLLBACK_OCCURRED__ROLLBACK_TYPE__ROLLBACK_INITIATE = 1;
  
  public static final int WATCHDOG_ROLLBACK_OCCURRED__ROLLBACK_TYPE__ROLLBACK_SUCCESS = 2;
  
  public static final int WATCHDOG_ROLLBACK_OCCURRED__ROLLBACK_TYPE__UNKNOWN = 0;
  
  public static final int WIFI_ACTIVITY_INFO = 10011;
  
  public static final int WIFI_BYTES_TRANSFER = 10000;
  
  public static final int WIFI_BYTES_TRANSFER_BY_FG_BG = 10001;
  
  public static final int WIFI_ENABLED_STATE_CHANGED = 113;
  
  public static final int WIFI_ENABLED_STATE_CHANGED__STATE__OFF = 0;
  
  public static final int WIFI_ENABLED_STATE_CHANGED__STATE__ON = 1;
  
  public static final int WIFI_RADIO_POWER_STATE_CHANGED = 13;
  
  public static final int WIFI_RADIO_POWER_STATE_CHANGED__STATE__DATA_CONNECTION_POWER_STATE_HIGH = 3;
  
  public static final int WIFI_RADIO_POWER_STATE_CHANGED__STATE__DATA_CONNECTION_POWER_STATE_LOW = 1;
  
  public static final int WIFI_RADIO_POWER_STATE_CHANGED__STATE__DATA_CONNECTION_POWER_STATE_MEDIUM = 2;
  
  public static final int WIFI_RADIO_POWER_STATE_CHANGED__STATE__DATA_CONNECTION_POWER_STATE_UNKNOWN = 2147483647;
  
  public static final int WIFI_RUNNING_STATE_CHANGED = 114;
  
  public static final int WIFI_RUNNING_STATE_CHANGED__STATE__OFF = 0;
  
  public static final int WIFI_RUNNING_STATE_CHANGED__STATE__ON = 1;
  
  public static final int WTFOCCURRED__ERROR_SOURCE__DATA_APP = 1;
  
  public static final int WTFOCCURRED__ERROR_SOURCE__ERROR_SOURCE_UNKNOWN = 0;
  
  public static final int WTFOCCURRED__ERROR_SOURCE__SYSTEM_APP = 2;
  
  public static final int WTFOCCURRED__ERROR_SOURCE__SYSTEM_SERVER = 3;
  
  public static final int WTF_OCCURRED = 80;
  
  public static void write(int paramInt) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int[] paramArrayOfint, String[] paramArrayOfString, int paramInt2) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    if (12 == paramInt1)
      builder.addBooleanAnnotation((byte)2, true); 
    if (23 == paramInt1)
      builder.addBooleanAnnotation((byte)2, true); 
    if (paramArrayOfint == null)
      paramArrayOfint = new int[0]; 
    if (paramArrayOfString == null)
      paramArrayOfString = new String[0]; 
    builder.writeAttributionChain(paramArrayOfint, paramArrayOfString);
    builder.writeInt(paramInt2);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int[] paramArrayOfint, String[] paramArrayOfString, int paramInt2, int paramInt3) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    if (paramArrayOfint == null)
      paramArrayOfint = new int[0]; 
    if (paramArrayOfString == null)
      paramArrayOfString = new String[0]; 
    builder.writeAttributionChain(paramArrayOfint, paramArrayOfString);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int[] paramArrayOfint, String[] paramArrayOfString, int paramInt2, int paramInt3, String paramString) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    if (paramArrayOfint == null)
      paramArrayOfint = new int[0]; 
    if (paramArrayOfString == null)
      paramArrayOfString = new String[0]; 
    builder.writeAttributionChain(paramArrayOfint, paramArrayOfString);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    builder.writeString(paramString);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int[] paramArrayOfint, String[] paramArrayOfString, int paramInt2, long paramLong) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    if (paramArrayOfint == null)
      paramArrayOfint = new int[0]; 
    if (paramArrayOfString == null)
      paramArrayOfString = new String[0]; 
    builder.writeAttributionChain(paramArrayOfint, paramArrayOfString);
    builder.writeInt(paramInt2);
    builder.writeLong(paramLong);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int[] paramArrayOfint, String[] paramArrayOfString, int paramInt2, String paramString, int paramInt3) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    if (paramArrayOfint == null)
      paramArrayOfint = new int[0]; 
    if (paramArrayOfString == null)
      paramArrayOfString = new String[0]; 
    builder.writeAttributionChain(paramArrayOfint, paramArrayOfString);
    if (10 == paramInt1)
      builder.addBooleanAnnotation((byte)5, true); 
    builder.writeInt(paramInt2);
    if (10 == paramInt1)
      builder.addBooleanAnnotation((byte)3, true); 
    builder.writeString(paramString);
    if (10 == paramInt1)
      builder.addBooleanAnnotation((byte)3, true); 
    builder.writeInt(paramInt3);
    if (10 == paramInt1) {
      builder.addBooleanAnnotation((byte)4, true);
      builder.addBooleanAnnotation((byte)8, true);
    } 
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int[] paramArrayOfint, String[] paramArrayOfString, String paramString, int paramInt2) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    if (paramArrayOfint == null)
      paramArrayOfint = new int[0]; 
    if (paramArrayOfString == null)
      paramArrayOfString = new String[0]; 
    builder.writeAttributionChain(paramArrayOfint, paramArrayOfString);
    builder.writeString(paramString);
    builder.writeInt(paramInt2);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int[] paramArrayOfint, String[] paramArrayOfString, String paramString, int paramInt2, int paramInt3) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    if (paramArrayOfint == null)
      paramArrayOfint = new int[0]; 
    if (paramArrayOfString == null)
      paramArrayOfString = new String[0]; 
    builder.writeAttributionChain(paramArrayOfint, paramArrayOfString);
    builder.writeString(paramString);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int[] paramArrayOfint, String[] paramArrayOfString, String paramString, int paramInt2, int paramInt3, int paramInt4, int paramInt5, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, boolean paramBoolean7, boolean paramBoolean8) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    if (paramArrayOfint == null)
      paramArrayOfint = new int[0]; 
    if (paramArrayOfString == null)
      paramArrayOfString = new String[0]; 
    builder.writeAttributionChain(paramArrayOfint, paramArrayOfString);
    builder.writeString(paramString);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    builder.writeInt(paramInt4);
    builder.writeInt(paramInt5);
    builder.writeBoolean(paramBoolean1);
    builder.writeBoolean(paramBoolean2);
    builder.writeBoolean(paramBoolean3);
    builder.writeBoolean(paramBoolean4);
    builder.writeBoolean(paramBoolean5);
    builder.writeBoolean(paramBoolean6);
    builder.writeBoolean(paramBoolean7);
    builder.writeBoolean(paramBoolean8);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int[] paramArrayOfint, String[] paramArrayOfString, String paramString1, String paramString2, int paramInt2) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    if (paramArrayOfint == null)
      paramArrayOfint = new int[0]; 
    if (paramArrayOfString == null)
      paramArrayOfString = new String[0]; 
    builder.writeAttributionChain(paramArrayOfint, paramArrayOfString);
    builder.writeString(paramString1);
    builder.writeString(paramString2);
    builder.writeInt(paramInt2);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt, boolean paramBoolean) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt);
    builder.writeBoolean(paramBoolean);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, boolean paramBoolean, String paramString, long paramLong1, int paramInt2, long paramLong2) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeBoolean(paramBoolean);
    builder.writeString(paramString);
    builder.writeLong(paramLong1);
    builder.writeInt(paramInt2);
    builder.writeLong(paramLong2);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt, boolean paramBoolean, String paramString, long paramLong1, long paramLong2) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt);
    builder.writeBoolean(paramBoolean);
    builder.writeString(paramString);
    builder.writeLong(paramLong1);
    builder.writeLong(paramLong2);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt, boolean paramBoolean, String paramString1, String paramString2) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt);
    builder.writeBoolean(paramBoolean);
    builder.writeString(paramString1);
    builder.writeString(paramString2);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    if (40 == paramInt1)
      builder.addBooleanAnnotation((byte)2, true); 
    builder.writeInt(paramInt2);
    if (14 == paramInt1) {
      builder.addBooleanAnnotation((byte)4, true);
      builder.addBooleanAnnotation((byte)8, false);
    } 
    if (15 == paramInt1)
      builder.addBooleanAnnotation((byte)4, true); 
    if (20 == paramInt1) {
      builder.addBooleanAnnotation((byte)4, true);
      builder.addBooleanAnnotation((byte)8, false);
    } 
    if (21 == paramInt1) {
      builder.addBooleanAnnotation((byte)4, true);
      builder.addBooleanAnnotation((byte)8, false);
    } 
    if (22 == paramInt1) {
      builder.addBooleanAnnotation((byte)4, true);
      builder.addBooleanAnnotation((byte)8, false);
    } 
    if (29 == paramInt1) {
      builder.addBooleanAnnotation((byte)4, true);
      builder.addBooleanAnnotation((byte)8, false);
    } 
    if (31 == paramInt1) {
      builder.addBooleanAnnotation((byte)4, true);
      builder.addBooleanAnnotation((byte)8, false);
    } 
    if (32 == paramInt1) {
      builder.addBooleanAnnotation((byte)4, true);
      builder.addBooleanAnnotation((byte)8, false);
    } 
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, int paramInt3) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    if (27 == paramInt1) {
      builder.addBooleanAnnotation((byte)1, true);
      builder.addBooleanAnnotation((byte)3, true);
    } 
    if (276 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    builder.writeInt(paramInt3);
    if (27 == paramInt1) {
      builder.addBooleanAnnotation((byte)4, true);
      builder.addBooleanAnnotation((byte)8, false);
    } 
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    builder.writeBoolean(paramBoolean);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, int paramInt4, long paramLong) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    builder.writeBoolean(paramBoolean1);
    builder.writeBoolean(paramBoolean2);
    builder.writeBoolean(paramBoolean3);
    builder.writeInt(paramInt4);
    builder.writeLong(paramLong);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean1, int paramInt4, boolean paramBoolean2, int paramInt5, long paramLong, boolean paramBoolean3) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    builder.writeBoolean(paramBoolean1);
    builder.writeInt(paramInt4);
    builder.writeBoolean(paramBoolean2);
    builder.writeInt(paramInt5);
    builder.writeLong(paramLong);
    builder.writeBoolean(paramBoolean3);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean1, int paramInt4, int paramInt5, int paramInt6, int paramInt7, boolean paramBoolean2) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    builder.writeBoolean(paramBoolean1);
    builder.writeInt(paramInt4);
    builder.writeInt(paramInt5);
    builder.writeInt(paramInt6);
    builder.writeInt(paramInt7);
    builder.writeBoolean(paramBoolean2);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean1, int paramInt4, int paramInt5, int paramInt6, int paramInt7, boolean paramBoolean2, long paramLong) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    builder.writeBoolean(paramBoolean1);
    builder.writeInt(paramInt4);
    builder.writeInt(paramInt5);
    builder.writeInt(paramInt6);
    builder.writeInt(paramInt7);
    builder.writeBoolean(paramBoolean2);
    builder.writeLong(paramLong);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    if (280 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    builder.writeInt(paramInt3);
    builder.writeInt(paramInt4);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, int paramInt5, int paramInt6, String paramString1, String paramString2, int paramInt7, int paramInt8, boolean paramBoolean4, boolean paramBoolean5, int paramInt9) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    builder.writeInt(paramInt4);
    builder.writeBoolean(paramBoolean1);
    builder.writeBoolean(paramBoolean2);
    builder.writeBoolean(paramBoolean3);
    builder.writeInt(paramInt5);
    builder.writeInt(paramInt6);
    builder.writeString(paramString1);
    builder.writeString(paramString2);
    builder.writeInt(paramInt7);
    builder.writeInt(paramInt8);
    builder.writeBoolean(paramBoolean4);
    builder.writeBoolean(paramBoolean5);
    builder.writeInt(paramInt9);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    if (256 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    if (279 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    builder.writeInt(paramInt3);
    builder.writeInt(paramInt4);
    builder.writeInt(paramInt5);
    builder.writeInt(paramInt6);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, int paramInt3, int paramInt4, String paramString, int paramInt5) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    builder.writeInt(paramInt4);
    builder.writeString(paramString);
    builder.writeInt(paramInt5);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, int paramInt3, int paramInt4, String paramString1, String paramString2, boolean paramBoolean) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    builder.writeInt(paramInt4);
    builder.writeString(paramString1);
    builder.writeString(paramString2);
    builder.writeBoolean(paramBoolean);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, int paramInt3, long paramLong, boolean paramBoolean) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    builder.writeLong(paramLong);
    builder.writeBoolean(paramBoolean);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, int paramInt3, float paramFloat, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, int paramInt10, int paramInt11, int paramInt12, int paramInt13, int paramInt14, int paramInt15, int paramInt16, int paramInt17) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    builder.writeFloat(paramFloat);
    builder.writeInt(paramInt4);
    builder.writeInt(paramInt5);
    builder.writeInt(paramInt6);
    builder.writeInt(paramInt7);
    builder.writeInt(paramInt8);
    builder.writeInt(paramInt9);
    builder.writeInt(paramInt10);
    builder.writeInt(paramInt11);
    builder.writeInt(paramInt12);
    builder.writeInt(paramInt13);
    builder.writeInt(paramInt14);
    builder.writeInt(paramInt15);
    builder.writeInt(paramInt16);
    builder.writeInt(paramInt17);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, int paramInt3, String paramString) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    builder.writeString(paramString);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, int paramInt3, String paramString, int paramInt4) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    if (90 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    builder.writeString(paramString);
    builder.writeInt(paramInt4);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, int paramInt3, String paramString, int paramInt4, int paramInt5, boolean paramBoolean) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    if (281 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    builder.writeString(paramString);
    builder.writeInt(paramInt4);
    builder.writeInt(paramInt5);
    builder.writeBoolean(paramBoolean);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, int paramInt3, String paramString, int paramInt4, int paramInt5, int paramInt6) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    if (246 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    builder.writeString(paramString);
    builder.writeInt(paramInt4);
    builder.writeInt(paramInt5);
    builder.writeInt(paramInt6);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, int paramInt3, String paramString1, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, boolean paramBoolean, String paramString2, int paramInt9, int paramInt10, int paramInt11, int paramInt12, int paramInt13, int paramInt14, int paramInt15, int paramInt16, int paramInt17, int paramInt18, float paramFloat) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    if (244 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    builder.writeString(paramString1);
    builder.writeInt(paramInt4);
    builder.writeInt(paramInt5);
    builder.writeInt(paramInt6);
    builder.writeInt(paramInt7);
    builder.writeInt(paramInt8);
    builder.writeBoolean(paramBoolean);
    builder.writeString(paramString2);
    builder.writeInt(paramInt9);
    builder.writeInt(paramInt10);
    builder.writeInt(paramInt11);
    builder.writeInt(paramInt12);
    builder.writeInt(paramInt13);
    builder.writeInt(paramInt14);
    builder.writeInt(paramInt15);
    builder.writeInt(paramInt16);
    builder.writeInt(paramInt17);
    builder.writeInt(paramInt18);
    builder.writeFloat(paramFloat);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, int paramInt3, String paramString, int paramInt4, int paramInt5, int paramInt6, int paramInt7, long paramLong, int paramInt8, int paramInt9, int paramInt10, int paramInt11) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    builder.writeString(paramString);
    builder.writeInt(paramInt4);
    builder.writeInt(paramInt5);
    builder.writeInt(paramInt6);
    builder.writeInt(paramInt7);
    builder.writeLong(paramLong);
    builder.writeInt(paramInt8);
    builder.writeInt(paramInt9);
    builder.writeInt(paramInt10);
    builder.writeInt(paramInt11);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, int paramInt3, String paramString1, int paramInt4, long paramLong, int paramInt5, int paramInt6, String paramString2, String paramString3) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    if (169 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    builder.writeInt(paramInt3);
    builder.writeString(paramString1);
    builder.writeInt(paramInt4);
    builder.writeLong(paramLong);
    builder.writeInt(paramInt5);
    builder.writeInt(paramInt6);
    builder.writeString(paramString2);
    builder.writeString(paramString3);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, int paramInt3, String paramString, long paramLong) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    builder.writeString(paramString);
    builder.writeLong(paramLong);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, long paramLong) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeLong(paramLong);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, long paramLong, int paramInt3) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeLong(paramLong);
    builder.writeInt(paramInt3);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, long paramLong, int paramInt3, int paramInt4) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    if (228 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    builder.writeLong(paramLong);
    builder.writeInt(paramInt3);
    builder.writeInt(paramInt4);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, long paramLong1, long paramLong2, int paramInt3) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    if (298 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    if (299 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    if (300 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    builder.writeLong(paramLong1);
    builder.writeLong(paramLong2);
    builder.writeInt(paramInt3);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, String paramString) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeString(paramString);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, String paramString, boolean paramBoolean, int paramInt3) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    if (59 == paramInt1) {
      builder.addBooleanAnnotation((byte)1, true);
      builder.addBooleanAnnotation((byte)3, true);
    } 
    builder.writeString(paramString);
    if (59 == paramInt1)
      builder.addBooleanAnnotation((byte)3, true); 
    builder.writeBoolean(paramBoolean);
    builder.writeInt(paramInt3);
    if (59 == paramInt1) {
      builder.addBooleanAnnotation((byte)4, true);
      builder.addBooleanAnnotation((byte)8, false);
    } 
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, String paramString, int paramInt3) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    if (28 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    if (52 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    builder.writeString(paramString);
    builder.writeInt(paramInt3);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, String paramString, int paramInt3, boolean paramBoolean) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    if (60 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    if (178 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    builder.writeString(paramString);
    builder.writeInt(paramInt3);
    builder.writeBoolean(paramBoolean);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, String paramString, int paramInt3, boolean paramBoolean, long paramLong, byte[] paramArrayOfbyte) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeString(paramString);
    builder.writeInt(paramInt3);
    builder.writeBoolean(paramBoolean);
    builder.writeLong(paramLong);
    if (paramArrayOfbyte == null)
      paramArrayOfbyte = new byte[0]; 
    builder.writeByteArray(paramArrayOfbyte);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, String paramString, int paramInt3, int paramInt4) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeString(paramString);
    builder.writeInt(paramInt3);
    builder.writeInt(paramInt4);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, String paramString1, int paramInt3, int paramInt4, String paramString2, byte[] paramArrayOfbyte) {
    byte[] arrayOfByte;
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeString(paramString1);
    builder.writeInt(paramInt3);
    builder.writeInt(paramInt4);
    builder.writeString(paramString2);
    if (paramArrayOfbyte == null) {
      arrayOfByte = new byte[0];
    } else {
      arrayOfByte = paramArrayOfbyte;
    } 
    builder.writeByteArray(arrayOfByte);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, String paramString, int paramInt3, long paramLong1, long paramLong2, long paramLong3, long paramLong4, long paramLong5, long paramLong6, long paramLong7, long paramLong8, long paramLong9, int paramInt4, long paramLong10, int paramInt5, int paramInt6, long paramLong11, long paramLong12) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeString(paramString);
    builder.writeInt(paramInt3);
    builder.writeLong(paramLong1);
    builder.writeLong(paramLong2);
    builder.writeLong(paramLong3);
    builder.writeLong(paramLong4);
    builder.writeLong(paramLong5);
    builder.writeLong(paramLong6);
    builder.writeLong(paramLong7);
    builder.writeLong(paramLong8);
    builder.writeLong(paramLong9);
    builder.writeInt(paramInt4);
    builder.writeLong(paramLong10);
    builder.writeInt(paramInt5);
    builder.writeInt(paramInt6);
    builder.writeLong(paramLong11);
    builder.writeLong(paramLong12);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, String paramString1, int paramInt3, String paramString2) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    if (49 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    builder.writeString(paramString1);
    builder.writeInt(paramInt3);
    builder.writeString(paramString2);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, String paramString1, int paramInt3, String paramString2, boolean paramBoolean, long paramLong) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    if (50 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    builder.writeString(paramString1);
    builder.writeInt(paramInt3);
    builder.writeString(paramString2);
    builder.writeBoolean(paramBoolean);
    builder.writeLong(paramLong);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, String paramString1, int paramInt3, String paramString2, int paramInt4, int paramInt5, boolean paramBoolean, int paramInt6, int paramInt7) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeString(paramString1);
    builder.writeInt(paramInt3);
    builder.writeString(paramString2);
    builder.writeInt(paramInt4);
    builder.writeInt(paramInt5);
    builder.writeBoolean(paramBoolean);
    builder.writeInt(paramInt6);
    builder.writeInt(paramInt7);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, String paramString1, int paramInt3, String paramString2, String paramString3, boolean paramBoolean, long paramLong, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, String paramString4, int paramInt9, int paramInt10) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    if (48 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    builder.writeString(paramString1);
    builder.writeInt(paramInt3);
    builder.writeString(paramString2);
    builder.writeString(paramString3);
    builder.writeBoolean(paramBoolean);
    builder.writeLong(paramLong);
    builder.writeInt(paramInt4);
    builder.writeInt(paramInt5);
    builder.writeInt(paramInt6);
    builder.writeInt(paramInt7);
    builder.writeInt(paramInt8);
    builder.writeString(paramString4);
    builder.writeInt(paramInt9);
    builder.writeInt(paramInt10);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, String paramString1, int paramInt3, String paramString2, String paramString3, String paramString4, int paramInt4, int paramInt5, int paramInt6) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    if (44 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    builder.writeString(paramString1);
    builder.writeInt(paramInt3);
    builder.writeString(paramString2);
    builder.writeString(paramString3);
    builder.writeString(paramString4);
    builder.writeInt(paramInt4);
    builder.writeInt(paramInt5);
    builder.writeInt(paramInt6);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, String paramString, long paramLong) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    if (70 == paramInt1) {
      builder.addBooleanAnnotation((byte)4, true);
      builder.addBooleanAnnotation((byte)8, false);
    } 
    builder.writeString(paramString);
    if (70 == paramInt1)
      builder.addBooleanAnnotation((byte)3, true); 
    builder.writeLong(paramLong);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, String paramString1, String paramString2) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    if (100 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    builder.writeString(paramString1);
    builder.writeString(paramString2);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, String paramString1, String paramString2, int paramInt3) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    if (42 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    if (99 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    if (269 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    builder.writeString(paramString1);
    builder.writeString(paramString2);
    builder.writeInt(paramInt3);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, String paramString1, String paramString2, int paramInt3, int paramInt4) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    if (80 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    builder.writeString(paramString1);
    builder.writeString(paramString2);
    builder.writeInt(paramInt3);
    builder.writeInt(paramInt4);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, String paramString1, String paramString2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, int paramInt10, int paramInt11) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeString(paramString1);
    builder.writeString(paramString2);
    builder.writeInt(paramInt3);
    builder.writeInt(paramInt4);
    builder.writeInt(paramInt5);
    builder.writeInt(paramInt6);
    builder.writeInt(paramInt7);
    builder.writeInt(paramInt8);
    builder.writeInt(paramInt9);
    builder.writeInt(paramInt10);
    builder.writeInt(paramInt11);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, String paramString1, String paramString2, int paramInt3, long paramLong) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeString(paramString1);
    builder.writeString(paramString2);
    builder.writeInt(paramInt3);
    builder.writeLong(paramLong);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, String paramString1, String paramString2, int paramInt3, String paramString3, int paramInt4, int paramInt5, int paramInt6) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    if (78 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    builder.writeString(paramString1);
    builder.writeString(paramString2);
    builder.writeInt(paramInt3);
    builder.writeString(paramString3);
    builder.writeInt(paramInt4);
    builder.writeInt(paramInt5);
    builder.writeInt(paramInt6);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, String paramString1, String paramString2, long paramLong) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeString(paramString1);
    builder.writeString(paramString2);
    builder.writeLong(paramLong);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, String paramString1, String paramString2, long paramLong1, long paramLong2) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeString(paramString1);
    builder.writeString(paramString2);
    builder.writeLong(paramLong1);
    builder.writeLong(paramLong2);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, String paramString1, String paramString2, long paramLong1, long paramLong2, long paramLong3, int paramInt3, long paramLong4, long paramLong5) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    builder.writeString(paramString1);
    builder.writeString(paramString2);
    builder.writeLong(paramLong1);
    builder.writeLong(paramLong2);
    builder.writeLong(paramLong3);
    builder.writeInt(paramInt3);
    builder.writeLong(paramLong4);
    builder.writeLong(paramLong5);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, String paramString1, String paramString2, long paramLong1, long paramLong2, long paramLong3, long paramLong4, long paramLong5) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    if (55 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    builder.writeString(paramString1);
    builder.writeString(paramString2);
    builder.writeLong(paramLong1);
    builder.writeLong(paramLong2);
    builder.writeLong(paramLong3);
    builder.writeLong(paramLong4);
    builder.writeLong(paramLong5);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, String paramString1, String paramString2, String paramString3, int paramInt3, int paramInt4, int paramInt5, String paramString4) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    if (79 == paramInt1)
      builder.addBooleanAnnotation((byte)1, true); 
    builder.writeString(paramString1);
    builder.writeString(paramString2);
    builder.writeString(paramString3);
    builder.writeInt(paramInt3);
    builder.writeInt(paramInt4);
    builder.writeInt(paramInt5);
    builder.writeString(paramString4);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, int paramInt2, SparseArray<Object> paramSparseArray) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeInt(paramInt2);
    paramInt2 = paramSparseArray.size();
    SparseIntArray sparseIntArray1 = null;
    SparseLongArray sparseLongArray = null;
    SparseIntArray sparseIntArray2 = null;
    SparseIntArray sparseIntArray3 = null;
    SparseArray sparseArray;
    for (paramInt1 = 0; paramInt1 < paramInt2; paramInt1++, sparseIntArray1 = sparseIntArray5, sparseLongArray = sparseLongArray1, sparseIntArray2 = sparseIntArray4, sparseArray = sparseArray1) {
      SparseIntArray sparseIntArray4, sparseIntArray5;
      SparseLongArray sparseLongArray1;
      SparseArray sparseArray1;
      int i = paramSparseArray.keyAt(paramInt1);
      Object object = paramSparseArray.valueAt(paramInt1);
      if (object instanceof Integer) {
        sparseIntArray4 = sparseIntArray1;
        if (sparseIntArray1 == null)
          sparseIntArray4 = new SparseIntArray(); 
        sparseIntArray4.put(i, ((Integer)object).intValue());
        sparseIntArray5 = sparseIntArray4;
        sparseLongArray1 = sparseLongArray;
        sparseIntArray4 = sparseIntArray2;
        SparseIntArray sparseIntArray = sparseIntArray3;
      } else if (object instanceof Long) {
        SparseLongArray sparseLongArray2 = sparseLongArray;
        if (sparseLongArray == null)
          sparseLongArray2 = new SparseLongArray(); 
        sparseLongArray2.put(i, ((Long)object).longValue());
        sparseIntArray5 = sparseIntArray1;
        sparseLongArray1 = sparseLongArray2;
        sparseIntArray4 = sparseIntArray2;
        SparseIntArray sparseIntArray = sparseIntArray3;
      } else if (object instanceof String) {
        SparseArray sparseArray2;
        SparseIntArray sparseIntArray6 = sparseIntArray2;
        if (sparseIntArray2 == null)
          sparseArray2 = new SparseArray(); 
        sparseArray2.put(i, object);
        sparseIntArray5 = sparseIntArray1;
        sparseLongArray1 = sparseLongArray;
        SparseIntArray sparseIntArray7 = sparseIntArray3;
      } else {
        sparseIntArray5 = sparseIntArray1;
        sparseLongArray1 = sparseLongArray;
        sparseIntArray4 = sparseIntArray2;
        SparseIntArray sparseIntArray = sparseIntArray3;
        if (object instanceof Float) {
          SparseArray sparseArray2;
          sparseIntArray4 = sparseIntArray3;
          if (sparseIntArray3 == null)
            sparseArray2 = new SparseArray(); 
          sparseArray2.put(i, object);
          sparseArray1 = sparseArray2;
          sparseIntArray4 = sparseIntArray2;
          sparseLongArray1 = sparseLongArray;
          sparseIntArray5 = sparseIntArray1;
        } 
      } 
    } 
    builder.writeKeyValuePairs(sparseIntArray1, sparseLongArray, (SparseArray)sparseIntArray2, sparseArray);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt, long paramLong) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt);
    builder.writeLong(paramLong);
    if (65 == paramInt)
      builder.addBooleanAnnotation((byte)4, true); 
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, long paramLong, int paramInt2, int paramInt3, int paramInt4) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeLong(paramLong);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    builder.writeInt(paramInt4);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, long paramLong, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeLong(paramLong);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    builder.writeInt(paramInt4);
    builder.writeInt(paramInt5);
    builder.writeInt(paramInt6);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, long paramLong, int paramInt2, String paramString, int paramInt3) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeLong(paramLong);
    builder.writeInt(paramInt2);
    builder.writeString(paramString);
    builder.writeInt(paramInt3);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, long paramLong1, long paramLong2, int paramInt2, boolean paramBoolean) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeLong(paramLong1);
    builder.writeLong(paramLong2);
    builder.writeInt(paramInt2);
    builder.writeBoolean(paramBoolean);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt, String paramString) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt);
    builder.writeString(paramString);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, String paramString, int paramInt2) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeString(paramString);
    builder.writeInt(paramInt2);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, String paramString, int paramInt2, int paramInt3) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeString(paramString);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, String paramString, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean1, boolean paramBoolean2, int paramInt5) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeString(paramString);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    builder.writeInt(paramInt4);
    builder.writeBoolean(paramBoolean1);
    builder.writeBoolean(paramBoolean2);
    builder.writeInt(paramInt5);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, String paramString, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeString(paramString);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    builder.writeInt(paramInt4);
    builder.writeInt(paramInt5);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, String paramString, int paramInt2, int paramInt3, float paramFloat) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeString(paramString);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    builder.writeFloat(paramFloat);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, String paramString1, int paramInt2, String paramString2, int paramInt3, int paramInt4, int paramInt5, boolean paramBoolean1, int paramInt6, int paramInt7, boolean paramBoolean2, int paramInt8, int paramInt9, String paramString3) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeString(paramString1);
    builder.writeInt(paramInt2);
    builder.writeString(paramString2);
    builder.writeInt(paramInt3);
    builder.writeInt(paramInt4);
    builder.writeInt(paramInt5);
    builder.writeBoolean(paramBoolean1);
    builder.writeInt(paramInt6);
    builder.writeInt(paramInt7);
    builder.writeBoolean(paramBoolean2);
    builder.writeInt(paramInt8);
    builder.writeInt(paramInt9);
    builder.writeString(paramString3);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, String paramString1, int paramInt2, String paramString2, int paramInt3, String paramString3, int paramInt4, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeString(paramString1);
    builder.writeInt(paramInt2);
    builder.writeString(paramString2);
    builder.writeInt(paramInt3);
    builder.writeString(paramString3);
    builder.writeInt(paramInt4);
    builder.writeBoolean(paramBoolean1);
    builder.writeBoolean(paramBoolean2);
    builder.writeBoolean(paramBoolean3);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt, String paramString, long paramLong) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt);
    builder.writeString(paramString);
    builder.writeLong(paramLong);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt, String paramString, long paramLong1, long paramLong2, boolean paramBoolean) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt);
    builder.writeString(paramString);
    builder.writeLong(paramLong1);
    builder.writeLong(paramLong2);
    builder.writeBoolean(paramBoolean);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt, String paramString1, String paramString2) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt);
    builder.writeString(paramString1);
    builder.writeString(paramString2);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, String paramString1, String paramString2, long paramLong, String paramString3, int paramInt2, boolean paramBoolean1, boolean paramBoolean2) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeString(paramString1);
    builder.writeString(paramString2);
    builder.writeLong(paramLong);
    builder.writeString(paramString3);
    builder.writeInt(paramInt2);
    builder.writeBoolean(paramBoolean1);
    builder.writeBoolean(paramBoolean2);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, boolean paramBoolean, int paramInt2, int paramInt3) {
    StatsEvent.Builder builder = StatsEvent.newBuilder();
    builder.setAtomId(paramInt1);
    builder.writeString(paramString1);
    builder.writeString(paramString2);
    builder.writeString(paramString3);
    builder.writeString(paramString4);
    builder.writeString(paramString5);
    builder.writeBoolean(paramBoolean);
    builder.writeInt(paramInt2);
    builder.writeInt(paramInt3);
    builder.usePooledBuffer();
    StatsLog.write(builder.build());
  }
  
  public static void write_non_chained(int paramInt1, int paramInt2, String paramString, int paramInt3) {
    write(paramInt1, new int[] { paramInt2 }, new String[] { paramString }, paramInt3);
  }
  
  public static void write_non_chained(int paramInt1, int paramInt2, String paramString, int paramInt3, int paramInt4) {
    write(paramInt1, new int[] { paramInt2 }, new String[] { paramString }, paramInt3, paramInt4);
  }
  
  public static void write_non_chained(int paramInt1, int paramInt2, String paramString1, int paramInt3, int paramInt4, String paramString2) {
    write(paramInt1, new int[] { paramInt2 }, new String[] { paramString1 }, paramInt3, paramInt4, paramString2);
  }
  
  public static void write_non_chained(int paramInt1, int paramInt2, String paramString, int paramInt3, long paramLong) {
    write(paramInt1, new int[] { paramInt2 }, new String[] { paramString }, paramInt3, paramLong);
  }
  
  public static void write_non_chained(int paramInt1, int paramInt2, String paramString1, int paramInt3, String paramString2, int paramInt4) {
    write(paramInt1, new int[] { paramInt2 }, new String[] { paramString1 }, paramInt3, paramString2, paramInt4);
  }
  
  public static void write_non_chained(int paramInt1, int paramInt2, String paramString1, String paramString2, int paramInt3) {
    write(paramInt1, new int[] { paramInt2 }, new String[] { paramString1 }, paramString2, paramInt3);
  }
  
  public static void write_non_chained(int paramInt1, int paramInt2, String paramString1, String paramString2, int paramInt3, int paramInt4) {
    write(paramInt1, new int[] { paramInt2 }, new String[] { paramString1 }, paramString2, paramInt3, paramInt4);
  }
  
  public static void write_non_chained(int paramInt1, int paramInt2, String paramString1, String paramString2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, boolean paramBoolean7, boolean paramBoolean8) {
    write(paramInt1, new int[] { paramInt2 }, new String[] { paramString1 }, paramString2, paramInt3, paramInt4, paramInt5, paramInt6, paramBoolean1, paramBoolean2, paramBoolean3, paramBoolean4, paramBoolean5, paramBoolean6, paramBoolean7, paramBoolean8);
  }
  
  public static void write_non_chained(int paramInt1, int paramInt2, String paramString1, String paramString2, String paramString3, int paramInt3) {
    write(paramInt1, new int[] { paramInt2 }, new String[] { paramString1 }, paramString2, paramString3, paramInt3);
  }
  
  public static void write(int paramInt1, WorkSource paramWorkSource, int paramInt2) {
    for (byte b = 0; b < paramWorkSource.size(); b++)
      write_non_chained(paramInt1, paramWorkSource.getUid(b), paramWorkSource.getPackageName(b), paramInt2); 
    List list = paramWorkSource.getWorkChains();
    if (list != null)
      for (WorkSource.WorkChain workChain : list)
        write(paramInt1, workChain.getUids(), workChain.getTags(), paramInt2);  
  }
  
  public static void write(int paramInt1, WorkSource paramWorkSource, int paramInt2, int paramInt3) {
    for (byte b = 0; b < paramWorkSource.size(); b++)
      write_non_chained(paramInt1, paramWorkSource.getUid(b), paramWorkSource.getPackageName(b), paramInt2, paramInt3); 
    List list = paramWorkSource.getWorkChains();
    if (list != null)
      for (WorkSource.WorkChain workChain : list)
        write(paramInt1, workChain.getUids(), workChain.getTags(), paramInt2, paramInt3);  
  }
  
  public static void write(int paramInt1, WorkSource paramWorkSource, int paramInt2, int paramInt3, String paramString) {
    for (byte b = 0; b < paramWorkSource.size(); b++)
      write_non_chained(paramInt1, paramWorkSource.getUid(b), paramWorkSource.getPackageName(b), paramInt2, paramInt3, paramString); 
    List list = paramWorkSource.getWorkChains();
    if (list != null)
      for (WorkSource.WorkChain workChain : list)
        write(paramInt1, workChain.getUids(), workChain.getTags(), paramInt2, paramInt3, paramString);  
  }
  
  public static void write(int paramInt1, WorkSource paramWorkSource, int paramInt2, long paramLong) {
    for (byte b = 0; b < paramWorkSource.size(); b++)
      write_non_chained(paramInt1, paramWorkSource.getUid(b), paramWorkSource.getPackageName(b), paramInt2, paramLong); 
    List list = paramWorkSource.getWorkChains();
    if (list != null)
      for (WorkSource.WorkChain workChain : list)
        write(paramInt1, workChain.getUids(), workChain.getTags(), paramInt2, paramLong);  
  }
  
  public static void write(int paramInt1, WorkSource paramWorkSource, int paramInt2, String paramString, int paramInt3) {
    for (byte b = 0; b < paramWorkSource.size(); b++)
      write_non_chained(paramInt1, paramWorkSource.getUid(b), paramWorkSource.getPackageName(b), paramInt2, paramString, paramInt3); 
    List list = paramWorkSource.getWorkChains();
    if (list != null)
      for (WorkSource.WorkChain workChain : list)
        write(paramInt1, workChain.getUids(), workChain.getTags(), paramInt2, paramString, paramInt3);  
  }
  
  public static void write(int paramInt1, WorkSource paramWorkSource, String paramString, int paramInt2) {
    for (byte b = 0; b < paramWorkSource.size(); b++)
      write_non_chained(paramInt1, paramWorkSource.getUid(b), paramWorkSource.getPackageName(b), paramString, paramInt2); 
    List list = paramWorkSource.getWorkChains();
    if (list != null)
      for (WorkSource.WorkChain workChain : list)
        write(paramInt1, workChain.getUids(), workChain.getTags(), paramString, paramInt2);  
  }
  
  public static void write(int paramInt1, WorkSource paramWorkSource, String paramString, int paramInt2, int paramInt3) {
    for (byte b = 0; b < paramWorkSource.size(); b++)
      write_non_chained(paramInt1, paramWorkSource.getUid(b), paramWorkSource.getPackageName(b), paramString, paramInt2, paramInt3); 
    List list = paramWorkSource.getWorkChains();
    if (list != null)
      for (WorkSource.WorkChain workChain : list)
        write(paramInt1, workChain.getUids(), workChain.getTags(), paramString, paramInt2, paramInt3);  
  }
  
  public static void write(int paramInt1, WorkSource paramWorkSource, String paramString, int paramInt2, int paramInt3, int paramInt4, int paramInt5, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, boolean paramBoolean7, boolean paramBoolean8) {
    for (byte b = 0; b < paramWorkSource.size(); b++)
      write_non_chained(paramInt1, paramWorkSource.getUid(b), paramWorkSource.getPackageName(b), paramString, paramInt2, paramInt3, paramInt4, paramInt5, paramBoolean1, paramBoolean2, paramBoolean3, paramBoolean4, paramBoolean5, paramBoolean6, paramBoolean7, paramBoolean8); 
    List list = paramWorkSource.getWorkChains();
    if (list != null)
      for (WorkSource.WorkChain workChain : list)
        write(paramInt1, workChain.getUids(), workChain.getTags(), paramString, paramInt2, paramInt3, paramInt4, paramInt5, paramBoolean1, paramBoolean2, paramBoolean3, paramBoolean4, paramBoolean5, paramBoolean6, paramBoolean7, paramBoolean8);  
  }
  
  public static void write(int paramInt1, WorkSource paramWorkSource, String paramString1, String paramString2, int paramInt2) {
    for (byte b = 0; b < paramWorkSource.size(); b++)
      write_non_chained(paramInt1, paramWorkSource.getUid(b), paramWorkSource.getPackageName(b), paramString1, paramString2, paramInt2); 
    List list = paramWorkSource.getWorkChains();
    if (list != null)
      for (WorkSource.WorkChain workChain : list)
        write(paramInt1, workChain.getUids(), workChain.getTags(), paramString1, paramString2, paramInt2);  
  }
}
