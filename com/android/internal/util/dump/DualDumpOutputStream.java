package com.android.internal.util.dump;

import android.util.Log;
import android.util.proto.ProtoOutputStream;
import com.android.internal.util.IndentingPrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;

public class DualDumpOutputStream {
  private static final String LOG_TAG = DualDumpOutputStream.class.getSimpleName();
  
  private final LinkedList<DumpObject> mDumpObjects;
  
  private final IndentingPrintWriter mIpw;
  
  private final ProtoOutputStream mProtoStream;
  
  private static abstract class Dumpable {
    final String name;
    
    abstract void print(IndentingPrintWriter param1IndentingPrintWriter, boolean param1Boolean);
    
    private Dumpable(String param1String) {
      this.name = param1String;
    }
  }
  
  class DumpObject extends Dumpable {
    private final LinkedHashMap<String, ArrayList<DualDumpOutputStream.Dumpable>> mSubObjects = new LinkedHashMap<>();
    
    void print(IndentingPrintWriter param1IndentingPrintWriter, boolean param1Boolean) {
      if (param1Boolean) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.name);
        stringBuilder.append("={");
        param1IndentingPrintWriter.println(stringBuilder.toString());
      } else {
        param1IndentingPrintWriter.println("{");
      } 
      param1IndentingPrintWriter.increaseIndent();
      for (ArrayList<DualDumpOutputStream.Dumpable> arrayList : this.mSubObjects.values()) {
        int i = arrayList.size();
        if (i == 1) {
          ((DualDumpOutputStream.Dumpable)arrayList.get(0)).print(param1IndentingPrintWriter, true);
          continue;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(((DualDumpOutputStream.Dumpable)arrayList.get(0)).name);
        stringBuilder.append("=[");
        param1IndentingPrintWriter.println(stringBuilder.toString());
        param1IndentingPrintWriter.increaseIndent();
        for (byte b = 0; b < i; b++)
          ((DualDumpOutputStream.Dumpable)arrayList.get(b)).print(param1IndentingPrintWriter, false); 
        param1IndentingPrintWriter.decreaseIndent();
        param1IndentingPrintWriter.println("]");
      } 
      param1IndentingPrintWriter.decreaseIndent();
      param1IndentingPrintWriter.println("}");
    }
    
    public void add(String param1String, DualDumpOutputStream.Dumpable param1Dumpable) {
      ArrayList<DualDumpOutputStream.Dumpable> arrayList1 = this.mSubObjects.get(param1String);
      ArrayList<DualDumpOutputStream.Dumpable> arrayList2 = arrayList1;
      if (arrayList1 == null) {
        arrayList2 = new ArrayList(1);
        this.mSubObjects.put(param1String, arrayList2);
      } 
      arrayList2.add(param1Dumpable);
    }
    
    private DumpObject(DualDumpOutputStream this$0) {}
  }
  
  class DumpField extends Dumpable {
    private final String mValue;
    
    private DumpField(DualDumpOutputStream this$0, String param1String1) {
      this.mValue = param1String1;
    }
    
    void print(IndentingPrintWriter param1IndentingPrintWriter, boolean param1Boolean) {
      if (param1Boolean) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.name);
        stringBuilder.append("=");
        stringBuilder.append(this.mValue);
        param1IndentingPrintWriter.println(stringBuilder.toString());
      } else {
        param1IndentingPrintWriter.println(this.mValue);
      } 
    }
  }
  
  public DualDumpOutputStream(ProtoOutputStream paramProtoOutputStream) {
    this.mDumpObjects = new LinkedList<>();
    this.mProtoStream = paramProtoOutputStream;
    this.mIpw = null;
  }
  
  public DualDumpOutputStream(IndentingPrintWriter paramIndentingPrintWriter) {
    LinkedList<DumpObject> linkedList = new LinkedList();
    this.mProtoStream = null;
    this.mIpw = paramIndentingPrintWriter;
    linkedList.add(new DumpObject());
  }
  
  public void write(String paramString, long paramLong, double paramDouble) {
    ProtoOutputStream protoOutputStream = this.mProtoStream;
    if (protoOutputStream != null) {
      protoOutputStream.write(paramLong, paramDouble);
    } else {
      ((DumpObject)this.mDumpObjects.getLast()).add(paramString, new DumpField(String.valueOf(paramDouble)));
    } 
  }
  
  public void write(String paramString, long paramLong, boolean paramBoolean) {
    ProtoOutputStream protoOutputStream = this.mProtoStream;
    if (protoOutputStream != null) {
      protoOutputStream.write(paramLong, paramBoolean);
    } else {
      ((DumpObject)this.mDumpObjects.getLast()).add(paramString, new DumpField(String.valueOf(paramBoolean)));
    } 
  }
  
  public void write(String paramString, long paramLong, int paramInt) {
    ProtoOutputStream protoOutputStream = this.mProtoStream;
    if (protoOutputStream != null) {
      protoOutputStream.write(paramLong, paramInt);
    } else {
      ((DumpObject)this.mDumpObjects.getLast()).add(paramString, new DumpField(String.valueOf(paramInt)));
    } 
  }
  
  public void write(String paramString, long paramLong, float paramFloat) {
    ProtoOutputStream protoOutputStream = this.mProtoStream;
    if (protoOutputStream != null) {
      protoOutputStream.write(paramLong, paramFloat);
    } else {
      ((DumpObject)this.mDumpObjects.getLast()).add(paramString, new DumpField(String.valueOf(paramFloat)));
    } 
  }
  
  public void write(String paramString, long paramLong, byte[] paramArrayOfbyte) {
    ProtoOutputStream protoOutputStream = this.mProtoStream;
    if (protoOutputStream != null) {
      protoOutputStream.write(paramLong, paramArrayOfbyte);
    } else {
      ((DumpObject)this.mDumpObjects.getLast()).add(paramString, new DumpField(Arrays.toString(paramArrayOfbyte)));
    } 
  }
  
  public void write(String paramString, long paramLong1, long paramLong2) {
    ProtoOutputStream protoOutputStream = this.mProtoStream;
    if (protoOutputStream != null) {
      protoOutputStream.write(paramLong1, paramLong2);
    } else {
      ((DumpObject)this.mDumpObjects.getLast()).add(paramString, new DumpField(String.valueOf(paramLong2)));
    } 
  }
  
  public void write(String paramString1, long paramLong, String paramString2) {
    ProtoOutputStream protoOutputStream = this.mProtoStream;
    if (protoOutputStream != null) {
      protoOutputStream.write(paramLong, paramString2);
    } else {
      ((DumpObject)this.mDumpObjects.getLast()).add(paramString1, new DumpField(String.valueOf(paramString2)));
    } 
  }
  
  public long start(String paramString, long paramLong) {
    ProtoOutputStream protoOutputStream = this.mProtoStream;
    if (protoOutputStream != null)
      return protoOutputStream.start(paramLong); 
    DumpObject dumpObject = new DumpObject();
    ((DumpObject)this.mDumpObjects.getLast()).add(paramString, dumpObject);
    this.mDumpObjects.addLast(dumpObject);
    return System.identityHashCode(dumpObject);
  }
  
  public void end(long paramLong) {
    ProtoOutputStream protoOutputStream = this.mProtoStream;
    if (protoOutputStream != null) {
      protoOutputStream.end(paramLong);
    } else {
      if (System.identityHashCode(this.mDumpObjects.getLast()) != paramLong) {
        String str1 = LOG_TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unexpected token for ending ");
        stringBuilder.append(((DumpObject)this.mDumpObjects.getLast()).name);
        stringBuilder.append(" at ");
        stringBuilder.append(Arrays.toString((Object[])Thread.currentThread().getStackTrace()));
        String str2 = stringBuilder.toString();
        Log.w(str1, str2);
      } 
      this.mDumpObjects.removeLast();
    } 
  }
  
  public void flush() {
    ProtoOutputStream protoOutputStream = this.mProtoStream;
    if (protoOutputStream != null) {
      protoOutputStream.flush();
    } else {
      if (this.mDumpObjects.size() == 1) {
        ((DumpObject)this.mDumpObjects.getFirst()).print(this.mIpw, false);
        this.mDumpObjects.clear();
        this.mDumpObjects.add(new DumpObject());
      } 
      this.mIpw.flush();
    } 
  }
  
  public void writeNested(String paramString, byte[] paramArrayOfbyte) {
    if (this.mIpw == null) {
      Log.w(LOG_TAG, "writeNested does not work for proto logging");
      return;
    } 
    DumpObject dumpObject = this.mDumpObjects.getLast();
    String str = new String(paramArrayOfbyte, StandardCharsets.UTF_8);
    DumpField dumpField = new DumpField(str.trim());
    dumpObject.add(paramString, dumpField);
  }
  
  public boolean isProto() {
    boolean bool;
    if (this.mProtoStream != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
}
