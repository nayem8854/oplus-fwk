package com.android.internal.util;

import java.util.ArrayList;
import java.util.List;

public class CallbackRegistry<C, T, A> implements Cloneable {
  private static final String TAG = "CallbackRegistry";
  
  private List<C> mCallbacks = new ArrayList<>();
  
  private long mFirst64Removed = 0L;
  
  private int mNotificationLevel;
  
  private final NotifierCallback<C, T, A> mNotifier;
  
  private long[] mRemainderRemoved;
  
  public CallbackRegistry(NotifierCallback<C, T, A> paramNotifierCallback) {
    this.mNotifier = paramNotifierCallback;
  }
  
  public void notifyCallbacks(T paramT, int paramInt, A paramA) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_0
    //   4: getfield mNotificationLevel : I
    //   7: iconst_1
    //   8: iadd
    //   9: putfield mNotificationLevel : I
    //   12: aload_0
    //   13: aload_1
    //   14: iload_2
    //   15: aload_3
    //   16: invokespecial notifyRecurseLocked : (Ljava/lang/Object;ILjava/lang/Object;)V
    //   19: aload_0
    //   20: getfield mNotificationLevel : I
    //   23: iconst_1
    //   24: isub
    //   25: istore_2
    //   26: aload_0
    //   27: iload_2
    //   28: putfield mNotificationLevel : I
    //   31: iload_2
    //   32: ifne -> 117
    //   35: aload_0
    //   36: getfield mRemainderRemoved : [J
    //   39: ifnull -> 94
    //   42: aload_0
    //   43: getfield mRemainderRemoved : [J
    //   46: arraylength
    //   47: iconst_1
    //   48: isub
    //   49: istore_2
    //   50: iload_2
    //   51: iflt -> 94
    //   54: aload_0
    //   55: getfield mRemainderRemoved : [J
    //   58: iload_2
    //   59: laload
    //   60: lstore #4
    //   62: lload #4
    //   64: lconst_0
    //   65: lcmp
    //   66: ifeq -> 88
    //   69: aload_0
    //   70: iload_2
    //   71: iconst_1
    //   72: iadd
    //   73: bipush #64
    //   75: imul
    //   76: lload #4
    //   78: invokespecial removeRemovedCallbacks : (IJ)V
    //   81: aload_0
    //   82: getfield mRemainderRemoved : [J
    //   85: iload_2
    //   86: lconst_0
    //   87: lastore
    //   88: iinc #2, -1
    //   91: goto -> 50
    //   94: aload_0
    //   95: getfield mFirst64Removed : J
    //   98: lconst_0
    //   99: lcmp
    //   100: ifeq -> 117
    //   103: aload_0
    //   104: iconst_0
    //   105: aload_0
    //   106: getfield mFirst64Removed : J
    //   109: invokespecial removeRemovedCallbacks : (IJ)V
    //   112: aload_0
    //   113: lconst_0
    //   114: putfield mFirst64Removed : J
    //   117: aload_0
    //   118: monitorexit
    //   119: return
    //   120: astore_1
    //   121: aload_0
    //   122: monitorexit
    //   123: aload_1
    //   124: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #92	-> 2
    //   #93	-> 12
    //   #94	-> 19
    //   #95	-> 31
    //   #96	-> 35
    //   #97	-> 42
    //   #98	-> 54
    //   #99	-> 62
    //   #100	-> 69
    //   #101	-> 81
    //   #97	-> 88
    //   #105	-> 94
    //   #106	-> 103
    //   #107	-> 112
    //   #110	-> 117
    //   #91	-> 120
    // Exception table:
    //   from	to	target	type
    //   2	12	120	finally
    //   12	19	120	finally
    //   19	31	120	finally
    //   35	42	120	finally
    //   42	50	120	finally
    //   54	62	120	finally
    //   69	81	120	finally
    //   81	88	120	finally
    //   94	103	120	finally
    //   103	112	120	finally
    //   112	117	120	finally
  }
  
  private void notifyFirst64Locked(T paramT, int paramInt, A paramA) {
    int i = Math.min(64, this.mCallbacks.size());
    notifyCallbacksLocked(paramT, paramInt, paramA, 0, i, this.mFirst64Removed);
  }
  
  private void notifyRecurseLocked(T paramT, int paramInt, A paramA) {
    int j, i = this.mCallbacks.size();
    long[] arrayOfLong = this.mRemainderRemoved;
    if (arrayOfLong == null) {
      j = -1;
    } else {
      j = arrayOfLong.length - 1;
    } 
    notifyRemainderLocked(paramT, paramInt, paramA, j);
    notifyCallbacksLocked(paramT, paramInt, paramA, (j + 2) * 64, i, 0L);
  }
  
  private void notifyRemainderLocked(T paramT, int paramInt1, A paramA, int paramInt2) {
    if (paramInt2 < 0) {
      notifyFirst64Locked(paramT, paramInt1, paramA);
    } else {
      long l = this.mRemainderRemoved[paramInt2];
      int i = (paramInt2 + 1) * 64;
      int j = Math.min(this.mCallbacks.size(), i + 64);
      notifyRemainderLocked(paramT, paramInt1, paramA, paramInt2 - 1);
      notifyCallbacksLocked(paramT, paramInt1, paramA, i, j, l);
    } 
  }
  
  private void notifyCallbacksLocked(T paramT, int paramInt1, A paramA, int paramInt2, int paramInt3, long paramLong) {
    long l = 1L;
    for (; paramInt2 < paramInt3; paramInt2++) {
      if ((paramLong & l) == 0L)
        this.mNotifier.onNotifyCallback(this.mCallbacks.get(paramInt2), paramT, paramInt1, paramA); 
      l <<= 1L;
    } 
  }
  
  public void add(C paramC) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mCallbacks : Ljava/util/List;
    //   6: aload_1
    //   7: invokeinterface lastIndexOf : (Ljava/lang/Object;)I
    //   12: istore_2
    //   13: iload_2
    //   14: iflt -> 25
    //   17: aload_0
    //   18: iload_2
    //   19: invokespecial isRemovedLocked : (I)Z
    //   22: ifeq -> 36
    //   25: aload_0
    //   26: getfield mCallbacks : Ljava/util/List;
    //   29: aload_1
    //   30: invokeinterface add : (Ljava/lang/Object;)Z
    //   35: pop
    //   36: aload_0
    //   37: monitorexit
    //   38: return
    //   39: astore_1
    //   40: aload_0
    //   41: monitorexit
    //   42: aload_1
    //   43: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #216	-> 2
    //   #217	-> 13
    //   #218	-> 25
    //   #220	-> 36
    //   #215	-> 39
    // Exception table:
    //   from	to	target	type
    //   2	13	39	finally
    //   17	25	39	finally
    //   25	36	39	finally
  }
  
  private boolean isRemovedLocked(int paramInt) {
    boolean bool1 = true, bool2 = true;
    if (paramInt < 64) {
      if ((this.mFirst64Removed & 1L << paramInt) == 0L)
        bool2 = false; 
      return bool2;
    } 
    long[] arrayOfLong = this.mRemainderRemoved;
    if (arrayOfLong == null)
      return false; 
    int i = paramInt / 64 - 1;
    if (i >= arrayOfLong.length)
      return false; 
    long l = arrayOfLong[i];
    if ((l & 1L << paramInt % 64) != 0L) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    return bool2;
  }
  
  private void removeRemovedCallbacks(int paramInt, long paramLong) {
    long l = Long.MIN_VALUE;
    for (int i = paramInt + 64 - 1; i >= paramInt; i--) {
      if ((paramLong & l) != 0L)
        this.mCallbacks.remove(i); 
      l >>>= 1L;
    } 
  }
  
  public void remove(C paramC) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mNotificationLevel : I
    //   6: ifne -> 23
    //   9: aload_0
    //   10: getfield mCallbacks : Ljava/util/List;
    //   13: aload_1
    //   14: invokeinterface remove : (Ljava/lang/Object;)Z
    //   19: pop
    //   20: goto -> 43
    //   23: aload_0
    //   24: getfield mCallbacks : Ljava/util/List;
    //   27: aload_1
    //   28: invokeinterface lastIndexOf : (Ljava/lang/Object;)I
    //   33: istore_2
    //   34: iload_2
    //   35: iflt -> 43
    //   38: aload_0
    //   39: iload_2
    //   40: invokespecial setRemovalBitLocked : (I)V
    //   43: aload_0
    //   44: monitorexit
    //   45: return
    //   46: astore_1
    //   47: aload_0
    //   48: monitorexit
    //   49: aload_1
    //   50: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #275	-> 2
    //   #276	-> 9
    //   #278	-> 23
    //   #279	-> 34
    //   #280	-> 38
    //   #283	-> 43
    //   #274	-> 46
    // Exception table:
    //   from	to	target	type
    //   2	9	46	finally
    //   9	20	46	finally
    //   23	34	46	finally
    //   38	43	46	finally
  }
  
  private void setRemovalBitLocked(int paramInt) {
    if (paramInt < 64) {
      this.mFirst64Removed |= 1L << paramInt;
    } else {
      int i = paramInt / 64 - 1;
      long[] arrayOfLong = this.mRemainderRemoved;
      if (arrayOfLong == null) {
        this.mRemainderRemoved = new long[this.mCallbacks.size() / 64];
      } else if (arrayOfLong.length < i) {
        long[] arrayOfLong1 = new long[this.mCallbacks.size() / 64];
        arrayOfLong = this.mRemainderRemoved;
        System.arraycopy(arrayOfLong, 0, arrayOfLong1, 0, arrayOfLong.length);
        this.mRemainderRemoved = arrayOfLong1;
      } 
      arrayOfLong = this.mRemainderRemoved;
      arrayOfLong[i] = arrayOfLong[i] | 1L << paramInt % 64;
    } 
  }
  
  public ArrayList<C> copyListeners() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new java/util/ArrayList
    //   5: astore_1
    //   6: aload_1
    //   7: aload_0
    //   8: getfield mCallbacks : Ljava/util/List;
    //   11: invokeinterface size : ()I
    //   16: invokespecial <init> : (I)V
    //   19: aload_0
    //   20: getfield mCallbacks : Ljava/util/List;
    //   23: invokeinterface size : ()I
    //   28: istore_2
    //   29: iconst_0
    //   30: istore_3
    //   31: iload_3
    //   32: iload_2
    //   33: if_icmpge -> 65
    //   36: aload_0
    //   37: iload_3
    //   38: invokespecial isRemovedLocked : (I)Z
    //   41: ifne -> 59
    //   44: aload_1
    //   45: aload_0
    //   46: getfield mCallbacks : Ljava/util/List;
    //   49: iload_3
    //   50: invokeinterface get : (I)Ljava/lang/Object;
    //   55: invokevirtual add : (Ljava/lang/Object;)Z
    //   58: pop
    //   59: iinc #3, 1
    //   62: goto -> 31
    //   65: aload_0
    //   66: monitorexit
    //   67: aload_1
    //   68: areturn
    //   69: astore_1
    //   70: aload_0
    //   71: monitorexit
    //   72: aload_1
    //   73: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #311	-> 2
    //   #312	-> 19
    //   #313	-> 29
    //   #314	-> 36
    //   #315	-> 44
    //   #313	-> 59
    //   #318	-> 65
    //   #310	-> 69
    // Exception table:
    //   from	to	target	type
    //   2	19	69	finally
    //   19	29	69	finally
    //   36	44	69	finally
    //   44	59	69	finally
  }
  
  public boolean isEmpty() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mCallbacks : Ljava/util/List;
    //   6: invokeinterface isEmpty : ()Z
    //   11: istore_1
    //   12: iload_1
    //   13: ifeq -> 20
    //   16: aload_0
    //   17: monitorexit
    //   18: iconst_1
    //   19: ireturn
    //   20: aload_0
    //   21: getfield mNotificationLevel : I
    //   24: istore_2
    //   25: iload_2
    //   26: ifne -> 33
    //   29: aload_0
    //   30: monitorexit
    //   31: iconst_0
    //   32: ireturn
    //   33: aload_0
    //   34: getfield mCallbacks : Ljava/util/List;
    //   37: invokeinterface size : ()I
    //   42: istore_3
    //   43: iconst_0
    //   44: istore_2
    //   45: iload_2
    //   46: iload_3
    //   47: if_icmpge -> 70
    //   50: aload_0
    //   51: iload_2
    //   52: invokespecial isRemovedLocked : (I)Z
    //   55: istore_1
    //   56: iload_1
    //   57: ifne -> 64
    //   60: aload_0
    //   61: monitorexit
    //   62: iconst_0
    //   63: ireturn
    //   64: iinc #2, 1
    //   67: goto -> 45
    //   70: aload_0
    //   71: monitorexit
    //   72: iconst_1
    //   73: ireturn
    //   74: astore #4
    //   76: aload_0
    //   77: monitorexit
    //   78: aload #4
    //   80: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #327	-> 2
    //   #328	-> 16
    //   #329	-> 20
    //   #330	-> 29
    //   #332	-> 33
    //   #333	-> 43
    //   #334	-> 50
    //   #335	-> 60
    //   #333	-> 64
    //   #338	-> 70
    //   #326	-> 74
    // Exception table:
    //   from	to	target	type
    //   2	12	74	finally
    //   20	25	74	finally
    //   33	43	74	finally
    //   50	56	74	finally
  }
  
  public void clear() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mNotificationLevel : I
    //   6: ifne -> 21
    //   9: aload_0
    //   10: getfield mCallbacks : Ljava/util/List;
    //   13: invokeinterface clear : ()V
    //   18: goto -> 60
    //   21: aload_0
    //   22: getfield mCallbacks : Ljava/util/List;
    //   25: invokeinterface isEmpty : ()Z
    //   30: ifne -> 60
    //   33: aload_0
    //   34: getfield mCallbacks : Ljava/util/List;
    //   37: invokeinterface size : ()I
    //   42: iconst_1
    //   43: isub
    //   44: istore_1
    //   45: iload_1
    //   46: iflt -> 60
    //   49: aload_0
    //   50: iload_1
    //   51: invokespecial setRemovalBitLocked : (I)V
    //   54: iinc #1, -1
    //   57: goto -> 45
    //   60: aload_0
    //   61: monitorexit
    //   62: return
    //   63: astore_2
    //   64: aload_0
    //   65: monitorexit
    //   66: aload_2
    //   67: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #346	-> 2
    //   #347	-> 9
    //   #348	-> 21
    //   #349	-> 33
    //   #350	-> 49
    //   #349	-> 54
    //   #353	-> 60
    //   #345	-> 63
    // Exception table:
    //   from	to	target	type
    //   2	9	63	finally
    //   9	18	63	finally
    //   21	33	63	finally
    //   33	45	63	finally
    //   49	54	63	finally
  }
  
  public CallbackRegistry<C, T, A> clone() {
    /* monitor enter ThisExpression{ObjectType{com/android/internal/util/CallbackRegistry}} */
    CallbackRegistry<C, T, A> callbackRegistry = null;
    try {
      CallbackRegistry<C, T, A> callbackRegistry1 = (CallbackRegistry)super.clone();
      callbackRegistry = callbackRegistry1;
      callbackRegistry1.mFirst64Removed = 0L;
      callbackRegistry = callbackRegistry1;
      callbackRegistry1.mRemainderRemoved = null;
      callbackRegistry = callbackRegistry1;
      callbackRegistry1.mNotificationLevel = 0;
      callbackRegistry = callbackRegistry1;
      ArrayList<C> arrayList = new ArrayList();
      callbackRegistry = callbackRegistry1;
      this();
      callbackRegistry = callbackRegistry1;
      callbackRegistry1.mCallbacks = arrayList;
      callbackRegistry = callbackRegistry1;
      int i = this.mCallbacks.size();
      for (byte b = 0; b < i; b++) {
        callbackRegistry = callbackRegistry1;
        if (!isRemovedLocked(b)) {
          callbackRegistry = callbackRegistry1;
          callbackRegistry1.mCallbacks.add(this.mCallbacks.get(b));
        } 
      } 
      callbackRegistry = callbackRegistry1;
    } catch (CloneNotSupportedException cloneNotSupportedException) {
      cloneNotSupportedException.printStackTrace();
    } finally {}
    /* monitor exit ThisExpression{ObjectType{com/android/internal/util/CallbackRegistry}} */
    return callbackRegistry;
  }
  
  public static abstract class NotifierCallback<C, T, A> {
    public abstract void onNotifyCallback(C param1C, T param1T, int param1Int, A param1A);
  }
}
