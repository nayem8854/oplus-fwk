package com.android.internal.policy;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.hardware.display.DisplayManager;
import android.view.Display;
import android.view.DisplayInfo;
import java.util.ArrayList;

public class DividerSnapAlgorithm {
  private final int mTaskHeightInMinimizedMode;
  
  private final ArrayList<SnapTarget> mTargets = new ArrayList<>();
  
  private final int mSnapMode;
  
  private final int mMinimalSizeResizableTask;
  
  private final float mMinFlingVelocityPxPerSecond;
  
  private final float mMinDismissVelocityPxPerSecond;
  
  private final SnapTarget mMiddleTarget;
  
  private final SnapTarget mLastSplitTarget;
  
  private boolean mIsHorizontalDivision;
  
  private final Rect mInsets = new Rect();
  
  private final boolean mFreeSnapMode;
  
  private final float mFixedRatio;
  
  private final SnapTarget mFirstSplitTarget;
  
  private final int mDividerSize;
  
  private final int mDisplayWidth;
  
  private final int mDisplayHeight;
  
  private final SnapTarget mDismissStartTarget;
  
  private final SnapTarget mDismissEndTarget;
  
  private static final int SNAP_ONLY_1_1 = 2;
  
  private static final int SNAP_MODE_MINIMIZED = 3;
  
  private static final int SNAP_MODE_16_9 = 0;
  
  private static final boolean SNAP_FORCE_3_RATIOS = true;
  
  private static final int SNAP_FIXED_RATIO = 1;
  
  private static final int MIN_FLING_VELOCITY_DP_PER_SECOND = 400;
  
  private static final int MIN_DISMISS_VELOCITY_DP_PER_SECOND = 600;
  
  public static DividerSnapAlgorithm create(Context paramContext, Rect paramRect) {
    DisplayInfo displayInfo = new DisplayInfo();
    Display display = ((DisplayManager)paramContext.getSystemService(DisplayManager.class)).getDisplay(0);
    display.getDisplayInfo(displayInfo);
    int i = paramContext.getResources().getDimensionPixelSize(17105174);
    int j = paramContext.getResources().getDimensionPixelSize(17105173);
    Resources resources = paramContext.getResources();
    int k = displayInfo.logicalWidth, m = displayInfo.logicalHeight;
    int n = (paramContext.getApplicationContext().getResources().getConfiguration()).orientation;
    boolean bool = true;
    if (n != 1)
      bool = false; 
    return new DividerSnapAlgorithm(resources, k, m, i - j * 2, bool, paramRect);
  }
  
  public DividerSnapAlgorithm(Resources paramResources, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean, Rect paramRect) {
    this(paramResources, paramInt1, paramInt2, paramInt3, paramBoolean, paramRect, -1, false, true);
  }
  
  public DividerSnapAlgorithm(Resources paramResources, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean, Rect paramRect, int paramInt4) {
    this(paramResources, paramInt1, paramInt2, paramInt3, paramBoolean, paramRect, paramInt4, false, true);
  }
  
  public DividerSnapAlgorithm(Resources paramResources, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean1, Rect paramRect, int paramInt4, boolean paramBoolean2, boolean paramBoolean3) {
    this.mMinFlingVelocityPxPerSecond = (paramResources.getDisplayMetrics()).density * 400.0F;
    this.mMinDismissVelocityPxPerSecond = (paramResources.getDisplayMetrics()).density * 600.0F;
    this.mDividerSize = paramInt3;
    this.mDisplayWidth = paramInt1;
    this.mDisplayHeight = paramInt2;
    this.mIsHorizontalDivision = paramBoolean1;
    this.mInsets.set(paramRect);
    if (paramBoolean2) {
      paramInt1 = 3;
    } else {
      paramInt1 = paramResources.getInteger(17694799);
    } 
    this.mSnapMode = paramInt1;
    this.mFreeSnapMode = paramResources.getBoolean(17891419);
    this.mFixedRatio = paramResources.getFraction(18022407, 1, 1);
    this.mMinimalSizeResizableTask = paramResources.getDimensionPixelSize(17105155);
    if (paramBoolean3) {
      paramInt1 = paramResources.getDimensionPixelSize(17105490);
    } else {
      paramInt1 = 0;
    } 
    this.mTaskHeightInMinimizedMode = paramInt1;
    calculateTargets(paramBoolean1, paramInt4);
    this.mFirstSplitTarget = this.mTargets.get(1);
    ArrayList<SnapTarget> arrayList = this.mTargets;
    this.mLastSplitTarget = arrayList.get(arrayList.size() - 2);
    this.mDismissStartTarget = this.mTargets.get(0);
    arrayList = this.mTargets;
    this.mDismissEndTarget = arrayList.get(arrayList.size() - 1);
    arrayList = this.mTargets;
    SnapTarget snapTarget = arrayList.get(arrayList.size() / 2);
    snapTarget.isMiddleTarget = true;
  }
  
  public boolean isSplitScreenFeasible() {
    int j, k;
    boolean bool;
    int i = this.mInsets.top;
    if (this.mIsHorizontalDivision) {
      j = this.mInsets.bottom;
    } else {
      j = this.mInsets.right;
    } 
    if (this.mIsHorizontalDivision) {
      k = this.mDisplayHeight;
    } else {
      k = this.mDisplayWidth;
    } 
    int m = this.mDividerSize;
    if ((k - j - i - m) / 2 >= this.mMinimalSizeResizableTask) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public SnapTarget calculateSnapTarget(int paramInt, float paramFloat) {
    return calculateSnapTarget(paramInt, paramFloat, true);
  }
  
  public SnapTarget calculateSnapTarget(int paramInt, float paramFloat, boolean paramBoolean) {
    if (paramInt < this.mFirstSplitTarget.position && paramFloat < -this.mMinDismissVelocityPxPerSecond)
      return this.mDismissStartTarget; 
    if (paramInt > this.mLastSplitTarget.position && paramFloat > this.mMinDismissVelocityPxPerSecond)
      return this.mDismissEndTarget; 
    if (Math.abs(paramFloat) < this.mMinFlingVelocityPxPerSecond)
      return snap(paramInt, paramBoolean); 
    if (paramFloat < 0.0F)
      return this.mFirstSplitTarget; 
    return this.mLastSplitTarget;
  }
  
  public SnapTarget calculateNonDismissingSnapTarget(int paramInt) {
    SnapTarget snapTarget = snap(paramInt, false);
    if (snapTarget == this.mDismissStartTarget)
      return this.mFirstSplitTarget; 
    if (snapTarget == this.mDismissEndTarget)
      return this.mLastSplitTarget; 
    return snapTarget;
  }
  
  public float calculateDismissingFraction(int paramInt) {
    if (paramInt < this.mFirstSplitTarget.position) {
      float f = (paramInt - getStartInset());
      paramInt = this.mFirstSplitTarget.position;
      f /= (paramInt - getStartInset());
      return 1.0F - f;
    } 
    if (paramInt > this.mLastSplitTarget.position)
      return (paramInt - this.mLastSplitTarget.position) / (this.mDismissEndTarget.position - this.mLastSplitTarget.position - this.mDividerSize); 
    return 0.0F;
  }
  
  public SnapTarget getClosestDismissTarget(int paramInt) {
    if (paramInt < this.mFirstSplitTarget.position)
      return this.mDismissStartTarget; 
    if (paramInt > this.mLastSplitTarget.position)
      return this.mDismissEndTarget; 
    if (paramInt - this.mDismissStartTarget.position < this.mDismissEndTarget.position - paramInt)
      return this.mDismissStartTarget; 
    return this.mDismissEndTarget;
  }
  
  public SnapTarget getFirstSplitTarget() {
    return this.mFirstSplitTarget;
  }
  
  public SnapTarget getLastSplitTarget() {
    return this.mLastSplitTarget;
  }
  
  public SnapTarget getDismissStartTarget() {
    return this.mDismissStartTarget;
  }
  
  public SnapTarget getDismissEndTarget() {
    return this.mDismissEndTarget;
  }
  
  private int getStartInset() {
    if (this.mIsHorizontalDivision)
      return this.mInsets.top; 
    return this.mInsets.left;
  }
  
  private int getEndInset() {
    if (this.mIsHorizontalDivision)
      return this.mInsets.bottom; 
    return this.mInsets.right;
  }
  
  private boolean shouldApplyFreeSnapMode(int paramInt) {
    boolean bool = this.mFreeSnapMode;
    boolean bool1 = false;
    if (!bool)
      return false; 
    if (!isFirstSplitTargetAvailable() || !isLastSplitTargetAvailable())
      return false; 
    bool = bool1;
    if (this.mFirstSplitTarget.position < paramInt) {
      bool = bool1;
      if (paramInt < this.mLastSplitTarget.position)
        bool = true; 
    } 
    return bool;
  }
  
  private SnapTarget snap(int paramInt, boolean paramBoolean) {
    if (shouldApplyFreeSnapMode(paramInt))
      return new SnapTarget(paramInt, paramInt, 0); 
    byte b = -1;
    float f = Float.MAX_VALUE;
    int i = this.mTargets.size();
    for (byte b1 = 0; b1 < i; b1++, f = f1) {
      SnapTarget snapTarget = this.mTargets.get(b1);
      float f1 = Math.abs(paramInt - snapTarget.position);
      float f2 = f1;
      if (paramBoolean)
        f2 = f1 / snapTarget.distanceMultiplier; 
      f1 = f;
      if (f2 < f) {
        b = b1;
        f1 = f2;
      } 
    } 
    return this.mTargets.get(b);
  }
  
  private void calculateTargets(boolean paramBoolean, int paramInt) {
    int i, j;
    this.mTargets.clear();
    if (paramBoolean) {
      i = this.mDisplayHeight;
    } else {
      i = this.mDisplayWidth;
    } 
    Rect rect = this.mInsets;
    if (paramBoolean) {
      j = rect.bottom;
    } else {
      j = rect.right;
    } 
    int k = -this.mDividerSize;
    int m = k;
    if (paramInt == 3)
      m = k + this.mInsets.left; 
    this.mTargets.add(new SnapTarget(m, m, 1, 0.35F));
    m = this.mSnapMode;
    if (m != 0) {
      if (m != 1) {
        if (m != 2) {
          if (m == 3)
            addMinimizedTarget(paramBoolean, paramInt); 
        } else {
          addMiddleTarget(paramBoolean);
        } 
      } else {
        addFixedDivisionTargets(paramBoolean, i);
      } 
    } else {
      addRatio16_9Targets(paramBoolean, i);
    } 
    this.mTargets.add(new SnapTarget(i - j, i, 2, 0.35F));
  }
  
  private void addNonDismissingTargets(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3) {
    maybeAddTarget(paramInt1, paramInt1 - getStartInset());
    addMiddleTarget(paramBoolean);
    paramInt1 = getEndInset();
    int i = this.mDividerSize;
    maybeAddTarget(paramInt2, paramInt3 - paramInt1 - i + paramInt2);
  }
  
  private void addFixedDivisionTargets(boolean paramBoolean, int paramInt) {
    int i, j;
    Rect rect = this.mInsets;
    if (paramBoolean) {
      i = rect.top;
    } else {
      i = rect.left;
    } 
    if (paramBoolean) {
      j = this.mDisplayHeight - this.mInsets.bottom;
    } else {
      j = this.mDisplayWidth - this.mInsets.right;
    } 
    int k = (int)(this.mFixedRatio * (j - i)), m = this.mDividerSize;
    k -= m / 2;
    addNonDismissingTargets(paramBoolean, i + k, j - k - m, paramInt);
  }
  
  private void addRatio16_9Targets(boolean paramBoolean, int paramInt) {
    Rect rect = this.mInsets;
    if (paramBoolean) {
      i = rect.top;
    } else {
      i = rect.left;
    } 
    if (paramBoolean) {
      j = this.mDisplayHeight - this.mInsets.bottom;
    } else {
      j = this.mDisplayWidth - this.mInsets.right;
    } 
    rect = this.mInsets;
    if (paramBoolean) {
      k = rect.left;
    } else {
      k = rect.top;
    } 
    if (paramBoolean) {
      m = this.mDisplayWidth - this.mInsets.right;
    } else {
      m = this.mDisplayHeight - this.mInsets.bottom;
    } 
    float f = (m - k);
    int k = (int)Math.floor((f * 0.5625F));
    int m = i + k;
    k = j - k - this.mDividerSize;
    int i = m;
    if (m - this.mInsets.top < this.mMinimalSizeResizableTask)
      i = this.mInsets.top + this.mMinimalSizeResizableTask; 
    int j = k;
    if (paramInt - this.mInsets.bottom - this.mDividerSize + k < this.mMinimalSizeResizableTask)
      j = paramInt - this.mInsets.bottom - this.mDividerSize - this.mMinimalSizeResizableTask; 
    addNonDismissingTargets(paramBoolean, i, j, paramInt);
  }
  
  private void maybeAddTarget(int paramInt1, int paramInt2) {
    if (paramInt2 >= this.mMinimalSizeResizableTask)
      this.mTargets.add(new SnapTarget(paramInt1, paramInt1, 0)); 
  }
  
  private void addMiddleTarget(boolean paramBoolean) {
    int i = DockedDividerUtils.calculateMiddlePosition(paramBoolean, this.mInsets, this.mDisplayWidth, this.mDisplayHeight, this.mDividerSize);
    this.mTargets.add(new SnapTarget(i, i, 0));
  }
  
  private void addMinimizedTarget(boolean paramBoolean, int paramInt) {
    int i = this.mTaskHeightInMinimizedMode + this.mInsets.top;
    int j = i;
    if (!paramBoolean)
      if (paramInt == 1) {
        j = i + this.mInsets.left;
      } else {
        j = i;
        if (paramInt == 3)
          j = this.mDisplayWidth - i - this.mInsets.right - this.mDividerSize; 
      }  
    this.mTargets.add(new SnapTarget(j, j, 0));
  }
  
  public SnapTarget getMiddleTarget() {
    return this.mMiddleTarget;
  }
  
  public SnapTarget getNextTarget(SnapTarget paramSnapTarget) {
    int i = this.mTargets.indexOf(paramSnapTarget);
    if (i != -1 && i < this.mTargets.size() - 1)
      return this.mTargets.get(i + 1); 
    return paramSnapTarget;
  }
  
  public SnapTarget getPreviousTarget(SnapTarget paramSnapTarget) {
    int i = this.mTargets.indexOf(paramSnapTarget);
    if (i != -1 && i > 0)
      return this.mTargets.get(i - 1); 
    return paramSnapTarget;
  }
  
  public boolean showMiddleSplitTargetForAccessibility() {
    int i = this.mTargets.size();
    boolean bool = true;
    if (i - 2 <= 1)
      bool = false; 
    return bool;
  }
  
  public boolean isFirstSplitTargetAvailable() {
    boolean bool;
    if (this.mFirstSplitTarget != this.mMiddleTarget) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isLastSplitTargetAvailable() {
    boolean bool;
    if (this.mLastSplitTarget != this.mMiddleTarget) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public SnapTarget cycleNonDismissTarget(SnapTarget paramSnapTarget, int paramInt) {
    SnapTarget snapTarget;
    int i = this.mTargets.indexOf(paramSnapTarget);
    if (i != -1) {
      ArrayList<SnapTarget> arrayList2 = this.mTargets;
      int j = arrayList2.size();
      ArrayList<SnapTarget> arrayList1 = this.mTargets;
      int k = arrayList1.size();
      snapTarget = arrayList2.get((j + i + paramInt) % k);
      if (snapTarget == this.mDismissStartTarget)
        return this.mLastSplitTarget; 
      if (snapTarget == this.mDismissEndTarget)
        return this.mFirstSplitTarget; 
      return snapTarget;
    } 
    return snapTarget;
  }
  
  public static class SnapTarget {
    public static final int FLAG_DISMISS_END = 2;
    
    public static final int FLAG_DISMISS_START = 1;
    
    public static final int FLAG_NONE = 0;
    
    private final float distanceMultiplier;
    
    public final int flag;
    
    public boolean isMiddleTarget;
    
    public final int position;
    
    public final int taskPosition;
    
    public SnapTarget(int param1Int1, int param1Int2, int param1Int3) {
      this(param1Int1, param1Int2, param1Int3, 1.0F);
    }
    
    public SnapTarget(int param1Int1, int param1Int2, int param1Int3, float param1Float) {
      this.position = param1Int1;
      this.taskPosition = param1Int2;
      this.flag = param1Int3;
      this.distanceMultiplier = param1Float;
    }
  }
}
