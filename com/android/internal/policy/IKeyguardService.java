package com.android.internal.policy;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.text.TextUtils;

public interface IKeyguardService extends IInterface {
  void addStateMonitorCallback(IKeyguardStateCallback paramIKeyguardStateCallback) throws RemoteException;
  
  void dismiss(IKeyguardDismissCallback paramIKeyguardDismissCallback, CharSequence paramCharSequence) throws RemoteException;
  
  void doKeyguardTimeout(Bundle paramBundle) throws RemoteException;
  
  void onBootCompleted() throws RemoteException;
  
  void onDreamingStarted() throws RemoteException;
  
  void onDreamingStopped() throws RemoteException;
  
  void onFinishedGoingToSleep(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void onFinishedWakingUp() throws RemoteException;
  
  void onScreenTurnedOff() throws RemoteException;
  
  void onScreenTurnedOn() throws RemoteException;
  
  void onScreenTurningOff() throws RemoteException;
  
  void onScreenTurningOn(IKeyguardDrawnCallback paramIKeyguardDrawnCallback) throws RemoteException;
  
  void onShortPowerPressedGoHome() throws RemoteException;
  
  void onStartedGoingToSleep(int paramInt) throws RemoteException;
  
  void onStartedWakingUp() throws RemoteException;
  
  void onSystemReady() throws RemoteException;
  
  void requestKeyguard(String paramString) throws RemoteException;
  
  void setCurrentUser(int paramInt) throws RemoteException;
  
  void setKeyguardEnabled(boolean paramBoolean) throws RemoteException;
  
  void setOccluded(boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  void setSwitchingUser(boolean paramBoolean) throws RemoteException;
  
  void startKeyguardExitAnimation(long paramLong1, long paramLong2) throws RemoteException;
  
  void verifyUnlock(IKeyguardExitCallback paramIKeyguardExitCallback) throws RemoteException;
  
  class Default implements IKeyguardService {
    public void setOccluded(boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {}
    
    public void addStateMonitorCallback(IKeyguardStateCallback param1IKeyguardStateCallback) throws RemoteException {}
    
    public void verifyUnlock(IKeyguardExitCallback param1IKeyguardExitCallback) throws RemoteException {}
    
    public void dismiss(IKeyguardDismissCallback param1IKeyguardDismissCallback, CharSequence param1CharSequence) throws RemoteException {}
    
    public void onDreamingStarted() throws RemoteException {}
    
    public void onDreamingStopped() throws RemoteException {}
    
    public void onStartedGoingToSleep(int param1Int) throws RemoteException {}
    
    public void onFinishedGoingToSleep(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void onStartedWakingUp() throws RemoteException {}
    
    public void onFinishedWakingUp() throws RemoteException {}
    
    public void onScreenTurningOn(IKeyguardDrawnCallback param1IKeyguardDrawnCallback) throws RemoteException {}
    
    public void onScreenTurnedOn() throws RemoteException {}
    
    public void onScreenTurningOff() throws RemoteException {}
    
    public void onScreenTurnedOff() throws RemoteException {}
    
    public void setKeyguardEnabled(boolean param1Boolean) throws RemoteException {}
    
    public void onSystemReady() throws RemoteException {}
    
    public void doKeyguardTimeout(Bundle param1Bundle) throws RemoteException {}
    
    public void setSwitchingUser(boolean param1Boolean) throws RemoteException {}
    
    public void setCurrentUser(int param1Int) throws RemoteException {}
    
    public void onBootCompleted() throws RemoteException {}
    
    public void startKeyguardExitAnimation(long param1Long1, long param1Long2) throws RemoteException {}
    
    public void onShortPowerPressedGoHome() throws RemoteException {}
    
    public void requestKeyguard(String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IKeyguardService {
    private static final String DESCRIPTOR = "com.android.internal.policy.IKeyguardService";
    
    static final int TRANSACTION_addStateMonitorCallback = 2;
    
    static final int TRANSACTION_dismiss = 4;
    
    static final int TRANSACTION_doKeyguardTimeout = 17;
    
    static final int TRANSACTION_onBootCompleted = 20;
    
    static final int TRANSACTION_onDreamingStarted = 5;
    
    static final int TRANSACTION_onDreamingStopped = 6;
    
    static final int TRANSACTION_onFinishedGoingToSleep = 8;
    
    static final int TRANSACTION_onFinishedWakingUp = 10;
    
    static final int TRANSACTION_onScreenTurnedOff = 14;
    
    static final int TRANSACTION_onScreenTurnedOn = 12;
    
    static final int TRANSACTION_onScreenTurningOff = 13;
    
    static final int TRANSACTION_onScreenTurningOn = 11;
    
    static final int TRANSACTION_onShortPowerPressedGoHome = 22;
    
    static final int TRANSACTION_onStartedGoingToSleep = 7;
    
    static final int TRANSACTION_onStartedWakingUp = 9;
    
    static final int TRANSACTION_onSystemReady = 16;
    
    static final int TRANSACTION_requestKeyguard = 23;
    
    static final int TRANSACTION_setCurrentUser = 19;
    
    static final int TRANSACTION_setKeyguardEnabled = 15;
    
    static final int TRANSACTION_setOccluded = 1;
    
    static final int TRANSACTION_setSwitchingUser = 18;
    
    static final int TRANSACTION_startKeyguardExitAnimation = 21;
    
    static final int TRANSACTION_verifyUnlock = 3;
    
    public Stub() {
      attachInterface(this, "com.android.internal.policy.IKeyguardService");
    }
    
    public static IKeyguardService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.policy.IKeyguardService");
      if (iInterface != null && iInterface instanceof IKeyguardService)
        return (IKeyguardService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 23:
          return "requestKeyguard";
        case 22:
          return "onShortPowerPressedGoHome";
        case 21:
          return "startKeyguardExitAnimation";
        case 20:
          return "onBootCompleted";
        case 19:
          return "setCurrentUser";
        case 18:
          return "setSwitchingUser";
        case 17:
          return "doKeyguardTimeout";
        case 16:
          return "onSystemReady";
        case 15:
          return "setKeyguardEnabled";
        case 14:
          return "onScreenTurnedOff";
        case 13:
          return "onScreenTurningOff";
        case 12:
          return "onScreenTurnedOn";
        case 11:
          return "onScreenTurningOn";
        case 10:
          return "onFinishedWakingUp";
        case 9:
          return "onStartedWakingUp";
        case 8:
          return "onFinishedGoingToSleep";
        case 7:
          return "onStartedGoingToSleep";
        case 6:
          return "onDreamingStopped";
        case 5:
          return "onDreamingStarted";
        case 4:
          return "dismiss";
        case 3:
          return "verifyUnlock";
        case 2:
          return "addStateMonitorCallback";
        case 1:
          break;
      } 
      return "setOccluded";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IKeyguardDismissCallback iKeyguardDismissCallback;
      if (param1Int1 != 1598968902) {
        String str;
        IKeyguardDrawnCallback iKeyguardDrawnCallback;
        IKeyguardExitCallback iKeyguardExitCallback;
        IKeyguardStateCallback iKeyguardStateCallback;
        long l1, l2;
        boolean bool1 = false, bool2 = false, bool3 = false, bool4 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 23:
            param1Parcel1.enforceInterface("com.android.internal.policy.IKeyguardService");
            str = param1Parcel1.readString();
            requestKeyguard(str);
            return true;
          case 22:
            str.enforceInterface("com.android.internal.policy.IKeyguardService");
            onShortPowerPressedGoHome();
            return true;
          case 21:
            str.enforceInterface("com.android.internal.policy.IKeyguardService");
            l1 = str.readLong();
            l2 = str.readLong();
            startKeyguardExitAnimation(l1, l2);
            return true;
          case 20:
            str.enforceInterface("com.android.internal.policy.IKeyguardService");
            onBootCompleted();
            return true;
          case 19:
            str.enforceInterface("com.android.internal.policy.IKeyguardService");
            param1Int1 = str.readInt();
            setCurrentUser(param1Int1);
            return true;
          case 18:
            str.enforceInterface("com.android.internal.policy.IKeyguardService");
            bool2 = bool4;
            if (str.readInt() != 0)
              bool2 = true; 
            setSwitchingUser(bool2);
            return true;
          case 17:
            str.enforceInterface("com.android.internal.policy.IKeyguardService");
            if (str.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            doKeyguardTimeout((Bundle)str);
            return true;
          case 16:
            str.enforceInterface("com.android.internal.policy.IKeyguardService");
            onSystemReady();
            return true;
          case 15:
            str.enforceInterface("com.android.internal.policy.IKeyguardService");
            bool2 = bool1;
            if (str.readInt() != 0)
              bool2 = true; 
            setKeyguardEnabled(bool2);
            return true;
          case 14:
            str.enforceInterface("com.android.internal.policy.IKeyguardService");
            onScreenTurnedOff();
            return true;
          case 13:
            str.enforceInterface("com.android.internal.policy.IKeyguardService");
            onScreenTurningOff();
            return true;
          case 12:
            str.enforceInterface("com.android.internal.policy.IKeyguardService");
            onScreenTurnedOn();
            return true;
          case 11:
            str.enforceInterface("com.android.internal.policy.IKeyguardService");
            iKeyguardDrawnCallback = IKeyguardDrawnCallback.Stub.asInterface(str.readStrongBinder());
            onScreenTurningOn(iKeyguardDrawnCallback);
            return true;
          case 10:
            iKeyguardDrawnCallback.enforceInterface("com.android.internal.policy.IKeyguardService");
            onFinishedWakingUp();
            return true;
          case 9:
            iKeyguardDrawnCallback.enforceInterface("com.android.internal.policy.IKeyguardService");
            onStartedWakingUp();
            return true;
          case 8:
            iKeyguardDrawnCallback.enforceInterface("com.android.internal.policy.IKeyguardService");
            param1Int1 = iKeyguardDrawnCallback.readInt();
            if (iKeyguardDrawnCallback.readInt() != 0)
              bool2 = true; 
            onFinishedGoingToSleep(param1Int1, bool2);
            return true;
          case 7:
            iKeyguardDrawnCallback.enforceInterface("com.android.internal.policy.IKeyguardService");
            param1Int1 = iKeyguardDrawnCallback.readInt();
            onStartedGoingToSleep(param1Int1);
            return true;
          case 6:
            iKeyguardDrawnCallback.enforceInterface("com.android.internal.policy.IKeyguardService");
            onDreamingStopped();
            return true;
          case 5:
            iKeyguardDrawnCallback.enforceInterface("com.android.internal.policy.IKeyguardService");
            onDreamingStarted();
            return true;
          case 4:
            iKeyguardDrawnCallback.enforceInterface("com.android.internal.policy.IKeyguardService");
            iKeyguardDismissCallback = IKeyguardDismissCallback.Stub.asInterface(iKeyguardDrawnCallback.readStrongBinder());
            if (iKeyguardDrawnCallback.readInt() != 0) {
              CharSequence charSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel((Parcel)iKeyguardDrawnCallback);
            } else {
              iKeyguardDrawnCallback = null;
            } 
            dismiss(iKeyguardDismissCallback, (CharSequence)iKeyguardDrawnCallback);
            return true;
          case 3:
            iKeyguardDrawnCallback.enforceInterface("com.android.internal.policy.IKeyguardService");
            iKeyguardExitCallback = IKeyguardExitCallback.Stub.asInterface(iKeyguardDrawnCallback.readStrongBinder());
            verifyUnlock(iKeyguardExitCallback);
            return true;
          case 2:
            iKeyguardExitCallback.enforceInterface("com.android.internal.policy.IKeyguardService");
            iKeyguardStateCallback = IKeyguardStateCallback.Stub.asInterface(iKeyguardExitCallback.readStrongBinder());
            addStateMonitorCallback(iKeyguardStateCallback);
            return true;
          case 1:
            break;
        } 
        iKeyguardStateCallback.enforceInterface("com.android.internal.policy.IKeyguardService");
        if (iKeyguardStateCallback.readInt() != 0) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        if (iKeyguardStateCallback.readInt() != 0)
          bool3 = true; 
        setOccluded(bool2, bool3);
        return true;
      } 
      iKeyguardDismissCallback.writeString("com.android.internal.policy.IKeyguardService");
      return true;
    }
    
    private static class Proxy implements IKeyguardService {
      public static IKeyguardService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.policy.IKeyguardService";
      }
      
      public void setOccluded(boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.policy.IKeyguardService");
          boolean bool1 = false;
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel.writeInt(bool2);
          boolean bool2 = bool1;
          if (param2Boolean2)
            bool2 = true; 
          parcel.writeInt(bool2);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IKeyguardService.Stub.getDefaultImpl() != null) {
            IKeyguardService.Stub.getDefaultImpl().setOccluded(param2Boolean1, param2Boolean2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void addStateMonitorCallback(IKeyguardStateCallback param2IKeyguardStateCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.policy.IKeyguardService");
          if (param2IKeyguardStateCallback != null) {
            iBinder = param2IKeyguardStateCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IKeyguardService.Stub.getDefaultImpl() != null) {
            IKeyguardService.Stub.getDefaultImpl().addStateMonitorCallback(param2IKeyguardStateCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void verifyUnlock(IKeyguardExitCallback param2IKeyguardExitCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.policy.IKeyguardService");
          if (param2IKeyguardExitCallback != null) {
            iBinder = param2IKeyguardExitCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IKeyguardService.Stub.getDefaultImpl() != null) {
            IKeyguardService.Stub.getDefaultImpl().verifyUnlock(param2IKeyguardExitCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void dismiss(IKeyguardDismissCallback param2IKeyguardDismissCallback, CharSequence param2CharSequence) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.policy.IKeyguardService");
          if (param2IKeyguardDismissCallback != null) {
            iBinder = param2IKeyguardDismissCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2CharSequence != null) {
            parcel.writeInt(1);
            TextUtils.writeToParcel(param2CharSequence, parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IKeyguardService.Stub.getDefaultImpl() != null) {
            IKeyguardService.Stub.getDefaultImpl().dismiss(param2IKeyguardDismissCallback, param2CharSequence);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDreamingStarted() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.policy.IKeyguardService");
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IKeyguardService.Stub.getDefaultImpl() != null) {
            IKeyguardService.Stub.getDefaultImpl().onDreamingStarted();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDreamingStopped() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.policy.IKeyguardService");
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IKeyguardService.Stub.getDefaultImpl() != null) {
            IKeyguardService.Stub.getDefaultImpl().onDreamingStopped();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onStartedGoingToSleep(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.policy.IKeyguardService");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IKeyguardService.Stub.getDefaultImpl() != null) {
            IKeyguardService.Stub.getDefaultImpl().onStartedGoingToSleep(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onFinishedGoingToSleep(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.policy.IKeyguardService");
          parcel.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(8, parcel, null, 1);
          if (!bool1 && IKeyguardService.Stub.getDefaultImpl() != null) {
            IKeyguardService.Stub.getDefaultImpl().onFinishedGoingToSleep(param2Int, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onStartedWakingUp() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.policy.IKeyguardService");
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IKeyguardService.Stub.getDefaultImpl() != null) {
            IKeyguardService.Stub.getDefaultImpl().onStartedWakingUp();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onFinishedWakingUp() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.policy.IKeyguardService");
          boolean bool = this.mRemote.transact(10, parcel, null, 1);
          if (!bool && IKeyguardService.Stub.getDefaultImpl() != null) {
            IKeyguardService.Stub.getDefaultImpl().onFinishedWakingUp();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onScreenTurningOn(IKeyguardDrawnCallback param2IKeyguardDrawnCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.policy.IKeyguardService");
          if (param2IKeyguardDrawnCallback != null) {
            iBinder = param2IKeyguardDrawnCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(11, parcel, null, 1);
          if (!bool && IKeyguardService.Stub.getDefaultImpl() != null) {
            IKeyguardService.Stub.getDefaultImpl().onScreenTurningOn(param2IKeyguardDrawnCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onScreenTurnedOn() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.policy.IKeyguardService");
          boolean bool = this.mRemote.transact(12, parcel, null, 1);
          if (!bool && IKeyguardService.Stub.getDefaultImpl() != null) {
            IKeyguardService.Stub.getDefaultImpl().onScreenTurnedOn();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onScreenTurningOff() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.policy.IKeyguardService");
          boolean bool = this.mRemote.transact(13, parcel, null, 1);
          if (!bool && IKeyguardService.Stub.getDefaultImpl() != null) {
            IKeyguardService.Stub.getDefaultImpl().onScreenTurningOff();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onScreenTurnedOff() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.policy.IKeyguardService");
          boolean bool = this.mRemote.transact(14, parcel, null, 1);
          if (!bool && IKeyguardService.Stub.getDefaultImpl() != null) {
            IKeyguardService.Stub.getDefaultImpl().onScreenTurnedOff();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setKeyguardEnabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.policy.IKeyguardService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(15, parcel, null, 1);
          if (!bool1 && IKeyguardService.Stub.getDefaultImpl() != null) {
            IKeyguardService.Stub.getDefaultImpl().setKeyguardEnabled(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSystemReady() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.policy.IKeyguardService");
          boolean bool = this.mRemote.transact(16, parcel, null, 1);
          if (!bool && IKeyguardService.Stub.getDefaultImpl() != null) {
            IKeyguardService.Stub.getDefaultImpl().onSystemReady();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void doKeyguardTimeout(Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.policy.IKeyguardService");
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(17, parcel, null, 1);
          if (!bool && IKeyguardService.Stub.getDefaultImpl() != null) {
            IKeyguardService.Stub.getDefaultImpl().doKeyguardTimeout(param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setSwitchingUser(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.policy.IKeyguardService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(18, parcel, null, 1);
          if (!bool1 && IKeyguardService.Stub.getDefaultImpl() != null) {
            IKeyguardService.Stub.getDefaultImpl().setSwitchingUser(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setCurrentUser(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.policy.IKeyguardService");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(19, parcel, null, 1);
          if (!bool && IKeyguardService.Stub.getDefaultImpl() != null) {
            IKeyguardService.Stub.getDefaultImpl().setCurrentUser(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onBootCompleted() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.policy.IKeyguardService");
          boolean bool = this.mRemote.transact(20, parcel, null, 1);
          if (!bool && IKeyguardService.Stub.getDefaultImpl() != null) {
            IKeyguardService.Stub.getDefaultImpl().onBootCompleted();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void startKeyguardExitAnimation(long param2Long1, long param2Long2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.policy.IKeyguardService");
          parcel.writeLong(param2Long1);
          parcel.writeLong(param2Long2);
          boolean bool = this.mRemote.transact(21, parcel, null, 1);
          if (!bool && IKeyguardService.Stub.getDefaultImpl() != null) {
            IKeyguardService.Stub.getDefaultImpl().startKeyguardExitAnimation(param2Long1, param2Long2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onShortPowerPressedGoHome() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.policy.IKeyguardService");
          boolean bool = this.mRemote.transact(22, parcel, null, 1);
          if (!bool && IKeyguardService.Stub.getDefaultImpl() != null) {
            IKeyguardService.Stub.getDefaultImpl().onShortPowerPressedGoHome();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void requestKeyguard(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.policy.IKeyguardService");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(23, parcel, null, 1);
          if (!bool && IKeyguardService.Stub.getDefaultImpl() != null) {
            IKeyguardService.Stub.getDefaultImpl().requestKeyguard(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IKeyguardService param1IKeyguardService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IKeyguardService != null) {
          Proxy.sDefaultImpl = param1IKeyguardService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IKeyguardService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
