package com.android.internal.policy;

import android.content.res.Resources;
import android.graphics.Rect;

public class DockedDividerUtils {
  public static void calculateBoundsForPosition(int paramInt1, int paramInt2, Rect paramRect, int paramInt3, int paramInt4, int paramInt5) {
    boolean bool = false;
    paramRect.set(0, 0, paramInt3, paramInt4);
    if (paramInt2 != 1) {
      if (paramInt2 != 2) {
        if (paramInt2 != 3) {
          if (paramInt2 == 4)
            paramRect.top = paramInt1 + paramInt5; 
        } else {
          paramRect.left = paramInt1 + paramInt5;
        } 
      } else {
        paramRect.bottom = paramInt1;
      } 
    } else {
      paramRect.right = paramInt1;
    } 
    if (paramInt2 == 1 || paramInt2 == 2)
      bool = true; 
    sanitizeStackBounds(paramRect, bool);
  }
  
  public static void sanitizeStackBounds(Rect paramRect, boolean paramBoolean) {
    if (paramBoolean) {
      if (paramRect.left >= paramRect.right)
        paramRect.left = paramRect.right - 1; 
      if (paramRect.top >= paramRect.bottom)
        paramRect.top = paramRect.bottom - 1; 
    } else {
      if (paramRect.right <= paramRect.left)
        paramRect.right = paramRect.left + 1; 
      if (paramRect.bottom <= paramRect.top)
        paramRect.bottom = paramRect.top + 1; 
    } 
  }
  
  public static int calculatePositionForBounds(Rect paramRect, int paramInt1, int paramInt2) {
    if (paramInt1 != 1) {
      if (paramInt1 != 2) {
        if (paramInt1 != 3) {
          if (paramInt1 != 4)
            return 0; 
          return paramRect.top - paramInt2;
        } 
        return paramRect.left - paramInt2;
      } 
      return paramRect.bottom;
    } 
    return paramRect.right;
  }
  
  public static int calculateMiddlePosition(boolean paramBoolean, Rect paramRect, int paramInt1, int paramInt2, int paramInt3) {
    int i;
    if (paramBoolean) {
      i = paramRect.top;
    } else {
      i = paramRect.left;
    } 
    if (paramBoolean) {
      paramInt1 = paramInt2 - paramRect.bottom;
    } else {
      paramInt1 -= paramRect.right;
    } 
    return (paramInt1 - i) / 2 + i - paramInt3 / 2;
  }
  
  public static int invertDockSide(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 3) {
          if (paramInt != 4)
            return -1; 
          return 2;
        } 
        return 1;
      } 
      return 4;
    } 
    return 3;
  }
  
  public static int getDividerInsets(Resources paramResources) {
    return paramResources.getDimensionPixelSize(17105173);
  }
  
  public static int getDividerSize(Resources paramResources, int paramInt) {
    int i = paramResources.getDimensionPixelSize(17105174);
    return i - paramInt * 2;
  }
  
  public static int getDockSide(int paramInt1, int paramInt2) {
    if (paramInt1 > paramInt2) {
      paramInt1 = 1;
    } else {
      paramInt1 = 2;
    } 
    return paramInt1;
  }
}
