package com.android.internal.policy;

import android.content.res.Resources;

public class ScreenDecorationsUtils {
  public static float getWindowCornerRadius(Resources paramResources) {
    if (!supportsRoundedCornersOnWindows(paramResources))
      return 0.0F; 
    float f1 = paramResources.getDimension(17105454);
    f1 -= paramResources.getDimension(17105455);
    float f2 = paramResources.getDimension(17105458);
    float f3 = f2 - paramResources.getDimension(17105459);
    f2 = f3;
    if (f3 == 0.0F)
      f2 = f1; 
    f3 = paramResources.getDimension(17105456);
    float f4 = f3 - paramResources.getDimension(17105457);
    f3 = f4;
    if (f4 == 0.0F)
      f3 = f1; 
    return Math.min(f2, f3);
  }
  
  public static boolean supportsRoundedCornersOnWindows(Resources paramResources) {
    return paramResources.getBoolean(17891554);
  }
}
