package com.android.internal.policy;

import android.graphics.Point;
import android.graphics.Rect;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class TaskResizingAlgorithm {
  public static final int CTRL_BOTTOM = 8;
  
  public static final int CTRL_LEFT = 1;
  
  public static final int CTRL_NONE = 0;
  
  public static final int CTRL_RIGHT = 2;
  
  public static final int CTRL_TOP = 4;
  
  public static final float MIN_ASPECT = 1.2F;
  
  public static Rect resizeDrag(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, Rect paramRect, int paramInt1, int paramInt2, int paramInt3, Point paramPoint, boolean paramBoolean1, boolean paramBoolean2) {
    int i4, i = Math.round(paramFloat1 - paramFloat3);
    int j = Math.round(paramFloat2 - paramFloat4);
    int k = paramRect.left;
    int m = paramRect.top;
    int n = paramRect.right;
    int i1 = paramRect.bottom;
    int i2 = n - k;
    int i3 = i1 - m;
    if ((paramInt1 & 0x1) != 0) {
      i4 = Math.max(paramInt2, Math.min(i2 - i, paramPoint.x));
    } else {
      i4 = i2;
      if ((paramInt1 & 0x2) != 0)
        i4 = Math.max(paramInt2, Math.min(i2 + i, paramPoint.x)); 
    } 
    if ((paramInt1 & 0x4) != 0) {
      i2 = Math.max(paramInt3, Math.min(i3 - j, paramPoint.y));
    } else {
      i2 = i3;
      if ((paramInt1 & 0x8) != 0)
        i2 = Math.max(paramInt3, Math.min(i3 + j, paramPoint.y)); 
    } 
    paramFloat1 = i4 / i2;
    if (paramBoolean1 && ((paramBoolean2 && paramFloat1 < 1.2F) || (!paramBoolean2 && paramFloat1 > 0.8333333002196431D))) {
      if (paramBoolean2) {
        j = Math.max(paramInt2, Math.min(paramPoint.x, i4));
        i3 = Math.min(i2, Math.round(j / 1.2F));
        if (i3 < paramInt3) {
          i3 = paramInt3;
          j = paramPoint.x;
          paramFloat1 = i3;
          j = Math.min(j, Math.round(paramFloat1 * 1.2F));
          j = Math.max(paramInt2, j);
        } 
        int i5 = Math.max(paramInt3, Math.min(paramPoint.y, i2));
        i = Math.max(i4, Math.round(i5 * 1.2F));
        if (i < paramInt2) {
          i = paramInt2;
          paramInt2 = paramPoint.y;
          paramFloat1 = i / 1.2F;
          paramInt2 = Math.min(paramInt2, Math.round(paramFloat1));
          paramInt2 = Math.max(paramInt3, paramInt2);
          paramInt3 = i;
        } else {
          paramInt2 = i5;
          paramInt3 = i;
        } 
      } else {
        j = Math.max(paramInt2, Math.min(paramPoint.x, i4));
        i3 = Math.max(i2, Math.round(j * 1.2F));
        if (i3 < paramInt3) {
          i3 = paramPoint.x;
          paramFloat1 = paramInt3 / 1.2F;
          i3 = Math.min(i3, Math.round(paramFloat1));
          j = Math.max(paramInt2, i3);
          i3 = paramInt3;
        } 
        i = Math.max(paramInt3, Math.min(paramPoint.y, i2));
        int i5 = Math.min(i4, Math.round(i / 1.2F));
        if (i5 < paramInt2) {
          i = paramInt2;
          paramInt2 = paramPoint.y;
          paramFloat1 = i;
          paramInt2 = Math.min(paramInt2, Math.round(paramFloat1 * 1.2F));
          paramInt2 = Math.max(paramInt3, paramInt2);
          paramInt3 = i;
        } else {
          paramInt3 = i5;
          paramInt2 = i;
        } 
      } 
      if (i4 > n - k || i2 > i1 - m) {
        i4 = 1;
      } else {
        i4 = 0;
      } 
      if (j * i3 > paramInt3 * paramInt2) {
        i2 = 1;
      } else {
        i2 = 0;
      } 
      if (i4 == i2) {
        i4 = j;
        i2 = i3;
      } else {
        i4 = paramInt3;
        i2 = paramInt2;
      } 
    } 
    if ((paramInt1 & 0x1) != 0) {
      paramInt2 = n - i4;
      paramInt3 = n;
    } else {
      paramInt3 = k + i4;
      paramInt2 = k;
    } 
    if ((paramInt1 & 0x4) != 0) {
      paramInt1 = i1 - i2;
      i4 = i1;
    } else {
      i4 = m + i2;
      paramInt1 = m;
    } 
    return new Rect(paramInt2, paramInt1, paramInt3, i4);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface CtrlType {}
}
