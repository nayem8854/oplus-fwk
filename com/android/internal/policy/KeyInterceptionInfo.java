package com.android.internal.policy;

public class KeyInterceptionInfo {
  public final int layoutParamsPrivateFlags;
  
  public final int layoutParamsType;
  
  public final String windowTitle;
  
  public KeyInterceptionInfo(int paramInt1, int paramInt2, String paramString) {
    this.layoutParamsType = paramInt1;
    this.layoutParamsPrivateFlags = paramInt2;
    this.windowTitle = paramString;
  }
}
