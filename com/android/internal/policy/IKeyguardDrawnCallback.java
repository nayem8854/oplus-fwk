package com.android.internal.policy;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IKeyguardDrawnCallback extends IInterface {
  void onDrawn() throws RemoteException;
  
  class Default implements IKeyguardDrawnCallback {
    public void onDrawn() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IKeyguardDrawnCallback {
    private static final String DESCRIPTOR = "com.android.internal.policy.IKeyguardDrawnCallback";
    
    static final int TRANSACTION_onDrawn = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.policy.IKeyguardDrawnCallback");
    }
    
    public static IKeyguardDrawnCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.policy.IKeyguardDrawnCallback");
      if (iInterface != null && iInterface instanceof IKeyguardDrawnCallback)
        return (IKeyguardDrawnCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onDrawn";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.policy.IKeyguardDrawnCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.policy.IKeyguardDrawnCallback");
      onDrawn();
      return true;
    }
    
    private static class Proxy implements IKeyguardDrawnCallback {
      public static IKeyguardDrawnCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.policy.IKeyguardDrawnCallback";
      }
      
      public void onDrawn() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.policy.IKeyguardDrawnCallback");
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IKeyguardDrawnCallback.Stub.getDefaultImpl() != null) {
            IKeyguardDrawnCallback.Stub.getDefaultImpl().onDrawn();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IKeyguardDrawnCallback param1IKeyguardDrawnCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IKeyguardDrawnCallback != null) {
          Proxy.sDefaultImpl = param1IKeyguardDrawnCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IKeyguardDrawnCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
