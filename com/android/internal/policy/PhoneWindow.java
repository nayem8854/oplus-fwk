package com.android.internal.policy;

import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.app.SearchManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.OplusPackageManager;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Insets;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.session.MediaController;
import android.media.session.MediaSession;
import android.media.session.MediaSessionManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.transition.Scene;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.transition.TransitionManager;
import android.transition.TransitionSet;
import android.util.AndroidRuntimeException;
import android.util.Log;
import android.util.Pair;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.IRotationWatcher;
import android.view.IScrollCaptureController;
import android.view.IWindowManager;
import android.view.InputDevice;
import android.view.InputEvent;
import android.view.InputQueue;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScrollCaptureCallback;
import android.view.SearchEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewRootImpl;
import android.view.Window;
import android.view.WindowInsets;
import android.view.WindowInsetsController;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.android.internal.R;
import com.android.internal.view.menu.ContextMenuBuilder;
import com.android.internal.view.menu.IconMenuPresenter;
import com.android.internal.view.menu.ListMenuPresenter;
import com.android.internal.view.menu.MenuBuilder;
import com.android.internal.view.menu.MenuDialogHelper;
import com.android.internal.view.menu.MenuHelper;
import com.android.internal.view.menu.MenuPresenter;
import com.android.internal.view.menu.MenuView;
import com.android.internal.widget.DecorContentParent;
import com.oplus.statusbar.OplusStatusBarController;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class PhoneWindow extends Window implements MenuBuilder.Callback {
  private static final String ACTION_BAR_TAG = "android:ActionBar";
  
  private static final int CUSTOM_TITLE_COMPATIBLE_FEATURES = 13505;
  
  private static final boolean DEBUG = false;
  
  private static final int DEFAULT_BACKGROUND_FADE_DURATION_MS = 300;
  
  static final int FLAG_RESOURCE_SET_ICON = 1;
  
  static final int FLAG_RESOURCE_SET_ICON_FALLBACK = 4;
  
  static final int FLAG_RESOURCE_SET_LOGO = 2;
  
  private static final String FOCUSED_ID_TAG = "android:focusedViewId";
  
  private static final String PANELS_TAG = "android:Panels";
  
  private static final String TAG = "PhoneWindow";
  
  private static final Transition USE_DEFAULT_TRANSITION;
  
  private static final String VIEWS_TAG = "android:views";
  
  private static final Window.OnContentApplyWindowInsetsListener sDefaultContentInsetsApplier = (Window.OnContentApplyWindowInsetsListener)_$$Lambda$PhoneWindow$F9lizKYeW8CQHD_8FLjKaBpUfBQ.INSTANCE;
  
  static final RotationWatcher sRotationWatcher;
  
  private ActionMenuPresenterCallback mActionMenuPresenterCallback;
  
  private ViewRootImpl.ActivityConfigCallback mActivityConfigCallback;
  
  private Boolean mAllowEnterTransitionOverlap;
  
  private Boolean mAllowReturnTransitionOverlap;
  
  private boolean mAlwaysReadCloseOnTouchAttr;
  
  private AudioManager mAudioManager;
  
  Drawable mBackgroundDrawable;
  
  private long mBackgroundFadeDurationMillis;
  
  Drawable mBackgroundFallbackDrawable;
  
  private ProgressBar mCircularProgressBar;
  
  private boolean mClipToOutline;
  
  private boolean mClosingActionMenu;
  
  ViewGroup mContentParent;
  
  private boolean mContentParentExplicitlySet;
  
  private Scene mContentScene;
  
  ContextMenuBuilder mContextMenu;
  
  static {
    USE_DEFAULT_TRANSITION = (Transition)new TransitionSet();
    sRotationWatcher = new RotationWatcher();
  }
  
  final PhoneWindowMenuCallback mContextMenuCallback = new PhoneWindowMenuCallback(this);
  
  MenuHelper mContextMenuHelper;
  
  private DecorView mDecor;
  
  private int mDecorCaptionShade;
  
  DecorContentParent mDecorContentParent;
  
  boolean mDecorFitsSystemWindows;
  
  private DrawableFeatureState[] mDrawables;
  
  private float mElevation;
  
  boolean mEnsureNavigationBarContrastWhenTransparent;
  
  boolean mEnsureStatusBarContrastWhenTransparent;
  
  private Transition mEnterTransition;
  
  private Transition mExitTransition;
  
  TypedValue mFixedHeightMajor;
  
  TypedValue mFixedHeightMinor;
  
  TypedValue mFixedWidthMajor;
  
  TypedValue mFixedWidthMinor;
  
  private boolean mForceDecorInstall;
  
  private boolean mForcedNavigationBarColor;
  
  private boolean mForcedStatusBarColor;
  
  private int mFrameResource;
  
  private ProgressBar mHorizontalProgressBar;
  
  int mIconRes;
  
  private int mInvalidatePanelMenuFeatures;
  
  private boolean mInvalidatePanelMenuPosted;
  
  private final Runnable mInvalidatePanelMenuRunnable;
  
  boolean mIsFloating;
  
  private boolean mIsStartingWindow;
  
  private boolean mIsTranslucent;
  
  public boolean mIsUseDefaultNavigationBarColor;
  
  private KeyguardManager mKeyguardManager;
  
  private LayoutInflater mLayoutInflater;
  
  private ImageView mLeftIconView;
  
  private boolean mLoadElevation;
  
  int mLogoRes;
  
  private MediaController mMediaController;
  
  private MediaSessionManager mMediaSessionManager;
  
  final TypedValue mMinWidthMajor = new TypedValue();
  
  final TypedValue mMinWidthMinor = new TypedValue();
  
  int mNavigationBarColor;
  
  int mNavigationBarDividerColor;
  
  private OplusPackageManager mOpm;
  
  int mPanelChordingKey;
  
  private PanelMenuPresenterCallback mPanelMenuPresenterCallback;
  
  private PanelFeatureState[] mPanels;
  
  PanelFeatureState mPreparedPanel;
  
  private Transition mReenterTransition;
  
  public final boolean mRenderShadowsInCompositor;
  
  int mResourcesSetFlags;
  
  private Transition mReturnTransition;
  
  private ImageView mRightIconView;
  
  private Transition mSharedElementEnterTransition;
  
  private Transition mSharedElementExitTransition;
  
  private Transition mSharedElementReenterTransition;
  
  private Transition mSharedElementReturnTransition;
  
  private Boolean mSharedElementsUseOverlay;
  
  int mStatusBarColor;
  
  private boolean mSupportsPictureInPicture;
  
  InputQueue.Callback mTakeInputQueueCallback;
  
  SurfaceHolder.Callback2 mTakeSurfaceCallback;
  
  private int mTextColor;
  
  private int mTheme;
  
  private CharSequence mTitle;
  
  private int mTitleColor;
  
  private TextView mTitleView;
  
  private TransitionManager mTransitionManager;
  
  private int mUiOptions;
  
  private boolean mUseDecorContext;
  
  private int mVolumeControlStreamType;
  
  class WindowManagerHolder {
    static final IWindowManager sWindowManager;
    
    static {
      IBinder iBinder = ServiceManager.getService("window");
      sWindowManager = IWindowManager.Stub.asInterface(iBinder);
    }
  }
  
  public PhoneWindow(Context paramContext) {
    super(paramContext);
    boolean bool = false;
    this.mForceDecorInstall = false;
    this.mContentParentExplicitlySet = false;
    this.mBackgroundDrawable = null;
    this.mBackgroundFallbackDrawable = null;
    this.mLoadElevation = true;
    this.mFrameResource = 0;
    this.mTextColor = 0;
    this.mStatusBarColor = 0;
    this.mNavigationBarColor = 0;
    this.mNavigationBarDividerColor = 0;
    this.mForcedStatusBarColor = false;
    this.mForcedNavigationBarColor = false;
    this.mIsUseDefaultNavigationBarColor = false;
    this.mTitle = null;
    this.mTitleColor = 0;
    this.mAlwaysReadCloseOnTouchAttr = false;
    this.mVolumeControlStreamType = Integer.MIN_VALUE;
    this.mUiOptions = 0;
    this.mInvalidatePanelMenuRunnable = (Runnable)new Object(this);
    this.mEnterTransition = null;
    Transition transition = USE_DEFAULT_TRANSITION;
    this.mExitTransition = null;
    this.mReenterTransition = transition;
    this.mSharedElementEnterTransition = null;
    this.mSharedElementReturnTransition = transition;
    this.mSharedElementExitTransition = null;
    this.mSharedElementReenterTransition = transition;
    this.mBackgroundFadeDurationMillis = -1L;
    this.mTheme = -1;
    this.mDecorCaptionShade = 0;
    this.mUseDecorContext = false;
    this.mDecorFitsSystemWindows = true;
    this.mLayoutInflater = LayoutInflater.from(paramContext);
    if (Settings.Global.getInt(paramContext.getContentResolver(), "render_shadows_in_compositor", 1) != 0)
      bool = true; 
    this.mRenderShadowsInCompositor = bool;
    this.mOpm = new OplusPackageManager(paramContext);
  }
  
  public PhoneWindow(Context paramContext, Window paramWindow, ViewRootImpl.ActivityConfigCallback paramActivityConfigCallback) {
    this(paramContext);
    boolean bool2, bool1 = true;
    this.mUseDecorContext = true;
    if (paramWindow != null) {
      this.mDecor = (DecorView)paramWindow.getDecorView();
      this.mElevation = paramWindow.getElevation();
      this.mLoadElevation = false;
      this.mForceDecorInstall = true;
      (getAttributes()).token = (paramWindow.getAttributes()).token;
    } 
    if (Settings.Global.getInt(paramContext.getContentResolver(), "force_resizable_activities", 0) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    boolean bool3 = bool1;
    if (!bool2)
      if (paramContext.getPackageManager().hasSystemFeature("android.software.picture_in_picture")) {
        bool3 = bool1;
      } else {
        bool3 = false;
      }  
    this.mSupportsPictureInPicture = bool3;
    this.mActivityConfigCallback = paramActivityConfigCallback;
  }
  
  public final void setContainer(Window paramWindow) {
    super.setContainer(paramWindow);
  }
  
  public boolean requestFeature(int paramInt) {
    if (!this.mContentParentExplicitlySet) {
      int i = getFeatures();
      int j = 1 << paramInt | i;
      if ((j & 0x80) == 0 || (j & 0xFFFFCB3E) == 0) {
        if ((i & 0x2) != 0 && paramInt == 8)
          return false; 
        if ((i & 0x100) != 0 && paramInt == 1)
          removeFeature(8); 
        if (paramInt != 5 || 
          !getContext().getPackageManager().hasSystemFeature("android.hardware.type.watch"))
          return super.requestFeature(paramInt); 
        throw new AndroidRuntimeException("You cannot use indeterminate progress on a watch.");
      } 
      throw new AndroidRuntimeException("You cannot combine custom titles with other title features");
    } 
    throw new AndroidRuntimeException("requestFeature() must be called before adding content");
  }
  
  public void setUiOptions(int paramInt) {
    this.mUiOptions = paramInt;
  }
  
  public void setUiOptions(int paramInt1, int paramInt2) {
    this.mUiOptions = this.mUiOptions & (paramInt2 ^ 0xFFFFFFFF) | paramInt1 & paramInt2;
  }
  
  public TransitionManager getTransitionManager() {
    return this.mTransitionManager;
  }
  
  public void setTransitionManager(TransitionManager paramTransitionManager) {
    this.mTransitionManager = paramTransitionManager;
  }
  
  public Scene getContentScene() {
    return this.mContentScene;
  }
  
  public void setContentView(int paramInt) {
    if (this.mContentParent == null) {
      installDecor();
    } else if (!hasFeature(12)) {
      this.mContentParent.removeAllViews();
    } 
    if (hasFeature(12)) {
      ViewGroup viewGroup = this.mContentParent;
      Context context = getContext();
      Scene scene = Scene.getSceneForLayout(viewGroup, paramInt, context);
      transitionTo(scene);
    } else {
      this.mLayoutInflater.inflate(paramInt, this.mContentParent);
    } 
    this.mContentParent.requestApplyInsets();
    Window.Callback callback = getCallback();
    if (callback != null && !isDestroyed())
      callback.onContentChanged(); 
    this.mContentParentExplicitlySet = true;
  }
  
  public void setContentView(View paramView) {
    setContentView(paramView, new ViewGroup.LayoutParams(-1, -1));
  }
  
  public void setContentView(View paramView, ViewGroup.LayoutParams paramLayoutParams) {
    Scene scene;
    if (this.mContentParent == null) {
      installDecor();
    } else if (!hasFeature(12)) {
      this.mContentParent.removeAllViews();
    } 
    if (hasFeature(12)) {
      paramView.setLayoutParams(paramLayoutParams);
      scene = new Scene(this.mContentParent, paramView);
      transitionTo(scene);
    } else {
      this.mContentParent.addView((View)scene, paramLayoutParams);
    } 
    this.mContentParent.requestApplyInsets();
    Window.Callback callback = getCallback();
    if (callback != null && !isDestroyed())
      callback.onContentChanged(); 
    this.mContentParentExplicitlySet = true;
  }
  
  public void addContentView(View paramView, ViewGroup.LayoutParams paramLayoutParams) {
    if (this.mContentParent == null)
      installDecor(); 
    if (hasFeature(12))
      Log.v("PhoneWindow", "addContentView does not support content transitions"); 
    this.mContentParent.addView(paramView, paramLayoutParams);
    this.mContentParent.requestApplyInsets();
    Window.Callback callback = getCallback();
    if (callback != null && !isDestroyed())
      callback.onContentChanged(); 
  }
  
  public void clearContentView() {
    DecorView decorView = this.mDecor;
    if (decorView != null)
      decorView.clearContentView(); 
  }
  
  private void transitionTo(Scene paramScene) {
    if (this.mContentScene == null) {
      paramScene.enter();
    } else {
      this.mTransitionManager.transitionTo(paramScene);
    } 
    this.mContentScene = paramScene;
  }
  
  public View getCurrentFocus() {
    DecorView decorView = this.mDecor;
    if (decorView != null) {
      View view = decorView.findFocus();
    } else {
      decorView = null;
    } 
    return (View)decorView;
  }
  
  public void takeSurface(SurfaceHolder.Callback2 paramCallback2) {
    this.mTakeSurfaceCallback = paramCallback2;
  }
  
  public void takeInputQueue(InputQueue.Callback paramCallback) {
    this.mTakeInputQueueCallback = paramCallback;
  }
  
  public boolean isFloating() {
    return this.mIsFloating;
  }
  
  public boolean isTranslucent() {
    return this.mIsTranslucent;
  }
  
  boolean isShowingWallpaper() {
    boolean bool;
    if (((getAttributes()).flags & 0x100000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public LayoutInflater getLayoutInflater() {
    return this.mLayoutInflater;
  }
  
  public void setTitle(CharSequence paramCharSequence) {
    setTitle(paramCharSequence, true);
  }
  
  public void setTitle(CharSequence paramCharSequence, boolean paramBoolean) {
    TextView textView = this.mTitleView;
    if (textView != null) {
      textView.setText(paramCharSequence);
    } else {
      DecorContentParent decorContentParent = this.mDecorContentParent;
      if (decorContentParent != null)
        decorContentParent.setWindowTitle(paramCharSequence); 
    } 
    this.mTitle = paramCharSequence;
    if (paramBoolean) {
      WindowManager.LayoutParams layoutParams = getAttributes();
      if (!TextUtils.equals(paramCharSequence, layoutParams.accessibilityTitle)) {
        layoutParams.accessibilityTitle = TextUtils.stringOrSpannedString(paramCharSequence);
        DecorView decorView = this.mDecor;
        if (decorView != null) {
          ViewRootImpl viewRootImpl = decorView.getViewRootImpl();
          if (viewRootImpl != null)
            viewRootImpl.onWindowTitleChanged(); 
        } 
        dispatchWindowAttributesChanged(getAttributes());
      } 
    } 
  }
  
  @Deprecated
  public void setTitleColor(int paramInt) {
    TextView textView = this.mTitleView;
    if (textView != null)
      textView.setTextColor(paramInt); 
    this.mTitleColor = paramInt;
  }
  
  public final boolean preparePanel(PanelFeatureState paramPanelFeatureState, KeyEvent paramKeyEvent) {
    DecorContentParent decorContentParent;
    int i;
    if (isDestroyed())
      return false; 
    if (paramPanelFeatureState.isPrepared)
      return true; 
    PanelFeatureState panelFeatureState = this.mPreparedPanel;
    if (panelFeatureState != null && panelFeatureState != paramPanelFeatureState)
      closePanel(panelFeatureState, false); 
    Window.Callback callback = getCallback();
    if (callback != null)
      paramPanelFeatureState.createdPanelView = callback.onCreatePanelView(paramPanelFeatureState.featureId); 
    if (paramPanelFeatureState.featureId == 0 || paramPanelFeatureState.featureId == 8) {
      i = 1;
    } else {
      i = 0;
    } 
    if (i) {
      DecorContentParent decorContentParent1 = this.mDecorContentParent;
      if (decorContentParent1 != null)
        decorContentParent1.setMenuPrepared(); 
    } 
    if (paramPanelFeatureState.createdPanelView == null) {
      DecorContentParent decorContentParent1;
      boolean bool;
      if (paramPanelFeatureState.menu == null || paramPanelFeatureState.refreshMenuContent) {
        if (paramPanelFeatureState.menu == null && (
          !initializePanelMenu(paramPanelFeatureState) || paramPanelFeatureState.menu == null))
          return false; 
        if (i && this.mDecorContentParent != null) {
          if (this.mActionMenuPresenterCallback == null)
            this.mActionMenuPresenterCallback = new ActionMenuPresenterCallback(); 
          this.mDecorContentParent.setMenu(paramPanelFeatureState.menu, this.mActionMenuPresenterCallback);
        } 
        paramPanelFeatureState.menu.stopDispatchingItemsChanged();
        if (callback == null || !callback.onCreatePanelMenu(paramPanelFeatureState.featureId, paramPanelFeatureState.menu)) {
          paramPanelFeatureState.setMenu(null);
          if (i) {
            decorContentParent = this.mDecorContentParent;
            if (decorContentParent != null)
              decorContentParent.setMenu(null, this.mActionMenuPresenterCallback); 
          } 
          return false;
        } 
        ((PanelFeatureState)decorContentParent).refreshMenuContent = false;
      } 
      ((PanelFeatureState)decorContentParent).menu.stopDispatchingItemsChanged();
      if (((PanelFeatureState)decorContentParent).frozenActionViewState != null) {
        ((PanelFeatureState)decorContentParent).menu.restoreActionViewStates(((PanelFeatureState)decorContentParent).frozenActionViewState);
        ((PanelFeatureState)decorContentParent).frozenActionViewState = null;
      } 
      if (!callback.onPreparePanel(((PanelFeatureState)decorContentParent).featureId, ((PanelFeatureState)decorContentParent).createdPanelView, ((PanelFeatureState)decorContentParent).menu)) {
        if (i) {
          decorContentParent1 = this.mDecorContentParent;
          if (decorContentParent1 != null)
            decorContentParent1.setMenu(null, this.mActionMenuPresenterCallback); 
        } 
        ((PanelFeatureState)decorContentParent).menu.startDispatchingItemsChanged();
        return false;
      } 
      if (decorContentParent1 != null) {
        i = decorContentParent1.getDeviceId();
      } else {
        i = -1;
      } 
      KeyCharacterMap keyCharacterMap = KeyCharacterMap.load(i);
      if (keyCharacterMap.getKeyboardType() != 1) {
        bool = true;
      } else {
        bool = false;
      } 
      ((PanelFeatureState)decorContentParent).qwertyMode = bool;
      ((PanelFeatureState)decorContentParent).menu.setQwertyMode(((PanelFeatureState)decorContentParent).qwertyMode);
      ((PanelFeatureState)decorContentParent).menu.startDispatchingItemsChanged();
    } 
    ((PanelFeatureState)decorContentParent).isPrepared = true;
    ((PanelFeatureState)decorContentParent).isHandled = false;
    this.mPreparedPanel = (PanelFeatureState)decorContentParent;
    return true;
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration) {
    if (this.mDecorContentParent == null) {
      PanelFeatureState panelFeatureState = getPanelState(0, false);
      if (panelFeatureState != null && panelFeatureState.menu != null)
        if (panelFeatureState.isOpen) {
          Bundle bundle = new Bundle();
          if (panelFeatureState.iconMenuPresenter != null)
            panelFeatureState.iconMenuPresenter.saveHierarchyState(bundle); 
          if (panelFeatureState.listMenuPresenter != null)
            panelFeatureState.listMenuPresenter.saveHierarchyState(bundle); 
          clearMenuViews(panelFeatureState);
          reopenMenu(false);
          if (panelFeatureState.iconMenuPresenter != null)
            panelFeatureState.iconMenuPresenter.restoreHierarchyState(bundle); 
          if (panelFeatureState.listMenuPresenter != null)
            panelFeatureState.listMenuPresenter.restoreHierarchyState(bundle); 
        } else {
          clearMenuViews(panelFeatureState);
        }  
    } 
  }
  
  public void onMultiWindowModeChanged() {
    DecorView decorView = this.mDecor;
    if (decorView != null)
      decorView.onConfigurationChanged(getContext().getResources().getConfiguration()); 
  }
  
  public void onPictureInPictureModeChanged(boolean paramBoolean) {
    DecorView decorView = this.mDecor;
    if (decorView != null)
      decorView.updatePictureInPictureOutlineProvider(paramBoolean); 
  }
  
  public void reportActivityRelaunched() {
    DecorView decorView = this.mDecor;
    if (decorView != null && decorView.getViewRootImpl() != null)
      this.mDecor.getViewRootImpl().reportActivityRelaunched(); 
  }
  
  private static void clearMenuViews(PanelFeatureState paramPanelFeatureState) {
    paramPanelFeatureState.createdPanelView = null;
    paramPanelFeatureState.refreshDecorView = true;
    paramPanelFeatureState.clearMenuPresenters();
  }
  
  public final void openPanel(int paramInt, KeyEvent paramKeyEvent) {
    if (paramInt == 0) {
      DecorContentParent decorContentParent = this.mDecorContentParent;
      if (decorContentParent != null && 
        decorContentParent.canShowOverflowMenu() && 
        !ViewConfiguration.get(getContext()).hasPermanentMenuKey()) {
        this.mDecorContentParent.showOverflowMenu();
        return;
      } 
    } 
    openPanel(getPanelState(paramInt, true), paramKeyEvent);
  }
  
  private void openPanel(PanelFeatureState paramPanelFeatureState, KeyEvent paramKeyEvent) {
    byte b1;
    if (paramPanelFeatureState.isOpen || isDestroyed())
      return; 
    if (paramPanelFeatureState.featureId == 0) {
      boolean bool;
      Context context = getContext();
      Configuration configuration = context.getResources().getConfiguration();
      if ((configuration.screenLayout & 0xF) == 4) {
        b1 = 1;
      } else {
        b1 = 0;
      } 
      if ((context.getApplicationInfo()).targetSdkVersion >= 11) {
        bool = true;
      } else {
        bool = false;
      } 
      if (b1 && bool)
        return; 
    } 
    Window.Callback callback = getCallback();
    if (callback != null && !callback.onMenuOpened(paramPanelFeatureState.featureId, paramPanelFeatureState.menu)) {
      closePanel(paramPanelFeatureState, true);
      return;
    } 
    WindowManager windowManager = getWindowManager();
    if (windowManager == null)
      return; 
    if (!preparePanel(paramPanelFeatureState, paramKeyEvent))
      return; 
    byte b2 = -2;
    if (paramPanelFeatureState.decorView == null || paramPanelFeatureState.refreshDecorView) {
      int i;
      if (paramPanelFeatureState.decorView == null) {
        if (!initializePanelDecor(paramPanelFeatureState) || paramPanelFeatureState.decorView == null)
          return; 
      } else if (paramPanelFeatureState.refreshDecorView && paramPanelFeatureState.decorView.getChildCount() > 0) {
        paramPanelFeatureState.decorView.removeAllViews();
      } 
      if (!initializePanelContent(paramPanelFeatureState) || !paramPanelFeatureState.hasPanelItems())
        return; 
      ViewGroup.LayoutParams layoutParams2 = paramPanelFeatureState.shownPanelView.getLayoutParams();
      ViewGroup.LayoutParams layoutParams1 = layoutParams2;
      if (layoutParams2 == null)
        layoutParams1 = new ViewGroup.LayoutParams(-2, -2); 
      if (layoutParams1.width == -1) {
        i = paramPanelFeatureState.fullBackground;
        b1 = -1;
      } else {
        i = paramPanelFeatureState.background;
        b1 = b2;
      } 
      paramPanelFeatureState.decorView.setWindowBackground(getContext().getDrawable(i));
      ViewParent viewParent = paramPanelFeatureState.shownPanelView.getParent();
      if (viewParent != null && viewParent instanceof ViewGroup)
        ((ViewGroup)viewParent).removeView(paramPanelFeatureState.shownPanelView); 
      paramPanelFeatureState.decorView.addView(paramPanelFeatureState.shownPanelView, layoutParams1);
      if (!paramPanelFeatureState.shownPanelView.hasFocus())
        paramPanelFeatureState.shownPanelView.requestFocus(); 
    } else if (!paramPanelFeatureState.isInListMode()) {
      b1 = -1;
    } else {
      b1 = b2;
      if (paramPanelFeatureState.createdPanelView != null) {
        ViewGroup.LayoutParams layoutParams1 = paramPanelFeatureState.createdPanelView.getLayoutParams();
        b1 = b2;
        if (layoutParams1 != null) {
          b1 = b2;
          if (layoutParams1.width == -1)
            b1 = -1; 
        } 
      } 
    } 
    paramPanelFeatureState.isHandled = false;
    WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(b1, -2, paramPanelFeatureState.x, paramPanelFeatureState.y, 1003, 8519680, paramPanelFeatureState.decorView.mDefaultOpacity);
    if (paramPanelFeatureState.isCompact) {
      layoutParams.gravity = getOptionsPanelGravity();
      sRotationWatcher.addWindow(this);
    } else {
      layoutParams.gravity = paramPanelFeatureState.gravity;
    } 
    layoutParams.windowAnimations = paramPanelFeatureState.windowAnimations;
    windowManager.addView((View)paramPanelFeatureState.decorView, (ViewGroup.LayoutParams)layoutParams);
    paramPanelFeatureState.isOpen = true;
  }
  
  public final void closePanel(int paramInt) {
    if (paramInt == 0) {
      DecorContentParent decorContentParent = this.mDecorContentParent;
      if (decorContentParent != null && 
        decorContentParent.canShowOverflowMenu() && 
        !ViewConfiguration.get(getContext()).hasPermanentMenuKey()) {
        this.mDecorContentParent.hideOverflowMenu();
        return;
      } 
    } 
    if (paramInt == 6) {
      closeContextMenu();
    } else {
      closePanel(getPanelState(paramInt, true), true);
    } 
  }
  
  public final void closePanel(PanelFeatureState paramPanelFeatureState, boolean paramBoolean) {
    if (paramBoolean && paramPanelFeatureState.featureId == 0) {
      DecorContentParent decorContentParent = this.mDecorContentParent;
      if (decorContentParent != null && 
        decorContentParent.isOverflowMenuShowing()) {
        checkCloseActionMenu(paramPanelFeatureState.menu);
        return;
      } 
    } 
    WindowManager windowManager = getWindowManager();
    if (windowManager != null && paramPanelFeatureState.isOpen) {
      if (paramPanelFeatureState.decorView != null) {
        windowManager.removeView((View)paramPanelFeatureState.decorView);
        if (paramPanelFeatureState.isCompact)
          sRotationWatcher.removeWindow(this); 
      } 
      if (paramBoolean)
        callOnPanelClosed(paramPanelFeatureState.featureId, paramPanelFeatureState, (Menu)null); 
    } 
    paramPanelFeatureState.isPrepared = false;
    paramPanelFeatureState.isHandled = false;
    paramPanelFeatureState.isOpen = false;
    paramPanelFeatureState.shownPanelView = null;
    if (paramPanelFeatureState.isInExpandedMode) {
      paramPanelFeatureState.refreshDecorView = true;
      paramPanelFeatureState.isInExpandedMode = false;
    } 
    if (this.mPreparedPanel == paramPanelFeatureState) {
      this.mPreparedPanel = null;
      this.mPanelChordingKey = 0;
    } 
  }
  
  void checkCloseActionMenu(Menu paramMenu) {
    if (this.mClosingActionMenu)
      return; 
    this.mClosingActionMenu = true;
    this.mDecorContentParent.dismissPopups();
    Window.Callback callback = getCallback();
    if (callback != null && !isDestroyed())
      callback.onPanelClosed(8, paramMenu); 
    this.mClosingActionMenu = false;
  }
  
  public final void togglePanel(int paramInt, KeyEvent paramKeyEvent) {
    PanelFeatureState panelFeatureState = getPanelState(paramInt, true);
    if (panelFeatureState.isOpen) {
      closePanel(panelFeatureState, true);
    } else {
      openPanel(panelFeatureState, paramKeyEvent);
    } 
  }
  
  public void invalidatePanelMenu(int paramInt) {
    this.mInvalidatePanelMenuFeatures |= 1 << paramInt;
    if (!this.mInvalidatePanelMenuPosted) {
      DecorView decorView = this.mDecor;
      if (decorView != null) {
        decorView.postOnAnimation(this.mInvalidatePanelMenuRunnable);
        this.mInvalidatePanelMenuPosted = true;
      } 
    } 
  }
  
  void doPendingInvalidatePanelMenu() {
    if (this.mInvalidatePanelMenuPosted) {
      this.mDecor.removeCallbacks(this.mInvalidatePanelMenuRunnable);
      this.mInvalidatePanelMenuRunnable.run();
    } 
  }
  
  void doInvalidatePanelMenu(int paramInt) {
    PanelFeatureState panelFeatureState = getPanelState(paramInt, false);
    if (panelFeatureState == null)
      return; 
    if (panelFeatureState.menu != null) {
      Bundle bundle = new Bundle();
      panelFeatureState.menu.saveActionViewStates(bundle);
      if (bundle.size() > 0)
        panelFeatureState.frozenActionViewState = bundle; 
      panelFeatureState.menu.stopDispatchingItemsChanged();
      panelFeatureState.menu.clear();
    } 
    panelFeatureState.refreshMenuContent = true;
    panelFeatureState.refreshDecorView = true;
    if ((paramInt == 8 || paramInt == 0) && this.mDecorContentParent != null) {
      PanelFeatureState panelFeatureState1 = getPanelState(0, false);
      if (panelFeatureState1 != null) {
        panelFeatureState1.isPrepared = false;
        preparePanel(panelFeatureState1, (KeyEvent)null);
      } 
    } 
  }
  
  public final boolean onKeyDownPanel(int paramInt, KeyEvent paramKeyEvent) {
    int i = paramKeyEvent.getKeyCode();
    if (paramKeyEvent.getRepeatCount() == 0) {
      this.mPanelChordingKey = i;
      PanelFeatureState panelFeatureState = getPanelState(paramInt, false);
      if (panelFeatureState != null && !panelFeatureState.isOpen)
        return preparePanel(panelFeatureState, paramKeyEvent); 
    } 
    return false;
  }
  
  public final void onKeyUpPanel(int paramInt, KeyEvent paramKeyEvent) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mPanelChordingKey : I
    //   4: ifeq -> 293
    //   7: aload_0
    //   8: iconst_0
    //   9: putfield mPanelChordingKey : I
    //   12: aload_0
    //   13: iload_1
    //   14: iconst_0
    //   15: invokevirtual getPanelState : (IZ)Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;
    //   18: astore_3
    //   19: aload_2
    //   20: invokevirtual isCanceled : ()Z
    //   23: ifne -> 292
    //   26: aload_0
    //   27: getfield mDecor : Lcom/android/internal/policy/DecorView;
    //   30: astore #4
    //   32: aload #4
    //   34: ifnull -> 45
    //   37: aload #4
    //   39: getfield mPrimaryActionMode : Landroid/view/ActionMode;
    //   42: ifnonnull -> 292
    //   45: aload_3
    //   46: ifnonnull -> 52
    //   49: goto -> 292
    //   52: iconst_0
    //   53: istore #5
    //   55: iload_1
    //   56: ifne -> 157
    //   59: aload_0
    //   60: getfield mDecorContentParent : Lcom/android/internal/widget/DecorContentParent;
    //   63: astore #4
    //   65: aload #4
    //   67: ifnull -> 157
    //   70: aload #4
    //   72: invokeinterface canShowOverflowMenu : ()Z
    //   77: ifeq -> 157
    //   80: aload_0
    //   81: invokevirtual getContext : ()Landroid/content/Context;
    //   84: invokestatic get : (Landroid/content/Context;)Landroid/view/ViewConfiguration;
    //   87: invokevirtual hasPermanentMenuKey : ()Z
    //   90: ifne -> 157
    //   93: aload_0
    //   94: getfield mDecorContentParent : Lcom/android/internal/widget/DecorContentParent;
    //   97: invokeinterface isOverflowMenuShowing : ()Z
    //   102: ifne -> 143
    //   105: iload #5
    //   107: istore #6
    //   109: aload_0
    //   110: invokevirtual isDestroyed : ()Z
    //   113: ifne -> 249
    //   116: iload #5
    //   118: istore #6
    //   120: aload_0
    //   121: aload_3
    //   122: aload_2
    //   123: invokevirtual preparePanel : (Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;Landroid/view/KeyEvent;)Z
    //   126: ifeq -> 249
    //   129: aload_0
    //   130: getfield mDecorContentParent : Lcom/android/internal/widget/DecorContentParent;
    //   133: invokeinterface showOverflowMenu : ()Z
    //   138: istore #6
    //   140: goto -> 249
    //   143: aload_0
    //   144: getfield mDecorContentParent : Lcom/android/internal/widget/DecorContentParent;
    //   147: invokeinterface hideOverflowMenu : ()Z
    //   152: istore #6
    //   154: goto -> 249
    //   157: aload_3
    //   158: getfield isOpen : Z
    //   161: ifne -> 237
    //   164: aload_3
    //   165: getfield isHandled : Z
    //   168: ifeq -> 174
    //   171: goto -> 237
    //   174: iload #5
    //   176: istore #6
    //   178: aload_3
    //   179: getfield isPrepared : Z
    //   182: ifeq -> 249
    //   185: iconst_1
    //   186: istore #7
    //   188: aload_3
    //   189: getfield refreshMenuContent : Z
    //   192: ifeq -> 208
    //   195: aload_3
    //   196: iconst_0
    //   197: putfield isPrepared : Z
    //   200: aload_0
    //   201: aload_3
    //   202: aload_2
    //   203: invokevirtual preparePanel : (Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;Landroid/view/KeyEvent;)Z
    //   206: istore #7
    //   208: iload #5
    //   210: istore #6
    //   212: iload #7
    //   214: ifeq -> 249
    //   217: ldc_w 50001
    //   220: iconst_0
    //   221: invokestatic writeEvent : (II)I
    //   224: pop
    //   225: aload_0
    //   226: aload_3
    //   227: aload_2
    //   228: invokespecial openPanel : (Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;Landroid/view/KeyEvent;)V
    //   231: iconst_1
    //   232: istore #6
    //   234: goto -> 249
    //   237: aload_3
    //   238: getfield isOpen : Z
    //   241: istore #6
    //   243: aload_0
    //   244: aload_3
    //   245: iconst_1
    //   246: invokevirtual closePanel : (Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;Z)V
    //   249: iload #6
    //   251: ifeq -> 293
    //   254: aload_0
    //   255: invokevirtual getContext : ()Landroid/content/Context;
    //   258: ldc_w 'audio'
    //   261: invokevirtual getSystemService : (Ljava/lang/String;)Ljava/lang/Object;
    //   264: checkcast android/media/AudioManager
    //   267: astore_2
    //   268: aload_2
    //   269: ifnull -> 280
    //   272: aload_2
    //   273: iconst_0
    //   274: invokevirtual playSoundEffect : (I)V
    //   277: goto -> 293
    //   280: ldc 'PhoneWindow'
    //   282: ldc_w 'Couldn't get audio manager'
    //   285: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   288: pop
    //   289: goto -> 293
    //   292: return
    //   293: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1112	-> 0
    //   #1113	-> 7
    //   #1115	-> 12
    //   #1117	-> 19
    //   #1122	-> 52
    //   #1123	-> 55
    //   #1124	-> 70
    //   #1125	-> 80
    //   #1126	-> 93
    //   #1127	-> 105
    //   #1128	-> 129
    //   #1131	-> 143
    //   #1134	-> 157
    //   #1143	-> 174
    //   #1144	-> 185
    //   #1145	-> 188
    //   #1148	-> 195
    //   #1149	-> 200
    //   #1152	-> 208
    //   #1154	-> 217
    //   #1157	-> 225
    //   #1159	-> 231
    //   #1138	-> 237
    //   #1141	-> 243
    //   #1164	-> 249
    //   #1165	-> 254
    //   #1167	-> 268
    //   #1168	-> 272
    //   #1170	-> 280
    //   #1119	-> 292
    //   #1174	-> 293
  }
  
  public final void closeAllPanels() {
    byte b1;
    WindowManager windowManager = getWindowManager();
    if (windowManager == null)
      return; 
    PanelFeatureState[] arrayOfPanelFeatureState = this.mPanels;
    if (arrayOfPanelFeatureState != null) {
      b1 = arrayOfPanelFeatureState.length;
    } else {
      b1 = 0;
    } 
    for (byte b2 = 0; b2 < b1; b2++) {
      PanelFeatureState panelFeatureState = arrayOfPanelFeatureState[b2];
      if (panelFeatureState != null)
        closePanel(panelFeatureState, true); 
    } 
    closeContextMenu();
  }
  
  private void closeContextMenu() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mContextMenu : Lcom/android/internal/view/menu/ContextMenuBuilder;
    //   6: ifnull -> 20
    //   9: aload_0
    //   10: getfield mContextMenu : Lcom/android/internal/view/menu/ContextMenuBuilder;
    //   13: invokevirtual close : ()V
    //   16: aload_0
    //   17: invokespecial dismissContextMenu : ()V
    //   20: aload_0
    //   21: monitorexit
    //   22: return
    //   23: astore_1
    //   24: aload_0
    //   25: monitorexit
    //   26: aload_1
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1200	-> 2
    //   #1201	-> 9
    //   #1202	-> 16
    //   #1204	-> 20
    //   #1199	-> 23
    // Exception table:
    //   from	to	target	type
    //   2	9	23	finally
    //   9	16	23	finally
    //   16	20	23	finally
  }
  
  private void dismissContextMenu() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aconst_null
    //   4: putfield mContextMenu : Lcom/android/internal/view/menu/ContextMenuBuilder;
    //   7: aload_0
    //   8: getfield mContextMenuHelper : Lcom/android/internal/view/menu/MenuHelper;
    //   11: ifnull -> 28
    //   14: aload_0
    //   15: getfield mContextMenuHelper : Lcom/android/internal/view/menu/MenuHelper;
    //   18: invokeinterface dismiss : ()V
    //   23: aload_0
    //   24: aconst_null
    //   25: putfield mContextMenuHelper : Lcom/android/internal/view/menu/MenuHelper;
    //   28: aload_0
    //   29: monitorexit
    //   30: return
    //   31: astore_1
    //   32: aload_0
    //   33: monitorexit
    //   34: aload_1
    //   35: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1211	-> 2
    //   #1213	-> 7
    //   #1214	-> 14
    //   #1215	-> 23
    //   #1217	-> 28
    //   #1210	-> 31
    // Exception table:
    //   from	to	target	type
    //   2	7	31	finally
    //   7	14	31	finally
    //   14	23	31	finally
    //   23	28	31	finally
  }
  
  public boolean performPanelShortcut(int paramInt1, int paramInt2, KeyEvent paramKeyEvent, int paramInt3) {
    return performPanelShortcut(getPanelState(paramInt1, false), paramInt2, paramKeyEvent, paramInt3);
  }
  
  boolean performPanelShortcut(PanelFeatureState paramPanelFeatureState, int paramInt1, KeyEvent paramKeyEvent, int paramInt2) {
    // Byte code:
    //   0: aload_3
    //   1: invokevirtual isSystem : ()Z
    //   4: ifne -> 94
    //   7: aload_1
    //   8: ifnonnull -> 14
    //   11: goto -> 94
    //   14: iconst_0
    //   15: istore #5
    //   17: aload_1
    //   18: getfield isPrepared : Z
    //   21: ifne -> 37
    //   24: iload #5
    //   26: istore #6
    //   28: aload_0
    //   29: aload_1
    //   30: aload_3
    //   31: invokevirtual preparePanel : (Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;Landroid/view/KeyEvent;)Z
    //   34: ifeq -> 61
    //   37: iload #5
    //   39: istore #6
    //   41: aload_1
    //   42: getfield menu : Lcom/android/internal/view/menu/MenuBuilder;
    //   45: ifnull -> 61
    //   48: aload_1
    //   49: getfield menu : Lcom/android/internal/view/menu/MenuBuilder;
    //   52: iload_2
    //   53: aload_3
    //   54: iload #4
    //   56: invokevirtual performShortcut : (ILandroid/view/KeyEvent;I)Z
    //   59: istore #6
    //   61: iload #6
    //   63: ifeq -> 91
    //   66: aload_1
    //   67: iconst_1
    //   68: putfield isHandled : Z
    //   71: iload #4
    //   73: iconst_1
    //   74: iand
    //   75: ifne -> 91
    //   78: aload_0
    //   79: getfield mDecorContentParent : Lcom/android/internal/widget/DecorContentParent;
    //   82: ifnonnull -> 91
    //   85: aload_0
    //   86: aload_1
    //   87: iconst_1
    //   88: invokevirtual closePanel : (Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;Z)V
    //   91: iload #6
    //   93: ireturn
    //   94: iconst_0
    //   95: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1226	-> 0
    //   #1230	-> 14
    //   #1234	-> 17
    //   #1236	-> 48
    //   #1239	-> 61
    //   #1241	-> 66
    //   #1244	-> 71
    //   #1245	-> 85
    //   #1249	-> 91
    //   #1227	-> 94
  }
  
  public boolean performPanelIdentifierAction(int paramInt1, int paramInt2, int paramInt3) {
    PanelFeatureState panelFeatureState = getPanelState(paramInt1, true);
    if (!preparePanel(panelFeatureState, new KeyEvent(0, 82)))
      return false; 
    if (panelFeatureState.menu == null)
      return false; 
    boolean bool = panelFeatureState.menu.performIdentifierAction(paramInt2, paramInt3);
    if (this.mDecorContentParent == null)
      closePanel(panelFeatureState, true); 
    return bool;
  }
  
  public PanelFeatureState findMenuPanel(Menu paramMenu) {
    byte b1;
    PanelFeatureState[] arrayOfPanelFeatureState = this.mPanels;
    if (arrayOfPanelFeatureState != null) {
      b1 = arrayOfPanelFeatureState.length;
    } else {
      b1 = 0;
    } 
    for (byte b2 = 0; b2 < b1; b2++) {
      PanelFeatureState panelFeatureState = arrayOfPanelFeatureState[b2];
      if (panelFeatureState != null && panelFeatureState.menu == paramMenu)
        return panelFeatureState; 
    } 
    return null;
  }
  
  public boolean onMenuItemSelected(MenuBuilder paramMenuBuilder, MenuItem paramMenuItem) {
    Window.Callback callback = getCallback();
    if (callback != null && !isDestroyed()) {
      PanelFeatureState panelFeatureState = findMenuPanel(paramMenuBuilder.getRootMenu());
      if (panelFeatureState != null)
        return callback.onMenuItemSelected(panelFeatureState.featureId, paramMenuItem); 
    } 
    return false;
  }
  
  public void onMenuModeChange(MenuBuilder paramMenuBuilder) {
    reopenMenu(true);
  }
  
  private void reopenMenu(boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mDecorContentParent : Lcom/android/internal/widget/DecorContentParent;
    //   4: astore_2
    //   5: aload_2
    //   6: ifnull -> 244
    //   9: aload_2
    //   10: invokeinterface canShowOverflowMenu : ()Z
    //   15: ifeq -> 244
    //   18: aload_0
    //   19: invokevirtual getContext : ()Landroid/content/Context;
    //   22: invokestatic get : (Landroid/content/Context;)Landroid/view/ViewConfiguration;
    //   25: invokevirtual hasPermanentMenuKey : ()Z
    //   28: ifeq -> 45
    //   31: aload_0
    //   32: getfield mDecorContentParent : Lcom/android/internal/widget/DecorContentParent;
    //   35: astore_2
    //   36: aload_2
    //   37: invokeinterface isOverflowMenuShowPending : ()Z
    //   42: ifeq -> 244
    //   45: aload_0
    //   46: invokevirtual getCallback : ()Landroid/view/Window$Callback;
    //   49: astore_2
    //   50: aload_0
    //   51: getfield mDecorContentParent : Lcom/android/internal/widget/DecorContentParent;
    //   54: invokeinterface isOverflowMenuShowing : ()Z
    //   59: ifeq -> 116
    //   62: iload_1
    //   63: ifne -> 69
    //   66: goto -> 116
    //   69: aload_0
    //   70: getfield mDecorContentParent : Lcom/android/internal/widget/DecorContentParent;
    //   73: invokeinterface hideOverflowMenu : ()Z
    //   78: pop
    //   79: aload_0
    //   80: iconst_0
    //   81: iconst_0
    //   82: invokevirtual getPanelState : (IZ)Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;
    //   85: astore_3
    //   86: aload_3
    //   87: ifnull -> 243
    //   90: aload_2
    //   91: ifnull -> 243
    //   94: aload_0
    //   95: invokevirtual isDestroyed : ()Z
    //   98: ifne -> 243
    //   101: aload_2
    //   102: bipush #8
    //   104: aload_3
    //   105: getfield menu : Lcom/android/internal/view/menu/MenuBuilder;
    //   108: invokeinterface onPanelClosed : (ILandroid/view/Menu;)V
    //   113: goto -> 243
    //   116: aload_2
    //   117: ifnull -> 243
    //   120: aload_0
    //   121: invokevirtual isDestroyed : ()Z
    //   124: ifne -> 243
    //   127: aload_0
    //   128: getfield mInvalidatePanelMenuPosted : Z
    //   131: ifeq -> 164
    //   134: iconst_1
    //   135: aload_0
    //   136: getfield mInvalidatePanelMenuFeatures : I
    //   139: iand
    //   140: ifeq -> 164
    //   143: aload_0
    //   144: getfield mDecor : Lcom/android/internal/policy/DecorView;
    //   147: aload_0
    //   148: getfield mInvalidatePanelMenuRunnable : Ljava/lang/Runnable;
    //   151: invokevirtual removeCallbacks : (Ljava/lang/Runnable;)Z
    //   154: pop
    //   155: aload_0
    //   156: getfield mInvalidatePanelMenuRunnable : Ljava/lang/Runnable;
    //   159: invokeinterface run : ()V
    //   164: aload_0
    //   165: iconst_0
    //   166: iconst_0
    //   167: invokevirtual getPanelState : (IZ)Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;
    //   170: astore #4
    //   172: aload #4
    //   174: ifnull -> 243
    //   177: aload #4
    //   179: getfield menu : Lcom/android/internal/view/menu/MenuBuilder;
    //   182: ifnull -> 243
    //   185: aload #4
    //   187: getfield refreshMenuContent : Z
    //   190: ifne -> 243
    //   193: aload #4
    //   195: getfield createdPanelView : Landroid/view/View;
    //   198: astore_3
    //   199: aload #4
    //   201: getfield menu : Lcom/android/internal/view/menu/MenuBuilder;
    //   204: astore #5
    //   206: aload_2
    //   207: iconst_0
    //   208: aload_3
    //   209: aload #5
    //   211: invokeinterface onPreparePanel : (ILandroid/view/View;Landroid/view/Menu;)Z
    //   216: ifeq -> 243
    //   219: aload_2
    //   220: bipush #8
    //   222: aload #4
    //   224: getfield menu : Lcom/android/internal/view/menu/MenuBuilder;
    //   227: invokeinterface onMenuOpened : (ILandroid/view/Menu;)Z
    //   232: pop
    //   233: aload_0
    //   234: getfield mDecorContentParent : Lcom/android/internal/widget/DecorContentParent;
    //   237: invokeinterface showOverflowMenu : ()Z
    //   242: pop
    //   243: return
    //   244: aload_0
    //   245: iconst_0
    //   246: iconst_0
    //   247: invokevirtual getPanelState : (IZ)Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;
    //   250: astore_2
    //   251: aload_2
    //   252: ifnonnull -> 256
    //   255: return
    //   256: aload_2
    //   257: getfield isInExpandedMode : Z
    //   260: istore #6
    //   262: iload #6
    //   264: istore #7
    //   266: iload_1
    //   267: ifeq -> 284
    //   270: iload #6
    //   272: ifne -> 281
    //   275: iconst_1
    //   276: istore #7
    //   278: goto -> 284
    //   281: iconst_0
    //   282: istore #7
    //   284: aload_2
    //   285: iconst_1
    //   286: putfield refreshDecorView : Z
    //   289: aload_0
    //   290: aload_2
    //   291: iconst_0
    //   292: invokevirtual closePanel : (Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;Z)V
    //   295: aload_2
    //   296: iload #7
    //   298: putfield isInExpandedMode : Z
    //   301: aload_0
    //   302: aload_2
    //   303: aconst_null
    //   304: invokespecial openPanel : (Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;Landroid/view/KeyEvent;)V
    //   307: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1301	-> 0
    //   #1302	-> 18
    //   #1303	-> 36
    //   #1304	-> 45
    //   #1305	-> 50
    //   #1325	-> 69
    //   #1326	-> 79
    //   #1327	-> 86
    //   #1328	-> 101
    //   #1306	-> 116
    //   #1308	-> 127
    //   #1310	-> 143
    //   #1311	-> 155
    //   #1314	-> 164
    //   #1318	-> 172
    //   #1319	-> 206
    //   #1320	-> 219
    //   #1321	-> 233
    //   #1331	-> 243
    //   #1334	-> 244
    //   #1336	-> 251
    //   #1337	-> 255
    //   #1341	-> 256
    //   #1343	-> 284
    //   #1344	-> 289
    //   #1347	-> 295
    //   #1349	-> 301
    //   #1350	-> 307
  }
  
  protected boolean initializePanelMenu(PanelFeatureState paramPanelFeatureState) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual getContext : ()Landroid/content/Context;
    //   4: astore_2
    //   5: aload_1
    //   6: getfield featureId : I
    //   9: ifeq -> 23
    //   12: aload_2
    //   13: astore_3
    //   14: aload_1
    //   15: getfield featureId : I
    //   18: bipush #8
    //   20: if_icmpne -> 190
    //   23: aload_2
    //   24: astore_3
    //   25: aload_0
    //   26: getfield mDecorContentParent : Lcom/android/internal/widget/DecorContentParent;
    //   29: ifnull -> 190
    //   32: new android/util/TypedValue
    //   35: dup
    //   36: invokespecial <init> : ()V
    //   39: astore #4
    //   41: aload_2
    //   42: invokevirtual getTheme : ()Landroid/content/res/Resources$Theme;
    //   45: astore #5
    //   47: aload #5
    //   49: ldc_w 16843825
    //   52: aload #4
    //   54: iconst_1
    //   55: invokevirtual resolveAttribute : (ILandroid/util/TypedValue;Z)Z
    //   58: pop
    //   59: aconst_null
    //   60: astore_3
    //   61: aload #4
    //   63: getfield resourceId : I
    //   66: ifeq -> 107
    //   69: aload_2
    //   70: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   73: invokevirtual newTheme : ()Landroid/content/res/Resources$Theme;
    //   76: astore_3
    //   77: aload_3
    //   78: aload #5
    //   80: invokevirtual setTo : (Landroid/content/res/Resources$Theme;)V
    //   83: aload_3
    //   84: aload #4
    //   86: getfield resourceId : I
    //   89: iconst_1
    //   90: invokevirtual applyStyle : (IZ)V
    //   93: aload_3
    //   94: ldc_w 16843671
    //   97: aload #4
    //   99: iconst_1
    //   100: invokevirtual resolveAttribute : (ILandroid/util/TypedValue;Z)Z
    //   103: pop
    //   104: goto -> 119
    //   107: aload #5
    //   109: ldc_w 16843671
    //   112: aload #4
    //   114: iconst_1
    //   115: invokevirtual resolveAttribute : (ILandroid/util/TypedValue;Z)Z
    //   118: pop
    //   119: aload_3
    //   120: astore #6
    //   122: aload #4
    //   124: getfield resourceId : I
    //   127: ifeq -> 164
    //   130: aload_3
    //   131: astore #6
    //   133: aload_3
    //   134: ifnonnull -> 153
    //   137: aload_2
    //   138: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   141: invokevirtual newTheme : ()Landroid/content/res/Resources$Theme;
    //   144: astore #6
    //   146: aload #6
    //   148: aload #5
    //   150: invokevirtual setTo : (Landroid/content/res/Resources$Theme;)V
    //   153: aload #6
    //   155: aload #4
    //   157: getfield resourceId : I
    //   160: iconst_1
    //   161: invokevirtual applyStyle : (IZ)V
    //   164: aload_2
    //   165: astore_3
    //   166: aload #6
    //   168: ifnull -> 190
    //   171: new android/view/ContextThemeWrapper
    //   174: dup
    //   175: aload_2
    //   176: iconst_0
    //   177: invokespecial <init> : (Landroid/content/Context;I)V
    //   180: astore_3
    //   181: aload_3
    //   182: invokevirtual getTheme : ()Landroid/content/res/Resources$Theme;
    //   185: aload #6
    //   187: invokevirtual setTo : (Landroid/content/res/Resources$Theme;)V
    //   190: new com/android/internal/view/menu/MenuBuilder
    //   193: dup
    //   194: aload_3
    //   195: invokespecial <init> : (Landroid/content/Context;)V
    //   198: astore_3
    //   199: aload_3
    //   200: aload_0
    //   201: invokevirtual setCallback : (Lcom/android/internal/view/menu/MenuBuilder$Callback;)V
    //   204: aload_1
    //   205: aload_3
    //   206: invokevirtual setMenu : (Lcom/android/internal/view/menu/MenuBuilder;)V
    //   209: iconst_1
    //   210: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1362	-> 0
    //   #1365	-> 5
    //   #1367	-> 32
    //   #1368	-> 41
    //   #1369	-> 47
    //   #1371	-> 59
    //   #1372	-> 61
    //   #1373	-> 69
    //   #1374	-> 77
    //   #1375	-> 83
    //   #1376	-> 93
    //   #1379	-> 107
    //   #1383	-> 119
    //   #1384	-> 130
    //   #1385	-> 137
    //   #1386	-> 146
    //   #1388	-> 153
    //   #1391	-> 164
    //   #1392	-> 171
    //   #1393	-> 181
    //   #1397	-> 190
    //   #1398	-> 199
    //   #1399	-> 204
    //   #1401	-> 209
  }
  
  protected boolean initializePanelDecor(PanelFeatureState paramPanelFeatureState) {
    paramPanelFeatureState.decorView = generateDecor(paramPanelFeatureState.featureId);
    paramPanelFeatureState.gravity = 81;
    paramPanelFeatureState.setStyle(getContext());
    TypedArray typedArray = getContext().obtainStyledAttributes(null, R.styleable.Window, 0, paramPanelFeatureState.listPresenterTheme);
    float f = typedArray.getDimension(37, 0.0F);
    if (f != 0.0F)
      paramPanelFeatureState.decorView.setElevation(f); 
    typedArray.recycle();
    return true;
  }
  
  private int getOptionsPanelGravity() {
    try {
      IWindowManager iWindowManager = WindowManagerHolder.sWindowManager;
      null = getContext().getDisplayId();
      return iWindowManager.getPreferredOptionsPanelGravity(null);
    } catch (RemoteException remoteException) {
      Log.e("PhoneWindow", "Couldn't getOptionsPanelGravity; using default", (Throwable)remoteException);
      return 81;
    } 
  }
  
  void onOptionsPanelRotationChanged() {
    ViewGroup.LayoutParams layoutParams;
    PanelFeatureState panelFeatureState = getPanelState(0, false);
    if (panelFeatureState == null)
      return; 
    if (panelFeatureState.decorView != null) {
      layoutParams = panelFeatureState.decorView.getLayoutParams();
    } else {
      layoutParams = null;
    } 
    if (layoutParams != null) {
      ((WindowManager.LayoutParams)layoutParams).gravity = getOptionsPanelGravity();
      WindowManager windowManager = getWindowManager();
      if (windowManager != null)
        windowManager.updateViewLayout((View)panelFeatureState.decorView, layoutParams); 
    } 
  }
  
  protected boolean initializePanelContent(PanelFeatureState paramPanelFeatureState) {
    MenuView menuView;
    if (paramPanelFeatureState.createdPanelView != null) {
      paramPanelFeatureState.shownPanelView = paramPanelFeatureState.createdPanelView;
      return true;
    } 
    if (paramPanelFeatureState.menu == null)
      return false; 
    if (this.mPanelMenuPresenterCallback == null)
      this.mPanelMenuPresenterCallback = new PanelMenuPresenterCallback(); 
    if (paramPanelFeatureState.isInListMode()) {
      menuView = paramPanelFeatureState.getListMenuView(getContext(), this.mPanelMenuPresenterCallback);
    } else {
      menuView = paramPanelFeatureState.getIconMenuView(getContext(), this.mPanelMenuPresenterCallback);
    } 
    paramPanelFeatureState.shownPanelView = (View)menuView;
    if (paramPanelFeatureState.shownPanelView != null) {
      int i = menuView.getWindowAnimations();
      if (i != 0)
        paramPanelFeatureState.windowAnimations = i; 
      return true;
    } 
    return false;
  }
  
  public boolean performContextMenuIdentifierAction(int paramInt1, int paramInt2) {
    boolean bool;
    ContextMenuBuilder contextMenuBuilder = this.mContextMenu;
    if (contextMenuBuilder != null) {
      bool = contextMenuBuilder.performIdentifierAction(paramInt1, paramInt2);
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final void setElevation(float paramFloat) {
    this.mElevation = paramFloat;
    WindowManager.LayoutParams layoutParams = getAttributes();
    DecorView decorView = this.mDecor;
    if (decorView != null) {
      decorView.setElevation(paramFloat);
      layoutParams.setSurfaceInsets((View)this.mDecor, true, false);
    } 
    dispatchWindowAttributesChanged(layoutParams);
  }
  
  public float getElevation() {
    return this.mElevation;
  }
  
  public final void setClipToOutline(boolean paramBoolean) {
    this.mClipToOutline = paramBoolean;
    DecorView decorView = this.mDecor;
    if (decorView != null)
      decorView.setClipToOutline(paramBoolean); 
  }
  
  public final void setBackgroundDrawable(Drawable paramDrawable) {
    if (paramDrawable != this.mBackgroundDrawable) {
      this.mBackgroundDrawable = paramDrawable;
      DecorView decorView = this.mDecor;
      if (decorView != null) {
        decorView.setWindowBackground(paramDrawable);
        Drawable drawable = this.mBackgroundFallbackDrawable;
        if (drawable != null) {
          DecorView decorView1 = this.mDecor;
          if (paramDrawable != null) {
            paramDrawable = null;
          } else {
            paramDrawable = drawable;
          } 
          decorView1.setBackgroundFallback(paramDrawable);
        } 
      } 
    } 
  }
  
  public final void setFeatureDrawableResource(int paramInt1, int paramInt2) {
    if (paramInt2 != 0) {
      DrawableFeatureState drawableFeatureState = getDrawableState(paramInt1, true);
      if (drawableFeatureState.resid != paramInt2) {
        drawableFeatureState.resid = paramInt2;
        drawableFeatureState.uri = null;
        drawableFeatureState.local = getContext().getDrawable(paramInt2);
        updateDrawable(paramInt1, drawableFeatureState, false);
      } 
    } else {
      setFeatureDrawable(paramInt1, (Drawable)null);
    } 
  }
  
  public final void setFeatureDrawableUri(int paramInt, Uri paramUri) {
    if (paramUri != null) {
      DrawableFeatureState drawableFeatureState = getDrawableState(paramInt, true);
      if (drawableFeatureState.uri == null || !drawableFeatureState.uri.equals(paramUri)) {
        drawableFeatureState.resid = 0;
        drawableFeatureState.uri = paramUri;
        drawableFeatureState.local = loadImageURI(paramUri);
        updateDrawable(paramInt, drawableFeatureState, false);
      } 
    } else {
      setFeatureDrawable(paramInt, (Drawable)null);
    } 
  }
  
  public final void setFeatureDrawable(int paramInt, Drawable paramDrawable) {
    DrawableFeatureState drawableFeatureState = getDrawableState(paramInt, true);
    drawableFeatureState.resid = 0;
    drawableFeatureState.uri = null;
    if (drawableFeatureState.local != paramDrawable) {
      drawableFeatureState.local = paramDrawable;
      updateDrawable(paramInt, drawableFeatureState, false);
    } 
  }
  
  public void setFeatureDrawableAlpha(int paramInt1, int paramInt2) {
    DrawableFeatureState drawableFeatureState = getDrawableState(paramInt1, true);
    if (drawableFeatureState.alpha != paramInt2) {
      drawableFeatureState.alpha = paramInt2;
      updateDrawable(paramInt1, drawableFeatureState, false);
    } 
  }
  
  protected final void setFeatureDefaultDrawable(int paramInt, Drawable paramDrawable) {
    DrawableFeatureState drawableFeatureState = getDrawableState(paramInt, true);
    if (drawableFeatureState.def != paramDrawable) {
      drawableFeatureState.def = paramDrawable;
      updateDrawable(paramInt, drawableFeatureState, false);
    } 
  }
  
  public final void setFeatureInt(int paramInt1, int paramInt2) {
    updateInt(paramInt1, paramInt2, false);
  }
  
  protected final void updateDrawable(int paramInt, boolean paramBoolean) {
    DrawableFeatureState drawableFeatureState = getDrawableState(paramInt, false);
    if (drawableFeatureState != null)
      updateDrawable(paramInt, drawableFeatureState, paramBoolean); 
  }
  
  protected void onDrawableChanged(int paramInt1, Drawable paramDrawable, int paramInt2) {
    ImageView imageView;
    if (paramInt1 == 3) {
      imageView = getLeftIconView();
    } else if (paramInt1 == 4) {
      imageView = getRightIconView();
    } else {
      return;
    } 
    if (paramDrawable != null) {
      paramDrawable.setAlpha(paramInt2);
      imageView.setImageDrawable(paramDrawable);
      imageView.setVisibility(0);
    } else {
      imageView.setVisibility(8);
    } 
  }
  
  protected void onIntChanged(int paramInt1, int paramInt2) {
    if (paramInt1 == 2 || paramInt1 == 5) {
      updateProgressBars(paramInt2);
      return;
    } 
    if (paramInt1 == 7) {
      FrameLayout frameLayout = (FrameLayout)findViewById(16909546);
      if (frameLayout != null)
        this.mLayoutInflater.inflate(paramInt2, (ViewGroup)frameLayout); 
    } 
  }
  
  private void updateProgressBars(int paramInt) {
    ProgressBar progressBar1 = getCircularProgressBar(true);
    ProgressBar progressBar2 = getHorizontalProgressBar(true);
    int i = getLocalFeatures();
    if (paramInt == -1) {
      if ((i & 0x4) != 0)
        if (progressBar2 != null) {
          paramInt = progressBar2.getProgress();
          if (progressBar2.isIndeterminate() || paramInt < 10000) {
            paramInt = 0;
          } else {
            paramInt = 4;
          } 
          progressBar2.setVisibility(paramInt);
        } else {
          Log.e("PhoneWindow", "Horizontal progress bar not located in current window decor");
        }  
      if ((i & 0x20) != 0)
        if (progressBar1 != null) {
          progressBar1.setVisibility(0);
        } else {
          Log.e("PhoneWindow", "Circular progress bar not located in current window decor");
        }  
    } else if (paramInt == -2) {
      if ((i & 0x4) != 0)
        if (progressBar2 != null) {
          progressBar2.setVisibility(8);
        } else {
          Log.e("PhoneWindow", "Horizontal progress bar not located in current window decor");
        }  
      if ((i & 0x20) != 0)
        if (progressBar1 != null) {
          progressBar1.setVisibility(8);
        } else {
          Log.e("PhoneWindow", "Circular progress bar not located in current window decor");
        }  
    } else if (paramInt == -3) {
      if (progressBar2 != null) {
        progressBar2.setIndeterminate(true);
      } else {
        Log.e("PhoneWindow", "Horizontal progress bar not located in current window decor");
      } 
    } else if (paramInt == -4) {
      if (progressBar2 != null) {
        progressBar2.setIndeterminate(false);
      } else {
        Log.e("PhoneWindow", "Horizontal progress bar not located in current window decor");
      } 
    } else if (paramInt >= 0 && paramInt <= 10000) {
      if (progressBar2 != null) {
        progressBar2.setProgress(paramInt + 0);
      } else {
        Log.e("PhoneWindow", "Horizontal progress bar not located in current window decor");
      } 
      if (paramInt < 10000) {
        showProgressBars(progressBar2, progressBar1);
      } else {
        hideProgressBars(progressBar2, progressBar1);
      } 
    } else if (20000 <= paramInt && paramInt <= 30000) {
      if (progressBar2 != null) {
        progressBar2.setSecondaryProgress(paramInt - 20000);
      } else {
        Log.e("PhoneWindow", "Horizontal progress bar not located in current window decor");
      } 
      showProgressBars(progressBar2, progressBar1);
    } 
  }
  
  private void showProgressBars(ProgressBar paramProgressBar1, ProgressBar paramProgressBar2) {
    int i = getLocalFeatures();
    if ((i & 0x20) != 0 && paramProgressBar2 != null && 
      paramProgressBar2.getVisibility() == 4)
      paramProgressBar2.setVisibility(0); 
    if ((i & 0x4) != 0 && paramProgressBar1 != null && 
      paramProgressBar1.getProgress() < 10000)
      paramProgressBar1.setVisibility(0); 
  }
  
  private void hideProgressBars(ProgressBar paramProgressBar1, ProgressBar paramProgressBar2) {
    int i = getLocalFeatures();
    Animation animation = AnimationUtils.loadAnimation(getContext(), 17432577);
    animation.setDuration(1000L);
    if ((i & 0x20) != 0 && paramProgressBar2 != null)
      if (paramProgressBar2.getVisibility() == 0) {
        paramProgressBar2.startAnimation(animation);
        paramProgressBar2.setVisibility(4);
      }  
    if ((i & 0x4) != 0 && paramProgressBar1 != null && 
      paramProgressBar1.getVisibility() == 0) {
      paramProgressBar1.startAnimation(animation);
      paramProgressBar1.setVisibility(4);
    } 
  }
  
  public void setIcon(int paramInt) {
    this.mIconRes = paramInt;
    int i = this.mResourcesSetFlags | 0x1;
    this.mResourcesSetFlags = i & 0xFFFFFFFB;
    DecorContentParent decorContentParent = this.mDecorContentParent;
    if (decorContentParent != null)
      decorContentParent.setIcon(paramInt); 
  }
  
  public void setDefaultIcon(int paramInt) {
    if ((this.mResourcesSetFlags & 0x1) != 0)
      return; 
    this.mIconRes = paramInt;
    DecorContentParent decorContentParent = this.mDecorContentParent;
    if (decorContentParent != null && (!decorContentParent.hasIcon() || (this.mResourcesSetFlags & 0x4) != 0))
      if (paramInt != 0) {
        this.mDecorContentParent.setIcon(paramInt);
        this.mResourcesSetFlags &= 0xFFFFFFFB;
      } else {
        DecorContentParent decorContentParent1 = this.mDecorContentParent;
        Drawable drawable = getContext().getPackageManager().getDefaultActivityIcon();
        decorContentParent1.setIcon(drawable);
        this.mResourcesSetFlags |= 0x4;
      }  
  }
  
  public void setLogo(int paramInt) {
    this.mLogoRes = paramInt;
    this.mResourcesSetFlags |= 0x2;
    DecorContentParent decorContentParent = this.mDecorContentParent;
    if (decorContentParent != null)
      decorContentParent.setLogo(paramInt); 
  }
  
  public void setDefaultLogo(int paramInt) {
    if ((this.mResourcesSetFlags & 0x2) != 0)
      return; 
    this.mLogoRes = paramInt;
    DecorContentParent decorContentParent = this.mDecorContentParent;
    if (decorContentParent != null && !decorContentParent.hasLogo())
      this.mDecorContentParent.setLogo(paramInt); 
  }
  
  public void setLocalFocus(boolean paramBoolean1, boolean paramBoolean2) {
    getViewRootImpl().windowFocusChanged(paramBoolean1, paramBoolean2);
  }
  
  public void injectInputEvent(InputEvent paramInputEvent) {
    getViewRootImpl().dispatchInputEvent(paramInputEvent);
  }
  
  private ViewRootImpl getViewRootImpl() {
    ViewRootImpl viewRootImpl = getViewRootImplOrNull();
    if (viewRootImpl != null)
      return viewRootImpl; 
    throw new IllegalStateException("view not added");
  }
  
  private ViewRootImpl getViewRootImplOrNull() {
    DecorView decorView = this.mDecor;
    if (decorView == null)
      return null; 
    return decorView.getViewRootImpl();
  }
  
  public void takeKeyEvents(boolean paramBoolean) {
    this.mDecor.setFocusable(paramBoolean);
  }
  
  public boolean superDispatchKeyEvent(KeyEvent paramKeyEvent) {
    return this.mDecor.superDispatchKeyEvent(paramKeyEvent);
  }
  
  public boolean superDispatchKeyShortcutEvent(KeyEvent paramKeyEvent) {
    return this.mDecor.superDispatchKeyShortcutEvent(paramKeyEvent);
  }
  
  public boolean superDispatchTouchEvent(MotionEvent paramMotionEvent) {
    return this.mDecor.superDispatchTouchEvent(paramMotionEvent);
  }
  
  public boolean superDispatchTrackballEvent(MotionEvent paramMotionEvent) {
    return this.mDecor.superDispatchTrackballEvent(paramMotionEvent);
  }
  
  public boolean superDispatchGenericMotionEvent(MotionEvent paramMotionEvent) {
    return this.mDecor.superDispatchGenericMotionEvent(paramMotionEvent);
  }
  
  protected boolean onKeyDown(int paramInt1, int paramInt2, KeyEvent paramKeyEvent) {
    MediaSessionManager mediaSessionManager;
    DecorView decorView = this.mDecor;
    if (decorView != null) {
      KeyEvent.DispatcherState dispatcherState = decorView.getKeyDispatcherState();
    } else {
      decorView = null;
    } 
    boolean bool = false;
    if (paramInt2 != 4) {
      if (paramInt2 != 79)
        if (paramInt2 != 82) {
          if (paramInt2 != 130)
            if (paramInt2 != 164 && paramInt2 != 24 && paramInt2 != 25) {
              if (paramInt2 != 126 && paramInt2 != 127)
                switch (paramInt2) {
                  default:
                    return false;
                  case 85:
                  case 86:
                  case 87:
                  case 88:
                  case 89:
                  case 90:
                  case 91:
                    break;
                }  
            } else {
              if (this.mMediaController != null) {
                mediaSessionManager = getMediaSessionManager();
                MediaController mediaController = this.mMediaController;
                MediaSession.Token token = mediaController.getSessionToken();
                mediaSessionManager.dispatchVolumeKeyEventAsSystemService(token, paramKeyEvent);
              } else {
                getMediaSessionManager().dispatchVolumeKeyEventAsSystemService(paramKeyEvent, this.mVolumeControlStreamType);
              } 
              return true;
            }  
        } else {
          if (paramInt1 < 0)
            paramInt1 = bool; 
          onKeyDownPanel(paramInt1, paramKeyEvent);
          return true;
        }  
      if (this.mMediaController != null) {
        mediaSessionManager = getMediaSessionManager();
        MediaController mediaController = this.mMediaController;
        MediaSession.Token token = mediaController.getSessionToken();
        if (mediaSessionManager.dispatchMediaKeyEventAsSystemService(token, paramKeyEvent))
          return true; 
      } 
      return false;
    } 
    if (paramKeyEvent.getRepeatCount() > 0 || paramInt1 < 0);
    if (mediaSessionManager != null)
      mediaSessionManager.startTracking(paramKeyEvent, this); 
    return true;
  }
  
  private KeyguardManager getKeyguardManager() {
    if (this.mKeyguardManager == null)
      this.mKeyguardManager = (KeyguardManager)getContext().getSystemService("keyguard"); 
    return this.mKeyguardManager;
  }
  
  AudioManager getAudioManager() {
    if (this.mAudioManager == null)
      this.mAudioManager = (AudioManager)getContext().getSystemService("audio"); 
    return this.mAudioManager;
  }
  
  private MediaSessionManager getMediaSessionManager() {
    if (this.mMediaSessionManager == null)
      this.mMediaSessionManager = (MediaSessionManager)getContext().getSystemService("media_session"); 
    return this.mMediaSessionManager;
  }
  
  protected boolean onKeyUp(int paramInt1, int paramInt2, KeyEvent paramKeyEvent) {
    DecorView decorView = this.mDecor;
    if (decorView != null) {
      KeyEvent.DispatcherState dispatcherState = decorView.getKeyDispatcherState();
    } else {
      decorView = null;
    } 
    if (decorView != null)
      decorView.handleUpEvent(paramKeyEvent); 
    boolean bool = false;
    if (paramInt2 != 4) {
      if (paramInt2 != 79)
        if (paramInt2 != 82) {
          if (paramInt2 != 130)
            if (paramInt2 != 164) {
              if (paramInt2 != 171) {
                if (paramInt2 != 24 && paramInt2 != 25) {
                  if (paramInt2 != 126 && paramInt2 != 127)
                    switch (paramInt2) {
                      default:
                        return false;
                      case 84:
                        if (!isNotInstantAppAndKeyguardRestricted())
                          if (((getContext().getResources().getConfiguration()).uiMode & 0xF) != 6) {
                            if (paramKeyEvent.isTracking() && !paramKeyEvent.isCanceled())
                              launchDefaultSearch(paramKeyEvent); 
                            return true;
                          }  
                      case 85:
                      case 86:
                      case 87:
                      case 88:
                      case 89:
                      case 90:
                      case 91:
                        break;
                    }  
                } else {
                  if (this.mMediaController != null) {
                    MediaSessionManager mediaSessionManager = getMediaSessionManager();
                    MediaController mediaController = this.mMediaController;
                    MediaSession.Token token = mediaController.getSessionToken();
                    mediaSessionManager.dispatchVolumeKeyEventAsSystemService(token, paramKeyEvent);
                  } else {
                    getMediaSessionManager().dispatchVolumeKeyEventAsSystemService(paramKeyEvent, this.mVolumeControlStreamType);
                  } 
                  return true;
                } 
              } else {
                if (this.mSupportsPictureInPicture && !paramKeyEvent.isCanceled())
                  getWindowControllerCallback().enterPictureInPictureModeIfPossible(); 
                return true;
              } 
            } else {
              getMediaSessionManager().dispatchVolumeKeyEventAsSystemService(paramKeyEvent, -2147483648);
              return true;
            }  
        } else {
          if (paramInt1 < 0)
            paramInt1 = bool; 
          onKeyUpPanel(paramInt1, paramKeyEvent);
          return true;
        }  
      if (this.mMediaController != null) {
        MediaSessionManager mediaSessionManager = getMediaSessionManager();
        MediaController mediaController = this.mMediaController;
        MediaSession.Token token = mediaController.getSessionToken();
        if (mediaSessionManager.dispatchMediaKeyEventAsSystemService(token, paramKeyEvent))
          return true; 
      } 
      return false;
    } 
    if (paramInt1 >= 0 && paramKeyEvent.isTracking() && !paramKeyEvent.isCanceled()) {
      if (paramInt1 == 0) {
        PanelFeatureState panelFeatureState = getPanelState(paramInt1, false);
        if (panelFeatureState != null && panelFeatureState.isInExpandedMode) {
          reopenMenu(true);
          return true;
        } 
      } 
      closePanel(paramInt1);
      return true;
    } 
  }
  
  private boolean isNotInstantAppAndKeyguardRestricted() {
    boolean bool;
    if (!getContext().getPackageManager().isInstantApp() && 
      getKeyguardManager().inKeyguardRestrictedInputMode()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected void onActive() {}
  
  public final View getDecorView() {
    if (this.mDecor == null || this.mForceDecorInstall)
      installDecor(); 
    return (View)this.mDecor;
  }
  
  public final View peekDecorView() {
    return (View)this.mDecor;
  }
  
  void onViewRootImplSet(ViewRootImpl paramViewRootImpl) {
    paramViewRootImpl.setActivityConfigCallback(this.mActivityConfigCallback);
    applyDecorFitsSystemWindows();
  }
  
  public Bundle saveHierarchyState() {
    Bundle bundle = new Bundle();
    if (this.mContentParent == null)
      return bundle; 
    SparseArray sparseArray1 = new SparseArray();
    this.mContentParent.saveHierarchyState(sparseArray1);
    bundle.putSparseParcelableArray("android:views", sparseArray1);
    View view = this.mContentParent.findFocus();
    if (view != null && view.getId() != -1)
      bundle.putInt("android:focusedViewId", view.getId()); 
    SparseArray<Parcelable> sparseArray = new SparseArray();
    savePanelState(sparseArray);
    if (sparseArray.size() > 0)
      bundle.putSparseParcelableArray("android:Panels", sparseArray); 
    if (this.mDecorContentParent != null) {
      sparseArray = new SparseArray();
      this.mDecorContentParent.saveToolbarHierarchyState(sparseArray);
      bundle.putSparseParcelableArray("android:ActionBar", sparseArray);
    } 
    return bundle;
  }
  
  public void restoreHierarchyState(Bundle paramBundle) {
    if (this.mContentParent == null)
      return; 
    SparseArray<Parcelable> sparseArray = paramBundle.getSparseParcelableArray("android:views");
    if (sparseArray != null)
      this.mContentParent.restoreHierarchyState(sparseArray); 
    int i = paramBundle.getInt("android:focusedViewId", -1);
    if (i != -1) {
      View view = this.mContentParent.findViewById(i);
      if (view != null) {
        view.requestFocus();
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Previously focused view reported id ");
        stringBuilder.append(i);
        stringBuilder.append(" during save, but can't be found during restore.");
        Log.w("PhoneWindow", stringBuilder.toString());
      } 
    } 
    sparseArray = paramBundle.getSparseParcelableArray("android:Panels");
    if (sparseArray != null)
      restorePanelState(sparseArray); 
    if (this.mDecorContentParent != null) {
      SparseArray<Parcelable> sparseArray1 = paramBundle.getSparseParcelableArray("android:ActionBar");
      if (sparseArray1 != null) {
        doPendingInvalidatePanelMenu();
        this.mDecorContentParent.restoreToolbarHierarchyState(sparseArray1);
      } else {
        Log.w("PhoneWindow", "Missing saved instance states for action bar views! State will not be restored.");
      } 
    } 
  }
  
  private void savePanelState(SparseArray<Parcelable> paramSparseArray) {
    PanelFeatureState[] arrayOfPanelFeatureState = this.mPanels;
    if (arrayOfPanelFeatureState == null)
      return; 
    for (int i = arrayOfPanelFeatureState.length - 1; i >= 0; i--) {
      if (arrayOfPanelFeatureState[i] != null)
        paramSparseArray.put(i, arrayOfPanelFeatureState[i].onSaveInstanceState()); 
    } 
  }
  
  private void restorePanelState(SparseArray<Parcelable> paramSparseArray) {
    for (int i = paramSparseArray.size() - 1; i >= 0; i--) {
      int j = paramSparseArray.keyAt(i);
      PanelFeatureState panelFeatureState = getPanelState(j, false);
      if (panelFeatureState != null) {
        panelFeatureState.onRestoreInstanceState((Parcelable)paramSparseArray.get(j));
        invalidatePanelMenu(j);
      } 
    } 
  }
  
  void openPanelsAfterRestore() {
    PanelFeatureState[] arrayOfPanelFeatureState = this.mPanels;
    if (arrayOfPanelFeatureState == null)
      return; 
    for (int i = arrayOfPanelFeatureState.length - 1; i >= 0; i--) {
      PanelFeatureState panelFeatureState = arrayOfPanelFeatureState[i];
      if (panelFeatureState != null) {
        panelFeatureState.applyFrozenState();
        if (!panelFeatureState.isOpen && panelFeatureState.wasLastOpen) {
          panelFeatureState.isInExpandedMode = panelFeatureState.wasLastExpanded;
          openPanel(panelFeatureState, (KeyEvent)null);
        } 
      } 
    } 
  }
  
  class PanelMenuPresenterCallback implements MenuPresenter.Callback {
    final PhoneWindow this$0;
    
    private PanelMenuPresenterCallback() {}
    
    public void onCloseMenu(MenuBuilder param1MenuBuilder, boolean param1Boolean) {
      boolean bool;
      MenuBuilder menuBuilder = param1MenuBuilder.getRootMenu();
      if (menuBuilder != param1MenuBuilder) {
        bool = true;
      } else {
        bool = false;
      } 
      PhoneWindow phoneWindow = PhoneWindow.this;
      if (bool)
        param1MenuBuilder = menuBuilder; 
      PhoneWindow.PanelFeatureState panelFeatureState = phoneWindow.findMenuPanel(param1MenuBuilder);
      if (panelFeatureState != null)
        if (bool) {
          PhoneWindow.this.callOnPanelClosed(panelFeatureState.featureId, panelFeatureState, menuBuilder);
          PhoneWindow.this.closePanel(panelFeatureState, true);
        } else {
          PhoneWindow.this.closePanel(panelFeatureState, param1Boolean);
        }  
    }
    
    public boolean onOpenSubMenu(MenuBuilder param1MenuBuilder) {
      if (param1MenuBuilder == null && PhoneWindow.this.hasFeature(8)) {
        Window.Callback callback = PhoneWindow.this.getCallback();
        if (callback != null && !PhoneWindow.this.isDestroyed())
          callback.onMenuOpened(8, param1MenuBuilder); 
      } 
      return true;
    }
  }
  
  class ActionMenuPresenterCallback implements MenuPresenter.Callback {
    final PhoneWindow this$0;
    
    private ActionMenuPresenterCallback() {}
    
    public boolean onOpenSubMenu(MenuBuilder param1MenuBuilder) {
      Window.Callback callback = PhoneWindow.this.getCallback();
      if (callback != null) {
        callback.onMenuOpened(8, param1MenuBuilder);
        return true;
      } 
      return false;
    }
    
    public void onCloseMenu(MenuBuilder param1MenuBuilder, boolean param1Boolean) {
      PhoneWindow.this.checkCloseActionMenu(param1MenuBuilder);
    }
  }
  
  protected DecorView generateDecor(int paramInt) {
    Context context;
    if (this.mUseDecorContext) {
      context = getContext().getApplicationContext();
      if (context == null) {
        context = getContext();
      } else {
        DecorContext decorContext2 = new DecorContext(context, this);
        int i = this.mTheme;
        DecorContext decorContext1 = decorContext2;
        if (i != -1) {
          decorContext2.setTheme(i);
          decorContext1 = decorContext2;
        } 
      } 
    } else {
      context = getContext();
    } 
    return new DecorView(context, paramInt, this, getAttributes());
  }
  
  protected ViewGroup generateLayout(DecorView paramDecorView) {
    boolean bool;
    TypedArray typedArray = getWindowStyle();
    this.mIsFloating = typedArray.getBoolean(4, false);
    int i = (getForcedWindowFlags() ^ 0xFFFFFFFF) & 0x10100;
    if (this.mIsFloating) {
      setLayout(-2, -2);
      setFlags(0, i);
    } else {
      setFlags(65792, i);
      getAttributes().setFitInsetsSides(0);
      getAttributes().setFitInsetsTypes(0);
    } 
    if (typedArray.getBoolean(3, false)) {
      requestFeature(1);
    } else if (typedArray.getBoolean(15, false)) {
      requestFeature(8);
    } 
    if (typedArray.getBoolean(17, false))
      requestFeature(9); 
    if (typedArray.getBoolean(16, false))
      requestFeature(10); 
    if (typedArray.getBoolean(9, false))
      setFlags(1024, (getForcedWindowFlags() ^ 0xFFFFFFFF) & 0x400); 
    if (typedArray.getBoolean(23, false)) {
      i = getForcedWindowFlags();
      setFlags(67108864, (i ^ 0xFFFFFFFF) & 0x4000000);
    } 
    if (typedArray.getBoolean(24, false)) {
      i = getForcedWindowFlags();
      setFlags(134217728, (i ^ 0xFFFFFFFF) & 0x8000000);
    } 
    if (typedArray.getBoolean(14, false))
      setFlags(1048576, (getForcedWindowFlags() ^ 0xFFFFFFFF) & 0x100000); 
    if ((getContext().getApplicationInfo()).targetSdkVersion >= 11) {
      bool = true;
    } else {
      bool = false;
    } 
    if (typedArray.getBoolean(18, bool))
      setFlags(8388608, (getForcedWindowFlags() ^ 0xFFFFFFFF) & 0x800000); 
    typedArray.getValue(19, this.mMinWidthMajor);
    typedArray.getValue(20, this.mMinWidthMinor);
    if (typedArray.hasValue(56)) {
      if (this.mFixedWidthMajor == null)
        this.mFixedWidthMajor = new TypedValue(); 
      typedArray.getValue(56, this.mFixedWidthMajor);
    } 
    if (typedArray.hasValue(57)) {
      if (this.mFixedWidthMinor == null)
        this.mFixedWidthMinor = new TypedValue(); 
      typedArray.getValue(57, this.mFixedWidthMinor);
    } 
    if (typedArray.hasValue(54)) {
      if (this.mFixedHeightMajor == null)
        this.mFixedHeightMajor = new TypedValue(); 
      typedArray.getValue(54, this.mFixedHeightMajor);
    } 
    if (typedArray.hasValue(55)) {
      if (this.mFixedHeightMinor == null)
        this.mFixedHeightMinor = new TypedValue(); 
      typedArray.getValue(55, this.mFixedHeightMinor);
    } 
    if (typedArray.getBoolean(25, false))
      requestFeature(12); 
    if (typedArray.getBoolean(44, false))
      requestFeature(13); 
    this.mIsTranslucent = typedArray.getBoolean(5, false);
    Context context = getContext();
    int j = (context.getApplicationInfo()).targetSdkVersion;
    if (j < 21) {
      i = 1;
    } else {
      i = 0;
    } 
    if (j < 29) {
      j = 1;
    } else {
      j = 0;
    } 
    if (!this.mForcedStatusBarColor)
      this.mStatusBarColor = typedArray.getColor(34, -16777216); 
    if (!this.mForcedNavigationBarColor) {
      if (this.mOpm.isClosedSuperFirewall()) {
        k = -16777216;
      } else {
        k = 1;
      } 
      int k = typedArray.getColor(35, k);
      if (k == 1) {
        this.mIsUseDefaultNavigationBarColor = true;
        this.mDecor.updateColorNavigationGuardColor(k);
        k = OplusStatusBarController.getDefaultNavigationBarColor(context);
      } 
      OplusStatusBarController oplusStatusBarController = OplusStatusBarController.getInstance();
      String str1 = context.getPackageName(), str2 = context.getClass().getName();
      this.mNavigationBarColor = oplusStatusBarController.caculateSystemBarColor(context, str1, str2, k, 1);
      this.mNavigationBarDividerColor = typedArray.getColor(49, 0);
    } 
    if (j == 0) {
      this.mEnsureStatusBarContrastWhenTransparent = typedArray.getBoolean(51, false);
      this.mEnsureNavigationBarContrastWhenTransparent = typedArray.getBoolean(52, true);
    } 
    WindowManager.LayoutParams layoutParams = getAttributes();
    if (!this.mIsFloating) {
      if (i == 0 && typedArray.getBoolean(33, false)) {
        i = getForcedWindowFlags();
        setFlags(-2147483648, Integer.MIN_VALUE & (i ^ 0xFFFFFFFF));
      } 
      if (this.mDecor.mForceWindowDrawsBarBackgrounds)
        layoutParams.privateFlags |= 0x20000; 
    } 
    if (typedArray.getBoolean(45, false)) {
      i = paramDecorView.getSystemUiVisibility();
      paramDecorView.setSystemUiVisibility(i | 0x2000);
    } 
    if (typedArray.getBoolean(48, false)) {
      i = paramDecorView.getSystemUiVisibility();
      paramDecorView.setSystemUiVisibility(0x10 | i);
    } 
    if (typedArray.hasValue(50)) {
      i = typedArray.getInt(50, -1);
      if (i >= 0 && i <= 3) {
        layoutParams.layoutInDisplayCutoutMode = i;
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown windowLayoutInDisplayCutoutMode: ");
        stringBuilder.append(typedArray.getString(50));
        throw new UnsupportedOperationException(stringBuilder.toString());
      } 
    } 
    if (this.mAlwaysReadCloseOnTouchAttr || (getContext().getApplicationInfo()).targetSdkVersion >= 11)
      if (typedArray.getBoolean(21, false))
        setCloseOnTouchOutsideIfNotSet(true);  
    if (!hasSoftInputMode())
      layoutParams.softInputMode = typedArray.getInt(13, layoutParams.softInputMode); 
    if (typedArray.getBoolean(11, this.mIsFloating)) {
      if ((getForcedWindowFlags() & 0x2) == 0)
        layoutParams.flags |= 0x2; 
      if (!haveDimAmount())
        layoutParams.dimAmount = typedArray.getFloat(0, 0.5F); 
    } 
    if (layoutParams.windowAnimations == 0)
      layoutParams.windowAnimations = typedArray.getResourceId(8, 0); 
    if (getContainer() == null) {
      if (this.mBackgroundDrawable == null) {
        if (this.mFrameResource == 0)
          this.mFrameResource = typedArray.getResourceId(2, 0); 
        if (typedArray.hasValue(1))
          this.mBackgroundDrawable = typedArray.getDrawable(1); 
      } 
      if (typedArray.hasValue(46))
        this.mBackgroundFallbackDrawable = typedArray.getDrawable(46); 
      if (this.mLoadElevation)
        this.mElevation = typedArray.getDimension(37, 0.0F); 
      this.mClipToOutline = typedArray.getBoolean(38, false);
      this.mTextColor = typedArray.getColor(7, 0);
    } 
    j = getLocalFeatures();
    if ((j & 0x18) != 0) {
      if (this.mIsFloating) {
        TypedValue typedValue = new TypedValue();
        getContext().getTheme().resolveAttribute(17956911, typedValue, true);
        i = typedValue.resourceId;
      } else {
        i = 17367287;
      } 
      removeFeature(8);
    } else if ((j & 0x24) != 0 && (j & 0x100) == 0) {
      i = 17367283;
    } else if ((j & 0x80) != 0) {
      if (this.mIsFloating) {
        TypedValue typedValue = new TypedValue();
        getContext().getTheme().resolveAttribute(17956908, typedValue, true);
        i = typedValue.resourceId;
      } else {
        i = 17367282;
      } 
      removeFeature(8);
    } else if ((j & 0x2) == 0) {
      if (this.mIsFloating) {
        TypedValue typedValue = new TypedValue();
        getContext().getTheme().resolveAttribute(17956910, typedValue, true);
        i = typedValue.resourceId;
      } else if ((j & 0x100) != 0) {
        i = typedArray.getResourceId(53, 17367281);
      } else {
        i = 17367286;
      } 
    } else if ((j & 0x400) != 0) {
      i = 17367285;
    } else {
      i = 17367284;
    } 
    this.mDecor.startChanging();
    this.mDecor.onResourcesLoaded(this.mLayoutInflater, i);
    ViewGroup viewGroup = (ViewGroup)findViewById(16908290);
    if (viewGroup != null) {
      if ((j & 0x20) != 0) {
        ProgressBar progressBar = getCircularProgressBar(false);
        if (progressBar != null)
          progressBar.setIndeterminate(true); 
      } 
      if (getContainer() == null) {
        this.mDecor.setWindowBackground(this.mBackgroundDrawable);
        if (this.mFrameResource != 0) {
          Drawable drawable = getContext().getDrawable(this.mFrameResource);
        } else {
          paramDecorView = null;
        } 
        this.mDecor.setWindowFrame((Drawable)paramDecorView);
        this.mDecor.setElevation(this.mElevation);
        this.mDecor.setClipToOutline(this.mClipToOutline);
        CharSequence charSequence = this.mTitle;
        if (charSequence != null)
          setTitle(charSequence); 
        if (this.mTitleColor == 0)
          this.mTitleColor = this.mTextColor; 
        setTitleColor(this.mTitleColor);
      } 
      this.mDecor.finishChanging();
      return viewGroup;
    } 
    throw new RuntimeException("Window couldn't find content container view");
  }
  
  public void alwaysReadCloseOnTouchAttr() {
    this.mAlwaysReadCloseOnTouchAttr = true;
  }
  
  private void installDecor() {
    // Byte code:
    //   0: aload_0
    //   1: iconst_0
    //   2: putfield mForceDecorInstall : Z
    //   5: aload_0
    //   6: getfield mDecor : Lcom/android/internal/policy/DecorView;
    //   9: astore_1
    //   10: aload_1
    //   11: ifnonnull -> 68
    //   14: aload_0
    //   15: iconst_m1
    //   16: invokevirtual generateDecor : (I)Lcom/android/internal/policy/DecorView;
    //   19: astore_1
    //   20: aload_0
    //   21: aload_1
    //   22: putfield mDecor : Lcom/android/internal/policy/DecorView;
    //   25: aload_1
    //   26: ldc_w 262144
    //   29: invokevirtual setDescendantFocusability : (I)V
    //   32: aload_0
    //   33: getfield mDecor : Lcom/android/internal/policy/DecorView;
    //   36: iconst_1
    //   37: invokevirtual setIsRootNamespace : (Z)V
    //   40: aload_0
    //   41: getfield mInvalidatePanelMenuPosted : Z
    //   44: ifne -> 73
    //   47: aload_0
    //   48: getfield mInvalidatePanelMenuFeatures : I
    //   51: ifeq -> 73
    //   54: aload_0
    //   55: getfield mDecor : Lcom/android/internal/policy/DecorView;
    //   58: aload_0
    //   59: getfield mInvalidatePanelMenuRunnable : Ljava/lang/Runnable;
    //   62: invokevirtual postOnAnimation : (Ljava/lang/Runnable;)V
    //   65: goto -> 73
    //   68: aload_1
    //   69: aload_0
    //   70: invokevirtual setWindow : (Lcom/android/internal/policy/PhoneWindow;)V
    //   73: aload_0
    //   74: getfield mContentParent : Landroid/view/ViewGroup;
    //   77: ifnonnull -> 805
    //   80: aload_0
    //   81: aload_0
    //   82: aload_0
    //   83: getfield mDecor : Lcom/android/internal/policy/DecorView;
    //   86: invokevirtual generateLayout : (Lcom/android/internal/policy/DecorView;)Landroid/view/ViewGroup;
    //   89: putfield mContentParent : Landroid/view/ViewGroup;
    //   92: aload_0
    //   93: getfield mDecor : Lcom/android/internal/policy/DecorView;
    //   96: invokevirtual makeFrameworkOptionalFitsSystemWindows : ()V
    //   99: aload_0
    //   100: getfield mDecor : Lcom/android/internal/policy/DecorView;
    //   103: ldc_w 16908917
    //   106: invokevirtual findViewById : (I)Landroid/view/View;
    //   109: checkcast com/android/internal/widget/DecorContentParent
    //   112: astore_1
    //   113: aload_1
    //   114: ifnull -> 405
    //   117: aload_0
    //   118: aload_1
    //   119: putfield mDecorContentParent : Lcom/android/internal/widget/DecorContentParent;
    //   122: aload_1
    //   123: aload_0
    //   124: invokevirtual getCallback : ()Landroid/view/Window$Callback;
    //   127: invokeinterface setWindowCallback : (Landroid/view/Window$Callback;)V
    //   132: aload_0
    //   133: getfield mDecorContentParent : Lcom/android/internal/widget/DecorContentParent;
    //   136: invokeinterface getTitle : ()Ljava/lang/CharSequence;
    //   141: ifnonnull -> 157
    //   144: aload_0
    //   145: getfield mDecorContentParent : Lcom/android/internal/widget/DecorContentParent;
    //   148: aload_0
    //   149: getfield mTitle : Ljava/lang/CharSequence;
    //   152: invokeinterface setWindowTitle : (Ljava/lang/CharSequence;)V
    //   157: aload_0
    //   158: invokevirtual getLocalFeatures : ()I
    //   161: istore_2
    //   162: iconst_0
    //   163: istore_3
    //   164: iload_3
    //   165: bipush #13
    //   167: if_icmpge -> 194
    //   170: iconst_1
    //   171: iload_3
    //   172: ishl
    //   173: iload_2
    //   174: iand
    //   175: ifeq -> 188
    //   178: aload_0
    //   179: getfield mDecorContentParent : Lcom/android/internal/widget/DecorContentParent;
    //   182: iload_3
    //   183: invokeinterface initFeature : (I)V
    //   188: iinc #3, 1
    //   191: goto -> 164
    //   194: aload_0
    //   195: getfield mDecorContentParent : Lcom/android/internal/widget/DecorContentParent;
    //   198: aload_0
    //   199: getfield mUiOptions : I
    //   202: invokeinterface setUiOptions : (I)V
    //   207: aload_0
    //   208: getfield mResourcesSetFlags : I
    //   211: iconst_1
    //   212: iand
    //   213: ifne -> 308
    //   216: aload_0
    //   217: getfield mIconRes : I
    //   220: ifeq -> 240
    //   223: aload_0
    //   224: getfield mDecorContentParent : Lcom/android/internal/widget/DecorContentParent;
    //   227: astore_1
    //   228: aload_1
    //   229: invokeinterface hasIcon : ()Z
    //   234: ifne -> 240
    //   237: goto -> 308
    //   240: aload_0
    //   241: getfield mResourcesSetFlags : I
    //   244: iconst_1
    //   245: iand
    //   246: ifne -> 321
    //   249: aload_0
    //   250: getfield mIconRes : I
    //   253: ifne -> 321
    //   256: aload_0
    //   257: getfield mDecorContentParent : Lcom/android/internal/widget/DecorContentParent;
    //   260: astore_1
    //   261: aload_1
    //   262: invokeinterface hasIcon : ()Z
    //   267: ifne -> 321
    //   270: aload_0
    //   271: getfield mDecorContentParent : Lcom/android/internal/widget/DecorContentParent;
    //   274: astore_1
    //   275: aload_0
    //   276: invokevirtual getContext : ()Landroid/content/Context;
    //   279: invokevirtual getPackageManager : ()Landroid/content/pm/PackageManager;
    //   282: invokevirtual getDefaultActivityIcon : ()Landroid/graphics/drawable/Drawable;
    //   285: astore #4
    //   287: aload_1
    //   288: aload #4
    //   290: invokeinterface setIcon : (Landroid/graphics/drawable/Drawable;)V
    //   295: aload_0
    //   296: aload_0
    //   297: getfield mResourcesSetFlags : I
    //   300: iconst_4
    //   301: ior
    //   302: putfield mResourcesSetFlags : I
    //   305: goto -> 321
    //   308: aload_0
    //   309: getfield mDecorContentParent : Lcom/android/internal/widget/DecorContentParent;
    //   312: aload_0
    //   313: getfield mIconRes : I
    //   316: invokeinterface setIcon : (I)V
    //   321: aload_0
    //   322: getfield mResourcesSetFlags : I
    //   325: iconst_2
    //   326: iand
    //   327: ifne -> 351
    //   330: aload_0
    //   331: getfield mLogoRes : I
    //   334: ifeq -> 364
    //   337: aload_0
    //   338: getfield mDecorContentParent : Lcom/android/internal/widget/DecorContentParent;
    //   341: astore_1
    //   342: aload_1
    //   343: invokeinterface hasLogo : ()Z
    //   348: ifne -> 364
    //   351: aload_0
    //   352: getfield mDecorContentParent : Lcom/android/internal/widget/DecorContentParent;
    //   355: aload_0
    //   356: getfield mLogoRes : I
    //   359: invokeinterface setLogo : (I)V
    //   364: aload_0
    //   365: iconst_0
    //   366: iconst_0
    //   367: invokevirtual getPanelState : (IZ)Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;
    //   370: astore_1
    //   371: aload_0
    //   372: invokevirtual isDestroyed : ()Z
    //   375: ifne -> 402
    //   378: aload_1
    //   379: ifnull -> 389
    //   382: aload_1
    //   383: getfield menu : Lcom/android/internal/view/menu/MenuBuilder;
    //   386: ifnonnull -> 402
    //   389: aload_0
    //   390: getfield mIsStartingWindow : Z
    //   393: ifne -> 402
    //   396: aload_0
    //   397: bipush #8
    //   399: invokevirtual invalidatePanelMenu : (I)V
    //   402: goto -> 486
    //   405: aload_0
    //   406: ldc_w 16908310
    //   409: invokevirtual findViewById : (I)Landroid/view/View;
    //   412: checkcast android/widget/TextView
    //   415: astore_1
    //   416: aload_0
    //   417: aload_1
    //   418: putfield mTitleView : Landroid/widget/TextView;
    //   421: aload_1
    //   422: ifnull -> 486
    //   425: aload_0
    //   426: invokevirtual getLocalFeatures : ()I
    //   429: iconst_2
    //   430: iand
    //   431: ifeq -> 475
    //   434: aload_0
    //   435: ldc_w 16909546
    //   438: invokevirtual findViewById : (I)Landroid/view/View;
    //   441: astore_1
    //   442: aload_1
    //   443: ifnull -> 455
    //   446: aload_1
    //   447: bipush #8
    //   449: invokevirtual setVisibility : (I)V
    //   452: goto -> 464
    //   455: aload_0
    //   456: getfield mTitleView : Landroid/widget/TextView;
    //   459: bipush #8
    //   461: invokevirtual setVisibility : (I)V
    //   464: aload_0
    //   465: getfield mContentParent : Landroid/view/ViewGroup;
    //   468: aconst_null
    //   469: invokevirtual setForeground : (Landroid/graphics/drawable/Drawable;)V
    //   472: goto -> 486
    //   475: aload_0
    //   476: getfield mTitleView : Landroid/widget/TextView;
    //   479: aload_0
    //   480: getfield mTitle : Ljava/lang/CharSequence;
    //   483: invokevirtual setText : (Ljava/lang/CharSequence;)V
    //   486: aload_0
    //   487: getfield mDecor : Lcom/android/internal/policy/DecorView;
    //   490: invokevirtual getBackground : ()Landroid/graphics/drawable/Drawable;
    //   493: ifnonnull -> 513
    //   496: aload_0
    //   497: getfield mBackgroundFallbackDrawable : Landroid/graphics/drawable/Drawable;
    //   500: astore_1
    //   501: aload_1
    //   502: ifnull -> 513
    //   505: aload_0
    //   506: getfield mDecor : Lcom/android/internal/policy/DecorView;
    //   509: aload_1
    //   510: invokevirtual setBackgroundFallback : (Landroid/graphics/drawable/Drawable;)V
    //   513: aload_0
    //   514: bipush #13
    //   516: invokevirtual hasFeature : (I)Z
    //   519: ifeq -> 805
    //   522: aload_0
    //   523: getfield mTransitionManager : Landroid/transition/TransitionManager;
    //   526: ifnonnull -> 579
    //   529: aload_0
    //   530: invokevirtual getWindowStyle : ()Landroid/content/res/TypedArray;
    //   533: bipush #26
    //   535: iconst_0
    //   536: invokevirtual getResourceId : (II)I
    //   539: istore_3
    //   540: iload_3
    //   541: ifeq -> 568
    //   544: aload_0
    //   545: invokevirtual getContext : ()Landroid/content/Context;
    //   548: invokestatic from : (Landroid/content/Context;)Landroid/transition/TransitionInflater;
    //   551: astore_1
    //   552: aload_0
    //   553: aload_1
    //   554: iload_3
    //   555: aload_0
    //   556: getfield mContentParent : Landroid/view/ViewGroup;
    //   559: invokevirtual inflateTransitionManager : (ILandroid/view/ViewGroup;)Landroid/transition/TransitionManager;
    //   562: putfield mTransitionManager : Landroid/transition/TransitionManager;
    //   565: goto -> 579
    //   568: aload_0
    //   569: new android/transition/TransitionManager
    //   572: dup
    //   573: invokespecial <init> : ()V
    //   576: putfield mTransitionManager : Landroid/transition/TransitionManager;
    //   579: aload_0
    //   580: aload_0
    //   581: aload_0
    //   582: getfield mEnterTransition : Landroid/transition/Transition;
    //   585: aconst_null
    //   586: bipush #27
    //   588: invokespecial getTransition : (Landroid/transition/Transition;Landroid/transition/Transition;I)Landroid/transition/Transition;
    //   591: putfield mEnterTransition : Landroid/transition/Transition;
    //   594: aload_0
    //   595: aload_0
    //   596: aload_0
    //   597: getfield mReturnTransition : Landroid/transition/Transition;
    //   600: getstatic com/android/internal/policy/PhoneWindow.USE_DEFAULT_TRANSITION : Landroid/transition/Transition;
    //   603: bipush #39
    //   605: invokespecial getTransition : (Landroid/transition/Transition;Landroid/transition/Transition;I)Landroid/transition/Transition;
    //   608: putfield mReturnTransition : Landroid/transition/Transition;
    //   611: aload_0
    //   612: aload_0
    //   613: aload_0
    //   614: getfield mExitTransition : Landroid/transition/Transition;
    //   617: aconst_null
    //   618: bipush #28
    //   620: invokespecial getTransition : (Landroid/transition/Transition;Landroid/transition/Transition;I)Landroid/transition/Transition;
    //   623: putfield mExitTransition : Landroid/transition/Transition;
    //   626: aload_0
    //   627: aload_0
    //   628: aload_0
    //   629: getfield mReenterTransition : Landroid/transition/Transition;
    //   632: getstatic com/android/internal/policy/PhoneWindow.USE_DEFAULT_TRANSITION : Landroid/transition/Transition;
    //   635: bipush #40
    //   637: invokespecial getTransition : (Landroid/transition/Transition;Landroid/transition/Transition;I)Landroid/transition/Transition;
    //   640: putfield mReenterTransition : Landroid/transition/Transition;
    //   643: aload_0
    //   644: aload_0
    //   645: aload_0
    //   646: getfield mSharedElementEnterTransition : Landroid/transition/Transition;
    //   649: aconst_null
    //   650: bipush #29
    //   652: invokespecial getTransition : (Landroid/transition/Transition;Landroid/transition/Transition;I)Landroid/transition/Transition;
    //   655: putfield mSharedElementEnterTransition : Landroid/transition/Transition;
    //   658: aload_0
    //   659: aload_0
    //   660: aload_0
    //   661: getfield mSharedElementReturnTransition : Landroid/transition/Transition;
    //   664: getstatic com/android/internal/policy/PhoneWindow.USE_DEFAULT_TRANSITION : Landroid/transition/Transition;
    //   667: bipush #41
    //   669: invokespecial getTransition : (Landroid/transition/Transition;Landroid/transition/Transition;I)Landroid/transition/Transition;
    //   672: putfield mSharedElementReturnTransition : Landroid/transition/Transition;
    //   675: aload_0
    //   676: aload_0
    //   677: aload_0
    //   678: getfield mSharedElementExitTransition : Landroid/transition/Transition;
    //   681: aconst_null
    //   682: bipush #30
    //   684: invokespecial getTransition : (Landroid/transition/Transition;Landroid/transition/Transition;I)Landroid/transition/Transition;
    //   687: putfield mSharedElementExitTransition : Landroid/transition/Transition;
    //   690: aload_0
    //   691: aload_0
    //   692: aload_0
    //   693: getfield mSharedElementReenterTransition : Landroid/transition/Transition;
    //   696: getstatic com/android/internal/policy/PhoneWindow.USE_DEFAULT_TRANSITION : Landroid/transition/Transition;
    //   699: bipush #42
    //   701: invokespecial getTransition : (Landroid/transition/Transition;Landroid/transition/Transition;I)Landroid/transition/Transition;
    //   704: putfield mSharedElementReenterTransition : Landroid/transition/Transition;
    //   707: aload_0
    //   708: getfield mAllowEnterTransitionOverlap : Ljava/lang/Boolean;
    //   711: ifnonnull -> 731
    //   714: aload_0
    //   715: aload_0
    //   716: invokevirtual getWindowStyle : ()Landroid/content/res/TypedArray;
    //   719: bipush #32
    //   721: iconst_1
    //   722: invokevirtual getBoolean : (IZ)Z
    //   725: invokestatic valueOf : (Z)Ljava/lang/Boolean;
    //   728: putfield mAllowEnterTransitionOverlap : Ljava/lang/Boolean;
    //   731: aload_0
    //   732: getfield mAllowReturnTransitionOverlap : Ljava/lang/Boolean;
    //   735: ifnonnull -> 755
    //   738: aload_0
    //   739: aload_0
    //   740: invokevirtual getWindowStyle : ()Landroid/content/res/TypedArray;
    //   743: bipush #31
    //   745: iconst_1
    //   746: invokevirtual getBoolean : (IZ)Z
    //   749: invokestatic valueOf : (Z)Ljava/lang/Boolean;
    //   752: putfield mAllowReturnTransitionOverlap : Ljava/lang/Boolean;
    //   755: aload_0
    //   756: getfield mBackgroundFadeDurationMillis : J
    //   759: lconst_0
    //   760: lcmp
    //   761: ifge -> 781
    //   764: aload_0
    //   765: aload_0
    //   766: invokevirtual getWindowStyle : ()Landroid/content/res/TypedArray;
    //   769: bipush #36
    //   771: sipush #300
    //   774: invokevirtual getInteger : (II)I
    //   777: i2l
    //   778: putfield mBackgroundFadeDurationMillis : J
    //   781: aload_0
    //   782: getfield mSharedElementsUseOverlay : Ljava/lang/Boolean;
    //   785: ifnonnull -> 805
    //   788: aload_0
    //   789: aload_0
    //   790: invokevirtual getWindowStyle : ()Landroid/content/res/TypedArray;
    //   793: bipush #43
    //   795: iconst_1
    //   796: invokevirtual getBoolean : (IZ)Z
    //   799: invokestatic valueOf : (Z)Ljava/lang/Boolean;
    //   802: putfield mSharedElementsUseOverlay : Ljava/lang/Boolean;
    //   805: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2712	-> 0
    //   #2713	-> 5
    //   #2714	-> 14
    //   #2715	-> 25
    //   #2716	-> 32
    //   #2717	-> 40
    //   #2718	-> 54
    //   #2721	-> 68
    //   #2723	-> 73
    //   #2724	-> 80
    //   #2727	-> 92
    //   #2729	-> 99
    //   #2732	-> 113
    //   #2733	-> 117
    //   #2734	-> 122
    //   #2735	-> 132
    //   #2736	-> 144
    //   #2739	-> 157
    //   #2740	-> 162
    //   #2741	-> 170
    //   #2742	-> 178
    //   #2740	-> 188
    //   #2746	-> 194
    //   #2748	-> 207
    //   #2749	-> 228
    //   #2751	-> 240
    //   #2752	-> 261
    //   #2753	-> 270
    //   #2754	-> 275
    //   #2753	-> 287
    //   #2755	-> 295
    //   #2750	-> 308
    //   #2757	-> 321
    //   #2758	-> 342
    //   #2759	-> 351
    //   #2767	-> 364
    //   #2768	-> 371
    //   #2769	-> 396
    //   #2771	-> 402
    //   #2772	-> 405
    //   #2773	-> 421
    //   #2774	-> 425
    //   #2775	-> 434
    //   #2776	-> 442
    //   #2777	-> 446
    //   #2779	-> 455
    //   #2781	-> 464
    //   #2782	-> 472
    //   #2783	-> 475
    //   #2788	-> 486
    //   #2789	-> 505
    //   #2794	-> 513
    //   #2795	-> 522
    //   #2796	-> 529
    //   #2799	-> 540
    //   #2800	-> 544
    //   #2801	-> 552
    //   #2803	-> 565
    //   #2804	-> 568
    //   #2808	-> 579
    //   #2810	-> 594
    //   #2812	-> 611
    //   #2814	-> 626
    //   #2816	-> 643
    //   #2818	-> 658
    //   #2821	-> 675
    //   #2823	-> 690
    //   #2826	-> 707
    //   #2827	-> 714
    //   #2830	-> 731
    //   #2831	-> 738
    //   #2834	-> 755
    //   #2835	-> 764
    //   #2839	-> 781
    //   #2840	-> 788
    //   #2845	-> 805
  }
  
  private Transition getTransition(Transition paramTransition1, Transition paramTransition2, int paramInt) {
    Transition transition;
    if (paramTransition1 != paramTransition2)
      return paramTransition1; 
    paramInt = getWindowStyle().getResourceId(paramInt, -1);
    paramTransition1 = paramTransition2;
    if (paramInt != -1) {
      paramTransition1 = paramTransition2;
      if (paramInt != 17760256) {
        TransitionInflater transitionInflater = TransitionInflater.from(getContext());
        paramTransition2 = transitionInflater.inflateTransition(paramInt);
        transition = paramTransition2;
        if (paramTransition2 instanceof TransitionSet) {
          TransitionSet transitionSet = (TransitionSet)paramTransition2;
          transition = paramTransition2;
          if (transitionSet.getTransitionCount() == 0)
            transition = null; 
        } 
      } 
    } 
    return transition;
  }
  
  private Drawable loadImageURI(Uri paramUri) {
    try {
      InputStream inputStream = getContext().getContentResolver().openInputStream(paramUri);
      return Drawable.createFromStream(inputStream, null);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to open content: ");
      stringBuilder.append(paramUri);
      Log.w("PhoneWindow", stringBuilder.toString());
      return null;
    } 
  }
  
  private DrawableFeatureState getDrawableState(int paramInt, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual getFeatures : ()I
    //   4: iconst_1
    //   5: iload_1
    //   6: ishl
    //   7: iand
    //   8: ifne -> 28
    //   11: iload_2
    //   12: ifne -> 17
    //   15: aconst_null
    //   16: areturn
    //   17: new java/lang/RuntimeException
    //   20: dup
    //   21: ldc_w 'The feature has not been requested'
    //   24: invokespecial <init> : (Ljava/lang/String;)V
    //   27: athrow
    //   28: aload_0
    //   29: getfield mDrawables : [Lcom/android/internal/policy/PhoneWindow$DrawableFeatureState;
    //   32: astore_3
    //   33: aload_3
    //   34: astore #4
    //   36: aload_3
    //   37: ifnull -> 50
    //   40: aload #4
    //   42: astore_3
    //   43: aload #4
    //   45: arraylength
    //   46: iload_1
    //   47: if_icmpgt -> 84
    //   50: iload_1
    //   51: iconst_1
    //   52: iadd
    //   53: anewarray com/android/internal/policy/PhoneWindow$DrawableFeatureState
    //   56: astore #5
    //   58: aload #4
    //   60: ifnull -> 75
    //   63: aload #4
    //   65: iconst_0
    //   66: aload #5
    //   68: iconst_0
    //   69: aload #4
    //   71: arraylength
    //   72: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   75: aload #5
    //   77: astore_3
    //   78: aload_0
    //   79: aload #5
    //   81: putfield mDrawables : [Lcom/android/internal/policy/PhoneWindow$DrawableFeatureState;
    //   84: aload_3
    //   85: iload_1
    //   86: aaload
    //   87: astore #5
    //   89: aload #5
    //   91: astore #4
    //   93: aload #5
    //   95: ifnonnull -> 117
    //   98: new com/android/internal/policy/PhoneWindow$DrawableFeatureState
    //   101: dup
    //   102: iload_1
    //   103: invokespecial <init> : (I)V
    //   106: astore #5
    //   108: aload #5
    //   110: astore #4
    //   112: aload_3
    //   113: iload_1
    //   114: aload #5
    //   116: aastore
    //   117: aload #4
    //   119: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2875	-> 0
    //   #2876	-> 11
    //   #2877	-> 15
    //   #2879	-> 17
    //   #2883	-> 28
    //   #2884	-> 50
    //   #2885	-> 58
    //   #2886	-> 63
    //   #2888	-> 75
    //   #2891	-> 84
    //   #2892	-> 89
    //   #2893	-> 98
    //   #2895	-> 117
  }
  
  PanelFeatureState getPanelState(int paramInt, boolean paramBoolean) {
    return getPanelState(paramInt, paramBoolean, (PanelFeatureState)null);
  }
  
  private PanelFeatureState getPanelState(int paramInt, boolean paramBoolean, PanelFeatureState paramPanelFeatureState) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual getFeatures : ()I
    //   4: iconst_1
    //   5: iload_1
    //   6: ishl
    //   7: iand
    //   8: ifne -> 28
    //   11: iload_2
    //   12: ifne -> 17
    //   15: aconst_null
    //   16: areturn
    //   17: new java/lang/RuntimeException
    //   20: dup
    //   21: ldc_w 'The feature has not been requested'
    //   24: invokespecial <init> : (Ljava/lang/String;)V
    //   27: athrow
    //   28: aload_0
    //   29: getfield mPanels : [Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;
    //   32: astore #4
    //   34: aload #4
    //   36: astore #5
    //   38: aload #4
    //   40: ifnull -> 54
    //   43: aload #5
    //   45: astore #4
    //   47: aload #5
    //   49: arraylength
    //   50: iload_1
    //   51: if_icmpgt -> 89
    //   54: iload_1
    //   55: iconst_1
    //   56: iadd
    //   57: anewarray com/android/internal/policy/PhoneWindow$PanelFeatureState
    //   60: astore #6
    //   62: aload #5
    //   64: ifnull -> 79
    //   67: aload #5
    //   69: iconst_0
    //   70: aload #6
    //   72: iconst_0
    //   73: aload #5
    //   75: arraylength
    //   76: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   79: aload #6
    //   81: astore #4
    //   83: aload_0
    //   84: aload #6
    //   86: putfield mPanels : [Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;
    //   89: aload #4
    //   91: iload_1
    //   92: aaload
    //   93: astore #6
    //   95: aload #6
    //   97: astore #5
    //   99: aload #6
    //   101: ifnonnull -> 128
    //   104: aload_3
    //   105: ifnull -> 111
    //   108: goto -> 120
    //   111: new com/android/internal/policy/PhoneWindow$PanelFeatureState
    //   114: dup
    //   115: iload_1
    //   116: invokespecial <init> : (I)V
    //   119: astore_3
    //   120: aload_3
    //   121: astore #5
    //   123: aload #4
    //   125: iload_1
    //   126: aload_3
    //   127: aastore
    //   128: aload #5
    //   130: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2922	-> 0
    //   #2923	-> 11
    //   #2924	-> 15
    //   #2926	-> 17
    //   #2930	-> 28
    //   #2931	-> 54
    //   #2932	-> 62
    //   #2933	-> 67
    //   #2935	-> 79
    //   #2938	-> 89
    //   #2939	-> 95
    //   #2940	-> 104
    //   #2941	-> 108
    //   #2942	-> 111
    //   #2944	-> 128
  }
  
  public final void setChildDrawable(int paramInt, Drawable paramDrawable) {
    DrawableFeatureState drawableFeatureState = getDrawableState(paramInt, true);
    drawableFeatureState.child = paramDrawable;
    updateDrawable(paramInt, drawableFeatureState, false);
  }
  
  public final void setChildInt(int paramInt1, int paramInt2) {
    updateInt(paramInt1, paramInt2, false);
  }
  
  public boolean isShortcutKey(int paramInt, KeyEvent paramKeyEvent) {
    boolean bool1 = false;
    PanelFeatureState panelFeatureState = getPanelState(0, false);
    boolean bool2 = bool1;
    if (panelFeatureState != null) {
      bool2 = bool1;
      if (panelFeatureState.menu != null) {
        bool2 = bool1;
        if (panelFeatureState.menu.isShortcutKey(paramInt, paramKeyEvent))
          bool2 = true; 
      } 
    } 
    return bool2;
  }
  
  private void updateDrawable(int paramInt, DrawableFeatureState paramDrawableFeatureState, boolean paramBoolean) {
    if (this.mContentParent == null)
      return; 
    int i = 1 << paramInt;
    if ((getFeatures() & i) == 0 && !paramBoolean)
      return; 
    Drawable drawable = null;
    if (paramDrawableFeatureState != null) {
      drawable = paramDrawableFeatureState.child;
      Drawable drawable1 = drawable;
      if (drawable == null)
        drawable1 = paramDrawableFeatureState.local; 
      drawable = drawable1;
      if (drawable1 == null)
        drawable = paramDrawableFeatureState.def; 
    } 
    if ((getLocalFeatures() & i) == 0) {
      if (getContainer() != null && (
        isActive() || paramBoolean))
        getContainer().setChildDrawable(paramInt, drawable); 
    } else if (paramDrawableFeatureState != null && (paramDrawableFeatureState.cur != drawable || paramDrawableFeatureState.curAlpha != paramDrawableFeatureState.alpha)) {
      paramDrawableFeatureState.cur = drawable;
      paramDrawableFeatureState.curAlpha = paramDrawableFeatureState.alpha;
      onDrawableChanged(paramInt, drawable, paramDrawableFeatureState.alpha);
    } 
  }
  
  private void updateInt(int paramInt1, int paramInt2, boolean paramBoolean) {
    if (this.mContentParent == null)
      return; 
    int i = 1 << paramInt1;
    if ((getFeatures() & i) == 0 && !paramBoolean)
      return; 
    if ((getLocalFeatures() & i) == 0) {
      if (getContainer() != null)
        getContainer().setChildInt(paramInt1, paramInt2); 
    } else {
      onIntChanged(paramInt1, paramInt2);
    } 
  }
  
  private ImageView getLeftIconView() {
    ImageView imageView = this.mLeftIconView;
    if (imageView != null)
      return imageView; 
    if (this.mContentParent == null)
      installDecor(); 
    this.mLeftIconView = imageView = (ImageView)findViewById(16909123);
    return imageView;
  }
  
  protected void dispatchWindowAttributesChanged(WindowManager.LayoutParams paramLayoutParams) {
    super.dispatchWindowAttributesChanged(paramLayoutParams);
    DecorView decorView = this.mDecor;
    if (decorView != null)
      decorView.updateColorViews((WindowInsets)null, true); 
  }
  
  private ProgressBar getCircularProgressBar(boolean paramBoolean) {
    ProgressBar progressBar = this.mCircularProgressBar;
    if (progressBar != null)
      return progressBar; 
    if (this.mContentParent == null && paramBoolean)
      installDecor(); 
    this.mCircularProgressBar = progressBar = (ProgressBar)findViewById(16909325);
    if (progressBar != null)
      progressBar.setVisibility(4); 
    return this.mCircularProgressBar;
  }
  
  private ProgressBar getHorizontalProgressBar(boolean paramBoolean) {
    ProgressBar progressBar = this.mHorizontalProgressBar;
    if (progressBar != null)
      return progressBar; 
    if (this.mContentParent == null && paramBoolean)
      installDecor(); 
    this.mHorizontalProgressBar = progressBar = (ProgressBar)findViewById(16909326);
    if (progressBar != null)
      progressBar.setVisibility(4); 
    return this.mHorizontalProgressBar;
  }
  
  private ImageView getRightIconView() {
    ImageView imageView = this.mRightIconView;
    if (imageView != null)
      return imageView; 
    if (this.mContentParent == null)
      installDecor(); 
    this.mRightIconView = imageView = (ImageView)findViewById(16909369);
    return imageView;
  }
  
  private void callOnPanelClosed(int paramInt, PanelFeatureState paramPanelFeatureState, Menu paramMenu) {
    Window.Callback callback = getCallback();
    if (callback == null)
      return; 
    PanelFeatureState panelFeatureState = paramPanelFeatureState;
    Menu menu = paramMenu;
    if (paramMenu == null) {
      PanelFeatureState panelFeatureState1 = paramPanelFeatureState;
      if (paramPanelFeatureState == null) {
        panelFeatureState1 = paramPanelFeatureState;
        if (paramInt >= 0) {
          PanelFeatureState[] arrayOfPanelFeatureState = this.mPanels;
          panelFeatureState1 = paramPanelFeatureState;
          if (paramInt < arrayOfPanelFeatureState.length)
            panelFeatureState1 = arrayOfPanelFeatureState[paramInt]; 
        } 
      } 
      panelFeatureState = panelFeatureState1;
      menu = paramMenu;
      if (panelFeatureState1 != null) {
        menu = panelFeatureState1.menu;
        panelFeatureState = panelFeatureState1;
      } 
    } 
    if (panelFeatureState != null && !panelFeatureState.isOpen)
      return; 
    if (!isDestroyed())
      callback.onPanelClosed(paramInt, menu); 
  }
  
  private boolean isTvUserSetupComplete() {
    boolean bool2;
    ContentResolver contentResolver = getContext().getContentResolver();
    boolean bool1 = false;
    if (Settings.Secure.getInt(contentResolver, "user_setup_complete", 0) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if (Settings.Secure.getInt(getContext().getContentResolver(), "tv_user_setup_complete", 0) != 0)
      bool1 = true; 
    return bool2 & bool1;
  }
  
  private boolean launchDefaultSearch(KeyEvent paramKeyEvent) {
    boolean bool;
    if (getContext().getPackageManager().hasSystemFeature("android.software.leanback") && 
      !isTvUserSetupComplete())
      return false; 
    Window.Callback callback = getCallback();
    if (callback == null || isDestroyed()) {
      bool = false;
    } else {
      sendCloseSystemWindows("search");
      int i = paramKeyEvent.getDeviceId();
      SearchEvent searchEvent = null;
      if (i != 0)
        searchEvent = new SearchEvent(InputDevice.getDevice(i)); 
      try {
        bool = callback.onSearchRequested(searchEvent);
      } catch (AbstractMethodError abstractMethodError) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("WindowCallback ");
        stringBuilder.append(callback.getClass().getName());
        stringBuilder.append(" does not implement method onSearchRequested(SearchEvent); fa");
        Log.e("PhoneWindow", stringBuilder.toString(), abstractMethodError);
        bool = callback.onSearchRequested();
      } 
    } 
    if (!bool && ((getContext().getResources().getConfiguration()).uiMode & 0xF) == 4) {
      Bundle bundle = new Bundle();
      bundle.putInt("android.intent.extra.ASSIST_INPUT_DEVICE_ID", paramKeyEvent.getDeviceId());
      SearchManager searchManager = (SearchManager)getContext().getSystemService("search");
      searchManager.launchAssist(bundle);
      return true;
    } 
    return bool;
  }
  
  public void setVolumeControlStream(int paramInt) {
    this.mVolumeControlStreamType = paramInt;
  }
  
  public int getVolumeControlStream() {
    return this.mVolumeControlStreamType;
  }
  
  public void setMediaController(MediaController paramMediaController) {
    this.mMediaController = paramMediaController;
  }
  
  public MediaController getMediaController() {
    return this.mMediaController;
  }
  
  public void setEnterTransition(Transition paramTransition) {
    this.mEnterTransition = paramTransition;
  }
  
  public void setReturnTransition(Transition paramTransition) {
    this.mReturnTransition = paramTransition;
  }
  
  public void setExitTransition(Transition paramTransition) {
    this.mExitTransition = paramTransition;
  }
  
  public void setReenterTransition(Transition paramTransition) {
    this.mReenterTransition = paramTransition;
  }
  
  public void setSharedElementEnterTransition(Transition paramTransition) {
    this.mSharedElementEnterTransition = paramTransition;
  }
  
  public void setSharedElementReturnTransition(Transition paramTransition) {
    this.mSharedElementReturnTransition = paramTransition;
  }
  
  public void setSharedElementExitTransition(Transition paramTransition) {
    this.mSharedElementExitTransition = paramTransition;
  }
  
  public void setSharedElementReenterTransition(Transition paramTransition) {
    this.mSharedElementReenterTransition = paramTransition;
  }
  
  public Transition getEnterTransition() {
    return this.mEnterTransition;
  }
  
  public Transition getReturnTransition() {
    Transition transition = this.mReturnTransition;
    if (transition == USE_DEFAULT_TRANSITION)
      transition = getEnterTransition(); 
    return transition;
  }
  
  public Transition getExitTransition() {
    return this.mExitTransition;
  }
  
  public Transition getReenterTransition() {
    Transition transition = this.mReenterTransition;
    if (transition == USE_DEFAULT_TRANSITION)
      transition = getExitTransition(); 
    return transition;
  }
  
  public Transition getSharedElementEnterTransition() {
    return this.mSharedElementEnterTransition;
  }
  
  public Transition getSharedElementReturnTransition() {
    Transition transition = this.mSharedElementReturnTransition;
    if (transition == USE_DEFAULT_TRANSITION)
      transition = getSharedElementEnterTransition(); 
    return transition;
  }
  
  public Transition getSharedElementExitTransition() {
    return this.mSharedElementExitTransition;
  }
  
  public Transition getSharedElementReenterTransition() {
    Transition transition = this.mSharedElementReenterTransition;
    if (transition == USE_DEFAULT_TRANSITION)
      transition = getSharedElementExitTransition(); 
    return transition;
  }
  
  public void setAllowEnterTransitionOverlap(boolean paramBoolean) {
    this.mAllowEnterTransitionOverlap = Boolean.valueOf(paramBoolean);
  }
  
  public boolean getAllowEnterTransitionOverlap() {
    boolean bool1;
    Boolean bool = this.mAllowEnterTransitionOverlap;
    if (bool == null) {
      bool1 = true;
    } else {
      bool1 = bool.booleanValue();
    } 
    return bool1;
  }
  
  public void setAllowReturnTransitionOverlap(boolean paramBoolean) {
    this.mAllowReturnTransitionOverlap = Boolean.valueOf(paramBoolean);
  }
  
  public boolean getAllowReturnTransitionOverlap() {
    boolean bool1;
    Boolean bool = this.mAllowReturnTransitionOverlap;
    if (bool == null) {
      bool1 = true;
    } else {
      bool1 = bool.booleanValue();
    } 
    return bool1;
  }
  
  public long getTransitionBackgroundFadeDuration() {
    long l = this.mBackgroundFadeDurationMillis;
    if (l < 0L)
      l = 300L; 
    return l;
  }
  
  public void setTransitionBackgroundFadeDuration(long paramLong) {
    if (paramLong >= 0L) {
      this.mBackgroundFadeDurationMillis = paramLong;
      return;
    } 
    throw new IllegalArgumentException("negative durations are not allowed");
  }
  
  public void setSharedElementsUseOverlay(boolean paramBoolean) {
    this.mSharedElementsUseOverlay = Boolean.valueOf(paramBoolean);
  }
  
  public boolean getSharedElementsUseOverlay() {
    boolean bool1;
    Boolean bool = this.mSharedElementsUseOverlay;
    if (bool == null) {
      bool1 = true;
    } else {
      bool1 = bool.booleanValue();
    } 
    return bool1;
  }
  
  class DrawableFeatureState {
    int alpha;
    
    Drawable child;
    
    Drawable cur;
    
    int curAlpha;
    
    Drawable def;
    
    final int featureId;
    
    Drawable local;
    
    int resid;
    
    Uri uri;
    
    DrawableFeatureState(PhoneWindow this$0) {
      this.alpha = 255;
      this.curAlpha = 255;
      this.featureId = this$0;
    }
  }
  
  class PanelFeatureState {
    int background;
    
    View createdPanelView;
    
    DecorView decorView;
    
    int featureId;
    
    Bundle frozenActionViewState;
    
    Bundle frozenMenuState;
    
    int fullBackground;
    
    int gravity;
    
    IconMenuPresenter iconMenuPresenter;
    
    boolean isCompact;
    
    boolean isHandled;
    
    boolean isInExpandedMode;
    
    boolean isOpen;
    
    boolean isPrepared;
    
    ListMenuPresenter listMenuPresenter;
    
    int listPresenterTheme;
    
    MenuBuilder menu;
    
    public boolean qwertyMode;
    
    boolean refreshDecorView;
    
    boolean refreshMenuContent;
    
    View shownPanelView;
    
    boolean wasLastExpanded;
    
    boolean wasLastOpen;
    
    int windowAnimations;
    
    int x;
    
    int y;
    
    PanelFeatureState(PhoneWindow this$0) {
      this.featureId = this$0;
      this.refreshDecorView = false;
    }
    
    public boolean isInListMode() {
      return (this.isInExpandedMode || this.isCompact);
    }
    
    public boolean hasPanelItems() {
      View view = this.shownPanelView;
      boolean bool1 = false, bool2 = false;
      if (view == null)
        return false; 
      if (this.createdPanelView != null)
        return true; 
      if (this.isCompact || this.isInExpandedMode) {
        bool2 = bool1;
        if (this.listMenuPresenter.getAdapter().getCount() > 0)
          bool2 = true; 
        return bool2;
      } 
      if (((ViewGroup)view).getChildCount() > 0)
        bool2 = true; 
      return bool2;
    }
    
    public void clearMenuPresenters() {
      MenuBuilder menuBuilder = this.menu;
      if (menuBuilder != null) {
        menuBuilder.removeMenuPresenter(this.iconMenuPresenter);
        this.menu.removeMenuPresenter(this.listMenuPresenter);
      } 
      this.iconMenuPresenter = null;
      this.listMenuPresenter = null;
    }
    
    void setStyle(Context param1Context) {
      TypedArray typedArray = param1Context.obtainStyledAttributes(R.styleable.Theme);
      this.background = typedArray.getResourceId(46, 0);
      this.fullBackground = typedArray.getResourceId(47, 0);
      this.windowAnimations = typedArray.getResourceId(93, 0);
      this.isCompact = typedArray.getBoolean(314, false);
      this.listPresenterTheme = typedArray.getResourceId(315, 16974861);
      typedArray.recycle();
    }
    
    void setMenu(MenuBuilder param1MenuBuilder) {
      MenuBuilder menuBuilder = this.menu;
      if (param1MenuBuilder == menuBuilder)
        return; 
      if (menuBuilder != null) {
        menuBuilder.removeMenuPresenter(this.iconMenuPresenter);
        this.menu.removeMenuPresenter(this.listMenuPresenter);
      } 
      this.menu = param1MenuBuilder;
      if (param1MenuBuilder != null) {
        IconMenuPresenter iconMenuPresenter = this.iconMenuPresenter;
        if (iconMenuPresenter != null)
          param1MenuBuilder.addMenuPresenter(iconMenuPresenter); 
        ListMenuPresenter listMenuPresenter = this.listMenuPresenter;
        if (listMenuPresenter != null)
          param1MenuBuilder.addMenuPresenter(listMenuPresenter); 
      } 
    }
    
    MenuView getListMenuView(Context param1Context, MenuPresenter.Callback param1Callback) {
      if (this.menu == null)
        return null; 
      if (!this.isCompact)
        getIconMenuView(param1Context, param1Callback); 
      if (this.listMenuPresenter == null) {
        ListMenuPresenter listMenuPresenter = new ListMenuPresenter(17367190, this.listPresenterTheme);
        listMenuPresenter.setCallback(param1Callback);
        this.listMenuPresenter.setId(16909132);
        this.menu.addMenuPresenter(this.listMenuPresenter);
      } 
      IconMenuPresenter iconMenuPresenter = this.iconMenuPresenter;
      if (iconMenuPresenter != null) {
        ListMenuPresenter listMenuPresenter = this.listMenuPresenter;
        int i = iconMenuPresenter.getNumActualItemsShown();
        listMenuPresenter.setItemIndexOffset(i);
      } 
      return this.listMenuPresenter.getMenuView((ViewGroup)this.decorView);
    }
    
    MenuView getIconMenuView(Context param1Context, MenuPresenter.Callback param1Callback) {
      if (this.menu == null)
        return null; 
      if (this.iconMenuPresenter == null) {
        IconMenuPresenter iconMenuPresenter = new IconMenuPresenter(param1Context);
        iconMenuPresenter.setCallback(param1Callback);
        this.iconMenuPresenter.setId(16909053);
        this.menu.addMenuPresenter(this.iconMenuPresenter);
      } 
      return this.iconMenuPresenter.getMenuView((ViewGroup)this.decorView);
    }
    
    Parcelable onSaveInstanceState() {
      SavedState savedState = new SavedState();
      savedState.featureId = this.featureId;
      savedState.isOpen = this.isOpen;
      savedState.isInExpandedMode = this.isInExpandedMode;
      if (this.menu != null) {
        savedState.menuState = new Bundle();
        this.menu.savePresenterStates(savedState.menuState);
      } 
      return savedState;
    }
    
    void onRestoreInstanceState(Parcelable param1Parcelable) {
      param1Parcelable = param1Parcelable;
      this.featureId = ((SavedState)param1Parcelable).featureId;
      this.wasLastOpen = ((SavedState)param1Parcelable).isOpen;
      this.wasLastExpanded = ((SavedState)param1Parcelable).isInExpandedMode;
      this.frozenMenuState = ((SavedState)param1Parcelable).menuState;
      this.createdPanelView = null;
      this.shownPanelView = null;
      this.decorView = null;
    }
    
    void applyFrozenState() {
      MenuBuilder menuBuilder = this.menu;
      if (menuBuilder != null) {
        Bundle bundle = this.frozenMenuState;
        if (bundle != null) {
          menuBuilder.restorePresenterStates(bundle);
          this.frozenMenuState = null;
        } 
      } 
    }
    
    class SavedState implements Parcelable {
      private SavedState() {}
      
      public int describeContents() {
        return 0;
      }
      
      public void writeToParcel(Parcel param2Parcel, int param2Int) {
        param2Parcel.writeInt(this.featureId);
        param2Parcel.writeInt(this.isOpen);
        param2Parcel.writeInt(this.isInExpandedMode);
        if (this.isOpen)
          param2Parcel.writeBundle(this.menuState); 
      }
      
      private static SavedState readFromParcel(Parcel param2Parcel) {
        SavedState savedState = new SavedState();
        savedState.featureId = param2Parcel.readInt();
        int i = param2Parcel.readInt();
        boolean bool1 = false;
        if (i == 1) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        savedState.isOpen = bool2;
        boolean bool2 = bool1;
        if (param2Parcel.readInt() == 1)
          bool2 = true; 
        savedState.isInExpandedMode = bool2;
        if (savedState.isOpen)
          savedState.menuState = param2Parcel.readBundle(); 
        return savedState;
      }
      
      public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
          public PhoneWindow.PanelFeatureState.SavedState createFromParcel(Parcel param3Parcel) {
            return PhoneWindow.PanelFeatureState.SavedState.readFromParcel(param3Parcel);
          }
          
          public PhoneWindow.PanelFeatureState.SavedState[] newArray(int param3Int) {
            return new PhoneWindow.PanelFeatureState.SavedState[param3Int];
          }
        };
      
      int featureId;
      
      boolean isInExpandedMode;
      
      boolean isOpen;
      
      Bundle menuState;
    }
  }
  
  class RotationWatcher extends IRotationWatcher.Stub {
    private Handler mHandler;
    
    private boolean mIsWatching;
    
    private final Runnable mRotationChanged = (Runnable)new Object(this);
    
    private final ArrayList<WeakReference<PhoneWindow>> mWindows = new ArrayList<>();
    
    public void onRotationChanged(int param1Int) throws RemoteException {
      this.mHandler.post(this.mRotationChanged);
    }
    
    public void addWindow(PhoneWindow param1PhoneWindow) {
      synchronized (this.mWindows) {
        boolean bool = this.mIsWatching;
        if (!bool)
          try {
            IWindowManager iWindowManager = PhoneWindow.WindowManagerHolder.sWindowManager;
            int i = param1PhoneWindow.getContext().getDisplayId();
            iWindowManager.watchRotation((IRotationWatcher)this, i);
            Handler handler = new Handler();
            this();
            this.mHandler = handler;
            this.mIsWatching = true;
          } catch (RemoteException remoteException) {
            Log.e("PhoneWindow", "Couldn't start watching for device rotation", (Throwable)remoteException);
          }  
        ArrayList<WeakReference<PhoneWindow>> arrayList = this.mWindows;
        WeakReference<PhoneWindow> weakReference = new WeakReference();
        this((T)param1PhoneWindow);
        arrayList.add(weakReference);
        return;
      } 
    }
    
    public void removeWindow(PhoneWindow param1PhoneWindow) {
      // Byte code:
      //   0: aload_0
      //   1: getfield mWindows : Ljava/util/ArrayList;
      //   4: astore_2
      //   5: aload_2
      //   6: monitorenter
      //   7: iconst_0
      //   8: istore_3
      //   9: iload_3
      //   10: aload_0
      //   11: getfield mWindows : Ljava/util/ArrayList;
      //   14: invokevirtual size : ()I
      //   17: if_icmpge -> 75
      //   20: aload_0
      //   21: getfield mWindows : Ljava/util/ArrayList;
      //   24: iload_3
      //   25: invokevirtual get : (I)Ljava/lang/Object;
      //   28: checkcast java/lang/ref/WeakReference
      //   31: astore #4
      //   33: aload #4
      //   35: invokevirtual get : ()Ljava/lang/Object;
      //   38: checkcast com/android/internal/policy/PhoneWindow
      //   41: astore #4
      //   43: aload #4
      //   45: ifnull -> 63
      //   48: aload #4
      //   50: aload_1
      //   51: if_acmpne -> 57
      //   54: goto -> 63
      //   57: iinc #3, 1
      //   60: goto -> 72
      //   63: aload_0
      //   64: getfield mWindows : Ljava/util/ArrayList;
      //   67: iload_3
      //   68: invokevirtual remove : (I)Ljava/lang/Object;
      //   71: pop
      //   72: goto -> 9
      //   75: aload_2
      //   76: monitorexit
      //   77: return
      //   78: astore_1
      //   79: aload_2
      //   80: monitorexit
      //   81: aload_1
      //   82: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #3652	-> 0
      //   #3653	-> 7
      //   #3654	-> 9
      //   #3655	-> 20
      //   #3656	-> 33
      //   #3657	-> 43
      //   #3660	-> 57
      //   #3658	-> 63
      //   #3662	-> 72
      //   #3663	-> 75
      //   #3664	-> 77
      //   #3663	-> 78
      // Exception table:
      //   from	to	target	type
      //   9	20	78	finally
      //   20	33	78	finally
      //   33	43	78	finally
      //   63	72	78	finally
      //   75	77	78	finally
      //   79	81	78	finally
    }
    
    void dispatchRotationChanged() {
      // Byte code:
      //   0: aload_0
      //   1: getfield mWindows : Ljava/util/ArrayList;
      //   4: astore_1
      //   5: aload_1
      //   6: monitorenter
      //   7: iconst_0
      //   8: istore_2
      //   9: iload_2
      //   10: aload_0
      //   11: getfield mWindows : Ljava/util/ArrayList;
      //   14: invokevirtual size : ()I
      //   17: if_icmpge -> 66
      //   20: aload_0
      //   21: getfield mWindows : Ljava/util/ArrayList;
      //   24: iload_2
      //   25: invokevirtual get : (I)Ljava/lang/Object;
      //   28: checkcast java/lang/ref/WeakReference
      //   31: astore_3
      //   32: aload_3
      //   33: invokevirtual get : ()Ljava/lang/Object;
      //   36: checkcast com/android/internal/policy/PhoneWindow
      //   39: astore_3
      //   40: aload_3
      //   41: ifnull -> 54
      //   44: aload_3
      //   45: invokevirtual onOptionsPanelRotationChanged : ()V
      //   48: iinc #2, 1
      //   51: goto -> 63
      //   54: aload_0
      //   55: getfield mWindows : Ljava/util/ArrayList;
      //   58: iload_2
      //   59: invokevirtual remove : (I)Ljava/lang/Object;
      //   62: pop
      //   63: goto -> 9
      //   66: aload_1
      //   67: monitorexit
      //   68: return
      //   69: astore_3
      //   70: aload_1
      //   71: monitorexit
      //   72: aload_3
      //   73: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #3667	-> 0
      //   #3668	-> 7
      //   #3669	-> 9
      //   #3670	-> 20
      //   #3671	-> 32
      //   #3672	-> 40
      //   #3673	-> 44
      //   #3674	-> 48
      //   #3676	-> 54
      //   #3678	-> 63
      //   #3679	-> 66
      //   #3680	-> 68
      //   #3679	-> 69
      // Exception table:
      //   from	to	target	type
      //   9	20	69	finally
      //   20	32	69	finally
      //   32	40	69	finally
      //   44	48	69	finally
      //   54	63	69	finally
      //   66	68	69	finally
      //   70	72	69	finally
    }
  }
  
  class PhoneWindowMenuCallback implements MenuBuilder.Callback, MenuPresenter.Callback {
    private static final int FEATURE_ID = 6;
    
    private boolean mShowDialogForSubmenu;
    
    private MenuDialogHelper mSubMenuHelper;
    
    private final PhoneWindow mWindow;
    
    public PhoneWindowMenuCallback(PhoneWindow this$0) {
      this.mWindow = this$0;
    }
    
    public void onCloseMenu(MenuBuilder param1MenuBuilder, boolean param1Boolean) {
      if (param1MenuBuilder.getRootMenu() != param1MenuBuilder)
        onCloseSubMenu(param1MenuBuilder); 
      if (param1Boolean) {
        Window.Callback callback = this.mWindow.getCallback();
        if (callback != null && !this.mWindow.isDestroyed())
          callback.onPanelClosed(6, param1MenuBuilder); 
        if (param1MenuBuilder == this.mWindow.mContextMenu)
          this.mWindow.dismissContextMenu(); 
        MenuDialogHelper menuDialogHelper = this.mSubMenuHelper;
        if (menuDialogHelper != null) {
          menuDialogHelper.dismiss();
          this.mSubMenuHelper = null;
        } 
      } 
    }
    
    private void onCloseSubMenu(MenuBuilder param1MenuBuilder) {
      Window.Callback callback = this.mWindow.getCallback();
      if (callback != null && !this.mWindow.isDestroyed())
        callback.onPanelClosed(6, param1MenuBuilder.getRootMenu()); 
    }
    
    public boolean onMenuItemSelected(MenuBuilder param1MenuBuilder, MenuItem param1MenuItem) {
      boolean bool;
      Window.Callback callback = this.mWindow.getCallback();
      if (callback != null && !this.mWindow.isDestroyed() && 
        callback.onMenuItemSelected(6, param1MenuItem)) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public void onMenuModeChange(MenuBuilder param1MenuBuilder) {}
    
    public boolean onOpenSubMenu(MenuBuilder param1MenuBuilder) {
      if (param1MenuBuilder == null)
        return false; 
      param1MenuBuilder.setCallback(this);
      if (this.mShowDialogForSubmenu) {
        MenuDialogHelper menuDialogHelper = new MenuDialogHelper(param1MenuBuilder);
        menuDialogHelper.show(null);
        return true;
      } 
      return false;
    }
    
    public void setShowDialogForSubmenu(boolean param1Boolean) {
      this.mShowDialogForSubmenu = param1Boolean;
    }
  }
  
  int getLocalFeaturesPrivate() {
    return getLocalFeatures();
  }
  
  protected void setDefaultWindowFormat(int paramInt) {
    super.setDefaultWindowFormat(paramInt);
  }
  
  void sendCloseSystemWindows() {
    sendCloseSystemWindows(getContext(), (String)null);
  }
  
  void sendCloseSystemWindows(String paramString) {
    sendCloseSystemWindows(getContext(), paramString);
  }
  
  public static void sendCloseSystemWindows(Context paramContext, String paramString) {
    if (ActivityManager.isSystemReady())
      try {
        ActivityManager.getService().closeSystemDialogs(paramString);
      } catch (RemoteException remoteException) {} 
  }
  
  public int getStatusBarColor() {
    return this.mStatusBarColor;
  }
  
  public void setStatusBarColor(int paramInt) {
    this.mStatusBarColor = paramInt;
    this.mForcedStatusBarColor = true;
    DecorView decorView = this.mDecor;
    if (decorView != null)
      decorView.updateColorViews((WindowInsets)null, false); 
    Window.WindowControllerCallback windowControllerCallback = getWindowControllerCallback();
    if (windowControllerCallback != null)
      getWindowControllerCallback().updateStatusBarColor(paramInt); 
  }
  
  public int getNavigationBarColor() {
    return this.mNavigationBarColor;
  }
  
  public void setNavigationBarColor(int paramInt) {
    this.mNavigationBarColor = paramInt;
    this.mForcedNavigationBarColor = true;
    DecorView decorView = this.mDecor;
    if (decorView != null) {
      decorView.updateColorNavigationGuardColor(paramInt);
      this.mDecor.updateColorViews((WindowInsets)null, false);
    } 
    Window.WindowControllerCallback windowControllerCallback = getWindowControllerCallback();
    if (windowControllerCallback != null)
      getWindowControllerCallback().updateNavigationBarColor(paramInt); 
  }
  
  public void setNavigationBarDividerColor(int paramInt) {
    this.mNavigationBarDividerColor = paramInt;
    DecorView decorView = this.mDecor;
    if (decorView != null)
      decorView.updateColorViews((WindowInsets)null, false); 
  }
  
  public int getNavigationBarDividerColor() {
    return this.mNavigationBarDividerColor;
  }
  
  public void setStatusBarContrastEnforced(boolean paramBoolean) {
    this.mEnsureStatusBarContrastWhenTransparent = paramBoolean;
    DecorView decorView = this.mDecor;
    if (decorView != null)
      decorView.updateColorViews((WindowInsets)null, false); 
  }
  
  public boolean isStatusBarContrastEnforced() {
    return this.mEnsureStatusBarContrastWhenTransparent;
  }
  
  public void setNavigationBarContrastEnforced(boolean paramBoolean) {
    this.mEnsureNavigationBarContrastWhenTransparent = paramBoolean;
    DecorView decorView = this.mDecor;
    if (decorView != null)
      decorView.updateColorViews((WindowInsets)null, false); 
  }
  
  public boolean isNavigationBarContrastEnforced() {
    return this.mEnsureNavigationBarContrastWhenTransparent;
  }
  
  public void setIsStartingWindow(boolean paramBoolean) {
    this.mIsStartingWindow = paramBoolean;
  }
  
  public void setTheme(int paramInt) {
    this.mTheme = paramInt;
    DecorView decorView = this.mDecor;
    if (decorView != null) {
      Context context = decorView.getContext();
      if (context instanceof DecorContext)
        context.setTheme(paramInt); 
    } 
  }
  
  public void setResizingCaptionDrawable(Drawable paramDrawable) {
    this.mDecor.setUserCaptionBackgroundDrawable(paramDrawable);
  }
  
  public void setDecorCaptionShade(int paramInt) {
    this.mDecorCaptionShade = paramInt;
    DecorView decorView = this.mDecor;
    if (decorView != null)
      decorView.updateDecorCaptionShade(); 
  }
  
  int getDecorCaptionShade() {
    return this.mDecorCaptionShade;
  }
  
  public void setAttributes(WindowManager.LayoutParams paramLayoutParams) {
    super.setAttributes(paramLayoutParams);
    DecorView decorView = this.mDecor;
    if (decorView != null)
      decorView.updateLogTag(paramLayoutParams); 
  }
  
  public WindowInsetsController getInsetsController() {
    return this.mDecor.getWindowInsetsController();
  }
  
  public void setSystemGestureExclusionRects(List<Rect> paramList) {
    getViewRootImpl().setRootSystemGestureExclusionRects(paramList);
  }
  
  public List<Rect> getSystemGestureExclusionRects() {
    return getViewRootImpl().getRootSystemGestureExclusionRects();
  }
  
  public void setDecorFitsSystemWindows(boolean paramBoolean) {
    this.mDecorFitsSystemWindows = paramBoolean;
    applyDecorFitsSystemWindows();
  }
  
  private void applyDecorFitsSystemWindows() {
    ViewRootImpl viewRootImpl = getViewRootImplOrNull();
    if (viewRootImpl != null) {
      Window.OnContentApplyWindowInsetsListener onContentApplyWindowInsetsListener;
      if (this.mDecorFitsSystemWindows) {
        onContentApplyWindowInsetsListener = sDefaultContentInsetsApplier;
      } else {
        onContentApplyWindowInsetsListener = null;
      } 
      viewRootImpl.setOnContentApplyWindowInsetsListener(onContentApplyWindowInsetsListener);
    } 
  }
  
  public void setSystemBarColor(int paramInt) {
    this.mStatusBarColor = paramInt;
    this.mForcedStatusBarColor = true;
    this.mNavigationBarColor = paramInt;
    this.mForcedNavigationBarColor = true;
    this.mEnsureStatusBarContrastWhenTransparent = false;
    this.mEnsureNavigationBarContrastWhenTransparent = false;
    DecorView decorView = this.mDecor;
    if (decorView != null)
      decorView.updateColorViews((WindowInsets)null, false); 
    Window.WindowControllerCallback windowControllerCallback = getWindowControllerCallback();
    if (windowControllerCallback != null) {
      getWindowControllerCallback().updateStatusBarColor(paramInt);
      getWindowControllerCallback().updateNavigationBarColor(paramInt);
    } 
  }
  
  public ViewGroup getContentParent() {
    return this.mContentParent;
  }
  
  public CharSequence getWindowTitle() {
    return this.mTitle;
  }
  
  public void requestScrollCapture(IScrollCaptureController paramIScrollCaptureController) {
    getViewRootImpl().dispatchScrollCaptureRequest(paramIScrollCaptureController);
  }
  
  public void addScrollCaptureCallback(ScrollCaptureCallback paramScrollCaptureCallback) {
    getViewRootImpl().addScrollCaptureCallback(paramScrollCaptureCallback);
  }
  
  public void removeScrollCaptureCallback(ScrollCaptureCallback paramScrollCaptureCallback) {
    getViewRootImpl().removeScrollCaptureCallback(paramScrollCaptureCallback);
  }
  
  public View getStatusBarBackgroundView() {
    DecorView decorView = this.mDecor;
    if (decorView != null) {
      View view = decorView.getStatusBarBackgroundView();
    } else {
      decorView = null;
    } 
    return (View)decorView;
  }
  
  public View getNavigationBarBackgroundView() {
    DecorView decorView = this.mDecor;
    if (decorView != null) {
      View view = decorView.getNavigationBarBackgroundView();
    } else {
      decorView = null;
    } 
    return (View)decorView;
  }
  
  public boolean isUseDefaultNavigationBarColor() {
    boolean bool;
    if (this.mIsUseDefaultNavigationBarColor && !this.mForcedNavigationBarColor) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
}
