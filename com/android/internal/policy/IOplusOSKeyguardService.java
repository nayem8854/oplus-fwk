package com.android.internal.policy;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusOSKeyguardService extends IInterface {
  void onKeyguardDoneForOplus(boolean paramBoolean) throws RemoteException;
  
  void sendCommandToApps(String paramString) throws RemoteException;
  
  void setNotificationListener(boolean paramBoolean) throws RemoteException;
  
  class Default implements IOplusOSKeyguardService {
    public void onKeyguardDoneForOplus(boolean param1Boolean) throws RemoteException {}
    
    public void setNotificationListener(boolean param1Boolean) throws RemoteException {}
    
    public void sendCommandToApps(String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusOSKeyguardService {
    private static final String DESCRIPTOR = "com.android.internal.policy.IOplusOSKeyguardService";
    
    static final int TRANSACTION_onKeyguardDoneForOplus = 1;
    
    static final int TRANSACTION_sendCommandToApps = 3;
    
    static final int TRANSACTION_setNotificationListener = 2;
    
    public Stub() {
      attachInterface(this, "com.android.internal.policy.IOplusOSKeyguardService");
    }
    
    public static IOplusOSKeyguardService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.policy.IOplusOSKeyguardService");
      if (iInterface != null && iInterface instanceof IOplusOSKeyguardService)
        return (IOplusOSKeyguardService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "sendCommandToApps";
        } 
        return "setNotificationListener";
      } 
      return "onKeyguardDoneForOplus";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      boolean bool1 = false, bool2 = false;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("com.android.internal.policy.IOplusOSKeyguardService");
            return true;
          } 
          param1Parcel1.enforceInterface("com.android.internal.policy.IOplusOSKeyguardService");
          str = param1Parcel1.readString();
          sendCommandToApps(str);
          param1Parcel2.writeNoException();
          return true;
        } 
        str.enforceInterface("com.android.internal.policy.IOplusOSKeyguardService");
        bool1 = bool2;
        if (str.readInt() != 0)
          bool1 = true; 
        setNotificationListener(bool1);
        return true;
      } 
      str.enforceInterface("com.android.internal.policy.IOplusOSKeyguardService");
      if (str.readInt() != 0)
        bool1 = true; 
      onKeyguardDoneForOplus(bool1);
      return true;
    }
    
    private static class Proxy implements IOplusOSKeyguardService {
      public static IOplusOSKeyguardService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.policy.IOplusOSKeyguardService";
      }
      
      public void onKeyguardDoneForOplus(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.policy.IOplusOSKeyguardService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, null, 1);
          if (!bool1 && IOplusOSKeyguardService.Stub.getDefaultImpl() != null) {
            IOplusOSKeyguardService.Stub.getDefaultImpl().onKeyguardDoneForOplus(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setNotificationListener(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.policy.IOplusOSKeyguardService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(2, parcel, null, 1);
          if (!bool1 && IOplusOSKeyguardService.Stub.getDefaultImpl() != null) {
            IOplusOSKeyguardService.Stub.getDefaultImpl().setNotificationListener(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void sendCommandToApps(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.policy.IOplusOSKeyguardService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IOplusOSKeyguardService.Stub.getDefaultImpl() != null) {
            IOplusOSKeyguardService.Stub.getDefaultImpl().sendCommandToApps(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusOSKeyguardService param1IOplusOSKeyguardService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusOSKeyguardService != null) {
          Proxy.sDefaultImpl = param1IOplusOSKeyguardService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusOSKeyguardService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
