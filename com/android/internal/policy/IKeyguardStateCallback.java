package com.android.internal.policy;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IKeyguardStateCallback extends IInterface {
  void onHasLockscreenWallpaperChanged(boolean paramBoolean) throws RemoteException;
  
  void onInputRestrictedStateChanged(boolean paramBoolean) throws RemoteException;
  
  void onShowingStateChanged(boolean paramBoolean) throws RemoteException;
  
  void onSimSecureStateChanged(boolean paramBoolean) throws RemoteException;
  
  void onTrustedChanged(boolean paramBoolean) throws RemoteException;
  
  class Default implements IKeyguardStateCallback {
    public void onShowingStateChanged(boolean param1Boolean) throws RemoteException {}
    
    public void onSimSecureStateChanged(boolean param1Boolean) throws RemoteException {}
    
    public void onInputRestrictedStateChanged(boolean param1Boolean) throws RemoteException {}
    
    public void onTrustedChanged(boolean param1Boolean) throws RemoteException {}
    
    public void onHasLockscreenWallpaperChanged(boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IKeyguardStateCallback {
    private static final String DESCRIPTOR = "com.android.internal.policy.IKeyguardStateCallback";
    
    static final int TRANSACTION_onHasLockscreenWallpaperChanged = 5;
    
    static final int TRANSACTION_onInputRestrictedStateChanged = 3;
    
    static final int TRANSACTION_onShowingStateChanged = 1;
    
    static final int TRANSACTION_onSimSecureStateChanged = 2;
    
    static final int TRANSACTION_onTrustedChanged = 4;
    
    public Stub() {
      attachInterface(this, "com.android.internal.policy.IKeyguardStateCallback");
    }
    
    public static IKeyguardStateCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.policy.IKeyguardStateCallback");
      if (iInterface != null && iInterface instanceof IKeyguardStateCallback)
        return (IKeyguardStateCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "onHasLockscreenWallpaperChanged";
            } 
            return "onTrustedChanged";
          } 
          return "onInputRestrictedStateChanged";
        } 
        return "onSimSecureStateChanged";
      } 
      return "onShowingStateChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool1 = false, bool2 = false, bool3 = false, bool4 = false, bool5 = false;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("com.android.internal.policy.IKeyguardStateCallback");
                return true;
              } 
              param1Parcel1.enforceInterface("com.android.internal.policy.IKeyguardStateCallback");
              if (param1Parcel1.readInt() != 0)
                bool5 = true; 
              onHasLockscreenWallpaperChanged(bool5);
              param1Parcel2.writeNoException();
              return true;
            } 
            param1Parcel1.enforceInterface("com.android.internal.policy.IKeyguardStateCallback");
            bool5 = bool1;
            if (param1Parcel1.readInt() != 0)
              bool5 = true; 
            onTrustedChanged(bool5);
            param1Parcel2.writeNoException();
            return true;
          } 
          param1Parcel1.enforceInterface("com.android.internal.policy.IKeyguardStateCallback");
          bool5 = bool2;
          if (param1Parcel1.readInt() != 0)
            bool5 = true; 
          onInputRestrictedStateChanged(bool5);
          param1Parcel2.writeNoException();
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.internal.policy.IKeyguardStateCallback");
        bool5 = bool3;
        if (param1Parcel1.readInt() != 0)
          bool5 = true; 
        onSimSecureStateChanged(bool5);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.policy.IKeyguardStateCallback");
      bool5 = bool4;
      if (param1Parcel1.readInt() != 0)
        bool5 = true; 
      onShowingStateChanged(bool5);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IKeyguardStateCallback {
      public static IKeyguardStateCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.policy.IKeyguardStateCallback";
      }
      
      public void onShowingStateChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.policy.IKeyguardStateCallback");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool1 && IKeyguardStateCallback.Stub.getDefaultImpl() != null) {
            IKeyguardStateCallback.Stub.getDefaultImpl().onShowingStateChanged(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onSimSecureStateChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.policy.IKeyguardStateCallback");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool1 && IKeyguardStateCallback.Stub.getDefaultImpl() != null) {
            IKeyguardStateCallback.Stub.getDefaultImpl().onSimSecureStateChanged(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onInputRestrictedStateChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.policy.IKeyguardStateCallback");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool1 && IKeyguardStateCallback.Stub.getDefaultImpl() != null) {
            IKeyguardStateCallback.Stub.getDefaultImpl().onInputRestrictedStateChanged(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onTrustedChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.policy.IKeyguardStateCallback");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool1 && IKeyguardStateCallback.Stub.getDefaultImpl() != null) {
            IKeyguardStateCallback.Stub.getDefaultImpl().onTrustedChanged(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onHasLockscreenWallpaperChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.policy.IKeyguardStateCallback");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool1 && IKeyguardStateCallback.Stub.getDefaultImpl() != null) {
            IKeyguardStateCallback.Stub.getDefaultImpl().onHasLockscreenWallpaperChanged(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IKeyguardStateCallback param1IKeyguardStateCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IKeyguardStateCallback != null) {
          Proxy.sDefaultImpl = param1IKeyguardStateCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IKeyguardStateCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
