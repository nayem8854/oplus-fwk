package com.android.internal.policy;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

public class PhoneLayoutInflater extends LayoutInflater {
  private static final String[] sClassPrefixList = new String[] { "android.widget.", "android.webkit.", "android.app." };
  
  public PhoneLayoutInflater(Context paramContext) {
    super(paramContext);
  }
  
  protected PhoneLayoutInflater(LayoutInflater paramLayoutInflater, Context paramContext) {
    super(paramLayoutInflater, paramContext);
  }
  
  protected View onCreateView(String paramString, AttributeSet paramAttributeSet) throws ClassNotFoundException {
    for (String str : sClassPrefixList) {
      try {
        View view = createView(paramString, str, paramAttributeSet);
        if (view != null)
          return view; 
      } catch (ClassNotFoundException classNotFoundException) {}
    } 
    return super.onCreateView(paramString, paramAttributeSet);
  }
  
  public LayoutInflater cloneInContext(Context paramContext) {
    return new PhoneLayoutInflater(this, paramContext);
  }
}
