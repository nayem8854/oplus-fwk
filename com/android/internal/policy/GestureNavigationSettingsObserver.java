package com.android.internal.policy;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.provider.DeviceConfig;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.TypedValue;

public class GestureNavigationSettingsObserver extends ContentObserver {
  private Context mContext;
  
  private Handler mMainHandler;
  
  private Runnable mOnChangeRunnable;
  
  private final DeviceConfig.OnPropertiesChangedListener mOnPropertiesChangedListener;
  
  public GestureNavigationSettingsObserver(Handler paramHandler, Context paramContext, Runnable paramRunnable) {
    super(paramHandler);
    this.mOnPropertiesChangedListener = new DeviceConfig.OnPropertiesChangedListener() {
        final GestureNavigationSettingsObserver this$0;
        
        public void onPropertiesChanged(DeviceConfig.Properties param1Properties) {
          if ("systemui".equals(param1Properties.getNamespace())) {
            GestureNavigationSettingsObserver gestureNavigationSettingsObserver = GestureNavigationSettingsObserver.this;
            if (gestureNavigationSettingsObserver.mOnChangeRunnable != null)
              GestureNavigationSettingsObserver.this.mOnChangeRunnable.run(); 
          } 
        }
      };
    this.mMainHandler = paramHandler;
    this.mContext = paramContext;
    this.mOnChangeRunnable = paramRunnable;
  }
  
  public void register() {
    ContentResolver contentResolver = this.mContext.getContentResolver();
    Uri uri = Settings.Secure.getUriFor("back_gesture_inset_scale_left");
    contentResolver.registerContentObserver(uri, false, this, -1);
    uri = Settings.Secure.getUriFor("back_gesture_inset_scale_right");
    contentResolver.registerContentObserver(uri, false, this, -1);
    uri = Settings.Secure.getUriFor("user_setup_complete");
    contentResolver.registerContentObserver(uri, false, this, -1);
    DeviceConfig.addOnPropertiesChangedListener("systemui", new _$$Lambda$GestureNavigationSettingsObserver$F3d_7CsBNLG_3ODg3tTHhvWfOA4(this), this.mOnPropertiesChangedListener);
  }
  
  public void unregister() {
    this.mContext.getContentResolver().unregisterContentObserver(this);
    DeviceConfig.removeOnPropertiesChangedListener(this.mOnPropertiesChangedListener);
  }
  
  public void onChange(boolean paramBoolean) {
    super.onChange(paramBoolean);
    Runnable runnable = this.mOnChangeRunnable;
    if (runnable != null)
      runnable.run(); 
  }
  
  public int getLeftSensitivity(Resources paramResources) {
    return getSensitivity(paramResources, "back_gesture_inset_scale_left");
  }
  
  public int getRightSensitivity(Resources paramResources) {
    return getSensitivity(paramResources, "back_gesture_inset_scale_right");
  }
  
  public boolean areNavigationButtonForcedVisible() {
    ContentResolver contentResolver = this.mContext.getContentResolver();
    boolean bool = false;
    if (Settings.Secure.getIntForUser(contentResolver, "user_setup_complete", 0, -2) == 0)
      bool = true; 
    return bool;
  }
  
  private int getSensitivity(Resources paramResources, String paramString) {
    DisplayMetrics displayMetrics = paramResources.getDisplayMetrics();
    float f1 = paramResources.getDimension(17105051) / displayMetrics.density;
    f1 = DeviceConfig.getFloat("systemui", "back_gesture_edge_width", f1);
    float f2 = TypedValue.applyDimension(1, f1, displayMetrics);
    Context context = this.mContext;
    ContentResolver contentResolver = context.getContentResolver();
    f1 = Settings.Secure.getFloatForUser(contentResolver, paramString, 1.0F, -2);
    return (int)(f2 * f1);
  }
}
