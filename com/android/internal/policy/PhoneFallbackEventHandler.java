package com.android.internal.policy;

import android.app.KeyguardManager;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.session.MediaSessionManager;
import android.os.Parcelable;
import android.os.UserHandle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.FallbackEventHandler;
import android.view.KeyEvent;
import android.view.View;
import com.oplus.debug.InputLog;

public class PhoneFallbackEventHandler implements FallbackEventHandler {
  private static final boolean DEBUG = false;
  
  private static String TAG = "PhoneFallbackEventHandler";
  
  AudioManager mAudioManager;
  
  Context mContext;
  
  KeyguardManager mKeyguardManager;
  
  MediaSessionManager mMediaSessionManager;
  
  SearchManager mSearchManager;
  
  TelephonyManager mTelephonyManager;
  
  View mView;
  
  public PhoneFallbackEventHandler(Context paramContext) {
    this.mContext = paramContext;
  }
  
  public void setView(View paramView) {
    this.mView = paramView;
  }
  
  public void preDispatchKeyEvent(KeyEvent paramKeyEvent) {
    getAudioManager().preDispatchKeyEvent(paramKeyEvent, -2147483648);
  }
  
  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent) {
    if (InputLog.DEBUG) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(" dispatchKeyEvent  ");
      stringBuilder.append(paramKeyEvent);
      InputLog.d(str, stringBuilder.toString());
    } 
    int i = paramKeyEvent.getAction();
    int j = paramKeyEvent.getKeyCode();
    if (i == 0)
      return onKeyDown(j, paramKeyEvent); 
    return onKeyUp(j, paramKeyEvent);
  }
  
  boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
    Intent intent;
    KeyEvent.DispatcherState dispatcherState = this.mView.getKeyDispatcherState();
    if (paramInt != 5) {
      if (paramInt != 27) {
        if (paramInt != 79 && paramInt != 130) {
          if (paramInt != 164)
            if (paramInt != 222) {
              if (paramInt != 24 && paramInt != 25)
                if (paramInt != 126 && paramInt != 127) {
                  switch (paramInt) {
                    default:
                      return false;
                    case 84:
                      if (!isNotInstantAppAndKeyguardRestricted(dispatcherState))
                        if (paramKeyEvent.getRepeatCount() == 0) {
                          dispatcherState.startTracking(paramKeyEvent, this);
                        } else if (paramKeyEvent.isLongPress() && dispatcherState.isTracking(paramKeyEvent)) {
                          Configuration configuration = this.mContext.getResources().getConfiguration();
                          if (configuration.keyboard == 1 || configuration.hardKeyboardHidden == 2)
                            if (isUserSetupComplete()) {
                              Intent intent1 = new Intent("android.intent.action.SEARCH_LONG_PRESS");
                              intent1.setFlags(268435456);
                              try {
                                this.mView.performHapticFeedback(0);
                                sendCloseSystemWindows();
                                getSearchManager().stopSearch();
                                this.mContext.startActivity(intent1);
                                dispatcherState.performedLongPress(paramKeyEvent);
                                return true;
                              } catch (ActivityNotFoundException activityNotFoundException) {}
                            } else {
                              Log.i(TAG, "Not dispatching SEARCH long press because user setup is in progress.");
                            }  
                        }  
                    case 85:
                      if (getTelephonyManager().getCallState() != 0)
                        return true; 
                      break;
                    case 86:
                    case 87:
                    case 88:
                    case 89:
                    case 90:
                    case 91:
                      break;
                  } 
                  handleMediaKeyEvent((KeyEvent)activityNotFoundException);
                  return true;
                }  
            } else {
              handleMediaKeyEvent((KeyEvent)activityNotFoundException);
              return true;
            }  
          if (InputLog.DEBUG)
            InputLog.d(TAG, " getAudioManager.handleKeyDown() in  PhoneFallbackEventHandler"); 
          handleVolumeKeyEvent((KeyEvent)activityNotFoundException);
          return true;
        } 
        handleMediaKeyEvent((KeyEvent)activityNotFoundException);
        return true;
      } 
      if (isNotInstantAppAndKeyguardRestricted(dispatcherState));
      if (activityNotFoundException.getRepeatCount() == 0) {
        dispatcherState.startTracking((KeyEvent)activityNotFoundException, this);
      } else if (activityNotFoundException.isLongPress() && dispatcherState.isTracking((KeyEvent)activityNotFoundException)) {
        dispatcherState.performedLongPress((KeyEvent)activityNotFoundException);
        if (isUserSetupComplete()) {
          this.mView.performHapticFeedback(0);
          sendCloseSystemWindows();
          intent = new Intent("android.intent.action.CAMERA_BUTTON", null);
          intent.addFlags(268435456);
          intent.putExtra("android.intent.extra.KEY_EVENT", (Parcelable)activityNotFoundException);
          this.mContext.sendOrderedBroadcastAsUser(intent, UserHandle.CURRENT_OR_SELF, null, null, null, 0, null, null);
        } else {
          Log.i(TAG, "Not dispatching CAMERA long press because user setup is in progress.");
        } 
      } 
      return true;
    } 
    if (isNotInstantAppAndKeyguardRestricted((KeyEvent.DispatcherState)intent));
    if (activityNotFoundException.getRepeatCount() == 0) {
      intent.startTracking((KeyEvent)activityNotFoundException, this);
    } else if (activityNotFoundException.isLongPress() && intent.isTracking((KeyEvent)activityNotFoundException)) {
      intent.performedLongPress((KeyEvent)activityNotFoundException);
      if (isUserSetupComplete()) {
        this.mView.performHapticFeedback(0);
        Intent intent1 = new Intent("android.intent.action.VOICE_COMMAND");
        intent1.setFlags(268435456);
        try {
          sendCloseSystemWindows();
          this.mContext.startActivity(intent1);
        } catch (ActivityNotFoundException activityNotFoundException1) {
          startCallActivity();
        } 
      } else {
        Log.i(TAG, "Not starting call activity because user setup is in progress.");
      } 
    } 
    return true;
  }
  
  private boolean isNotInstantAppAndKeyguardRestricted(KeyEvent.DispatcherState paramDispatcherState) {
    boolean bool;
    if (!this.mContext.getPackageManager().isInstantApp() && (
      getKeyguardManager().inKeyguardRestrictedInputMode() || paramDispatcherState == null)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent) {
    KeyEvent.DispatcherState dispatcherState = this.mView.getKeyDispatcherState();
    if (dispatcherState != null)
      dispatcherState.handleUpEvent(paramKeyEvent); 
    if (paramInt != 5) {
      if (paramInt != 27) {
        if (paramInt != 79 && paramInt != 130) {
          if (paramInt != 164)
            if (paramInt != 222) {
              if (paramInt != 24 && paramInt != 25) {
                if (paramInt != 126 && paramInt != 127)
                  switch (paramInt) {
                    default:
                      return false;
                    case 85:
                    case 86:
                    case 87:
                    case 88:
                    case 89:
                    case 90:
                    case 91:
                      break;
                  }  
                handleMediaKeyEvent(paramKeyEvent);
                return true;
              } 
            } else {
              handleMediaKeyEvent(paramKeyEvent);
              return true;
            }  
          if (!paramKeyEvent.isCanceled()) {
            handleVolumeKeyEvent(paramKeyEvent);
            if (InputLog.DEBUG)
              InputLog.d(TAG, " getAudioManager.handleKeyUp() in  PhoneFallbackEventHandler"); 
          } 
          return true;
        } 
        handleMediaKeyEvent(paramKeyEvent);
        return true;
      } 
      if (isNotInstantAppAndKeyguardRestricted(dispatcherState));
      if (paramKeyEvent.isTracking())
        paramKeyEvent.isCanceled(); 
      return true;
    } 
    if (isNotInstantAppAndKeyguardRestricted(dispatcherState));
    if (paramKeyEvent.isTracking() && !paramKeyEvent.isCanceled())
      if (isUserSetupComplete()) {
        startCallActivity();
      } else {
        Log.i(TAG, "Not starting call activity because user setup is in progress.");
      }  
    return true;
  }
  
  void startCallActivity() {
    sendCloseSystemWindows();
    Intent intent = new Intent("android.intent.action.CALL_BUTTON");
    intent.setFlags(268435456);
    try {
      this.mContext.startActivity(intent);
    } catch (ActivityNotFoundException activityNotFoundException) {
      Log.w(TAG, "No activity found for android.intent.action.CALL_BUTTON.");
    } 
  }
  
  SearchManager getSearchManager() {
    if (this.mSearchManager == null)
      this.mSearchManager = (SearchManager)this.mContext.getSystemService("search"); 
    return this.mSearchManager;
  }
  
  TelephonyManager getTelephonyManager() {
    if (this.mTelephonyManager == null)
      this.mTelephonyManager = (TelephonyManager)this.mContext.getSystemService("phone"); 
    return this.mTelephonyManager;
  }
  
  KeyguardManager getKeyguardManager() {
    if (this.mKeyguardManager == null)
      this.mKeyguardManager = (KeyguardManager)this.mContext.getSystemService("keyguard"); 
    return this.mKeyguardManager;
  }
  
  AudioManager getAudioManager() {
    if (this.mAudioManager == null)
      this.mAudioManager = (AudioManager)this.mContext.getSystemService("audio"); 
    return this.mAudioManager;
  }
  
  MediaSessionManager getMediaSessionManager() {
    if (this.mMediaSessionManager == null) {
      Context context = this.mContext;
      this.mMediaSessionManager = (MediaSessionManager)context.getSystemService("media_session");
    } 
    return this.mMediaSessionManager;
  }
  
  void sendCloseSystemWindows() {
    PhoneWindow.sendCloseSystemWindows(this.mContext, null);
  }
  
  private void handleVolumeKeyEvent(KeyEvent paramKeyEvent) {
    getMediaSessionManager().dispatchVolumeKeyEventAsSystemService(paramKeyEvent, -2147483648);
  }
  
  private void handleMediaKeyEvent(KeyEvent paramKeyEvent) {
    getMediaSessionManager().dispatchMediaKeyEventAsSystemService(paramKeyEvent);
    if (InputLog.DEBUG) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(" dispatchMediaKeyEvent to AudioSerive , ");
      stringBuilder.append(paramKeyEvent);
      InputLog.d(str, stringBuilder.toString());
    } 
  }
  
  private boolean isUserSetupComplete() {
    ContentResolver contentResolver = this.mContext.getContentResolver();
    boolean bool = false;
    if (Settings.Secure.getInt(contentResolver, "user_setup_complete", 0) != 0)
      bool = true; 
    return bool;
  }
}
