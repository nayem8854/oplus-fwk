package com.android.internal.policy;

import android.content.AutofillOptions;
import android.content.ContentCaptureOptions;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.view.ContextThemeWrapper;
import android.view.Display;
import android.view.contentcapture.ContentCaptureManager;
import java.lang.ref.WeakReference;

public class DecorContext extends ContextThemeWrapper {
  private ContentCaptureManager mContentCaptureManager;
  
  private WeakReference<Context> mContext;
  
  private PhoneWindow mPhoneWindow;
  
  private Resources mResources;
  
  public DecorContext(Context paramContext, PhoneWindow paramPhoneWindow) {
    super(null, null);
    setPhoneWindow(paramPhoneWindow);
    Display display = paramPhoneWindow.getContext().getDisplayNoVerify();
    paramContext = paramContext.createDisplayContext(display);
    attachBaseContext(paramContext);
  }
  
  void setPhoneWindow(PhoneWindow paramPhoneWindow) {
    this.mPhoneWindow = paramPhoneWindow;
    Context context = paramPhoneWindow.getContext();
    this.mContext = new WeakReference<>(context);
    this.mResources = context.getResources();
  }
  
  public Object getSystemService(String paramString) {
    Object object;
    if ("window".equals(paramString))
      return this.mPhoneWindow.getWindowManager(); 
    Context context = this.mContext.get();
    if ("content_capture".equals(paramString)) {
      if (context != null && this.mContentCaptureManager == null)
        this.mContentCaptureManager = (ContentCaptureManager)context.getSystemService(paramString); 
      return this.mContentCaptureManager;
    } 
    if ("display".equals(paramString))
      return super.getSystemService(paramString); 
    if (context != null) {
      object = context.getSystemService(paramString);
    } else {
      object = super.getSystemService((String)object);
    } 
    return object;
  }
  
  public Resources getResources() {
    Context context = this.mContext.get();
    if (context != null)
      this.mResources = context.getResources(); 
    return this.mResources;
  }
  
  public AssetManager getAssets() {
    return this.mResources.getAssets();
  }
  
  public AutofillOptions getAutofillOptions() {
    Context context = this.mContext.get();
    if (context != null)
      return context.getAutofillOptions(); 
    return null;
  }
  
  public ContentCaptureOptions getContentCaptureOptions() {
    Context context = this.mContext.get();
    if (context != null)
      return context.getContentCaptureOptions(); 
    return null;
  }
  
  public boolean isUiContext() {
    Context context = this.mContext.get();
    if (context != null)
      return context.isUiContext(); 
    return false;
  }
}
