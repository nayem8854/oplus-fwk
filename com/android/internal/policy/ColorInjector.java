package com.android.internal.policy;

import android.content.Context;
import android.text.TextUtils;
import android.view.WindowManager;
import com.oplus.util.OplusDisplayCompatUtils;

class ColorInjector {
  static class PhoneWindow {
    static void generateLayoutInPhoneWindow(Context param1Context, WindowManager.LayoutParams param1LayoutParams) {
      String str = param1Context.getPackageName();
      if (!TextUtils.isEmpty(str) && !str.startsWith("android.server.cts") && !str.startsWith("android.server.am")) {
        boolean bool = OplusDisplayCompatUtils.getInstance().shouldNonImmersiveAdjustForPkg(str);
        if (!bool)
          param1LayoutParams.layoutInDisplayCutoutMode = 5; 
      } 
    }
  }
}
