package com.android.internal.policy;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IShortcutService extends IInterface {
  void notifyShortcutKeyPressed(long paramLong) throws RemoteException;
  
  class Default implements IShortcutService {
    public void notifyShortcutKeyPressed(long param1Long) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IShortcutService {
    private static final String DESCRIPTOR = "com.android.internal.policy.IShortcutService";
    
    static final int TRANSACTION_notifyShortcutKeyPressed = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.policy.IShortcutService");
    }
    
    public static IShortcutService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.policy.IShortcutService");
      if (iInterface != null && iInterface instanceof IShortcutService)
        return (IShortcutService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "notifyShortcutKeyPressed";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.policy.IShortcutService");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.policy.IShortcutService");
      long l = param1Parcel1.readLong();
      notifyShortcutKeyPressed(l);
      return true;
    }
    
    private static class Proxy implements IShortcutService {
      public static IShortcutService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.policy.IShortcutService";
      }
      
      public void notifyShortcutKeyPressed(long param2Long) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.policy.IShortcutService");
          parcel.writeLong(param2Long);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IShortcutService.Stub.getDefaultImpl() != null) {
            IShortcutService.Stub.getDefaultImpl().notifyShortcutKeyPressed(param2Long);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IShortcutService param1IShortcutService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IShortcutService != null) {
          Proxy.sDefaultImpl = param1IShortcutService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IShortcutService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
