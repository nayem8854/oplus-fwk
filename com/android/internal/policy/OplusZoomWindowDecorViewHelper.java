package com.android.internal.policy;

import android.app.WindowConfiguration;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.Slog;
import android.view.IWindow;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import com.android.internal.widget.ZoomWindowDecorView;

public class OplusZoomWindowDecorViewHelper {
  private static final int PAINT_FOCUS_ALPHA = 38;
  
  private static final int PAINT_NO_FOCUS_ALPHA = 13;
  
  private static final String SAFECENTER_PACKAGE = "com.coloros.safecenter";
  
  private static final String TAG = "OplusZoomWindowDecorViewHelper";
  
  private static final float ZOOM_NO_FOCUS_STROKE_WIDTH = 3.5F;
  
  private static final float ZOOM_STROKE_WIDTH = 3.5F;
  
  private Context mContext;
  
  private boolean mHasFocus;
  
  private Paint mPaint = new Paint();
  
  private String mPkgName;
  
  private float mRadius;
  
  private RectF mRectF = new RectF();
  
  public IWindow mWho;
  
  private PhoneWindow mWindow;
  
  private float mZoomStrokeWidthNofocus;
  
  private float mZoomStrokeWidthfocus;
  
  private ZoomWindowDecorView mZoomWindowDecorView;
  
  public OplusZoomWindowDecorViewHelper(Context paramContext, PhoneWindow paramPhoneWindow, String paramString) {
    this.mWindow = paramPhoneWindow;
    this.mContext = paramContext;
    this.mPkgName = paramString;
    this.mPaint.setColor(Color.parseColor("#999999"));
    this.mPaint.setStyle(Paint.Style.STROKE);
    this.mPaint.setAntiAlias(true);
    this.mZoomStrokeWidthfocus = dipToPixel(paramContext, 3.5F);
    this.mZoomStrokeWidthNofocus = dipToPixel(paramContext, 3.5F);
    this.mPaint.setStrokeWidth(this.mZoomStrokeWidthfocus);
    this.mPaint.setAlpha(38);
    Configuration configuration = paramContext.getResources().getConfiguration();
    setRectRadius(configuration);
  }
  
  public void setWindow(IWindow paramIWindow) {
    this.mWho = paramIWindow;
  }
  
  protected ZoomWindowDecorView createZoomWindowDecorView(LayoutInflater paramLayoutInflater, Configuration paramConfiguration) {
    boolean bool;
    if (this.mPkgName.equals("com.coloros.safecenter")) {
      Slog.w("OplusZoomWindowDecorViewHelper", "Failed to createZoomWindowDecorView: unsupport pkg");
      return null;
    } 
    WindowManager.LayoutParams layoutParams2 = null, layoutParams3 = null;
    WindowConfiguration windowConfiguration = paramConfiguration.windowConfiguration;
    WindowManager.LayoutParams layoutParams1 = this.mWindow.getAttributes();
    if (layoutParams1.type == 1 || layoutParams1.type == 2 || layoutParams1.type == 4) {
      bool = true;
    } else {
      bool = false;
    } 
    layoutParams1 = layoutParams2;
    if (bool) {
      layoutParams1 = layoutParams2;
      if (windowConfiguration.getWindowingMode() == 100) {
        layoutParams1 = layoutParams3;
        if (!false) {
          try {
            ZoomWindowDecorView zoomWindowDecorView = inflateZoomWindowDecorView(paramLayoutInflater);
            zoomWindowDecorView.setPhoneWindow(this.mWindow, true, this);
          } catch (Exception exception) {
            layoutParams1 = null;
            exception.printStackTrace();
          } 
          return (ZoomWindowDecorView)layoutParams1;
        } 
      } else {
        return (ZoomWindowDecorView)layoutParams1;
      } 
    } else {
      return (ZoomWindowDecorView)layoutParams1;
    } 
    layoutParams1.setPhoneWindow(this.mWindow, true, this);
  }
  
  void onResourcesLoaded(LayoutInflater paramLayoutInflater) {}
  
  protected void onConfigurationChanged(Configuration paramConfiguration) {
    boolean bool;
    if (this.mZoomWindowDecorView == null)
      return; 
    if (paramConfiguration.windowConfiguration.getWindowingMode() == 100) {
      bool = true;
    } else {
      bool = false;
    } 
    if (!bool)
      return; 
    setRectRadius(paramConfiguration);
  }
  
  private ZoomWindowDecorView inflateZoomWindowDecorView(LayoutInflater paramLayoutInflater) {
    paramLayoutInflater = LayoutInflater.from(this.mContext);
    ZoomWindowDecorView zoomWindowDecorView = (ZoomWindowDecorView)paramLayoutInflater.inflate(202440719, null);
    if (zoomWindowDecorView != null)
      zoomWindowDecorView.setForceDarkAllowed(false); 
    return zoomWindowDecorView;
  }
  
  public void setLastReportedMergedConfiguration(Configuration paramConfiguration) {
    boolean bool;
    if (paramConfiguration.windowConfiguration.getWindowingMode() == 100) {
      bool = true;
    } else {
      bool = false;
    } 
    if (this.mZoomWindowDecorView == null && bool) {
      LayoutInflater layoutInflater = this.mWindow.getLayoutInflater();
      this.mZoomWindowDecorView = createZoomWindowDecorView(layoutInflater, paramConfiguration);
    } else {
      ZoomWindowDecorView zoomWindowDecorView1 = this.mZoomWindowDecorView;
      if (zoomWindowDecorView1 != null)
        zoomWindowDecorView1.onConfigurationChanged(bool); 
    } 
    ZoomWindowDecorView zoomWindowDecorView = this.mZoomWindowDecorView;
    if (zoomWindowDecorView != null)
      zoomWindowDecorView.setForceDarkAllowed(false); 
  }
  
  protected void updateZoomWindowDecorStatus(Configuration paramConfiguration) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("updateZoomWindowDecorStatus config:");
    stringBuilder.append(paramConfiguration);
    Slog.v("OplusZoomWindowDecorViewHelper", stringBuilder.toString());
  }
  
  protected boolean remeasureZoomView(int paramInt1, int paramInt2) {
    boolean bool1 = false;
    ZoomWindowDecorView zoomWindowDecorView = this.mZoomWindowDecorView;
    boolean bool2 = bool1;
    if (zoomWindowDecorView != null) {
      View view = zoomWindowDecorView.getZoomDecor();
      bool2 = bool1;
      if (view != null) {
        bool2 = bool1;
        if (view.getVisibility() != 8) {
          this.mZoomWindowDecorView.measure(paramInt1, paramInt2);
          bool2 = true;
        } 
      } 
    } 
    return bool2;
  }
  
  protected void relayoutZoomView() {
    ZoomWindowDecorView zoomWindowDecorView = this.mZoomWindowDecorView;
    if (zoomWindowDecorView != null) {
      View view = zoomWindowDecorView.getZoomDecor();
      if (view != null && view.getVisibility() != 8) {
        ZoomWindowDecorView zoomWindowDecorView1 = this.mZoomWindowDecorView;
        zoomWindowDecorView1.layout(0, 0, zoomWindowDecorView1.getMeasuredWidth(), this.mZoomWindowDecorView.getMeasuredHeight());
      } 
    } 
  }
  
  protected void draw(Canvas paramCanvas) {
    ZoomWindowDecorView zoomWindowDecorView = this.mZoomWindowDecorView;
    if (zoomWindowDecorView != null) {
      View view = zoomWindowDecorView.getZoomDecor();
      if (view != null && view.getVisibility() != 8) {
        PhoneWindow phoneWindow = this.mWindow;
        if (phoneWindow != null && !phoneWindow.isFloating()) {
          this.mZoomWindowDecorView.draw(paramCanvas);
          this.mRectF.set(0.0F, 0.0F, this.mZoomWindowDecorView.getWidth(), this.mZoomWindowDecorView.getHeight());
          if (this.mHasFocus) {
            this.mPaint.setColor(Color.parseColor("#999999"));
            this.mPaint.setAlpha(38);
            this.mPaint.setStrokeWidth(this.mZoomStrokeWidthfocus);
          } else {
            this.mPaint.setColor(Color.parseColor("#999999"));
            this.mPaint.setAlpha(13);
            this.mPaint.setStrokeWidth(this.mZoomStrokeWidthNofocus);
          } 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("draw ZoomWindowDecorView : mRectF = ");
          stringBuilder.append(this.mRectF);
          stringBuilder.append("   mHasFocus = ");
          stringBuilder.append(this.mHasFocus);
          stringBuilder.append("   this = ");
          stringBuilder.append(this);
          Slog.d("OplusZoomWindowDecorViewHelper", stringBuilder.toString());
          RectF rectF = this.mRectF;
          float f1 = dipToPixel(this.mContext, this.mRadius);
          Context context = this.mContext;
          float f2 = this.mRadius;
          f2 = dipToPixel(context, f2);
          Paint paint = this.mPaint;
          paramCanvas.drawRoundRect(rectF, f1, f2, paint);
        } 
      } 
    } 
  }
  
  protected void onResourcesLoaded(LayoutInflater paramLayoutInflater, int paramInt) {}
  
  protected boolean dispatchTouchEvent(MotionEvent paramMotionEvent) {
    ZoomWindowDecorView zoomWindowDecorView = this.mZoomWindowDecorView;
    if (zoomWindowDecorView != null) {
      View view = zoomWindowDecorView.getZoomDecor();
      if (view != null && view.getVisibility() != 8 && 
        this.mZoomWindowDecorView.dispatchTouchEvent(paramMotionEvent))
        return true; 
    } 
    return false;
  }
  
  protected void onWindowFocusChangedByRoot(boolean paramBoolean) {
    if (this.mHasFocus != paramBoolean)
      this.mHasFocus = paramBoolean; 
    ZoomWindowDecorView zoomWindowDecorView = this.mZoomWindowDecorView;
    if (zoomWindowDecorView != null) {
      View view = zoomWindowDecorView.getZoomDecor();
      if (view != null && view.getVisibility() != 8)
        this.mZoomWindowDecorView.onWindowFocusChangedByRoot(paramBoolean); 
    } 
  }
  
  private static float dipToPixel(Context paramContext, float paramFloat) {
    if (paramContext != null) {
      float f = (paramContext.getResources().getDisplayMetrics()).density;
      return paramFloat * f;
    } 
    Slog.e("OplusZoomWindowDecorViewHelper", "Failed to dipToPixel: context is null");
    return 0.0F;
  }
  
  private void setRectRadius(Configuration paramConfiguration) {
    if (paramConfiguration == null)
      return; 
    if (paramConfiguration.orientation == 1) {
      this.mRadius = 22.0F;
    } else if (paramConfiguration.orientation == 2) {
      this.mRadius = 15.0F;
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(" orientation undefined, mRadius =");
      stringBuilder.append(this.mRadius);
      Slog.e("OplusZoomWindowDecorViewHelper", stringBuilder.toString());
    } 
  }
}
