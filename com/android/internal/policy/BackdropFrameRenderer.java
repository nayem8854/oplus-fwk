package com.android.internal.policy;

import android.common.OplusFeatureCache;
import android.graphics.Canvas;
import android.graphics.Insets;
import android.graphics.RecordingCanvas;
import android.graphics.Rect;
import android.graphics.RenderNode;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.Choreographer;
import android.view.ThreadedRenderer;
import android.view.View;
import com.oplus.darkmode.IOplusDarkModeManager;

public class BackdropFrameRenderer extends Thread implements Choreographer.FrameCallback {
  private final Rect mTargetRect = new Rect();
  
  private final Rect mOldTargetRect = new Rect();
  
  private final Rect mNewTargetRect = new Rect();
  
  private final Rect mOldSystemBarInsets = new Rect();
  
  private final Rect mSystemBarInsets = new Rect();
  
  private final Rect mTmpRect = new Rect();
  
  private Drawable mCaptionBackgroundDrawable;
  
  private Choreographer mChoreographer;
  
  private DecorView mDecorView;
  
  private RenderNode mFrameAndBackdropNode;
  
  private boolean mFullscreen;
  
  private int mLastCaptionHeight;
  
  private int mLastContentHeight;
  
  private int mLastContentWidth;
  
  private int mLastXOffset;
  
  private int mLastYOffset;
  
  private ColorDrawable mNavigationBarColor;
  
  private boolean mOldFullscreen;
  
  private ThreadedRenderer mRenderer;
  
  private boolean mReportNextDraw;
  
  private Drawable mResizingBackgroundDrawable;
  
  private ColorDrawable mStatusBarColor;
  
  private RenderNode mSystemBarBackgroundNode;
  
  private Drawable mUserCaptionBackgroundDrawable;
  
  public BackdropFrameRenderer(DecorView paramDecorView, ThreadedRenderer paramThreadedRenderer, Rect paramRect, Drawable paramDrawable1, Drawable paramDrawable2, Drawable paramDrawable3, int paramInt1, int paramInt2, boolean paramBoolean, Insets paramInsets) {
    setName("ResizeFrame");
    this.mRenderer = paramThreadedRenderer;
    onResourcesLoaded(paramDecorView, paramDrawable1, paramDrawable2, paramDrawable3, paramInt1, paramInt2);
    RenderNode renderNode = RenderNode.create("FrameAndBackdropNode", null);
    this.mRenderer.addRenderNode(renderNode, true);
    this.mTargetRect.set(paramRect);
    this.mFullscreen = paramBoolean;
    this.mOldFullscreen = paramBoolean;
    this.mSystemBarInsets.set(paramInsets.toRect());
    this.mOldSystemBarInsets.set(paramInsets.toRect());
    start();
  }
  
  void onResourcesLoaded(DecorView paramDecorView, Drawable paramDrawable1, Drawable paramDrawable2, Drawable paramDrawable3, int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: putfield mDecorView : Lcom/android/internal/policy/DecorView;
    //   7: aload_2
    //   8: ifnull -> 29
    //   11: aload_2
    //   12: invokevirtual getConstantState : ()Landroid/graphics/drawable/Drawable$ConstantState;
    //   15: ifnull -> 29
    //   18: aload_2
    //   19: invokevirtual getConstantState : ()Landroid/graphics/drawable/Drawable$ConstantState;
    //   22: invokevirtual newDrawable : ()Landroid/graphics/drawable/Drawable;
    //   25: astore_1
    //   26: goto -> 31
    //   29: aconst_null
    //   30: astore_1
    //   31: aload_0
    //   32: aload_1
    //   33: putfield mResizingBackgroundDrawable : Landroid/graphics/drawable/Drawable;
    //   36: aload_3
    //   37: ifnull -> 58
    //   40: aload_3
    //   41: invokevirtual getConstantState : ()Landroid/graphics/drawable/Drawable$ConstantState;
    //   44: ifnull -> 58
    //   47: aload_3
    //   48: invokevirtual getConstantState : ()Landroid/graphics/drawable/Drawable$ConstantState;
    //   51: invokevirtual newDrawable : ()Landroid/graphics/drawable/Drawable;
    //   54: astore_1
    //   55: goto -> 60
    //   58: aconst_null
    //   59: astore_1
    //   60: aload_0
    //   61: aload_1
    //   62: putfield mCaptionBackgroundDrawable : Landroid/graphics/drawable/Drawable;
    //   65: aload #4
    //   67: ifnull -> 90
    //   70: aload #4
    //   72: invokevirtual getConstantState : ()Landroid/graphics/drawable/Drawable$ConstantState;
    //   75: ifnull -> 90
    //   78: aload #4
    //   80: invokevirtual getConstantState : ()Landroid/graphics/drawable/Drawable$ConstantState;
    //   83: invokevirtual newDrawable : ()Landroid/graphics/drawable/Drawable;
    //   86: astore_1
    //   87: goto -> 92
    //   90: aconst_null
    //   91: astore_1
    //   92: aload_0
    //   93: aload_1
    //   94: putfield mUserCaptionBackgroundDrawable : Landroid/graphics/drawable/Drawable;
    //   97: aload_0
    //   98: getfield mCaptionBackgroundDrawable : Landroid/graphics/drawable/Drawable;
    //   101: ifnonnull -> 112
    //   104: aload_0
    //   105: aload_0
    //   106: getfield mResizingBackgroundDrawable : Landroid/graphics/drawable/Drawable;
    //   109: putfield mCaptionBackgroundDrawable : Landroid/graphics/drawable/Drawable;
    //   112: iload #5
    //   114: ifeq -> 139
    //   117: new android/graphics/drawable/ColorDrawable
    //   120: astore_1
    //   121: aload_1
    //   122: iload #5
    //   124: invokespecial <init> : (I)V
    //   127: aload_0
    //   128: aload_1
    //   129: putfield mStatusBarColor : Landroid/graphics/drawable/ColorDrawable;
    //   132: aload_0
    //   133: invokespecial addSystemBarNodeIfNeeded : ()V
    //   136: goto -> 144
    //   139: aload_0
    //   140: aconst_null
    //   141: putfield mStatusBarColor : Landroid/graphics/drawable/ColorDrawable;
    //   144: iload #6
    //   146: ifeq -> 171
    //   149: new android/graphics/drawable/ColorDrawable
    //   152: astore_1
    //   153: aload_1
    //   154: iload #6
    //   156: invokespecial <init> : (I)V
    //   159: aload_0
    //   160: aload_1
    //   161: putfield mNavigationBarColor : Landroid/graphics/drawable/ColorDrawable;
    //   164: aload_0
    //   165: invokespecial addSystemBarNodeIfNeeded : ()V
    //   168: goto -> 176
    //   171: aload_0
    //   172: aconst_null
    //   173: putfield mNavigationBarColor : Landroid/graphics/drawable/ColorDrawable;
    //   176: aload_0
    //   177: monitorexit
    //   178: return
    //   179: astore_1
    //   180: aload_0
    //   181: monitorexit
    //   182: aload_1
    //   183: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #112	-> 0
    //   #113	-> 2
    //   #114	-> 7
    //   #115	-> 7
    //   #116	-> 18
    //   #117	-> 29
    //   #118	-> 36
    //   #119	-> 36
    //   #120	-> 47
    //   #121	-> 58
    //   #122	-> 65
    //   #123	-> 65
    //   #124	-> 78
    //   #125	-> 90
    //   #126	-> 97
    //   #127	-> 104
    //   #129	-> 112
    //   #130	-> 117
    //   #131	-> 132
    //   #133	-> 139
    //   #135	-> 144
    //   #136	-> 149
    //   #137	-> 164
    //   #139	-> 171
    //   #141	-> 176
    //   #142	-> 178
    //   #141	-> 179
    // Exception table:
    //   from	to	target	type
    //   2	7	179	finally
    //   11	18	179	finally
    //   18	26	179	finally
    //   31	36	179	finally
    //   40	47	179	finally
    //   47	55	179	finally
    //   60	65	179	finally
    //   70	78	179	finally
    //   78	87	179	finally
    //   92	97	179	finally
    //   97	104	179	finally
    //   104	112	179	finally
    //   117	132	179	finally
    //   132	136	179	finally
    //   139	144	179	finally
    //   149	164	179	finally
    //   164	168	179	finally
    //   171	176	179	finally
    //   176	178	179	finally
    //   180	182	179	finally
  }
  
  private void addSystemBarNodeIfNeeded() {
    if (this.mSystemBarBackgroundNode != null)
      return; 
    RenderNode renderNode = RenderNode.create("SystemBarBackgroundNode", null);
    this.mRenderer.addRenderNode(renderNode, false);
  }
  
  public void setTargetRect(Rect paramRect1, boolean paramBoolean, Rect paramRect2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iload_2
    //   4: putfield mFullscreen : Z
    //   7: aload_0
    //   8: getfield mTargetRect : Landroid/graphics/Rect;
    //   11: aload_1
    //   12: invokevirtual set : (Landroid/graphics/Rect;)V
    //   15: aload_0
    //   16: getfield mSystemBarInsets : Landroid/graphics/Rect;
    //   19: aload_3
    //   20: invokevirtual set : (Landroid/graphics/Rect;)V
    //   23: aload_0
    //   24: iconst_0
    //   25: invokespecial pingRenderLocked : (Z)V
    //   28: aload_0
    //   29: monitorexit
    //   30: return
    //   31: astore_1
    //   32: aload_0
    //   33: monitorexit
    //   34: aload_1
    //   35: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #162	-> 0
    //   #163	-> 2
    //   #164	-> 7
    //   #165	-> 15
    //   #167	-> 23
    //   #168	-> 28
    //   #169	-> 30
    //   #168	-> 31
    // Exception table:
    //   from	to	target	type
    //   2	7	31	finally
    //   7	15	31	finally
    //   15	23	31	finally
    //   23	28	31	finally
    //   28	30	31	finally
    //   32	34	31	finally
  }
  
  public void onConfigurationChange() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mRenderer : Landroid/view/ThreadedRenderer;
    //   6: ifnull -> 25
    //   9: aload_0
    //   10: getfield mOldTargetRect : Landroid/graphics/Rect;
    //   13: iconst_0
    //   14: iconst_0
    //   15: iconst_0
    //   16: iconst_0
    //   17: invokevirtual set : (IIII)V
    //   20: aload_0
    //   21: iconst_0
    //   22: invokespecial pingRenderLocked : (Z)V
    //   25: aload_0
    //   26: monitorexit
    //   27: return
    //   28: astore_1
    //   29: aload_0
    //   30: monitorexit
    //   31: aload_1
    //   32: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #175	-> 0
    //   #176	-> 2
    //   #178	-> 9
    //   #179	-> 20
    //   #181	-> 25
    //   #182	-> 27
    //   #181	-> 28
    // Exception table:
    //   from	to	target	type
    //   2	9	28	finally
    //   9	20	28	finally
    //   20	25	28	finally
    //   25	27	28	finally
    //   29	31	28	finally
  }
  
  void releaseRenderer() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mRenderer : Landroid/view/ThreadedRenderer;
    //   6: ifnull -> 59
    //   9: aload_0
    //   10: getfield mRenderer : Landroid/view/ThreadedRenderer;
    //   13: iconst_0
    //   14: iconst_0
    //   15: iconst_0
    //   16: iconst_0
    //   17: invokevirtual setContentDrawBounds : (IIII)V
    //   20: aload_0
    //   21: getfield mRenderer : Landroid/view/ThreadedRenderer;
    //   24: aload_0
    //   25: getfield mFrameAndBackdropNode : Landroid/graphics/RenderNode;
    //   28: invokevirtual removeRenderNode : (Landroid/graphics/RenderNode;)V
    //   31: aload_0
    //   32: getfield mSystemBarBackgroundNode : Landroid/graphics/RenderNode;
    //   35: ifnull -> 49
    //   38: aload_0
    //   39: getfield mRenderer : Landroid/view/ThreadedRenderer;
    //   42: aload_0
    //   43: getfield mSystemBarBackgroundNode : Landroid/graphics/RenderNode;
    //   46: invokevirtual removeRenderNode : (Landroid/graphics/RenderNode;)V
    //   49: aload_0
    //   50: aconst_null
    //   51: putfield mRenderer : Landroid/view/ThreadedRenderer;
    //   54: aload_0
    //   55: iconst_0
    //   56: invokespecial pingRenderLocked : (Z)V
    //   59: aload_0
    //   60: monitorexit
    //   61: return
    //   62: astore_1
    //   63: aload_0
    //   64: monitorexit
    //   65: aload_1
    //   66: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #189	-> 0
    //   #190	-> 2
    //   #192	-> 9
    //   #196	-> 20
    //   #197	-> 31
    //   #198	-> 38
    //   #201	-> 49
    //   #204	-> 54
    //   #206	-> 59
    //   #207	-> 61
    //   #206	-> 62
    // Exception table:
    //   from	to	target	type
    //   2	9	62	finally
    //   9	20	62	finally
    //   20	31	62	finally
    //   31	38	62	finally
    //   38	49	62	finally
    //   49	54	62	finally
    //   54	59	62	finally
    //   59	61	62	finally
    //   63	65	62	finally
  }
  
  public void run() {
    // Byte code:
    //   0: invokestatic prepare : ()V
    //   3: aload_0
    //   4: monitorenter
    //   5: aload_0
    //   6: invokestatic getInstance : ()Landroid/view/Choreographer;
    //   9: putfield mChoreographer : Landroid/view/Choreographer;
    //   12: aload_0
    //   13: monitorexit
    //   14: invokestatic loop : ()V
    //   17: aload_0
    //   18: invokevirtual releaseRenderer : ()V
    //   21: aload_0
    //   22: monitorenter
    //   23: aload_0
    //   24: aconst_null
    //   25: putfield mChoreographer : Landroid/view/Choreographer;
    //   28: invokestatic releaseInstance : ()V
    //   31: aload_0
    //   32: monitorexit
    //   33: return
    //   34: astore_1
    //   35: aload_0
    //   36: monitorexit
    //   37: aload_1
    //   38: athrow
    //   39: astore_1
    //   40: aload_0
    //   41: monitorexit
    //   42: aload_1
    //   43: athrow
    //   44: astore_1
    //   45: aload_0
    //   46: invokevirtual releaseRenderer : ()V
    //   49: aload_1
    //   50: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #212	-> 0
    //   #213	-> 3
    //   #214	-> 5
    //   #215	-> 12
    //   #216	-> 14
    //   #218	-> 17
    //   #219	-> 21
    //   #220	-> 21
    //   #222	-> 23
    //   #223	-> 28
    //   #224	-> 31
    //   #225	-> 33
    //   #224	-> 34
    //   #215	-> 39
    //   #218	-> 44
    //   #219	-> 49
    // Exception table:
    //   from	to	target	type
    //   0	3	44	finally
    //   3	5	44	finally
    //   5	12	39	finally
    //   12	14	39	finally
    //   14	17	44	finally
    //   23	28	34	finally
    //   28	31	34	finally
    //   31	33	34	finally
    //   35	37	34	finally
    //   40	42	39	finally
    //   42	44	44	finally
  }
  
  public void doFrame(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mRenderer : Landroid/view/ThreadedRenderer;
    //   6: ifnonnull -> 22
    //   9: aload_0
    //   10: invokespecial reportDrawIfNeeded : ()V
    //   13: invokestatic myLooper : ()Landroid/os/Looper;
    //   16: invokevirtual quit : ()V
    //   19: aload_0
    //   20: monitorexit
    //   21: return
    //   22: aload_0
    //   23: invokespecial doFrameUncheckedLocked : ()V
    //   26: aload_0
    //   27: monitorexit
    //   28: return
    //   29: astore_3
    //   30: aload_0
    //   31: monitorexit
    //   32: aload_3
    //   33: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #234	-> 0
    //   #235	-> 2
    //   #236	-> 9
    //   #238	-> 13
    //   #239	-> 19
    //   #241	-> 22
    //   #242	-> 26
    //   #243	-> 28
    //   #242	-> 29
    // Exception table:
    //   from	to	target	type
    //   2	9	29	finally
    //   9	13	29	finally
    //   13	19	29	finally
    //   19	21	29	finally
    //   22	26	29	finally
    //   26	28	29	finally
    //   30	32	29	finally
  }
  
  private void doFrameUncheckedLocked() {
    this.mNewTargetRect.set(this.mTargetRect);
    if (this.mNewTargetRect.equals(this.mOldTargetRect) && this.mOldFullscreen == this.mFullscreen) {
      Rect rect1 = this.mSystemBarInsets, rect2 = this.mOldSystemBarInsets;
      if (!rect1.equals(rect2) || this.mReportNextDraw) {
        this.mOldFullscreen = this.mFullscreen;
        this.mOldTargetRect.set(this.mNewTargetRect);
        this.mOldSystemBarInsets.set(this.mSystemBarInsets);
        redrawLocked(this.mNewTargetRect, this.mFullscreen);
        return;
      } 
      return;
    } 
    this.mOldFullscreen = this.mFullscreen;
    this.mOldTargetRect.set(this.mNewTargetRect);
    this.mOldSystemBarInsets.set(this.mSystemBarInsets);
    redrawLocked(this.mNewTargetRect, this.mFullscreen);
  }
  
  boolean onContentDrawn(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mLastContentWidth : I
    //   6: istore #5
    //   8: iconst_1
    //   9: istore #6
    //   11: iload #5
    //   13: ifne -> 22
    //   16: iconst_1
    //   17: istore #5
    //   19: goto -> 25
    //   22: iconst_0
    //   23: istore #5
    //   25: aload_0
    //   26: iload_3
    //   27: putfield mLastContentWidth : I
    //   30: iload #4
    //   32: aload_0
    //   33: getfield mLastCaptionHeight : I
    //   36: isub
    //   37: istore #4
    //   39: aload_0
    //   40: iload #4
    //   42: putfield mLastContentHeight : I
    //   45: aload_0
    //   46: iload_1
    //   47: putfield mLastXOffset : I
    //   50: aload_0
    //   51: iload_2
    //   52: putfield mLastYOffset : I
    //   55: aload_0
    //   56: getfield mRenderer : Landroid/view/ThreadedRenderer;
    //   59: iload_1
    //   60: iload_2
    //   61: iload_1
    //   62: iload_3
    //   63: iadd
    //   64: aload_0
    //   65: getfield mLastCaptionHeight : I
    //   68: iload_2
    //   69: iadd
    //   70: iload #4
    //   72: iadd
    //   73: invokevirtual setContentDrawBounds : (IIII)V
    //   76: iload #5
    //   78: ifeq -> 105
    //   81: aload_0
    //   82: getfield mLastCaptionHeight : I
    //   85: ifne -> 102
    //   88: aload_0
    //   89: getfield mDecorView : Lcom/android/internal/policy/DecorView;
    //   92: astore #7
    //   94: aload #7
    //   96: invokevirtual isShowingCaption : ()Z
    //   99: ifne -> 105
    //   102: goto -> 108
    //   105: iconst_0
    //   106: istore #6
    //   108: aload_0
    //   109: monitorexit
    //   110: iload #6
    //   112: ireturn
    //   113: astore #7
    //   115: aload_0
    //   116: monitorexit
    //   117: aload #7
    //   119: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #269	-> 0
    //   #270	-> 2
    //   #272	-> 25
    //   #273	-> 30
    //   #274	-> 45
    //   #275	-> 50
    //   #278	-> 55
    //   #286	-> 76
    //   #287	-> 94
    //   #286	-> 110
    //   #288	-> 113
    // Exception table:
    //   from	to	target	type
    //   2	8	113	finally
    //   25	30	113	finally
    //   30	45	113	finally
    //   45	50	113	finally
    //   50	55	113	finally
    //   55	76	113	finally
    //   81	94	113	finally
    //   94	102	113	finally
    //   108	110	113	finally
    //   115	117	113	finally
  }
  
  void onRequestDraw(boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iload_1
    //   4: putfield mReportNextDraw : Z
    //   7: aload_0
    //   8: getfield mOldTargetRect : Landroid/graphics/Rect;
    //   11: iconst_0
    //   12: iconst_0
    //   13: iconst_0
    //   14: iconst_0
    //   15: invokevirtual set : (IIII)V
    //   18: aload_0
    //   19: iconst_1
    //   20: invokespecial pingRenderLocked : (Z)V
    //   23: aload_0
    //   24: monitorexit
    //   25: return
    //   26: astore_2
    //   27: aload_0
    //   28: monitorexit
    //   29: aload_2
    //   30: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #292	-> 0
    //   #293	-> 2
    //   #294	-> 7
    //   #295	-> 18
    //   #296	-> 23
    //   #297	-> 25
    //   #296	-> 26
    // Exception table:
    //   from	to	target	type
    //   2	7	26	finally
    //   7	18	26	finally
    //   18	23	26	finally
    //   23	25	26	finally
    //   27	29	26	finally
  }
  
  private void redrawLocked(Rect paramRect, boolean paramBoolean) {
    int i = this.mDecorView.getCaptionHeight();
    if (i != 0)
      this.mLastCaptionHeight = i; 
    if ((this.mLastCaptionHeight == 0 && this.mDecorView.isShowingCaption()) || this.mLastContentWidth == 0 || this.mLastContentHeight == 0)
      return; 
    i = this.mLastXOffset + paramRect.left;
    int j = this.mLastYOffset + paramRect.top;
    int k = paramRect.width();
    int m = paramRect.height();
    this.mFrameAndBackdropNode.setLeftTopRightBottom(i, j, i + k, j + m);
    RecordingCanvas recordingCanvas = this.mFrameAndBackdropNode.beginRecording(k, m);
    Drawable drawable = this.mUserCaptionBackgroundDrawable;
    if (drawable == null)
      drawable = this.mCaptionBackgroundDrawable; 
    if (drawable != null) {
      drawable.setBounds(0, 0, i + k, this.mLastCaptionHeight + j);
      drawable.draw((Canvas)recordingCanvas);
    } 
    if (((this.mDecorView.getResources().getConfiguration()).uiMode & 0x30) == 32) {
      if (this.mResizingBackgroundDrawable != null)
        this.mFrameAndBackdropNode.setForceDarkAllowed(false); 
      RenderNode renderNode = this.mSystemBarBackgroundNode;
      if (renderNode != null && this.mStatusBarColor != null && this.mNavigationBarColor != null)
        renderNode.setForceDarkAllowed(false); 
    } 
    if (this.mResizingBackgroundDrawable != null && (
      (IOplusDarkModeManager)OplusFeatureCache.getOrCreate(IOplusDarkModeManager.DEFAULT, new Object[0])).darkenSplitScreenDrawable((View)this.mDecorView, this.mResizingBackgroundDrawable, 0, this.mLastCaptionHeight, i + k, j + m, recordingCanvas)) {
      this.mResizingBackgroundDrawable.setBounds(0, this.mLastCaptionHeight, i + k, j + m);
      this.mResizingBackgroundDrawable.draw((Canvas)recordingCanvas);
    } 
    this.mFrameAndBackdropNode.endRecording();
    drawColorViews(i, j, k, m, paramBoolean);
    this.mRenderer.drawRenderNode(this.mFrameAndBackdropNode);
    reportDrawIfNeeded();
  }
  
  private void drawColorViews(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean) {
    RenderNode renderNode = this.mSystemBarBackgroundNode;
    if (renderNode == null)
      return; 
    RecordingCanvas recordingCanvas = renderNode.beginRecording(paramInt3, paramInt4);
    this.mSystemBarBackgroundNode.setLeftTopRightBottom(paramInt1, paramInt2, paramInt1 + paramInt3, paramInt2 + paramInt4);
    paramInt2 = this.mSystemBarInsets.top;
    ColorDrawable colorDrawable = this.mStatusBarColor;
    if (colorDrawable != null) {
      colorDrawable.setBounds(0, 0, paramInt1 + paramInt3, paramInt2);
      this.mStatusBarColor.draw((Canvas)recordingCanvas);
    } 
    if (this.mNavigationBarColor != null && paramBoolean) {
      DecorView.getNavigationBarRect(paramInt3, paramInt4, this.mSystemBarInsets, this.mTmpRect, 1.0F);
      this.mNavigationBarColor.setBounds(this.mTmpRect);
      this.mNavigationBarColor.draw((Canvas)recordingCanvas);
    } 
    this.mSystemBarBackgroundNode.endRecording();
    this.mRenderer.drawRenderNode(this.mSystemBarBackgroundNode);
  }
  
  private void reportDrawIfNeeded() {
    if (this.mReportNextDraw) {
      if (this.mDecorView.isAttachedToWindow())
        this.mDecorView.getViewRootImpl().reportDrawFinish(); 
      this.mReportNextDraw = false;
    } 
  }
  
  private void pingRenderLocked(boolean paramBoolean) {
    Choreographer choreographer = this.mChoreographer;
    if (choreographer != null && !paramBoolean) {
      choreographer.postFrameCallback(this);
    } else {
      doFrameUncheckedLocked();
    } 
  }
  
  void setUserCaptionBackgroundDrawable(Drawable paramDrawable) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: putfield mUserCaptionBackgroundDrawable : Landroid/graphics/drawable/Drawable;
    //   7: aload_0
    //   8: monitorexit
    //   9: return
    //   10: astore_1
    //   11: aload_0
    //   12: monitorexit
    //   13: aload_1
    //   14: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #434	-> 0
    //   #435	-> 2
    //   #436	-> 7
    //   #437	-> 9
    //   #436	-> 10
    // Exception table:
    //   from	to	target	type
    //   2	7	10	finally
    //   7	9	10	finally
    //   11	13	10	finally
  }
}
