package com.android.internal.policy;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IKeyguardDismissCallback extends IInterface {
  void onDismissCancelled() throws RemoteException;
  
  void onDismissError() throws RemoteException;
  
  void onDismissSucceeded() throws RemoteException;
  
  class Default implements IKeyguardDismissCallback {
    public void onDismissError() throws RemoteException {}
    
    public void onDismissSucceeded() throws RemoteException {}
    
    public void onDismissCancelled() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IKeyguardDismissCallback {
    private static final String DESCRIPTOR = "com.android.internal.policy.IKeyguardDismissCallback";
    
    static final int TRANSACTION_onDismissCancelled = 3;
    
    static final int TRANSACTION_onDismissError = 1;
    
    static final int TRANSACTION_onDismissSucceeded = 2;
    
    public Stub() {
      attachInterface(this, "com.android.internal.policy.IKeyguardDismissCallback");
    }
    
    public static IKeyguardDismissCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.policy.IKeyguardDismissCallback");
      if (iInterface != null && iInterface instanceof IKeyguardDismissCallback)
        return (IKeyguardDismissCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "onDismissCancelled";
        } 
        return "onDismissSucceeded";
      } 
      return "onDismissError";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("com.android.internal.policy.IKeyguardDismissCallback");
            return true;
          } 
          param1Parcel1.enforceInterface("com.android.internal.policy.IKeyguardDismissCallback");
          onDismissCancelled();
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.internal.policy.IKeyguardDismissCallback");
        onDismissSucceeded();
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.policy.IKeyguardDismissCallback");
      onDismissError();
      return true;
    }
    
    private static class Proxy implements IKeyguardDismissCallback {
      public static IKeyguardDismissCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.policy.IKeyguardDismissCallback";
      }
      
      public void onDismissError() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.policy.IKeyguardDismissCallback");
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IKeyguardDismissCallback.Stub.getDefaultImpl() != null) {
            IKeyguardDismissCallback.Stub.getDefaultImpl().onDismissError();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDismissSucceeded() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.policy.IKeyguardDismissCallback");
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IKeyguardDismissCallback.Stub.getDefaultImpl() != null) {
            IKeyguardDismissCallback.Stub.getDefaultImpl().onDismissSucceeded();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDismissCancelled() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.policy.IKeyguardDismissCallback");
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IKeyguardDismissCallback.Stub.getDefaultImpl() != null) {
            IKeyguardDismissCallback.Stub.getDefaultImpl().onDismissCancelled();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IKeyguardDismissCallback param1IKeyguardDismissCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IKeyguardDismissCallback != null) {
          Proxy.sDefaultImpl = param1IKeyguardDismissCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IKeyguardDismissCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
