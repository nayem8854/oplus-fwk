package com.android.internal.compat;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOverrideValidator extends IInterface {
  OverrideAllowedState getOverrideAllowedState(long paramLong, String paramString) throws RemoteException;
  
  class Default implements IOverrideValidator {
    public OverrideAllowedState getOverrideAllowedState(long param1Long, String param1String) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOverrideValidator {
    private static final String DESCRIPTOR = "com.android.internal.compat.IOverrideValidator";
    
    static final int TRANSACTION_getOverrideAllowedState = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.compat.IOverrideValidator");
    }
    
    public static IOverrideValidator asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.compat.IOverrideValidator");
      if (iInterface != null && iInterface instanceof IOverrideValidator)
        return (IOverrideValidator)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "getOverrideAllowedState";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.compat.IOverrideValidator");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.compat.IOverrideValidator");
      long l = param1Parcel1.readLong();
      String str = param1Parcel1.readString();
      OverrideAllowedState overrideAllowedState = getOverrideAllowedState(l, str);
      param1Parcel2.writeNoException();
      if (overrideAllowedState != null) {
        param1Parcel2.writeInt(1);
        overrideAllowedState.writeToParcel(param1Parcel2, 1);
      } else {
        param1Parcel2.writeInt(0);
      } 
      return true;
    }
    
    private static class Proxy implements IOverrideValidator {
      public static IOverrideValidator sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.compat.IOverrideValidator";
      }
      
      public OverrideAllowedState getOverrideAllowedState(long param2Long, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.compat.IOverrideValidator");
          parcel1.writeLong(param2Long);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOverrideValidator.Stub.getDefaultImpl() != null)
            return IOverrideValidator.Stub.getDefaultImpl().getOverrideAllowedState(param2Long, param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            OverrideAllowedState overrideAllowedState = (OverrideAllowedState)OverrideAllowedState.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (OverrideAllowedState)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOverrideValidator param1IOverrideValidator) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOverrideValidator != null) {
          Proxy.sDefaultImpl = param1IOverrideValidator;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOverrideValidator getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
