package com.android.internal.compat;

import android.content.pm.ApplicationInfo;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public interface IPlatformCompat extends IInterface {
  boolean clearOverride(long paramLong, String paramString) throws RemoteException;
  
  void clearOverrides(String paramString) throws RemoteException;
  
  void clearOverridesForTest(String paramString) throws RemoteException;
  
  int disableTargetSdkChanges(String paramString, int paramInt) throws RemoteException;
  
  int enableTargetSdkChanges(String paramString, int paramInt) throws RemoteException;
  
  CompatibilityChangeConfig getAppConfig(ApplicationInfo paramApplicationInfo) throws RemoteException;
  
  IOverrideValidator getOverrideValidator() throws RemoteException;
  
  boolean isChangeEnabled(long paramLong, ApplicationInfo paramApplicationInfo) throws RemoteException;
  
  boolean isChangeEnabledByPackageName(long paramLong, String paramString, int paramInt) throws RemoteException;
  
  boolean isChangeEnabledByUid(long paramLong, int paramInt) throws RemoteException;
  
  CompatibilityChangeInfo[] listAllChanges() throws RemoteException;
  
  CompatibilityChangeInfo[] listUIChanges() throws RemoteException;
  
  void reportChange(long paramLong, ApplicationInfo paramApplicationInfo) throws RemoteException;
  
  void reportChangeByPackageName(long paramLong, String paramString, int paramInt) throws RemoteException;
  
  void reportChangeByUid(long paramLong, int paramInt) throws RemoteException;
  
  void setOverrides(CompatibilityChangeConfig paramCompatibilityChangeConfig, String paramString) throws RemoteException;
  
  void setOverridesForTest(CompatibilityChangeConfig paramCompatibilityChangeConfig, String paramString) throws RemoteException;
  
  class Default implements IPlatformCompat {
    public void reportChange(long param1Long, ApplicationInfo param1ApplicationInfo) throws RemoteException {}
    
    public void reportChangeByPackageName(long param1Long, String param1String, int param1Int) throws RemoteException {}
    
    public void reportChangeByUid(long param1Long, int param1Int) throws RemoteException {}
    
    public boolean isChangeEnabled(long param1Long, ApplicationInfo param1ApplicationInfo) throws RemoteException {
      return false;
    }
    
    public boolean isChangeEnabledByPackageName(long param1Long, String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isChangeEnabledByUid(long param1Long, int param1Int) throws RemoteException {
      return false;
    }
    
    public void setOverrides(CompatibilityChangeConfig param1CompatibilityChangeConfig, String param1String) throws RemoteException {}
    
    public void setOverridesForTest(CompatibilityChangeConfig param1CompatibilityChangeConfig, String param1String) throws RemoteException {}
    
    public boolean clearOverride(long param1Long, String param1String) throws RemoteException {
      return false;
    }
    
    public int enableTargetSdkChanges(String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public int disableTargetSdkChanges(String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public void clearOverrides(String param1String) throws RemoteException {}
    
    public void clearOverridesForTest(String param1String) throws RemoteException {}
    
    public CompatibilityChangeConfig getAppConfig(ApplicationInfo param1ApplicationInfo) throws RemoteException {
      return null;
    }
    
    public CompatibilityChangeInfo[] listAllChanges() throws RemoteException {
      return null;
    }
    
    public CompatibilityChangeInfo[] listUIChanges() throws RemoteException {
      return null;
    }
    
    public IOverrideValidator getOverrideValidator() throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPlatformCompat {
    private static final String DESCRIPTOR = "com.android.internal.compat.IPlatformCompat";
    
    static final int TRANSACTION_clearOverride = 9;
    
    static final int TRANSACTION_clearOverrides = 12;
    
    static final int TRANSACTION_clearOverridesForTest = 13;
    
    static final int TRANSACTION_disableTargetSdkChanges = 11;
    
    static final int TRANSACTION_enableTargetSdkChanges = 10;
    
    static final int TRANSACTION_getAppConfig = 14;
    
    static final int TRANSACTION_getOverrideValidator = 17;
    
    static final int TRANSACTION_isChangeEnabled = 4;
    
    static final int TRANSACTION_isChangeEnabledByPackageName = 5;
    
    static final int TRANSACTION_isChangeEnabledByUid = 6;
    
    static final int TRANSACTION_listAllChanges = 15;
    
    static final int TRANSACTION_listUIChanges = 16;
    
    static final int TRANSACTION_reportChange = 1;
    
    static final int TRANSACTION_reportChangeByPackageName = 2;
    
    static final int TRANSACTION_reportChangeByUid = 3;
    
    static final int TRANSACTION_setOverrides = 7;
    
    static final int TRANSACTION_setOverridesForTest = 8;
    
    public Stub() {
      attachInterface(this, "com.android.internal.compat.IPlatformCompat");
    }
    
    public static IPlatformCompat asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.compat.IPlatformCompat");
      if (iInterface != null && iInterface instanceof IPlatformCompat)
        return (IPlatformCompat)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 17:
          return "getOverrideValidator";
        case 16:
          return "listUIChanges";
        case 15:
          return "listAllChanges";
        case 14:
          return "getAppConfig";
        case 13:
          return "clearOverridesForTest";
        case 12:
          return "clearOverrides";
        case 11:
          return "disableTargetSdkChanges";
        case 10:
          return "enableTargetSdkChanges";
        case 9:
          return "clearOverride";
        case 8:
          return "setOverridesForTest";
        case 7:
          return "setOverrides";
        case 6:
          return "isChangeEnabledByUid";
        case 5:
          return "isChangeEnabledByPackageName";
        case 4:
          return "isChangeEnabled";
        case 3:
          return "reportChangeByUid";
        case 2:
          return "reportChangeByPackageName";
        case 1:
          break;
      } 
      return "reportChange";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        int i;
        IOverrideValidator iOverrideValidator;
        CompatibilityChangeInfo[] arrayOfCompatibilityChangeInfo;
        CompatibilityChangeConfig compatibilityChangeConfig;
        String str1, str2;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 17:
            param1Parcel1.enforceInterface("com.android.internal.compat.IPlatformCompat");
            iOverrideValidator = getOverrideValidator();
            param1Parcel2.writeNoException();
            if (iOverrideValidator != null) {
              IBinder iBinder = iOverrideValidator.asBinder();
            } else {
              iOverrideValidator = null;
            } 
            param1Parcel2.writeStrongBinder((IBinder)iOverrideValidator);
            return true;
          case 16:
            iOverrideValidator.enforceInterface("com.android.internal.compat.IPlatformCompat");
            arrayOfCompatibilityChangeInfo = listUIChanges();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray((Parcelable[])arrayOfCompatibilityChangeInfo, 1);
            return true;
          case 15:
            arrayOfCompatibilityChangeInfo.enforceInterface("com.android.internal.compat.IPlatformCompat");
            arrayOfCompatibilityChangeInfo = listAllChanges();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray((Parcelable[])arrayOfCompatibilityChangeInfo, 1);
            return true;
          case 14:
            arrayOfCompatibilityChangeInfo.enforceInterface("com.android.internal.compat.IPlatformCompat");
            if (arrayOfCompatibilityChangeInfo.readInt() != 0) {
              ApplicationInfo applicationInfo = (ApplicationInfo)ApplicationInfo.CREATOR.createFromParcel((Parcel)arrayOfCompatibilityChangeInfo);
            } else {
              arrayOfCompatibilityChangeInfo = null;
            } 
            compatibilityChangeConfig = getAppConfig((ApplicationInfo)arrayOfCompatibilityChangeInfo);
            param1Parcel2.writeNoException();
            if (compatibilityChangeConfig != null) {
              param1Parcel2.writeInt(1);
              compatibilityChangeConfig.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 13:
            compatibilityChangeConfig.enforceInterface("com.android.internal.compat.IPlatformCompat");
            str1 = compatibilityChangeConfig.readString();
            clearOverridesForTest(str1);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            str1.enforceInterface("com.android.internal.compat.IPlatformCompat");
            str1 = str1.readString();
            clearOverrides(str1);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            str1.enforceInterface("com.android.internal.compat.IPlatformCompat");
            str2 = str1.readString();
            param1Int1 = str1.readInt();
            param1Int1 = disableTargetSdkChanges(str2, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 10:
            str1.enforceInterface("com.android.internal.compat.IPlatformCompat");
            str2 = str1.readString();
            param1Int1 = str1.readInt();
            param1Int1 = enableTargetSdkChanges(str2, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 9:
            str1.enforceInterface("com.android.internal.compat.IPlatformCompat");
            l = str1.readLong();
            str1 = str1.readString();
            bool3 = clearOverride(l, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 8:
            str1.enforceInterface("com.android.internal.compat.IPlatformCompat");
            if (str1.readInt() != 0) {
              CompatibilityChangeConfig compatibilityChangeConfig1 = (CompatibilityChangeConfig)CompatibilityChangeConfig.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str2 = null;
            } 
            str1 = str1.readString();
            setOverridesForTest((CompatibilityChangeConfig)str2, str1);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            str1.enforceInterface("com.android.internal.compat.IPlatformCompat");
            if (str1.readInt() != 0) {
              CompatibilityChangeConfig compatibilityChangeConfig1 = (CompatibilityChangeConfig)CompatibilityChangeConfig.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str2 = null;
            } 
            str1 = str1.readString();
            setOverrides((CompatibilityChangeConfig)str2, str1);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            str1.enforceInterface("com.android.internal.compat.IPlatformCompat");
            l = str1.readLong();
            k = str1.readInt();
            bool2 = isChangeEnabledByUid(l, k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 5:
            str1.enforceInterface("com.android.internal.compat.IPlatformCompat");
            l = str1.readLong();
            str2 = str1.readString();
            j = str1.readInt();
            bool1 = isChangeEnabledByPackageName(l, str2, j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 4:
            str1.enforceInterface("com.android.internal.compat.IPlatformCompat");
            l = str1.readLong();
            if (str1.readInt() != 0) {
              ApplicationInfo applicationInfo = (ApplicationInfo)ApplicationInfo.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            bool1 = isChangeEnabled(l, (ApplicationInfo)str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 3:
            str1.enforceInterface("com.android.internal.compat.IPlatformCompat");
            l = str1.readLong();
            i = str1.readInt();
            reportChangeByUid(l, i);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            str1.enforceInterface("com.android.internal.compat.IPlatformCompat");
            l = str1.readLong();
            str2 = str1.readString();
            i = str1.readInt();
            reportChangeByPackageName(l, str2, i);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("com.android.internal.compat.IPlatformCompat");
        long l = str1.readLong();
        if (str1.readInt() != 0) {
          ApplicationInfo applicationInfo = (ApplicationInfo)ApplicationInfo.CREATOR.createFromParcel((Parcel)str1);
        } else {
          str1 = null;
        } 
        reportChange(l, (ApplicationInfo)str1);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("com.android.internal.compat.IPlatformCompat");
      return true;
    }
    
    private static class Proxy implements IPlatformCompat {
      public static IPlatformCompat sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.compat.IPlatformCompat";
      }
      
      public void reportChange(long param2Long, ApplicationInfo param2ApplicationInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.compat.IPlatformCompat");
          parcel1.writeLong(param2Long);
          if (param2ApplicationInfo != null) {
            parcel1.writeInt(1);
            param2ApplicationInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IPlatformCompat.Stub.getDefaultImpl() != null) {
            IPlatformCompat.Stub.getDefaultImpl().reportChange(param2Long, param2ApplicationInfo);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportChangeByPackageName(long param2Long, String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.compat.IPlatformCompat");
          parcel1.writeLong(param2Long);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IPlatformCompat.Stub.getDefaultImpl() != null) {
            IPlatformCompat.Stub.getDefaultImpl().reportChangeByPackageName(param2Long, param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportChangeByUid(long param2Long, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.compat.IPlatformCompat");
          parcel1.writeLong(param2Long);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IPlatformCompat.Stub.getDefaultImpl() != null) {
            IPlatformCompat.Stub.getDefaultImpl().reportChangeByUid(param2Long, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isChangeEnabled(long param2Long, ApplicationInfo param2ApplicationInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.compat.IPlatformCompat");
          parcel1.writeLong(param2Long);
          boolean bool1 = true;
          if (param2ApplicationInfo != null) {
            parcel1.writeInt(1);
            param2ApplicationInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IPlatformCompat.Stub.getDefaultImpl() != null) {
            bool1 = IPlatformCompat.Stub.getDefaultImpl().isChangeEnabled(param2Long, param2ApplicationInfo);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isChangeEnabledByPackageName(long param2Long, String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.compat.IPlatformCompat");
          parcel1.writeLong(param2Long);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IPlatformCompat.Stub.getDefaultImpl() != null) {
            bool1 = IPlatformCompat.Stub.getDefaultImpl().isChangeEnabledByPackageName(param2Long, param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isChangeEnabledByUid(long param2Long, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.compat.IPlatformCompat");
          parcel1.writeLong(param2Long);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IPlatformCompat.Stub.getDefaultImpl() != null) {
            bool1 = IPlatformCompat.Stub.getDefaultImpl().isChangeEnabledByUid(param2Long, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setOverrides(CompatibilityChangeConfig param2CompatibilityChangeConfig, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.compat.IPlatformCompat");
          if (param2CompatibilityChangeConfig != null) {
            parcel1.writeInt(1);
            param2CompatibilityChangeConfig.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IPlatformCompat.Stub.getDefaultImpl() != null) {
            IPlatformCompat.Stub.getDefaultImpl().setOverrides(param2CompatibilityChangeConfig, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setOverridesForTest(CompatibilityChangeConfig param2CompatibilityChangeConfig, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.compat.IPlatformCompat");
          if (param2CompatibilityChangeConfig != null) {
            parcel1.writeInt(1);
            param2CompatibilityChangeConfig.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IPlatformCompat.Stub.getDefaultImpl() != null) {
            IPlatformCompat.Stub.getDefaultImpl().setOverridesForTest(param2CompatibilityChangeConfig, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean clearOverride(long param2Long, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.compat.IPlatformCompat");
          parcel1.writeLong(param2Long);
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(9, parcel1, parcel2, 0);
          if (!bool2 && IPlatformCompat.Stub.getDefaultImpl() != null) {
            bool1 = IPlatformCompat.Stub.getDefaultImpl().clearOverride(param2Long, param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int enableTargetSdkChanges(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.compat.IPlatformCompat");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IPlatformCompat.Stub.getDefaultImpl() != null) {
            param2Int = IPlatformCompat.Stub.getDefaultImpl().enableTargetSdkChanges(param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int disableTargetSdkChanges(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.compat.IPlatformCompat");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IPlatformCompat.Stub.getDefaultImpl() != null) {
            param2Int = IPlatformCompat.Stub.getDefaultImpl().disableTargetSdkChanges(param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearOverrides(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.compat.IPlatformCompat");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IPlatformCompat.Stub.getDefaultImpl() != null) {
            IPlatformCompat.Stub.getDefaultImpl().clearOverrides(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearOverridesForTest(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.compat.IPlatformCompat");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IPlatformCompat.Stub.getDefaultImpl() != null) {
            IPlatformCompat.Stub.getDefaultImpl().clearOverridesForTest(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public CompatibilityChangeConfig getAppConfig(ApplicationInfo param2ApplicationInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.compat.IPlatformCompat");
          if (param2ApplicationInfo != null) {
            parcel1.writeInt(1);
            param2ApplicationInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IPlatformCompat.Stub.getDefaultImpl() != null)
            return IPlatformCompat.Stub.getDefaultImpl().getAppConfig(param2ApplicationInfo); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            CompatibilityChangeConfig compatibilityChangeConfig = (CompatibilityChangeConfig)CompatibilityChangeConfig.CREATOR.createFromParcel(parcel2);
          } else {
            param2ApplicationInfo = null;
          } 
          return (CompatibilityChangeConfig)param2ApplicationInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public CompatibilityChangeInfo[] listAllChanges() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.compat.IPlatformCompat");
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IPlatformCompat.Stub.getDefaultImpl() != null)
            return IPlatformCompat.Stub.getDefaultImpl().listAllChanges(); 
          parcel2.readException();
          return (CompatibilityChangeInfo[])parcel2.createTypedArray(CompatibilityChangeInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public CompatibilityChangeInfo[] listUIChanges() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.compat.IPlatformCompat");
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IPlatformCompat.Stub.getDefaultImpl() != null)
            return IPlatformCompat.Stub.getDefaultImpl().listUIChanges(); 
          parcel2.readException();
          return (CompatibilityChangeInfo[])parcel2.createTypedArray(CompatibilityChangeInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IOverrideValidator getOverrideValidator() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.compat.IPlatformCompat");
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IPlatformCompat.Stub.getDefaultImpl() != null)
            return IPlatformCompat.Stub.getDefaultImpl().getOverrideValidator(); 
          parcel2.readException();
          return IOverrideValidator.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPlatformCompat param1IPlatformCompat) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPlatformCompat != null) {
          Proxy.sDefaultImpl = param1IPlatformCompat;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPlatformCompat getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
