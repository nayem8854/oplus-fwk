package com.android.internal.compat;

import android.util.Log;
import android.util.Slog;
import com.android.internal.util.FrameworkStatsLog;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public final class ChangeReporter {
  public static final int SOURCE_APP_PROCESS = 1;
  
  public static final int SOURCE_SYSTEM_SERVER = 2;
  
  public static final int SOURCE_UNKNOWN_SOURCE = 0;
  
  public static final int STATE_DISABLED = 2;
  
  public static final int STATE_ENABLED = 1;
  
  public static final int STATE_LOGGED = 3;
  
  public static final int STATE_UNKNOWN_STATE = 0;
  
  private static final String TAG = "CompatibilityChangeReporter";
  
  private boolean mDebugLogAll;
  
  private final Map<Integer, Set<ChangeReport>> mReportedChanges;
  
  private int mSource;
  
  private static final class ChangeReport {
    long mChangeId;
    
    int mState;
    
    ChangeReport(long param1Long, int param1Int) {
      this.mChangeId = param1Long;
      this.mState = param1Int;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = true;
      if (this == param1Object)
        return true; 
      if (param1Object == null || getClass() != param1Object.getClass())
        return false; 
      param1Object = param1Object;
      if (this.mChangeId != ((ChangeReport)param1Object).mChangeId || this.mState != ((ChangeReport)param1Object).mState)
        bool = false; 
      return bool;
    }
    
    public int hashCode() {
      return Objects.hash(new Object[] { Long.valueOf(this.mChangeId), Integer.valueOf(this.mState) });
    }
  }
  
  public ChangeReporter(int paramInt) {
    this.mSource = paramInt;
    this.mReportedChanges = new HashMap<>();
    this.mDebugLogAll = false;
  }
  
  public void reportChange(int paramInt1, long paramLong, int paramInt2) {
    if (shouldWriteToStatsLog(paramInt1, paramLong, paramInt2))
      FrameworkStatsLog.write(228, paramInt1, paramLong, paramInt2, this.mSource); 
    if (shouldWriteToDebug(paramInt1, paramLong, paramInt2))
      debugLog(paramInt1, paramLong, paramInt2); 
    markAsReported(paramInt1, new ChangeReport(paramLong, paramInt2));
  }
  
  public void startDebugLogAll() {
    this.mDebugLogAll = true;
  }
  
  public void stopDebugLogAll() {
    this.mDebugLogAll = false;
  }
  
  public boolean shouldWriteToStatsLog(int paramInt1, long paramLong, int paramInt2) {
    return isAlreadyReported(paramInt1, new ChangeReport(paramLong, paramInt2)) ^ true;
  }
  
  public boolean shouldWriteToDebug(int paramInt1, long paramLong, int paramInt2) {
    return (this.mDebugLogAll || !isAlreadyReported(paramInt1, new ChangeReport(paramLong, paramInt2)));
  }
  
  private boolean isAlreadyReported(int paramInt, ChangeReport paramChangeReport) {
    synchronized (this.mReportedChanges) {
      Set set = this.mReportedChanges.get(Integer.valueOf(paramInt));
      if (set == null)
        return false; 
      return set.contains(paramChangeReport);
    } 
  }
  
  private void markAsReported(int paramInt, ChangeReport paramChangeReport) {
    synchronized (this.mReportedChanges) {
      Set<ChangeReport> set1 = this.mReportedChanges.get(Integer.valueOf(paramInt));
      Set<ChangeReport> set2 = set1;
      if (set1 == null) {
        Map<Integer, Set<ChangeReport>> map = this.mReportedChanges;
        set1 = new HashSet();
        super();
        map.put(Integer.valueOf(paramInt), set1);
        set2 = this.mReportedChanges.get(Integer.valueOf(paramInt));
      } 
      set2.add(paramChangeReport);
      return;
    } 
  }
  
  public void resetReportedChanges(int paramInt) {
    synchronized (this.mReportedChanges) {
      this.mReportedChanges.remove(Integer.valueOf(paramInt));
      return;
    } 
  }
  
  private void debugLog(int paramInt1, long paramLong, int paramInt2) {
    String str = stateToString(paramInt2);
    str = String.format("Compat change id reported: %d; UID %d; state: %s", new Object[] { Long.valueOf(paramLong), Integer.valueOf(paramInt1), str });
    if (this.mSource == 2) {
      Slog.d("CompatibilityChangeReporter", str);
    } else {
      Log.d("CompatibilityChangeReporter", str);
    } 
  }
  
  private static String stateToString(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 3)
          return "UNKNOWN"; 
        return "LOGGED";
      } 
      return "DISABLED";
    } 
    return "ENABLED";
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Source {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface State {}
}
