package com.android.internal.compat;

import android.os.Parcel;
import android.os.Parcelable;

public class CompatibilityChangeInfo implements Parcelable {
  public long getId() {
    return this.mChangeId;
  }
  
  public String getName() {
    return this.mName;
  }
  
  public int getEnableAfterTargetSdk() {
    return this.mEnableAfterTargetSdk;
  }
  
  public boolean getDisabled() {
    return this.mDisabled;
  }
  
  public boolean getLoggingOnly() {
    return this.mLoggingOnly;
  }
  
  public String getDescription() {
    return this.mDescription;
  }
  
  public CompatibilityChangeInfo(Long paramLong, String paramString1, int paramInt, boolean paramBoolean1, boolean paramBoolean2, String paramString2) {
    this.mChangeId = paramLong.longValue();
    this.mName = paramString1;
    this.mEnableAfterTargetSdk = paramInt;
    this.mDisabled = paramBoolean1;
    this.mLoggingOnly = paramBoolean2;
    this.mDescription = paramString2;
  }
  
  private CompatibilityChangeInfo(Parcel paramParcel) {
    this.mChangeId = paramParcel.readLong();
    this.mName = paramParcel.readString();
    this.mEnableAfterTargetSdk = paramParcel.readInt();
    this.mDisabled = paramParcel.readBoolean();
    this.mLoggingOnly = paramParcel.readBoolean();
    this.mDescription = paramParcel.readString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.mChangeId);
    paramParcel.writeString(this.mName);
    paramParcel.writeInt(this.mEnableAfterTargetSdk);
    paramParcel.writeBoolean(this.mDisabled);
    paramParcel.writeBoolean(this.mLoggingOnly);
    paramParcel.writeString(this.mDescription);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("CompatibilityChangeInfo(");
    stringBuilder = stringBuilder.append(getId());
    if (getName() != null) {
      stringBuilder.append("; name=");
      stringBuilder.append(getName());
    } 
    if (getEnableAfterTargetSdk() != -1) {
      stringBuilder.append("; enableAfterTargetSdk=");
      stringBuilder.append(getEnableAfterTargetSdk());
    } 
    if (getDisabled())
      stringBuilder.append("; disabled"); 
    if (getLoggingOnly())
      stringBuilder.append("; loggingOnly"); 
    stringBuilder.append(")");
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || !(paramObject instanceof CompatibilityChangeInfo))
      return false; 
    paramObject = paramObject;
    if (this.mChangeId == ((CompatibilityChangeInfo)paramObject).mChangeId) {
      String str1 = this.mName, str2 = ((CompatibilityChangeInfo)paramObject).mName;
      if (str1.equals(str2) && this.mEnableAfterTargetSdk == ((CompatibilityChangeInfo)paramObject).mEnableAfterTargetSdk && this.mDisabled == ((CompatibilityChangeInfo)paramObject).mDisabled && this.mLoggingOnly == ((CompatibilityChangeInfo)paramObject).mLoggingOnly) {
        str2 = this.mDescription;
        paramObject = ((CompatibilityChangeInfo)paramObject).mDescription;
        if (str2.equals(paramObject))
          return null; 
      } 
    } 
    return false;
  }
  
  public static final Parcelable.Creator<CompatibilityChangeInfo> CREATOR = new Parcelable.Creator<CompatibilityChangeInfo>() {
      public CompatibilityChangeInfo createFromParcel(Parcel param1Parcel) {
        return new CompatibilityChangeInfo(param1Parcel);
      }
      
      public CompatibilityChangeInfo[] newArray(int param1Int) {
        return new CompatibilityChangeInfo[param1Int];
      }
    };
  
  private final long mChangeId;
  
  private final String mDescription;
  
  private final boolean mDisabled;
  
  private final int mEnableAfterTargetSdk;
  
  private final boolean mLoggingOnly;
  
  private final String mName;
}
