package com.android.internal.compat;

import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class OverrideAllowedState implements Parcelable {
  public static final int ALLOWED = 0;
  
  private OverrideAllowedState(Parcel paramParcel) {
    this.state = paramParcel.readInt();
    this.appTargetSdk = paramParcel.readInt();
    this.changeIdTargetSdk = paramParcel.readInt();
  }
  
  public OverrideAllowedState(int paramInt1, int paramInt2, int paramInt3) {
    this.state = paramInt1;
    this.appTargetSdk = paramInt2;
    this.changeIdTargetSdk = paramInt3;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.state);
    paramParcel.writeInt(this.appTargetSdk);
    paramParcel.writeInt(this.changeIdTargetSdk);
  }
  
  public void enforce(long paramLong, String paramString) throws SecurityException {
    int i = this.state;
    if (i != 1) {
      if (i != 2) {
        if (i != 3) {
          if (i != 4) {
            if (i != 5)
              return; 
            throw new SecurityException(String.format("Cannot override %1$d because it is marked as a logging-only change.", new Object[] { Long.valueOf(paramLong) }));
          } 
          throw new SecurityException(String.format("Cannot override %1$d for %2$s because the package does not exist, and the change is targetSdk gated.", new Object[] { Long.valueOf(paramLong), paramString }));
        } 
        i = this.appTargetSdk;
        int j = this.changeIdTargetSdk;
        throw new SecurityException(String.format("Cannot override %1$d for %2$s because the app's targetSdk (%3$d) is above the change's targetSdk threshold (%4$d)", new Object[] { Long.valueOf(paramLong), paramString, Integer.valueOf(i), Integer.valueOf(j) }));
      } 
      throw new SecurityException("Cannot override a default enabled/disabled change on a user build.");
    } 
    throw new SecurityException("Cannot override a change on a non-debuggable app and user build.");
  }
  
  public static final Parcelable.Creator<OverrideAllowedState> CREATOR = new Parcelable.Creator<OverrideAllowedState>() {
      public OverrideAllowedState createFromParcel(Parcel param1Parcel) {
        return new OverrideAllowedState(param1Parcel);
      }
      
      public OverrideAllowedState[] newArray(int param1Int) {
        return new OverrideAllowedState[param1Int];
      }
    };
  
  public static final int DISABLED_NON_TARGET_SDK = 2;
  
  public static final int DISABLED_NOT_DEBUGGABLE = 1;
  
  public static final int DISABLED_TARGET_SDK_TOO_HIGH = 3;
  
  public static final int LOGGING_ONLY_CHANGE = 5;
  
  public static final int PACKAGE_DOES_NOT_EXIST = 4;
  
  public final int appTargetSdk;
  
  public final int changeIdTargetSdk;
  
  public final int state;
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (!(paramObject instanceof OverrideAllowedState))
      return false; 
    paramObject = paramObject;
    if (this.state != ((OverrideAllowedState)paramObject).state || this.appTargetSdk != ((OverrideAllowedState)paramObject).appTargetSdk || this.changeIdTargetSdk != ((OverrideAllowedState)paramObject).changeIdTargetSdk)
      bool = false; 
    return bool;
  }
  
  private String stateName() {
    int i = this.state;
    if (i != 0) {
      if (i != 1) {
        if (i != 2) {
          if (i != 3) {
            if (i != 4) {
              if (i != 5)
                return "UNKNOWN"; 
              return "LOGGING_ONLY_CHANGE";
            } 
            return "PACKAGE_DOES_NOT_EXIST";
          } 
          return "DISABLED_TARGET_SDK_TOO_HIGH";
        } 
        return "DISABLED_NON_TARGET_SDK";
      } 
      return "DISABLED_NOT_DEBUGGABLE";
    } 
    return "ALLOWED";
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("OverrideAllowedState(state=");
    stringBuilder.append(stateName());
    stringBuilder.append("; appTargetSdk=");
    stringBuilder.append(this.appTargetSdk);
    stringBuilder.append("; changeIdTargetSdk=");
    stringBuilder.append(this.changeIdTargetSdk);
    stringBuilder.append(")");
    return stringBuilder.toString();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class State implements Annotation {}
}
