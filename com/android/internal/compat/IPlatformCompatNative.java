package com.android.internal.compat;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IPlatformCompatNative extends IInterface {
  boolean isChangeEnabledByPackageName(long paramLong, String paramString, int paramInt) throws RemoteException;
  
  boolean isChangeEnabledByUid(long paramLong, int paramInt) throws RemoteException;
  
  void reportChangeByPackageName(long paramLong, String paramString, int paramInt) throws RemoteException;
  
  void reportChangeByUid(long paramLong, int paramInt) throws RemoteException;
  
  class Default implements IPlatformCompatNative {
    public void reportChangeByPackageName(long param1Long, String param1String, int param1Int) throws RemoteException {}
    
    public void reportChangeByUid(long param1Long, int param1Int) throws RemoteException {}
    
    public boolean isChangeEnabledByPackageName(long param1Long, String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isChangeEnabledByUid(long param1Long, int param1Int) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPlatformCompatNative {
    private static final String DESCRIPTOR = "com.android.internal.compat.IPlatformCompatNative";
    
    static final int TRANSACTION_isChangeEnabledByPackageName = 3;
    
    static final int TRANSACTION_isChangeEnabledByUid = 4;
    
    static final int TRANSACTION_reportChangeByPackageName = 1;
    
    static final int TRANSACTION_reportChangeByUid = 2;
    
    public Stub() {
      attachInterface(this, "com.android.internal.compat.IPlatformCompatNative");
    }
    
    public static IPlatformCompatNative asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.compat.IPlatformCompatNative");
      if (iInterface != null && iInterface instanceof IPlatformCompatNative)
        return (IPlatformCompatNative)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "isChangeEnabledByUid";
          } 
          return "isChangeEnabledByPackageName";
        } 
        return "reportChangeByUid";
      } 
      return "reportChangeByPackageName";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("com.android.internal.compat.IPlatformCompatNative");
              return true;
            } 
            param1Parcel1.enforceInterface("com.android.internal.compat.IPlatformCompatNative");
            long l3 = param1Parcel1.readLong();
            param1Int1 = param1Parcel1.readInt();
            boolean bool1 = isChangeEnabledByUid(l3, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          } 
          param1Parcel1.enforceInterface("com.android.internal.compat.IPlatformCompatNative");
          long l2 = param1Parcel1.readLong();
          String str1 = param1Parcel1.readString();
          param1Int1 = param1Parcel1.readInt();
          boolean bool = isChangeEnabledByPackageName(l2, str1, param1Int1);
          param1Parcel2.writeNoException();
          param1Parcel2.writeInt(bool);
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.internal.compat.IPlatformCompatNative");
        long l1 = param1Parcel1.readLong();
        param1Int1 = param1Parcel1.readInt();
        reportChangeByUid(l1, param1Int1);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.compat.IPlatformCompatNative");
      long l = param1Parcel1.readLong();
      String str = param1Parcel1.readString();
      param1Int1 = param1Parcel1.readInt();
      reportChangeByPackageName(l, str, param1Int1);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IPlatformCompatNative {
      public static IPlatformCompatNative sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.compat.IPlatformCompatNative";
      }
      
      public void reportChangeByPackageName(long param2Long, String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.compat.IPlatformCompatNative");
          parcel1.writeLong(param2Long);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IPlatformCompatNative.Stub.getDefaultImpl() != null) {
            IPlatformCompatNative.Stub.getDefaultImpl().reportChangeByPackageName(param2Long, param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportChangeByUid(long param2Long, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.compat.IPlatformCompatNative");
          parcel1.writeLong(param2Long);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IPlatformCompatNative.Stub.getDefaultImpl() != null) {
            IPlatformCompatNative.Stub.getDefaultImpl().reportChangeByUid(param2Long, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isChangeEnabledByPackageName(long param2Long, String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.compat.IPlatformCompatNative");
          parcel1.writeLong(param2Long);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IPlatformCompatNative.Stub.getDefaultImpl() != null) {
            bool1 = IPlatformCompatNative.Stub.getDefaultImpl().isChangeEnabledByPackageName(param2Long, param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isChangeEnabledByUid(long param2Long, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.compat.IPlatformCompatNative");
          parcel1.writeLong(param2Long);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IPlatformCompatNative.Stub.getDefaultImpl() != null) {
            bool1 = IPlatformCompatNative.Stub.getDefaultImpl().isChangeEnabledByUid(param2Long, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPlatformCompatNative param1IPlatformCompatNative) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPlatformCompatNative != null) {
          Proxy.sDefaultImpl = param1IPlatformCompatNative;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPlatformCompatNative getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
