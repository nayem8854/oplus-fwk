package com.android.internal.compat;

import android.compat.Compatibility;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.HashSet;
import java.util.Set;

public final class CompatibilityChangeConfig implements Parcelable {
  public CompatibilityChangeConfig(Compatibility.ChangeConfig paramChangeConfig) {
    this.mChangeConfig = paramChangeConfig;
  }
  
  public Set<Long> enabledChanges() {
    return this.mChangeConfig.forceEnabledSet();
  }
  
  public Set<Long> disabledChanges() {
    return this.mChangeConfig.forceDisabledSet();
  }
  
  public boolean isChangeEnabled(long paramLong) {
    if (this.mChangeConfig.isForceEnabled(paramLong))
      return true; 
    if (this.mChangeConfig.isForceDisabled(paramLong))
      return false; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Change ");
    stringBuilder.append(paramLong);
    stringBuilder.append(" is not defined.");
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  private CompatibilityChangeConfig(Parcel paramParcel) {
    long[] arrayOfLong2 = paramParcel.createLongArray();
    long[] arrayOfLong1 = paramParcel.createLongArray();
    Set<Long> set2 = toLongSet(arrayOfLong2);
    Set<Long> set1 = toLongSet(arrayOfLong1);
    this.mChangeConfig = new Compatibility.ChangeConfig(set2, set1);
  }
  
  private static Set<Long> toLongSet(long[] paramArrayOflong) {
    HashSet<Long> hashSet = new HashSet();
    int i;
    byte b;
    for (i = paramArrayOflong.length, b = 0; b < i; ) {
      long l = paramArrayOflong[b];
      hashSet.add(Long.valueOf(l));
      b++;
    } 
    return hashSet;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    long[] arrayOfLong1 = this.mChangeConfig.forceEnabledChangesArray();
    long[] arrayOfLong2 = this.mChangeConfig.forceDisabledChangesArray();
    paramParcel.writeLongArray(arrayOfLong1);
    paramParcel.writeLongArray(arrayOfLong2);
  }
  
  public static final Parcelable.Creator<CompatibilityChangeConfig> CREATOR = new Parcelable.Creator<CompatibilityChangeConfig>() {
      public CompatibilityChangeConfig createFromParcel(Parcel param1Parcel) {
        return new CompatibilityChangeConfig(param1Parcel);
      }
      
      public CompatibilityChangeConfig[] newArray(int param1Int) {
        return new CompatibilityChangeConfig[param1Int];
      }
    };
  
  private final Compatibility.ChangeConfig mChangeConfig;
}
