package com.android.internal.compat;

import android.os.Build;

public class AndroidBuildClassifier {
  public boolean isDebuggableBuild() {
    return Build.IS_DEBUGGABLE;
  }
  
  public boolean isFinalBuild() {
    return "REL".equals(Build.VERSION.CODENAME);
  }
}
