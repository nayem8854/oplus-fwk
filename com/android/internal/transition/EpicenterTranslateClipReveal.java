package com.android.internal.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.TypeEvaluator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.transition.TransitionValues;
import android.transition.Visibility;
import android.util.AttributeSet;
import android.util.Property;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import com.android.internal.R;

public class EpicenterTranslateClipReveal extends Visibility {
  private static final String PROPNAME_BOUNDS = "android:epicenterReveal:bounds";
  
  private static final String PROPNAME_CLIP = "android:epicenterReveal:clip";
  
  private static final String PROPNAME_TRANSLATE_X = "android:epicenterReveal:translateX";
  
  private static final String PROPNAME_TRANSLATE_Y = "android:epicenterReveal:translateY";
  
  private static final String PROPNAME_TRANSLATE_Z = "android:epicenterReveal:translateZ";
  
  private static final String PROPNAME_Z = "android:epicenterReveal:z";
  
  private final TimeInterpolator mInterpolatorX;
  
  private final TimeInterpolator mInterpolatorY;
  
  private final TimeInterpolator mInterpolatorZ;
  
  public EpicenterTranslateClipReveal() {
    this.mInterpolatorX = null;
    this.mInterpolatorY = null;
    this.mInterpolatorZ = null;
  }
  
  public EpicenterTranslateClipReveal(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.EpicenterTranslateClipReveal, 0, 0);
    int i = typedArray.getResourceId(0, 0);
    if (i != 0) {
      this.mInterpolatorX = (TimeInterpolator)AnimationUtils.loadInterpolator(paramContext, i);
    } else {
      this.mInterpolatorX = TransitionConstants.LINEAR_OUT_SLOW_IN;
    } 
    i = typedArray.getResourceId(1, 0);
    if (i != 0) {
      this.mInterpolatorY = (TimeInterpolator)AnimationUtils.loadInterpolator(paramContext, i);
    } else {
      this.mInterpolatorY = TransitionConstants.FAST_OUT_SLOW_IN;
    } 
    i = typedArray.getResourceId(2, 0);
    if (i != 0) {
      this.mInterpolatorZ = (TimeInterpolator)AnimationUtils.loadInterpolator(paramContext, i);
    } else {
      this.mInterpolatorZ = TransitionConstants.FAST_OUT_SLOW_IN;
    } 
    typedArray.recycle();
  }
  
  public void captureStartValues(TransitionValues paramTransitionValues) {
    super.captureStartValues(paramTransitionValues);
    captureValues(paramTransitionValues);
  }
  
  public void captureEndValues(TransitionValues paramTransitionValues) {
    super.captureEndValues(paramTransitionValues);
    captureValues(paramTransitionValues);
  }
  
  private void captureValues(TransitionValues paramTransitionValues) {
    View view = paramTransitionValues.view;
    if (view.getVisibility() == 8)
      return; 
    Rect rect = new Rect(0, 0, view.getWidth(), view.getHeight());
    paramTransitionValues.values.put("android:epicenterReveal:bounds", rect);
    paramTransitionValues.values.put("android:epicenterReveal:translateX", Float.valueOf(view.getTranslationX()));
    paramTransitionValues.values.put("android:epicenterReveal:translateY", Float.valueOf(view.getTranslationY()));
    paramTransitionValues.values.put("android:epicenterReveal:translateZ", Float.valueOf(view.getTranslationZ()));
    paramTransitionValues.values.put("android:epicenterReveal:z", Float.valueOf(view.getZ()));
    rect = view.getClipBounds();
    paramTransitionValues.values.put("android:epicenterReveal:clip", rect);
  }
  
  public Animator onAppear(ViewGroup paramViewGroup, View paramView, TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2) {
    if (paramTransitionValues2 == null)
      return null; 
    Rect rect2 = (Rect)paramTransitionValues2.values.get("android:epicenterReveal:bounds");
    Rect rect1 = getEpicenterOrCenter(rect2);
    float f1 = (rect1.centerX() - rect2.centerX());
    float f2 = (rect1.centerY() - rect2.centerY());
    float f3 = 0.0F - ((Float)paramTransitionValues2.values.get("android:epicenterReveal:z")).floatValue();
    paramView.setTranslationX(f1);
    paramView.setTranslationY(f2);
    paramView.setTranslationZ(f3);
    float f4 = ((Float)paramTransitionValues2.values.get("android:epicenterReveal:translateX")).floatValue();
    float f5 = ((Float)paramTransitionValues2.values.get("android:epicenterReveal:translateY")).floatValue();
    float f6 = ((Float)paramTransitionValues2.values.get("android:epicenterReveal:translateZ")).floatValue();
    Rect rect3 = getBestRect(paramTransitionValues2);
    Rect rect4 = getEpicenterOrCenter(rect3);
    paramView.setClipBounds(rect4);
    State state2 = new State(rect4.left, rect4.right, f1);
    State state1 = new State(rect3.left, rect3.right, f4);
    State state4 = new State(rect4.top, rect4.bottom, f2);
    State state3 = new State(rect3.top, rect3.bottom, f5);
    return createRectAnimator(paramView, state2, state4, f3, state1, state3, f6, paramTransitionValues2, this.mInterpolatorX, this.mInterpolatorY, this.mInterpolatorZ);
  }
  
  public Animator onDisappear(ViewGroup paramViewGroup, View paramView, TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2) {
    if (paramTransitionValues1 == null)
      return null; 
    Rect rect1 = (Rect)paramTransitionValues2.values.get("android:epicenterReveal:bounds");
    Rect rect2 = getEpicenterOrCenter(rect1);
    float f1 = (rect2.centerX() - rect1.centerX());
    float f2 = (rect2.centerY() - rect1.centerY());
    float f3 = ((Float)paramTransitionValues1.values.get("android:epicenterReveal:z")).floatValue();
    float f4 = ((Float)paramTransitionValues2.values.get("android:epicenterReveal:translateX")).floatValue();
    float f5 = ((Float)paramTransitionValues2.values.get("android:epicenterReveal:translateY")).floatValue();
    float f6 = ((Float)paramTransitionValues2.values.get("android:epicenterReveal:translateZ")).floatValue();
    Rect rect3 = getBestRect(paramTransitionValues1);
    rect2 = getEpicenterOrCenter(rect3);
    paramView.setClipBounds(rect3);
    State state1 = new State(rect3.left, rect3.right, f4);
    State state2 = new State(rect2.left, rect2.right, f1);
    State state4 = new State(rect3.top, rect3.bottom, f5);
    State state3 = new State(rect2.top, rect2.bottom, f2);
    return createRectAnimator(paramView, state1, state4, f6, state2, state3, 0.0F - f3, paramTransitionValues2, this.mInterpolatorX, this.mInterpolatorY, this.mInterpolatorZ);
  }
  
  private Rect getEpicenterOrCenter(Rect paramRect) {
    Rect rect = getEpicenter();
    if (rect != null)
      return rect; 
    int i = paramRect.centerX();
    int j = paramRect.centerY();
    return new Rect(i, j, i, j);
  }
  
  private Rect getBestRect(TransitionValues paramTransitionValues) {
    Rect rect = (Rect)paramTransitionValues.values.get("android:epicenterReveal:clip");
    if (rect == null)
      return (Rect)paramTransitionValues.values.get("android:epicenterReveal:bounds"); 
    return rect;
  }
  
  private static Animator createRectAnimator(final View view, State paramState1, State paramState2, float paramFloat1, State paramState3, State paramState4, float paramFloat2, TransitionValues paramTransitionValues, TimeInterpolator paramTimeInterpolator1, TimeInterpolator paramTimeInterpolator2, TimeInterpolator paramTimeInterpolator3) {
    StateEvaluator stateEvaluator = new StateEvaluator();
    ObjectAnimator objectAnimator3 = ObjectAnimator.ofFloat(view, View.TRANSLATION_Z, new float[] { paramFloat1, paramFloat2 });
    if (paramTimeInterpolator3 != null)
      objectAnimator3.setInterpolator(paramTimeInterpolator3); 
    StateProperty stateProperty2 = new StateProperty('x');
    ObjectAnimator objectAnimator1 = ObjectAnimator.ofObject(view, stateProperty2, stateEvaluator, (Object[])new State[] { paramState1, paramState3 });
    if (paramTimeInterpolator1 != null)
      objectAnimator1.setInterpolator(paramTimeInterpolator1); 
    StateProperty stateProperty1 = new StateProperty('y');
    ObjectAnimator objectAnimator2 = ObjectAnimator.ofObject(view, stateProperty1, stateEvaluator, (Object[])new State[] { paramState2, paramState4 });
    if (paramTimeInterpolator2 != null)
      objectAnimator2.setInterpolator(paramTimeInterpolator2); 
    final Rect terminalClip = (Rect)paramTransitionValues.values.get("android:epicenterReveal:clip");
    AnimatorListenerAdapter animatorListenerAdapter = new AnimatorListenerAdapter() {
        final Rect val$terminalClip;
        
        final View val$view;
        
        public void onAnimationEnd(Animator param1Animator) {
          view.setClipBounds(terminalClip);
        }
      };
    AnimatorSet animatorSet = new AnimatorSet();
    animatorSet.playTogether(new Animator[] { (Animator)objectAnimator1, (Animator)objectAnimator2, (Animator)objectAnimator3 });
    animatorSet.addListener((Animator.AnimatorListener)animatorListenerAdapter);
    return (Animator)animatorSet;
  }
  
  class State {
    int lower;
    
    float trans;
    
    int upper;
    
    public State() {}
    
    public State(EpicenterTranslateClipReveal this$0, int param1Int1, float param1Float) {
      this.lower = this$0;
      this.upper = param1Int1;
      this.trans = param1Float;
    }
  }
  
  class StateEvaluator implements TypeEvaluator<State> {
    private final EpicenterTranslateClipReveal.State mTemp;
    
    private StateEvaluator() {
      this.mTemp = new EpicenterTranslateClipReveal.State();
    }
    
    public EpicenterTranslateClipReveal.State evaluate(float param1Float, EpicenterTranslateClipReveal.State param1State1, EpicenterTranslateClipReveal.State param1State2) {
      param1State1.upper += (int)((param1State2.upper - param1State1.upper) * param1Float);
      param1State1.lower += (int)((param1State2.lower - param1State1.lower) * param1Float);
      param1State1.trans += (int)((param1State2.trans - param1State1.trans) * param1Float);
      return this.mTemp;
    }
  }
  
  class StateProperty extends Property<View, State> {
    public static final char TARGET_X = 'x';
    
    public static final char TARGET_Y = 'y';
    
    private final int mTargetDimension;
    
    private final Rect mTempRect = new Rect();
    
    private final EpicenterTranslateClipReveal.State mTempState = new EpicenterTranslateClipReveal.State();
    
    public StateProperty(EpicenterTranslateClipReveal this$0) {
      super(EpicenterTranslateClipReveal.State.class, stringBuilder.toString());
      this.mTargetDimension = this$0;
    }
    
    public EpicenterTranslateClipReveal.State get(View param1View) {
      Rect rect = this.mTempRect;
      if (!param1View.getClipBounds(rect))
        rect.setEmpty(); 
      EpicenterTranslateClipReveal.State state = this.mTempState;
      if (this.mTargetDimension == 120) {
        state.trans = param1View.getTranslationX();
        state.lower = rect.left + (int)state.trans;
        state.upper = rect.right + (int)state.trans;
      } else {
        state.trans = param1View.getTranslationY();
        state.lower = rect.top + (int)state.trans;
        state.upper = rect.bottom + (int)state.trans;
      } 
      return state;
    }
    
    public void set(View param1View, EpicenterTranslateClipReveal.State param1State) {
      Rect rect = this.mTempRect;
      if (param1View.getClipBounds(rect)) {
        if (this.mTargetDimension == 120) {
          rect.left = param1State.lower - (int)param1State.trans;
          rect.right = param1State.upper - (int)param1State.trans;
        } else {
          rect.top = param1State.lower - (int)param1State.trans;
          rect.bottom = param1State.upper - (int)param1State.trans;
        } 
        param1View.setClipBounds(rect);
      } 
      if (this.mTargetDimension == 120) {
        param1View.setTranslationX(param1State.trans);
      } else {
        param1View.setTranslationY(param1State.trans);
      } 
    }
  }
}
