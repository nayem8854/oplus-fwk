package com.android.internal.accessibility.dialog;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.Context;
import android.widget.TextView;
import com.android.internal.accessibility.util.AccessibilityUtils;

class ToggleAccessibilityServiceTarget extends AccessibilityServiceTarget {
  ToggleAccessibilityServiceTarget(Context paramContext, int paramInt, AccessibilityServiceInfo paramAccessibilityServiceInfo) {
    super(paramContext, paramInt, 2, paramAccessibilityServiceInfo);
  }
  
  public void updateActionItem(TargetAdapter.ViewHolder paramViewHolder, int paramInt) {
    super.updateActionItem(paramViewHolder, paramInt);
    byte b = 0;
    boolean bool = true;
    if (paramInt == 1) {
      paramInt = bool;
    } else {
      paramInt = 0;
    } 
    TextView textView = paramViewHolder.mStatusView;
    if (paramInt != 0)
      b = 8; 
    textView.setVisibility(b);
    if (AccessibilityUtils.isAccessibilityServiceEnabled(getContext(), getId())) {
      paramInt = 17039566;
    } else {
      paramInt = 17039565;
    } 
    paramViewHolder.mStatusView.setText(getContext().getString(paramInt));
  }
}
