package com.android.internal.accessibility.dialog;

interface OnTargetSelectedListener {
  void onSelected();
}
