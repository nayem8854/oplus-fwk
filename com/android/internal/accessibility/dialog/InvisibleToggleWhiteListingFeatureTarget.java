package com.android.internal.accessibility.dialog;

import android.content.Context;
import android.graphics.drawable.Drawable;

class InvisibleToggleWhiteListingFeatureTarget extends AccessibilityTarget {
  InvisibleToggleWhiteListingFeatureTarget(Context paramContext, int paramInt, boolean paramBoolean, String paramString1, CharSequence paramCharSequence, Drawable paramDrawable, String paramString2) {
    super(paramContext, paramInt, 1, paramBoolean, paramString1, paramCharSequence, paramDrawable, paramString2);
  }
}
