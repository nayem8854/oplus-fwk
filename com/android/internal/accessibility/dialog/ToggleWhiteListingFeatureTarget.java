package com.android.internal.accessibility.dialog;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.provider.Settings;
import android.widget.TextView;

class ToggleWhiteListingFeatureTarget extends AccessibilityTarget {
  ToggleWhiteListingFeatureTarget(Context paramContext, int paramInt, boolean paramBoolean, String paramString1, CharSequence paramCharSequence, Drawable paramDrawable, String paramString2) {
    super(paramContext, paramInt, 2, paramBoolean, paramString1, paramCharSequence, paramDrawable, paramString2);
  }
  
  public void updateActionItem(TargetAdapter.ViewHolder paramViewHolder, int paramInt) {
    super.updateActionItem(paramViewHolder, paramInt);
    byte b = 0;
    boolean bool = true;
    if (paramInt == 1) {
      paramInt = bool;
    } else {
      paramInt = 0;
    } 
    TextView textView = paramViewHolder.mStatusView;
    if (paramInt != 0)
      b = 8; 
    textView.setVisibility(b);
    if (isFeatureEnabled()) {
      paramInt = 17039566;
    } else {
      paramInt = 17039565;
    } 
    paramViewHolder.mStatusView.setText(getContext().getString(paramInt));
  }
  
  private boolean isFeatureEnabled() {
    ContentResolver contentResolver = getContext().getContentResolver();
    String str = getKey();
    boolean bool = false;
    if (Settings.Secure.getInt(contentResolver, str, 0) == 1)
      bool = true; 
    return bool;
  }
}
