package com.android.internal.accessibility.dialog;

interface OnTargetCheckedChangeListener {
  void onCheckedChanged(boolean paramBoolean);
}
