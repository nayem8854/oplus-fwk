package com.android.internal.accessibility.dialog;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.ComponentName;
import android.content.Context;
import com.android.internal.accessibility.util.AccessibilityUtils;
import com.android.internal.accessibility.util.ShortcutUtils;

class InvisibleToggleAccessibilityServiceTarget extends AccessibilityServiceTarget {
  InvisibleToggleAccessibilityServiceTarget(Context paramContext, int paramInt, AccessibilityServiceInfo paramAccessibilityServiceInfo) {
    super(paramContext, paramInt, 1, paramAccessibilityServiceInfo);
  }
  
  public void onCheckedChanged(boolean paramBoolean) {
    ComponentName componentName = ComponentName.unflattenFromString(getId());
    if (!isComponentIdExistingInOtherShortcut())
      AccessibilityUtils.setAccessibilityServiceState(getContext(), componentName, paramBoolean); 
    super.onCheckedChanged(paramBoolean);
  }
  
  private boolean isComponentIdExistingInOtherShortcut() {
    int i = getShortcutType();
    if (i != 0) {
      if (i == 1) {
        Context context1 = getContext();
        String str1 = getId();
        return ShortcutUtils.isComponentIdExistingInSettings(context1, 1, str1);
      } 
      throw new IllegalStateException("Unexpected shortcut type");
    } 
    Context context = getContext();
    String str = getId();
    return ShortcutUtils.isComponentIdExistingInSettings(context, 2, str);
  }
}
