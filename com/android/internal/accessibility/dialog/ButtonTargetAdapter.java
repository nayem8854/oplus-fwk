package com.android.internal.accessibility.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

class ButtonTargetAdapter extends TargetAdapter {
  private List<AccessibilityTarget> mTargets;
  
  ButtonTargetAdapter(List<AccessibilityTarget> paramList) {
    this.mTargets = paramList;
  }
  
  public int getCount() {
    return this.mTargets.size();
  }
  
  public Object getItem(int paramInt) {
    return this.mTargets.get(paramInt);
  }
  
  public long getItemId(int paramInt) {
    return paramInt;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup) {
    Context context = paramViewGroup.getContext();
    View view = LayoutInflater.from(context).inflate(17367065, paramViewGroup, false);
    AccessibilityTarget accessibilityTarget = this.mTargets.get(paramInt);
    ImageView imageView = view.<ImageView>findViewById(16908673);
    TextView textView = view.<TextView>findViewById(16908674);
    imageView.setImageDrawable(accessibilityTarget.getIcon());
    textView.setText(accessibilityTarget.getLabel());
    return view;
  }
}
