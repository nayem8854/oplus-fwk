package com.android.internal.accessibility.dialog;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.Context;
import android.graphics.drawable.Drawable;

class AccessibilityServiceTarget extends AccessibilityTarget {
  AccessibilityServiceTarget(Context paramContext, int paramInt1, int paramInt2, AccessibilityServiceInfo paramAccessibilityServiceInfo) {
    super(paramContext, paramInt1, paramInt2, bool, str2, charSequence, drawable, str3);
  }
}
