package com.android.internal.accessibility.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

class ShortcutTargetAdapter extends TargetAdapter {
  private int mShortcutMenuMode = 0;
  
  private final List<AccessibilityTarget> mTargets;
  
  ShortcutTargetAdapter(List<AccessibilityTarget> paramList) {
    this.mTargets = paramList;
  }
  
  public int getCount() {
    return this.mTargets.size();
  }
  
  public Object getItem(int paramInt) {
    return this.mTargets.get(paramInt);
  }
  
  public long getItemId(int paramInt) {
    return paramInt;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup) {
    TargetAdapter.ViewHolder viewHolder;
    Context context = paramViewGroup.getContext();
    if (paramView == null) {
      paramView = LayoutInflater.from(context).inflate(17367067, paramViewGroup, false);
      viewHolder = new TargetAdapter.ViewHolder();
      viewHolder.mCheckBoxView = paramView.<CheckBox>findViewById(16908687);
      viewHolder.mIconView = paramView.<ImageView>findViewById(16908688);
      viewHolder.mLabelView = paramView.<TextView>findViewById(16908689);
      viewHolder.mStatusView = paramView.<TextView>findViewById(16908690);
      paramView.setTag(viewHolder);
    } else {
      viewHolder = (TargetAdapter.ViewHolder)paramView.getTag();
    } 
    AccessibilityTarget accessibilityTarget = this.mTargets.get(paramInt);
    accessibilityTarget.updateActionItem(viewHolder, this.mShortcutMenuMode);
    return paramView;
  }
  
  void setShortcutMenuMode(int paramInt) {
    this.mShortcutMenuMode = paramInt;
  }
  
  int getShortcutMenuMode() {
    return this.mShortcutMenuMode;
  }
}
