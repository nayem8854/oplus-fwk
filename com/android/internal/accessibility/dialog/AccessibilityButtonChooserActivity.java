package com.android.internal.accessibility.dialog;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import com.android.internal.accessibility.AccessibilityShortcutController;
import com.android.internal.accessibility.util.AccessibilityStatsLogUtils;
import com.android.internal.widget.ResolverDrawerLayout;
import java.util.ArrayList;
import java.util.List;

public class AccessibilityButtonChooserActivity extends Activity {
  private final List<AccessibilityTarget> mTargets = new ArrayList<>();
  
  protected void onCreate(Bundle paramBundle) {
    int i;
    super.onCreate(paramBundle);
    setContentView(17367064);
    ResolverDrawerLayout resolverDrawerLayout = (ResolverDrawerLayout)findViewById(16908864);
    if (resolverDrawerLayout != null)
      resolverDrawerLayout.setOnDismissedListener(new _$$Lambda$sHKZ5vjXGMSX6145vMupSTXUy_E(this)); 
    String str = Settings.Secure.getString(getContentResolver(), "accessibility_button_target_component");
    AccessibilityManager accessibilityManager = (AccessibilityManager)getSystemService(AccessibilityManager.class);
    boolean bool = accessibilityManager.isTouchExplorationEnabled();
    if (2 == getResources().getInteger(17694850)) {
      i = 1;
    } else {
      i = 0;
    } 
    if (i) {
      int j;
      TextView textView = (TextView)findViewById(16908672);
      if (bool) {
        j = 17039553;
      } else {
        j = 17039555;
      } 
      textView.setText(j);
    } 
    if (TextUtils.isEmpty(str)) {
      TextView textView = (TextView)findViewById(16908671);
      if (i) {
        if (bool) {
          i = 17039552;
        } else {
          i = 17039554;
        } 
        textView.setText(i);
      } 
      textView.setVisibility(0);
    } 
    this.mTargets.addAll(AccessibilityTargetHelper.getTargets((Context)this, 0));
    GridView gridView = (GridView)findViewById(16908670);
    gridView.setAdapter(new ButtonTargetAdapter(this.mTargets));
    gridView.setOnItemClickListener(new _$$Lambda$AccessibilityButtonChooserActivity$5c5SEWQujtPn3_TYI5Kxc0U_G9I(this));
  }
}
