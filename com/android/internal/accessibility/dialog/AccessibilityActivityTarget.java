package com.android.internal.accessibility.dialog;

import android.accessibilityservice.AccessibilityShortcutInfo;
import android.content.Context;
import android.graphics.drawable.Drawable;

class AccessibilityActivityTarget extends AccessibilityTarget {
  AccessibilityActivityTarget(Context paramContext, int paramInt, AccessibilityShortcutInfo paramAccessibilityShortcutInfo) {
    super(paramContext, paramInt, 3, bool, str1, charSequence, drawable, str2);
  }
}
