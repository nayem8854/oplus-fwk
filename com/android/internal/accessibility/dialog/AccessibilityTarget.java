package com.android.internal.accessibility.dialog;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.accessibility.AccessibilityManager;
import android.widget.CheckBox;
import com.android.internal.accessibility.util.ShortcutUtils;

public abstract class AccessibilityTarget implements TargetOperations, OnTargetSelectedListener, OnTargetCheckedChangeListener {
  private Context mContext;
  
  private int mFragmentType;
  
  private Drawable mIcon;
  
  private String mId;
  
  private String mKey;
  
  private CharSequence mLabel;
  
  private boolean mShortcutEnabled;
  
  private int mShortcutType;
  
  AccessibilityTarget(Context paramContext, int paramInt1, int paramInt2, boolean paramBoolean, String paramString1, CharSequence paramCharSequence, Drawable paramDrawable, String paramString2) {
    this.mContext = paramContext;
    this.mShortcutType = paramInt1;
    this.mFragmentType = paramInt2;
    this.mShortcutEnabled = paramBoolean;
    this.mId = paramString1;
    this.mLabel = paramCharSequence;
    this.mIcon = paramDrawable;
    this.mKey = paramString2;
  }
  
  public void updateActionItem(TargetAdapter.ViewHolder paramViewHolder, int paramInt) {
    boolean bool1 = false, bool2 = true;
    if (paramInt == 1) {
      paramInt = 1;
    } else {
      paramInt = 0;
    } 
    CheckBox checkBox = paramViewHolder.mCheckBoxView;
    if (paramInt == 0 || !isShortcutEnabled())
      bool2 = false; 
    checkBox.setChecked(bool2);
    checkBox = paramViewHolder.mCheckBoxView;
    if (paramInt != 0) {
      paramInt = bool1;
    } else {
      paramInt = 8;
    } 
    checkBox.setVisibility(paramInt);
    paramViewHolder.mIconView.setImageDrawable(getIcon());
    paramViewHolder.mLabelView.setText(getLabel());
    paramViewHolder.mStatusView.setVisibility(8);
  }
  
  public void onSelected() {
    AccessibilityManager accessibilityManager = (AccessibilityManager)getContext().getSystemService(AccessibilityManager.class);
    int i = getShortcutType();
    if (i != 0) {
      if (i == 1) {
        accessibilityManager.performAccessibilityShortcut(getId());
        return;
      } 
      throw new IllegalStateException("Unexpected shortcut type");
    } 
    accessibilityManager.notifyAccessibilityButtonClicked(getContext().getDisplayId(), getId());
  }
  
  public void onCheckedChanged(boolean paramBoolean) {
    setShortcutEnabled(paramBoolean);
    if (paramBoolean) {
      ShortcutUtils.optInValueToSettings(getContext(), ShortcutUtils.convertToUserType(getShortcutType()), getId());
    } else {
      ShortcutUtils.optOutValueFromSettings(getContext(), ShortcutUtils.convertToUserType(getShortcutType()), getId());
    } 
  }
  
  public void setShortcutEnabled(boolean paramBoolean) {
    this.mShortcutEnabled = paramBoolean;
  }
  
  public Context getContext() {
    return this.mContext;
  }
  
  public int getShortcutType() {
    return this.mShortcutType;
  }
  
  public int getFragmentType() {
    return this.mFragmentType;
  }
  
  public boolean isShortcutEnabled() {
    return this.mShortcutEnabled;
  }
  
  public String getId() {
    return this.mId;
  }
  
  public CharSequence getLabel() {
    return this.mLabel;
  }
  
  public Drawable getIcon() {
    return this.mIcon;
  }
  
  public String getKey() {
    return this.mKey;
  }
}
