package com.android.internal.accessibility.dialog;

import android.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import com.android.internal.accessibility.util.AccessibilityUtils;
import java.util.ArrayList;
import java.util.List;

public class AccessibilityShortcutChooserActivity extends Activity {
  private AlertDialog mMenuDialog;
  
  private AlertDialog mPermissionDialog;
  
  private final int mShortcutType = 1;
  
  private ShortcutTargetAdapter mTargetAdapter;
  
  private final List<AccessibilityTarget> mTargets = new ArrayList<>();
  
  protected void onCreate(Bundle paramBundle) {
    super.onCreate(paramBundle);
    TypedArray typedArray = getTheme().obtainStyledAttributes(R.styleable.Theme);
    if (!typedArray.getBoolean(38, false))
      requestWindowFeature(1); 
    this.mTargets.addAll(AccessibilityTargetHelper.getTargets((Context)this, 1));
    this.mTargetAdapter = new ShortcutTargetAdapter(this.mTargets);
    AlertDialog alertDialog = createMenuDialog();
    alertDialog.setOnShowListener(new _$$Lambda$AccessibilityShortcutChooserActivity$QHjVdgLm9vFoFVxKPm9eDeb4m68(this));
    this.mMenuDialog.show();
  }
  
  protected void onDestroy() {
    this.mMenuDialog.dismiss();
    super.onDestroy();
  }
  
  private void onTargetSelected(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong) {
    AccessibilityTarget accessibilityTarget = this.mTargets.get(paramInt);
    accessibilityTarget.onSelected();
    this.mMenuDialog.dismiss();
  }
  
  private void onTargetChecked(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong) {
    _$$Lambda$AccessibilityShortcutChooserActivity$ecrvRIv0UfpIIECtsMnGvroliEw _$$Lambda$AccessibilityShortcutChooserActivity$ecrvRIv0UfpIIECtsMnGvroliEw;
    AccessibilityTarget accessibilityTarget = this.mTargets.get(paramInt);
    if (accessibilityTarget instanceof AccessibilityServiceTarget && !accessibilityTarget.isShortcutEnabled()) {
      AlertDialog.Builder builder = new AlertDialog.Builder((Context)this);
      AccessibilityServiceTarget accessibilityServiceTarget = (AccessibilityServiceTarget)accessibilityTarget;
      _$$Lambda$AccessibilityShortcutChooserActivity$ecrvRIv0UfpIIECtsMnGvroliEw = new _$$Lambda$AccessibilityShortcutChooserActivity$ecrvRIv0UfpIIECtsMnGvroliEw(this);
      _$$Lambda$AccessibilityShortcutChooserActivity$pZ2DTppLcNVn85LHvHTc3oReX3o _$$Lambda$AccessibilityShortcutChooserActivity$pZ2DTppLcNVn85LHvHTc3oReX3o = new _$$Lambda$AccessibilityShortcutChooserActivity$pZ2DTppLcNVn85LHvHTc3oReX3o(this);
      builder = builder.setView(AccessibilityTargetHelper.createEnableDialogContentView((Context)this, accessibilityServiceTarget, _$$Lambda$AccessibilityShortcutChooserActivity$ecrvRIv0UfpIIECtsMnGvroliEw, _$$Lambda$AccessibilityShortcutChooserActivity$pZ2DTppLcNVn85LHvHTc3oReX3o));
      AlertDialog alertDialog = builder.create();
      alertDialog.show();
      return;
    } 
    _$$Lambda$AccessibilityShortcutChooserActivity$ecrvRIv0UfpIIECtsMnGvroliEw.onCheckedChanged(_$$Lambda$AccessibilityShortcutChooserActivity$ecrvRIv0UfpIIECtsMnGvroliEw.isShortcutEnabled() ^ true);
    this.mTargetAdapter.notifyDataSetChanged();
  }
  
  private void onDoneButtonClicked() {
    this.mTargets.clear();
    this.mTargets.addAll(AccessibilityTargetHelper.getTargets((Context)this, 1));
    if (this.mTargets.isEmpty()) {
      this.mMenuDialog.dismiss();
      return;
    } 
    this.mTargetAdapter.setShortcutMenuMode(0);
    this.mTargetAdapter.notifyDataSetChanged();
    Button button = this.mMenuDialog.getButton(-1);
    String str = getString(17040098);
    button.setText(str);
    updateDialogListeners();
  }
  
  private void onEditButtonClicked() {
    this.mTargets.clear();
    this.mTargets.addAll(AccessibilityTargetHelper.getInstalledTargets((Context)this, 1));
    this.mTargetAdapter.setShortcutMenuMode(1);
    this.mTargetAdapter.notifyDataSetChanged();
    Button button = this.mMenuDialog.getButton(-1);
    String str = getString(17040084);
    button.setText(str);
    updateDialogListeners();
  }
  
  private void updateDialogListeners() {
    _$$Lambda$AccessibilityShortcutChooserActivity$Kzygz0R0mJ90YuPwTKYJCTqRAEI _$$Lambda$AccessibilityShortcutChooserActivity$Kzygz0R0mJ90YuPwTKYJCTqRAEI;
    _$$Lambda$AccessibilityShortcutChooserActivity$Rk3gwLIkvA4fFbJWs4by4DhZtnA _$$Lambda$AccessibilityShortcutChooserActivity$Rk3gwLIkvA4fFbJWs4by4DhZtnA;
    ShortcutTargetAdapter shortcutTargetAdapter = this.mTargetAdapter;
    int i = shortcutTargetAdapter.getShortcutMenuMode();
    boolean bool = true;
    if (i != 1)
      bool = false; 
    AlertDialog alertDialog = this.mMenuDialog;
    if (bool) {
      i = 17039548;
    } else {
      i = 17039557;
    } 
    alertDialog.setTitle(getString(i));
    Button button = this.mMenuDialog.getButton(-1);
    if (bool) {
      _$$Lambda$AccessibilityShortcutChooserActivity$2QlbmchcPPXlr_l_WdY8qCWUDFc _$$Lambda$AccessibilityShortcutChooserActivity$2QlbmchcPPXlr_l_WdY8qCWUDFc = new _$$Lambda$AccessibilityShortcutChooserActivity$2QlbmchcPPXlr_l_WdY8qCWUDFc(this);
    } else {
      _$$Lambda$AccessibilityShortcutChooserActivity$Kzygz0R0mJ90YuPwTKYJCTqRAEI = new _$$Lambda$AccessibilityShortcutChooserActivity$Kzygz0R0mJ90YuPwTKYJCTqRAEI(this);
    } 
    button.setOnClickListener(_$$Lambda$AccessibilityShortcutChooserActivity$Kzygz0R0mJ90YuPwTKYJCTqRAEI);
    ListView listView = this.mMenuDialog.getListView();
    if (bool) {
      _$$Lambda$AccessibilityShortcutChooserActivity$4u_1K4iiJhwQxpSdVzqRYO0jECM _$$Lambda$AccessibilityShortcutChooserActivity$4u_1K4iiJhwQxpSdVzqRYO0jECM = new _$$Lambda$AccessibilityShortcutChooserActivity$4u_1K4iiJhwQxpSdVzqRYO0jECM(this);
    } else {
      _$$Lambda$AccessibilityShortcutChooserActivity$Rk3gwLIkvA4fFbJWs4by4DhZtnA = new _$$Lambda$AccessibilityShortcutChooserActivity$Rk3gwLIkvA4fFbJWs4by4DhZtnA(this);
    } 
    listView.setOnItemClickListener(_$$Lambda$AccessibilityShortcutChooserActivity$Rk3gwLIkvA4fFbJWs4by4DhZtnA);
  }
  
  private AlertDialog createMenuDialog() {
    String str = getString(17039557);
    AlertDialog.Builder builder2 = new AlertDialog.Builder((Context)this);
    builder2 = builder2.setTitle(str);
    ShortcutTargetAdapter shortcutTargetAdapter = this.mTargetAdapter;
    builder2 = builder2.setAdapter(shortcutTargetAdapter, null);
    _$$Lambda$AccessibilityShortcutChooserActivity$SYqOavSaPTdv3_6SarffTIovKpg _$$Lambda$AccessibilityShortcutChooserActivity$SYqOavSaPTdv3_6SarffTIovKpg = new _$$Lambda$AccessibilityShortcutChooserActivity$SYqOavSaPTdv3_6SarffTIovKpg(this);
    AlertDialog.Builder builder1 = builder2.setOnDismissListener(_$$Lambda$AccessibilityShortcutChooserActivity$SYqOavSaPTdv3_6SarffTIovKpg);
    if (AccessibilityUtils.isUserSetupCompleted((Context)this)) {
      String str1 = getString(17040098);
      builder1.setPositiveButton(str1, null);
    } 
    return builder1.create();
  }
}
