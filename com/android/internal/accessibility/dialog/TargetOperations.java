package com.android.internal.accessibility.dialog;

interface TargetOperations {
  void updateActionItem(TargetAdapter.ViewHolder paramViewHolder, int paramInt);
}
