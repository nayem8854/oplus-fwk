package com.android.internal.accessibility.dialog;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.ComponentName;
import android.content.Context;
import android.widget.Toast;
import com.android.internal.accessibility.util.AccessibilityUtils;
import com.android.internal.accessibility.util.ShortcutUtils;

class VolumeShortcutToggleAccessibilityServiceTarget extends AccessibilityServiceTarget {
  VolumeShortcutToggleAccessibilityServiceTarget(Context paramContext, int paramInt, AccessibilityServiceInfo paramAccessibilityServiceInfo) {
    super(paramContext, paramInt, 0, paramAccessibilityServiceInfo);
  }
  
  public void onCheckedChanged(boolean paramBoolean) {
    int i = getShortcutType();
    if (i != 0) {
      if (i == 1) {
        super.onCheckedChanged(paramBoolean);
        return;
      } 
      throw new IllegalStateException("Unexpected shortcut type");
    } 
    onCheckedFromAccessibilityButton(paramBoolean);
  }
  
  private void onCheckedFromAccessibilityButton(boolean paramBoolean) {
    setShortcutEnabled(paramBoolean);
    ComponentName componentName = ComponentName.unflattenFromString(getId());
    AccessibilityUtils.setAccessibilityServiceState(getContext(), componentName, paramBoolean);
    if (!paramBoolean) {
      ShortcutUtils.optOutValueFromSettings(getContext(), 2, getId());
      Context context = getContext();
      CharSequence charSequence = getLabel();
      String str = context.getString(17039588, new Object[] { charSequence });
      Toast.makeText(getContext(), str, 0).show();
    } 
  }
}
