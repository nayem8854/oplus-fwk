package com.android.internal.accessibility.dialog;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.accessibilityservice.AccessibilityShortcutInfo;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.storage.StorageManager;
import android.text.BidiFormatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.internal.accessibility.AccessibilityShortcutController;
import com.android.internal.accessibility.util.AccessibilityUtils;
import com.android.internal.accessibility.util.ShortcutUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public final class AccessibilityTargetHelper {
  public static List<AccessibilityTarget> getTargets(Context paramContext, int paramInt) {
    List<AccessibilityTarget> list = getInstalledTargets(paramContext, paramInt);
    AccessibilityManager accessibilityManager = (AccessibilityManager)paramContext.getSystemService("accessibility");
    List<String> list1 = accessibilityManager.getAccessibilityShortcutTargets(paramInt);
    ArrayList<AccessibilityTarget> arrayList = new ArrayList();
    for (String str : list1) {
      for (AccessibilityTarget accessibilityTarget : list) {
        if (!"com.android.server.accessibility.MagnificationController".contentEquals(str)) {
          ComponentName componentName1 = ComponentName.unflattenFromString(str);
          String str1 = accessibilityTarget.getId();
          ComponentName componentName2 = ComponentName.unflattenFromString(str1);
          if (componentName1.equals(componentName2)) {
            arrayList.add(accessibilityTarget);
            continue;
          } 
        } 
        if (str.contentEquals(accessibilityTarget.getId()))
          arrayList.add(accessibilityTarget); 
      } 
    } 
    return arrayList;
  }
  
  static List<AccessibilityTarget> getInstalledTargets(Context paramContext, int paramInt) {
    ArrayList<AccessibilityTarget> arrayList = new ArrayList();
    arrayList.addAll(getAccessibilityFilteredTargets(paramContext, paramInt));
    arrayList.addAll(getWhiteListingFeatureTargets(paramContext, paramInt));
    return arrayList;
  }
  
  private static List<AccessibilityTarget> getAccessibilityFilteredTargets(Context paramContext, int paramInt) {
    List<AccessibilityTarget> list2 = getAccessibilityServiceTargets(paramContext, paramInt);
    List<AccessibilityTarget> list1 = getAccessibilityActivityTargets(paramContext, paramInt);
    for (AccessibilityTarget accessibilityTarget : list1)
      list2.removeIf(new _$$Lambda$AccessibilityTargetHelper$eIaGTmpp9XwiG92wEkzn8tORb6k(accessibilityTarget)); 
    ArrayList<AccessibilityTarget> arrayList = new ArrayList();
    arrayList.addAll(list2);
    arrayList.addAll(list1);
    return arrayList;
  }
  
  private static boolean arePackageNameAndLabelTheSame(AccessibilityTarget paramAccessibilityTarget1, AccessibilityTarget paramAccessibilityTarget2) {
    ComponentName componentName1 = ComponentName.unflattenFromString(paramAccessibilityTarget1.getId());
    ComponentName componentName2 = ComponentName.unflattenFromString(paramAccessibilityTarget2.getId());
    String str2 = componentName2.getPackageName();
    String str1 = componentName1.getPackageName();
    boolean bool1 = str2.equals(str1);
    CharSequence charSequence2 = paramAccessibilityTarget2.getLabel();
    CharSequence charSequence1 = paramAccessibilityTarget1.getLabel();
    boolean bool2 = charSequence2.equals(charSequence1);
    if (bool1 && bool2) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    return bool2;
  }
  
  private static List<AccessibilityTarget> getAccessibilityServiceTargets(Context paramContext, int paramInt) {
    AccessibilityManager accessibilityManager = (AccessibilityManager)paramContext.getSystemService("accessibility");
    List<AccessibilityServiceInfo> list = accessibilityManager.getInstalledAccessibilityServiceList();
    if (list == null)
      return Collections.emptyList(); 
    ArrayList<AccessibilityTarget> arrayList = new ArrayList(list.size());
    for (AccessibilityServiceInfo accessibilityServiceInfo : list) {
      boolean bool;
      int i = (accessibilityServiceInfo.getResolveInfo()).serviceInfo.applicationInfo.targetSdkVersion;
      if ((accessibilityServiceInfo.flags & 0x100) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      if (i <= 29 && !bool && paramInt == 0)
        continue; 
      arrayList.add(createAccessibilityServiceTarget(paramContext, paramInt, accessibilityServiceInfo));
    } 
    return arrayList;
  }
  
  private static List<AccessibilityTarget> getAccessibilityActivityTargets(Context paramContext, int paramInt) {
    AccessibilityManager accessibilityManager = (AccessibilityManager)paramContext.getSystemService("accessibility");
    int i = ActivityManager.getCurrentUser();
    List<AccessibilityShortcutInfo> list = accessibilityManager.getInstalledAccessibilityShortcutListAsUser(paramContext, i);
    if (list == null)
      return Collections.emptyList(); 
    ArrayList<AccessibilityActivityTarget> arrayList = new ArrayList(list.size());
    for (AccessibilityShortcutInfo accessibilityShortcutInfo : list)
      arrayList.add(new AccessibilityActivityTarget(paramContext, paramInt, accessibilityShortcutInfo)); 
    return (List)arrayList;
  }
  
  private static List<AccessibilityTarget> getWhiteListingFeatureTargets(Context paramContext, int paramInt) {
    ArrayList<InvisibleToggleWhiteListingFeatureTarget> arrayList = new ArrayList();
    boolean bool = ShortcutUtils.isShortcutContained(paramContext, paramInt, "com.android.server.accessibility.MagnificationController");
    String str1 = paramContext.getString(17039556);
    InvisibleToggleWhiteListingFeatureTarget invisibleToggleWhiteListingFeatureTarget = new InvisibleToggleWhiteListingFeatureTarget(paramContext, paramInt, bool, "com.android.server.accessibility.MagnificationController", str1, paramContext.getDrawable(17302298), "accessibility_display_magnification_navbar_enabled");
    ComponentName componentName2 = AccessibilityShortcutController.DALTONIZER_COMPONENT_NAME;
    String str3 = componentName2.flattenToString();
    bool = ShortcutUtils.isShortcutContained(paramContext, paramInt, str3);
    ComponentName componentName1 = AccessibilityShortcutController.DALTONIZER_COMPONENT_NAME;
    String str6 = componentName1.flattenToString();
    String str2 = paramContext.getString(17039824);
    ToggleWhiteListingFeatureTarget toggleWhiteListingFeatureTarget2 = new ToggleWhiteListingFeatureTarget(paramContext, paramInt, bool, str6, str2, paramContext.getDrawable(17302296), "accessibility_display_daltonizer_enabled");
    ComponentName componentName4 = AccessibilityShortcutController.COLOR_INVERSION_COMPONENT_NAME;
    String str5 = componentName4.flattenToString();
    bool = ShortcutUtils.isShortcutContained(paramContext, paramInt, str5);
    ComponentName componentName3 = AccessibilityShortcutController.COLOR_INVERSION_COMPONENT_NAME;
    String str7 = componentName3.flattenToString();
    String str4 = paramContext.getString(17039825);
    ToggleWhiteListingFeatureTarget toggleWhiteListingFeatureTarget1 = new ToggleWhiteListingFeatureTarget(paramContext, paramInt, bool, str7, str4, paramContext.getDrawable(17302297), "accessibility_display_inversion_enabled");
    arrayList.add(invisibleToggleWhiteListingFeatureTarget);
    arrayList.add(toggleWhiteListingFeatureTarget2);
    arrayList.add(toggleWhiteListingFeatureTarget1);
    return (List)arrayList;
  }
  
  private static AccessibilityTarget createAccessibilityServiceTarget(Context paramContext, int paramInt, AccessibilityServiceInfo paramAccessibilityServiceInfo) {
    int i = AccessibilityUtils.getAccessibilityServiceFragmentType(paramAccessibilityServiceInfo);
    if (i != 0) {
      if (i != 1) {
        if (i == 2)
          return new ToggleAccessibilityServiceTarget(paramContext, paramInt, paramAccessibilityServiceInfo); 
        throw new IllegalStateException("Unexpected fragment type");
      } 
      return new InvisibleToggleAccessibilityServiceTarget(paramContext, paramInt, paramAccessibilityServiceInfo);
    } 
    return new VolumeShortcutToggleAccessibilityServiceTarget(paramContext, paramInt, paramAccessibilityServiceInfo);
  }
  
  static View createEnableDialogContentView(Context paramContext, AccessibilityServiceTarget paramAccessibilityServiceTarget, View.OnClickListener paramOnClickListener1, View.OnClickListener paramOnClickListener2) {
    LayoutInflater layoutInflater = (LayoutInflater)paramContext.getSystemService("layout_inflater");
    View view = layoutInflater.inflate(17367066, (ViewGroup)null);
    TextView textView2 = view.<TextView>findViewById(16908678);
    if (StorageManager.isNonDefaultBlockEncrypted()) {
      CharSequence charSequence1 = getServiceName(paramContext, paramAccessibilityServiceTarget.getLabel());
      charSequence1 = paramContext.getString(17039549, new Object[] { charSequence1 });
      textView2.setText(charSequence1);
      textView2.setVisibility(0);
    } else {
      textView2.setVisibility(8);
    } 
    ImageView imageView = view.<ImageView>findViewById(16908683);
    imageView.setImageDrawable(paramAccessibilityServiceTarget.getIcon());
    TextView textView1 = view.<TextView>findViewById(16908684);
    CharSequence charSequence = getServiceName(paramContext, paramAccessibilityServiceTarget.getLabel());
    textView1.setText(paramContext.getString(17039550, new Object[] { charSequence }));
    textView1 = view.<Button>findViewById(16908685);
    Button button = view.<Button>findViewById(16908686);
    textView1.setOnClickListener(new _$$Lambda$AccessibilityTargetHelper$p07MIbd1XcAGmZDRfNaTs3RXoOc(paramAccessibilityServiceTarget, paramOnClickListener1));
    button.setOnClickListener(new _$$Lambda$AccessibilityTargetHelper$QVEb0xJRKeodTO7fUpIUwWC51jE(paramAccessibilityServiceTarget, paramOnClickListener2));
    return view;
  }
  
  private static CharSequence getServiceName(Context paramContext, CharSequence paramCharSequence) {
    Locale locale = paramContext.getResources().getConfiguration().getLocales().get(0);
    return BidiFormatter.getInstance(locale).unicodeWrap(paramCharSequence);
  }
}
