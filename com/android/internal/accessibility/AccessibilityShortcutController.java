package com.android.internal.accessibility;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.ActivityManager;
import android.app.ActivityThread;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.media.AudioAttributes;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.speech.tts.Voice;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Slog;
import android.view.Window;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityManager;
import android.widget.Toast;
import com.android.internal.accessibility.dialog.AccessibilityTarget;
import com.android.internal.accessibility.dialog.AccessibilityTargetHelper;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.function.pooled.PooledLambda;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Consumer;

public class AccessibilityShortcutController {
  public static final ComponentName COLOR_INVERSION_COMPONENT_NAME = new ComponentName("com.android.server.accessibility", "ColorInversion");
  
  public static final ComponentName DALTONIZER_COMPONENT_NAME = new ComponentName("com.android.server.accessibility", "Daltonizer");
  
  public static final ComponentName MAGNIFICATION_COMPONENT_NAME = new ComponentName("com.android.server.accessibility", "Magnification");
  
  public static final String MAGNIFICATION_CONTROLLER_NAME = "com.android.server.accessibility.MagnificationController";
  
  private static final String TAG = "AccessibilityShortcutController";
  
  private static final AudioAttributes VIBRATION_ATTRIBUTES;
  
  private static Map<ComponentName, ToggleableFrameworkFeatureInfo> sFrameworkShortcutFeaturesMap;
  
  private AlertDialog mAlertDialog;
  
  private final Context mContext;
  
  private boolean mEnabledOnLockScreen;
  
  static {
    AudioAttributes.Builder builder = new AudioAttributes.Builder();
    builder = builder.setContentType(4);
    builder = builder.setUsage(11);
    VIBRATION_ATTRIBUTES = builder.build();
  }
  
  public FrameworkObjectProvider mFrameworkObjectProvider = new FrameworkObjectProvider();
  
  private final Handler mHandler;
  
  private boolean mIsShortcutEnabled;
  
  private int mUserId;
  
  public static Map<ComponentName, ToggleableFrameworkFeatureInfo> getFrameworkShortcutFeaturesMap() {
    if (sFrameworkShortcutFeaturesMap == null) {
      ArrayMap<Object, Object> arrayMap = new ArrayMap<>(2);
      arrayMap.put(COLOR_INVERSION_COMPONENT_NAME, new ToggleableFrameworkFeatureInfo("accessibility_display_inversion_enabled", "1", "0", 17039825));
      arrayMap.put(DALTONIZER_COMPONENT_NAME, new ToggleableFrameworkFeatureInfo("accessibility_display_daltonizer_enabled", "1", "0", 17039824));
      sFrameworkShortcutFeaturesMap = Collections.unmodifiableMap(arrayMap);
    } 
    return sFrameworkShortcutFeaturesMap;
  }
  
  public AccessibilityShortcutController(Context paramContext, Handler paramHandler, int paramInt) {
    this.mContext = paramContext;
    this.mHandler = paramHandler;
    this.mUserId = paramInt;
    Object object = new Object(this, paramHandler);
    ContentResolver contentResolver1 = this.mContext.getContentResolver();
    Uri uri2 = Settings.Secure.getUriFor("accessibility_shortcut_target_service");
    contentResolver1.registerContentObserver(uri2, false, (ContentObserver)object, -1);
    contentResolver1 = this.mContext.getContentResolver();
    uri2 = Settings.Secure.getUriFor("accessibility_shortcut_on_lock_screen");
    contentResolver1.registerContentObserver(uri2, false, (ContentObserver)object, -1);
    ContentResolver contentResolver2 = this.mContext.getContentResolver();
    Uri uri1 = Settings.Secure.getUriFor("accessibility_shortcut_dialog_shown");
    contentResolver2.registerContentObserver(uri1, false, (ContentObserver)object, -1);
    setCurrentUser(this.mUserId);
  }
  
  public void setCurrentUser(int paramInt) {
    this.mUserId = paramInt;
    onSettingsChanged();
  }
  
  public boolean isAccessibilityShortcutAvailable(boolean paramBoolean) {
    if (this.mIsShortcutEnabled && (!paramBoolean || this.mEnabledOnLockScreen)) {
      paramBoolean = true;
    } else {
      paramBoolean = false;
    } 
    return paramBoolean;
  }
  
  public void onSettingsChanged() {
    boolean bool = hasShortcutTarget();
    ContentResolver contentResolver = this.mContext.getContentResolver();
    int i = this.mUserId;
    boolean bool1 = false;
    i = Settings.Secure.getIntForUser(contentResolver, "accessibility_shortcut_dialog_shown", 0, i);
    if (Settings.Secure.getIntForUser(contentResolver, "accessibility_shortcut_on_lock_screen", i, this.mUserId) == 1)
      bool1 = true; 
    this.mEnabledOnLockScreen = bool1;
    this.mIsShortcutEnabled = bool;
  }
  
  public void performAccessibilityShortcut() {
    Slog.d("AccessibilityShortcutController", "Accessibility shortcut activated");
    ContentResolver contentResolver = this.mContext.getContentResolver();
    int i = ActivityManager.getCurrentUser();
    int j = Settings.Secure.getIntForUser(contentResolver, "accessibility_shortcut_dialog_shown", 0, i);
    Vibrator vibrator = (Vibrator)this.mContext.getSystemService("vibrator");
    if (vibrator != null && vibrator.hasVibrator()) {
      Context context = this.mContext;
      int[] arrayOfInt = context.getResources().getIntArray(17236049);
      long[] arrayOfLong = ArrayUtils.convertToLongArray(arrayOfInt);
      vibrator.vibrate(arrayOfLong, -1, VIBRATION_ATTRIBUTES);
    } 
    if (j == 0) {
      AlertDialog alertDialog = createShortcutWarningDialog(i);
      if (alertDialog == null)
        return; 
      if (!performTtsPrompt(alertDialog))
        playNotificationTone(); 
      Window window = this.mAlertDialog.getWindow();
      WindowManager.LayoutParams layoutParams = window.getAttributes();
      layoutParams.type = 2009;
      window.setAttributes(layoutParams);
      this.mAlertDialog.show();
      Settings.Secure.putIntForUser(contentResolver, "accessibility_shortcut_dialog_shown", 1, i);
    } else {
      playNotificationTone();
      AlertDialog alertDialog = this.mAlertDialog;
      if (alertDialog != null) {
        alertDialog.dismiss();
        this.mAlertDialog = null;
      } 
      showToast();
      AccessibilityManager accessibilityManager = this.mFrameworkObjectProvider.getAccessibilityManagerInstance(this.mContext);
      accessibilityManager.performAccessibilityShortcut();
    } 
  }
  
  private void showToast() {
    int i;
    AccessibilityServiceInfo accessibilityServiceInfo = getInfoForTargetService();
    if (accessibilityServiceInfo == null)
      return; 
    String str2 = getShortcutFeatureDescription(false);
    if (str2 == null)
      return; 
    if ((accessibilityServiceInfo.flags & 0x100) != 0) {
      i = 1;
    } else {
      i = 0;
    } 
    boolean bool = isServiceEnabled(accessibilityServiceInfo);
    if ((accessibilityServiceInfo.getResolveInfo()).serviceInfo.applicationInfo.targetSdkVersion > 29 && i && bool)
      return; 
    Context context = this.mContext;
    if (bool) {
      i = 17039563;
    } else {
      i = 17039564;
    } 
    String str1 = context.getString(i);
    str2 = String.format(str1, new Object[] { str2 });
    Toast toast = this.mFrameworkObjectProvider.makeToastFromText(this.mContext, str2, 1);
    toast.show();
  }
  
  private AlertDialog createShortcutWarningDialog(int paramInt) {
    List<AccessibilityTarget> list = AccessibilityTargetHelper.getTargets(this.mContext, 1);
    if (list.size() == 0)
      return null; 
    FrameworkObjectProvider frameworkObjectProvider = this.mFrameworkObjectProvider;
    Context context = frameworkObjectProvider.getSystemUiContext();
    AlertDialog.Builder builder4 = frameworkObjectProvider.getAlertDialogBuilder(context);
    builder4 = builder4.setTitle(getShortcutWarningTitle(list));
    AlertDialog.Builder builder2 = builder4.setMessage(getShortcutWarningMessage(list));
    builder2 = builder2.setCancelable(false);
    builder2 = builder2.setNegativeButton(17039571, null);
    _$$Lambda$AccessibilityShortcutController$2NcDVJHkpsPbwr45v1_NfIM8row _$$Lambda$AccessibilityShortcutController$2NcDVJHkpsPbwr45v1_NfIM8row = new _$$Lambda$AccessibilityShortcutController$2NcDVJHkpsPbwr45v1_NfIM8row(this, paramInt);
    AlertDialog.Builder builder3 = builder2.setPositiveButton(17039570, _$$Lambda$AccessibilityShortcutController$2NcDVJHkpsPbwr45v1_NfIM8row);
    _$$Lambda$AccessibilityShortcutController$T96D356_n5VObNOonEIYV8s83Fc _$$Lambda$AccessibilityShortcutController$T96D356_n5VObNOonEIYV8s83Fc = new _$$Lambda$AccessibilityShortcutController$T96D356_n5VObNOonEIYV8s83Fc(this, paramInt);
    AlertDialog.Builder builder1 = builder3.setOnCancelListener(_$$Lambda$AccessibilityShortcutController$T96D356_n5VObNOonEIYV8s83Fc);
    return builder1.create();
  }
  
  private String getShortcutWarningTitle(List<AccessibilityTarget> paramList) {
    if (paramList.size() == 1) {
      Context context = this.mContext;
      CharSequence charSequence = ((AccessibilityTarget)paramList.get(0)).getLabel();
      return context.getString(17039573, new Object[] { charSequence });
    } 
    return this.mContext.getString(17039569);
  }
  
  private String getShortcutWarningMessage(List<AccessibilityTarget> paramList) {
    CharSequence charSequence;
    if (paramList.size() == 1) {
      Context context1 = this.mContext;
      charSequence = ((AccessibilityTarget)paramList.get(0)).getLabel();
      return context1.getString(17039572, new Object[] { charSequence });
    } 
    StringBuilder stringBuilder = new StringBuilder();
    for (AccessibilityTarget accessibilityTarget : charSequence) {
      Context context1 = this.mContext;
      CharSequence charSequence1 = accessibilityTarget.getLabel();
      stringBuilder.append(context1.getString(17039567, new Object[] { charSequence1 }));
    } 
    Context context = this.mContext;
    String str = stringBuilder.toString();
    return context.getString(17039568, new Object[] { str });
  }
  
  private AccessibilityServiceInfo getInfoForTargetService() {
    ComponentName componentName = getShortcutTargetComponentName();
    if (componentName == null)
      return null; 
    FrameworkObjectProvider frameworkObjectProvider = this.mFrameworkObjectProvider;
    Context context = this.mContext;
    AccessibilityManager accessibilityManager = frameworkObjectProvider.getAccessibilityManagerInstance(context);
    return accessibilityManager.getInstalledServiceInfoWithComponentName(componentName);
  }
  
  private String getShortcutFeatureDescription(boolean paramBoolean) {
    ComponentName componentName = getShortcutTargetComponentName();
    if (componentName == null)
      return null; 
    ToggleableFrameworkFeatureInfo toggleableFrameworkFeatureInfo = getFrameworkShortcutFeaturesMap().get(componentName);
    if (toggleableFrameworkFeatureInfo != null)
      return toggleableFrameworkFeatureInfo.getLabel(this.mContext); 
    FrameworkObjectProvider frameworkObjectProvider = this.mFrameworkObjectProvider;
    Context context = this.mContext;
    AccessibilityServiceInfo accessibilityServiceInfo = frameworkObjectProvider.getAccessibilityManagerInstance(context).getInstalledServiceInfoWithComponentName(componentName);
    if (accessibilityServiceInfo == null)
      return null; 
    PackageManager packageManager = this.mContext.getPackageManager();
    String str = accessibilityServiceInfo.getResolveInfo().loadLabel(packageManager).toString();
    CharSequence charSequence = accessibilityServiceInfo.loadSummary(packageManager);
    if (!paramBoolean || TextUtils.isEmpty(charSequence))
      return str; 
    return String.format("%s\n%s", new Object[] { str, charSequence });
  }
  
  private boolean isServiceEnabled(AccessibilityServiceInfo paramAccessibilityServiceInfo) {
    FrameworkObjectProvider frameworkObjectProvider = this.mFrameworkObjectProvider;
    Context context = this.mContext;
    AccessibilityManager accessibilityManager = frameworkObjectProvider.getAccessibilityManagerInstance(context);
    List<AccessibilityServiceInfo> list = accessibilityManager.getEnabledAccessibilityServiceList(-1);
    return 
      list.contains(paramAccessibilityServiceInfo);
  }
  
  private boolean hasFeatureLeanback() {
    return this.mContext.getPackageManager().hasSystemFeature("android.software.leanback");
  }
  
  private void playNotificationTone() {
    byte b;
    if (hasFeatureLeanback()) {
      b = 11;
    } else {
      b = 10;
    } 
    Ringtone ringtone = this.mFrameworkObjectProvider.getRingtone(this.mContext, Settings.System.DEFAULT_NOTIFICATION_URI);
    if (ringtone != null) {
      AudioAttributes.Builder builder = new AudioAttributes.Builder();
      builder = builder.setUsage(b);
      AudioAttributes audioAttributes = builder.build();
      ringtone.setAudioAttributes(audioAttributes);
      ringtone.play();
    } 
  }
  
  private boolean performTtsPrompt(AlertDialog paramAlertDialog) {
    String str = getShortcutFeatureDescription(false);
    AccessibilityServiceInfo accessibilityServiceInfo = getInfoForTargetService();
    if (TextUtils.isEmpty(str) || accessibilityServiceInfo == null)
      return false; 
    if ((accessibilityServiceInfo.flags & 0x400) == 0)
      return false; 
    TtsPrompt ttsPrompt = new TtsPrompt(str);
    paramAlertDialog.setOnDismissListener(new _$$Lambda$AccessibilityShortcutController$cQtLiNhDc4H3BvMBZy00zj21oKg(ttsPrompt));
    return true;
  }
  
  private boolean hasShortcutTarget() {
    String str1 = Settings.Secure.getStringForUser(this.mContext.getContentResolver(), "accessibility_shortcut_target_service", this.mUserId);
    String str2 = str1;
    if (str1 == null)
      str2 = this.mContext.getString(17039868); 
    return TextUtils.isEmpty(str2) ^ true;
  }
  
  private ComponentName getShortcutTargetComponentName() {
    FrameworkObjectProvider frameworkObjectProvider = this.mFrameworkObjectProvider;
    Context context = this.mContext;
    AccessibilityManager accessibilityManager = frameworkObjectProvider.getAccessibilityManagerInstance(context);
    List<String> list = accessibilityManager.getAccessibilityShortcutTargets(1);
    if (list.size() != 1)
      return null; 
    return ComponentName.unflattenFromString(list.get(0));
  }
  
  class TtsPrompt implements TextToSpeech.OnInitListener {
    final AccessibilityShortcutController this$0;
    
    private TextToSpeech mTts;
    
    private final CharSequence mText;
    
    private int mRetryCount = 3;
    
    private boolean mLanguageReady = false;
    
    private boolean mDismiss;
    
    private static final int RETRY_MILLIS = 1000;
    
    TtsPrompt(String param1String) {
      this.mText = AccessibilityShortcutController.this.mContext.getString(17039574, new Object[] { param1String });
      this.mTts = AccessibilityShortcutController.this.mFrameworkObjectProvider.getTextToSpeech(AccessibilityShortcutController.this.mContext, this);
    }
    
    public void dismiss() {
      this.mDismiss = true;
      AccessibilityShortcutController.this.mHandler.sendMessage(PooledLambda.obtainMessage((Consumer)_$$Lambda$qdzoyIBhDB17ZFWPp1Rf8ICv_R8.INSTANCE, this.mTts));
    }
    
    public void onInit(int param1Int) {
      if (param1Int != 0) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Tts init fail, status=");
        stringBuilder.append(Integer.toString(param1Int));
        Slog.d("AccessibilityShortcutController", stringBuilder.toString());
        AccessibilityShortcutController.this.playNotificationTone();
        return;
      } 
      AccessibilityShortcutController.this.mHandler.sendMessage(PooledLambda.obtainMessage((Consumer)_$$Lambda$AccessibilityShortcutController$TtsPrompt$Ls8APHnBqFb3_dkjhe9CHYaDi7g.INSTANCE, this));
    }
    
    private void play() {
      if (this.mDismiss)
        return; 
      int i = this.mTts.speak(this.mText, 0, null, null);
      if (i != 0) {
        Slog.d("AccessibilityShortcutController", "Tts play fail");
        AccessibilityShortcutController.this.playNotificationTone();
      } 
    }
    
    private void waitForTtsReady() {
      if (this.mDismiss)
        return; 
      boolean bool = this.mLanguageReady;
      int i = 0;
      if (!bool) {
        int j = this.mTts.setLanguage(Locale.getDefault());
        if (j != -1 && j != -2) {
          bool = true;
        } else {
          bool = false;
        } 
        this.mLanguageReady = bool;
      } 
      if (this.mLanguageReady) {
        Voice voice = this.mTts.getVoice();
        if (voice != null && 
          voice.getFeatures() != null && 
          !voice.getFeatures().contains("notInstalled"))
          i = 1; 
        if (i) {
          AccessibilityShortcutController.this.mHandler.sendMessage(PooledLambda.obtainMessage((Consumer)_$$Lambda$AccessibilityShortcutController$TtsPrompt$HwizF4cvqRFiaqAcMrC7W8y6zYA.INSTANCE, this));
          return;
        } 
      } 
      i = this.mRetryCount;
      if (i == 0) {
        Slog.d("AccessibilityShortcutController", "Tts not ready to speak.");
        AccessibilityShortcutController.this.playNotificationTone();
        return;
      } 
      this.mRetryCount = i - 1;
      AccessibilityShortcutController.this.mHandler.sendMessageDelayed(PooledLambda.obtainMessage((Consumer)_$$Lambda$AccessibilityShortcutController$TtsPrompt$Ls8APHnBqFb3_dkjhe9CHYaDi7g.INSTANCE, this), 1000L);
    }
  }
  
  public static class ToggleableFrameworkFeatureInfo {
    private int mIconDrawableId;
    
    private final int mLabelStringResourceId;
    
    private final String mSettingKey;
    
    private final String mSettingOffValue;
    
    private final String mSettingOnValue;
    
    ToggleableFrameworkFeatureInfo(String param1String1, String param1String2, String param1String3, int param1Int) {
      this.mSettingKey = param1String1;
      this.mSettingOnValue = param1String2;
      this.mSettingOffValue = param1String3;
      this.mLabelStringResourceId = param1Int;
    }
    
    public String getSettingKey() {
      return this.mSettingKey;
    }
    
    public String getSettingOnValue() {
      return this.mSettingOnValue;
    }
    
    public String getSettingOffValue() {
      return this.mSettingOffValue;
    }
    
    public String getLabel(Context param1Context) {
      return param1Context.getString(this.mLabelStringResourceId);
    }
  }
  
  public static class FrameworkObjectProvider {
    public AccessibilityManager getAccessibilityManagerInstance(Context param1Context) {
      return AccessibilityManager.getInstance(param1Context);
    }
    
    public AlertDialog.Builder getAlertDialogBuilder(Context param1Context) {
      return new AlertDialog.Builder(param1Context);
    }
    
    public Toast makeToastFromText(Context param1Context, CharSequence param1CharSequence, int param1Int) {
      return Toast.makeText(param1Context, param1CharSequence, param1Int);
    }
    
    public Context getSystemUiContext() {
      return (Context)ActivityThread.currentActivityThread().getSystemUiContext();
    }
    
    public TextToSpeech getTextToSpeech(Context param1Context, TextToSpeech.OnInitListener param1OnInitListener) {
      return new TextToSpeech(param1Context, param1OnInitListener);
    }
    
    public Ringtone getRingtone(Context param1Context, Uri param1Uri) {
      return RingtoneManager.getRingtone(param1Context, param1Uri);
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  private static @interface DialogStaus {
    public static final int NOT_SHOWN = 0;
    
    public static final int SHOWN = 1;
  }
}
