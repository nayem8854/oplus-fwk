package com.android.internal.accessibility.util;

import android.content.ComponentName;
import com.android.internal.accessibility.AccessibilityShortcutController;
import com.android.internal.util.FrameworkStatsLog;

public final class AccessibilityStatsLogUtils {
  private static final int UNKNOWN_STATUS = 0;
  
  public static void logAccessibilityShortcutActivated(ComponentName paramComponentName, int paramInt) {
    logAccessibilityShortcutActivated(paramComponentName, paramInt, 0);
  }
  
  public static void logAccessibilityShortcutActivated(ComponentName paramComponentName, int paramInt, boolean paramBoolean) {
    int i = convertToLoggingServiceStatus(paramBoolean);
    logAccessibilityShortcutActivated(paramComponentName, paramInt, i);
  }
  
  private static void logAccessibilityShortcutActivated(ComponentName paramComponentName, int paramInt1, int paramInt2) {
    String str = paramComponentName.flattenToString();
    paramInt1 = convertToLoggingShortcutType(paramInt1);
    FrameworkStatsLog.write(266, str, paramInt1, paramInt2);
  }
  
  public static void logMagnificationTripleTap(boolean paramBoolean) {
    ComponentName componentName = AccessibilityShortcutController.MAGNIFICATION_COMPONENT_NAME;
    String str = componentName.flattenToString();
    int i = convertToLoggingServiceStatus(paramBoolean);
    FrameworkStatsLog.write(266, str, 3, i);
  }
  
  public static void logAccessibilityButtonLongPressStatus(ComponentName paramComponentName) {
    String str = paramComponentName.flattenToString();
    FrameworkStatsLog.write(266, str, 4, 0);
  }
  
  private static int convertToLoggingShortcutType(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1)
        return 0; 
      return 2;
    } 
    return 1;
  }
  
  private static int convertToLoggingServiceStatus(boolean paramBoolean) {
    byte b;
    if (paramBoolean) {
      b = 1;
    } else {
      b = 2;
    } 
    return b;
  }
}
