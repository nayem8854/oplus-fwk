package com.android.internal.accessibility.util;

import android.content.Context;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.accessibility.AccessibilityManager;
import java.util.List;
import java.util.StringJoiner;

public final class ShortcutUtils {
  private static final TextUtils.SimpleStringSplitter sStringColonSplitter = new TextUtils.SimpleStringSplitter(':');
  
  public static void optInValueToSettings(Context paramContext, int paramInt, String paramString) {
    StringJoiner stringJoiner = new StringJoiner(String.valueOf(':'));
    String str1 = convertToKey(paramInt);
    String str2 = Settings.Secure.getString(paramContext.getContentResolver(), str1);
    if (isComponentIdExistingInSettings(paramContext, paramInt, paramString))
      return; 
    if (!TextUtils.isEmpty(str2))
      stringJoiner.add(str2); 
    stringJoiner.add(paramString);
    Settings.Secure.putString(paramContext.getContentResolver(), str1, stringJoiner.toString());
  }
  
  public static void optOutValueFromSettings(Context paramContext, int paramInt, String paramString) {
    StringJoiner stringJoiner = new StringJoiner(String.valueOf(':'));
    String str1 = convertToKey(paramInt);
    String str2 = Settings.Secure.getString(paramContext.getContentResolver(), str1);
    if (TextUtils.isEmpty(str2))
      return; 
    sStringColonSplitter.setString(str2);
    while (sStringColonSplitter.hasNext()) {
      str2 = sStringColonSplitter.next();
      if (TextUtils.isEmpty(str2) || paramString.equals(str2))
        continue; 
      stringJoiner.add(str2);
    } 
    Settings.Secure.putString(paramContext.getContentResolver(), str1, stringJoiner.toString());
  }
  
  public static boolean isComponentIdExistingInSettings(Context paramContext, int paramInt, String paramString) {
    String str2 = convertToKey(paramInt);
    String str1 = Settings.Secure.getString(paramContext.getContentResolver(), str2);
    if (TextUtils.isEmpty(str1))
      return false; 
    sStringColonSplitter.setString(str1);
    while (sStringColonSplitter.hasNext()) {
      str1 = sStringColonSplitter.next();
      if (paramString.equals(str1))
        return true; 
    } 
    return false;
  }
  
  public static boolean isShortcutContained(Context paramContext, int paramInt, String paramString) {
    AccessibilityManager accessibilityManager = (AccessibilityManager)paramContext.getSystemService("accessibility");
    List<String> list = accessibilityManager.getAccessibilityShortcutTargets(paramInt);
    return list.contains(paramString);
  }
  
  public static String convertToKey(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt == 4)
          return "accessibility_display_magnification_enabled"; 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unsupported user shortcut type: ");
        stringBuilder.append(paramInt);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      return "accessibility_shortcut_target_service";
    } 
    return "accessibility_button_targets";
  }
  
  public static int convertToUserType(int paramInt) {
    if (paramInt != 0) {
      if (paramInt == 1)
        return 2; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unsupported shortcut type:");
      stringBuilder.append(paramInt);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    return 1;
  }
}
