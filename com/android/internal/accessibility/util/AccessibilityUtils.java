package com.android.internal.accessibility.util;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.os.UserHandle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.ArraySet;
import android.view.accessibility.AccessibilityManager;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class AccessibilityUtils {
  public static Set<ComponentName> getEnabledServicesFromSettings(Context paramContext, int paramInt) {
    ContentResolver contentResolver = paramContext.getContentResolver();
    String str = Settings.Secure.getStringForUser(contentResolver, "enabled_accessibility_services", paramInt);
    if (TextUtils.isEmpty(str))
      return Collections.emptySet(); 
    HashSet<ComponentName> hashSet = new HashSet();
    TextUtils.SimpleStringSplitter simpleStringSplitter = new TextUtils.SimpleStringSplitter(':');
    simpleStringSplitter.setString(str);
    for (String str1 : simpleStringSplitter) {
      ComponentName componentName = ComponentName.unflattenFromString(str1);
      if (componentName != null)
        hashSet.add(componentName); 
    } 
    return hashSet;
  }
  
  public static void setAccessibilityServiceState(Context paramContext, ComponentName paramComponentName, boolean paramBoolean) {
    setAccessibilityServiceState(paramContext, paramComponentName, paramBoolean, UserHandle.myUserId());
  }
  
  public static void setAccessibilityServiceState(Context paramContext, ComponentName paramComponentName, boolean paramBoolean, int paramInt) {
    Set<ComponentName> set1 = getEnabledServicesFromSettings(paramContext, paramInt);
    Set<ComponentName> set2 = set1;
    if (set1.isEmpty())
      set2 = new ArraySet<>(1); 
    if (paramBoolean) {
      set2.add(paramComponentName);
    } else {
      set2.remove(paramComponentName);
    } 
    StringBuilder stringBuilder = new StringBuilder();
    for (ComponentName componentName : set2) {
      stringBuilder.append(componentName.flattenToString());
      stringBuilder.append(':');
    } 
    int i = stringBuilder.length();
    if (i > 0)
      stringBuilder.deleteCharAt(i - 1); 
    ContentResolver contentResolver = paramContext.getContentResolver();
    String str = stringBuilder.toString();
    Settings.Secure.putStringForUser(contentResolver, "enabled_accessibility_services", str, paramInt);
  }
  
  public static int getAccessibilityServiceFragmentType(AccessibilityServiceInfo paramAccessibilityServiceInfo) {
    int i = (paramAccessibilityServiceInfo.getResolveInfo()).serviceInfo.applicationInfo.targetSdkVersion;
    int j = paramAccessibilityServiceInfo.flags;
    boolean bool = true;
    if ((j & 0x100) != 0) {
      j = 1;
    } else {
      j = 0;
    } 
    if (i <= 29)
      return 0; 
    if (j != 0) {
      j = bool;
    } else {
      j = 2;
    } 
    return j;
  }
  
  public static boolean isAccessibilityServiceEnabled(Context paramContext, String paramString) {
    AccessibilityManager accessibilityManager = (AccessibilityManager)paramContext.getSystemService("accessibility");
    List<AccessibilityServiceInfo> list = accessibilityManager.getEnabledAccessibilityServiceList(-1);
    for (AccessibilityServiceInfo accessibilityServiceInfo : list) {
      String str = accessibilityServiceInfo.getComponentName().flattenToString();
      if (str.equals(paramString))
        return true; 
    } 
    return false;
  }
  
  public static boolean isUserSetupCompleted(Context paramContext) {
    ContentResolver contentResolver = paramContext.getContentResolver();
    boolean bool = false;
    if (Settings.Secure.getIntForUser(contentResolver, "user_setup_complete", 0, -2) != 0)
      bool = true; 
    return bool;
  }
}
