package com.android.internal.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.CLASS)
public @interface VisibleForTesting {
  Visibility visibility() default Visibility.PRIVATE;
  
  public enum Visibility {
    PACKAGE, PRIVATE, PROTECTED;
    
    private static final Visibility[] $VALUES;
    
    static {
      Visibility visibility = new Visibility("PRIVATE", 2);
      $VALUES = new Visibility[] { PROTECTED, PACKAGE, visibility };
    }
  }
}
