package com.android.internal.telecom;

import android.content.ComponentName;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface RemoteServiceCallback extends IInterface {
  void onError() throws RemoteException;
  
  void onResult(List<ComponentName> paramList, List<IBinder> paramList1) throws RemoteException;
  
  class Default implements RemoteServiceCallback {
    public void onError() throws RemoteException {}
    
    public void onResult(List<ComponentName> param1List, List<IBinder> param1List1) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements RemoteServiceCallback {
    private static final String DESCRIPTOR = "com.android.internal.telecom.RemoteServiceCallback";
    
    static final int TRANSACTION_onError = 1;
    
    static final int TRANSACTION_onResult = 2;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telecom.RemoteServiceCallback");
    }
    
    public static RemoteServiceCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telecom.RemoteServiceCallback");
      if (iInterface != null && iInterface instanceof RemoteServiceCallback)
        return (RemoteServiceCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onResult";
      } 
      return "onError";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      ArrayList<IBinder> arrayList;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("com.android.internal.telecom.RemoteServiceCallback");
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.internal.telecom.RemoteServiceCallback");
        ArrayList<ComponentName> arrayList1 = param1Parcel1.createTypedArrayList(ComponentName.CREATOR);
        arrayList = param1Parcel1.createBinderArrayList();
        onResult(arrayList1, arrayList);
        return true;
      } 
      arrayList.enforceInterface("com.android.internal.telecom.RemoteServiceCallback");
      onError();
      return true;
    }
    
    private static class Proxy implements RemoteServiceCallback {
      public static RemoteServiceCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telecom.RemoteServiceCallback";
      }
      
      public void onError() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.RemoteServiceCallback");
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && RemoteServiceCallback.Stub.getDefaultImpl() != null) {
            RemoteServiceCallback.Stub.getDefaultImpl().onError();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onResult(List<ComponentName> param2List, List<IBinder> param2List1) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.RemoteServiceCallback");
          parcel.writeTypedList(param2List);
          parcel.writeBinderList(param2List1);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && RemoteServiceCallback.Stub.getDefaultImpl() != null) {
            RemoteServiceCallback.Stub.getDefaultImpl().onResult(param2List, param2List1);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(RemoteServiceCallback param1RemoteServiceCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1RemoteServiceCallback != null) {
          Proxy.sDefaultImpl = param1RemoteServiceCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static RemoteServiceCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
