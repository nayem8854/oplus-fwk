package com.android.internal.telecom;

import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telecom.ConnectionRequest;
import android.telecom.DisconnectCause;
import android.telecom.Logging.Session;
import android.telecom.ParcelableConference;
import android.telecom.ParcelableConnection;
import android.telecom.PhoneAccountHandle;
import android.telecom.StatusHints;
import java.util.ArrayList;
import java.util.List;

public interface IConnectionServiceAdapter extends IInterface {
  void addConferenceCall(String paramString, ParcelableConference paramParcelableConference, Session.Info paramInfo) throws RemoteException;
  
  void addExistingConnection(String paramString, ParcelableConnection paramParcelableConnection, Session.Info paramInfo) throws RemoteException;
  
  void handleCreateConferenceComplete(String paramString, ConnectionRequest paramConnectionRequest, ParcelableConference paramParcelableConference, Session.Info paramInfo) throws RemoteException;
  
  void handleCreateConnectionComplete(String paramString, ConnectionRequest paramConnectionRequest, ParcelableConnection paramParcelableConnection, Session.Info paramInfo) throws RemoteException;
  
  void onConnectionEvent(String paramString1, String paramString2, Bundle paramBundle, Session.Info paramInfo) throws RemoteException;
  
  void onConnectionServiceFocusReleased(Session.Info paramInfo) throws RemoteException;
  
  void onPhoneAccountChanged(String paramString, PhoneAccountHandle paramPhoneAccountHandle, Session.Info paramInfo) throws RemoteException;
  
  void onPostDialChar(String paramString, char paramChar, Session.Info paramInfo) throws RemoteException;
  
  void onPostDialWait(String paramString1, String paramString2, Session.Info paramInfo) throws RemoteException;
  
  void onRemoteRttRequest(String paramString, Session.Info paramInfo) throws RemoteException;
  
  void onRttInitiationFailure(String paramString, int paramInt, Session.Info paramInfo) throws RemoteException;
  
  void onRttInitiationSuccess(String paramString, Session.Info paramInfo) throws RemoteException;
  
  void onRttSessionRemotelyTerminated(String paramString, Session.Info paramInfo) throws RemoteException;
  
  void putExtras(String paramString, Bundle paramBundle, Session.Info paramInfo) throws RemoteException;
  
  void queryRemoteConnectionServices(RemoteServiceCallback paramRemoteServiceCallback, String paramString, Session.Info paramInfo) throws RemoteException;
  
  void removeCall(String paramString, Session.Info paramInfo) throws RemoteException;
  
  void removeExtras(String paramString, List<String> paramList, Session.Info paramInfo) throws RemoteException;
  
  void resetConnectionTime(String paramString, Session.Info paramInfo) throws RemoteException;
  
  void setActive(String paramString, Session.Info paramInfo) throws RemoteException;
  
  void setAddress(String paramString, Uri paramUri, int paramInt, Session.Info paramInfo) throws RemoteException;
  
  void setAudioRoute(String paramString1, int paramInt, String paramString2, Session.Info paramInfo) throws RemoteException;
  
  void setCallDirection(String paramString, int paramInt, Session.Info paramInfo) throws RemoteException;
  
  void setCallerDisplayName(String paramString1, String paramString2, int paramInt, Session.Info paramInfo) throws RemoteException;
  
  void setConferenceMergeFailed(String paramString, Session.Info paramInfo) throws RemoteException;
  
  void setConferenceState(String paramString, boolean paramBoolean, Session.Info paramInfo) throws RemoteException;
  
  void setConferenceableConnections(String paramString, List<String> paramList, Session.Info paramInfo) throws RemoteException;
  
  void setConnectionCapabilities(String paramString, int paramInt, Session.Info paramInfo) throws RemoteException;
  
  void setConnectionProperties(String paramString, int paramInt, Session.Info paramInfo) throws RemoteException;
  
  void setDialing(String paramString, Session.Info paramInfo) throws RemoteException;
  
  void setDisconnected(String paramString, DisconnectCause paramDisconnectCause, Session.Info paramInfo) throws RemoteException;
  
  void setIsConferenced(String paramString1, String paramString2, Session.Info paramInfo) throws RemoteException;
  
  void setIsVoipAudioMode(String paramString, boolean paramBoolean, Session.Info paramInfo) throws RemoteException;
  
  void setOnHold(String paramString, Session.Info paramInfo) throws RemoteException;
  
  void setPulling(String paramString, Session.Info paramInfo) throws RemoteException;
  
  void setRingbackRequested(String paramString, boolean paramBoolean, Session.Info paramInfo) throws RemoteException;
  
  void setRinging(String paramString, Session.Info paramInfo) throws RemoteException;
  
  void setStatusHints(String paramString, StatusHints paramStatusHints, Session.Info paramInfo) throws RemoteException;
  
  void setVideoProvider(String paramString, IVideoProvider paramIVideoProvider, Session.Info paramInfo) throws RemoteException;
  
  void setVideoState(String paramString, int paramInt, Session.Info paramInfo) throws RemoteException;
  
  class Default implements IConnectionServiceAdapter {
    public void handleCreateConnectionComplete(String param1String, ConnectionRequest param1ConnectionRequest, ParcelableConnection param1ParcelableConnection, Session.Info param1Info) throws RemoteException {}
    
    public void handleCreateConferenceComplete(String param1String, ConnectionRequest param1ConnectionRequest, ParcelableConference param1ParcelableConference, Session.Info param1Info) throws RemoteException {}
    
    public void setActive(String param1String, Session.Info param1Info) throws RemoteException {}
    
    public void setRinging(String param1String, Session.Info param1Info) throws RemoteException {}
    
    public void setDialing(String param1String, Session.Info param1Info) throws RemoteException {}
    
    public void setPulling(String param1String, Session.Info param1Info) throws RemoteException {}
    
    public void setDisconnected(String param1String, DisconnectCause param1DisconnectCause, Session.Info param1Info) throws RemoteException {}
    
    public void setOnHold(String param1String, Session.Info param1Info) throws RemoteException {}
    
    public void setRingbackRequested(String param1String, boolean param1Boolean, Session.Info param1Info) throws RemoteException {}
    
    public void setConnectionCapabilities(String param1String, int param1Int, Session.Info param1Info) throws RemoteException {}
    
    public void setConnectionProperties(String param1String, int param1Int, Session.Info param1Info) throws RemoteException {}
    
    public void setIsConferenced(String param1String1, String param1String2, Session.Info param1Info) throws RemoteException {}
    
    public void setConferenceMergeFailed(String param1String, Session.Info param1Info) throws RemoteException {}
    
    public void addConferenceCall(String param1String, ParcelableConference param1ParcelableConference, Session.Info param1Info) throws RemoteException {}
    
    public void removeCall(String param1String, Session.Info param1Info) throws RemoteException {}
    
    public void onPostDialWait(String param1String1, String param1String2, Session.Info param1Info) throws RemoteException {}
    
    public void onPostDialChar(String param1String, char param1Char, Session.Info param1Info) throws RemoteException {}
    
    public void queryRemoteConnectionServices(RemoteServiceCallback param1RemoteServiceCallback, String param1String, Session.Info param1Info) throws RemoteException {}
    
    public void setVideoProvider(String param1String, IVideoProvider param1IVideoProvider, Session.Info param1Info) throws RemoteException {}
    
    public void setVideoState(String param1String, int param1Int, Session.Info param1Info) throws RemoteException {}
    
    public void setIsVoipAudioMode(String param1String, boolean param1Boolean, Session.Info param1Info) throws RemoteException {}
    
    public void setStatusHints(String param1String, StatusHints param1StatusHints, Session.Info param1Info) throws RemoteException {}
    
    public void setAddress(String param1String, Uri param1Uri, int param1Int, Session.Info param1Info) throws RemoteException {}
    
    public void setCallerDisplayName(String param1String1, String param1String2, int param1Int, Session.Info param1Info) throws RemoteException {}
    
    public void setConferenceableConnections(String param1String, List<String> param1List, Session.Info param1Info) throws RemoteException {}
    
    public void addExistingConnection(String param1String, ParcelableConnection param1ParcelableConnection, Session.Info param1Info) throws RemoteException {}
    
    public void putExtras(String param1String, Bundle param1Bundle, Session.Info param1Info) throws RemoteException {}
    
    public void removeExtras(String param1String, List<String> param1List, Session.Info param1Info) throws RemoteException {}
    
    public void setAudioRoute(String param1String1, int param1Int, String param1String2, Session.Info param1Info) throws RemoteException {}
    
    public void onConnectionEvent(String param1String1, String param1String2, Bundle param1Bundle, Session.Info param1Info) throws RemoteException {}
    
    public void onRttInitiationSuccess(String param1String, Session.Info param1Info) throws RemoteException {}
    
    public void onRttInitiationFailure(String param1String, int param1Int, Session.Info param1Info) throws RemoteException {}
    
    public void onRttSessionRemotelyTerminated(String param1String, Session.Info param1Info) throws RemoteException {}
    
    public void onRemoteRttRequest(String param1String, Session.Info param1Info) throws RemoteException {}
    
    public void onPhoneAccountChanged(String param1String, PhoneAccountHandle param1PhoneAccountHandle, Session.Info param1Info) throws RemoteException {}
    
    public void onConnectionServiceFocusReleased(Session.Info param1Info) throws RemoteException {}
    
    public void resetConnectionTime(String param1String, Session.Info param1Info) throws RemoteException {}
    
    public void setConferenceState(String param1String, boolean param1Boolean, Session.Info param1Info) throws RemoteException {}
    
    public void setCallDirection(String param1String, int param1Int, Session.Info param1Info) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IConnectionServiceAdapter {
    private static final String DESCRIPTOR = "com.android.internal.telecom.IConnectionServiceAdapter";
    
    static final int TRANSACTION_addConferenceCall = 14;
    
    static final int TRANSACTION_addExistingConnection = 26;
    
    static final int TRANSACTION_handleCreateConferenceComplete = 2;
    
    static final int TRANSACTION_handleCreateConnectionComplete = 1;
    
    static final int TRANSACTION_onConnectionEvent = 30;
    
    static final int TRANSACTION_onConnectionServiceFocusReleased = 36;
    
    static final int TRANSACTION_onPhoneAccountChanged = 35;
    
    static final int TRANSACTION_onPostDialChar = 17;
    
    static final int TRANSACTION_onPostDialWait = 16;
    
    static final int TRANSACTION_onRemoteRttRequest = 34;
    
    static final int TRANSACTION_onRttInitiationFailure = 32;
    
    static final int TRANSACTION_onRttInitiationSuccess = 31;
    
    static final int TRANSACTION_onRttSessionRemotelyTerminated = 33;
    
    static final int TRANSACTION_putExtras = 27;
    
    static final int TRANSACTION_queryRemoteConnectionServices = 18;
    
    static final int TRANSACTION_removeCall = 15;
    
    static final int TRANSACTION_removeExtras = 28;
    
    static final int TRANSACTION_resetConnectionTime = 37;
    
    static final int TRANSACTION_setActive = 3;
    
    static final int TRANSACTION_setAddress = 23;
    
    static final int TRANSACTION_setAudioRoute = 29;
    
    static final int TRANSACTION_setCallDirection = 39;
    
    static final int TRANSACTION_setCallerDisplayName = 24;
    
    static final int TRANSACTION_setConferenceMergeFailed = 13;
    
    static final int TRANSACTION_setConferenceState = 38;
    
    static final int TRANSACTION_setConferenceableConnections = 25;
    
    static final int TRANSACTION_setConnectionCapabilities = 10;
    
    static final int TRANSACTION_setConnectionProperties = 11;
    
    static final int TRANSACTION_setDialing = 5;
    
    static final int TRANSACTION_setDisconnected = 7;
    
    static final int TRANSACTION_setIsConferenced = 12;
    
    static final int TRANSACTION_setIsVoipAudioMode = 21;
    
    static final int TRANSACTION_setOnHold = 8;
    
    static final int TRANSACTION_setPulling = 6;
    
    static final int TRANSACTION_setRingbackRequested = 9;
    
    static final int TRANSACTION_setRinging = 4;
    
    static final int TRANSACTION_setStatusHints = 22;
    
    static final int TRANSACTION_setVideoProvider = 19;
    
    static final int TRANSACTION_setVideoState = 20;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telecom.IConnectionServiceAdapter");
    }
    
    public static IConnectionServiceAdapter asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telecom.IConnectionServiceAdapter");
      if (iInterface != null && iInterface instanceof IConnectionServiceAdapter)
        return (IConnectionServiceAdapter)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 39:
          return "setCallDirection";
        case 38:
          return "setConferenceState";
        case 37:
          return "resetConnectionTime";
        case 36:
          return "onConnectionServiceFocusReleased";
        case 35:
          return "onPhoneAccountChanged";
        case 34:
          return "onRemoteRttRequest";
        case 33:
          return "onRttSessionRemotelyTerminated";
        case 32:
          return "onRttInitiationFailure";
        case 31:
          return "onRttInitiationSuccess";
        case 30:
          return "onConnectionEvent";
        case 29:
          return "setAudioRoute";
        case 28:
          return "removeExtras";
        case 27:
          return "putExtras";
        case 26:
          return "addExistingConnection";
        case 25:
          return "setConferenceableConnections";
        case 24:
          return "setCallerDisplayName";
        case 23:
          return "setAddress";
        case 22:
          return "setStatusHints";
        case 21:
          return "setIsVoipAudioMode";
        case 20:
          return "setVideoState";
        case 19:
          return "setVideoProvider";
        case 18:
          return "queryRemoteConnectionServices";
        case 17:
          return "onPostDialChar";
        case 16:
          return "onPostDialWait";
        case 15:
          return "removeCall";
        case 14:
          return "addConferenceCall";
        case 13:
          return "setConferenceMergeFailed";
        case 12:
          return "setIsConferenced";
        case 11:
          return "setConnectionProperties";
        case 10:
          return "setConnectionCapabilities";
        case 9:
          return "setRingbackRequested";
        case 8:
          return "setOnHold";
        case 7:
          return "setDisconnected";
        case 6:
          return "setPulling";
        case 5:
          return "setDialing";
        case 4:
          return "setRinging";
        case 3:
          return "setActive";
        case 2:
          return "handleCreateConferenceComplete";
        case 1:
          break;
      } 
      return "handleCreateConnectionComplete";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        String str1;
        ArrayList<String> arrayList1;
        String str4;
        ArrayList<String> arrayList2;
        String str3;
        IVideoProvider iVideoProvider;
        RemoteServiceCallback remoteServiceCallback;
        String str2;
        char c;
        boolean bool1 = false, bool2 = false, bool3 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 39:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str1 = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setCallDirection(str1, param1Int1, (Session.Info)param1Parcel1);
            return true;
          case 38:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0)
              bool3 = true; 
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setConferenceState(str1, bool3, (Session.Info)param1Parcel1);
            return true;
          case 37:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            resetConnectionTime(str1, (Session.Info)param1Parcel1);
            return true;
          case 36:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onConnectionServiceFocusReleased((Session.Info)param1Parcel1);
            return true;
          case 35:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str4 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str1 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onPhoneAccountChanged(str4, (PhoneAccountHandle)str1, (Session.Info)param1Parcel1);
            return true;
          case 34:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onRemoteRttRequest(str1, (Session.Info)param1Parcel1);
            return true;
          case 33:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onRttSessionRemotelyTerminated(str1, (Session.Info)param1Parcel1);
            return true;
          case 32:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str1 = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onRttInitiationFailure(str1, param1Int1, (Session.Info)param1Parcel1);
            return true;
          case 31:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onRttInitiationSuccess(str1, (Session.Info)param1Parcel1);
            return true;
          case 30:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str5 = param1Parcel1.readString();
            str4 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str1 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onConnectionEvent(str5, str4, (Bundle)str1, (Session.Info)param1Parcel1);
            return true;
          case 29:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str4 = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setAudioRoute(str4, param1Int1, str1, (Session.Info)param1Parcel1);
            return true;
          case 28:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str1 = param1Parcel1.readString();
            arrayList2 = param1Parcel1.createStringArrayList();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            removeExtras(str1, arrayList2, (Session.Info)param1Parcel1);
            return true;
          case 27:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str3 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str1 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            putExtras(str3, (Bundle)str1, (Session.Info)param1Parcel1);
            return true;
          case 26:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str3 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              ParcelableConnection parcelableConnection = (ParcelableConnection)ParcelableConnection.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str1 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            addExistingConnection(str3, (ParcelableConnection)str1, (Session.Info)param1Parcel1);
            return true;
          case 25:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str3 = param1Parcel1.readString();
            arrayList1 = param1Parcel1.createStringArrayList();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setConferenceableConnections(str3, arrayList1, (Session.Info)param1Parcel1);
            return true;
          case 24:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str = param1Parcel1.readString();
            str3 = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setCallerDisplayName(str, str3, param1Int1, (Session.Info)param1Parcel1);
            return true;
          case 23:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str3 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setAddress(str3, (Uri)str, param1Int1, (Session.Info)param1Parcel1);
            return true;
          case 22:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str3 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              StatusHints statusHints = (StatusHints)StatusHints.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setStatusHints(str3, (StatusHints)str, (Session.Info)param1Parcel1);
            return true;
          case 21:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str = param1Parcel1.readString();
            bool3 = bool1;
            if (param1Parcel1.readInt() != 0)
              bool3 = true; 
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setIsVoipAudioMode(str, bool3, (Session.Info)param1Parcel1);
            return true;
          case 20:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setVideoState(str, param1Int1, (Session.Info)param1Parcel1);
            return true;
          case 19:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str = param1Parcel1.readString();
            iVideoProvider = IVideoProvider.Stub.asInterface(param1Parcel1.readStrongBinder());
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setVideoProvider(str, iVideoProvider, (Session.Info)param1Parcel1);
            return true;
          case 18:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            remoteServiceCallback = RemoteServiceCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            str = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            queryRemoteConnectionServices(remoteServiceCallback, str, (Session.Info)param1Parcel1);
            return true;
          case 17:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str = param1Parcel1.readString();
            c = (char)param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onPostDialChar(str, c, (Session.Info)param1Parcel1);
            return true;
          case 16:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str2 = param1Parcel1.readString();
            str = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onPostDialWait(str2, str, (Session.Info)param1Parcel1);
            return true;
          case 15:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            removeCall(str, (Session.Info)param1Parcel1);
            return true;
          case 14:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str2 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              ParcelableConference parcelableConference = (ParcelableConference)ParcelableConference.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            addConferenceCall(str2, (ParcelableConference)str, (Session.Info)param1Parcel1);
            return true;
          case 13:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setConferenceMergeFailed(str, (Session.Info)param1Parcel1);
            return true;
          case 12:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str2 = param1Parcel1.readString();
            str = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setIsConferenced(str2, str, (Session.Info)param1Parcel1);
            return true;
          case 11:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setConnectionProperties(str, param1Int1, (Session.Info)param1Parcel1);
            return true;
          case 10:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setConnectionCapabilities(str, param1Int1, (Session.Info)param1Parcel1);
            return true;
          case 9:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str = param1Parcel1.readString();
            bool3 = bool2;
            if (param1Parcel1.readInt() != 0)
              bool3 = true; 
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setRingbackRequested(str, bool3, (Session.Info)param1Parcel1);
            return true;
          case 8:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setOnHold(str, (Session.Info)param1Parcel1);
            return true;
          case 7:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str2 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              DisconnectCause disconnectCause = (DisconnectCause)DisconnectCause.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setDisconnected(str2, (DisconnectCause)str, (Session.Info)param1Parcel1);
            return true;
          case 6:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setPulling(str, (Session.Info)param1Parcel1);
            return true;
          case 5:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setDialing(str, (Session.Info)param1Parcel1);
            return true;
          case 4:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setRinging(str, (Session.Info)param1Parcel1);
            return true;
          case 3:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setActive(str, (Session.Info)param1Parcel1);
            return true;
          case 2:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
            str5 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              ConnectionRequest connectionRequest = (ConnectionRequest)ConnectionRequest.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              ParcelableConference parcelableConference = (ParcelableConference)ParcelableConference.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str2 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            handleCreateConferenceComplete(str5, (ConnectionRequest)str, (ParcelableConference)str2, (Session.Info)param1Parcel1);
            return true;
          case 1:
            break;
        } 
        param1Parcel1.enforceInterface("com.android.internal.telecom.IConnectionServiceAdapter");
        String str5 = param1Parcel1.readString();
        if (param1Parcel1.readInt() != 0) {
          ConnectionRequest connectionRequest = (ConnectionRequest)ConnectionRequest.CREATOR.createFromParcel(param1Parcel1);
        } else {
          str = null;
        } 
        if (param1Parcel1.readInt() != 0) {
          ParcelableConnection parcelableConnection = (ParcelableConnection)ParcelableConnection.CREATOR.createFromParcel(param1Parcel1);
        } else {
          str2 = null;
        } 
        if (param1Parcel1.readInt() != 0) {
          Session.Info info = (Session.Info)Session.Info.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        handleCreateConnectionComplete(str5, (ConnectionRequest)str, (ParcelableConnection)str2, (Session.Info)param1Parcel1);
        return true;
      } 
      str.writeString("com.android.internal.telecom.IConnectionServiceAdapter");
      return true;
    }
    
    private static class Proxy implements IConnectionServiceAdapter {
      public static IConnectionServiceAdapter sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telecom.IConnectionServiceAdapter";
      }
      
      public void handleCreateConnectionComplete(String param2String, ConnectionRequest param2ConnectionRequest, ParcelableConnection param2ParcelableConnection, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          if (param2ConnectionRequest != null) {
            parcel.writeInt(1);
            param2ConnectionRequest.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ParcelableConnection != null) {
            parcel.writeInt(1);
            param2ParcelableConnection.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().handleCreateConnectionComplete(param2String, param2ConnectionRequest, param2ParcelableConnection, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void handleCreateConferenceComplete(String param2String, ConnectionRequest param2ConnectionRequest, ParcelableConference param2ParcelableConference, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          if (param2ConnectionRequest != null) {
            parcel.writeInt(1);
            param2ConnectionRequest.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ParcelableConference != null) {
            parcel.writeInt(1);
            param2ParcelableConference.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().handleCreateConferenceComplete(param2String, param2ConnectionRequest, param2ParcelableConference, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setActive(String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().setActive(param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setRinging(String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().setRinging(param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setDialing(String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().setDialing(param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setPulling(String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().setPulling(param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setDisconnected(String param2String, DisconnectCause param2DisconnectCause, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          if (param2DisconnectCause != null) {
            parcel.writeInt(1);
            param2DisconnectCause.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().setDisconnected(param2String, param2DisconnectCause, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setOnHold(String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().setOnHold(param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setRingbackRequested(String param2String, boolean param2Boolean, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool1 = this.mRemote.transact(9, parcel, null, 1);
          if (!bool1 && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().setRingbackRequested(param2String, param2Boolean, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setConnectionCapabilities(String param2String, int param2Int, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().setConnectionCapabilities(param2String, param2Int, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setConnectionProperties(String param2String, int param2Int, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(11, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().setConnectionProperties(param2String, param2Int, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setIsConferenced(String param2String1, String param2String2, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(12, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().setIsConferenced(param2String1, param2String2, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setConferenceMergeFailed(String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(13, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().setConferenceMergeFailed(param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void addConferenceCall(String param2String, ParcelableConference param2ParcelableConference, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          if (param2ParcelableConference != null) {
            parcel.writeInt(1);
            param2ParcelableConference.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(14, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().addConferenceCall(param2String, param2ParcelableConference, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void removeCall(String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(15, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().removeCall(param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPostDialWait(String param2String1, String param2String2, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(16, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().onPostDialWait(param2String1, param2String2, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPostDialChar(String param2String, char param2Char, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          parcel.writeInt(param2Char);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(17, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().onPostDialChar(param2String, param2Char, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void queryRemoteConnectionServices(RemoteServiceCallback param2RemoteServiceCallback, String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          if (param2RemoteServiceCallback != null) {
            iBinder = param2RemoteServiceCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(18, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().queryRemoteConnectionServices(param2RemoteServiceCallback, param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setVideoProvider(String param2String, IVideoProvider param2IVideoProvider, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          if (param2IVideoProvider != null) {
            iBinder = param2IVideoProvider.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(19, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().setVideoProvider(param2String, param2IVideoProvider, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setVideoState(String param2String, int param2Int, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(20, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().setVideoState(param2String, param2Int, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setIsVoipAudioMode(String param2String, boolean param2Boolean, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool1 = this.mRemote.transact(21, parcel, null, 1);
          if (!bool1 && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().setIsVoipAudioMode(param2String, param2Boolean, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setStatusHints(String param2String, StatusHints param2StatusHints, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          if (param2StatusHints != null) {
            parcel.writeInt(1);
            param2StatusHints.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(22, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().setStatusHints(param2String, param2StatusHints, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setAddress(String param2String, Uri param2Uri, int param2Int, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(23, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().setAddress(param2String, param2Uri, param2Int, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setCallerDisplayName(String param2String1, String param2String2, int param2Int, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          parcel.writeInt(param2Int);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(24, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().setCallerDisplayName(param2String1, param2String2, param2Int, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setConferenceableConnections(String param2String, List<String> param2List, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          parcel.writeStringList(param2List);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(25, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().setConferenceableConnections(param2String, param2List, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void addExistingConnection(String param2String, ParcelableConnection param2ParcelableConnection, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          if (param2ParcelableConnection != null) {
            parcel.writeInt(1);
            param2ParcelableConnection.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(26, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().addExistingConnection(param2String, param2ParcelableConnection, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void putExtras(String param2String, Bundle param2Bundle, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(27, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().putExtras(param2String, param2Bundle, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void removeExtras(String param2String, List<String> param2List, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          parcel.writeStringList(param2List);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(28, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().removeExtras(param2String, param2List, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setAudioRoute(String param2String1, int param2Int, String param2String2, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String1);
          parcel.writeInt(param2Int);
          parcel.writeString(param2String2);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(29, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().setAudioRoute(param2String1, param2Int, param2String2, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onConnectionEvent(String param2String1, String param2String2, Bundle param2Bundle, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(30, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().onConnectionEvent(param2String1, param2String2, param2Bundle, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onRttInitiationSuccess(String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(31, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().onRttInitiationSuccess(param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onRttInitiationFailure(String param2String, int param2Int, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(32, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().onRttInitiationFailure(param2String, param2Int, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onRttSessionRemotelyTerminated(String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(33, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().onRttSessionRemotelyTerminated(param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onRemoteRttRequest(String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(34, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().onRemoteRttRequest(param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPhoneAccountChanged(String param2String, PhoneAccountHandle param2PhoneAccountHandle, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          if (param2PhoneAccountHandle != null) {
            parcel.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(35, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().onPhoneAccountChanged(param2String, param2PhoneAccountHandle, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onConnectionServiceFocusReleased(Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(36, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().onConnectionServiceFocusReleased(param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void resetConnectionTime(String param2String, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(37, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().resetConnectionTime(param2String, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setConferenceState(String param2String, boolean param2Boolean, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool1 = this.mRemote.transact(38, parcel, null, 1);
          if (!bool1 && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().setConferenceState(param2String, param2Boolean, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setCallDirection(String param2String, int param2Int, Session.Info param2Info) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IConnectionServiceAdapter");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          if (param2Info != null) {
            parcel.writeInt(1);
            param2Info.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(39, parcel, null, 1);
          if (!bool && IConnectionServiceAdapter.Stub.getDefaultImpl() != null) {
            IConnectionServiceAdapter.Stub.getDefaultImpl().setCallDirection(param2String, param2Int, param2Info);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IConnectionServiceAdapter param1IConnectionServiceAdapter) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IConnectionServiceAdapter != null) {
          Proxy.sDefaultImpl = param1IConnectionServiceAdapter;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IConnectionServiceAdapter getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
