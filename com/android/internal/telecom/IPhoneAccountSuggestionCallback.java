package com.android.internal.telecom;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telecom.PhoneAccountSuggestion;
import java.util.ArrayList;
import java.util.List;

public interface IPhoneAccountSuggestionCallback extends IInterface {
  void suggestPhoneAccounts(String paramString, List<PhoneAccountSuggestion> paramList) throws RemoteException;
  
  class Default implements IPhoneAccountSuggestionCallback {
    public void suggestPhoneAccounts(String param1String, List<PhoneAccountSuggestion> param1List) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPhoneAccountSuggestionCallback {
    private static final String DESCRIPTOR = "com.android.internal.telecom.IPhoneAccountSuggestionCallback";
    
    static final int TRANSACTION_suggestPhoneAccounts = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telecom.IPhoneAccountSuggestionCallback");
    }
    
    public static IPhoneAccountSuggestionCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telecom.IPhoneAccountSuggestionCallback");
      if (iInterface != null && iInterface instanceof IPhoneAccountSuggestionCallback)
        return (IPhoneAccountSuggestionCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "suggestPhoneAccounts";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.telecom.IPhoneAccountSuggestionCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.telecom.IPhoneAccountSuggestionCallback");
      String str = param1Parcel1.readString();
      ArrayList<PhoneAccountSuggestion> arrayList = param1Parcel1.createTypedArrayList(PhoneAccountSuggestion.CREATOR);
      suggestPhoneAccounts(str, arrayList);
      return true;
    }
    
    private static class Proxy implements IPhoneAccountSuggestionCallback {
      public static IPhoneAccountSuggestionCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telecom.IPhoneAccountSuggestionCallback";
      }
      
      public void suggestPhoneAccounts(String param2String, List<PhoneAccountSuggestion> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IPhoneAccountSuggestionCallback");
          parcel.writeString(param2String);
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IPhoneAccountSuggestionCallback.Stub.getDefaultImpl() != null) {
            IPhoneAccountSuggestionCallback.Stub.getDefaultImpl().suggestPhoneAccounts(param2String, param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPhoneAccountSuggestionCallback param1IPhoneAccountSuggestionCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPhoneAccountSuggestionCallback != null) {
          Proxy.sDefaultImpl = param1IPhoneAccountSuggestionCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPhoneAccountSuggestionCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
