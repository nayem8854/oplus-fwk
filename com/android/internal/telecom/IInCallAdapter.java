package com.android.internal.telecom;

import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telecom.PhoneAccountHandle;
import java.util.ArrayList;
import java.util.List;

public interface IInCallAdapter extends IInterface {
  void addConferenceParticipants(String paramString, List<Uri> paramList) throws RemoteException;
  
  void answerCall(String paramString, int paramInt) throws RemoteException;
  
  void conference(String paramString1, String paramString2) throws RemoteException;
  
  void consultativeTransfer(String paramString1, String paramString2) throws RemoteException;
  
  void deflectCall(String paramString, Uri paramUri) throws RemoteException;
  
  void disconnectCall(String paramString) throws RemoteException;
  
  void enterBackgroundAudioProcessing(String paramString) throws RemoteException;
  
  void exitBackgroundAudioProcessing(String paramString, boolean paramBoolean) throws RemoteException;
  
  void handoverTo(String paramString, PhoneAccountHandle paramPhoneAccountHandle, int paramInt, Bundle paramBundle) throws RemoteException;
  
  void holdCall(String paramString) throws RemoteException;
  
  void mergeConference(String paramString) throws RemoteException;
  
  void mute(boolean paramBoolean) throws RemoteException;
  
  void phoneAccountSelected(String paramString, PhoneAccountHandle paramPhoneAccountHandle, boolean paramBoolean) throws RemoteException;
  
  void playDtmfTone(String paramString, char paramChar) throws RemoteException;
  
  void postDialContinue(String paramString, boolean paramBoolean) throws RemoteException;
  
  void pullExternalCall(String paramString) throws RemoteException;
  
  void putExtras(String paramString, Bundle paramBundle) throws RemoteException;
  
  void rejectCall(String paramString1, boolean paramBoolean, String paramString2) throws RemoteException;
  
  void rejectCallWithReason(String paramString, int paramInt) throws RemoteException;
  
  void removeExtras(String paramString, List<String> paramList) throws RemoteException;
  
  void respondToRttRequest(String paramString, int paramInt, boolean paramBoolean) throws RemoteException;
  
  void sendCallEvent(String paramString1, String paramString2, int paramInt, Bundle paramBundle) throws RemoteException;
  
  void sendRttRequest(String paramString) throws RemoteException;
  
  void setAudioRoute(int paramInt, String paramString) throws RemoteException;
  
  void setRttMode(String paramString, int paramInt) throws RemoteException;
  
  void splitFromConference(String paramString) throws RemoteException;
  
  void stopDtmfTone(String paramString) throws RemoteException;
  
  void stopRtt(String paramString) throws RemoteException;
  
  void swapConference(String paramString) throws RemoteException;
  
  void transferCall(String paramString, Uri paramUri, boolean paramBoolean) throws RemoteException;
  
  void turnOffProximitySensor(boolean paramBoolean) throws RemoteException;
  
  void turnOnProximitySensor() throws RemoteException;
  
  void unholdCall(String paramString) throws RemoteException;
  
  class Default implements IInCallAdapter {
    public void answerCall(String param1String, int param1Int) throws RemoteException {}
    
    public void deflectCall(String param1String, Uri param1Uri) throws RemoteException {}
    
    public void rejectCall(String param1String1, boolean param1Boolean, String param1String2) throws RemoteException {}
    
    public void rejectCallWithReason(String param1String, int param1Int) throws RemoteException {}
    
    public void transferCall(String param1String, Uri param1Uri, boolean param1Boolean) throws RemoteException {}
    
    public void consultativeTransfer(String param1String1, String param1String2) throws RemoteException {}
    
    public void disconnectCall(String param1String) throws RemoteException {}
    
    public void holdCall(String param1String) throws RemoteException {}
    
    public void unholdCall(String param1String) throws RemoteException {}
    
    public void mute(boolean param1Boolean) throws RemoteException {}
    
    public void setAudioRoute(int param1Int, String param1String) throws RemoteException {}
    
    public void enterBackgroundAudioProcessing(String param1String) throws RemoteException {}
    
    public void exitBackgroundAudioProcessing(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public void playDtmfTone(String param1String, char param1Char) throws RemoteException {}
    
    public void stopDtmfTone(String param1String) throws RemoteException {}
    
    public void postDialContinue(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public void phoneAccountSelected(String param1String, PhoneAccountHandle param1PhoneAccountHandle, boolean param1Boolean) throws RemoteException {}
    
    public void conference(String param1String1, String param1String2) throws RemoteException {}
    
    public void splitFromConference(String param1String) throws RemoteException {}
    
    public void mergeConference(String param1String) throws RemoteException {}
    
    public void swapConference(String param1String) throws RemoteException {}
    
    public void addConferenceParticipants(String param1String, List<Uri> param1List) throws RemoteException {}
    
    public void turnOnProximitySensor() throws RemoteException {}
    
    public void turnOffProximitySensor(boolean param1Boolean) throws RemoteException {}
    
    public void pullExternalCall(String param1String) throws RemoteException {}
    
    public void sendCallEvent(String param1String1, String param1String2, int param1Int, Bundle param1Bundle) throws RemoteException {}
    
    public void putExtras(String param1String, Bundle param1Bundle) throws RemoteException {}
    
    public void removeExtras(String param1String, List<String> param1List) throws RemoteException {}
    
    public void sendRttRequest(String param1String) throws RemoteException {}
    
    public void respondToRttRequest(String param1String, int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void stopRtt(String param1String) throws RemoteException {}
    
    public void setRttMode(String param1String, int param1Int) throws RemoteException {}
    
    public void handoverTo(String param1String, PhoneAccountHandle param1PhoneAccountHandle, int param1Int, Bundle param1Bundle) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IInCallAdapter {
    private static final String DESCRIPTOR = "com.android.internal.telecom.IInCallAdapter";
    
    static final int TRANSACTION_addConferenceParticipants = 22;
    
    static final int TRANSACTION_answerCall = 1;
    
    static final int TRANSACTION_conference = 18;
    
    static final int TRANSACTION_consultativeTransfer = 6;
    
    static final int TRANSACTION_deflectCall = 2;
    
    static final int TRANSACTION_disconnectCall = 7;
    
    static final int TRANSACTION_enterBackgroundAudioProcessing = 12;
    
    static final int TRANSACTION_exitBackgroundAudioProcessing = 13;
    
    static final int TRANSACTION_handoverTo = 33;
    
    static final int TRANSACTION_holdCall = 8;
    
    static final int TRANSACTION_mergeConference = 20;
    
    static final int TRANSACTION_mute = 10;
    
    static final int TRANSACTION_phoneAccountSelected = 17;
    
    static final int TRANSACTION_playDtmfTone = 14;
    
    static final int TRANSACTION_postDialContinue = 16;
    
    static final int TRANSACTION_pullExternalCall = 25;
    
    static final int TRANSACTION_putExtras = 27;
    
    static final int TRANSACTION_rejectCall = 3;
    
    static final int TRANSACTION_rejectCallWithReason = 4;
    
    static final int TRANSACTION_removeExtras = 28;
    
    static final int TRANSACTION_respondToRttRequest = 30;
    
    static final int TRANSACTION_sendCallEvent = 26;
    
    static final int TRANSACTION_sendRttRequest = 29;
    
    static final int TRANSACTION_setAudioRoute = 11;
    
    static final int TRANSACTION_setRttMode = 32;
    
    static final int TRANSACTION_splitFromConference = 19;
    
    static final int TRANSACTION_stopDtmfTone = 15;
    
    static final int TRANSACTION_stopRtt = 31;
    
    static final int TRANSACTION_swapConference = 21;
    
    static final int TRANSACTION_transferCall = 5;
    
    static final int TRANSACTION_turnOffProximitySensor = 24;
    
    static final int TRANSACTION_turnOnProximitySensor = 23;
    
    static final int TRANSACTION_unholdCall = 9;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telecom.IInCallAdapter");
    }
    
    public static IInCallAdapter asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telecom.IInCallAdapter");
      if (iInterface != null && iInterface instanceof IInCallAdapter)
        return (IInCallAdapter)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 33:
          return "handoverTo";
        case 32:
          return "setRttMode";
        case 31:
          return "stopRtt";
        case 30:
          return "respondToRttRequest";
        case 29:
          return "sendRttRequest";
        case 28:
          return "removeExtras";
        case 27:
          return "putExtras";
        case 26:
          return "sendCallEvent";
        case 25:
          return "pullExternalCall";
        case 24:
          return "turnOffProximitySensor";
        case 23:
          return "turnOnProximitySensor";
        case 22:
          return "addConferenceParticipants";
        case 21:
          return "swapConference";
        case 20:
          return "mergeConference";
        case 19:
          return "splitFromConference";
        case 18:
          return "conference";
        case 17:
          return "phoneAccountSelected";
        case 16:
          return "postDialContinue";
        case 15:
          return "stopDtmfTone";
        case 14:
          return "playDtmfTone";
        case 13:
          return "exitBackgroundAudioProcessing";
        case 12:
          return "enterBackgroundAudioProcessing";
        case 11:
          return "setAudioRoute";
        case 10:
          return "mute";
        case 9:
          return "unholdCall";
        case 8:
          return "holdCall";
        case 7:
          return "disconnectCall";
        case 6:
          return "consultativeTransfer";
        case 5:
          return "transferCall";
        case 4:
          return "rejectCallWithReason";
        case 3:
          return "rejectCall";
        case 2:
          return "deflectCall";
        case 1:
          break;
      } 
      return "answerCall";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        String str3;
        ArrayList<String> arrayList1;
        String str2;
        ArrayList<Uri> arrayList;
        String str1, str4;
        char c;
        boolean bool1 = false, bool2 = false, bool3 = false, bool4 = false, bool5 = false, bool6 = false, bool7 = false, bool8 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 33:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str4 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            handoverTo(str4, (PhoneAccountHandle)param1Parcel2, param1Int1, (Bundle)param1Parcel1);
            return true;
          case 32:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            setRttMode(str, param1Int1);
            return true;
          case 31:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str3 = param1Parcel1.readString();
            stopRtt(str3);
            return true;
          case 30:
            str3.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str = str3.readString();
            param1Int1 = str3.readInt();
            bool4 = bool8;
            if (str3.readInt() != 0)
              bool4 = true; 
            respondToRttRequest(str, param1Int1, bool4);
            return true;
          case 29:
            str3.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str3 = str3.readString();
            sendRttRequest(str3);
            return true;
          case 28:
            str3.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str = str3.readString();
            arrayList1 = str3.createStringArrayList();
            removeExtras(str, arrayList1);
            return true;
          case 27:
            arrayList1.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str = arrayList1.readString();
            if (arrayList1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)arrayList1);
            } else {
              arrayList1 = null;
            } 
            putExtras(str, (Bundle)arrayList1);
            return true;
          case 26:
            arrayList1.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str4 = arrayList1.readString();
            str = arrayList1.readString();
            param1Int1 = arrayList1.readInt();
            if (arrayList1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)arrayList1);
            } else {
              arrayList1 = null;
            } 
            sendCallEvent(str4, str, param1Int1, (Bundle)arrayList1);
            return true;
          case 25:
            arrayList1.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str2 = arrayList1.readString();
            pullExternalCall(str2);
            return true;
          case 24:
            str2.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            bool4 = bool1;
            if (str2.readInt() != 0)
              bool4 = true; 
            turnOffProximitySensor(bool4);
            return true;
          case 23:
            str2.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            turnOnProximitySensor();
            return true;
          case 22:
            str2.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str = str2.readString();
            arrayList = str2.createTypedArrayList(Uri.CREATOR);
            addConferenceParticipants(str, arrayList);
            return true;
          case 21:
            arrayList.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str1 = arrayList.readString();
            swapConference(str1);
            return true;
          case 20:
            str1.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str1 = str1.readString();
            mergeConference(str1);
            return true;
          case 19:
            str1.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str1 = str1.readString();
            splitFromConference(str1);
            return true;
          case 18:
            str1.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str = str1.readString();
            str1 = str1.readString();
            conference(str, str1);
            return true;
          case 17:
            str1.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str4 = str1.readString();
            if (str1.readInt() != 0) {
              PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str = null;
            } 
            bool4 = bool2;
            if (str1.readInt() != 0)
              bool4 = true; 
            phoneAccountSelected(str4, (PhoneAccountHandle)str, bool4);
            return true;
          case 16:
            str1.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str = str1.readString();
            bool4 = bool3;
            if (str1.readInt() != 0)
              bool4 = true; 
            postDialContinue(str, bool4);
            return true;
          case 15:
            str1.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str1 = str1.readString();
            stopDtmfTone(str1);
            return true;
          case 14:
            str1.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str = str1.readString();
            c = (char)str1.readInt();
            playDtmfTone(str, c);
            return true;
          case 13:
            str1.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str = str1.readString();
            if (str1.readInt() != 0)
              bool4 = true; 
            exitBackgroundAudioProcessing(str, bool4);
            return true;
          case 12:
            str1.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str1 = str1.readString();
            enterBackgroundAudioProcessing(str1);
            return true;
          case 11:
            str1.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            param1Int1 = str1.readInt();
            str1 = str1.readString();
            setAudioRoute(param1Int1, str1);
            return true;
          case 10:
            str1.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            bool4 = bool5;
            if (str1.readInt() != 0)
              bool4 = true; 
            mute(bool4);
            return true;
          case 9:
            str1.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str1 = str1.readString();
            unholdCall(str1);
            return true;
          case 8:
            str1.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str1 = str1.readString();
            holdCall(str1);
            return true;
          case 7:
            str1.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str1 = str1.readString();
            disconnectCall(str1);
            return true;
          case 6:
            str1.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str = str1.readString();
            str1 = str1.readString();
            consultativeTransfer(str, str1);
            return true;
          case 5:
            str1.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str4 = str1.readString();
            if (str1.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str = null;
            } 
            bool4 = bool6;
            if (str1.readInt() != 0)
              bool4 = true; 
            transferCall(str4, (Uri)str, bool4);
            return true;
          case 4:
            str1.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str = str1.readString();
            param1Int1 = str1.readInt();
            rejectCallWithReason(str, param1Int1);
            return true;
          case 3:
            str1.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str = str1.readString();
            bool4 = bool7;
            if (str1.readInt() != 0)
              bool4 = true; 
            str1 = str1.readString();
            rejectCall(str, bool4, str1);
            return true;
          case 2:
            str1.enforceInterface("com.android.internal.telecom.IInCallAdapter");
            str = str1.readString();
            if (str1.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            deflectCall(str, (Uri)str1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("com.android.internal.telecom.IInCallAdapter");
        str = str1.readString();
        param1Int1 = str1.readInt();
        answerCall(str, param1Int1);
        return true;
      } 
      str.writeString("com.android.internal.telecom.IInCallAdapter");
      return true;
    }
    
    private static class Proxy implements IInCallAdapter {
      public static IInCallAdapter sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telecom.IInCallAdapter";
      }
      
      public void answerCall(String param2String, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().answerCall(param2String, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void deflectCall(String param2String, Uri param2Uri) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String);
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().deflectCall(param2String, param2Uri);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void rejectCall(String param2String1, boolean param2Boolean, String param2String2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String1);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          parcel.writeString(param2String2);
          boolean bool1 = this.mRemote.transact(3, parcel, null, 1);
          if (!bool1 && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().rejectCall(param2String1, param2Boolean, param2String2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void rejectCallWithReason(String param2String, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().rejectCallWithReason(param2String, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void transferCall(String param2String, Uri param2Uri, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String);
          boolean bool = false;
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean)
            bool = true; 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(5, parcel, null, 1);
          if (!bool1 && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().transferCall(param2String, param2Uri, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void consultativeTransfer(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().consultativeTransfer(param2String1, param2String2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void disconnectCall(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().disconnectCall(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void holdCall(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().holdCall(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void unholdCall(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().unholdCall(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void mute(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(10, parcel, null, 1);
          if (!bool1 && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().mute(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setAudioRoute(int param2Int, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(11, parcel, null, 1);
          if (!bool && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().setAudioRoute(param2Int, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void enterBackgroundAudioProcessing(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(12, parcel, null, 1);
          if (!bool && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().enterBackgroundAudioProcessing(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void exitBackgroundAudioProcessing(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(13, parcel, null, 1);
          if (!bool1 && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().exitBackgroundAudioProcessing(param2String, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void playDtmfTone(String param2String, char param2Char) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String);
          parcel.writeInt(param2Char);
          boolean bool = this.mRemote.transact(14, parcel, null, 1);
          if (!bool && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().playDtmfTone(param2String, param2Char);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void stopDtmfTone(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(15, parcel, null, 1);
          if (!bool && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().stopDtmfTone(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void postDialContinue(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(16, parcel, null, 1);
          if (!bool1 && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().postDialContinue(param2String, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void phoneAccountSelected(String param2String, PhoneAccountHandle param2PhoneAccountHandle, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String);
          boolean bool = false;
          if (param2PhoneAccountHandle != null) {
            parcel.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean)
            bool = true; 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(17, parcel, null, 1);
          if (!bool1 && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().phoneAccountSelected(param2String, param2PhoneAccountHandle, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void conference(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          boolean bool = this.mRemote.transact(18, parcel, null, 1);
          if (!bool && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().conference(param2String1, param2String2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void splitFromConference(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(19, parcel, null, 1);
          if (!bool && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().splitFromConference(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void mergeConference(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(20, parcel, null, 1);
          if (!bool && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().mergeConference(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void swapConference(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(21, parcel, null, 1);
          if (!bool && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().swapConference(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void addConferenceParticipants(String param2String, List<Uri> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String);
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(22, parcel, null, 1);
          if (!bool && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().addConferenceParticipants(param2String, param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void turnOnProximitySensor() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          boolean bool = this.mRemote.transact(23, parcel, null, 1);
          if (!bool && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().turnOnProximitySensor();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void turnOffProximitySensor(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(24, parcel, null, 1);
          if (!bool1 && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().turnOffProximitySensor(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void pullExternalCall(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(25, parcel, null, 1);
          if (!bool && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().pullExternalCall(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void sendCallEvent(String param2String1, String param2String2, int param2Int, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          parcel.writeInt(param2Int);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(26, parcel, null, 1);
          if (!bool && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().sendCallEvent(param2String1, param2String2, param2Int, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void putExtras(String param2String, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(27, parcel, null, 1);
          if (!bool && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().putExtras(param2String, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void removeExtras(String param2String, List<String> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String);
          parcel.writeStringList(param2List);
          boolean bool = this.mRemote.transact(28, parcel, null, 1);
          if (!bool && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().removeExtras(param2String, param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void sendRttRequest(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(29, parcel, null, 1);
          if (!bool && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().sendRttRequest(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void respondToRttRequest(String param2String, int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(30, parcel, null, 1);
          if (!bool1 && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().respondToRttRequest(param2String, param2Int, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void stopRtt(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(31, parcel, null, 1);
          if (!bool && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().stopRtt(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setRttMode(String param2String, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(32, parcel, null, 1);
          if (!bool && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().setRttMode(param2String, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void handoverTo(String param2String, PhoneAccountHandle param2PhoneAccountHandle, int param2Int, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IInCallAdapter");
          parcel.writeString(param2String);
          if (param2PhoneAccountHandle != null) {
            parcel.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(33, parcel, null, 1);
          if (!bool && IInCallAdapter.Stub.getDefaultImpl() != null) {
            IInCallAdapter.Stub.getDefaultImpl().handoverTo(param2String, param2PhoneAccountHandle, param2Int, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IInCallAdapter param1IInCallAdapter) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IInCallAdapter != null) {
          Proxy.sDefaultImpl = param1IInCallAdapter;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IInCallAdapter getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
