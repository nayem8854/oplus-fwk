package com.android.internal.telecom;

import android.content.ComponentName;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ICallScreeningAdapter extends IInterface {
  void allowCall(String paramString) throws RemoteException;
  
  void disallowCall(String paramString, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, ComponentName paramComponentName) throws RemoteException;
  
  void screenCallFurther(String paramString) throws RemoteException;
  
  void silenceCall(String paramString) throws RemoteException;
  
  class Default implements ICallScreeningAdapter {
    public void allowCall(String param1String) throws RemoteException {}
    
    public void silenceCall(String param1String) throws RemoteException {}
    
    public void screenCallFurther(String param1String) throws RemoteException {}
    
    public void disallowCall(String param1String, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3, ComponentName param1ComponentName) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICallScreeningAdapter {
    private static final String DESCRIPTOR = "com.android.internal.telecom.ICallScreeningAdapter";
    
    static final int TRANSACTION_allowCall = 1;
    
    static final int TRANSACTION_disallowCall = 4;
    
    static final int TRANSACTION_screenCallFurther = 3;
    
    static final int TRANSACTION_silenceCall = 2;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telecom.ICallScreeningAdapter");
    }
    
    public static ICallScreeningAdapter asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telecom.ICallScreeningAdapter");
      if (iInterface != null && iInterface instanceof ICallScreeningAdapter)
        return (ICallScreeningAdapter)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "disallowCall";
          } 
          return "screenCallFurther";
        } 
        return "silenceCall";
      } 
      return "allowCall";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            boolean bool1, bool2, bool3;
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("com.android.internal.telecom.ICallScreeningAdapter");
              return true;
            } 
            param1Parcel1.enforceInterface("com.android.internal.telecom.ICallScreeningAdapter");
            String str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            if (param1Parcel1.readInt() != 0) {
              bool2 = true;
            } else {
              bool2 = false;
            } 
            if (param1Parcel1.readInt() != 0) {
              bool3 = true;
            } else {
              bool3 = false;
            } 
            if (param1Parcel1.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            disallowCall(str1, bool1, bool2, bool3, (ComponentName)param1Parcel1);
            return true;
          } 
          param1Parcel1.enforceInterface("com.android.internal.telecom.ICallScreeningAdapter");
          str = param1Parcel1.readString();
          screenCallFurther(str);
          return true;
        } 
        str.enforceInterface("com.android.internal.telecom.ICallScreeningAdapter");
        str = str.readString();
        silenceCall(str);
        return true;
      } 
      str.enforceInterface("com.android.internal.telecom.ICallScreeningAdapter");
      String str = str.readString();
      allowCall(str);
      return true;
    }
    
    private static class Proxy implements ICallScreeningAdapter {
      public static ICallScreeningAdapter sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telecom.ICallScreeningAdapter";
      }
      
      public void allowCall(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.ICallScreeningAdapter");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && ICallScreeningAdapter.Stub.getDefaultImpl() != null) {
            ICallScreeningAdapter.Stub.getDefaultImpl().allowCall(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void silenceCall(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.ICallScreeningAdapter");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && ICallScreeningAdapter.Stub.getDefaultImpl() != null) {
            ICallScreeningAdapter.Stub.getDefaultImpl().silenceCall(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void screenCallFurther(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.ICallScreeningAdapter");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && ICallScreeningAdapter.Stub.getDefaultImpl() != null) {
            ICallScreeningAdapter.Stub.getDefaultImpl().screenCallFurther(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void disallowCall(String param2String, boolean param2Boolean1, boolean param2Boolean2, boolean param2Boolean3, ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("com.android.internal.telecom.ICallScreeningAdapter");
          parcel.writeString(param2String);
          if (param2Boolean1) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2Boolean2) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2Boolean3) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2ComponentName != null) {
            parcel.writeInt(1);
            param2ComponentName.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool1 = this.mRemote.transact(4, parcel, null, 1);
          if (!bool1 && ICallScreeningAdapter.Stub.getDefaultImpl() != null) {
            ICallScreeningAdapter.Stub.getDefaultImpl().disallowCall(param2String, param2Boolean1, param2Boolean2, param2Boolean3, param2ComponentName);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICallScreeningAdapter param1ICallScreeningAdapter) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICallScreeningAdapter != null) {
          Proxy.sDefaultImpl = param1ICallScreeningAdapter;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICallScreeningAdapter getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
