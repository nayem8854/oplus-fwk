package com.android.internal.telecom;

import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomAnalytics;
import java.util.ArrayList;
import java.util.List;

public interface ITelecomService extends IInterface {
  void acceptHandover(Uri paramUri, int paramInt, PhoneAccountHandle paramPhoneAccountHandle) throws RemoteException;
  
  void acceptRingingCall(String paramString) throws RemoteException;
  
  void acceptRingingCallWithVideoState(String paramString, int paramInt) throws RemoteException;
  
  void addNewIncomingCall(PhoneAccountHandle paramPhoneAccountHandle, Bundle paramBundle) throws RemoteException;
  
  void addNewIncomingConference(PhoneAccountHandle paramPhoneAccountHandle, Bundle paramBundle) throws RemoteException;
  
  void addNewOutgoingCall(Intent paramIntent) throws RemoteException;
  
  void addNewUnknownCall(PhoneAccountHandle paramPhoneAccountHandle, Bundle paramBundle) throws RemoteException;
  
  void addOrRemoveTestCallCompanionApp(String paramString, boolean paramBoolean) throws RemoteException;
  
  void cancelMissedCallsNotification(String paramString) throws RemoteException;
  
  void clearAccounts(String paramString) throws RemoteException;
  
  Intent createLaunchEmergencyDialerIntent(String paramString) throws RemoteException;
  
  Intent createManageBlockedNumbersIntent() throws RemoteException;
  
  TelecomAnalytics dumpCallAnalytics() throws RemoteException;
  
  boolean enablePhoneAccount(PhoneAccountHandle paramPhoneAccountHandle, boolean paramBoolean) throws RemoteException;
  
  boolean endCall(String paramString) throws RemoteException;
  
  Uri getAdnUriForPhoneAccount(PhoneAccountHandle paramPhoneAccountHandle, String paramString) throws RemoteException;
  
  List<PhoneAccountHandle> getAllPhoneAccountHandles() throws RemoteException;
  
  List<PhoneAccount> getAllPhoneAccounts() throws RemoteException;
  
  int getAllPhoneAccountsCount() throws RemoteException;
  
  List<PhoneAccountHandle> getCallCapablePhoneAccounts(boolean paramBoolean, String paramString1, String paramString2) throws RemoteException;
  
  int getCallState() throws RemoteException;
  
  int getCurrentTtyMode(String paramString1, String paramString2) throws RemoteException;
  
  String getDefaultDialerPackage() throws RemoteException;
  
  String getDefaultDialerPackageForUser(int paramInt) throws RemoteException;
  
  PhoneAccountHandle getDefaultOutgoingPhoneAccount(String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  ComponentName getDefaultPhoneApp() throws RemoteException;
  
  String getLine1Number(PhoneAccountHandle paramPhoneAccountHandle, String paramString1, String paramString2) throws RemoteException;
  
  PhoneAccount getPhoneAccount(PhoneAccountHandle paramPhoneAccountHandle) throws RemoteException;
  
  List<PhoneAccountHandle> getPhoneAccountsForPackage(String paramString) throws RemoteException;
  
  List<PhoneAccountHandle> getPhoneAccountsSupportingScheme(String paramString1, String paramString2) throws RemoteException;
  
  List<PhoneAccountHandle> getSelfManagedPhoneAccounts(String paramString1, String paramString2) throws RemoteException;
  
  PhoneAccountHandle getSimCallManager(int paramInt) throws RemoteException;
  
  PhoneAccountHandle getSimCallManagerForUser(int paramInt) throws RemoteException;
  
  String getSystemDialerPackage() throws RemoteException;
  
  PhoneAccountHandle getUserSelectedOutgoingPhoneAccount(String paramString) throws RemoteException;
  
  String getVoiceMailNumber(PhoneAccountHandle paramPhoneAccountHandle, String paramString1, String paramString2) throws RemoteException;
  
  void handleCallIntent(Intent paramIntent, String paramString) throws RemoteException;
  
  boolean handlePinMmi(String paramString1, String paramString2) throws RemoteException;
  
  boolean handlePinMmiForPhoneAccount(PhoneAccountHandle paramPhoneAccountHandle, String paramString1, String paramString2) throws RemoteException;
  
  boolean isInCall(String paramString1, String paramString2) throws RemoteException;
  
  boolean isInEmergencyCall() throws RemoteException;
  
  boolean isInManagedCall(String paramString1, String paramString2) throws RemoteException;
  
  boolean isIncomingCallPermitted(PhoneAccountHandle paramPhoneAccountHandle) throws RemoteException;
  
  boolean isOutgoingCallPermitted(PhoneAccountHandle paramPhoneAccountHandle) throws RemoteException;
  
  boolean isRinging(String paramString) throws RemoteException;
  
  boolean isTtySupported(String paramString1, String paramString2) throws RemoteException;
  
  boolean isVoiceMailNumber(PhoneAccountHandle paramPhoneAccountHandle, String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  void oplusCancelMissedCallsNotification(String paramString, Bundle paramBundle) throws RemoteException;
  
  String oplusInteractWithTelecomService(int paramInt, String paramString) throws RemoteException;
  
  void placeCall(Uri paramUri, Bundle paramBundle, String paramString1, String paramString2) throws RemoteException;
  
  void registerPhoneAccount(PhoneAccount paramPhoneAccount) throws RemoteException;
  
  boolean setDefaultDialer(String paramString) throws RemoteException;
  
  void setSystemDialer(ComponentName paramComponentName) throws RemoteException;
  
  void setTestDefaultCallRedirectionApp(String paramString) throws RemoteException;
  
  void setTestDefaultCallScreeningApp(String paramString) throws RemoteException;
  
  void setTestDefaultDialer(String paramString) throws RemoteException;
  
  void setTestEmergencyPhoneAccountPackageNameFilter(String paramString) throws RemoteException;
  
  void setTestPhoneAcctSuggestionComponent(String paramString) throws RemoteException;
  
  void setUserSelectedOutgoingPhoneAccount(PhoneAccountHandle paramPhoneAccountHandle) throws RemoteException;
  
  void showInCallScreen(boolean paramBoolean, String paramString1, String paramString2) throws RemoteException;
  
  void silenceRinger(String paramString) throws RemoteException;
  
  void startConference(List<Uri> paramList, Bundle paramBundle, String paramString) throws RemoteException;
  
  void stopBlockSuppression() throws RemoteException;
  
  void unregisterPhoneAccount(PhoneAccountHandle paramPhoneAccountHandle) throws RemoteException;
  
  void waitOnHandlers() throws RemoteException;
  
  class Default implements ITelecomService {
    public void showInCallScreen(boolean param1Boolean, String param1String1, String param1String2) throws RemoteException {}
    
    public PhoneAccountHandle getDefaultOutgoingPhoneAccount(String param1String1, String param1String2, String param1String3) throws RemoteException {
      return null;
    }
    
    public PhoneAccountHandle getUserSelectedOutgoingPhoneAccount(String param1String) throws RemoteException {
      return null;
    }
    
    public void setUserSelectedOutgoingPhoneAccount(PhoneAccountHandle param1PhoneAccountHandle) throws RemoteException {}
    
    public List<PhoneAccountHandle> getCallCapablePhoneAccounts(boolean param1Boolean, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public List<PhoneAccountHandle> getSelfManagedPhoneAccounts(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public List<PhoneAccountHandle> getPhoneAccountsSupportingScheme(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public List<PhoneAccountHandle> getPhoneAccountsForPackage(String param1String) throws RemoteException {
      return null;
    }
    
    public PhoneAccount getPhoneAccount(PhoneAccountHandle param1PhoneAccountHandle) throws RemoteException {
      return null;
    }
    
    public int getAllPhoneAccountsCount() throws RemoteException {
      return 0;
    }
    
    public List<PhoneAccount> getAllPhoneAccounts() throws RemoteException {
      return null;
    }
    
    public List<PhoneAccountHandle> getAllPhoneAccountHandles() throws RemoteException {
      return null;
    }
    
    public PhoneAccountHandle getSimCallManager(int param1Int) throws RemoteException {
      return null;
    }
    
    public PhoneAccountHandle getSimCallManagerForUser(int param1Int) throws RemoteException {
      return null;
    }
    
    public void registerPhoneAccount(PhoneAccount param1PhoneAccount) throws RemoteException {}
    
    public void unregisterPhoneAccount(PhoneAccountHandle param1PhoneAccountHandle) throws RemoteException {}
    
    public void clearAccounts(String param1String) throws RemoteException {}
    
    public boolean isVoiceMailNumber(PhoneAccountHandle param1PhoneAccountHandle, String param1String1, String param1String2, String param1String3) throws RemoteException {
      return false;
    }
    
    public String getVoiceMailNumber(PhoneAccountHandle param1PhoneAccountHandle, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String getLine1Number(PhoneAccountHandle param1PhoneAccountHandle, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public ComponentName getDefaultPhoneApp() throws RemoteException {
      return null;
    }
    
    public String getDefaultDialerPackage() throws RemoteException {
      return null;
    }
    
    public String getDefaultDialerPackageForUser(int param1Int) throws RemoteException {
      return null;
    }
    
    public String getSystemDialerPackage() throws RemoteException {
      return null;
    }
    
    public TelecomAnalytics dumpCallAnalytics() throws RemoteException {
      return null;
    }
    
    public void silenceRinger(String param1String) throws RemoteException {}
    
    public boolean isInCall(String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public boolean isInManagedCall(String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public boolean isRinging(String param1String) throws RemoteException {
      return false;
    }
    
    public int getCallState() throws RemoteException {
      return 0;
    }
    
    public boolean endCall(String param1String) throws RemoteException {
      return false;
    }
    
    public void acceptRingingCall(String param1String) throws RemoteException {}
    
    public void acceptRingingCallWithVideoState(String param1String, int param1Int) throws RemoteException {}
    
    public void cancelMissedCallsNotification(String param1String) throws RemoteException {}
    
    public boolean handlePinMmi(String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public boolean handlePinMmiForPhoneAccount(PhoneAccountHandle param1PhoneAccountHandle, String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public Uri getAdnUriForPhoneAccount(PhoneAccountHandle param1PhoneAccountHandle, String param1String) throws RemoteException {
      return null;
    }
    
    public boolean isTtySupported(String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public int getCurrentTtyMode(String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public void addNewIncomingCall(PhoneAccountHandle param1PhoneAccountHandle, Bundle param1Bundle) throws RemoteException {}
    
    public void addNewIncomingConference(PhoneAccountHandle param1PhoneAccountHandle, Bundle param1Bundle) throws RemoteException {}
    
    public void addNewUnknownCall(PhoneAccountHandle param1PhoneAccountHandle, Bundle param1Bundle) throws RemoteException {}
    
    public void startConference(List<Uri> param1List, Bundle param1Bundle, String param1String) throws RemoteException {}
    
    public void placeCall(Uri param1Uri, Bundle param1Bundle, String param1String1, String param1String2) throws RemoteException {}
    
    public boolean enablePhoneAccount(PhoneAccountHandle param1PhoneAccountHandle, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean setDefaultDialer(String param1String) throws RemoteException {
      return false;
    }
    
    public void stopBlockSuppression() throws RemoteException {}
    
    public Intent createManageBlockedNumbersIntent() throws RemoteException {
      return null;
    }
    
    public Intent createLaunchEmergencyDialerIntent(String param1String) throws RemoteException {
      return null;
    }
    
    public boolean isIncomingCallPermitted(PhoneAccountHandle param1PhoneAccountHandle) throws RemoteException {
      return false;
    }
    
    public boolean isOutgoingCallPermitted(PhoneAccountHandle param1PhoneAccountHandle) throws RemoteException {
      return false;
    }
    
    public void waitOnHandlers() throws RemoteException {}
    
    public void acceptHandover(Uri param1Uri, int param1Int, PhoneAccountHandle param1PhoneAccountHandle) throws RemoteException {}
    
    public void setTestEmergencyPhoneAccountPackageNameFilter(String param1String) throws RemoteException {}
    
    public boolean isInEmergencyCall() throws RemoteException {
      return false;
    }
    
    public void handleCallIntent(Intent param1Intent, String param1String) throws RemoteException {}
    
    public void setTestDefaultCallRedirectionApp(String param1String) throws RemoteException {}
    
    public void setTestPhoneAcctSuggestionComponent(String param1String) throws RemoteException {}
    
    public void setTestDefaultCallScreeningApp(String param1String) throws RemoteException {}
    
    public void addOrRemoveTestCallCompanionApp(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public String oplusInteractWithTelecomService(int param1Int, String param1String) throws RemoteException {
      return null;
    }
    
    public void addNewOutgoingCall(Intent param1Intent) throws RemoteException {}
    
    public void oplusCancelMissedCallsNotification(String param1String, Bundle param1Bundle) throws RemoteException {}
    
    public void setSystemDialer(ComponentName param1ComponentName) throws RemoteException {}
    
    public void setTestDefaultDialer(String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITelecomService {
    private static final String DESCRIPTOR = "com.android.internal.telecom.ITelecomService";
    
    static final int TRANSACTION_acceptHandover = 53;
    
    static final int TRANSACTION_acceptRingingCall = 32;
    
    static final int TRANSACTION_acceptRingingCallWithVideoState = 33;
    
    static final int TRANSACTION_addNewIncomingCall = 40;
    
    static final int TRANSACTION_addNewIncomingConference = 41;
    
    static final int TRANSACTION_addNewOutgoingCall = 62;
    
    static final int TRANSACTION_addNewUnknownCall = 42;
    
    static final int TRANSACTION_addOrRemoveTestCallCompanionApp = 60;
    
    static final int TRANSACTION_cancelMissedCallsNotification = 34;
    
    static final int TRANSACTION_clearAccounts = 17;
    
    static final int TRANSACTION_createLaunchEmergencyDialerIntent = 49;
    
    static final int TRANSACTION_createManageBlockedNumbersIntent = 48;
    
    static final int TRANSACTION_dumpCallAnalytics = 25;
    
    static final int TRANSACTION_enablePhoneAccount = 45;
    
    static final int TRANSACTION_endCall = 31;
    
    static final int TRANSACTION_getAdnUriForPhoneAccount = 37;
    
    static final int TRANSACTION_getAllPhoneAccountHandles = 12;
    
    static final int TRANSACTION_getAllPhoneAccounts = 11;
    
    static final int TRANSACTION_getAllPhoneAccountsCount = 10;
    
    static final int TRANSACTION_getCallCapablePhoneAccounts = 5;
    
    static final int TRANSACTION_getCallState = 30;
    
    static final int TRANSACTION_getCurrentTtyMode = 39;
    
    static final int TRANSACTION_getDefaultDialerPackage = 22;
    
    static final int TRANSACTION_getDefaultDialerPackageForUser = 23;
    
    static final int TRANSACTION_getDefaultOutgoingPhoneAccount = 2;
    
    static final int TRANSACTION_getDefaultPhoneApp = 21;
    
    static final int TRANSACTION_getLine1Number = 20;
    
    static final int TRANSACTION_getPhoneAccount = 9;
    
    static final int TRANSACTION_getPhoneAccountsForPackage = 8;
    
    static final int TRANSACTION_getPhoneAccountsSupportingScheme = 7;
    
    static final int TRANSACTION_getSelfManagedPhoneAccounts = 6;
    
    static final int TRANSACTION_getSimCallManager = 13;
    
    static final int TRANSACTION_getSimCallManagerForUser = 14;
    
    static final int TRANSACTION_getSystemDialerPackage = 24;
    
    static final int TRANSACTION_getUserSelectedOutgoingPhoneAccount = 3;
    
    static final int TRANSACTION_getVoiceMailNumber = 19;
    
    static final int TRANSACTION_handleCallIntent = 56;
    
    static final int TRANSACTION_handlePinMmi = 35;
    
    static final int TRANSACTION_handlePinMmiForPhoneAccount = 36;
    
    static final int TRANSACTION_isInCall = 27;
    
    static final int TRANSACTION_isInEmergencyCall = 55;
    
    static final int TRANSACTION_isInManagedCall = 28;
    
    static final int TRANSACTION_isIncomingCallPermitted = 50;
    
    static final int TRANSACTION_isOutgoingCallPermitted = 51;
    
    static final int TRANSACTION_isRinging = 29;
    
    static final int TRANSACTION_isTtySupported = 38;
    
    static final int TRANSACTION_isVoiceMailNumber = 18;
    
    static final int TRANSACTION_oplusCancelMissedCallsNotification = 63;
    
    static final int TRANSACTION_oplusInteractWithTelecomService = 61;
    
    static final int TRANSACTION_placeCall = 44;
    
    static final int TRANSACTION_registerPhoneAccount = 15;
    
    static final int TRANSACTION_setDefaultDialer = 46;
    
    static final int TRANSACTION_setSystemDialer = 64;
    
    static final int TRANSACTION_setTestDefaultCallRedirectionApp = 57;
    
    static final int TRANSACTION_setTestDefaultCallScreeningApp = 59;
    
    static final int TRANSACTION_setTestDefaultDialer = 65;
    
    static final int TRANSACTION_setTestEmergencyPhoneAccountPackageNameFilter = 54;
    
    static final int TRANSACTION_setTestPhoneAcctSuggestionComponent = 58;
    
    static final int TRANSACTION_setUserSelectedOutgoingPhoneAccount = 4;
    
    static final int TRANSACTION_showInCallScreen = 1;
    
    static final int TRANSACTION_silenceRinger = 26;
    
    static final int TRANSACTION_startConference = 43;
    
    static final int TRANSACTION_stopBlockSuppression = 47;
    
    static final int TRANSACTION_unregisterPhoneAccount = 16;
    
    static final int TRANSACTION_waitOnHandlers = 52;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telecom.ITelecomService");
    }
    
    public static ITelecomService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telecom.ITelecomService");
      if (iInterface != null && iInterface instanceof ITelecomService)
        return (ITelecomService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 65:
          return "setTestDefaultDialer";
        case 64:
          return "setSystemDialer";
        case 63:
          return "oplusCancelMissedCallsNotification";
        case 62:
          return "addNewOutgoingCall";
        case 61:
          return "oplusInteractWithTelecomService";
        case 60:
          return "addOrRemoveTestCallCompanionApp";
        case 59:
          return "setTestDefaultCallScreeningApp";
        case 58:
          return "setTestPhoneAcctSuggestionComponent";
        case 57:
          return "setTestDefaultCallRedirectionApp";
        case 56:
          return "handleCallIntent";
        case 55:
          return "isInEmergencyCall";
        case 54:
          return "setTestEmergencyPhoneAccountPackageNameFilter";
        case 53:
          return "acceptHandover";
        case 52:
          return "waitOnHandlers";
        case 51:
          return "isOutgoingCallPermitted";
        case 50:
          return "isIncomingCallPermitted";
        case 49:
          return "createLaunchEmergencyDialerIntent";
        case 48:
          return "createManageBlockedNumbersIntent";
        case 47:
          return "stopBlockSuppression";
        case 46:
          return "setDefaultDialer";
        case 45:
          return "enablePhoneAccount";
        case 44:
          return "placeCall";
        case 43:
          return "startConference";
        case 42:
          return "addNewUnknownCall";
        case 41:
          return "addNewIncomingConference";
        case 40:
          return "addNewIncomingCall";
        case 39:
          return "getCurrentTtyMode";
        case 38:
          return "isTtySupported";
        case 37:
          return "getAdnUriForPhoneAccount";
        case 36:
          return "handlePinMmiForPhoneAccount";
        case 35:
          return "handlePinMmi";
        case 34:
          return "cancelMissedCallsNotification";
        case 33:
          return "acceptRingingCallWithVideoState";
        case 32:
          return "acceptRingingCall";
        case 31:
          return "endCall";
        case 30:
          return "getCallState";
        case 29:
          return "isRinging";
        case 28:
          return "isInManagedCall";
        case 27:
          return "isInCall";
        case 26:
          return "silenceRinger";
        case 25:
          return "dumpCallAnalytics";
        case 24:
          return "getSystemDialerPackage";
        case 23:
          return "getDefaultDialerPackageForUser";
        case 22:
          return "getDefaultDialerPackage";
        case 21:
          return "getDefaultPhoneApp";
        case 20:
          return "getLine1Number";
        case 19:
          return "getVoiceMailNumber";
        case 18:
          return "isVoiceMailNumber";
        case 17:
          return "clearAccounts";
        case 16:
          return "unregisterPhoneAccount";
        case 15:
          return "registerPhoneAccount";
        case 14:
          return "getSimCallManagerForUser";
        case 13:
          return "getSimCallManager";
        case 12:
          return "getAllPhoneAccountHandles";
        case 11:
          return "getAllPhoneAccounts";
        case 10:
          return "getAllPhoneAccountsCount";
        case 9:
          return "getPhoneAccount";
        case 8:
          return "getPhoneAccountsForPackage";
        case 7:
          return "getPhoneAccountsSupportingScheme";
        case 6:
          return "getSelfManagedPhoneAccounts";
        case 5:
          return "getCallCapablePhoneAccounts";
        case 4:
          return "setUserSelectedOutgoingPhoneAccount";
        case 3:
          return "getUserSelectedOutgoingPhoneAccount";
        case 2:
          return "getDefaultOutgoingPhoneAccount";
        case 1:
          break;
      } 
      return "showInCallScreen";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool6;
        int i1;
        boolean bool5;
        int n;
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        int i;
        String str12;
        Intent intent;
        String str11;
        Uri uri;
        String str10;
        TelecomAnalytics telecomAnalytics;
        String str9;
        ComponentName componentName;
        String str8;
        PhoneAccountHandle phoneAccountHandle3;
        List<PhoneAccountHandle> list5;
        PhoneAccount phoneAccount;
        String str7;
        List<PhoneAccountHandle> list4;
        String str6;
        List<PhoneAccountHandle> list3;
        String str5;
        List<PhoneAccountHandle> list2;
        String str4;
        List<PhoneAccountHandle> list1;
        String str3;
        PhoneAccountHandle phoneAccountHandle2;
        String str2;
        PhoneAccountHandle phoneAccountHandle1;
        Bundle bundle;
        ArrayList<Uri> arrayList;
        String str14, str15;
        boolean bool7 = false, bool8 = false, bool9 = false, bool10 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 65:
            param1Parcel1.enforceInterface("com.android.internal.telecom.ITelecomService");
            str12 = param1Parcel1.readString();
            setTestDefaultDialer(str12);
            param1Parcel2.writeNoException();
            return true;
          case 64:
            str12.enforceInterface("com.android.internal.telecom.ITelecomService");
            if (str12.readInt() != 0) {
              ComponentName componentName1 = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)str12);
            } else {
              str12 = null;
            } 
            setSystemDialer((ComponentName)str12);
            param1Parcel2.writeNoException();
            return true;
          case 63:
            str12.enforceInterface("com.android.internal.telecom.ITelecomService");
            str13 = str12.readString();
            if (str12.readInt() != 0) {
              Bundle bundle1 = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)str12);
            } else {
              str12 = null;
            } 
            oplusCancelMissedCallsNotification(str13, (Bundle)str12);
            param1Parcel2.writeNoException();
            return true;
          case 62:
            str12.enforceInterface("com.android.internal.telecom.ITelecomService");
            if (str12.readInt() != 0) {
              Intent intent1 = (Intent)Intent.CREATOR.createFromParcel((Parcel)str12);
            } else {
              str12 = null;
            } 
            addNewOutgoingCall((Intent)str12);
            param1Parcel2.writeNoException();
            return true;
          case 61:
            str12.enforceInterface("com.android.internal.telecom.ITelecomService");
            param1Int1 = str12.readInt();
            str12 = str12.readString();
            str12 = oplusInteractWithTelecomService(param1Int1, str12);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str12);
            return true;
          case 60:
            str12.enforceInterface("com.android.internal.telecom.ITelecomService");
            str13 = str12.readString();
            if (str12.readInt() != 0)
              bool10 = true; 
            addOrRemoveTestCallCompanionApp(str13, bool10);
            param1Parcel2.writeNoException();
            return true;
          case 59:
            str12.enforceInterface("com.android.internal.telecom.ITelecomService");
            str12 = str12.readString();
            setTestDefaultCallScreeningApp(str12);
            param1Parcel2.writeNoException();
            return true;
          case 58:
            str12.enforceInterface("com.android.internal.telecom.ITelecomService");
            str12 = str12.readString();
            setTestPhoneAcctSuggestionComponent(str12);
            param1Parcel2.writeNoException();
            return true;
          case 57:
            str12.enforceInterface("com.android.internal.telecom.ITelecomService");
            str12 = str12.readString();
            setTestDefaultCallRedirectionApp(str12);
            param1Parcel2.writeNoException();
            return true;
          case 56:
            str12.enforceInterface("com.android.internal.telecom.ITelecomService");
            if (str12.readInt() != 0) {
              Intent intent1 = (Intent)Intent.CREATOR.createFromParcel((Parcel)str12);
            } else {
              str13 = null;
            } 
            str12 = str12.readString();
            handleCallIntent((Intent)str13, str12);
            param1Parcel2.writeNoException();
            return true;
          case 55:
            str12.enforceInterface("com.android.internal.telecom.ITelecomService");
            bool6 = isInEmergencyCall();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 54:
            str12.enforceInterface("com.android.internal.telecom.ITelecomService");
            str12 = str12.readString();
            setTestEmergencyPhoneAccountPackageNameFilter(str12);
            param1Parcel2.writeNoException();
            return true;
          case 53:
            str12.enforceInterface("com.android.internal.telecom.ITelecomService");
            if (str12.readInt() != 0) {
              Uri uri1 = (Uri)Uri.CREATOR.createFromParcel((Parcel)str12);
            } else {
              str13 = null;
            } 
            i1 = str12.readInt();
            if (str12.readInt() != 0) {
              PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel((Parcel)str12);
            } else {
              str12 = null;
            } 
            acceptHandover((Uri)str13, i1, (PhoneAccountHandle)str12);
            param1Parcel2.writeNoException();
            return true;
          case 52:
            str12.enforceInterface("com.android.internal.telecom.ITelecomService");
            waitOnHandlers();
            param1Parcel2.writeNoException();
            return true;
          case 51:
            str12.enforceInterface("com.android.internal.telecom.ITelecomService");
            if (str12.readInt() != 0) {
              PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel((Parcel)str12);
            } else {
              str12 = null;
            } 
            bool5 = isOutgoingCallPermitted((PhoneAccountHandle)str12);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 50:
            str12.enforceInterface("com.android.internal.telecom.ITelecomService");
            if (str12.readInt() != 0) {
              PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel((Parcel)str12);
            } else {
              str12 = null;
            } 
            bool5 = isIncomingCallPermitted((PhoneAccountHandle)str12);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 49:
            str12.enforceInterface("com.android.internal.telecom.ITelecomService");
            str12 = str12.readString();
            intent = createLaunchEmergencyDialerIntent(str12);
            param1Parcel2.writeNoException();
            if (intent != null) {
              param1Parcel2.writeInt(1);
              intent.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 48:
            intent.enforceInterface("com.android.internal.telecom.ITelecomService");
            intent = createManageBlockedNumbersIntent();
            param1Parcel2.writeNoException();
            if (intent != null) {
              param1Parcel2.writeInt(1);
              intent.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 47:
            intent.enforceInterface("com.android.internal.telecom.ITelecomService");
            stopBlockSuppression();
            param1Parcel2.writeNoException();
            return true;
          case 46:
            intent.enforceInterface("com.android.internal.telecom.ITelecomService");
            str11 = intent.readString();
            bool5 = setDefaultDialer(str11);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 45:
            str11.enforceInterface("com.android.internal.telecom.ITelecomService");
            if (str11.readInt() != 0) {
              PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel((Parcel)str11);
            } else {
              str13 = null;
            } 
            bool10 = bool7;
            if (str11.readInt() != 0)
              bool10 = true; 
            bool5 = enablePhoneAccount((PhoneAccountHandle)str13, bool10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 44:
            str11.enforceInterface("com.android.internal.telecom.ITelecomService");
            if (str11.readInt() != 0) {
              Uri uri1 = (Uri)Uri.CREATOR.createFromParcel((Parcel)str11);
            } else {
              str13 = null;
            } 
            if (str11.readInt() != 0) {
              bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)str11);
            } else {
              bundle = null;
            } 
            str15 = str11.readString();
            str11 = str11.readString();
            placeCall((Uri)str13, bundle, str15, str11);
            param1Parcel2.writeNoException();
            return true;
          case 43:
            str11.enforceInterface("com.android.internal.telecom.ITelecomService");
            arrayList = str11.createTypedArrayList(Uri.CREATOR);
            if (str11.readInt() != 0) {
              Bundle bundle1 = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)str11);
            } else {
              str13 = null;
            } 
            str11 = str11.readString();
            startConference(arrayList, (Bundle)str13, str11);
            param1Parcel2.writeNoException();
            return true;
          case 42:
            str11.enforceInterface("com.android.internal.telecom.ITelecomService");
            if (str11.readInt() != 0) {
              PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel((Parcel)str11);
            } else {
              str13 = null;
            } 
            if (str11.readInt() != 0) {
              Bundle bundle1 = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)str11);
            } else {
              str11 = null;
            } 
            addNewUnknownCall((PhoneAccountHandle)str13, (Bundle)str11);
            param1Parcel2.writeNoException();
            return true;
          case 41:
            str11.enforceInterface("com.android.internal.telecom.ITelecomService");
            if (str11.readInt() != 0) {
              PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel((Parcel)str11);
            } else {
              str13 = null;
            } 
            if (str11.readInt() != 0) {
              Bundle bundle1 = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)str11);
            } else {
              str11 = null;
            } 
            addNewIncomingConference((PhoneAccountHandle)str13, (Bundle)str11);
            param1Parcel2.writeNoException();
            return true;
          case 40:
            str11.enforceInterface("com.android.internal.telecom.ITelecomService");
            if (str11.readInt() != 0) {
              PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel((Parcel)str11);
            } else {
              str13 = null;
            } 
            if (str11.readInt() != 0) {
              Bundle bundle1 = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)str11);
            } else {
              str11 = null;
            } 
            addNewIncomingCall((PhoneAccountHandle)str13, (Bundle)str11);
            param1Parcel2.writeNoException();
            return true;
          case 39:
            str11.enforceInterface("com.android.internal.telecom.ITelecomService");
            str13 = str11.readString();
            str11 = str11.readString();
            n = getCurrentTtyMode(str13, str11);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(n);
            return true;
          case 38:
            str11.enforceInterface("com.android.internal.telecom.ITelecomService");
            str13 = str11.readString();
            str11 = str11.readString();
            bool4 = isTtySupported(str13, str11);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 37:
            str11.enforceInterface("com.android.internal.telecom.ITelecomService");
            if (str11.readInt() != 0) {
              PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel((Parcel)str11);
            } else {
              str13 = null;
            } 
            str11 = str11.readString();
            uri = getAdnUriForPhoneAccount((PhoneAccountHandle)str13, str11);
            param1Parcel2.writeNoException();
            if (uri != null) {
              param1Parcel2.writeInt(1);
              uri.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 36:
            uri.enforceInterface("com.android.internal.telecom.ITelecomService");
            if (uri.readInt() != 0) {
              PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel((Parcel)uri);
            } else {
              str13 = null;
            } 
            str14 = uri.readString();
            str10 = uri.readString();
            bool4 = handlePinMmiForPhoneAccount((PhoneAccountHandle)str13, str14, str10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 35:
            str10.enforceInterface("com.android.internal.telecom.ITelecomService");
            str13 = str10.readString();
            str10 = str10.readString();
            bool4 = handlePinMmi(str13, str10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 34:
            str10.enforceInterface("com.android.internal.telecom.ITelecomService");
            str10 = str10.readString();
            cancelMissedCallsNotification(str10);
            param1Parcel2.writeNoException();
            return true;
          case 33:
            str10.enforceInterface("com.android.internal.telecom.ITelecomService");
            str13 = str10.readString();
            m = str10.readInt();
            acceptRingingCallWithVideoState(str13, m);
            param1Parcel2.writeNoException();
            return true;
          case 32:
            str10.enforceInterface("com.android.internal.telecom.ITelecomService");
            str10 = str10.readString();
            acceptRingingCall(str10);
            param1Parcel2.writeNoException();
            return true;
          case 31:
            str10.enforceInterface("com.android.internal.telecom.ITelecomService");
            str10 = str10.readString();
            bool3 = endCall(str10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 30:
            str10.enforceInterface("com.android.internal.telecom.ITelecomService");
            k = getCallState();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 29:
            str10.enforceInterface("com.android.internal.telecom.ITelecomService");
            str10 = str10.readString();
            bool2 = isRinging(str10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 28:
            str10.enforceInterface("com.android.internal.telecom.ITelecomService");
            str13 = str10.readString();
            str10 = str10.readString();
            bool2 = isInManagedCall(str13, str10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 27:
            str10.enforceInterface("com.android.internal.telecom.ITelecomService");
            str13 = str10.readString();
            str10 = str10.readString();
            bool2 = isInCall(str13, str10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 26:
            str10.enforceInterface("com.android.internal.telecom.ITelecomService");
            str10 = str10.readString();
            silenceRinger(str10);
            param1Parcel2.writeNoException();
            return true;
          case 25:
            str10.enforceInterface("com.android.internal.telecom.ITelecomService");
            telecomAnalytics = dumpCallAnalytics();
            param1Parcel2.writeNoException();
            if (telecomAnalytics != null) {
              param1Parcel2.writeInt(1);
              telecomAnalytics.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 24:
            telecomAnalytics.enforceInterface("com.android.internal.telecom.ITelecomService");
            str9 = getSystemDialerPackage();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str9);
            return true;
          case 23:
            str9.enforceInterface("com.android.internal.telecom.ITelecomService");
            j = str9.readInt();
            str9 = getDefaultDialerPackageForUser(j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str9);
            return true;
          case 22:
            str9.enforceInterface("com.android.internal.telecom.ITelecomService");
            str9 = getDefaultDialerPackage();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str9);
            return true;
          case 21:
            str9.enforceInterface("com.android.internal.telecom.ITelecomService");
            componentName = getDefaultPhoneApp();
            param1Parcel2.writeNoException();
            if (componentName != null) {
              param1Parcel2.writeInt(1);
              componentName.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 20:
            componentName.enforceInterface("com.android.internal.telecom.ITelecomService");
            if (componentName.readInt() != 0) {
              PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel((Parcel)componentName);
            } else {
              str13 = null;
            } 
            str14 = componentName.readString();
            str8 = componentName.readString();
            str8 = getLine1Number((PhoneAccountHandle)str13, str14, str8);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str8);
            return true;
          case 19:
            str8.enforceInterface("com.android.internal.telecom.ITelecomService");
            if (str8.readInt() != 0) {
              PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel((Parcel)str8);
            } else {
              str13 = null;
            } 
            str14 = str8.readString();
            str8 = str8.readString();
            str8 = getVoiceMailNumber((PhoneAccountHandle)str13, str14, str8);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str8);
            return true;
          case 18:
            str8.enforceInterface("com.android.internal.telecom.ITelecomService");
            if (str8.readInt() != 0) {
              PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel((Parcel)str8);
            } else {
              str13 = null;
            } 
            str15 = str8.readString();
            str14 = str8.readString();
            str8 = str8.readString();
            bool1 = isVoiceMailNumber((PhoneAccountHandle)str13, str15, str14, str8);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 17:
            str8.enforceInterface("com.android.internal.telecom.ITelecomService");
            str8 = str8.readString();
            clearAccounts(str8);
            param1Parcel2.writeNoException();
            return true;
          case 16:
            str8.enforceInterface("com.android.internal.telecom.ITelecomService");
            if (str8.readInt() != 0) {
              PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel((Parcel)str8);
            } else {
              str8 = null;
            } 
            unregisterPhoneAccount((PhoneAccountHandle)str8);
            param1Parcel2.writeNoException();
            return true;
          case 15:
            str8.enforceInterface("com.android.internal.telecom.ITelecomService");
            if (str8.readInt() != 0) {
              PhoneAccount phoneAccount1 = (PhoneAccount)PhoneAccount.CREATOR.createFromParcel((Parcel)str8);
            } else {
              str8 = null;
            } 
            registerPhoneAccount((PhoneAccount)str8);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            str8.enforceInterface("com.android.internal.telecom.ITelecomService");
            i = str8.readInt();
            phoneAccountHandle3 = getSimCallManagerForUser(i);
            param1Parcel2.writeNoException();
            if (phoneAccountHandle3 != null) {
              param1Parcel2.writeInt(1);
              phoneAccountHandle3.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 13:
            phoneAccountHandle3.enforceInterface("com.android.internal.telecom.ITelecomService");
            i = phoneAccountHandle3.readInt();
            phoneAccountHandle3 = getSimCallManager(i);
            param1Parcel2.writeNoException();
            if (phoneAccountHandle3 != null) {
              param1Parcel2.writeInt(1);
              phoneAccountHandle3.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 12:
            phoneAccountHandle3.enforceInterface("com.android.internal.telecom.ITelecomService");
            list5 = getAllPhoneAccountHandles();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list5);
            return true;
          case 11:
            list5.enforceInterface("com.android.internal.telecom.ITelecomService");
            list5 = (List)getAllPhoneAccounts();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list5);
            return true;
          case 10:
            list5.enforceInterface("com.android.internal.telecom.ITelecomService");
            i = getAllPhoneAccountsCount();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 9:
            list5.enforceInterface("com.android.internal.telecom.ITelecomService");
            if (list5.readInt() != 0) {
              PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel((Parcel)list5);
            } else {
              list5 = null;
            } 
            phoneAccount = getPhoneAccount((PhoneAccountHandle)list5);
            param1Parcel2.writeNoException();
            if (phoneAccount != null) {
              param1Parcel2.writeInt(1);
              phoneAccount.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 8:
            phoneAccount.enforceInterface("com.android.internal.telecom.ITelecomService");
            str7 = phoneAccount.readString();
            list4 = getPhoneAccountsForPackage(str7);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list4);
            return true;
          case 7:
            list4.enforceInterface("com.android.internal.telecom.ITelecomService");
            str13 = list4.readString();
            str6 = list4.readString();
            list3 = getPhoneAccountsSupportingScheme(str13, str6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list3);
            return true;
          case 6:
            list3.enforceInterface("com.android.internal.telecom.ITelecomService");
            str13 = list3.readString();
            str5 = list3.readString();
            list2 = getSelfManagedPhoneAccounts(str13, str5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list2);
            return true;
          case 5:
            list2.enforceInterface("com.android.internal.telecom.ITelecomService");
            bool10 = bool8;
            if (list2.readInt() != 0)
              bool10 = true; 
            str13 = list2.readString();
            str4 = list2.readString();
            list1 = getCallCapablePhoneAccounts(bool10, str13, str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list1);
            return true;
          case 4:
            list1.enforceInterface("com.android.internal.telecom.ITelecomService");
            if (list1.readInt() != 0) {
              PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel((Parcel)list1);
            } else {
              list1 = null;
            } 
            setUserSelectedOutgoingPhoneAccount((PhoneAccountHandle)list1);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            list1.enforceInterface("com.android.internal.telecom.ITelecomService");
            str3 = list1.readString();
            phoneAccountHandle2 = getUserSelectedOutgoingPhoneAccount(str3);
            param1Parcel2.writeNoException();
            if (phoneAccountHandle2 != null) {
              param1Parcel2.writeInt(1);
              phoneAccountHandle2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 2:
            phoneAccountHandle2.enforceInterface("com.android.internal.telecom.ITelecomService");
            str14 = phoneAccountHandle2.readString();
            str13 = phoneAccountHandle2.readString();
            str2 = phoneAccountHandle2.readString();
            phoneAccountHandle1 = getDefaultOutgoingPhoneAccount(str14, str13, str2);
            param1Parcel2.writeNoException();
            if (phoneAccountHandle1 != null) {
              param1Parcel2.writeInt(1);
              phoneAccountHandle1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 1:
            break;
        } 
        phoneAccountHandle1.enforceInterface("com.android.internal.telecom.ITelecomService");
        bool10 = bool9;
        if (phoneAccountHandle1.readInt() != 0)
          bool10 = true; 
        String str13 = phoneAccountHandle1.readString();
        String str1 = phoneAccountHandle1.readString();
        showInCallScreen(bool10, str13, str1);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("com.android.internal.telecom.ITelecomService");
      return true;
    }
    
    private static class Proxy implements ITelecomService {
      public static ITelecomService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telecom.ITelecomService";
      }
      
      public void showInCallScreen(boolean param2Boolean, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool1 = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool1 && ITelecomService.Stub.getDefaultImpl() != null) {
            ITelecomService.Stub.getDefaultImpl().showInCallScreen(param2Boolean, param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public PhoneAccountHandle getDefaultOutgoingPhoneAccount(String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null)
            return ITelecomService.Stub.getDefaultImpl().getDefaultOutgoingPhoneAccount(param2String1, param2String2, param2String3); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (PhoneAccountHandle)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public PhoneAccountHandle getUserSelectedOutgoingPhoneAccount(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null)
            return ITelecomService.Stub.getDefaultImpl().getUserSelectedOutgoingPhoneAccount(param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (PhoneAccountHandle)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUserSelectedOutgoingPhoneAccount(PhoneAccountHandle param2PhoneAccountHandle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          if (param2PhoneAccountHandle != null) {
            parcel1.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            ITelecomService.Stub.getDefaultImpl().setUserSelectedOutgoingPhoneAccount(param2PhoneAccountHandle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<PhoneAccountHandle> getCallCapablePhoneAccounts(boolean param2Boolean, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool1 = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool1 && ITelecomService.Stub.getDefaultImpl() != null)
            return ITelecomService.Stub.getDefaultImpl().getCallCapablePhoneAccounts(param2Boolean, param2String1, param2String2); 
          parcel2.readException();
          return parcel2.createTypedArrayList(PhoneAccountHandle.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<PhoneAccountHandle> getSelfManagedPhoneAccounts(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null)
            return ITelecomService.Stub.getDefaultImpl().getSelfManagedPhoneAccounts(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.createTypedArrayList(PhoneAccountHandle.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<PhoneAccountHandle> getPhoneAccountsSupportingScheme(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null)
            return ITelecomService.Stub.getDefaultImpl().getPhoneAccountsSupportingScheme(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.createTypedArrayList(PhoneAccountHandle.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<PhoneAccountHandle> getPhoneAccountsForPackage(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null)
            return ITelecomService.Stub.getDefaultImpl().getPhoneAccountsForPackage(param2String); 
          parcel2.readException();
          return parcel2.createTypedArrayList(PhoneAccountHandle.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public PhoneAccount getPhoneAccount(PhoneAccountHandle param2PhoneAccountHandle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          if (param2PhoneAccountHandle != null) {
            parcel1.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null)
            return ITelecomService.Stub.getDefaultImpl().getPhoneAccount(param2PhoneAccountHandle); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            PhoneAccount phoneAccount = (PhoneAccount)PhoneAccount.CREATOR.createFromParcel(parcel2);
          } else {
            param2PhoneAccountHandle = null;
          } 
          return (PhoneAccount)param2PhoneAccountHandle;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getAllPhoneAccountsCount() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null)
            return ITelecomService.Stub.getDefaultImpl().getAllPhoneAccountsCount(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<PhoneAccount> getAllPhoneAccounts() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null)
            return ITelecomService.Stub.getDefaultImpl().getAllPhoneAccounts(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(PhoneAccount.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<PhoneAccountHandle> getAllPhoneAccountHandles() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null)
            return ITelecomService.Stub.getDefaultImpl().getAllPhoneAccountHandles(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(PhoneAccountHandle.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public PhoneAccountHandle getSimCallManager(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          PhoneAccountHandle phoneAccountHandle;
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            phoneAccountHandle = ITelecomService.Stub.getDefaultImpl().getSimCallManager(param2Int);
            return phoneAccountHandle;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel(parcel2);
          } else {
            phoneAccountHandle = null;
          } 
          return phoneAccountHandle;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public PhoneAccountHandle getSimCallManagerForUser(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          PhoneAccountHandle phoneAccountHandle;
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            phoneAccountHandle = ITelecomService.Stub.getDefaultImpl().getSimCallManagerForUser(param2Int);
            return phoneAccountHandle;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel(parcel2);
          } else {
            phoneAccountHandle = null;
          } 
          return phoneAccountHandle;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerPhoneAccount(PhoneAccount param2PhoneAccount) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          if (param2PhoneAccount != null) {
            parcel1.writeInt(1);
            param2PhoneAccount.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            ITelecomService.Stub.getDefaultImpl().registerPhoneAccount(param2PhoneAccount);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterPhoneAccount(PhoneAccountHandle param2PhoneAccountHandle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          if (param2PhoneAccountHandle != null) {
            parcel1.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            ITelecomService.Stub.getDefaultImpl().unregisterPhoneAccount(param2PhoneAccountHandle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearAccounts(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            ITelecomService.Stub.getDefaultImpl().clearAccounts(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isVoiceMailNumber(PhoneAccountHandle param2PhoneAccountHandle, String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          boolean bool1 = true;
          if (param2PhoneAccountHandle != null) {
            parcel1.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool2 = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool2 && ITelecomService.Stub.getDefaultImpl() != null) {
            bool1 = ITelecomService.Stub.getDefaultImpl().isVoiceMailNumber(param2PhoneAccountHandle, param2String1, param2String2, param2String3);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getVoiceMailNumber(PhoneAccountHandle param2PhoneAccountHandle, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          if (param2PhoneAccountHandle != null) {
            parcel1.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null)
            return ITelecomService.Stub.getDefaultImpl().getVoiceMailNumber(param2PhoneAccountHandle, param2String1, param2String2); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getLine1Number(PhoneAccountHandle param2PhoneAccountHandle, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          if (param2PhoneAccountHandle != null) {
            parcel1.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null)
            return ITelecomService.Stub.getDefaultImpl().getLine1Number(param2PhoneAccountHandle, param2String1, param2String2); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ComponentName getDefaultPhoneApp() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ComponentName componentName;
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            componentName = ITelecomService.Stub.getDefaultImpl().getDefaultPhoneApp();
            return componentName;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            componentName = (ComponentName)ComponentName.CREATOR.createFromParcel(parcel2);
          } else {
            componentName = null;
          } 
          return componentName;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getDefaultDialerPackage() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null)
            return ITelecomService.Stub.getDefaultImpl().getDefaultDialerPackage(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getDefaultDialerPackageForUser(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null)
            return ITelecomService.Stub.getDefaultImpl().getDefaultDialerPackageForUser(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getSystemDialerPackage() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null)
            return ITelecomService.Stub.getDefaultImpl().getSystemDialerPackage(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public TelecomAnalytics dumpCallAnalytics() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          TelecomAnalytics telecomAnalytics;
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            telecomAnalytics = ITelecomService.Stub.getDefaultImpl().dumpCallAnalytics();
            return telecomAnalytics;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            telecomAnalytics = (TelecomAnalytics)TelecomAnalytics.CREATOR.createFromParcel(parcel2);
          } else {
            telecomAnalytics = null;
          } 
          return telecomAnalytics;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void silenceRinger(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            ITelecomService.Stub.getDefaultImpl().silenceRinger(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isInCall(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(27, parcel1, parcel2, 0);
          if (!bool2 && ITelecomService.Stub.getDefaultImpl() != null) {
            bool1 = ITelecomService.Stub.getDefaultImpl().isInCall(param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isInManagedCall(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(28, parcel1, parcel2, 0);
          if (!bool2 && ITelecomService.Stub.getDefaultImpl() != null) {
            bool1 = ITelecomService.Stub.getDefaultImpl().isInManagedCall(param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isRinging(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(29, parcel1, parcel2, 0);
          if (!bool2 && ITelecomService.Stub.getDefaultImpl() != null) {
            bool1 = ITelecomService.Stub.getDefaultImpl().isRinging(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getCallState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null)
            return ITelecomService.Stub.getDefaultImpl().getCallState(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean endCall(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(31, parcel1, parcel2, 0);
          if (!bool2 && ITelecomService.Stub.getDefaultImpl() != null) {
            bool1 = ITelecomService.Stub.getDefaultImpl().endCall(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void acceptRingingCall(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            ITelecomService.Stub.getDefaultImpl().acceptRingingCall(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void acceptRingingCallWithVideoState(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            ITelecomService.Stub.getDefaultImpl().acceptRingingCallWithVideoState(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cancelMissedCallsNotification(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            ITelecomService.Stub.getDefaultImpl().cancelMissedCallsNotification(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean handlePinMmi(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(35, parcel1, parcel2, 0);
          if (!bool2 && ITelecomService.Stub.getDefaultImpl() != null) {
            bool1 = ITelecomService.Stub.getDefaultImpl().handlePinMmi(param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean handlePinMmiForPhoneAccount(PhoneAccountHandle param2PhoneAccountHandle, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          boolean bool1 = true;
          if (param2PhoneAccountHandle != null) {
            parcel1.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool2 = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool2 && ITelecomService.Stub.getDefaultImpl() != null) {
            bool1 = ITelecomService.Stub.getDefaultImpl().handlePinMmiForPhoneAccount(param2PhoneAccountHandle, param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Uri getAdnUriForPhoneAccount(PhoneAccountHandle param2PhoneAccountHandle, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          if (param2PhoneAccountHandle != null) {
            parcel1.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null)
            return ITelecomService.Stub.getDefaultImpl().getAdnUriForPhoneAccount(param2PhoneAccountHandle, param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Uri uri = (Uri)Uri.CREATOR.createFromParcel(parcel2);
          } else {
            param2PhoneAccountHandle = null;
          } 
          return (Uri)param2PhoneAccountHandle;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isTtySupported(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(38, parcel1, parcel2, 0);
          if (!bool2 && ITelecomService.Stub.getDefaultImpl() != null) {
            bool1 = ITelecomService.Stub.getDefaultImpl().isTtySupported(param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getCurrentTtyMode(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null)
            return ITelecomService.Stub.getDefaultImpl().getCurrentTtyMode(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addNewIncomingCall(PhoneAccountHandle param2PhoneAccountHandle, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          if (param2PhoneAccountHandle != null) {
            parcel1.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(40, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            ITelecomService.Stub.getDefaultImpl().addNewIncomingCall(param2PhoneAccountHandle, param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addNewIncomingConference(PhoneAccountHandle param2PhoneAccountHandle, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          if (param2PhoneAccountHandle != null) {
            parcel1.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(41, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            ITelecomService.Stub.getDefaultImpl().addNewIncomingConference(param2PhoneAccountHandle, param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addNewUnknownCall(PhoneAccountHandle param2PhoneAccountHandle, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          if (param2PhoneAccountHandle != null) {
            parcel1.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(42, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            ITelecomService.Stub.getDefaultImpl().addNewUnknownCall(param2PhoneAccountHandle, param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startConference(List<Uri> param2List, Bundle param2Bundle, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeTypedList(param2List);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(43, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            ITelecomService.Stub.getDefaultImpl().startConference(param2List, param2Bundle, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void placeCall(Uri param2Uri, Bundle param2Bundle, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(44, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            ITelecomService.Stub.getDefaultImpl().placeCall(param2Uri, param2Bundle, param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean enablePhoneAccount(PhoneAccountHandle param2PhoneAccountHandle, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          boolean bool = true;
          if (param2PhoneAccountHandle != null) {
            parcel1.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(45, parcel1, parcel2, 0);
          if (!bool1 && ITelecomService.Stub.getDefaultImpl() != null) {
            param2Boolean = ITelecomService.Stub.getDefaultImpl().enablePhoneAccount(param2PhoneAccountHandle, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setDefaultDialer(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(46, parcel1, parcel2, 0);
          if (!bool2 && ITelecomService.Stub.getDefaultImpl() != null) {
            bool1 = ITelecomService.Stub.getDefaultImpl().setDefaultDialer(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopBlockSuppression() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          boolean bool = this.mRemote.transact(47, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            ITelecomService.Stub.getDefaultImpl().stopBlockSuppression();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Intent createManageBlockedNumbersIntent() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Intent intent;
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          boolean bool = this.mRemote.transact(48, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            intent = ITelecomService.Stub.getDefaultImpl().createManageBlockedNumbersIntent();
            return intent;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            intent = (Intent)Intent.CREATOR.createFromParcel(parcel2);
          } else {
            intent = null;
          } 
          return intent;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Intent createLaunchEmergencyDialerIntent(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(49, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null)
            return ITelecomService.Stub.getDefaultImpl().createLaunchEmergencyDialerIntent(param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Intent intent = (Intent)Intent.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (Intent)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isIncomingCallPermitted(PhoneAccountHandle param2PhoneAccountHandle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          boolean bool1 = true;
          if (param2PhoneAccountHandle != null) {
            parcel1.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(50, parcel1, parcel2, 0);
          if (!bool2 && ITelecomService.Stub.getDefaultImpl() != null) {
            bool1 = ITelecomService.Stub.getDefaultImpl().isIncomingCallPermitted(param2PhoneAccountHandle);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isOutgoingCallPermitted(PhoneAccountHandle param2PhoneAccountHandle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          boolean bool1 = true;
          if (param2PhoneAccountHandle != null) {
            parcel1.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(51, parcel1, parcel2, 0);
          if (!bool2 && ITelecomService.Stub.getDefaultImpl() != null) {
            bool1 = ITelecomService.Stub.getDefaultImpl().isOutgoingCallPermitted(param2PhoneAccountHandle);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void waitOnHandlers() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          boolean bool = this.mRemote.transact(52, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            ITelecomService.Stub.getDefaultImpl().waitOnHandlers();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void acceptHandover(Uri param2Uri, int param2Int, PhoneAccountHandle param2PhoneAccountHandle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          if (param2PhoneAccountHandle != null) {
            parcel1.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(53, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            ITelecomService.Stub.getDefaultImpl().acceptHandover(param2Uri, param2Int, param2PhoneAccountHandle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setTestEmergencyPhoneAccountPackageNameFilter(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(54, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            ITelecomService.Stub.getDefaultImpl().setTestEmergencyPhoneAccountPackageNameFilter(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isInEmergencyCall() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(55, parcel1, parcel2, 0);
          if (!bool2 && ITelecomService.Stub.getDefaultImpl() != null) {
            bool1 = ITelecomService.Stub.getDefaultImpl().isInEmergencyCall();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void handleCallIntent(Intent param2Intent, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(56, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            ITelecomService.Stub.getDefaultImpl().handleCallIntent(param2Intent, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setTestDefaultCallRedirectionApp(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(57, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            ITelecomService.Stub.getDefaultImpl().setTestDefaultCallRedirectionApp(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setTestPhoneAcctSuggestionComponent(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(58, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            ITelecomService.Stub.getDefaultImpl().setTestPhoneAcctSuggestionComponent(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setTestDefaultCallScreeningApp(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(59, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            ITelecomService.Stub.getDefaultImpl().setTestDefaultCallScreeningApp(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addOrRemoveTestCallCompanionApp(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(60, parcel1, parcel2, 0);
          if (!bool1 && ITelecomService.Stub.getDefaultImpl() != null) {
            ITelecomService.Stub.getDefaultImpl().addOrRemoveTestCallCompanionApp(param2String, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String oplusInteractWithTelecomService(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(61, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            param2String = ITelecomService.Stub.getDefaultImpl().oplusInteractWithTelecomService(param2Int, param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addNewOutgoingCall(Intent param2Intent) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(62, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            ITelecomService.Stub.getDefaultImpl().addNewOutgoingCall(param2Intent);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void oplusCancelMissedCallsNotification(String param2String, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeString(param2String);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(63, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            ITelecomService.Stub.getDefaultImpl().oplusCancelMissedCallsNotification(param2String, param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSystemDialer(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(64, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            ITelecomService.Stub.getDefaultImpl().setSystemDialer(param2ComponentName);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setTestDefaultDialer(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("com.android.internal.telecom.ITelecomService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(65, parcel1, parcel2, 0);
          if (!bool && ITelecomService.Stub.getDefaultImpl() != null) {
            ITelecomService.Stub.getDefaultImpl().setTestDefaultDialer(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITelecomService param1ITelecomService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITelecomService != null) {
          Proxy.sDefaultImpl = param1ITelecomService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITelecomService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
