package com.android.internal.telecom;

import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telecom.PhoneAccountHandle;

public interface ICallRedirectionAdapter extends IInterface {
  void cancelCall() throws RemoteException;
  
  void placeCallUnmodified() throws RemoteException;
  
  void redirectCall(Uri paramUri, PhoneAccountHandle paramPhoneAccountHandle, boolean paramBoolean) throws RemoteException;
  
  class Default implements ICallRedirectionAdapter {
    public void cancelCall() throws RemoteException {}
    
    public void placeCallUnmodified() throws RemoteException {}
    
    public void redirectCall(Uri param1Uri, PhoneAccountHandle param1PhoneAccountHandle, boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICallRedirectionAdapter {
    private static final String DESCRIPTOR = "com.android.internal.telecom.ICallRedirectionAdapter";
    
    static final int TRANSACTION_cancelCall = 1;
    
    static final int TRANSACTION_placeCallUnmodified = 2;
    
    static final int TRANSACTION_redirectCall = 3;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telecom.ICallRedirectionAdapter");
    }
    
    public static ICallRedirectionAdapter asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telecom.ICallRedirectionAdapter");
      if (iInterface != null && iInterface instanceof ICallRedirectionAdapter)
        return (ICallRedirectionAdapter)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "redirectCall";
        } 
        return "placeCallUnmodified";
      } 
      return "cancelCall";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          PhoneAccountHandle phoneAccountHandle;
          boolean bool;
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("com.android.internal.telecom.ICallRedirectionAdapter");
            return true;
          } 
          param1Parcel1.enforceInterface("com.android.internal.telecom.ICallRedirectionAdapter");
          if (param1Parcel1.readInt() != 0) {
            Uri uri = (Uri)Uri.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel2 = null;
          } 
          if (param1Parcel1.readInt() != 0) {
            phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel(param1Parcel1);
          } else {
            phoneAccountHandle = null;
          } 
          if (param1Parcel1.readInt() != 0) {
            bool = true;
          } else {
            bool = false;
          } 
          redirectCall((Uri)param1Parcel2, phoneAccountHandle, bool);
          return true;
        } 
        param1Parcel1.enforceInterface("com.android.internal.telecom.ICallRedirectionAdapter");
        placeCallUnmodified();
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.telecom.ICallRedirectionAdapter");
      cancelCall();
      return true;
    }
    
    private static class Proxy implements ICallRedirectionAdapter {
      public static ICallRedirectionAdapter sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telecom.ICallRedirectionAdapter";
      }
      
      public void cancelCall() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.ICallRedirectionAdapter");
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && ICallRedirectionAdapter.Stub.getDefaultImpl() != null) {
            ICallRedirectionAdapter.Stub.getDefaultImpl().cancelCall();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void placeCallUnmodified() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.ICallRedirectionAdapter");
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && ICallRedirectionAdapter.Stub.getDefaultImpl() != null) {
            ICallRedirectionAdapter.Stub.getDefaultImpl().placeCallUnmodified();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void redirectCall(Uri param2Uri, PhoneAccountHandle param2PhoneAccountHandle, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.ICallRedirectionAdapter");
          boolean bool = false;
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2PhoneAccountHandle != null) {
            parcel.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean)
            bool = true; 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(3, parcel, null, 1);
          if (!bool1 && ICallRedirectionAdapter.Stub.getDefaultImpl() != null) {
            ICallRedirectionAdapter.Stub.getDefaultImpl().redirectCall(param2Uri, param2PhoneAccountHandle, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICallRedirectionAdapter param1ICallRedirectionAdapter) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICallRedirectionAdapter != null) {
          Proxy.sDefaultImpl = param1ICallRedirectionAdapter;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICallRedirectionAdapter getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
