package com.android.internal.telecom;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telecom.ParcelableCall;

public interface ICallScreeningService extends IInterface {
  void screenCall(ICallScreeningAdapter paramICallScreeningAdapter, ParcelableCall paramParcelableCall) throws RemoteException;
  
  class Default implements ICallScreeningService {
    public void screenCall(ICallScreeningAdapter param1ICallScreeningAdapter, ParcelableCall param1ParcelableCall) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICallScreeningService {
    private static final String DESCRIPTOR = "com.android.internal.telecom.ICallScreeningService";
    
    static final int TRANSACTION_screenCall = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telecom.ICallScreeningService");
    }
    
    public static ICallScreeningService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telecom.ICallScreeningService");
      if (iInterface != null && iInterface instanceof ICallScreeningService)
        return (ICallScreeningService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "screenCall";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.telecom.ICallScreeningService");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.telecom.ICallScreeningService");
      ICallScreeningAdapter iCallScreeningAdapter = ICallScreeningAdapter.Stub.asInterface(param1Parcel1.readStrongBinder());
      if (param1Parcel1.readInt() != 0) {
        ParcelableCall parcelableCall = (ParcelableCall)ParcelableCall.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      screenCall(iCallScreeningAdapter, (ParcelableCall)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements ICallScreeningService {
      public static ICallScreeningService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telecom.ICallScreeningService";
      }
      
      public void screenCall(ICallScreeningAdapter param2ICallScreeningAdapter, ParcelableCall param2ParcelableCall) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telecom.ICallScreeningService");
          if (param2ICallScreeningAdapter != null) {
            iBinder = param2ICallScreeningAdapter.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2ParcelableCall != null) {
            parcel.writeInt(1);
            param2ParcelableCall.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && ICallScreeningService.Stub.getDefaultImpl() != null) {
            ICallScreeningService.Stub.getDefaultImpl().screenCall(param2ICallScreeningAdapter, param2ParcelableCall);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICallScreeningService param1ICallScreeningService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICallScreeningService != null) {
          Proxy.sDefaultImpl = param1ICallScreeningService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICallScreeningService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
