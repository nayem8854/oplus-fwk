package com.android.internal.telecom;

import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telecom.PhoneAccountHandle;

public interface ICallRedirectionService extends IInterface {
  void placeCall(ICallRedirectionAdapter paramICallRedirectionAdapter, Uri paramUri, PhoneAccountHandle paramPhoneAccountHandle, boolean paramBoolean) throws RemoteException;
  
  class Default implements ICallRedirectionService {
    public void placeCall(ICallRedirectionAdapter param1ICallRedirectionAdapter, Uri param1Uri, PhoneAccountHandle param1PhoneAccountHandle, boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICallRedirectionService {
    private static final String DESCRIPTOR = "com.android.internal.telecom.ICallRedirectionService";
    
    static final int TRANSACTION_placeCall = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telecom.ICallRedirectionService");
    }
    
    public static ICallRedirectionService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telecom.ICallRedirectionService");
      if (iInterface != null && iInterface instanceof ICallRedirectionService)
        return (ICallRedirectionService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "placeCall";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      PhoneAccountHandle phoneAccountHandle;
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.telecom.ICallRedirectionService");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.telecom.ICallRedirectionService");
      ICallRedirectionAdapter iCallRedirectionAdapter = ICallRedirectionAdapter.Stub.asInterface(param1Parcel1.readStrongBinder());
      if (param1Parcel1.readInt() != 0) {
        Uri uri = (Uri)Uri.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      if (param1Parcel1.readInt() != 0) {
        phoneAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel(param1Parcel1);
      } else {
        phoneAccountHandle = null;
      } 
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      placeCall(iCallRedirectionAdapter, (Uri)param1Parcel2, phoneAccountHandle, bool);
      return true;
    }
    
    private static class Proxy implements ICallRedirectionService {
      public static ICallRedirectionService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telecom.ICallRedirectionService";
      }
      
      public void placeCall(ICallRedirectionAdapter param2ICallRedirectionAdapter, Uri param2Uri, PhoneAccountHandle param2PhoneAccountHandle, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telecom.ICallRedirectionService");
          if (param2ICallRedirectionAdapter != null) {
            iBinder = param2ICallRedirectionAdapter.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = false;
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2PhoneAccountHandle != null) {
            parcel.writeInt(1);
            param2PhoneAccountHandle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean)
            bool = true; 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, null, 1);
          if (!bool1 && ICallRedirectionService.Stub.getDefaultImpl() != null) {
            ICallRedirectionService.Stub.getDefaultImpl().placeCall(param2ICallRedirectionAdapter, param2Uri, param2PhoneAccountHandle, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICallRedirectionService param1ICallRedirectionService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICallRedirectionService != null) {
          Proxy.sDefaultImpl = param1ICallRedirectionService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICallRedirectionService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
