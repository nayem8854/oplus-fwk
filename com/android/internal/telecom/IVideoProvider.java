package com.android.internal.telecom;

import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.telecom.VideoProfile;
import android.view.Surface;

public interface IVideoProvider extends IInterface {
  void addVideoCallback(IBinder paramIBinder) throws RemoteException;
  
  void removeVideoCallback(IBinder paramIBinder) throws RemoteException;
  
  void requestCallDataUsage() throws RemoteException;
  
  void requestCameraCapabilities() throws RemoteException;
  
  void sendSessionModifyRequest(VideoProfile paramVideoProfile1, VideoProfile paramVideoProfile2) throws RemoteException;
  
  void sendSessionModifyResponse(VideoProfile paramVideoProfile) throws RemoteException;
  
  void setCamera(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  void setDeviceOrientation(int paramInt) throws RemoteException;
  
  void setDisplaySurface(Surface paramSurface) throws RemoteException;
  
  void setPauseImage(Uri paramUri) throws RemoteException;
  
  void setPreviewSurface(Surface paramSurface) throws RemoteException;
  
  void setZoom(float paramFloat) throws RemoteException;
  
  class Default implements IVideoProvider {
    public void addVideoCallback(IBinder param1IBinder) throws RemoteException {}
    
    public void removeVideoCallback(IBinder param1IBinder) throws RemoteException {}
    
    public void setCamera(String param1String1, String param1String2, int param1Int) throws RemoteException {}
    
    public void setPreviewSurface(Surface param1Surface) throws RemoteException {}
    
    public void setDisplaySurface(Surface param1Surface) throws RemoteException {}
    
    public void setDeviceOrientation(int param1Int) throws RemoteException {}
    
    public void setZoom(float param1Float) throws RemoteException {}
    
    public void sendSessionModifyRequest(VideoProfile param1VideoProfile1, VideoProfile param1VideoProfile2) throws RemoteException {}
    
    public void sendSessionModifyResponse(VideoProfile param1VideoProfile) throws RemoteException {}
    
    public void requestCameraCapabilities() throws RemoteException {}
    
    public void requestCallDataUsage() throws RemoteException {}
    
    public void setPauseImage(Uri param1Uri) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IVideoProvider {
    private static final String DESCRIPTOR = "com.android.internal.telecom.IVideoProvider";
    
    static final int TRANSACTION_addVideoCallback = 1;
    
    static final int TRANSACTION_removeVideoCallback = 2;
    
    static final int TRANSACTION_requestCallDataUsage = 11;
    
    static final int TRANSACTION_requestCameraCapabilities = 10;
    
    static final int TRANSACTION_sendSessionModifyRequest = 8;
    
    static final int TRANSACTION_sendSessionModifyResponse = 9;
    
    static final int TRANSACTION_setCamera = 3;
    
    static final int TRANSACTION_setDeviceOrientation = 6;
    
    static final int TRANSACTION_setDisplaySurface = 5;
    
    static final int TRANSACTION_setPauseImage = 12;
    
    static final int TRANSACTION_setPreviewSurface = 4;
    
    static final int TRANSACTION_setZoom = 7;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telecom.IVideoProvider");
    }
    
    public static IVideoProvider asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telecom.IVideoProvider");
      if (iInterface != null && iInterface instanceof IVideoProvider)
        return (IVideoProvider)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 12:
          return "setPauseImage";
        case 11:
          return "requestCallDataUsage";
        case 10:
          return "requestCameraCapabilities";
        case 9:
          return "sendSessionModifyResponse";
        case 8:
          return "sendSessionModifyRequest";
        case 7:
          return "setZoom";
        case 6:
          return "setDeviceOrientation";
        case 5:
          return "setDisplaySurface";
        case 4:
          return "setPreviewSurface";
        case 3:
          return "setCamera";
        case 2:
          return "removeVideoCallback";
        case 1:
          break;
      } 
      return "addVideoCallback";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        float f;
        String str1;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 12:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IVideoProvider");
            if (param1Parcel1.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setPauseImage((Uri)param1Parcel1);
            return true;
          case 11:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IVideoProvider");
            requestCallDataUsage();
            return true;
          case 10:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IVideoProvider");
            requestCameraCapabilities();
            return true;
          case 9:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IVideoProvider");
            if (param1Parcel1.readInt() != 0) {
              VideoProfile videoProfile = (VideoProfile)VideoProfile.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            sendSessionModifyResponse((VideoProfile)param1Parcel1);
            return true;
          case 8:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IVideoProvider");
            if (param1Parcel1.readInt() != 0) {
              VideoProfile videoProfile = (VideoProfile)VideoProfile.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              VideoProfile videoProfile = (VideoProfile)VideoProfile.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            sendSessionModifyRequest((VideoProfile)param1Parcel2, (VideoProfile)param1Parcel1);
            return true;
          case 7:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IVideoProvider");
            f = param1Parcel1.readFloat();
            setZoom(f);
            return true;
          case 6:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IVideoProvider");
            param1Int1 = param1Parcel1.readInt();
            setDeviceOrientation(param1Int1);
            return true;
          case 5:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IVideoProvider");
            if (param1Parcel1.readInt() != 0) {
              Surface surface = (Surface)Surface.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setDisplaySurface((Surface)param1Parcel1);
            return true;
          case 4:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IVideoProvider");
            if (param1Parcel1.readInt() != 0) {
              Surface surface = (Surface)Surface.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setPreviewSurface((Surface)param1Parcel1);
            return true;
          case 3:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IVideoProvider");
            str = param1Parcel1.readString();
            str1 = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            setCamera(str, str1, param1Int1);
            return true;
          case 2:
            param1Parcel1.enforceInterface("com.android.internal.telecom.IVideoProvider");
            iBinder = param1Parcel1.readStrongBinder();
            removeVideoCallback(iBinder);
            return true;
          case 1:
            break;
        } 
        iBinder.enforceInterface("com.android.internal.telecom.IVideoProvider");
        IBinder iBinder = iBinder.readStrongBinder();
        addVideoCallback(iBinder);
        return true;
      } 
      str.writeString("com.android.internal.telecom.IVideoProvider");
      return true;
    }
    
    private static class Proxy implements IVideoProvider {
      public static IVideoProvider sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telecom.IVideoProvider";
      }
      
      public void addVideoCallback(IBinder param2IBinder) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IVideoProvider");
          parcel.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IVideoProvider.Stub.getDefaultImpl() != null) {
            IVideoProvider.Stub.getDefaultImpl().addVideoCallback(param2IBinder);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void removeVideoCallback(IBinder param2IBinder) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IVideoProvider");
          parcel.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IVideoProvider.Stub.getDefaultImpl() != null) {
            IVideoProvider.Stub.getDefaultImpl().removeVideoCallback(param2IBinder);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setCamera(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IVideoProvider");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IVideoProvider.Stub.getDefaultImpl() != null) {
            IVideoProvider.Stub.getDefaultImpl().setCamera(param2String1, param2String2, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setPreviewSurface(Surface param2Surface) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IVideoProvider");
          if (param2Surface != null) {
            parcel.writeInt(1);
            param2Surface.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IVideoProvider.Stub.getDefaultImpl() != null) {
            IVideoProvider.Stub.getDefaultImpl().setPreviewSurface(param2Surface);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setDisplaySurface(Surface param2Surface) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IVideoProvider");
          if (param2Surface != null) {
            parcel.writeInt(1);
            param2Surface.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IVideoProvider.Stub.getDefaultImpl() != null) {
            IVideoProvider.Stub.getDefaultImpl().setDisplaySurface(param2Surface);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setDeviceOrientation(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IVideoProvider");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IVideoProvider.Stub.getDefaultImpl() != null) {
            IVideoProvider.Stub.getDefaultImpl().setDeviceOrientation(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setZoom(float param2Float) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IVideoProvider");
          parcel.writeFloat(param2Float);
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IVideoProvider.Stub.getDefaultImpl() != null) {
            IVideoProvider.Stub.getDefaultImpl().setZoom(param2Float);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void sendSessionModifyRequest(VideoProfile param2VideoProfile1, VideoProfile param2VideoProfile2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IVideoProvider");
          if (param2VideoProfile1 != null) {
            parcel.writeInt(1);
            param2VideoProfile1.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2VideoProfile2 != null) {
            parcel.writeInt(1);
            param2VideoProfile2.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IVideoProvider.Stub.getDefaultImpl() != null) {
            IVideoProvider.Stub.getDefaultImpl().sendSessionModifyRequest(param2VideoProfile1, param2VideoProfile2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void sendSessionModifyResponse(VideoProfile param2VideoProfile) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IVideoProvider");
          if (param2VideoProfile != null) {
            parcel.writeInt(1);
            param2VideoProfile.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IVideoProvider.Stub.getDefaultImpl() != null) {
            IVideoProvider.Stub.getDefaultImpl().sendSessionModifyResponse(param2VideoProfile);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void requestCameraCapabilities() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IVideoProvider");
          boolean bool = this.mRemote.transact(10, parcel, null, 1);
          if (!bool && IVideoProvider.Stub.getDefaultImpl() != null) {
            IVideoProvider.Stub.getDefaultImpl().requestCameraCapabilities();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void requestCallDataUsage() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IVideoProvider");
          boolean bool = this.mRemote.transact(11, parcel, null, 1);
          if (!bool && IVideoProvider.Stub.getDefaultImpl() != null) {
            IVideoProvider.Stub.getDefaultImpl().requestCallDataUsage();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setPauseImage(Uri param2Uri) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("com.android.internal.telecom.IVideoProvider");
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(12, parcel, null, 1);
          if (!bool && IVideoProvider.Stub.getDefaultImpl() != null) {
            IVideoProvider.Stub.getDefaultImpl().setPauseImage(param2Uri);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IVideoProvider param1IVideoProvider) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IVideoProvider != null) {
          Proxy.sDefaultImpl = param1IVideoProvider;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IVideoProvider getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
