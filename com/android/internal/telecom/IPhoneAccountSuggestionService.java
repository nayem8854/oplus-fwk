package com.android.internal.telecom;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IPhoneAccountSuggestionService extends IInterface {
  void onAccountSuggestionRequest(IPhoneAccountSuggestionCallback paramIPhoneAccountSuggestionCallback, String paramString) throws RemoteException;
  
  class Default implements IPhoneAccountSuggestionService {
    public void onAccountSuggestionRequest(IPhoneAccountSuggestionCallback param1IPhoneAccountSuggestionCallback, String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPhoneAccountSuggestionService {
    private static final String DESCRIPTOR = "com.android.internal.telecom.IPhoneAccountSuggestionService";
    
    static final int TRANSACTION_onAccountSuggestionRequest = 1;
    
    public Stub() {
      attachInterface(this, "com.android.internal.telecom.IPhoneAccountSuggestionService");
    }
    
    public static IPhoneAccountSuggestionService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("com.android.internal.telecom.IPhoneAccountSuggestionService");
      if (iInterface != null && iInterface instanceof IPhoneAccountSuggestionService)
        return (IPhoneAccountSuggestionService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onAccountSuggestionRequest";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("com.android.internal.telecom.IPhoneAccountSuggestionService");
        return true;
      } 
      param1Parcel1.enforceInterface("com.android.internal.telecom.IPhoneAccountSuggestionService");
      IPhoneAccountSuggestionCallback iPhoneAccountSuggestionCallback = IPhoneAccountSuggestionCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
      String str = param1Parcel1.readString();
      onAccountSuggestionRequest(iPhoneAccountSuggestionCallback, str);
      return true;
    }
    
    private static class Proxy implements IPhoneAccountSuggestionService {
      public static IPhoneAccountSuggestionService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "com.android.internal.telecom.IPhoneAccountSuggestionService";
      }
      
      public void onAccountSuggestionRequest(IPhoneAccountSuggestionCallback param2IPhoneAccountSuggestionCallback, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("com.android.internal.telecom.IPhoneAccountSuggestionService");
          if (param2IPhoneAccountSuggestionCallback != null) {
            iBinder = param2IPhoneAccountSuggestionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IPhoneAccountSuggestionService.Stub.getDefaultImpl() != null) {
            IPhoneAccountSuggestionService.Stub.getDefaultImpl().onAccountSuggestionRequest(param2IPhoneAccountSuggestionCallback, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPhoneAccountSuggestionService param1IPhoneAccountSuggestionService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPhoneAccountSuggestionService != null) {
          Proxy.sDefaultImpl = param1IPhoneAccountSuggestionService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPhoneAccountSuggestionService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
